using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.EasyFlow.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Employee class.
	/// </summary>
    [Serializable]
	public partial class EmployeeCollection : ActiveList<Employee, EmployeeCollection>
	{	   
		public EmployeeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EmployeeCollection</returns>
		public EmployeeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Employee o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the Employee table.
	/// </summary>
	[Serializable]
	public partial class Employee : ActiveRecord<Employee>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public Employee()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Employee(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public Employee(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public Employee(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("Employee", TableType.Table, DataService.GetInstance("EasyFlowDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIsTowner = new TableSchema.TableColumn(schema);
				colvarIsTowner.ColumnName = "IsTowner";
				colvarIsTowner.DataType = DbType.String;
				colvarIsTowner.MaxLength = 200;
				colvarIsTowner.AutoIncrement = false;
				colvarIsTowner.IsNullable = true;
				colvarIsTowner.IsPrimaryKey = false;
				colvarIsTowner.IsForeignKey = false;
				colvarIsTowner.IsReadOnly = false;
				colvarIsTowner.DefaultSetting = @"";
				colvarIsTowner.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTowner);
				
				TableSchema.TableColumn colvarSource = new TableSchema.TableColumn(schema);
				colvarSource.ColumnName = "Source";
				colvarSource.DataType = DbType.String;
				colvarSource.MaxLength = 200;
				colvarSource.AutoIncrement = false;
				colvarSource.IsNullable = true;
				colvarSource.IsPrimaryKey = false;
				colvarSource.IsForeignKey = false;
				colvarSource.IsReadOnly = false;
				colvarSource.DefaultSetting = @"";
				colvarSource.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSource);
				
				TableSchema.TableColumn colvarFundBeginDate = new TableSchema.TableColumn(schema);
				colvarFundBeginDate.ColumnName = "FundBeginDate";
				colvarFundBeginDate.DataType = DbType.DateTime;
				colvarFundBeginDate.MaxLength = 0;
				colvarFundBeginDate.AutoIncrement = false;
				colvarFundBeginDate.IsNullable = true;
				colvarFundBeginDate.IsPrimaryKey = false;
				colvarFundBeginDate.IsForeignKey = false;
				colvarFundBeginDate.IsReadOnly = false;
				colvarFundBeginDate.DefaultSetting = @"";
				colvarFundBeginDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFundBeginDate);
				
				TableSchema.TableColumn colvarFundEndDate = new TableSchema.TableColumn(schema);
				colvarFundEndDate.ColumnName = "FundEndDate";
				colvarFundEndDate.DataType = DbType.DateTime;
				colvarFundEndDate.MaxLength = 0;
				colvarFundEndDate.AutoIncrement = false;
				colvarFundEndDate.IsNullable = true;
				colvarFundEndDate.IsPrimaryKey = false;
				colvarFundEndDate.IsForeignKey = false;
				colvarFundEndDate.IsReadOnly = false;
				colvarFundEndDate.DefaultSetting = @"";
				colvarFundEndDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFundEndDate);
				
				TableSchema.TableColumn colvarIsFundId = new TableSchema.TableColumn(schema);
				colvarIsFundId.ColumnName = "IsFundId";
				colvarIsFundId.DataType = DbType.String;
				colvarIsFundId.MaxLength = 200;
				colvarIsFundId.AutoIncrement = false;
				colvarIsFundId.IsNullable = true;
				colvarIsFundId.IsPrimaryKey = false;
				colvarIsFundId.IsForeignKey = false;
				colvarIsFundId.IsReadOnly = false;
				colvarIsFundId.DefaultSetting = @"";
				colvarIsFundId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsFundId);
				
				TableSchema.TableColumn colvarIsAuthorized = new TableSchema.TableColumn(schema);
				colvarIsAuthorized.ColumnName = "IsAuthorized";
				colvarIsAuthorized.DataType = DbType.String;
				colvarIsAuthorized.MaxLength = 200;
				colvarIsAuthorized.AutoIncrement = false;
				colvarIsAuthorized.IsNullable = true;
				colvarIsAuthorized.IsPrimaryKey = false;
				colvarIsAuthorized.IsForeignKey = false;
				colvarIsAuthorized.IsReadOnly = false;
				colvarIsAuthorized.DefaultSetting = @"";
				colvarIsAuthorized.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsAuthorized);
				
				TableSchema.TableColumn colvarIsReserveId = new TableSchema.TableColumn(schema);
				colvarIsReserveId.ColumnName = "IsReserveId";
				colvarIsReserveId.DataType = DbType.String;
				colvarIsReserveId.MaxLength = 200;
				colvarIsReserveId.AutoIncrement = false;
				colvarIsReserveId.IsNullable = true;
				colvarIsReserveId.IsPrimaryKey = false;
				colvarIsReserveId.IsForeignKey = false;
				colvarIsReserveId.IsReadOnly = false;
				colvarIsReserveId.DefaultSetting = @"";
				colvarIsReserveId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReserveId);
				
				TableSchema.TableColumn colvarInnerEmployee = new TableSchema.TableColumn(schema);
				colvarInnerEmployee.ColumnName = "InnerEmployee";
				colvarInnerEmployee.DataType = DbType.String;
				colvarInnerEmployee.MaxLength = 200;
				colvarInnerEmployee.AutoIncrement = false;
				colvarInnerEmployee.IsNullable = true;
				colvarInnerEmployee.IsPrimaryKey = false;
				colvarInnerEmployee.IsForeignKey = false;
				colvarInnerEmployee.IsReadOnly = false;
				colvarInnerEmployee.DefaultSetting = @"";
				colvarInnerEmployee.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInnerEmployee);
				
				TableSchema.TableColumn colvarCertificateLimit = new TableSchema.TableColumn(schema);
				colvarCertificateLimit.ColumnName = "CertificateLimit";
				colvarCertificateLimit.DataType = DbType.DateTime;
				colvarCertificateLimit.MaxLength = 0;
				colvarCertificateLimit.AutoIncrement = false;
				colvarCertificateLimit.IsNullable = true;
				colvarCertificateLimit.IsPrimaryKey = false;
				colvarCertificateLimit.IsForeignKey = false;
				colvarCertificateLimit.IsReadOnly = false;
				colvarCertificateLimit.DefaultSetting = @"";
				colvarCertificateLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCertificateLimit);
				
				TableSchema.TableColumn colvarIsThisYearId = new TableSchema.TableColumn(schema);
				colvarIsThisYearId.ColumnName = "IsThisYearId";
				colvarIsThisYearId.DataType = DbType.String;
				colvarIsThisYearId.MaxLength = 200;
				colvarIsThisYearId.AutoIncrement = false;
				colvarIsThisYearId.IsNullable = true;
				colvarIsThisYearId.IsPrimaryKey = false;
				colvarIsThisYearId.IsForeignKey = false;
				colvarIsThisYearId.IsReadOnly = false;
				colvarIsThisYearId.DefaultSetting = @"";
				colvarIsThisYearId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsThisYearId);
				
				TableSchema.TableColumn colvarBonusRate = new TableSchema.TableColumn(schema);
				colvarBonusRate.ColumnName = "BonusRate";
				colvarBonusRate.DataType = DbType.Decimal;
				colvarBonusRate.MaxLength = 0;
				colvarBonusRate.AutoIncrement = false;
				colvarBonusRate.IsNullable = true;
				colvarBonusRate.IsPrimaryKey = false;
				colvarBonusRate.IsForeignKey = false;
				colvarBonusRate.IsReadOnly = false;
				colvarBonusRate.DefaultSetting = @"";
				colvarBonusRate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBonusRate);
				
				TableSchema.TableColumn colvarCCBCode = new TableSchema.TableColumn(schema);
				colvarCCBCode.ColumnName = "CCBCode";
				colvarCCBCode.DataType = DbType.String;
				colvarCCBCode.MaxLength = 200;
				colvarCCBCode.AutoIncrement = false;
				colvarCCBCode.IsNullable = true;
				colvarCCBCode.IsPrimaryKey = false;
				colvarCCBCode.IsForeignKey = false;
				colvarCCBCode.IsReadOnly = false;
				colvarCCBCode.DefaultSetting = @"";
				colvarCCBCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCCBCode);
				
				TableSchema.TableColumn colvarJobDate = new TableSchema.TableColumn(schema);
				colvarJobDate.ColumnName = "JobDate";
				colvarJobDate.DataType = DbType.DateTime;
				colvarJobDate.MaxLength = 0;
				colvarJobDate.AutoIncrement = false;
				colvarJobDate.IsNullable = true;
				colvarJobDate.IsPrimaryKey = false;
				colvarJobDate.IsForeignKey = false;
				colvarJobDate.IsReadOnly = false;
				colvarJobDate.DefaultSetting = @"";
				colvarJobDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarJobDate);
				
				TableSchema.TableColumn colvarEmployeeId = new TableSchema.TableColumn(schema);
				colvarEmployeeId.ColumnName = "EmployeeId";
				colvarEmployeeId.DataType = DbType.Guid;
				colvarEmployeeId.MaxLength = 0;
				colvarEmployeeId.AutoIncrement = false;
				colvarEmployeeId.IsNullable = false;
				colvarEmployeeId.IsPrimaryKey = true;
				colvarEmployeeId.IsForeignKey = false;
				colvarEmployeeId.IsReadOnly = false;
				colvarEmployeeId.DefaultSetting = @"";
				colvarEmployeeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmployeeId);
				
				TableSchema.TableColumn colvarNumber = new TableSchema.TableColumn(schema);
				colvarNumber.ColumnName = "Number";
				colvarNumber.DataType = DbType.Int32;
				colvarNumber.MaxLength = 0;
				colvarNumber.AutoIncrement = false;
				colvarNumber.IsNullable = true;
				colvarNumber.IsPrimaryKey = false;
				colvarNumber.IsForeignKey = false;
				colvarNumber.IsReadOnly = false;
				colvarNumber.DefaultSetting = @"";
				colvarNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumber);
				
				TableSchema.TableColumn colvarCnName = new TableSchema.TableColumn(schema);
				colvarCnName.ColumnName = "CnName";
				colvarCnName.DataType = DbType.String;
				colvarCnName.MaxLength = 200;
				colvarCnName.AutoIncrement = false;
				colvarCnName.IsNullable = true;
				colvarCnName.IsPrimaryKey = false;
				colvarCnName.IsForeignKey = false;
				colvarCnName.IsReadOnly = false;
				colvarCnName.DefaultSetting = @"";
				colvarCnName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCnName);
				
				TableSchema.TableColumn colvarEnName = new TableSchema.TableColumn(schema);
				colvarEnName.ColumnName = "EnName";
				colvarEnName.DataType = DbType.String;
				colvarEnName.MaxLength = 200;
				colvarEnName.AutoIncrement = false;
				colvarEnName.IsNullable = true;
				colvarEnName.IsPrimaryKey = false;
				colvarEnName.IsForeignKey = false;
				colvarEnName.IsReadOnly = false;
				colvarEnName.DefaultSetting = @"";
				colvarEnName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnName);
				
				TableSchema.TableColumn colvarIDCardNo = new TableSchema.TableColumn(schema);
				colvarIDCardNo.ColumnName = "IDCardNo";
				colvarIDCardNo.DataType = DbType.String;
				colvarIDCardNo.MaxLength = 200;
				colvarIDCardNo.AutoIncrement = false;
				colvarIDCardNo.IsNullable = true;
				colvarIDCardNo.IsPrimaryKey = false;
				colvarIDCardNo.IsForeignKey = false;
				colvarIDCardNo.IsReadOnly = false;
				colvarIDCardNo.DefaultSetting = @"";
				colvarIDCardNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIDCardNo);
				
				TableSchema.TableColumn colvarIDCardKindId = new TableSchema.TableColumn(schema);
				colvarIDCardKindId.ColumnName = "IDCardKindId";
				colvarIDCardKindId.DataType = DbType.String;
				colvarIDCardKindId.MaxLength = 200;
				colvarIDCardKindId.AutoIncrement = false;
				colvarIDCardKindId.IsNullable = true;
				colvarIDCardKindId.IsPrimaryKey = false;
				colvarIDCardKindId.IsForeignKey = false;
				colvarIDCardKindId.IsReadOnly = false;
				colvarIDCardKindId.DefaultSetting = @"";
				colvarIDCardKindId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIDCardKindId);
				
				TableSchema.TableColumn colvarSocialWelfareNo = new TableSchema.TableColumn(schema);
				colvarSocialWelfareNo.ColumnName = "SocialWelfareNo";
				colvarSocialWelfareNo.DataType = DbType.String;
				colvarSocialWelfareNo.MaxLength = 200;
				colvarSocialWelfareNo.AutoIncrement = false;
				colvarSocialWelfareNo.IsNullable = true;
				colvarSocialWelfareNo.IsPrimaryKey = false;
				colvarSocialWelfareNo.IsForeignKey = false;
				colvarSocialWelfareNo.IsReadOnly = false;
				colvarSocialWelfareNo.DefaultSetting = @"";
				colvarSocialWelfareNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSocialWelfareNo);
				
				TableSchema.TableColumn colvarBirthDate = new TableSchema.TableColumn(schema);
				colvarBirthDate.ColumnName = "BirthDate";
				colvarBirthDate.DataType = DbType.DateTime;
				colvarBirthDate.MaxLength = 0;
				colvarBirthDate.AutoIncrement = false;
				colvarBirthDate.IsNullable = true;
				colvarBirthDate.IsPrimaryKey = false;
				colvarBirthDate.IsForeignKey = false;
				colvarBirthDate.IsReadOnly = false;
				colvarBirthDate.DefaultSetting = @"";
				colvarBirthDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBirthDate);
				
				TableSchema.TableColumn colvarGenderId = new TableSchema.TableColumn(schema);
				colvarGenderId.ColumnName = "GenderId";
				colvarGenderId.DataType = DbType.String;
				colvarGenderId.MaxLength = 200;
				colvarGenderId.AutoIncrement = false;
				colvarGenderId.IsNullable = true;
				colvarGenderId.IsPrimaryKey = false;
				colvarGenderId.IsForeignKey = false;
				colvarGenderId.IsReadOnly = false;
				colvarGenderId.DefaultSetting = @"";
				colvarGenderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGenderId);
				
				TableSchema.TableColumn colvarDateX = new TableSchema.TableColumn(schema);
				colvarDateX.ColumnName = "Date";
				colvarDateX.DataType = DbType.DateTime;
				colvarDateX.MaxLength = 0;
				colvarDateX.AutoIncrement = false;
				colvarDateX.IsNullable = true;
				colvarDateX.IsPrimaryKey = false;
				colvarDateX.IsForeignKey = false;
				colvarDateX.IsReadOnly = false;
				colvarDateX.DefaultSetting = @"";
				colvarDateX.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDateX);
				
				TableSchema.TableColumn colvarEmployeeStateId = new TableSchema.TableColumn(schema);
				colvarEmployeeStateId.ColumnName = "EmployeeStateId";
				colvarEmployeeStateId.DataType = DbType.String;
				colvarEmployeeStateId.MaxLength = 200;
				colvarEmployeeStateId.AutoIncrement = false;
				colvarEmployeeStateId.IsNullable = true;
				colvarEmployeeStateId.IsPrimaryKey = false;
				colvarEmployeeStateId.IsForeignKey = false;
				colvarEmployeeStateId.IsReadOnly = false;
				colvarEmployeeStateId.DefaultSetting = @"";
				colvarEmployeeStateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmployeeStateId);
				
				TableSchema.TableColumn colvarWorkTypeId = new TableSchema.TableColumn(schema);
				colvarWorkTypeId.ColumnName = "WorkTypeId";
				colvarWorkTypeId.DataType = DbType.String;
				colvarWorkTypeId.MaxLength = 200;
				colvarWorkTypeId.AutoIncrement = false;
				colvarWorkTypeId.IsNullable = true;
				colvarWorkTypeId.IsPrimaryKey = false;
				colvarWorkTypeId.IsForeignKey = false;
				colvarWorkTypeId.IsReadOnly = false;
				colvarWorkTypeId.DefaultSetting = @"";
				colvarWorkTypeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWorkTypeId);
				
				TableSchema.TableColumn colvarMarriageId = new TableSchema.TableColumn(schema);
				colvarMarriageId.ColumnName = "MarriageId";
				colvarMarriageId.DataType = DbType.String;
				colvarMarriageId.MaxLength = 200;
				colvarMarriageId.AutoIncrement = false;
				colvarMarriageId.IsNullable = true;
				colvarMarriageId.IsPrimaryKey = false;
				colvarMarriageId.IsForeignKey = false;
				colvarMarriageId.IsReadOnly = false;
				colvarMarriageId.DefaultSetting = @"";
				colvarMarriageId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMarriageId);
				
				TableSchema.TableColumn colvarServiceYears = new TableSchema.TableColumn(schema);
				colvarServiceYears.ColumnName = "ServiceYears";
				colvarServiceYears.DataType = DbType.String;
				colvarServiceYears.MaxLength = 200;
				colvarServiceYears.AutoIncrement = false;
				colvarServiceYears.IsNullable = true;
				colvarServiceYears.IsPrimaryKey = false;
				colvarServiceYears.IsForeignKey = false;
				colvarServiceYears.IsReadOnly = false;
				colvarServiceYears.DefaultSetting = @"";
				colvarServiceYears.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceYears);
				
				TableSchema.TableColumn colvarCorporationId = new TableSchema.TableColumn(schema);
				colvarCorporationId.ColumnName = "CorporationId";
				colvarCorporationId.DataType = DbType.Guid;
				colvarCorporationId.MaxLength = 0;
				colvarCorporationId.AutoIncrement = false;
				colvarCorporationId.IsNullable = true;
				colvarCorporationId.IsPrimaryKey = false;
				colvarCorporationId.IsForeignKey = false;
				colvarCorporationId.IsReadOnly = false;
				colvarCorporationId.DefaultSetting = @"";
				colvarCorporationId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCorporationId);
				
				TableSchema.TableColumn colvarTelephone = new TableSchema.TableColumn(schema);
				colvarTelephone.ColumnName = "Telephone";
				colvarTelephone.DataType = DbType.String;
				colvarTelephone.MaxLength = 200;
				colvarTelephone.AutoIncrement = false;
				colvarTelephone.IsNullable = true;
				colvarTelephone.IsPrimaryKey = false;
				colvarTelephone.IsForeignKey = false;
				colvarTelephone.IsReadOnly = false;
				colvarTelephone.DefaultSetting = @"";
				colvarTelephone.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTelephone);
				
				TableSchema.TableColumn colvarAreaId = new TableSchema.TableColumn(schema);
				colvarAreaId.ColumnName = "AreaId";
				colvarAreaId.DataType = DbType.Guid;
				colvarAreaId.MaxLength = 0;
				colvarAreaId.AutoIncrement = false;
				colvarAreaId.IsNullable = true;
				colvarAreaId.IsPrimaryKey = false;
				colvarAreaId.IsForeignKey = false;
				colvarAreaId.IsReadOnly = false;
				colvarAreaId.DefaultSetting = @"";
				colvarAreaId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAreaId);
				
				TableSchema.TableColumn colvarFactoryId = new TableSchema.TableColumn(schema);
				colvarFactoryId.ColumnName = "FactoryId";
				colvarFactoryId.DataType = DbType.String;
				colvarFactoryId.MaxLength = 200;
				colvarFactoryId.AutoIncrement = false;
				colvarFactoryId.IsNullable = true;
				colvarFactoryId.IsPrimaryKey = false;
				colvarFactoryId.IsForeignKey = false;
				colvarFactoryId.IsReadOnly = false;
				colvarFactoryId.DefaultSetting = @"";
				colvarFactoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFactoryId);
				
				TableSchema.TableColumn colvarDepartmentId = new TableSchema.TableColumn(schema);
				colvarDepartmentId.ColumnName = "DepartmentId";
				colvarDepartmentId.DataType = DbType.Guid;
				colvarDepartmentId.MaxLength = 0;
				colvarDepartmentId.AutoIncrement = false;
				colvarDepartmentId.IsNullable = true;
				colvarDepartmentId.IsPrimaryKey = false;
				colvarDepartmentId.IsForeignKey = false;
				colvarDepartmentId.IsReadOnly = false;
				colvarDepartmentId.DefaultSetting = @"";
				colvarDepartmentId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDepartmentId);
				
				TableSchema.TableColumn colvarJobId = new TableSchema.TableColumn(schema);
				colvarJobId.ColumnName = "JobId";
				colvarJobId.DataType = DbType.Guid;
				colvarJobId.MaxLength = 0;
				colvarJobId.AutoIncrement = false;
				colvarJobId.IsNullable = true;
				colvarJobId.IsPrimaryKey = false;
				colvarJobId.IsForeignKey = false;
				colvarJobId.IsReadOnly = false;
				colvarJobId.DefaultSetting = @"";
				colvarJobId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarJobId);
				
				TableSchema.TableColumn colvarPartTimeJob = new TableSchema.TableColumn(schema);
				colvarPartTimeJob.ColumnName = "PartTimeJob";
				colvarPartTimeJob.DataType = DbType.String;
				colvarPartTimeJob.MaxLength = 200;
				colvarPartTimeJob.AutoIncrement = false;
				colvarPartTimeJob.IsNullable = true;
				colvarPartTimeJob.IsPrimaryKey = false;
				colvarPartTimeJob.IsForeignKey = false;
				colvarPartTimeJob.IsReadOnly = false;
				colvarPartTimeJob.DefaultSetting = @"";
				colvarPartTimeJob.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPartTimeJob);
				
				TableSchema.TableColumn colvarDirectorId = new TableSchema.TableColumn(schema);
				colvarDirectorId.ColumnName = "DirectorId";
				colvarDirectorId.DataType = DbType.Guid;
				colvarDirectorId.MaxLength = 0;
				colvarDirectorId.AutoIncrement = false;
				colvarDirectorId.IsNullable = true;
				colvarDirectorId.IsPrimaryKey = false;
				colvarDirectorId.IsForeignKey = false;
				colvarDirectorId.IsReadOnly = false;
				colvarDirectorId.DefaultSetting = @"";
				colvarDirectorId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDirectorId);
				
				TableSchema.TableColumn colvarMobilePhone = new TableSchema.TableColumn(schema);
				colvarMobilePhone.ColumnName = "MobilePhone";
				colvarMobilePhone.DataType = DbType.String;
				colvarMobilePhone.MaxLength = 200;
				colvarMobilePhone.AutoIncrement = false;
				colvarMobilePhone.IsNullable = true;
				colvarMobilePhone.IsPrimaryKey = false;
				colvarMobilePhone.IsForeignKey = false;
				colvarMobilePhone.IsReadOnly = false;
				colvarMobilePhone.DefaultSetting = @"";
				colvarMobilePhone.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobilePhone);
				
				TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
				colvarEmail.ColumnName = "Email";
				colvarEmail.DataType = DbType.String;
				colvarEmail.MaxLength = 200;
				colvarEmail.AutoIncrement = false;
				colvarEmail.IsNullable = true;
				colvarEmail.IsPrimaryKey = false;
				colvarEmail.IsForeignKey = false;
				colvarEmail.IsReadOnly = false;
				colvarEmail.DefaultSetting = @"";
				colvarEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmail);
				
				TableSchema.TableColumn colvarEducationId = new TableSchema.TableColumn(schema);
				colvarEducationId.ColumnName = "EducationId";
				colvarEducationId.DataType = DbType.String;
				colvarEducationId.MaxLength = 200;
				colvarEducationId.AutoIncrement = false;
				colvarEducationId.IsNullable = true;
				colvarEducationId.IsPrimaryKey = false;
				colvarEducationId.IsForeignKey = false;
				colvarEducationId.IsReadOnly = false;
				colvarEducationId.DefaultSetting = @"";
				colvarEducationId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEducationId);
				
				TableSchema.TableColumn colvarMajor = new TableSchema.TableColumn(schema);
				colvarMajor.ColumnName = "Major";
				colvarMajor.DataType = DbType.String;
				colvarMajor.MaxLength = 200;
				colvarMajor.AutoIncrement = false;
				colvarMajor.IsNullable = true;
				colvarMajor.IsPrimaryKey = false;
				colvarMajor.IsForeignKey = false;
				colvarMajor.IsReadOnly = false;
				colvarMajor.DefaultSetting = @"";
				colvarMajor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMajor);
				
				TableSchema.TableColumn colvarNativePlace = new TableSchema.TableColumn(schema);
				colvarNativePlace.ColumnName = "NativePlace";
				colvarNativePlace.DataType = DbType.String;
				colvarNativePlace.MaxLength = 200;
				colvarNativePlace.AutoIncrement = false;
				colvarNativePlace.IsNullable = true;
				colvarNativePlace.IsPrimaryKey = false;
				colvarNativePlace.IsForeignKey = false;
				colvarNativePlace.IsReadOnly = false;
				colvarNativePlace.DefaultSetting = @"";
				colvarNativePlace.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNativePlace);
				
				TableSchema.TableColumn colvarPoliticalIdentityId = new TableSchema.TableColumn(schema);
				colvarPoliticalIdentityId.ColumnName = "PoliticalIdentityId";
				colvarPoliticalIdentityId.DataType = DbType.String;
				colvarPoliticalIdentityId.MaxLength = 200;
				colvarPoliticalIdentityId.AutoIncrement = false;
				colvarPoliticalIdentityId.IsNullable = true;
				colvarPoliticalIdentityId.IsPrimaryKey = false;
				colvarPoliticalIdentityId.IsForeignKey = false;
				colvarPoliticalIdentityId.IsReadOnly = false;
				colvarPoliticalIdentityId.DefaultSetting = @"";
				colvarPoliticalIdentityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPoliticalIdentityId);
				
				TableSchema.TableColumn colvarLocation = new TableSchema.TableColumn(schema);
				colvarLocation.ColumnName = "Location";
				colvarLocation.DataType = DbType.String;
				colvarLocation.MaxLength = 200;
				colvarLocation.AutoIncrement = false;
				colvarLocation.IsNullable = true;
				colvarLocation.IsPrimaryKey = false;
				colvarLocation.IsForeignKey = false;
				colvarLocation.IsReadOnly = false;
				colvarLocation.DefaultSetting = @"";
				colvarLocation.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLocation);
				
				TableSchema.TableColumn colvarPostalcode = new TableSchema.TableColumn(schema);
				colvarPostalcode.ColumnName = "Postalcode";
				colvarPostalcode.DataType = DbType.String;
				colvarPostalcode.MaxLength = 200;
				colvarPostalcode.AutoIncrement = false;
				colvarPostalcode.IsNullable = true;
				colvarPostalcode.IsPrimaryKey = false;
				colvarPostalcode.IsForeignKey = false;
				colvarPostalcode.IsReadOnly = false;
				colvarPostalcode.DefaultSetting = @"";
				colvarPostalcode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPostalcode);
				
				TableSchema.TableColumn colvarNationId = new TableSchema.TableColumn(schema);
				colvarNationId.ColumnName = "NationId";
				colvarNationId.DataType = DbType.String;
				colvarNationId.MaxLength = 200;
				colvarNationId.AutoIncrement = false;
				colvarNationId.IsNullable = true;
				colvarNationId.IsPrimaryKey = false;
				colvarNationId.IsForeignKey = false;
				colvarNationId.IsReadOnly = false;
				colvarNationId.DefaultSetting = @"";
				colvarNationId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNationId);
				
				TableSchema.TableColumn colvarAddress = new TableSchema.TableColumn(schema);
				colvarAddress.ColumnName = "Address";
				colvarAddress.DataType = DbType.String;
				colvarAddress.MaxLength = 200;
				colvarAddress.AutoIncrement = false;
				colvarAddress.IsNullable = true;
				colvarAddress.IsPrimaryKey = false;
				colvarAddress.IsForeignKey = false;
				colvarAddress.IsReadOnly = false;
				colvarAddress.DefaultSetting = @"";
				colvarAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAddress);
				
				TableSchema.TableColumn colvarCostCenterId = new TableSchema.TableColumn(schema);
				colvarCostCenterId.ColumnName = "CostCenterId";
				colvarCostCenterId.DataType = DbType.Guid;
				colvarCostCenterId.MaxLength = 0;
				colvarCostCenterId.AutoIncrement = false;
				colvarCostCenterId.IsNullable = true;
				colvarCostCenterId.IsPrimaryKey = false;
				colvarCostCenterId.IsForeignKey = false;
				colvarCostCenterId.IsReadOnly = false;
				colvarCostCenterId.DefaultSetting = @"";
				colvarCostCenterId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostCenterId);
				
				TableSchema.TableColumn colvarInterest = new TableSchema.TableColumn(schema);
				colvarInterest.ColumnName = "Interest";
				colvarInterest.DataType = DbType.String;
				colvarInterest.MaxLength = 1073741823;
				colvarInterest.AutoIncrement = false;
				colvarInterest.IsNullable = true;
				colvarInterest.IsPrimaryKey = false;
				colvarInterest.IsForeignKey = false;
				colvarInterest.IsReadOnly = false;
				colvarInterest.DefaultSetting = @"";
				colvarInterest.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInterest);
				
				TableSchema.TableColumn colvarPicture = new TableSchema.TableColumn(schema);
				colvarPicture.ColumnName = "Picture";
				colvarPicture.DataType = DbType.Binary;
				colvarPicture.MaxLength = 2147483647;
				colvarPicture.AutoIncrement = false;
				colvarPicture.IsNullable = true;
				colvarPicture.IsPrimaryKey = false;
				colvarPicture.IsForeignKey = false;
				colvarPicture.IsReadOnly = false;
				colvarPicture.DefaultSetting = @"";
				colvarPicture.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPicture);
				
				TableSchema.TableColumn colvarPassword = new TableSchema.TableColumn(schema);
				colvarPassword.ColumnName = "Password";
				colvarPassword.DataType = DbType.String;
				colvarPassword.MaxLength = 200;
				colvarPassword.AutoIncrement = false;
				colvarPassword.IsNullable = true;
				colvarPassword.IsPrimaryKey = false;
				colvarPassword.IsForeignKey = false;
				colvarPassword.IsReadOnly = false;
				colvarPassword.DefaultSetting = @"";
				colvarPassword.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPassword);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "Code";
				colvarCode.DataType = DbType.String;
				colvarCode.MaxLength = 200;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = true;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarCreateDate = new TableSchema.TableColumn(schema);
				colvarCreateDate.ColumnName = "CreateDate";
				colvarCreateDate.DataType = DbType.DateTime;
				colvarCreateDate.MaxLength = 0;
				colvarCreateDate.AutoIncrement = false;
				colvarCreateDate.IsNullable = true;
				colvarCreateDate.IsPrimaryKey = false;
				colvarCreateDate.IsForeignKey = false;
				colvarCreateDate.IsReadOnly = false;
				colvarCreateDate.DefaultSetting = @"";
				colvarCreateDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateDate);
				
				TableSchema.TableColumn colvarLastModifiedDate = new TableSchema.TableColumn(schema);
				colvarLastModifiedDate.ColumnName = "LastModifiedDate";
				colvarLastModifiedDate.DataType = DbType.DateTime;
				colvarLastModifiedDate.MaxLength = 0;
				colvarLastModifiedDate.AutoIncrement = false;
				colvarLastModifiedDate.IsNullable = true;
				colvarLastModifiedDate.IsPrimaryKey = false;
				colvarLastModifiedDate.IsForeignKey = false;
				colvarLastModifiedDate.IsReadOnly = false;
				colvarLastModifiedDate.DefaultSetting = @"";
				colvarLastModifiedDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastModifiedDate);
				
				TableSchema.TableColumn colvarCreateBy = new TableSchema.TableColumn(schema);
				colvarCreateBy.ColumnName = "CreateBy";
				colvarCreateBy.DataType = DbType.Guid;
				colvarCreateBy.MaxLength = 0;
				colvarCreateBy.AutoIncrement = false;
				colvarCreateBy.IsNullable = true;
				colvarCreateBy.IsPrimaryKey = false;
				colvarCreateBy.IsForeignKey = false;
				colvarCreateBy.IsReadOnly = false;
				colvarCreateBy.DefaultSetting = @"";
				colvarCreateBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateBy);
				
				TableSchema.TableColumn colvarLastModifiedBy = new TableSchema.TableColumn(schema);
				colvarLastModifiedBy.ColumnName = "LastModifiedBy";
				colvarLastModifiedBy.DataType = DbType.Guid;
				colvarLastModifiedBy.MaxLength = 0;
				colvarLastModifiedBy.AutoIncrement = false;
				colvarLastModifiedBy.IsNullable = true;
				colvarLastModifiedBy.IsPrimaryKey = false;
				colvarLastModifiedBy.IsForeignKey = false;
				colvarLastModifiedBy.IsReadOnly = false;
				colvarLastModifiedBy.DefaultSetting = @"";
				colvarLastModifiedBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastModifiedBy);
				
				TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
				colvarFlag.ColumnName = "Flag";
				colvarFlag.DataType = DbType.Boolean;
				colvarFlag.MaxLength = 0;
				colvarFlag.AutoIncrement = false;
				colvarFlag.IsNullable = true;
				colvarFlag.IsPrimaryKey = false;
				colvarFlag.IsForeignKey = false;
				colvarFlag.IsReadOnly = false;
				colvarFlag.DefaultSetting = @"";
				colvarFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFlag);
				
				TableSchema.TableColumn colvarIsPrincipal = new TableSchema.TableColumn(schema);
				colvarIsPrincipal.ColumnName = "IsPrincipal";
				colvarIsPrincipal.DataType = DbType.Boolean;
				colvarIsPrincipal.MaxLength = 0;
				colvarIsPrincipal.AutoIncrement = false;
				colvarIsPrincipal.IsNullable = true;
				colvarIsPrincipal.IsPrimaryKey = false;
				colvarIsPrincipal.IsForeignKey = false;
				colvarIsPrincipal.IsReadOnly = false;
				colvarIsPrincipal.DefaultSetting = @"";
				colvarIsPrincipal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsPrincipal);
				
				TableSchema.TableColumn colvarIsSend = new TableSchema.TableColumn(schema);
				colvarIsSend.ColumnName = "IsSend";
				colvarIsSend.DataType = DbType.Boolean;
				colvarIsSend.MaxLength = 0;
				colvarIsSend.AutoIncrement = false;
				colvarIsSend.IsNullable = true;
				colvarIsSend.IsPrimaryKey = false;
				colvarIsSend.IsForeignKey = false;
				colvarIsSend.IsReadOnly = false;
				colvarIsSend.DefaultSetting = @"";
				colvarIsSend.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSend);
				
				TableSchema.TableColumn colvarITCode = new TableSchema.TableColumn(schema);
				colvarITCode.ColumnName = "ITCode";
				colvarITCode.DataType = DbType.String;
				colvarITCode.MaxLength = 200;
				colvarITCode.AutoIncrement = false;
				colvarITCode.IsNullable = true;
				colvarITCode.IsPrimaryKey = false;
				colvarITCode.IsForeignKey = false;
				colvarITCode.IsReadOnly = false;
				colvarITCode.DefaultSetting = @"";
				colvarITCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarITCode);
				
				TableSchema.TableColumn colvarAssignReason = new TableSchema.TableColumn(schema);
				colvarAssignReason.ColumnName = "AssignReason";
				colvarAssignReason.DataType = DbType.String;
				colvarAssignReason.MaxLength = 200;
				colvarAssignReason.AutoIncrement = false;
				colvarAssignReason.IsNullable = true;
				colvarAssignReason.IsPrimaryKey = false;
				colvarAssignReason.IsForeignKey = false;
				colvarAssignReason.IsReadOnly = false;
				colvarAssignReason.DefaultSetting = @"";
				colvarAssignReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAssignReason);
				
				TableSchema.TableColumn colvarOwnerId = new TableSchema.TableColumn(schema);
				colvarOwnerId.ColumnName = "OwnerId";
				colvarOwnerId.DataType = DbType.String;
				colvarOwnerId.MaxLength = 200;
				colvarOwnerId.AutoIncrement = false;
				colvarOwnerId.IsNullable = true;
				colvarOwnerId.IsPrimaryKey = false;
				colvarOwnerId.IsForeignKey = false;
				colvarOwnerId.IsReadOnly = false;
				colvarOwnerId.DefaultSetting = @"";
				colvarOwnerId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOwnerId);
				
				TableSchema.TableColumn colvarOtherBirthDate = new TableSchema.TableColumn(schema);
				colvarOtherBirthDate.ColumnName = "OtherBirthDate";
				colvarOtherBirthDate.DataType = DbType.DateTime;
				colvarOtherBirthDate.MaxLength = 0;
				colvarOtherBirthDate.AutoIncrement = false;
				colvarOtherBirthDate.IsNullable = true;
				colvarOtherBirthDate.IsPrimaryKey = false;
				colvarOtherBirthDate.IsForeignKey = false;
				colvarOtherBirthDate.IsReadOnly = false;
				colvarOtherBirthDate.DefaultSetting = @"";
				colvarOtherBirthDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOtherBirthDate);
				
				TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
				colvarRemark.ColumnName = "Remark";
				colvarRemark.DataType = DbType.String;
				colvarRemark.MaxLength = 1073741823;
				colvarRemark.AutoIncrement = false;
				colvarRemark.IsNullable = true;
				colvarRemark.IsPrimaryKey = false;
				colvarRemark.IsForeignKey = false;
				colvarRemark.IsReadOnly = false;
				colvarRemark.DefaultSetting = @"";
				colvarRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemark);
				
				TableSchema.TableColumn colvarIntendTransformDate = new TableSchema.TableColumn(schema);
				colvarIntendTransformDate.ColumnName = "IntendTransformDate";
				colvarIntendTransformDate.DataType = DbType.DateTime;
				colvarIntendTransformDate.MaxLength = 0;
				colvarIntendTransformDate.AutoIncrement = false;
				colvarIntendTransformDate.IsNullable = true;
				colvarIntendTransformDate.IsPrimaryKey = false;
				colvarIntendTransformDate.IsForeignKey = false;
				colvarIntendTransformDate.IsReadOnly = false;
				colvarIntendTransformDate.DefaultSetting = @"";
				colvarIntendTransformDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntendTransformDate);
				
				TableSchema.TableColumn colvarATMealFeeType = new TableSchema.TableColumn(schema);
				colvarATMealFeeType.ColumnName = "ATMealFeeType";
				colvarATMealFeeType.DataType = DbType.String;
				colvarATMealFeeType.MaxLength = 200;
				colvarATMealFeeType.AutoIncrement = false;
				colvarATMealFeeType.IsNullable = true;
				colvarATMealFeeType.IsPrimaryKey = false;
				colvarATMealFeeType.IsForeignKey = false;
				colvarATMealFeeType.IsReadOnly = false;
				colvarATMealFeeType.DefaultSetting = @"";
				colvarATMealFeeType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarATMealFeeType);
				
				TableSchema.TableColumn colvarLastWorkDate = new TableSchema.TableColumn(schema);
				colvarLastWorkDate.ColumnName = "LastWorkDate";
				colvarLastWorkDate.DataType = DbType.DateTime;
				colvarLastWorkDate.MaxLength = 0;
				colvarLastWorkDate.AutoIncrement = false;
				colvarLastWorkDate.IsNullable = true;
				colvarLastWorkDate.IsPrimaryKey = false;
				colvarLastWorkDate.IsForeignKey = false;
				colvarLastWorkDate.IsReadOnly = false;
				colvarLastWorkDate.DefaultSetting = @"";
				colvarLastWorkDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastWorkDate);
				
				TableSchema.TableColumn colvarDirectDeptId = new TableSchema.TableColumn(schema);
				colvarDirectDeptId.ColumnName = "DirectDeptId";
				colvarDirectDeptId.DataType = DbType.Guid;
				colvarDirectDeptId.MaxLength = 0;
				colvarDirectDeptId.AutoIncrement = false;
				colvarDirectDeptId.IsNullable = true;
				colvarDirectDeptId.IsPrimaryKey = false;
				colvarDirectDeptId.IsForeignKey = false;
				colvarDirectDeptId.IsReadOnly = false;
				colvarDirectDeptId.DefaultSetting = @"";
				colvarDirectDeptId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDirectDeptId);
				
				TableSchema.TableColumn colvarTimeType = new TableSchema.TableColumn(schema);
				colvarTimeType.ColumnName = "TimeType";
				colvarTimeType.DataType = DbType.String;
				colvarTimeType.MaxLength = 200;
				colvarTimeType.AutoIncrement = false;
				colvarTimeType.IsNullable = true;
				colvarTimeType.IsPrimaryKey = false;
				colvarTimeType.IsForeignKey = false;
				colvarTimeType.IsReadOnly = false;
				colvarTimeType.DefaultSetting = @"";
				colvarTimeType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTimeType);
				
				TableSchema.TableColumn colvarHomePhone = new TableSchema.TableColumn(schema);
				colvarHomePhone.ColumnName = "HomePhone";
				colvarHomePhone.DataType = DbType.String;
				colvarHomePhone.MaxLength = 200;
				colvarHomePhone.AutoIncrement = false;
				colvarHomePhone.IsNullable = true;
				colvarHomePhone.IsPrimaryKey = false;
				colvarHomePhone.IsForeignKey = false;
				colvarHomePhone.IsReadOnly = false;
				colvarHomePhone.DefaultSetting = @"";
				colvarHomePhone.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHomePhone);
				
				TableSchema.TableColumn colvarTransformDate = new TableSchema.TableColumn(schema);
				colvarTransformDate.ColumnName = "TransformDate";
				colvarTransformDate.DataType = DbType.DateTime;
				colvarTransformDate.MaxLength = 0;
				colvarTransformDate.AutoIncrement = false;
				colvarTransformDate.IsNullable = true;
				colvarTransformDate.IsPrimaryKey = false;
				colvarTransformDate.IsForeignKey = false;
				colvarTransformDate.IsReadOnly = false;
				colvarTransformDate.DefaultSetting = @"";
				colvarTransformDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransformDate);
				
				TableSchema.TableColumn colvarWorkBeginDate = new TableSchema.TableColumn(schema);
				colvarWorkBeginDate.ColumnName = "WorkBeginDate";
				colvarWorkBeginDate.DataType = DbType.DateTime;
				colvarWorkBeginDate.MaxLength = 0;
				colvarWorkBeginDate.AutoIncrement = false;
				colvarWorkBeginDate.IsNullable = true;
				colvarWorkBeginDate.IsPrimaryKey = false;
				colvarWorkBeginDate.IsForeignKey = false;
				colvarWorkBeginDate.IsReadOnly = false;
				colvarWorkBeginDate.DefaultSetting = @"";
				colvarWorkBeginDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWorkBeginDate);
				
				TableSchema.TableColumn colvarHealthStatus = new TableSchema.TableColumn(schema);
				colvarHealthStatus.ColumnName = "HealthStatus";
				colvarHealthStatus.DataType = DbType.String;
				colvarHealthStatus.MaxLength = 200;
				colvarHealthStatus.AutoIncrement = false;
				colvarHealthStatus.IsNullable = true;
				colvarHealthStatus.IsPrimaryKey = false;
				colvarHealthStatus.IsForeignKey = false;
				colvarHealthStatus.IsReadOnly = false;
				colvarHealthStatus.DefaultSetting = @"";
				colvarHealthStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHealthStatus);
				
				TableSchema.TableColumn colvarStature = new TableSchema.TableColumn(schema);
				colvarStature.ColumnName = "Stature";
				colvarStature.DataType = DbType.String;
				colvarStature.MaxLength = 200;
				colvarStature.AutoIncrement = false;
				colvarStature.IsNullable = true;
				colvarStature.IsPrimaryKey = false;
				colvarStature.IsForeignKey = false;
				colvarStature.IsReadOnly = false;
				colvarStature.DefaultSetting = @"";
				colvarStature.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStature);
				
				TableSchema.TableColumn colvarWeight = new TableSchema.TableColumn(schema);
				colvarWeight.ColumnName = "Weight";
				colvarWeight.DataType = DbType.String;
				colvarWeight.MaxLength = 200;
				colvarWeight.AutoIncrement = false;
				colvarWeight.IsNullable = true;
				colvarWeight.IsPrimaryKey = false;
				colvarWeight.IsForeignKey = false;
				colvarWeight.IsReadOnly = false;
				colvarWeight.DefaultSetting = @"";
				colvarWeight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWeight);
				
				TableSchema.TableColumn colvarEyeSight = new TableSchema.TableColumn(schema);
				colvarEyeSight.ColumnName = "EyeSight";
				colvarEyeSight.DataType = DbType.String;
				colvarEyeSight.MaxLength = 200;
				colvarEyeSight.AutoIncrement = false;
				colvarEyeSight.IsNullable = true;
				colvarEyeSight.IsPrimaryKey = false;
				colvarEyeSight.IsForeignKey = false;
				colvarEyeSight.IsReadOnly = false;
				colvarEyeSight.DefaultSetting = @"";
				colvarEyeSight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEyeSight);
				
				TableSchema.TableColumn colvarBloodType = new TableSchema.TableColumn(schema);
				colvarBloodType.ColumnName = "BloodType";
				colvarBloodType.DataType = DbType.String;
				colvarBloodType.MaxLength = 200;
				colvarBloodType.AutoIncrement = false;
				colvarBloodType.IsNullable = true;
				colvarBloodType.IsPrimaryKey = false;
				colvarBloodType.IsForeignKey = false;
				colvarBloodType.IsReadOnly = false;
				colvarBloodType.DefaultSetting = @"";
				colvarBloodType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBloodType);
				
				TableSchema.TableColumn colvarPersonality = new TableSchema.TableColumn(schema);
				colvarPersonality.ColumnName = "Personality";
				colvarPersonality.DataType = DbType.String;
				colvarPersonality.MaxLength = 1073741823;
				colvarPersonality.AutoIncrement = false;
				colvarPersonality.IsNullable = true;
				colvarPersonality.IsPrimaryKey = false;
				colvarPersonality.IsForeignKey = false;
				colvarPersonality.IsReadOnly = false;
				colvarPersonality.DefaultSetting = @"";
				colvarPersonality.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPersonality);
				
				TableSchema.TableColumn colvarDiseaseHistory = new TableSchema.TableColumn(schema);
				colvarDiseaseHistory.ColumnName = "DiseaseHistory";
				colvarDiseaseHistory.DataType = DbType.String;
				colvarDiseaseHistory.MaxLength = 1073741823;
				colvarDiseaseHistory.AutoIncrement = false;
				colvarDiseaseHistory.IsNullable = true;
				colvarDiseaseHistory.IsPrimaryKey = false;
				colvarDiseaseHistory.IsForeignKey = false;
				colvarDiseaseHistory.IsReadOnly = false;
				colvarDiseaseHistory.DefaultSetting = @"";
				colvarDiseaseHistory.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiseaseHistory);
				
				TableSchema.TableColumn colvarCountryId = new TableSchema.TableColumn(schema);
				colvarCountryId.ColumnName = "CountryId";
				colvarCountryId.DataType = DbType.String;
				colvarCountryId.MaxLength = 200;
				colvarCountryId.AutoIncrement = false;
				colvarCountryId.IsNullable = true;
				colvarCountryId.IsPrimaryKey = false;
				colvarCountryId.IsForeignKey = false;
				colvarCountryId.IsReadOnly = false;
				colvarCountryId.DefaultSetting = @"";
				colvarCountryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCountryId);
				
				TableSchema.TableColumn colvarProvinceId = new TableSchema.TableColumn(schema);
				colvarProvinceId.ColumnName = "ProvinceId";
				colvarProvinceId.DataType = DbType.String;
				colvarProvinceId.MaxLength = 200;
				colvarProvinceId.AutoIncrement = false;
				colvarProvinceId.IsNullable = true;
				colvarProvinceId.IsPrimaryKey = false;
				colvarProvinceId.IsForeignKey = false;
				colvarProvinceId.IsReadOnly = false;
				colvarProvinceId.DefaultSetting = @"";
				colvarProvinceId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProvinceId);
				
				TableSchema.TableColumn colvarWorkingAgeBeginDate = new TableSchema.TableColumn(schema);
				colvarWorkingAgeBeginDate.ColumnName = "WorkingAgeBeginDate";
				colvarWorkingAgeBeginDate.DataType = DbType.DateTime;
				colvarWorkingAgeBeginDate.MaxLength = 0;
				colvarWorkingAgeBeginDate.AutoIncrement = false;
				colvarWorkingAgeBeginDate.IsNullable = true;
				colvarWorkingAgeBeginDate.IsPrimaryKey = false;
				colvarWorkingAgeBeginDate.IsForeignKey = false;
				colvarWorkingAgeBeginDate.IsReadOnly = false;
				colvarWorkingAgeBeginDate.DefaultSetting = @"";
				colvarWorkingAgeBeginDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWorkingAgeBeginDate);
				
				TableSchema.TableColumn colvarTWTaxTypeId = new TableSchema.TableColumn(schema);
				colvarTWTaxTypeId.ColumnName = "TWTaxTypeId";
				colvarTWTaxTypeId.DataType = DbType.String;
				colvarTWTaxTypeId.MaxLength = 200;
				colvarTWTaxTypeId.AutoIncrement = false;
				colvarTWTaxTypeId.IsNullable = true;
				colvarTWTaxTypeId.IsPrimaryKey = false;
				colvarTWTaxTypeId.IsForeignKey = false;
				colvarTWTaxTypeId.IsReadOnly = false;
				colvarTWTaxTypeId.DefaultSetting = @"";
				colvarTWTaxTypeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTWTaxTypeId);
				
				TableSchema.TableColumn colvarTWFixedValue = new TableSchema.TableColumn(schema);
				colvarTWFixedValue.ColumnName = "TWFixedValue";
				colvarTWFixedValue.DataType = DbType.Decimal;
				colvarTWFixedValue.MaxLength = 0;
				colvarTWFixedValue.AutoIncrement = false;
				colvarTWFixedValue.IsNullable = true;
				colvarTWFixedValue.IsPrimaryKey = false;
				colvarTWFixedValue.IsForeignKey = false;
				colvarTWFixedValue.IsReadOnly = false;
				colvarTWFixedValue.DefaultSetting = @"";
				colvarTWFixedValue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTWFixedValue);
				
				TableSchema.TableColumn colvarTWFixedTaxRate = new TableSchema.TableColumn(schema);
				colvarTWFixedTaxRate.ColumnName = "TWFixedTaxRate";
				colvarTWFixedTaxRate.DataType = DbType.Decimal;
				colvarTWFixedTaxRate.MaxLength = 0;
				colvarTWFixedTaxRate.AutoIncrement = false;
				colvarTWFixedTaxRate.IsNullable = true;
				colvarTWFixedTaxRate.IsPrimaryKey = false;
				colvarTWFixedTaxRate.IsForeignKey = false;
				colvarTWFixedTaxRate.IsReadOnly = false;
				colvarTWFixedTaxRate.DefaultSetting = @"";
				colvarTWFixedTaxRate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTWFixedTaxRate);
				
				TableSchema.TableColumn colvarFamilyNumber = new TableSchema.TableColumn(schema);
				colvarFamilyNumber.ColumnName = "FamilyNumber";
				colvarFamilyNumber.DataType = DbType.Int32;
				colvarFamilyNumber.MaxLength = 0;
				colvarFamilyNumber.AutoIncrement = false;
				colvarFamilyNumber.IsNullable = true;
				colvarFamilyNumber.IsPrimaryKey = false;
				colvarFamilyNumber.IsForeignKey = false;
				colvarFamilyNumber.IsReadOnly = false;
				colvarFamilyNumber.DefaultSetting = @"";
				colvarFamilyNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamilyNumber);
				
				TableSchema.TableColumn colvarAdmitWorkAge = new TableSchema.TableColumn(schema);
				colvarAdmitWorkAge.ColumnName = "AdmitWorkAge";
				colvarAdmitWorkAge.DataType = DbType.Decimal;
				colvarAdmitWorkAge.MaxLength = 0;
				colvarAdmitWorkAge.AutoIncrement = false;
				colvarAdmitWorkAge.IsNullable = true;
				colvarAdmitWorkAge.IsPrimaryKey = false;
				colvarAdmitWorkAge.IsForeignKey = false;
				colvarAdmitWorkAge.IsReadOnly = false;
				colvarAdmitWorkAge.DefaultSetting = @"";
				colvarAdmitWorkAge.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAdmitWorkAge);
				
				TableSchema.TableColumn colvarCharacterTest = new TableSchema.TableColumn(schema);
				colvarCharacterTest.ColumnName = "CharacterTest";
				colvarCharacterTest.DataType = DbType.String;
				colvarCharacterTest.MaxLength = 200;
				colvarCharacterTest.AutoIncrement = false;
				colvarCharacterTest.IsNullable = true;
				colvarCharacterTest.IsPrimaryKey = false;
				colvarCharacterTest.IsForeignKey = false;
				colvarCharacterTest.IsReadOnly = false;
				colvarCharacterTest.DefaultSetting = @"";
				colvarCharacterTest.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCharacterTest);
				
				TableSchema.TableColumn colvarDeputy = new TableSchema.TableColumn(schema);
				colvarDeputy.ColumnName = "Deputy";
				colvarDeputy.DataType = DbType.Guid;
				colvarDeputy.MaxLength = 0;
				colvarDeputy.AutoIncrement = false;
				colvarDeputy.IsNullable = true;
				colvarDeputy.IsPrimaryKey = false;
				colvarDeputy.IsForeignKey = false;
				colvarDeputy.IsReadOnly = false;
				colvarDeputy.DefaultSetting = @"";
				colvarDeputy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeputy);
				
				TableSchema.TableColumn colvarEmployTypeId = new TableSchema.TableColumn(schema);
				colvarEmployTypeId.ColumnName = "EmployTypeId";
				colvarEmployTypeId.DataType = DbType.String;
				colvarEmployTypeId.MaxLength = 200;
				colvarEmployTypeId.AutoIncrement = false;
				colvarEmployTypeId.IsNullable = true;
				colvarEmployTypeId.IsPrimaryKey = false;
				colvarEmployTypeId.IsForeignKey = false;
				colvarEmployTypeId.IsReadOnly = false;
				colvarEmployTypeId.DefaultSetting = @"";
				colvarEmployTypeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmployTypeId);
				
				TableSchema.TableColumn colvarPositionId = new TableSchema.TableColumn(schema);
				colvarPositionId.ColumnName = "PositionId";
				colvarPositionId.DataType = DbType.Guid;
				colvarPositionId.MaxLength = 0;
				colvarPositionId.AutoIncrement = false;
				colvarPositionId.IsNullable = true;
				colvarPositionId.IsPrimaryKey = false;
				colvarPositionId.IsForeignKey = false;
				colvarPositionId.IsReadOnly = false;
				colvarPositionId.DefaultSetting = @"";
				colvarPositionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPositionId);
				
				TableSchema.TableColumn colvarPositionGroupId = new TableSchema.TableColumn(schema);
				colvarPositionGroupId.ColumnName = "PositionGroupId";
				colvarPositionGroupId.DataType = DbType.String;
				colvarPositionGroupId.MaxLength = 200;
				colvarPositionGroupId.AutoIncrement = false;
				colvarPositionGroupId.IsNullable = true;
				colvarPositionGroupId.IsPrimaryKey = false;
				colvarPositionGroupId.IsForeignKey = false;
				colvarPositionGroupId.IsReadOnly = false;
				colvarPositionGroupId.DefaultSetting = @"";
				colvarPositionGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPositionGroupId);
				
				TableSchema.TableColumn colvarEnterDate = new TableSchema.TableColumn(schema);
				colvarEnterDate.ColumnName = "EnterDate";
				colvarEnterDate.DataType = DbType.DateTime;
				colvarEnterDate.MaxLength = 0;
				colvarEnterDate.AutoIncrement = false;
				colvarEnterDate.IsNullable = true;
				colvarEnterDate.IsPrimaryKey = false;
				colvarEnterDate.IsForeignKey = false;
				colvarEnterDate.IsReadOnly = false;
				colvarEnterDate.DefaultSetting = @"";
				colvarEnterDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnterDate);
				
				TableSchema.TableColumn colvarResidenceExpireDate = new TableSchema.TableColumn(schema);
				colvarResidenceExpireDate.ColumnName = "ResidenceExpireDate";
				colvarResidenceExpireDate.DataType = DbType.DateTime;
				colvarResidenceExpireDate.MaxLength = 0;
				colvarResidenceExpireDate.AutoIncrement = false;
				colvarResidenceExpireDate.IsNullable = true;
				colvarResidenceExpireDate.IsPrimaryKey = false;
				colvarResidenceExpireDate.IsForeignKey = false;
				colvarResidenceExpireDate.IsReadOnly = false;
				colvarResidenceExpireDate.DefaultSetting = @"";
				colvarResidenceExpireDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResidenceExpireDate);
				
				TableSchema.TableColumn colvarAge = new TableSchema.TableColumn(schema);
				colvarAge.ColumnName = "Age";
				colvarAge.DataType = DbType.Int32;
				colvarAge.MaxLength = 0;
				colvarAge.AutoIncrement = false;
				colvarAge.IsNullable = true;
				colvarAge.IsPrimaryKey = false;
				colvarAge.IsForeignKey = false;
				colvarAge.IsReadOnly = false;
				colvarAge.DefaultSetting = @"";
				colvarAge.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAge);
				
				TableSchema.TableColumn colvarWorkingAge = new TableSchema.TableColumn(schema);
				colvarWorkingAge.ColumnName = "WorkingAge";
				colvarWorkingAge.DataType = DbType.Int32;
				colvarWorkingAge.MaxLength = 0;
				colvarWorkingAge.AutoIncrement = false;
				colvarWorkingAge.IsNullable = true;
				colvarWorkingAge.IsPrimaryKey = false;
				colvarWorkingAge.IsForeignKey = false;
				colvarWorkingAge.IsReadOnly = false;
				colvarWorkingAge.DefaultSetting = @"";
				colvarWorkingAge.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWorkingAge);
				
				TableSchema.TableColumn colvarOtherCorpWorkingAge = new TableSchema.TableColumn(schema);
				colvarOtherCorpWorkingAge.ColumnName = "OtherCorpWorkingAge";
				colvarOtherCorpWorkingAge.DataType = DbType.Int32;
				colvarOtherCorpWorkingAge.MaxLength = 0;
				colvarOtherCorpWorkingAge.AutoIncrement = false;
				colvarOtherCorpWorkingAge.IsNullable = true;
				colvarOtherCorpWorkingAge.IsPrimaryKey = false;
				colvarOtherCorpWorkingAge.IsForeignKey = false;
				colvarOtherCorpWorkingAge.IsReadOnly = false;
				colvarOtherCorpWorkingAge.DefaultSetting = @"";
				colvarOtherCorpWorkingAge.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOtherCorpWorkingAge);
				
				TableSchema.TableColumn colvarHomeMail = new TableSchema.TableColumn(schema);
				colvarHomeMail.ColumnName = "HomeMail";
				colvarHomeMail.DataType = DbType.String;
				colvarHomeMail.MaxLength = 200;
				colvarHomeMail.AutoIncrement = false;
				colvarHomeMail.IsNullable = true;
				colvarHomeMail.IsPrimaryKey = false;
				colvarHomeMail.IsForeignKey = false;
				colvarHomeMail.IsReadOnly = false;
				colvarHomeMail.DefaultSetting = @"";
				colvarHomeMail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHomeMail);
				
				TableSchema.TableColumn colvarHobbies = new TableSchema.TableColumn(schema);
				colvarHobbies.ColumnName = "Hobbies";
				colvarHobbies.DataType = DbType.String;
				colvarHobbies.MaxLength = 1073741823;
				colvarHobbies.AutoIncrement = false;
				colvarHobbies.IsNullable = true;
				colvarHobbies.IsPrimaryKey = false;
				colvarHobbies.IsForeignKey = false;
				colvarHobbies.IsReadOnly = false;
				colvarHobbies.DefaultSetting = @"";
				colvarHobbies.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHobbies);
				
				TableSchema.TableColumn colvarMultiSysCode = new TableSchema.TableColumn(schema);
				colvarMultiSysCode.ColumnName = "MultiSysCode";
				colvarMultiSysCode.DataType = DbType.String;
				colvarMultiSysCode.MaxLength = 200;
				colvarMultiSysCode.AutoIncrement = false;
				colvarMultiSysCode.IsNullable = true;
				colvarMultiSysCode.IsPrimaryKey = false;
				colvarMultiSysCode.IsForeignKey = false;
				colvarMultiSysCode.IsReadOnly = false;
				colvarMultiSysCode.DefaultSetting = @"";
				colvarMultiSysCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMultiSysCode);
				
				TableSchema.TableColumn colvarZhiJian = new TableSchema.TableColumn(schema);
				colvarZhiJian.ColumnName = "ZhiJian";
				colvarZhiJian.DataType = DbType.String;
				colvarZhiJian.MaxLength = 200;
				colvarZhiJian.AutoIncrement = false;
				colvarZhiJian.IsNullable = true;
				colvarZhiJian.IsPrimaryKey = false;
				colvarZhiJian.IsForeignKey = false;
				colvarZhiJian.IsReadOnly = false;
				colvarZhiJian.DefaultSetting = @"";
				colvarZhiJian.ForeignKeyTableName = "";
				schema.Columns.Add(colvarZhiJian);
				
				TableSchema.TableColumn colvarExtraField1 = new TableSchema.TableColumn(schema);
				colvarExtraField1.ColumnName = "ExtraField1";
				colvarExtraField1.DataType = DbType.String;
				colvarExtraField1.MaxLength = 200;
				colvarExtraField1.AutoIncrement = false;
				colvarExtraField1.IsNullable = true;
				colvarExtraField1.IsPrimaryKey = false;
				colvarExtraField1.IsForeignKey = false;
				colvarExtraField1.IsReadOnly = false;
				colvarExtraField1.DefaultSetting = @"";
				colvarExtraField1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraField1);
				
				TableSchema.TableColumn colvarExtraField2 = new TableSchema.TableColumn(schema);
				colvarExtraField2.ColumnName = "ExtraField2";
				colvarExtraField2.DataType = DbType.String;
				colvarExtraField2.MaxLength = 200;
				colvarExtraField2.AutoIncrement = false;
				colvarExtraField2.IsNullable = true;
				colvarExtraField2.IsPrimaryKey = false;
				colvarExtraField2.IsForeignKey = false;
				colvarExtraField2.IsReadOnly = false;
				colvarExtraField2.DefaultSetting = @"";
				colvarExtraField2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraField2);
				
				TableSchema.TableColumn colvarExtraField3 = new TableSchema.TableColumn(schema);
				colvarExtraField3.ColumnName = "ExtraField3";
				colvarExtraField3.DataType = DbType.String;
				colvarExtraField3.MaxLength = 200;
				colvarExtraField3.AutoIncrement = false;
				colvarExtraField3.IsNullable = true;
				colvarExtraField3.IsPrimaryKey = false;
				colvarExtraField3.IsForeignKey = false;
				colvarExtraField3.IsReadOnly = false;
				colvarExtraField3.DefaultSetting = @"";
				colvarExtraField3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraField3);
				
				TableSchema.TableColumn colvarExtraField4 = new TableSchema.TableColumn(schema);
				colvarExtraField4.ColumnName = "ExtraField4";
				colvarExtraField4.DataType = DbType.String;
				colvarExtraField4.MaxLength = 200;
				colvarExtraField4.AutoIncrement = false;
				colvarExtraField4.IsNullable = true;
				colvarExtraField4.IsPrimaryKey = false;
				colvarExtraField4.IsForeignKey = false;
				colvarExtraField4.IsReadOnly = false;
				colvarExtraField4.DefaultSetting = @"";
				colvarExtraField4.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraField4);
				
				TableSchema.TableColumn colvarExtraField5 = new TableSchema.TableColumn(schema);
				colvarExtraField5.ColumnName = "ExtraField5";
				colvarExtraField5.DataType = DbType.String;
				colvarExtraField5.MaxLength = 200;
				colvarExtraField5.AutoIncrement = false;
				colvarExtraField5.IsNullable = true;
				colvarExtraField5.IsPrimaryKey = false;
				colvarExtraField5.IsForeignKey = false;
				colvarExtraField5.IsReadOnly = false;
				colvarExtraField5.DefaultSetting = @"";
				colvarExtraField5.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraField5);
				
				TableSchema.TableColumn colvarExtraField6 = new TableSchema.TableColumn(schema);
				colvarExtraField6.ColumnName = "ExtraField6";
				colvarExtraField6.DataType = DbType.String;
				colvarExtraField6.MaxLength = 200;
				colvarExtraField6.AutoIncrement = false;
				colvarExtraField6.IsNullable = true;
				colvarExtraField6.IsPrimaryKey = false;
				colvarExtraField6.IsForeignKey = false;
				colvarExtraField6.IsReadOnly = false;
				colvarExtraField6.DefaultSetting = @"";
				colvarExtraField6.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraField6);
				
				TableSchema.TableColumn colvarExtraField7 = new TableSchema.TableColumn(schema);
				colvarExtraField7.ColumnName = "ExtraField7";
				colvarExtraField7.DataType = DbType.String;
				colvarExtraField7.MaxLength = 200;
				colvarExtraField7.AutoIncrement = false;
				colvarExtraField7.IsNullable = true;
				colvarExtraField7.IsPrimaryKey = false;
				colvarExtraField7.IsForeignKey = false;
				colvarExtraField7.IsReadOnly = false;
				colvarExtraField7.DefaultSetting = @"";
				colvarExtraField7.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraField7);
				
				TableSchema.TableColumn colvarExtraField8 = new TableSchema.TableColumn(schema);
				colvarExtraField8.ColumnName = "ExtraField8";
				colvarExtraField8.DataType = DbType.String;
				colvarExtraField8.MaxLength = 200;
				colvarExtraField8.AutoIncrement = false;
				colvarExtraField8.IsNullable = true;
				colvarExtraField8.IsPrimaryKey = false;
				colvarExtraField8.IsForeignKey = false;
				colvarExtraField8.IsReadOnly = false;
				colvarExtraField8.DefaultSetting = @"";
				colvarExtraField8.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraField8);
				
				TableSchema.TableColumn colvarExtraField9 = new TableSchema.TableColumn(schema);
				colvarExtraField9.ColumnName = "ExtraField9";
				colvarExtraField9.DataType = DbType.String;
				colvarExtraField9.MaxLength = 200;
				colvarExtraField9.AutoIncrement = false;
				colvarExtraField9.IsNullable = true;
				colvarExtraField9.IsPrimaryKey = false;
				colvarExtraField9.IsForeignKey = false;
				colvarExtraField9.IsReadOnly = false;
				colvarExtraField9.DefaultSetting = @"";
				colvarExtraField9.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraField9);
				
				TableSchema.TableColumn colvarExtraField10 = new TableSchema.TableColumn(schema);
				colvarExtraField10.ColumnName = "ExtraField10";
				colvarExtraField10.DataType = DbType.String;
				colvarExtraField10.MaxLength = 200;
				colvarExtraField10.AutoIncrement = false;
				colvarExtraField10.IsNullable = true;
				colvarExtraField10.IsPrimaryKey = false;
				colvarExtraField10.IsForeignKey = false;
				colvarExtraField10.IsReadOnly = false;
				colvarExtraField10.DefaultSetting = @"";
				colvarExtraField10.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraField10);
				
				TableSchema.TableColumn colvarExtraField11 = new TableSchema.TableColumn(schema);
				colvarExtraField11.ColumnName = "ExtraField11";
				colvarExtraField11.DataType = DbType.String;
				colvarExtraField11.MaxLength = 200;
				colvarExtraField11.AutoIncrement = false;
				colvarExtraField11.IsNullable = true;
				colvarExtraField11.IsPrimaryKey = false;
				colvarExtraField11.IsForeignKey = false;
				colvarExtraField11.IsReadOnly = false;
				colvarExtraField11.DefaultSetting = @"";
				colvarExtraField11.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraField11);
				
				TableSchema.TableColumn colvarESSUserAccount = new TableSchema.TableColumn(schema);
				colvarESSUserAccount.ColumnName = "ESSUserAccount";
				colvarESSUserAccount.DataType = DbType.String;
				colvarESSUserAccount.MaxLength = 200;
				colvarESSUserAccount.AutoIncrement = false;
				colvarESSUserAccount.IsNullable = true;
				colvarESSUserAccount.IsPrimaryKey = false;
				colvarESSUserAccount.IsForeignKey = false;
				colvarESSUserAccount.IsReadOnly = false;
				colvarESSUserAccount.DefaultSetting = @"";
				colvarESSUserAccount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarESSUserAccount);
				
				TableSchema.TableColumn colvarDomainServer = new TableSchema.TableColumn(schema);
				colvarDomainServer.ColumnName = "DomainServer";
				colvarDomainServer.DataType = DbType.String;
				colvarDomainServer.MaxLength = 200;
				colvarDomainServer.AutoIncrement = false;
				colvarDomainServer.IsNullable = true;
				colvarDomainServer.IsPrimaryKey = false;
				colvarDomainServer.IsForeignKey = false;
				colvarDomainServer.IsReadOnly = false;
				colvarDomainServer.DefaultSetting = @"";
				colvarDomainServer.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDomainServer);
				
				TableSchema.TableColumn colvarLevelId = new TableSchema.TableColumn(schema);
				colvarLevelId.ColumnName = "LevelId";
				colvarLevelId.DataType = DbType.String;
				colvarLevelId.MaxLength = 200;
				colvarLevelId.AutoIncrement = false;
				colvarLevelId.IsNullable = true;
				colvarLevelId.IsPrimaryKey = false;
				colvarLevelId.IsForeignKey = false;
				colvarLevelId.IsReadOnly = false;
				colvarLevelId.DefaultSetting = @"";
				colvarLevelId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLevelId);
				
				TableSchema.TableColumn colvarSecretId = new TableSchema.TableColumn(schema);
				colvarSecretId.ColumnName = "SecretId";
				colvarSecretId.DataType = DbType.String;
				colvarSecretId.MaxLength = 200;
				colvarSecretId.AutoIncrement = false;
				colvarSecretId.IsNullable = true;
				colvarSecretId.IsPrimaryKey = false;
				colvarSecretId.IsForeignKey = false;
				colvarSecretId.IsReadOnly = false;
				colvarSecretId.DefaultSetting = @"";
				colvarSecretId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSecretId);
				
				TableSchema.TableColumn colvarCalendarId = new TableSchema.TableColumn(schema);
				colvarCalendarId.ColumnName = "CalendarId";
				colvarCalendarId.DataType = DbType.String;
				colvarCalendarId.MaxLength = 200;
				colvarCalendarId.AutoIncrement = false;
				colvarCalendarId.IsNullable = true;
				colvarCalendarId.IsPrimaryKey = false;
				colvarCalendarId.IsForeignKey = false;
				colvarCalendarId.IsReadOnly = false;
				colvarCalendarId.DefaultSetting = @"";
				colvarCalendarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCalendarId);
				
				TableSchema.TableColumn colvarAttendanceWorkRankId = new TableSchema.TableColumn(schema);
				colvarAttendanceWorkRankId.ColumnName = "AttendanceWorkRankId";
				colvarAttendanceWorkRankId.DataType = DbType.String;
				colvarAttendanceWorkRankId.MaxLength = 200;
				colvarAttendanceWorkRankId.AutoIncrement = false;
				colvarAttendanceWorkRankId.IsNullable = true;
				colvarAttendanceWorkRankId.IsPrimaryKey = false;
				colvarAttendanceWorkRankId.IsForeignKey = false;
				colvarAttendanceWorkRankId.IsReadOnly = false;
				colvarAttendanceWorkRankId.DefaultSetting = @"";
				colvarAttendanceWorkRankId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAttendanceWorkRankId);
				
				TableSchema.TableColumn colvarAttendanceRestRankId = new TableSchema.TableColumn(schema);
				colvarAttendanceRestRankId.ColumnName = "AttendanceRestRankId";
				colvarAttendanceRestRankId.DataType = DbType.String;
				colvarAttendanceRestRankId.MaxLength = 200;
				colvarAttendanceRestRankId.AutoIncrement = false;
				colvarAttendanceRestRankId.IsNullable = true;
				colvarAttendanceRestRankId.IsPrimaryKey = false;
				colvarAttendanceRestRankId.IsForeignKey = false;
				colvarAttendanceRestRankId.IsReadOnly = false;
				colvarAttendanceRestRankId.DefaultSetting = @"";
				colvarAttendanceRestRankId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAttendanceRestRankId);
				
				TableSchema.TableColumn colvarLocationPostalCode = new TableSchema.TableColumn(schema);
				colvarLocationPostalCode.ColumnName = "LocationPostalCode";
				colvarLocationPostalCode.DataType = DbType.String;
				colvarLocationPostalCode.MaxLength = 200;
				colvarLocationPostalCode.AutoIncrement = false;
				colvarLocationPostalCode.IsNullable = true;
				colvarLocationPostalCode.IsPrimaryKey = false;
				colvarLocationPostalCode.IsForeignKey = false;
				colvarLocationPostalCode.IsReadOnly = false;
				colvarLocationPostalCode.DefaultSetting = @"";
				colvarLocationPostalCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLocationPostalCode);
				
				TableSchema.TableColumn colvarGrade = new TableSchema.TableColumn(schema);
				colvarGrade.ColumnName = "Grade";
				colvarGrade.DataType = DbType.String;
				colvarGrade.MaxLength = 200;
				colvarGrade.AutoIncrement = false;
				colvarGrade.IsNullable = true;
				colvarGrade.IsPrimaryKey = false;
				colvarGrade.IsForeignKey = false;
				colvarGrade.IsReadOnly = false;
				colvarGrade.DefaultSetting = @"";
				colvarGrade.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGrade);
				
				TableSchema.TableColumn colvarRank = new TableSchema.TableColumn(schema);
				colvarRank.ColumnName = "Rank";
				colvarRank.DataType = DbType.String;
				colvarRank.MaxLength = 200;
				colvarRank.AutoIncrement = false;
				colvarRank.IsNullable = true;
				colvarRank.IsPrimaryKey = false;
				colvarRank.IsForeignKey = false;
				colvarRank.IsReadOnly = false;
				colvarRank.DefaultSetting = @"";
				colvarRank.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRank);
				
				TableSchema.TableColumn colvarHoroscopeID = new TableSchema.TableColumn(schema);
				colvarHoroscopeID.ColumnName = "HoroscopeID";
				colvarHoroscopeID.DataType = DbType.String;
				colvarHoroscopeID.MaxLength = 200;
				colvarHoroscopeID.AutoIncrement = false;
				colvarHoroscopeID.IsNullable = true;
				colvarHoroscopeID.IsPrimaryKey = false;
				colvarHoroscopeID.IsForeignKey = false;
				colvarHoroscopeID.IsReadOnly = false;
				colvarHoroscopeID.DefaultSetting = @"";
				colvarHoroscopeID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHoroscopeID);
				
				TableSchema.TableColumn colvarPositionRankID = new TableSchema.TableColumn(schema);
				colvarPositionRankID.ColumnName = "PositionRankID";
				colvarPositionRankID.DataType = DbType.Guid;
				colvarPositionRankID.MaxLength = 0;
				colvarPositionRankID.AutoIncrement = false;
				colvarPositionRankID.IsNullable = true;
				colvarPositionRankID.IsPrimaryKey = false;
				colvarPositionRankID.IsForeignKey = false;
				colvarPositionRankID.IsReadOnly = false;
				colvarPositionRankID.DefaultSetting = @"";
				colvarPositionRankID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPositionRankID);
				
				TableSchema.TableColumn colvarPositionRankName = new TableSchema.TableColumn(schema);
				colvarPositionRankName.ColumnName = "PositionRankName";
				colvarPositionRankName.DataType = DbType.String;
				colvarPositionRankName.MaxLength = 200;
				colvarPositionRankName.AutoIncrement = false;
				colvarPositionRankName.IsNullable = true;
				colvarPositionRankName.IsPrimaryKey = false;
				colvarPositionRankName.IsForeignKey = false;
				colvarPositionRankName.IsReadOnly = false;
				colvarPositionRankName.DefaultSetting = @"";
				colvarPositionRankName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPositionRankName);
				
				TableSchema.TableColumn colvarNewReportDate = new TableSchema.TableColumn(schema);
				colvarNewReportDate.ColumnName = "NewReportDate";
				colvarNewReportDate.DataType = DbType.DateTime;
				colvarNewReportDate.MaxLength = 0;
				colvarNewReportDate.AutoIncrement = false;
				colvarNewReportDate.IsNullable = true;
				colvarNewReportDate.IsPrimaryKey = false;
				colvarNewReportDate.IsForeignKey = false;
				colvarNewReportDate.IsReadOnly = false;
				colvarNewReportDate.DefaultSetting = @"";
				colvarNewReportDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNewReportDate);
				
				TableSchema.TableColumn colvarSalaryBankCardId = new TableSchema.TableColumn(schema);
				colvarSalaryBankCardId.ColumnName = "SalaryBankCardId";
				colvarSalaryBankCardId.DataType = DbType.Guid;
				colvarSalaryBankCardId.MaxLength = 0;
				colvarSalaryBankCardId.AutoIncrement = false;
				colvarSalaryBankCardId.IsNullable = true;
				colvarSalaryBankCardId.IsPrimaryKey = false;
				colvarSalaryBankCardId.IsForeignKey = false;
				colvarSalaryBankCardId.IsReadOnly = false;
				colvarSalaryBankCardId.DefaultSetting = @"";
				colvarSalaryBankCardId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalaryBankCardId);
				
				TableSchema.TableColumn colvarStatusCode = new TableSchema.TableColumn(schema);
				colvarStatusCode.ColumnName = "StatusCode";
				colvarStatusCode.DataType = DbType.Boolean;
				colvarStatusCode.MaxLength = 0;
				colvarStatusCode.AutoIncrement = false;
				colvarStatusCode.IsNullable = true;
				colvarStatusCode.IsPrimaryKey = false;
				colvarStatusCode.IsForeignKey = false;
				colvarStatusCode.IsReadOnly = false;
				colvarStatusCode.DefaultSetting = @"";
				colvarStatusCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatusCode);
				
				TableSchema.TableColumn colvarUniqueID = new TableSchema.TableColumn(schema);
				colvarUniqueID.ColumnName = "UniqueID";
				colvarUniqueID.DataType = DbType.String;
				colvarUniqueID.MaxLength = 200;
				colvarUniqueID.AutoIncrement = false;
				colvarUniqueID.IsNullable = true;
				colvarUniqueID.IsPrimaryKey = false;
				colvarUniqueID.IsForeignKey = false;
				colvarUniqueID.IsReadOnly = false;
				colvarUniqueID.DefaultSetting = @"";
				colvarUniqueID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniqueID);
				
				TableSchema.TableColumn colvarOtherCountTypeId = new TableSchema.TableColumn(schema);
				colvarOtherCountTypeId.ColumnName = "OtherCountTypeId";
				colvarOtherCountTypeId.DataType = DbType.String;
				colvarOtherCountTypeId.MaxLength = 200;
				colvarOtherCountTypeId.AutoIncrement = false;
				colvarOtherCountTypeId.IsNullable = true;
				colvarOtherCountTypeId.IsPrimaryKey = false;
				colvarOtherCountTypeId.IsForeignKey = false;
				colvarOtherCountTypeId.IsReadOnly = false;
				colvarOtherCountTypeId.DefaultSetting = @"";
				colvarOtherCountTypeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOtherCountTypeId);
				
				TableSchema.TableColumn colvarDecisionLevel = new TableSchema.TableColumn(schema);
				colvarDecisionLevel.ColumnName = "DecisionLevel";
				colvarDecisionLevel.DataType = DbType.String;
				colvarDecisionLevel.MaxLength = 200;
				colvarDecisionLevel.AutoIncrement = false;
				colvarDecisionLevel.IsNullable = true;
				colvarDecisionLevel.IsPrimaryKey = false;
				colvarDecisionLevel.IsForeignKey = false;
				colvarDecisionLevel.IsReadOnly = false;
				colvarDecisionLevel.DefaultSetting = @"";
				colvarDecisionLevel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDecisionLevel);
				
				TableSchema.TableColumn colvarSalaryOverId = new TableSchema.TableColumn(schema);
				colvarSalaryOverId.ColumnName = "SalaryOverId";
				colvarSalaryOverId.DataType = DbType.String;
				colvarSalaryOverId.MaxLength = 200;
				colvarSalaryOverId.AutoIncrement = false;
				colvarSalaryOverId.IsNullable = true;
				colvarSalaryOverId.IsPrimaryKey = false;
				colvarSalaryOverId.IsForeignKey = false;
				colvarSalaryOverId.IsReadOnly = false;
				colvarSalaryOverId.DefaultSetting = @"";
				colvarSalaryOverId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalaryOverId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["EasyFlowDB"].AddSchema("Employee",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IsTowner")]
		[Bindable(true)]
		public string IsTowner 
		{
			get { return GetColumnValue<string>(Columns.IsTowner); }
			set { SetColumnValue(Columns.IsTowner, value); }
		}
		  
		[XmlAttribute("Source")]
		[Bindable(true)]
		public string Source 
		{
			get { return GetColumnValue<string>(Columns.Source); }
			set { SetColumnValue(Columns.Source, value); }
		}
		  
		[XmlAttribute("FundBeginDate")]
		[Bindable(true)]
		public DateTime? FundBeginDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.FundBeginDate); }
			set { SetColumnValue(Columns.FundBeginDate, value); }
		}
		  
		[XmlAttribute("FundEndDate")]
		[Bindable(true)]
		public DateTime? FundEndDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.FundEndDate); }
			set { SetColumnValue(Columns.FundEndDate, value); }
		}
		  
		[XmlAttribute("IsFundId")]
		[Bindable(true)]
		public string IsFundId 
		{
			get { return GetColumnValue<string>(Columns.IsFundId); }
			set { SetColumnValue(Columns.IsFundId, value); }
		}
		  
		[XmlAttribute("IsAuthorized")]
		[Bindable(true)]
		public string IsAuthorized 
		{
			get { return GetColumnValue<string>(Columns.IsAuthorized); }
			set { SetColumnValue(Columns.IsAuthorized, value); }
		}
		  
		[XmlAttribute("IsReserveId")]
		[Bindable(true)]
		public string IsReserveId 
		{
			get { return GetColumnValue<string>(Columns.IsReserveId); }
			set { SetColumnValue(Columns.IsReserveId, value); }
		}
		  
		[XmlAttribute("InnerEmployee")]
		[Bindable(true)]
		public string InnerEmployee 
		{
			get { return GetColumnValue<string>(Columns.InnerEmployee); }
			set { SetColumnValue(Columns.InnerEmployee, value); }
		}
		  
		[XmlAttribute("CertificateLimit")]
		[Bindable(true)]
		public DateTime? CertificateLimit 
		{
			get { return GetColumnValue<DateTime?>(Columns.CertificateLimit); }
			set { SetColumnValue(Columns.CertificateLimit, value); }
		}
		  
		[XmlAttribute("IsThisYearId")]
		[Bindable(true)]
		public string IsThisYearId 
		{
			get { return GetColumnValue<string>(Columns.IsThisYearId); }
			set { SetColumnValue(Columns.IsThisYearId, value); }
		}
		  
		[XmlAttribute("BonusRate")]
		[Bindable(true)]
		public decimal? BonusRate 
		{
			get { return GetColumnValue<decimal?>(Columns.BonusRate); }
			set { SetColumnValue(Columns.BonusRate, value); }
		}
		  
		[XmlAttribute("CCBCode")]
		[Bindable(true)]
		public string CCBCode 
		{
			get { return GetColumnValue<string>(Columns.CCBCode); }
			set { SetColumnValue(Columns.CCBCode, value); }
		}
		  
		[XmlAttribute("JobDate")]
		[Bindable(true)]
		public DateTime? JobDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.JobDate); }
			set { SetColumnValue(Columns.JobDate, value); }
		}
		  
		[XmlAttribute("EmployeeId")]
		[Bindable(true)]
		public Guid EmployeeId 
		{
			get { return GetColumnValue<Guid>(Columns.EmployeeId); }
			set { SetColumnValue(Columns.EmployeeId, value); }
		}
		  
		[XmlAttribute("Number")]
		[Bindable(true)]
		public int? Number 
		{
			get { return GetColumnValue<int?>(Columns.Number); }
			set { SetColumnValue(Columns.Number, value); }
		}
		  
		[XmlAttribute("CnName")]
		[Bindable(true)]
		public string CnName 
		{
			get { return GetColumnValue<string>(Columns.CnName); }
			set { SetColumnValue(Columns.CnName, value); }
		}
		  
		[XmlAttribute("EnName")]
		[Bindable(true)]
		public string EnName 
		{
			get { return GetColumnValue<string>(Columns.EnName); }
			set { SetColumnValue(Columns.EnName, value); }
		}
		  
		[XmlAttribute("IDCardNo")]
		[Bindable(true)]
		public string IDCardNo 
		{
			get { return GetColumnValue<string>(Columns.IDCardNo); }
			set { SetColumnValue(Columns.IDCardNo, value); }
		}
		  
		[XmlAttribute("IDCardKindId")]
		[Bindable(true)]
		public string IDCardKindId 
		{
			get { return GetColumnValue<string>(Columns.IDCardKindId); }
			set { SetColumnValue(Columns.IDCardKindId, value); }
		}
		  
		[XmlAttribute("SocialWelfareNo")]
		[Bindable(true)]
		public string SocialWelfareNo 
		{
			get { return GetColumnValue<string>(Columns.SocialWelfareNo); }
			set { SetColumnValue(Columns.SocialWelfareNo, value); }
		}
		  
		[XmlAttribute("BirthDate")]
		[Bindable(true)]
		public DateTime? BirthDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.BirthDate); }
			set { SetColumnValue(Columns.BirthDate, value); }
		}
		  
		[XmlAttribute("GenderId")]
		[Bindable(true)]
		public string GenderId 
		{
			get { return GetColumnValue<string>(Columns.GenderId); }
			set { SetColumnValue(Columns.GenderId, value); }
		}
		  
		[XmlAttribute("DateX")]
		[Bindable(true)]
		public DateTime? DateX 
		{
			get { return GetColumnValue<DateTime?>(Columns.DateX); }
			set { SetColumnValue(Columns.DateX, value); }
		}
		  
		[XmlAttribute("EmployeeStateId")]
		[Bindable(true)]
		public string EmployeeStateId 
		{
			get { return GetColumnValue<string>(Columns.EmployeeStateId); }
			set { SetColumnValue(Columns.EmployeeStateId, value); }
		}
		  
		[XmlAttribute("WorkTypeId")]
		[Bindable(true)]
		public string WorkTypeId 
		{
			get { return GetColumnValue<string>(Columns.WorkTypeId); }
			set { SetColumnValue(Columns.WorkTypeId, value); }
		}
		  
		[XmlAttribute("MarriageId")]
		[Bindable(true)]
		public string MarriageId 
		{
			get { return GetColumnValue<string>(Columns.MarriageId); }
			set { SetColumnValue(Columns.MarriageId, value); }
		}
		  
		[XmlAttribute("ServiceYears")]
		[Bindable(true)]
		public string ServiceYears 
		{
			get { return GetColumnValue<string>(Columns.ServiceYears); }
			set { SetColumnValue(Columns.ServiceYears, value); }
		}
		  
		[XmlAttribute("CorporationId")]
		[Bindable(true)]
		public Guid? CorporationId 
		{
			get { return GetColumnValue<Guid?>(Columns.CorporationId); }
			set { SetColumnValue(Columns.CorporationId, value); }
		}
		  
		[XmlAttribute("Telephone")]
		[Bindable(true)]
		public string Telephone 
		{
			get { return GetColumnValue<string>(Columns.Telephone); }
			set { SetColumnValue(Columns.Telephone, value); }
		}
		  
		[XmlAttribute("AreaId")]
		[Bindable(true)]
		public Guid? AreaId 
		{
			get { return GetColumnValue<Guid?>(Columns.AreaId); }
			set { SetColumnValue(Columns.AreaId, value); }
		}
		  
		[XmlAttribute("FactoryId")]
		[Bindable(true)]
		public string FactoryId 
		{
			get { return GetColumnValue<string>(Columns.FactoryId); }
			set { SetColumnValue(Columns.FactoryId, value); }
		}
		  
		[XmlAttribute("DepartmentId")]
		[Bindable(true)]
		public Guid? DepartmentId 
		{
			get { return GetColumnValue<Guid?>(Columns.DepartmentId); }
			set { SetColumnValue(Columns.DepartmentId, value); }
		}
		  
		[XmlAttribute("JobId")]
		[Bindable(true)]
		public Guid? JobId 
		{
			get { return GetColumnValue<Guid?>(Columns.JobId); }
			set { SetColumnValue(Columns.JobId, value); }
		}
		  
		[XmlAttribute("PartTimeJob")]
		[Bindable(true)]
		public string PartTimeJob 
		{
			get { return GetColumnValue<string>(Columns.PartTimeJob); }
			set { SetColumnValue(Columns.PartTimeJob, value); }
		}
		  
		[XmlAttribute("DirectorId")]
		[Bindable(true)]
		public Guid? DirectorId 
		{
			get { return GetColumnValue<Guid?>(Columns.DirectorId); }
			set { SetColumnValue(Columns.DirectorId, value); }
		}
		  
		[XmlAttribute("MobilePhone")]
		[Bindable(true)]
		public string MobilePhone 
		{
			get { return GetColumnValue<string>(Columns.MobilePhone); }
			set { SetColumnValue(Columns.MobilePhone, value); }
		}
		  
		[XmlAttribute("Email")]
		[Bindable(true)]
		public string Email 
		{
			get { return GetColumnValue<string>(Columns.Email); }
			set { SetColumnValue(Columns.Email, value); }
		}
		  
		[XmlAttribute("EducationId")]
		[Bindable(true)]
		public string EducationId 
		{
			get { return GetColumnValue<string>(Columns.EducationId); }
			set { SetColumnValue(Columns.EducationId, value); }
		}
		  
		[XmlAttribute("Major")]
		[Bindable(true)]
		public string Major 
		{
			get { return GetColumnValue<string>(Columns.Major); }
			set { SetColumnValue(Columns.Major, value); }
		}
		  
		[XmlAttribute("NativePlace")]
		[Bindable(true)]
		public string NativePlace 
		{
			get { return GetColumnValue<string>(Columns.NativePlace); }
			set { SetColumnValue(Columns.NativePlace, value); }
		}
		  
		[XmlAttribute("PoliticalIdentityId")]
		[Bindable(true)]
		public string PoliticalIdentityId 
		{
			get { return GetColumnValue<string>(Columns.PoliticalIdentityId); }
			set { SetColumnValue(Columns.PoliticalIdentityId, value); }
		}
		  
		[XmlAttribute("Location")]
		[Bindable(true)]
		public string Location 
		{
			get { return GetColumnValue<string>(Columns.Location); }
			set { SetColumnValue(Columns.Location, value); }
		}
		  
		[XmlAttribute("Postalcode")]
		[Bindable(true)]
		public string Postalcode 
		{
			get { return GetColumnValue<string>(Columns.Postalcode); }
			set { SetColumnValue(Columns.Postalcode, value); }
		}
		  
		[XmlAttribute("NationId")]
		[Bindable(true)]
		public string NationId 
		{
			get { return GetColumnValue<string>(Columns.NationId); }
			set { SetColumnValue(Columns.NationId, value); }
		}
		  
		[XmlAttribute("Address")]
		[Bindable(true)]
		public string Address 
		{
			get { return GetColumnValue<string>(Columns.Address); }
			set { SetColumnValue(Columns.Address, value); }
		}
		  
		[XmlAttribute("CostCenterId")]
		[Bindable(true)]
		public Guid? CostCenterId 
		{
			get { return GetColumnValue<Guid?>(Columns.CostCenterId); }
			set { SetColumnValue(Columns.CostCenterId, value); }
		}
		  
		[XmlAttribute("Interest")]
		[Bindable(true)]
		public string Interest 
		{
			get { return GetColumnValue<string>(Columns.Interest); }
			set { SetColumnValue(Columns.Interest, value); }
		}
		  
		[XmlAttribute("Picture")]
		[Bindable(true)]
		public byte[] Picture 
		{
			get { return GetColumnValue<byte[]>(Columns.Picture); }
			set { SetColumnValue(Columns.Picture, value); }
		}
		  
		[XmlAttribute("Password")]
		[Bindable(true)]
		public string Password 
		{
			get { return GetColumnValue<string>(Columns.Password); }
			set { SetColumnValue(Columns.Password, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public string Code 
		{
			get { return GetColumnValue<string>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("CreateDate")]
		[Bindable(true)]
		public DateTime? CreateDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateDate); }
			set { SetColumnValue(Columns.CreateDate, value); }
		}
		  
		[XmlAttribute("LastModifiedDate")]
		[Bindable(true)]
		public DateTime? LastModifiedDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastModifiedDate); }
			set { SetColumnValue(Columns.LastModifiedDate, value); }
		}
		  
		[XmlAttribute("CreateBy")]
		[Bindable(true)]
		public Guid? CreateBy 
		{
			get { return GetColumnValue<Guid?>(Columns.CreateBy); }
			set { SetColumnValue(Columns.CreateBy, value); }
		}
		  
		[XmlAttribute("LastModifiedBy")]
		[Bindable(true)]
		public Guid? LastModifiedBy 
		{
			get { return GetColumnValue<Guid?>(Columns.LastModifiedBy); }
			set { SetColumnValue(Columns.LastModifiedBy, value); }
		}
		  
		[XmlAttribute("Flag")]
		[Bindable(true)]
		public bool? Flag 
		{
			get { return GetColumnValue<bool?>(Columns.Flag); }
			set { SetColumnValue(Columns.Flag, value); }
		}
		  
		[XmlAttribute("IsPrincipal")]
		[Bindable(true)]
		public bool? IsPrincipal 
		{
			get { return GetColumnValue<bool?>(Columns.IsPrincipal); }
			set { SetColumnValue(Columns.IsPrincipal, value); }
		}
		  
		[XmlAttribute("IsSend")]
		[Bindable(true)]
		public bool? IsSend 
		{
			get { return GetColumnValue<bool?>(Columns.IsSend); }
			set { SetColumnValue(Columns.IsSend, value); }
		}
		  
		[XmlAttribute("ITCode")]
		[Bindable(true)]
		public string ITCode 
		{
			get { return GetColumnValue<string>(Columns.ITCode); }
			set { SetColumnValue(Columns.ITCode, value); }
		}
		  
		[XmlAttribute("AssignReason")]
		[Bindable(true)]
		public string AssignReason 
		{
			get { return GetColumnValue<string>(Columns.AssignReason); }
			set { SetColumnValue(Columns.AssignReason, value); }
		}
		  
		[XmlAttribute("OwnerId")]
		[Bindable(true)]
		public string OwnerId 
		{
			get { return GetColumnValue<string>(Columns.OwnerId); }
			set { SetColumnValue(Columns.OwnerId, value); }
		}
		  
		[XmlAttribute("OtherBirthDate")]
		[Bindable(true)]
		public DateTime? OtherBirthDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.OtherBirthDate); }
			set { SetColumnValue(Columns.OtherBirthDate, value); }
		}
		  
		[XmlAttribute("Remark")]
		[Bindable(true)]
		public string Remark 
		{
			get { return GetColumnValue<string>(Columns.Remark); }
			set { SetColumnValue(Columns.Remark, value); }
		}
		  
		[XmlAttribute("IntendTransformDate")]
		[Bindable(true)]
		public DateTime? IntendTransformDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.IntendTransformDate); }
			set { SetColumnValue(Columns.IntendTransformDate, value); }
		}
		  
		[XmlAttribute("ATMealFeeType")]
		[Bindable(true)]
		public string ATMealFeeType 
		{
			get { return GetColumnValue<string>(Columns.ATMealFeeType); }
			set { SetColumnValue(Columns.ATMealFeeType, value); }
		}
		  
		[XmlAttribute("LastWorkDate")]
		[Bindable(true)]
		public DateTime? LastWorkDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastWorkDate); }
			set { SetColumnValue(Columns.LastWorkDate, value); }
		}
		  
		[XmlAttribute("DirectDeptId")]
		[Bindable(true)]
		public Guid? DirectDeptId 
		{
			get { return GetColumnValue<Guid?>(Columns.DirectDeptId); }
			set { SetColumnValue(Columns.DirectDeptId, value); }
		}
		  
		[XmlAttribute("TimeType")]
		[Bindable(true)]
		public string TimeType 
		{
			get { return GetColumnValue<string>(Columns.TimeType); }
			set { SetColumnValue(Columns.TimeType, value); }
		}
		  
		[XmlAttribute("HomePhone")]
		[Bindable(true)]
		public string HomePhone 
		{
			get { return GetColumnValue<string>(Columns.HomePhone); }
			set { SetColumnValue(Columns.HomePhone, value); }
		}
		  
		[XmlAttribute("TransformDate")]
		[Bindable(true)]
		public DateTime? TransformDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.TransformDate); }
			set { SetColumnValue(Columns.TransformDate, value); }
		}
		  
		[XmlAttribute("WorkBeginDate")]
		[Bindable(true)]
		public DateTime? WorkBeginDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.WorkBeginDate); }
			set { SetColumnValue(Columns.WorkBeginDate, value); }
		}
		  
		[XmlAttribute("HealthStatus")]
		[Bindable(true)]
		public string HealthStatus 
		{
			get { return GetColumnValue<string>(Columns.HealthStatus); }
			set { SetColumnValue(Columns.HealthStatus, value); }
		}
		  
		[XmlAttribute("Stature")]
		[Bindable(true)]
		public string Stature 
		{
			get { return GetColumnValue<string>(Columns.Stature); }
			set { SetColumnValue(Columns.Stature, value); }
		}
		  
		[XmlAttribute("Weight")]
		[Bindable(true)]
		public string Weight 
		{
			get { return GetColumnValue<string>(Columns.Weight); }
			set { SetColumnValue(Columns.Weight, value); }
		}
		  
		[XmlAttribute("EyeSight")]
		[Bindable(true)]
		public string EyeSight 
		{
			get { return GetColumnValue<string>(Columns.EyeSight); }
			set { SetColumnValue(Columns.EyeSight, value); }
		}
		  
		[XmlAttribute("BloodType")]
		[Bindable(true)]
		public string BloodType 
		{
			get { return GetColumnValue<string>(Columns.BloodType); }
			set { SetColumnValue(Columns.BloodType, value); }
		}
		  
		[XmlAttribute("Personality")]
		[Bindable(true)]
		public string Personality 
		{
			get { return GetColumnValue<string>(Columns.Personality); }
			set { SetColumnValue(Columns.Personality, value); }
		}
		  
		[XmlAttribute("DiseaseHistory")]
		[Bindable(true)]
		public string DiseaseHistory 
		{
			get { return GetColumnValue<string>(Columns.DiseaseHistory); }
			set { SetColumnValue(Columns.DiseaseHistory, value); }
		}
		  
		[XmlAttribute("CountryId")]
		[Bindable(true)]
		public string CountryId 
		{
			get { return GetColumnValue<string>(Columns.CountryId); }
			set { SetColumnValue(Columns.CountryId, value); }
		}
		  
		[XmlAttribute("ProvinceId")]
		[Bindable(true)]
		public string ProvinceId 
		{
			get { return GetColumnValue<string>(Columns.ProvinceId); }
			set { SetColumnValue(Columns.ProvinceId, value); }
		}
		  
		[XmlAttribute("WorkingAgeBeginDate")]
		[Bindable(true)]
		public DateTime? WorkingAgeBeginDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.WorkingAgeBeginDate); }
			set { SetColumnValue(Columns.WorkingAgeBeginDate, value); }
		}
		  
		[XmlAttribute("TWTaxTypeId")]
		[Bindable(true)]
		public string TWTaxTypeId 
		{
			get { return GetColumnValue<string>(Columns.TWTaxTypeId); }
			set { SetColumnValue(Columns.TWTaxTypeId, value); }
		}
		  
		[XmlAttribute("TWFixedValue")]
		[Bindable(true)]
		public decimal? TWFixedValue 
		{
			get { return GetColumnValue<decimal?>(Columns.TWFixedValue); }
			set { SetColumnValue(Columns.TWFixedValue, value); }
		}
		  
		[XmlAttribute("TWFixedTaxRate")]
		[Bindable(true)]
		public decimal? TWFixedTaxRate 
		{
			get { return GetColumnValue<decimal?>(Columns.TWFixedTaxRate); }
			set { SetColumnValue(Columns.TWFixedTaxRate, value); }
		}
		  
		[XmlAttribute("FamilyNumber")]
		[Bindable(true)]
		public int? FamilyNumber 
		{
			get { return GetColumnValue<int?>(Columns.FamilyNumber); }
			set { SetColumnValue(Columns.FamilyNumber, value); }
		}
		  
		[XmlAttribute("AdmitWorkAge")]
		[Bindable(true)]
		public decimal? AdmitWorkAge 
		{
			get { return GetColumnValue<decimal?>(Columns.AdmitWorkAge); }
			set { SetColumnValue(Columns.AdmitWorkAge, value); }
		}
		  
		[XmlAttribute("CharacterTest")]
		[Bindable(true)]
		public string CharacterTest 
		{
			get { return GetColumnValue<string>(Columns.CharacterTest); }
			set { SetColumnValue(Columns.CharacterTest, value); }
		}
		  
		[XmlAttribute("Deputy")]
		[Bindable(true)]
		public Guid? Deputy 
		{
			get { return GetColumnValue<Guid?>(Columns.Deputy); }
			set { SetColumnValue(Columns.Deputy, value); }
		}
		  
		[XmlAttribute("EmployTypeId")]
		[Bindable(true)]
		public string EmployTypeId 
		{
			get { return GetColumnValue<string>(Columns.EmployTypeId); }
			set { SetColumnValue(Columns.EmployTypeId, value); }
		}
		  
		[XmlAttribute("PositionId")]
		[Bindable(true)]
		public Guid? PositionId 
		{
			get { return GetColumnValue<Guid?>(Columns.PositionId); }
			set { SetColumnValue(Columns.PositionId, value); }
		}
		  
		[XmlAttribute("PositionGroupId")]
		[Bindable(true)]
		public string PositionGroupId 
		{
			get { return GetColumnValue<string>(Columns.PositionGroupId); }
			set { SetColumnValue(Columns.PositionGroupId, value); }
		}
		  
		[XmlAttribute("EnterDate")]
		[Bindable(true)]
		public DateTime? EnterDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.EnterDate); }
			set { SetColumnValue(Columns.EnterDate, value); }
		}
		  
		[XmlAttribute("ResidenceExpireDate")]
		[Bindable(true)]
		public DateTime? ResidenceExpireDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ResidenceExpireDate); }
			set { SetColumnValue(Columns.ResidenceExpireDate, value); }
		}
		  
		[XmlAttribute("Age")]
		[Bindable(true)]
		public int? Age 
		{
			get { return GetColumnValue<int?>(Columns.Age); }
			set { SetColumnValue(Columns.Age, value); }
		}
		  
		[XmlAttribute("WorkingAge")]
		[Bindable(true)]
		public int? WorkingAge 
		{
			get { return GetColumnValue<int?>(Columns.WorkingAge); }
			set { SetColumnValue(Columns.WorkingAge, value); }
		}
		  
		[XmlAttribute("OtherCorpWorkingAge")]
		[Bindable(true)]
		public int? OtherCorpWorkingAge 
		{
			get { return GetColumnValue<int?>(Columns.OtherCorpWorkingAge); }
			set { SetColumnValue(Columns.OtherCorpWorkingAge, value); }
		}
		  
		[XmlAttribute("HomeMail")]
		[Bindable(true)]
		public string HomeMail 
		{
			get { return GetColumnValue<string>(Columns.HomeMail); }
			set { SetColumnValue(Columns.HomeMail, value); }
		}
		  
		[XmlAttribute("Hobbies")]
		[Bindable(true)]
		public string Hobbies 
		{
			get { return GetColumnValue<string>(Columns.Hobbies); }
			set { SetColumnValue(Columns.Hobbies, value); }
		}
		  
		[XmlAttribute("MultiSysCode")]
		[Bindable(true)]
		public string MultiSysCode 
		{
			get { return GetColumnValue<string>(Columns.MultiSysCode); }
			set { SetColumnValue(Columns.MultiSysCode, value); }
		}
		  
		[XmlAttribute("ZhiJian")]
		[Bindable(true)]
		public string ZhiJian 
		{
			get { return GetColumnValue<string>(Columns.ZhiJian); }
			set { SetColumnValue(Columns.ZhiJian, value); }
		}
		  
		[XmlAttribute("ExtraField1")]
		[Bindable(true)]
		public string ExtraField1 
		{
			get { return GetColumnValue<string>(Columns.ExtraField1); }
			set { SetColumnValue(Columns.ExtraField1, value); }
		}
		  
		[XmlAttribute("ExtraField2")]
		[Bindable(true)]
		public string ExtraField2 
		{
			get { return GetColumnValue<string>(Columns.ExtraField2); }
			set { SetColumnValue(Columns.ExtraField2, value); }
		}
		  
		[XmlAttribute("ExtraField3")]
		[Bindable(true)]
		public string ExtraField3 
		{
			get { return GetColumnValue<string>(Columns.ExtraField3); }
			set { SetColumnValue(Columns.ExtraField3, value); }
		}
		  
		[XmlAttribute("ExtraField4")]
		[Bindable(true)]
		public string ExtraField4 
		{
			get { return GetColumnValue<string>(Columns.ExtraField4); }
			set { SetColumnValue(Columns.ExtraField4, value); }
		}
		  
		[XmlAttribute("ExtraField5")]
		[Bindable(true)]
		public string ExtraField5 
		{
			get { return GetColumnValue<string>(Columns.ExtraField5); }
			set { SetColumnValue(Columns.ExtraField5, value); }
		}
		  
		[XmlAttribute("ExtraField6")]
		[Bindable(true)]
		public string ExtraField6 
		{
			get { return GetColumnValue<string>(Columns.ExtraField6); }
			set { SetColumnValue(Columns.ExtraField6, value); }
		}
		  
		[XmlAttribute("ExtraField7")]
		[Bindable(true)]
		public string ExtraField7 
		{
			get { return GetColumnValue<string>(Columns.ExtraField7); }
			set { SetColumnValue(Columns.ExtraField7, value); }
		}
		  
		[XmlAttribute("ExtraField8")]
		[Bindable(true)]
		public string ExtraField8 
		{
			get { return GetColumnValue<string>(Columns.ExtraField8); }
			set { SetColumnValue(Columns.ExtraField8, value); }
		}
		  
		[XmlAttribute("ExtraField9")]
		[Bindable(true)]
		public string ExtraField9 
		{
			get { return GetColumnValue<string>(Columns.ExtraField9); }
			set { SetColumnValue(Columns.ExtraField9, value); }
		}
		  
		[XmlAttribute("ExtraField10")]
		[Bindable(true)]
		public string ExtraField10 
		{
			get { return GetColumnValue<string>(Columns.ExtraField10); }
			set { SetColumnValue(Columns.ExtraField10, value); }
		}
		  
		[XmlAttribute("ExtraField11")]
		[Bindable(true)]
		public string ExtraField11 
		{
			get { return GetColumnValue<string>(Columns.ExtraField11); }
			set { SetColumnValue(Columns.ExtraField11, value); }
		}
		  
		[XmlAttribute("ESSUserAccount")]
		[Bindable(true)]
		public string ESSUserAccount 
		{
			get { return GetColumnValue<string>(Columns.ESSUserAccount); }
			set { SetColumnValue(Columns.ESSUserAccount, value); }
		}
		  
		[XmlAttribute("DomainServer")]
		[Bindable(true)]
		public string DomainServer 
		{
			get { return GetColumnValue<string>(Columns.DomainServer); }
			set { SetColumnValue(Columns.DomainServer, value); }
		}
		  
		[XmlAttribute("LevelId")]
		[Bindable(true)]
		public string LevelId 
		{
			get { return GetColumnValue<string>(Columns.LevelId); }
			set { SetColumnValue(Columns.LevelId, value); }
		}
		  
		[XmlAttribute("SecretId")]
		[Bindable(true)]
		public string SecretId 
		{
			get { return GetColumnValue<string>(Columns.SecretId); }
			set { SetColumnValue(Columns.SecretId, value); }
		}
		  
		[XmlAttribute("CalendarId")]
		[Bindable(true)]
		public string CalendarId 
		{
			get { return GetColumnValue<string>(Columns.CalendarId); }
			set { SetColumnValue(Columns.CalendarId, value); }
		}
		  
		[XmlAttribute("AttendanceWorkRankId")]
		[Bindable(true)]
		public string AttendanceWorkRankId 
		{
			get { return GetColumnValue<string>(Columns.AttendanceWorkRankId); }
			set { SetColumnValue(Columns.AttendanceWorkRankId, value); }
		}
		  
		[XmlAttribute("AttendanceRestRankId")]
		[Bindable(true)]
		public string AttendanceRestRankId 
		{
			get { return GetColumnValue<string>(Columns.AttendanceRestRankId); }
			set { SetColumnValue(Columns.AttendanceRestRankId, value); }
		}
		  
		[XmlAttribute("LocationPostalCode")]
		[Bindable(true)]
		public string LocationPostalCode 
		{
			get { return GetColumnValue<string>(Columns.LocationPostalCode); }
			set { SetColumnValue(Columns.LocationPostalCode, value); }
		}
		  
		[XmlAttribute("Grade")]
		[Bindable(true)]
		public string Grade 
		{
			get { return GetColumnValue<string>(Columns.Grade); }
			set { SetColumnValue(Columns.Grade, value); }
		}
		  
		[XmlAttribute("Rank")]
		[Bindable(true)]
		public string Rank 
		{
			get { return GetColumnValue<string>(Columns.Rank); }
			set { SetColumnValue(Columns.Rank, value); }
		}
		  
		[XmlAttribute("HoroscopeID")]
		[Bindable(true)]
		public string HoroscopeID 
		{
			get { return GetColumnValue<string>(Columns.HoroscopeID); }
			set { SetColumnValue(Columns.HoroscopeID, value); }
		}
		  
		[XmlAttribute("PositionRankID")]
		[Bindable(true)]
		public Guid? PositionRankID 
		{
			get { return GetColumnValue<Guid?>(Columns.PositionRankID); }
			set { SetColumnValue(Columns.PositionRankID, value); }
		}
		  
		[XmlAttribute("PositionRankName")]
		[Bindable(true)]
		public string PositionRankName 
		{
			get { return GetColumnValue<string>(Columns.PositionRankName); }
			set { SetColumnValue(Columns.PositionRankName, value); }
		}
		  
		[XmlAttribute("NewReportDate")]
		[Bindable(true)]
		public DateTime? NewReportDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.NewReportDate); }
			set { SetColumnValue(Columns.NewReportDate, value); }
		}
		  
		[XmlAttribute("SalaryBankCardId")]
		[Bindable(true)]
		public Guid? SalaryBankCardId 
		{
			get { return GetColumnValue<Guid?>(Columns.SalaryBankCardId); }
			set { SetColumnValue(Columns.SalaryBankCardId, value); }
		}
		  
		[XmlAttribute("StatusCode")]
		[Bindable(true)]
		public bool? StatusCode 
		{
			get { return GetColumnValue<bool?>(Columns.StatusCode); }
			set { SetColumnValue(Columns.StatusCode, value); }
		}
		  
		[XmlAttribute("UniqueID")]
		[Bindable(true)]
		public string UniqueID 
		{
			get { return GetColumnValue<string>(Columns.UniqueID); }
			set { SetColumnValue(Columns.UniqueID, value); }
		}
		  
		[XmlAttribute("OtherCountTypeId")]
		[Bindable(true)]
		public string OtherCountTypeId 
		{
			get { return GetColumnValue<string>(Columns.OtherCountTypeId); }
			set { SetColumnValue(Columns.OtherCountTypeId, value); }
		}
		  
		[XmlAttribute("DecisionLevel")]
		[Bindable(true)]
		public string DecisionLevel 
		{
			get { return GetColumnValue<string>(Columns.DecisionLevel); }
			set { SetColumnValue(Columns.DecisionLevel, value); }
		}
		  
		[XmlAttribute("SalaryOverId")]
		[Bindable(true)]
		public string SalaryOverId 
		{
			get { return GetColumnValue<string>(Columns.SalaryOverId); }
			set { SetColumnValue(Columns.SalaryOverId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varIsTowner,string varSource,DateTime? varFundBeginDate,DateTime? varFundEndDate,string varIsFundId,string varIsAuthorized,string varIsReserveId,string varInnerEmployee,DateTime? varCertificateLimit,string varIsThisYearId,decimal? varBonusRate,string varCCBCode,DateTime? varJobDate,Guid varEmployeeId,int? varNumber,string varCnName,string varEnName,string varIDCardNo,string varIDCardKindId,string varSocialWelfareNo,DateTime? varBirthDate,string varGenderId,DateTime? varDateX,string varEmployeeStateId,string varWorkTypeId,string varMarriageId,string varServiceYears,Guid? varCorporationId,string varTelephone,Guid? varAreaId,string varFactoryId,Guid? varDepartmentId,Guid? varJobId,string varPartTimeJob,Guid? varDirectorId,string varMobilePhone,string varEmail,string varEducationId,string varMajor,string varNativePlace,string varPoliticalIdentityId,string varLocation,string varPostalcode,string varNationId,string varAddress,Guid? varCostCenterId,string varInterest,byte[] varPicture,string varPassword,string varCode,DateTime? varCreateDate,DateTime? varLastModifiedDate,Guid? varCreateBy,Guid? varLastModifiedBy,bool? varFlag,bool? varIsPrincipal,bool? varIsSend,string varITCode,string varAssignReason,string varOwnerId,DateTime? varOtherBirthDate,string varRemark,DateTime? varIntendTransformDate,string varATMealFeeType,DateTime? varLastWorkDate,Guid? varDirectDeptId,string varTimeType,string varHomePhone,DateTime? varTransformDate,DateTime? varWorkBeginDate,string varHealthStatus,string varStature,string varWeight,string varEyeSight,string varBloodType,string varPersonality,string varDiseaseHistory,string varCountryId,string varProvinceId,DateTime? varWorkingAgeBeginDate,string varTWTaxTypeId,decimal? varTWFixedValue,decimal? varTWFixedTaxRate,int? varFamilyNumber,decimal? varAdmitWorkAge,string varCharacterTest,Guid? varDeputy,string varEmployTypeId,Guid? varPositionId,string varPositionGroupId,DateTime? varEnterDate,DateTime? varResidenceExpireDate,int? varAge,int? varWorkingAge,int? varOtherCorpWorkingAge,string varHomeMail,string varHobbies,string varMultiSysCode,string varZhiJian,string varExtraField1,string varExtraField2,string varExtraField3,string varExtraField4,string varExtraField5,string varExtraField6,string varExtraField7,string varExtraField8,string varExtraField9,string varExtraField10,string varExtraField11,string varESSUserAccount,string varDomainServer,string varLevelId,string varSecretId,string varCalendarId,string varAttendanceWorkRankId,string varAttendanceRestRankId,string varLocationPostalCode,string varGrade,string varRank,string varHoroscopeID,Guid? varPositionRankID,string varPositionRankName,DateTime? varNewReportDate,Guid? varSalaryBankCardId,bool? varStatusCode,string varUniqueID,string varOtherCountTypeId,string varDecisionLevel,string varSalaryOverId)
		{
			Employee item = new Employee();
			
			item.IsTowner = varIsTowner;
			
			item.Source = varSource;
			
			item.FundBeginDate = varFundBeginDate;
			
			item.FundEndDate = varFundEndDate;
			
			item.IsFundId = varIsFundId;
			
			item.IsAuthorized = varIsAuthorized;
			
			item.IsReserveId = varIsReserveId;
			
			item.InnerEmployee = varInnerEmployee;
			
			item.CertificateLimit = varCertificateLimit;
			
			item.IsThisYearId = varIsThisYearId;
			
			item.BonusRate = varBonusRate;
			
			item.CCBCode = varCCBCode;
			
			item.JobDate = varJobDate;
			
			item.EmployeeId = varEmployeeId;
			
			item.Number = varNumber;
			
			item.CnName = varCnName;
			
			item.EnName = varEnName;
			
			item.IDCardNo = varIDCardNo;
			
			item.IDCardKindId = varIDCardKindId;
			
			item.SocialWelfareNo = varSocialWelfareNo;
			
			item.BirthDate = varBirthDate;
			
			item.GenderId = varGenderId;
			
			item.DateX = varDateX;
			
			item.EmployeeStateId = varEmployeeStateId;
			
			item.WorkTypeId = varWorkTypeId;
			
			item.MarriageId = varMarriageId;
			
			item.ServiceYears = varServiceYears;
			
			item.CorporationId = varCorporationId;
			
			item.Telephone = varTelephone;
			
			item.AreaId = varAreaId;
			
			item.FactoryId = varFactoryId;
			
			item.DepartmentId = varDepartmentId;
			
			item.JobId = varJobId;
			
			item.PartTimeJob = varPartTimeJob;
			
			item.DirectorId = varDirectorId;
			
			item.MobilePhone = varMobilePhone;
			
			item.Email = varEmail;
			
			item.EducationId = varEducationId;
			
			item.Major = varMajor;
			
			item.NativePlace = varNativePlace;
			
			item.PoliticalIdentityId = varPoliticalIdentityId;
			
			item.Location = varLocation;
			
			item.Postalcode = varPostalcode;
			
			item.NationId = varNationId;
			
			item.Address = varAddress;
			
			item.CostCenterId = varCostCenterId;
			
			item.Interest = varInterest;
			
			item.Picture = varPicture;
			
			item.Password = varPassword;
			
			item.Code = varCode;
			
			item.CreateDate = varCreateDate;
			
			item.LastModifiedDate = varLastModifiedDate;
			
			item.CreateBy = varCreateBy;
			
			item.LastModifiedBy = varLastModifiedBy;
			
			item.Flag = varFlag;
			
			item.IsPrincipal = varIsPrincipal;
			
			item.IsSend = varIsSend;
			
			item.ITCode = varITCode;
			
			item.AssignReason = varAssignReason;
			
			item.OwnerId = varOwnerId;
			
			item.OtherBirthDate = varOtherBirthDate;
			
			item.Remark = varRemark;
			
			item.IntendTransformDate = varIntendTransformDate;
			
			item.ATMealFeeType = varATMealFeeType;
			
			item.LastWorkDate = varLastWorkDate;
			
			item.DirectDeptId = varDirectDeptId;
			
			item.TimeType = varTimeType;
			
			item.HomePhone = varHomePhone;
			
			item.TransformDate = varTransformDate;
			
			item.WorkBeginDate = varWorkBeginDate;
			
			item.HealthStatus = varHealthStatus;
			
			item.Stature = varStature;
			
			item.Weight = varWeight;
			
			item.EyeSight = varEyeSight;
			
			item.BloodType = varBloodType;
			
			item.Personality = varPersonality;
			
			item.DiseaseHistory = varDiseaseHistory;
			
			item.CountryId = varCountryId;
			
			item.ProvinceId = varProvinceId;
			
			item.WorkingAgeBeginDate = varWorkingAgeBeginDate;
			
			item.TWTaxTypeId = varTWTaxTypeId;
			
			item.TWFixedValue = varTWFixedValue;
			
			item.TWFixedTaxRate = varTWFixedTaxRate;
			
			item.FamilyNumber = varFamilyNumber;
			
			item.AdmitWorkAge = varAdmitWorkAge;
			
			item.CharacterTest = varCharacterTest;
			
			item.Deputy = varDeputy;
			
			item.EmployTypeId = varEmployTypeId;
			
			item.PositionId = varPositionId;
			
			item.PositionGroupId = varPositionGroupId;
			
			item.EnterDate = varEnterDate;
			
			item.ResidenceExpireDate = varResidenceExpireDate;
			
			item.Age = varAge;
			
			item.WorkingAge = varWorkingAge;
			
			item.OtherCorpWorkingAge = varOtherCorpWorkingAge;
			
			item.HomeMail = varHomeMail;
			
			item.Hobbies = varHobbies;
			
			item.MultiSysCode = varMultiSysCode;
			
			item.ZhiJian = varZhiJian;
			
			item.ExtraField1 = varExtraField1;
			
			item.ExtraField2 = varExtraField2;
			
			item.ExtraField3 = varExtraField3;
			
			item.ExtraField4 = varExtraField4;
			
			item.ExtraField5 = varExtraField5;
			
			item.ExtraField6 = varExtraField6;
			
			item.ExtraField7 = varExtraField7;
			
			item.ExtraField8 = varExtraField8;
			
			item.ExtraField9 = varExtraField9;
			
			item.ExtraField10 = varExtraField10;
			
			item.ExtraField11 = varExtraField11;
			
			item.ESSUserAccount = varESSUserAccount;
			
			item.DomainServer = varDomainServer;
			
			item.LevelId = varLevelId;
			
			item.SecretId = varSecretId;
			
			item.CalendarId = varCalendarId;
			
			item.AttendanceWorkRankId = varAttendanceWorkRankId;
			
			item.AttendanceRestRankId = varAttendanceRestRankId;
			
			item.LocationPostalCode = varLocationPostalCode;
			
			item.Grade = varGrade;
			
			item.Rank = varRank;
			
			item.HoroscopeID = varHoroscopeID;
			
			item.PositionRankID = varPositionRankID;
			
			item.PositionRankName = varPositionRankName;
			
			item.NewReportDate = varNewReportDate;
			
			item.SalaryBankCardId = varSalaryBankCardId;
			
			item.StatusCode = varStatusCode;
			
			item.UniqueID = varUniqueID;
			
			item.OtherCountTypeId = varOtherCountTypeId;
			
			item.DecisionLevel = varDecisionLevel;
			
			item.SalaryOverId = varSalaryOverId;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varIsTowner,string varSource,DateTime? varFundBeginDate,DateTime? varFundEndDate,string varIsFundId,string varIsAuthorized,string varIsReserveId,string varInnerEmployee,DateTime? varCertificateLimit,string varIsThisYearId,decimal? varBonusRate,string varCCBCode,DateTime? varJobDate,Guid varEmployeeId,int? varNumber,string varCnName,string varEnName,string varIDCardNo,string varIDCardKindId,string varSocialWelfareNo,DateTime? varBirthDate,string varGenderId,DateTime? varDateX,string varEmployeeStateId,string varWorkTypeId,string varMarriageId,string varServiceYears,Guid? varCorporationId,string varTelephone,Guid? varAreaId,string varFactoryId,Guid? varDepartmentId,Guid? varJobId,string varPartTimeJob,Guid? varDirectorId,string varMobilePhone,string varEmail,string varEducationId,string varMajor,string varNativePlace,string varPoliticalIdentityId,string varLocation,string varPostalcode,string varNationId,string varAddress,Guid? varCostCenterId,string varInterest,byte[] varPicture,string varPassword,string varCode,DateTime? varCreateDate,DateTime? varLastModifiedDate,Guid? varCreateBy,Guid? varLastModifiedBy,bool? varFlag,bool? varIsPrincipal,bool? varIsSend,string varITCode,string varAssignReason,string varOwnerId,DateTime? varOtherBirthDate,string varRemark,DateTime? varIntendTransformDate,string varATMealFeeType,DateTime? varLastWorkDate,Guid? varDirectDeptId,string varTimeType,string varHomePhone,DateTime? varTransformDate,DateTime? varWorkBeginDate,string varHealthStatus,string varStature,string varWeight,string varEyeSight,string varBloodType,string varPersonality,string varDiseaseHistory,string varCountryId,string varProvinceId,DateTime? varWorkingAgeBeginDate,string varTWTaxTypeId,decimal? varTWFixedValue,decimal? varTWFixedTaxRate,int? varFamilyNumber,decimal? varAdmitWorkAge,string varCharacterTest,Guid? varDeputy,string varEmployTypeId,Guid? varPositionId,string varPositionGroupId,DateTime? varEnterDate,DateTime? varResidenceExpireDate,int? varAge,int? varWorkingAge,int? varOtherCorpWorkingAge,string varHomeMail,string varHobbies,string varMultiSysCode,string varZhiJian,string varExtraField1,string varExtraField2,string varExtraField3,string varExtraField4,string varExtraField5,string varExtraField6,string varExtraField7,string varExtraField8,string varExtraField9,string varExtraField10,string varExtraField11,string varESSUserAccount,string varDomainServer,string varLevelId,string varSecretId,string varCalendarId,string varAttendanceWorkRankId,string varAttendanceRestRankId,string varLocationPostalCode,string varGrade,string varRank,string varHoroscopeID,Guid? varPositionRankID,string varPositionRankName,DateTime? varNewReportDate,Guid? varSalaryBankCardId,bool? varStatusCode,string varUniqueID,string varOtherCountTypeId,string varDecisionLevel,string varSalaryOverId)
		{
			Employee item = new Employee();
			
				item.IsTowner = varIsTowner;
			
				item.Source = varSource;
			
				item.FundBeginDate = varFundBeginDate;
			
				item.FundEndDate = varFundEndDate;
			
				item.IsFundId = varIsFundId;
			
				item.IsAuthorized = varIsAuthorized;
			
				item.IsReserveId = varIsReserveId;
			
				item.InnerEmployee = varInnerEmployee;
			
				item.CertificateLimit = varCertificateLimit;
			
				item.IsThisYearId = varIsThisYearId;
			
				item.BonusRate = varBonusRate;
			
				item.CCBCode = varCCBCode;
			
				item.JobDate = varJobDate;
			
				item.EmployeeId = varEmployeeId;
			
				item.Number = varNumber;
			
				item.CnName = varCnName;
			
				item.EnName = varEnName;
			
				item.IDCardNo = varIDCardNo;
			
				item.IDCardKindId = varIDCardKindId;
			
				item.SocialWelfareNo = varSocialWelfareNo;
			
				item.BirthDate = varBirthDate;
			
				item.GenderId = varGenderId;
			
				item.DateX = varDateX;
			
				item.EmployeeStateId = varEmployeeStateId;
			
				item.WorkTypeId = varWorkTypeId;
			
				item.MarriageId = varMarriageId;
			
				item.ServiceYears = varServiceYears;
			
				item.CorporationId = varCorporationId;
			
				item.Telephone = varTelephone;
			
				item.AreaId = varAreaId;
			
				item.FactoryId = varFactoryId;
			
				item.DepartmentId = varDepartmentId;
			
				item.JobId = varJobId;
			
				item.PartTimeJob = varPartTimeJob;
			
				item.DirectorId = varDirectorId;
			
				item.MobilePhone = varMobilePhone;
			
				item.Email = varEmail;
			
				item.EducationId = varEducationId;
			
				item.Major = varMajor;
			
				item.NativePlace = varNativePlace;
			
				item.PoliticalIdentityId = varPoliticalIdentityId;
			
				item.Location = varLocation;
			
				item.Postalcode = varPostalcode;
			
				item.NationId = varNationId;
			
				item.Address = varAddress;
			
				item.CostCenterId = varCostCenterId;
			
				item.Interest = varInterest;
			
				item.Picture = varPicture;
			
				item.Password = varPassword;
			
				item.Code = varCode;
			
				item.CreateDate = varCreateDate;
			
				item.LastModifiedDate = varLastModifiedDate;
			
				item.CreateBy = varCreateBy;
			
				item.LastModifiedBy = varLastModifiedBy;
			
				item.Flag = varFlag;
			
				item.IsPrincipal = varIsPrincipal;
			
				item.IsSend = varIsSend;
			
				item.ITCode = varITCode;
			
				item.AssignReason = varAssignReason;
			
				item.OwnerId = varOwnerId;
			
				item.OtherBirthDate = varOtherBirthDate;
			
				item.Remark = varRemark;
			
				item.IntendTransformDate = varIntendTransformDate;
			
				item.ATMealFeeType = varATMealFeeType;
			
				item.LastWorkDate = varLastWorkDate;
			
				item.DirectDeptId = varDirectDeptId;
			
				item.TimeType = varTimeType;
			
				item.HomePhone = varHomePhone;
			
				item.TransformDate = varTransformDate;
			
				item.WorkBeginDate = varWorkBeginDate;
			
				item.HealthStatus = varHealthStatus;
			
				item.Stature = varStature;
			
				item.Weight = varWeight;
			
				item.EyeSight = varEyeSight;
			
				item.BloodType = varBloodType;
			
				item.Personality = varPersonality;
			
				item.DiseaseHistory = varDiseaseHistory;
			
				item.CountryId = varCountryId;
			
				item.ProvinceId = varProvinceId;
			
				item.WorkingAgeBeginDate = varWorkingAgeBeginDate;
			
				item.TWTaxTypeId = varTWTaxTypeId;
			
				item.TWFixedValue = varTWFixedValue;
			
				item.TWFixedTaxRate = varTWFixedTaxRate;
			
				item.FamilyNumber = varFamilyNumber;
			
				item.AdmitWorkAge = varAdmitWorkAge;
			
				item.CharacterTest = varCharacterTest;
			
				item.Deputy = varDeputy;
			
				item.EmployTypeId = varEmployTypeId;
			
				item.PositionId = varPositionId;
			
				item.PositionGroupId = varPositionGroupId;
			
				item.EnterDate = varEnterDate;
			
				item.ResidenceExpireDate = varResidenceExpireDate;
			
				item.Age = varAge;
			
				item.WorkingAge = varWorkingAge;
			
				item.OtherCorpWorkingAge = varOtherCorpWorkingAge;
			
				item.HomeMail = varHomeMail;
			
				item.Hobbies = varHobbies;
			
				item.MultiSysCode = varMultiSysCode;
			
				item.ZhiJian = varZhiJian;
			
				item.ExtraField1 = varExtraField1;
			
				item.ExtraField2 = varExtraField2;
			
				item.ExtraField3 = varExtraField3;
			
				item.ExtraField4 = varExtraField4;
			
				item.ExtraField5 = varExtraField5;
			
				item.ExtraField6 = varExtraField6;
			
				item.ExtraField7 = varExtraField7;
			
				item.ExtraField8 = varExtraField8;
			
				item.ExtraField9 = varExtraField9;
			
				item.ExtraField10 = varExtraField10;
			
				item.ExtraField11 = varExtraField11;
			
				item.ESSUserAccount = varESSUserAccount;
			
				item.DomainServer = varDomainServer;
			
				item.LevelId = varLevelId;
			
				item.SecretId = varSecretId;
			
				item.CalendarId = varCalendarId;
			
				item.AttendanceWorkRankId = varAttendanceWorkRankId;
			
				item.AttendanceRestRankId = varAttendanceRestRankId;
			
				item.LocationPostalCode = varLocationPostalCode;
			
				item.Grade = varGrade;
			
				item.Rank = varRank;
			
				item.HoroscopeID = varHoroscopeID;
			
				item.PositionRankID = varPositionRankID;
			
				item.PositionRankName = varPositionRankName;
			
				item.NewReportDate = varNewReportDate;
			
				item.SalaryBankCardId = varSalaryBankCardId;
			
				item.StatusCode = varStatusCode;
			
				item.UniqueID = varUniqueID;
			
				item.OtherCountTypeId = varOtherCountTypeId;
			
				item.DecisionLevel = varDecisionLevel;
			
				item.SalaryOverId = varSalaryOverId;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IsTownerColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SourceColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FundBeginDateColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn FundEndDateColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IsFundIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IsAuthorizedColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReserveIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn InnerEmployeeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CertificateLimitColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn IsThisYearIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn BonusRateColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CCBCodeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn JobDateColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn EmployeeIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn NumberColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CnNameColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn EnNameColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn IDCardNoColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn IDCardKindIdColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn SocialWelfareNoColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn BirthDateColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn GenderIdColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn DateXColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn EmployeeStateIdColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn WorkTypeIdColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn MarriageIdColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn ServiceYearsColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn CorporationIdColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn TelephoneColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn AreaIdColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn FactoryIdColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn DepartmentIdColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn JobIdColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn PartTimeJobColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn DirectorIdColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn MobilePhoneColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn EmailColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn EducationIdColumn
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn MajorColumn
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn NativePlaceColumn
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        public static TableSchema.TableColumn PoliticalIdentityIdColumn
        {
            get { return Schema.Columns[40]; }
        }
        
        
        
        public static TableSchema.TableColumn LocationColumn
        {
            get { return Schema.Columns[41]; }
        }
        
        
        
        public static TableSchema.TableColumn PostalcodeColumn
        {
            get { return Schema.Columns[42]; }
        }
        
        
        
        public static TableSchema.TableColumn NationIdColumn
        {
            get { return Schema.Columns[43]; }
        }
        
        
        
        public static TableSchema.TableColumn AddressColumn
        {
            get { return Schema.Columns[44]; }
        }
        
        
        
        public static TableSchema.TableColumn CostCenterIdColumn
        {
            get { return Schema.Columns[45]; }
        }
        
        
        
        public static TableSchema.TableColumn InterestColumn
        {
            get { return Schema.Columns[46]; }
        }
        
        
        
        public static TableSchema.TableColumn PictureColumn
        {
            get { return Schema.Columns[47]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordColumn
        {
            get { return Schema.Columns[48]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[49]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateDateColumn
        {
            get { return Schema.Columns[50]; }
        }
        
        
        
        public static TableSchema.TableColumn LastModifiedDateColumn
        {
            get { return Schema.Columns[51]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateByColumn
        {
            get { return Schema.Columns[52]; }
        }
        
        
        
        public static TableSchema.TableColumn LastModifiedByColumn
        {
            get { return Schema.Columns[53]; }
        }
        
        
        
        public static TableSchema.TableColumn FlagColumn
        {
            get { return Schema.Columns[54]; }
        }
        
        
        
        public static TableSchema.TableColumn IsPrincipalColumn
        {
            get { return Schema.Columns[55]; }
        }
        
        
        
        public static TableSchema.TableColumn IsSendColumn
        {
            get { return Schema.Columns[56]; }
        }
        
        
        
        public static TableSchema.TableColumn ITCodeColumn
        {
            get { return Schema.Columns[57]; }
        }
        
        
        
        public static TableSchema.TableColumn AssignReasonColumn
        {
            get { return Schema.Columns[58]; }
        }
        
        
        
        public static TableSchema.TableColumn OwnerIdColumn
        {
            get { return Schema.Columns[59]; }
        }
        
        
        
        public static TableSchema.TableColumn OtherBirthDateColumn
        {
            get { return Schema.Columns[60]; }
        }
        
        
        
        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[61]; }
        }
        
        
        
        public static TableSchema.TableColumn IntendTransformDateColumn
        {
            get { return Schema.Columns[62]; }
        }
        
        
        
        public static TableSchema.TableColumn ATMealFeeTypeColumn
        {
            get { return Schema.Columns[63]; }
        }
        
        
        
        public static TableSchema.TableColumn LastWorkDateColumn
        {
            get { return Schema.Columns[64]; }
        }
        
        
        
        public static TableSchema.TableColumn DirectDeptIdColumn
        {
            get { return Schema.Columns[65]; }
        }
        
        
        
        public static TableSchema.TableColumn TimeTypeColumn
        {
            get { return Schema.Columns[66]; }
        }
        
        
        
        public static TableSchema.TableColumn HomePhoneColumn
        {
            get { return Schema.Columns[67]; }
        }
        
        
        
        public static TableSchema.TableColumn TransformDateColumn
        {
            get { return Schema.Columns[68]; }
        }
        
        
        
        public static TableSchema.TableColumn WorkBeginDateColumn
        {
            get { return Schema.Columns[69]; }
        }
        
        
        
        public static TableSchema.TableColumn HealthStatusColumn
        {
            get { return Schema.Columns[70]; }
        }
        
        
        
        public static TableSchema.TableColumn StatureColumn
        {
            get { return Schema.Columns[71]; }
        }
        
        
        
        public static TableSchema.TableColumn WeightColumn
        {
            get { return Schema.Columns[72]; }
        }
        
        
        
        public static TableSchema.TableColumn EyeSightColumn
        {
            get { return Schema.Columns[73]; }
        }
        
        
        
        public static TableSchema.TableColumn BloodTypeColumn
        {
            get { return Schema.Columns[74]; }
        }
        
        
        
        public static TableSchema.TableColumn PersonalityColumn
        {
            get { return Schema.Columns[75]; }
        }
        
        
        
        public static TableSchema.TableColumn DiseaseHistoryColumn
        {
            get { return Schema.Columns[76]; }
        }
        
        
        
        public static TableSchema.TableColumn CountryIdColumn
        {
            get { return Schema.Columns[77]; }
        }
        
        
        
        public static TableSchema.TableColumn ProvinceIdColumn
        {
            get { return Schema.Columns[78]; }
        }
        
        
        
        public static TableSchema.TableColumn WorkingAgeBeginDateColumn
        {
            get { return Schema.Columns[79]; }
        }
        
        
        
        public static TableSchema.TableColumn TWTaxTypeIdColumn
        {
            get { return Schema.Columns[80]; }
        }
        
        
        
        public static TableSchema.TableColumn TWFixedValueColumn
        {
            get { return Schema.Columns[81]; }
        }
        
        
        
        public static TableSchema.TableColumn TWFixedTaxRateColumn
        {
            get { return Schema.Columns[82]; }
        }
        
        
        
        public static TableSchema.TableColumn FamilyNumberColumn
        {
            get { return Schema.Columns[83]; }
        }
        
        
        
        public static TableSchema.TableColumn AdmitWorkAgeColumn
        {
            get { return Schema.Columns[84]; }
        }
        
        
        
        public static TableSchema.TableColumn CharacterTestColumn
        {
            get { return Schema.Columns[85]; }
        }
        
        
        
        public static TableSchema.TableColumn DeputyColumn
        {
            get { return Schema.Columns[86]; }
        }
        
        
        
        public static TableSchema.TableColumn EmployTypeIdColumn
        {
            get { return Schema.Columns[87]; }
        }
        
        
        
        public static TableSchema.TableColumn PositionIdColumn
        {
            get { return Schema.Columns[88]; }
        }
        
        
        
        public static TableSchema.TableColumn PositionGroupIdColumn
        {
            get { return Schema.Columns[89]; }
        }
        
        
        
        public static TableSchema.TableColumn EnterDateColumn
        {
            get { return Schema.Columns[90]; }
        }
        
        
        
        public static TableSchema.TableColumn ResidenceExpireDateColumn
        {
            get { return Schema.Columns[91]; }
        }
        
        
        
        public static TableSchema.TableColumn AgeColumn
        {
            get { return Schema.Columns[92]; }
        }
        
        
        
        public static TableSchema.TableColumn WorkingAgeColumn
        {
            get { return Schema.Columns[93]; }
        }
        
        
        
        public static TableSchema.TableColumn OtherCorpWorkingAgeColumn
        {
            get { return Schema.Columns[94]; }
        }
        
        
        
        public static TableSchema.TableColumn HomeMailColumn
        {
            get { return Schema.Columns[95]; }
        }
        
        
        
        public static TableSchema.TableColumn HobbiesColumn
        {
            get { return Schema.Columns[96]; }
        }
        
        
        
        public static TableSchema.TableColumn MultiSysCodeColumn
        {
            get { return Schema.Columns[97]; }
        }
        
        
        
        public static TableSchema.TableColumn ZhiJianColumn
        {
            get { return Schema.Columns[98]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraField1Column
        {
            get { return Schema.Columns[99]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraField2Column
        {
            get { return Schema.Columns[100]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraField3Column
        {
            get { return Schema.Columns[101]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraField4Column
        {
            get { return Schema.Columns[102]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraField5Column
        {
            get { return Schema.Columns[103]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraField6Column
        {
            get { return Schema.Columns[104]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraField7Column
        {
            get { return Schema.Columns[105]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraField8Column
        {
            get { return Schema.Columns[106]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraField9Column
        {
            get { return Schema.Columns[107]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraField10Column
        {
            get { return Schema.Columns[108]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraField11Column
        {
            get { return Schema.Columns[109]; }
        }
        
        
        
        public static TableSchema.TableColumn ESSUserAccountColumn
        {
            get { return Schema.Columns[110]; }
        }
        
        
        
        public static TableSchema.TableColumn DomainServerColumn
        {
            get { return Schema.Columns[111]; }
        }
        
        
        
        public static TableSchema.TableColumn LevelIdColumn
        {
            get { return Schema.Columns[112]; }
        }
        
        
        
        public static TableSchema.TableColumn SecretIdColumn
        {
            get { return Schema.Columns[113]; }
        }
        
        
        
        public static TableSchema.TableColumn CalendarIdColumn
        {
            get { return Schema.Columns[114]; }
        }
        
        
        
        public static TableSchema.TableColumn AttendanceWorkRankIdColumn
        {
            get { return Schema.Columns[115]; }
        }
        
        
        
        public static TableSchema.TableColumn AttendanceRestRankIdColumn
        {
            get { return Schema.Columns[116]; }
        }
        
        
        
        public static TableSchema.TableColumn LocationPostalCodeColumn
        {
            get { return Schema.Columns[117]; }
        }
        
        
        
        public static TableSchema.TableColumn GradeColumn
        {
            get { return Schema.Columns[118]; }
        }
        
        
        
        public static TableSchema.TableColumn RankColumn
        {
            get { return Schema.Columns[119]; }
        }
        
        
        
        public static TableSchema.TableColumn HoroscopeIDColumn
        {
            get { return Schema.Columns[120]; }
        }
        
        
        
        public static TableSchema.TableColumn PositionRankIDColumn
        {
            get { return Schema.Columns[121]; }
        }
        
        
        
        public static TableSchema.TableColumn PositionRankNameColumn
        {
            get { return Schema.Columns[122]; }
        }
        
        
        
        public static TableSchema.TableColumn NewReportDateColumn
        {
            get { return Schema.Columns[123]; }
        }
        
        
        
        public static TableSchema.TableColumn SalaryBankCardIdColumn
        {
            get { return Schema.Columns[124]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusCodeColumn
        {
            get { return Schema.Columns[125]; }
        }
        
        
        
        public static TableSchema.TableColumn UniqueIDColumn
        {
            get { return Schema.Columns[126]; }
        }
        
        
        
        public static TableSchema.TableColumn OtherCountTypeIdColumn
        {
            get { return Schema.Columns[127]; }
        }
        
        
        
        public static TableSchema.TableColumn DecisionLevelColumn
        {
            get { return Schema.Columns[128]; }
        }
        
        
        
        public static TableSchema.TableColumn SalaryOverIdColumn
        {
            get { return Schema.Columns[129]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IsTowner = @"IsTowner";
			 public static string Source = @"Source";
			 public static string FundBeginDate = @"FundBeginDate";
			 public static string FundEndDate = @"FundEndDate";
			 public static string IsFundId = @"IsFundId";
			 public static string IsAuthorized = @"IsAuthorized";
			 public static string IsReserveId = @"IsReserveId";
			 public static string InnerEmployee = @"InnerEmployee";
			 public static string CertificateLimit = @"CertificateLimit";
			 public static string IsThisYearId = @"IsThisYearId";
			 public static string BonusRate = @"BonusRate";
			 public static string CCBCode = @"CCBCode";
			 public static string JobDate = @"JobDate";
			 public static string EmployeeId = @"EmployeeId";
			 public static string Number = @"Number";
			 public static string CnName = @"CnName";
			 public static string EnName = @"EnName";
			 public static string IDCardNo = @"IDCardNo";
			 public static string IDCardKindId = @"IDCardKindId";
			 public static string SocialWelfareNo = @"SocialWelfareNo";
			 public static string BirthDate = @"BirthDate";
			 public static string GenderId = @"GenderId";
			 public static string DateX = @"Date";
			 public static string EmployeeStateId = @"EmployeeStateId";
			 public static string WorkTypeId = @"WorkTypeId";
			 public static string MarriageId = @"MarriageId";
			 public static string ServiceYears = @"ServiceYears";
			 public static string CorporationId = @"CorporationId";
			 public static string Telephone = @"Telephone";
			 public static string AreaId = @"AreaId";
			 public static string FactoryId = @"FactoryId";
			 public static string DepartmentId = @"DepartmentId";
			 public static string JobId = @"JobId";
			 public static string PartTimeJob = @"PartTimeJob";
			 public static string DirectorId = @"DirectorId";
			 public static string MobilePhone = @"MobilePhone";
			 public static string Email = @"Email";
			 public static string EducationId = @"EducationId";
			 public static string Major = @"Major";
			 public static string NativePlace = @"NativePlace";
			 public static string PoliticalIdentityId = @"PoliticalIdentityId";
			 public static string Location = @"Location";
			 public static string Postalcode = @"Postalcode";
			 public static string NationId = @"NationId";
			 public static string Address = @"Address";
			 public static string CostCenterId = @"CostCenterId";
			 public static string Interest = @"Interest";
			 public static string Picture = @"Picture";
			 public static string Password = @"Password";
			 public static string Code = @"Code";
			 public static string CreateDate = @"CreateDate";
			 public static string LastModifiedDate = @"LastModifiedDate";
			 public static string CreateBy = @"CreateBy";
			 public static string LastModifiedBy = @"LastModifiedBy";
			 public static string Flag = @"Flag";
			 public static string IsPrincipal = @"IsPrincipal";
			 public static string IsSend = @"IsSend";
			 public static string ITCode = @"ITCode";
			 public static string AssignReason = @"AssignReason";
			 public static string OwnerId = @"OwnerId";
			 public static string OtherBirthDate = @"OtherBirthDate";
			 public static string Remark = @"Remark";
			 public static string IntendTransformDate = @"IntendTransformDate";
			 public static string ATMealFeeType = @"ATMealFeeType";
			 public static string LastWorkDate = @"LastWorkDate";
			 public static string DirectDeptId = @"DirectDeptId";
			 public static string TimeType = @"TimeType";
			 public static string HomePhone = @"HomePhone";
			 public static string TransformDate = @"TransformDate";
			 public static string WorkBeginDate = @"WorkBeginDate";
			 public static string HealthStatus = @"HealthStatus";
			 public static string Stature = @"Stature";
			 public static string Weight = @"Weight";
			 public static string EyeSight = @"EyeSight";
			 public static string BloodType = @"BloodType";
			 public static string Personality = @"Personality";
			 public static string DiseaseHistory = @"DiseaseHistory";
			 public static string CountryId = @"CountryId";
			 public static string ProvinceId = @"ProvinceId";
			 public static string WorkingAgeBeginDate = @"WorkingAgeBeginDate";
			 public static string TWTaxTypeId = @"TWTaxTypeId";
			 public static string TWFixedValue = @"TWFixedValue";
			 public static string TWFixedTaxRate = @"TWFixedTaxRate";
			 public static string FamilyNumber = @"FamilyNumber";
			 public static string AdmitWorkAge = @"AdmitWorkAge";
			 public static string CharacterTest = @"CharacterTest";
			 public static string Deputy = @"Deputy";
			 public static string EmployTypeId = @"EmployTypeId";
			 public static string PositionId = @"PositionId";
			 public static string PositionGroupId = @"PositionGroupId";
			 public static string EnterDate = @"EnterDate";
			 public static string ResidenceExpireDate = @"ResidenceExpireDate";
			 public static string Age = @"Age";
			 public static string WorkingAge = @"WorkingAge";
			 public static string OtherCorpWorkingAge = @"OtherCorpWorkingAge";
			 public static string HomeMail = @"HomeMail";
			 public static string Hobbies = @"Hobbies";
			 public static string MultiSysCode = @"MultiSysCode";
			 public static string ZhiJian = @"ZhiJian";
			 public static string ExtraField1 = @"ExtraField1";
			 public static string ExtraField2 = @"ExtraField2";
			 public static string ExtraField3 = @"ExtraField3";
			 public static string ExtraField4 = @"ExtraField4";
			 public static string ExtraField5 = @"ExtraField5";
			 public static string ExtraField6 = @"ExtraField6";
			 public static string ExtraField7 = @"ExtraField7";
			 public static string ExtraField8 = @"ExtraField8";
			 public static string ExtraField9 = @"ExtraField9";
			 public static string ExtraField10 = @"ExtraField10";
			 public static string ExtraField11 = @"ExtraField11";
			 public static string ESSUserAccount = @"ESSUserAccount";
			 public static string DomainServer = @"DomainServer";
			 public static string LevelId = @"LevelId";
			 public static string SecretId = @"SecretId";
			 public static string CalendarId = @"CalendarId";
			 public static string AttendanceWorkRankId = @"AttendanceWorkRankId";
			 public static string AttendanceRestRankId = @"AttendanceRestRankId";
			 public static string LocationPostalCode = @"LocationPostalCode";
			 public static string Grade = @"Grade";
			 public static string Rank = @"Rank";
			 public static string HoroscopeID = @"HoroscopeID";
			 public static string PositionRankID = @"PositionRankID";
			 public static string PositionRankName = @"PositionRankName";
			 public static string NewReportDate = @"NewReportDate";
			 public static string SalaryBankCardId = @"SalaryBankCardId";
			 public static string StatusCode = @"StatusCode";
			 public static string UniqueID = @"UniqueID";
			 public static string OtherCountTypeId = @"OtherCountTypeId";
			 public static string DecisionLevel = @"DecisionLevel";
			 public static string SalaryOverId = @"SalaryOverId";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

﻿using System;
using LunchKingSite.Core.Component;
using System.Net;
using System.Net.Mail;
using System.Transactions;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Configuration
{
    public class JobLoadBalance : IJob
    {
        IJob job;
        ISystemProvider sp;
        ISysConfProvider config;
        public JobLoadBalance(IJob originalJob, JobSetting js)
        {
            this.job = originalJob;
            config = ProviderFactory.Instance().GetConfig();
            sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            //判斷是否有設定單一執行或多台server執行
            string singleJobValue = js.Parameters[SingleOperator.SingleJob.ToString()];
            if (String.IsNullOrEmpty(singleJobValue) || singleJobValue == JobLoadBalanceMode.Single.ToString("D"))
            {
                string hostName = Dns.GetHostName();
                string defaultHostName = sp.SystemCodeGetName(SingleOperator.SingleJob.ToString(), 1);
                //判斷哪台執行
                if (!hostName.Equals(defaultHostName, StringComparison.CurrentCultureIgnoreCase))
                {
                    this.job = null;
                }
            }
        }

        public void Execute(JobSetting js)
        {
            if (this.job != null)
            {
                //job會在執行完畢後oncomplete時更新上次執行時間
                //避免job執行時間較長，造成判斷上次執行時間誤差
                //在執行前先更新執行時間，完成後再更新一次
                JobContext jc = Jobs.Instance.FindJob(js.Name);

                var jobClassName = string.Format("{0}.{1}", Dns.GetHostName(), js.Name);

                using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                {
                    DateTime repeatTimeoutPoint = DateTime.Now.AddSeconds(-config.JobRepeatTimeoutSec);
                    JobsStatus jobstatus = sp.JobsStatusGet(jobClassName);
                    if (jobstatus.IsLoaded)
                    {
                        if (config.JobRepeatCheckEnabled && jobstatus.Processing && jobstatus.ModifyTime > repeatTimeoutPoint)
                        {
                            var msg = new MailMessage();
                            msg.From = new MailAddress(config.AdminEmail);
                            msg.To.Add(new MailAddress(config.AdminEmail));
                            msg.Subject = "Job重複執行";
                            msg.Body = string.Format("Job{0}預定在{1}{2}執行，上次DB紀錄執行時間為{3}，因目前有相同JOB正在執行，已取消本次執行避免重複，請檢查其他Job是否發生相同狀況",
                                js.Name, Dns.GetHostName(), DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), jobstatus.LastTime.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                            msg.IsBodyHtml = false;
                            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                            return;
                        }
                    }

                    jobstatus.ClassName = jobClassName;
                    jobstatus.Processing = true;
                    sp.JobsStatusSet(jobstatus);
                    ts.Complete();
                }

                DateTime lastExecutionRunTime = DateTime.Now;
                if (jc != null)
                {
                    jc.LastExecution.RunTime = lastExecutionRunTime;
                }

                DateTime now = DateTime.Now;
                this.job.Execute(js);

                using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                {
                    JobsStatus jobstatus = sp.JobsStatusGet(jobClassName);
                    jobstatus.ExecutionTime = (DateTime.Now - now).TotalSeconds;
                    jobstatus.LastTime = lastExecutionRunTime;
                    jobstatus.Processing = false;
                    sp.JobsStatusSet(jobstatus);
                    ts.Complete();
                }
            }
        }
    }
}

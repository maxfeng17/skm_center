using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;

namespace LunchKingSite.Core.Configuration
{
    public sealed class JobSettingConfiguration : ConfigurationElement
    {
        #region private members
        private readonly ConfigurationProperty _name;
        private readonly ConfigurationProperty _type;
        private readonly ConfigurationProperty _desc;
        private readonly ConfigurationProperty _enabled;
        private readonly ConfigurationProperty _seconds;
        private readonly ConfigurationProperty _starttime;
        private readonly ConfigurationProperty _endtime;
        private readonly ConfigurationProperty _advsched;
        private readonly ConfigurationPropertyCollection _properties;
        private NameValueCollection _otherParameters;
        #endregion

        #region .ctor
        public JobSettingConfiguration()
        {
            _name = new ConfigurationProperty(Constant.ConfigProperty.NAME, typeof(string), null, ConfigurationPropertyOptions.IsKey);
            _type = new ConfigurationProperty(Constant.ConfigProperty.TYPE, typeof(string), null, ConfigurationPropertyOptions.IsRequired);
            _desc = new ConfigurationProperty(Constant.ConfigProperty.DESCRIPTION, typeof(string), null, ConfigurationPropertyOptions.None);
            _enabled = new ConfigurationProperty(Constant.ConfigProperty.ENABLED, typeof(bool), true, ConfigurationPropertyOptions.None);
            _seconds = new ConfigurationProperty(Constant.ConfigProperty.SECONDS, typeof(int), 900, ConfigurationPropertyOptions.None);
            _starttime = new ConfigurationProperty(Constant.ConfigProperty.START_TIME, typeof(string), null, ConfigurationPropertyOptions.None);
            _endtime = new ConfigurationProperty(Constant.ConfigProperty.END_TIME, typeof(string), null, ConfigurationPropertyOptions.None);
            _advsched = new ConfigurationProperty(Constant.ConfigProperty.ADV_SCHEDULE, typeof(string), null, ConfigurationPropertyOptions.None);

            _properties = new ConfigurationPropertyCollection();
            _properties.Add(_name);
            _properties.Add(_type);
            _properties.Add(_desc);
            _properties.Add(_enabled);
            _properties.Add(_seconds);
            _properties.Add(_starttime);
            _properties.Add(_endtime);
            _properties.Add(_advsched);
        }

        public JobSettingConfiguration(string name, string type)
            : this()
        {
            Name = name;
            Type = type;
        }
        #endregion

        #region properties
        [ConfigurationProperty(Constant.ConfigProperty.NAME)]
        public string Name
        {
            get { return (string)base[_name]; }
            set { base[_name] = value; }
        }

        [ConfigurationProperty(Constant.ConfigProperty.TYPE)]
        public string Type
        {
            get { return (string)base[_type]; }
            set { base[_type] = value; }
        }

        [ConfigurationProperty(Constant.ConfigProperty.DESCRIPTION)]
        public string Description
        {
            get { return (string)base[_desc]; }
            set { base[_desc] = value; }
        }

        [ConfigurationProperty(Constant.ConfigProperty.ENABLED)]
        public bool Enabled
        {
            get { return (bool)base[_enabled]; }
            set { base[_enabled] = value; }
        }

        [ConfigurationProperty(Constant.ConfigProperty.SECONDS)]
        public int Seconds
        {
            get { return (int)base[_seconds]; }
            set { base[_seconds] = value; }
        }

        [ConfigurationProperty(Constant.ConfigProperty.START_TIME)]
        public DateTime? StartTime
        {
            get
            {
                DateTime dt;
                if (DateTime.TryParse((string)base[_starttime], out dt))
                    return dt;
                return null;
            }
            set
            {
                if (value.HasValue)
                    base[_starttime] = value.Value.ToString();
                else
                    base[_starttime] = null;
            }
        }

        [ConfigurationProperty(Constant.ConfigProperty.END_TIME)]
        public DateTime? EndTime
        {
            get
            {
                DateTime dt;
                if (DateTime.TryParse((string)base[_endtime], out dt))
                    return dt;
                return null;
            }
            set
            {
                if (value.HasValue)
                    base[_endtime] = value.Value.ToString();
                else
                    base[_endtime] = null;
            }
        }

        [ConfigurationProperty(Constant.ConfigProperty.ADV_SCHEDULE)]
        public string Schedule
        {
            get { return (string)base[_advsched]; }
            set { base[_advsched] = value; }
        }

        public NameValueCollection Parameters
        {
            get
            {
                if (_otherParameters == null)
                {
                    lock (this)
                    {
                        _otherParameters = new NameValueCollection(StringComparer.InvariantCulture);
                        foreach (ConfigurationProperty property in _properties)
                        {
                            switch (property.Name)
                            {
                                case Constant.ConfigProperty.NAME:
                                case Constant.ConfigProperty.ENABLED:
                                case Constant.ConfigProperty.TYPE:
                                case Constant.ConfigProperty.DESCRIPTION:
                                case Constant.ConfigProperty.SECONDS:
                                case Constant.ConfigProperty.START_TIME:
                                case Constant.ConfigProperty.END_TIME:
                                case Constant.ConfigProperty.ADV_SCHEDULE:
                                    break;

                                default:
                                    _otherParameters.Add(property.Name, (string)base[property]);
                                    break;
                            }
                        }
                    }
                }
                return _otherParameters;
            }
        }

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                UpdatePropertyCollection();
                return base.Properties;
            }
        }
        #endregion

        #region private methods
        private string GetProperty(string name)
        {
            if (this._properties.Contains(name))
            {
                ConfigurationProperty property = this._properties[name];
                if (property != null)
                {
                    return (string)base[property];
                }
            }
            return null;
        }

        private bool SetProperty(string name, string value)
        {
            ConfigurationProperty property = null;

            if (this._properties.Contains(name))
            {
                property = this._properties[name];
            }
            else
            {
                property = new ConfigurationProperty(name, typeof(string), null);
                this._properties.Add(property);
            }

            if (property != null)
            {
                base[property] = value;
                return true;
            }

            return false;
        }

        protected override bool IsModified()
        {
            if (!this.UpdatePropertyCollection())
            {
                return base.IsModified();
            }
            return true;
        }

        protected override bool OnDeserializeUnrecognizedAttribute(string name, string value)
        {
            ConfigurationProperty property = new ConfigurationProperty(name, typeof(string), value);
            this._properties.Add(property);

            base[property] = value;
            this.Parameters[name] = value;
            return true;
        }
        internal bool UpdatePropertyCollection()
        {
            bool isModified = false;
            ArrayList newPropertyNames = null;

            if (_otherParameters != null)
            {
                foreach (ConfigurationProperty property in _properties)
                {
                    switch (property.Name)
                    {
                        case Constant.ConfigProperty.NAME:
                        case Constant.ConfigProperty.ENABLED:
                        case Constant.ConfigProperty.SECONDS:
                        case Constant.ConfigProperty.TYPE:
                        case Constant.ConfigProperty.DESCRIPTION:
                        case Constant.ConfigProperty.START_TIME:
                        case Constant.ConfigProperty.END_TIME:
                        case Constant.ConfigProperty.ADV_SCHEDULE:
                            break;

                        default:
                            if (_otherParameters.Get(property.Name) == null)
                            {
                                if (newPropertyNames == null)
                                    newPropertyNames = new ArrayList();
                                newPropertyNames.Add(property.Name);
                                isModified = true;
                            }
                            break;
                    }                   
                }

                if (newPropertyNames != null)
                {
                    foreach (string name in newPropertyNames)
                    {
                        _properties.Remove(name);
                    }
                }

                foreach (string name in this._otherParameters)
                {
                    string oldVal = this._otherParameters[name];
                    string newVal = this.GetProperty(name);
                    if ((newVal == null) || (oldVal != newVal))
                    {
                        this.SetProperty(name, oldVal);
                        isModified = true;
                    }
                }
            }
            return isModified;
        }
        #endregion
    }

    public static class JobSettingExtension
    {
        public static JobSetting ToJobSetting(this JobSettingConfiguration conf)
        {
            JobSetting js = new JobSetting(conf.Name, Type.GetType(conf.Type), conf.Description);
            js.Enabled = conf.Enabled;
            js.PeriodSchedule = conf.Seconds;
            js.StartTime = conf.StartTime;
            js.EndTime = conf.EndTime;
            js.CronSchedule = conf.Schedule;
            js.Parameters = conf.Parameters;
            
            return js;
        }
    }

    [Serializable]
    public class JobSetting
    {
        public string Name { get; set; }
        public Type Worker { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; }
        public int? PeriodSchedule { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string CronSchedule { get; set; }
        public NameValueCollection Parameters { get; set; }
        public bool Recurring
        {
            get { return !string.IsNullOrEmpty(CronSchedule) || PeriodSchedule != null; }
        }

        public JobSetting(string name, Type worker, string description)
        {
            Init(name, worker, description);
        }

        public JobSetting(Type worker, string description)
        {
            if (worker == null)
                throw new ArgumentNullException("worker");
            Name = worker.Name + DateTime.Now.Ticks;
            Worker = worker;
            Description = description;
        }

        private void Init(string name, Type worker, string description)
        {
            Name = name;
            Worker = worker;
            Description = description;
        }

        public override string ToString()
        {
            return string.IsNullOrEmpty(this.CronSchedule) ? (this.PeriodSchedule.HasValue ? "every " + this.PeriodSchedule + " seconds" : "fire once") : this.CronSchedule;
        }
    }
}

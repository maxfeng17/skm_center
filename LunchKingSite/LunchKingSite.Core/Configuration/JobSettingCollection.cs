using System.Configuration;

namespace LunchKingSite.Core.Configuration
{
    [ConfigurationCollection(typeof(JobSettingConfiguration), AddItemName = Constant.ConfigSection.JOB)]
    public sealed class JobSettingCollection : ConfigurationElementCollection
    {
        #region private static members
        private static readonly ConfigurationProperty _enabled;
        private static readonly ConfigurationProperty _delay;
        private static readonly ConfigurationProperty _prop;
        private static ConfigurationPropertyCollection _propCol; 
        #endregion

        #region .ctors
        static JobSettingCollection()
        {
            JobSettingCollection._enabled = new ConfigurationProperty(Constant.ConfigProperty.ENABLED, typeof(bool), true, ConfigurationPropertyOptions.None);
            JobSettingCollection._delay = new ConfigurationProperty(Constant.ConfigProperty.DELAY, typeof(int), 3, ConfigurationPropertyOptions.None);
            JobSettingCollection._prop = new ConfigurationProperty(null, typeof(JobSettingCollection), null, ConfigurationPropertyOptions.IsDefaultCollection);
            JobSettingCollection._propCol = new ConfigurationPropertyCollection();
            JobSettingCollection._propCol.Add(JobSettingCollection._prop);
            JobSettingCollection._propCol.Add(JobSettingCollection._enabled);
        }
        #endregion

        #region properties
        [ConfigurationProperty(Constant.ConfigProperty.ENABLED, DefaultValue = true)]
        public bool Enabled
        {
            get { return (bool)base[JobSettingCollection._enabled]; }
            set { base[JobSettingCollection._enabled] = value; }
        }

        [ConfigurationProperty(Constant.ConfigProperty.DELAY, DefaultValue = 3)]
        public int Delay
        {
            get { return (int)base[JobSettingCollection._delay]; }
            set { base[JobSettingCollection._delay] = value; }
        }

        public JobSettingCollection this[int index]
        {
            get { return (JobSettingCollection)base.BaseGet(index); }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                this.BaseAdd(index, value);
            }
        }
        #endregion

        #region public methods
        public void Add(JobSettingConfiguration job)
        {
            if (job != null)
            {
                job.UpdatePropertyCollection();
                this.BaseAdd(job);
            }
        }

        public void Clear()
        {
            base.BaseClear();
        }

        public void Remove(string name)
        {
            base.BaseRemove(name);
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new JobSettingConfiguration();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            string key = ((JobSettingConfiguration)element).Name;
            return key;
        }
        #endregion
    }
}

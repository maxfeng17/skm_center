using System.Configuration;

namespace LunchKingSite.Core.Configuration
{
    public sealed class LKSiteConfigSection : ConfigurationSection
    {
        #region private variables
        
        private static readonly ConfigurationProperty _jobs;
        private static ConfigurationPropertyCollection _properties;
        #endregion

        #region .ctor
        static LKSiteConfigSection()
        {
            _jobs = new ConfigurationProperty(Constant.ConfigSection.JOBS, typeof(JobSettingCollection), null, ConfigurationPropertyOptions.IsRequired);
            _properties = new ConfigurationPropertyCollection();
            _properties.Add(_jobs);
        }
        #endregion

        #region properties
        [ConfigurationProperty(Constant.ConfigSection.JOBS)]
        public JobSettingCollection Jobs
        {
            get { return (JobSettingCollection)base[LKSiteConfigSection._jobs]; }
            set { base[LKSiteConfigSection._jobs] = value; }
        }
        #endregion
    }
}

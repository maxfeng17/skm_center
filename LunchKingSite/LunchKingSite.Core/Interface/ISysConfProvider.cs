﻿using LunchKingSite.Core.Component;
using System;
using System.Text;

namespace LunchKingSite.Core
{
    public interface ISysConfProvider : IProvider
    {
        #region collection related functions

        /// <summary>
        /// save parameters to persistence model
        /// </summary>
        /// <returns>true if successful</returns>
        bool Persist();

        #endregion collection related functions

        #region System Configuration

        [ConfigParameter("EnableLowestComboDealPriceForGoogleShopping", true, "啟用最低子檔價格給GoogleShopping", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnableLowestComboDealPriceForGoogleShopping { get; set; }

        [ConfigParameter("EnableGSASubstituteDiscountPriceForPrice", false, "啟用折扣後均價給GoogleShopping", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnableGSASubstituteDiscountPriceForPrice { get; set; }

        [ConfigParameter("DefaultTitle", "17Life，團購、宅配24小時出貨、優惠券、即買即用", "網站首頁標題", ConfigParameterAttribute.ConfigCategories.System)]
        string DefaultTitle { get; set; }

        [ConfigParameter("DefaultMetaDescription", "17Life-吃喝玩樂團購3折起，全家便利商店5折專區免費索取兌換！全台美食餐券、旅遊住宿、宅配24H快速到貨、SPA專區，還有品生活獨享的頂級精品、用餐優惠，即買即用，盡在17Life與你一起享受生活！", "網站首頁描述", ConfigParameterAttribute.ConfigCategories.System)]
        string DefaultMetaDescription { get; set; }

        [ConfigParameter("Title", "17Life生活電商", "網站檔次頁標題", ConfigParameterAttribute.ConfigCategories.System)]
        string Title { get; set; }

        [ConfigParameter("DefaultPiinlifeTitle", "PiinLife 品生活", "品生活網站首頁標題", ConfigParameterAttribute.ConfigCategories.System)]
        string DefaultPiinlifeTitle { get; set; }

        [ConfigParameter("DefaultPiinlifeMetaDescription", "這是一個以優惠價格，提供您各式頂級享受的服務網站。以團購概念出發，限時限量，卻更具時尚品味的會員獨享專屬體驗。走入屬於您的城市品味，就從PiinLife品生活開始。", "品生活網站首頁描述", ConfigParameterAttribute.ConfigCategories.System)]
        string DefaultPiinlifeMetaDescription { get; set; }

        [ConfigParameter("DefaultPiinlifeKeywords", "piinlife品生活,團購,17p,payeasy,品味,時尚,精品,設計,餐券,visa", "品生活網站首頁關鍵字", ConfigParameterAttribute.ConfigCategories.System)]
        string DefaultPiinlifeKeywords { get; set; }

        [ConfigParameter("DefaultMetaOgTitle", "17Life-吃喝玩樂3折起，優惠券隨身帶著走", "社群(FB)分享首頁的標題", ConfigParameterAttribute.ConfigCategories.System)]
        string DefaultMetaOgTitle { get; set; }

        [ConfigParameter("DefaultMetaOgImage", "https://www.17life.com/images/17P/100X100/200x200_17Life2014.jpg", "社群(FB)分享首頁的圖片", ConfigParameterAttribute.ConfigCategories.System)]
        string DefaultMetaOgImage { get; set; }

        [ConfigParameter("DefaultMetaOgDescription", "17Life團購3折起！第一手優惠情報，盡在17Life與你一起享受生活！", "社群(FB)分享首頁的描述", ConfigParameterAttribute.ConfigCategories.System)]
        string DefaultMetaOgDescription { get; set; }

        [ConfigParameter("ServiceTel", "(02)3316-9105", "客服電話", ConfigParameterAttribute.ConfigCategories.System)]
        string ServiceTel { get; set; }

        [ConfigParameter("ServiceFax", "(02)2511-9110", "客服傳真", ConfigParameterAttribute.ConfigCategories.System)]
        string ServiceFax { get; set; }

        [ConfigParameter("ServiceName", "Service", "客服顯示名稱，如客服信件", ConfigParameterAttribute.ConfigCategories.System)]
        string ServiceName { get; set; }

        [ConfigParameter("EvaluateServiceName", "17ife 使用評鑑", "客服顯示名稱，如客服信件", ConfigParameterAttribute.ConfigCategories.System)]
        string EvaluateServiceName { get; set; }

        [ConfigParameter("BusinessServiceToHouseAccountTel", "#8692、#8186、#8185", "宅配帳務服務專線", ConfigParameterAttribute.ConfigCategories.System)]
        string BusinessServiceToHouseAccountTel { get; set; }

        [ConfigParameter("BusinessServiceToHouseTel", "#8534、#8535、#8506、#8531", "宅配廠商服務專線", ConfigParameterAttribute.ConfigCategories.System)]
        string BusinessServiceToHouseTel { get; set; }


        [ConfigParameter("BusinessServicePponAccountTel", "#8023", "憑證帳務服務專線", ConfigParameterAttribute.ConfigCategories.System)]
        string BusinessServicePponAccountTel { get; set; }

        [ConfigParameter("BusinessServicePponTel", "#8003 #8762 #8768", "一般憑證廠商服務專線", ConfigParameterAttribute.ConfigCategories.System)]
        string BusinessServicePponTel { get; set; }

        [ConfigParameter("BusinessServiceTravelPponAccountTel", " #8655 #8770 #8771", "旅遊憑證廠商服務專線", ConfigParameterAttribute.ConfigCategories.System)]
        string BusinessServiceTravelPponAccountTel { get; set; }

        [ConfigParameter("BusinessServiceOnDutyTel", "（02）2521 - 9131 # 8666 ，上午09:00 ~ 12:30；下午01:30 ~ 08:00（週一 ~ 週五）", "廠商服務值班專員電話", ConfigParameterAttribute.ConfigCategories.System)]
        string BusinessServiceOnDutyTel { get; set; }

        [ConfigParameter("MediaBaseUrl", "https://localhost/media/", "", ConfigParameterAttribute.ConfigCategories.System)]
        string MediaBaseUrl { get; set; }

        [ConfigParameter("CDNBaseUrl", "http://EDEB.http.cdn.softlayer.net/80EDEB/www.17life.com/", "CDN base Url", ConfigParameterAttribute.ConfigCategories.System)]
        string CDNBaseUrl { get; set; }

        [ConfigParameter("ShoppingCartReserveDays", 7, "", ConfigParameterAttribute.ConfigCategories.System)]
        int ShoppingCartReserveDays { get; set; }

        [ConfigParameter("GroupOrderReserveDays", 15, "", ConfigParameterAttribute.ConfigCategories.System)]
        int GroupOrderReserveDays { get; set; }

        [ConfigParameter("BypassSSOVerify", false, "", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool BypassSsoVerify { get; set; }

        [ConfigParameter("TrackerExcludedIp", "127.", "", ConfigParameterAttribute.ConfigCategories.System)]
        string TrackerExcludedIp { get; set; }

        [ConfigParameter("TemporaryStoragePath", "/cache", "", ConfigParameterAttribute.ConfigCategories.System)]
        string TemporaryStoragePath { get; set; }

        [ConfigParameter("WebTempPath", "/cache", "功用同TemporaryStoragePath，但預期所有web會指向同一目錄", ConfigParameterAttribute.ConfigCategories.System)]
        string WebTempPath { get; set; }

        [ConfigParameter("WebAppDataPath", "/cache", "類似app_data，但預期所有web會指向同一目錄", ConfigParameterAttribute.ConfigCategories.System)]
        string WebAppDataPath { get; set; }

        [ConfigParameter("ReturnWaitingDays", 10, "退貨等待天數", ConfigParameterAttribute.ConfigCategories.System)]
        int ReturnWaitingDays { get; set; }

        [ConfigParameter("FinalReturnNotifyDays", 3, "退貨申請第N天後再次通知廠商", ConfigParameterAttribute.ConfigCategories.System)]
        int NotifyVendorProcessReturnDays { get; set; }

        [ConfigParameter("SysRefundScashExpriedDays", 2, "檔次逾期天數後系統自動退購物金", ConfigParameterAttribute.ConfigCategories.System)]
        int SysRefundScashExpriedDays { get; set; }
        [ConfigParameter("ExhibitionDealExpiredVerifiedExpriedDays", 1, "展覽檔次逾期天數後系統自動核銷", ConfigParameterAttribute.ConfigCategories.System)]
        int ExhibitionDealExpiredVerifiedExpriedDays { get; set; }

        [ConfigParameter("IsReturnByPaymentTypeAmount", false, "", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsReturnByPaymentTypeAmount { get; set; }

        [ConfigParameter("ImagePoolUrl", "www.17life.com", "", ConfigParameterAttribute.ConfigCategories.System)]
        string ImagePoolUrl { get; set; }

        [ConfigParameter("EnableImagesCDN", true, "", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnableImagesCDN { get; set; }

        [ConfigParameter("SiteUrl", "https://localhost", "", ConfigParameterAttribute.ConfigCategories.System)]
        string SiteUrl { get; set; }

        [ConfigParameter("SSLSiteUrl", "https://localhost", "", ConfigParameterAttribute.ConfigCategories.System)]
        string SSLSiteUrl { get; set; }

        [ConfigParameter("EIPSiteUrl", "http://eip.17life.com", "", ConfigParameterAttribute.ConfigCategories.System)]
        string EIPSiteUrl { get; set; }

        [ConfigParameter("SiteServiceUrl", "/Ppon/NewbieGuide.aspx", "17Life客服頁面入口", ConfigParameterAttribute.ConfigCategories.System)]
        string SiteServiceUrl { get; set; }
        [ConfigParameter("CustomerSiteService", "/User/ServiceList", "17Life客服頁面入口", ConfigParameterAttribute.ConfigCategories.System)]
        string CustomerSiteService { get; set; }

        [ConfigParameter("EmailTemplateDirectory", "C:\\source\\web_main\\template\\", "", ConfigParameterAttribute.ConfigCategories.System)]
        string EmailTemplateDirectory { get; set; }

        [ConfigParameter("InvoiceMailFolder", "C:\\source\\web_main\\cache\\einvoiceMail\\", "電子發票通知信存放路徑Folder", ConfigParameterAttribute.ConfigCategories.System)]
        string InvoiceMailFolder { get; set; }

        [ConfigParameter("DefaultTestCreditcard", "8888-8800-0000-0001", "預設測試信用卡號", ConfigParameterAttribute.ConfigCategories.System)]
        string DefaultTestCreditcard { get; set; }
        [ConfigParameter("DefaultTestCreditcardExpireDate", "18/12", "預設測試信用卡號有效期限", ConfigParameterAttribute.ConfigCategories.System)]
        string DefaultTestCreditcardExpireDate { get; set; }
        [ConfigParameter("DefaultTestCreditcardSecurityCode", "793", "預設測試信用卡號背面3碼", ConfigParameterAttribute.ConfigCategories.System)]
        string DefaultTestCreditcardSecurityCode { get; set; }
        /// <summary>
        /// 電子發票實施要點的連結
        /// </summary>
        [ConfigParameter("InvoiceKeyPointUrl", "https://www.einvoice.nat.gov.tw/home/DownLoad;jsessionid=vQQwSKsGjG0byP92rhZtc2tWBKppy2G61gDYWLJ1zLyNjhB1Jj10!-1862511490?fileName=1377486728934_0.pdf&CSRT=14953669909836288913", "電子發票實施要點的連結", ConfigParameterAttribute.ConfigCategories.System)]
        string InvoiceKeyPointUrl { get; set; }

        [ConfigParameter("InvoiceDelayDays", -1, "開立發票延遲天數", ConfigParameterAttribute.ConfigCategories.System)]
        int InvoiceDelayDays { get; set; }

        [ConfigParameter("EInvoiceAppId", "EINV9201310315863", "電子發票實17Life申請的客戶端ID", ConfigParameterAttribute.ConfigCategories.System)]
        string EInvoiceAppId { get; set; }

        [ConfigParameter("EInvoiceApiUrl", "https://www.einvoice.nat.gov.tw", "政府提供的電子發票service網址", ConfigParameterAttribute.ConfigCategories.System)]
        string EInvoiceApiUrl { get; set; }

        [ConfigParameter("InvoiceBufferDays", 5, "電子發票跨期處理緩衝天數", ConfigParameterAttribute.ConfigCategories.System)]
        int InvoiceBufferDays { get; set; }

        [ConfigParameter("VendorInvoiceNumberMin", 10000, "廠商對開發票數", ConfigParameterAttribute.ConfigCategories.System)]
        int VendorInvoiceNumberCount { get; set; }

        [ConfigParameter("EnabledInvoicePaperRequest", false, "提供前台索取紙本發票", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnabledInvoicePaperRequest { get; set; }

        /// <summary>
        /// 判斷寄送Mail的環境是否需要過依host過濾之
        /// </summary>
        [ConfigParameter("SmtpMailAgent", SmtpMailServerAgent.SiteSmtpMailAgentFilterHost, "判斷寄送Mail的環境是否需要過依host過濾之", ConfigParameterAttribute.ConfigCategories.System)]
        SmtpMailServerAgent SmtpMailAgent { get; set; }

        [ConfigParameter("SmtpOtherHosts", "", "分流用的 smtp 網址，可不設，可多筆，逗號分隔。<br />啟用是將SmtpMailAgent設為SmtpMailLoadBalanceAgent", ConfigParameterAttribute.ConfigCategories.System)]
        string SmtpOtherHosts { get; set; }

        [ConfigParameter("EnableCpa", true, "是否開啟CPA", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableCpa { get; set; }

        [ConfigParameter("EnableIChannel", true, "是否開啟iChannel", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableIChannel { get; set; }

        [ConfigParameter("IChannelCookieExpires", 15, "iChannel Cookie的逾時天數", ConfigParameterAttribute.ConfigCategories.System)]
        int IChannelCookieExpires { get; set; }

        [ConfigParameter("IChannelApiUrl", "http://t.conn.tw/sp", "丟訂單給iChannel呼叫的API", ConfigParameterAttribute.ConfigCategories.System)]
        string IChannelApiUrl { get; set; }

        [ConfigParameter("IChannelMcode", "pySAiqSnjGvA94Zt", "廣告主在iChannels的編號", ConfigParameterAttribute.ConfigCategories.System)]
        string IChannelMcode { get; set; }

        [ConfigParameter("IChannelItemToShop", "1412_2", "廣告主在iChannels的合作項目代號", ConfigParameterAttribute.ConfigCategories.System)]
        string IChannelItemToShop { get; set; }

        [ConfigParameter("IChannelItemToHouse", "1412_1", "廣告主在iChannels的合作項目代號", ConfigParameterAttribute.ConfigCategories.System)]
        string IChannelItemToHouse { get; set; }

        [ConfigParameter("IChannelRunDay", 60, "iChannel訂單成立後幾日確定分潤", ConfigParameterAttribute.ConfigCategories.System)]
        int IChannelRunDay { get; set; }

        [ConfigParameter("EnableCommissionRule", true, "啟用導購商分潤設定進行分潤", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnableCommissionRule { get; set; }

        #region 上傳到mail server之相關設定

        /// <summary>
        /// Mail Server的FPT HOST此設定為正式站所使用的
        /// 正式站外之環境，請自行設定為edm@edm.17life.com.tw
        /// </summary>
        [ConfigParameter("EmailServerFTPHost", @"10.3.0.50", "[Mail Server 設定] FTP IP", ConfigParameterAttribute.ConfigCategories.System)]
        string EmailServerFTPHost { get; set; }

        [ConfigParameter("EmailServerFTPPort", 21, "[Mail Server 設定] FTP port", ConfigParameterAttribute.ConfigCategories.System)]
        int EmailServerFTPPort { get; set; }

        /// <summary>
        /// Mail Server的FPT之登入帳號
        /// </summary>
        [ConfigParameter("EmailServerFTPUser", @"edm", "[Mail Server 設定] FTP 之登入帳號", ConfigParameterAttribute.ConfigCategories.System)]
        string EmailServerFTPUser { get; set; }

        /// <summary>
        /// Mail Server的FPT之登入密碼
        /// </summary>
        [ConfigParameter("EmailServerFTPPw", @"edm123", "[Mail Server 設定] FTP 之登入密碼", ConfigParameterAttribute.ConfigCategories.System)]
        string EmailServerFTPPw { get; set; }

        [ConfigParameter("EmailServerFTPSavePath", @"./", "[Mail Server 設定] 登入後要進入的目錄", ConfigParameterAttribute.ConfigCategories.System)]
        string EmailServerFTPSavePath { get; set; }

        #endregion 上傳到mail server之相關設定

        [ConfigParameter("DataManagerUsed", true, "", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool DataManagerUsed { get; set; }

        [ConfigParameter("SmsSystem", SmsSystem.Sybase, "預設簡訊系統(目前請勿選Sybase/S2T之外的)", ConfigParameterAttribute.ConfigCategories.System)]
        SmsSystem SmsSystem { get; set; }

        [ConfigParameter("SmsRatio", "1:0", "簡訊商服務比例(Sybase:iTe2)", ConfigParameterAttribute.ConfigCategories.System)]
        string SmsRatio { get; set; }

        [ConfigParameter("SmsPhoneNumberLimit", 50, "當簡訊送給多號碼。如果號碼超過這個數字會拆分成多個Request", ConfigParameterAttribute.ConfigCategories.System)]
        int SmsPhoneNumberLimit { get; set; }

        [ConfigParameter("NewInvoiceDate", "2012/7/1", "核銷後開發票起始日", ConfigParameterAttribute.ConfigCategories.System)]
        DateTime NewInvoiceDate { get; set; }

        /// <summary>
        /// This is the query ratio between Publisher db and Subscriber db.
        /// </summary>
        [ConfigParameter("BalanceRatio", "4:3", "", ConfigParameterAttribute.ConfigCategories.System)]
        string BalanceRatio { get; set; }

        /// <summary>
        /// 手機版好康主頁網址，預設BuildingGuid值，如找不到BuildingGuid就給"FF1CCEED-FD3B-472D-AC4E-3A8C2D66D429"
        /// </summary>
        [ConfigParameter("DefaultBuildingGuid", "FF1CCEED-FD3B-472D-AC4E-3A8C2D66D429", "", ConfigParameterAttribute.ConfigCategories.System)]
        Guid DefaultBuildingGuid { get; set; }

        /// <summary>
        /// 商品鑑賞期
        /// </summary>
        [ConfigParameter("GoodsAppreciationPeriod", 7, "", ConfigParameterAttribute.ConfigCategories.System)]
        int GoodsAppreciationPeriod { get; set; }

        /// <summary>
        /// 票券檔退貨期限天數
        /// </summary>
        [ConfigParameter("ProductRefundDays", 7, "票券檔退貨期限天數", ConfigParameterAttribute.ConfigCategories.System)]
        int ProductRefundDays { get; set; }

        /// <summary>
        /// 演出時間前幾日內不能退貨之天數(相關flag: OrderConfig.NoRefundBeforeDays 之天數)
        /// </summary>
        [ConfigParameter("PponNoRefundBeforeDays", 10, "演出時間前幾日內不能退貨之天數", ConfigParameterAttribute.ConfigCategories.System)]
        int PponNoRefundBeforeDays { get; set; }

        /// <summary>
        /// 訂閱活動視窗Cookie期限
        /// </summary>
        [ConfigParameter("EventEmailCookieDays", "-1,5,7", "訂閱活動視窗Cookie期限", ConfigParameterAttribute.ConfigCategories.System)]
        string EventEmailCookieDays { get; set; }

        /// <summary>
        /// 每周付款ACH分頁設定
        /// </summary>
        [ConfigParameter("WeeklyPayPageSize", 15, "", ConfigParameterAttribute.ConfigCategories.System)]
        int WeeklyPayPageSize { get; set; }

        /// <summary>
        /// 首頁跳窗cache
        /// </summary>
        [ConfigParameter("EdmPopUpCacheName", "EdmPopUp", "跳窗cache名稱", ConfigParameterAttribute.ConfigCategories.System)]
        string EdmPopUpCacheName { get; set; }

        /// <summary>
        /// 是否啟用行銷滿額贈活動
        /// </summary>
        [ConfigParameter("IsEnabledSpecialConsumingDiscount", false, "是否啟用行銷滿額贈活動", ConfigParameterAttribute.ConfigCategories.System)]
        bool IsEnabledSpecialConsumingDiscount { get; set; }

        /// <summary>
        /// 行銷滿額贈活動期間
        /// </summary>
        [ConfigParameter("SpecialConsumingDiscountPeriod", "2013/10/24 12:00:00|2013/11/04 23:59:00", "行銷滿額贈活動期間", ConfigParameterAttribute.ConfigCategories.System)]
        string SpecialConsumingDiscountPeriod { get; set; }

        /// <summary>
        /// 行銷滿額贈活動指定分類(0為不指定)
        /// </summary>
        [ConfigParameter("SpecialConsumingDiscountCid", 0, "行銷滿額贈活動指定分類", ConfigParameterAttribute.ConfigCategories.System)]
        int SpecialConsumingDiscountCid { get; set; }

        /// <summary>
        /// 行銷滿額贈活動指定消費額度
        /// </summary>
        [ConfigParameter("SpecialConsumingDiscountAmount", 1000, "行銷滿額贈活動指定消費額度", ConfigParameterAttribute.ConfigCategories.System)]
        int SpecialConsumingDiscountAmount { get; set; }

        /// <summary>
        /// 行銷滿額贈活動詳細活動說明網址
        /// </summary>
        [ConfigParameter("SpecialConsumingDiscountUrl", "/Event/Exhibition.aspx?u=201410_foodsafe&rsrc=17_eventLocal", "行銷滿額贈活動詳細活動說明網址", ConfigParameterAttribute.ConfigCategories.System)]
        string SpecialConsumingDiscountUrl { get; set; }

        /// <summary>
        /// 是否啟用購物車
        /// </summary>
        [ConfigParameter("ShoppingCartEnabled", false, "是否啟用購物車", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool ShoppingCartEnabled { get; set; }

        /// <summary>
        /// 是否啟用購物車V2
        /// </summary>
        [ConfigParameter("ShoppingCartV2Enabled", false, "是否啟用購物車", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool ShoppingCartV2Enabled { get; set; }

        /// <summary>
        /// 是否啟用商家模擬存取限制
        /// </summary>
        [ConfigParameter("SimulateReadOnlyEnabled", false, "是否啟用商家模擬存取限制", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool SimulateReadOnlyEnabled { get; set; }

        /// <summary>
        /// MasterPass紅利贈送活動期間
        /// </summary>
        [ConfigParameter("MasterPassSendBonusPeriod", "2014/11/19 00:00:00|2014/12/31 23:59:59", "MasterPass紅利贈活動期間", ConfigParameterAttribute.ConfigCategories.System)]
        string MasterPassSendBonusPeriod { get; set; }

        /// <summary>
        /// MasterPass紅利贈送活動設定
        /// </summary>
        [ConfigParameter("MasterPassSendBonusSetting", "2014/11/19 00:00:00|2014/12/31 23:59:59|500|MasterPass送紅利", "MasterPass紅利贈活動設定({紅利起日|紅利迄日|紅利點數|活動名稱})", ConfigParameterAttribute.ConfigCategories.System)]
        string MasterPassSendBonusSetting { get; set; }

        /// <summary>
        /// 業務工單-自動儲存時間(毫秒)
        /// </summary>
        [ConfigParameter("BusinessOrderUpdateTimerInterval", 180000, "業務工單-自動儲存時間(毫秒)", ConfigParameterAttribute.ConfigCategories.System)]
        int BusinessOrderUpdateTimerInterval { get; set; }

        [ConfigParameter("NegativeCommentsRate", 5, "超過負評比率發送警訊通知(%)", ConfigParameterAttribute.ConfigCategories.System)]
        int NegativeCommentsRate { get; set; }

        [ConfigParameter("SearchApiUrl", "http://localhost:1766", "外部的搜尋服務API網址", ConfigParameterAttribute.ConfigCategories.System)]
        string SearchApiUrl { get; set; }

        [ConfigParameter("SearchEngine", 0, "預設的搜尋服務", ConfigParameterAttribute.ConfigCategories.System)]
        int SearchEngine { get; set; }

        #region Mohist

        /// <summary>
        /// 墨攻合作舊廠商id
        /// </summary>
        [ConfigParameter("MohistId", "OSL", "墨攻合作舊廠商id", ConfigParameterAttribute.ConfigCategories.System)]
        string MohistId { get; set; }

        /// <summary>
        /// 墨攻核銷舊ftp路徑
        /// </summary>
        [ConfigParameter("MohistVerifyFtpPath", "10.13.0.1", "墨攻核銷舊ftp路徑", ConfigParameterAttribute.ConfigCategories.System)]
        string MohistVerifyFtpPath { get; set; }

        /// <summary>
        /// 墨攻核銷舊ftp帳號
        /// </summary>
        [ConfigParameter("MohistVerifyFtpAccountId", "mohist", "墨攻核銷舊ftp帳號", ConfigParameterAttribute.ConfigCategories.System)]
        string MohistVerifyFtpAccountId { get; set; }

        /// <summary>
        /// 墨攻核銷舊ftp密碼
        /// </summary>
        [ConfigParameter("MohistVerifyFtpPassWord", "1111", "墨攻核銷舊ftp密碼", ConfigParameterAttribute.ConfigCategories.System)]
        string MohistVerifyFtpPassWord { get; set; }

        /// <summary>
        /// 墨攻合作新廠商id
        /// </summary>
        [ConfigParameter("MohistId2", "OST", "墨攻合作新廠商id", ConfigParameterAttribute.ConfigCategories.System)]
        string MohistId2 { get; set; }

        /// <summary>
        /// 墨攻核銷新ftp路徑
        /// </summary>
        [ConfigParameter("MohistVerifyFtpPath2", "www.mohist.com.tw", "墨攻核銷新ftp路徑", ConfigParameterAttribute.ConfigCategories.System)]
        string MohistVerifyFtpPath2 { get; set; }

        /// <summary>
        /// 墨攻核銷新ftp帳號
        /// </summary>
        [ConfigParameter("MohistVerifyFtpAccountId2", "OST", "墨攻核銷新ftp帳號", ConfigParameterAttribute.ConfigCategories.System)]
        string MohistVerifyFtpAccountId2 { get; set; }

        /// <summary>
        /// 墨攻核銷新ftp密碼
        /// </summary>
        [ConfigParameter("MohistVerifyFtpPassWord2", "1111", "墨攻核銷新ftp密碼", ConfigParameterAttribute.ConfigCategories.System)]
        string MohistVerifyFtpPassWord2 { get; set; }

        /// <summary>
        /// 開啟墨攻上傳
        /// </summary>
        [ConfigParameter("EnabledMohistUpload", false, "開啟墨攻上傳", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnabledMohistUpload { get; set; }
        #endregion Mohist

        #region Sybase

        /// <summary>
        /// Sybase URL (http://sms-pp.sapmobileservices.com/cmn/17life17623/17life17623.sms)
        /// </summary>
        [ConfigParameter("SybaseUrl", "http://178.248.228.151/cmn/17life17623/17life17623.sms", "Sybase URL", ConfigParameterAttribute.ConfigCategories.System)]
        string SybaseUrl { get; set; }

        /// <summary>
        /// Sybase 備援 URL (http://messaging.ota.com/17life17623/17life17623.sms)
        /// </summary>
        [ConfigParameter("SybaseSecondUrl", "http://messaging.ota.com/17life17623/17life17623.sms", "Sybase 備援 URL", ConfigParameterAttribute.ConfigCategories.System)]
        string SybaseSecondUrl { get; set; }

        #endregion Sybase

        #region S2T

        /// <summary>
        /// Sim2Travel伺服器優先權-- First:S2tSms Second:S2tSms2
        /// </summary>
        [ConfigParameter("S2tServerPriority", "1st", "Sim2Travel伺服器優先權(1st/2nd)", ConfigParameterAttribute.ConfigCategories.System)]
        string S2tServerPriority { get; set; }

        #endregion S2T

        #region Cowell

        /// <summary>
        /// 科威收據: FTP IP (127.0.0.1)
        /// </summary>
        [ConfigParameter("CowellFtpHost", "localhost", "科威收據: FTP IP (127.0.0.1)", ConfigParameterAttribute.ConfigCategories.System)]
        string CowellFtpHost { get; set; }

        /// <summary>
        /// 科威: FTP 帳號
        /// </summary>
        [ConfigParameter("CowellFtpUserName", "cowell", "科威: FTP 帳號", ConfigParameterAttribute.ConfigCategories.System)]
        string CowellFtpUserName { get; set; }

        /// <summary>
        /// 科威: FTP 密碼
        /// </summary>
        [ConfigParameter("CowellFtpPassword", "g0gIKBo5", "科威: FTP 密碼", ConfigParameterAttribute.ConfigCategories.System)]
        string CowellFtpPassword { get; set; }

        /// <summary>
        /// 科威: FTP 資料匯入的路徑 (upload/)
        /// </summary>
        [ConfigParameter("CowellFtpRemoteUploadFolder", "get/", "科威: FTP 資料匯入的路徑 (upload/)", ConfigParameterAttribute.ConfigCategories.System)]
        string CowellFtpRemoteUploadFolder { get; set; }

        /// <summary>
        /// 科威: FTP 取回處理結果的路徑 (complete/)
        /// </summary>
        [ConfigParameter("CowellFtpRemoteCompletedFolder", "put/", "科威: FTP 取回處理結果的路徑 (complete/)", ConfigParameterAttribute.ConfigCategories.System)]
        string CowellFtpRemoteCompletedFolder { get; set; }

        /// <summary>
        /// 備份資料到本機的路徑 (c:\temp\)
        /// </summary>
        [ConfigParameter("CowellDataBackupPath", @"C:\source\web_main\CowellTemp\", "備份資料到本機的路徑 (c:\\temp\\)", ConfigParameterAttribute.ConfigCategories.System)]
        string CowellDataBackupPath { get; set; }

        #endregion Cowell

        /// <summary>
        /// 核銷緩衝天數
        /// </summary>
        [ConfigParameter("VerificationBuffer", 3, "核銷緩衝天數", ConfigParameterAttribute.ConfigCategories.System)]
        int VerificationBuffer { get; set; }

        /// <summary>
        /// 反核銷緩衝天數
        /// </summary>
        [ConfigParameter("UnverificationBuffer", 0, "反核銷緩衝天數", ConfigParameterAttribute.ConfigCategories.System)]
        int UnverificationBuffer { get; set; }

        /// <summary>
        /// 出貨緩衝天數(計算鑑賞期使用)
        /// </summary>
        [ConfigParameter("ShippingBuffer", 10, "出貨緩衝天數(計算鑑賞期使用) 判斷請使用日期", ConfigParameterAttribute.ConfigCategories.System)]
        int ShippingBuffer { get; set; }

        /// <summary>
        /// 7-11取貨對帳天數
        /// </summary>
        [ConfigParameter("SevenShippingBuffer", 15, "7-11取貨對帳天數", ConfigParameterAttribute.ConfigCategories.System)]
        int SevenShippingBuffer { get; set; }

        [ConfigParameter("NewCouponPdfWriter", true, "設成true，將使用iTextSharp產生憑證PDF", ConfigParameterAttribute.ConfigCategories.System)]
        bool NewCouponPdfWriter { get; set; }

        [ConfigParameter("NoneCreditRefundPeriod", 360, "訂單訂購完後, 滿此天數則不可刷退", ConfigParameterAttribute.ConfigCategories.System)]
        int NoneCreditRefundPeriod { get; set; }

        [ConfigParameter("EnabledValidateBuyerAddress", false, "web購買頁檢查地址合法性", ConfigParameterAttribute.ConfigCategories.Ppon)]
        bool EnabledValidateBuyerAddress { get; set; }

        [ConfigParameter("GAConsolePath", @"C:\project\Gaia\GoogleAnalytics\ConsoleApplicationGA\bin\Debug\ConsoleApplicationGA.exe", "GA console路徑", ConfigParameterAttribute.ConfigCategories.System)]
        string GAConsolePath { get; set; }

        #region FamilyMart

        /// <summary>
        /// 全家防止重覆取PIN
        /// </summary>
        [ConfigParameter("FamiOrderExgGate", false, "全家防止重覆取PIN", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        bool FamiOrderExgGate { get; set; }

        /// <summary>
        /// 全家Famiport廠商代碼
        /// </summary>
        [ConfigParameter("Famiport17LifeCode", "667002", "全家Famiport廠商代碼", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string Famiport17LifeCode { get; set; }

        /// <summary>
        /// 全家Famiport廠商代碼
        /// </summary>
        [ConfigParameter("FamiportAesKey", "3H2D4G37BPQEFS52", "全家Pincode AES Key", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string FamiportAesKey { get; set; }

        /// <summary>
        /// 全家Famiport AUTHID Rule
        /// </summary>
        [ConfigParameter("FamiportAuthFormat", "F{0}${1}N{2}@{3}N{4}E{5}T", "全家Famiport AUTHID Rule", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string FamiportAuthFormat { get; set; }

        /// <summary>
        /// 全家Famiport 取 Pincode API URL
        /// </summary>
        [ConfigParameter("FamiportApiUrl", "https://test.familynet.com.tw/bonWBS_API_V4/wap/{0}", "全家Famiport 取Pin API Url", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string FamiportApiUrl { get; set; }

        /// <summary>
        /// 全家Famiport 取一段條碼 Pincode API URL
        /// </summary>
        [ConfigParameter("FamiportSingleBarcodeApiUrl", "https://ect.familynet.com.tw/bonWBS_API_19/wap/{0}", "全家Famiport 取一段條碼 Pin API Url", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string FamiportSingleBarcodeApiUrl { get; set; }

        /// <summary>
        /// 全家Famiport Pincode換Barcode Fail Retry 次數
        /// </summary>
        [ConfigParameter("FamiportPinOrderExgRetryTimes", 6, "全家Pin換Barcode Fail Retry 次數", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        int FamiportPinOrderExgRetryTimes { get; set; }

        /// <summary>
        /// 全家Famiport Pincode換Barcode Fail Retry 間隔秒數
        /// </summary>
        [ConfigParameter("FamiportPinOrderExgRetryTimeSpan", 600, "全家Pin換Barcode Fail Retry 間隔秒數", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        int FamiportPinOrderExgRetryTimeSpan { get; set; }

        /// <summary>
        /// 全家Famiport Pincode Verify Fail Retry 次數
        /// </summary>
        [ConfigParameter("FamiportPinVerifyRetryTimes", 4, "全家Famiport Pincode Verify Fail Retry 次數", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        int FamiportPinVerifyRetryTimes { get; set; }

        /// <summary>
        /// 全家Famiport Pincode Verify Fail Retry 間隔秒數
        /// </summary>
        [ConfigParameter("FamiportPinVerifyRetryTimeSpan", 300, "全家Pincode Verify Fail Retry 間隔秒數", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        int FamiportPinVerifyRetryTimeSpan { get; set; }

        /// <summary>
        /// 全家Famiport Pincode換Barcode Fail Retry 間隔秒數
        /// </summary>
        [ConfigParameter("FamiportPinOrderExgRetryPerDay", 1, "全家Pin換Barcode Fail Retry 幾天內資料", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        int FamiportPinOrderExgRetryPerDay { get; set; }

        /// <summary>
        /// 全家Famiport Pincode換Barcode Fail Retry 間隔秒數
        /// </summary>
        [ConfigParameter("FamiportPinVerifyRetryPerDay", 3, "全家Pincode Verify Retry 幾天內資料", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        int FamiportPinVerifyRetryPerDay { get; set; }

        /// <summary>
        /// 全家兌換寄杯鎖定後的解鎖秒數
        /// </summary>
        [ConfigParameter("FamilyNetUnlockPincodeSec", 480, "全家兌換寄杯鎖定後的解鎖秒數", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        int FamilyNetUnlockPincodeSec { get; set; }

        /// <summary>
        /// 全家檔次按鈕字串
        /// </summary>
        [ConfigParameter("FamilyCityButtonString", "立即索取", "全家檔次按鈕字串", ConfigParameterAttribute.ConfigCategories.System)]
        string FamilyCityButtonString { get; set; }

        [ConfigParameter("FamiDailyPath", @"E:\ftproot\famiport\", "全家檔次每日上傳路徑", ConfigParameterAttribute.ConfigCategories.System)]
        string FamiDailyPath { get; set; }

        /// <summary>
        /// 全家遙遙茶緊急處理
        /// </summary>
        [ConfigParameter("FamiteaDealId", "10031199", "全家遙遙茶緊急處理", ConfigParameterAttribute.ConfigCategories.System)]
        int[] FamiteaDealId { get; set; }

        /// <summary>
        /// 全家遙遙茶緊急處理，憑證描述字串
        /// </summary>
        [ConfigParameter("FamiteaCouponDesc", "點擊店家資訊 > 官網連結", "全家遙遙茶緊急處理，憑證描述字串", ConfigParameterAttribute.ConfigCategories.System)]
        string FamiteaCouponDesc { get; set; }

        [ConfigParameter("FamilyMariOpenDatetime", "2012-12-26 12:00:00", "是否開放全家專區", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.normal)]
        string FamilyMariOpenDatetime { get; set; }

        /// <summary>
        ///全家Seller Guid 
        /// </summary>
        [ConfigParameter("FamilyNetSellerGuid", "916378E4-EA03-4537-BB47-163C043F51D0", "全家Seller Guid", ConfigParameterAttribute.ConfigCategories.System)]
        Guid FamilyNetSellerGuid { get; set; }
        /// <summary>
        ///全家Seller Guid 
        /// </summary>
        [ConfigParameter("FamilyCategordId", 91, "全家 CategoryId", ConfigParameterAttribute.ConfigCategories.System)]
        int FamilyCategordId { get; set; }

        /// <summary>
        /// 全家寄杯逾期天數
        /// </summary>
        [ConfigParameter("FamilyExpiredReturnDay", 1, "全家寄杯逾期處理天數", ConfigParameterAttribute.ConfigCategories.System)]
        int FamilyExpiredReturnDay { get; set; }

        /// <summary>
        /// 全家寄杯自動註銷過期未兌換Pin
        /// </summary>
        [ConfigParameter("FamilyNetExpiredPinAutoReturn", false, "全家寄杯自動註銷過期未兌換Pin", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        bool FamilyNetExpiredPinAutoReturn { get; set; }

        /// <summary>
        /// 全家退貨API更新中停止呼叫開始時間
        /// </summary>
        [ConfigParameter("FamilyNetReturnApiStopTimeS", "2017/5/24 01:00:00", "全家退貨API更新中停止呼叫開始時間", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        DateTime FamilyNetReturnApiStopTimeS { get; set; }

        /// <summary>
        /// 全家退貨API更新中停止呼叫結束時間
        /// </summary>
        [ConfigParameter("FamilyNetReturnApiStopTimeE", "2017/5/24 03:00:00", "全家退貨API更新中停止呼叫結束時間", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        DateTime FamilyNetReturnApiStopTimeE { get; set; }

        /// <summary>
        /// 啟用全家一段條碼
        /// </summary>
        [ConfigParameter("EnableFamilyBarcodeVersion", "true", "啟用全家一段條碼", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnableFamilyBarcodeVersion { get; set; }

        #endregion FamilyMart

        /// <summary>
        /// 商家收據寄回截止日: 在截止日前, 對帳單繼續匯錢. 截止日後, 若收據未寄回就停止匯錢
        /// </summary>
        [ConfigParameter("ReceiptReceivedThresholdDay", 15, "商家收據寄回截止日", ConfigParameterAttribute.ConfigCategories.System)]
        int ReceiptReceivedThresholdDay { get; set; }

        [ConfigParameter("BusinessTax", "0.05", "營業稅", ConfigParameterAttribute.ConfigCategories.System)]
        decimal BusinessTax { get; set; }

        [ConfigParameter("PromoMailDealsCount", 6, "通知信推薦好康檔次數", ConfigParameterAttribute.ConfigCategories.System)]
        int PromoMailDealsCount { get; set; }

        [ConfigParameter("PromoMailDealsProportion", "0.5", "通知信推薦好康檔次城市占比", ConfigParameterAttribute.ConfigCategories.System)]
        decimal PromoMailDealsProportion { get; set; }

        [ConfigParameter("ShipBatchImport", "/cache/ShipBatchImport", "商家系統出貨管理清冊匯入失敗結果檔存放目錄", ConfigParameterAttribute.ConfigCategories.System)]
        string ShipBatchImport { get; set; }

        /// <summary>
        /// 出貨日期限制最大不超過系統日期天數(填寫出貨日期檢查使用)
        /// </summary>
        [ConfigParameter("ShipTimeDayDiffLimit", 20, "出貨日期限制最大不超過系統日期天數(填寫出貨日期檢查使用)", ConfigParameterAttribute.ConfigCategories.System)]
        int ShipTimeDayDiffLimit { get; set; }

        /// <summary>
        /// 優惠券抽獎活動預設活動內容
        /// </summary>
        [ConfigParameter("VourcherLotteryEvent", "刷台新卡，即可參加抽獎活動", "紀錄Api優惠券抽獎活動預設活動內容", ConfigParameterAttribute.ConfigCategories.System)]
        string VourcherLotteryEvent { get; set; }

        /// <summary>
        /// 需同步資料的WebServer位置
        /// </summary>
        [ConfigParameter("SyncWebServerUrl", "", "需同步資料的WebServer位置", ConfigParameterAttribute.ConfigCategories.System)]
        string[] SyncWebServerUrl { get; set; }

        /// <summary>
        /// 商家系統使用手冊檔案存放目錄
        /// </summary>
        [ConfigParameter("VendorBillingSystemManual", "/Document/Manual", "商家系統使用手冊檔案存放目錄", ConfigParameterAttribute.ConfigCategories.System)]
        string VendorBillingSystemManual { get; set; }

        /// <summary>
        /// 客服詢問按鈕-P好康
        /// </summary>
        [ConfigParameter("PponServiceButtonVisible", true, "P好康客服詢問按鈕", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool PponServiceButtonVisible { get; set; }

        [ConfigParameter("EnablePageAccessControl", true, "頁面存取控制", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnablePageAccessControl { get; set; }

        [ConfigParameter("IsImageLoadBalance", true, "圖片LoadBalance", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsImageLoadBalance { get; set; }

        [ConfigParameter("MediaCDNEnabled", false, "Media是否啟用CDN", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool MediaCDNEnabled { get; set; }

        [ConfigParameter("ImagesCDNEnabled", false, "Images是否啟用CDN", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool ImagesCDNEnabled { get; set; }

        [ConfigParameter("MASCDNAPIUrl", "https://control.griffinmas.com/api/control/user/files/", "MAS CDN API Url", ConfigParameterAttribute.ConfigCategories.System)]
        string MASCDNAPIUrl { get; set; }

        [ConfigParameter("MASCDNSlug", "17life", "MAS CDN Slug", ConfigParameterAttribute.ConfigCategories.System)]
        string MASCDNSlug { get; set; }

        [ConfigParameter("MASCDNAPIKey", "572bd7bf7eeee33539220838f0fb4426012c26ba", "MAS CDN API Key", ConfigParameterAttribute.ConfigCategories.System)]
        string MASCDNAPIKey { get; set; }

        [ConfigParameter("AWSAutoSync", true, "設定檔次時自動標註需要上傳雲端", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.normal)]
        bool AWSAutoSync { get; set; }

        [ConfigParameter("AWSEnabled", false, "是否啟用AWS上圖", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.normal)]
        bool AWSEnabled { get; set; }

        [ConfigParameter("AWSBucketName", "cdn.localhost.17life", "AWS CDN BucketName", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.normal)]
        string AWSBucketName { get; set; }

        [ConfigParameter("AWSAccessKey", "AKIAJJBRNJB3E5TQDTGQ", "AWS AccessKey", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.normal)]
        string AWSAccessKey { get; set; }

        [ConfigParameter("AWSSecretKey", "+tvpCen2ofOJGFtK8L835m8iCnmvPMZkeBVlZLSW", "AWS SecretKey", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.normal)]
        string AWSSecretKey { get; set; }

        //SMS簡訊是否發送
        [ConfigParameter("IsSmsSend", true, "簡訊是否發送", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsSmsSend { get; set; }

        //是否啟用帳務修改通知信
        [ConfigParameter("IsSendWarnEmail", true, "啟用帳務修改通知信", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsSendWarnEmail { get; set; }

        /// <summary>
        /// 紀錄apiLog
        /// </summary>
        [ConfigParameter("SetApiLog", false, "紀錄Api呼叫的Log", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool SetApiLog { get; set; }

        /// <summary>
        /// 開啟記錄Buy頁操作階段
        /// </summary>
        [ConfigParameter("IsSetBuyTransaction", false, "開啟記錄Buy頁操作階段", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsSetBuyTransaction { get; set; }

        /// <summary>
        /// 如功能無異常，此設定保留3個月(10/5)後可刪除
        /// </summary>
        [ConfigParameter("EnableChangeEmail", true, "會員可更改Email", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableChangeEmail { get; set; }

        [ConfigParameter("TwentyFourHours", false, "購買倒數時間24小時制", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool TwentyFourHours { get; set; }

        [ConfigParameter("MultipleMainDeal", true, "使用多主檔模式", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool MultipleMainDeal { get; set; }

        [ConfigParameter("OAuthLoginUrl", "https://localhost/m/Login", "OAuth用的導到登入頁的網址", ConfigParameterAttribute.ConfigCategories.System)]
        string OAuthLoginUrl { get; set; }

        [ConfigParameter("CacheServerMode", 1, "啟用外部Cache Serverd的模式", ConfigParameterAttribute.ConfigCategories.System)]
        int CacheServerMode { get; set; }

        [ConfigParameter("CacheItemCompress", true, "Cache的資料是否壓縮後再傳", ConfigParameterAttribute.ConfigCategories.System)]
        bool CacheItemCompress { get; set; }

        [ConfigParameter("RedisServer", "127.0.0.1:6379,allowAdmin=true,SyncTimeout=10000,abortConnect=false", "Redis服務位置", ConfigParameterAttribute.ConfigCategories.System)]
        string RedisServer { get; set; }

        [ConfigParameter("ViewPponDealManagerLogEnabled", false, "總是log ViewPponDealManager 載入資料的時間", ConfigParameterAttribute.ConfigCategories.System)]
        bool ViewPponDealManagerLogEnabled { get; set; }

        [ConfigParameter("IsPrivilegeLogged", false, "紀錄log IsInSystemFunctionPrivilege",
            ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsPrivilegeLogged { get; set; }

        #region 會員串接

        [ConfigParameter("EnableMemberLinkBind", true, "開啟會員串接的綁定與取消功能",
            ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableMemberLinkBind { get; set; }

        #endregion

        [ConfigParameter("JobRepeatCheckEnabled", true, "檢查job是否重複執行(10秒內)",
           ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool JobRepeatCheckEnabled { get; set; }

        [ConfigParameter("JobRepeatTimeoutSec", 3600, "判斷JOB是否同時間執行兩個的逾時秒數(最後執行時間若超過此秒數則視為已執行完畢)",
            ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.normal)]
        int JobRepeatTimeoutSec { get; set; }

        [ConfigParameter("DataManagerResetImmediately", false, "後臺執行暫存資料重設時，是否馬上執行?",
           ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool DataManagerResetImmediately { get; set; }

        [ConfigParameter("CkFinderBaseDir", "", "CkFinder上圖路徑", ConfigParameterAttribute.ConfigCategories.System)]
        string CkFinderBaseDir { get; set; }

        [ConfigParameter("ContactToPayeasy", "2012/3/1", "康太轉換為康迅起始日", ConfigParameterAttribute.ConfigCategories.System)]
        DateTime ContactToPayeasy { get; set; }

        [ConfigParameter("PayeasyToContact", "2014/12/1", "康迅轉換為康太起始日", ConfigParameterAttribute.ConfigCategories.System)]
        DateTime PayeasyToContact { get; set; }

        [ConfigParameter("EinvoiceMIGv312", "2015/1/1", "電子發票資料交換標準訊息建置指引MIG 3.1.2規格啟用日", ConfigParameterAttribute.ConfigCategories.System)]
        DateTime EinvoiceMIGv312 { get; set; }

        [ConfigParameter("EinvAllowanceStartDate", "2016/1/1", "訂單適用電子折讓單起始日", ConfigParameterAttribute.ConfigCategories.System)]
        DateTime EinvAllowanceStartDate { get; set; }

        [ConfigParameter("PayeasyCompanyId", "70553216", "康迅統一編號", ConfigParameterAttribute.ConfigCategories.System)]
        string PayeasyCompanyId { get; set; }

        [ConfigParameter("PayeasyTravelCompanyId", "27734293", "康迅旅遊統一編號", ConfigParameterAttribute.ConfigCategories.System)]
        string PayeasyTravelCompanyId { get; set; }

        [ConfigParameter("ContactCompanyId", "24317014", "康太統一編號", ConfigParameterAttribute.ConfigCategories.System)]
        string ContactCompanyId { get; set; }

        [ConfigParameter("StarRatingEnabled", false, "是否啟用評等顯示功能", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool StarRatingEnabled { get; set; }

        [ConfigParameter("GoogleProductFeedSet", PponDealType.All, "Google Product Feed設定", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        PponDealType GoogleProductFeedSet { get; set; }

        [ConfigParameter("FBProductFeedSet", PponDealType.All, "Facebook Product Feed設定", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        PponDealType FBProductFeedSet { get; set; }

        /// <summary>
        /// 是否啟用新分享邀請連結
        /// </summary>
        [ConfigParameter("NewShareLinkEnabled", false, "是否啟用新分享邀請連結", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool NewShareLinkEnabled { get; set; }

        /// <summary>
        /// 預設管理者USER ID
        /// </summary>
        [ConfigParameter("SystemUserId", 1111355236, "預設管理者USER ID", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        int SystemUserId { get; set; }

        [ConfigParameter("SessionStateTimeOut", 1440, "未保持登入之狀態保持時限(分鐘)", ConfigParameterAttribute.ConfigCategories.System)]
        int SessionStateTimeOut { get; set; }

        /// <summary>
        /// Web17Life AccessToken
        /// </summary>
        [ConfigParameter("Web17LifeAccessToken", "705c0cb742d24d7fb85823e052124a64_cf56566a9de546bb840f9a5eae89ddda", "web17Life Access Token", ConfigParameterAttribute.ConfigCategories.System)]
        string Web17LifeAccessToken { get; set; }

        [ConfigParameter("MobileMaxWidth", 768, "手機版本的最大寬度", ConfigParameterAttribute.ConfigCategories.System)]
        int MobileMaxWidth { get; set; }

        [ConfigParameter("IsNewMobileSetting", true, "M版是否啟用開關", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsNewMobileSetting { get; set; }

        /// <summary>
        /// 產對帳單時使用 因臨時調整核銷緩衝天數 對帳單產出之緩衝天數需延後一週調整 
        /// 方能將緩衝天數調整之前進行核銷之核銷明細產出對帳單
        /// 原設計為給宅配對帳單使用 暫與憑證對帳單共用
        /// </summary>
        [ConfigParameter("GenToHouseBalanceSheetBuffer", 30, "宅配對帳單產出緩衝天數", ConfigParameterAttribute.ConfigCategories.System)]
        int GenToHouseBalanceSheetBuffer { get; set; }

        [ConfigParameter("RemovePeztempOrderGuid", false, "是否清除Peztemp中OrderGuid未成單的資料", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool RemovePeztempOrderGuid { get; set; }

        [ConfigParameter("CouponSmsEnabled", true, "憑證產生時是否發送簡訊", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool CouponSmsEnabled { get; set; }


        [ConfigParameter("ProductReturnProcessExpiredDay", 10, "宅配退貨單廠商逾期未處理天數", ConfigParameterAttribute.ConfigCategories.System)]
        int ProductReturnProcessExpiredDay { get; set; }

        [ConfigParameter("EnableProductAutoReturnProcess", false, "開啟宅配退貨單自動退貨功能", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnableProductAutoReturnProcess { get; set; }

        [ConfigParameter("ActiveMQServerUri", "failover:tcp://localhost:61616?connection.AsyncSend=true", "ActiveMQ連線字串", ConfigParameterAttribute.ConfigCategories.System)]
        string ActiveMQServerUri { get; set; }

        [ConfigParameter("RsrcDetailError", false, "Rsrc log啟用", ConfigParameterAttribute.ConfigCategories.System)]
        bool RsrcDetailError { get; set; }

        [ConfigParameter("SendVerificationNoticeEmailBeforeDays", 2, "核銷通知信於截止日幾天前發送", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        int SendVerificationNoticeEmailBeforeDays { get; set; }

        [ConfigParameter("EnableUserTrackingModule", true, "啟用UserTracking紀錄模組", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableUserTrackingModule { get; set; }

        [ConfigParameter("EnableUserTrackingMQSend", true, "啟用UserTracking紀錄模組，發送到MQ", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableUserTrackingMQSend { get; set; }

        [ConfigParameter("EnableUserTrackingLogOutput", false, "啟用UserTracking紀錄回傳資料", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableUserTrackingLogOutput { get; set; }

        #region MessageQueue相關
        [ConfigParameter("ActiveMQSSQueueName", "subSonicInsert", "ActiveMQ SubSonic Insert Queue Name", ConfigParameterAttribute.ConfigCategories.System)]
        string ActiveMQSSQueueName { get; set; }

        [ConfigParameter("MQ2AWSQueueName", "userTracking", "MQ2AWS  Insert Queue Name", ConfigParameterAttribute.ConfigCategories.System)]
        string MQ2AWSQueueName { get; set; }

        [ConfigParameter("ActiveMQEnable", false, "Enable ActiveMQ", ConfigParameterAttribute.ConfigCategories.System)]
        bool ActiveMQEnable { get; set; }

        [ConfigParameter("OopsEnabled", false, "Oops Enabled", ConfigParameterAttribute.ConfigCategories.System)]
        bool OopsEnabled { get; set; }
        [ConfigParameter("OopsHost", "oops.uni2.tw", "Oops Host", ConfigParameterAttribute.ConfigCategories.System)]
        string OopsHost { get; set; }
        [ConfigParameter("OopsTopic", "Dev", "Oops Topic", ConfigParameterAttribute.ConfigCategories.System)]
        string OopsTopic { get; set; }

        #endregion

        [ConfigParameter("ActiveMQSendTimeoutMillSeconds", 1000, "ActiveMQ發送訊息的timeut毫秒數", ConfigParameterAttribute.ConfigCategories.System)]
        int ActiveMQSendTimeoutMillSeconds { get; set; }

        [ConfigParameter("VisitorIdentityCookieExpires", 90, "VisitorIdentity Cookie的逾時天數", ConfigParameterAttribute.ConfigCategories.System)]
        int VisitorIdentityCookieExpires { get; set; }

        //[ConfigParameter("ServerIPs", "{\"CAPTAIN-AMERICA\":\"192.168.170.2\",\"CYCLOPS\":\"192.168.170.4\",\"PROFESSORX\":\"192.168.170.1\",\"WEB_HV01\":\"192.168.170.5\",\"WEB_HV02\":\"192.168.170.3\"}", 
        //    "伺服器的內部IP位址", ConfigParameterAttribute.ConfigCategories.System)]
        [ConfigParameter("ServerIPs", "{\"Localhost\":\"127.0.0.1\"}",
            "伺服器的內部IP位址", ConfigParameterAttribute.ConfigCategories.System)]
        string ServerIPs { get; set; }

        [ConfigParameter("DefenseModuleEnabledLevel", "0", "Defense Module 的開關設定 0:關 1:防止一直試登入 2:防止短時間多次連線", ConfigParameterAttribute.ConfigCategories.System)]
        int DefenseModuleEnabledLevel { get; set; }

        [ConfigParameter("DefenseModuleShareBlackList", false, "DefenseModule伺服器共享黑名單", ConfigParameterAttribute.ConfigCategories.System)]
        bool DefenseModuleShareBlackList { get; set; }

        [ConfigParameter("CheckDiscountStringNewOldVersionsMatch", false, "檢查新舊版本的DiscountCode差異程度", ConfigParameterAttribute.ConfigCategories.System)]
        bool CheckDiscountStringNewOldVersionsMatch { get; set; }

        [ConfigParameter("SwitchableHttpsEnabled", false, "SwitchableHttps屬性的開關", ConfigParameterAttribute.ConfigCategories.System)]
        bool SwitchableHttpsEnabled { get; set; }
        [ConfigParameter("CssAndJsVersion", "20200117.1",
            "CSS/Javascript的後綴版號", ConfigParameterAttribute.ConfigCategories.System)]
        string CssAndJsVersion { get; set; }

        #endregion System Configuration

        #region Email Configuration

        #region 客服系統pio 

        /// <summary>
        /// 是否開啟轉單寄信
        /// </summary>
        [ConfigParameter("EnableTransferMailNotify", false, "是否開啟轉單寄信", ConfigParameterAttribute.ConfigCategories.Email)]
        bool EnableTransferMailNotify { get; set; }
        /// <summary>
        /// 17Life客服案件信箱
        /// </summary>
        [ConfigParameter("CsTransferEmailAddress", "17lifecs@17life.com", "17Life客服案件信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string CsTransferEmailAddress { get; set; }
        /// <summary>
        /// 17Life客服案件寄件人
        /// </summary>
        [ConfigParameter("CsTransferEmailDisplayName", "17Life客服案件", "17Life客服案件寄件人", ConfigParameterAttribute.ConfigCategories.Email)]
        string CsTransferEmailDisplayName { get; set; }
        /// <summary>
        /// 客服主管信箱
        /// </summary>
        [ConfigParameter("CsMasterEmailAddress", "sys.cs_mg@17life.com", "客服主管信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string CsMasterEmailAddress { get; set; }

        /// <summary>
        /// 是否開啟通知廠商出貨信
        /// </summary>
        [ConfigParameter("EnableNewOrderShipMailNotify", false, "是否開啟通知廠商出貨信", ConfigParameterAttribute.ConfigCategories.Email)]
        bool EnableNewOrderShipMailNotify { get; set; }
        /// <summary>
        /// 17Life訂單資訊信箱
        /// </summary>
        [ConfigParameter("NewOrderShipEmailAddress", "order@17life.com", "17Life訂單資訊信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string NewOrderShipEmailAddress { get; set; }
        /// <summary>
        /// 17Life訂單資訊寄件人
        /// </summary>
        [ConfigParameter("NewOrderShipEmailDisplayName", "17Life訂單資訊", "17Life訂單資訊寄件人", ConfigParameterAttribute.ConfigCategories.Email)]
        string NewOrderShipEmailDisplayName { get; set; }

        /// <summary>
        /// 是否開啟通知廠商退換貨信
        /// </summary>
        [ConfigParameter("EnableNewReturnOrExchangeMailNotify", false, "是否開啟通知廠商退換貨訂單信", ConfigParameterAttribute.ConfigCategories.Email)]
        bool EnableNewReturnOrExchangeMailNotify { get; set; }
        /// <summary>
        /// 17Life退換貨訂單信箱
        /// </summary>
        [ConfigParameter("NewReturnOrExchangepEmailAddress", "refund@17life.com", "17Life退換貨訂單信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string NewReturnOrExchangepEmailAddress { get; set; }
        /// <summary>
        /// 17Life退換貨訂單寄件人
        /// </summary>
        [ConfigParameter("NewReturnOrExchangeEmailDisplayName", "17Life退換貨訂單", "17Life退換貨訂單寄件人", ConfigParameterAttribute.ConfigCategories.Email)]
        string NewReturnOrExchangeEmailDisplayName { get; set; }

        /// <summary>
        /// 轉單多少小時已讀未回要通知客服
        /// </summary>
        [ConfigParameter("TransferNoReplyWithinHours", 24, "轉單多少小時已讀未回要通知客服", ConfigParameterAttribute.ConfigCategories.Email)]
        int TransferNoReplyWithinHours { get; set; }

        #endregion
        /// <summary>
        /// 系統信箱
        /// </summary>
        [ConfigParameter("SystemEmail", "sys@17life.com", "系統信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string SystemEmail { get; set; }

        /// <summary>
        /// 網管小組信箱
        /// </summary>
        [ConfigParameter("AdminEmail", "admin@17life.com", "網管小組信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string AdminEmail { get; set; }

        /// <summary>
        /// 技術測試信箱 (寄發請用此信箱測試, 在小config蓋掉, 測完後請刪除)
        /// </summary>
        [ConfigParameter("ItTesterEmail", "sys.it_tester@17life.com", "技術測試信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string ItTesterEmail { get; set; }

        /// <summary>
        /// 技術平台應用部信箱
        /// </summary>
        [ConfigParameter("ItPAD", "pad@17life.com", "技術平台應用部信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string ItPAD { get; set; }

        /// <summary>
        /// 信託報表信箱
        /// </summary>
        [ConfigParameter("TrustReportEmail", "trust_report@17life.com", "信託報表信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string TrustReportEmail { get; set; }

        /// <summary>
        /// 每期媒體檔信箱
        /// </summary>
        [ConfigParameter("EinvoiceMediaEmail", "einvoice_media@17life.com", "每期媒體檔信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string EinvoiceMediaEmail { get; set; }

        /// <summary>
        /// 客服信箱
        /// </summary>
        [ConfigParameter("ServiceEmail", "service@17life.com", "客服信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string ServiceEmail { get; set; }

        /// <summary>
        /// 客服一線
        /// </summary>
        [ConfigParameter("CsdEmail", "csd@17life.com", "客服一線", ConfigParameterAttribute.ConfigCategories.Email)]
        string CsdEmail { get; set; }
        /// <summary>
        /// 客服二線
        /// </summary>
        [ConfigParameter("Cs02Email", "csd_02@17life.com", "客服二線", ConfigParameterAttribute.ConfigCategories.Email)]
        string Cs02Email { get; set; }

        /// <summary>
        /// 客服廠商退換貨處理信箱
        /// </summary>
        [ConfigParameter("CsReturnEmail", "17life_service@17life.com", "客服廠商退換貨處理信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string CsReturnEmail { get; set; }

        /// <summary>
        /// 企服宅配管理信箱
        /// </summary>
        [ConfigParameter("CodShipEmail", "oad.cod_ship@17life.com", "營運分析部-宅配企服群組信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string CodShipEmail { get; set; }

        /// <summary>
        /// 客服主管信箱
        /// </summary>
        [ConfigParameter("CsManagerEmail", "sys.cs_mg@17life.com", "客服主管信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string CsManagerEmail { get; set; }

        /// <summary>
        /// 客服信件歸檔員信箱
        /// </summary>
        [ConfigParameter("CsMailDealerEmail", "sys.cs_maildealer@17life.com", "客服信件歸檔員信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string CsMailDealerEmail { get; set; }

        /// <summary>
        /// 企劃小組信箱
        /// </summary>
        [ConfigParameter("PlanningEmail", "planning@17life.com", "企劃小組信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string PlanningEmail { get; set; }

        /// <summary>
        /// 財會信箱
        /// </summary>
        [ConfigParameter("FinanceEmail", "finance@17life.com", "財會信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string FinanceEmail { get; set; }

        /// <summary>
        /// 財會帳務管理信箱
        /// </summary>
        [ConfigParameter("FinanceAccountsEmail", "sys.finance_accounts@17life.com", "財會帳務管理信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string FinanceAccountsEmail { get; set; }

        /// <summary>
        /// 午餐王廠商聯絡信箱
        /// </summary>
        [ConfigParameter("BizEmail", "biz@lunchking.com", "午餐王廠商聯絡信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string BizEmail { get; set; }

        /// <summary>
        /// P好康廠商聯絡信箱
        /// </summary>
        [ConfigParameter("PponEmail", "pponbiz@17life.com", "P好康廠商聯絡信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string PponEmail { get; set; }

        /// <summary>
        /// P玩美廠商聯絡信箱
        /// </summary>
        [ConfigParameter("PeautyEmail", "peautybiz@17life.com", "P玩美廠商聯絡信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string PeautyEmail { get; set; }

        /// <summary>
        /// 品生活客服信箱
        /// </summary>
        [ConfigParameter("PiinlifeServiceEmail", "piinlife_noreply@17life.com", "品生活客服信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string PiinlifeServiceEmail { get; set; }

        /// <summary>
        /// P好康客服信箱
        /// </summary>
        [ConfigParameter("PponServiceEmail", "noreply@17life.com", "P好康客服信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string PponServiceEmail { get; set; }

        /// <summary>
        /// 兌換截止日更改通知信箱
        /// </summary>
        [ConfigParameter("ExpirationChangeEmail", "expiration_change_notice@17life.com", "兌換截止日更改通知信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string ExpirationChangeEmail { get; set; }

        /// <summary>
        /// P好康EDM信箱
        /// </summary>
        [ConfigParameter("EdmEmail", "Hi@edm.17life.com.tw", "P好康EDM信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string EdmEmail { get; set; }

        /// <summary>
        /// 品生活EDM信箱
        /// </summary>
        [ConfigParameter("PiinlifeEdmEmail", "hi@edm.piinlife.com", "品生活EDM信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string PiinlifeEdmEmail { get; set; }

        /// <summary>
        /// 資策會購買使用的會員帳號
        /// </summary>
        [ConfigParameter("IDEASUserEmail", "ideas@17life.com.tw", "", ConfigParameterAttribute.ConfigCategories.Email)]
        string IDEASUserEmail { get; set; }

        /// <summary>
        /// 預約系統設定變更通知信箱
        /// </summary>
        [ConfigParameter("BookingSystemChangeNoticeEmail", "planning@17life.com", "預約系統設定變更通知信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string BookingSystemChangeNoticeEmail { get; set; }

        /// <summary>
        /// 招商專區收件者信箱
        /// </summary>
        [ConfigParameter("SolicitBusinessEmail", "sinco_chang@17life.com;norah_chang@17life.com;maggie_lai@17life.com", "商家提案_實體商品收件者信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string SolicitBusinessEmail { get; set; }

        /// <summary>
        /// 招商專區收件者信箱
        /// </summary>
        [ConfigParameter("SolicitMarketEmail", "yihan_liu@17life.com", "商家提案_非實體商品收件者信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string SolicitMarketEmail { get; set; }

        /// <summary>
        /// 招商專區收件者信箱
        /// </summary>
        [ConfigParameter("SolicitKindEmail", "erin_chen@17life.com", "商家提案_行銷與公益合作收件者信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string SolicitKindEmail { get; set; }

        /// <summary>
        /// 業務長信箱
        /// </summary>
        [ConfigParameter("SalesManagerEmail", "cso@17life.com", "業務長信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string SalesManagerEmail { get; set; }

        /// <summary>
        /// 業務助理信箱
        /// </summary>
        [ConfigParameter("SalesAssistantEmail", "shipping@17life.com", "業務助理信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string SalesAssistantEmail { get; set; }

        /// <summary>
        /// 業務特助信箱
        /// </summary>
        [ConfigParameter("SalesSpecialAssistantEmail", "Sales.ass@17life.com", "業務特助信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string SalesSpecialAssistantEmail { get; set; }

        /// <summary>
        /// 全國宅配業務特助群組信箱
        /// </summary>
        [ConfigParameter("SalesSpecialAssistantShippingEmail", "sales.ass_shipping@17life.com", "全國宅配業務特助群組信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string SalesSpecialAssistantShippingEmail { get; set; }

        /// <summary>
        /// 企業服務部信箱
        /// </summary>
        [ConfigParameter("CodEmail", "cod@17life.com", "企業服務部信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string CodEmail { get; set; }

        /// <summary>
        /// 客群服務處-企業服務部(好康)
        /// </summary>
        [ConfigParameter("CodLocalEmail", "cod_local@17life.com", "客群服務處-企業服務部(好康)", ConfigParameterAttribute.ConfigCategories.Email)]
        string CodLocalEmail { get; set; }

        /// <summary>
        /// 在地旅助群組
        /// </summary>
        [ConfigParameter("LocalEmail", "tps.sa@17life.com", "客群服務處-在地旅助群組", ConfigParameterAttribute.ConfigCategories.Email)]
        string LocalEmail { get; set; }

        /// <summary>
        /// 旅遊業助群組
        /// </summary>
        [ConfigParameter("TravelEmail", "lid.sa@17life.com", "客群服務處-旅遊業助群組", ConfigParameterAttribute.ConfigCategories.Email)]
        string TravelEmail { get; set; }

        /// <summary>
        /// 紙本折讓下載通知客服聯絡窗口
        /// </summary>
        [ConfigParameter("PaperAllowanceSerivceEmail", "tingwei_chu@17life.com", "紙本折讓下載通知客服聯絡窗口(分號分格)", ConfigParameterAttribute.ConfigCategories.Email)]
        string PaperAllowanceSerivceEmail { get; set; }

        /// <summary>
        /// 紙本折讓下載通知客服CC聯絡窗口
        /// </summary>
        [ConfigParameter("PaperAllowanceSerivceCCEmail", "lynne_chung@17life.com;angel_chen@17life.com", "紙本折讓下載通知客服CC聯絡窗口(分號分格)", ConfigParameterAttribute.ConfigCategories.Email)]
        string PaperAllowanceSerivceCCEmail { get; set; }

        /// <summary>
        /// 全家專區檔次申請通知
        /// </summary>
        [ConfigParameter("FamiDealBuildEmail", "famideal@17life.com", "全家專區檔次申請通知(分號分格)", ConfigParameterAttribute.ConfigCategories.Email)]
        string FamiDealBuildEmail { get; set; }

        [ConfigParameter("DebugGuyEmail", "", "如果設定DebugGuyEmail且PostMan為SiteSmtpMailAgentFilterHost模式，信件將改到此Email", ConfigParameterAttribute.ConfigCategories.Email)]
        string DebugGuyEmail { get; set; }
        /// <summary>
        /// 創編組長信箱
        /// </summary>
        [ConfigParameter("SupportingLeaderEmail", "Sgpl@17life.com", "創編組長信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string SupportingLeaderEmail { get; set; }
        /// <summary>
        /// 宅配事業處-商品開發部信箱
        /// </summary>
        [ConfigParameter("MhdEmail", "MHD@17life.com", "宅配事業處-商品開發部信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string MhdEmail { get; set; }
        /// <summary>
        /// 營運分析部-宅配策展群組信箱
        /// </summary>
        [ConfigParameter("OadMpShipEmail", "oad.mp_shipping@17life.com", "營運分析部-宅配策展群組信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string OadMpShipEmail { get; set; }
        /// <summary>
        /// 宅配業助群組信箱
        /// </summary>
        [ConfigParameter("OadEmail", "oad@17life.com", "宅配業助群組信箱", ConfigParameterAttribute.ConfigCategories.Email)]
        string OadEmail { get; set; }

        /// <summary>
        /// 營運企劃部信箱
        /// </summary>
        [ConfigParameter("OpdEmail", "opd@17life.com", "營運企劃部", ConfigParameterAttribute.ConfigCategories.Email)]
        string OpdEmail { get; set; }

        /// <summary>
        /// 提案單約拍通知信
        /// </summary>
        [ConfigParameter("ProposalPhotographerAppointEmail", "Elaine@17life.com;sinco_chang@17life.com", "提案單約拍通知信", ConfigParameterAttribute.ConfigCategories.Email)]
        string ProposalPhotographerAppointEmail { get; set; }

        /// <summary>
        /// 提案單約拍通知信副本
        /// </summary>
        [ConfigParameter("ProposalPhotographerAppointCCEmail", "chelsea_chang@17life.com", "提案單約拍通知信副本", ConfigParameterAttribute.ConfigCategories.Email)]
        string ProposalPhotographerAppointCCEmail { get; set; }

        /// <summary>
        /// 憑證疑似盜刷通知信
        /// </summary>
        [ConfigParameter("PponCreditcardFraudEmail", "eva_liu@17life.com;mark_yeh@17life.com;tingwei_chu@17life.com", "憑證疑似盜刷通知信", ConfigParameterAttribute.ConfigCategories.Email)]
        string PponCreditcardFraudEmail { get; set; }


        [ConfigParameter("MailLogToDb", true, "Mail記錄到料庫", ConfigParameterAttribute.ConfigCategories.Email)]
        bool MailLogToDb { get; set; }

        #endregion Email Configuration

        #region Payment Configuration

        [ConfigParameter("CreditCardAPIProviderRatio", "1:0", "信用卡串接廠商服務啟用比例 (TSPG:Neweb)", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string CreditCardAPIProviderRatio { get; set; }

        [ConfigParameter("BypassPaymentProcess", false, "是否跳過結帳金流", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.option)]
        bool BypassPaymentProcess { get; set; }

        /// <summary>
        /// 是否啟用國外卡判斷
        /// </summary>
        [ConfigParameter("EnableAntiCreditcardFraud", true, "是否啟用國外卡判斷", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableAntiCreditcardFraud { get; set; }

        [ConfigParameter("TurnATMOn", true, "是否啟用 ATM", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.option)]
        bool TurnATMOn { get; set; }

        //11.02.15 new url
        [ConfigParameter("PCashAPI", "http://61.67.221.74/PCashPayment/PCashDataExchangeServlet", "PCash API網址", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string PCashAPI { get; set; }

        [ConfigParameter("CreditCardAPIProvider", "TaishinPaymnetGateway", "信用卡串接廠商", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string CreditCardAPIProvider { get; set; }

        [ConfigParameter("UnionPayBinPatterns", "62", "銀聯卡 bin 碼開頭 (請用半形逗號(,)分隔)", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string UnionPayBinPatterns { get; set; }

        [ConfigParameter("NotOkOrderQueryDays", "-3", "未成立單款項退回查詢天數設定", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NotOkOrderQueryDays { get; set; }

        [ConfigParameter("CreditcardFailLineToken", "", "即時收到刷卡失敗的通知用的LineToken", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string CreditcardFailLineToken { get; set; }

        [ConfigParameter("AutoBlockCreditcardFraudSuspect", false, "啟動自動防盜刷機制", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool AutoBlockCreditcardFraudSuspect { get; set; }

        [ConfigParameter("EnableOTP", "false", "是否開啟3D驗證OTP", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool EnableOTP { get; set; }

        [ConfigParameter("EnableOTPUser", "", "使用OTP帳號(內測用 ;區隔userid)", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string EnableOTPUser { get; set; }

        [ConfigParameter("ForceOTPDeal", "", "強制OTP的檔次(;區隔bid)", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string ForceOTPDeal { get; set; }

        [ConfigParameter("ChargingDays", "7", "幾天內的授權資料會進行請款", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        int ChargingDays { get; set; }

        [ConfigParameter("UsingPreventingOversellProcedure",  false, "零元檔預防超賣使用預存程序", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool UsingPreventingOversellProcedure { get; set; }

        #region NCCC

        [ConfigParameter("MerchantID", "0100817912", "串接NCCC商店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string MerchantID { get; set; }

        [ConfigParameter("TerminalID", "70505543", "串接NCCC終端代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string TerminalID { get; set; }

        [ConfigParameter("CardPath", "/cache/CreditCard", "信用卡LOG檔位址", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string CardPath { get; set; }

        [ConfigParameter("NcccRequestUrl", "https://nccnet-ec.nccc.com.tw/merchant/HPPRequest", "串接NCCC交易位址", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NcccRequestUrl { get; set; }

        [ConfigParameter("NcccFileName", "NCCC", "NCCC信用卡資料檔名", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NcccFileName { get; set; }

        #endregion NCCC

        #region HiTrust

        [ConfigParameter("HiTrustMerchantID", "50825", "網際威信好康用特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string HiTrustMerchantID { get; set; }

        [ConfigParameter("HiTrustPiinLifeMerchantID", "50826", "網際威信品生活用特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string HiTrustPiinLifeMerchantID { get; set; }

        [ConfigParameter("HiTrustPayeasyTravelMerchantID", "50827", "網際威信旅遊檔用特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string HiTrustPayeasyTravelMerchantID { get; set; }

        [ConfigParameter("HiTrustUnionPayMerchantID", "51172", "網際威信好康用銀聯卡特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string HiTrustUnionPayMerchantID { get; set; }

        [ConfigParameter("HiTrustPiinLifeUnionPayMerchantID", "51173", "網際威信品生活用銀聯卡特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string HiTrustPiinLifeUnionPayMerchantID { get; set; }

        [ConfigParameter("HiTrustContactWithCVV2MerchantID", "51422", "網際威信康太用驗證後三碼特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string HiTrustContactWithCVV2MerchantID { get; set; }

        [ConfigParameter("HiTrustContactWithoutCVV2MerchantID", "51423", "網際威信康太用不驗證後三碼特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string HiTrustContactWithoutCVV2MerchantID { get; set; }

        [ConfigParameter("HiTrustTestMerchantID", "60859", "網際威信測試用特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string HiTrustTestMerchantID { get; set; }

        [ConfigParameter("HiTrustConfigPath", @"C:\source\web_main\HiTrust\", "網際威信設定檔資料夾", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string HiTrustConfigPath { get; set; }

        [ConfigParameter("HiTrustBatchFtpUri", "ftp://unicorn:123456@192.168.1.110", "網際威信批次請款用的FTP網址", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string HiTrustBatchFtpUri { get; set; }

        #endregion HiTrust

        #region Neweb

        [ConfigParameter("NewebMerchantID", "759851", "藍新好康用特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebMerchantID { get; set; }

        [ConfigParameter("NewebUserID", "M_17life123", "藍新商店帳號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebUserID { get; set; }

        [ConfigParameter("NewebUserPassword", "hpz39jju", "藍新商店密碼", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebUserPassword { get; set; }

        [ConfigParameter("NewebPiinLifeMerchantID", "759942", "藍新品生活用特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebPiinLifeMerchantID { get; set; }

        [ConfigParameter("NewebContactMerchantID", "760512", "藍新康太用特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebContactMerchantID { get; set; }

        [ConfigParameter("NewebContactPiinLifeMerchantID", "760513", "藍新康太品生活用特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebContactPiinLifeMerchantID { get; set; }

        [ConfigParameter("NewebContactInstallmentMerchantID", "715431", "藍新康太分期付款特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebContactInstallmentMerchantID { get; set; }

        [ConfigParameter("NewebContactUserID", "M_17life", "藍新康太用商店帳號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebContactUserID { get; set; }

        [ConfigParameter("NewebContactUserPassword", "v8ukyfm3", "藍新康太用商店密碼", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebContactUserPassword { get; set; }

        [ConfigParameter("NewebAcceptServer", "https://pound.neweb.com.tw/NewebPayment/CCAccept", "藍新授權 API URL", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebAcceptServer { get; set; }

        [ConfigParameter("NewebAdminServer", "https://pound.neweb.com.tw/NewebPayment/CCAdmin", "藍新管理 API URL", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebAdminServer { get; set; }

        [ConfigParameter("NewebTestMerchantID", "759851", "藍新測試用特店代號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebTestMerchantID { get; set; }

        [ConfigParameter("NewebTestUserID", "M_test", "藍新測試商店帳號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebTestUserID { get; set; }

        [ConfigParameter("NewebTestUserPassword", "test123", "藍新測試商店密碼", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebTestUserPassword { get; set; }

        [ConfigParameter("NewebTestAcceptServer", "https://testmaple2.neweb.com.tw/NewebPayment2/CCAccept", "藍新測試授權 API URL", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebTestAcceptServer { get; set; }

        [ConfigParameter("NewebTestAdminServer", "https://testmaple2.neweb.com.tw/NewebPayment2/CCAdmin", "藍新測試管理 API URL", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string NewebTestAdminServer { get; set; }

        #endregion Neweb

        #region ATM

        [ConfigParameter("AtmTestPostIp", "192.168.1.66", "ATM測試購買IP", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string AtmTestPostIp { get; set; }

        /// <summary>
        /// Atm匯款帳號的銀行名稱
        /// </summary>
        [ConfigParameter("AtmAccountBankName", "中國信託商業銀行", "Atm匯款帳號的銀行名稱", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string AtmAccountBankName { get; set; }

        /// <summary>
        /// Atm匯款帳號的銀行編號
        /// </summary>
        [ConfigParameter("AtmAccountBankCode", "822", "Atm匯款帳號的銀行編號", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string AtmAccountBankCode { get; set; }

        [ConfigParameter("AtmRemitDeadline", 5, "ATM匯款期限", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        int AtmRemitDeadline { get; set; }

        /// <summary>
        /// 中國信託ATM公司代碼
        /// "95948"-測試站用
        /// "49343"-康迅
        /// "95931"-康太
        /// </summary>
        [ConfigParameter("ChinaTrustAtmId", "95931", "中國信託ATM公司代碼", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string ChinaTrustAtmId { get; set; }

        #endregion ATM

        [ConfigParameter("PponDescriptionTestEnabled", false, "PponDescriptionTestEnabled", ConfigParameterAttribute.ConfigCategories.System)]
        bool PponDescriptionTestEnabled { get; set; }

        /// <summary>
        /// 啟用App檔次內頁新版本
        /// </summary>
        [ConfigParameter("EnableNewAppPponDescriptionVersion", false, "啟用App檔次內頁新版本", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnableNewAppPponDescriptionVersion { get; set; }

        [ConfigParameter("CreditcardReferAmount", 5000, "信用卡照會金額下限", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        int CreditcardReferAmount { get; set; }

        [ConfigParameter("EnableCreditcardRefer", false, "是否開啟照會", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool EnableCreditcardRefer { get; set; }

        [ConfigParameter("PayWayLimitPaymentType", "10", "啟用付款限制的PaymentType，逗號分隔", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string PayWayLimitPaymentType { get; set; }

        [ConfigParameter("PayWayLimitExpiresMinute", "20", "付款限制的過期時效(分鐘)，超過即解除限制", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        int PayWayLimitExpiresMinute { get; set; }

        [ConfigParameter("PayWayLimitEnable", false, "啟用付款限制", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool PayWayLimitEnable { get; set; }

        [ConfigParameter("TaiShinPaymentBankAndBranchNo", "8120687", "台新網銀付款銀行分行代碼，共7碼", ConfigParameterAttribute.ConfigCategories.Payment)]
        string TaiShinPaymentBankAndBranchNo { get; set; }

        [ConfigParameter("EnabledTaiShinPaymnetGateway", false, "啟用TSPG", ConfigParameterAttribute.ConfigCategories.Payment)]
        bool EnabledTaiShinPaymnetGateway { get; set; }

        [ConfigParameter("TaiShinPaymnetGatewayApi", "https://tspg-t.taishinbank.com.tw/tspgapi/restapi/", "TSPG API", ConfigParameterAttribute.ConfigCategories.Payment)]
        string TaiShinPaymnetGatewayApi { get; set; }

        [ConfigParameter("TaiShinMerchantID", "999812666555140", "台新特店代號", ConfigParameterAttribute.ConfigCategories.Payment)]
        string TaiShinMerchantID { get; set; }

        [ConfigParameter("TaiShinMallMerchantID", "000812770010704", "台新商城特店代號", ConfigParameterAttribute.ConfigCategories.Payment)]
        string TaiShinMallMerchantID { get; set; }

        [ConfigParameter("TaiShinMallToken", "fe01022eaad045abb16064757b582083_4b4945ea4dda7752cca84374064c60da", "台新商城Token", ConfigParameterAttribute.ConfigCategories.Payment)]
        string TaiShinMallToken { get; set; }

        [ConfigParameter("TaiShinTerminalID", "T0000000", "台新端末代號", ConfigParameterAttribute.ConfigCategories.Payment)]
        string TaiShinTerminalID { get; set; }

        [ConfigParameter("TaiShin3DTerminalID", "T0000000", "台新端末代號(3D)", ConfigParameterAttribute.ConfigCategories.Payment)]
        string TaiShin3DTerminalID { get; set; }

        [ConfigParameter("EnabledApplePay", false, "開啟Apple Pay支付", ConfigParameterAttribute.ConfigCategories.Payment)]
        bool EnabledApplePay { get; set; }

        [ConfigParameter("IsApplePayEMVManualProcess", true, "銀聯Apple Pay請退款是否人工執行", ConfigParameterAttribute.ConfigCategories.Payment)]
        bool IsApplePayEMVManualProcess { get; set; }

        [ConfigParameter("ApplePaySessionVersion", 2, "Session版本", ConfigParameterAttribute.ConfigCategories.Payment)]
        int ApplePaySessionVersion { get; set; }

        [ConfigParameter("ApplePayMerchantCerThumbprint", "67392c65113bbbf3bc907d12c1cbadae10a788c7", "apple pay 商家識別憑證指紋", ConfigParameterAttribute.ConfigCategories.Payment)]
        string ApplePayMerchantCerThumbprint { get; set; }

        [ConfigParameter("ApplePayPaymentProcessingThumbprint", "0941a427aa98d7f027203cb4c8bafdc9bc201a09", "apple pay 付款處理憑證指紋", ConfigParameterAttribute.ConfigCategories.Payment)]
        string ApplePayPaymentProcessingThumbprint { get; set; }

        [ConfigParameter("ApplePayPrivateKey", "MHcCAQEEILXR3ucU83w9Wy3qwQ9WvLkeOkE4gCtCkAoVG7HWRGsEoAoGCCqGSM49AwEHoUQDQgAEU8ATSH9YQiYTDCynv4rqkDLt2teq5FglgCZB2qpiNQXx3U9U02r7b8uQlCWgXIWpc85UAN+AopvpFH2ivC+cpg==", "apple pay 金鑰", ConfigParameterAttribute.ConfigCategories.Payment)]
        string ApplePayPrivateKey { get; set; }

        [ConfigParameter("PreventingOversellTimeoutSec", 600, "預防超賣超時資料條件(排除超過此時間的未完成交易數量)", ConfigParameterAttribute.ConfigCategories.Payment)]
        int PreventingOversellTimeoutSec { get; set; }

        [ConfigParameter("PaymentChargeAndRefundUseCartVersion", true, "使用支援購物車的請退款功能", ConfigParameterAttribute.ConfigCategories.Payment)]
        bool PaymentChargeAndRefundUseCartVersion { get; set; }

        [ConfigParameter("PaymentChargeAndRefundDebugMode", false, "請退款測試模式，不進行請退與寫回結果", ConfigParameterAttribute.ConfigCategories.Payment)]
        bool PaymentChargeAndRefundDebugMode { get; set; }

        [ConfigParameter("LogPaymentDTO", true, "側錄PaymentDTO", ConfigParameterAttribute.ConfigCategories.Payment)]
        bool LogPaymentDTO { get; set; }

        [ConfigParameter("EnableWebUseAPIBuy", false, "EnableWebUseAPIBuy", ConfigParameterAttribute.ConfigCategories.Payment)]
        bool EnableWebUseAPIBuy { get; set; }

        [ConfigParameter("CathayMerchantID", "010990040", "國泰特店代號", ConfigParameterAttribute.ConfigCategories.Payment)]
        string CathayMerchantID { get; set; }

        [ConfigParameter("CathayCubKey", "6029f201451248da83cea68541ae2c08", "國泰一般交易CUBKEY", ConfigParameterAttribute.ConfigCategories.Payment)]
        string CathayCubKey { get; set; }

        [ConfigParameter("CathayInstallmentMerchantID", "010990049", "國泰分期特店代號", ConfigParameterAttribute.ConfigCategories.Payment)]
        string CathayInstallmentMerchantID { get; set; }

        [ConfigParameter("CathayInstallmentCubKey", "29deaf0f08aadf1504686510d4f6d34b", "國泰分期交易CUBKEY", ConfigParameterAttribute.ConfigCategories.Payment)]
        string CathayInstallmentCubKey { get; set; }

        [ConfigParameter("EnabledCathayPaymnetGateway", false, "啟用國泰PG", ConfigParameterAttribute.ConfigCategories.Payment)]
        bool EnabledCathayPaymnetGateway { get; set; }

        [ConfigParameter("CathayPaymnetGatewayApi", "https://sslpayment.uwccb.com.tw/EPOSService/", "國泰PG API", ConfigParameterAttribute.ConfigCategories.Payment)]
        string CathayPaymnetGatewayApi { get; set; }

        /// <summary>
        /// 上線後移除
        /// </summary>
        [ConfigParameter("EnabledCathayPaymnetGatewayTest", false, "信用卡走國泰PG", ConfigParameterAttribute.ConfigCategories.Payment)]
        bool EnabledCathayPaymnetGatewayTest { get; set; }

        /// <summary>
        /// 上線後移除
        /// </summary>
        [ConfigParameter("CathayPaymnetTestBid", "f58e9101-73ba-4f94-8db4-33f2cbf38ea6", "國泰PG測試檔次", ConfigParameterAttribute.ConfigCategories.Payment)]
        string CathayPaymnetTestBid { get; set; }

        #endregion Payment Configuration

        #region Ppon Configuration

        [ConfigParameter("PponOnTime", "0.0:0:0", "", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PponOnTime { get; set; }

        [ConfigParameter("PponEndTime", "0.23:59:0", "", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PponEndTime { get; set; }

        [ConfigParameter("GrossMargin", 10, "最低毛利率", ConfigParameterAttribute.ConfigCategories.Ppon)]
        decimal GrossMargin { get; set; }

        [ConfigParameter("LowQuantity", 500, "過往好康最低購買人數限制", ConfigParameterAttribute.ConfigCategories.Ppon)]
        int LowQuantity { get; set; }

        [ConfigParameter("PponDefaultPageUrl", "https://www.17life.com", "", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PponDefaultPageUrl { get; set; }

        [ConfigParameter("PiinLifeDefaultPageUrl", "https://www.17life.com/piinlife/deal.aspx", "品生活檔次頁", ConfigParameterAttribute.ConfigCategories.PiinLife)]
        string PiinLifeDefaultPageUrl { get; set; }

        /// <summary>
        /// 手機版好康主頁網址
        /// </summary>
        [ConfigParameter("PponDefaultMobilePageUrl", "https://www.17life.com/ppon/m/thedeal.aspx", "", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PponDefaultMobilePageUrl { get; set; }

        /// <summary>
        /// For SEO，每日建立sitemap檔案存放路徑!!
        /// </summary>
        [ConfigParameter("SiteMapCreatePath", @"~/share", "[P好康]建立Sitemap存放路徑", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string SiteMapCreatePath { get; set; }

        /// <summary>
        /// For SEO，每日建立sitemap檔案存放路徑!!
        /// </summary>
        [ConfigParameter("SiteMapBaseUrl", @"http://www.17life.com/share/", "指定連到sitemap的網址", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string SiteMapBaseUrl { get; set; }

        /// <summary>
        /// 啟用行動版商品主題活動頁
        /// </summary>
        [ConfigParameter("EnableMobileEventPromoPage", false, "啟用行動版商品主題活動頁", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableMobileEventPromoPage { get; set; }

        /// <summary>
        /// 啟用策展頻道頁
        /// </summary>
        [ConfigParameter("EnableThemeCurationChannel", true, "啟用策展頻道頁", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableThemeCurationChannel { get; set; }

        /// <summary>
        /// 啟用獨立策展子分類頁面功能
        /// </summary>
        [ConfigParameter("EnableNewCurationSetupPage", false, "啟用獨立策展子分類頁面功能", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableNewCurationSetupPage { get; set; }



        /// <summary>
        /// APP是否啟用策展子2.0功能
        /// </summary>
        [ConfigParameter("EnableCurationTwoInApp", true, "APP啟用策展2.0相關功能", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableCurationTwoInApp { get; set; }

        /// <summary>
        /// 啟用限制策展筆數(APP)
        /// </summary>
        [ConfigParameter("EnableCurationLimitCountOfApp", true, "啟用限制策展筆數", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableCurationLimitCountInApp { get; set; }

        /// <summary>
        /// 限制策展筆數(APP)
        /// </summary>
        [ConfigParameter("CurationCountInApp", 50, "限制策展筆數(APP)", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        int CurationCountInApp { get; set; }

        /// <summary>
        /// 特殊按鈕1
        /// </summary>
        [ConfigParameter("ParticipateBtnBid1", "", "P好康顯示特殊按鈕1 BID", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string ParticipateBtnBid1 { get; set; }

        /// <summary>
        /// 特殊按鈕2
        /// </summary>
        [ConfigParameter("ParticipateBtnBid2", "", "P好康顯示特殊按鈕2 BID", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string ParticipateBtnBid2 { get; set; }

        /// <summary>
        /// 特殊按鈕3
        /// </summary>
        [ConfigParameter("ParticipateBtnBid3", "", "P好康顯示特殊按鈕3 BID", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string ParticipateBtnBid3 { get; set; }

        [ConfigParameter("ThousandSelling", 1000, "破千熱銷，提出此設定，是為了讓測試機可以調整以做測試", ConfigParameterAttribute.ConfigCategories.Ppon)]
        int ThousandSelling { get; set; }

        /// <summary>
        /// 宅配/玩美版型取營業額最高檔次數
        /// </summary>
        [ConfigParameter("DeliveryTemplateRandomDealsCount", 6, "宅配/玩美版型取營業額最高檔次數", ConfigParameterAttribute.ConfigCategories.Ppon)]
        int DeliveryTemplateRandomDealsCount { get; set; }

        /// <summary>
        /// 毛利率限制啟用
        /// </summary>
        [ConfigParameter("EnableGrossMarginRestrictions", true, "毛利率限制啟用", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableGrossMarginRestrictions { get; set; }


        /// <summary>
        /// 新版Cpa啟
        /// </summary>
        [ConfigParameter("NewCpaEnable", false, "新版Cpa啟用", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool NewCpaEnable { get; set; }

        /// <summary>
        /// 是否啟用新版宅配/玩美版型
        /// </summary>
        [ConfigParameter("IsEnabledNewDeliveryTemplate", false, "是否啟用新版宅配/玩美版型", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsEnabledNewDeliveryTemplate { get; set; }

        [ConfigParameter("IsUsePponTinyUrl", false, "P好康首頁是否要轉址為短網址", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsUsePponTinyUrl { get; set; }

        //是否使用Buy付款
        [ConfigParameter("IsPiinlifeBuy", false, "品生活 使用Buy付款", ConfigParameterAttribute.ConfigCategories.PiinLife, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsPiinlifeBuy { get; set; }

        /// <summary>
        /// 判斷是否首頁的馬上看是另開視窗
        /// </summary>
        [ConfigParameter("DefaultPageOpenNewWindow", false, "首頁好康列表另開視窗切換", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool DefaultPageOpenNewWindow { get; set; }

        [ConfigParameter("GoogleMapInDefaultPage", true, "前臺地圖顯示使用googl map", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool GoogleMapInDefaultPage { get; set; }

        /// <summary>
        /// 是否啟用全家三段式條碼取代原Pincode顯示
        /// </summary>
        [ConfigParameter("EnableFami3Barcode", false, "啟用全家三段式條碼取代原Pincode顯示", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableFami3Barcode { get; set; }

        /// <summary>
        /// 檔次頁下方其他好康團購顯示數量
        /// </summary>
        [ConfigParameter("RelatedDealCount", 10, "檔次頁下方其他好康團購顯示數量", ConfigParameterAttribute.ConfigCategories.Ppon)]
        int RelatedDealCount { get; set; }

        /// <summary>
        /// TodayDeals allow ip
        /// </summary>
        [ConfigParameter("TodayDealsAllowIp", "23.21.211.173", "Today Deals Allow IP", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string TodayDealsAllowIp { get; set; }

        /// <summary>
        /// 行銷Banner Cache保留時間
        /// </summary>
        [ConfigParameter("RandomParagraphCacheMinuteSet", 60, "行銷Banner Cache保留時間", ConfigParameterAttribute.ConfigCategories.Ppon)]
        int RandomParagraphCacheMinuteSet { get; set; }

        /// <summary>
        /// 是否啟用退貨按鈕
        /// </summary>
        [ConfigParameter("IsReturnsBtnEnabled", false, "啟用退貨按鈕", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsReturnsBtnEnabled { get; set; }

        [ConfigParameter("FamiDealDeliverTimeAlertDay", 3, "全家檔次到期通知天數(天前)", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        int FamiDealDeliverTimeAlertDay { get; set; }

        [ConfigParameter("EventPromoBindCategoryList", "86", "商品主題活動頁團購可綁定Id，以 , 分隔", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        string EventPromoBindCategoryList { get; set; }

        /// <summary>
        /// 全家POS直刷手機barcode服務代號
        /// </summary>
        [ConfigParameter("FamiPosMMKId", 154, "全家服務代號(手機直秀barcode)", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        string FamiPosMMKId { get; set; }

        /// <summary>
        /// 全家famiport列印小白單服務代號
        /// </summary>
        [ConfigParameter("FamiPortMMKId", 153, "全家服務代號(famiport)", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        string FamiportMMKId { get; set; }

        /// <summary>
        /// 全家核銷後發送折價券Log
        /// </summary>
        [ConfigParameter("FamiLogSendDiscountEvent", false, "全家核銷後發送折價券Log", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        bool FamiLogSendDiscountEvent { get; set; }

        /// <summary>
        /// 全家核銷後是否發送折價券
        /// </summary>
        [ConfigParameter("FamiSendDiscountCodeAtVerified", false, "全家核銷後是否發送折價券", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        bool FamiSendDiscountCodeAtVerified { get; set; }

        /// <summary>
        /// 全家核銷後發送折價券ID
        /// </summary>
        [ConfigParameter("FamiVerifiedDiscountCampaignId", 0, "全家核銷後折價券活動Id", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        int FamiVerifiedDiscountCampaignId { get; set; }

        /// <summary>
        /// 全家核銷後發送折價券活動起始日期
        /// </summary>
        [ConfigParameter("FamiVerifiedDiscountStartDate", "2017/11/1 00:00:00", "全家核銷後發送折價券活動起始日期", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        DateTime FamiVerifiedDiscountStartDate { get; set; }

        /// <summary>
        /// 全家核銷後發送折價券活動起始日期
        /// </summary>
        [ConfigParameter("FamiVerifiedDiscountEndDate", "2017/11/14 23:59:59", "全家核銷後發送折價券活動結束日期", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        DateTime FamiVerifiedDiscountEndDate { get; set; }

        /// <summary>
        /// 全家累積核銷數
        /// </summary>
        [ConfigParameter("FamiAccumulatedVerifiedCount", 3, "全家累積核銷數", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        int FamiAccumulatedVerifiedCount { get; set; }

        /// <summary>
        /// 全家累積核銷後發送折價券ID
        /// </summary>
        [ConfigParameter("FamiAccumulatedVerifiedDiscountCampaignId", 0, "全家累積核銷後折價券活動Id", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        int FamiAccumulatedVerifiedDiscountCampaignId { get; set; }

        /// <summary>
        /// 全家累積核銷後發送折價券活動起始日期
        /// </summary>
        [ConfigParameter("FamiAccumulatedVerifiedDiscountStartDate", "2017/11/1 00:00:00", "全家累積核銷後發送折價券活動起始日期", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        DateTime FamiAccumulatedVerifiedDiscountStartDate { get; set; }

        /// <summary>
        /// 全家累積核銷後發送折價券活動起始日期
        /// </summary>
        [ConfigParameter("FamiAccumulatedVerifiedDiscountEndDate", "2017/11/14 23:59:59", "全家累積核銷後發送折價券活動結束日期", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        DateTime FamiAccumulatedVerifiedDiscountEndDate { get; set; }

        #region App Links

        [ConfigParameter("AppLinksiOSId", "543439591", "App Links iOS Id", ConfigParameterAttribute.ConfigCategories.None, ConfigParameterAttribute.ConfigTypes.normal)]
        string AppLinksiOSId { get; set; }

        [ConfigParameter("AppLinksiOSName", "17Life", "App Links iOS Name", ConfigParameterAttribute.ConfigCategories.None, ConfigParameterAttribute.ConfigTypes.normal)]
        string AppLinksiOSName { get; set; }

        [ConfigParameter("AppLinksAndroidPackage", "com.uranus.e7plife", "App Links Android Package", ConfigParameterAttribute.ConfigCategories.None, ConfigParameterAttribute.ConfigTypes.normal)]
        string AppLinksAndroidPackage { get; set; }

        [ConfigParameter("AppLinksAndroidAppName", "17Life", "App Links Android AppName", ConfigParameterAttribute.ConfigCategories.None, ConfigParameterAttribute.ConfigTypes.normal)]
        string AppLinksAndroidAppName { get; set; }

        #endregion App Links

        [ConfigParameter("ResponseWriteType", 0, "測試Response.Write方式", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        int ResponseWriteType { get; set; }

        [ConfigParameter("IsGroupCouponOn", false, "商品券成套販售是否上線設定", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        bool IsGroupCouponOn { get; set; }

        [ConfigParameter("EnableGroupCouponNewMakeOrderApi", false, "啟用成套商品手機購買新Api", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        bool EnableGroupCouponNewMakeOrderApi { get; set; }

        [ConfigParameter("NewGroupCoupon", "2016/10/24 00:00:00", "新成套票券上線", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        DateTime NewGroupCouponOn { get; set; }

        [ConfigParameter("EnabledGroupCouponTypeB", true, "啟用新成套票券B型", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.normal)]
        bool EnabledGroupCouponTypeB { get; set; }

        #region ZiWeiEventPromo

        [ConfigParameter("ZiWeiPromoStartDate", "2014/4/14 12:00:00", "紫薇科技網活動開始時間", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        DateTime ZiWeiPromoStartDate { get; set; }

        [ConfigParameter("ZiWeiPromoEndDate", "2014/4/28", "紫薇科技網活動開始時間", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        DateTime ZiWeiPromoEndDate { get; set; }

        [ConfigParameter("ZiWeiPromoUrl", "http://www.click108.com.tw/unit007/item00611/input.php", "紫薇科技網活動算命URL", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string ZiWeiPromoUrl { get; set; }

        #endregion ZiWeiEventPromo

        /// <summary>
        /// 是否開啟M版首頁儲存Browser寬度Session
        /// </summary>
        [ConfigParameter("IsMobileWindowSizeSession", true, "是否開啟儲存Browser寬度Session", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsMobileWindowSizeSession { get; set; }

        [ConfigParameter("EnableHotDealSetting", true, "是否開啟熱銷檔次設定功能", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableHotDealSetting { get; set; }

        /// <summary>
        /// 是否開啟DealTimeSlot整點自動排序
        /// </summary>
        [ConfigParameter("IsSortDealTimeSlotSequence", false, "是否開啟DealTimeSlot整點自動排序", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsSortDealTimeSlotSequence { get; set; }

        /// <summary>
        /// 是否記錄DealTimeSlot操作
        /// </summary>
        [ConfigParameter("IsLogOperationOfDealTimeSlot", false, "是否記錄DealTimeSlot操作", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsLogOperationOfDealTimeSlot { get; set; }

        /// <summary>
        /// DealTimeSlot顯示天數
        /// </summary>
        [ConfigParameter("DealTimeSlotDays", 5, "DealTimeSlot顯示天數", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        int DealTimeSlotDays { get; set; }

        /// <summary>
        /// 檔次自動排序-每次至DB取得子檔檔質筆數
        /// </summary>
        [ConfigParameter("TakeViewPponDealOperatingInfoCount", 1000, "檔次自動排序-每次至DB取得子檔檔質筆數", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        int TakeViewPponDealOperatingInfoCount { get; set; }

        [ConfigParameter("GuestBuyEnabled", false, "訪客購買啟用", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool GuestBuyEnabled { get; set; }

        [ConfigParameter("InstallmentPayEnabled", true, "刷卡分期的功能是否啟用", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool InstallmentPayEnabled { get; set; }

        [ConfigParameter("EnableInstallment12TimeS", "2016/5/9 12:00:00", "開啟刷卡12分期起始日", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        DateTime EnableInstallment12TimeS { get; set; }

        [ConfigParameter("EnableInstallment12TimeE", "2016/5/31 12:00:00", "開啟刷卡12分期結束日", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        DateTime EnableInstallment12TimeE { get; set; }

        [ConfigParameter("EnableInstallment12GrossMargin", 3, "刷卡12分期最低毛利率", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        decimal EnableInstallment12GrossMargin { get; set; }
        /// <summary>
        /// 是否開啟檔次分類及檔次頻道CategoryId檢查
        /// </summary>
        [ConfigParameter("EnableInspectDealCategorises", true, "是否開啟檔次分類及檔次頻道CategoryId檢查", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableInspectDealCategorises { get; set; }

        /// <summary>
        /// 區域選單是否要顯示沒有檔次的分類
        /// </summary>
        [ConfigParameter("ShowEmptyDealCategory", false, " 區域選單是否要顯示沒有檔次的分類", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool ShowEmptyDealCategory { get; set; }

        [ConfigParameter("NumberOfDealInOnePage", 200, "Default頁面一頁要顯示幾檔", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        int NumberOfDealInOnePage { get; set; }

        [ConfigParameter("CheckCreditCardEnabled", false, "信用卡格式檢查開關", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool CheckCreditCardEnabled { get; set; }

        [ConfigParameter("OrderFilterVerion", 3, "訂單Filter版本", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        int OrderFilterVerion { get; set; }

        [ConfigParameter("MDealEnabled", true, "新M版商品頁啟用", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool MDealEnabled { get; set; }

        [ConfigParameter("MDealTester", "unicorn@17life.com,stan_chang@17life.com,vita_chang@17life.com,roger_huang@17life.com,bella_hsiao@17life.com,evonne_wu@17life.com", "新M版商品頁，設定測試者帳號，多筆以逗號隔開", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string MDealTester { get; set; }

        [ConfigParameter("PersonalPushMessageEnabled", true, "啟用個人推播訊息", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool PersonalPushMessageEnabled { get; set; }

        [ConfigParameter("FacebookSuspendLogin", false, "FB登入暫時無法使用", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool FacebookSuspendLogin { get; set; }

        [ConfigParameter("LineSuspendLogin", false, "Line登入暫時無法使用", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool LineSuspendLogin { get; set; }

        [ConfigParameter("EnableDiscountPrice", true, "啟用前臺檔次顯示折扣價", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableDiscountPrice { get; set; }

        [ConfigParameter("DealPagePriceMode", 1, "商品頁價格顯示的模式,1與2是要符合GSA要求", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        int DealPagePriceMode { get; set; }

        #endregion Ppon Configuration

        #region PiinLife Configuration

        [ConfigParameter("HidealCustomerServiceName", "PiinLife品生活客服中心", "品生活客服顯示名稱", ConfigParameterAttribute.ConfigCategories.PiinLife)]
        string HidealCustomerServiceName { get; set; }

        /// <summary>
        /// HiDeal寄送特定系統信時顯示的名稱
        /// </summary>
        [ConfigParameter("HiDealServiceName", "PiinLife品生活", "品生活系統信顯示名稱", ConfigParameterAttribute.ConfigCategories.PiinLife)]
        string HiDealServiceName { get; set; }

        /// <summary>
        /// HiDeal客服中心的網址
        /// </summary>
        [ConfigParameter("HiDealServiceCenterUrl", "https://www.17life.com/piinlife/member/service.aspx", "品生活客服中心的網址", ConfigParameterAttribute.ConfigCategories.PiinLife)]
        string HiDealServiceCenterUrl { get; set; }

        /// <summary>
        /// 存放HiDeal 系統信件範本的檔案系統路徑
        /// </summary>
        [ConfigParameter("HiDealNotificationTemplatesPath", @"C:\source\web_main\MailTemplate\notifications\", "存放品生活系統信件範本的檔案系統路徑", ConfigParameterAttribute.ConfigCategories.PiinLife)]
        string HiDealNotificationTemplatesPath { get; set; }

        /// <summary>
        /// 好康單次訂單可選擇商品數量上限。
        /// </summary>
        [ConfigParameter("HiDealOrderCountlimit", 99, "品生活單次訂單可選擇商品數量上限", ConfigParameterAttribute.ConfigCategories.PiinLife)]
        int HiDealOrderCountlimit { get; set; }

        /// <summary>
        /// 檔次剩下的最後天數. 攸關 [hi_deal_city].[is_main] 的設定 => 影響到首頁即將售完區域裡的檔次.
        /// </summary>
        [ConfigParameter("HiDealLastNDays", 3, "品生活檔剩下幾天要放到\"即將售完區域\"", ConfigParameterAttribute.ConfigCategories.PiinLife)]
        int HiDealLastNDays { get; set; }

        /// <summary>
        /// 客服詢問按鈕-品生活
        /// </summary>
        [ConfigParameter("PiinlifeServiceButtonVisible", true, "品生活客服詢問按鈕", ConfigParameterAttribute.ConfigCategories.PiinLife, ConfigParameterAttribute.ConfigTypes.option)]
        bool PiinlifeServiceButtonVisible { get; set; }

        [ConfigParameter("IsVisa2013", true, "品生活啟用 Visa 2013", ConfigParameterAttribute.ConfigCategories.PiinLife, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsVisa2013 { get; set; }

        [ConfigParameter("NewPiinlifeDate", "2014/8/25", "新品生活上線日期", ConfigParameterAttribute.ConfigCategories.System)]
        DateTime NewPiinlifeDate { get; set; }

        #endregion PiinLife Configuration

        #region Mobile Device Settings

        [ConfigParameter("iOS17LifeAppOAuthClientId", "292297625606812", "iOS APP OAuth Client Id", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string iOS17LifeAppOAuthClientId { get; set; }

        [ConfigParameter("Android17LifeAppOauthClientId", "077516388116954", "Android APP OAuth Client Id", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string Android17LifeAppOauthClientId { get; set; }

        [ConfigParameter("Vbs17LifeOauthClientId", "130965316404594", "Vbs17LifeOauthClientId OAuth Client Id", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string Vbs17LifeOauthClientId { get; set; }

        [ConfigParameter("MobileAndroidAppDownloadUrl", "https://play.google.com/store/apps/details?id=com.uranus.e7plife&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS51cmFudXMuZTdwbGlmZSJd", "行動裝置Android App下載網址", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string AndroidAppDownloadUrl { get; set; }

        [ConfigParameter("AndroidUserAgent", "Android", "Android行動裝置", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string AndroidUserAgent { get; set; }

        [ConfigParameter("iOSUserAgent", "iPod|iPhone|iPad", "iOS行動裝置", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string iOSUserAgent { get; set; }

        [ConfigParameter("iOSAppId", "543439591", "iOS AppId", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string iOSAppId { get; set; }

        [ConfigParameter("AndroidAppId", "com.uranus.e7plife", "Android AppId", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string AndroidAppId { get; set; }

        [ConfigParameter("CloseAppBlockHiddenDays", 7, "關閉建議下載App後隱藏天數", ConfigParameterAttribute.ConfigCategories.Mobile)]
        int CloseAppBlockHiddenDays { get; set; }

        [ConfigParameter("ViewAppBlockHiddenDays", 7, "檢視建議下載App後隱藏天數", ConfigParameterAttribute.ConfigCategories.Mobile)]
        int ViewAppBlockHiddenDays { get; set; }

        [ConfigParameter("AppDownloadSMSCheckDays", 90, "下載APP簡訊發送限制(限天數內)", ConfigParameterAttribute.ConfigCategories.Mobile)]
        int AppDownloadSMSCheckDays { get; set; }

        [ConfigParameter("AppDownloadSMSCheckSendTimes", 3, "下載APP簡訊發送限制(發送次數)", ConfigParameterAttribute.ConfigCategories.Mobile)]
        int AppDownloadSMSCheckSendTimes { get; set; }

        [ConfigParameter("AppHtmlIncludeIFrame", false, "APP讀取的HTML中是否包含iframe", ConfigParameterAttribute.ConfigCategories.Mobile)]
        bool AppHtmlIncludeIFrame { get; set; }

        [ConfigParameter("AppNewVersion", false, "APP是否為新版本", ConfigParameterAttribute.ConfigCategories.Mobile)]
        bool AppNewVersion { get; set; }

        [ConfigParameter("RecommendDealsLogicType", 3, "App 推薦檔次方式", ConfigParameterAttribute.ConfigCategories.Ppon)]
        int RecommendDealsLogicType { get; set; }

        [ConfigParameter("MemoryCache2Mode", "2", "2開啟備用快取機制，1，一般快取，0 關閉快取", ConfigParameterAttribute.ConfigCategories.Ppon)]
        int MemoryCache2Mode { get; set; }

        [ConfigParameter("AppDescEnableIframe", "true", "APP裡檔次介紹的html，如果有iframe是否要使用", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool AppDescEnableIframe { get; set; }

        [ConfigParameter("EanbleABTestRecommendDealsByViewHistory", true, "啟用APP首頁用使用使用推薦檔次當列表", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EanbleABTestRecommendDealsByViewHistory { get; set; }

        #endregion

        [ConfigParameter("IsDefaultPageSettingByCache", true, "首頁精選設定是否讀Cache", ConfigParameterAttribute.ConfigCategories.APP)]
        bool IsDefaultPageSettingByCache { get; set; }
        [ConfigParameter("AppDealNameIncludeLocation", false, "App檔次名稱包含使用區域", ConfigParameterAttribute.ConfigCategories.APP)]
        bool AppDealNameIncludeLocation { get; set; }

        #region App Notification

        /// <summary>
        /// APP推播相關config設定
        /// </summary>
        /// 
        [ConfigParameter("AwsPush", false, "[APP]是否啟用AWS推播", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool AwsPush { get; set; }

        [ConfigParameter("SNSAccessKey", "AKIAJNI5IORUO7RMABAA", "AWS SNS AccessKey", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.normal)]
        string AwsSnsAccessKey { get; set; }

        [ConfigParameter("SNSSecretKey", "8i1fhToiT2Kv0s0nr4R13gOiYmUCY6iYz1ngUZQ+", "AWS SNS SecretKey", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.normal)]
        string AwsSnsSecretKey { get; set; }

        [ConfigParameter("NotificationDescriptionLimit", 80, "[APP]推播訊息字元長度限制", ConfigParameterAttribute.ConfigCategories.APP)]
        int NotificationDescriptionLimit { get; set; }

        [ConfigParameter("AppNotificationJob", false, "[APP]Job是否啟用", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool AppNotificationJob { get; set; }

        [ConfigParameter("AppNotificationByDevice", false, "[APP]是否開啟推播By裝置功能", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool AppNotificationByDevice { get; set; }

        [ConfigParameter("AppPushServiceUri", "http://localhost:9000/", "[APP]PushService網址", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        string AppPushServiceUri { get; set; }

        [ConfigParameter("AppIOSNotification", false, "[APP]IOS推播是否啟用", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool AppIOSNotification { get; set; }

        [ConfigParameter("IOSPushP12FilePath", @"D:\Cognac\Main\Source\LunchKingSite\LunchKingSite.Web\P12\", "[APP]IOS推播檔案路徑", ConfigParameterAttribute.ConfigCategories.APP)]
        string IOSPushP12FilePath { get; set; }

        [ConfigParameter("IOSPushP12FileName", "PushNotification.p12", "[APP]IOS推播檔案名稱", ConfigParameterAttribute.ConfigCategories.APP)]
        string IOSPushP12FileName { get; set; }

        [ConfigParameter("IOSPushP12FilePassword", "", "[APP]IOS推播檔案解鎖密碼", ConfigParameterAttribute.ConfigCategories.APP)]
        string IOSPushP12FilePassword { get; set; }

        [ConfigParameter("IOSPushChannelNumber", 40, "[APP]IOS推播時允許使用的頻道數", ConfigParameterAttribute.ConfigCategories.APP)]
        int IOSPushChannelNumber { get; set; }

        //true :正式 gatewaypush.apple.com  false:測試 gateway.sandbox.push.apple.com
        [ConfigParameter("IOSOfficialApsHost", false, "[APP]IOS推播Server使用正式Server", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool IOSOfficialApsHost { get; set; }

        [ConfigParameter("AndroidPushChannelNumber", 40, "[APP]Android推播時允許使用的頻道數", ConfigParameterAttribute.ConfigCategories.APP)]
        int AndroidPushChannelNumber { get; set; }

        [ConfigParameter("AppAndroidNotification", false, "[APP]Android推播是否啟用", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool AppAndroidNotification { get; set; }

        [ConfigParameter("AndroidPushSenderID", "341007392287", "[APP]Android推播SenderID", ConfigParameterAttribute.ConfigCategories.APP)]
        string AndroidPushSenderID { get; set; }

        [ConfigParameter("AndroidPushAPIAccessAPIKey", "AIzaSyAceNzd8_WBbZ2RDZs4rO9GGzYpOMupsNs", "[APP]AndroidAppAPIAccessAPIKey", ConfigParameterAttribute.ConfigCategories.APP)]
        string AndroidPushAPIAccessAPIKey { get; set; }

        [ConfigParameter("AndroidAppPackageName", "com.uranus.e7plife", "[APP]AndroidApp軟體名稱", ConfigParameterAttribute.ConfigCategories.APP)]
        string AndroidAppPackageName { get; set; }

        [ConfigParameter("AndroidRegistrationIdRange", 1000, "[APP]一筆推播訊息中RegisteIds的Device數", ConfigParameterAttribute.ConfigCategories.APP)]
        int AndroidRegistrationIdRange { get; set; }

        /// <summary>
        /// 是否開啟跳轉到APP的功能
        /// </summary>
        [ConfigParameter("ReferralToApp", false, "是否開啟跳轉到APP的功能", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool ReferralToApp { get; set; }

        /// <summary>
        /// 是否顯示全家折扣數
        /// </summary>
        [ConfigParameter("ShowFamiDiscount", true, "APP是否顯示全家折扣數", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool ShowFamiDiscount { get; set; }

        [ConfigParameter("SubscriptionNoticeLimitHours", 24, "[APP]收藏推播訊息時間限制-小時", ConfigParameterAttribute.ConfigCategories.APP)]
        int SubscriptionNoticeLimitHours { get; set; }

        [ConfigParameter("SubscriptionNoticeSingleMsgFormat", "您收藏的［{0}］即將於{1}結束，快把握機會搶購", "[APP]單檔收藏推播訊息格式", ConfigParameterAttribute.ConfigCategories.APP)]
        string SubscriptionNoticeSingleMsgFormat { get; set; }

        [ConfigParameter("SubscriptionNoticeMultiMsgFormat", "您收藏的［{0}］和其他{2}個好康即將於{1}結束，快把握機會搶購", "[APP]多檔收藏推播訊息格式", ConfigParameterAttribute.ConfigCategories.APP)]
        string SubscriptionNoticeMultiMsgFormat { get; set; }

        [ConfigParameter("MemberCollectionDealAfterCloseMonths", 0, "結檔多少個月後的收藏檔次不顯示", ConfigParameterAttribute.ConfigCategories.Ppon)]
        int MemberCollectionDealAfterCloseMonths { get; set; }

        [ConfigParameter("RefreshTokenExtendExpiredDays", 1, "[APP]RefreshToken延長失效的天數", ConfigParameterAttribute.ConfigCategories.APP)]
        int RefreshTokenExtendExpiredDays { get; set; }

        [ConfigParameter("OAuthTokenExpiredSeconds", 7200, "[APP]OAuthToken失效秒數", ConfigParameterAttribute.ConfigCategories.APP)]
        int OAuthTokenExpiredSeconds { get; set; }

        [ConfigParameter("PokeBallBannerImage", "https://www.17life.com/media/Event/712_EventPromoAppMainImage.jpg", "活動圖片", ConfigParameterAttribute.ConfigCategories.APP)]
        string PokeBallBannerImage { get; set; }

        [ConfigParameter("PokeBallBannerUrl", "https://www.facebook.com/17life.com.tw/videos/10154226344077431", "活動網址", ConfigParameterAttribute.ConfigCategories.APP)]
        string PokeBallBannerUrl { get; set; }

        [ConfigParameter("PokeBallBannerStart", "2016/6/1 18:00", "活動開始時間", ConfigParameterAttribute.ConfigCategories.APP)]
        DateTime PokeBallBannerStart { get; set; }

        [ConfigParameter("PokeBallBannerEnd", "2016/6/1 22:00", "活動結束時間", ConfigParameterAttribute.ConfigCategories.APP)]
        DateTime PokeBallBannerEnd { get; set; }

        [ConfigParameter("PushMessageDefaultExpirationDays", "30", "訊息中心顯示的訊息，超過這個時間即不顯示", ConfigParameterAttribute.ConfigCategories.APP)]
        int PushMessageDefaultExpirationDays { get; set; }

        [ConfigParameter("EnableControlroomPushBrand", false, "是否啟用後台推播策展2.0功能", ConfigParameterAttribute.ConfigCategories.APP)]
        bool EnableControlroomPushBrand { get; set; }

        #endregion App Notification

        #region EDM Configuration

        /// <summary>
        /// 切換EDM發送之環境
        /// MailServerSendType.MailServer
        /// MailServerSendType.PostMan
        /// </summary>
        [ConfigParameter("MailServiceType", MailServerSendType.MailServer, "發送時要用的伺服器系統", ConfigParameterAttribute.ConfigCategories.EDM)]
        MailServerSendType MailServiceType { get; set; }

        /// <summary>
        /// EDM之代表mail name
        /// </summary>
        [ConfigParameter("EDMSenderName", "17Life", "[P好康EDM]寄件者名稱", ConfigParameterAttribute.ConfigCategories.EDM)]
        string EDMSenderName { get; set; }

        [ConfigParameter("EDMPrePath", "/Ppon/EDM", "EDM線上版本放置的目錄", ConfigParameterAttribute.ConfigCategories.EDM)]
        string EDMPrePath { get; set; }

        /// <summary>
        /// EDM預設之圖片，以備圖片錯誤時之用
        /// </summary>
        [ConfigParameter("EDMDefaultImage", @"/Themes/default/images/17Life/EDM/EDMDefault.jpg", "EDM預設圖片", ConfigParameterAttribute.ConfigCategories.EDM)]
        string EDMDefaultImage { get; set; }

        #region Piinlife EDM

        [ConfigParameter("PiinlifeEDMTempPath", @"C:\source\web_main\template\PiinLife\EDM\current", "[品生活EDM]樣版存放目錄", ConfigParameterAttribute.ConfigCategories.EDM)]
        string PiinlifeEDMTempPath { get; set; }

        /// <summary>
        /// 品生活 EDM 寄送之城市
        /// Null: 不送出
        /// All： 全送(排除重復以及取消訂閱者)
        /// AllCity: 所有城市
        /// Overview: 總覽
        /// TP, TY, XZ, TC, TN, GX: 依序為 臺北/桃園/新竹/臺中/臺南/高雄
        /// </summary>
        [ConfigParameter("PiinlifeEDMSendCity", PiinlifeMailSendCity.Overview, "品生活EDM欲寄發之城市", ConfigParameterAttribute.ConfigCategories.EDM)]
        PiinlifeMailSendCity PiinlifeEDMSendCity { get; set; }

        #endregion Piinlife EDM

        #region new edm

        [ConfigParameter("NewEdmPath", "/cache/NewEdm", "新版EDM", ConfigParameterAttribute.ConfigCategories.EDM)]
        string NewEdmPath { get; set; }

        [ConfigParameter("EnableNewEdmUploadFtp", true, "啟用新版EDM上傳FTP", ConfigParameterAttribute.ConfigCategories.EDM, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableNewEdmUploadFtp { get; set; }

        #endregion new edm

        #region edm log

        /// <summary>
        /// 是否啟用EDM發送數量調整
        /// MailServerSendType.MailServer
        /// MailServerSendType.PostMan
        /// </summary>
        [ConfigParameter("EnableNewEdmSendRules", false, "啟用EDM發送數量調整", ConfigParameterAttribute.ConfigCategories.EDM)]
        bool EnableNewEdmSendRules { get; set; }

        /// <summary>
        /// EDM排除幾個月未開啟的Email
        /// MailServerSendType.MailServer
        /// MailServerSendType.PostMan
        /// </summary>
        [ConfigParameter("RemoveMonthsCount", 12, "EDM排除未開啟月份", ConfigParameterAttribute.ConfigCategories.EDM)]
        int RemoveMonthsCount { get; set; }

        #endregion

        #endregion EDM Configuration

        #region Discount Configuration

        /// <summary>
        /// 是否啟用折價券
        /// </summary>
        [ConfigParameter("DiscountCodeUsed", false, "是否啟用折價券", ConfigParameterAttribute.ConfigCategories.Discount, ConfigParameterAttribute.ConfigTypes.option)]
        bool DiscountCodeUsed { get; set; }

        /// <summary>
        /// 折價券預設毛利率設定
        /// </summary>
        [ConfigParameter("DiscountCodeMinGrossMarginSet", 15, "折價券預設毛利率設定", ConfigParameterAttribute.ConfigCategories.Discount)]
        int DiscountCodeMinGrossMarginSet { get; set; }

        /// <summary>
        /// 首購&親友折價券預設門檻
        /// </summary>
        [ConfigParameter("ReferrerAndFirstBuyDiscountMinAmountSet", 8, "首購&親友折價券預設門檻比例", ConfigParameterAttribute.ConfigCategories.Discount)]
        int ReferrerAndFirstBuyDiscountMinAmountSet { get; set; }

        /// <summary>
        /// 邀請親友贈送折價券設定(預設200*1、100*2、50*2)
        /// </summary>
        [ConfigParameter("ReferrerDiscountCodeSet", "200#1|100#2|50#2", "邀請親友贈送折價券設定", ConfigParameterAttribute.ConfigCategories.Discount)]
        string ReferrerDiscountCodeSet { get; set; }

        /// <summary>
        /// 邀請折價券至少可使用期限(單位:月份)
        /// </summary>
        [ConfigParameter("DiscountReferrerExpireDays", 0.5, "邀請折價券至少可使用期限(單位:月份)", ConfigParameterAttribute.ConfigCategories.Discount)]
        decimal DiscountReferrerExpireMonths { get; set; }

        /// <summary>
        /// 首購贈送折價券設定(預設200*1、100*2、50*2)
        /// </summary>
        [ConfigParameter("FirstBuyDiscountCodeSet", "200#1|100#2|50#2", "首購贈送折價券設定", ConfigParameterAttribute.ConfigCategories.Discount)]
        string FirstBuyDiscountCodeSet { get; set; }

        /// <summary>
        /// 首購折價券至少可使用期限(單位:月份)
        /// </summary>
        [ConfigParameter("FirstBuyDiscountExpireMonths", 0.5, "首購折價券至少可使用期限(單位:月份)", ConfigParameterAttribute.ConfigCategories.Discount)]
        decimal FirstBuyDiscountExpireMonths { get; set; }

        /// <summary>
        /// 是否啟用首購贈送折價券
        /// </summary>
        [ConfigParameter("FirstBuyDiscountCodeEnabled", true, "是否啟用首購贈送折價券", ConfigParameterAttribute.ConfigCategories.Discount, ConfigParameterAttribute.ConfigTypes.option)]
        bool FirstBuyDiscountCodeEnabled { get; set; }

        /// <summary>
        /// 是否啟用新會員贈送折價券
        /// </summary>
        [ConfigParameter("NewUserSendDiscountCodeEnabled", true, "是否啟用新會員贈送折價券", ConfigParameterAttribute.ConfigCategories.Discount, ConfigParameterAttribute.ConfigTypes.option)]
        bool NewUserSendDiscountCodeEnabled { get; set; }

        /// <summary>
        /// 是否啟用自動發送折價券
        /// </summary>
        [ConfigParameter("AutoSendDiscountCodeEnabled", false, "是否啟用自動發送折價券", ConfigParameterAttribute.ConfigCategories.Discount, ConfigParameterAttribute.ConfigTypes.option)]
        bool AutoSendDiscountCodeEnabled { get; set; }

        /// <summary>
        /// 是否啟用自動發送折價券
        /// </summary>
        [ConfigParameter("DiscountUserCollectType", DiscountUserCollectType.Auto, "折價券領取機制", ConfigParameterAttribute.ConfigCategories.Discount, ConfigParameterAttribute.ConfigTypes.option)]
        DiscountUserCollectType DiscountUserCollectType { get; set; }

        /// <summary>
        /// 折價券多少張數以下稱之為限量
        /// </summary>
        [ConfigParameter("DisciuntLimitedCount", 3000, "折價券多少張數以下稱之為限量", ConfigParameterAttribute.ConfigCategories.Discount, ConfigParameterAttribute.ConfigTypes.option)]
        int DisciuntLimitedCount { get; set; }

        /// <summary>
        /// 預帶折價券資料啟用
        /// </summary>
        [ConfigParameter("EnableBuyMemberDiscountList", true, "預帶折價券資料啟用", ConfigParameterAttribute.ConfigCategories.Discount, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableBuyMemberDiscountList { get; set; }


        #endregion Discount Configuration

        #region 信託

        [ConfigParameter("TrustReportFtpPath", @"\\192.168.170.2\Trust_Report\", "每月信託報表Ftp位置", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        string TrustReportFtpPath { get; set; }

        #endregion 信託

        #region PayEasy Configuration

        /// <summary>
        /// web service路徑，用來連線到payeasy進行身分驗證
        /// </summary>
        [ConfigParameter("PayeasyIdCheckWebServiceUrl", "https://eip.payeasy.com.tw/WebServices/services/internal/member/login", "Payeasy確認使用者帳號用的webService (c:\\temp\\)", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PayeasyIdCheckWebServiceUrl { get; set; }

        [ConfigParameter("PayEasyCasServerUrl", "https://www.payeasy.com.tw/Cas/", "payeasy sso server", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PayEasyCasServerUrl { get; set; }

        [ConfigParameter("PezSsoVerifyUrl", "http://www.payeasy.com.tw/MyPayeasy/ExtraSysIdentify", "", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PezSsoVerifyUrl { get; set; }

        /// <summary>
        /// 新版 PEZ SSO 串接功能
        /// </summary>
        [ConfigParameter("NewPezSso", false, "新版 PEZ SSO 串接功能", ConfigParameterAttribute.ConfigCategories.PayEasy, ConfigParameterAttribute.ConfigTypes.option)]
        bool NewPezSso { get; set; }

        [ConfigParameter("PezSyncLogout", false, "從PEZ串接進來的會員，當他登出後是否同步登出PEZ", ConfigParameterAttribute.ConfigCategories.PayEasy, ConfigParameterAttribute.ConfigTypes.option)]
        bool PezSyncLogout { get; set; }

        [ConfigParameter("PcashBatchFtp", "211.78.88.4", "", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PcashBatchFtp { get; set; }
        [ConfigParameter("PcashXchAESKey1", "sq9/W4/4kjy0OI7TB0yaeSAcbZf16EemSx80t1yeo1E=", "兌換Pcash用的加密鑰匙1", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PcashXchAESKey1 { get; set; }
        [ConfigParameter("PcashXchAESKey2", "3bxr1VjEwiZYEkTEZJQJ5V5xPkL6IbZzM530NPK7lyA=", "兌換Pcash用的加密鑰匙2", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PcashXchAESKey2 { get; set; }
        [ConfigParameter("PcashXchClientId", "testPay", "兌換Pcash用的應用程式代號，Pez發給17", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PcashXchClientId { get; set; }
        [ConfigParameter("PcashXchClientSecret", "kd8c73j7llP", "兌換Pcash用的應用程式密碼，Pez發給17", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PcashXchClientSecret { get; set; }
        [ConfigParameter("EnablePcashXchUser", "", "使用兌換Pcash帳號(內測用 ;區隔userid)", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string EnablePcashXchUser { get; set; }
        [ConfigParameter("PezAPIClientId", "2Y8N3xT;6QWQYQLy", "PezSCMWebServiceAPI帳號", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PezAPIClientId { get; set; }
        [ConfigParameter("PezAPIClientSecret", "iLt8vDuPkMKMt7F4xapkNnIaylGs77+4rJpzClilJzQ=", "PezSCMWebServiceAPI密碼", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PezAPIClientSecret { get; set; }
        [ConfigParameter("PezAPIUsrNum", "45808", "PezSCMWebServiceAPI參數UsrNum", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PezAPIUsrNum { get; set; }
        [ConfigParameter("PezAPIVenNum", "5535", "PezSCMWebServiceAPI參數VenNum", ConfigParameterAttribute.ConfigCategories.PayEasy)]
        string PezAPIVenNum { get; set; }


        #endregion PayEasy Configuration

        #region ThirdParty

        [ConfigParameter("GoogleMapsAPIKey", "ABQIAAAABlINUmA4D71HZgZULYUd4hRJdwZhmNWNLtr9rKrQCfWjqt1AHxT9Sr67-3vRbLA5UAcdQtAnxuRAHw", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string GoogleMapsAPIKey { get; set; }

        [ConfigParameter("FacebookApplicationSecret", "848332c974cf54b0e20dc896eaf978a1", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string FacebookApplicationSecret { get; set; }

        [ConfigParameter("FacebookApplicationId", "169978179852434", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string FacebookApplicationId { get; set; }

        [ConfigParameter("FacebookGiftObjectId", "1675581679432200", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string FacebookGiftObjectId { get; set; }

        [ConfigParameter("FacebookShareAppId", "189065144614", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string FacebookShareAppId { get; set; }

        /// <summary>
        /// This tell our system to use the beta version Facebook Graph API or not to.
        /// </summary>
        [ConfigParameter("EnableBetaGraphAPI", false, "", ConfigParameterAttribute.ConfigCategories.ThirdParty, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableBetaGraphAPI { get; set; }

        [ConfigParameter("HamiHMACSHA1Key", "12345", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string HamiHMACSHA1Key { get; set; }

        [ConfigParameter("DEAfilterApiKey", "clUBKqA8oPkgDqWYxbAwyrMXeduf6T1B", "拋棄式Email檢查API Key", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string DEAfilterApiKey { get; set; }

        [ConfigParameter("GoogleApiKey", "347909659903-vi5hq1chjgghu5ivnhmksjd9ro2s9bj1.apps.googleusercontent.com", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string GoogleApiKey { get; set; }

        [ConfigParameter("GoogleApiSecret", "GTI0ofh7XnB4nJ2FGZikPcFY", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string GoogleApiSecret { get; set; }

        [ConfigParameter("EnableGoogleMapApiV2", true, "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        bool EnableGoogleMapApiV2 { get; set; }

        [ConfigParameter("EnableOpenStreetMap", true, "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        bool EnableOpenStreetMap { get; set; }

        [ConfigParameter("IDEASServiceToken", "iK89Odju", "呼叫資策會API需要用到的識別編號", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string IDEASServiceToken { get; set; }

        [ConfigParameter("IDEASServiceUrl", "http://175.98.115.21:14298/oneclickshoppingwall/rest", "資策會服務之連線字串", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string IDEASServiceUrl { get; set; }

        [ConfigParameter("EnableBingMapAPI", false, "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        bool EnableBingMapAPI { get; set; }

        [ConfigParameter("BingMapApiKey", "AoLupzlwwQSPkSf9awe0Y2LWqLV6WFafLCS6FZ9HlAhyAvRtQDpEf2xS9VFF2vz6", "Bing Map API Key", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string BingMapApiKey { get; set; }

        [ConfigParameter("FilterLowGrossMarginDealForShoppingGuideApi", false, "導購API過濾低毛利檔次", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        bool FilterLowGrossMarginDealForShoppingGuideApi { get; set; }

        [ConfigParameter("EnableTaisihinPayCashPoint", false, "啟用台新儲值支付", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        bool EnableTaisihinPayCashPoint { get; set; }

        [ConfigParameter("ShoppingGuidePageSize", 500, "導購商API分頁大小", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        int ShoppingGuidePageSize { get; set; }

        #endregion ThirdParty

        #region Tmall

        [ConfigParameter("TmallDefaultUserName", "tmall@17life.com", "天貓預設購買人", ConfigParameterAttribute.ConfigCategories.Tmall)]
        string TmallDefaultUserName { get; set; }

        #endregion Tmall

        #region WeChat

        [ConfigParameter("WeChatToken", "17life_wechat", "WeChat接口驗證token", ConfigParameterAttribute.ConfigCategories.WeChat)]
        string WeChatToken { get; set; }

        [ConfigParameter("WeChatAppId", "wx60d7d50d70a351c4", "WeChat接口驗證AppId", ConfigParameterAttribute.ConfigCategories.WeChat)]
        string WeChatAppId { get; set; }

        [ConfigParameter("WeChatAppSecret", "278865bd7b559aedf392efaa0e7bd311", "WeChat接口驗證AppSecret", ConfigParameterAttribute.ConfigCategories.WeChat)]
        string WeChatAppSecret { get; set; }

        [ConfigParameter("WeChatArticleCount", "3", "WeChat接口驗證AppSecret", ConfigParameterAttribute.ConfigCategories.WeChat)]
        int WeChatArticleCount { get; set; }

        #endregion WeChat

        #region PponSetup
        [ConfigParameter("OperationPath", "~/App_Data/PponSetup/Operation_manual.pdf", "系統操作說明手冊核銷路徑")]
        string OperationPath { get; set; }
        [ConfigParameter("ReconciliationPath", "~/App_Data/PponSetup/Operation_Reconciliation.pdf", "系統操作說明手冊對帳路徑")]
        string ReconciliationPath { get; set; }

        [ConfigParameter("WriteOffPath", "~/App_Data/PponSetup/WriteOff.doc", "憑證核銷紀錄表路徑")]
        string WriteOffPath { get; set; }
        #endregion

        #region MasterPass

        [ConfigParameter("ConsumerKey", "gfCQmxGw_oPUqeb2dnozVQDOWBcz5twtRiWLkPZcfb1c4dc4!414c6f6d4e644e73676375414237536e394d684b7050673d", "MasterPass Manage Development Checkout Project中參數", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string ConsumerKey { get; set; }

        [ConfigParameter("CheckoutIdentifier", "a4a6w4wyj9knwi11bnjcw1i1iydqsr52es", "MasterPass Manage Development Checkout Project中參數", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string CheckoutIdentifier { get; set; }

        [ConfigParameter("CertPath", "~/App_Data/17Life2015mp.pfx", "MasterPass 憑證路徑", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string CertPath { get; set; }

        [ConfigParameter("CertPassword", "!not4u2c17*", "MasterPass 憑證密碼", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string CertPassword { get; set; }

        [ConfigParameter("ApiPrefix", "", "MasterPass Api環境(空白為正式環境，'sandbox.'為測試環境)", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string ApiPrefix { get; set; }

        [ConfigParameter("CallbackPath", "/ppon/buy.aspx", "MasterPass 回傳網址", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string CallbackPath { get; set; }

        [ConfigParameter("EnableMasterPass", "false", "MasterPass 啟用", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool EnableMasterPass { get; set; }

        [ConfigParameter("LightboxJsUrl", "https://www.masterpass.com/lightbox/Switch/integration/MasterPass.client.js", "MasterPass Lightbox Js連結", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string LightboxJsUrl { get; set; }

        [ConfigParameter("EncryptCount", 500, "MasterpassLog accountNumber 一次加跑幾筆加密筆數", ConfigParameterAttribute.ConfigCategories.None, ConfigParameterAttribute.ConfigTypes.normal)]
        int EncryptCount { get; set; }

        [ConfigParameter("IsMasterPassLocalTest", "false", "MasterPass 本機測試", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool IsMasterPassLocalTest { get; set; }

        [ConfigParameter("EnableMasterpassPreCheckout", false, "Buy頁啟用PreCheckout", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool EnableMasterpassPreCheckout { get; set; }

        [ConfigParameter("EnableMasterpassExpressCheckout", false, "Buy頁啟用ExpressCheckout", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool EnableMasterpassExpressCheckout { get; set; }

        [ConfigParameter("EnableMasterpassStandardCheckout", false, "Buy頁啟用StandardCheckout", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool EnableMasterpassStandardCheckout { get; set; }

        #endregion MasterPass

        #region LinePay

        [ConfigParameter("EnableLinePay", false, "Line Pay 啟用", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool EnableLinePay { get; set; }

        [ConfigParameter("LinePayCaptureWithConfirm", true, "LinePay授權時一併請款", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        bool LinePayCaptureWithConfirm { get; set; }

        [ConfigParameter("LinePayChannelId", "1434792337", "Line Pay Channel Id", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string LinePayChannelId { get; set; }

        [ConfigParameter("LinePayChannelSecretKey", "9f711e6857addc80121dfd5b7616f2d8", "Line Pay Channel Secret Key", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string LinePayChannelSecretKey { get; set; }

        [ConfigParameter("LinePayApiReserve", "https://api-pay.line.me/v1/payments/request", "Line Pay API Reserve(取得導頁Url/交易序號)", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string LinePayApiReserve { get; set; }

        [ConfigParameter("LinePayApiConfirm", "https://api-pay.line.me/v1/payments/{0}/confirm", "Line Pay API Confirm(確認授權)", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string LinePayApiConfirm { get; set; }

        [ConfigParameter("LinePayApiCapture", "https://api-pay.line.me/v1/payments/authorizations/{0}/capture", "Line Pay API Capture(請款)", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string LinePayApiCapture { get; set; }

        [ConfigParameter("LinePayApiVoid", "https://api-pay.line.me/v1/payments/authorizations/{0}/void", "Line Pay API Void(取消授權)", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string LinePayApiVoid { get; set; }

        [ConfigParameter("LinePayApiRefund", "https://api-pay.line.me/v1/payments/{0}/refund", "Line Pay API Refund(退款)", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string LinePayApiRefund { get; set; }

        [ConfigParameter("LinePayLangCd", "zh-Hant", "Line Pay 語系(繁中 zh-Hant)", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string LinePayLangCd { get; set; }

        [ConfigParameter("LinePayUsingLogo", "https://www.17life.com/images/17P/17topbn/mobile/images/17life-app-icon-120.png", "Line Pay頁面使用Logo圖檔", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        string LinePayUsingLogo { get; set; }

        [ConfigParameter("LinePayApiRetryTimes", 5, "取銷授權/退款失敗重試次數", ConfigParameterAttribute.ConfigCategories.Payment, ConfigParameterAttribute.ConfigTypes.normal)]
        int LinePayApiRetryTimes { get; set; }

        [ConfigParameter("LinePayRefundPeriodLimit", 60, "LinePay訂單訂購完後, 滿此天數則不可刷退", ConfigParameterAttribute.ConfigCategories.Payment)]
        int LinePayRefundPeriodLimit { get; set; }

        #endregion LinePay

        #region 台新銀行

        [ConfigParameter("EnableTaishinCreditCardOnly", true, "啟用由台新APP進入僅能使用台新卡", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableTaishinCreditCardOnly { get; set; }

        [ConfigParameter("TSBankAppRsrc", "TSBank_app", "台新銀行APP專用rsrc", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.normal)]
        string TSBankAppRsrc { get; set; }

        [ConfigParameter("EnableTaishinThirdPartyPay", false, "啟用由台新儲值支付", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableTaishinThirdPartyPay { get; set; }

        [ConfigParameter("TaishinThirdPartyPayApiUrl", "https://210.242.31.142", "台新儲值支付 API服務網止", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.option)]
        string TaishinThirdPartyPayApiUrl { get; set; }

        [ConfigParameter("TaishinThirdPartyPayUserUrl", "https://210.242.31.142", "台新儲值支付 客戶服務網止", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.option)]
        string TaishinThirdPartyPayUserUrl { get; set; }

        [ConfigParameter("TaishinThirdPartyPayRegisterUrl", "https://210.242.31.142/tsb/oa-type2-step1.aspx", "台新儲值支付 註冊網址", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.option)]
        string TaishinThirdPartyPayRegisterUrl { get; set; }

        [ConfigParameter("TaishinThirdPartyPayIndexUrl", "https://210.242.31.142/tsb/login.aspx", "台新儲值支付 首頁", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.option)]
        string TaishinThirdPartyPayIndexUrl { get; set; }

        [ConfigParameter("TaishinThirdPartyPayClientId", "17Life", "台新儲值支付 client_id", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.option)]
        string TaishinThirdPartyPayClientId { get; set; }

        [ConfigParameter("TaishinThirdPartyPayClientPassword", "17Life123", "台新儲值支付 client_pw", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.option)]
        string TaishinThirdPartyPayClientPassword { get; set; }

        [ConfigParameter("TaishinThirdPartyPayVirtualAcctStart", "95980", "台新儲值支付 企業虛擬帳戶前5碼", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.option)]
        string TaishinThirdPartyPayVirtualAcctStart { get; set; }

        [ConfigParameter("TaishinNavName", "每日下殺", "台新商城專用頻道", ConfigParameterAttribute.ConfigCategories.TaishinBank)]
        string TaishinNavName { get; set; }

        [ConfigParameter("TaishinNavUrl", "/m/TaishinApp?cid=477&cat=844", "台新商城專用頻道連結", ConfigParameterAttribute.ConfigCategories.TaishinBank)]
        string TaishinNavUrl { get; set; }

        [ConfigParameter("TaishinNavImgUrl", "/Themes/mobile/images/icon_channel_low_price.png", "台新商城專用頻道Icon url", ConfigParameterAttribute.ConfigCategories.TaishinBank)]
        string TaishinNavImgUrl { get; set; }

        [ConfigParameter("TaishinCreditCardBankId", 1, "台新銀行信用卡BankId", ConfigParameterAttribute.ConfigCategories.TaishinBank)]
        int TaishinCreditCardBankId { get; set; }

        #endregion

        #region LionTravel

        [ConfigParameter("LionTravelNotifyAPI", @"https://uetkt.api.liontravel.com/api/V1/ProductToDBM06", "雄獅檔次異動通知API網址", ConfigParameterAttribute.ConfigCategories.LionTravel, ConfigParameterAttribute.ConfigTypes.normal)]
        string LionTravelNotifyAPI { get; set; }

        [ConfigParameter("LionTravelNofityAPIToken", "MTg0NTI1MDk5MTE5MzUwNDY0MzE", "雄獅檔次異動通知API Token", ConfigParameterAttribute.ConfigCategories.LionTravel, ConfigParameterAttribute.ConfigTypes.normal)]
        string LionTravelNofityAPIToken { get; set; }

        [ConfigParameter("LionTravelChannelCheckBillBeginDate", "2015/11/13", "對帳單上線日", ConfigParameterAttribute.ConfigCategories.LionTravel, ConfigParameterAttribute.ConfigTypes.normal)]
        string LionTravelChannelCheckBillBeginDate { get; set; }

        [ConfigParameter("LionTravelServiceEmail", "cs_02@17life.com", "雄獅客服平台信箱", ConfigParameterAttribute.ConfigCategories.LionTravel, ConfigParameterAttribute.ConfigTypes.normal)]
        string LionTravelServiceEmail { get; set; }

        #endregion LionTravel

        [ConfigParameter("UnUsed17PayMsg", "申辦使用17life pay付款成功，月底將可以獲得全家咖啡一杯！", "17life 行動支付未申辦行銷文案", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.option)]
        string UnUsed17PayMsg { get; set; }

        [ConfigParameter("Used17PayMsg", "首次使用17life pay付款成功，月底將可以獲得全家咖啡一杯！", "17life 行動支付已申辦行銷文案", ConfigParameterAttribute.ConfigCategories.TaishinBank, ConfigParameterAttribute.ConfigTypes.option)]
        string Used17PayMsg { get; set; }

        /// <summary>
        /// PCP webapi 送出測試用資料
        /// </summary>
        [ConfigParameter("IsPCPApiTestMode", true, "PCP webapi 送出測試用資料", ConfigParameterAttribute.ConfigCategories.PCP, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsPCPApiTestMode { get; set; }

        #region ACH Configuration

        [ConfigParameter("TriggerBankAndBranchNo", "8120012", "提出行代號(發動行金資代號)，共7碼", ConfigParameterAttribute.ConfigCategories.ACH)]
        string TriggerBankAndBranchNo { get; set; }

        [ConfigParameter("TriggerAccountNo", "20680180001708", "發動者帳號(於發動行開戶的帳號)，共14碼；不足位往左補0", ConfigParameterAttribute.ConfigCategories.ACH)]
        string TriggerAccountNo { get; set; }

        [ConfigParameter("TriggerCompanyId", "24317014", "發動者統一編號(公司統編)", ConfigParameterAttribute.ConfigCategories.ACH)]
        string TriggerCompanyId { get; set; }

        [ConfigParameter("SendUnitNo", "8120012", "發送單位代號，共7碼(表頭表尾)", ConfigParameterAttribute.ConfigCategories.ACH)]
        string SendUnitNo { get; set; }

        [ConfigParameter("ReceiveUnitNo", "9990250", "接收單位代號，共7碼(表頭表尾)", ConfigParameterAttribute.ConfigCategories.ACH)]
        string ReceiveUnitNo { get; set; }

        /* ACH匯費機制已開發一部分 但決議暫觀察一陣子 再決定是否施行
        [ConfigParameter("AchRemittanceFee", "15", "ACH單筆匯費", ConfigParameterAttribute.ConfigCategories.ACH)]
        int AchRemittanceFee { get; set; }

        [ConfigParameter("AchRemittanceTimeLimit", "4", "ACH同一帳號每月匯款限制次數", ConfigParameterAttribute.ConfigCategories.ACH)]
        int AchRemittanceTimeLimit { get; set; }
         */

        #endregion ACH Configuration

        [ConfigParameter("GreetingCardPponDiscountCampaignId", 258, "聖誕賀卡活動-P好康折價券活動ID", ConfigParameterAttribute.ConfigCategories.None, ConfigParameterAttribute.ConfigTypes.normal)]
        int GreetingCardPponDiscountCampaignId { get; set; }

        [ConfigParameter("GreetingCardHiDealDiscountCampaignId", 259, "聖誕賀卡活動-品生活折價券活動ID", ConfigParameterAttribute.ConfigCategories.None, ConfigParameterAttribute.ConfigTypes.normal)]
        int GreetingCardHiDealDiscountCampaignId { get; set; }

        /// <summary>
        /// 商圈子區域分類暫存資料檢查
        /// </summary>
        [ConfigParameter("IsSubLocationListInspection", true, "商圈子區域分類暫存資料檢查", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsSubLocationListInspection { get; set; }

        [ConfigParameter("BeaconEventPushValidStartTime", "2016/6/20 09:00:00", "每日推播有效時間(起)", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        DateTime BeaconEventPushValidStartTime { get; set; }

        [ConfigParameter("BeaconEventPushValidEndTime", "2016/6/20 17:00:00", "每日推播有效時間(迄)", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        DateTime BeaconEventPushValidEndTime { get; set; }

        [ConfigParameter("BeaconEventPushIntervalMinute", 60, "Beacon推播間隔分鐘數", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        int BeaconEventPushIntervalMinute { get; set; }
        [ConfigParameter("BeaconPushDailyUpperLimit", 4, "Beacona推播每日上限", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        int BeaconPushDailyUpperLimit { get; set; }

        [ConfigParameter("BeaconEventAdEnabled", false, "BeaconAPP蓋版AD是否開啟", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool BeaconEventAdEnabled { get; set; }

        [ConfigParameter("FamiChannelBannerEnabled", false, "APP 全家頻道Banner是否開啟", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool FamiChannelBannerEnabled { get; set; }

        [ConfigParameter("PponSearchLogLimitCount", 10, "新增關鍵字搜尋次數", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        int PponSearchLogLimitCount { get; set; }
        [ConfigParameter("EnableComboDealNewOrderTotalLimitSum", true, "多檔次母檔總購買量排除鎖檔檔次(購買上限設為0)購買量", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableComboDealNewOrderTotalLimitSum { get; set; }

        [ConfigParameter("BusinessOrderTimeSMinute", 0, "預設開檔時間(分)", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        int BusinessOrderTimeSMinute { get; set; }

        [ConfigParameter("BusinessOrderTimeEMinute", 1439, "預設結檔時間(分)", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        int BusinessOrderTimeEMinute { get; set; }

        [ConfigParameter("PicSetupUploadImageSleepTime", 2000, "多圖上傳功能上傳圖檔間隔", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        int PicSetupUploadImageSleepTime { get; set; }

        [ConfigParameter("SellerContractFtpUri", "ftp://hftp.17life.com", "賣家合約上傳FTP網址", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string SellerContractFtpUri { get; set; }

        [ConfigParameter("SellerContractIsFtp", false, "賣家合約是否上傳至FTP", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool SellerContractIsFtp { get; set; }

        [ConfigParameter("SellerContractVersionHouse", "", "宅配賣家合約版本", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string SellerContractVersionHouse { get; set; }

        [ConfigParameter("SellerContractVersionPpon", "", "在地賣家合約版本", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string SellerContractVersionPpon { get; set; }

        [ConfigParameter("EnableEvaluateCache", false, "啟用評價資料快取", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableEvaluateCache { get; set; }

        [ConfigParameter("EnableAppEvaluateSystem", false, "啟用App填評價功能", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableAppEvaluateSystem { get; set; }

        [ConfigParameter("EnableSHA256Encode64To64Char", true, "字串SHA256加密，輸出64個字元(true:自製版,false:廠商版)", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableSHA256Encode64To64Char { get; set; }

        #region Sales
        [ConfigParameter("SaleGoogleCalendarAPIKey", "482673039178-cm9ri8hfrs9f2rjpuh74rahbv6371lkq.apps.googleusercontent.com", "業務系統Google Calendar API Key", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string SaleGoogleCalendarAPIKey { get; set; }

        [ConfigParameter("IsProposalOnMemory", true, "提案單資料是否抓暫存", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsProposalOnMemory { get; set; }

        /// <summary>
        /// 庫存不足通知信
        /// </summary>
        [ConfigParameter("VbsShortageMail", "elin@17life.com,chelsea_chang@17life.com,neo_tseng@17life.com,fausting_pan@17life.com", "庫存不足通知信", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string VbsShortageMail { get; set; }

        [ConfigParameter("IsConsignment", false, "是否使用小倉系統", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool IsConsignment { get; set; }

        #endregion

        #region PCP

        /// <summary>
        /// 店家請款超級紅利金可兌換額度
        /// </summary>
        [ConfigParameter("ExchangeSuperBounsAmount", 1000, "店家請款超級紅利金可兌換額度", ConfigParameterAttribute.ConfigCategories.PCP, ConfigParameterAttribute.ConfigTypes.option)]
        int ExchangeSuperBounsAmount { get; set; }

        /// <summary>
        /// 超級紅利金請款兌換比例
        /// </summary>
        [ConfigParameter("ExchangeSuperBounsRatio", 0.7, "超級紅利金請款兌換比例", ConfigParameterAttribute.ConfigCategories.PCP, ConfigParameterAttribute.ConfigTypes.option)]
        decimal ExchangeSuperBounsRatio { get; set; }

        /// <summary>
        /// 是否回傳demo用的資料
        /// </summary>
        [ConfigParameter("PCPApiDemo", true, "是否回傳demo用的資料", ConfigParameterAttribute.ConfigCategories.PCP, ConfigParameterAttribute.ConfigTypes.option)]
        bool PCPApiDemo { get; set; }

        /// <summary>
        /// PCP圖檔壓縮 image_type list
        /// </summary>
        [ConfigParameter("PcpCompressImageTypeList", "2|0.2,3|0.4,4|0.4", "PCP圖檔壓縮image_type|壓縮倍率(逗號分隔)", ConfigParameterAttribute.ConfigCategories.PCP)]
        string PcpCompressImageTypeList { get; set; }

        /// <summary>
        /// PCP產生測試核銷PCP交易Code所使用的UserId
        /// </summary>
        [ConfigParameter("PcpGenTestIdentityCodeUserId", 1111123306, "產生測試核銷PCP交易Code所使用的UserId", ConfigParameterAttribute.ConfigCategories.PCP)]
        int PcpGenTestIdentityCodeUserId { get; set; }

        /// <summary>
        /// PCP使用熟客卡舊有模式核銷的GroupId
        /// </summary>
        [ConfigParameter("PcpIdentityCodeGroupIdList", "1045", "使用熟客卡舊有模式核銷的GroupId", ConfigParameterAttribute.ConfigCategories.PCP)]
        string PcpIdentityCodeGroupIdList { get; set; }

        #endregion PCP

        #region Skm

        [ConfigParameter("IsEnableDealCategory", false, "是否開啟主商品分類功能", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool IsEnableDealCategory { get; set; }

        [ConfigParameter("IsEnableCashAndGiftCouponOnStore", false, "是否開啟電子抵用金與電子贈品禮券在精選優惠上", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool IsEnableCashAndGiftCouponOnStore { get; set; }

        [ConfigParameter("IsEnableCostByHq", false, "是否開啟歸帳於總公司功能", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool IsEnableCostByHq { get; set; }

        [ConfigParameter("IsEnableCRMUserGroup", false, "是否開啟策展CRM客群勾選功能", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool IsEnableCRMUserGroup { get; set; }
        
        [ConfigParameter("SkmPayTradeNoErrorAlertEmail", "skm@17life.com", "SKM PAY 交易序號錯誤通知email", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmPayTradeNoErrorAlertEmail { get; set; }

        [ConfigParameter("SkmWalletAuthToken", "3ccc47bb8ddf4a0e950f20ee9aeffffd_f93b5f5b4da13e18e85ff3b3039b84de", "SKM PAY Wallet Token", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmWalletAuthToken { get; set; }
        
        [ConfigParameter("SkmWalletUseStaticClient", false, "呼叫SKM Wallet 單一 Http Client，試著避免重複開關連線造成的資源損耗", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool SkmWalletUseStaticClient { get; set; }

        [ConfigParameter("SkmWalletSiteUrl", "https://spwalletest.17life.com/", "SKM PAY Wallet Site Url", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmWalletSiteUrl { get; set; }

        [ConfigParameter("SkmTurnCloudSiteUrl", "https://192.168.2.51", "SKM PAY Turn  Cloud Site Url", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmTurnCloudSiteUrl { get; set; }

        [ConfigParameter("SkmDealCurrentVerion", 1, "SKM建檔現行版本號", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmDealCurrentVerion { get; set; }

        [ConfigParameter("EnableSkmPay", false, "建檔後台是否開啟skm pay版本", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableSkmPay { get; set; }

        [ConfigParameter("SkmRsvData", "[{\"shopCode\":\"110\",\"url\":\"/groups/skm-TaipeiStation\"},{\"shopCode\":\"100\",\"url\":\"/groups/skm-nanxi\"},{\"shopCode\":\"150\",\"url\":\"/groups/skm-TaipeiTianmu\"},{\"shopCode\":\"160\",\"url\":\"/groups/skm-TaoyuanDayou\"},{\"shopCode\":\"170\",\"url\":\"/groups/skm-TaoyuanStation\"},{\"shopCode\":\"200\",\"url\":\"/groups/skm-taichung\"},{\"shopCode\":\"330\",\"url\":\"/groups/skm-ChiayiChuiyang\"},{\"shopCode\":\"310\",\"url\":\"/groups/skm-TainanZhongshan\"},{\"shopCode\":\"320\",\"url\":\"/groups/skm-tainanplace\"},{\"shopCode\":\"300\",\"url\":\"/groups/skm-KaohsiungSanduo\"},{\"shopCode\":\"350\",\"url\":\"/groups/skm-KaohsiungZuoying\"},{\"shopCode\":\"120\",\"url\":\"/groups/skm-xinyi\"}]", "SKM餐廳訂位分店位置", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmRsvData { get; set; }

        [ConfigParameter("SkmRsvUrl", "http://skm-inline-beta.s3-website-us-east-1.amazonaws.com", "SKM餐廳訂位網址", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmRsvUrl { get; set; }

        [ConfigParameter("SkmImages", "[{\"position\":\"homeLeftMiddle\",\"imgUrl\":\"https://www.17life.com/Images/17P/skm_app/banner/banner_1.jpg\"},{\"position\":\"homeRightTop\",\"imgUrl\":\"https://www.17life.com/Images/17P/skm_app/banner/banner_2.jpg\"},{\"position\":\"homeRightBottom\",\"imgUrl\":\"https://www.17life.com/Images/17P/skm_app/banner/banner_3.jpg\"}]", "SKM圖檔", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmImages { get; set; }

        [ConfigParameter("MaxCrmDataCount", 3, "CRM 專屬推薦最多則數", ConfigParameterAttribute.ConfigCategories.Skm)]
        int MaxCrmDataCount { get; set; }

        [ConfigParameter("MaxCrmExhibitionCount", 2, "CRM 策展最多則數", ConfigParameterAttribute.ConfigCategories.Skm)]
        int MaxCrmExhibitionCount { get; set; }

        [ConfigParameter("CheckPrizeOverdraw", false, "啟動抽獎預防超抽", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool CheckPrizeOverdraw { get; set; }

        [ConfigParameter("SkmAppId", "226311077158321", "新光APP Id", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmAppId { get; set; }

        [ConfigParameter("SkmWebserviceUrl", "http://124.9.53.111/AppwebService/WSUmall2.asmx", "新光環友WebService", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmWebserviceUrl { get; set; }

        [ConfigParameter("SkmWsNamespace", "WSUmall2Soap", "新光環友WebService Namespace", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmWsNamespace { get; set; }

        [ConfigParameter("SkmWsClassName", "WSUmall2", "新光環友WebService Class Name", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmWsClassName { get; set; }

        [ConfigParameter("SkmWsMethod", "UmallService", "新光環友WebService Method", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmWsMethod { get; set; }

        [ConfigParameter("IsValidateSkmMember", false, "驗證新光會員是否還有效", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool IsValidateSkmMember { get; set; }

        [ConfigParameter("MakeSkmMemberValidateFail", false, "驗證新光會員時模擬失敗", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool MakeSkmMemberValidateFail { get; set; }

        [ConfigParameter("SkmCityId", 450, "新光 CityId", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmCityId { get; set; }

        [ConfigParameter("SkmCategordId", 185, "新光 CategoryId", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmCategordId { get; set; }

        [ConfigParameter("SkmDefaultDealCategordId", 2297, "新光預設精選優惠類別編號 SkmDealCategordId", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmDefaultDealCategordId { get; set; }

        [ConfigParameter("SkmFayaqueCategoryId", 2204, "法雅客 CategoryId", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmFayaqueCategoryId { get; set; }

        [ConfigParameter("SkmBeaconDefaultMsg", "精心推薦 {0} ，限時驚喜，快到指定專櫃了解詳情！", "新光 Beacon預設訊息", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmBeaconDefaultMsg { get; set; }

        [ConfigParameter("SkmBeaconNotificationDailyLimit", "3", "新光 Beacon推播訊息-每日上限", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmBeaconNotificationDailyLimit { get; set; }

        [ConfigParameter("SkmBeaconMsgBoxDailyLimit", "3", "新光 Beacon非推播訊息(messagebox)", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmBeaconMsgBoxDailyLimit { get; set; }

        [ConfigParameter("SkmBeaconNotificationTimeSpanMInutes", "60", "新光 Beacon推播訊息-觸發時間間隔", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmBeaconNotificationTimeSpanMinutes { get; set; }

        [ConfigParameter("SkmBeaconMsgBoxTimeSpanMinutes", "30", "新光 Beacon非推播訊息(messagebox)-觸發時間間隔", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmBeaconMsgBoxTimeSpanMinutes { get; set; }

        [ConfigParameter("SkmBeaconChecked", true, "新光 Beacon對應訊息", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool SkmBeaconChecked { get; set; }

        [ConfigParameter("SkmAgreementOfCmsContentId", 1579, "新光共同行銷會員條款於編輯器的ID", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmAgreementOfCmsContentId { get; set; }

        [ConfigParameter("SkmPublicImag", "{\"2203\":\"ANR001.jpg\",\"2204\":\"ANR002.jpg\",\"2205\":\"ANR003.png\",\"2206\":\"ANR004.jpg\",\"2235\":\"ANR005.jpg\"}", "新光形象圖", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmPublicImag { get; set; }

        [ConfigParameter("SkmQrCodeExpireTime", 12, "新光實際QrCode倒數分鐘數", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmQrCodeExpireTime { get; set; }

        [ConfigParameter("SkmQrCodeAppExpireTime", 10, "新光 APP Qr 倒數分鐘數", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmQrCodeAppExpireTime { get; set; }

        [ConfigParameter("SkmDealSales", "SKM@17life.com.tw", "新光上檔用業務", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmDealSales { get; set; }

        [ConfigParameter("IsEnableCouponSettlement", false, "是否啟用清算JOB", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool IsEnableCouponSettlement { get; set; }

        [ConfigParameter("SkmDealBuildEmail", "", "新光提品通知(分號分格)", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmDealBuildEmail { get; set; }

        [ConfigParameter("SkmOldDeal", "2016/3/6", "不顯示舊的提品", ConfigParameterAttribute.ConfigCategories.Skm)]
        DateTime SkmOldDeal { get; set; }

        [ConfigParameter("SkmBackendOauth", "9bd66a6350774338a4bf736a2d4b9d72_55ed662e598d4e90dcc7e3c6a086f86c", "新光 APP Backend Oauth Token ", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmBackendOauth { get; set; }

        [ConfigParameter("SkmDeliveryCategoryId", "100130", "新光M宅配分類Id", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmDeliveryCategoryId { get; set; }

        [ConfigParameter("SkmDealPageSize", "30", "新光檔次分頁大小", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmDealPageSize { get; set; }

        [ConfigParameter("SkmRootSellerGuid", "5d0d750a-9999-41f4-907b-c95dcbe2de13", "新光三越總公司Seller Guid", ConfigParameterAttribute.ConfigCategories.Skm)]
        Guid SkmRootSellerGuid { get; set; }

        [ConfigParameter("SkmDefaultSellerGuid", "b48f23f0-a7ff-4245-967d-43d20bbe71d8", "新光三越Beacon下拉預設Seller Guid", ConfigParameterAttribute.ConfigCategories.Skm)]
        Guid SkmDefaultSellerGuid { get; set; }

        [ConfigParameter("SkmBuyGetFreeIsShow", false, "是否顯示買幾送幾", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool SkmBuyGetFreeIsShow { get; set; }
        [ConfigParameter("SkmNoInvoicesIsShow", true, "是否顯示買a送b只核銷不出發票", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool SkmNoInvoicesIsShow { get; set; }

        [ConfigParameter("SkmHQCategories", "2235,2236,2237", "總公司權限擁有的類別(多筆請用逗號分隔)", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmHQCategories { get; set; }

        [ConfigParameter("SkmAppHomeDisableCategory", "", "APP首頁版型設定不可新增的類別(多筆請用逗號分隔)", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmAppHomeDisableCategory { get; set; }

        [ConfigParameter("SkmShoppeDisplayNameCheckDate", "2016/7/28", "新光新舊檔次的判斷日", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmShoppeDisplayNameCheckDate { get; set; }

        [ConfigParameter("NotShowSkmDealsInWeb", true, "新光檔次強制不顯示在網頁上", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool NotShowSkmDealsInWeb { get; set; }

        [ConfigParameter("UsingLowLoadingBeaconMethod", true, "使用低負載Beacon方法", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool UsingLowLoadingBeaconMethod { get; set; }

        [ConfigParameter("SkmDealDisableCategory", "2216", "檔次列表不可新增的類別", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmDealDisableCategory { get; set; }

        [ConfigParameter("SkmRootPowerSellerGuid", "5d0d750a-9999-41f4-907b-c95dcbe2de13,43E4CC32-3EF9-4461-AFF8-A2D9D73140D0", "新光三越總公司權限Seller Guid", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmRootPowerSellerGuid { get; set; }
        [ConfigParameter("EnableNewSKMAppDealStyle", false, "新光專區新版App優惠活動Style切換", ConfigParameterAttribute.ConfigCategories.Mobile)]
        bool EnableNewSKMAppDealStyle { get; set; }

        [ConfigParameter("EnableSKMNewBeacon", false, "新光專區新版Beacon切換", ConfigParameterAttribute.ConfigCategories.Mobile)]
        bool EnableSKMNewBeacon { get; set; }

        [ConfigParameter("EnableSKMBeaconLimit", true, "新光Beacon限制開關", ConfigParameterAttribute.ConfigCategories.Mobile)]
        bool EnableSKMBeaconLimit { get; set; }

        [ConfigParameter("SKMBeaconMessageDueDays", "-90", "新光Beacon訊息保留天數", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SKMBeaconMessageDueDays { get; set; }

        [ConfigParameter("SkmLatestActivityTakeCnt", "20", "新光最新活動取得數量", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmLatestActivityTakeCnt { get; set; }

        [ConfigParameter("SkmExpiredDataMonth", "3", "優惠與收藏下檔多久月份算過期", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmExpiredDataMonth { get; set; }

        [ConfigParameter("SkmEcExpiredDataMonth", "12", "EC訂單多久月份算過期", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmEcExpiredDataMonth { get; set; }

        [ConfigParameter("EnableSKMActivityStyleNewImgSize", false, "新光最新活動圖檔新尺寸770X330", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableSKMActivityStyleNewImgSize { get; set; }

        [ConfigParameter("EnableCallBurningWebService", true, "是否呼叫新光燒點API", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableCallBurningWebService { get; set; }

        [ConfigParameter("EnableSkmBurning", false, "新光後台3.7版燒點切換開關(為false時,skm帳號還是3.6版,切換後皆為3.7)", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableSkmBurning { get; set; }

        [ConfigParameter("EnableSkmPrizeDraw", false, "新光開啟活動抽獎功能", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableSkmPrizeDraw { get; set; }

        [ConfigParameter("EnableQueryProductCode", true, "開啟提品填商品條碼時自動帶入產品資訊", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableQueryProductCode { get; set; }

        [ConfigParameter("SkmEnablePrizeDealCallBurningWs", false, "開啟抽獎商品環友燒點機制", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool SkmEnablePrizeDealCallBurningWs { get; set; }

        [ConfigParameter("SkmEnablePrizeEventTestMode", false, "開啟抽獎商品燒點燒點測試(繞過WS)", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool SkmEnablePrizeEventTestMode { get; set; }

        [ConfigParameter("SkmEnableSendPrizeData", false, "開啟傳送中獎清冊(關閉則繞過WS)", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool SkmEnableSendPrizeData { get; set; }

        [ConfigParameter("SkmGiftGetFromUmall", false, "新光後台管理查詢訂單贈品啟用查詢Umall WS", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool SkmGiftGetFromUmall { get; set; }

        [ConfigParameter("ApposPauseRefundStartTime", "23:00", "Appos暫停退貨交易開始時間(HH:mm)", ConfigParameterAttribute.ConfigCategories.Skm)]
        string ApposPauseRefundStartTime { get; set; }

        [ConfigParameter("ApposPauseRefundEndTime", "23:30", "Appos暫停退貨交易結束時間(HH:mm)", ConfigParameterAttribute.ConfigCategories.Skm)]
        string ApposPauseRefundEndTime { get; set; }
        [ConfigParameter("SkmPayPreDayClearPosExecDate", "", "執行上傳銷售總額資訊的銷售日(yyyy/MM/dd)", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmPayPreDayClearPosExecDate { get; set; }


        [ConfigParameter("SKMDeepLinkStartDate", "3000/1/1 12:00:00", "SKM Deep Link 顯示開始時間", ConfigParameterAttribute.ConfigCategories.Skm)]
        DateTime SKMDeepLinkStartDate { get; set; }
        [ConfigParameter("SKMDeepLinkEndDate", "3000/1/2 12:00:00", "SKM Deep Link 顯示結束時間", ConfigParameterAttribute.ConfigCategories.Skm)]
        DateTime SKMDeepLinkEndDate { get; set; }
        [ConfigParameter("SKMDeepLinkUrl", "", "SKM Deep Link連結網址", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SKMDeepLinkUrl { get; set; }
        [ConfigParameter("SKMDeepLinkImage", "", "SKM Deep Link 圖檔網址", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SKMDeepLinkImage { get; set; }
        [ConfigParameter("SkmGetPaymentResultAccseeLimite", 30, "APP 呼叫 GetPaymentResult 上限次數", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmGetPaymentResultAccseeLimite { get; set; }
        [ConfigParameter("SkmPayOTPTimeoutSeconds", 90, "Skm Pay OTP 驗證逾時秒數", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmPayOTPTimeoutSeconds { get; set; }
        [ConfigParameter("SKMPrizeItemSafetyStock", 10, "抽獎活動獎品安全庫存量(數字為百分比)", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SKMPrizeItemSafetyStock { get; set; }
        [ConfigParameter("SkmRefreshDealSalesInfoMode", 3, "SKM 更新DealSalesInfo 方式 (1:TempTable, 2:StoredProcedure 3:PayEventsOnRefresh)", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmRefreshDealSalesInfoMode { get; set; }
        [ConfigParameter("EnableSKMListLikeQuery", false, "SKM 檔次列表 檔次名稱查詢功能開關", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableSKMListLikeQuery { get; set; }
        [ConfigParameter("ResendPrizeCouponEventId", 0, "Skm 重新發送抽獎活動選擇的館別給Umall之活動編號", ConfigParameterAttribute.ConfigCategories.Skm)]
        int ResendPrizeCouponEventId { get; set; }
        [ConfigParameter("IsCheckSkmDealTimeSlotStatusWhenMakeOrder", true, "Skm 線上購買的時候是否檢查檔次已隱藏", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool IsCheckSkmDealTimeSlotStatusWhenMakeOrder { get; set; }
        [ConfigParameter("EnableAdList", true, "Skm 蓋板廣告設定功能是否開啟", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableAdList { get; set; }
        [ConfigParameter("EnableSkmDealRepeatPurchase", true, "Skm 每人每日/每個檔次限次功能是否開啟", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableSkmDealRepeatPurchase { get; set; }
        [ConfigParameter("NavigationShops", "121,122,123,124", "開啟導航的館別", ConfigParameterAttribute.ConfigCategories.Skm)]
        string NavigationShops { get; set; }

        /// <summary>
        /// 新光推播時間
        /// </summary>
        [ConfigParameter("SkmCashTrustLogExpectSendTime", "110000", "新光推播時間", ConfigParameterAttribute.ConfigCategories.System)]
        string SkmCashTrustLogExpectSendTime { get; set; }

        /// <summary>
        /// 新光即將逾期憑證天數設定
        /// </summary>
        [ConfigParameter("SkmCashTrustLogEndDays", 2, "新光即將逾期憑證提前通知天數設定", ConfigParameterAttribute.ConfigCategories.System)]
        int SkmCashTrustLogEndDays { get; set; }
        /// <summary>
        /// 新光即預購憑證開始天數設定
        /// </summary>
        [ConfigParameter("SkmCashTrustLogStartDays", 1, "新光即預購憑證提前通知天數設定", ConfigParameterAttribute.ConfigCategories.System)]
        int SkmCashTrustLogStartDays { get; set; }

        /// <summary>
        /// 顯示後台精選優惠原價
        /// </summary>
        [ConfigParameter("DisplaySkmPayDealInfoOriPrice", false, "顯示後台精選優惠原價", ConfigParameterAttribute.ConfigCategories.System)]
        bool DisplaySkmPayDealInfoOriPrice { get; set; }

        /// <summary>
        /// 新光信用卡卡樣開關
        /// </summary>
        [ConfigParameter("EnableSkmCreditCardCategory", false, "新光信用卡卡樣開關", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableSkmCreditCardCategory { get; set; }
        /// <summary>
        /// 美麗台網址
        /// </summary>
        [ConfigParameter("SkmBeautyStageUrl", "https://www.beautystage.com.tw/skm/", "美麗台網址", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmBeautyStageUrl { get; set; }
        /// <summary>
        /// 是否啟用複製DeepLink功能
        /// </summary>
        [ConfigParameter("EnableCopyDeepLink", "true", "是否啟用複製DeepLink功能", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableCopyDeepLink { get; set; }
        /// <summary>
        /// 預購&即期憑證推播SiteUrl
        /// </summary>
        [ConfigParameter("SkmPushSiteUrl", "http://124.9.53.114:8088/", "預購&即期憑證推播SiteUrl", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmPushSiteUrl { get; set; }
        /// <summary>
        /// 預購&即期憑證推播Route
        /// </summary>
        [ConfigParameter("SkmPushRoute", "push.aspx", "預購&即期憑證推播SkmPushRoute", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmPushRoute { get; set; }
        /// <summary>
        /// 預購&即期憑證推播XSignature
        /// </summary>
        [ConfigParameter("SkmPushXSignature", "sha1=xK4AIj0SILLguV1lUHkIHeLbjFb1yM9adbN2jdyRj+I=", "預購&即期憑證推播XSignature", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SkmPushXSignature { get; set; }
        /// <summary>
        /// 是否開啟自訂版位功能
        /// </summary>
        [ConfigParameter("EnableCustomizedBoardList", false, "是否開啟自訂版位功能", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableCustomizedBoardList { get; set; }

        [ConfigParameter("ShowCRMZeroOption", false, "是否開啟CRM零級會員選項", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool ShowCRMZeroOption { get; set; }

        [ConfigParameter("ShowCutomizedBoardPrizeDrawOption", false, "是否開啟自訂版位抽獎類型選項", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool ShowCutomizedBoardPrizeDrawOption { get; set; }

        [ConfigParameter("EnableCheckDealImageLength", false, "是否檢查精選優惠圖片大小", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableCheckDealImageLength { get; set; }

        [ConfigParameter("EnableCheckDealImageLengthDate", "2019-11-06", "是否檢查精選優惠圖片大小新舊檔次日期", ConfigParameterAttribute.ConfigCategories.Skm)]
        string EnableCheckDealImageLengthDate { get; set; }

        [ConfigParameter("EnableInstallment", false, "是否開啟分期功能", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableInstallment { get; set; }
        [ConfigParameter("EnableCheckDealInstallProperty", false, "是否檢測檔次分期屬性", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableCheckDealInstallProperty { get; set; }

        [ConfigParameter("SkmCacheDataMinute", 5, "SKM快取分鐘數", ConfigParameterAttribute.ConfigCategories.Skm)]
        int SkmCacheDataMinute { get; set; }

        [ConfigParameter("SkmPrizeEventCustomEndDate", "2020/2/20 21:59:59", "SKM強制抽紅包活動截止日期", ConfigParameterAttribute.ConfigCategories.Skm)]
        DateTime SkmPrizeEventCustomEndDate { get; set; }

        [ConfigParameter("IsEnableForceCoverAd", false, "強制紅包模組邏輯適用在汽車抽獎上", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool IsEnableForceCoverAd { get; set; }


        #endregion Skm

        #region HiLife
        [ConfigParameter("EnableHiLifeDealSetup", false, "啟用萊爾富檔次設定", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableHiLifeDealSetup { get; set; }

        [ConfigParameter("EnableHiLifeApiTransfer", false, "啟用與萊爾富Api串接", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableHiLifeApiTransfer { get; set; }

        [ConfigParameter("HiLifeFixedFunctionPincode", "86010026", "萊爾富第一段固定條碼", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        string HiLifeFixedFunctionPincode { get; set; }

        [ConfigParameter("HiLifeTransferServerDomain", "http://60.251.154.166:88/", "17Life轉拋Server(專線跟萊爾富溝通)", ConfigParameterAttribute.ConfigCategories.APP, ConfigParameterAttribute.ConfigTypes.option)]
        string HiLifeTransferServerDomain { get; set; }

        #endregion

        #region Mobile
        [ConfigParameter("IsMobileShowCanBeUsedImmediatelyChannel", false, "行動版即買即用啟用", ConfigParameterAttribute.ConfigCategories.Mobile)]
        bool IsMobileShowCanBeUsedImmediatelyChannel { get; set; }

        [ConfigParameter("IsMobileShowCanBeUsedImmediatelyChanneliOSVersion", "5.5.2", "行動版即買即用啟用iOS版號", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string IsMobileShowCanBeUsedImmediatelyChanneliOSVersion { get; set; }

        [ConfigParameter("IsMobileShowCanBeUsedImmediatelyChannelAndroidVersion", "5.5.132", "行動版即買即用啟用Android版號", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string IsMobileShowCanBeUsedImmediatelyChannelAndroidVersion { get; set; }
        
        [ConfigParameter("MobileChannel", "87,90,89,91,88,100001,148,3001", "行動版Channel設定", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string MobileChannel { get; set; }

        // 優惠活動:0  熱賣商品:1  注目商品:2
        [ConfigParameter("MobileChannelSKMDefaultDealTypes", "0", "新光專區預設撈取類別", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string MobileChannelSKMDefaultDealTypes { get; set; }

        [ConfigParameter("IsShowSKMFilter", false, "新光專區Filter顯示", ConfigParameterAttribute.ConfigCategories.Mobile)]
        bool IsShowSKMFilter { get; set; }

        [ConfigParameter("EnableFamiCoffee", true, "是否啟用全家寄杯功能", ConfigParameterAttribute.ConfigCategories.Mobile)]
        bool EnableFamiCoffee { get; set; }

        [ConfigParameter("EnableFamiCoffeeIcon", true, "是否啟用全家寄杯Icon", ConfigParameterAttribute.ConfigCategories.Mobile)]
        bool EnableFamiCoffeeIcon { get; set; }

        [ConfigParameter("EnableKindDealInApp", false, "app開啟公益檔", ConfigParameterAttribute.ConfigCategories.Mobile)]
        bool EnableKindDealInApp { get; set; }

        [ConfigParameter("LoginPath", "/m/login", "m版登入", ConfigParameterAttribute.ConfigCategories.Mobile)]
        string LoginUrl { get; set; }

        [ConfigParameter("EnableDepositCoffee", false, "是否啟用廠商序號寄杯功能", ConfigParameterAttribute.ConfigCategories.Mobile)]
        bool EnableDepositCoffee { get; set; }

        [ConfigParameter("PushOrderCpaWithInHour", 2, "追蹤幾小時內的推播成立訂單(0則不開啟)", ConfigParameterAttribute.ConfigCategories.Mobile)]
        int PushOrderCpaWithInHour { get; set; }

        #endregion

        #region Lucene Search
        [ConfigParameter("dealCountLimit", 1000, "搜尋後顯示的檔次筆數", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        int dealCountLimit { get; set; }

        [ConfigParameter("EnabledSearchExcludeSKMChannel", true, "受否搜尋功能排除新光頻道檔次", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnabledSearchExcludeSKMChannel { get; set; }

        #endregion

        #region Debug用(錯誤修正後會移除)
        [ConfigParameter("EnableElmahLogUriIsEmpty", false, "是否啟用elmah偵錯「Invalid URI: The URI is empty.」", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnableElmahLogUriIsEmpty { get; set; }
        #endregion

        #region MGM

        [ConfigParameter("MGMEnabled", false, "MGM是否開啟", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool MGMEnabled { get; set; }

        [ConfigParameter("MGMGiftCount", 4, "列表夜裡物數量", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        int MGMGiftCount { get; set; }

        [ConfigParameter("MGMGiftLockDay", 3, "禮物鎖定天數", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        int MGMGiftLockDay { get; set; }

        [ConfigParameter("MGMGiftGetUrl", "gift.17life.com", "", ConfigParameterAttribute.ConfigCategories.System)]
        string MGMGiftGetUrl { get; set; }

        #endregion

        #region 網站自動化測試
        [ConfigParameter("ExecuteAutomatedTestingAPI", "http://iceman.17life.com:1765/api/TestRunner/RunAcceptanceTests", " 執行自動化測試的API", ConfigParameterAttribute.ConfigCategories.AutoTest, ConfigParameterAttribute.ConfigTypes.option)]
        string ExecuteAutomatedTestingAPI { get; set; }
        [ConfigParameter("AccountForAutoTest", "test17life@gmail.com", "自動化測試的會員帳號", ConfigParameterAttribute.ConfigCategories.AutoTest, ConfigParameterAttribute.ConfigTypes.option)]
        string AccountForAutoTest { get; set; }
        [ConfigParameter("AccountForAutoTest", "1qaz2wsx#EDC$RFV", "自動化測試用的萬用驗證碼", ConfigParameterAttribute.ConfigCategories.AutoTest, ConfigParameterAttribute.ConfigTypes.option)]
        string GeneralForgotPasswordCaptcha { get; set; }
        #endregion

        #region 會員 Member

        [ConfigParameter("EnableCaptcha", true, "開啟登入時的Captcha驗證", ConfigParameterAttribute.ConfigCategories.None, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableCaptcha { get; set; }

        #endregion

        [ConfigParameter("EnabledRefundOrExchangeEditReceiverInfo", true, "啟用退換貨收件地址管理功能", ConfigParameterAttribute.ConfigCategories.None, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnabledRefundOrExchangeEditReceiverInfo { get; set; }

        [ConfigParameter("EnabledExchangeProcess", true, "啟用換貨管理功能", ConfigParameterAttribute.ConfigCategories.None, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnabledExchangeProcess { get; set; }

        [ConfigParameter("MobileIndexEventIconName", "結帳86折", "M版首頁活動Icon名稱", ConfigParameterAttribute.ConfigCategories.System)]
        string MobileIndexEventIconName { get; set; }

        [ConfigParameter("StopRefundQuantityCalculateDate", "2016/11/21 00:00:00", "停止退貨數量計算日期", ConfigParameterAttribute.ConfigCategories.None, ConfigParameterAttribute.ConfigTypes.option)]
        DateTime StopRefundQuantityCalculateDate { get; set; }

        [ConfigParameter("FamiportCityId", 490, "全家廠商CityId", ConfigParameterAttribute.ConfigCategories.System)]
        int FamiportCityId { get; set; }

        [ConfigParameter("FamilyNetDealSales", "joecy_jain@17life.com.tw", "全家上檔用業務", ConfigParameterAttribute.ConfigCategories.System)]
        string FamilyNetDealSales { get; set; }

        [ConfigParameter("IsEveryDayNewDeals", false, "是否開啟每日下殺自動壓圖", ConfigParameterAttribute.ConfigCategories.System)]
        bool IsEveryDayNewDeals { get; set; }

        [ConfigParameter("EveryDayNewDealsCategoryId", 844, "每日下殺分類ID", ConfigParameterAttribute.ConfigCategories.System)]
        int EveryDayNewDealsCategoryId { get; set; }

        #region 商家系統
        [ConfigParameter("IsEnableVbsBindAccount", false, "商家綁定17life會員帳號功能", ConfigParameterAttribute.ConfigCategories.System)]
        bool IsEnableVbsBindAccount { get; set; }

        [ConfigParameter("VbsTemporaryPasswordHourLimit", 240, "商家系統忘記密碼過期分鐘數", ConfigParameterAttribute.ConfigCategories.System)]
        int VbsTemporaryPasswordHourLimit { get; set; }
        [ConfigParameter("VbsShipRule2k18", false, "商家系統出貨規格調整", ConfigParameterAttribute.ConfigCategories.System)]
        bool VbsShipRule2k18 { get; set; }

        [ConfigParameter("ForcedVerificationAccount", "sys@forced", "強制核銷使用帳號", ConfigParameterAttribute.ConfigCategories.System)]
        string ForcedVerificationAccount { get; set; }

        [ConfigParameter("IsEnableVbsNewShipFile", true, "是否啟用新版商家出貨匯出匯入(華歌爾企劃相關)", ConfigParameterAttribute.ConfigCategories.System)]
        bool IsEnableVbsNewShipFile { get; set; }

        [ConfigParameter("IsEnableVbsNewUi", true, "是否啟用商家系統新UI", ConfigParameterAttribute.ConfigCategories.System)]
        bool IsEnableVbsNewUi { get; set; }

        [ConfigParameter("VbsGsaNotifyStartDate", "2/21/2018", "GSA開始通知時間", ConfigParameterAttribute.ConfigCategories.System)]
        string VbsGsaNotifyStartDate { get; set; }

        [ConfigParameter("VbsGsaNotifyEndDate", "5/1/2018", "GSA結束通知時間", ConfigParameterAttribute.ConfigCategories.System)]
        string VbsGsaNotifyEndDate { get; set; }
        [ConfigParameter("DeliveryClosePop", "false", "20190828商家系統跳窗", ConfigParameterAttribute.ConfigCategories.System)]
        bool DeliveryClosePop { get; set; }

        #endregion

        [ConfigParameter("IsProvisionSwitchToSql", false, "權益說明是否從Mongo轉移到SQL", ConfigParameterAttribute.ConfigCategories.System)]
        bool IsProvisionSwitchToSql { get; set; }

        [ConfigParameter("EnableParallelTestGetDeductibleMemberPromotionDepositBetaVersion", true,
            "啟用GetDeductibleMemberPromotionDeposit平行測試", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnableParallelTestGetDeductibleMemberPromotionDepositBetaVersion { get; set; }

        [ConfigParameter("IsVbsProposalNewVersion", true, "商家提案單新版", ConfigParameterAttribute.ConfigCategories.System)]
        bool IsVbsProposalNewVersion { get; set; }

        [ConfigParameter("IsRemittanceFortnightly", false, "是否開啟雙周出帳", ConfigParameterAttribute.ConfigCategories.System)]
        bool IsRemittanceFortnightly { get; set; }

        [ConfigParameter("CreditcardFraudSeller", "2f0e5fbd-759e-4ed1-b33b-912e8f88f5f2,a69d4f71-8a17-40cd-8678-0f294ad9df39,b8a14b5f-ba04-4e03-a053-c44cb6a178e1,f2c604ea-9e5c-44e0-927e-548970dc2879,e72ed10d-0288-40af-a7b8-97b2e7413c87,8380bb7e-73f5-4963-a583-9c1c613e353e,b3ec5e08-ec2a-427b-9735-b4aacd134a21,e3949359-21ab-4870-9f34-5f16caa4e0ac,5d612c3a-52e2-4a3c-b22f-8ba616e75eab,1d79a799-39e9-47d2-b9a6-a3c104bc1618,88a38d4e-b3fe-46ec-9990-4935e9c149c2,2487796e-c148-4d0b-ba8d-1db776c84d5b,d690de5b-4950-46cc-826f-9798ce9dcf5b,6b9ff78d-4743-4853-b952-6a2b30b165b5", "購買風險賣家(;區隔)", ConfigParameterAttribute.ConfigCategories.System)]
        string CreditcardFraudSeller { get; set; }

        /// <summary>
        /// 提案單允許外連網站
        /// </summary>
        [ConfigParameter("ProposalImageAllowDomain", "www.17life.com;", "提案單允許外連網站", ConfigParameterAttribute.ConfigCategories.System)]
        string ProposalImageAllowDomain { get; set; }

        /// <summary>
        /// 是否檢查手機載具正確性
        /// </summary>
        [ConfigParameter("CheckPhoneCarrierValid", true, "是否檢查手機載具正確性", ConfigParameterAttribute.ConfigCategories.System)]
        bool CheckPhoneCarrierValid { get; set; }

        #region CustomerService
        [ConfigParameter("InSideUrl", "~/images/Service/InSide/", "", ConfigParameterAttribute.ConfigCategories.System)]
        string InSideUrl { get; set; }

        [ConfigParameter("OutSideUrl", "~/images/Service/OutSide/", "", ConfigParameterAttribute.ConfigCategories.System)]
        string OutSideUrl { get; set; }

        [ConfigParameter("OrderDetailV2LinkVisable", false, "啟用後台新版訂單明細連結", ConfigParameterAttribute.ConfigCategories.System)]
        bool OrderDetailV2LinkVisable { get; set; }
        #endregion

        #region 超取設定

        [ConfigParameter("ISPOAuthToken", "d229473af6fc4002bbfd089ff845d870_810ddd4a3d88a7f59c5ec2436be736f3", "超取 OAuth Token", ConfigParameterAttribute.ConfigCategories.Isp)]
        string ISPOAuthToken { get; set; }

        [ConfigParameter("ISPApiUrl", "https://testasp.17life.com/api/isp/", "超取 API", ConfigParameterAttribute.ConfigCategories.Isp)]
        string ISPApiUrl { get; set; }

        [ConfigParameter("ISPFamilyFreightsChangeDate", "", "全家(日翊)超取運費成本異動日期(逗號分隔)", ConfigParameterAttribute.ConfigCategories.Isp)]
        string ISPFamilyFreightsChangeDate { get; set; }

        [ConfigParameter("ISPFamilyBeforeFreights", "", "全家(日翊)超取運費異動過成本(逗號分隔)", ConfigParameterAttribute.ConfigCategories.Isp)]
        string ISPFamilyBeforeFreights { get; set; }

        [ConfigParameter("ISPFamilyNowFreights", "42", "全家(日翊)超取運費目前成本", ConfigParameterAttribute.ConfigCategories.Isp)]
        int ISPFamilyNowFreights { get; set; }

        [ConfigParameter("ISPSevenFreightsChangeDate", "", "統一超取運費成本異動日期(逗號分隔)", ConfigParameterAttribute.ConfigCategories.Isp)]
        string ISPSevenFreightsChangeDate { get; set; }

        [ConfigParameter("ISPSevenBeforeFreights", "", "統一超取運費異動過成本(逗號分隔)", ConfigParameterAttribute.ConfigCategories.Isp)]
        string ISPSevenBeforeFreights { get; set; }

        [ConfigParameter("ISPSevenNowFreights", "47", "統一超取運費目前成本", ConfigParameterAttribute.ConfigCategories.Isp)]
        int ISPSevenNowFreights { get; set; }

        [ConfigParameter("FamilyIspPromoTips", "", "全家超商取貨推薦提示", ConfigParameterAttribute.ConfigCategories.Isp)]
        string FamilyIspPromoTips { get; set; }

        [ConfigParameter("SevenIspPromoTips", "", "7-11超商取貨推薦提示", ConfigParameterAttribute.ConfigCategories.Isp)]
        string SevenIspPromoTips { get; set; }

        [ConfigParameter("FamilyIspShipCompanyId", 55, "全家物流代號", ConfigParameterAttribute.ConfigCategories.Isp)]
        int FamilyIspShipCompanyId { get; set; }

        [ConfigParameter("FamilyIspWebSide", "https://www.famiport.com.tw/Web_Famiport/page/process.aspx", "全家物流查詢", ConfigParameterAttribute.ConfigCategories.Isp)]
        string FamilyIspWebSide { get; set; }

        [ConfigParameter("SevenIspShipCompanyId", 56, "7-11物流代號", ConfigParameterAttribute.ConfigCategories.Isp)]
        int SevenIspShipCompanyId { get; set; }

        [ConfigParameter("SevenIspWebSide", "https://eservice.7-11.com.tw/E-Tracking/search.aspx", "7-11物流查詢", ConfigParameterAttribute.ConfigCategories.Isp)]
        string SevenIspWebSide { get; set; }

        [ConfigParameter("AllowBidsForOnlineTest", "", "允許超取的Bids，多檔以逗號隔開，上線後局部開放測試。如為減號，全不允許，當上線後發生問題要全面禁用。如為空值，依檔次自己的設定為準，即上線時的狀況。", ConfigParameterAttribute.ConfigCategories.Isp)]
        string AllowBidsForOnlineTest { get; set; }

        [ConfigParameter("EnableFamiMap", false, "全家超商取貨啟用電子地圖(正式站應為true)", ConfigParameterAttribute.ConfigCategories.Isp)]
        bool EnableFamiMap { get; set; }

        [ConfigParameter("EnableFamiMapTestMode", false, "全家電子地圖測試模式", ConfigParameterAttribute.ConfigCategories.Isp)]
        bool EnableFamiMapTestMode { get; set; }

        [ConfigParameter("EnableSevenMap", false, "超商取貨啟用7-11電子地圖(正式站應為true)", ConfigParameterAttribute.ConfigCategories.Isp)]
        bool EnableSevenMap { get; set; }

        [ConfigParameter("EnableSevenIsp", false, "啟用7-11超商取貨", ConfigParameterAttribute.ConfigCategories.Isp)]
        bool EnableSevenIsp { get; set; }

        [ConfigParameter("EnableCancelPaidByIspOrder", "false", "是否啟用超付取消訂單功能", ConfigParameterAttribute.ConfigCategories.Isp)]
        bool EnableCancelPaidByIspOrder { get; set; }

        [ConfigParameter("AllowsIspReturnForShipping", "false", "允許超付訂單於運送中可申請退貨", ConfigParameterAttribute.ConfigCategories.Isp)]
        bool AllowsIspReturnForShipping { get; set; }

        [ConfigParameter("SevenEmapUrl", "https://ec.shopping7.com.tw/ec3gmap/emap/eServiceMap.php", "超取電子地圖Url", ConfigParameterAttribute.ConfigCategories.Isp)]
        string SevenEmapUrl { get; set; }

        [ConfigParameter("RedirectSevenMap", true, "中繼頁是否導去7-11 e-Map", ConfigParameterAttribute.ConfigCategories.Isp)]
        bool RedirectSevenMap { get; set; }

        [ConfigParameter("FamilyParentId", "682", "全家母代碼", ConfigParameterAttribute.ConfigCategories.Isp)]
        int FamilyParentId { get; set; }

        [ConfigParameter("SevenParentId", "829", "7-11母代碼", ConfigParameterAttribute.ConfigCategories.Isp)]
        int SevenParentId { get; set; }

        [ConfigParameter("ApplyPopout", false, "7-11週期不一致跳窗", ConfigParameterAttribute.ConfigCategories.Isp)]
        bool ApplyPopout { get; set; }

        #endregion

        #region 連結設定

        [ConfigParameter("RedirectUrlToEvent1111", "https://www.17life.com/images/17P/active/20171101/m_campaign_1101.html", "設定 Event1111 連結", ConfigParameterAttribute.ConfigCategories.System)]
        string RedirectUrlToEvent1111 { get; set; }

        #endregion

        #region Marketing 行銷設定

        /// <summary>
        /// Google gtag 17Life廠商代碼
        /// </summary>
        [ConfigParameter("GoogleGtagSendToData", "", "Google gtag 17Life廠商代碼", ConfigParameterAttribute.ConfigCategories.Marketing)]
        string GoogleGtagSendToData { get; set; }

        /// <summary>
        /// Google gtag 17Life廠商代碼(Conversion data)
        /// </summary>
        [ConfigParameter("GoogleGtagConversionSendToData", "", "Google gtag 17Life廠商代碼(Conversion Data)", ConfigParameterAttribute.ConfigCategories.Marketing)]
        string GoogleGtagConversionSendToData { get; set; }

        /// <summary>
        /// Google gtag UrAD 廠商代碼(Conversion data)
        /// </summary>
        [ConfigParameter("UrADGoogleGtagConversionSendToData", "", "Google gtag UrAD 廠商代碼(Conversion Data)", ConfigParameterAttribute.ConfigCategories.Marketing)]
        string UrADGoogleGtagConversionSendToData { get; set; }

        [ConfigParameter("YahooProjectId", "", "Yahoo 埋碼 Project Id", ConfigParameterAttribute.ConfigCategories.Marketing)]
        string YahooProjectId { get; set; }

        [ConfigParameter("YahooPixelId", "", "Yahoo 埋碼 Pixel Id", ConfigParameterAttribute.ConfigCategories.Marketing)]
        string YahooPixelId { get; set; }

        [ConfigParameter("ShopBackCookieExpires", 5, "ShopBakck 的逾時天數", ConfigParameterAttribute.ConfigCategories.System)]
        int ShopBackCookieExpires { get; set; }

        //[ConfigParameter("EnableLastClick", true, "是否開啟Last Click分潤", ConfigParameterAttribute.ConfigCategories.System)]
        //bool EnableLastClick { get; set; }

        [ConfigParameter("ShopbackApiUrl", "http://shopback.go2cloud.org/aff_lsr", "丟訂單給Shopback呼叫的API", ConfigParameterAttribute.ConfigCategories.System)]
        string ShopbackApiUrl { get; set; }

        [ConfigParameter("ShopbackOrderAPIOfferId", "1694", "廠商代碼", ConfigParameterAttribute.ConfigCategories.System)]
        string ShopbackOrderAPIOfferId { get; set; }

        [ConfigParameter("ShopbackValidationAPIOfferId", "2232", "廠商代碼", ConfigParameterAttribute.ConfigCategories.System)]
        string ShopbackValidationAPIOfferId { get; set; }

        [ConfigParameter("ShopbackCommissionElectronic", "2234", "Shopback分潤-家電", ConfigParameterAttribute.ConfigCategories.System)]
        int ShopbackCommissionElectronic { get; set; }

        [ConfigParameter("ShopbackCommission3C", "117", "Shopback分潤-3C", ConfigParameterAttribute.ConfigCategories.System)]
        int ShopbackCommission3C { get; set; }

        [ConfigParameter("ShopbackAffId", "1152", "Taiwan Code", ConfigParameterAttribute.ConfigCategories.System)]
        string ShopbackAffId { get; set; }

        [ConfigParameter("EnableShopBack", true, "是否開啟ShopBack", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableShopBack { get; set; }

        [ConfigParameter("ShopBackEffectiveShipDays", 20, "ShopBack 出貨後幾日後確定分潤", ConfigParameterAttribute.ConfigCategories.System)]
        int ShopBackEffectiveShipDays { get; set; }

        [ConfigParameter("ShopBackEffectiveOrderDays", 60, "ShopBack 訂單產生日起幾日確定分潤", ConfigParameterAttribute.ConfigCategories.System)]
        int ShopBackEffectiveOrderDays { get; set; }

        [ConfigParameter("EnabledOneAD", false, "啟用One AD 埋碼", ConfigParameterAttribute.ConfigCategories.Marketing)]
        bool EnabledOneAD { get; set; }

        [ConfigParameter("EnableAffiliates", true, "是否開啟Affiliates", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableAffiliates { get; set; }

        #endregion Marketing 行銷設定

        #region 騰雲發票相關(TurnCloud)
        /// <summary>
        /// 騰雲發票服務Url
        /// </summary>
        [ConfigParameter("TurnCloudServerSiteUrl", "http://124.9.53.111:8081", "騰雲發票服務Url", ConfigParameterAttribute.ConfigCategories.Skm)]

        string TurnCloudServerSiteUrl { get; set; }
        /// <summary>
        /// 是否真實呼叫騰雲發票服務
        /// </summary>
        [ConfigParameter("IsExecuteTurnCloudServer", true, "是否真實呼叫騰雲發票服務", ConfigParameterAttribute.ConfigCategories.Skm)]

        bool IsExecuteTurnCloudServer { get; set; }
        /// <summary>
        /// 是否使用測試的會員編號與館號呼叫騰雲發票服務
        /// </summary>
        [ConfigParameter("IsUseTestVipNoAndShopCode", true, "是否使用測試的會員編號與館號呼叫騰雲發票服務", ConfigParameterAttribute.ConfigCategories.Skm)]

        bool IsUseTestVipNoAndShopCode { get; set; }
        /// <summary>
        /// 騰雲發票服務HashKey精選優惠
        /// </summary>
        [ConfigParameter("TurnCloudServerHashKey", "", "騰雲發票服務HashKey", ConfigParameterAttribute.ConfigCategories.Skm)]
        string TurnCloudServerHashKey { get; set; }
        /// <summary>
        /// 騰雲發票服務HashKey停車支付(正式測試都相同)
        /// </summary>
        [ConfigParameter("TurnCloudServerParkingHashKey", "zx3ld91j3m4", "騰雲發票服務HashKey停車支付", ConfigParameterAttribute.ConfigCategories.Skm)]
        string TurnCloudServerParkingHashKey { get; set; }

        [ConfigParameter("EnableTurnCloudInvoice6X", false, "騰雲發票啟用6X櫃號", ConfigParameterAttribute.ConfigCategories.Skm)]
        bool EnableTurnCloudInvoice6X { get; set; }

        [ConfigParameter("NationalCreditCardCenterBankNo", "956", "聯合信用中心銀行代碼", ConfigParameterAttribute.ConfigCategories.Skm)]
        string NationalCreditCardCenterBankNo { get; set; }

        #endregion

        #region 雙11 Game
        /// <summary>
        /// 雙11遊戲 FB Secret 
        /// </summary>
        [ConfigParameter("GameFacebookApplicationSecret", "848332c974cf54b0e20dc896eaf978a1", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string GameFacebookApplicationSecret { get; set; }
        /// <summary>
        /// 雙11遊戲 FB Id  
        /// </summary>
        [ConfigParameter("GameFacebookApplicationId", "169978179852434", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string GameFacebookApplicationId { get; set; }

        /// <summary>
        /// 雙11遊戲 FB驗証導頁網址
        /// </summary>
        [ConfigParameter("GameAuthRedirectUri", "/api/Game/FBAuth", "雙11遊戲 FB驗証導頁網址", ConfigParameterAttribute.ConfigCategories.System)]
        string GameFbAuthRedirectUri { get; set; }
        /// <summary>
        /// 雙11遊戲 Line驗証導頁網址
        /// </summary>
        [ConfigParameter("GameLineAuthRedirectUri", "/api/Game/LineAuth", "雙11遊戲 Line驗証導頁網址", ConfigParameterAttribute.ConfigCategories.System)]
        string GameLineAuthRedirectUri { get; set; }
        /// <summary>
        /// 雙11遊戲 Line Secret 
        /// </summary>
        [ConfigParameter("GameLineApplicationSecret", "848332c974cf54b0e20dc896eaf978a1", " ", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string GameLineApplicationSecret { get; set; }
        /// <summary>
        /// 雙11遊戲 Line Secret 
        /// </summary>
        [ConfigParameter("GameLineId", "848332c974cf54b0e20dc896eaf978a22", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string GameLineId { get; set; }

        #endregion

        #region
        /// <summary>
        /// Line登入驗証導頁網址
        /// </summary>
        [ConfigParameter("LineAuthRedirectUri", "/api/NewMember/LineAuth", "Line驗証導頁網址", ConfigParameterAttribute.ConfigCategories.System)]
        string LineAuthRedirectUri { get; set; }

        /// <summary>
        /// Line Secret 
        /// </summary>
        [ConfigParameter("LineApplicationSecret", "37f687b14ac98c6fb81d233d86171ee3", " ", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string LineApplicationSecret { get; set; }

        [ConfigParameter("LineApplicationId", "1548269043", "", ConfigParameterAttribute.ConfigCategories.ThirdParty)]
        string LineApplicationId { get; set; }

        #endregion

        #region Jobs

        [ConfigParameter("EnableAutoFixLostDealTimeSlotWorker", true, "自動偵測並修正可能遺失的DealTimeSlot資料", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableAutoFixLostDealTimeSlotWorker { get; set; }

        #endregion

        [ConfigParameter("DealTimeSlotEnableSortLockHide", false, "活動時程表 DealTimeSlot 權限", ConfigParameterAttribute.ConfigCategories.Ppon)]
        bool DealTimeSlotEnableSortLockHide { get; set; }

        [ConfigParameter("EnableRushBuy", true, "是否開啟記錄限時搶購點擊數", ConfigParameterAttribute.ConfigCategories.Marketing, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableRushBuy { get; set; }

        #region PChome 代銷
        [ConfigParameter("EnablePChomeChannel", false, "開啟正式的Pchome代銷API", ConfigParameterAttribute.ConfigCategories.Ppon)]
        bool EnablePChomeChannel { get; set; }

        [ConfigParameter("PChomeChannelVendorId", "00000", "Pchome給我們的VendorId(呼叫API使用)", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PChomeChannelVendorId { get; set; }

        [ConfigParameter("PChomeChannelVendorNo", "V00000", "Pchome給我們的VendorNo(macCode壓碼使用)", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PChomeChannelVendorNo { get; set; }

        [ConfigParameter("PChomeChannelCouponQRCODE", "", "Pchome券號呈現方式-QRCODE", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PChomeChannelCouponQRCODE { get; set; }

        [ConfigParameter("PChomeChannelCouponBarCode", "", "Pchome券號呈現方式-BarCode", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PChomeChannelCouponBarCode { get; set; }


        [ConfigParameter("PChomeChannelAESKey", "12345678901234561234567890123456", "Pchome Channel AES Key", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PChomeChannelAESKey { get; set; }

        [ConfigParameter("PChomeChannelAESIV", "1234567890123456", "Pchome Channel AES Key", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PChomeChannelAESIV { get; set; }

        [ConfigParameter("PChomeChannelTestBid", "", "Pchome Channel 測試新增BID", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PChomeChannelTestBid { get; set; }
        [ConfigParameter("PChomeChannelShortTimeUpdateStockBid", "", "Pchome Channel 短時間更新庫存BID", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PChomeChannelShortTimeUpdateStockBid { get; set; }
        [ConfigParameter("PChomeChannelNoUpdateStockBid", "", "Pchome Channel 不更新庫存BID", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PChomeChannelNoUpdateStockBid { get; set; }

        

        #endregion

        #region Pchome Wms

        [ConfigParameter("EnableWms", false, "開啟正式的Pchome倉儲API", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableWms { get; set; }

        [ConfigParameter("WmsVendorId", "V00000", "Pchome給我們的VendorId", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string WmsVendorId { get; set; }

        [ConfigParameter("PchomeWmsAESKey", "9801326d116dc0e5c81f47a0212e39bc", "Pchome AES Key", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PchomeWmsAESKey { get; set; }

        [ConfigParameter("PchomeWmsAESIV", "1bd5e43059e2ac87", "Pchome AES Key", ConfigParameterAttribute.ConfigCategories.Ppon)]
        string PchomeWmsAESIV { get; set; }

        [ConfigParameter("SyncInventoryBatchSize", 20, "一次更新多少筆品項庫存資料", ConfigParameterAttribute.ConfigCategories.Ppon)]
        int SyncInventoryBatchSize { get; set; }

        [ConfigParameter("WmsContractVersion", "20181225", "Pchome寄倉合約最新版本", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string WmsContractVersion { get; set; }

        [ConfigParameter("EnableP24HPersonalNotification", false, "24H到貨出貨通知", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableP24HPersonalNotification { get; set; }

        [ConfigParameter("MerchantReturnAccount", "test123", "還貨單測試帳號-商家系統", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string MerchantReturnAccount { get; set; }
        [ConfigParameter("MerchantReturnEnable", false, "開啟正式的還貨單", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool MerchantReturnEnable { get; set; }

        [ConfigParameter("EnablePchomeDeductingInventory", false, "是否立即扣除庫存", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnablePchomeDeductingInventory { get; set; }
        [ConfigParameter("PchomeDeductingInventoryBids", "", "立即扣除庫存Bid", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string PchomeDeductingInventoryBids { get; set; }

        [ConfigParameter("WmsRefundEnable", false, "開啟PCHOME退貨單", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        bool WmsRefundEnable { get; set; }

        [ConfigParameter("WmsRefundAccount", "", "還貨單測試帳號", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string WmsRefundAccount { get; set; }

        [ConfigParameter("WmsRefundVbsAccount", "", "還貨單測試商家帳號", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string WmsRefundVbsAccount { get; set; }

        [ConfigParameter("WmsOrderAPI", "", "查出貨訂單資訊(測試站使用)", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string WmsOrderAPI { get; set; }
        [ConfigParameter("WmsProgressStatusAPI", "", "查出貨進度歷程(測試站使用)", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string WmsProgressStatusAPI { get; set; }

        [ConfigParameter("WmsRefundAPI", "", "查退貨單資訊(測試站使用)", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string WmsRefundAPI { get; set; }

        [ConfigParameter("WmsRefundLogisticAPI", "", "查詢退貨單逆物流配送資訊(測試站使用)", ConfigParameterAttribute.ConfigCategories.Ppon, ConfigParameterAttribute.ConfigTypes.option)]
        string WmsRefundLogisticAPI { get; set; }
        #endregion

        #region Payeasy 導購

        [ConfigParameter("EnablePezChannel", true, "是否開啟Payeasy導購", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnablePezChannel { get; set; }

        [ConfigParameter("PezChannelCookieExpires", 5, "Payeasy導購的逾時天數", ConfigParameterAttribute.ConfigCategories.System)]
        int PezChannelCookieExpires { get; set; }

        [ConfigParameter("PayeasyOrderApiUrl", "https://ecshop.payeasy.com.tw/GoShopping/Vendor/api/order/create", "Payeasy導購 Order API Url", ConfigParameterAttribute.ConfigCategories.System)]
        string PezChannelOrderApiUrl { get; set; }

        [ConfigParameter("PayeasyVenId", "a1b2c3d4", "Payeasy導購合作網站識別碼號", ConfigParameterAttribute.ConfigCategories.System)]
        string PezChannelVenId { get; set; }

        [ConfigParameter("PezChannelRunDay", 60, "Payeasy 訂單成立後幾日確定分潤", ConfigParameterAttribute.ConfigCategories.System)]
        int PezChannelRunDay { get; set; }

        #endregion

        #region Line導購
        [ConfigParameter("LineShopOrderFirstApiUrl", "https://buy.line.me/tracking/orderinfo", "呼叫LineAPI首次訂單回拋", ConfigParameterAttribute.ConfigCategories.System)]
        string LineShopOrderFirstApiUrl { get; set; }
        [ConfigParameter("LineShopOrderFinishApiUrl", "https://buy.line.me/tracking/orderfinish", "呼叫LineAPI請款/發點訂單回拋", ConfigParameterAttribute.ConfigCategories.System)]
        string LineShopOrderFinishApiUrl { get; set; }
        [ConfigParameter("LineShopSite", "2", "LineShop代號", ConfigParameterAttribute.ConfigCategories.System)]
        int LineShopSite { get; set; }
        [ConfigParameter("LineShopId", "295", "LineShop商城代號", ConfigParameterAttribute.ConfigCategories.System)]
        int LineShopId { get; set; }
        [ConfigParameter("LineShopAuthKey", "1874617c", "LineShop認證登入代碼", ConfigParameterAttribute.ConfigCategories.System)]
        string LineShopAuthKey { get; set; }
        [ConfigParameter("EnableLineShop", false, "是否開啟Line導購", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableLineShop { get; set; }
        [ConfigParameter("LineShopCookieExpires", 24, "Line Cookie 逾時小時", ConfigParameterAttribute.ConfigCategories.System)]
        int LineShopCookieExpires { get; set; }
        [ConfigParameter("LineShopEffectiveOrderDays", 60, " Line 購物憑證訂單產生日起幾日確定分潤", ConfigParameterAttribute.ConfigCategories.System)]
        int LineShopEffectiveOrderDays { get; set; }
        [ConfigParameter("LineShopEffectiveShipDays", 30, "Line 購物宅配訂單出貨後幾日確定分潤 ", ConfigParameterAttribute.ConfigCategories.System)]
        int LineShopEffectiveShipDays { get; set; }
        [ConfigParameter("LineShopForTest", false, "Line導購銷售認列時間是否為當下", ConfigParameterAttribute.ConfigCategories.System, ConfigParameterAttribute.ConfigTypes.option)]
        bool LineShopForTest { get; set; }

        [ConfigParameter("LineRsrc", "LINESHOP_17Life", "LineRsrc", ConfigParameterAttribute.ConfigCategories.System)]
        string LineRsrc { get; set; }

        [ConfigParameter("LineMinGrossMargin", 0.05, "Line過檔最低毛利率", ConfigParameterAttribute.ConfigCategories.System)]
        decimal LineMinGrossMargin { get; set; }

        #endregion

        #region Affiliates 導購

        [ConfigParameter("AffiliatesCookieExpires", 5, "Affiliates 的逾時天數", ConfigParameterAttribute.ConfigCategories.System)]
        int AffiliatesCookieExpires { get; set; }

        [ConfigParameter("AffiliatesApiUrl", "https://vbtrax.com/track/postback/conversions/8/global", "丟訂單給 Affiliates呼叫的API", ConfigParameterAttribute.ConfigCategories.System)]
        string AffiliatesApiUrl { get; set; }

        #endregion

        #region 台新中台

        [ConfigParameter("EnbaledTaishinEcPayApi", false, "啟用台新中台支付API", ConfigParameterAttribute.ConfigCategories.System)]
        bool EnbaledTaishinEcPayApi { get; set; }

        #endregion

        #region 富利核銷

        [ConfigParameter("PizzaHutFtpUri", "ftp://admin:1234@localhost", "必勝客FtpUri", ConfigParameterAttribute.ConfigCategories.System)]
        string PizzaHutFtpUri { get; set; }

        [ConfigParameter("KfcFtpUri", "ftp://admin:1234@localhost", "肯德基FtpUri", ConfigParameterAttribute.ConfigCategories.System)]
        string KfcFtpUri { get; set; }

        [ConfigParameter("PizzaHutFtpPath", "JF_Pizzahut", "必勝客FtpPath", ConfigParameterAttribute.ConfigCategories.System)]
        string PizzaHutFtpPath { get; set; }

        [ConfigParameter("KfcFtpPath", "jf_kfc", "肯德基FtpPath", ConfigParameterAttribute.ConfigCategories.System)]
        string KfcFtpPath { get; set; }

        [ConfigParameter("JfMailTo", "tanner_chiang@17life.com,unicorn@17life.com", "富利核銷資料收件人,逗號隔開", ConfigParameterAttribute.ConfigCategories.System)]
        string JfMailTo { get; set; }

        [ConfigParameter("JfMailSubject", "富利核銷資料", "富利核銷資料信件主旨", ConfigParameterAttribute.ConfigCategories.System)]
        string JfMailSubject { get; set; }

        #endregion

        #region 生活優惠中台 Entrepot

        [ConfigParameter("LifeEntrepotSiteUrl", "http://iceman.17life.com:1771", "生活優惠站台位置", ConfigParameterAttribute.ConfigCategories.Entrepot)]
        string LifeEntrepotSiteUrl { get; set; }

        #endregion 生活優惠中台 Entrepot

        #region Alibaba skm 千人千面
        [ConfigParameter("skmFirstShowLimit", 10, "千人千面檔次首次呈現10筆上限", ConfigParameterAttribute.ConfigCategories.Skm)]
        int skmFirstShowLimit { get; set; }


        [ConfigParameter("SkmFtpHost", "192.168.1.2", "千人千面 FTP位置", ConfigParameterAttribute.ConfigCategories.Skm, ConfigParameterAttribute.ConfigTypes.option)]
        string SkmFtpHost { get; set; }

        [ConfigParameter("SkmFtpUserId", "", "千人千面 FTP 帳號", ConfigParameterAttribute.ConfigCategories.Skm, ConfigParameterAttribute.ConfigTypes.option)]
        string SkmFtpUserId { get; set; }

        [ConfigParameter("SkmFPassword", "", "千人千面 FTP 密碼", ConfigParameterAttribute.ConfigCategories.Skm, ConfigParameterAttribute.ConfigTypes.option)]
        string SkmFPassword { get; set; }

        [ConfigParameter("LocalFilePath", "C:\\FTP\\", "千人千面 本地儲存資料夾", ConfigParameterAttribute.ConfigCategories.Skm, ConfigParameterAttribute.ConfigTypes.option)]
        string LocalFilePath { get; set; }

        [ConfigParameter("RemoteWorkPath", "/appos/FTP/Work/", "千人千面 待處理資料夾", ConfigParameterAttribute.ConfigCategories.Skm, ConfigParameterAttribute.ConfigTypes.option)]
        string RemoteWorkPath { get; set; }

        [ConfigParameter("RemoteFinishPath", "/appos/FTP/Finish/", "千人千面 處理完成資料夾", ConfigParameterAttribute.ConfigCategories.Skm, ConfigParameterAttribute.ConfigTypes.option)]
        string RemoteFinishPath { get; set; }

        [ConfigParameter("RemoteFailPath", "/appos/FTP/Fail/", "千人千面 處理失敗資料夾", ConfigParameterAttribute.ConfigCategories.Skm, ConfigParameterAttribute.ConfigTypes.option)]
        string RemoteFailPath { get; set; }

        #endregion

        #region Alibaba skm業務中台

        [ConfigParameter("SKMCenterAppKey", "", "新光業務中台open api key", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SKMCenterAppKey { get; set; }

        [ConfigParameter("SKMCenterAppSecret", "", "新光業務中台open api secret", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SKMCenterAppSecret { get; set; }

        [ConfigParameter("SKMCenterApiUrl", "", "新光業務中台open api 連線網址", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SKMCenterApiUrl { get; set; }


        [ConfigParameter("SKMCenterRocketMQTopic", "", "新光業務中台RocketMQ Topic", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SKMCenterRocketMQTopic { get; set; }

        [ConfigParameter("SKMCenterRocketMQAccessKey", "", "新光業務中台RocketMQ AccessKey", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SKMCenterRocketMQAccessKey { get; set; }

        [ConfigParameter("SKMCenterRocketMQSecretKey", "", "新光業務中台RocketMQ SecretKey", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SKMCenterRocketMQSecretKey { get; set; }

        [ConfigParameter("SKMCenterRocketMQGroupId", "", "新光業務中台RocketMQ GroupID", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SKMCenterRocketMQGroupId { get; set; }

        [ConfigParameter("SKMCenterRocketMQNameSrv", "", "新光業務中台RocketMQ 服務網址", ConfigParameterAttribute.ConfigCategories.Skm)]
        string SKMCenterRocketMQNameSrv { get; set; }

        #endregion

        #region Alibaba商品頁面控制
        [ConfigParameter("CreateItemBySelf", true, "是否開啟商品建檔功能", ConfigParameterAttribute.ConfigCategories.Skm, ConfigParameterAttribute.ConfigTypes.option)]
        bool CreateItemBySelf { get; set; }

        [ConfigParameter("EnableCreateCategory" , true , "是否開啟商品主分類功能" , ConfigParameterAttribute.ConfigCategories.Skm, ConfigParameterAttribute.ConfigTypes.option)]
        bool EnableCreateCategory { get; set; }
        #endregion

        #region Alibaba api
        [ConfigParameter("AlibabaAPIWhiteIps", "127.0.0.1,::1", "內部API白名單", ConfigParameterAttribute.ConfigCategories.Skm)]
        string AlibabaAPIWhiteIps { get; set; }
        #endregion

    }
}
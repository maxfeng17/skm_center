﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core
{
    public interface IEIPProvider : IProvider
    {
        #region DocumentList
        DocumentListCollection DocumentListGet(params string[] filter);
        DocumentList DocumentListGetById(Guid guid);
        DocumentListCollection DocumentListGetByCategoryId(Guid catGuid);
        bool DocumentListSet(DocumentList dl);
        #endregion DocumentList

        #region DocumentCategory
        DocumentCategory DocumentCategoryGetById(Guid guid);
        DocumentCategoryCollection DocumentCategoryGet();
        bool DocumentCategorySet(DocumentCategory dc);

        #endregion DocumentCategory

        #region TaskFlow
        TaskFlow TaskFlowGetByProcessId(Guid processId);
        TaskFlowCollection TaskFlowRunningGet(Guid execUser);
        TaskFlowCollection TaskFlowApplyGet(Guid applyUser);
        bool TaskFlowSet(TaskFlow tf);
        #endregion TaskFlow

        #region TaskFlowContent
        TaskFlowContent TaskFlowContentGetByTaskId(Guid taskId);
        TaskFlowContent TaskFlowContentRunningGet(Guid processId);
        bool TaskFlowContentSet(TaskFlowContent tfc);
        #endregion TaskFlowContent
    }
}

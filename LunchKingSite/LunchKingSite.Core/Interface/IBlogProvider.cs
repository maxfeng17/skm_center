﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.Core
{
    public interface IBlogProvider : IProvider
    {
        #region BlogPost
        BlogPost BlogPostGet(int id);
        BlogPost BlogPostGetByGid(Guid gid);
        BlogPost BlogPostGetAllByGid(Guid gid);
        int BlogPostGetCount(params string[] filter);
        BlogPostCollection BlogPostGetList(int pageNumber, int pageSize, string orderBy, params string[] filter);
        bool BlogPostSet(BlogPost blog);
        #endregion BlogPost

        #region BlogPageView
        int BlogPageViewGetCount(params string[] filter);
        bool BlogPageViewSet(BlogPageView view);
        #endregion BlogPageView
    }
}

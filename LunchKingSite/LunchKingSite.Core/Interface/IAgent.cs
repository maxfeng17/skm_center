using System;
using System.Net.Mail;

namespace LunchKingSite.Core
{
    public interface IMailAgent
    {
        bool Send(MailMessage msg);
        void SendAsync(MailMessage msg, Action successCallback, Action<Exception> failCallback);
    }

    public interface IFaxAgent
    {
        string StoragePath { get; set; }
        bool Send(string faxNumber, string faxDestName, string title, string content, string sellerNumber, string orderId);
    }
}

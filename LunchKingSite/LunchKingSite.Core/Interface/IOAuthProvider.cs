﻿using LunchKingSite.DataOrm;
using System;

namespace LunchKingSite.Core
{
    public interface IOAuthProvider : IProvider
    {
        OauthClient ClientGet(string appId);

        OauthClient ClientGetByName(string name);

        void ClientRemoveUser(int clientId, int userId);

        OauthClientUser ClientUserGet(int clientId, int userId);

        void ClientUserSet(int clientId, int userId);

        OauthClientCollection ClientGetList();

        OauthClientPermissionCollection ClientPermissionGetList(int clientId);

        void ClientSet(OauthClient client);

        void ClientPermissionDeleteByClientId(int clientId);

        void ClientPermissionSetList(int clientId, OauthClientPermissionCollection oauthClientPermissionCollection);

        OauthTokenCollection OAuthTokenGetList(string appId);

        void TokenSetList(OauthTokenCollection tokens);

        void TokenSet(OauthToken token);

        OauthToken TokenGetByAccessToken(string accessToken);

        OauthToken TokenGetByCode(string code);

        void TokenPermissionSetList(OauthTokenPermissionCollection tokenPerms);

        void TokenPermissionDeleteByTokenId(int tokenId);

        OauthTokenPermissionCollection TokenPermissionGetList(int tokenId);

        OauthToken TokenGetByRefreshToken(string refreshToken);

        void ClientUserDelete(int id);

        void TokenSetInvalid(int clientId, int userId);

        OauthClientCollection ClientGetListByUser(int userId);
        /// <summary>
        /// 取得逾期的Token集合
        /// </summary>
        /// <param name="expiredDate"></param>
        /// <returns></returns>
        OauthTokenCollection OauthTokenGetExpiredList(DateTime expiredDate);

        OauthClientCollection OauthClientGetListByChannelClient();

        OauthClientCollection OauthClientGetByAccessToken(string accessToken);
    }
}

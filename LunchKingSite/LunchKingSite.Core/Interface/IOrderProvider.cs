﻿using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.OrderEntities;

namespace LunchKingSite.Core
{
    /// <summary>
    /// Order Provider Interface
    /// </summary>
    public interface IOrderProvider : IProvider
    {
        #region Order

        Order OrderGet(Guid guidOrder);

        OrderCollection OrderGet(List<Guid> guids);

        Order OrderGet(string column, object value);

        Order OrderGetByOrderId(string orderId);

        OrderCollection OrderGetListPaging(int pageStart, int PageLength, OrderStatus status, string orderBy, params string[] filter);

        OrderCollection OrderGetListPaging(int pageStart, int PageLength, OrderStatus statusMask, OrderStatus expectStatus, string orderBy, params string[] filter);

        Guid OrderGuidGetById(string orderId);

        OrderCollection GetExpriedOrderList();
        OrderCollection GetNoReceiptPaidIspOrder();

        OrderCollection GetExpriedOrderListOverThreeDays();
        OrderCollection GetHiLifeExpriedOrderList();

        ViewPponOrderCollection GetNoUseCouponOrderList(DateTime d);

        int OrderGetCount(params string[] filter);

        int OrderGetCount(OrderStatus statusMask, OrderStatus expectStatus, params string[] filter);

        int OrderGetDailyOrderCount(Guid sellerGuid, DateTime deliveryDate);

        double OrderGetSumTotal(params string[] filter);
        /// <summary>
        /// 取得會員帶來的營業額，扣折價券
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int OrderGetSuccessTurnover(int userId);
        /// <summary>
        /// 使用者的未成單數
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int FailOrderGetCountByUser(int userId);

        bool OrderSet(Order o);

        bool OrderDelete(Guid odrGuid);

        List<Guid> OrderGuidListGetByUser(int userId);

        bool OrderSetId(Guid orderGuid, string orderId);

        bool OrderSetStatus(Guid orderGuid, string userName, OrderStatus status, bool setIt);

        void OrderUpdateSumTotal(Guid orderId);

        int OrderUnlockAll();

        void OrderSetPartialCancel(Guid oid, bool ispartial);

        DataTable MemberOrderListGet(int pageStart, int pageLength, string userName, string defOrderBy, bool filterFunds, int famCode, string filter);

        int MemberOrderListGetCount(string userName, bool filterFunds, int famCode, string filter);

        DataTable MemberLunchKingOrderListGet(int pageStart, int pageLength, string userName, string defOrderBy);

        int MemberLunchKingOrderListGetCount(string userName);

        /// 根據bid由cash_trust_log統計該檔次已成立訂單之銷售總數及金額(扣除結檔前完全退貨訂單)
        /// 取代view_ppon_deal由order_detail統計銷售總數及金額做法(未考慮結檔前部分退貨)
        /// <param name="bid">檔次bid</param>
        /// <returns></returns>
        List<OrderQuantityAmount> OrderCashTrustLogGetQuantityAmount(IEnumerable<Guid> bid);

        //商品可付款異常（退貨份數已超過結檔份數的30%的檔次）
        DataTable GetDataTableOrderPaytableAlert(string queryStartTime, string queryEndTime);

        /// <summary>
        /// 根據訂單日期取得使用者訂單總額(行銷滿額贈送等值折價券活動)
        /// </summary>
        /// <param name="dateStart">訂單起日</param>
        /// <param name="dateStart">訂單迄日</param>
        /// <param name="amount">每消費多少單位</param>
        /// <param name="takeCeiling">無條件進位</param>
        /// <returns></returns>
        DataTable OrderGetSubtotalByOrderDate(DateTime dateStart, DateTime dateEnd, decimal amount, bool takeCeiling);

        #endregion Order

        #region OrderProductDelivery
        OrderProductDelivery OrderProductDeliveryGetByOrderGuid(Guid oid);
        OrderProductDeliveryCollection OrderProductDeliveryGetListByOrderGuid(List<Guid> oid);
        bool OrderProductDeliveryCollectionBulkInsert(OrderProductDeliveryCollection opdCol);
        bool OrderProductDeliveryUpdateIsApplicationServerConnectedStatus(IspIsApplicationServerConnectedStatus status, List<Guid> orderGuidList);
        bool OrderProductDeliverySet(OrderProductDelivery opd);
        OrderProductDelivery OrderProductDeliveryGet(int id);

        OrderProductDeliveryCollection OrderProductDeliveryGetByReturn(List<Guid> sellerGuids);

        #endregion

        #region ViewOrderProductDelivery

        /// <summary>
        /// 取得未完成的宅配超取訂單
        /// </summary>
        /// <returns></returns>
        ViewOrderProductDeliveryCollection ViewOrderProductDeliveryGetUndoneList();

        /// <summary>
        /// 以OrderGuid清單查詢ViewOrderProductDeliveryCollection
        /// </summary>
        /// <param name="orderGuids"></param>
        /// <returns></returns>
        ViewOrderProductDeliveryCollection ViewOrderProductDeliveryCollectionGetByOrderGuids(List<Guid> orderGuids);

        /// <summary>
        /// 以OrderGuid查詢ViewOrderProductDelivery
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        ViewOrderProductDelivery ViewOrderProductDeliveryGetByOrderGuid(Guid orderGuid);

        /// <summary>
        /// 以物流單號查詢ViewOrderProductDelivery
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        ViewOrderProductDelivery ViewOrderProductDeliveryGetByShipNo(string shipNo);

        #endregion ViewOrderProductDelivery

        #region OrderDetail

        OrderDetail OrderDetailGet(Guid guidOrderDetail);

        OrderDetail OrderDetailPartialRefundGet(Guid orderGuid);

        OrderDetailCollection OrderDetailGetList(Guid guidOrder);

        OrderDetailCollection OrderDetailGetList(int pageStart, int pageLength, string orderBy, Guid guidOrder);

        OrderDetailCollection OrderDetailGetList(int pageStart, int pageLengh, string orderBy, Guid guidOrder, OrderDetailTypes type);

        OrderDetailCollection OrderDetailGetListAggregate(Guid orderGuid);

        OrderDetailCollection OrderDetailGetListAggregate(Guid orderGuid, OrderDetailTypes odType);

        int OrderDetailGetOrderedQuantityByBid(Guid businessHourGuid, bool includeRefund = false);

        int OrderDetailGetCount(Guid guidOrder);

        bool OrderDetailSet(OrderDetail od);

        bool OrderDetailDelete(Guid guidOrderDetail);

        int OrderDetailGetIfPartialCancel(Guid oid);

        OrderDetailCollection OrderDetailGetListByStatus(Guid guidOrder);

        OrderDetailCollection OrderDetailGetListByBid(Guid bid);

        int OrderDetailSetList(OrderDetailCollection ods);

        BusinessHour GetHourStatusBybid(Guid bid);

        #endregion OrderDetail

        #region Order related auxiliary functions

        void MakeOrder(Order theOrder, Guid bizHourGuid, OrderDetailCollection odrDtlCol, OrderDetailItemCollection odItemCol,
            ViewShoppingCartItemCollection vsci, MemberStatusFlag memStUpd, OrderProductDelivery opd);

        void MakeOrderWithoutCarts(Order theOrder, Guid bizHourGuid, OrderDetailCollection odrDtlCol, MemberStatusFlag memStUpd);

        void MakeGroupOrder(Order ord, GroupOrder go, bool setBizHourOpenModeToFull);

        void CompleteGroupOrder(GroupOrder go, Order o, OrderDetailCollection extraOd, MemberStatusFlag memStUpd);

        #endregion Order related auxiliary functions

        #region ReturnForm

        int ReturnFormSet(ReturnForm form);
        ReturnForm ReturnFormGet(int id);
        int GetRefundCountByOrderGuid(Guid orderGuid);
        ReturnFormCollection ReturnFormGetListByOrderGuid(Guid orderGuid);
        ReturnFormCollection ReturnFormGetListByOrderGuidProgressStatusRefundType(Guid orderGuid, ProgressStatus processStatus, RefundType refundType);
        List<int> ReturnFormGetListForScheduledRefund();
        ReturnFormCollection GetExpiredProductReturnFormList();
        ReturnFormCollection GetCancelReturnForm(DateTime s_date, DateTime e_date);
        ReturnFormCollection GetNoCompleteAndRequireCreditNote(DateTime s_date, DateTime e_date, InvoiceMode2 invoiceMode2, AllowanceStatus allowanceStatus);
        #endregion

        #region ReturnFormProduct

        /// <summary>
        /// 取得退貨訂單商品明細資料
        /// </summary>
        /// <param name="returnFormId">return_form id </param>
        /// <returns></returns>
        ReturnFormProductCollection ReturnFormProductGetList(int returnFormId);

        /// <summary>
        /// 取得訂單所有退貨單退貨商品明細資料
        /// </summary>
        /// <param name="orderGuid">order guid </param>
        /// <param name="bsModel">Ppon or PiinLife </param>
        /// <returns></returns>
        ReturnFormProductCollection ReturnFormProductGetList(Guid orderGuid, BusinessModel bsModel);

        /// <summary>
        /// 取得退貨訂單商品明細資料
        /// </summary>
        /// <param name="returnFormIds">多筆 return_form id </param>
        /// <returns></returns>
        ReturnFormProductCollection ReturnFormProductGetList(IEnumerable<int> returnFormIds);

        int ReturnFormProductSetList(ReturnFormProductCollection formProducts);

        #endregion

        #region ReturnFormRefund

        int ReturnFormRefundSetList(ReturnFormRefundCollection formRefunds);

        ReturnFormRefundCollection ReturnFormRefundGetList(int returnFormId);

        ReturnFormRefundCollection ReturnFormRefundGetList(IEnumerable<int> returnFormIds);

        ReturnFormRefundCollection ReturnFormRefundGetList(Guid trustId);

        #endregion

        #region ReturnFormStatusLog

        int ReturnFormStatusLogSet(ReturnFormStatusLog log);
        ReturnFormStatusLogCollection ReturnFormStatusLogGetListByReturnFormId(int returnFormId);

        #endregion

        #region ViewCashTrustLogReturnFormList

        ViewCashTrustLogRetrnFormListCollection ViewCashTrustLogRetrnFormListCollectionGet(int returnFormId);

        #endregion

        #region ShoppingCart

        ShoppingCart ShoppingCartGet(string column, object value);

        IList<Order> OrderGetListByShoppingCartGroupGuid(Guid shoppingCartGroupGuid);

        ShoppingCart ShoppingCartGet(string ticketId, Guid itemId, string itemName);

        ShoppingCartCollection ShoppingCartGetList(string column, object value);

        ShoppingCartCollection ShoppingCartGetListByFilter(params string[] filter);

        bool ShoppingCartSet(ShoppingCart cart);

        void ShoppingCartDeleteByTicketId(string ticketId);

        bool ShoppingCartConvert2OrdDtl(OrderDetailCollection odrDtlCol, Order odr, string ticketId, bool updateOrder);

        bool ShoppingCartPurge(DateTime deadline);

        #endregion ShoppingCart

        #region GroupOrder

        GroupOrder GroupOrderGet(Guid goId);

        GroupOrder GroupOrderGetByBid(Guid bid);

        GroupOrder GroupOrderGetByOrderGuid(Guid orderGuid);

        GroupOrderCollection GroupOrderGetList(params string[] filter);

        GroupOrderCollection GroupOrderGetList(string filter, string orderBy);

        bool GroupOrderSet(GroupOrder go);

        bool GroupOrderUpdateClosingTime(Guid goId, DateTime closeTime);

        bool GroupOrderDelete(string column, object value);

        #endregion GroupOrder

        #region RedeemOrder

        RedeemOrder RedeemOrderGet(Guid guid);

        RedeemOrderCollection RedeemOrderGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        int RedeemOrderGetCount();

        bool RedeemOrderSet(RedeemOrder odr);

        DataTable RedeemGetForCheckOut(DateTime sDate, DateTime eDate);

        void RedeemUpdateForCheckOut(DateTime sDate, DateTime eDate, DateTime checkouttime);

        DataTable RedeemGetForCheckOutCSV(DateTime sDate, DateTime eDate);

        int RedeemOrderGetCreateIdMaxLenght();

        #endregion RedeemOrder

        #region RedeemOrderDetail

        RedeemOrderDetailCollection RedeemOrderDetailGetList(Guid redOrdGuid);

        RedeemOrderDetailCollection RedeemOrderDetailGetList(string column, object value);

        int RedeemOrderDetailGetCreateIdMaxLength();

        #endregion RedeemOrderDetail

        #region ViewOrderMemberBuildingSeller

        ViewOrderMemberBuildingSeller ViewOrderMemberBuildingSellerGet(Guid orderGuid);

        ViewOrderMemberBuildingSeller ViewOrderMemberBuildingSellerGet(OrderStatus statusMask, OrderStatus expectStatus, string orderBy, params string[] filter);

        #endregion ViewOrderMemberBuildingSeller

        #region ViewMemberGroupOrder

        ViewMemberGroupOrder ViewMemberGroupOrderGet(string column, object value);

        ViewMemberGroupOrderCollection ViewMemberGroupOrderGetList(string username, string orderBy, params string[] filter);

        #endregion ViewMemberGroupOrder

        #region ViewShoppingCartItem

        ViewShoppingCartItemCollection ViewShoppingCartItemGetList(string ticketId);

        #endregion ViewShoppingCartItem

        #region ViewOrderDetailItem

        ViewOrderDetailItemCollection ViewOrderDetailItemGetList(Guid orderGuid);

        #endregion ViewOrderDetailItem

        #region

        /// <summary>
        /// 依OrderDetail Guid搜尋訂購品項資料
        /// </summary>
        /// <param name="order_detail_guid"></param>
        /// <returns></returns>
        OrderDetailItemCollection OrderDetailItemCollectionGetByOrderDetailGuid(Guid order_detail_guid);

        OrderDetailItemCollection OrderDetailItemCollectionGetByOrderGuid(Guid orderGuid);

        #endregion

        #region ViewOrderMemberBuildingSeller

        ViewOrderMemberBuildingSellerCollection ViewOrderMemberBuildingSellerGet(int pageStart, int pageLength, string orderBy, params string[] filter);
        ViewOrderMemberBuildingSellerCollection ViewOrderMemberBuildingSellerGetListPaging(int pageStart, int pageLength, string orderBy, string orderId, int? uniqueId, string memberName, DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime, int? excludeFamilyMartCityId, bool paymentFreeze);
        ViewOrderMemberBuildingSellerCollection ViewOrderMemberBuildingSellerGetListPagingForNew(int pageStart, int pageLength, string orderBy, string orderId, int? uniqueId, string memberName, DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime,string dateType,string shipType,string csvType);
        ViewOrderMemberBuildingSellerCollection ViewOrderMemberBuildingSellerGetListPagingForReturn(int pageStart, int pageLength, string orderBy, string orderId, int? uniqueId, string sellerId, DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime, string dateType, string csvType);
        ViewOrderMemberBuildingSellerCollection ViewOrderMemberBuildingSellerGetListExcelForReturn(string orderBy, string orderId, int? uniqueId, string sellerId, DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime, string dateType, string csvType);
        int ViewOrderMemberBuildingSellerGetCount(string orderId, int? uniqueId, string memberName, DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime, int? excludeFamilyMartCityId, bool paymentFreeze);

        int ViewOrderMemberBuildingSellerGetCountForNew(string orderId, int? uniqueId, string memberName, DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime, string dateType, string shipType, string csvType);
        int ViewOrderMemberBuildingSellerGetCountForReturn(string orderId, int? uniqueId, string sellerId, DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime, string dateType, string csvType);
        DataTable ViewOrderMemberBuildingSellerGetListByDepartment(DateTime[] during);

        #endregion

        #region OrderStatusLog

        OrderStatusLogCollection OrderStatusLogGetList(Guid orderGuid);
        OrderStatusLogCollection OrderStatusLogGetVendorNotifyList(int orderReturnId);
        bool OrderStatusLogSet(OrderStatusLog data);
        OrderStatusLog OrderStatusLogGetLatest(Guid orderGuid);

        #endregion

        #region PaymentTransaction

        PaymentTransaction PaymentTransactionGet(int id);

        PaymentTransaction PaymentTransactionGet(Guid OrderGuid);

        PaymentTransaction PaymentTransactionGet(string transId, PaymentType paymentType, PayTransType transType);

        PaymentTransactionCollection PaymentTransactionGetListByTransIdAndTransType(string transId, PayTransType transType);

        PaymentTransactionCollection PaymentTransactionGetListByTransIdAndPaymentType(string transId, PaymentType payType, int transType);

        PaymentTransactionCollection PaymentTransactionGetList(Guid order_guid, OrderClassification orderClassification = OrderClassification.LkSite);

        PaymentTransactionCollection PaymentTransactionGetListPaging(int pageStart, int pageLength, string orderByDesc,
                                                               PayTransStatusFlag mask, int expectStatus, params string[] filter);

        PaymentTransactionCollection PaymentTransactionGetList(int pageStart, int pageLength, string orderByDesc, params string[] filter);
        /// <summary>
        ///
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        PaymentTransactionCollection PaymentTransactionGetListByOrderGuid(Guid orderGuid);

        bool PaymentTransactionInsert(PaymentTransaction pt);

        void PaymentTransactionDelete(string transId, PayTransType transType);

        bool PaymentTransactionSet(PaymentTransaction pt);

        void PaymentTransactionSetAll(PaymentTransactionCollection pts);

        PaymentTransactionCollection PaymentTransactionGetNullOrderIdPCash();

        PaymentTransactionCollection PaymentTransactionGetListForCharging(bool manual = false);

        PaymentTransactionCollection PaymentTransactionGetListForRefunding(bool manual = false);

        bool TransactionidLogSet(TransactionidLog transactionidLog);

        void HitrustRefundRecordSet(HitrustRefundRecord record);

        #endregion PaymentTransaction

        #region PaymentTypeAmount

        bool PaymentTypeAmountSet(PaymentTypeAmount paymentTypeAmount);
        bool PaymentTypeAmountCollectionSet(PaymentTypeAmountCollection paymentTypeAmountCollection);

        #endregion PaymentTypeAmount

        #region ViewPaymentTransactionLeftjoinOrder

        ViewPaymentTransactionLeftjoinOrderCollection ViewPaymentTransactionLeftjoinOrderGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        int ViewPaymentTransactionLeftjoinOrderGetCount(params string[] filter);

        #endregion

        #region ViewPponOrderStatusLog

        ViewPponOrderStatusLogCollection ViewPponOrderStatusLogGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter);

        ViewPponOrderStatusLogCollection ViewPponOrderStatusLogGetListForApplicationInfo(Guid orderGuid);

        #endregion ViewPponOrderStatusLog

        #region ViewPponOrderDetail

        ViewPponOrderDetailCollection ViewPponOrderDetailGetListByOrderGuid(Guid orderGuid);

        ViewPponOrderDetail ViewPponOrderDetailGet(Guid orderDetailGuid);

        #endregion ViewPponOrderDetailList

        #region ViewOrderReturnList

        ViewPponOrderReturnListCollection ViewPponOrderReturnListPaging(int pageStart, int pageLength, string orderBy, params string[] filter);

        ViewPponOrderReturnListCollection ViewPponOrderReturnListByLastReturnStatusPaging(int pageStart, int pageLength, string orderBy, params string[] filter);

        int ViewPponOrderReturnListGetCount(params string[] filter);

        int ViewPponOrderReturnListGetCountByLastReturnStatus(params string[] filter);

        ViewPponOrderReturnListCollection ViewPponOrderReturnListGetList(IEnumerable<Guid> bids, OrderReturnType type);

        ViewPponOrderReturnListCollection ViewPponOrderReturnListGetList(IEnumerable<Guid> productGuids, OrderReturnType type,
            IEnumerable<int> progressStatus, IEnumerable<int> vendorProgressStatus);

        ViewPponOrderReturnList ViewOrderReturnListGet(int id);

        #endregion

        #region OrderReturnList

        OrderReturnList OrderReturnListGet(Guid orderGuid);
        OrderReturnList OrderReturnListGet(int id);
        OrderReturnList OrderReturnListGetbyType(Guid orderGuid, int? type);
        int GetExchangeCountByOrderGuid(Guid orderGuid);
        OrderReturnListCollection OrderReturnListGetListByType(Guid orderGuid, int? type);
        int OrderReturnListGetCountByType(Guid orderGuid, int? type);
        bool OrderReturnListSet(OrderReturnList data);

        #endregion

        #region ViewOrderMemberPayment

        ViewOrderMemberPaymentCollection ViewOrderMemberPaymentGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter);

        ViewOrderMemberPaymentCollection ViewOrderMemberPaymentGetListForPcashRecords(string userName);

        ViewOrderMemberPaymentCollection ViewOrderMemberPaymentGetListForPcashRecords(int pageStart, int pageLength, string userName);

        int ViewOrderMemberPaymentGetListForPcashRecordsCount(string userName);

        ViewOrderMemberPaymentCollection ViewOrderMemberPaymentGetListForCreditcardRecords(string userName);

        #endregion

        #region CashPointOrder

        bool CashPointOrderSet(CashpointOrder data);

        #endregion

        #region CashPointOrderDetail

        bool CashPointOrderDetailSet(CashpointOrderDetail data);

        #endregion

        #region CashPointList

        bool CashPointListSet(CashpointList data);

        decimal CashPointListSum(string username, int lastId);

        CashpointListCollection CashpointGetList(Guid guid);

        /// <summary>
        /// 新購物金於 2012/05/08 06:00 上線.  由於過渡期時, 新舊購物金兩邊都會寫資料, 此方法只會抓出上線前的舊購物金資料.
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        ViewMemberCashpointListCollection CashpointOldGetRecordsWithTransScash(int pageStart, int pageLength, string username);

        int CashpointGetCount(string username);

        #endregion

        #region ScashTransaction

        bool ScashDepositSet(ScashDeposit data);

        bool ScashDepositDelete(Guid orderGuid);

        bool ScashWithdrawalSet(ScashWithdrawal data);

        bool ScashWithdrawalDelete(Guid orderGuid);

        ViewScashTransactionCollection ViewScashTransactionGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        ViewScashTransactionCollection ViewScashTransactionGetListByNotBalanceOrderByInvoicedFirst(int userId);

        ViewScashTransactionCollection ViewScashTransactionGetAll(string username);

        int ViewScashTransactionGetCount(string userName);

        decimal ScachTransactionSum(string username);

        decimal ScachTransactionSum(int userId);

        ViewScashTransactionCollection ViewScashTransactionGetListHaveBalance(int userId);
        /// <summary>
        /// 取得訂單裏，使用購物金的金額(可分別取得E7與Pez儲值的購物金數目)
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        decimal ScashGetByOrder(Guid orderGuid, out decimal scash, out decimal pscash);

        DataTable PezScashRecordsDataTableGet(int userId);

        List<PscashDepositBalance> PscashDepositBalanceGetListForWithdrawal(int userId);
        List<PscashWithdrawalView> PscashWithdrawalViewGetByOrder(Guid orderGuid);

        bool PscashDepositDelete(Guid orderGuid);
        bool PscashWithdrawalDelete(Guid orderGuid);

        #endregion

        #region

        PcashXchOrder PcashXchOrderGet(string pezAuthCode);
        void PcashXchOrderSet(PcashXchOrder pxOrder);

        PcashXchOrder PscashOrderGetByGuid(Guid pxOrderGuid);

        void PscashDepositSet(PscashDeposit deposit);
        void PscashWithdrawalSet(PscashWithdrawal withdrawal);

        #endregion

        #region Einvoice

        void EinvoiceSetSerial(EinvoiceSerial data);

        EinvoiceSerialCollection EinvoiceSerialCollectionGetByDate(string d);

        EinvoiceSerial EinvoiceSerialCollectionGetByInvInfo(string InvoiceNumber, DateTime InvoiceNumberTime);
        EinvoiceSerial EinvoiceSerialCollectionGetById(int id);

        int EinvoiceLogGetCount(string datecode, int invoice_type);

        void EinvoiceLogSet(EinvoiceLog data);

        EinvoiceMainCollection EinvoiceMainCollectionGet(DateTime date_start, DateTime date_end, DateTime newInvoiceDate, bool isnull);

        ViewEinvoiceMaindetailCollection EinvoiceMainDetailCollectionGet(DateTime date_start, DateTime date_end, DateTime newInvoiceDate, int invoice_type, bool with_triplicate = false);

        ViewEinvoiceMaindetailCollection EinvoiceMainDetailCollectionGet(DateTime date_start, DateTime date_end, string column_name, int invoice_type, bool with_triplicate = false);

        ViewEinvoiceMaindetailCollection EinvoiceMainDetailCollectionGetVoid(DateTime date_start, DateTime date_end, bool IsVoid);

        ViewEinvoiceMaindetail EinvoiceMainDetailGet(int id, int invoice_type);

        ViewEinvoiceMaindetail EinvoiceMainDetailGet(int returnFromId);

        /// <summary>
        /// 抓出發票集合
        /// </summary>
        /// <param name="column_name">欄位名稱</param>
        /// <param name="value">條件值</param>
        /// <param name="allowNullNum">是否抓出沒發票號碼的發票集合</param>
        /// <returns></returns>
        EinvoiceMainCollection EinvoiceMainCollectionGet(string column_name, string value, bool allowNullNum);

        EinvoiceDetailCollection EinvoiceDetailCollectionGet(int main_id);

        EinvoiceMain EinvoiceMainGet(string num);

        /// <summary>
        /// 查詢單一一筆發票主檔的紀錄
        /// </summary>
        /// <param name="order_guid">訂單編號</param>
        /// <param name="orderClassification">訂單類型，預設為LkSite</param>
        /// <returns></returns>
        EinvoiceMain EinvoiceMainGet(Guid order_guid, OrderClassification orderClassification = OrderClassification.LkSite);

        EinvoiceMain EinvoiceMainGetById(int id);

        EinvoiceMain EinvoiceMainGetByCouponid(int couponId, OrderClassification orderClassification = OrderClassification.LkSite);

        EinvoiceMainCollection EinvoiceMainCollectionGetByOrderId(string orderId, OrderClassification orderClassification = OrderClassification.LkSite);

        void EinvoiceMainVerifiedTimeSet(int couponId, OrderClassification orderClassification, DateTime verifiedTime, bool isJf = false);
        void PartnerEinvoiceMainVerifiedTimeSet(int couponId, DateTime verifiedTime);
        void EinvoiceSetDetail(EinvoiceDetail detail);

        void EinvoiceUpdateFileSerial(int id, int fileserial, DateTime now);

        void EinvoiceDetailUpdateAllowanceNumber(int id, int fileserial, string allowanceNum, DateTime now);

        void EinvoiceUpdateSum(decimal sum_amount, int id, int invoice_status);

        void EinvoiceRequestPaper(string message, string invoice_number, string buyer_name, string buyer_address);

        void EinvoiceSetMain(EinvoiceMain main);

        void EinvoiceMainCollectionSet(EinvoiceMainCollection invCol);

        EinvoiceMain EinvoiceMainGetByUser(int userId, Guid orderGuid, bool hasNum);

        EinvoiceMainCollection EinvoiceMainGetListByOrderGuid(Guid order_guid, OrderClassification orderClassification = OrderClassification.LkSite);

        EinvoiceMainCollection EinvoiceMainInvoicedOnlyGetListByOrderGuid(Guid order_guid);

        EinvoiceMainCollection EinvoiceMainUninvoicedOnlyGetListByOrderGuid(Guid order_guid);
        EinvoiceMainCollection EinvoiceMainByUserId(int userId, DateTime d_start, DateTime d_end);

        EinvoiceMainCollection EinvoiceMainGetByWinner(int userId, DateTime d_start, DateTime d_end);

        EinvoiceMainCollection EinvoiceMainGetByVoid(DateTime d_start, DateTime d_end, bool IsVoid);

        ViewEinvoiceAllowanceInstoreCollection EinvoiceAllowanceInstoreCollectionGet(DateTime date_start, DateTime date_end);

        ViewEinvoiceAllowanceInstoreIncludeScashCollection EinvoiceAllowanceInstoreIncludeScashCollectionGet(DateTime date_start, DateTime date_end);

        List<EinvoiceAllowanceInfo> EinvoiceAllowanceGetList(DateTime date_start, DateTime date_end);

        List<EinvoiceAllowanceInfo> EinvoiceAllowanceGetListV2(DateTime date_start, DateTime date_end);

        List<EinvoiceAllowanceInfo> EinvoiceAllowanceGetList(int einvoice_id);

        List<EinvoiceAllowanceInfo> EinvoiceAllowanceGetList(List<int> einvoice_id_list);

        EinvoiceMainCollection EinvoicePaperCollectionGetByDate(DateTime date_start, DateTime date_end, InvoiceMode2 invoicemode, bool isCopy, bool hasWinning, InvoiceQueryOption query);

        EinvoiceMainCollection EinvoicePaperMediaCollectionGetByDate(DateTime date_start, DateTime date_end);

        void EinvoiceUpdatePaperTime(int id, DateTime now);

        void EinvoiceUpdateComInfo(int id, string comtitle, string comid, string name, string address);

        void EinvoiceUpdateComInfo(int id, bool backpaper, bool allowance);

        int EinvoiceDetailGetCount(int main_id, int invoice_type);

        decimal EinvoiceCompareDailyIncome(DateTime d_satrt, DateTime d_end);

        void EinvoiceUpdateWinner(string winnername, string winnerphone, string winneraddress, int userId, DateTime d_start, DateTime d_end);

        int EinvoiceWinnerGetCount(DateTime d_start, DateTime d_end, string filter);

        void EinvoiceUpdateWinner(string invoicenumber, string winnername, string winneraddress);

        ViewEinvoiceMailCollection ViewEinvoiceMailGetListByOrderGuid(Guid orderGuid);

        ViewEinvoiceMailCollection EinvoiceMailCollectionGet(DateTime date_start, DateTime date_end);

        ViewEinvoiceMailCollection EinvoiceWinnerMailCollectionGet(DateTime date_start, DateTime date_end);

        ViewEinvoiceWinnerCollection EinvoiceWinnerGetList(int pageStart, int pageLength, DateTime d_start, DateTime d_end, string filter);

        ViewEinvoiceWinnerCollection EinvoiceWinnerGetList(DateTime d_start, DateTime d_end, string filter);

        ViewEinvoiceWinnerCollection EinvoiceWinnerGetByNum(string invoicenum);

        void EinvoiceMainCollectionSetWinningStatus(DateTime d_start, DateTime d_end, string number);
        int EinvoiceMainCollectionSetWinningStatusCount(DateTime d_start, DateTime d_end, string number);
        void EinvoiceMainCollectionSetWinnersStatus(string invoiceNumber);

        /// <summary>
        /// 傳入 TrustId, 傳回該次宅配退貨所對應的發票
        /// </summary>
        EinvoiceMainCollection EinvoiceGetListByToHouseTrustIds(string trustIds);

        /// <summary>
        /// 傳入 CtAtmRefund 的 Si, 傳回該次憑證退貨所對應的發票
        /// </summary>
        EinvoiceMainCollection EinvoiceGetListByToShopTrustIds(string trustIds);

        void CancelEinvoiceSetByUndo(Guid oGuid, int couponId, bool isGroupCoupon);

        void EinvoiceChangeLogSet(EinvoiceChangeLog log);

        void EinvoiceChangeLogCollectionSet(EinvoiceChangeLogCollection log);

        ViewEinvoiceChangeLogCollection ViewEinvoiceChangeLogCollectionGet(Guid orderGuid);
        #endregion

        #region VendorEinvoiceMain

        void VendorEinvoiceMainSet(VendorEinvoiceMain vem);

        void VendorEinvoiceMainCollectionSet(VendorEinvoiceMainCollection vemc);

        VendorEinvoiceMain VendorEinvoiceMainGet(string num);

        VendorEinvoiceMainCollection VendorEinvoiceCollectionGetByCancelDate(DateTime date_start, DateTime date_end);

        VendorEinvoiceMainCollection VendorEinvoiceMainCollectionGetNew();

        VendorEinvoiceMainCollection VendorEinvoiceMainCollectionGetUpload();

        VendorEinvoiceMainCollection VendorEinvoicePaperCollectionGetByDate(DateTime date_start, DateTime date_end, InvoiceMode2 invoicemode, bool isCopy);
        #endregion

        #region PartnerEinvoiceMain

        void PartnerEinvoiceMainSet(PartnerEinvoiceMain main);

        PartnerEinvoiceMainCollection PartnerEinvoiceMainGet(string pOrderId, int pType);

        PartnerEinvoiceMainCollection PartnerEinvoiceMainGetByDate(DateTime sDate, DateTime eDate);

        PartnerEinvoiceMain PartnerEinvoiceMainGet(string num);

        PartnerEinvoiceMainCollection PartnerEinvoicePaperCollectionGetByDate(DateTime date_start, DateTime date_end, InvoiceMode2 invoicemode, bool isCopy, bool hasWinning, InvoiceQueryOption query);

        void PartnerEinvoiceUpdatePaperTime(string invoiceNumber, DateTime now);
        #endregion

        #region PartnerEinvoiceDetail

        void PartnerEinvoiceDetailSet(PartnerEinvoiceDetail detail);

        PartnerEinvoiceDetailCollection PartnerEinvoiceDetailCollectionGetByDate(DateTime sDate, DateTime eDate);
        #endregion

        #region ViewPartnerEinvoiceMaindetail

        ViewPartnerEinvoiceMaindetailCollection ViewPartnerEinvoiceMaindetailCollectionGetByDate(DateTime sDate, DateTime eDate);

        ViewPartnerEinvoiceMaindetailCollection ViewPartnerEinvoiceWinnerMailCollectionGet(DateTime date_start, DateTime date_end);
        #endregion

        #region Discount_campaign

        void DiscountCampaignSet(DiscountCampaign campaign);

        void DiscountCampaignDeleteById(int id);

        DiscountCampaign DiscountCampaignGet(int id);

        DiscountCampaignCollection DiscountCampaignGetByDay();

        DiscountCampaignCollection DiscountCampaignGetByDayForShow();

        DiscountCampaignCollection DiscountCampaignGetByDayForBuy();

        DiscountCampaign DiscountCampaignGetByReference(DiscountCampaignUsedFlags flag, int amount, DateTime endDate);

        DiscountCampaignCollection DiscountCampaignGetEventList(DiscountCampaignUsedFlags flag);

        DiscountCampaignCollection DiscountCampaignGetByCreator(string createId);

        DiscountCampaignCollection DiscountCampaignCollectionGetByCampaignNameAndEffectiveTime(string campaignName);

        DiscountCampaignCollection DiscountCampaignGetList(List<int> campaignIds);
        /// <summary>
        /// 取得目前仍有效的折價券活動，但排除 (未指定分類＆未指定策展＆未指定單檔)，也排除「開始時間」到「結束時間」設定超過2年的
        /// </summary>
        /// <returns></returns>
        List<int> DiscountCampaignIdGetListOnline();

        #endregion

        #region Discount_code

        void DiscountCodeSet(DiscountCode dc);

        void DiscountCodeDeleteByCampaignId(int campaignid);

        void DiscountCodeCollectionBulkInsert(DiscountCodeCollection dclist);

        bool DiscountCodeCellectionBulkInsertFromDataTable(DataTable table);

        /// <summary>
        /// 折價券使用use_time更新，避免超賣
        /// </summary>
        /// <param name="code">折價券序號</param>
        /// <param name="useamount">折扣金額</param>
        /// <param name="useid">會員Id</param>
        /// <param name="orderguid">訂單Guid</param>
        /// <param name="ordercost">訂單cost</param>
        /// <returns>折價券更新筆數</returns>
        int DiscountCodeUpdateStatusSet(int campaignId, string code, int useamount, int useid, Guid orderguid, int orderamount, int ordercost, int? codeId = null);

        /// <summary>
        /// 依據Code取得DiscountCode的資料，code正常狀況下為唯一值。(統一序號例外)
        /// </summary>
        /// <param name="code">要查詢的code</param>
        /// <returns></returns>
        DiscountCode DiscountCodeGetByCode(string code);

        /// <summary>
        /// 依Id取得DiscountCode的資料，Id正常狀況下為唯一值。
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DiscountCode DiscountCodeGetById(int id);

        /// <summary>
        /// 未使用過的折價券
        /// </summary>
        /// <param name="code">序號</param>
        /// <returns></returns>
        DiscountCode DiscountCodeGetByCodeWithoutUseTime(string code);

        /// <summary>
        /// 利用orderGuid查詢DiscountCode的紀錄，正常情況下，一張訂單只能使用一組DiscountCode
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        DiscountCode DiscountCodeGetByOrderGuid(Guid orderGuid);

        DiscountCodeCollection DiscountCodeGetByCampaignIdAndUseTimeIsnotnull(int campaignid);

        DiscountCodeCollection DiscountCodeGetListByCampaign(int campaignId);

        DiscountCodeCollection DiscountCodeGetListWithNonOwner(int campaignId);

        DiscountCodeCollection DiscountCodeGetListByOwner(int campaignId, int owner);

        DiscountCodeCollection DiscountCodeGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        int DiscountCodeGetCount(params string[] filter);

        bool DiscountCodeClean(Guid orderGuid);

        #endregion

        #region discount_limit

        DiscountLimit DiscountLimitGetById(int id);
        DiscountLimit DiscountLimitGetByBid(Guid bid);
        /// <summary>
        /// 檔次是否被列入折價券黑名單
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        bool DiscountLimitEnabledGetByBid(Guid bid);
        ViewDiscountLimitCollection DiscountLimitGetAll();
        void DiscountLimitSet(DiscountLimit dc);
        /// <summary>
        /// 策展是否有商品被列入折價券黑名單
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        bool DiscountLimitEnabledGetByBrandId(int brandid);
        #endregion

        #region discount_referrer

        void DiscountReferrerSet(DiscountReferrer dr);

        DiscountReferrerCollection DiscountReferrerGetList(DiscountReferrerStatus status);

        #endregion

        #region discount_referrer_url

        DiscountReferrerUrl DiscountReferrerUrlGet(string origUrl);

        void DiscountReferrerUrlSet(DiscountReferrerUrl dru);

        DiscountReferrerUrl DiscountReferrerUrlGet(int userId, int eventActivityId);

        #endregion

        #region discount_event_campaign

        DiscountEventCampaignCollection DiscountEventCampaignGetByEventId(int eventId);

        #endregion

        #region discount_event

        DiscountEvent DiscountEventGet(int id);

        void DiscountEventSet(DiscountEvent discountEvent);

        DiscountEventCollection DiscountEventGetByPage(int page, int pageSize, string orderBy, params string[] filter);

        int DiscountEventGetCount(params string[] filter);

        DiscountEvent DiscountEventGet(string column, object value);

        #endregion

        #region discount_user

        void DiscountUserSet(DiscountUser discountUser);

        void DiscountUserCollectionBulkInsert(DiscountUserCollection usersList);

        /// <summary>
        /// 刪除指定活動Id的折價券名單
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="username"></param>
        void DiscountUserDeleteList(int eventId);

        /// <summary>
        /// 匯入全會員名單
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="username"></param>
        int DiscountUserSetAllMembers(int eventId, string username);

        /// <summary>
        /// 刪除原本的名單當中(Status=Initial)，沒有出現在新上傳的名單(Status=None)者
        /// </summary>
        /// <param name="eventId"></param>
        void DiscountUserDeleteOld(int eventId);

        /// <summary>
        /// 刪除新上傳的名單(Status=None)，重複出現於原本的名單當中(Status=Initial)者
        /// </summary>
        /// <param name="eventId"></param>
        void DiscountUserDeleteDuplicate(int eventId);

        /// <summary>
        /// 更新狀態為未發送
        /// </summary>
        /// <param name="eventId"></param>
        void DiscountUserUpdateStatus(int eventId);

        DiscountUserCollection DiscountUserGetList(int userId, int eventId);

        /// <summary>
        /// 取得折價券活動匯入名單資料筆數
        /// </summary>
        /// <returns></returns>
        int DiscountUserGetCount(int eventId, DiscountUserStatus status = DiscountUserStatus.None);

        /// <summary>
        /// 更新折價券名單狀態
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <param name="username"></param>
        void DiscountUserUpdateStatus(int eventId, int userId, DiscountUserStatus status, string username);

        #endregion

        #region discount_event_campaign

        bool DiscountEventCampaignDelete(int id);

        void DiscountEventCampaignSet(DiscountEventCampaign discountEventCampaign);

        void EventCampaignDeleteList(int eventId);

        #endregion

        #region view_discount_event_campaign

        ViewDiscountEventCampaignCollection ViewDiscountEventCampaignGetList(int eventId);

        ViewDiscountEventCampaignCollection ViewDiscountEventCampaignGetAddList(DateTime eventStart, DateTime eventEnd);

        #endregion

        #region view_discount_user

        /// <summary>
        /// 取得活動期間內，未發送/發送不足的折價券名單
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        ViewDiscountUserCollection ViewDiscountUserGetList(int userId, DateTime date);

        #endregion

        #region discount_event_promo_link
        void DiscountEventPromoLinkSet(DiscountEventPromoLink depl);
        DiscountEventPromoLinkCollection DiscountEventPromoLinkGetList(int campaign_id);
        DiscountEventPromoLinkCollection DiscountEventPromoLinkGetListByCampaignIdList(List<int> campaignIds);
        DiscountEventPromoLinkCollection DiscountEventIdGetList(int event_promo_id);
        #endregion

        #region discount_brand_link
        void DiscountBrandLinkSet(DiscountBrandLink dbl);
        DiscountBrandLinkCollection DiscountBrandLinkGetList(int campaign_id);
        DiscountBrandLinkCollection DiscountBrandLinkGetListByCampaignIdList(List<int> campaignIds);
        #endregion

        #region discount_category

        DiscountCategoryCollection DiscountCategoryGetList(int campaignId);
        DiscountCategoryCollection DiscountCategoryGetListByCampaignIds(List<int> campaignIds);
        void DiscountCategorySet(DiscountCategory discountCategory);

        #endregion

        #region discount_use_type

        void DiscountUseTypeSet(DiscountUseType discountiscountUseType);
        DiscountUseType DiscountUseTypeGet(Guid bid);

        #endregion

        #region Discount_Store

        DiscountStoreCollection DiscountStoreGetList(List<int> campaignIds);

        DiscountStoreCollection DiscountStoreGetList(Guid storeGuid);

        void DiscountStoreCloneByTemplate(int templateId, int campaignId);

        #endregion

        #region ViewDiscountMain

        ViewDiscountMainCollection ViewDiscountMainGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        int ViewDiscountMainGetCount(params string[] filter);

        #endregion

        #region ViewDiscountDetail

        /// <summary>
        /// 判斷自定序號在同期間內是否有重複
        /// </summary>
        /// <param name="customCode">自訂活動短碼</param>
        /// <param name="startTime">有效期限(啟)</param>
        /// <param name="endTime">有效期限(迄)</param>
        /// <returns>回傳重複資料Top 1</returns>
        ViewDiscountDetail ViewDiscountDetailCheckSingleSerialDuplicate(string customCode, DateTime startTime, DateTime endTime);

        /// <summary>
        /// 優先抓取 [當期] 第一筆未使用的折價券，若抓不到則回傳第一筆已使用的折價券 (自定短碼用)
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        ViewDiscountDetail ViewDiscountDetailByCustomCodeOnTime(string code);

        /// <summary>
        /// 回傳下一期一筆未使用的折價券 (自定短碼用)
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        ViewDiscountDetail ViewDiscountDetailByCustomCodeNextTime(string code);

        /// <summary>
        /// 依 code 取回折價券資料。(配合短碼功能加入 start_time desc 排序)
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        ViewDiscountDetail ViewDiscountDetailByCode(string code);

        /// <summary>
        /// 依 code 與 campaignId 取回折價券資料。(提升效能)
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        ViewDiscountDetail ViewDiscountDetailByCode(string code, int campaignId);

        ViewDiscountDetail ViewDiscountDetailByDiscountCodeId(int discountCodeId);

        /// <summary>
        /// 抓取未使用的第一筆折價券
        /// </summary>
        /// <param name="code">折價券序號</param>
        /// <returns></returns>
        ViewDiscountDetail ViewDiscountDetailByCodeWithoutUseTime(string code);

        /// <summary>
        /// 依折價券編號和使用者名稱查詢是否該活動已使用過
        /// </summary>
        /// <param name="campiagn_id">折價券編號</param>
        /// <param name="userId">使用者編號</param>
        /// <returns></returns>
        int ViewDiscountDetailCountByCampiagnIdUserId(int campiagn_id, int userId);

        /// <summary>
        /// 折價券序號資料分頁
        /// </summary>
        /// <param name="pageStart">起始頁</param>
        /// <param name="pageLength">資料數量</param>
        /// <param name="orderBy">排序欄位</param>
        /// <param name="owner">擁有者</param>
        /// <param name="type">折價券類型,預設為17Life折價券</param>
        /// <returns></returns>
        ViewDiscountDetailCollection ViewDiscountDetailCollectionGetByOwner(
            int pageStart, int pageLength, string orderBy, int owner, DiscountCampaignType type = DiscountCampaignType.PponDiscountTicket);

        /// <summary>
        /// 折價券序號資料分頁
        /// </summary>
        /// <param name="pageStart">起始頁</param>
        /// <param name="pageLength">資料數量</param>
        /// <param name="orderBy">排序欄位</param>
        /// <param name="owner">擁有者</param>
        /// <param name="filter">搜尋條件</param>
        /// <returns></returns>
        ViewDiscountDetailCollection ViewDiscountDetailGetList(
            int pageStart, int pageLength, string orderBy, params string[] filter);

        /// <summary>
        /// 折價券序號資料筆數
        /// </summary>
        /// <param name="filter">搜尋條件</param>
        /// <returns></returns>
        int ViewDiscountDetailGetListCount(params string[] filter);

        ViewDiscountDetailCollection ViewDiscountDetailGetList(int userId, DiscountCampaignType type, List<int> campaignId);

        ViewDiscountDetail ViewDiscountDetailGetByCodeId(int codeId);
        /// <summary>
        /// 取得會員可用的折價券
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        ViewDiscountDetailCollection ViewDiscountDetailGetUnUsedList(int userId, DiscountCampaignType type);

        ViewDiscountDetailCollection ViewDiscountDetailGetListByCampaignId(int campaignId);
        ViewDiscountDetailCollection ViewDiscountDetailByOrderGuid(Guid orderGuid);

        #endregion ViewDiscountDetail

        #region ViewDiscountOrder

        ViewDiscountOrderCollection ViewDiscountOrderGetList(string column, object value, int discountCampaignType = (int)DiscountCampaignType.PponDiscountTicket);

        #endregion

        #region CT_ATM

        CtAtmCollection CtAtmGetList(DateTime d);

        CtAtm CtAtmGetByOrderId(string orderId);

        CtAtm CtAtmGet(Guid orderGuid);

        CtAtm CtAtmGetByVirtualAccount(string acc);

        int CtAtmGetMaxSi();

        int CtAtmGetTodayCount(DateTime date);

        int CtAtmGetDealCount(Guid bid);

        CtAtm CtAtmGetBySeqno(string seqno, int amount);

        int CtAtmGetDealAtmMax(Guid bid);

        bool CtAtmSet(CtAtm ctatm);

        void CtAtmUpdateStatus(int status, Guid oid);

        CtAtmCompared CtAtmComparedGetByVirtualAccount(string account);

        CtAtmComparedCollection CtAtmComparedGetAtmTime(string atmTime);

        void CtAtmComparedSet(CtAtmCompared ct);

        string CtAtmMessageInformGetMaxSeqno();

        bool CtAtmLogSet(CtAtmMessageInform atmlog);

        bool CtAtmAp2ApLogCheck(string seqno);

        void CtAtmAchMarkRefundItem();

        CtAtmRefundCollection CtAtmRefundGetList();

        ViewCtAtmRefundCollection ViewCtAtmRefundGetList();

        CtAtmRefundCollection CtAtmGetRefundList(DateTime d);

        CtAtmRefund CtAtmRefundGetLatest(Guid oid);

        CtAtmRefundCollection CtAtmRefundGetList(string orderby, params string[] filter);

        CtAtmP1logCollection CtAtmGetP1LogList();

        ViewCtAtmRefundCollection ViewCtAtmRefundGetListIn(int[] si);

        CtAtmRefundCollection CtAtmGetRefundFailListIn(string[] si);

        bool CtAtmAchP1LogSet(CtAtmP1log p1Log);

        bool CtAtmAchR1LogSet(CtAtmR1log r1Log);

        CtAtmRefund CtAtmRefundGetByOrderGuid(Guid orderGuid);

        bool CtAtmRefundSet(CtAtmRefund refund);

        CtAtmRefund CtAtmRefundGetForAchReturn(string pid, string bankCode, string userAccount, int amount, DateTime date);

        CtAtmCollection CtAtmGetPast(DateTime date);

        #endregion

        #region Bank_Info

        BankInfo BankInfoGetById(int id);

        BankInfo BankInfoGet(string bankNo);

        BankInfo BankInfoGetByBranch(string bankNo, string branchNo);

        void BankInfoSet(BankInfo bankInfo);

        BankInfoCollection BankInfoGetMainList();
        /// <summary>
        /// 依銀行代碼取得銀行相關資訊
        /// </summary>
        /// <param name="bankNo">銀行代碼</param>
        /// <returns>銀行相關資訊</returns>
        BankInfo BankInfoGetMainByBankNo(string bankNo);

        /// <summary>
        /// 回傳所有BankInfo的資料
        /// </summary>
        /// <returns></returns>
        BankInfoCollection BankInfoGetList();

        BankInfoCollection BankInfoGetBranchList(string bankNo);

        bool BankInfoCollectionSet(BankInfoCollection bankInfoList);

        #endregion

        #region weeklypay

        /// <summary>
        /// 回傳賣家/分店之匯款資料
        /// 依storeGuid傳入之guid 決定回傳賣家(weekly_pay_account store_guid若匯至賣家 則為儲存seller_guid)/分店之匯款帳號
        /// </summary>
        WeeklyPayAccount GetWeeklyPayAccount(Guid bid, Guid storeGuid);

        WeeklyPayAccountCollection WeeklyPayAccountGetList(Guid bid);

        void SetWeeklyPayAccount(WeeklyPayAccount account);

        void WeeklyPayAccountSetList(WeeklyPayAccountCollection accounts);

        WeeklyPayAccountCollection WeeklyPayAccountGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        int SetWeeklyPayReport(WeeklyPayReport report);

        WeeklyPayReportCollection WeeklyPayReportGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        ViewWeeklyPayReportCollection GetViewWeeklyPayReportByDate(DateTime d_start, DateTime d_end);

        ViewWeeklyPayReportCollection GetViewWeeklyPayReportByBid(Guid bid);

        ViewWeeklyPayReportCollection GetViewWeeklyPayReportByRid(Guid rid);

        ViewBalanceSheetBillListCollection GetViewBalanceSheetByGuId(string companyId);

        ViewWeeklyPayReportCollection GetViewWeeklyPayReportByUniqueId(string uid);

        int GetViewWeeklyPayReportSumByBidRid(Guid bid, Guid rid);

        ViewWeeklyPayReportCollection GetViewWeeklyPayReportById(int id);

        void UpdateBusinessHourStatusForWeeklyPay(bool isweeklypay, Guid bid);

        WeeklyPayReport GetWeeklyPayReportById(int id);

        int WeeklyPayReportCollectionSet(WeeklyPayReportCollection wprCol);

        DataTable WeeklyPayMonitorJob(int deliveryType);

        #endregion

        #region WeeklyPayReportWms
        int SetWeeklyPayReportWms(WeeklyPayReportWm report);
        #endregion

        #region ViewPponCloseDownExpirationChange

        ViewPponCloseDownExpirationChange ViewPponCloseDownExpirationChangeGet(Guid orderGuid);
        ViewPponCloseDownExpirationChange ViewPponCloseDownExpirationChangeGetByBid(Guid bid, Guid storeGuid);

        #endregion

        #region CreditcardOrder

        CreditcardOrder CreditcardOrderGet(int id);

        CreditcardOrder CreditcardOrderGetByOrderGuid(Guid orderGuid);

        bool SetCreditcardOrder(CreditcardOrder creditcardOrder);

        /// <summary>
        /// 取得交易時間在一時間內是否超過某個金額
        /// </summary>
        /// <param name="creditcardInfo">信用卡號</param>
        /// <param name="amount">本次消費金額</param>
        /// <param name="transcationTime">交易時間</param>
        /// <param name="hours">時間(小時)<br />預設值 = 1</param>
        /// <param name="amountLimit">金額限制<br />預設值 = 5000</param>
        /// <returns>超過(true) /未超過(false)</returns>
        bool IsCreditcardOrderOverAmount(string creditcardInfo, int amount, DateTime transcationTime, int hours = 1, int amountLimit = 5000);

        CreditcardOrderCollection GetCreditcardOrderByInfo(string creditcardInfo);

        CreditcardOrderCollection GetCreditcardOrderList(Dictionary<string, string> dicCondition = null);
        CreditcardOrderCollection GetCreditcardOrderByOrderGuid(Guid guid);
        void UpdateCreditcardOrderByIdWithRefer(int id, string referenceId, bool isReference);
        void SetUserUnlockCreditcardToLock(int userId, string locker = "auto");

        CreditcardOrderCollection GetCreditcardOrderBlackList();

        void UpdateCreditcardOrderByInfoWithLocked(string creditcardInfo, string lockedId, bool isLock);
        /// <summary>
        /// 取得一段時間會員信用卡刷失敗的信用卡數目
        /// 檢查是否疑盜刷
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        int GetUserCreditcardFailCardCount(int userId, DateTime startTime, DateTime endTime);
        /// <summary>
        /// 取得一段時間會員使用盜刷卡的數目
        /// 檢查是否疑盜刷
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        int GetUserCreditcardStolenCardCount(int userId, DateTime startTime, DateTime endTime);

        #endregion

        #region CreditcardBankInfo

        CreditcardBankInfoCollection CreditcardBankInfoGetList();

        List<string> CreditcardBankInstallmentBinsGetList();

        List<string> CreditcardBankBinsGetList(int bankId);

        CreditcardBankInfo CreditcardBankInfoGetByBin(string bin);

        void CreditcardBankInfoSet(CreditcardBankInfo bankInfo);
        string CreditcardBankGetById(int id);

        List<int> CreditcardBankByInstallment();
        CreditcardBankCollection CreditcardBankGetList();

        void CreditcardBankSet(CreditcardBank bank);

        #endregion

        #region 代收轉付

        void EntrustSellReceiptSet(EntrustSellReceipt receipt);

        //依訂單Guid查詢代收轉付收據，可能有多筆但只取第1筆
        EntrustSellReceipt EntrustSellReceiptGetByOrder(Guid orderGuid);

        List<EntrustSellReceipt> EntrustSellReceiptGetListByOrder(Guid orderGuid);

        EntrustSellReceipt EntrustSellReceiptGetByTrustId(Guid orderDetailGuid);

        EntrustSellReceipt EntrustSellReceiptGetByCouponId(int couponId);

        List<ViewEntrustSellDailyPayment> ViewEntrustSellDailyPaymentList(DateTime? startTime, DateTime? endTime);

        List<ViewEntrustSellReceipt> ViewEntrustSellReceiptList(string columnName, string columnValue,
            DateTime? verifiedStartTime, DateTime? verifiedEndTime, bool? isPhysical, bool? isExported, bool? hasReceiptCode);

        /// <summary>
        /// 匯出後更動IsExported以做註記
        /// </summary>
        /// <param name="receiptIds"></param>
        void EntrustSellReceiptsSetExported(int[] receiptIds);

        EntrustSellReceipt EntrustSellReceiptGet(int receiptId);

        ViewEntrustsellOrderCollection EntruslSellOrderList(
            string filterColumn, string filterValue, string filterOrderStatus, DateTime? startTime, DateTime? endTime);

        List<ViewEntrustSellReceipt> ViewEntrustSellReceiptExportableList();

        #endregion

        #region MohistOrderInfo

        bool MohistVerifyInfoSet(MohistVerifyInfo mohist);

        MohistVerifyInfo MohistVerifyInfoGet(string couponId, string SequenceNumber);

        MohistVerifyInfo MohistVerifyInfoGet(Guid bid, string trustSequenceNumber);
        /// <summary>
        /// 取得購買期間內墨攻核銷資訊
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        MohistVerifyInfoCollection MohistVerifyInfoGetByOrderDatePeriod(DateTime dateStart, DateTime dateEnd);

        /// <summary>
        /// 取得異動期間內墨攻核銷資訊
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        MohistVerifyInfoCollection MohistVerifyInfoGetByPeriod(DateTime dateStart, DateTime dateEnd);

        /// <summary>
        /// 根據墨攻訂單編號取得墨攻核銷資訊
        /// </summary>
        /// <param name="mohistDealsId"></param>
        /// <returns></returns>
        MohistVerifyInfo MohistVerifyInfoGet(string mohistDealsId, string sequenceNumber, string couponCode);

        /// <summary>
        /// 取得需重新上傳的墨攻資訊
        /// </summary>
        /// <param name="mohistDealsId"></param>
        /// <returns></returns>
        MohistVerifyInfoCollection MohistVerifyInfoGetByRedoFlag(int flag);
        #endregion

        #region IChannelInfo

        /// <summary>
        /// 寫入iChannel資訊
        /// </summary>
        bool IChannelInfoSet(IchannelInfo iChannel);
        /// <summary>
        /// 取得iChannel資訊
        /// </summary>
        IchannelInfo IchannelInfoGetById(int id);

        #endregion

        #region ShipCompany

        /// <summary>
        /// 商家後臺_出貨管理:取得物流公司資料
        /// </summary>
        /// <param name="shipCompanyId">ship_compant id</param>
        /// <returns></returns>
        ShipCompanyCollection ShipCompanyGetList(int? shipCompanyId);

        ShipCompanyCollection ShipCompanyGetList(string searchItem, string searchValue, bool showLkSeller,
           int pageStart, int pageLength, out int totalCount);

        ShipCompany ShipCompanyGetByName(string comapnyName);
        ShipCompany ShipCompanyGet(int Id);

        bool ShipCompanySet(ShipCompany sc);
        #endregion

        #region OrderShip

        OrderShip GetOrderShipById(int id);

        /// <summary>
        /// 取最新一筆 order_ship by order guid
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        OrderShip OrderShipGet(Guid orderGuid);

        /// <summary>
        /// 商家後臺_出貨管理:查詢訂單出貨紀錄
        /// </summary>
        /// <param name="orderGuid">Order Guid</param>
        /// <returns>訂單出貨紀錄</returns>
        OrderShipCollection OrderShipGetListByOrderGuid(Guid orderGuid);

        OrderShipCollection OrderShipGetListByShipNo(string shipNo);

        /// <summary>
        /// 商家後臺_出貨管理:儲存訂單出貨紀錄
        /// </summary>
        /// <param name="os">OrderShip 欄位資料</param>
        /// <returns>OrderShip id</returns>
        int OrderShipSet(OrderShip os);

        /// <summary>
        /// 商家後臺_出貨管理:更新訂單出貨紀錄
        /// </summary>
        /// <param name="os">OrderShip 欄位資料</param>
        /// <returns>資料儲存成功與否</returns>
        bool OrderShipUpdate(OrderShip os);

        /// <summary>
        /// 商家後臺_出貨管理: 刪除出貨紀錄 (將對應出貨紀錄的 運單編號/出貨時間/物流公司 設為空值)
        /// </summary>
        /// <param name="Id">OrderShipId, 要刪除的出貨紀錄ID</param>
        /// <returns></returns>
        bool OrderShipDelete(int Id);

        #endregion

        #region OrderShipLog

        /// <summary>
        /// 商家後臺_出貨管理:儲存訂單出貨異動紀錄
        /// </summary>
        /// <param name="os">OrderShipLog 欄位資料</param>
        /// <returns>資料儲存成功與否</returns>
        bool OrderShipLogSet(OrderShipLog osl);

        #endregion

        #region ViewOrderShipList

        /// <summary>
        /// 商家後臺_出貨管理:取得ppon or hideal訂單出貨資訊
        /// </summary>
        /// <param name="productGuid">ppon:bid / hideal:hi_deal_product之guid</param>
        /// <param name="orderClassification">OrderClassification</param>
        /// <param name="type">出貨類型:一般出貨/換貨出貨</param>
        /// <returns></returns>
        ViewOrderShipListCollection ViewOrderShipListGetListByProductGuid(Guid productGuid, OrderClassification orderClassification, OrderShipType type = OrderShipType.Normal);

        /// <summary>
        /// 傳入檔次Guid, 傳回該檔次底下未填出貨單的訂單數:只抓取一般出貨單 不考慮換貨出貨單
        /// </summary>
        int ViewOrderShipListGetCountByProductGuid(Guid productGuid, OrderClassification orderClassification);

        /// <summary>
        /// 商家後台_出貨管理: 依OrderGuid, type出貨類型 取得出貨資訊
        /// </summary>
        /// <param name="orderGuid">order_guid</param>
        /// <param name="type">出貨類型:一般出貨/換貨出貨</param>
        /// <returns></returns>
        ViewOrderShipListCollection ViewOrderShipListGetListByOrderGuid(Guid orderGuid, OrderShipType type = OrderShipType.Normal);

        /// <summary>
        /// 商家後台_出貨管理: 依ProductGuid 取得出貨資訊
        /// </summary>
        /// <param name="productGuids">多筆ProductGuid組成list</param>
        /// <param name="type">出貨類型:一般出貨/換貨出貨</param>
        /// <returns></returns>
        ViewOrderShipListCollection ViewOrderShipListGetListByProductGuid(IEnumerable<Guid> productGuids, OrderShipType type = OrderShipType.Normal);

        /// <summary>
        /// Job:ShipStatusUpdate使用-抓出P好康或品生活已結檔且訂單出貨資訊皆已填寫完畢檔次
        /// (排除未出貨但已提出退貨申請訂單)
        /// 並抓取填寫之出貨日期中最大日期 以利更新
        /// 好康:deal_accounting/品生活:hi_deal_product shipped_date(出貨回覆日)
        /// </summary>
        /// <param name="classification">Ppon or HiDeal</param>
        /// <param name="queryEndTime">搜尋某時間以前之資料</param>
        /// <returns></returns>
        DataTable GetProductGuidAndMaxShipTime(OrderClassification orderClassification, DateTime queryEndTime);

        /// <summary>
        /// Job:ShipStatusUpdate使用-抓出P好康已結檔且訂單出貨資訊皆已填寫完畢檔次
        /// (排除未出貨但已提出退貨申請訂單)/(combo_detail)
        /// 並抓取填寫之出貨日期中最大日期 以利更新母檔檔次
        /// 好康:deal_accounting(出貨回覆日)
        /// </summary>
        /// <param name="classification">Ppon</param>
        /// <param name="queryEndTime">搜尋某時間以前之資料</param>
        /// <returns></returns>
        DataTable GetProductMainComboGuidAndMaxShipTime(DateTime queryEndTime);

        /// <summary>
        /// Job:ShipStatusUpdate使用-抓出P好康已結檔且訂單出貨資訊皆已填寫完畢檔次
        /// (排除未出貨但已提出退貨申請訂單)/(combo_detail)
        /// 並抓取填寫之已核對對帳單中最大日期 以利更新母檔檔次
        /// 好康:deal_accounting(已核對對帳單)
        /// </summary>
        /// <param name="classification">Ppon</param>
        /// <param name="queryEndTime">搜尋某時間以前之資料</param>
        /// <returns></returns>
        DataTable GetProductMainComboGuidAndFinalBalanceSheetDate(DateTime queryEndTime);

        DataTable ProductProgressStatusGetTable(string queryStartTime, string queryEndTime, bool isCompeleted);

        DataTable GetProductMainComboGuidAndBalanceSheetCreateDate(DateTime queryEndTime);

        #endregion

        #region view_ship_order_status

        ViewShipOrderStatusCollection ViewShipOrderStatusCollectionGet(params string[] filter);

        ViewVbsExchangeOrderListCollection ViewVbsExchangeOrderListCollectionGet(params string[] filter);

        ViewVbsReturnOrderListCollection ViewVbsReturnOrderListCollectionGet(DateTime? beginDate, DateTime? endDate, List<int> progressStatus, List<int> vendorProgressStatus, string accountId, string queryOption, string queryKeyword);

        #endregion

        #region ViewNewShipOrderToSeller

        /// <summary>
        /// 取得未出貨新訂單(預設最近1天)
        /// </summary>
        /// <param name="days"></param>
        /// <returns></returns>
        ViewNewShipOrderToSellerCollection ViewNewShipOrderToSellerCollectionGet(double days = 1);

        #endregion

        #region 逾期/即將逾期訂單 

        ViewExpiringOrderCollection ViewExpiringOrderCollectionGet();
        ViewOverdueOrderCollection ViewOverdueOrderCollectionGet();
        ViewSimpleOverdueOrder ViewSimpleOverdueOrderGet(Guid orderGuid);
        /// <summary>
        /// 取得未壓已出貨且未壓最後出貨日訂單
        /// </summary>
        /// <returns></returns>
        ViewOrderNotYetShippedCollection ViewOrderNotYetShippedCollectionGetByNoLastShipDate();

        #endregion

        ViewVendorShipLogCollection ViewVendorShipLogCollectionGetByOrderGuid(Guid orderGuid);

        #region OrderUserMemoList

        OrderUserMemoListCollection OrderUserMemoListGetList(Guid order_guid, VbsDealType type = VbsDealType.Ppon);
        OrderUserMemoListCollection OrderUserMemoListGetList(IEnumerable<Guid> order_guids, VbsDealType type = VbsDealType.Ppon);
        int OrderUserMemoListSet(OrderUserMemoList uml);
        bool OrderUserMemoListDelete(int orderUserMemoId);

        #endregion OrderUserMemoList

        #region OrderProduct

        /// <summary>
        /// 訂單商品狀態
        /// </summary>
        /// <param name="op">OrderProduct 欄位資料</param>
        /// <returns>OrderProduct id</returns>
        int OrderProductSet(OrderProduct op);

        int OrderProductSetList(OrderProductCollection products);

        OrderProductCollection OrderProductGetListByOrderGuid(Guid order_guid);

        OrderProductCollection OrderProductGetList(IEnumerable<int> orderProductIds);

        OrderProductCollection OrderProductGetList(IEnumerable<Guid> orderGuids);

        #endregion OrderProduct

        #region OrderProductOption

        OrderProductOptionCollection OrderProductOptionGetListByOrderProductIds(IEnumerable<int> orderProductIds);

        /// <summary>
        /// 訂單商品選項明細
        /// </summary>
        /// <param name="opo">OrderProductOption 欄位資料</param>
        /// <returns>資料儲存成功與否</returns>
        bool OrderProductOptionSet(OrderProductOption opo);

        int OrderProductOptionSetList(OrderProductOptionCollection options);

        #endregion OrderProductOption

        #region ViewOrderProductOptionList

        /// <summary>
        /// 透過 Product Id 及 Deal Type 取得訂單商品選項資料
        /// </summary>
        /// <param name="productId">Ppon: unique_id or HiDeal: hi_deal_product id</param>
        /// <param name="dealType">Ppon or HiDeal</param>
        /// <returns></returns>
        List<ViewOrderProductOptionList> ViewOrderProductOptionListGetIsCurrentList(int productId, VbsDealType dealType);

        /// <summary>
        /// 透過 Product Id 及 Deal Type 取得訂單商品選項資料
        /// </summary>
        /// <param name="productIds">Ppon: unique_id or HiDeal: hi_deal_product id</param>
        /// <returns></returns>
        List<ViewOrderProductOptionList> ViewOrderProductOptionListGetIsCurrentList(IEnumerable<int> productIds);

        /// <summary>
        /// 取得訂單商品選項名稱數量 Key:order_guid Value:品項規格1, 品項規格2,.. 數量
        /// </summary>
        /// <param name="infos">view_order_product_option_list 資料</param>
        /// <returns></returns>
        Dictionary<Guid, string> GetOrderProductOptionInfo(List<ViewOrderProductOptionList> infos);

        /// <summary>
        /// 取得訂單商品選項名稱數量 Key:order_guid Value:品項規格1, 品項規格2,.. 數量
        /// </summary>
        /// <param name="infos">view_order_product_option_list 資料</param>
        /// <returns></returns>
        List<OrderProductOptionInfo> GetOrderProductOptionList(List<ViewOrderProductOptionList> infos);

        /// <summary>
        /// 取得訂單商品選項名稱數量 Key:order_guid Value:品項規格1, 品項規格2,.. 數量 (沒有Group)
        /// </summary>
        /// <param name="infos">view_order_product_option_list 資料</param>
        /// <returns></returns>
        List<OrderProductOptionInfo> GetOrderProductOptionListNotGroup(List<ViewOrderProductOptionList> infos);

        /// <summary>
        /// 取得待出貨品項統計表
        /// </summary>
        /// <param name="productIds">unique id</param>
        /// <returns></returns>
        List<ViewOrderProductOptionList> ViewOrderProductOptionListGetReadyItems(IEnumerable<int> productIds);

        /// <summary>
        /// 取得商品選項統計數量 Key:品項規格1, 品項規格2,.. Value:數量
        /// </summary>
        /// <param name="infos">view_order_product_option_list 資料</param>
        /// <returns></returns>
        Dictionary<string, int> GetProductOptionStatistics(List<ViewOrderProductOptionList> infos);

        #endregion ViewOrderProductOptionList

        #region ViewReturnFormProductOptionList

        /// <summary>
        /// 透過 多筆 return_form id 取得退貨單商品選項資料
        /// </summary>
        /// <param name="returnFormIds">多筆return_Form id 組成 list</param>
        /// <returns></returns>
        List<ViewReturnFormProductOptionList> ViewReturnFormProductOptionListGetList(IEnumerable<int> returnFormIds);

        /// <summary>
        /// 取得退貨單商品選項名稱數量 Key:return_form id Value:品項規格1, 品項規格2,.. 數量
        /// </summary>
        /// <param name="returnFormIds">多筆return_Form id 組成 list</param>
        /// <returns></returns>
        Dictionary<int, string> GetReturnOrderProductOptionDesc(IEnumerable<int> returnFormIds);

        /// <summary>
        /// 商家系統-退貨管理使用:取得退貨單商品選項名稱info
        /// </summary>
        /// <param name="returnFormIds">多筆return_Form id 組成 list</param>
        /// <returns></returns>
        List<OrderProductOptionInfo> GetReturnOrderProductOptionInfo(IEnumerable<int> returnFormIds, bool? isCollected = null);

        #endregion ViewReturnFormProductOptionList

        #region ViewOrderReturnFormList

        /// <summary>
        /// 取得好康檔次退貨單資料
        /// </summary>
        /// <param name="productGuid">bid</param>
        /// <returns></returns>
        ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListByDealGuid(Guid productGuid);

        /// <summary>
        /// 取得好康檔次退貨單資料
        /// </summary>
        /// <param name="productGuids">多筆bid組成list</param>
        /// <returns></returns>
        ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListByDealGuid(IEnumerable<Guid> productGuids);

        /// <summary>
        /// 取得好康檔次退貨單資料
        /// </summary>
        /// <param name="productGuids">多筆bid組成list</param>
        /// <param name="progressStatus">多筆退款狀態組成list</param>
        /// <param name="vendorProgressStatus">多筆廠商處理進度狀態組成list</param>
        /// <param name="refundType">多筆退款方式狀態組成list</param>
        /// <returns></returns>
        ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListByDealGuid(IEnumerable<Guid> productGuids, IEnumerable<int> progressStatus, IEnumerable<int> vendorProgressStatus, IEnumerable<int> refundType);

        /// <summary>
        /// 取得好康檔次退貨單資料
        /// </summary>
        /// <param name="orderGuid">order guid</param>
        /// <returns></returns>
        ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListByOrderGuid(Guid orderGuid);

        /// <summary>
        /// 取得多個檔次之退貨單資料
        /// </summary>
        /// <param name="orderGuids">多筆 order_guid 組成List</param>
        ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListByOrderGuid(IEnumerable<Guid> orderGuids);

        /// <summary>
        /// 依退貨單號取得退貨單資料
        /// </summary>
        /// <param name="returnFormId">return_form id</param>
        /// <returns></returns>
        ViewOrderReturnFormList ViewOrderReturnFormListGet(int returnFormId);

        /// <summary>
        /// 依查詢條件取得退貨單資料(後台退貨訂單列表使用)
        /// </summary>
        /// <param name="deliveryType">商品或憑證</param>
        /// <param name="progressStatusLists">退款進度 組成List傳多筆</param>
        /// <param name="vendorProgressStatusLists">廠商退貨進度 組成List傳多筆</param>
        /// <param name="queryOption">查詢欄位</param>
        /// <param name="queryKeyword">查詢關鍵字</param>
        /// <param name="queryStartTime">退貨申請日期起</param>
        /// <param name="queryEndTime">退貨申請日期迄</param>
        /// <param name="signCompanyID">廠商統編</param>
        /// <param name="processStartTime">廠商處理日期起</param>
        /// <param name="processEndTime">廠商處理日期迄</param>
        /// <returns></returns>
        ViewOrderReturnFormListCollection ViewOrderReturnFormListGetList(
            DeliveryType? deliveryType, ProductDeliveryType? productDeliveryType, IEnumerable<int> progressStatusLists, IEnumerable<int> vendorProgressStatusLists,
            string queryOption, string queryKeyword, DateTime? queryStartTime, DateTime? queryEndTime, string signCompanyID, DateTime? processStartTime, DateTime? processEndTime, int AllowanceOption);

        ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter);
        Dictionary<int, string> GetToNotifyChannelReturnForm();

        #endregion

        #region CreditcardRefundRecord

        bool CreditcardRefundRecordSet(CreditcardRefundRecord crr);

        CreditcardRefundRecord CreditcardRefundRecordGet(string transId);

        #endregion

        #region CompanyUserOrder

        /// <summary>
        /// 新增或修改CompanyUserOrder 資料
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        bool CompanyUserOrderSet(CompanyUserOrder order);
        /// <summary>
        /// 修改訂單 WaitUpdate欄位的狀態
        /// </summary>
        /// <param name="orderGuidList">要異動的訂單編號列表</param>
        /// <param name="value">異動的值</param>
        /// <returns>異動筆數</returns>
        int CompanyUserOrderUpdateWaitUpdate(List<Guid> orderGuidList, bool value);
        /// <summary>
        /// 單筆異動訂單的WaitUpdate欄位
        /// </summary>
        /// <param name="orderGuid">訂單的GUID</param>
        /// <param name="value"></param>
        /// <returns></returns>
        int CompanyUserOrderUpdateWaitUpdate(Guid orderGuid, bool value);
        /// <summary>
        /// 查詢companyUserOrder的資料
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        CompanyUserOrderCollection CompanyUserOrderGetList(params string[] filter);
        /// <summary>
        /// 以OrderGuid查詢CompanyUserOrder資料，查無資料回傳isLoad為false的新物件
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        CompanyUserOrder CompanyUserOrderGet(Guid orderGuid);
        #endregion CompanyUserOrder

        #region ViewCompanyUserOrder

        ViewCompanyUserOrderCollection ViewCompanyUserOrderGetList(int pageNumber, int pageSize, string orderBy,
                                                                   params string[] filter);

        /// <summary>
        /// 回傳符合條件時的資料筆數
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        int ViewCompanyUserOrderListGetCount(params string[] filter);

        #endregion ViewCompanyUserOrder

        #region Corresponding Order

        /// <summary>
        /// 依廠商訂單編號取得 OrderCorresponding
        /// </summary>
        /// <param name="relatedOrderId"></param>
        /// <param name="tokenClientId"></param>
        /// <returns></returns>
        OrderCorrespondingCollection OrderCorrespondingGet(string relatedOrderId, int tokenClientId);

        /// <summary>
        /// 對應P好康訂單的其他供應商訂單資料(天貓)
        /// </summary>
        /// <param name="oc"></param>
        void OrderCorrespondingSet(OrderCorresponding oc);
        /// <summary>
        /// 依天貓欄位撈取對應P好康訂單資料
        /// </summary>
        /// <param name="related_order_id">天貓訂單號碼</param>
        /// <returns></returns>
        OrderCorrespondingCollection OrderCorrespondingListGet(string[] filter);
        /// <summary>
        /// 依訂單guid選取對應資料
        /// </summary>
        /// <param name="order_guid"></param>
        /// <returns></returns>
        OrderCorresponding OrderCorrespondingListGetByOrderGuid(Guid order_guid);
        /// <summary>
        /// 依訂單guid選取對應資料
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        OrderCorrespondingCollection OrderCorrespondingGetByOrderGuid(Guid orderGuid, AgentChannel type);
        /// <summary>
        /// 依訂單guid找出代銷訂單
        /// </summary>
        /// <param name="orderGuids"></param>
        /// <returns></returns>
        OrderCorrespondingCollection OrderCorrespondingCollectionListGetByOrderGuidList(List<Guid> orderGuids);
        /// <summary>
        /// 依訂單guid、OrderClassification找出代銷訂單
        /// </summary>
        /// <param name="orderGuids"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        OrderCorrespondingCollection OrderCorrespondingCollectionListGetByOrderGuidListAndClassification(List<Guid> orderGuids, OrderClassification type);
        OrderCorrespondingCollection OrderCorrespondingGetByOrderCheck(int type, int orderCheck);
        /// <summary>
        /// 依憑證id搜尋天貓對應P好康訂單資料
        /// <summary>
        /// 天貓檔次簡訊和信託資料筆數
        /// </summary>
        /// <param name="filter">搜尋條件</param>
        /// <returns></returns>
        int ViewOrderCorrespondingSmsLogListGetCount(params string[] filter);
        /// <summary>
        /// 天貓檔次簡訊和信託資料筆數(因為sms_log要取最後一筆，效能考量改用sql command)
        /// </summary>
        /// <param name="business_hour_guid"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        int ViewOrderCorrespondingSmsLogListGetCount(Guid business_hour_guid, params string[] filter);
        /// <summary>
        /// 天貓檔次簡訊和信託資料分頁
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewOrderCorrespondingSmsLogCollection ViewOrderCorrespondingSmsLogGetList(int pageNumber, int pageSize, string orderBy, params string[] filter);
        /// <summary>
        /// 天貓檔次簡訊和信託資料分頁(因為sms_log要取最後一筆，效能考量改用sql command)
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <param name="business_hour_guid"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewOrderCorrespondingSmsLogCollection ViewOrderCorrespondingSmsLogGetList(int pageNumber, int pageSize, string orderBy, Guid business_hour_guid, params string[] filter);
        /// <summary>
        /// 天貓訂單簡訊錯誤列表
        /// </summary>
        /// <returns></returns>
        ViewOrderCorrespondingSmsLogCollection ViewOrderCorrespondingSmsLogGetFailList();
        ViewLiontravelCouponListMainCollection ViewLionTravelCouponListMainGetList(int pageNumber, int pageSize, params string[] filter);
        int ViewLionTravelCouponListMainGetCount(params string[] filter);
        ViewOrderCorrespondingCouponCollection ViewOrderCorrespondingCouponGetList(params string[] filter);
        ViewLiontravelOrderReturnFormListCollection ViewLiontravelOrderReturnFormListGetList(int pageNumber, int pageSize, params string[] filter);
        int ViewLiontravelOrderReturnFormListGetCount(params string[] filter);
        #endregion

        #region temporary_payment_date

        /// <summary>
        /// Job:抓出檔次購買數量及退貨數量
        /// 暫付七成付款異常
        /// 暫付七成通知
        /// 以便回填暫付七成付款日
        /// </summary>
        DataTable TempPaymentGetTable(string stime, string etime);

        DataTable NoticePartillyPaymentGetTable(string queryTime);

        DataTable TempPaymentAlertGetTable(string queryStartTime, string queryEndTime);

        DataTable ReturningCountGetTable(IEnumerable<Guid> Guids);
        #endregion

        #region 補扣款通知

        /// <summary>
        /// Job:
        /// </summary>
        DataTable PaymentChangeGetTable(string queryDate);

        #endregion

        #region MasterPass

        void MasterPassAdd(MasterpassLog masterpass_log);
        void MasterPassTempAdd(MasterpassLogTemp masterpassLogTemp);
        bool MasterPassGetByOrderGuid(Guid order_guid);
        MasterpassLogCollection MasterPassGetList(DateTime dateStart, DateTime dateEnd);
        MasterpassLogCollection MasterPassUnencryptGetList(int count);
        MasterpassPreCheckoutToken MasterPassPreCheckoutTokenGet(int userId);
        bool MasterPassPreCheckoutTokenUpdateUsed(int userId);
        bool MasterPassPreCheckoutTokenSet(MasterpassPreCheckoutToken token);
        bool MasterPassPreCheckoutTokenUpdateAccessToken(int userId, string accessToken, int spentSec);
        bool MasterpassCardInfoLogNew(MasterpassCardInfoLog cardInfo);
        MasterpassCardInfoLog MasterpassCardInfoLogGet(int verifierCode, string authToken);
        bool MasterpassCardInfoLogSetUsed(int verifierCode);
        bool MasterPassPreCheckoutTokenUpdateFailReason(int userId, string reason);

        #endregion

        #region shopping_cart_item_options
        ShoppingCartItemOptionCollection ShoppingCartItemOptionGetByUid(int uid);
        ShoppingCartItemOptionCollection ShoppingCartItemOptionGetByTicketid(string ticketId);
        bool ShoppingCartItemOptionSet(ShoppingCartItemOption item);
        #endregion shopping_cart_item_options

        #region view_ichannel_info

        /// <summary>
        /// 根據訂單時間取得iChannel資訊
        /// </summary>
        ViewIchannelInfoCollection ViewIchannelInfoGetList(DateTime dateStart, DateTime dateEnd);

        /// <summary>
        /// 根據訂單編號取得iChannel資訊
        /// </summary>
        ViewIchannelInfoCollection ViewIchannelInfoGet(Guid guid);

        #endregion

        #region LinePay

        bool LinePayApiLogSet(LinePayApiLog data);

        #region LinePayTransLog

        int LinePayTransLogIdGetIdByTransId(string transId);
        LinePayTransLog LinePayTransLogGet(int id);
        LinePayTransLog LinePayTransLogSet(LinePayTransLog trans);
        LinePayTransLog LinePayTransLogGet(string ticketId);
        LinePayTransLog LinePayTransLogGetByTransId(string transId);
        LinePayTransLogCollection LinePayTransLogGetRetryData(LinePayTransStatus status, string checkColumn, int retryLimit);

        #endregion

        LinePayRefundLog LinePayRefundLogSet(LinePayRefundLog refundLog, string userId = "sys");
        LinePayRefundLogCollection LinePayRefundLogGetRetryData();

        #endregion LinePay

        #region ThirdPartyPay

        ThirdPartyPayTransLog ThirdPartyPayTransLogGetById(int id);
        ThirdPartyPayTransLog ThirdPartyPayTransLogGet(PaymentAPIProvider paymentOrg, int userId, string shortGuid);
        ThirdPartyPayTransLog ThirdPartyPayTransLogGetByOrgTransId(PaymentAPIProvider paymentOrg, string transId);
        ThirdPartyPayTransLog ThirdPartyPayTransLogGetByTransId(string transId);
        int ThirdPartyPayTransLogSet(ThirdPartyPayTransLog data);
        void ThirdPartyPayRefundLogSet(ThirdPartyPayRefundLog refundLog);
        void ThirdPartyPayApiLogSet(ThirdPartyPayApiLog apiLog);

        #endregion

        #region shopping_cart_group

        ShoppingCartGroup ShoppingCartGroupGet(Guid guid);
        ShoppingCartGroupCollection ShoppingCartGroupGetByUid(int uid);
        void ShoppingCartGroupSet(ShoppingCartGroup group);
        #endregion

        #region shopping_cart_group_d
        ShoppingCartGroupDCollection ShoppingCartGroupDGetByCartGuid(Guid cid);
        void ShoppingCartGroupDSet(ShoppingCartGroupD groupD);
        void AddTaxDeclareEinvoiceMedia(DateTime taxDstart, DateTime taxDend);
        #endregion

        #region preventing_oversell

        PreventingOversell AddPreventingOversell(PreventingOversell entity);
        int GetPreventingOversellProcessingQty(Guid businessHourGuid);
        bool SetPreventingOversellExpired(Guid bid, int qty, int userId);
        bool SetPreventingOversellExpired(int pid);

        #endregion

        #region ExpiredVerifiedLog
        void ExpiredVerifiedLogSet(ExpiredVerifiedLog log);
        #endregion ExpiredVerifiedLog

        #region blog_referrer_url
        void BlogReferrerUrlSet(BlogReferrerUrl url);
        #endregion blog_referrer_url

        IEnumerable<Guid> GetOldOrderWithNoUninvoiceAmount(int? count);

        #region
        ViewIspMakeOrderInfoCollection GetViewIspMakeOrderInfoCollection();
        ViewIspMakeOrderInfoCollection GetViewIspMakeOrderInfo(List<Guid> orderGuidList);
        #endregion

        #region OrderLog

        void OrderLogSet(OrderLog log);

        OrderLog OrderLogGet(int id);

        #endregion OrderLog

        #region TaishinEcPayLog
        void TaishinEcPayLogSet(TaishinEcPayLog log);
        #endregion TaishinEcPayLog

        DataTable ReturnFormToShopCouponID(int rtnID);

        DataTable GetNonEinvoicedOrderMember(DateTime start, DateTime end);

        DataTable GetEinvoiceMedia(string com_id);

        void CreditcardFraudDataBulkInsert(CreditcardFraudDatumCollection cfdc);

        void CreditcardFraudDataSet(CreditcardFraudDatum cfd);

        CreditcardFraudDatum CreditcardFraudDataGet(bool addressIsEmpty, string ip, string address, string cardNumber, int? userId, string type);

        CreditcardFraudDatumCollection CreditcardFraudDataGet(CreditcardFraudDataType type);

        CreditcardFraudDatum CreditcardFraudDataGetById(string id);

        CreditcardFraudDatumCollection CreditcardFraudDataGet(DateTime? sDataTime, DateTime? eDataTime);

        CreditcardFraudDatumCollection CreditcardFraudDataGetUserId(DateTime sDataTime, DateTime eDataTime);

        CreditcardFraudDatumCollection CreditcardFraudDataGetIp(DateTime sDataTime, DateTime eDataTime);

        DataTable CreditcardFraudDataGetByUserId(int userId);

        DataTable CreditcardFraudDataGetByErrorCard(DateTime sDataTime, DateTime eDataTime);

        DataTable CreditcardFraudDataGetByCreateIp(string ip);

        CreditcardOtpWhitelistCollection CreditcardOTPWhiteList();

        void CreditcardOtpWhitelistBulkInsert(CreditcardOtpWhitelistCollection cowc);

        int DeleteExcludeWhiteList();
        void TruncateCreditcardOtpWhitelist();

        CreditcardOtpWhitelist CreditcardOtpWhitelistGet(int userId);

        void CreditcardOtpWhitelistSet(CreditcardOtpWhitelist cow);

        #region OTPLog
        void OtpLogSet(OtpLog log);

        OtpLog OtpLogGet(int id);
        #endregion

        #region TempCreditcardOtp

        TempCreditcardOtp TempCreditcardOtpGet(string transId);
        bool TempCreditcardOtpSet(TempCreditcardOtp otp);
        bool TempCreditcardOtpDelete(int Id);
        #endregion

        void CreditcardOtpSet(CreditcardOtp otp);
        CreditcardOtp CreditcardOtpGet(int userId, string creditcardInfo);
        CreditcardOtp CreditcardOtpGetByTime(int uniqueId, string creditcardInfo);

        #region Wms

        void WmsOrderSet(WmsOrder wo);


        #endregion

        #region PayeasyInfo

        bool PezChannelSet(PezChannel info);
        PezChannel PezChannelGetById(int id);
        ViewPezChannelCollection ViewPezChannelGetList(DateTime startTime, DateTime endTime);

        #endregion

        DataTable SKmOrderGetForAlibaba(Guid TrustId);
        DataTable SKmOrderGetNotVerifiedQuantityForAlibaba();
    }

    #region class

    public class OrderQuantityAmount
    {
        public Guid BusinessHourGuid { get; set; }

        public int Quantity { get; set; }

        public int Amount { get; set; }
    }

    public class OrderProductOptionInfo
    {
        public int ReturnFormId { get; set; }

        public Guid OrderGuid { get; set; }

        public string OptionDescription { get; set; }

        public string ItemNo { get; set; }

        public int Quantity { get; set; }

        public List<int> OrderProductIds { get; set; }
    }

    public class RefundedAmount
    {
        public int CreditCard { get; set; }

        public int PCash { get; set; }

        public int SCash { get; set; }

        public int Pscash { get; set; }

        public int BCash { get; set; }

        public int Atm { get; set; }

        public int TCash { get; set; }
        public int LinePay { set; get; }
        public int TaishinPay { set; get; }
        public int IspPay { get; set; }
    }

    public class EinvoiceAllowanceInfo
    {
        public int PaymentTransId { get; set; }
        public int Id { get; set; }
        public string TransId { get; set; }
        public DateTime? TransTime { get; set; }
        public int? Amount { get; set; }
        public DateTime OrderTime { get; set; }
        public decimal InvoiceSumAmount { get; set; }
        public int InvoiceMode { get; set; }
        public DateTime? InvoiceNumberTime { get; set; }
        public decimal OrderAmount { get; set; }
        public string InvoiceNumber { get; set; }
        public bool OrderIsPponitem { get; set; }
        public int BusinessHourStatus { get; set; }
        public int? ReturnFormId { get; set; }
        public int? AllowanceStatus { get; set; }
        public bool InvoicePapered { get; set; }
    }

    #endregion
}
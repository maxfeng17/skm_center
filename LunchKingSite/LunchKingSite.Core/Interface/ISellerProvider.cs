using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using LunchKingSite.Core.ModelCustom;
using SubSonic;

namespace LunchKingSite.Core
{
    public interface ISellerProvider : IProvider
    {
        #region Category

        Dictionary<int, string> CategoriesNameGet();

        Category CategoryGet(int categoryId);

        Category CategoryGetAlibaba (string AlibabaId);

        CategoryDependencyCollection GatCategoryParent (int categoryId);

        void CategoryDelete(Category category);

        /// <summary>
        /// 取得需要排DealTimeSlot活動時程表的Category
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        CategoryDealTimeSlotCollection CategoryDealTimeSlotGetList(int type = 0);

        int GetCategoryMaxCode();
        int GetCategoryMaxId();
        CategoryCollection CategoryGetAll(string orderBy);

        CategoryCollection CategoryGetList(int type = 0);

        CategoryCollection CategoryGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        CategoryCollection CategoryGetList(List<int> categoryId);

        Dictionary<int, Category> CategoryGetDictionary();

        int CategorySetList(CategoryCollection cc);

        int CategorySet(Category category);

        /// <summary>
        /// 根據公司別(館別)取得類別
        /// </summary>
        /// <param name="deptType">公司別(館別)</param>
        /// <returns>類別集合</returns>
        CategoryCollection CategoryGetListByDepartment(DepartmentTypes deptType);

        /// <summary>
        /// 由新光業務中台的id取得我們對應的類別
        /// </summary>
        /// <param name="AlibabaId">業務中台的類別id</param>
        /// <returns>類別集合</returns>
        CategoryCollection CategoryGetListByAlibabaId(long AlibabaId);

        string GetCategoryNameByCid(int cid);
        #endregion Category

        #region CategoryDependency

        void CategoryDependencyDelete(CategoryDependencyCollection categoryDependencyCollection);
        CategoryDependencyCollection CategoryDependencyGetList(string orderBy, params string[] filter);
        CategoryDependencyCollection CategoryDependencyGetAll(string orderBy);
        CategoryDependencyCollection CategoryDependencyByRegionGet(int regionId);
        CategoryDependencyCollection CategoryDependencyGet(int parentId, int categoryId);
        int CategoryDependencySetList(CategoryDependencyCollection cdc);
        int CategoryDependencySet(CategoryDependency categoryDependency);

        #endregion CategoryDependency

        #region CategoryRegionDisplayRule

        CategoryRegionDisplayRuleCollection CategoryRegionDisplayRuleGetList();
		 
	    #endregion

        #region ViewCategoryDependency

        ViewCategoryDependencyCollection ViewCategoryDependencyGetAll(string orderBy);
        ViewCategoryDependencyCollection ViewCategoryDependencyGetByParentId(int parentId);
        ViewCategoryDependencyCollection ViewCategoryDependencyGetByParentType(int parent_type);
        ViewCategoryDependencyCollection ViewCategoryDependencyGetByCategoryId(int category_type);
        ViewCategoryDependencyCollection ViewCategoryDependencyGetByCid(int categoryId);
        ViewCategoryDependencyCollection ViewCategoryDependencyGetByCategoryIdList(List<int> categoryid_list);
        ViewCategoryDependencyCollection ViewCategoryDependencyGetByCategoryCodeList(List<int> categoryCode_list);

        #endregion ViewCategoryDependency

        #region Seller

        Seller SellerGet(Guid g);

        Seller SellerGet(string column, object value);

        Seller SellerGetByBid(Guid bid);

        Seller SellerGetByCompany(string SellerName, string SignCompanyID);

        SellerCollection SellerGetList();

        SellerCollection SellerGetList(string column, object value);

        SellerCollection SellerGetList(List<Guid> guids);

        List<string> SellerGetColumnListByLike(string column, string partValue);

        SellerCollection SellerGetByCompanyList(string SellerName, string SignCompanyID);
        Seller SellerGetBySellerGuid(string CompanyName, string SellerGuid);

        SellerCollection SellerGetListBySellerGuidList(List<Guid> seelerGuidList);
        /// <summary>
        /// 判斷前台自行新增賣家資訊是否有重複
        /// </summary>
        /// <param name="seller_name">賣家名稱</param>
        /// <param name="signcompany_id">統編</param>
        /// <param name="company_boss_name">負責人</param>
        /// <returns></returns>
        SellerCollection SellerGetRepeatList(string seller_name, string signcompany_id, string company_boss_name);

        SellerCollection SellerGetList(int pageStart, int pageLength, string orderBy, params string[] filter);
        SellerCollection SellerGetList(int pageStart, int pageLength, string orderBy, int SalesId, string CrossDeptTeam, params string[] filter);
        SellerCollection SellerGetListByAppId(string OauthClientAppId);

        /// <summary>
        /// 取得商家聯絡資訊 by 在地/宅配
        /// (此sql為一次性使用沒有下索引)
        /// </summary>
        /// <param name="deliveryType"></param>
        /// <returns></returns>
        ViewSellerContactCollection GetSellerContactsByDeliveryType(DeliveryType deliveryType);
        

        /// <summary>
        /// [商家列表-搜尋]根據輸入的條件，取得商家列表
        /// </summary>
        /// <param name="currentPage">當前頁數</param>
        /// <param name="pageSize">一頁筆數</param>
        /// <param name="orderColumn">要排序的欄位</param>
        /// <param name="seller">查詢條件</param>
        /// <param name="userEmail">使用者的email (查詢條件)</param>
        /// <param name="isPrivate">是否勾選[自己的] (查詢條件)</param>
        /// <param name="isPublic">是否勾選[公池] (查詢條件)</param>
        /// <returns></returns>
        List<Seller> SellerGetList(int currentPage, int pageSize, string orderColumn, Seller seller, string userEmail,
            bool isPrivate, bool isPublic, int EmpUserId, string CrossDeptTeam, string Dept, int CityId, string SellerLevelList,
            string SellerPorperty, int SellerManagLogNumber, string SellerManagLogDatepart, int? SalesId,
            ref int RowCount);

        int SellerGetCount(Seller seller, string userEmail, bool isPrivate, bool isPublic, int EmpUserId, string CrossDeptTeam,
            string Dept, int CityId, string SellerLevelList, string SellerPorperty, int SellerManagLogNumber,
            string SellerManagLogDatepart, int RowCount, int? SalesId);

        SellerCollection SellerGetParentList(params string[] filter);

        /// <summary>
        /// 新光三越取得分店
        /// </summary>
        /// <returns></returns>
        SellerCollection SellerGetSkmParentList();

        int SellerGetCount(int SalesId, string CrossDeptTeam, params string[] filter);
        /// <summary>
        /// 搜尋退件的賣家紀錄
        /// </summary>
        /// <param name="username">申請人</param>
        /// <param name="temp_status">申請狀態</param>
        /// <param name="theType"></param>
        /// <returns></returns>
        SellerCollection SellerCollectionGetReturnCase(string username, SellerTempStatus temp_status);

        /// <summary>
        /// 搜尋賣家分頁
        /// </summary>
        /// <param name="pageStart">分頁開始頁數</param>
        /// <param name="pageLength">每頁筆數</param>
        /// <param name="search_keys">搜尋條件(欄位和值)</param>
        /// <param name="orderBy">排序欄位</param>
        /// <returns>賣家列表</returns>
        SellerCollection SellerGetTempList(int pageStart, int pageLength, KeyValuePair<string, string> search_keys, string user_name, string orderBy);

        SellerCollection SellerGetListByNameWithActiveTransport(string sellerName);

        SellerCollection SellerGetListByLikelyName(string sellerName);

        SellerCollection SellerGetList(IEnumerable<Guid> sellerGuids);

        #region SellerContractFiles
        SellerContractFile SellerContractFileGetByGuid(Guid guid);
        SellerContractFileCollection SellerContractFileGetListBySellerGuid(Guid sellerGuid);
        bool SellerContractFileSet(SellerContractFile s);

        #endregion
        int SellerGetCount();

        /// <summary>
        /// 計算搜尋賣家的資料筆數
        /// </summary>
        /// <param name="search_keys">搜尋條件(賣家欄位和值)</param>
        /// <returns>筆數</returns>
        int SellerGetTempCount(KeyValuePair<string, string> search_keys, string user_name);

        int SellerGetCountByPrefix(string vbsPrefix);

        SellerCollection SellerGetByPrefix(string vbsPrefix);
        bool SellerSet(Seller s);
        bool SellerSaveAll(SellerCollection s);
        string SellerGetLastSellerId();
        SellerCollection SellerGetListBySalesName(string salesName, bool isPool);

        #endregion Seller

        SellerSaleCollection SellerSaleGetBySellerGuid(Guid sellerGuid);

        SellerSale SellerSaleGetBySellerGuid(Guid sellerGuid, int salesGroup, int salesType);

        bool SellerSaleSet(SellerSale sellerSales);

        void SellerSaleDelete(Guid sellerGuid);

        void SellerSaleDelete(Guid sellerGuid, int salesGroup);

        void SellerSaleDelete(Guid sellerGuid, int salesGroup, int salesType);


        #region SellerCategory

        SellerCategoryCollection SellerCategoryGetList(Guid SellerGuid);

        bool SellerCategorySave(SellerCategory s);

        bool SellerCategoryDelete(int id);

        #endregion SellerCategory

        #region BusinessHour

        BusinessHour BusinessHourGet(Guid guid);

        BusinessHourCollection BusinessHourGetListBySeller(Guid guid);

        BusinessHourCollection BusinessHourGetList(string column, object value);

        BusinessHourCollection BusinessHourGetList(IEnumerable<Guid> bids);

        BusinessHourCollection BusinessHourGetListBySettlementTime(DateTime date_start, DateTime date_end);

        bool BusinessHourDelete(Guid value);

        bool BusinessHourSet(BusinessHour bh);

        /// <summary>
        /// 撈取全家過期檔次(business_hour_delivery_date_e 超過 now)
        /// </summary>
        /// <param name="precheckDays">幾天前到現在</param>
        /// <returns></returns>
        Dictionary<Guid, Guid> BusinessHourGetFamiExpiredDeal(int precheckDays);

        #endregion BusinessHour

        #region ViewBuildingSellerDeliveryHour

        ViewBuildingSellerDeliveryHourCollection ViewBuildingSellerDeliveryHourGetList(Guid sellerGuid, Guid buildingGuid);

        #endregion ViewBuildingSellerDeliveryHour

        #region ViewSellerCityBizHour

        ViewSellerCityBizHour ViewSellerCityBizHourGet(string column, object value);

        ViewSellerCityBizHourCollection ViewSellerCityBizHourGetList(int pageStart, int pageLength, string orderby, params string[] filter);

        ViewSellerCityBizHourCollection ViewSellerCityBizHourGetListByBuilding(int pageStart, int pageLength, string orderby, Guid buildingGuid, params string[] filter);

        int ViewSellerCityBizHourGetCountByBuilding(Guid buildingGuid, params string[] filter);

        #endregion ViewSellerCityBizHour

        #region ViewSellerCategoryCity

        ViewSellerCategoryCityCollection ViewSellerCategoryCityGetList(string column, object value, bool onlyOnline, string orderBy);

        #endregion ViewSellerCategoryCity

        #region Store

        bool StoreSet(Store store);

        Store StoreGet(Guid storeGuid);

        StoreCollection StoreGetListBySellerGuid(Guid sellerGuid);

        SellerCollection StoreGetListBySellerGuidAndStoreCode(List<Guid> sellerGuid, string storeCode);

        SellerCollection StoreGetListByPCPmemberCard(string cardGroupId);

        StoreCollection StoreGetListBySellerName(string sellerName, bool fuzzySearch = false);

        StoreCollection StoreGetListByStoreGuidList(List<Guid> storeGuidList);

        StoreCollection StoreGetListBySellerGuidList(List<Guid> sellerGuidList);


        /// <summary>
        /// 傳入Bid 和 1個布林參數 (true的話代表只抓該檔次有賣的分店), 傳回分店集合
        /// </summary>
        /// <param name="bid">BusinessHourGuid</param>
        /// <param name="isBidSellStoresOnly">是否只抓有賣該檔次的分店, 預設為否</param>
        /// <returns></returns>
        StoreCollection StoreGetListByBid(Guid bid, bool isBidSellStoresOnly = false);

        DataTable GetInitialSellerBranchStores(Guid sellerGuid);

        /// <summary>
        /// 搜尋退件的分店紀錄
        /// </summary>
        /// <param name="username">申請人</param>
        /// <param name="temp_status">申請狀態</param>
        /// <param name="theType"></param>
        /// <returns></returns>
        StoreCollection StoreCollectionGetReturnCase(string username, SellerTempStatus temp_status);

        ViewSellerStoreCollection ViewSellerStoreCollectionGetReturnCase(string username, SellerTempStatus temp_status);

        ViewSellerStoreCollection ViewSellerStoreCollectionGetSellerList(List<Guid> list);

        /// <summary>
        /// 依異動日期區間抓取該期間內有檔次上檔或資料異動之賣家資料
        /// </summary>
        /// <returns>賣家list</returns>
        StoreCollection StoreGetListByPponDeal();

        #endregion Store

        #region SkmShoppe
        SkmShoppe SkmShoppeGet(Guid storeGuid);
        SkmShoppeCollection SkmShoppeCollectionGetStore();
        SkmShoppeCollection SkmShoppeCollectionGetAll(bool isShoppe = true,bool isContainsNotAvailableShoppe = false);
        SkmShoppeCollection SkmShoppeCollectionGetAllSeller(bool isAvailable);
        SkmShoppeCollection SkmShoppeCollectionGetBySeller(Guid sellerGuid, bool IsShoppe);
        SkmShoppeCollection SkmShoppeCollectionGetByShopCode(string shopCode);
        SkmShoppeCollection SkmShoppeCollectionGetBySellerList(List<Guid> lists);
        ViewPponStoreSkmCollection ViewPponStoreSkmGetByBid(Guid bid);
        ViewPponStoreSkmCollection ViewPponStoreSkmByBidStoreGuid(Guid bid, Guid storeGuid);
        Dictionary<string, string> SkmShoppeGetShopCodeNameList();
        #endregion SkmShoppe


        #region StoreAccountInfo

        bool IsStoreAcctInfoHave(Guid sellerGuid, Guid sotreGuid);

        bool StoreAcctSet(StoreAcctInfo sai);

        bool StoreAcctLogSet(StoreAcctInfoLog sail);

        StoreAcctInfo GetStoreAcctByGuids(Guid sellerGuid, Guid storeGuid);

        StoreAcctInfoCollection StoreAcctInfoGetAllList();

        string GetStoreAcctCodeBySeller(Guid sellerGuid);

        int GetStoreAcctNoLastestBySeller(Guid sellerGuid);

        SellerCollection GetSellerAll();
        SellerCollection GetSellerByGuid(Guid guid);

        StoreCollection GetStoreAll();

        #endregion StoreAccountInfo

        #region ViewCouponStore

        ViewCouponStoreCollection ViewCouponStoreCollectionGet(Guid bid, Guid sellerid, Guid storeid);

        ViewCouponStoreCollection ViewCouponStoreCollectionGet(Guid bid, Guid sellerid, Guid storeid, string sequencenumber, string code);

        ViewCouponStoreCollection ViewCouponStoreCollectionGet(Guid sellerid, Guid storeid, string sequencenumber, string code);

        #endregion ViewCouponStore

        DealLabelCollection GetDealLabelCollection();

        #region VbsUserManualCategory

        VbsUserManualCategoryCollection VbsUserManualCategoryGetEnabledList();

        #endregion VbsUserManualCategory

        #region VbsUserManualFile

        VbsUserManualFileCollection VbsUserManualFileGetEnabledList();

        #endregion VbsUserManualFile

        #region view_seller_deal

        ViewSellerDealCollection ViewSellerDealGetList(string searchItem, string searchValue, bool showLkSeller,
            int pageStart, int pageLength, out int totalCount);

        #endregion

        #region view_Member_Detail

        ViewVbsMemberDetailCollection ViewVbsMemberDetailGet(int userId);

        #endregion

        #region vbs_company_detail

        bool VbsCompanyDetailSet(VbsCompanyDetail vcd);

        VbsCompanyDetail VbsCompanyDetailGet(Guid sellerGuid);

        #endregion

        #region view_vbs_seller_member_consumption
        int ViewVbsSellerMemberConsumptionCollectionGetCount(int cardGroupId, string search);
        ViewVbsSellerMemberConsumptionCollection ViewVbsSellerMemberConsumptionCollectionGet(int cardGroupId, string search, int page, int pageSize, int OrderByField);
        ViewVbsSellerMemberConsumption ViewVbsSellerMemberConsumptionGet(int userId, int cardGroupId);
        #endregion


        #region view_vbs_seller_member

        int ViewVbsSellerMemberCount(int sellerUserId, SellerMemberType type);
        ViewVbsSellerMemberCollection ViewVbsSellerMemberGet(int sellerUserId, SellerMemberType type, string criteria, int startIndex, int size, params string[] filter);
        
        int ViewVbsSellerMemberCount(int sellerUserId, SellerMemberType type, string criteria, params string[] filter);
        ViewVbsSellerMember ViewVbsSellerMemberGet(int? userId, int? sellerMemberId, int sellerUserId);
        ViewVbsSellerMember ViewVbsSellerMemberGet(int userId, int cardGroupId);

        #endregion

        #region vbs

        List<int> VbsRelationshipGet(int userId);
        
        SellerCollection VbsSellerGuidGet(int userId);
        //StoreCollection VbsSellerGuidGet(int userId);

        #endregion


        #region vbsISP

        VbsInstorePickupCollection VbsInstorePickupGet(Guid sellerGuid);

        VbsInstorePickup VbsInstorePickupGet(Guid sellerId, int channel);

        VbsInstorePickupCollection GetISPStoreStauts();

        void VbsInstorePickupSet(VbsInstorePickup vip);

        int VbsInstorePickupSetList(VbsInstorePickupCollection vipc);

        ViewVbsInstorePickupCollection ViewVbsInstorePickupCollectionGet(Guid sellerId);

        ViewVbsInstorePickupCollection ViewVbsInstorePickupCollectionGet(int pageStart, int pageLength, params string[] filter);

        int ViewVbsInstorePickupCollectionGetCount(params string[] filter);

        ViewVbsInstorePickup ViewVbsInstorePickupGet(Guid sellerId, int channel);
        #endregion vbsISP

        #region proposal

        Proposal ProposalGet(int id);
        Proposal ProposalGet(Guid bid);

        void ProposalSet(Proposal Proposal);

        Proposal ProposalGet(string column, object value);
        ProposalCollection ProposalGetListByHouse();
        ProposalCollection ProposalGetByBids(IEnumerable<Guid> bids);
        ProposalCollection ProposalGetByOrderTimeS();

        ProposalCollection ProposalGetListBySellerGuid(Guid sellerGuid);

        ProposalCollection ProposalGetListByProduction();
        ProposalCollection ProposalGetListByRestriction(int dealType2);

        bool ProposalDelete(int id);

        ProposalContractFile ProposalContractFileGetByGuid(Guid guid);

        ProposalContractFileCollection ProposalContractFileGetListBypid(int ProposalId);

        bool ProposalContractFileSet(ProposalContractFile s);

        ProposalContractFileCollection ProposalContractFileGetByType(int proposal_id, int Type);

        SellerContractFileCollection SellerContractFileGetByType(Guid seller_guid, int Type);

        ProposalCollection ProposalGetPhotographer();

        List<int> ProposalMultiDealGetByBrandName(string brandName);

        #endregion



        #region view_proposal_seller

        ViewProposalSeller ViewProposalSellerGet(string column, object value);

        ViewProposalSellerCollection ViewProposalSellerGetListByPeriod(int ProposalApplyFlag, int EmpUserId, string CrossDeptTeam, int month);

        ProposalCollection SellerProposalSellerGetList(List<Guid> sellerGuid,int pid, Guid? BusinessHourGuid, string BrandName, string Deals, string OrderTimeS, string OrderTimeE, string SalesName, string isWms);

        ViewProposalSellerCollection ViewProposalSellerGetList(int pageStart, int pageLength, string orderBy, ProposalStatus status, int flag, int specialFlag,bool showChecked,int? SalesId, string StatusFlag,string DeptId ,  string CrossDeptTeam ,int PhotographerFlag , int PhotoType , int EmpUserId, string dealStatus, List<int> pids, System.Text.StringBuilder sSQL, params string[] filter);

        int ViewProposalSellerGetCount(ProposalStatus status, int flag, int specialFlag, bool showChecked, int? SalesId, string StatusFlag, string DeptId , string CrossDeptTeam , int PhotographerFlag, int PhotoType, int EmpUserId, string dealStatus, List<int> pids, ref string sSQL, params string[] filter);

        ViewProposalSellerCollection ViewProposalSellerGetListForProduction(int pageStart, int pageLength, string orderBy, string city, int dateType, 
                                                                            DateTime dateStart, DateTime dateEnd, int statusType, int status, string emp, 
                                                                            int dealProperty, int picType, int dealType, int dealSubType, bool marketingResource, bool chkProduction,
                                                                            ref string sSQL);

        int ViewProposalSellerGetCountForProduction(string city, int dateType, DateTime dateStart, DateTime dateEnd, int statusType, 
                                                    int status, string emp, int dealProperty, int picType, int dealType, 
                                                    int dealSubType, bool marketingResource, bool chkProduction);

        ViewProposalSellerCollection ViewProposalSellerGetListForPhotohrapher(DateTime beginTime, DateTime endTime);

        #endregion

        #region ProposalLog

        void ProposalLogSet(ProposalLog log);
        ProposalLog ProposalLogSaveThenSelect(ProposalLog log);
        ProposalLogCollection ProposalLogGetList(int proposalId, ProposalLogType type);

        ProposalLogCollection ProposalLogGetList(int proposalId, List<int> types);

        void ProposalLogDeleteList(int pid);

        #endregion

        #region ProposalAssignLog

        void ProposalAssignLogSet(ProposalAssignLog log);

        ProposalAssignLogCollection ProposalAssignLogGetList(int proposalId);
        ProposalAssignLogCollection ProposalAssignLogGetList(List<int> proposalId);

        bool ProposalAssignLogDelete(int pid);

        #endregion

        #region ProposalReturnDetail
        void ProposalReturnDetailSave(ProposalReturnDetail returnDetail);
        #endregion

        #region ProposalMultiDeal
        ProposalMultiDealCollection ProposalMultiDealGetByPid(int pid);
        ProposalMultiDealCollection ProposalMultiDealGetListByPid(List<int> pids);
        //ProposalMultiDealCollection ProposalMultiDealGetByOptions(string searchValue);
        ProposalMultiDeal ProposalMultiDealGetByBid(Guid bid);
        ProposalMultiDeal ProposalMultiDealGetAllByBid(Guid bid);
        ProposalMultiDealCollection ProposalMultiDealGetByItemName(string itemName);
        ProposalMultiDealCollection ProposalMultiDealGetListByBid(List<Guid> bids);
        ProposalMultiDeal ProposalMultiDealGetById(int id);
        ProposalMultiDealCollection ProposalMultiDealGetListById(List<int> id);

        List<int> ProposalMultiDealGetPidListById(List<int> id);
        Dictionary<Guid, string> ProposalMultiDealGetOptionsDictByOnlineDeals();

        bool ProposalMultiDealDelete(ProposalMultiDeal deal);

        bool ProposalMultiDealDeleteByPid(int pid);

        bool ProposalMultiDealSetList(ProposalMultiDealCollection multiDeals);

        bool ProposalMultiDealsSet(ProposalMultiDeal deal);

        bool ProposalMultiDealsDelete(int pid);
        #endregion ProposalMultiDeal

        #region ProposalMultiOptionSpec
        ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByMid(int mid);
        ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByMids(List<int> mids);
        ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByProduct(Guid gid);
        ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByItem(Guid gid);
        ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByBid(Guid bid);
        ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByProductItem(Guid pid, Guid itemGuid);
        List<Guid> ProposalMultiOptionSpecGetItemGuidByBid(Guid bid);
        bool ProposalMultiOptionSpecSet(ProposalMultiOptionSpec spec);
        bool ProposalMultiOptionSpecDelete(int mid, Guid pid, Guid iid);
        bool ProposalMultiOptionSpecDelete(Guid bid);
        #endregion ProposalMultiOptionSpec

        #region ProposalRestriction
        bool ProposalRestrictionSet(ProposalRestriction res);

        ProposalRestrictionCollection ProposalRestrictionGet();

        ProposalRestriction ProposalRestrictionGetById(int id);

        ProposalRestriction ProposalRestrictionGetByCodeId(int codeId);
        #endregion


        #region ProposalRelatedDeal
        ProposalRelatedDealCollection ProposalRelatedDealGetByPid(int pid);
        bool ProposalRelatedDealSet(ProposalRelatedDeal deal);
        bool ProposalRelatedDealDelete(ProposalRelatedDeal deal);
        #endregion ProposalRelatedDeal

        #region BusinessChangeLog

        void BusinessChangeLogSet(BusinessChangeLog log);

        BusinessChangeLogCollection BusinessChangeLogGetList(Guid bid);

        #endregion

        #region SellerChangeLog

        void SellerChangeLogSet(SellerChangeLog log);

        SellerChangeLogCollection SellerChangeLogGetList(Guid sid);

        #endregion

        #region SellerManageLog

        void SellerManageLogSet(SellerManageLog log);

        SellerManageLogCollection SellerManageLogGetList(Guid sid);

        #endregion

        #region StoreChangeLog

        void StoreChangeLogSet(StoreChangeLog log);

        StoreChangeLogCollection StoreChangeLogGetList(Guid sid);

        #endregion

        #region store_category

        StoreCategoryCollection StoreCategoryCollectionGetByStore(Guid storeGuid);

        bool StoreCategorySet(StoreCategory storeCategory);

        #endregion store_category

        #region view_store_category

        ViewStoreCategoryCollection ViewStoreCategoryCollectionGetByStore(Guid storeGuid);
        ViewStoreCategoryCollection ViewStoreCategoryCollectionGetByStoreList(List<Guid> list);

        #endregion view_store_category

        #region ProposalCategoryDeal
		
        ProposalCategoryDealCollection ProposalCategoryDealsGetList(int pid);
        bool ProposalCategoryDealSetList(ProposalCategoryDealCollection categories);
        void DeleteProposalCategoryDealsByCategoryType(int pid, CategoryType type);

        void DeleteProposalCategoryDealsByCategoryType(int pid, int cid);

        bool ProposalCategoryDealsDelete(int pid);

        #endregion

        #region ProposalSellerFiles
        ProposalSellerFile ProposalSellerFileGet(int id);
        ProposalSellerFileCollection ProposalSellerFileListGet(int pid);
        bool ProposalSellerFileSet(ProposalSellerFile files);
        #endregion

        #region seller_tree

        SellerTreeCollection SellerTreeGetListByParentSellerGuid(Guid sellerGuid);
        IEnumerable<Guid> SellerTreeGetSellerGuidListByParentSellerGuid(Guid sellerGuid);
        SellerTreeCollection SellerTreeGetListBySellerGuid(Guid sellerGuid);
        SellerTreeCollection SellerTreeGetListByRootSellerGuid(Guid sellerGuid);
        Guid? SellerTreeGetRootSellerGuidBySellerGuid(Guid sellerGuid);
        IEnumerable<Guid> SellerTreeGetSellerGuidListByRootSellerGuid(Guid sellerGuid);
        SellerTreeCollection SellerTreeGetListBySellerGuid(IEnumerable<Guid> sellerGuids);
        SellerTreeCollection SellerTreeGetListByParentSellerGuid(IEnumerable<Guid> sellerGuids);
        bool SellerTreeSet(SellerTree sellerTree);
        void SellerTreeDelete(SellerTree sellerTree);
        IEnumerable<SellerTreeModel> SellerTreeGetListByRootGuid(Guid rootGuid);
        IEnumerable<Guid> SellerTreeMiddleGetSellerGuid(List<Guid> paretnSellerGuid, Guid? sellerGuid);

        #endregion seller_tree

        #region seller_tree_log

        bool SellerTreeLogSet(SellerTreeLog sellerTreeLog);

        #endregion seller_tree_log

        #region proposal_performance_log
        void ProposalPerformanceLogSet(ProposalPerformanceLog log);
        #endregion

        #region proposal_file_oauth
        ProposalFileOauth ProposalFileOauthGetByTicketId(string TicketId);
        void ProposalFileOauthSet(ProposalFileOauth oauth);
        #endregion

        #region shopping_cart_freights
        ShoppingCartFreight ShoppingCartFreightsGet(int id);
        ShoppingCartFreightCollection ShoppingCartFreightsGet(List<int> ids);
        ShoppingCartFreightCollection ShoppingCartFreightsGetBySellerGuid(List<Guid> sids);
        ShoppingCartFreightCollection ShoppingCartFreightsGetByUniqueId(List<string> fids);
        void ShoppingCartFreightsSet(ShoppingCartFreight freight);
        void ShoppingCartFreightsDelete(ShoppingCartFreight freight);
        ShoppingCartFreightCollection ShoppingCartFreightsGetByStatus(ShoppingCartFreightStatus status);

        #endregion shopping_cart_freights

        #region shopping_cart_freights_item
        ShoppingCartFreightsItem ShoppingCartFreightsItemGet(int id);
        ShoppingCartFreightsItemCollection ShoppingCartFreightsItemGetByFid(int fid);
        ShoppingCartFreightsItem ShoppingCartFreightsItemGetByBid(Guid bid);
        ShoppingCartFreightsItemCollection ShoppingCartFreightsItemGetByBids(List<Guid> bids);
        void ShoppingCartFreightsItemSet(ShoppingCartFreightsItem item);
        bool ShoppingCartFreightsItemSetBulkInsert(List<ShoppingCartFreightsItem> items);
        bool ShoppingCartFreightsItemDelete(List<Guid> bids);
        //bool ShoppingCartFreightsItemDelete(List<int> ids);
        #endregion shopping_cart_freights_item

        #region shopping_cart_freights_log
        ShoppingCartFreightsLogCollection ShoppingCartFreightsLogGetByIds(List<int> ids);
        void ShoppingCartFreightsLogSet(ShoppingCartFreightsLog log);
        #endregion

        #region WmsContractLog
        WmsContractLogCollection GetWmsContractLogByAccountid(string accountId);

        void WmsContractLogSet(WmsContractLog log); 
        #endregion

        DataTable SellerTreeGetDataTableByRoot(Guid rootGuid);

        bool SellerSellerInfoIsExistsGet(Guid sellerGuid);

        ViewDealSellerRelationshipPreCollection GetAllViewDealSellerRelationshipPreCollectionBeforeBaseDate(DateTime baseDate);
        ViewDealSellerRelationshipPreCollection GetAllViewDealSellerRelationshipPreCollectionByOneDays(DateTime baseDate);
        ViewDealSellerRelationshipPreCollection GetAllViewDealSellerRelationshipPreCollectionBySeller(Guid sellerGuid);
        bool TruncateAndBulkInsertBulkInsertDealSellerRelationshipCol(DealSellerRelationshipCollection col);
        bool BulkInsertBulkInsertDealSellerRelationshipCol(DealSellerRelationshipCollection col);

        #region vbs_confirm_notice_log
        VbsConfirmNoticeLogCollection VbsConfirmNoticeLogGet(string accountId);
        void VbsConfirmNoticeLogSet(VbsConfirmNoticeLog info);
        #endregion vbs_confirm_notice_log
    }
}
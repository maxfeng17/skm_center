﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Core
{
    public interface IChannelProvider : IProvider
    {
        #region 代銷
        
        ChannelClientProperty ChannelClientPropertyGet(string appId);

        void ChannelClientPropertySet(ChannelClientProperty clientProperty);

        ViewChannelCheckBillOrderCollection ViewChannelCheckBillOrderGet(int type, string selectedMonth);

        ViewChannelCheckBillCancelVerificationCollection ViewChannelCheckBillCancelVerificationGet(int type, string selectedMonth);

        ViewChannelCheckBillVerificationCollection ViewChannelCheckBillVerificationGet(int type, string selectedMonth);

        ViewChannelCheckBillReturnCollection ViewChannelCheckBillReturnGet(int type, string selectedMonth);

        #endregion

        #region Odm

        ChannelOdmCustomer ChannelOdmCustomerGet(Guid sellerRootGuid);
        ChannelOdmCustomerCollection ChannelOdmCustomerGetAll();
        ChannelOdmSellerRoleCollection ChannelOdmSellerRoleGetByUserId(int userId);
        List<Guid> ChannelOdmCustomerGetAllSellerRootGuid();
        bool ChannelOdmSellerRoleCollectionSet(ChannelOdmSellerRoleCollection channelOdmSellerRoleCollection);
        void ChannelOdmSellerRoleDeleteByUserid(int userid);
        ViewMasterpassBankLogCollection MasterpassBankLogCollectionGet(int? bankId);
        #endregion

        #region ChannelCommissionRule

        List<ChannelCommissionRule> GetChannelCommissionRules(ChannelSource source);
        ChannelCommissionRule GetChannelCommissionRule(ChannelSource source, int dealType, string keyWord);
        ChannelCommissionRule GetChannelCommissionRule(int id);
        bool DeleteChannelCommissionRule(int id);
        bool InsertOrUpdateChannelCommissionRule(ChannelSource source, int dealType, string keyWord,
            decimal grossMarginLimit, string returnCode, int ruleId = 0);

        bool InsertOrUpdateChannelCommissionRule(List<ChannelCommissionRule> rules);

        #endregion

        #region ChannelCommissionList
        ChannelCommissionListCollection ChannelCommissionListGet(int source);
        ChannelCommissionList ChannelCommissionListGetBySid(int channel, Guid sid);
        ChannelCommissionList ChannelCommissionListGetByMainBid(int channel, Guid mainBid);
        ChannelCommissionList ChannelCommissionListGetByBid(int channel, Guid bid);
        void ChannelCommissionListSet(ChannelCommissionList list);
        bool ChannelCommissionListDelete(int id);

        ChannelCommissionListCollection ChannelCommissionListGetBySeller(int channel, int type);
        ChannelCommissionListCollection ChannelCommissionListGetByMainDeal(int channel, int type);
        ChannelCommissionListCollection ChannelCommissionListGetBySubDeal(int channel, int type);
        ChannelCommissionListCollection ChannelCommissionListGetByDeal(int channel, int type);
        #endregion

        #region ChannelSpecialCommission
        ChannelSpecialCommissionCollection ChannelSpecialCommissionGetByChannel(int channel);
        ChannelSpecialCommission ChannelCommissionListGetByChannelBid(int channel, Guid bid);
        void ChannelSpecialCommissionSet(ChannelSpecialCommission special);
        bool ChannelSpecialCommissionSaveAll(ChannelSpecialCommissionCollection specials); 
        #endregion

        #region ChannelPostList
        void ChannelPostListSet(ChannelPostListCollection list);
        bool ChannelPostListDelete(int channel_source); 
        #endregion

        #region ShoppingGuideQuery

        /// <summary>
        /// Save ShoppingGuidQuery Data
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="bids"></param>
        /// <returns>QueryToken</returns>
        Guid ShoppingGuideQueryCollectionSet(string appId, List<Guid> bids);
        List<Guid> ShoppingGuideQueryCollectionGet(Guid queryToken, string appId);
        void ShoppingGuideQueryDelete(int keepDay);

        #endregion ShoppingGuideQuery

        #region shopping_guide_channel & shopping_guide_channel_detail & shopping_guide_deal_category

        ShoppingGuideChannel ShoppingGuideChannelGet(ShoppingGuideDealType type);
        ShoppingGuideChannel ShoppingGuideChannelSet(ShoppingGuideChannel channel);
        List<Guid> ShoppingGuideChannelDetailGet(ShoppingGuideDealType type, string batchId);
        ShoppingGuideChannel ShoppingGuideChannelSaveDetail(ShoppingGuideDealType type, List<Guid> bids);
        ShoppingGuideDealCategoryCollection ShoppingGuideDealCategoryGet(List<Guid> bids);
        void ShoppingGuideCategorySave(Dictionary<Guid, string> dealCategoryCol);
        List<Guid> GetFilterLowGrossMarginDeal(int channelId, string batchId, decimal couponGrossMargin,
            decimal deliveryGrossMargin);

        #endregion shopping_guide_channel & shopping_guide_channel_detail & shopping_guide_deal_category

        #region PChome代銷 產品
        bool PChomeChannelProdSetList(PchomeChannelProdCollection prods);
        PchomeChannelProd PChomeChannelProdGetByBid(Guid bid);
        PchomeChannelProd PChomeChannelProdGetByProdId(string prodId);
        PchomeChannelProdCollection PchomeChannelProdGetByShelfVerify(int? isVerify,int? isShelf);
        void PchomeChannelProdSet(PchomeChannelProd prod);

        #endregion

        #region PChome代銷 聯絡單客訴單
        bool PChomeChannelContactSetList(PchomeChannelContactCollection contacts);
        PchomeChannelContact PchomeChannelContactGetById(string contactId);
        void PchomeChannelContactSet(PchomeChannelContact contact);
        PchomeChannelContactCollection PchomeChannelContactGetToReply();
        bool PchomeChannelComplaintSetList(PchomeChannelComplaintCollection complaints);
        PchomeChannelComplaint PchomeChannelComplaintGetById(string complaintId);
        PchomeChannelComplaintCollection PchomeChannelComplaintGetToReply();
        void PchomeChannelComplaintSet(PchomeChannelComplaint complaint);
        #endregion
    }
}

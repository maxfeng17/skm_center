﻿using LunchKingSite.Core.Models.Entities;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LunchKingSite.Core
{
    public interface ISkmProvider : IProvider
    {
        bool InsertSkmMember(SkmMember member);
        bool MemberSet(SkmMember member);
        void MemberDeleteBySkmToken(string token);
        bool MemberCancelSharedByUserId(int userId, bool isShared);
        SkmMember MemberIsSharedGetByUserid(int userId);
        SkmMember MemberGetBySkmToken(string token, bool isShared);
        SkmMember MemberGetBySkmToken(string token);
        SkmMember MemberGetByUserId(int userId);
        SkmMemberCollection MemberListGet();
        bool ShoppeSet(SkmShoppe Shoppe);
        bool IsExistSkmShoppe(string shopCode, string brandCounterCode);
        SkmShoppe SkmShoppeGet(Guid storeGuid,bool isContainsNotAvailableShoppe = false);
        SkmShoppe SkmShoppeGet(string shopCode, string brandCounterCode);
        SkmShoppe ShoppeGetSellerByShopeCode(string shopCode);
        SkmShoppeCollection SkmShoppeGetAllShoppe(Guid sellerGuid, string shopCode);
        SkmShoppeCollection SkmShoppeGetAllShoppe(Guid sellerGuid);
        
        List<string> SkmShoppeGetAllShopCodeList();
        bool ShoppeCollectionSet(SkmShoppeCollection shoppes);
        List<string> SkmShoppeGetShopCodeBySellerGuid(Guid sellerGuid);
        SkmShoppeCollection SkmShoppeGetAll();
        SkmShoppeCollection SkmShoppeGetByShopeCdoe(string shopCode);
        SkmShoppe SkmShoppeGetFirstByShopeCdoe(string shopCode);
        SkmShoppeCollection SkmShoppeGetAllShop();
        bool SkmShoppeChangeLogAdd(SkmShoppeChangeLog log);

        #region skm_verify_log

        bool VerifyLogSet(SkmVerifyLog model);

        #endregion

        #region skm_verify_reply

        SkmVerifyReply SkmVerifyReplySet(SkmVerifyReply reply);
        SkmVerifyReply SkmVerifyReplyGet(Guid trustId);
        SkmVerifyReplyCollection SkmVerifyReplyGetRetry(DateTime start, DateTime end);
        SkmVerifyReplyHistory SkmVerifyReplyHistorySet(SkmVerifyReplyHistory history);

        #endregion skm_verify_reply
        
        #region Skm_Exchage
        SkmExchangeCollection SkmExchangeGet();
        bool SkmExchangeSet(SkmExchange skmExchange);
        #endregion Skm_Exchage

        #region Skm_App_Style
        SkmAppStyle SkmAppStyleGet(int id);
        SkmAppStyle SkmAppStyleGet(Guid gid);
        SkmAppStyleCollection SkmAppStyleGetBySeller(Guid SellerGuid);
        SkmLatestActivityCollection SkmLatestActivityGetBySid(Guid sid);
        SkmLatestActivity SkmLatestActivityGet(Guid guid);
        SkmLatestSellerDealCollection SkmLatestSellerDealGetBySid(Guid sid);
        void SkmLatestActivitySet(SkmLatestActivity act);
        void SkmLatestSellerDealSet(SkmLatestSellerDeal deal);
        bool SkmAppStyleSet(SkmAppStyle skmAppStyle);
        #endregion

        #region Skm_App_Style_Setup
        SkmAppStyleSetup SkmAppStyleSetupGet(Guid gid);
        SkmAppStyleSetupCollection SkmAppStyleSetupGetBySid(Guid sid);
        bool SkmAppStyleSetupSet(SkmAppStyleSetup skmAppStyleSetup);
        #endregion

        #region Skm_Deal_Time_Slot
        SkmDealTimeSlot SkmDealTimeSlotGet(int id);
        SkmDealTimeSlotCollection SkmDealTimeSlotGetByBid(Guid bid);
        SkmDealTimeSlotCollection SkmDealTimeSlotGetByDate(DateTime effective_sdate);
        /// <summary>
        /// 依時間取得檔次列表
        /// </summary>
        /// <param name="effectiveSdate">要取得的時間</param>
        /// <returns>檔次列表</returns>
        SkmDealTimeSlotCollection SkmDealTimeSlotGetAllByDate(DateTime effectiveSdate);
        SkmDealTimeSlot SkmDealTimeSlotGetByBidAndDate(Guid bid, DateTime effective_sdate, string CategoryType, string DealType);
        SkmDealTimeSlot SkmDealTimeSlotGetByBidAndDate(Guid bid, DateTime effective_sdate);
        SkmDealTimeSlotCollection SkmDealTimeSlotGetList(string orderBy, params string[] filter);
        SkmDealTimeSlotCollection SkmDealTimeSlotGetList(DateTime startDate, DateTime endDate, int cityId, List<Guid> sellerGuid);
        bool SkmDealTimeSlotSet(SkmDealTimeSlot skmDealTimeSlot);
        bool SkmDealTimeSlotSaveAll(SkmDealTimeSlotCollection skmDealTimeSlots);
        bool UpdateSkmDealTimeSlotSetStatus(int status, List<Guid?> bids);
        void SkmDealTimeSlotDelete(int id);
        void SkmDealTimeSlotDeleteAll(List<int> ids);
        #endregion

        #region Skm_Beacon_Message
        SkmBeaconMessage SkmBeaconMessageGet(int id);
        SkmBeaconMessage SkmBeaconMessageGetByBid(Guid bid, string shop_code, string floor);
        SkmBeaconMessage SkmBeaconMessageGetByBid(Guid bid, string shop_code);
        SkmBeaconMessageCollection SkmBeaconMessageGetListByTimeByBeaconID(bool skmBeaconChecked, string date, string time, string beaconId);
        SkmBeaconMessageCollection SkmBeaconMessageGetList(string orderBy, params string[] filter);
        bool SkmBeaconMessageSet(SkmBeaconMessage skmBeaconMessage);
        void SkmBeaconMessageDelete(int id);
        #endregion

        #region Skm_Beacon_Message_Info
        SkmBeaconMessageInfoCollection SkmBeaconMessageInfoAll();
        SkmBeaconMessageInfoCollection SkmBeaconMessageInfoGet(Guid guid);
        bool SkmBeaconMessageInfoSet(SkmBeaconMessageInfo skmBeaconMessageInfo);
        void SkmBeaconMessageInfoDelete(Guid guid); 
        #endregion

        #region Skm_Beacon_Message_time_slot
        SkmBeaconMessageTimeSlot SkmBeaconMessageTimeSlotGet(int id);
        SkmBeaconMessageTimeSlotCollection SkmBeaconMessageTimeSlotGetByBid(Guid bid, int beaconType);
        SkmBeaconMessageTimeSlotCollection SkmBeaconMessageTimeSlotGetList(string orderBy, params string[] filter);
        SkmBeaconMessageTimeSlotCollection SkmBeaconMessageTimeSlotGetListByDate(string orderBy, string startDate, string endDate, params string[] filter);
        SkmBeaconMessageTimeSlotCollection SkmBeaconMessageTimeSlotGetListByDateV1(string orderBy, string startDate, string endDate, params string[] filter);
        bool UpdateSkmBeaconMessageTimeSlotStatus(int status, List<Guid?> bids, int beaconType);
        bool SkmBeaconMessageTimeSlotSet(SkmBeaconMessageTimeSlot skmBeaconMessageTimeSlot);
        void SkmBeaconMessageTimeSlotDelete(int id);
        #endregion

        #region Skm_Beacon_Message_Device_Info_Link
        void SkmBeaconMessageDeviceInfoLinkDeleteBySkmBeaconMessageId(int skmBeaconMessageId);

        void SkmBeaconMessageDeviceInfoLinkSet(SkmBeaconMessageDeviceInfoLink skmBeaconMessageDeviceInfoLink);

        #endregion Skm_Beacon_Message_Device_Info_Link
        
        #region skmBeaconGroup
        SkmBeaconGroupCollection SkmBeaconGroupGetByGuid(Guid gid);
        int SkmBeaconGroupGetCountByGuid(Guid gid);
        SkmBeaconGroupCollection SkmBeaconGroupGetByGuid(int pageStart, int pageLength, string orderBy, Guid gid);
        void SkmBeaconGroupDelete(Guid gid);
        bool SkmBeaconGroupSet(SkmBeaconGroupCollection groups);
        #endregion

        #region skmlog
        bool SkmLogSet(SkmLog skmLog);
        #endregion

        #region ViewSkmBeaconMessagePushRecord

        ViewSkmBeaconMessagePushRecordCollection GetBeaconMessagePushRecordByUserId(int userId);

        #endregion

        #region skm_burning_event

        bool SkmBurningEventSet(SkmBurningEvent e);
        SkmBurningEvent SkmBurningEventGet(Guid externalGuid, string storeCode, bool checkStatus = true);
        List<SkmBurningEvent> SkmBurningEventListGet(List<Guid> externalGuidList, bool checkStatus);
        List<SkmBurningEvent> SkmBurningEventGet(Guid externalGuid, bool checkStatus = true);
        int SkmBurningEventGetMonthCountByDate(DateTime eventDate);
        int SkmBurningEventGetYearCountByDate(DateTime eventDate);

        #endregion

        #region skm_burning_cost_center

        bool SkmBurningCostCenterSet(SkmBurningCostCenterCollection centerCol);
        SkmBurningCostCenterCollection SkmBurningCostCenterGet(int burningEventId);
        SkmBurningCostCenterCollection SkmBurningCostCenterGet(List<int> burningEventIds);
        void SkmBurningCostCenterDel(int eventId);

        #endregion

        #region skm_burning_order_log

        bool SkmBurningOrderLogSet(SkmBurningOrderLog log);
        int SkmBurningOrderCountByEventId(int eventId);
        SkmBurningOrderLog SkmBurningOrderLogGet(int userId, Guid orderGuid, int couponId);

        #endregion

        #region skm_system_log

        void SkmSystemLogSet(SkmSystemLogType type, int logId, string content);

        #endregion

        #region skmCostCenterInfo

        SkmCostCenterInfoCollection SkmCostCenterInfoGetAll();
        SkmCostCenterInfoCollection SkmCostCenterInfoGetByStoreCode(string storeCode);

        #endregion

        #region skm_member_ph
        SkmMemberPh SkmMemberPhGetBySkmToken(string skmToken);
        bool SkmMemberPhCollectionBulkInsert(SkmMemberPhCollection smpCol);
        bool SkmMemberPhCollectionSet(SkmMemberPhCollection smpCol);
        #endregion
    }
}

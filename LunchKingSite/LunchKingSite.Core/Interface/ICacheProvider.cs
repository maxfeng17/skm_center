﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.Core
{
    public interface ICacheProvider : IProvider
    {
        T HGet<T>(string key, string fieldName, bool compress = false) where T : class;
        Dictionary<string, T> HGetAll<T>(string key, bool compress) where T : class;
        void HSetAll<T>(string key, Dictionary<string, T> items, bool compress = false) where T : class;
        bool HSet(string key, string fieldName, object value, bool compress = false);

        T Get<T>(string key, bool compress = false);
        bool Set(string key, object value, TimeSpan? timeout = null, bool compress = false);
        bool Remove(string key);
        CacheServerStatus GetStatus();
        string GetServer();
        long Increment(string key, int incrNumber = 1);
    }

    public class CacheServerStatus
    {
        private static CacheServerStatus _offline =
            new CacheServerStatus
            {
                UsedBytes = 0,
                TotalBytes = 0,
                Online = false
            };

        public static CacheServerStatus Offline
        {
            get
            {
                return _offline;
            }
        }

        public bool Online { get; set; }
        public long UsedBytes { get; set; }
        public long TotalBytes { get; set; }
        public TimeSpan Uptime { get; set; }
        public DateTime ServerTime { get;set; }

        public override string ToString()
        {
            if (TotalBytes == 0)
            {
                return "Offline";
            }
            return string.Format("{0} / {1}, 使用 {2}%",
                BytesAsString(this.UsedBytes),
                BytesAsString(this.TotalBytes),
                (this.UsedBytes * 10000 / this.TotalBytes) / 100.0f);
        }

        public string BytesAsString(long bytes)
        {
            string[] suffix = { "", "KB", "MB", "GB", "TB" };
            int i;
            double doubleBytes = 0;

            for (i = 0; (int)(bytes / 1024) > 0; i++, bytes /= 1024)
            {
                doubleBytes = bytes / 1024.0;
            }

            return string.Format("{0:0.00} {1}", doubleBytes, suffix[i]);
        }
    }
}

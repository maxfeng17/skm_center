﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Interface
{
    public interface IVbsProvider : IProvider
    {
        #region VbsMembershipPermission

        bool VbsMembershipPermissionSet(VbsMembershipPermissionCollection vmpc);
        VbsMembershipPermissionCollection VbsMembershipPermissionGetList(int vbsMembershipId);
        VbsMembershipPermissionCollection VbsMembershipPermissionGetList(string vbsMembershipAccountId);
        VbsMembershipPermission VbsMembershipPermissionGet(int vbsMembershipId, VbsMembershipPermissionScope scope);
        /// <summary>
        /// 刪除VbsMembershipPermission by vbs_membership id
        /// </summary>
        /// <param name="vbsMembershipId"></param>
        void VbsMembershipPermissionDelAll(int vbsMembershipId);
        /// <summary>
        /// 刪除VbsMembershipPermission by accountid
        /// </summary>
        /// <param name="vbsMembershipAccountId"></param>
        void VbsMembershipPermissionDelAll(string vbsMembershipAccountId);

        #endregion

        #region vbs_document_category
        VbsDocumentCategory VbsDocumentCategoryGet(int id);
        VbsDocumentCategoryCollection VbsDocumentCategoryGetList();
        void VbsDocumentCategorySet(VbsDocumentCategory category);
        #endregion vbs_document_category

        #region vbs_document_file
        VbsDocumentFile VbsDocumentFileGet(int id);
        VbsDocumentFileCollection VbsDocumentFileGetList();
        void VbsDocumentFileSet(VbsDocumentFile file);
        #endregion vbs_document_file

        #region vbs_contact_info
        VbsContactInfo VbsContactInfoGet(int id);
        void VbsContactInfoSet(VbsContactInfo info);
        #endregion vbs_contact_info
    }
}

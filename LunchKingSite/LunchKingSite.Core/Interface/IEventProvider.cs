﻿using System;
using System.Collections.Generic;
using System.Data;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.Core
{
    public interface IEventProvider : IProvider
    {
        #region AppDownloadSMSLog

        AppDownloadSmsLogCollection AppDownloadSmsLogGetList(string phoneNo, DateTime startDateTime, DateTime endDateTime);
        AppDownloadSmsLogCollection AppDownloadSmsLogGetList(string phoneNo);
        bool AppDownloadSmsLogSet(AppDownloadSmsLog log);

        #endregion

        #region EventContent
        bool EventContentSet(EventContent content);
        EventContent EventContentGet(Guid eid);
        EventContent EventContentGet(string url);
        EventContentCollection EventContentGetList(int page, int size);
        #endregion

        #region EventEmail
        bool EventEmailSet(EventEmailList el);
        DataTable EmailGetListFromEventEmailList(string eid);
        #endregion

        #region ViewPponDealTimeSlot
        ViewPponDealTimeSlotCollection GetDealFromCity(int ct);
        #endregion

        #region 活動頁設定活動主題 (EventActivity)
        void EventActivitySet(EventActivity ea);
        EventActivityCollection EventActivityGetList(DateTime dateStart, DateTime dateEnd);
        EventActivity EventActivityGet(int id);
        /// <summary>
        /// 由網頁名稱取得該活動設定
        /// </summary>
        /// <param name="pageName"></param>
        /// <returns></returns>
        EventActivity EventActivityGetByPageName(string pageName);
        EventActivityCollection EventActivityGetByPageNameList(string pageName);
        EventActivityCollection EventActivityEdmGetList(int exclude_id);
        #endregion

        #region PromoItem
        void PromoItemSet(PromoItem item);
        PromoItem PromoItemGet(int id);
        PromoItem PromoItemGetByUserId(string userId);
        PromoItem PromoItemGetByCode(string code);
        PromoItem PromoItemGetByCode(int eventId, string code);
        PromoItemCollection PromoItemGetIsUsedListByEventyId(int eventId);
        PromoItemCollection PromoItemGetListPaging(int pageStart, int pageLength, params string[] filter);
        int PromoItemGetListCount(params string[] filter);
        #endregion

        #region EventPremiumPromo

        void EventPremiumPromoSet(EventPremiumPromo eventPremiumPromo);
        EventPremiumPromo EventPremiumPromoGet(int eventPremiumPromoId);
        EventPremiumPromo EventPremiumPromoGet(string url, EventPremiumPromoType type);
        EventPremiumPromoCollection EventPremiumPromoGetListPaging(int pageStart, int pageLength, params string[] filter);

        #endregion EventPremiumsPromo

        #region EventPremiumPromoItem

        void EventPremiumPromoItemSet(EventPremiumPromoItem item);
        int EventPremiumPromoItemSet(int eventpremiumid, int premiumamount, string awardname, string awardmail, string awardaddress, string awardmobile, int userid);
        EventPremiumPromoItem EventPremiumPromoItemGet(int id);
        EventPremiumPromoItem EventPremiumPromoItemGetByUserId(string userId);
        EventPremiumPromoItemCollection EventPremiumPromoItemGetListByEventyId(int eventPremiumPromoId);
        EventPremiumPromoItemCollection EventPremiumPromoItemGetListPaging(int pageStart, int pageLength, params string[] filter);
        int EventPremiumPromoItemGetListCount(params string[] filter);

        #endregion EvnetPremiumPromoItem

        #region 優惠券

        #region VourcherEvent
        /// <summary>
        /// 儲存優惠券內容
        /// </summary>
        /// <param name="vourcher_event">優惠券資料</param>
        void VourcherEventSet(VourcherEvent vourcher_event);
        /// <summary>
        ///依優惠券Id撈取優惠券內容 
        /// </summary>
        /// <param name="id">優惠券id</param>
        /// <returns></returns>
        VourcherEvent VourcherEventGetById(int id);
        VourcherEventCollection VourcherEventGetBySellerGuid(Guid seller_guid);
        /// <summary>
        /// 回傳使用者收藏的優惠券資料
        /// </summary>
        /// <param name="userId">會員編號</param>
        /// <param name="collectType">VourcherCollectType的狀態，表示為收藏或其他可能的紀錄形式</param>
        /// <param name="collectStatus">收藏狀態，未傳入表示全部查詢</param>
        /// <returns></returns>
        VourcherEventCollection VourcherEventGetByUserCollect(int userId, VourcherCollectType? collectType, VourcherCollectStatus? collectStatus);
        /// <summary>
        /// 更新優惠券審核狀態(退件)
        /// </summary>
        /// <param name="event_id">優惠券編號</param>
        /// <param name="status">狀態</param>
        void VourcherEventReturnApply(int event_id, string message);
        /// <summary>
        /// 異動優惠券瀏覽數，設定為原連值+異動數
        /// </summary>
        /// <param name="event_id">優惠券編號</param>
        /// <param name="addPageCount">異動數</param>
        void VourcherEventAddPageCount(int event_id, int addPageCount);
        /// <summary>
        /// 修正瀏覽數為輸入的值
        /// </summary>
        /// <param name="event_id"></param>
        /// <param name="newPageCount">要異動的結果</param>
        void VourcherEventUpdatePageCount(int event_id, int newPageCount);
        /// <summary>
        /// 計算總關注數
        /// </summary>
        /// <returns></returns>
        int VourcherEventGetPageCountSum();
        /// <summary>
        /// 優惠券行銷增加點擊數
        /// </summary>
        /// <param name="promo_id"></param>
        /// <param name="addPageCount"></param>
        void VoucherPromoAddPageCount(int promo_id, int addPageCount);
        #endregion VourcherEvent

        #region VourcherStore
        /// <summary>
        /// 依優惠券Id回傳對應分店
        /// </summary>
        /// <param name="event_id">優惠券id</param>
        /// <returns></returns>
        VourcherStoreCollection VourcherStoreCollectionGetByEventId(int event_id);
        /// <summary>
        /// 依優惠券Id刪除優惠券對應分店資料
        /// </summary>
        /// <param name="event_id">優惠券Id</param>
        void VourcherStoreDeleteByEventId(int event_id);
        void VourcherStoreDeleteByStoreGuid(Guid store_guid);
        /// <summary>
        /// 儲存優惠券對應分店
        /// </summary>
        /// <param name="vourcherstores">對應分店資料</param>
        void VourcherStoresSet(VourcherStoreCollection vourcherstores);
        VourcherStoreCollection VourcherEventGetByStoreGuid(Guid store_guid);
        #endregion VourcherStore

        #region ViewVourcherSeller
        /// <summary>
        /// Toyota搜尋今日優惠券資訊
        /// </summary>
        /// <param name="todayDateTime"></param>
        /// <returns></returns>
        ViewVourcherSellerCollection TodayVourcherEventSellerCollectionGet(DateTime todayDateTime);

        /// <summary>
        /// 搜尋優惠券(分頁)
        /// </summary>
        /// <param name="search_keys">搜尋條件</param>
        /// <param name="theType"></param>
        /// <returns></returns>
        ViewVourcherSellerCollection VourcherEventSellerCollectionGetBySearch(int pageStart, int pageLength, KeyValuePair<string, string> search_keys, string orderBy, int status = -1);
        /// <summary>
        /// 搜尋優惠券webservice(分頁)
        /// </summary>
        /// <param name="seller_id">賣家guid</param>
        /// <param name="theType"></param>
        /// <returns></returns>
        ViewVourcherSellerCollection VourcherEventSellerCollectionGetSellerId(int pageStart, int pageLength, string seller_id, string orderBy);
        /// <summary>
        /// 計算搜尋的總筆數
        /// </summary>
        /// <param name="search_keys">搜尋條件</param>
        /// <param name="theType"></param>
        /// <returns></returns>
        int VourcherEventSellerCollectionGetCount(KeyValuePair<string, string> search_keys, int status = -1);
        int VourcherEventSellerCollectionGetCount(string username, int status);
        /// <summary>
        /// 依申請人撈取符合狀態的優惠券
        /// </summary>
        /// <param name="username">申請人</param>
        /// <param name="status">狀態</param>
        /// <returns></returns>
        ViewVourcherSellerCollection VourcherEventSellerCollectionGetByStatus(string username, int status);
        ViewVourcherSellerCollection VourcherEventSellerCollectionGetByStatus(int pageStart, int pageLength, string orderBy, string username, int status);
        /// <summary>
        /// 依賣家guid搜尋優惠券
        /// </summary>
        /// <param name="seller_guid">賣家guid</param>
        /// <returns></returns>
        ViewVourcherSellerCollection ViewVourcherSellerCollectionGetBySellerGuid(Guid seller_guid);
        /// <summary>
        /// 依優惠券id搜尋優惠券
        /// </summary>
        /// <param name="id">優惠券id</param>
        /// <returns></returns>
        ViewVourcherSeller ViewVourcherSellerGetById(int id);
        /// <summary>
        /// 依據需要的商家類別取得目前有優惠券的商家數量,若傳入條件category為null表示查詢所有類別的商家
        /// </summary>
        /// <param name="category">要查詢的商家類別</param> 
        /// <returns></returns>
        int ViewVourcherSellerGetSellerCountBySampleCategory(SellerSampleCategory? category);
        /// <summary>
        /// 依據需要的某城市、商家類別取得目前有優惠券的商家數量,若傳入條件category為null表示查詢所有類別的商家
        /// </summary>
        /// <param name="category">要查詢的商家類別</param>
        /// <param name="cityId">城市條件</param>
        /// <returns></returns>
        int ViewVourcherSellerGetSellerCountBySampleCategoryAndCityId(SellerSampleCategory? category, int cityId);
        #endregion ViewVourcherSeller

        #region ViewVourcherStoreFacade
        /// <summary>
        /// 依 城市id/經緯度 搜尋分店資訊webservice(分頁)
        /// </summary>
        /// <param name="event_id">優惠券id</param>
        /// <param name="theType"></param>
        /// <returns></returns>
        ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeCollectionGet(int pageStart, int pageLength, int regionId, int distance, SqlGeography geo, string orderBy);

        /// <summary>
        /// 依 城市id 優惠券類型、地理區域 為條件查詢分店資訊，並依據排序欄位進行排序。
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="regionId">城市ID</param>
        /// <param name="category">優惠券類型，未傳入表示不區分</param>
        /// <param name="geography">地理資訊的條件，未傳入表示不做地理區間的限制</param>
        /// <param name="orderBy">排序條件</param>
        /// <param name="theType">資料庫查詢附載平衡的要求，預設為止允許查詢主要的DB</param>
        /// <returns></returns>
        ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeCollectionGet(int pageStart, int pageLength, int regionId, SellerSampleCategory? category
           , GeographySearchParameter geography, string orderBy);

        /// <summary>
        /// 依 優惠券類型、地理區域 為條件查詢分店資訊，並依據排序欄位進行排序。
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="category">優惠券類型，未傳入表示不區分</param>
        /// <param name="geography">地理資訊的條件，未傳入表示不做地理區間的限制</param>
        /// <param name="orderBy">排序條件</param>
        /// <param name="theType">資料庫查詢附載平衡的要求，預設為止允許查詢主要的DB</param>
        /// <returns></returns>
        ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeGetList(int pageStart, int pageLength, SellerSampleCategory? category
           , GeographySearchParameter geography, string orderBy);

        /// <summary>
        /// 計算搜尋 城市區域id/經緯度 的總筆數
        /// </summary>
        /// <param name="regionId">城市區域id</param>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="geo">經緯度座標</param>
        /// <param name="orderBy"></param>
        /// <param name="theType"></param>
        /// <returns></returns>
        int ViewVourcherStoreFacadeCollectionGetCount(int regionId, int distance, SqlGeography geo);
        /// <summary>
        /// 查詢優惠券商家資料
        /// </summary>
        /// <param name="pageStart">第幾頁</param>
        /// <param name="pageLength">每頁幾筆</param>
        /// <param name="orderBy">排序條件</param>
        /// <param name="filter">搜尋條件</param>
        /// <returns></returns>
        ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeGetListByEventPromoId(int eventId);
        #endregion ViewVourcherStoreFacade

        #region ViewVourcherStore
        /// <summary>
        /// 依優惠券id搜尋分店資訊
        /// </summary>
        /// <param name="event_id">優惠券id</param>
        /// <param name="theType"></param>
        /// <returns></returns>
        ViewVourcherStoreCollection ViewVourcherStoreCollectionGetByEventId(int event_id);
        /// <summary>
        /// 依vourcher_store的id去查詢ViewVourcherStore
        /// </summary>
        /// <param name="id">vourcher_store的id</param>
        /// <returns></returns>
        ViewVourcherStore ViewVourcherStoreGetById(int id);
        /// <summary>
        /// 依優惠券id搜尋分店資訊webservice(分頁)
        /// </summary>
        /// <param name="event_id">優惠券id</param>
        /// <param name="theType"></param>
        /// <returns></returns>
        ViewVourcherStoreCollection ViewVourcherStoreCollectionGetByEventId(int pageStart, int pageLength, int event_id, string orderBy);
        /// <summary>
        /// 依據任何條件搜尋優惠券的分店
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="orderBy">排序</param>
        /// <param name="theType">搜尋DB 複載平衡的狀況</param>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewVourcherStoreCollection ViewVourcherStoreGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        #endregion ViewVourcherStore

        #region VourcherOrder
        /// <summary>
        /// 新增優惠券使用紀錄
        /// </summary>
        /// <param name="vourcherOrder"></param>
        void VourcherOrdertSet(VourcherOrder vourcherOrder);
        /// <summary>
        /// 查詢優惠券使用紀錄
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="orderBy"></param>
        /// <param name="filter"></param>
        VourcherOrderCollection VourcherOrderGetList(int pageStart, int pageLength, string orderBy, params string[] filter);
        #endregion VourcherOrder

        #region VourcherCollect
        /// <summary>
        /// 新增優惠券收藏紀錄
        /// </summary>
        /// <param name="vourcherCollect"></param>
        void VourcherCollectSet(VourcherCollect vourcherCollect);
        /// <summary>
        /// 取得優惠券收藏紀錄
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        VourcherCollect VourcherCollectGet(int uniqueId, int eventId);

        /// <summary>
        /// 取得優惠券收藏紀錄
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        VourcherCollectCollection VourcherCollectGetByType(int uniqueId, VourcherCollectType type);

        /// <summary>
        /// 將所有已過期的優惠券且類別為TYPE的資料，設為取消
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        int VourcherCollectSetDisabledForOverdueEvent(int uniqueId, VourcherCollectType type);

        #endregion VourcherCollect

        #region ViewVourcherSellerCollect
        /// <summary>
        /// 查取使用者優惠券收藏清單(分頁)
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="uniqueId">使用者id</param>
        /// <param name="orderBy"></param>
        /// <param name="theType"></param>
        /// <returns></returns>
        ViewVourcherSellerCollectCollection ViewVourcherSellerCollectCollectionGetByUniqueId(int pageStart, int pageLength, int uniqueId, string orderBy);
        /// <summary>
        /// 查取使用者優惠券收藏清單(分頁)
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="orderBy"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewVourcherSellerCollectCollection ViewVourcherSellerCollectGetList(int pageStart, int pageLength, string orderBy, params string[] filter);
        #endregion ViewVourcherSellerCollect

        #region ViewVourcherSellerOrder
        /// <summary>
        /// 查取使用者優惠券使用清單(分頁)
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="uniqueId">使用者id</param>
        /// <param name="orderBy"></param>
        /// <param name="theType"></param>
        /// <returns></returns>
        ViewVourcherSellerOrderCollection ViewVourcherSellerOrderCollectionGetByUniqueId(int pageStart, int pageLength, int uniqueId, string orderBy);

        #endregion ViewVourcherSellerOrder

        #region ViewVourcherPromo
        /// <summary>
        /// 搜尋優惠券行銷列表
        /// </summary>
        /// <returns></returns>
        ViewVourcherPromoCollection ViewVourcherPromoCollectionGetAll(int promo_type);
        #endregion ViewVourcherPromo

        #region VourcherPromo
        /// <summary>
        /// 刪除優惠券行銷
        /// </summary>
        /// <param name="id">id</param>
        void VourcherPromoDelete(int id);
        /// <summary>
        /// 新增或更新優惠券行銷
        /// </summary>
        /// <param name="promo">優惠券行銷</param>
        void VourcherPromoSet(VourcherPromo promo);
        /// <summary>
        /// 依id取得優惠券行銷
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        VourcherPromo VourcherPromoGet(int id);
        /// <summary>
        /// 依類別撈取VourcherPromo資料
        /// </summary>
        /// <param name="type">類別</param>
        /// <returns></returns>
        VourcherPromoCollection VourcherPromoGetList(int type);
        #endregion VourcherPromo

        #region ViewVourcherPromoShow
        /// <summary>
        /// 查詢 優惠券APP行銷BINNER顯示資料
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="orderBy"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewVourcherPromoShowCollection ViewVourcherPromoShowGetList(int pageStart, int pageLength, string orderBy, params string[] filter);
        #endregion ViewVourcherPromoShow

        #region ViewYahooVoucher

        ViewYahooVoucherSellerCollection ViewYahooVoucherSellerGetAvailableList();
        ViewYahooVoucherSellerCollection ViewYahooVoucherSellerGetById(int id);
        ViewYahooVoucherStoreCollection ViewYahooVoucherStoreGetAvailableList();
        ViewYahooVoucherStoreCollection ViewYahooVoucherStoreGetByEventId(int eventid);
        ViewYahooVoucherCategoryCollection ViewYahooVoucherCategoryGetAvailableList();
        ViewYahooVoucherPromoCollection ViewYahooVoucherPromoGetAvailableList();
        ViewYahooVoucherPromoCollection ViewYahooVoucherPromoGetAvailableList(int type,bool exclude_expired);
        #endregion
        #endregion

        #region SkmEventPromoInvoiceContent

        void SkmEventPromoInvoiceContentSet(SkmEventPromoInvoiceContent invoiceContent);
        SkmEventPromoInvoiceContentCollection SkmEventPromoInvoiceContentCollectionGet();

        #endregion

        #region SkmEventPrizeWinner

        void SkmEventPrizeWinnerSet(SkmEventPrizeWinner skmEventPrizeWinner);
        SkmEventPrizeWinnerCollection SkmEventPrizeWinnerCollectionGet();

        #endregion SkmEventPrizeWinner

        #region EventPromoVote 投票

        ViewEventPromoVote ViewEventPromoVoteGetByPromoItemId(int eventPromoItemId);

        ViewEventPromoVoteCollection ViewEventPromoVoteGet(int mainId);

        int EventPromoVoteSet(EventPromoVote vote);
        
        EventPromoVoteCollection EventPromoVoteGetList(int eventPromoId, int memberUniqueId, DateTime start, DateTime end, EventPromoItemType itemType);

        EventPromoVoteCollection EventPromoVoteGetList(int eventPromoId, int memberUniqueId, DateTime start, DateTime end);

        EventPromoVoteCollection EventPromoVoteGetList(int eventPromoId, int memberUniqueId, EventPromoItemType itemType);

        ViewEventPromoVourcherCollectCollection ViewEventPromoVourcherCollectGet(int mainId, int memberUniqueId);

        ViewEventPromoVoteRecordCollection ViewEventPromoVoteRecordGet(int eventPromoId, int memberUniqueId);

        #endregion

        #region 2016中元節活動
        DiscountCodeCollection CheckDiscountCodeByCondition(int uniqueId);
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Core.Interface
{
    public interface IQuestionProvider : IProvider
    {
        #region QuestionEvent

        QuestionEvent GetQuestionEvent(string eventToken);
        QuestionEvent GetQuestionEvent(int eventId);

        #endregion QuestionEvent

        #region QuestionItemCategory

        List<QuestionItemCategory> GetQuestionItemCategory(int eventId);

        #endregion QuestionItemCategory

        #region ViewQuestionItem

        List<ViewQuestionItem> GetViewQuestionItem(int eventId);
        List<ViewQuestionItem> GetViewQuestionItem(int eventId, int categoryId);

        #endregion ViewQuestionItem

        #region QuestionItemOption

        List<QuestionItemOption> GetQuestionItemOptions(int itemId);

        #endregion QuestionItemOption

        #region QuestionEventDiscount

        List<QuestionEventDiscount> GetQuestionEventDiscount(int eventId);

        #endregion QuestionEventDiscount

        #region QuestionAnswerDiscountLog
        
        List<QuestionAnswerDiscountLog> GetQuestionAnswerDiscountLog(int userId, int eventDiscountId);
        bool SaveQuestionAnswerDiscountLog(QuestionAnswerDiscountLog log);

        #endregion QuestionAnswerDiscountLog

        #region QuestionAnswer & QuestionAnswerOption

        /// <summary>
        /// 
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="options"></param>
        /// <returns>Success return QuestionAnswer.id, Fail return 0</returns>
        bool AnswerQuestion(QuestionAnswer answer, List<QuestionAnswerOption> options);

        bool SaveQuestionAnswer(QuestionAnswer answer);
        List<QuestionAnswer> GetQuestionAnswerByEventId(int userId, int eventId);
        List<QuestionAnswer> GetQuestionAnswer(List<int> answerIds);
        QuestionAnswer GetQuestionAnswer(int answerId);

        #endregion QuestionAnswer & QuestionAnswerOption
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Core.Interface
{
    public interface IExhibitionProvider : IProvider
    {
        #region ExhibitionEvent

        ExhibitionEvent GetExhibitionEvent(int id);
        bool SetExhibitionEvent(ExhibitionEvent evt);
        /// <summary>
        /// 依館代號取得策展資料清單
        /// </summary>
        /// <param name="guid">館代號</param>
        /// <returns>策展資料清單</returns>
        List<ExhibitionEvent> GetExhibitionEventsBySellerGuid(Guid guid);
        /// <summary>
        /// 依館代號取得可顯示的策展資料清單
        /// </summary>
        /// <param name="guid">館代號</param>
        /// <returns>可顯示的策展資料清單</returns>
        List<ExhibitionEvent> GetPublishExhibitionEventsBySellerGuid(Guid guid);
        /// <summary>
        /// 設定策展活動排序
        /// </summary>
        /// <param name="eventInfo">策展活動排序資料</param>
        /// <returns>是否成功</returns>
        bool SetExhibitionEventStatus(Dictionary<int, int> eventSortInfo);

        #endregion ExhibitionEvent

        #region ExhibitionCategory

        List<ExhibitionCategory> GetExhibitionCategoryList(int eventId);
        ExhibitionCategory GetExhibitionCategory(int id);
        bool SetExhibitionCategory(ExhibitionCategory exCate);
        bool DelExhibitionCategory(int id);
        /// <summary>
        /// 依策展編號刪除策展分類資訊
        /// </summary>
        /// <param name="eventId">策展編號</param>
        /// <returns>是否刪除成功</returns>
        bool DelExhibitionCategoryByEventID(int eventId);

        #endregion ExhibitionCategory

        #region ExhibitionDeal

        List<ExhibitionDeal> GetExhibitionDeal(Guid externalGuid);
        ExhibitionDeal GetExhibitionDeal(int categoryId, Guid externalGuid);
        /// <summary>
        /// 依策展代號取得策展檔次資訊清單
        /// </summary>
        /// <param name="eventID"></param>
        /// <returns>策展檔次資訊清單</returns>
        List<ExhibitionDeal> GetExhibitionDealListByEventId(int eventId);
        List<ExhibitionDeal> GetExhibitionDealList(int categoryId);
        bool SetExhibitionDeal(ExhibitionDeal deal);
        bool DelExhibitionDeal(int exDealId);
        /// <summary>
        /// 依策展編號刪除策展分類檔次資訊
        /// </summary>
        /// <param name="eventId">策展編號</param>
        /// <returns>是否刪除成功</returns>
        bool DelExhibitionDealByEventId(int eventId);

        int GetExhibitionDealCount(int exhibitionEventId, Guid sellerGuid);

        #endregion ExhibitionDeal

        #region ExhibitionStore

        List<ExhibitionStore> GetExhibitionStore(int eventId);
        bool SetExhibitionStore(int eventId, List<ExhibitionStore> stores);
        /// <summary>
        /// 依策展編號刪除策展適用分店
        /// </summary>
        /// <param name="eventId">策展編號</param>
        /// <returns>是否刪除成功</returns>
        bool DelExhibitionStoreByEventId(int eventId);
        #endregion ExhibitionStore

        #region ExhibitionLog

        bool SetExhibitionLog(ExhibitionLog log);

        #endregion ExhibitionLog

        #region ExhibitionCode

        ExhibitionCode GetExhibitionCode(string code);
        /// <summary>
        /// 依seller guid 清單取得策展代號清單
        /// </summary>
        /// <param name="list">seller guid 清單</param>
        /// <returns>策展代號清單</returns>
        List<ExhibitionCode> GetExhibitionCodeList(List<string> sellerGuids);
        /// <summary>
        /// 新增策展代號
        /// </summary>
        /// <param name="request">策展代號資料</param>
        /// <returns>是否新增成功</returns>
        bool InsertExhibitionCode(ExhibitionCode request);
        /// <summary>
        /// 編輯策展代號
        /// </summary>
        /// <param name="request">策展代號資料</param>
        /// <returns>是否編輯成功</returns>
        bool SetExhibitionCode(ExhibitionCode request);
        /// <summary>
        /// 刪除策展資料
        /// </summary>
        /// <param name="exhibitionCode">策展代號資料</param>
        /// <returns>是否刪除成功</returns>
        bool DeleteExhibitionCode(ExhibitionCode exhibitionCode);
        #endregion

        #region ViewExternalDealRelationStoreExhibitionCode

        /// <summary>
        /// 依策展代號等訊息取得檔次資訊清單
        /// </summary>
        /// <param name="exhibitionCodeId">策展代號</param>
        /// <param name="startDate">檔次上架時間</param>
        /// <param name="endDate">檔次下架時間</param>
        /// <param name="storeGuids">館別代號清單</param>
        /// <returns>檔次資訊清單</returns>
        List<ViewExternalDealRelationStoreExhibitionCode> GetExternalDealByExhibitionCode(long exhibitionCodeId, DateTime startDate, DateTime endDate, List<string> storeGuids);
        #endregion

        #region ExhibitionDealTimeSlot

        bool SaveExhibitionDealTimeSlot(Guid sellerGuid, int exhibitionEventId, int categoryId, Guid externalGuid,
            DateTime start, DateTime end);

        List<ExhibitionDealTimeSlot> GetExhibitionDealTimeSlot(DateTime beginDate, Guid sellerGuid, int exhibitionId,
            int categoryId);

        ExhibitionDealTimeSlot GetExhibitionDealTimeSlotById(int id);

        bool UpdateExhibitionDealTimeSlot(List<ExhibitionDealTimeSlot> data);

        /// <summary>
        /// 依策展編號刪除不存在分類的策展商品排序資料
        /// </summary>
        /// <param name="exhibitionEventId">策展編號</param>
        /// <returns>是否刪除成功</returns>
        bool DelExhibitionDealTimeSlotUnreferencedCategor(int exhibitionEventId);

        #endregion ExhibitionDealTimeSlot

        #region ExhibitionCardType
        /// <summary>
        /// 依策展編號儲存推薦卡別(先刪除再新增)
        /// </summary>
        /// <param name="id">策展編號</param>
        /// <param name="recommendCreditCardLevels"></param>
        /// <returns>是否儲存成功</returns>
        bool SetExhibitionCardType(int id, SkmCardLevelType[] recommendCreditCardLevels);
        /// <summary>
        /// 依策展編號取得推薦卡別清單
        /// </summary>
        /// <param name="eventId">策展編號</param>
        /// <returns>推薦卡別清單</returns>
        List<ExhibitionCardType> GetExhibitionCardTypesByEventId(int eventId);
        #endregion

        #region ViewExhibitionDealExternalDeal
        /// <summary>
        /// 依策展編號取得檔次基本資訊清單
        /// </summary>
        /// <param name="exhibitionEventId">策展編號</param>
        /// <returns>檔次基本資訊清單</returns>
        List<ViewExhibitionDealExternalDeal> GetExternalDealsSimpleInfoByExhibitionEventID(int exhibitionEventId);
        #endregion

        #region ViewExhibitionEvent

        List<ViewExhibitionEvent> GetViewExhibitionEvent(Guid sellerGuid, SkmCardLevelType cardLevel,
            SkmExhibitionEventStatus status);

        List<ViewExhibitionEvent> GetViewExhibitionEvent(Guid sellerGuid, List<SkmCardLevelType> cardLevel,
            SkmExhibitionEventStatus status);

        ViewExhibitionEvent GetViewExhibitionEvent(Guid sellerGuid, int exhibitionEventId);


        #endregion

        #region ExhibitionActivity

        List<ExhibitionActivity> GetExhibitionActivities(int eventId);
        /// <summary>
        /// 設定策展多重活動資訊清單
        /// </summary>
        /// <param name="exhibitionActivities">策展多重活動資訊清單</param>
        /// <returns>是否設定成功</returns>
        bool SetExhibitionActivity(IEnumerable<ExhibitionActivity> exhibitionActivities);

        int GetExhibitionActivityCount(int exhibitionEventId);

        #endregion ExhibitionActivity

        #region ExhibitionUserGroup

        bool BulkInsertExhibitionUserGroupMemberToken(List<ExhibitionUserGroupMemberToken> list, string dbName = "Default");
        void DelExhibitionUserGroupMemberToken(int eventId);
        bool DelExhibitionUserGroupMapping(int eventId);
        bool AddExhibitionUserGroupMapping(int eventId, List<string> userGroups);
        bool DelExhibitionUserGroup(int eventId);
        bool AddExhibitionUserGroup(ExhibitionUserGroup userGroup);
        bool UpdateExhibitionUserGroup(ExhibitionUserGroup userGroup);

        List<ExhibitionUserGroupMapping> GetExhibitionUserGroupMappings();
        List<int> GetHaveUserGroupExhibitionEventIds(List<int> eventIds);

        List<SelectListItem> GetExhibitionUserGroupList(int eventId, List<string> sellerGuids, DateTime eventSd, DateTime eventEd);
        List<ExhibitionUserGroup> GetExhibitionUserGroupList(string sellerGuid, out int dataCnt, int page = 1, int pageSize = 10);
        ExhibitionUserGroup GetExhibitionUserGroup(int id);

        List<ViewExhibitionEventUserGroup> GetViewExhibitionEventHaveUserGroupList(Guid sellerGuid,
            List<SkmCardLevelType> cardLevels, string memberToken);

        List<ViewExhibitionEventUserGroup> GetViewExhibitionEventNoUserGroupList(Guid sellerGuid,
            List<SkmCardLevelType> cardLevels);

        #endregion
    }
}
﻿using System;
using Newtonsoft.Json;

namespace LunchKingSite.Core
{
    public interface ISerializer
    {
        string Serialize(object input, bool indented = false);
        string Serialize(object input, bool indented, JsonIgnoreSetting ignoreSetting);
        T Deserialize<T>(string input);
        object Deserialize(string input, Type objType);
        dynamic DeserializeDynamic(string input);
    }
}

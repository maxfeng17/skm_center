﻿using System;
using System.Collections.Generic;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.Core
{
    public interface INotificationProvider : IProvider
    {
        #region IdriverToken
        int IdriverTokenSet(IdriverToken iDriverToken);
        int IdriverTokenDeleteList(IdriverTokenCollection iDriverTokens);
        IdriverToken IdriverTokenGetByTokenString(string tokenString);
        IdriverToken IdriverTokenGetByTokenString(string tokenString, string deviceId);
        IdriverTokenCollection IdriverTokenGetListe(params string[] filter);
        IdriverTokenCollection IdriverTokenCollectionGetByCityId(int cityId);
        #endregion

        #region DeviceToken
        /// <summary>
        /// 新增修改某筆裝置推播編號
        /// </summary>
        /// <param name="deviceToken"></param>
        /// <returns></returns>
        int DeviceTokenSet(DeviceToken deviceToken);
        /// <summary>
        /// 整批修改裝置資訊
        /// </summary>
        /// <param name="deviceTokenCols"></param>
        /// <returns></returns>
        int DeviceTokenSaveAll(DeviceTokenCollection deviceTokenCols);
        /// <summary>
        /// 依據裝置編號與裝置類型取得該裝置的推播資料
        /// </summary>
        /// <param name="device"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        DeviceToken DeviceTokenGet(string device, MobileOsType type);
        /// <summary>
        /// 依據裝置編號取得該裝置的推播資料
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        DeviceTokenCollection DeviceTokenCollectionGetList(string device);
        /// <summary>
        /// 依據PushToken取得所有裝置的推播資料
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        DeviceTokenCollection DeviceTokenCollectionGetListByToken(string token);
        /// <summary>
        /// 依據裝置Token取得該裝置的推播資料
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        DeviceToken DeviceTokenGet(string token);
        /// <summary>
        /// 取得推撥裝置列表
        /// </summary>
        /// <param name="filter">過濾條件</param>
        /// <returns></returns>
        DeviceTokenCollection DeviceTokenCollectionGetList(params string[] filter);
        #endregion DeviceToken

        #region PushApp
        void PushAppSet(PushApp pushApp);
        void DeletePushApp(int id);
        PushApp GetPushApp(int id);
        PushAppCollection GetPushAppListByPeriod(DateTime pushTimeStart, DateTime pushTimeEnd);
        PushAppCollection GetPushAppList(int pageNumber, int pageSize, string orderBy, params string[] filter);
        int GetPushAppListCount(params string[] filter);
        /// <summary>
        /// 增加IOS推播點擊數字
        /// </summary>
        /// <param name="id">推播編號</param>
        /// <param name="addCount">增加的數字</param>
        /// <returns></returns>
        int PushAppIOSViewCountAdd(int id, int addCount);
        /// <summary>
        /// 增加Android推播點擊數字
        /// </summary>
        /// <param name="id">推播數字</param>
        /// <param name="addCount">增加的數字</param>
        /// <returns></returns>
        int PushAppAndroidViewCountAdd(int id, int addCount);
        #endregion

        #region PcpAssignment

        PcpAssignmentCollection PcpAssignmentGetList(int userId, DateTime begin, DateTime end);
        PcpAssignmentCollection PcpAssignmentGet(DateTime executionTime, AssignmentStatus status, params AssignmentSendType[] sendType);
        PcpAssignment PcpAssignmentGet(int id);
        void PcpAssignmentSet(PcpAssignment pcp);
        void PcpAssignmentSetStatus(PcpAssignment pcp, AssignmentStatus status);
        void PcpAssignmentSetStatus(int id, AssignmentStatus status);

        void PcpAssignmentMemberSetSendTime(long id, DateTime sendTime);

        ViewPcpAssignmentMemberDeviceTokenCollection ViewPcpAssignmentMemberDeviceTokenGetList(int assignmentId);
        #endregion PcpAssignment

        #region PcpAssignmentMember
        List<PcpAssignmentMember> PcpAssignmentMemberGetList(int assignmentId);
        #endregion

        #region PcpAssignmentFilter

        PcpAssignmentFilterCollection PcpAssignmentFilterGetList(int pcpAssignmentId);
        void PcpAssignmentFilterSet(PcpAssignmentFilter filter);

        #endregion

        #region PcpAssignmentMember

        PcpAssignmentMember PcpAssignmentMemberGet(long id);
        void PcpAssignmentMemberSet(PcpAssignmentMember assignmentMember);

        #endregion

        #region DevicePushRecord  (Notification Message to DevicePushRecord)

        void NotificationToDevicePushRecordSet(int actionEventPushMessageId, params string[] filter);

        void NotificationToDevicePushRecordSet(int actionEventPushMessageId,
            DeviceTokenCollection deviceTokenCollection,
            out List<Tuple<int, string>> androidTokens,
            out List<Tuple<int, string>> iOSTokens);

        List<Tuple<int, string>> DevicePushRecordMessageTokenGetDict(int actionEventPushMessageId, MobileOsType osType);

        #endregion DevicePushRecord  (Notification Message to DevicePushRecord)

        List<UserDeviceInfoModel> GetUserDeviceInfoModels(int userId);
        UserDeviceInfoModel GetUserDeviceInfoModel(string token);
    }
}

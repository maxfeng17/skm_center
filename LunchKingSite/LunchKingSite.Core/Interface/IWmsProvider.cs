﻿using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core
{
    public interface IWmsProvider : IProvider
    {
        WmsContact WmsContactGet(string accountId);

        void WmsContactSave(WmsContact contact, string createId);

        WmsContact WmsContactGetBySellerGuid(Guid sellerGuid);
        ViewWmsPurchaseOrderCollection ViewWmsPurchaseOrderListGetByItemGuids(List<Guid> itemGuids);
        ViewWmsPurchaseOrderCollection ViewWmsPurchaseOrderListGetByPurchaseOrderGuid(List<Guid> purchaseOrderGuid);
        ViewWmsPurchaseOrderCollection ViewWmsPurchaseOrdermList();
        ViewWmsPurchaseOrderCollection ViewWmsPurchaseOrdermListGetBySellerGuidList(List<Guid> sellerGuids);
        void WmsPurchaseOrderSet(WmsPurchaseOrderCollection order);
        void WmsPurchaseOrderSet(WmsPurchaseOrder wpo);
        WmsPurchaseOrder WmsPurchaseOrderGet(Guid guid);
        WmsPurchaseOrder WmsPurchaseOrderGetWithUpdateLock(Guid guid);
        void UpdtaeWmsPurchaseOrderStatus(List<Guid> guid, int status, string userName);        
        /// <summary>
        /// 取得需要跟PChome倉儲，確認狀態的進貨資料列表
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        WmsPurchaseOrderCollection WmsPurchaseOrderGetListForWmsCheck();

        IList<ViewWmsOrder> GetSuccessfulViewWmsOrdersBySendStatus(WmsOrderSendStatus init);
        IList<ViewWmsOrder> GetSuccessfulViewWmsOrdersByOrderGuid(Guid oid);
        WmsOrderCollection GetReadySyncShipStatusOfWmsOrders();
        WmsOrderCollection GetReadySyncShipStatusOfWmsOrders(DateTime dateTime); 
        WmsOrder WmsOrderGet(Guid orderGuid);
        WmsOrderCollection WmsOrderGetFailure(DateTime sDate, DateTime eDate);

        WmsOrder WmsOrderGetByPchomeId(string pchomeOrderId);
        void WmsOrderSet(WmsOrder wmsOrder);
        OrderShip OrderShipGet(Guid orderGuid, string pickId);
        OrderShip OrderShipGetByshipNo(Guid orderGuid, string pickId, string shipNo);
        void OrderShipSet(OrderShip wmsOrderShip);
        ViewWmsProposalOrderDeliveryCollection GetViewWmsProposalOrderDeliveryByVbs(string serviceNo, string sellerGuids, bool isCompleteOrder, string[] queryParams);

        #region view_wms_return_order
        ViewWmsReturnOrderCollection ViewWmsReturnOrderListGetByReturnOrderGuid(List<Guid> returnOrderGuid);
        ViewWmsReturnOrderCollection ViewWmsReturnOrdermListGetBySellerGuidList(
            int pageStart, int pageLength, List<Guid> sellerGuids, string productNo,
            string productBrandName, string productName, string pchomeProdId, string specs, string productCode,
            string sDate, string eDate, string rtnStatus, string source, bool invalidation);
        ViewWmsReturnOrder ViewWmsReturnOrderGet(Guid guid);

        ViewWmsReturnOrderCollection ViewWmsReturnOrdermList();
        ViewWmsReturnOrderCollection ViewWmsReturnOrdermListByStatus(int returnOrderStatus,int invalidation);
        ViewWmsReturnOrder ViewWmsReturnOrderListGetByPchomeProdId(string pchomeProdId);

        #endregion
        #region wms_return_order
        void WmsReturnOrderSet(WmsReturnOrder wmsReturnOrder);
        WmsReturnOrder WmsReturnOrderGet(Guid guid);
        void WmsReturnOrderSet(WmsReturnOrderCollection returnOrders);

        WmsReturnOrderCollection WmsReturnOrderByPchomeReturnId(string pchomeReturnId);
        void UpdtaeWmsReturnOrderStatus(List<Guid> guid, int status, string userName);
        void UpdtaeWmsReturnOrderInvalidationStatus(Guid returnOrderGuid, bool invalidationStatus, string userName);

        #endregion

        #region wms_return_order_log
        void WmsReturnOrderLogSet(WmsReturnOrderLogCollection orderLogs);
        #endregion

        #region wms_reunf_order

        WmsRefundOrder WmsRefundOrderGetByReturnFormId(int returnFormId);

        #endregion

        #region WmsFee PCHOME倉儲費
        List<Guid> GetWmsProductItemForBalanceSheetSeller();
        ProductItemCollection GetViewProductItemBySellerProdId(Guid sellerGuid);
        void WmsPurchaseServFeeSet(WmsPurchaseServFeeCollection fee);
        void WmsStockFeeSet(WmsStockFeeCollection fee);
        void WmsReturnServFeeSet(WmsReturnServFeeCollection fee);
        void WmsReturnShipFeeSet(WmsReturnShipFeeCollection fee);
        void WmsReturnPackFeeSet(WmsReturnPackFeeCollection fee);
        void WmsOrderServFeeSet(WmsOrderServFeeCollection fee);
        void WmsShipFeeSet(WmsShipFeeCollection fee);
        void WmsPackFeeSet(WmsPackFeeCollection fee);
        void WmsRefundShipFeeSet(WmsRefundShipFeeCollection fee);
        int GetWmsPurchaseServFeeByAccountingDate(DateTime accountingDate);
        int GetWmsStockFeeByAccountingDate(DateTime accountingDate);
        int GetWmsReturnServFeeByAccountingDate(DateTime accountingDate);
        int GetWmsReturnShipFeeByAccountingDate(DateTime accountingDate);
        int GetWmsReturnPackFeeByAccountingDate(DateTime accountingDate);
        int GetWmsOrderServFeeByAccountingDate(DateTime accountingDate);
        int GetWmsShipFeeByAccountingDate(DateTime accountingDate);
        int GetWmsPackFeeByAccountingDate(DateTime accountingDate);
        int GetWmsRefundShipFeeByAccountingDate(DateTime accountingDate);
        WmsPurchaseServFeeCollection GetWmsPurchaseServFeeByNoBalanceSheet(List<string> pchomeProdId, DateTime endTime);
        WmsStockFeeCollection GetWmsStockFeeByNoBalanceSheet(List<string> pchomeProdId, DateTime endTime);
        WmsReturnServFeeCollection GetWmsReturnServFeeByNoBalanceSheet(List<string> pchomeProdId, DateTime endTime);
        WmsReturnShipFeeCollection GetWmsReturnShipFeeByNoBalanceSheet(List<string> pchomeProdId, DateTime endTime);
        WmsReturnPackFeeCollection GetWmsReturnPackFeeByNoBalanceSheet(List<string> pchomeProdId, DateTime endTime);
        WmsPurchaseServFeeCollection GetWmsPurchaseServFeeByBalanceSheetWmsId(int balanceSheetWmsId);

        WmsStockFeeCollection GetWmsStockFeeByBalanceSheetWmsId(int balanceSheetWmsId);

        WmsReturnServFeeCollection GetWmsReturnServFeeByBalanceSheetWmsId(int balanceSheetWmsId);

        WmsReturnShipFeeCollection GetWmsReturnShipFeeByBalanceSheetWmsId(int balanceSheetWmsId);
        WmsReturnPackFeeCollection GetWmsReturnPackFeeByBalanceSheetWmsId(int balanceSheetWmsId);
        WmsOrderServFeeCollection GetWmsOrderServFeeByBalanceSheetId(int bsId);
        WmsShipFeeCollection GetWmsShipFeeByBalanceSheetId(int bsId);
        WmsPackFeeCollection GetWmsPackFeeByBalanceSheetId(int bsId);
        WmsRefundShipFeeCollection GetWmsRefundShipFeeByBalanceSheetId(int bsId);
        WmsOrderServFeeCollection WmsOrderServFeeGetByOrder(Guid productGuid, DateTime endTime);
        WmsShipFeeCollection WmsShipFeeGetListGetByOrder(Guid productGuid, DateTime endTime);
        WmsPackFeeCollection WmsPackFeeGetByOrder(Guid productGuid, DateTime endTime);
        WmsRefundShipFeeCollection WmsRefundShipFeeGetByOrder(Guid productGuid, DateTime endTime);
        int WmsOrderFeeSumByBalanceSheet(int balanceSheetWmsId);
        DataTable WmsOrderFeeDetailByBalanceSheet(int balanceSheetId);
        #endregion

        void OrderShipSave(OrderShip wmsOrderShip);

        #region product_item_wms_inventory

        WmsProductItemInventory WmsProductItemInventoryGet(Guid itemGuid);
        void WmsProductItemInventorySet(WmsProductItemInventory inventory);

        #endregion
        void WmsPurchaseOrderLogSet(WmsPurchaseOrderLogCollection orderLogs);
        void UpdtaeWmsPurchaseOrderInvalidationStatus(Guid purchaseOrderGuid, bool invalidationStatus, string userName);
        void UpdtaeWmsPurchaseOrderStatusWithInvalidationStatus(List<Guid> purchaseOrderGuidList, int status,string userName, bool invalidationStatus);

        #region WmsRefundOrder
        void WmsRefundOrderSet(WmsRefundOrder order);
        WmsRefundOrder WmsRefundOrderGet(int returnFormId);
        WmsRefundOrderCollection WmsRefundOrderGetByUpdate();
        ReturnFormCollection WmsRefundOrderGetByArrivaled(int type);

        void WmsRefundOrderStatusLogSet(WmsRefundOrderStatusLog log);
        #endregion

        Dictionary<Guid, Guid> WmsAmtOrderGet();


    }
}

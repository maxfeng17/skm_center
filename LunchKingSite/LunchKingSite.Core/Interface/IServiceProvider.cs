﻿using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;

namespace LunchKingSite.Core
{
    public interface IServiceProvider : IProvider
    {
        #region customer_service_message
        CustomerServiceMessage CustomerServiceMessageGet(string serviceNo);
        CustomerServiceMessageCollection CustomerServiceMessageByStatusGet(int status);
        void CustomerServiceMessageSet(CustomerServiceMessage entity);
        void CustomerServiceMessageDelete(CustomerServiceMessage entity);
        int CustomerServiceMessageGetCountForFilter(params string[] filter);
        CustomerServiceMessageCollection CustomerServiceMessageGetListForFilter(int pageStart, int pageLength, string orderBy, params string[] filter);
        CustomerServiceMessageCollection GetCustomerMessageByUserId(int userId);
        CustomerServiceMessage GetCustomerMessageByOrderGuidNotCompelete(Guid orderGuid);
        CustomerServiceMessage GetLastCustomerMessageByOrderGuid(Guid orderGuid);

        /// <summary>
        /// 合併新舊客服訊息 Model
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <returns></returns>
        ViewMergeOldNewServiceMessageCollection ViewMergeOldNewServiceMessageCollectionGet(int userId, int pageStart, int pageLength);
        int ViewMergeOldNewServiceMessageGetCount(int userId);
        #endregion

        #region customer_service_category
        CustomerServiceCategory CustomerServiceCategoryGet(int category_id);
        void CustomerServiceCategorySet(CustomerServiceCategory entity);
        void CustomerServiceCategoryDelete(CustomerServiceCategory entity);
        void CustomerServiceCategoryDeleteByCategoryId(int categoryId);
        int GetCategoryListCount(params string[] filter);
        CustomerServiceCategoryCollection GetCategoryListByPage(int page, int pageSize, string orderBy, params string[] filter);

        CustomerServiceCategoryCollection GetCustomerServiceCategory();
        CustomerServiceCategoryCollection GetCustomerServiceCategoryForFrontEnd();
        CustomerServiceCategoryCollection GetCustomerServiceCategoryListByCategoryId(int? categoryId);
        CustomerServiceCategoryCollection GetCategoryListByParentId(int parentId);
        CustomerServiceCategory GetCategoryByParentIdAndName(int parentId, string categoryname);
        string GetCustomerNameByCategoryId(int categoryId);
        string GetCustomerNameByCategoryIdMain(int categoryId);
        int GetLastOrderId();
        int GetLastCategoryId();
        #endregion

        #region customer_service_category_sample
        CustomerServiceCategorySample CustomerServiceCategorySampleGet(int id);
        void CustomerServiceCategorySampleSet(CustomerServiceCategorySample entity);
        void CustomerServiceCategorySampleDelete(CustomerServiceCategorySample entity);
        int GetSampleListCount(params string[] filter);
        void CustomerServiceCategorySampleDeleteByCategoryId(int categoryId);
        CustomerServiceCategorySampleCollection CustomerServiceCategorySampleGetByCategoryId(int categoryId);
        CustomerServiceCategorySampleCollection GetSampleListByPage(int page, int pageSize, string orderBy, params string[] filter);

        #endregion

        #region customer_service_inside_log
        CustomerServiceInsideLog CustomerServiceInsideLogGet(int id);
        CustomerServiceInsideLogCollection CustomerServiceInsideLogGet(string serviceNo);
        int CustomerServiceInsideLogSet(CustomerServiceInsideLog entity);
        int CustomerServiceInsideLogCollectionSet(CustomerServiceInsideLogCollection entities);
        void CustomerServiceInsideLogDelete(CustomerServiceInsideLog entity);
        int CustomerServiceInsideLogByServiceNoAndStatus(string serviceNo, int status, DateTime? time);
        int CustomerServiceInsideLogByStatus(int status);
        int GetCustomerServiceInsideLogCount(string sellerGuids);
        List<string> GetCustomerServiceInsideLogServiceNoListBy24HNoReply(DateTime startTime, DateTime endTime);

        #endregion

        #region customer_service_outside_log
        CustomerServiceOutsideLog CustomerServiceOutsideLogGet(int id);
        void CustomerServiceOutsideLogSet(CustomerServiceOutsideLog entity);
        void CustomerServiceOutsideLogDelete(CustomerServiceOutsideLog entity);
        CustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogBySerivceNo(string serviceNo);
        ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogParentServiceNo(string parentServiceNo);


        #endregion

        #region view_customer_service_inside_log
        ViewCustomerServiceInsideLogCollection GetInsideLogByServiceNo(string serviceNo);
        #endregion
        #region view_customer_service_outside_log
        ViewCustomerServiceOutsideLogCollection GetOutsideLogByServiceNo(string serviceNo);
        int GetCustomerServiceOutsideLogCountByuserId(int userId);
        ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByuserId(int userId);
        ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByuserIdAndSerivceNo(string userId, string serviceNo);
        ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByuserIdAndParentServiceNo(int userId, string parentServiceNo);
        ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByTsMemberNoAndParentServiceNo(string tsMemberNo, string parentServiceNo, int issueFromType);
        int GetCustomerServiceOutsideLogCountByorderGuid(int userId, Guid orderGuid);
        ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByorderGuid(int userId, Guid orderGuid);
        ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByTsMemberNoAndOrderGuid(string tsMemberNo, Guid orderGuid, int issueFromType);
        #endregion

        #region view_customer_service_list
        ViewCustomerServiceListCollection GetViewCustomerServiceListByVbs(int pageStart, int pageLength, string orderBy, string sellerGuids, params string[] filter);
        int GetViewCustomerServiceListByVbsCount(string sellerGuids, params string[] filter);
        ViewCustomerServiceList GetViewCustomerServiceMessageByServiceNo(string serviceNo);
        ViewCustomerServiceListCollection GetViewCustomerServiceMessageByUserId(int userId);
        ViewCustomerServiceListCollection GetViewCustomerServiceMessageByUserIdV2(int userId);
        ViewCustomerServiceListCollection GetViewCustomerServiceMessageByTsMemberNoAndIssueFromType(string tsMemberNo, int issueFromType);
        ViewCustomerServiceList GetViewCustomerServiceMessageByUserIdAndOrderGuid(int userId, Guid orderGuid);
        ViewCustomerServiceList GetViewCustomerServiceMessageByTsMemberNoAndOrderGuid(string tsMemberNo, Guid orderGuid,int issueFromType);
        ViewCustomerServiceListCollection GetViewCustomerServiceMessageByOrderGuid(Guid orderGuid);
        ViewCustomerServiceListCollection GetViewCustomerServiceMessageByPage(int page, int pageSize, string orderBy, params string[] filter);
        ViewCustomerServiceListCollection GetViewCustomerServiceMessageForNotCaimed(int page, int pageSize, string orderBy, params string[] filter);
        ViewCustomerServiceListCollection GetQueryResultForProcess(int page, int pageSize, string orderBy, string orderByAsc, params string[] filter);
        int GetViewCustomerServiceMessageListCount(params string[] filter);
        #endregion
    }
}

﻿using LunchKingSite.Core.Models.PponEntities;

namespace LunchKingSite.Core
{
    public interface IEntrepotProvider : IProvider
    {
        #region Entrepot_member

        bool EntrepotMemberSet(EntrepotMember member);
        EntrepotMember EntrepotMemberGet(string token, int ouathClientId);

        #endregion Entrepot_member
    }
}

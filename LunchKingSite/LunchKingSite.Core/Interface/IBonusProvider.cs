﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core
{
    public interface IBonusProvider : IProvider
    {
        #region ViewIdentityCode

        ViewIdentityCode IdentityCodeSet(int membershipCardId, int sellerUserId, DateTime expiredTime, int createUserId, int? discountCodeId);

        ViewIdentityCode ViewIdentityCodeGet(string code, int sellerUserId, DateTime transDateTime);

        ViewIdentityCode ViewIdentityCodeGet(long identityCodeId);

        #endregion ViewIdentityCode
    }
}

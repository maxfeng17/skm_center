﻿using System;
using System.Collections.Generic;
using System.Data;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core
{
    public interface IAccountingProvider:IProvider
    {
        #region BankAccount

        int BankAccountGetId(string bankNo, string branchNo, string accountNo, string companyId, string accountTitle);

        #endregion

        #region FundTransfer

        FundTransfer FundTransferGet(int id);
        void FundTransferSet(FundTransfer fund);
        void FundTransferSetList(FundTransferCollection funds);

        #endregion

        #region Receivable 應收帳款

        bool ReceivableSet(Receivable obj);
        int ReceivableSetList(ReceivableCollection objList);
        /// <summary>
        /// 依據 會計帳款來源 查詢應收帳款資料
        /// </summary>
        /// <param name="classificationGuid">狀款來源的鍵值(GUID類型) 比如訂單編號等</param>
        /// <param name="classification">帳款來源類型</param>
        /// <returns></returns>
        ReceivableCollection ReceivableGetList(Guid classificationGuid, AccountsClassification classification);

        #endregion Receivable 應收帳款

        #region Payable 應付帳款
        bool PayableSet(Payable obj);
        int PayableSetList(PayableCollection objList);
        /// <summary>
        /// 依據 會計帳款來源 查詢應付帳款資料
        /// </summary>
        /// <param name="classificationGuid"></param>
        /// <param name="classification"></param>
        /// <returns></returns>
        PayableCollection PayableGetList(Guid classificationGuid, AccountsClassification classification);
        #endregion Payable 應付帳款

        #region BalanceSheet 對帳單

    	BalanceSheet BalanceSheetGet(int balanceSheetId);
        int BalanceSheetSet(BalanceSheet bs);
        int BalanceSheetEstAmountSet(BalanceSheet bs, int vendorPositivePaymentOverdueAmount, int vendorNegativePaymentOverdueAmount);
        BalanceSheet BalanceSheetGetDetailNotFixed(Guid productGuid, BusinessModel productType, Guid? storeGuid = null);
		BalanceSheet BalanceSheetGetLatest(BusinessModel productType, Guid productGuid, Guid? storeGuid, BalanceSheetGenerationFrequency frequency);
        int BalanceSheetCollectionSet(BalanceSheetCollection sheets);
        BalanceSheetCollection BalanceSheetGetListByProductGuid(Guid productGuid);
        BalanceSheetCollection BalanceSheetGetListByProductGuidStoreGuid(Guid productGuid, Guid? storeGuid, BalanceSheetGenerationFrequency? frequency);
        BalanceSheetCollection BalanceSheetGetListByProductGuidStoreGuid(List<Guid> productGuids);
        BalanceSheetCollection BalanceSheetGetWeekBalanceSheetsByMonth(Guid productGuid, BusinessModel productType, int year, int month);
        BalanceSheetCollection BalanceSheetGetWeekBalanceSheetsByMonth(Guid productGuid, Guid? storeGuid, BusinessModel productType, int year, int month);
        int BalanceSheetDuplicationCountCheck(Guid productGuid, BusinessModel productType, Guid? storeGuid, DateTime intervalStart, DateTime intervalEnd, BalanceSheetType bsType);
		
		/// <summary>
		/// 用來檢查週結的月對帳單是否重複
		/// </summary>
		/// <returns></returns>
		int BalanceSheetDuplicationCountCheck(Guid productGuid, BusinessModel productType, Guid? storeGuid, int year, int month, BalanceSheetType bsType);
        void CompleteWeekBalanceSheetMonthInfo();
        BalanceSheetCollection BalanceSheetGetList(string orderBy, params string[] filter);
        BalanceSheetCollection BalanceSheetGetListByIds(List<int> Ids);
		
        /// <summary>
		/// 用來抓取特定檔次、分店及時間區間以前未產週結月對帳單之週對帳單
		/// </summary>
		/// <returns></returns>
        BalanceSheetCollection GetWeekBalanceSheetHasNoMonthBalanceSheet(Guid productGuid, BusinessModel productType, IEnumerable<Guid?> storeGuids, int year, int month);
       
        /// <summary>
        /// 用來抓取時間區間以前未產週結月對帳單之週對帳單
        /// </summary>
        /// <returns></returns>
        BalanceSheetCollection GetWeekBalanceSheetHasNoMonthBalanceSheet(int year, int month);

        /// <summary>
        /// 用來抓取時間區間以前對帳單金額為負數之對帳單(母檔對帳)
        /// </summary>
        /// <returns></returns>
        DataTable GetPayByBillBalanceSheetHasNegativeAmount(DateTime checkStartTime, DateTime checkEndTime);

        /// <summary>
        /// 用來抓取特定bid對帳單金額為負數之對帳單(母檔對帳)
        /// </summary>
        /// <returns></returns>
        DataTable GetPayByBillBalanceSheetHasNegativeAmount(Guid bid);

        bool BalanceSheetDelete(int bsId);

        #endregion

        #region BalanceSheetDetail 對帳單明細

        void BalanceSheetDetailDelete(BalanceSheetDetail detail);
        bool BalanceSheetDetailDelete(int BsId);

        int BalanceSheetDetailSet(BalanceSheetDetail detail);
        /// <summary>
        /// Because balance_sheet_detail primary key includes the column [status],
        /// changing to undo modifies [status] and requires this special method.
        /// </summary>
        int BalanceSheetDetailUndoSet(BalanceSheetDetail detail);
        int BalanceSheetDetailModifyStatus(BalanceSheetDetail detail, BalanceSheetDetailStatus newStatus);
        int BalanceSheetDetailSetList(BalanceSheetDetailCollection details);
		/// <summary>
		/// 依 trust_id 找出要付款的資料.  (所有對帳單裡, 最多只會有一筆要付款的 trust_id)
		/// </summary>
		/// <param name="trustId"></param>
		/// <returns>找到就回傳detail, 沒找到就回傳 null</returns>
    	BalanceSheetDetail BalanceSheetDetailGetByTrustIdForUndo(Guid trustId);
        BalanceSheetDetailCollection BalanceSheetDetailGetListByTrustId(Guid trustId);
        BalanceSheetDetailCollection BalanceSheetDetailGetListByTrustId(List<Guid> trustId);
        BalanceSheetDetailCollection BalanceSheetDetailGetListByBalanceSheetId(int balanceSheetId);
        BalanceSheetDetailCollection BalanceSheetDetailGetListByBalanceSheetIds(List<int> balanceSheetIds);
        BalanceSheetDetailCollection BalanceSheetDetailGetListByProductGuidAndStoreGuid(Guid productGuid, Guid? storeGuid);
        /// <summary>
        /// 商家後臺_對帳查詢:根據輸入之對帳單編號抓取憑證核銷紀錄(單筆對帳單Id)
        /// </summary>
        /// <param name="balanceSheetId"></param>
        /// <returns></returns>
        DataTable CashTrustLogGetListByBalanceSheetId(int balanceSheetId);

        /// <summary>
        /// 商家後臺_對帳查詢:根據輸入之對帳單編號抓取訂單資料(單筆對帳單Id)
        /// </summary>
        /// <param name="balanceSheetId"></param>
        /// <returns></returns>
        DataTable OrderGetListByBalanceSheetId(int balanceSheetId);

        #endregion

        #region BalanceSheetIspDetail 對帳單超取明細
        int BalanceSheetIspDetailSetList(BalanceSheetIspDetailCollection ispDetails);

        BalanceSheetIspDetailCollection BalanceSheetIspDetailGetListByBalanceSheetId(int balanceSheetId, int productDeliveryType);

        ViewBalanceSheetIspDetailListCollection ViewBalanceSheetIspDetailListGetByBalanceSheetId(int balanceSheetId);        

        ViewBalanceSheetIspDetailListCollection ViewBalanceSheetIspDetailListGetByChangeDate(int balanceSheetId, int productDeliveryType, DateTime? ispFreightsChangeSDate, DateTime? ispFreightsChangeEDate);

        int ViewBalanceSheetIspDetailListGetCountByChangeDate(int balanceSheetId, int productDeliveryType, DateTime? ispFreightsChangeSDate, DateTime? ispFreightsChangeEDate);

        BalanceSheetIspDetailCollection BalanceSheetIspDetailGetListByBid(Guid MerchandiseGuid);

        bool BalanceSheetIspDetailDelete(int bsId);
        #endregion

        #region BalanceModificationLog 對帳單異動歷程

        BalanceModificationLog BalanceModificationLogSet(BalanceModificationLog log);

        #endregion

        #region BalanceSheetWms PCHOME倉儲對帳單
        BalanceSheetWm BalanceSheetWmsGet(int balanceSheetId);
        BalanceSheetWmCollection BalanceSheetWmsGetBySeller(Guid sid, string state);
        int BalanceSheetWmsSet(BalanceSheetWm bsw);
        BalanceSheetWmCollection BalanceSheetWmsGetListByIds(List<int> Ids);
        BalanceSheetWmCollection BalanceSheetWmsGetListBySellerGuid(Guid sellerGuid);
        int BalanceSheetWmsDuplicationCountCheck(Guid sellerGuid, DateTime intervalStart, DateTime intervalEnd, BalanceSheetType bsType);
        BalanceSheetWm BalanceSheetWmsGetLatest(Guid sellerGuid);
        bool BalanceSheetWmsDelete(int bswId);
        #endregion

        #region ViewBalanceDetail

        ViewBalanceDetailCollection ViewBalanceDetailGetList(IEnumerable<int> balanceSheetIds);

        #endregion

        #region ViewBalanceSheetUndeducted

        ViewBalanceSheetUndeducted ViewBalanceSheetUndeductedGetByTrustId(Guid trustId);
        ViewBalanceSheetUndeductedCollection ViewBalanceSheetUndeductedGetList(Guid merchandiseGuid, Guid? storeGuid);

        #endregion

        #region ViewBalanceSheetNoncorrespondedVerify

        ViewBalanceSheetNoncorrespondedVerifyCollection ViewBalanceSheetNoncorrespondedVerifyGetList(Guid merchandiseGuid, Guid? storeGuid);

        #endregion

        #region ViewBalanceSheetList
        /// <summary>
        /// 商家後臺_對帳查詢:根據輸入之查詢條件取出已產生對帳單之P好康/品生活憑證檔次、賣家及分店資料
        /// </summary>
        /// <param name="businessModel">P好康或品生活</param>
        /// <param name="productGuid"></param>
        /// <param name="storeGuid"></param>
        /// <returns></returns>
        ViewBalanceSheetListCollection ViewBalanceSheetListGetList(BusinessModel businessModel, Guid productGuid, Guid? storeGuid);

        /// <summary>
        /// 商家後臺_對帳查詢:根據輸入之product_guid取出已產生對帳單之P好康/品生活憑證檔次、賣家及分店資料
        /// </summary>
        /// <param name="productGuids">多筆product_guid組成list</param>
        /// <returns></returns>
		ViewBalanceSheetListCollection ViewBalanceSheetListGetList(IEnumerable<Guid> productGuids);
        ViewBalanceSheetListCollection ViewBalanceSheetListGetListByIsNotConfirmed(IEnumerable<Guid> productGuids, string userName);

        ViewBalanceSheetListCollection PartialPaymentToBalanceSheetList(IEnumerable<Guid> productGuids);

        /// <summary>
        /// 商家後臺_對帳查詢:根據輸入之對帳單id取出已產生對帳單之P好康/品生活憑證檔次、賣家及分店資料
        /// </summary>
        /// <param name="balanceSheetIds">對帳單id list</param>
        /// <returns></returns>
        ViewBalanceSheetListCollection ViewBalanceSheetListGetListByBalanceSheetIds(List<int> balanceSheetIds);

        /// <summary>
        /// 商家後臺_對帳查詢:根據輸入之檔次bid資料 撈取未產出對帳單之彈性請款檔次資料
        /// </summary>
        /// <param name="productGuids">檔次bid list</param>
        /// <returns></returns>
        ViewBalanceSheetListCollection GetFlexiblePayToShopDealWithOutBalanceSheet(IEnumerable<Guid> productGuids);

        #endregion

        #region ViewBalanceSheetDetailList

        ViewBalanceSheetDetailListCollection ViewBalanceSheetDetailListGetList(List<int> balanceSheetIds);

        #endregion ViewBalanceSheetDetailList

        #region ViewBalanceSheetBillList

        /// <summary>
        /// 商家後臺_核銷查詢:根據對帳單Id取出P好康憑證檔次對帳單單據及付款帳號資料
        /// </summary>
        /// <param name="balanceSheetId"></param>
        /// <returns></returns>
        ViewBalanceSheetBillListCollection ViewBalanceSheetBillListGetListByBalanceSheetIds(IEnumerable<int> balanceSheetIds);

        /// <summary>
        /// 商家後臺_單據資料查詢管理:根據條件取出P好康憑證檔次對帳單單據及付款帳號資料
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewBalanceSheetBillListCollection ViewBalanceSheetBillListGetList(params string[] filter);

        /// <summary>
        /// 商家後臺_單據查詢管理:根據輸入之查詢條件取出未確認且單據未回之月對帳單
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewBalanceSheetBillListCollection ViewBalanceSheetBillListGetBalancingList(params string[] filter);

        /// <summary>
        /// 商家後臺_單據資料查詢管理:根據條件取出P好康憑證檔次對帳單單據及付款帳號分頁資料
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewBalanceSheetBillListCollection ViewBalanceSheetBillListGetListPaging(int pageIndex, int pageSize ,params string[] filter);


        /// <summary>
        /// 撈取上月實際已付款單據的物流費
        /// </summary>
        /// <param name="now"></param>
        /// <returns></returns>
        DataTable GetShippingFeeOfLastMonth();
        #endregion

        #region ViewBalanceSheetWmsBillList
        ViewBalanceSheetWmsBillListCollection ViewBalanceSheetWmsBillListGetListPaging(int pageIndex, int pageSize, params string[] filter);
        ViewBalanceSheetWmsBillListCollection ViewBalanceSheetWmsBillListGetList(params string[] filter);
        ViewBalanceSheetWmsBillListCollection ViewBalanceSheetWmsBillListGetBalancingList(params string[] filter);
        ViewBalanceSheetWmsBillListCollection ViewBalanceSheetWmsBillListGetListByBalanceSheetIds(IEnumerable<int> balanceSheetIds); 
        #endregion

        #region ViewBalancingDeal : this view retrieves deals with [vendor_billing_model] set to vendor_billing_system, i.e. 1

        ViewBalancingDealCollection ViewBalancingDealGetListLikeSellerName(string sellerName, bool generatedOnly, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dealId">ppon: [unique_id]; piin: [hi_deal_product_id]</param>
        /// <param name="generatedOnly">true: 只撈產過對帳單的檔次</param>
        /// <param name="getComboDeals">true: 只對ppon有用-使用多檔次子/母檔之檔號撈取所有關聯多檔次檔次</param>
        /// <returns></returns>
        ViewBalancingDealCollection ViewBalancingDealGetByDealId(int dealId, bool generatedOnly, bool getComboDeals);
        ViewBalancingDealCollection ViewBalancingDealGetListByMerchandiseGuid(IEnumerable<Guid> merchandiseGuids);
        ViewBalancingDealCollection ViewBalancingDealGetListLikeDealName(string dealName, bool generated, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon);
        ViewBalancingDealCollection ViewBalancingDealGetListByOrderStartRegion(DateTime? orderStartMin, DateTime? orderStartMax, bool generatedOnly, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon);
        ViewBalancingDealCollection ViewBalancingDealGetListByOrderEndRegion(DateTime? orderEndMin, DateTime? orderEndMax, bool generatedOnly, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon);
        ViewBalancingDealCollection ViewBalancingDealGetListByUseStartRegion(DateTime? useStartMin, DateTime? useStartMax, bool generatedOnly, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon);
        ViewBalancingDealCollection ViewBalancingDealGetListByUseEndRegion(DateTime? useEndMin, DateTime? useEndMax, bool generatedOnly, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon);

        ViewBalancingDealCollection ViewBalancingDealGetListLikeSellerId(string sellerName, bool generatedOnly, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon);

        DataTable PartialPaymentToBalanceingDeal(string cond);

        ViewBalancingDealCollection GetFlexiblePayToShopDealWithOutBalanceSheet(string condiction);
        
        #endregion

        #region BalanceSheetBillRelationship
        void BalanceSheetBillRelationshipSet(BalanceSheetBillRelationship bsbr);
        bool BalanceSheetBillRelationshipDelete(int balanceSheetId, int billId);
        BalanceSheetBillRelationship BalanceSheetBillRelationshipGet(int balanceSheetId, int billId);
        BalanceSheetBillRelationshipCollection BalanceSheetBillRelationshipGetListByBalanceSheetId(int balanceSheetId);
        BalanceSheetBillRelationshipCollection BalanceSheetBillRelationshipGetListByBalanceSheetIds(IEnumerable<int> balanceSheetIds);
        BalanceSheetBillRelationshipCollection BalanceSheetBillRelationshipGetListByBillId(int billId);
        int BalanceSheetBillRelationshipSetList(BalanceSheetBillRelationshipCollection relationships);

        int BalanceSheetBillRelationshipGetCount(int sheetId);
        #endregion

        #region BalanceSheetWmsBillRelationship
        void BalanceSheetWmsBillRelationshipSet(BalanceSheetWmsBillRelationship bsbr);
        int BalanceSheetWmsBillRelationshipSetList(BalanceSheetWmsBillRelationshipCollection relationships);
        BalanceSheetWmsBillRelationshipCollection BalanceSheetWmsBillRelationshipGetListByBillId(int billId);
        BalanceSheetWmsBillRelationship BalanceSheetWmsBillRelationshipGet(int balanceSheetId, int billId);
        BalanceSheetWmsBillRelationshipCollection BalanceSheetWmsBillRelationshipGetListByBalanceSheetId(int balanceSheetId);
        bool BalanceSheetWmsBillRelationshipDelete(int balanceSheetId, int billId);
        int BalanceSheetWmsBillRelationshipGetCount(int sheetId);
        #endregion

        #region Bill

        Bill BillGet(int billId);
        int BillSet(Bill bill);
        int BillSet(BillCollection bills);
        bool BillDelete(int billId);
        BillCollection BillGetList(params string[] filter);
        BillCollection BillGetList(IEnumerable<int> billIds);
        BillCollection BillGetByGuid(Guid guid);
        BillCollection BillGetByBalanceSheetId(int bsid);

        #endregion Bill

        #region BillBalanceSheetChangeLog: Table Bill and BalanceSheetRelationShip change log

        int BillBanaceSheetChangeLogSetList(BillBalanceSheetChangeLogCollection logs);

        #endregion BillBalanceSheetChangeLog: Table Bill and BalanceSheetRelationShip change log

        #region ViewBalanceSheetFundsTransferPpon  對帳單匯款狀態

        ViewBalanceSheetFundsTransferPpon ViewBalanceSheetFundsTransferPponGetByBalanceSheetId(int bsId);
        ViewBalanceSheetFundsTransferPpon ViewBalanceSheetFundsTransferPponGetByPaymentId(int payId);
        ViewBalanceSheetFundsTransferPponCollection ViewBalanceSheetFundsTransferPponGetList(BusinessModel biz, int? dealId, DateTime? minBsIntervalStart, DateTime? maxBsIntervalEnd, bool? isTransferComplete);
        ViewBalanceSheetFundsTransferPponCollection ViewBalanceSheetFundsTransferPponGetList(DateTime transferIntervalStart, DateTime transferIntervalEnd, bool? isTransferComplete);
        ViewBalanceSheetFundsTransferPponCollection ViewBalanceSheetFundsTransferPponGetList(BusinessModel biz, int? dealId, DateTime? minBsIntervalStart, DateTime? maxBsIntervalEnd, DateTime? minBillCreateTime, DateTime? maxBillCreateTime, bool? transferComplete, List<BalanceSheetType> bsTypes);
        ViewBalanceSheetFundsTransferPponCollection ViewBalanceSheetFundsTransferPponGetListByIntervalEnd(DateTime minBsIntervalEnd, DateTime maxBsIntervalEnd, bool? isTransferComplete);
        ViewBalanceSheetFundsTransferPponCollection ViewBalanceSheetFundsTransferPponGetListByBalanceSheetIds(IEnumerable<int> bsIds);

        #endregion

        #region ViewRemainderBillList
        /// <summary>
        /// 商家後臺_單據查詢管理:抓取有剩餘金額之單據
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewRemainderBillListCollection ViewRemainderBillListGetList(params string[] filter);
        #endregion

        #region  ViewBalanceSheetBillFundTransfer       
        
        ViewBalanceSheetBillFundTransferCollection ViewBalanceSheetBillFundTransferGetList(
            IEnumerable<BalanceSheetType> bsTypes,
            BusinessModel biz, int? dealId, string billCreator, DateTime? minBsIntervalStart, DateTime? maxBsIntervalEnd,
            DateTime? minBillCreateTime, DateTime? maxBillCreateTime, bool? isTransferComplete, bool confirmedOnly = true);

        #endregion

        #region ViewBalanceSheetFundTransfer

        ViewBalanceSheetFundTransfer ViewBalanceSheetFundTransferGetByBalanceSheetId(int bsId);
        ViewBalanceSheetFundTransferCollection ViewBalanceSheetFundTransferGetListByBalanceSheetIds(IEnumerable<int> bsIds);

        #endregion

        #region VendorPaymentChange

        VendorPaymentChange VendorPaymentChangeGet(int id);
        VendorPaymentChangeCollection VendorPaymentChangeGetList(Guid bid);
        VendorPaymentChangeCollection VendorPaymentChangeGetList(IEnumerable<Guid> bids);
        VendorPaymentChangeCollection VendorPaymentChangeGetList(int bsId);
        VendorPaymentChangeCollection VendorPaymentChangeGetListByIds(List<int> vId);
        VendorPaymentChangeCollection VendorPaymentChangeGetList(IEnumerable<int> ids);
        int VendorPaymentChangeSet(VendorPaymentChange vendorPaymentChange);
        int VendorPaymentChangeSetList(VendorPaymentChangeCollection vendorPaymentChanges);

        #endregion VendorPaymentChange

        #region VendorPaymentOverdue
        int VendorPaymentOverdueSet(VendorPaymentOverdue vendorPaymentOverdue);
        VendorPaymentOverdueCollection VendorPaymentOverdueGetListByBid(Guid bid);
        int VendorPaymentOverdueSetList(VendorPaymentOverdueCollection vendorPaymentOverdues);
        VendorPaymentOverdue VendorPaymentOverdueGetByOrderGuid(Guid orderGuid);
        VendorPaymentOverdueCollection VendorPaymentOverdueGetListByBsid(int bsId);
        VendorPaymentOverdueCollection VendorPaymentOverdueGetListByOverdueId(int id);
        #endregion

        #region ViewVendorPaymentOverdue
        ViewVendorPaymentOverdueCollection ViewVendorPaymentOverdueGetListByBsid(int bsId); 
        #endregion

        #region VendorFineCategory
        VendorFineCategoryCollection VendorFineCategoryGetList();
        VendorFineCategory VendorFineCategoryGetList(int id);
        int VendorFineCategorySet(VendorFineCategory vendorFineCategory);
        #endregion

    }
}

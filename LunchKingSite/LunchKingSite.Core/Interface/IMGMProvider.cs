﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;

namespace LunchKingSite.Core
{
    public interface IMGMProvider : IProvider
    {
        MemberFastInfo MemberFastInfoGetByUserId(int userid);
        int SendGiftCountGetByOrderGuid(Guid orderGuid);
        MgmGiftCollection GiftGetByAccessCode(string accessCode);
        MgmGiftVersion GiftVersionGetByGiftiId(int giftiId);
        MgmGiftVersionCollection GiftVersionGetByMsgId(int giftiId);
        MemberMessage GiftCardGetById(int msgId);
        MemberMessage GiftMsgByAccessCode(string accessCode);
        MemberMessage ReplyMsgByMsgId(int msgId);
        MemberMessageCollection GiftCardListGetByUserId(int userId);
        MemberMessageCollection GiftAccessGetByUserId(int userId);
        MemberMessageCollection GiftCardListGetBySenderId(int senderId);
        MemberMessageTemplate GiftCardTemplateGetById(int id);
        ViewMgmGift GiftGetByGiftId(int giftId);
        ViewMgmGiftCollection GiftGetByReceiverIdAndSellerGuid(int userId, Guid sellerGuid);
        ViewMgmGiftCollection GiftViewGetByAccessCode(string accessCode);
        ViewMgmGiftCollection GiftGetByReceiverId(int userId);
        ViewMgmGiftCollection GiftGetBySenderId(int userId);
        ViewMgmGiftCollection GiftGetByOid(Guid oid);
        ViewMgmGiftCollection GiftGetByBidForSendMail(Guid bid);
        ViewMgmGift GiftGetByCouponId(int couponId);
        ViewMgmGift GiftGetByCouponIdAndAsCode(int couponId,string asCode);
        ViewMgmActiveCouponCollection ActiveGiftGetByOrderGuid(Guid orderGuid);
        ViewMgmGiftCollection GiftViweGetByAsCode(string asCode, int userId);
        DataTable GetViewMgmGiftByStoreGuidAndBusinessHourSendGift(Guid bid, Guid storeGuid);
        List<int?> GiftViweGetByAsCodeWithNoUserId(string asCode);
        bool GetMGMGiftUsed(int couponId, string asCode);
        ViewMgmGiftCollection GetUnacceptedExpiredGiftList(DateTime nowTime);
        ViewMgmGift GetActiveGiftByCouponId(Guid orderGuid, int couponId);
        MemberMessageTemplateCollection MessageTemplateGet();
        bool MgmGiftSet(MgmGift mgmGift);
        bool MemberMessageSet(MemberMessage msg);
        bool MgmGiftMatchSet(MgmGiftMatch mgmGiftMatch);
        bool MgmGiftVersionSet(MgmGiftVersion mgmGiftVersion);
        bool MgmGiftVersionSet(MgmGiftVersionCollection collection);
        bool ReturnUnacceptedExpiredGiftByNowTime(DateTime nowTime);
        int MgmGiftMatchIdGet(string matchValue);
        ViewMgmGift GiftGetByGetByMatchId(int matchId);
        MemberMessage MemberMessageGetbyId(int id);
        int MatchIdGetByContainsRequestId(string requestId);
        MgmGift GiftGetById(int giftId);
        MgmGiftMatch GiftMatchGetById(int id);
        bool MgmGiftExpiredLogSet(MgmGiftExpiredLog log);
        bool MgmGiftExpiredLogSet(MgmGiftExpiredLogCollection logs);
        MgmGiftHeaderCollection MgmGiftHeaderCollectionGet();
        bool MgmGiftHeaderSet(MgmGiftHeader mgmGiftHeader);
        ViewMgmGiftBacklistCollection ViewMgmGiftBacklistGetByPage(int page, int pageSize, string orderBy, params string[] filter);
        ViewMgmGiftCollection ViewMgmGiftGetAll(string sequenceNumber);
        int ViewMgmGiftGetCount(params string[] filter);
        MgmGiftHeaderCollection MgmGiftHeaderGetByPage(int page, int pageSize, string orderBy, params string[] filter);
        int MgmGiftHeaderGetCount(params string[] filter);
        MgmGiftHeader MgmGiftHeaderGet(int id);
        bool MgmGiftHeaderDelete(int id);
        MgmGiftHeader MgmGiftHeaderByShowType(int showType);
        int MgmNewGiftCountGetByReceiverId(int userId);
    }

}

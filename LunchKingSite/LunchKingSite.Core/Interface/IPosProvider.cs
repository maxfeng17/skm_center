﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Core.Interface
{
    public interface IPosProvider : IProvider
    {
        #region PosApiLog

        ChannelPosApiLog SavePosApiLog(ChannelPosApiLog log);

        #endregion
    }
}

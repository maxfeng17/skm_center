﻿using LunchKingSite.DataOrm;
using System;

namespace LunchKingSite.Core
{
    public interface ISystemProvider : IProvider
    {
        #region ManagerResettime
        /// <summary>
        /// 取出執行中的ManagerResettime設定值。
        /// </summary>
        /// <returns></returns>
        DataManagerResettimeCollection DataManagerResettimeGetListWithEnabled();

        /// <summary>
        /// 儲存DataManagerResettime的collection
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool DataManagerResettimeSetList(DataManagerResettimeCollection data);
        /// <summary>
        /// 取得特定條件的ManagerResettime設定值。
        /// </summary>
        /// <param name="job_type"></param>
        /// <param name="enabled"></param>
        /// <param name="nextRunningTime"></param>
        /// <returns></returns>
        DataManagerResettimeCollection DataManagerResettimeGetWithJobType(int job_type, bool enabled, DateTime? nextRunningTime);
        /// <summary>
        /// 取得特定條件的ManagerResettime設定值。
        /// </summary>
        /// <param name="job_type"></param>
        /// <param name="enabled"></param>
        /// <param name="nextRunningTime"></param>
        /// <param name="serverName"></param>
        /// <returns></returns>
        DataManagerResettimeCollection DataManagerResettimeGetWithJobType(int job_type, bool enabled, DateTime? nextRunningTime, string serverName);

        /// <summary>
        /// 寫入ManagerResettime資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool DataManagerResettimeSetData(DataManagerResettime data);

        #endregion

        #region SystemCode
        SystemCodeCollection SystemCodeGetListByCodeGroup(string codeGroupId);
        SystemCodeCollection SystemCodeGetListWithEnabled();
        SystemCodeCollection SystemCodeHiDealCategoryGetList();
        string SystemCodeGetName(string codegroup, int id);
        SystemCode SystemCodeGetByCodeGroupId(string codegroup, int id);
        SystemCode ParentSystemCodeGetByCodeGroupId(string codeGroup, int id);
        SystemCodeCollection SystemCodeGetListByParentId(int parentid);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="codeGroup"></param>
        /// <param name="parentid"></param>
        /// <returns></returns>
        SystemCodeCollection SystemCodeGetListByParentId(string codeGroup, int parentid);
        int SystemCodeSet(SystemCode system_code);
        /// <summary>
        /// 取得指定群組的下一個編號
        /// </summary>
        /// <param name="codeGroup"></param>
        /// <returns></returns>
        int GetNextCodeId(string codeGroup);
        #endregion SystemCode

        #region AccBusinessGroup

        AccBusinessGroupCollection AccBusinessGroupGetList();
        AccBusinessGroup AccBusinessGroupGet(int accBusinessGroupId);

        #endregion

        #region ApiUser
        ApiUserCollection ApiUserGetList(bool isEnabled);
        #endregion ApiUser

        #region AppVersion
        /// <summary>
        /// 取出所有的AppVersion資訊
        /// </summary>
        /// <returns></returns>
        AppVersionCollection AppVersionGetList();
        void UpdateAppVersion(AppVersion version);
        #endregion AppVersion

        #region JobsStatus
        void JobsStatusSet(JobsStatus jobs);
        JobsStatus JobsStatusGet(string className);
        #endregion

        #region SystemFunction
        void SetSystemFunction(SystemFunction systemFunction);
        void DeleteSystemFunction(int id);
        SystemFunction GetSystemFunction(int id);
        SystemFunction GetSystemFunction(string link, SystemFunctionType functype);
        SystemFunctionCollection GetSystemFunctionList(int pageNumber, int pageSize, string orderBy, params string[] filter);
        int GetSystemFunctionListCount(params string[] filter);
        #endregion

        #region OauthToken
        OauthToken OauthTokenGet(string accessToken);
        OauthToken OauthTokenGet(int userId);
        #endregion

        #region OauthTokenPermission
        OauthTokenPermissionCollection OauthTokenPermissionGetList(int token_id);
        #endregion

        #region SystemData
        void SystemDataSet(SystemData data);
        SystemData SystemDataGet(string name);
        void SystemDataDelete(string name);
        #endregion

        #region sys.extended_properties
        /// <summary>
        /// 依據name取得資料庫中的ExtendedProperties參數之值，查無資料回傳空字串
        /// </summary>
        /// <param name="name"></param>
        string DBExtendedPropertiesGet(string name);

        void DbExtendedPropertyAdd(string name, string value);

        void DbExtendedPropertyUpdate(string name, string value);

        #endregion sys.extended_properties

        #region HitLog

        void HitLogSet(HitLog log);

        #endregion

        #region serial_number

        int SerialNumberGetNext(string serialName, int start = 1);

        #endregion
    }
}

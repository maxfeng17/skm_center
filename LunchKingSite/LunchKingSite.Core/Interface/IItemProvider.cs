﻿using System;
using LunchKingSite.DataOrm;
using System.Collections.Generic;

namespace LunchKingSite.Core
{
    public interface IItemProvider : IProvider
    {
        #region Accessory
        Accessory AccessoryGet(string column, object value);
        Accessory AccessoryGet(Guid g);
        AccessoryCollection AccessoryGetList(Guid guidAccessoryCategory);
        AccessoryCollection AccessoryGetList(string column, object value);
        AccessoryCollection AccessoryGetList(params Guid[] accessoryGuids);

        bool AccessorySet(Accessory a);
        bool AccessoryDelete(string column, object value);
        #endregion

        #region AccessoryCategory
        AccessoryCategory AccessoryCategoryGet(Guid guid);
        AccessoryCategory AccessoryCategoryGet(string name);
        AccessoryCategory AccessoryCategoryGetByAccessory(Guid guidAccessory);
        AccessoryCategoryCollection AccessoryCategoryGetList();

        bool AccessoryCategorySet(AccessoryCategory ac);
        bool AccessoryCategoryDelete(string column, object value);
        #endregion

        #region AccessoryGroup
        AccessoryGroup AccessoryGroupGet(Guid guidAccessoryGroup);
        AccessoryGroupCollection AccessoryGroupGetListByItem(Guid[] guidItemList);
        AccessoryGroupCollection AccessoryGroupGetListByItem(Guid itemGuid);
        AccessoryGroupCollection AccessoryGroupGetList(string column, object value);
        AccessoryGroupCollection AccessoryGroupGetList(int pageStart, int pageLength, string orderby, params string[] filter);
        AccessoryGroupCollection AccessoryGroupGetListBySeller(Guid guidSeller);
        AccessoryGroupCollection AccessoryGroupGetListNoItem(Guid sellerGuid);

        bool AccessoryGroupSet(AccessoryGroup ag);
        bool AccessoryGroupDelete(string column, object value);
        bool AccessoryGroupPurgeNotReferenced(Guid sellerGuid);
        bool AccessoryGroupPurgeReference(Guid sellerGuid);
        #endregion

        #region AccessoryGroupMember
        AccessoryGroupMember AccessoryGroupMemberGet(Guid guidAccessoryGroupMember);
        AccessoryGroupMemberCollection AccessoryGroupMemberGetList(string column, object value);
        AccessoryGroupMemberCollection AccessoryGroupMemberGetList(params Guid[] accGrpGuidList);

        bool AccessoryGroupMemberSet(AccessoryGroupMember agm);
        bool AccessoryGroupMemberDelete(string column, object value);

        #endregion

        #region Item
        Item ItemGet(Guid g);
        Item ItemGetByBid(Guid bid);
        ItemCollection ItemGetByBid(IEnumerable<Guid> bid);
        int ItemGetCount(Guid g);
        ItemCollection ItemGetList(string column, object value);
        ItemCollection ItemGetListInMenu(params Guid[] menuGuidList);
        bool ItemSet(Item i);
        bool ItemDelete(Guid g);
        #endregion

        #region ItemAccessoryGroupList
        ItemAccessoryGroupList ItemAccessoryGroupListGet(string column, object value);
        ItemAccessoryGroupListCollection ItemAccessoryGroupListGetList(string column, object value);
        ItemAccessoryGroupListCollection ItemAccessoryGroupListGetListByItem(params Guid[] itemGuidList);

        bool ItemAccessoryGroupListSet(ItemAccessoryGroupList i);
        bool ItemAccessoryGroupListDelete(string column, object value);
        bool ItemAccessoryGroupListDelete(string column, Guid itemGuid, Guid accessoryGroupGuid);
        #endregion

        #region ViewAccessoryGroupCategory
        ViewAccessoryGroupCategoryCollection ViewAccGrpCatGetList(string column, object value);
        #endregion

        #region ViewItemAccessoryGroup
        ViewItemAccessoryGroupCollection ViewItemAccessoryGroupGetList(Guid itemGuid, Guid accessoryGroupGuid);

        /// <summary>
        /// Query ViewItemAccessoryGroup to get its collection, result is sorterd according to ItemAccessoryGroupListSequence sequence and then AccessoryGroupMemberSequence
        /// </summary>
        /// <param name="itemGuid">The item GUID.</param>
        /// <returns></returns>
        ViewItemAccessoryGroupCollection ViewItemAccessoryGroupGetList(Guid itemGuid);
        #endregion

    }
}

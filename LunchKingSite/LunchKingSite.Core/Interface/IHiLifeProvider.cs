﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Core.Interface
{
    public interface IHiLifeProvider : IProvider
    {
        bool SetHiLifeVerifyLog(List<HiLifeVerifyLog> logList);
        bool UpdateHiLifePincode(HiLifePincode entity);
        bool UpdateHiLifePincodeReturnStatus(List<HiLifePincode> list);
        bool SetHiLifePincode(List<HiLifePincode> pincodeList);
        HiLifePincode GetHiLifePincode(string pincode);
        List<HiLifePincode> GetHiLifePincodeAndSee(Guid orderGuid, int cnt, List<int> excludeCouponId);
        List<HiLifePincode> GetHiLifePincodeByGift(Guid orderGuid, int cnt, List<int> couponIds);
        List<HiLifePincode> GetHiLifePincode(Guid orderGuid);
        int UpdateHiLifePincodeViewCount(List<int> idList);
        List<ViewCouponPeztempList> GetViewCouponPeztempList(Guid orderGuid);
        bool AddHiLifeGetPincodeLog(List<HiLifePincode> data, int userId, Guid orderGuid, int requestQty);
        Peztemp GetPeztemp(int pezId);
        List<HiLifeReturnLog> AddHiLifeNetReturnLog(List<HiLifeReturnLog> entity);
        List<HiLifeReturnLog> GetHiLifeReturnDataByTime(DateTime time);
        bool SetHiLifeApiLog(HiLifeApiLog log);
    }
}

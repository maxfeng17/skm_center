﻿
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;

namespace LunchKingSite.Core
{
    public interface IBookingSystemProvider : IProvider
    {
        #region view_booking_system_store_date

        ViewBookingSystemStoreDateCollection ViewBookingSystemStoreDateGetByDate(BookingSystemStore store, DateTime date);
        DayType DayTypeParse(DateTime date);

        ViewBookingSystemStoreDateCollection ViewBookingSystemStoreDateGetByBookingSystemStoreGuid(Guid storeGuid);

        #endregion

        #region view_booking_system_store_reservation_count

        ViewBookingSystemStoreReservationCountCollection ViewBookingSystemReservationCountGet(BookingSystemStore store, DateTime dateStart, DateTime dateEnd);

        #endregion

        #region view_booking_system_reservation_list

        ViewBookingSystemReservationListCollection ViewBookingSystemReservationListGet(BookingSystemStore store, DateTime queryDate);

        ViewBookingSystemReservationListCollection ViewBookingSystemReservationListGet(Guid storeGuid, DateTime queryDate);

        ViewBookingSystemReservationListCollection ViewBookingSystemReservationListGet(DateTime queryDate);

        ViewBookingSystemReservationList ViewBookingSystemReservationListGet(int reservationId);

        ViewBookingSystemReservationListCollection ViewBookingSystemReservationListGet(Guid storeGuid, string contactName, string contactMobile, string couponSequence);

        ViewBookingSystemReservationListCollection ViewBookingSystemReservationListGetList(int bookingId);

        #endregion

        #region view_booking_system_coupon

        BookingSystemCouponCollection BookingSystemCouponGetByOrderKey(string orderKey);

        #endregion

        #region view_booking_system_max_number

        ViewBookingSystemMaxNumberCollection ViewBookingSystemMaxNumberListGet(int storeBookingId);

        #endregion view_booking_system_max_number

        #region view_booking_system_coupon_reservation

        ViewBookingSystemCouponReservationCollection ViewBookingSystemCouponReservationGetBySequenceNumber(Guid orderDetailGuid, string sequenceNumber);
        ViewBookingSystemCouponReservationCollection ViewBookingSystemCouponReservationGetBySequenceNumber(string orderKey, string sequenceNumber);
        ViewBookingSystemCouponReservationCollection ViewBookingSystemCouponReservationGetBySequenceNumber(string sequenceNumber);
        ViewBookingSystemCouponReservationCollection ViewBookingSystemCouponReservationGetByOrderKey(string orderKey);
        DataTable ViewBookingSystemCouponReservationGetByOrderKey(IEnumerable<string> orderKeys);

        #endregion

        #region view_booking_system_reservation_info_day

        ViewBookingSystemReservationInfoDayCollection ViewBookingSystemReservationInfoDayGetByDate(int bookingId, DateTime date);

        #endregion

        #region booking_system_time_slot

        BookingSystemTimeSlotCollection BookingSystemTimeSlotGet(int bookingSystemDateId);
        bool BookingSystemTimeSlotSet(BookingSystemTimeSlot bookingSystemTimeSlot);
        BookingSystemTimeSlot BookingSystemTimeSlotGetById(int timeSlotId);

        #endregion

        #region booking_system_store

        /// <summary>
        /// Insert Update Booking_System_Store
        /// </summary>
        /// <param name="bookingSystemStore"></param>
        /// <returns>success:return Primary key  fail:return 0</returns>
        int BookingSystemStoreSet(BookingSystemStore bookingSystemStore);

        /// <summary>
        /// 依 store_guid 回傳 Booking_System_Store Record.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="bookingType"></param>
        /// <returns></returns>
        BookingSystemStore BookingSystemStoreGetByStoreGuid(Guid guid, BookingType bookingType);

        BookingSystemStoreCollection BookingSystemStoreGetByStoreGuid(Guid guid);

        BookingSystemStore BookingSystemStoreGetBookingId(int bookingId);

        #endregion

        #region booking_system_date

        /// <summary>
        /// Insert Update Booking_System_Date
        /// </summary>
        /// <param name="bookingSystemDate"></param>
        /// <returns>success:return Primary key  fail:return 0</returns>
        int BookingSystemDateSet(BookingSystemDate bookingSystemDate);

        BookingSystemDate BookingSystemDateGet(int storeBookingId, DayType dayType, DateTime? effectiveDate);

        BookingSystemDate BookingSystemDateGetByStoreBookingId(int storeBookingId);

        BookingSystemDateCollection BookingSystemDateGetList(int storeBookingId);

        #endregion

        #region booking_system_reservation

        /// <summary>
        /// 存檔(新增/修改)
        /// </summary>
        /// <param name="bookingSystemReservation"></param>
        /// <returns>BookingSystemReservation</returns>
        BookingSystemReservation BookingSystemReservationSet(BookingSystemReservation bookingSystemReservation);

        bool VbsBookingSystemReservationSet(BookingSystemReservation bookingSystemReservation);

        /// <summary>
        /// 依流水號查出單筆資料
        /// </summary>
        /// <param name="reservationId"></param>
        /// <returns></returns>
        BookingSystemReservation BookingSystemReservationGet(int reservationId);

        /// <summary>
        /// 依分店BookingSystemDate及日期撈取當日的預約紀錄
        /// </summary>
        /// <param name="bookingSystemDateId"></param>
        /// <param name="queryDate"></param>
        /// <returns></returns>
        BookingSystemReservationCollection BookingSystemReservationGetListByBookingSystemDateAndDay(int bookingSystemDateId, DateTime queryDate);

        /// <summary>
        /// 查詢訂位時間的已訂位總人數
        /// </summary>
        /// <param name="timeSlotId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        int ReservationPeopleOfNumberGetByDate(int timeSlotId, DateTime date);

        ViewBookingSystemReservationRecordCollection ReservationRecordGetByServiceNameAndMemberKey(string memberKey, BookingSystemServiceName serviceName, BookingSystemRecordType type);

        void SetReservationRecordCancel(int reservationId, string cancelId = null);

        #endregion

        #region booking_system_coupon

        BookingSystemCouponCollection BookingSystemCouponGetListByReservationId(int reservationId);
        bool BookingSystemCouponIsExistBySequenceOrderDetailGuid(string sequence, Guid orderDetailGuid);
        bool BookingSystemCouponSetWithReservationId(BookingSystemCoupon bookingSystemCoupon, int reservationId);

        #endregion

        #region booking_system_api

        BookingSystemServiceName BookingSystemApiGetBookingSystemServiceName(string serviceName, BookingSystemApiServiceType serviceType);
        string BookingSystemApiGetApiKeyById(int apiId);
        BookingSystemApi BookingSystemApiSet(BookingSystemApi api);
        bool BookingSystemApiCheckExistByServiceName(string serviceName, BookingSystemApiServiceType serviceType);

        #endregion

        #region view_booking_system_store

        ViewBookingSystemStoreCollection ViewBookingSystemStoreGetBySellerGuid(Guid sellerGuid, BookingType bookingType);
        ViewBookingSystemStore ViewBookingSystemStoreGetByStoreGuid(Guid storeGuid, BookingType bookingType);

        #endregion

        #region booking_system_reserve_lock_status_log

        BookingSystemReserveLockStatusLogCollection BookingSystemReserveLockStatusLogListGet(params string[] filter);

        bool BookingSystemReserveLockStatusLogSet(BookingSystemReserveLockStatusLog bookingSystemReserveLockStatusLog);

        BookingSystemReserveLockStatusLogCollection BookingSystemReserveLockStatusLogGetByCouponId(int coupon_id, int lock_status);
        #endregion booking_system_reserve_lock_status_log

        List<ReserveCouponData> GetReserveCouponWithPpon(string firstCouponSequence, string secoundCouponSequence);

        List<ReserveCouponData> GetReserveCouponWithPiinLife(string firstCouponSequence, string secoundCouponSequence);

        List<ReserveCouponData> GetReserveLockCouponWithPpon(int couponLockSearchType, DateTime searchStartDate, DateTime searchEndDate);

        List<ReserveCouponData> GetReserveLockCouponWithPiinLife(int couponLockSearchType, DateTime searchStartDate, DateTime searchEndDate);


    }

    public class ReserveCouponData
    {
        public int CouponId { get; set; }
        public string CouponUsage { get; set; }
        public string CouponSequence { get; set; }
        public string EventName { get; set; }
        public string MemberName { get; set; }
        public bool IsReservatoinLock { get; set; }
        public BusinessModel DealType { get; set; }
        public int OrderStatus { get; set; }
        public int CouponStatus { get; set; }
        public bool IsDealReserveLock { get; set; }
        public Guid DealGuid { get; set; }
        public Guid? StoreGuid { get; set; }
        public int DealUniqueId { get; set; }
        public int? OrderStatusLog { get; set; }
        public Guid? OrderGuid { get; set; }
        public DateTime ExchangePeriodStartTime { get; set; }
        public DateTime ExchangePeriodEndTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.EasyFlow.DataOrm;

namespace LunchKingSite.Core
{
    public interface IEasyFlowProvider : IProvider
    {
        #region Employee
        List<Employee> EmployeeyGet();
        Employee EmployeeyGetByEmployeeId(Guid eid);
        EmployeeCollection EmployeeyGetByEmail(string email);

        EmployeeCollection EmployeeyGetByDepartmentId(Guid departmentId);        
        #endregion Employee

        #region Department
        DepartmentCollection DepartmentGet();
        #endregion Department
    }
}

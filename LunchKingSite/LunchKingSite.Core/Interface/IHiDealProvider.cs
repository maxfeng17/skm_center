﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;

namespace LunchKingSite.Core
{
    public interface IHiDealProvider : IProvider
    {
        #region HiDealOrder

        bool HiDealOrderSet(HiDealOrder order);

        HiDealOrder HiDealOrderGet(int orderPk);

        HiDealOrder HiDealOrderGet(Guid orderGuid);

        HiDealOrder HiDealOrderGet(string column, object value);

        HiDealOrder HiDealOrderGetByOrderId(string orderId, int userId);

        /// <summary>
        /// 取得所有訂單狀態為 confirm的紀錄，CreateTime 小於 傳入時間。
        /// </summary>
        /// <param name="beforeCreateTime"></param>
        /// <returns></returns>
        HiDealOrderCollection HiDealOrderGetListAtConfirm(DateTime beforeCreateTime);

        HiDealCouponCollection HiDealCouponGetListByProdcutId(int productId);

        #endregion HiDealOrder

        #region HiDealOrderDetail

        bool HiDealOrderDetailSetList(HiDealOrderDetailCollection orderDetails);

        /// <summary>
        /// 已鍵值orderDetailId取出訂單明細資料
        /// </summary>
        /// <param name="orderDetailId"></param>
        /// <returns></returns>
        HiDealOrderDetail HiDealOrderDetailGet(int orderDetailId);

        HiDealOrderDetail HiDealOrderDetailGet(Guid orderDetailGuid);

        /// <summary>
        /// 查詢orderGuid此訂單的orderDetail資料
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="productType">商品類型</param>
        /// <returns></returns>
        HiDealOrderDetailCollection HiDealOrderDetailGetListByOrderGuid(Guid orderGuid, HiDealProductType productType);

        /// <summary>
        /// 查詢orderGuid此訂單的orderDetail資料
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        HiDealOrderDetailCollection HiDealOrderDetailGetListByOrderGuid(Guid orderGuid);

        /// <summary>
        /// 查詢hi_deal_product id此訂單的orderDetail資料
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        HiDealOrderDetailCollection HiDealOrderDetailGetListByProductId(int productId);

        #endregion HiDealOrderDetail

        #region HiDealOrderShow

        bool HiDealOrderShowSet(HiDealOrderShow order);

        HiDealOrderShow HiDealOrderShowGet(Guid orderGuid);

        HiDealOrderShow HiDealOrderShowGetByOrderPk(int orderPk);

        HiDealOrderShowCollection GetHiDealOrderShowListByUser(int pageStart, int pageLength, int userId, string orderBy, string filter);

        int GetHiDealOrderShowListCount(int userId, string filter);

        int HidealRemainCouponCountGet(Guid hidealOrderGuid);

        #endregion HiDealOrderShow

        #region HiDealFreight

        HiDealFreightCollection HiDealFreightGetListByProductId(int productId, HiDealFreightType type);

        void HiDealFreightDelete(HiDealFreight freight);

        int HiDealFreightSet(HiDealFreight deal);

        HiDealFreight HiDealFreightGetLatest(int productId, HiDealFreightType type);

        #endregion HiDealFreight

        #region HiDealCity

        void HiDealCitySet(HiDealCity city);

        HiDealCityCollection HiDealCityGetListByDealId(int dealId);

        void HiDealCityDealTimeSet(int dealId, DateTime startTime, DateTime endTime, List<string> cityNames, bool IsAlwaysMain);

        void HiDealCityCollectionUpdateSequence(int id, int seq);

        /// <summary>
        /// 檢查某檔次在特定時間，被分配到的類別數量。
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="checkTime"></param>
        /// <returns></returns>
        int HiDealCityGetCityCountByPidAndTime(int productId, DateTime checkTime);

        /// <summary>
        /// 取得特定時間、特定城市之所有檔次
        /// </summary>
        /// <param name="cityId">city_id</param>
        /// <param name="startTime">開始時間</param>
        /// <param name="endTime">結束時間</param>
        /// <returns></returns>
        HiDealCityCollection HiDealCityGetListByCityIdAndDate(int cityId, DateTime startTime, DateTime endTime);

        /// <summary>
        /// 取得今天on檔、特定城市的所有檔次
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        HiDealCityCollection HiDealCityTodayDealsGetByCityId(int cityId, DateTime date);

        HiDealCity HiDealCityGetByCityIdAndDealIdAndDate(int cityId, int dealId, DateTime startTime, DateTime endTime);

        HiDealCityCollection HiDealCityGetListByDateWithFirstDealOfAllCity(DateTime startTime);

        #endregion HiDealCity

        #region HiDealCategory

        void HiDealCategorySet(HiDealCategory category);

        HiDealCategoryCollection HiDealDealCategoryGetListByDealId(int dealId);

        bool HiDealCategoryMerge(int dealId, List<int> catgIds);

        #endregion HiDealCategory

        #region HiDealSpecialDiscount

        HiDealSpecialDiscountCollection HiDealSpecialDiscountGetListByDealId(int dealId);

        bool HiDealSpecialDiscountMerge(int dealId, List<int> discountIds);

        #endregion HiDealSpecialDiscount

        #region HiDealDeals

        HiDealDeal HiDealDealGet(int id);

        HiDealDeal HiDealDealGet(Guid guid);

        int? HiDealSpecialIdGet(int dealId);

        int HiDealDealSet(HiDealDeal deal);

        HiDealDealCollection HiDealDealGetListByPeriod(DateTime dateStart, DateTime dateEnd);

        HiDealDealCollection HiDealDealGetList(params string[] filter);
        #endregion HiDealDeals

        #region HiDealContent

        void HiDealContentSet(HiDealContent content);

        HiDealContentCollection HiDealContentGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        HiDealContentCollection HiDealContentGetAllByDealId(int dealId);

        #endregion HiDealContent

        #region HiDealProduct

        HiDealProduct HiDealProductGet(int id);

        HiDealProduct HiDealProductGet(Guid prodGuid);

        int HiDealProductSet(HiDealProduct product);

        /// <summary>
        /// 檢查某個品生活Product中, 是否有任何其它訂單的運單編號跟傳入的運單編號一樣
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="shipNo"></param>
        /// <param name="orderShipId"></param>
        /// <returns></returns>
        bool IsDuplicateHiDealShipNo(int pid, string shipNo, int? orderShipId);

        HiDealProductCollection HiDealProductGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        HiDealProductCollection HiDealProductGetList(IEnumerable<int> productIds);

        HiDealProductCollection HiDealProductCollectionGet(int dealId);

        /// <summary>
        /// 抓出屬於新版核銷, 且沒有出貨紀錄的HiDealProductCollection
        /// </summary>
        /// <returns></returns>
        HiDealProductCollection HiDealProductCollectionGetListByNoOrderShip();

        #endregion HiDealProduct

        #region HiDealProductCost

        int HiDealProductCostSet(HiDealProductCost productCost);

        HiDealProductCostCollection HiDealProductCostCollectionGetByProductId(int pid);

        HiDealProductCost HiDealProductCostGetLatest(int pid);

        void HiDealProductCostDelete(HiDealProductCost productCost);

        #endregion HiDealProductCost

        #region HiDealProductOptionCategory

        int HiDealProductOptionCategorySet(HiDealProductOptionCategory category);

        HiDealProductOptionCategoryCollection HiDealProductOptionCategoryGetListByProductId(int pid);

        void HiDealProductOptionCategoryDelete(int catgId);

        #endregion HiDealProductOptionCategory

        #region HiDealProductOptionItem

        int HiDealProductOptionItemSet(HiDealProductOptionItem item);

        HiDealProductOptionItemCollection HiDealProductOptionItemGetListdByCategoryId(int cid);

        void HiDealProductOptionItemDeleteById(int itemId);

        void HiDealProductOptionItemUpdateSellQuantity(List<int> idList, int modifyQuantity);

        #endregion HiDealProductOptionItem

        #region ViewHiDeal

        ViewHiDealCollection ViewHiDealGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        /// <summary>
        /// 上檔3天「內」的visa優先檔次
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        ViewHiDealCollection ViewHiDealFreshVisaPriority(DateTime startTime, DateTime endTime, int cityId);

        /// <summary>
        /// 上檔3天「後」的visa優先檔次
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        ViewHiDealCollection ViewHiDealStaleVisaPriority(DateTime startTime, DateTime endTime, int cityId);

        ViewHiDealCollection ViewHiDealGetTodayDeals(int city, string category, int? special, bool isMain, string orderBy);

        DataTable ViewHiDealGetTodayCategorys(int cityId);

        Dictionary<HiDealSpecialDiscountType, int> ViewHiDealGetTodaySpecials(int cityId);

        ViewHiDealCityseqCollection ViewHiDealCollectionGetByTime(DateTime startTime, DateTime endTime, int CityCodeId = 0);

        ViewHiDeal ViewHiDealGetByDealId(int dealId);

        #endregion ViewHiDeal

        #region HiDealRegion

        HiDealRegionCollection HiDealRegionGetList(int dealId);

        bool HiDealRegionMerge(int dealId, List<string> cityNames);

        #endregion HiDealRegion

        #region HiDealDealsStore

        int HiDealDealsStoreSet(HiDealDealStore dealStore);

        void HiDealDealsStoreDelete(HiDealDealStore dealStore);

        HiDealDealStoreCollection HiDealDealsStoreGetListByHiDealId(int dealId);

        DataTable HiDealDealStoreWithStoreInformation(int dealId);

        #endregion HiDealDealsStore

        #region HiDealSubscription

        void HiDealSubscriptionSet(HiDealSubscription content);

        HiDealSubscriptionCollection HiDealSubscriptionGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        void HiDealSubscriptionSetToNewUser(string oldEmail, string newEmail);

        /// <summary>
        /// 刪除PiinLife訂閱重複的Email
        /// </summary>
        /// <param name="region_code_id"></param>
        void HiDealSuscriptionDeleteRepeat(int region_code_id);

        /// <summary>
        /// 抓取最後一筆ID，做分次處理
        /// </summary>
        /// <returns></returns>
        int HiDealSuscriptionGetLatestId();

        void HiDealSubscriptionSetList(HiDealSubscriptionCollection items);  //save從HiDealSubscriptionCollection撈取出來的值

        /// <summary>
        /// 依id區間抓取訂閱資料，為了做分批處理
        /// </summary>
        /// <param name="min_id">ID的最小值</param>
        /// <param name="max_id">ID的最大值</param>
        /// <returns></returns>
        HiDealSubscriptionCollection HiDealSubscribersGetOverviewByIdRegion(int min_id, int max_id);

        HiDealSubscriptionCollection HiDealSubscriptionGetListByUser(string userName);

        HiDealSubscription HiDealSubscriptionGetByUserRegionCodeId(string userName, int regionCodeId);

        HiDealSubscription HiDealSubscriptionGetByUserCityId(string userName, int cityId);

        #endregion HiDealSubscription

        #region Unsubscription

        Unsubscription UnsubscriptionGet(string email);

        void UnsubscriptionSet(Unsubscription content);

        #endregion Unsubscription

        #region HiDealSubscriptionPromo

        HiDealSubscriptionPromo HiDealSubscriptionPromoGet(int id);

        void HiDealSubscriptionPromoSet(HiDealSubscriptionPromo content);

        HiDealSubscriptionPromoCollection HiDealSubscriptionPromoGetListWithOutDelete();

        #endregion HiDealSubscriptionPromo

        #region HiDealSubscriptionPromoSlot

        int HiDealSubscriptionPromoSlotSet(HiDealSubscriptionPromoSlot content);

        HiDealSubscriptionPromoSlot HiDealSubscriptionPromoSlotGetByCodeIdAndPromoId(int codeId, int promoId);

        HiDealSubscriptionPromoSlotCollection HiDealSubscriptionPromoSlotGetList(int codeId);

        HiDealSubscriptionPromoSlotCollection HiDealSubscriptionPromoSlotGetList(int codeId, DateTime date);

        DataTable HiDealSubscriptionPromoSlotGetListSort(int intOrderby, bool isAsc);

        #endregion HiDealSubscriptionPromoSlot

        #region ViewHiDealProductStore

        ViewHiDealProductStoreCollection ViewHiDealProductStoreGetListByProductId(int productId);

        #endregion ViewHiDealProductStore

        #region HiDealCoupon

        HiDealCoupon HiDealCouponGet(long id);

        int HiDealCouponSet(HiDealCoupon c);

        HiDealCoupon HiDealCouponGetTop(int dealId, int productId, Guid? storeGuid);

        HiDealCoupon HiDealCouponGetTop(int dealId, int productId);

        /// <summary>
        /// 取出某商品某訂單的coupon資料
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="orderPk"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        HiDealCouponCollection HiDealCouponGetListByOrderPk(int productId, int orderPk, HiDealCouponStatus? status);

        /// <summary>
        /// 取出狀態為可用的coupon資料，不管有無分店設定的
        /// </summary>
        /// <param name="dealId">檔次編號</param>
        /// <param name="productId">商品編號</param>
        /// <param name="startIndex">起始CouponId</param>
        /// <param name="length">長度</param>
        /// <returns></returns>
        HiDealCouponCollection HiDealCouponGetListInAvailableAfterIndex(int dealId, int productId, long startIndex,
                                                                        int length);

        /// <summary>
        /// 取出狀態為可用的coupon資料，有分店設定的
        /// </summary>
        /// <param name="dealId">檔次編號</param>
        /// <param name="productId">商品編號</param>
        /// <param name="storeGuid">分店GUID</param>
        /// <param name="startIndex">起始CouponId</param>
        /// <param name="length">長度</param>
        /// <returns></returns>
        HiDealCouponCollection HiDealCouponGetListInAvailableAfterIndex(int dealId, int productId, Guid storeGuid, long startIndex,
                                                                int length);

        /// <summary>
        /// 儲存整組的Coupon資料
        /// </summary>
        /// <param name="dataList"></param>
        void HiDealCouponSetList(HiDealCouponCollection dataList);

        /// <summary>
        /// 查詢不存在於HiDealCouponOffer中，且狀態為可用的憑證數量(無分店)
        /// </summary>
        /// <param name="dealId">檔次編號</param>
        /// <param name="productId">商品編號</param>
        /// <returns></returns>
        int HiDealCouponGetNotInHiDealCouponOfferCount(int dealId, int productId);

        /// <summary>
        /// 查詢不存在於HiDealCouponOffer中，且狀態為可用的憑證數量(有分店設定)
        /// </summary>
        /// <param name="dealId">檔次編號</param>
        /// <param name="productId">商品編號</param>
        /// <param name="storeGuid">分店編號</param>
        /// <returns></returns>
        int HiDealCouponGetNotInHiDealCouponOfferCount(int dealId, int productId, Guid storeGuid);

        /// <summary>
        /// 取出消費者某商品的已購買數量
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <param name="userName">使用者編號</param>
        /// <returns></returns>
        int HiDealCouponGetCountByProductAndUser(int productId, int userId);

        int HiDealCouponGetProductAvailable(int dealId, int productId);

        int HiDealCouponGetStoreAvailable(int dealId, int productId, Guid? storeGuid);

        void HiDealCouponUpdateStatus(HiDealCouponStatus statusFrom, HiDealCouponStatus statusTo, int dealId, int productId);

        void HiDealCouponUpdateStatus(HiDealCouponStatus statusFrom, HiDealCouponStatus statusTo, int dealId, int productId, Guid? storeGuid);

        /// <summary>
        /// 修改Coupon的狀態為已分配
        /// </summary>
        /// <param name="couponId">要修改的coupon單號LIST</param>
        /// <param name="orderPk">分配到哪張訂單</param>
        /// <param name="detailGuid">分配到哪筆orderDetail</param>
        /// <param name="userName">會員編號</param>
        void HiDealCouponUpdateToAssigned(List<long> couponId, int orderPk, Guid detailGuid, int userId);

        /// <summary>
        /// 搜尋符合條件狀態的好康卷資料
        /// </summary>
        /// <param name="dealId">檔次ID</param>
        /// <param name="productId">商品ID</param>
        /// <param name="st">狀態</param>
        /// <param name="orderBy">排序條件的欄位名稱，空格後加desc為降冪</param>
        /// <returns></returns>
        HiDealCouponCollection HiDealCouponGetByStatus(int dealId, int productId, HiDealCouponStatus st, List<string> orderBy);

        /// <summary>
        /// 搜尋符合條件狀態的好康卷資料
        /// </summary>
        /// <param name="dealId">檔次ID</param>
        /// <param name="productId">商品ID</param>
        /// <param name="storeGuid">店鋪GUID</param>
        /// <param name="st">狀態</param>
        /// <param name="orderBy">排序條件的欄位名稱，空格後加desc為降冪</param>
        /// <returns></returns>
        HiDealCouponCollection HiDealCouponGetByStatus(int dealId, int productId, Guid? storeGuid, HiDealCouponStatus st, List<string> orderBy);

        /// <summary>
        /// 檢查是否有已發送或使用中的憑證 有回傳true 否則回傳false
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        bool HiDealCouponHaveAssignedOrUsed(int productId);

        /// <summary>
        /// 刪除product所有的憑證資料。
        /// </summary>
        /// <param name="productId"></param>
        void HiDealCouponDeleteAllByProductId(int productId);

        /// <summary>
        /// 商家系統用於匯出品生活商家清冊的DataTable
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        DataTable GetHiDealCouponListForExport(Guid productGuid, Guid? storeGuid);

        #endregion HiDealCoupon

        #region HiDealStaticDeal

        int HiDealStaticDealSet(HiDealStaticDeal sd);

        int HiDealStaticDealGetProductMaxChangeSet(int productId);

        HiDealStaticDealCollection HiDealStaticDealGetListByProductIdAndMaxChangeSet(int productId);

        #endregion HiDealStaticDeal

        #region HiDealProductStore

        HiDealProductStore HiDealProductStoreGetByPidAndStoreGuid(int productId, Guid storeGuid);

        int HiDealProductStoreSet(HiDealProductStore c);

        HiDealProductStoreCollection HiDealProductStoreGetListByProductId(int productId);

        DataTable HiDealProductStoreWithStoreInformation(int productId);

        void HiDealProductStoreDelete(HiDealProductStore productStore);

        #endregion HiDealProductStore

        #region HiDealCouponPrefix

        int HiDealCouponPrefixSet(HiDealCouponPrefix c);

        int HiDealCouponPrefixGetMaxSequence(int dealId);

        int HiDealCouponPrefixGetMaxSequenceBySellerGuid(Guid sellerguid);

        #endregion HiDealCouponPrefix

        #region HiDealCouponLog

        int HiDealCouponLogSet(HiDealCouponLog c);

        #endregion HiDealCouponLog

        #region HiDealCouponOffer

        bool HiDealCouponOfferSetList(HiDealCouponOfferCollection lists);

        void HiDealCouponOfferDeleteByOrderPk(int orderPk);

        #endregion HiDealCouponOffer

        #region HiDealStoreInfo

        ViewHiDealStoresInfoCollection ViewHiDealDealStoreInfoGetByHiDealId(int dealId);

        #endregion HiDealStoreInfo

        #region HiDealSystemCode

        /// <summary>
        /// 取得 HiDeal 所有會用到的城市
        /// </summary>
        /// <returns></returns>
        SystemCodeCollection HiDealGetAllCityFromSystemCode();

        SystemCode HiDealGetOneCityFromSystemCode(int cityId);

        #endregion HiDealSystemCode

        #region HiDealReturned

        int HiDealReturnedSet(HiDealReturned data);

        int HiDealReturnedSetList(HiDealReturnedCollection datas);

        HiDealReturned HiDealReturnedGet(int returnedId);

        HiDealReturnedCollection HiDealReturnedGetListByOrderPk(int orderPk);

        HiDealReturnedCollection HiDealReturnedGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        #endregion HiDealReturned

        #region HiDealReturnedCoupon

        int HiDealReturnedCouponSetList(HiDealReturnedCouponCollection data);

        HiDealReturnedCouponCollection HiDealReturnedCouponGetList(int returnedId);

        #endregion HiDealReturnedCoupon

        #region HiDealReturnedDetail

        int HiDealReturnedDetailSetList(HiDealReturnedDetailCollection data);

        HiDealReturnedDetailCollection HiDealReturnedDetailGetListByReturnedId(int returnedId);

        #endregion HiDealReturnedDetail

        #region ViewHiDealCouponListSequence

        ViewHiDealCouponListSequenceCollection GetHiDealCouponListSequenceListByOid(Guid orderGuid);

        #endregion ViewHiDealCouponListSequence

        #region ViewHiDealCoupon

        ViewHiDealCoupon ViewHiDealCouponGet(int couponId);

        ViewHiDealCouponCollection ViewHiDealCouponGetListByOrderPk(int orderPk);

        #endregion ViewHiDealCoupon

        #region ViewHiDealCouponTrustStatus

        ViewHiDealCouponTrustStatusCollection ViewHiDealCouponTrustStatusGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        #endregion ViewHiDealCouponTrustStatus

        #region ViewHiDealReturned

        /// <summary>
        /// 取出某訂單的退貨資料。如果傳入狀態statuses，則只取出特定狀態的退貨單。
        /// </summary>
        /// <param name="orderPk"></param>
        /// <param name="statuses"></param>
        /// <returns></returns>
        ViewHiDealReturnedCollection ViewHiDealReturnedGetList(int orderPk, List<HiDealReturnedStatus> statuses);

        /// <summary>
        /// 取出某訂單的退貨資料。
        /// </summary>
        /// <param name="orderPk"></param>
        /// <returns></returns>
        ViewHiDealReturnedCollection ViewHiDealReturnedGetList(int orderPk);

        /// <summary>
        /// 下SQL條件, 抓出所有符合的品生活退貨資料
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        ViewHiDealReturnedCollection ViewHiDealReturnedGetList(int pageStart, int pageLength, string[] filter, string orderBy);

        #endregion ViewHiDealReturned

        #region ViewHiDealOrderDealOrderReturned

        ViewHiDealOrderDealOrderReturnedCollection GetViewHiDealOrderDealOrderReturnedDialogData(int pageStart, int pageLength, string[] filter, string orderBy);

        int ViewHiDealOrderDealOrderReturnedDialogDataCount(string[] filter);

        int ViewHiDealOrderDealOrderReturnedDialogDataCount(int did);

        #endregion ViewHiDealOrderDealOrderReturned

        #region ViewHiDealOrderMemberOrderShow

        ViewHiDealOrderMemberOrderShowCollection ViewHiDealOrderMemberOrderShowDialogDataGet(int pageStart, int pageLength, string[] filter, string orderBy);

        int ViewHiDealOrderMemberOrderShowDialogDataGetCount(string[] filter);

        ViewHiDealOrderMemberOrderShowCollection ViewHiDealOrderMemberOrderShowGetByOrderGuid(Guid orderGuid);

        #endregion ViewHiDealOrderMemberOrderShow

        #region ViewHiDealSellerProduct

        ViewHiDealSellerProductCollection ViewHiDealSellerProductGetList(int pageStart, int pageLength, string[] filter, string orderBy);

        int ViewHiDealSellerProductDialogDataGetCount(string[] filter);

        ViewHiDealSellerProductCollection ViewHiDealSellerProductGetList(int pageStart, int pageLength, string CrossDeptTeam, string[] filter, string orderBy);

        int ViewHiDealSellerProductDialogDataGetCount(string CrossDeptTeam, string[] filter);

        /// <summary>
        /// 商品檔次
        /// </summary>
        ViewHiDealSellerProductCollection ViewHiDealSellerProductGetDeal(string dealid);

        #endregion ViewHiDealSellerProduct

        #region ViewHiDealProductSaleInfo

        ViewHiDealProductSaleInfoCollection GetViewHiDealProductSaleInfoBySellerGuidOnStart(Guid seGuid);

        ViewHiDealProductSaleInfoCollection GetViewHiDealProductSaleInfoBySellerAndStoreGuidOnStart(Guid seGuid, Guid stGuid);

        #endregion ViewHiDealProductSaleInfo

        #region ViewHiDealReturnedLight

        ViewHiDealReturnedLightCollection ViewHiDealReturnedLightGetList(int pageStart, int pageLength, string[] filter, string orderBy);

        int ViewHiDealReturnedLightDialogDataGetCount(string[] filter);

        #endregion ViewHiDealReturnedLight

        #region ViewHiDealOrderReturnQuantity

        /// <summary>
        /// 取商品購買與退貨數量
        /// </summary>
        ViewHiDealOrderReturnQuantityCollection ViewHiDealOrderReturnQuantityGetListByProductId(int productId);

        /// <summary>
        /// 取某檔次下的所有商品
        /// </summary>
        ViewHiDealOrderReturnQuantityCollection ViewHiDealOrderReturnQuantityGetListByDealId(int DealId);

        #endregion ViewHiDealOrderReturnQuantity

        #region ViewHiDealProductInfo

        /// <summary>
        /// 商家清冊(分為到店與宅配)
        /// </summ
        /// ary>
        ViewHiDealProductHouseInfoCollection ViewHiDealProductHouseInfoGetListByProductId(int productId);

        ViewHiDealProductShopInfoCollection ViewHiDealProductShopInfoGetListByProductId(int productId);

        #endregion ViewHiDealProductInfo

        #region ViewHiDealTimeSlot

        ViewHiDealTimeslotCollection GetViewHiDealTimeslotCollectionByDate(int cityid, DateTime date);

        #endregion ViewHiDealTimeSlot

        #region ACH

        List<ViewHiDealWeeklyPayReport> ViewHiDealWeeklyPayReportGetList(
            DateTime? dateStart, DateTime? dateEnd, Guid? reportGuid, int? productId, int? reportId, bool? isSummary);

        #endregion ACH

        #region ViewHiDealCouponStoreListCount

        ViewHiDealCouponStoreListCountCollection GetViewHiDealCouponStoreListCount(int DealId);

        #endregion ViewHiDealCouponStoreListCount

        #region ViewHiDealProductSellerStore

        /// <summary>
        /// 商家後臺_核銷查詢:根據輸入之查詢條件取出已成檔之憑證檔次、賣家及分店資料
        /// </summary>
        /// <param name="storeGuidList">具檢視權限之分店Guid list 以,連結多筆資料</param>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewHiDealProductSellerStoreCollection ViewHiDealProductSellerStoreGetToShopDealList(string storeGuidList, params string[] filter);

        #endregion ViewHiDealProductSellerStore

        #region ViewHiDealExpiration

        ViewHiDealExpirationCollection ViewHiDealExpirationGetList(Guid productGuid);

        ViewHiDealExpirationCollection ViewHiDealExpirationGetList(DateTime usageTime, int endBuffer = 0);

        #endregion ViewHiDealExpiration

        #region ViewHiDealCouponTrustStatusLog
        ViewHiDealCouponTrustLogCollection ViewHiDealCouponTrustStatusLogGet(string prefix, string sequence,string queryType);

        #endregion ViewHiDealCouponTrustStatusLog

        #region ViewSellerHiDeal

        ViewSellerHiDealCollection ViewSellerHiDealGetList(Guid sellerGuid);

        #endregion
    }
}
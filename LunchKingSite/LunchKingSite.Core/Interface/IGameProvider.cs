﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Models.GameEntities;

namespace LunchKingSite.Core.Interface
{
    public interface IGameProvider : IProvider
    {
        void SaveGameCampaign(GameCampaign campaign, string modifyId);
        GameCampaign GetGameCampaign(int campaignId);
        GameCampaign GetGameCampaign(string name);
        int GetCampaignIdFromRoundGuid(Guid roundGuid);

        void SaveGameActivity(GameActivity activity, string modifyId);        
        GameActivity GetGameActivity(Guid guid);

        GameActivity GetGameActivityBycampaignId(Guid bid, int campaignId);
        GameActivity GetGameActivity(int activityId);

        List<GameActivity> GetGameActivityByCampaignId(int campaignId);

        void SaveGameGame(GameGame game, string modifyId);
        List<GameGame> GetGamesByActivityId(int activityId);

        GameGame GetGame(int gameId);

        List<TopGameView> GetTopGameActivityViewList(int campaignId);

        GameRoundView GetGameRoundView(Guid roundGuid);
        /// <summary>
        /// 我的活動
        /// </summary>
        /// <returns></returns>
        List<GameRoundView> GetGameRoundViewList(int userId, GameCampaign campaign);
        
        List<GameActivityView> GetGameActivityViewByCampaignId(int id);
        void SaveGameRule(GameRule rule, string modifyId);

        GameRound InsertGameRoundWhenNoOtherRunningOne(int userId, GameGame game, DateTime expiredTime, string modifyId);
        void SaveGameRound(GameRound round, string modifyId);
        bool ExecuteGameRoundCompletePlan(GameRound round, string modifyId, out bool full);
        GameRound GetGameRound(int id);
        GameRound GetGameRound(Guid roundGuid);
        GameRound GetGameRoundByActivityId(int activityId, int userId);        
        GameRound GetGameRoundByActivityId(int activityId, int userId, GameRoundStatus status);
        List<GameRound> GetGameRoundByGameId(int gameId, GameRoundStatus status);
        GameCampaign GetCurrentGameCampaign();

        List<GameCampaign> GetAllGameCampaign();
        List<GameRule> GetAllGameRules();
        bool HasGameDealPermissionByMainOrSubDeal(int userId, Guid dealId);
        GameDealPermission GetUnexpiredGameDealPermission(int userId, Guid dealId);
        GameDealPermission GetAvailableGameDealPermission(int userId, Guid dealId);
        void SaveGameDealPermission(GameDealPermission gameDealPermission);

        /// <summary>
        /// 測試時，匯入用測試檔次用
        /// </summary>
        /// <param name="topNum"></param>
        /// <returns></returns>
        List<Guid> GetEligibleDealsDealIds(int topNum);
        bool GetGameActivitiesAndGamesByCampaignId(int campaignId,
            out List<GameActivity> activities, out List<List<GameGame>> activityGames);        

        GameBuddy GetGameBuddy(int gameBuddyId);
        GameBuddy GetGameBuddy(Guid roundGuid, SingleSignOnSource source, string externalId);
        /// <summary>
        /// 取得目前使用者建立的遊戲(GameRound)，有多少人幫助
        /// </summary>
        /// <param name="roundId"></param>
        /// <returns></returns>
        List<GameBuddy> GetGameBuddyList(int roundId);
        void SaveGameBuddy(GameBuddy buddy);
        bool InsertGameBuddyReward(GameBuddyReward reward);
        void UpdateGameBuddyReward(GameBuddyReward reward);
        GameBuddyReward GetGameBuddyReward(int userId, DateTime today);
        GameActivity GetGameActivityByRoundGuid(Guid roundGuid);
        List<GameRound> GetEpiredRunningGameRounds();
        void SaveGameDealOrder(GameDealOrder gameDealOrder);
        GameDealOrder GetGameDearOrder(Guid orderGuid);
        GameBuddyReward GetGameBuddyReward(int buddyId);
    }
}

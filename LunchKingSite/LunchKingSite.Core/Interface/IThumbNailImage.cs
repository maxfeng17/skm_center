﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace LunchKingSite.Core.Interface
{
    public interface IThumbNailImage
    {
        void TransferOriginalImage(string url);
        void ShowThumbNailImage(Stream outputstream);
    }
}

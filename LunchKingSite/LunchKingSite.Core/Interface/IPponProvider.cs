﻿using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Data;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.Core
{
    public interface IPponProvider : IProvider
    {
        #region BusinessHour

        BusinessHour BusinessHourGet(Guid businessHourGuid);

        void BusinessHourSet(BusinessHour bh);

        List<Guid> BusinessHourGetBidsBySeller(Guid sellerGuid);
        DataTable BusinessHourGetExpireRedirectUrl(DateTime sDate, DateTime eDate);

        #endregion BusinessHour

        #region Coupon

        Coupon CouponGet(string column, object value);

        Coupon CouponGet(int id);

        CouponCollection CouponGet(List<Guid> trustIds);

        CouponCollection CouponGetList(Guid orderDetailId);

        CouponCollection CouponGetListWithNolock(Guid orderDetailId);

        bool CouponSet(Coupon coupon);

        bool CouponSetBulk(CouponCollection couponCol);

        string CouponGetMaxSequence(string bid);

        #region peztemp_batch_log

        PeztempBatchLog PeztempBatchLogSave(PeztempBatchLog log);
        int PeztempBatchLogSave(PeztempBatchLogCollection logs);
        PeztempBatchLogCollection PeztempBatchLogGet(Guid bid, bool isBackupCode);
        bool PeztempBatchLogDelete(int peztempBatchLogId);

        #endregion peztemp_batch_log

        #region PezTemp
        //PEZ Event新增
        Peztemp PeztempGet(Guid bid);

        Peztemp PeztempGet(Guid bid, string userid);

        Peztemp PeztempGetByOrderDetailGuid(Guid order_detail_guid);

        Peztemp PeztempGet(Guid orderDetailGuid, Guid businessHourGuid);

        PeztempCollection PeztempGetByGid(Guid businessHourGuid);

        Peztemp PeztempGet(string pezCode);

        Peztemp PeztempGet(int peztempId);

        Peztemp PeztempGetTest(Guid bid);

        Peztemp PeztempMakeTestCode(Guid bid);

        Peztemp PeztempPincodeGet(string pezCode, PeztempServiceCode serviceCode);

        PeztempCollection PeztempMakeBackupCode(Guid bid, int batchId, int codeQty);

        PeztempCollection PeztempGetBackupCode(Guid bid, int batchId);

        PeztempCollection PeztempPincodeColGet(string pezCode, PeztempServiceCode serviceCode);

        /// <summary>
        /// 依bid抓取所有peztemp
        /// </summary>
        /// <param name="bid">Business_Hour_Guid</param>
        /// <returns></returns>
        PeztempCollection PeztempGetListByBid(Guid bid);

        bool PeztempSet(Peztemp pez);

        int PeztempGetCount(Guid bid, bool isused);

        void PeztempUpdatePrintTime(string pezcode, DateTime time);

        void PeztempUpdateCvsId(string pezcode, int cvsid);

        void PezErrorLogInsert(string pezcode, string cvsstoreid, int status);

        bool PezTempValidator(string pezcode);

        void PeztempCouponSet(PeztempCoupon peztempCoupon);

        void PeztempCouponDelete(string pezCode, Guid businessHourGuid);

        /// <summary>
        /// Peztemp自動隨機產碼(會檢查是否序號已存在)
        /// </summary>
        /// <param name="pezCode"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        int PeztempSetWithVerification(int batch_id, Guid bid, int service_code, int qty, bool isBackupCode);

        /// <summary>
        /// 根據兌換中檔次取得Peztemp資料
        /// </summary>
        /// <param name="pezCode">PezCode</param>
        /// <param name="serviceCode">服務代碼</param>
        /// <param name="deliverTimedate">兌換時間</param>
        /// <returns>Peztemp</returns>
        Peztemp PeztempGetWithDate(string pezCode, int serviceCode, DateTime deliverTimedate);

        /// <summary>
        /// 將已結檔的peztemp.status標記為已過期(PeztempStatus.Expired)
        /// </summary>
        /// <param name="service_code">服務代碼</param>
        void SetPeztempStatusExpiredWithDeliverTimeEndForFami(int service_code);

        int PeztempGetUsingMemberUserId(Peztemp pez);

        Peztemp PeztempGetByCode(Guid orderDetailGuid, string pezCode);        

        #endregion

        void UpdateSellerCouponListExportLog(Guid bid, string username);

        int CvsStoreGet(string cvsstoreid);

        /// <summary>
        /// 取得商家清冊
        /// </summary>
        /// <param name="bid"></param>
        /// <returns>datatable如果要新增或修改欄位請到Provider去改</returns>
        DataTable GetSellerCouponList(Guid bid);

        /// <summary>
        /// 福利網報表
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        DataTable GetComReport(Guid bid);

        /// <summary>
        /// 取得好康發票清單
        /// </summary>
        /// <param name="bid"></param>
        /// <returns>datatable如果要新增或修改欄位請到Provider去改</returns>
        DataTable GetSellerInvoiceList(Guid bid);

        void UpdateGetSellerItemListExportLog(Guid bid, string orderTimeStart, string orderTimeEnd, string username);

        /// <summary>
        /// 取得商家清冊(商品用) by filter 購買日期
        /// </summary>
        /// <param name="bid">bid</param>
        /// <param name="orderTimeStart">購買日期(起)</param>
        /// <param name="orderTimeEnd">購買日期(迄)</param>
        /// <returns></returns>
        DataTable GetSellerItemList(Guid bid, string orderTimeStart, string orderTimeEnd);

        //後臺取P完美清單 2010/11/12 40
        DataTable GetPeautySellerItemList(Guid bid, string eventName);

        bool UnlockCouponStatusByCouponIds(List<int> couponIds);

        #endregion Coupon

        #region CouponEventContent
        bool CouponEventContentSet(CouponEventContent cec);

        CouponEventContent CouponEventContentGet(string column, object value);
        CouponEventContent CouponEventContentGetByBid(Guid bid);

        /// <summary>
        /// 由多檔次子檔BID取出主檔的CouponEventContent資料
        /// </summary>
        /// <param name="subDealBid">子檔次BID</param>
        /// <returns>主檔次的CouponEventContent資料，由物件的isLoad判斷是否有取得資料</returns>
        CouponEventContent CouponEventContentGetBySubDealBid(Guid subDealBid);
        Item ItemGetByBid(Guid bid);
        /// <summary>
        /// 由多檔次子檔BID取出主檔的ItemName資料
        /// </summary>
        /// <param name="subDealBid">子檔次BID</param>
        /// <returns>主檔次的ItemName資料，由物件的isLoad判斷是否有取得資料</returns>
        Item ItemGetBySubDealBid(Guid subDealBid);
        /// <summary>
        /// 針對圖檔做更新
        /// </summary>
        /// <param name="image_path"></param>
        /// <param name="special_image_path"></param>
        /// <param name="travel_edm_special_image_path"></param>
        /// <param name="bid"></param>
        void CouponEventContentUpdatePic(string image_path, string special_image_path, string travel_edm_special_image_path, Guid bid);
        void CouponEventContentUpdateDescription(string description, int id);
        void CouponEventContentUpdateRemark(string remark, int id);
        #endregion CouponEventContent

        #region ProposalCouponEventContent
        ProposalCouponEventContent ProposalCouponEventContentGetByPid(int pid);

        List<int> ProposalCouponEventContentGetByAppTitle(string appTitle);
        bool ProposalCouponEventContentSet(ProposalCouponEventContent pcec);

        bool ProposalCouponEventContentDelete(int pid);
        #endregion ProposalCouponEventContent

        #region CouponFreight

        bool CouponFreightSetList(CouponFreightCollection couponFreightList);

        /// <summary>
        /// 刪除BusinessHourGuid與傳入參數相同的資料
        /// </summary>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        bool CouponFreightDeleteListByBid(Guid businessHourGuid);

        CouponFreightCollection CouponFreightGetList(Guid businessHourGuid);

        CouponFreightCollection CouponFreightGetList(Guid businessHourGuid, CouponFreightType type);

        #endregion CouponFreight

        #region DealProperty

        bool DealPropertySet(DealProperty dealProperty);

        DealProperty DealPropertyGet(Guid bid);
        /// <summary>
        /// 取得小倉檔次列表
        /// </summary>
        /// <param name="IsConsignment"></param>
        /// <returns></returns>
        List<Guid> ConsignmentDealPropertyGet(bool IsConsignment);
        /// <summary>
        /// 傳入uniqueId取得DealProperty
        /// </summary>
        /// <param name="uniqueId">對外用的唯一值</param>
        /// <returns></returns>
        DealProperty DealPropertyGet(int uniqueId);
        DealPropertyCollection DealPropertyGet(IEnumerable<int> uniqueId);
        DealPropertyCollection DealPropertyListGet(List<Guid> bids);

        /// <summary>
        /// 取得接續某檔的所有檔次資料
        /// </summary>
        /// <param name="bid">被接續的檔次 bid</param>
        /// <returns></returns>
        DealPropertyCollection DealPropertyGetByAncestor(Guid bid);

        /// <summary>
        /// 取得指定期間內DealProperty資料
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        DealPropertyCollection DealPropertyGetByDate(DateTime dateStart, DateTime dateEnd);
        List<int> RecentOrderDealTypeGetByUserId(int userId, Guid orderGuid);
        DealProperty DealPropertyGetByOrderGuid(Guid orderGuid);

        /// <summary>
        /// 取得某多檔次的子檔資料，且為成套販售的
        /// </summary>
        /// <param name="mainDealGuid">多檔次主檔次之GUID</param>
        /// <returns></returns>
        DealPropertyCollection DealPropertyGetSubDealIsPackSet(Guid mainDealGuid);

        #endregion DealProperty

        #region DealPayment
        bool DealPaymentSet(DealPayment dealPayment);

        DealPayment DealPaymentGet(Guid bid);

        DealPaymentCollection DealPaymentsGet(IEnumerable<Guid> bids);
        #endregion

        #region ImgCdn

        bool ImgPoolSet(ImgPool imgPool);

        ImgPool ImgPoolGet(Guid bid, string path);
        void ImgPoolDeleteByBid(Guid bid);
        ImgPoolCollection ImgPoolGetAllBybid(Guid bid);
        ImgPoolCollection ImgPoolGetListByDayWithNoLock(DateTime sDate, DateTime eDate);

        int ImgPoolDelete(int id);

        #endregion

        #region PponDeal

        PponDeal PponDealGet(Guid bid);

        bool PponDealSet(PponDeal data, bool syncOrderableWithin, DealTimeSlotCollection origDtsCol = null);

        bool PponDealDelete(Guid bizHour);

        bool PponDealExtendAfterClose(Guid bid);

        #region EDM用

        DataTable RemittanceTypeCheck();

        bool SetSubjectNameByBid(string bizHour, string subjectName);

        #endregion EDM用

        #endregion PponDeal

        #region appPponDeal

        AppDefaultPageDealsSettingCollection GetAllAppDefaultPageDealsSetting();
        void SaveAllAppDefaultPageDealsSetting(AppDefaultPageDealsSettingCollection data);

        #endregion

        #region ViewPponOrder

        ViewPponOrder ViewPponOrderGet(Guid orderGuid);

        ViewPponOrderCollection ViewPponOrderGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        ViewPponOrderCollection ViewPponOrderGetList(Guid dealGuid);

        int ViewPponOrderGetCount(params string[] filter);

        bool ViewPponOrderUpdateShipStatus(OrderStatusLog ol, int orderstatus);

        OrderCollection PponOrderGetByBid(Guid bid);
        OrderCollection PponOrderGetByOrderId(string oid);

        ViewPponOrderCollection ViewPponOrderGetListByUserId(int userId);

        #endregion ViewPponOrder

        #region ViewPponOrderDetail

        ViewPponOrderDetail ViewPponOrderDetailGet(Guid orderDetailGuid);

        ViewPponOrderDetailCollection ViewPponOrderDetailGetListByOrderGuid(Guid orderGuid);

        ViewPponOrderDetailCollection ViewPponOrderDetailGetList(string column, object value, string orderBy);

        /// <summary>
        /// 用來取出此使用者在此Deal中已經購買的份數
        /// </summary>
        /// <param name="user_name"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        int ViewPponOrderDetailGetCountByUser(string user_name, Guid bid, bool isDailyRstriction = false);
        /// <summary>
        /// 用來取出此使用者在此Deal中已經購買的份數
        /// </summary>
        /// <param name="user_name"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        int ViewPponOrderDetailGetCountByUser(int userId, Guid bid, bool isDailyRstriction);

        DataTable PponGoman(string user_name);

        #endregion ViewPponOrderDetail

        #region ViewPponCoupon

        ViewPponCoupon ViewPponCouponGet(int id);

        ViewPponCoupon ViewPponCouponGet(Guid orderDetailGuid, string sequenceNumber);

        ViewPponCouponCollection ViewPponCouponGetListByUserName(int pageStart, int pageLength, string user_name, string orderBy, string filter);

        ViewPponCouponCollection ViewPponCouponGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        ViewPponCouponCollection ViewPponCouponGetList(int pageStart, int pageLength, Guid business_hour_guid, string orderBy);

        ViewPponCouponCollection ViewPponCouponGetList(string column, object value);

        ViewPponCouponCollection ViewPponCouponGetList(string column, object[] values);

        ViewPponCouponCollection ViewPponCouponGetListByOrderGuid(Guid orderGuid);

        ViewPponCouponCollection ViewPponCouponGetNoCouponList(string column, object value);

        ViewPponCouponCollection ViewPponCouponGetListRegularOnly(string column, object value);

        ViewPponCouponCollection GetNoCouponList();

        DataTable ViewPponCouponGetIsReservationLockCouponList(Guid bid);

        int CheckCouponDetail(string orderDetailId);

        List<Guid> CouponlessOrderDetailGuidGetList();

        DataTable CheckShippingDate();

        int ViewPponCouponByBidAndOrderDetailStatusCount(Guid businessHourGuid);

        OrderCollection OverchargedMonitorJob();

        PaymentTransactionCollection OverchargedWithoutOrderMonitorJob();

        PaymentTransactionCollection GetOtpNotPostBack();

        List<string> GetCreditcatrInconsistent();

        DataTable ChargingFailureMonitorJob();

        int CheckCouponCount(Guid odid);

        int ClearCouponByCouponID(int id);

        QueryCommand GetInsertCouponCmd(Coupon c);

        int ViewPponCouponGetCount(string user_name, string filter);

        DataTable GetEinvoiceMain2To3();

        decimal ViewPponCouponGetSum(string user_name);

        void SetAtmCouponToQueue(Guid orderGuid);

        /// <summary>
        /// 用bid取CouponEvent資料及Coupon"商家序號"。因ViewPponCoupon裡有Join Coupon中的"序號"原本couponEvent則沒有。
        /// </summary>
        ViewPponCoupon ViewPponCouponGetCouponEventContent(string column, object value);

        ViewPponCoupon ViewPponCouponGetBySeqCodeCreateTime(int couponId, string sequenceNumber, string couponCode, string orderDetailCreateTime);

        /// <summary>
        /// 傳入賣家Guid, 抓出所有該賣家底下 "尚未使用" 的憑證
        /// </summary>
        ViewPponCouponCollection ViewPponCouponGetListUnusedBySellerGuid(Guid sellerGuid);

        /// <summary>
        /// 傳入賣家Guid與商店Guid, 抓出該店底下所有尚未使用的憑證
        /// </summary>
        ViewPponCouponCollection ViewPponCouponGetListUnusedBySellerStoreGuid(Guid sellerGuid, Guid storeGuid);

        /// <summary>
        /// 傳入Bid, 傳回該檔次下所有未使用的憑證
        /// </summary>
        ViewPponCouponCollection ViewPponCouponGetListUnusedByDeal(Guid bid);

        /// <summary>
        /// 傳入Bid 和 storeGuid, 傳回該檔次的該分店底下所有未使用的憑證
        /// </summary>
        ViewPponCouponCollection ViewPponCouponGetListUnusedByDealStoreGuid(Guid bid, Guid storeGuid);

        int ViewPponCouponGetFirstBusinessHourStatus(Guid bid, Guid storeGuid, Guid orderGuid);

        #endregion ViewPponCoupon

        #region ViewPponDeal

        ViewPponDealCollection ViewPponDealGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter);


        ViewPponDealCalendar ViewPponDealCalendarGet(Guid bid);

        ViewPponDealCalendarCollection ViewPponDealGetListPagingNoSub(int pageStart, int pageLength, string orderBy, int EmpUserId, string CrossDeptTeam, int? SalesId, params string[] filter);

        ViewPponDealCollection ViewPponDealGetListPagingForYahoo(int pageStart, int pageLength, DateTime dateStart, DateTime dateEnd, Guid bid, string dealNname);

        int ViewPponDealGetCountForYahoo(DateTime dateStart, DateTime dateEnd, Guid bid, string dealNname);

        ViewPponDealCalendarCollection ViewPponDealGetListPagingForCategory(int pageStart, int pageLength, string orderBy, int categoryId, int? dealtype1, int? dealtype2, DateTime? businessOrderTimeS, DateTime? businessOrderTimeE, DateTime? businessDeliverTimeS, DateTime? businessDeliverTimeE, string sellerName, string dealName, bool isCloseDeal, bool isOnDeal, int? SalesId, int UniqueId, string CrossDeptTeam, string businessHourGuid, int EmpUserId,int Wms, ref string sSQL);

        int ViewPponDealGetCountForCategory(int categoryId, int? dealtype1, int? dealtype2, DateTime? businessOrderTimeS, DateTime? businessOrderTimeE, DateTime? businessDeliverTimeS, DateTime? businessDeliverTimeE, string sellerName, string dealName, bool isCloseDeal, bool isOnDeal, int? SalesId, int UniqueId, string businessHourGuid, string CrossDeptTeam, int EmpUserId, int Wms);

        string GetViewPponDealForCategorySummaryPriceCommand(string type);

        ViewPponDealCollection ViewPponDealGetListByMainBid(Guid mainBid);

        ViewPponDealCollection ViewPponDealGetListOnShowBySellerGuid(Guid sellerGuid);

        ViewPponDealCollection ViewPponDealGetListOnUseBySellerGuid(Guid sellerGuid);

        List<Guid> OnlineDealGuidGetList(DateTime sDate, DateTime eDate);

        ViewPponDealCollection ViewPponDealGetList(params string[] filter);

        ViewPponDealStoreCollection ViewPponDealStoreGetListDeliverTime(DateTime date, Guid sellerguid, Guid storeguid);

        ViewPponDealStoreCollection ViewPponDealStoreGetList(Guid sellerguid, Guid storeguid);

        int ViewPponDealGetCount(params string[] filter);

        int ViewPponDealNoSubGetCount(int EmpUserId, string CrossDeptTeam, int? SalesId, params string[] filter);

        ViewPponDealCollection ViewPponDealGetListByCityIdOrderBySlotSeq(int cityId, DeliveryType[] deliverytype); //20120314 依city取得Deal資訊，以消費方式撈取(憑證宅配)

        List<Guid> ViewPponDealGetBidListByDay(DateTime sDate, DateTime eDate);

        List<Guid> BusinessHourGuidGetListByCityIdOrderBySlotSeq(int cityId, DeliveryType[] deliverytype);

        /// <summary>
        /// 依據傳入的日期區間，取得排程資料(DealTimeSolt)中，起始日期位於此區間中的ViewPponDeal資料。
        /// 使用NoLock關鍵字，所以資料也許不是最正確的。
        /// </summary>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <returns></returns>
        ViewPponDealCollection ViewPponDealGetListByDayWithNoLock(DateTime sDate, DateTime eDate);
        ViewPponDealCollection ViewPponDealGetListByDayWithNoLock();

        /// <summary>
        /// 依據傳入的日期區間，取得排程資料(DealTimeSolt)中，起始日期位於此區間中的ViewPponDeal資料。
        /// 使用NoLock關鍵字，所以資料也許不是最正確的。
        /// </summary>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <param name="cityType">排除福利網、品生活、天貓等特殊檔次</param>
        /// <param name="onlyDealShowInDefault">排除不顯示的檔次</param>
        /// <returns></returns>
        ViewPponDealCollection ViewPponDealGetListByDayWithNoLock(DateTime sDate, DateTime eDate, PponDealSpecialCityType cityType, bool onlyDealShowInDefault);
        List<Guid> ViewPponDealGetBidListByDayWithNoLock(DateTime sDate, DateTime eDate, PponDealSpecialCityType cityType, bool onlyDealShowInDefault);

        ViewPponDeal ViewPponDealGet(Guid orderGuid);

        ViewPponDeal ViewPponDealGetByBusinessHourGuid(Guid bzGuid);

        ViewPponDealCollection ViewPponDealListGetByMainbid(Guid mainBid);

        ViewPponDealCollection ViewPponDealGetByBusinessHourGuidList(List<Guid> bzGuid);

        /// <summary>
        /// 依照uniqueid取得viewppondeal資料
        /// </summary>
        /// <param name="uid">UniqueId</param>
        /// <returns></returns>
        ViewPponDeal ViewPponDealGetByUniqueId(int uid);

        ViewPponDeal ViewPponDealGet(params string[] filter);

        /// <summary>
        /// 取出今日全部好康
        /// </summary>
        /// <returns></returns>
        ViewPponDealCollection ViewPponDealsToday();

        ViewPponDealCollection ViewPponDealsToday(string filter_string);

        ViewPponDealCollection GetRandomPponDealByCity(int num, int cityId);

        ViewPponDealCollection DealTimeSlotGetToday(int cid);

        ViewPponDealCollection ViewPponDealGetBySellerGuid(Guid sellerGuid);

        ViewPponDealCollection ViewPponDealGetBySellerGuid(Guid sellerGuid, Guid businessHourGuid);

        /// <summary>
        /// 依據 seller guid 取出有成檔之檔次
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        ViewPponDealCollection ViewPponDealGetBySellerGuidWithDealDone(Guid sellerGuid);

        /// <summary>
        /// 從兌換開始至兌換結束為今日起三個月內之檔次皆取出
        /// </summary>
        /// <param name="sDate">兌換開始日(當天)</param>
        /// <returns></returns>
        ViewPponDealCollection ViewPponDealGetListByDeliverTimeStart(DateTime sDate);

        /// <summary>
        /// 從兌換開始至兌換結束為今日起三個月內之檔次皆取出(篩選有送禮成功且無送兌換者)
        /// </summary>
        /// <param name="sDate">兌換開始日(當天)</param>
        /// <returns></returns>
        ViewPponDealCollection ViewPponDealGetListByDeliverTimeStartForSendGift(DateTime sDate);

        /// <summary>
        /// 取得指定業務期間內檔次
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="salesName"></param>
        /// <returns></returns>
        ViewPponDealCalendarCollection ViewPponDealCalendarGetListByPeriod(DateTime dateStart, DateTime dateEnd, string deptId);
        ViewPponDealCalendarCollection ViewPponDealCalendarGetListByPeriod(DateTime dateStart, DateTime dateEnd, string deptId, string CrossDeptTeam, ref string sSQL);
        ViewPponDealCollection ViewPponDealGetListByPeriod(Guid bid, DateTime dateStart, DateTime dateEnd);
        ViewPponDealCollection ViewPponDealGetListBySellerGuid(DateTime dateStart, DateTime dateEnd, Guid sellerGuid);

        /// <summary>
        /// 取得期間內檔次
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        ViewPponDealCollection ViewPponDealGetListByPeriod(DateTime dateStart, DateTime dateEnd);

        /// <summary>
        /// 退貨期限截止通知信使用. 傳入一個日期A (需為零時零分零秒), 取得 "搶購結束隔日剛好為A日" 檔次
        /// </summary>
        ViewPponDealCollection ViewPponDealGetListByNoReturn(DateTime theDay);

        /// <summary>
        /// 退貨期限截止通知信使用. 傳入一個日期A (需為零時零分零秒), 取得 "配送結束隔日不可退貨" 且 "配送結束隔日剛好為A日" 的檔次
        /// </summary>
        ViewPponDealCollection ViewPponDealGetListByExpireNoReturn(DateTime theDay);

        /// <summary>
        /// 展期到期核銷通知信使用, 條件為 1.宅配 2.合乎三種 "不可退貨" 條件之一 3.傳入的日期為寄送截止日的後一天
        /// </summary>
        /// <param name="theDay"></param>
        /// <returns></returns>
        ViewPponDealCollection ViewPponDealGetListByVerifyInform(DateTime theDay);

        ViewPponDealCollection ViewPponDealGetVerificationNoticeList();

        #endregion ViewPponDeal

        #region subscription

        void SubscriptionStatusSleepUpdate(int count, int minId, int maxId);

        void SubscriptionStatusDefaultUpdate(int count, int minId, int maxId);

        void SubscriptionDelete(int id);

        SubscriptionCollection SubscriptionGetList(string email);

        Subscription SubscriptionGet(string email, int categoryId);

        void SubscriptionSet(Subscription ss);

        SubscriptionCollection SubscriptionGetListByCategoryList(params int[] categoryIdList);

        SubscriptionCollection SubscriptionGetListByCategoryId(int categoryId, int min_id, int max_id);

        List<SubscriptionModel> SubscriptionGetListByCategoryId(int categoryId);

        void SubscriptionCancel(int id);

        void SubscriptionReSet(int id);

        void SubscriptionEditStatus(int id, int iStats);

        int SubscriptionGetStatus(int id);

        void SubscriptionUpdateReliable(string email, bool reliable);

        bool SubscriptionSetToNewUser(string origUserName, string newUserName);

        /// <summary>
        /// 為了抓取資料效率，抓出目前最後一筆ID，分批處理
        /// </summary>
        /// <returns></returns>
        int SubscriptionGetLatestId();

        #endregion subscription

        #region SubscriptionNotice
        /// <summary>
        /// 新增或修改推播訂閱的資料
        /// </summary>
        /// <param name="subscription"></param>
        /// <returns></returns>
        int SubscriptionNoticeSet(SubscriptionNotice subscription);
        /// <summary>
        /// 依據裝置與訂閱的頻道區域，取得訂閱紀錄。
        /// 若無subCategoryId需傳入null
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="channelId"></param>
        /// <param name="subCategoryId"></param>
        /// <returns></returns>
        SubscriptionNotice SubscriptionNoticeGet(int deviceId, int channelId, int? subCategoryId);
        /// <summary>
        /// 修改訂閱推播的狀態，將某個裝置的訂閱資料enabled值全部設為某個數值
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="enabled"></param>
        /// <returns></returns>
        int SubscriptionNoticeSetEnabled(int deviceId, bool enabled);
        #endregion SubscriptionNotice


        #region TempSession

        void TempSessionSetForUpdate(TempSession ts);

        void TempSessionSet(TempSession ts);

        void TempSessionDelete(int id);

        TempSessionCollection TempSessionGetList(string sessionId);

        void TempSessionDelete(string sessionId);

        TempSession TempSessionGet(string sessionId, string name);

        #endregion TempSession

        #region HotDeal

        HotDealConfigCollection HotDealConfigGetAll();
        HotDealConfig HotDealConfigGet(int cityId);
        bool HotDealConfigSet(HotDealConfig hdc);
        DealTimeSlotHotDealOrderedTotalCollection HotDealOrderedTotalGetAll();

        /// <summary>
        /// 清除DealTimeSlot所有熱銷註記
        /// </summary>
        /// <param name="workDate"></param>
        int ClearHotDealFlag(DateTime workDate);
        /// <summary>
        /// 依頻道清除DealTimeSlot熱銷註記
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        int ClearHotDealFlagByCityId(int cityId, DateTime workDate);

        /// <summary>
        /// 清除熱銷暫存表所有資料
        /// </summary>
        void ClearDealTimeSlotHotDealOrderedTotal();
        /// <summary>
        /// 依頻道清除清除熱銷暫存表
        /// </summary>
        /// <param name="cityId"></param>
        int ClearDealTimeSlotHotDealOrderedTotalByCityId(int cityId);

        /// <summary>
        /// 取得最近n前n檔熱銷檔次
        /// </summary>
        /// <param name="topN"></param>
        /// <param name="lastNday"></param>
        /// <param name="cityId"></param>
        /// <param name="workDate"></param>
        void GetHotDealToTempTable(int topN, int lastNday, int cityId, DateTime workDate);

        /// <summary>
        /// 註記所有熱銷
        /// </summary>
        void SetDealTimeSlotHotDealFlag();
        /// <summary>
        /// 依頻道註記熱銷
        /// </summary>
        /// <param name="cityId"></param>
        void SetDealTimeSlotHotDealFlagByCity(int cityId);

        #endregion

        #region TempDealTimeSlotSeq

        bool TruncateAndBulkInsertTempDealTimeSlotSeqCol(TempDealTimeSlotSeqCollection data);
        int UpdateDealTimeSlotSeqFromTemp();

        #endregion

        #region DealTimeSlot

        bool DealTimeSlotUpdateStatusWithTransaction(List<DealTimeSlot> dtsList, int status);
        /// <summary>
        /// 異動指定檔次、頻道，且時間在 effectStart 之後的顯示狀態
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="cityId"></param>
        /// <param name="effectStart"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        bool DealTimeSlotUpdateStatusWithTransaction(Guid bid, int cityId, DateTime effectStart, int status);
        bool DealTimeSlotUpdateSeqWithTransaction(List<ViewPponDealTimeSlot> vpdtsList);
        bool DealTimeSlotUpdate(DealTimeSlot data);

        bool DealTimeSlotUpdateSeqAddOne(int cityId, DateTime effectiveStartDate);

        void SortDealTimeSlotBulkInsert(SortDealTimeSlotCollection data);
        void UpdateDealTimeSlotFromSortDealTimeSlotCol();
        void TruncateSortDealTimeSlot();

        DealTimeSlot DealTimeSlotGet(Guid guid, int cityId, DateTime effectiveStart);
        
        DealTimeSlotCollection DealTimeSlotGetCol(Guid guid);

        DealTimeSlotCollection DealTimeSlotGetAllCol(Guid guid);

        DealTimeSlot DealTimeSlotGetMainDeal(int cityId, DateTime showTime, DepartmentTypes[] departments);

        DealTimeSlotCollection DealTimeSlotGetMainDealOfAllCities(DateTime showTime);

        DealTimeSlotCollection DealTimeSlotGetList(string orderBy, params string[] filter);

        DealTimeSlotCollection DealTimeSlotGetToday();

        DataTable DealTomeSlotGetTodayCityId();

        DataTable DealTimeSlotGetCityListBySeller(Guid sellerGuid);
        
        int DealTimeSlotGetMaxSeqInCityAndDay(int cityId, DateTime effectiveStartDate);

        void DealTimeSlotDeleteList(Guid bid);

        void DealTimeSlotDeleteByToday(Guid bid);

        bool DealTimeSlotSet(DealTimeSlot DealTimeSlot);

        void DealTimeSlotDeleteAfterNewEndTime(Guid bid, DateTime newEndTime);

        #endregion DealTimeSlot

        #region ViewDealTimeSlot

        /// <summary>
        /// 取得當前時段需排序的DealTimeSlot
        /// </summary>
        /// <param name="cityIds"></param>
        /// <param name="runTime"></param>
        /// <returns></returns>
        ViewPponDealTimeSlotCollection ViewPponDealTimeSlotToSortList(string cityIds, DateTime runTime);

        /// <summary>
        /// 取得當前時段尚不需排序的DealTimeSlot(未開賣及已結束)
        /// </summary>
        /// <param name="cityIds"></param>
        /// <returns></returns>
        ViewPponDealTimeSlotCollection ViewPponDealTimeSlotNotToSortList(string cityIds);

        ViewPponDealTimeSlotCollection ViewPponDealTimeSlotGetList(string orderBy, params string[] filter);
        ViewPponDealTimeSlotCollection ViewPponDealTimeSlotCollectionByCityAndStartTime(DateTime startTime, int cityId);

        ViewPponDealTimeSlotCollection ViewPponDealTimeSlotGetListByCityDate(int city_id, DateTime effectdate);
        ViewPponDealTimeSlotCollection ViewPponDealTimeSlotGetWmsListByCityDate(DateTime effectdate);

        ViewPponDealTimeSlotCollection ViewPponDealTimeSlotByFastShip(int city_id, DateTime effectdate, string notContainBid);

        List<int> CategoryDealTimeSlotGetCategoryIdList();

        #endregion ViewDealTimeSlot

        #region Evaluate 檔次評價

        void ResetVerifiedStoreEvaluateDetail();

        /// <summary>
        /// 某時間點的「檔次與分店對照」(VerifiedStoreEvaluateBid)是否存在
        /// </summary>
        /// <param name="modifyDate"></param>
        /// <returns></returns>
        bool HaveVerifiedStoreEvaluateBidByModifyDate(DateTime modifyDate);
        /// <summary>
        /// Insert某時間點的「檔次與分店對照」
        /// </summary>
        /// <param name="modifyDate"></param>
        /// <returns></returns>
        int SetVerifiedStoreEvaluateBid(DateTime modifyDate);
        /// <summary>
        /// 刪除某時間點以前的「檔次與分店對照」
        /// </summary>
        /// <param name="modifyDate"></param>
        void DeleteVerifiedStoreEvaluateBidByModifyDate(DateTime modifyDate);

        ViewPponDealEvaluateStarCollection ViewPponDealEvaluateStarCollectionGet();
        PponDealEvaluateStarCollection PponDealEvaluateStarCollectionGet();

        int SetPponDealEvaluateStar();

        #endregion

        #region SMSLog

        void SMSLogSet(SmsLog sms);

        int SMSLogGetCountByPpon(string userid, string couponId);

        int SMSLogGetCountByPpon(string userid, string couponId, SmsType type);

        int SMSLogGetcountByCouponIdList(List<int> couponIdList);

        SmsLogCollection SMSLogGetQueue();

        void SmsLogSetToQueue(Guid bid);

        bool SmsLogSetStatus(string smsOrderId, string phoneNumber, SmsStatus status, SmsFailReason reason, DateTime resultTime);

        SmsLog SmsLogGet(string smsOrderId, string phoneNumber);

        List<SmsLog> SmsLogGetListByTaskId(Guid taskId);

        #endregion SMSLog

        #region FETSmsLog

        void FetSmsLogSet(FetSmsLog fsl);

        #endregion FETSmsLog

        #region S2teSmsLog

        void S2tSmsLogSet(S2tSmsLog stl);

        #endregion S2teSmsLog

        #region HinetSmsLog

        void HinetSmsLogSet(HinetSmsLog hsl);

        #endregion HinetSmsLog

        #region SMSContent

        void SMSContentSet(SmsContent sms);

        SmsContent SMSContentGet(Guid bid);

        #endregion SMSContent

        #region City

        CityCollection CityGetListByTop(string code);

        int CityGetTheCityId(string code);

        string CityGetTheCityCode(int cityId);

        List<int> CityGetDealTimeSlotListByDate(DateTime theDate);

        #endregion City

        #region CityGroup

        CityGroupCollection CityGroupGetByCityId(int cityId, CityGroupType type);

        CityGroupCollection CityGroupGetListByType(CityGroupType type);

        CityGroupCollection CityGroupGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        #endregion CityGroup

        #region DealAccounting

        void DealAccountingSet(DealAccounting dAccounting);

        DealAccounting DealAccountingGet(Guid businessHourGuid);

        DealAccountingCollection DealAccountingGet(IEnumerable<Guid> businessHourGuids);

        bool DealAccountingSetFlag(Guid businessHourGuid, AccountingFlag flag);

        #endregion DealAccounting

        #region DealSalesInfo & Daily

        void DealSalesInfoSave(DealSalesInfo dsi);
        DealSalesInfoCollection DealSalesInfoGet(IEnumerable<Guid> BusinessHourGuids);
        void DealSalesInfoAddTotalAndQuantity(Guid guid, int quantity, decimal total);
        void DealSalesInfoMinusTotalAndQuantity(Guid key, int quantity, decimal total);
        void DealSalesInfoRefresh(Guid bid);
        void DealSalesInfoRefresh();
        void DealSalesInfoRefreshForSkm(Guid bid);
        void ExecSpRefreshDealSalesInfoForSkm(Guid bid);

        void DealSalesInfoDailyAddTotal(Guid bid, int total);
        void DealSalesInfoDailyAddTotalAndQuantity(Guid bid, int quantity, int total);

        #endregion

        #region DealCost

        void DealCostSet(DealCost dCost);

        void DealCostSetList(DealCostCollection dCostList);

        DealCost DealCostGet(int id);

        DealCostCollection DealCostGetList(Guid businessHourGuid);

        DealCostCollection DealCostGetList(IEnumerable<Guid> businessHourGuid);

        void DealCostDelete(int id);

        void DealCostDeleteListByBid(Guid bid);

        decimal DealCostGetByQty(Guid bid, int qty);

        #endregion DealCost

        #region Faq

        FaqCollection FaqCollectionGet();

        void FaqSort(int id, int sequence);

        void FaqSet(Faq faq);

        Faq FaqGet(int id);

        void FaqDelete(int id);

        FaqCollection FaqCollectionGetByParentId(int pid);

        #endregion Faq

        #region ViewHamiDealProperty

        ViewHamiDealProperty ViewHamiDealPropertyGet(Guid bid);

        #endregion ViewHamiDealProperty

        #region ViewPponStore

        /// <summary>
        /// 依據傳入的日期區間，取得排程資料(DealTimeSolt)中，起始日期位於此區間中的Deal的ViewPponStore資料。
        /// 使用NoLock關鍵字，所以資料也許不是最新的。
        /// </summary>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <returns></returns>
        ViewPponStoreCollection ViewPponStoreGetListByDayWithNoLock(DateTime sDate, DateTime eDate);

        /// <summary>
        /// 依據BusinessHourGuid與StoreGuid查詢ViewPponStore的資料
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="storeGuid"></param>
        /// <returns></returns>
        ViewPponStore ViewPponStoreGetByBidAndStoreGuid(Guid bid, Guid storeGuid);

        /// <summary>
        /// 依據傳入的bid，取得ViewPponStore資料。
        /// 使用NoLock關鍵字，所以資料也許不是最新的。
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        ViewPponStoreCollection ViewPponStoreGetListByBidWithNoLock(Guid bid);

        ViewPponStoreCollection ViewPponStoreGetListByBidWithNoLock(Guid bid, VbsRightFlag vbsRight);

        ViewPponStoreCollection ViewPponStoreGetListByBidListWithNoLock(List<Guid> bidList);

        ViewPponStoreCollection ViewPponStoreGetListWithNoLock(params string[] filter);

        ViewPponStoreCollection ViewPponStoreGetStoreGuidWithSales(Guid storeGuid);

        #endregion ViewPponStore

        #region ViewOrderStore

        /// <summary>
        /// 依據傳入的日期區間，取得排程資料(DealTimeSolt)中，起始日期位於此區間中的Deal的PponStore資料。
        /// 使用NoLock關鍵字，所以資料也許不是最新的。
        /// </summary>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <returns></returns>
        ViewOrderStoreQuantityCollection ViewOrderStoreQuantityGetListByDayWithNoLock(DateTime sDate, DateTime eDate);

        /// <summary>
        /// 取得今日on檔資料
        /// (此為調校ViewOrderStoreQuantity過後的版本)
        /// </summary>
        /// <returns></returns>
        ViewOrderStoreQuantityTodayCollection ViewOrderStoreQuantityGetListByToday();

        #endregion ViewOrderStore


        #region PponStore
        ProposalStoreCollection ProposalStoreGetListByProposalId(int pid);
        ProposalStore ProposalStoreGet(int proposalId, Guid storeGuid);

        bool ProposalStoreSet(ProposalStore store);
        bool ProposalStoreSetList(ProposalStoreCollection stores);
        bool ProposalStoreDeleteList(ProposalStoreCollection stores);

        bool ProposalStoreDelete(int pid);

        #endregion

        #region PponStore

        PponStore PponStoreGet(Guid bid, Guid storeGuid);

        PponStore PponStoreGet(Guid bid, Guid storeGuid, VbsRightFlag vbsRight);

        PponStoreCollection PponStoreGetListByBusinessHourGuid(Guid bid);

        PponStoreCollection PponStoreGetListByResourceGuids(IEnumerable<Guid> resourceGuids);

        bool PponStoreSet(PponStore store);

        void PponStoreUpdateOrderQuantity(Guid bid, Guid storeGuid, int orderQuantity, bool isResetOrderQuantity = false);

        void PponStoreUpdateOrderQuantity(Guid bid, Guid storeGuid, int orderQuantity, int vbsRight);

        bool PponStoreSetList(PponStoreCollection stores);

        bool PponStoreDeleteList(PponStoreCollection stores);

        void PponStoreDeleteByBid(Guid bid);
        #endregion PponStore

        #region ViewPponCashTrustLog

        /// <summary>
        /// 取指定檔次的 cash_trust_log 及相關狀態資料
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        ViewPponCashTrustLogCollection ViewPponCashTrustLogGetList(Guid bid);

        /// <summary>
        ///
        /// </summary>
        /// <param name="oid"></param>
        /// <returns></returns>
        ViewPponCashTrustLogCollection ViewPponCashTrustLogGetListByOrderGuid(Guid oid);

        /// <summary>
        /// 取出指定 coupon序號之相關狀態資料
        /// </summary>
        /// <param name="couponSequenceNumber"></param>
        /// <returns></returns>
        ViewPponCashTrustLogCollection ViewPponCashTrustLogGet(string couponSequenceNumber);

        /// <summary>
        /// 取出指定檢查碼之相關狀態資料
        /// </summary>
        /// <param name="couponSequenceNumber"></param>
        /// <returns></returns>
        ViewPponCashTrustLogCollection ViewPponCashTrustLogGetByCode(string CodeNumber);
        
        /// <summary>
        /// 撈取天貓檔次憑證信託核銷資料
        /// </summary>
        /// <param name="couponSequenceNumber">憑證號碼</param>
        /// <returns></returns>
        ViewPponCashTrustLogCollection ViewPponCashTrustLogGetTmall(string couponSequenceNumber);

        /// <summary>
        /// 取出指定 trust_id之相關狀態資料
        /// </summary>
        /// <param name="trustId"></param>
        /// <returns></returns>
        ViewPponCashTrustLog ViewPponCashTrustLogGet(Guid trustId);

        /// <summary>
        /// 以rsrc及時間區間查詢訂單折扣後金額
        /// </summary>
        /// <param name="srcId"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <returns></returns>
        List<OrderDiscount> OrderDiscountListGetByReferenceIdAndTime(string srcId, DateTime starttime, DateTime endtime);


        #endregion ViewPponCashTrustLog

        #region VerificationStatisticsLog

        int VerificationStatisticsLogInsert(VerificationStatisticsLog vslog, Guid bid, string createId);

        VerificationStatisticsLog VerificationStatisticsLogGetById(int id);

        VerificationStatisticsLog VerificationStatisticsLogGetRecordByGuid(Guid bid, VerificationStatisticsLogStatus status);

        /// <summary>
        /// 商家後臺_對帳查詢:根據輸入之對帳單編號抓取對帳單對應之累計核銷統計資料
        /// </summary>
        /// <param name="balanceSheetId"></param>
        /// <returns></returns>
        VerificationStatisticsLog VerificationStatisticsLogGetListByBalanceSheetId(int balanceSheetId);

        bool VerificationStatisticsLogDelete(int vslId);

        #endregion VerificationStatisticsLog

        #region ComboDeals

        ComboDealCollection GetComboDealByBid(Guid bid, bool ismain = true);

        ComboDealCollection GetComboDealByBid(IEnumerable<Guid> bids);

        ComboDealCollection GetComboDealToday(bool ismain);

        ViewComboDealCollection GetViewComboDealToday();

        ViewComboDealCollection GetViewComboDealByBid(Guid bid, bool ismain = true);

        ViewComboDealCollection GetViewComboDealAllByBid(Guid bid);

        ComboDeal GetComboDeal(Guid bid);

        void DeleteComboDeal(int id, Guid bid);

        void DeleteComboDeal(Guid mainbid);

        void AddComboDeal(ComboDeal deal);

        #endregion ComboDeals

        #region EventPromo

        void SavePromoSeoKeyword(PromoSeoKeyword seokeyword);
        void DeletePromoSeoKeyword(PromoSeoKeyword seoKeyword);
        PromoSeoKeyword GetPromoSeoKeyword(int promoType, int activityId);
        int SaveEventPromo(EventPromo promo);



        EventPromo GetEventPromo(int id);



        EventPromo GetEventPromo(string url, EventPromoType type);

        EventPromo GetEventPromo(string url, EventPromoTemplateType type);

        EventPromoCollection GetEventPromoList(EventPromoTemplateType type);

        EventPromoCollection GetEventPromoList(EventPromoType type);

        EventPromoCollection EventPromoGetList(params string[] filter);

        EventPromoCollection GetEventPromoListByDiscountCampaignId(int discountCampaignId);

        EventPromoCollection EventPromoGetListByDiscountCampaignId(int campaignId);

        EventPromoCollection EventPromoOnGoingList(bool isBindCategory, bool checkIsShowInWeb, bool checkIsShowInApp);

        void UpdateEventPromoStatus(int id);

        void UpdateEventPromoItemStatus(int id);

        #endregion EventPromo

        #region MerchantEventPromo
        MerchantEventPromo MerchantEventPromoGet(int id);
        MerchantEventPromoCollection MerchantEventPromoGet(Guid guid);
        MerchantEventPromoCollection MerchantEventPromoListGet(MerchantEventType type);
        MerchantEventPromoCollection MerchantEventPromoListGet(int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter);
        MerchantEventPromoCollection MerchantEventPromoListGetToday(MerchantEventType type);
        void MerchantEventPromoSet(MerchantEventPromo promo);
        void MerchantEventPromoDelete(int id);
        #endregion MerchantEventPromo

        #region EventPromoBindCategoryList

        #endregion

        #region ViewEventPromo

        ViewEventPromoCollection GetViewEventPromoList(string url, bool? status);

        /// <summary>
        /// 以商品主題活動的Url取出參加該活動的團購檔次資料，以LIST傳入多個URL字串
        /// </summary>
        /// <param name="urls"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        ViewEventPromoCollection ViewEventPromoGetList(List<string> urls, bool? status);

        ViewEventPromoCollection ViewEventPromoGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        ViewEventPromoCollection ViewEventPromoGetList(int eventId, bool? status = null);

        #endregion ViewEventPromo

        #region ViewEvnetPromoItem

        ViewEventPromoItemCollection GetViewEventPromoItemList(int mainid, EventPromoItemType type);
        ViewEventPromoItemCollection GetViewEventPromoItemListByBid(Guid bid);

        #endregion ViewEvnetPromoItem

        #region EvnetPromoItem

        void SaveEventPromoItem(EventPromoItem item);

        void SaveEventPromoItemList(EventPromoItemCollection items);

        EventPromoItem GetEventPromoItem(int id);

        EventPromoItem GetEventPromoItem(int mainId, int itemId);

        EventPromoItem GetEventPromoItem(int mainId, int itemId, string category, string subCategory);

        EventPromoItemCollection GetEventPromoItemList(int mainId);

        EventPromoItemCollection GetEventPromoItemList(int mainid, EventPromoItemType type);

        EventPromoItemCollection EventPromoItemGetList(params string[] filter);

        EventPromoItemCollection EventPromoItemGetListByMainUrls(List<string> urls, DateTime itemStartDate, DateTime itemEndDate);

        /// <summary>
        /// 依據EventPromo的url回傳此商品活動擁有的item數量
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="itemType"></param>
        /// <returns></returns>
        int EventPromoItemGetListCount(int eventId, EventPromoItemType itemType);

        /// <summary>
        /// 依據EventPromo Id回傳此商品活動擁有的item數量
        /// </summary>
        /// <param name="mainId"></param>
        /// <returns></returns>
        int EventPromoItemGetListCount(int mainId);

        /// <summary>
        /// 刪除某筆EventPromoItem的資料
        /// </summary>
        /// <param name="item">要刪除的資料</param>
        /// <returns></returns>
        void DeleteEventPromoItem(EventPromoItem item);

        #endregion EvnetPromoItem

        #region ViewEventPromoItemPicVote

        ViewEventPromoItemPicVoteCollection ViewEventPromoItemPicVoteGet(int mainId);

        #endregion

        #region ViewEventPromoVourcher
        /// <summary>
        /// 以商品主題活動的Url取出參加該活動的優惠券資料
        /// </summary>
        /// <param name="url">活動URL字串(EventPromo.Url)</param>
        /// <param name="status"></param>
        /// <returns></returns>
        ViewEventPromoVourcherCollection ViewEventPromoVourcherGetList(string url, bool? status);
        /// <summary>
        /// 以商品主題活動的Url取出參加該活動的優惠券資料，以LIST傳入多個URL字串
        /// </summary>
        /// <param name="urls"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        ViewEventPromoVourcherCollection ViewEventPromoVourcherGetList(List<string> urls, bool? status);

        ViewEventPromoVourcherCollection ViewEventPromoVourcherGetList(int pageStart, int pageLength, string orderBy, params string[] filter);

        #endregion ViewEventPromoVourcher

        #region ViewEventPromoCategory

        ViewEventPromoCategoryCollection ViewEventPromoCategoryGetList(List<int> categoryIds);

        #endregion

        #region ViewEventPromoItemDealId

        ViewEventPromoItemDealIdCollection ViewEventPromoItemDealIdGetListByEventPromoId(List<int> eventPromoId, string dealKey);

        #endregion

        #region Brand
        BrandCollection BrandGetList();
        BrandCollection BrandGetList(params string[] filter);
        BrandCollection BrandGetByBrandIdList(List<int> brandIdlist);
        Brand GetBrand(int id);
        Brand GetBrand(string url);
        int SaveBrand(Brand brand);
        void UpdateBrandStatus(int id);
        BrandItem GetBrandItem(int id);
        BrandItem GetBrandItem(int mainId, int itemId);
        void SaveBrandItemList(BrandItemCollection items);
        void UpdateBrandItemStatus(int id);
        void UpdateSeqStatus(int id);
        void DeleteBrandItem(BrandItem item);
        BrandItemCollection GetBrandItemList(int mainId);
        void SaveBrandItemCategory(BrandItemCategory brandItemCategory);
        void SaveBrandItemCategoryList(BrandItemCategoryCollection itemCategorys);
        BrandItemCategoryCollection GetBrandItemCategoryByName(string bicName, int brandId);
        BrandItemCategoryCollection GetBrandItemCategoryByBrandId(int brandId);
        int BrandItemCategoryGetMaxSequenceByBrandId(int brandId);
        void DeleteBrandItemCategoryByBrandId(int brandId);
        void DeleteBrandItemCategoryByCategoryId(int brandId, int categoryId);
        void DeleteBrandItemCategoryDependency(int brandItemId);
        void DeleteBrandItemCategoryDependencyBybicId(int bicId, int brandItemId);
        void SaveBrandItemCategoryDependency(BrandItemCategoryDependency bicd);
        ViewBrandCollection ViewBrandCollectionByBrandId(int brandId);
        BrandCollection GetBrandListByDiscountCampaignId(int discountCampaignId);
        #endregion Brand

        #region 策展列表

        ThemeCurationMain GetThemeCurationMainById(int id);
        ThemeCurationMainCollection GetAllThemeCurationMainCollection();
        int SaveThemeCurationMain(ThemeCurationMain data);
        ThemeCurationMainCollection GetNowThemeCurationMainCollection();

        ThemeCurationGroup GetThemeCurationGroupByGroupId(int groupId);
        ThemeCurationGroupCollection GetThemeCurationGroupCollectionByMainId(int mainId);
        int SaveThemeCurationGroup(ThemeCurationGroup data);
        int SaveAllThemeCurationGroupCollection(ThemeCurationGroupCollection data);
        void DeleteThemeCurationGroupByGroupId(int groupId);
        int GetThemeCurationGroupNewSeq(int mainId);

        ThemeCurationItemCollection GetThemeCurationItemCollectionByGroupId(int groupId);
        ThemeCurationItem GetThemeCurationItemByItemId(int itemId);
        int SaveThemeCurationItem(ThemeCurationItem data);
        int SaveAllThemeCurationItemCollection(ThemeCurationItemCollection data);
        void DeleteThemeCurationItemByItemId(int itemId);
        void DeleteThemeCurationItemByGroupId(int groupId);

        ViewThemeCurationCollection GetViewThemeCurationByMainId(int mainId);
        ViewThemeCurationCollection GetViewThemeCurationByCurationId(int brandId);

        #endregion

        #region ViewBrandItem、ViewBrandCategory
        ViewBrandItemCollection GetViewBrandItemList(int mainid);
        ViewBrandItemCollection GetViewBrandItemListByBid(Guid bid);
        ViewBrandItemCollection GetViewBrandItemListbySearch(int brandId, string itemName, int categoryId);
        ViewBrandCategoryCollection GetViewBrandCategoryByBrandItemId(int brandItemId);
        ViewBrandCategoryCollection GetViewBrandCategoryByBrandId(int brandId);

        #endregion ViewBrandItem、ViewBrandCategory

        #region CategoryDeals
        Dictionary<Guid, List<int>> CategoryDealsGetListInEffectiveTime();
        CategoryDealCollection CategoryDealsGetList(Guid bid);
        CategoryCollection CategoryDealsTypeGetList(Guid bid);
        List<int> CategoryIdGetListByBid(Guid bid);
        CategoryDealCollection CategoryDealsGetList(List<Guid> bids);
        /// <summary>
        /// 依據檔次上檔區間，查詢此區間內的CategoryDeal資料
        /// </summary>
        /// <param name="dateStart">檔次起始時間</param>
        /// <param name="dateEnd">檔次截止時間</param>
        /// <param name="theType">查詢附載平衡設定</param>
        /// <returns></returns>
        CategoryDealCollection CategoryDealsGetList(DateTime dateStart, DateTime dateEnd);

        CategoryDealCollection CategoryDealsGetList(DateTime dateStart, DateTime dateEnd, CategoryType type);

        void CategoryDealSave(CategoryDealCollection cds, Guid bid);

        bool CategoryDealSetList(CategoryDealCollection categories);

        ViewCategoryDealCollection ViewCategoryDealGetList(CategoryType type);

        ViewCategoryDealCollection ViewCategoryDealGetCidListByBid(int cityId, CategoryType type, Guid bid);

        void DeleteCategoryDealsByCategoryType(Guid bid, CategoryType type);

        void UpdateCategoryDealsWithCategoryList();

        int GetCategoryDealCountByBidAndCid(Guid bid, int cid);

        int GetCategoryDealCountByBidAndCidString(Guid bid, string cidstring);

        void SaveCategoryDeal(CategoryDealCollection cdc);

        void RemoveCategoryDeal(CategoryDealCollection cdc);

        Dictionary<Guid, List<int>> DealCategoriesDictGetAll();

        #endregion CategoryDeals

        #region ViewCategoryDeals

        ViewCategoryDealCollection ViewCategoryDealGetList();

        #endregion

        #region ViewCategoryDealBase
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="theType"></param>
        /// <returns></returns>
        ViewCategoryDealBaseCollection ViewCategoryDealBaseGetList(DateTime dateStart, DateTime dateEnd);

        ViewCategoryDealBaseCollection ViewCategoryDealBaseGetListByBid(Guid bid);
        ViewCategoryDealBaseCollection ViewCategoryDealBaseGetListByBids(List<Guid> bids);
        #endregion ViewCategoryDealBase

        #region CategoryDealList
        ViewCategoryDealListCollection GetCategoryDealListByCidAndOrderTime(int cid, string dateS, string dateE);

        ViewCategoryDealListCollection GetCategoryDealListByBidAndCidAndOrderTime(string bidstring, int cid, string dateS, string dateE);

        ViewCategoryDealListCollection GetCategoryDealListByBidAndCidOutsideOfOrdertime(string bidstring, int cid, string dateS, string dateE);
        #endregion

        #region CategoryDependency

        CategoryDependencyCollection GetCategoryDependencyByParentId(int parentid);
        int GetCategoryDependencyParentIdByCategoryId(int categoryid);
        bool CategoryFinalNodeSet(int cid);
        int GetCategoryDependencyParentIdByCidAndBid(int categoryid, Guid bid);
        int[] GetCategoryDependencyPidByCid(int cid);
        IList<CategoryDependency> GetAllCategoryDependencies();

        #endregion

        #region ViewAllDealCategory

        ViewAllDealCategoryCollection ViewAllDealCategoryGetListByBid(Guid bid);
        ViewAllDealCategoryCollection ViewAllDealCategoryGetListByBidList(List<Guid> bids);

        #endregion ViewAllDealCategory

        #region CategoryVourcher

        CategoryVourcherCollection CategoryVourcherGetList(int vourcherId);

        CategoryVourcherCollection CategoryVourcherGetList(DateTime dateStart, DateTime dateEnd, CategoryType type);

        void CategoryVourcherSave(CategoryVourcherCollection cvs, int vourcherId);

        #endregion CategoryVourcher

        #region hami_category

        HamiCategoryCollection HamiCategoryGetList();

        #endregion hami_category

        #region ViewPponBusinessHourGuid

        ViewPponBusinessHourGuidCollection ViewPponBusinessHourGuidGetList(DateTime thedate);

        /// <summary>
        /// 取得某個區間內開檔的檔次
        /// </summary>
        /// <param name="startDate">起始時間</param>
        /// <param name="endDate">結束時間</param>
        /// <returns>ViewPponBusinessHourGuidCollection</returns>
        ViewPponBusinessHourGuidCollection ViewPponBusinessHourGuidGetList(DateTime startDate, DateTime endDate);

        #endregion ViewPponBusinessHourGuid

        #region ViewPponDealStore

        /// <summary>
        /// 商家後臺_核銷查詢:根據輸入之查詢條件取出已成檔之憑證檔次、賣家及分店資料
        /// </summary>
        /// <param name="storeGuidList">具檢視權限之分店Guid list 以,連結多筆資料</param>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewPponDealStoreCollection ViewPponDealStoreGetToShopDealList(string storeGuidList, params string[] filter);

        #endregion ViewPponDealStore

        #region ViewVbsToShopDeal

        /// <summary>
        /// 商家後臺_出貨管理:根據輸入之查詢條件取出已成檔之憑證檔次及賣家資料
        /// </summary>
        /// <param name="queryOption">查詢欄位選項</param>
        /// <param name="queryKeyword">查詢關鍵字</param>
        /// <param name="queryStartTime">查詢時間區間起</param>
        /// <param name="queryEndTime">查詢時間區間訖</param>
        /// <returns></returns>
        ViewVbsToShopDealCollection ViewVbsToShopDealGetList(
                string selQueryOption, string queryKeyword, DateTime? queryStartTime, DateTime? queryEndTime
        );

        #endregion ViewVbsToShopDeal

        #region ViewVbsToHouseDeal

        /// <summary>
        /// 商家後臺_出貨管理:根據輸入之查詢條件取出已成檔之宅配檔次及賣家資料
        /// </summary>
        /// <param name="queryOption">查詢欄位選項</param>
        /// <param name="queryKeyword">查詢關鍵字</param>
        /// <param name="queryStartTime">查詢時間區間起</param>
        /// <param name="queryEndTime">查詢時間區間訖</param>
        /// <returns></returns>
        ViewVbsToHouseDealCollection ViewVbsToHouseDealGetList(string queryOption, string queryKeyword, DateTime? queryStartTime, DateTime? queryEndTime);

        /// <summary>
        /// 商家後臺_出貨管理:根據輸入之查詢條件取出已成檔之宅配檔次及賣家資料
        /// </summary>
        /// <param name="queryOption">查詢欄位選項</param>
        /// <param name="queryKeyword">查詢關鍵字</param>
        /// <param name="queryTimeOption">查詢時間區間選擇(上檔區間/兌換區間)</param>
        /// <param name="queryStartTime">查詢時間區間起</param>
        /// <param name="queryEndTime">查詢時間區間訖</param>
        /// <returns></returns>
        ViewVbsToHouseDealCollection ViewVbsToHouseDealGetList(string queryOption, string queryKeyword, string queryTimeOption, DateTime? queryStartTime, DateTime? queryEndTime);

        /// <summary>
        /// 商家後臺_出貨管理:根據輸入之productGuid取出已成檔之宅配檔次及賣家資料
        /// </summary>
        /// <param name="productGuids">多筆 productGuid</param>
        /// <returns></returns>
        ViewVbsToHouseDealCollection ViewVbsToHouseDealGetListByProductGuid(IEnumerable<Guid> productGuids);


        /// <summary>
        /// 商家後臺_對帳查詢:根據輸入之對帳單編號抓取對帳單對應之累計核銷統計資料
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>

        DataTable BalanceSheetDeliverSubInfoGetTable(IEnumerable<Guid> productGuids);


        #endregion ViewVbsToHouseDeal

        #region New EDM


        EdmMainCollection GetEdmMainByDate(DateTime deliverydate, EdmMainType edmtype, bool status);

        EdmMain GetEdmMainByCityDate(int cityid, DateTime deliverydate, EdmMainType edmtype, bool status);

        EdmMainCollection GetEdmMainByCityDates(int cityid, DateTime deliverydateS, DateTime deliverydateE, EdmMainType edmtype, bool status);
        /// <summary>
        /// 取得未上傳的當日Edm主檔
        /// </summary>
        /// <param name="deliverydate"></param>
        /// <param name="edmtype"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        List<EdmMain> GetNewEdmMainListByDateWasNotUploaded(DateTime deliverydate, EdmMainType edmtype, bool status);

        EdmMainCollection GetEdmMainByCity(int cityid, EdmMainType edmtype, bool status);

        EdmMain GetEdmMainById(int id);

        EdmMain GetEdmMainByCityId(int cid);

        void SaveEdmMain(EdmMain edmmain);

        EdmAreaDetailCollection GetEdmAreaCollectionByPid(int pid);

        EdmDetailCollection GetEdmDetailCollectionByPid(int pid);

        void SaveEdmDetail(EdmDetail edmdetail);

        void SaveEdmAreaDetail(EdmAreaDetail edmdetail);

        void DeleteEdmDetailByPid(int pid);

        void DeleteEdmAreaDetailByPid(int pid);

        void DisableEdmMain(int id, bool enable);

        #endregion New EDM

        #region Solo EDM
        SoloEdmMainCollection GetSoloEdmMainByType(int type);
        SoloEdmMainCollection GetSoloEdmMainByDate(DateTime sendDate);
        SoloEdmMain GetSoloEdmMainById(int eid);
        SoloEdmBrandCollection GetSoloEdmBrandById(int eid);
        SoloEdmPponCollection GetSoloEdmPponById(int eid);
        SoloEdmSendUserCollection GetSoloEdmSendUserById(int eid);
        int SoloEdmSendCountGetByMainId(int mainId);
        void SaveSoloEdmMain(SoloEdmMain soloedm);
        void SaveSoloEdmBrand(SoloEdmBrand soloEdmBrand);
        void SaveSoloEdmPpon(SoloEdmPpon soloEdmPpon);
        void SaveSoloEdmSendUser(SoloEdmSendUser sendUser);
        void SoloEdmSetAllMembers(int MainId);
        void DeleteSoloEdmBrandByMainId(int MainId);
        void DeleteSoloEdmPponByMainId(int MainId);
        void DeleteSoloEdmSendUserByMainId(int mainId);
        #endregion

        #region Solo EDM
        EdmOpenLog GetEdmLogByEmail(int id);
        void SaveEdmOprnLog(EdmOpenLog edmOpenLog);
        #endregion

        #region EdmHotDeal

        void EdmHotDealSet(EdmHotDeal edmHotDeal);

        void EdmHotDealCollectionSet(EdmHotDealCollection edmHotDeals);

        EdmHotDealCollection EdmHotDealGet();

        EdmHotDealCollection EdmHotDealGetByBid(Guid bid, DateTime startDate);

        EdmHotDeal EdmHotDealGetById(int id);

        DataTable EdmHotDealByCityIdDate(int cityid, string startDate);

        DataTable EdmPriorityDealByCityIdDate(int cityid, string startDate);

        DataTable EdmPriorityDealByCityIdDate24(int cityid, string startDate);

        #endregion

        #region NewCpa

        void CpaMainSet(CpaMain main);

        CpaMain CpaMainGetById(int id);

        void CpaDetailSet(CpaDetail detail);

        /// <summary>
        /// 依時間區間抓取CpaMain列表
        /// </summary>
        /// <param name="startdate">cpa起始時間</param>
        /// <param name="enddate">cpa結束時間</param>
        /// <returns></returns>
        CpaMainCollection CpaMainGetListByDate(DateTime startdate, DateTime enddate);

        /// <summary>
        /// 依cpa代碼或名稱關鍵字抓取列表
        /// </summary>
        /// <param name="codename">cap代碼或名稱</param>
        CpaMainCollection CpaMainGetListByCodeName(string codename);

        /// <summary>
        /// 抓取目前運行的cpa列表
        /// </summary>
        /// <param name="now">目前時刻</param>
        /// <returns></returns>
        CpaMainCollection CpaMainGetListByCurrent(DateTime now);

        #endregion NewCpa

        #region ChangeLog

        //first designed for logging ViewPponDeal, pass "ViewPponDeal" for the parameter 'changeName'
        int ChangeLogInsert(string changeName, string keyValue, string content, bool enabled = true);

        /// <summary>
        /// 抓取最後一筆異動紀錄
        /// </summary>
        /// <param name="change_object_name">Table的名稱</param>
        /// <param name="key_val">Pkey</param>
        /// <param name="enabled">有效與否</param>
        /// <returns></returns>
        ChangeLog ChangeLogGetLatest(string change_object_name, string key_val, bool enabled);

        /// <summary>
        /// 更新ChangeLog欄位Eanbled
        /// </summary>
        /// <param name="change_object_name">Table名稱</param>
        /// <param name="key_val">Pkey</param>
        /// <param name="enabled">有效與否</param>
        void ChangeLogUpdateEnabled(string change_object_name, string key_val, bool enabled);

        /// <summary>
        /// 抓取異動紀錄列表
        /// </summary>
        /// <param name="change_object_name">Table名稱</param>
        /// <param name="key_val">Pkey</param>
        /// <returns></returns>
        ChangeLogCollection ChangeLogCollectionGetAll(string change_object_name, string key_val);

        #endregion ChangeLog

        #region vacation

        void SaveVacation(Vacation vacation);

        void DeleteVacation(int id);

        Vacation GetTodayVacation();

        Vacation GetVacationByDate(DateTime date);

        VacationCollection GetVacationListByPeriod(DateTime dateTimeStart, DateTime dateTimeEnd);

        #endregion vacation

        #region ViewPponExpiration

        ViewPponExpirationCollection ViewPponExpirationGetList(Guid bid);

        /// <summary>
        /// 依據使用時間查出所有還可以使用的檔 (ViewPponExpiration).
        /// 可提供 endBuffer 放寬搜尋檔次的使用期限.
        /// </summary>
        /// <param name="usageTime">使用時間</param>
        /// <param name="endBuffer">緩衝天數, 加在使用期限達到放寬的效果, 增加搜尋出來的檔次</param>
        /// <returns></returns>
        ViewPponExpirationCollection ViewPponExpirationGetList(DateTime usageTime, int endBuffer = 0);

        /// <summary>
        /// 依據使用時間查出所有可產商品對帳單的檔 (ViewPponExpiration).
        /// </summary>
        /// <param name="usageTime">使用時間</param>
        /// <returns></returns>
        ViewPponExpirationCollection ViewPponExpirationGetList(DateTime usageTime);

        /// <summary>
        /// 依據使用時間查出所有可產商品對帳單的檔 (ViewPponExpiration).
        /// </summary>
        /// <param name="usageTime">使用時間</param>
        /// <param name="deliveryBuffer">開始配送日+商品鑑賞期為開始產商品對帳單</param>
        /// <param name="endBuffer">退換貨完成日+緩衝時間為撈取應產商品對帳單檔次期限</param>
        /// <returns></returns>
        ViewPponExpirationCollection ViewPponExpirationGetList(DateTime usageTime, int deliveryBuffer, int endBuffer);

        #endregion ViewPponExpiration

        #region ViewDealPropertyBusinessHourContent

        ViewDealPropertyBusinessHourContent ViewDealPropertyBusinessHourContentGetByBid(Guid bid);

        ViewDealPropertyBusinessHourContentCollection ViewDealPropertyBusinessHourContentGetList(IEnumerable<Guid> bids);

        ViewDealPropertyBusinessHourContentCollection ViewDealPropertyBusinessHourContentGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter);

        ViewDealPropertyBusinessHourContentCollection ViewDealPropertyBusinessHourContentGetToHouseDealList(string queryOption, string queryKeyword,
                                                        string queryDateType, DateTime? queryStartTime, DateTime? queryEndTime,
                                                        RemittanceType? remittanceType, IEnumerable<string> columnNull, bool hasVendorPaymentChange);

        #endregion ViewDealPropertyBusinessHourContent

        #region FamiApiLog

        void FamiApiLogSet(FamiApiLog apiLog);

        #endregion

        #region Famiport

        void FamiportUpdateEventId(int famiportId, PeztempServiceCode serviceCode);

        void FamiportUpdate(Famiport fp);

        void FamiportSet(Famiport fp);

        int FamiportUpdateBid(int famiportId, Guid guid);

        Famiport FamiportGet(int id);

        void FamiportDelete(int id);

        Famiport FamiportGet(string colName, object obj_value);

        FamiportCollection FamiportGetList(int pageStart, int pageLength, string orderByDesc, params string[] filter);

        FamiportCollection FamiportGetList(int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter);

        FamiportCollection FamiportGetListByDate(DateTime date);

        int FamiportGetCount(params string[] filter);

        #endregion Famiport

        #region ExternalDeal

        void ExternalDealSet(ExternalDeal deal);
        void ExternalDealCollectionSet(ExternalDealCollection deal);
        ExternalDeal ExternalDealGet(Guid guid);
        ExternalDeal ExternalDealGet(string skuid);
        ExternalDealCollection ExternalDealGetList(int pageStart, int pageLength, string orderByDesc, params string[] filter);
        ExternalDealCollection ExternalDealGetList(int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter);
        ExternalDealCollection ExternalDealGetListByDate(DateTime date);
        ExternalDealRelationStoreCollection ExternalDealRelationStoreGetListByDeal(Guid guid);
        ViewExternalDealCollection ViewExternalDealGetListBySkuId (string skuId);
        ViewExternalDealCollection ViewExternalDealGetListBySellerGuid(Guid sellerGuid, int? shopCode, string searchTitle);
        ViewExternalDealCollection ViewExternalDealGetListByBid(List<Guid> bids);
        ViewExternalDealCollection ViewExternalDealGetListByPrizeDeal(string searchTitle);
        ViewExternalDealCollection ViewExternalDealGetListByDealrGuid(Guid dealGuid);
        ViewExternalDeal ViewExternalDealGetListByDealGuid(Guid dealGuid, Guid sellerGuid);
        ViewExternalDeal ViewExternalDealGetByBid(Guid bid, bool isAvailable = true);
        ViewExternalDealCollection ViewExternalDealGetListByBid(Guid bid);
        ViewExternalDealCollection ViewExternalDealGetListByNotExpired();
        ViewExternalDealCollection ViewExternalDealGetList(string storeCode);
        void ExternalDealRelationStoreDeleteByDeal(Guid guid);
        void ExternalDealRelationStoreDeleteByStores(ExternalDealRelationStoreCollection stores);
        void ExternalDealRelationStoreCollectionSet(ExternalDealRelationStoreCollection stores);
        ExternalDealRelationItemCollection ExternalDealItemGetByBid(Guid bid);
        ExternalDealRelationItemCollection ExternalDealRelationItemGetListByDealGuid(Guid dealGuid);
        void ExternalDealRelationItemDeleteByItems(ExternalDealRelationItemCollection items);
        void ExternalDealRelationItemCollectionSet(ExternalDealRelationItemCollection items);
        #endregion

        #region Famipos

        FamiposBarcode FamiposBarcodeGet(string tranNo);

        FamiposBarcode FamiposBarcodeGet(int peztempId);

        FamiposBarcode FamiposBarcodeGetCreate(string tranNoHeader, Guid orderDetailGuid, Guid businessHourGuid, IViewPponDeal vpd);

        bool FamiposBarcodeSet(FamiposBarcode fami);

        #endregion

        #region View_Fami_Pay_Log

        List<Tuple<string, int>> FamiUsageQuery(DateTime queryStart, DateTime queryEnd, bool day, bool hour);

        #endregion

        #region DailyFamiportItemTriggerReport

        void DailyFamiportItemTriggerReportSet(DailyFamiportItemTriggerReport dailyFamiportItemTriggerReport);
        DailyFamiportItemTriggerReportCollection DailyFamiportItemTriggerReportGetListByDate(bool isPager, int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter);

        #endregion DailyFamiportItemTriggerReport

        #region DailyBeaconStoreEffectReport

        void DailyBeaconStoreEffectReportSet(DailyBeaconStoreEffectReport dailyBeaconStoreEffectReport);
        DailyBeaconStoreEffectReportCollection DailyBeaconStoreEffectReportGetListByDate(bool isPager, int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter);

        #endregion DailyBeaconStoreEffectReport

        #region YahooProperty
        void YahooPropertySet(YahooProperty yp);
        int YahooPropertySetList(YahooPropertyCollection yps);
        YahooProperty YahooPropertyGet(int id);
        YahooProperty YahooPropertyGet(string column, object value);
        YahooPropertyCollection YahooPropertyGetListByBids(List<Guid> bids);
        void YahooPropertyDelete(int id);
        YahooPropertyCollection YahooPropertyGetList();
        void YahooPropertyActionUpdate(int id, string user_name, DateTime modify_time);
        void YahooPropertyActionListUpdate(List<int> ids, string user_name, DateTime modify_time);
        void YahooPropertyActionUpdateAll(string user_name, DateTime modify_time);
        void YahooPropertyUpdate(int id, int? sortN, int? sortC, int? sortS);
        #endregion

        #region PponOption

        PponOption PponOptionGet(int id);
        string PponOptionGetIdByGuid(Guid gid);
        PponOption PponOptionGetByGuid(Guid gid);
        int PponOptionSet(PponOption option);
        PponOptionCollection PponOptionGetList(Guid bid);
        PponOptionCollection PponOptionGetList(Guid bid, bool enabledOnly);
        List<Guid> PponOptionGetItemGuidListByBids(List<Guid> bids);
        List<Guid> PponOptionGetItemGuidListByBid(Guid bid);
        Dictionary<Guid, List<Guid>> PponOptionGetItemGuidDictByOnlineDeals();

        int PponOptionSetList(PponOptionCollection options);
        int? PponOptionGetIdByAccessory(Guid acc_grp_mem_guid);
        int? PponOptionGetIdByAccessoryAndBid(Guid acc_grp_mem_guid, Guid bid);
        List<Guid> PponOptionGetOverGroup();

        #endregion

        #region PponOptionLog
        PponOptionLog PponOptionLogGet(int id);
        PponOptionLogCollection PponOptionLogGet(Guid bid);
        int PponOptionLogSetList(PponOptionLogCollection options);
        #endregion

        #region ViewFamiDealDeliverTime

        ViewFamiDealDeliverTimeCollection GetViewFamiDealDeliverTimeByBeforeDay(int dayNumber);

        #endregion

        #region FamiportPeztempLink

        int FamiportPeztempLinkSet(FamiportPeztempLink fpl);
        FamiportPeztempLink FamiportPeztempLinkGetByTranNo(string tranNo);
        FamiportPeztempLink FamiportPeztempLinkGetByPeztempId(int peztempId);
        void FamiportPeztempLinkUpdate(FamiportPeztempLink fpl);
        void FamiportPeztempLinkDelete(FamiportPeztempLink fpl);

        #endregion

        #region FamiportPinRollbackLog

        int FamiportPinRollbackLogSet(FamiportPinRollbackLog log);

        #endregion

        #region ViewFamiportPinRollbackLog

        int ViewFamiportPinRollbackLogCount(params string[] filter);

        ViewFamiportPinRollbackLogCollection ViewFamiportPinRollBackLogGetList(int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter);

        #endregion

        #region 延遲出貨的檔次
        /// <summary>
        /// 延遲出貨檔次
        /// </summary>
        /// <param name="queryEndTime">搜尋昨天的資料</param>
        /// <returns>result</returns>
        DataTable GetDelayBidTable(string queryEndTime);


        /// <summary>
        /// 延遲出貨罰款檔次
        /// </summary>
        /// <param name="queryEndTime">搜尋昨天的資料</param>
        /// <returns>result</returns>
        DataTable DelayBidAmercementGetTable(string queryEndTime);

        DataTable GetDelayBidAmercements(string queryEndTime);
        #endregion

        int OrderedQuantityGet(Guid bid);

        #region IdeasStation
        void SaveIdeasStationItem(IdeasStation item);
        IdeasStation GetIdeasStationItem(int Id);
        IdeasStationCollection GetIdeasStationList();
        #endregion

        #region Product or something


        DataTable ProductDetailGet(IEnumerable<Guid> bids, int condition);


        #endregion

        #region 檔次質量相關 view_ppon_deal_operating_info

        ViewPponDealOperatingInfoCollection ViewPponDealOperatingInfoGetInBids(List<Guid> bids);
        DealTimeSlotSortElementCollection DealTimeSlotSortElementGet();
        DealTimeSlotSortElementRankCollection DealTimeSlotSortElementRankGet();
        bool DealTimeSlotSortElementUpdate(DealTimeSlotSortElement dtsse);
        bool DealTimeSlotSortElementInsert(DealTimeSlotSortElementCollection dtsseList);

        #endregion

        #region Keyword

        Keyword KeywordGet(int id);
        Keyword KeywordGet(string word);
        void KeywordSet(Keyword keyword);
        void KeywordDelete(int id);
        KeywordCollection KeywordGetAllList();
        KeywordCollection KeywordGetList(string search);
        KeywordCollection KeywordGetPromptList(string word);
        void KeywordInsertByPponSearchLog(int count);

        #endregion

        #region ViewDealBaseCostGrossMargin 毛利率/基礎成本

        ViewDealBaseGrossMargin ViewDealBaseGrossMarginGet(Guid bid);
        ViewDealBaseGrossMarginCollection ViewDealBaseGrossMarginGetTodayDeal();
        decimal DealMinGrossMarginGet(Guid bid);

        #endregion

        #region PponSearchLog

        void PponSearchLogSet(PponSearchLog pponSearchLog);
        PponSearchLog PponSearchLogGet(string sessionId, string word);
        List<Tuple<string, int>> SearchUsageQueryByMonth(DateTime queryStart, DateTime queryEnd);
        List<Tuple<string, int>> SearchUsageQueryByDay(DateTime queryStart, DateTime queryEnd);
        List<Tuple<string, string, int>> SearchTopKeywordsByMonth(DateTime queryStart, DateTime queryEnd, int topKeywords);
        List<Tuple<string, string, int>> SearchTopKeywordsByDay(DateTime queryStart, DateTime queryEnd, int topKeywords);
        List<Tuple<string, string, int>> SearchTopKeywordsByWord(string word, DateTime queryStart, DateTime queryEnd);
        List<Tuple<int, string, int>> SearchHopeData(DateTime queryStart, DateTime queryEnd);
        #endregion

        #region PromoSearchKey

        /// <summary>
        /// 取得推薦搜尋
        /// </summary>
        /// <param name="useMode"></param>
        /// <param name="takeCnt">0:取全部</param>
        /// <returns></returns>
        PromoSearchKeyCollection PromoSearchKeyGetColl(PromoSearchKeyUseMode useMode, int takeCnt = 0);

        int PromoSearchKeyRecordSet(PromoSearchKeyRecord d);

        PromoSearchKeyCollection PromoSearchKeyGetAllColl(PromoSearchKeyUseMode useMode);
        PromoSearchKey PromoSearchKeyGet(int id);
        PromoSearchKey PromoSearchKeyGet(string keyWord, PromoSearchKeyUseMode useMode);
        void PromoSearchKeydSet(PromoSearchKey promoSearchKey);
        void PromoSearchKeydSetColl(PromoSearchKeyCollection promoSearchKeyList);
        void PromoSearchKeyDelete(int id);
        int PromoSearchKeyMaxSeq(PromoSearchKeyUseMode useMode);
        PromoSearchKeyCollection PromoSearchKeyGetSort(int id);

        #endregion

        #region RelatedSystemPartner
        RelatedSystemPartner RelatedSystemPartnerGet(int id);

        void RelatedSystemPartnerSet(RelatedSystemPartner partner);

        int RelatedSystemPartnerGetCount(params string[] filter);

        RelatedSystemPartnerCollection RelatedSystemPartnerGetByPage(int page, int pageSize, string orderBy, params string[] filter);

        RelatedSystemPartnerCollection RelatedSystemPartnerGetList(params string[] filter);

        void RelatedSystemPartnerLogSet(RelatedSystemPartnerLog partnerlog);
        #endregion

        #region FreewifiUseLog
        void FreewifiUseLogSet(FreewifiUseLog wifiUseLog);
        #endregion

        #region FreewifiUseLog
        void BackendRecentDealsLogSet(BackendRecentDealsLog log);
        #endregion

        #region DealCloudStorage

        DealCloudStorage DealCloudStorageGet(int id);
        DealCloudStorage DealCloudStorageGet(Guid bid);
        void DealCloudStorageSet(DealCloudStorage cloudStorage);
        List<DealCloudStorage> DealCloudStorageGetListByMarkSync();
        List<DealCloudStorage> DealCloudStorageGetListByMarkDelete();
        DealCloudStorageCollection DealCloudStorageGetListByDayWithNoLock(DateTime startTime, DateTime endTime);
        List<DealCloudImageReference> DealCloudImageReferenceGetList(Guid bid);
        void DealCloudImageReferenceSet(DealCloudImageReference imgref);
        void DealCloudImageReferenceDelete(DealCloudImageReference imgref);
        void DealCloudImageReferenceDeleteByBid(Guid bid);
        int DealCloudImageReferenceGetCount(string imgkey);

        #endregion

        #region SalesCalendar
        SalesCalendarEvent SalesCalendarEventGet(int id);
        SalesCalendarEventCollection SalesCalendarEventGet(int pageStart, int pageLength, params string[] filter);
        int SalesCalendarEventGetCount(params string[] filter);
        SalesCalendarEvent SalesCalendarEventGetByGid(string gid);
        void SalesCalendarEventSet(SalesCalendarEvent calendar);
        bool SalesCalendarEventDelete(int id);
        #endregion

        #region provision_description
        ProvisionDescriptionCollection ProvisionDescriptionGetAll();
        ProvisionDescription ProvisionDescriptionGet(string id);
        ProvisionDescriptionCollection ProvisionDescriptionGetByRootId(string rootId);
        void ProvisionDescriptionSet(ProvisionDescription provision);
        void ProvisionDescriptionDelete(ProvisionDescription provision);
        #endregion provision_description


        #region product_info
        ProductInfo ProductInfoGet(Guid pid);
        ProductInfoCollection ProductInfoListGet(List<Guid> pids);
        ProductInfoCollection ProductInfoListGetByName(string pbName, string pName);
        ProductInfoCollection ProductInfoListGetSellerGuid(Guid sid);
        void ProductInfoSet(ProductInfo pi);
        #endregion product_info

        #region product_detail_info
        ProductItem ProductItemGet(Guid did);
        ProductItem ProductItemGetByNo(int pro_no);
        ProductItemCollection ProductItemGetList(List<Guid> dids);
        ProductItemCollection ProductItemListGetByProductGuid(Guid pid);
        ProductItemCollection ProductItemGetListByShortage();
        void ProductItemSet(ProductItem di);

        void ProductItemStockIncrease(Guid itemGuid, int incrQty, string modifyId);

        #endregion product_detail_info

        #region product_item
        ProductItem GetProductItemByProdId(string prodId);
        ProductSpec ProductSpecGet(Guid pdi);
        ProductSpecCollection ProductSpecListGetByItemGuid(Guid itemGuid);
        ProductSpecCollection ProductSpecListGetByItemGuid(List<Guid> itemGuids);
        void ProductSpecSet(ProductSpec spec);
        #endregion product_item

        #region product_info_import_log
        ProductInfoImportLogCollection ProductInfoImportLogListGet(Guid gid);
        void ProductInfoImportLogSet(ProductInfoImportLog log);
        #endregion product_info_import_log

        #region product_item_stock
        ProductItemStock ProductItemStockGet(Guid pdi);
        ProductItemStockCollection ProductItemStockListGetByProduct(Guid pid, Guid did);
        void ProductItemStockSet(ProductItemStock stock);
        #endregion product_item_stock

        #region product_log
        ProductLogCollection ProductLogGetListByPid(Guid pid);
        void ProductLogSet(ProductLog log);
        #endregion product_log

        #region view_product_item
        ViewProductItemCollection ViewProductItemListGetBySellerGuid(Guid sid);
        ViewProductItemCollection ViewProductItemListGetBySellerGuidList(List<Guid> sellerGuids);
        List<ViewProductItem> ViewProductItemListGet(int pageStart, int pageLength, List<Guid> sellerGuids, Guid? infoGuid, int productNo,
            string productBrandName, string productName, string gtin, string productCode, string items, int stockStatus, int warehouseType, bool hideDisabled, bool isSimilarProductNo = false);
        ViewProductItemCollection ViewProductItemListGet(int pageStart, int pageLength, List<Guid> sellerGuids, string productNo,
            string productBrandName, string productName, string pchomeProdId, string specs, string productCode);
        ViewProductItemCollection ViewProductItemListGetByItemGuids(List<Guid> itemGuids);
        Dictionary<Guid, ViewProductItem> ViewProductItemListGetByWithMpnOrGtin();

        ViewProductItem ViewProductItemGet(Guid itemGuid);

        #endregion view_product_item

        #region deal_product_delivery
        DealProductDelivery DealProductDeliveryGetByBid(Guid bid);
        DealProductDeliveryCollection DealProductDeliveryGetByBid(IEnumerable<Guid> businessHourGuids);
        void DealProductDeliverySet(DealProductDelivery dpDelivery);
        #endregion deal_product_delivery

        #region product_mapping_others
        List<Guid> GetGuidListByMainBid(Guid mainBid);
        #endregion

        int RushBuyGetCompleteOrderCount(Guid selectionDealBid, DateTime theDate, DateTime addDays);
        
        /// <summary>
        /// 取得美食/玩美/旅遊熱銷檔次，條件為近6小時銷售最多
        /// </summary>
        List<Guid> GetFoodBeautyTravelHotDealsGuidList();

    }
}
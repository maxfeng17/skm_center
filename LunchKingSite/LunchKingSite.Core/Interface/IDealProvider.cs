﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Core.Interface
{
    public interface IDealProvider : IProvider
    {
        #region GmcAvgPriceDeal

        GmcAvgPriceDeal GetGmcAvgPriceDeal(Guid bid);
        List<Guid> GetGmcAvgPriceDealAllBid();

        #endregion

        List<Guid> GetOnlineGroupCouponBid();
    }
}

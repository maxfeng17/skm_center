﻿using LunchKingSite.Core.Models.PponEntities;

namespace LunchKingSite.Core.Interface
{
    public interface IEventEntityProvider : IProvider
    {
        void SaveEventActivityHitLog(EventActivityHitLog log);
    }
}
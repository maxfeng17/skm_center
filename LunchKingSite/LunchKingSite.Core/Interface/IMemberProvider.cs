using System;
using System.Collections.Generic;
using System.Data;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core
{
    public interface IMemberProvider : IProvider
    {
        #region Member

        Member MemberGet(string userName);

        Member MemberGet(int uniqueId);

        Member MemberGetbyUniqueId(int uniqueId);

        Member MemberGetByTrueEmail(string userName);

        Member MemberGetByUserName(string userName);

        Member MemberGetByUserEmail(string userEmail);

        Member MemberGetByMobile(string mobile);

        Member MemberGetByAccessToken(string toekn);
        MemberCollection MemberGetByRoleGuid(Guid roleGuid);

        string MemberUserNameGet(int userId);

        int MemberUniqueIdGet(string userName);

        bool MemberSetUserName(int uniqueId, string newUserName, bool checkFirst);

        MemberCollection MemberAuthorizeGet(int userId);

        MemberCollection MemberGetList(string filter);

        MemberCollection MemberGetList(string column, object value);

        MemberCollection MemberGetList(List<int> unique_id);

        MemberCollection MemberGetList(int pageNumber, int pageSize, string orderBy, params string[] filter);

        MemberCollection MemberGetListByStatus(MemberStatusFlag status, MemberStatusFlag expectStatus, string orderBy);

        MemberCollection MemberGetListByStatusWithOutSubscription(MemberStatusFlag status, MemberStatusFlag expectStatus, string orderBy);

        MemberCollection MemberGetListByCity(MemberStatusFlag status, string[] cities, string[] buildings);

        MemberCollection MemberGetListByCityWithOutSubscription(MemberStatusFlag status, string[] cities, string[] buildings);

        MemberCollection MemberGetListDeliverableBySeller(MemberStatusFlag status, params Guid[] sellers);

        MemberCollection MemberGetListDeliverableBySellerWithOutSubscription(MemberStatusFlag status, params Guid[] sellers);

        MemberCollection MemberGetListByName(string name);

        MemberCollection MemberGetListByRole(string roleName);

        int MemberGetCount(string filter);

        bool MemberSet(Member member);

        void MemberSetCollectDealExpireMessage(int memberUniqueId, bool isEnable);

        MemberCollection MemberGetListByCollectDealBid(Guid businessHourGuid, MemberCollectDealExpireLogSendType type);

        MemberCollection MemberGetListBySql(string sql);

        void MemberPicSet(int userId, string pic);
        #endregion Member

        #region MemberProperty

        MemberProperty MemberPropertyGetByUserId(int userId);

        void MemberPropertySet(MemberProperty memProp);

        void MemberPropertyDelete(int userId);

        #endregion

        #region einvoice

        EinvoiceTriple EinvoiceTripleGetByUserid(int uniqueId);

        bool SaveEinvoiceTriple(EinvoiceTriple tripleData);

        EinvoiceWinner EinvoiceWinnerGetByUserid(int uniqueId);

        bool SaveEinvoiceWinner(EinvoiceWinner winnerData);

        bool EinvoiceTripleDelete(int Id);

        #endregion einvoice

        #region Role

        void RoleSet(Role role);
        Role RoleGet(string roleName);
        void RoleDeleteByName(string roleName);
        RoleCollection RoleGetList();
        RoleCollection RoleGetList(string userName);
        RoleCollection RoleGetList(int userId);
        void RoleMemberDelete(string userName, string[] roleNames);
        void RoleMemberDelete(int userId, string[] roleNames);

        void RoleMemberDelete(int userId);
        void RoleMemberSet(RoleMember rm);

        #endregion

        #region ViewRoleMember

        ViewRoleMemberCollection ViewRoleMemberCollectionGet(string roleName);

        #endregion

        #region Member Auth Info

        bool MemberAuthInfoSet(MemberAuthInfo memberAuthInfo);

        MemberAuthInfo MemberAuthInfoGet(int uniqueId);
        MemberAuthInfo MemberAuthInfoGetByEmail(string email);

        MemberAuthInfo MemberAuthInfoGetByKey(string email);

        bool MemberAuthInfoDelete(int userId);

        #endregion Member Auth Info

        #region ViewMemberBuildingCity

        ViewMemberBuildingCity ViewMemberBuildingCityGet(string username);

        ViewMemberBuildingCityCollection ViewMemberBuildingCityGetListPaging(int pageStart, int pageLength, string orderBy, string filter);

        #endregion ViewMemberBuildingCity

        #region MemberDelivery

        bool MemberDeliverySet(MemberDelivery memberdelivery);

        MemberDelivery SetMemberDelivery(MemberDelivery memberdelivery);

        MemberDeliveryCollection MemberDeliveryGetCollection(int memberId, bool isDefault);

        MemberDeliveryCollection MemberDeliveryGetCollection(int memberId);

        MemberDelivery MemberDeliveryGet(int id);

        bool MemberDeliveryDelete(string id);

        bool MemberDeliveryDelete(MemberDelivery delivery);

        #endregion MemberDelivery

        #region MemberCheckoutInfo
        void MemberCheckoutInfoUpdateByStoreId(string storeId, ServiceChannel serverChannel);
        void MemberCheckoutInfoSet(MemberCheckoutInfo info);
        MemberCheckoutInfo MemberCheckoutInfoGet(int userId);
        MemberCheckoutInfoCollection MemberCheckoutInfoCollectionGet();
        MemberCheckoutInfo MemberCheckoutInfoGetByStoreId(string storeId, ServiceChannel serverChannel);
        #endregion

        #region MemberLink

        MemberLink MemberLinkGet(int userid, SingleSignOnSource sourceOrg);

        MemberLink MemberLinkGetByExternalId(string externId);

        MemberLink MemberLinkGetByExternalId(string externId, SingleSignOnSource sourceOrg);

        MemberLinkCollection MemberLinkGetList(int userId);

        MemberLinkCollection MemberLinkGetList(string userName);

        bool MemberLinkSet(MemberLink link);

        void MemberLinkDelete(int userId, SingleSignOnSource singleSignOnSource);

        void MemberLinkDelete(int userId);

        #endregion MemberLink

        #region PcashMemberLink
        bool PCashMemberLinkSet(PcashMemberLink link);
        PcashMemberLink PcashMemberLinkGet(int userid);
        void UnbindPcashMemberLink(int userId);
        #endregion

        #region 第三方支付 MemberToken

        MemberToken MemberTokenGet(int userId, ThirdPartyPayment paymentOrg);
        int MemberTokenSet(MemberToken data);
        int MemberTokenDelete(int userId, ThirdPartyPayment paymentOrg);

        #endregion

        #region BonusTransaction

        bool BonusTransactionSetToNewUser(string oldUserName, string newUserName);

        #endregion BonusTransaction

        #region MemberPromotionDeposit

        bool MemberPromotionDepositSet(MemberPromotionDeposit data);

        /// <summary>
        /// 傳入P好康的order Guid 查詢紅利點數的正項紀錄，當MemberPromotionDeposit中 OrderGuid的parent_order_id為除傳入的GUID
        /// </summary>
        /// <param name="pponDealOrderGuid"></param>
        /// <returns></returns>
        MemberPromotionDepositCollection MemberPromotionDepositGetListByPponDealOrderGuid(Guid pponDealOrderGuid);

        /// <summary>
        /// 依據傳入的orderGuid查詢MemberPromotionDeposit中OrderGuid為傳入參數的資料。
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        MemberPromotionDepositCollection MemberPromotionDepositGetListByOrderGuid(Guid orderGuid);
        /// <summary>
        /// 依據傳入的vendor_paymemnt_change id查詢MemberPromotionDeposit。
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        MemberPromotionDepositCollection MemberPromotionDepositGetListByVendorPaymentChangeIds(IEnumerable<int> vpcIds);
        /// <summary>
        /// 依據傳入的vendor_paymemnt_change id查詢MemberNoneDeposit。
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        MemberNoneDepositCollection MemberNoneDepositGetListByVendorPaymentChangeIds(IEnumerable<int> vpcIds);
        MemberNoneDepositCollection MemberNoneDepositGetListByOids(Guid Oids);
        MemberPromotionDepositCollection MemberPromotionDepositGetListByOids(Guid Oids);

        /// <summary>
        /// 依據傳入的紅利資料儲存
        /// </summary>
        /// <param name="mpdc"></param>
        /// <returns></returns>
        int MemberPromotionDepositSetList(MemberPromotionDepositCollection mpdc);

        #endregion MemberPromotionDeposit

        #region MemberPromotionWithdrawal

        bool MemberPromotionWithdrawalSet(MemberPromotionWithdrawal data);

        bool MemberPromotionWithdrawalCollectionSet(MemberPromotionWithdrawalCollection data);

        /// <summary>
        /// 依據傳入的orderGuid查詢MemberPromotionWithdrawal中OrderGuid為傳入參數的資料。
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        MemberPromotionWithdrawalCollection MemberPromotionWithdrawalGetListByOrderGuid(Guid orderGuid);

        /// <summary>
        /// 傳入P好康的order Guid 查詢紅利點數消費的資料，當MemberPromotionWithdrawal中 OrderGuid的parent_order_id為除傳入的GUID
        /// </summary>
        /// <param name="pponDealOrderGuid">PponDeal的OrderGuid</param>
        /// <returns></returns>
        MemberPromotionWithdrawalCollection MemberPromotionWithdrawalGetListByPponDealOrderGuid(Guid pponDealOrderGuid);

        MemberPromotionDepositCollection MemberPromotionDepositGetAvailableList(int userId);

        MemberPromotionWithdrawalCollection MemberPromotionWithdrawalGetAvailableList(int userId);

        double MemberPromotionWithdralSumByDeposit(int depositId);

        /// <summary>
        /// 取得特定活動，發放的紅利金(BCash)總數
        /// 以元為單位
        /// </summary>
        /// <param name="actionGroupGuid"></param>
        /// <returns></returns>
        int MemberBounsGetGivenTotal(Guid actionGroupGuid);

        /// <summary>
        /// 取得特定活動，使用的紅利金(BCash)總數
        /// 以元為單位
        /// </summary>
        int MemberBounsGetUsedTotal(Guid actionGroupGuid);

        #endregion MemberPromotionWithdrawal

        #region ViewMemberPromotionTransaction

        decimal ViewMemberPromotionTransactionGetSum(string username, MemberPromotionType memberPromotionType);

        ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetList(
            string username, string orderBy,
            MemberPromotionType memberPromotionType = MemberPromotionType.Bouns,
            bool orderDesc = false, int? topNum = null);

        ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetList(string orderBy, params string[] filter);
        ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetListTop(string orderBy, params string[] filter);

        ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetListLastN(string username, string orderBy, int recCount, MemberPromotionType memberPromotionType = MemberPromotionType.Bouns);

        ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetListLastN(int userId, string orderBy, int recCount, MemberPromotionType memberPromotionType = MemberPromotionType.Bouns);

        ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetOrderGroupList(int pageStart, int pageLength, string username, string orderBy, MemberPromotionType memberPromotionType = MemberPromotionType.Bouns);

        int ViewMemberPromotionTransactionGetOrderGroupListCount(string username, MemberPromotionType memberPromotionType = MemberPromotionType.Bouns);


        #endregion ViewMemberPromotionTransaction

        #region ViewMemberBuildingCity

        ViewMemberDeliveryBuildingCityCollection ViewMemberDeliveryBuildingCityGetCollection(string memberId);

        #endregion ViewMemberBuildingCity

        #region Redeem

        int RedeemOrderSetToNewUser(string oldUserName, string newUserName);

        #endregion Redeem

        #region ServiceMessage

        void ServiceMessageSet(ServiceMessage sc, int? userId = null);

        ServiceMessage ServiceMessageGet(int id);

        ServiceMessageCollection ServiceMessageGetListForFilter(int pageStart, int pageLength, string orderBy, params string[] filter);

        int ServiceMessageGetCountForFilter(params string[] filter);

        ServiceMessageCategory ServiceMessageCategoryGetByCategoryId(int cateId);

        /// <summary>
        /// 以 parent_category_id抓取 ServiceMessage. 沒有ParentId的話就抓所有母類別，有的話就抓屬於這個ParentId的子類別
        /// </summary>
        ServiceMessageCategoryCollection ServiceMessageCategoryCollectionGetListByParentId(int? parentId);

        #endregion ServiceMessage

        #region ViewServiceMessageDetail

        ViewServiceMessageDetailCollection ViewServiceMessageDetailGetListForFilter(int pageStart, int pageLength, string orderBy, params string[] filter);

        int ViewServiceMessageDetailGetCountForFilter(params string[] filter);

        #endregion ViewServiceMessageDetail

        #region ServiceLog

        void ServiceLogSet(ServiceLog sl);

        ServiceLogCollection ServiceLogGetList(string serviceMessageId);

        #endregion ServiceLog

        #region Cash Trust Log

        CashTrustLog CashTrustLogGet(Guid businessHourGuid, string couponSequenceNumber);

        CashTrustLog CashTrustLogGetByCouponSequenceNumber(string couponSequenceNumber);

        CashTrustLog CashTrustLogGet(Guid trustId);

        bool CashTrustLogUpdateStoreGuidByTrustId(Guid trustId, Guid storeGuid);

        CashTrustLog CashTrustLogGetByCouponId(int couponId, OrderClassification orderClassification = OrderClassification.LkSite);

        CashTrustLog CashTrustLogGetByOrderId(Guid orderGuid);
        CashTrustLogCollection CashTrustLogListGetByOrderId(Guid orderGuid);

        CashTrustLogCollection CashTrustLogGetByTrustSequenceNumber(string trustSequenceNumber, TrustProvider provider);

        CashTrustLogCollection CashTrustLogGetList(string orderBy, params string[] filter);

        CashTrustLogCollection CashTrustLogGetList(IEnumerable<Guid> trustIds);

        CashTrustLogCollection CashTrustLogGetListByOrderGuid(Guid orderGuid, OrderClassification classification);

        CashTrustLogCollection CashTrustLogGetListByOrderGuid(Guid orderGuid);

        CashTrustLogCollection CashTrustLogGetCouponSequencesWithUnused(Guid orderGuid);

        CashTrustLogCollection CashTrustLogGetCouponSequencesWithUnused(Guid orderGuid, int userId);

        /// <summary>
        /// 由CashTrustLog中，取出某些狀態的資料。
        /// </summary>
        /// <param name="orderDetailGuid"></param>
        /// <param name="statusList"></param>
        /// <param name="classification"></param>
        /// <returns></returns>
        CashTrustLogCollection CashTrustLogGetListByOrderDetailGuid(Guid orderDetailGuid, List<TrustStatus> statusList, OrderClassification classification = OrderClassification.LkSite);

        /// <summary>
        /// 取出orderDetailGuid商品的CashTrustLog紀錄
        /// </summary>
        /// <param name="orderDetailGuid"></param>
        /// <param name="classification"></param>
        /// <returns></returns>
        CashTrustLogCollection CashTrustLogGetListByOrderDetailGuid(Guid orderDetailGuid, OrderClassification classification = OrderClassification.LkSite);

        CashTrustLogCollection CashTrustLogGetListByOrderDetailGuids(IEnumerable<Guid> orderDetailGuids, OrderClassification classification = OrderClassification.LkSite);

        CashTrustLogCollection CashTrustLogGetListByBid(Guid bid);

        CashTrustLogCollection CashTrustLogGetListByBid(IEnumerable<Guid> bids);

        CashTrustLogCollection CashTrustLogGetListByBidWhereOrderIsSuccessful(Guid bid);

        CashTrustLogCollection CashTrustLogGetListByBidWhereOrderIsSuccessful(Guid bid, DateTime genDate);

        /// <summary>
        /// 有效(退貨中/退貨完成)退貨單上的 cash_trust_log
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        CashTrustLogCollection PponRefundingCashTrustLogGetListByOrderGuid(Guid orderGuid);

        bool CashTrustLogSet(CashTrustLog cashTrustLog);

        bool CashTrustLogSetNew(CashTrustLog cashTrustLog);

        bool CashTrustLogColSetForVerifyCoupons(CashTrustLogCollection cashTrustLogs);

        bool CashTrustLogCollectionSet(CashTrustLogCollection cashTrustLogCollection);

        OldCashTrustLogCollection GetOldCashTrustLogCollectionByOid(Guid order_guid);
        CashTrustLogCollection GetCashTrustLogCollectionByOid(IEnumerable<Guid> order_guid);

        CashTrustLogCollection CashTrustLogGetListForRemain(TrustProvider provider);

        void CashTrustLogToUpdateFromWPF(Guid trust_id);

        /// <summary>
        /// 以對帳單代號查詢cash_trust_log
        /// </summary>
        /// <param name="balance_sheet_id">對帳單ID</param>
        /// <param name="balance_sheet_detail_status">核銷狀態</param>
        /// <returns></returns>
        CashTrustLogCollection CashTrustLogGetListByBalanceSheetId(int balance_sheet_id, BalanceSheetDetailStatus balance_sheet_detail_status);

        /// <summary>
        /// 以hi_deal_product id查詢cash_trust_log(包含憑證及宅配)
        /// </summary>
        /// <param name="productId">hi_deal_product id</param>
        /// <returns></returns>
        CashTrustLogCollection CashTrustLogGetAllListByHiDealProductId(int productId);

        /// <summary>
        /// 紀錄ACH匯款報表代號
        /// </summary>
        /// <param name="report_guid">報表代號</param>
        /// <param name="balance_sheet_id">對帳單ID</param>
        /// <returns></returns>
        void CashTrustLogToUpdateForAch(Guid report_guid, int balance_sheet_id);

        /// <summary>
        /// 取得購買期間內未使用資料
        /// </summary>
        /// <param name="dateStart">購買期間(起)</param>
        /// <param name="dateEnd">購買期間(迄)</param>
        /// <param name="provider">信託銀行</param>
        /// <returns></returns>
        CashTrustLogCollection CashTrustLogGetListByOrderCreateTime(DateTime dateStart, DateTime dateEnd, TrustProvider provider);

        /// <summary>
        /// 取得購買期間內已核銷(或退貨)資料
        /// </summary>
        /// <param name="dateStart">購買期間(起)</param>
        /// <param name="dateEnd">購買期間(迄)</param>
        /// <param name="provider">信託銀行</param>
        /// <returns></returns>
        CashTrustLogCollection CashTrustLogGreaterThanStatusGetListByPeriod(DateTime dateStart, DateTime dateEnd, TrustProvider provider, TrustStatus status);

        /// <summary>
        /// 取得品生活有效訂單資料
        /// </summary>
        /// <param name="productId">品生活商品檔Id</param>
        /// <returns></returns>
        CashTrustLogCollection CashTrustLogGetListByHiDealProductId(int productId);

        /// <summary>
        /// Job:ShipStatusUpdate使用-抓出P好康或品生活已開始兌換檔次且訂單未出貨之訂單並根據輸入之出貨日期
        /// 抓取須更新之CashTrustLog 資料 以利更新CashTrustLog status
        /// </summary>
        /// <param name="classification">Ppon or HiDeal</param>
        /// <param name="queryEndTime">搜尋某時間以前之資料</param>
        /// <returns></returns>
        CashTrustLogCollection CashTrustLogGetUnShipOrderListByOrderClassification(OrderClassification classification, DateTime queryEndTime);

        CashTrustLogCollection CashTrustLogByTrustId(List<Guid> trustIds);
        /// <summary>
        /// 抓取消費者於指定時間內之消費總額(行銷活動)
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <param name="dateStart">活動期間(起)</param>
        /// <param name="dateEnd">活動期間(迄)<</param>
        /// <param name="cid">指定分類id(傳入0則不指定分類)<</param>
        /// <returns>(刷卡、Pcash、Scash、Atm)總額</returns>
        int CashTrustLogConsumingAmoutGetByPeriod(int userId, DateTime dateStart, DateTime dateEnd, int cid = 0);

        /// <summary>
        /// 捉消費者指定時間內的全家零元檔核銷次數
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        int CashTrustLogGetFamiDealVerifiedCount(int userId, DateTime startTime, DateTime endTime);
        bool CashTrustLogIsSameStoreGuidByTrustId(List<Guid> trustIds, Guid storeGuid);

        /// <summary>
        /// 超取部分未付+未取或DC到貨異常
        /// </summary>
        /// <returns></returns>
        CashTrustLogCollection CashTrustLogGetListByNoReceiptPaid();

        /// <summary>
        /// 新光退貨壓Status
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="status"></param>
        /// <param name="execTime"></param>
        /// <returns></returns>
        void CashTrustLogUpdateRefundBySkm(Guid orderGuid, TrustStatus status);

        #endregion Cash Trust Log

        #region User Cash Trust Log

        UserCashTrustLogCollection UserCashTrustLogGetList(int userId);
        UserCashTrustLogCollection UserCashTrustLogPlusOverYearGetList(int userId);
        UserCashTrustLogCollection UserCashTrustLogNegativeOverYearGetList(int userId);
        UserCashTrustLogCollection UserCashTrustLogWithinYearGetList(int userId);

        DataTable CashTrustLogGetListForRrport();

        DataTable UserCashTrustLogGetListForRrport();

        void UserCashTrustSetToInitialFromAtm(Guid tid);

        UserCashTrustLogCollection UserCashTrustLogGetList(string orderBy, params string[] filter);

        UserCashTrustLogCollection UserCashTrustLogGet(Guid tid);

        bool UserCashTrustLogSet(UserCashTrustLog userCashTrustLog);

        bool UserCashTrustLogCollectionSet(UserCashTrustLogCollection userCashTrustLogCollection);

        #endregion User Cash Trust Log

        #region Cash Trust Status Log

        CashTrustStatusLogCollection CashTrustStatusLogGetList(Guid trustId);

        CashTrustStatusLogCollection CashTrustStatusLogGetList(IEnumerable<Guid> trustIds);

        bool CashTrustStatusLogSet(CashTrustStatusLog trustLog);

        bool CashTrustStatusLogCollectionSet(CashTrustStatusLogCollection trustLogCollection);

        OldCashTrustLog OldCashTrustLogGetByCouponId(int couponId);

        #endregion Cash Trust Status Log

        #region Trust Verification Report

        TrustVerificationReport TrustVerificationReportGetLatest(TrustProvider provider, TrustVerificationReportType type = TrustVerificationReportType.CouponAndCash, bool isUseFlag = true, TrustFlag flag = TrustFlag.SendReport);

        int TrustVerificationReportSet(TrustVerificationReport r);

        TrustVerificationReport TrustVerificationReportGetByDate(TrustProvider provider, DateTime date, TrustFlag flag, TrustVerificationReportType type = TrustVerificationReportType.CouponAndCash);

        TrustVerificationReportCollection TrustVerificationReportCollectionGet(TrustProvider provider, TrustVerificationReportType type = TrustVerificationReportType.CouponAndCash);
        #endregion Trust Verification Report

        #region Verification Log

        bool VerificationLogSet(VerificationLog vLog);

        bool OldCashTrustLogSet(OldCashTrustLog oldCashTrustLog);

        VerificationLog VerificationLogVerifyCouponGet(int? couponId);

        #endregion Verification Log

        #region Cash Trust Coupon Change Log

        bool CashTrustCouponChangeLogCollectionSet(CashTrustCouponChangeLogCollection ctcc);

        #endregion

        #region CouponListMain 訂單

        int GetMainFilterOrderCount(int? uniqueId, MemberOrderMainFilterType mainFilter);

        ViewCouponListMainCollection GetCouponListMainColBySubFilter(int uniqueId, MemberOrderSubFilterType subFilter, int pageLength);
        ViewCouponListMainCollection GetCouponListMainColByMainFilter(int uniqueId, MemberOrderMainFilterType mainFilter, int pageStart,
            int pageLength);

        int GetCouponListMainCount(int uniqueId, string filter);

        int GetCouponListMainCount(int uniqueId, Guid sellerGuid, string filter);

        int GetCouponOnlyListMainCount(int uniqueId, string filter);

        ViewCouponListMain GetCouponListMainByOid(Guid orderGuid);

        ViewCouponListSequence GetCouponListSequenceByOid(Guid orderGuid, int couponId);

        ViewCouponListMainCollection GetGiftOrderListByUser(int uniqueId);

        ViewCouponListMainCollection GetCouponListMainListByUser(int pageStart, int pageLength, int uniqueId, string orderBy, string filter, bool withPiinlife);

        ViewCouponListMainCollection GetCouponListMainListByUserAndSellerGuid(int pageStart, int pageLength, int uniqueId, Guid sellerGuid, string filter);

        ViewCouponOnlyListMainCollection GetCouponOnlyListMainListByUser(int pageStart, int pageLength, int uniqueId, string orderBy, string filter, bool withPiinlife);

        ViewCouponListMainCollection GetCouponListMainListByOrderID(int uniqueId, string orderid);

        decimal GetCouponTotalSavings(string user_name);

        ViewCouponListSequenceCollection GetCouponListSequenceListByOid(Guid guid, OrderClassification classification);

        ViewCouponListSequenceCollection GetCouponListSequenceListByOid(Guid guid);

        ViewCouponListSequenceCollection GetCouponListSequenceListByBetweenTime(int tokenId, DateTime startTime, DateTime endTime);

        ViewCouponListSequenceCollection GetCouponListSequenceByOidList(List<Guid> oidList);

        DataTable GetMemberListByDepartmentAndBusinessHourAndCouponUsageExcludeRefunding(DeliveryType theType, Guid theBusinessHourGuid, Guid storeGuid, int theRemainCount);
        DataTable GetMemberListByDepartmentAndBusinessHourAndCouponForSendGift(Guid theBusinessHourGuid, Guid storeGuid);

        ViewCouponListCollection GetViewCouponListByOrderGuid(Guid orderGuid);
        #endregion CouponList

        #region Trust Fix Tool

        DataTable GetTrustOverCoupon();

        DataTable GetCouponOverTrust();

        DataTable GetTrustCouponEmpty();

        DataTable GetTrustCouponEmptyOrderDetailByDays(int days);

        #endregion Trust Fix Tool

        #region Edm Eliminate Email

        EdmEliminateEmail EdmEliminateEmailGet(string column, object value);

        bool EdmEliminateEmailSet(EdmEliminateEmail data);

        #endregion Edm Eliminate Email

        #region ViewMemberBuildingCityParentCity

        ViewMemberBuildingCityParentCity ViewMemberBuildingCityParentCityGetByUserName(string userName);

        #endregion ViewMemberBuildingCityParentCity

        #region AccountAudit

        void AccountAuditSet(string accountId, string ipAddress, AccountAuditAction action, bool isSuccess,
            string memo = null, OrderFromType? deviceType = null);

        void AccountAuditSet(int userId, string accountId, string ipAddress, AccountAuditAction action, bool isSuccess,
            string memo = null, OrderFromType? deviceType = null);

        List<AccountAudit> AccountAuditGetList(int userId, AccountAuditAction action);

        #endregion AccountAudit

        #region LoginTicket

        bool LoginTicketSet(LoginTicket ticket);

        LoginTicket LoginTicketGet(string ticketId, int userId, string driverId, LoginTicketType logintickettype = LoginTicketType.AppLogin);
        LoginTicket LoginTicketGet(string ticketId);

        /// <summary>
        /// 查詢LoginTicket資料，檢查系統時間，取出尚未到期的登入資料
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="userId"></param>
        /// <param name="driverId"></param>
        /// <returns></returns>
        LoginTicket LoginTicketGetExpiration(string ticketId, int userId, string driverId, LoginTicketType logintickettype = LoginTicketType.AppLogin);

        /// <summary>
        /// 設定某一位使用者的所有login token的過期時間為當下系統時間
        /// </summary>
        /// <param name="userId"></param>
        void LoginTicketSetExpirationTimeAsNowByUser(int userId);

        LoginTicketCollection LoginTicketListGet(int userId, LoginTicketType logintickettype = LoginTicketType.AppLogin);

        #endregion LoginTicket

        #region VbsAccountAudit

        void VbsAccountAuditSet(int? vbsMembershipId, string accountId, string ipAddress, VbsAccountAuditAction action, bool isSuccess, bool isTemporary, string memo = null);

        #endregion VbsAccountAudit

        #region VbsShipfileImportLog
        /// <summary>
        /// 商家出貨檔案匯入使用紀錄
        /// </summary>
        /// <param name="vsfi">VbsShipfileImportLog</param>
        void VbsShipfileImportLogSet(VbsShipfileImportLog vsfi);
        #endregion

        #region VbsMembership

        void VbsMembershipSet(VbsMembership mem);

        int VbsPrefixUseGetCount(string prefix, int prefixLength);

        VbsMembership VbsMembershipGetById(int id);

        VbsMembership VbsMembershipGetByAccountId(string accountId);
        /// <summary>
        /// 以member.unique_id取得對應的vbs_membership資料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        VbsMembership VbsMembershipGetByUserId(int userId);

        VbsMembership VbsMembershipGetByAccountId(string accountId, List<VbsMembershipAccountType> accountTypes);

        VbsMembershipCollection VbsMembershipGetListByBindAccount(string name);

        VbsMembershipCollection VbsMembershipGetListLikeAccountId(string accountId, List<VbsMembershipAccountType> accountTypes, bool fuzzySearch = false);

        VbsMembershipCollection VbsMembershipGetListByAccountName(string name, List<VbsMembershipAccountType> accountTypes, bool fuzzySearch = false);

        VbsMembershipCollection VbsMembershipGetListByEmail(string email, List<VbsMembershipAccountType> accountTypes, bool fuzzySearch = false);

        VbsMembershipCollection VbsMembershipGetList(List<VbsMembershipAccountType> accountTypes);

        #endregion VbsMembership

        #region VbsRole
        void VbsRoleSet(VbsRole vbsRole);
        VbsRole VbsRoleGet(string vbsRoleName);
        VbsRoleCollection VbsRoleGetList(int userId);
        void VbsRoleMemberSet(VbsRoleMember vrm);
        void VbsRoleMemberDelete(int userId, string[] roleNames);
        #endregion

        #region VbsAccountApplyPasswordLog
        VbsAccountApplyPasswordLog VbsAccountApplyPasswordLogGet(Guid guid);
        void VbsAccountApplyPasswordLogSet(VbsAccountApplyPasswordLog applyLog);
        #endregion VbsAccountApplyPasswordLog

        #region VbsMemberShipBindAccount
        void VbsMembershipBindAccountSet(VbsMembershipBindAccount vbsmsba);
        VbsMembershipBindAccount VbsMembershipBindAccountGetByVbsAccountName(string vbsAccountName);
        VbsMembershipBindAccount VbsMembershipBindAccountGetByVbsAccountId(string vbsAccountId);
        VbsMembershipBindAccount VbsMembershipBindAccountGetByUserId(int userId);
        void VbsMemebershipBindAccountUpdateAllDelete(string vbsAccountId, int userId);
        #endregion

        #region ResourceAcl

        ResourceAcl ResourceAclGet(string accountId, ResourceAclAccountType accountType, Guid resourceGuid, ResourceType resourceType);

        ResourceAcl ResourceAclGetByAccountIdAndResourceType(string accountId, int resourceType);

        ResourceAclCollection ResourceAclGetListByAccountId(string accountId);

        int ResourceAclSet(ResourceAcl acl);

        ResourceAclCollection ResourceAclGetListByResourceGuidList(IEnumerable<Guid> resourceGuids);

        ResourceAcl ResourceAclGetListByResourceGuid(Guid resourceGuid);

        ResourceAclCollection ResourceAclGetListByIds(IEnumerable<int> resourceAclIds);

        void ResourceAclDelete(ResourceAcl acl);

        DataTable ResourceAclGetDealStorePermissionInfoByAccountId(string accountId);

        #endregion ResourceAcl

        #region ResourceAclLog

        int ResourceAclLogSet(ResourceAclLog log);

        #endregion ResourceAclLog

        #region ViewVbsFlattenedAcl

        /// <summary>
        /// resources with permission settings (allow+deny) .  
        /// </summary>
        ViewVbsFlattenedAclCollection ViewVbsFlattenedAclCurrentPermissionsGetList(string accountId);

        /// <summary>
        /// deals with permission settings (allow+deny) .  
        /// </summary>
        ViewVbsFlattenedAclCollection ViewVbsFlattenedAclDealLevelPermissionsGetList(string accountId);
        ViewVbsFlattenedAclCollection ViewVbsFlattenedAclDealLevelPermissionsGetList(string accountId, VbsRightFlag vbsRight);

        ViewVbsFlattenedAclCollection ViewVbsFlattenedAclDealLevelPermissionsGetList(string accountId, Guid merchandiseGuid);
        ViewVbsFlattenedAclCollection ViewVbsFlattenedAclDealLevelPermissionsGetList(string accountId, Guid merchandiseGuid, VbsRightFlag vbsRight);

        /// <summary>
        /// resources with or without permission settings
        /// </summary>
        ViewVbsFlattenedAclCollection ViewVbsFlattenedAclGetListBySid(string accountId, IEnumerable<Guid> sid);

        ViewVbsFlattenedAclCollection ViewVbsFlattenedAclGetListLikeSellerName(string accountId, string sellerName);
        ViewVbsFlattenedAclCollection ViewVbsFlattenedAclDealPermissionsGetList(string accountId);
        DataTable FnVbsFlattenedAclLiteGetList(string accountId);

        #endregion ViewVbsFlattenedAcl

        #region VbsVendorAceLite

        DataTable VbsVendorAceLiteFnGetDataTable(string accountId, VbsRightFlag? vbsRight = null);

        DataTable VbsVendorAceLiteFnGetDataTable(string accountId, Guid dealGuid, VbsRightFlag? vbsRight = null);

        #endregion

        #region MemberCollectDeal
        void MemberCollectDealSetPushTime(int MemberUniqueId, Guid BusinessHourGuid, DateTime PushTime);
        void MemberCollectDealSave(MemberCollectDeal mcd);
        void MemberCollectDealRemove(int memberUniqueId, Guid businessHourGuid);
        void MemberCollectDealRemove(int memberUniqueId, List<Guid> bidList);
        /// <summary>
        /// 取得收藏的檔次資料
        /// </summary>
        /// <param name="memberUniqueId"></param>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        bool MemberCollectDealIsCollected(int memberUniqueId, Guid businessHourGuid);

        /// <summary>
        /// 取得會員收藏的檔次，僅限目前在販售的檔次
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="eDate"></param>
        /// <param name="topNum"></param>
        /// <param name="sDate"></param>
        /// <returns></returns>
        List<Guid> MemberCollectDealGetOnlineDealGuids(int userId, DateTime sDate, DateTime eDate, int topNum = 1000);
        MemberCollectDeal MemberCollectDealGet(int memberUniqueId, Guid businessHourGuid);
        MemberCollectDealCollection MemberCollectDealGetByMemberUniqueId(int memberUniqueId);
        MemberCollectDealCollection MemberCollectDealGetByBusinessHourGuid(Guid bid);
        int MemberCollectDealCountByBidStatusType(Guid bid, MemberCollectDealStatus collectStatus, MemberCollectDealType collectType);
        int MemberCollectDealGetOutOfDateDealCount(int memberUniqueId);
        void MemberCollectDealRemoveOutOfDateDeal(int memberUniqueId);

        #endregion MemberCollectDeal

        #region MemberCollectNotice
        /// <summary>
        /// 儲存要收到收藏通知的裝置
        /// </summary>
        /// <param name="notice"></param>
        /// <returns></returns>
        int MemberCollectNoticeSet(MemberCollectNotice notice);
        /// <summary>
        /// 以使用者編號與裝置編號取得特定 要收到收藏推播裝置資料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="deviceId"></param>
        MemberCollectNotice MemberCollectNoticeGet(int userId, int deviceId);
        /// <summary>
        /// 以使用者編號與AccessToken編號取得特定 要收到收藏推播裝置資料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        MemberCollectNotice MemberCollectNoticeGetByToken(int userId, int tokenId);
        /// <summary>
        /// 查詢某裝置所有的設定紀錄
        /// </summary>
        /// <param name="deviceId">裝置編號</param>
        /// <returns></returns>
        MemberCollectNoticeCollection MemberCollectNoticeGetList(int deviceId);
        /// <summary>
        /// 移除deviceId為傳入值的所有紀錄
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        void MemberCollectNoticeRemove(int deviceId);

        void MemberCollectNoticeRemove(int userId, int deviceId);
        #endregion MemberCollectNotice

        #region ViewMemberCollectDeal

        int ViewMemberCollectDealGetDealCount(int memberUniqueId);
        ViewMemberCollectDealCollection ViewMemberCollectionDealGetListByPager(int memberUniqueId, int pageStart, int pageSize, string orderBy);
        string ViewMemberCollectDealGetDealList(int uniqueid);

        #endregion ViewMemberCollectDeal

        #region ViewMemberCollectDealContent
        /// <summary>
        /// 查詢會員收藏的檔次資料
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        ViewMemberCollectDealContentCollection ViewMemberCollectDealContentGetList(int pageNumber, int pageSize, string orderBy, params string[] filter);

        /// <summary>
        /// 取得會員收藏檔次(排除結檔超過N個月檔次)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="collectType"></param>
        /// <param name="months"></param>
        /// <returns></returns>
        ViewMemberCollectDealContentCollection ViewMemberCollectDealContentGetList(int userId, int months, MemberCollectDealType collectType = MemberCollectDealType.Coupon);

        #endregion ViewMemberCollectDealContent

        #region MemberCollectDealExpireLog

        void MemberCollectDealExpireLogSave(MemberCollectDealExpireLog log);

        #endregion MemberCollectDealExpireLog

        #region ViewMemberCollectDealPush
        /// <summary>
        /// 依照到期日取得收藏檔次
        /// </summary>
        /// <param name="due_date"></param>
        /// <returns></returns>
        ViewMemberCollectDealPushCollection ViewMemberCollectDealPushGetListByDate(DateTime due_date);
        /// <summary>
        /// 依照日期間隔取收藏檔次
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        ViewMemberCollectDealPushCollection ViewMemberCollectDealPushGetListByInterval(int interval);
        #endregion

        #region Evaluate

        /// <summary>
        /// 查詢評價資料
        /// </summary>
        DataTable EvaluateStatisticsByBidGet(Guid? sid);
        DataTable EvaluateStatisticsByBidGet(IEnumerable<Guid> productGuids, Guid? sid);

        DataTable EvaluateStoreList();
        DataTable EvaluateStoreListBySeller();
        DataTable EvaluateStoreListForCustomer(string account_id, int condition);
        DataTable EvaluateStoreListForCustomer3(List<Guid> stores, int condition);

        UserEvaluateItemCollection UserEvaluateItemCollectionGet();
        int UserEvaluateDetailSetList(UserEvaluateDetailCollection d);

        int UserEvaluateMainSet(UserEvaluateMain d);

        UserEvaluateDetailCollection EvaluateDetailGetByMainID(int mainid, int dataType);

        int EvaluateGetMainID(Guid tid);

        DataTable EvaluateDetailRatingGet(Guid bid, Guid sid);

        DataTable EvaluateDetailRatingGetTable(Guid bid, Guid sid);


        /// <summary>
        /// 依照日期取得使用檔次資料
        /// </summary>
        /// <param name="sendDate"></param>
        /// <returns></returns>
        DataTable EvaluateUserGet();
        DataTable EvaluateUserByNegativeComments();
        ViewUserEvaluateCollection ViewUserEvaluateGet();
        int UserEvaluateLogSet(UserEvaluateLog ueLog);
        UserEvaluateLog UserEvaluateLogGetByOGuid(Guid oGuid);
        #endregion

        #region Mobile Member

        MobileMember MobileMemberGet(int userId);

        MobileMember MobileMemberGet(string number);

        MobileMember MobileMemberGetByUserName(string username);

        void MobileMemberSet(MobileMember mobileMember);

        void MobileMemberDelete(string mobileMember);

        void MobileMemberDelete(int userId);

        MobileAuthInfo MobileAuthInfoGet(int userId, string mobile);

        List<MobileAuthInfo> MobileAuthInfoGetList(string mobile);

        void MobileAuthInfoSet(MobileAuthInfo mai);

        void MobileAuthInfoDelete(int userId);
        #endregion

        #region MembershipCardLog

        bool MembershipCardLogSet(MembershipCardLog membershipCardLog);

        bool MembershipCardLogSetCollection(MembershipCardLogCollection data);

        #endregion MembershipCardLog

        #region ViewMembershipCardLog

        ViewMembershipCardLogCollection ViewMembershipCardLogGetList(int userId, string orderBy);

        ViewMembershipCardLogCollection ViewMembershipCardLogGetListByCardId(int cardId, bool queryAfterCol = true);
        ViewMembershipCardLogCollection ViewMembershipCardLogGetListByUserCardId(int userCardId, MembershipCardLogType type);
        #endregion ViewMembershipCardLog

        #region TrsutRecord

        int TrustRecordSet(TrustRecordCollection records);

        void TrustRecordSetTrust(int userId, int provider, Guid guid);
        TrustRecordCollection TrustRecordGetUserTotalAmount(List<int> userId, DateTime now);

        TrustRecordCollection TrustRecordGetUnTrusted();

        TrustRecordCollection TrustRecordGetAll();

        #endregion

        #region TrsutUserBank

        int TrustUserBankSet(TrustUserBankCollection users);
        TrustUserBankCollection TrustUserBankGetLastUser(List<int> userId);

        #endregion

        #region MemberMessage

        void MemberMessageSet(MemberMessage mm);
        List<MemberMessage> MemberMessageGetByList(int userId);

        #endregion

        #region   (說明：訊息中心-訊息事件)

        ActionEventInfoCollection ActionEventInfoListGet();
        ActionEventInfo ActionEventInfoGet(string ActionName);
        ActionEventDeviceInfoCollection ActionEventDeviceInfoListGet(int actionEventInfoId);
        ActionEventDeviceInfo ActionEventDeviceInfoGet(int actionEventInfoId, string beaconMacAddress);
        ActionEventDeviceInfo ActionEventDeviceInfoByMajorMinorGet(int actionEventInfoId, int major, int minor);
        ActionEventPushMessageCollection ActionEventPushMessageByFamiGetList();
        ActionEventDeviceInfoCollection ActionEventDeviceInfoGetList(int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter);
        ActionEventDeviceInfoCollection ActionEventDeviceInfoGetByElePower(int actionEventInfoId,
            BeaconPowerLevel powerLevel, string shopCode = "");
        bool ActionEventDeviceInfoSet(ActionEventDeviceInfo actionEventDeviceInfo);
        ActionEventPushMessage ActionEventPushMessageGetByFamiPortId(int famiportId);
        ActionEventPushMessageCollection ActionEventPushMessageGetBySkmBeaconMessageId(int famiportId);
        ActionEventPushMessageCollection ActionEventPushMessageGetList(int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter);

        bool ActionEventPushMessageSet(ActionEventPushMessage actionEventPushMessage);
        ActionEventPushMessageCollection ActionEventPushMessageListGet();
        ActionEventPushMessageCollection ActionEventPushMessageOfNowListGet(DateTime nowDateTime);

        ActionEventPushMessage ActionEventPushMessageGetByActionEventPushMessageId(int actionEventPushMessageId);

        #endregion (說明：訊息中心-訊息事件)

        #region "MemberNoneDeposit"
        void MemberNoneDepositSet(MemberNoneDeposit mnd);
        #endregion

        #region EmailDomain
        EmailDomainCollection EmailDomainGetListByDomain(string domain);
        void EmailDomainSet(EmailDomain email_domain);
        #endregion

        #region MGM
        bool MemberMsgSet(MemberMessage memberMsg);
        MemberFastInfo MemberFastInfoGet(int userId);
        bool MemberFastInfoSet(MemberFastInfo memberFastInf);
        bool MemberContactSet(MemberContact memberContact);
        bool MemberGroupSet(MemberGroup memberGroup);
        bool MemberContactGroupSet(MemberContactGroup memberContactGroup);
        ViewMgmContactGroupCollection ViewMgmContactGroupGetById(int userId);
        MemberContact MemberContactGetByGuid(Guid guid);

        #endregion

        #region OneTimeEvent
        DataTable GetOneTimeEventMailList();
        #endregion

        #region MemberCreditCard
        int MemberCreditCardSet(MemberCreditCard memberCreditCard);
        MemberCreditCard MemberCreditCardGet(int id);
        MemberCreditCard MemberCreditCardGet(Guid guid);
        MemberCreditCard MemberCreditCardGetByGuidUserId(Guid guid, int userId);
        MemberCreditCardCollection MemberCreditCardGetList(int userId);
        MemberCreditCardCollection MemberCreditCardGetBinNullList();
        int MemberCreditCardDeleteByGuidUserId(Guid guid, int userId);
        MemberCreditCard MemberCreditCardGetLastUsed(int userId);
        MemberCreditCardCollection MemberCreditCardGetCardList(int userid);
        int MemberCreditCardSetLastUsed(Guid guid, int userId);

        #endregion

        #region einvoice
        EinvoiceMainCollection GetEinvoiceMainByAllowanceStatusAndOrderId(string orderId, int status);

        void EinvoiceMainSet(EinvoiceMain Einvoice);
        #endregion
    }
}

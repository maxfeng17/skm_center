using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.DataOrm;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.Core
{
    public interface ILocationProvider : IProvider
    {
        #region BuildingDelivery
        BuildingDelivery BuildingDeliveryGet(Guid bizHourGuid, Guid buildingGuid);
        BuildingDeliveryCollection BuildingDeliveryGetList(string column, object value);

        bool BuildingDeliverySet(BuildingDelivery bd);
        bool BuildingDeliveryDelete(int g);
        bool BuildingDeliveryDelete(string column, object value);
        #endregion

        #region Building
        Building BuildingGet(Guid g);
        Building BuildingGet(string column, object value);
        BuildingCollection BuildingGetList();
        BuildingCollection BuildingGetListByArea(int city_id);
        BuildingCollection BuildingGetListByName(string buildingName);
        ListItemCollection BuildingGetListInZoneAndPrefix(int city_id, string prefix);
        bool BuildingSet(Building bd);
        bool BuildingDelete(Guid g);
        #endregion

        #region City
        City CityGet(int id);
        City CityGetByCodeWithParentIsNull(string code);
        City CityGet(int id, bool appendParentName);
        CityCollection CityGetListFromAvailBuilding(AppendMode appendParentName);
        CityCollection CityGetListFromAvailBuilding(AppendMode appendParentName, bool includeTop);
        CityCollection CityGetListFromAvailBuilding(AppendMode appendParentName, bool includeTop, CityStatus status);
        CityCollection CityGetList(int parentId);
        CityCollection CityGetList(int parentId, string orderBy);
        CityCollection CityGetList(params string[] filter);
        CityCollection CityGetListSetOrder(string orderBy, params string[] filter);
        CityCollection CityGetListTopLevel();
        CityCollection CityGetListTopLevel(CityStatus status);
        CityCollection CityGetListTopLevelForCompany(CityStatus status, int topCount);
        /// <summary>
        /// 查詢城市列表，依據目前上架中的熟客卡可用分店判斷
        /// </summary>
        /// <returns></returns>
        CityCollection CityGetListForMembershipCardStore();

        bool CitySet(City c);
        bool CityDelete(int id);
        #endregion

        #region ViewBuildingCity
        ViewBuildingCity ViewBuildingCityGet(Guid g);
        ViewBuildingCityCollection ViewBuildingCityGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter);
        ViewBuildingCityCollection ViewBuildingCityGetListByBusinessHour(int pageStart, int pageLengh, string orderBy, Guid bizHourGuid, ShowListMode mode, SqlGeography geo, int distance, params string[] filter);
        #endregion

        #region ViewBuildingSellerCount
        ViewBuildingSellerCountCollection ViewBuildingSellerCountGetList(string colum, object vale, string orderBy);
        #endregion

        #region 取得訂閱/會員所需之城市名稱
        string CityNameGetWithSubscription(int cityId);
        #endregion

        #region Wechat
        /// <summary>
        /// 存入wechat目前位置
        /// </summary>
        /// <param name="wechat_location"></param>
        void WeChatLocationSet(WechatLocation wechat_location);
        /// <summary>
        /// 由wechat的openid取得位置
        /// </summary>
        /// <param name="open_id"></param>
        /// <returns></returns>
        WechatLocation WeChatLocationGet(string open_id);
        #endregion

        #region DeviceIdentifierInfo

        int? DeviceIdentyfierInfoGetCurrentDeviceId(string identifierCode);
        DeviceIdentyfierInfo DeviceIdentyfierInfoGet(string identifierCode);
        DeviceIdentyfierInfoCollection DeviceIdentyfierInfoListGet(string identifierCode);
        bool DeviceIdentyfierInfoSet(DeviceIdentyfierInfo deviceIdentyfierInfo);
        DevicePushRecordCollection DevicePushRecordListGet(int deviceid, int? memberUniqueId);
        DevicePushRecordCollection DevicePushRecordGetListByDeviceId(int deviceid);
        ViewPushRecordMessageCollection ViewPushRecordMessageGetListByDeviceAndNoOwner(int deviceid, int days);
        ViewPushRecordMessageCollection ViewPushRecordMessageGetListByMember(int userId, int days);
        void DevicePushRecordSetRemove(int devicePushId);
        int DevicePushRecordSetMemberIfNoOwner(int deviceId, int userId);

        DevicePushRecordCollection SkmDevicePushRecordListGet(int memberUniqueId);

        DevicePushRecordCollection DevicePushRecordGetListByUserId(int memberUniqueId);
        DevicePushRecord DevicePushRecordGet(int devicePushRecordId);
        bool DevicePushRecordSet(DevicePushRecord devicePushRecord);
        int DevicePushRecordSetNotExist(DevicePushRecord record);
        int DevicePushRecordCollectionSet(DevicePushRecordCollection devicePushRecordCollection);

        /// <summary>
        /// PushRecordId, ActionId, PushTime, ReadTime
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="deviceId"></param>
        /// <param name="hourVal"></param>
        /// <returns>PushRecordId, ActionId, PushTime, ReadTime</returns>
        List<DevicePushRecordReadModel> DevicePushRecordGetReadRecordWithInHour(int userId, int deviceId,
            int hourVal);

        #endregion DeviceIdentifierInfo

        #region mrt_location_info (MRT)
        MrtLocationInfoCollection MrtLocationInfoCollectionGetNearByLocation(double latitude, double longitude);

        #endregion mrt_location_info (MRT)

        #region DevicePushOrder

        bool DevicePushOrderSet(DevicePushOrder order);
        bool TryGetDevicePushOrderInfo(int id, out int orderCount, out int turnover);


        #endregion

    }
}

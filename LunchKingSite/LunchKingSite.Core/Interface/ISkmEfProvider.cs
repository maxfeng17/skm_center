﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Core
{
    public interface ISkmEfProvider : IProvider
    {
        int SaveSkmTokenTracking(SkmTokenTracking skmTokenTracking);
        SkmActivity GetSkmActivity(string token);
        SkmActivity GetSkmActivityWithJoinDate(string token);
        List<SkmActivityItem> GetSkmActivityItem(int activityId);
        SkmActivityItem GetSkmActivityItem(int activityId, Guid bid);
        List<SkmActivityDateRange> GetSkmActivityDateRange(int activityId);
        SkmActivityDateRange GetSkmActivityDateRangeNow(int activityId);
        List<int> GetSkmActivityDateShowRangeNow(int activityId);
        List<SkmActivityLog> GetSkmActivityLog(int activityId, int userId);
        List<SkmActivityLog> GetSkmActivityLogByUserId(int userId);
        List<ViewSkmActivityLog> GetViewSkmActivityLog(int activityId, int userId);
        List<ViewSkmActivityLog> GetViewSkmActivityLog(int activityId, int userId, DateTime minDate, DateTime maxDate);
        bool SaveSkmActivityLog(SkmActivityLog log);
        List<ViewSkmPponOrder> GetViewSkmPponOrder(int userId, int expiredMonth);
        List<ViewSkmPayPponOrder> GetViewSkmPayPponOrder(int userId, int expiredMonth);

        #region skm_serial_number

        SkmSerialNumber GetSkmSerialNumber(string storeFixcode);
        bool SetSkmSerialNumberLog(SkmSerialNumberLog log);
        bool UpdateSkmSerialNumber(int id, bool isProcessing, bool isTradeComplete);
        /// <summary>
        /// 更新Skm Serial Number info
        /// </summary>
        /// <param name="skmSerialNumber">Skm Serial Number info</param>
        /// <returns>是否更新成功</returns>
        bool UpdateSkmSerialNumber(SkmSerialNumber skmSerialNumber);
        /// <summary>
        /// 依Skm Trade No取得還在交易中的Skm Serial Number info
        /// </summary>
        /// <param name="skmTradeNo">Skm Trade No</param>
        /// <returns>Skm Serial Number info</returns>
        SkmSerialNumber GetProcessingSkmSerialNumberBySkmTradNo(string tempSkmTradeNo);
        /// <summary>
        /// 依Skm Trade No取得Skm Serial Number info
        /// </summary>
        /// <param name="skmTradeNo">Skm Trade No</param>
        /// <returns>Skm Serial Number info</returns>
        SkmSerialNumber GetSkmSerialNumberBySkmTradNo(string tempSkmTradeNo);

        #endregion

        #region skm_pay_banner

        List<SkmPayBanner> GetSkmPayBannerList();
        SkmPayBanner GetSkmPayBanner(int id);
        bool SetSkmPayBanner(SkmPayBanner banner, string modifyUserName);
        bool DelSkmPayBanner(int id);
        List<SkmPayBanner> GetSkmPayBannerSortData();
        List<SkmPayBanner> GetSkmPayBannerToday();

        #endregion skm_pay_banner

        #region Skm_pay_order
        
        /// <summary>
        /// 設定skm pay訂單資料
        /// </summary>
        /// <param name="skmPayOrder">要設定的skm pay訂單資料</param>
        /// <returns>設定後的skm pay訂單資料</returns>
        bool SetSkmPayOrder(SkmPayOrder skmPayOrder);
        
        bool SetSkmPayOrders(List<SkmPayOrder> skmPayOrders);
        /// <summary>
        /// 依 preTransNo 取回 Skm Pay Order
        /// </summary>
        /// <param name="preTransNo"></param>
        /// <returns></returns>
        SkmPayOrder GetSkmPayOrderByPreTransNo(string preTransNo);
        SkmPayOrder GetSkmPayOrderByOrderGuid(Guid orderGuid);
        SkmPayOrder GetSkmPayOrderByOrderNo(string orderNo);
        ViewSkmPayOrder GetViewSkmPayOrderByOrderNo(string orderNo, string shopCode);
        ViewSkmPayOrder GetViewSkmPayOrderByTradeNo(string tradeNo, string shopCode);
        ViewSkmPayOrderQueryUmallGift GetViewSkmPayOrderQueryUmallGift(string orderNo);

        /// <summary>
        /// 依TrustId取得Skm Pay訂單資訊
        /// </summary>
        /// <param name="trustId">依TrustId取得Skm</param>
        /// <returns>Skm Pay訂單資訊</returns>
        SkmPayOrder GetSkmPayOrderByTrustId(Guid trustId);
        /// <summary>
        /// 依交易TOKEN取得SkmPay訂單資訊
        /// </summary>
        /// <param name="paymentToken">交易TOKEN</param>
        /// <returns>SkmPay訂單資訊</returns>
        SkmPayOrder GetSkmPayOrderByPaymentToken(string paymentToken);
        bool SetSkmPayOrderTransLog(SkmPayOrderTransLog transLog);
        /// <summary>
        /// 依訂單狀態清單取得訂單資料清單
        /// </summary>
        /// <param name="skmPayOrderFaildNeedRetryStatus">訂單狀態清單</param>
        /// <returns>訂單資料清單</returns>
        List<SkmPayOrder> GetSkmPayOrdersByOrderStatus(List<SkmPayOrderStatus> skmPayOrderStatus);
        /// <summary>
        /// 取得OTP逾期訂單
        /// </summary>
        /// <param name="timeoutSeconds"></param>
        /// <returns></returns>
        List<SkmPayOrder> GetSkmPayOrdersByOtpTimeout(int timeoutSeconds);
        #endregion

        #region Skm_pay_parking_order

        bool SetSkmPayParkingOrder(SkmPayParkingOrder skmParkingOrder);
        SkmPayParkingOrder GetSkmParkingOrderByPreTransNo(string preTransNo);
        SkmPayParkingOrder GetSkmParkingOrderByPaymentToken(string paymentToken);
        SkmPayParkingOrder GetSkmParkingOrder(string orderNo, string parkingTicketNo);
        List<SkmPayParkingOrder> GetSkmParkingOrderByIds(List<int> orderIds);
        bool SetSkmPayParkingOrderTransLog(SkmPayParkingOrderTransLog transLog);
        SkmPayParkingOrderTransLog GetSkmPayParkingOrderTransLog(int orderId, int userId, string action);
        List<SkmPayParkingOrderTransLog> GetSkmPayParkingOrderTransLogList(string action, bool isSuccess);

        List<SkmPayParkingOrder> GetSkmPayParkingOrdersByOrderStatus(List<SkmPayOrderStatus> orderStatusList
            , string memberCardNo = null
            , string shopCode = null
            , string parkingTicketNo = null
            , string parkingToken = null
            , string sd = null
            , string ed = null);
        #endregion

        #region Skm_Pay_Order_Detail
        /// <summary> 設定skm pay訂單細項資料清單 </summary>
        /// <param name="skmPayOrderDetails">skm pay訂單細項資料清單</param>
        bool SetSkmPayOrderDetails(IEnumerable<SkmPayOrderDetail> skmPayOrderDetails);

        SkmPayOrderDetail GetskmPayOrderDetailByTrustId(Guid trustId);
        #endregion

        #region skm_pay_invoice
        /// <summary>
        /// 設定skm pay發票資料
        /// </summary>
        /// <param name="skmPayInvoice">要設定的skm pay發票資料</param>
        /// <returns>設定後的skm pay發票資料</returns>
        bool SetSkmPayInvoice(SkmPayInvoice skmPayInvoice);
        /// <summary>
        /// 依 skm_pay_order.id 取回發票資料
        /// </summary>
        /// <param name="skmPayOrderId"></param>
        /// <returns></returns>
        List<SkmPayInvoice> GetSkmPayInvoice(int skmPayOrderId, string platFormId);
        /// <summary>
        /// 查詢發票LIST
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        List<SkmPayInvoice> GetSkmPayInvoiceList(List<int> ids, string platFormId);
        /// <summary>
        /// 取得銷售日期為傳入日期且傳送成功發票資訊清單(預售&退貨)(精選優惠)
        /// </summary>
        /// <param name="sellStrtDate">要取得的開始日期</param>
        /// <param name="sellEndDate">要取得的結束日期</param>
        /// <returns>發票資訊清單</returns>
        List<ViewSkmPayClearPos> GetSentSuccessSkmPayInvoiceBySellDate(DateTime sellStrtDate, DateTime sellEndDate);
        /// <summary>
        /// 取得銷售日期為傳入日期且傳送成功發票資訊清單(預售&退貨)(停車)
        /// </summary>
        /// <param name="sellStrtDate">要取得的開始日期</param>
        /// <param name="sellEndDate">要取得的結束日期</param>
        /// <returns>發票資訊清單</returns>
        List<ViewSkmPayClearPos> GetSentSuccessSkmPayParkingInvoiceBySellDate(DateTime sellStrtDate,
            DateTime sellEndDate);
        #endregion

        #region skm_pay_invoice_sell_detail
        /// <summary>
        /// 新增/修改發票銷售明細
        /// </summary>
        /// <param name="skmPayInvoiceSellDetail">發票銷售明細</param>
        /// <returns>是否新增/修改成功</returns>
        bool SetSkmPayInvoiceSellDetail(SkmPayInvoiceSellDetail skmPayInvoiceSellDetail);
        /// <summary>
        /// 依照發票流水號取得發票銷售明細清單
        /// </summary>
        /// <param name="invoiceId">發票流水號</param>
        /// <returns>發票銷售明細清單</returns>
        List<SkmPayInvoiceSellDetail> GetSkmPayInvoiceSellDetailsByInvoiceId(int invoiceId);
        #endregion

        #region skm_pay_invoice_tender_detail
        /// <summary>
        /// 新增/修改發票付款明細
        /// </summary>
        /// <param name="skmPayInvoiceTenderDetails">發票付款明細</param>
        /// <returns>是否新增/修改成功</returns>
        bool SetSkmPayInvoiceTenderDetail(IEnumerable<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails);
        /// <summary>
        /// 依照發票流水號取得發票付款明細清單
        /// </summary>
        /// <param name="invoiceId">發票流水號</param>
        /// <returns>發票付款明細清單</returns>

        List<SkmPayInvoiceTenderDetail> GetSkmPayInvoiceTenderDetailsByInvoiceId(int invoiceId);
        #endregion

        #region skm_pay_clear_pos_trans_log
        /// <summary>
        /// 儲存 當日銷售總額 電文傳送紀錄
        /// </summary>
        /// <param name="skmPayClearPosTransLog">當日銷售總額 電文傳送紀錄</param>
        /// <returns>是否儲存成功</returns>
        bool SetSkmPayClearPosTransLog(SkmPayClearPosTransLog skmPayClearPosTransLog);
        /// <summary>
        /// 依銷售日期取得失敗的上傳當日銷售總額清單
        /// </summary>
        /// <param name="startDate">銷售日期起始</param>
        /// <param name="endDate">銷售日期結束</param>
        /// <returns>上傳當日銷售總額清單</returns>
        List<SkmPayClearPosTransLog> GetFailedSkmPayClearPosBySellDay(DateTime startDate, DateTime endDate, string platFormId);
        /// <summary>
        /// 依銷售日期判斷是否有執行過上傳當日銷售總額
        /// </summary>
        /// <param name="date">銷售日期</param>
        /// <returns></returns>
        bool IsPostSkmPayClearPos(DateTime sellDate, string platFormId);
        #endregion

        #region Skm_Pay_OTP
        /// <summary>
        /// 設定Skm Pay OTP資料
        /// </summary>
        /// <param name="skmPayOtp">Skm Pay OTP資料</param>
        /// <returns>是否設定成功</returns>
        bool SetSkmPayOtp(SkmPayOtp skmPayOtp);
        /// <summary>
        /// 依SkmPay訂單流水號取得Skm Pay Otp資料
        /// </summary>
        /// <param name="skmPayOrderId">SkmPay訂單流水號</param>
        /// <returns>Skm Pay Otp資料</returns>
        SkmPayOtp GetSkmPayOtpBySkmPayOrderId(int skmPayOrderId);
        #endregion

        #region view_skm_store_display_name
        /// <summary>
        /// 取得APP顯示館位名稱資料清單
        /// </summary>
        /// <returns>顯示館位名稱資料清單</returns>
        List<ViewSkmStoreDisplayName> GetViewSkmStoreDisplayNames();

        #endregion

        #region ViewExternalDealStoreDisplayName

        /// <summary>
        /// 依照檔次編號查詢APP顯示館名清單
        /// </summary>
        /// <param name="dealGuid">檔次編號</param>
        /// <returns>APP顯示館名清單</returns>
        List<ViewExternalDealStoreDisplayName> GetViewExternalDealStoreDisplayNameByDealGuid(Guid dealGuid);
        
        #endregion

        #region external_combo_deal

        bool SetExternalDealCombo(List<ExternalDealCombo> deals, bool isUpdate);
        List<ExternalDealCombo> GetExternalDealComboByMainBids(List<Guid> bids);
        List<ExternalDealCombo> GetExternalDealCombo(Guid externalGuid);
        ExternalDealCombo GetExternalDealComboByBid(Guid bid);
        List<ExternalDealCombo> GetExternalDealComboByMainBid(Guid bid);
        /// <summary>
        /// 取得SKM檔次子項資料
        /// </summary>
        /// <param name="externalDealGuid">SKM檔次編號</param>
        /// <param name="shopCode">館號</param>
        /// <returns>SKM檔次子項資料</returns>
        ExternalDealCombo GetExternalDealComboByGuidShopCode(Guid externalDealGuid, string shopCode);
        /// <summary>
        /// 更新子檔次憑證數量
        /// </summary>
        /// <param name="externalDealGuid">檔次編號</param>
        /// <param name="shopCode">館別代號</param>
        /// <param name="orderTotalLimit">憑證數量</param>
        /// <returns>是否更新成功</returns>
        bool SetExternalDealComboQty(Guid externalDealGuid, string shopCode, int orderTotalLimit);
        #endregion external_combo_deal

        #region skm_in_app_message

        bool SetSkmInAppMessage(SkmInAppMessage msg);
        bool BulkInsertSkmInAppMessage(List<SkmInAppMessage> msgs);
        List<SkmInAppMessage> GetSkmInAppMessage(int userId);
        bool SetInAppMessageRead(int id);
        bool SetInAppMessageDel(int id);

        #endregion skm_in_app_message

        #region  skm_ad_borad

        SkmAdBoard GetSkmAdBoard();
        /// <summary>
        /// 依廣告看板編號取得廣告看板資訊
        /// </summary>
        /// <param name="id">廣告看板編號</param>
        /// <returns></returns>
        SkmAdBoard GetSkmAdBoardById(int id);
        List<SkmAdBoard> GetSkmAdBoardList();
        /// <summary>
        /// 檢查此時間區間是否已有廣告看板資料
        /// </summary>
        /// <param name="startDate">開始時間</param>
        /// <param name="endDate">結束時間</param>
        /// <param name="nowAdBoardId">目前的廣告看板編號(若是新增填0)</param>
        /// <returns>是否已存在</returns>
        bool CheckAdBoardIsExistByDate(DateTime startDate, DateTime endDate,int nowAdBoardId);
        bool DeleteAdBoards(int ids);
        bool InsertAdBoard(SkmAdBoard data);
        /// <summary>
        /// 更新廣告看板狀態(顯示/隱藏)
        /// </summary>
        /// <param name="id">廣告看板編號</param>
        /// <param name="isEnable">是否顯示</param>
        /// <returns>更新後的廣告看板資訊</returns>
        SkmAdBoard UpdateAdBoardIsEnable(int id, bool isEnable);
        /// <summary>
        /// 更新廣告看板資料
        /// </summary>
        /// <param name="skmAdBoard">要更新的廣告看板資料</param>
        /// <returns>是否更新成功</returns>
        bool UpdatAdBoard(SkmAdBoard skmAdBoard);
        #endregion

        #region view_skm_deal_quantity

        /// <summary>
        /// 依贈品編號查詢各檔次的庫存量清單
        /// </summary>
        /// <param name="exchangeItemId">贈品編號</param>
        /// <returns>各檔次的庫存量清單</returns>
        List<ViewSkmDealQuantity> GetDealQuantityByExchangeItemId(string exchangeItemId);
        /// <summary>
        /// 依BusinessHourGuid取得庫存量資訊
        /// </summary>
        /// <param name="value">依BusinessHourGuid取得庫存量資訊</param>
        /// <returns>庫存量資訊</returns>
        ViewSkmDealQuantity DealQuantityByBid(Guid bid);
        #endregion

        #region view_skm_deal_guid_quantity
        /// <summary>
        /// 依檔次編號取得檔次數量相關資訊清單
        /// </summary>
        /// <param name="guid">檔次編號</param>
        /// <returns>檔次數量相關資訊清單</returns>
        List<ViewSkmDealGuidQuantity> GetExternalQuantityByGuid(Guid guid);
        #endregion

        #region ExternalDealInventoryLog
        /// <summary>
        /// 新增更新贈品庫存紀錄
        /// </summary>
        /// <param name="skmSetExternalDealInventoryLog">更新贈品庫存資料</param>
        /// <returns>是否新增成功</returns>
        bool InsertSkmSetExternalDealInventoryLog(SkmSetExternalDealInventoryLog skmSetExternalDealInventoryLog);
        /// <summary>
        /// 更新更新贈品庫存紀錄
        /// </summary>
        /// <param name="skmSetExternalDealInventoryLog">更新贈品庫存資料</param>
        /// <returns>是否更新成功</returns>
        bool UpdateSkmSetExternalDealInventoryLog(SkmSetExternalDealInventoryLog skmSetExternalDealInventoryLog);
        #endregion

        #region SkmCustomizedBoard
        /// <summary>
        /// 取得自訂版位基本資訊清單
        /// </summary>
        /// <param name="storeGuid">店編號</param>
        /// <returns>自訂版位基本資訊清單</returns>
        List<SkmCustomizedBoard> GetSkmCustomizedBoardList(Guid storeGuid);

        /// <summary>
        /// 依編號取得自訂版位資訊
        /// </summary>
        /// <param name="id">編號</param>
        /// <returns>自訂版位資訊</returns>
        SkmCustomizedBoard GetSkmCustomizedBoardById(int id);
        
        /// <summary>
        /// 更新自訂版位資訊
        /// </summary>
        /// <param name="skmCustomizedBoard">自訂版位資訊</param>
        /// <returns>是否更新成功</returns>
        bool UpdateSkmCustomizedBoard(SkmCustomizedBoard skmCustomizedBoard);
        #endregion

        #region ViewSkmCashTrustLogNotVerified
        /// <summary>
        /// 取得新光未核銷且未取消訂單憑證
        /// </summary>
        /// <param name="days">幾天後</param>
        /// <param name="type">StartDate or EndDate</param>
        /// <returns>取得相關資料</returns>
        List<ViewSkmCashTrustLogNotVerified> GetSkmCashTrustLogNotVerifiedByDay(int days, SkmCashTrustLogNotVerifiedType type);
                
        #endregion
        
        #region SkmInstallEvent

        List<ViewSkmInstallEventList> GetViewSkmInstallEventListByEventTitle(string eventTitle);
        List<SkmInstallEvent> GetSkmInstallEventList();
        SkmInstallEvent GetSkmInstallEventById(int id);
        bool AddOrUpdateSkmInstallEvent(SkmInstallEvent installEvent);
        bool AddOrUpdateSkmInstallEventBank(SkmInstallEventBank installEventBank);
        List<ViewSkmInstallEventBank> GetViewSkmInstallEventBankByDateTime(DateTime dateTime);
        List<ViewSkmInstallEventBank> GetViewSkmInstallEventBankByEventId(int eventId);
        List<ViewSkmInstallEventBank> GetViewSkmInstallEventBankByEventBankId(int eventBankId);
        List<SkmInstallEventPeriod> GetSkmInstallEventPeriodsByEventBankId(int eventBankId);
        List<SkmInstallEventBank> GetSkmInstallEventBankByEventId(int eventId);
        SkmInstallEventBank GetSkmInstallEventBankById(int id);
        bool DeleteSkmInstallEventPeriodsByEventBankId(int eventBankId);
        bool BulkInsertSkmInstallEventPeriods(List<SkmInstallEventPeriod> data);

        #endregion

        #region SKM 總部查詢資料

        /// <summary>
        /// 取得skmpay訂單交易明細
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>        
        List<SkmpayOrderInfo> ExecFnGetSkmpayOrderInfo(DateTime startDate, DateTime endDate);

        /// <summary>
        /// 取得Skm上架商品
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        List<SkmOnlineDeals> ExecFnGetSkmOnlineDeals(DateTime startDate, DateTime endDate);

        List<SkmCurationEvents> ExecFnGetSkmCurationEvents(DateTime startDate, DateTime endDate);

        List<SkmCurationDeals> ExecFnGetSkmCurationDeals(DateTime startDate, DateTime endDate);

        #endregion

        #region 預防超抽獎
        bool CheckPrizeOverdraw(int itemId, int qty, int userId, int timeoutSec, int prizeLimit, out int preventId, out string msg);
        #endregion
    }
}

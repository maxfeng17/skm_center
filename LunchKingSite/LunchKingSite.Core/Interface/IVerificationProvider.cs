﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core
{
    public interface IVerificationProvider : IProvider
    {
        #region cash_trust_log

        CashTrustLogCollection GetCashTrustLogGetListByBidAndSeller(Guid bid, Guid sellerGuid);
        CashTrustLogCollection GetCashTrustLogGetListByBidAndSellerAndStore(Guid bid, Guid sellerGuid, Guid storeGuid);
        CashTrustLogCollection CashTrustLogGetListByStore(List<Guid> stores);
        /// <summary>
        /// 商家後臺_核銷對帳系統:根據輸入之bid取出資料並依bid統計已成檔之憑證檔次之銷售數量
        /// </summary>
        /// <param name="bids">business_hour_guid</param>
        /// <returns></returns>
        List<VendorDealSalesCount> GetPponVerificationSummary(IEnumerable<Guid> bids);
        List<VendorDealSalesCount> GetPponVerificationSummaryFromArchive(List<Guid> bids);

        /// <summary>
        /// 商家後臺_核銷對帳系統:根據輸入之bid取出資料並依bid及store_guid統計已成檔之憑證檔次之銷售數量
        /// </summary>
        /// <param name="bids">business_hour_guid</param>
        /// <returns></returns>
        List<VendorDealSalesCount> GetPponVerificationSummaryGroupByStore(IEnumerable<Guid> bids);

        /// <summary>
        /// 商家後臺_出貨管理:根據輸入之bids取出已成檔之宅配檔次之銷售數量
        /// </summary>
        /// <param name="bids">business_hour_guid</param>
        /// <returns></returns>
        List<VendorDealSalesCount> GetPponToHouseDealSummary(IEnumerable<Guid> bids);

        #endregion cash_trust_log

        #region ViewVbsCashTrustLogStatusLogInfo

        /// <summary>
        /// 由cash_trust_log及cash_trust_status中取得憑證的 status 及最後核銷時間、退貨時間等資料
        /// </summary>
        /// <param name="productGuid">Guid : P好康 - business_hour Guid 
        ///                                  品生活 - hi_deal_product Guid
        /// </param>
        /// <param name="orderClassification">OrderClassification</param>
        /// <param name="couponNumber">憑證號碼</param>
        /// <param name="storeFilter">總店、分店或分店與總店</param>
        /// <param name="storeGuidList">string store_guid list(以逗號隔開)</param>
        /// <returns></returns>
        ViewVbsCashTrustLogStatusLogInfoCollection GetCashTrustLogGetListByProductGuidAndStore(
            Guid productGuid, OrderClassification orderClassification, string couponNumber, VbsStoreFilter storeFilter, string storeGuidList);

        /// <summary>
        /// 由cash_trust_log及cash_trust_status中取得 status 及最後核銷時間、退貨時間等資料
        /// </summary>
        /// <param name="productGuid">Guid : P好康 - business_hour Guid 
        ///                                  品生活 - hi_deal_product Guid
        /// </param>
        /// <param name="orderClassification">OrderClassification</param>
        /// <returns></returns>
        ViewVbsCashTrustLogStatusLogInfoCollection GetCashTrustLogGetListByProductGuid(Guid productGuid, OrderClassification orderClassification);

        ViewVbsCashTrustLogStatusLogInfo ViewVbsCashTrustLogStatusLogInfoGet(Guid trustId);
        #endregion ViewVbsCashTrustLogStatusLogInfo

        #region store_acct_info
        /// <summary>
        /// 是否有合法的APP物件
        /// </summary>
        /// <param name="strAcct">帳號(acct_code + acct_no)</param>
        /// <param name="strPwd">密碼</param>
        /// <param name="strImei">手機機碼(IMEI)</param>
        /// <param name="strImsi">SIM卡號(IMSI)</param>
        /// <param name="strRegCode">註冊碼</param>
        /// <returns>bool: true/false(是/否)</returns>
        bool HaveLegalAcctInfoByAppInfo(string strAcct, string strPwd, string strImei, string strImsi, string strRegCode);

        /// <summary>
        /// 註冊
        /// </summary>
        bool RegistAcctInfo(StoreAcctInfo sai);
        /// <summary>
        /// 重新註冊，要給一組新的註冊碼
        /// </summary>
        bool ResetRegistAcctInfo(StoreAcctInfo sai, string strNewRegCode);
        /// <summary>
        /// 清除註冊
        /// 會清空兩個記錄機碼的欄位(ISME, IMEI)，將狀態設定為未設定，同時要重新定訂註冊碼
        /// </summary>
        bool UndoRegistAcctInfo(StoreAcctInfo sai, string strRegCode1, string strRegCode2);

        StoreAcctInfo GetAcctInfoByAcct(string strAcct, string strRegCode);

        /// <summary>
        /// 根據輸入帳密取得帳號資料，帳密有大小寫之分！
        /// </summary>
        /// <param name="strAcct">帳號(acct_code + acct_no)</param>
        /// <param name="strPwd">密碼</param>
        /// <returns></returns>
        StoreAcctInfo GetAcctInfoByAcct(string strAcct, string strPwd, string strImei, string strImsi);
        StoreAcctInfo GetAcctInfoByAcctWithReRegist(string strAcct, string strOldRegCode, string strNewRegCode, string strImei, string strImsi);
        StoreAcctInfo GetAcctInfoByAcctWithUndoRegist(string strAcct, string strRegCode, string strImei, string strImsi);
        #endregion
        #region store_acct_info_log

        bool SetStoreAcctInfoLog(StoreAcctInfoLog sail);
        #endregion
        #region store_acct_special_info

        StoreAcctSpecialInfo GetStoreAcctSepcialInfoByAcct(string strAcct, string strPwd);
        #endregion

        #region verification_log

        /// <summary>
		/// 反核銷
		/// </summary>
    	bool UndoVerificationLog(int couponId, OrderClassification classification = OrderClassification.LkSite);
		
        /// <summary>
        /// 取出一筆符合 Coupon Id 以及 VerificationStatus 之 VerifcationLog
        /// </summary>
        /// <returns>一筆 VerifcationLog</returns>
		VerificationLog GetVerificationLogByCouponIdAndStatus(int? intCouponId, int intStatus, OrderClassification classification = OrderClassification.LkSite);
        #endregion
        #region view_ppon_coupon

        ViewPponCouponCollection GetCouponInfoBySNoAndCodeAndSeller(string strSequence, string strCode, Guid sellerGuid);
        ViewPponCouponCollection GetCouponInfoBySNoAndCodeAndSellerAndStoreGuid(string strSequence, string strCode, Guid sellerGuid, Guid storeGuid);
        #endregion

        #region view_seller_store_bid_menu

        ViewSellerStoreBidMenuCollection GetDealsBySeller(Guid sellerGuid);
        ViewSellerStoreBidMenuCollection GetDealsBySellerAndStoreGuid(Guid sellerGuid, Guid storeGuid);

        #endregion

        #region VbsBulletinBoard

        VbsBulletinBoard VbsBulletinBoardGetById(int id);
        VbsBulletinBoardCollection VbsBulletinBoardGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter);
        bool VbsBulletinBoardSet(VbsBulletinBoard board);
        bool VbsBulletinBoardDelete(int id);

        #endregion

        #region HiDeal

        bool HaveHiDealProd(string strAcct, string strPwd, Guid sellerGuid);

        #region view_hideal_verification_info

        ViewHiDealVerificationInfoCollection GetHiDealCouponInfoBySNoAndCodeAndSeller(string strSequence, string strCode, Guid sellerGuid);
        ViewHiDealVerificationInfoCollection GetHiDealCouponInfoBySNoAndCodeAndSellerAndStoreGuid(string strSequence, string strCode, Guid sellerGuid, Guid storeGuid);

        ViewHiDealVerificationInfoCollection GetHiDealsBySeller(Guid sellerGuid);
        ViewHiDealVerificationInfoCollection GetHiDealsBySellerAndStoreGuid(Guid sellerGuid, Guid storeGuid);

        /// <summary>
        /// 商家後臺_核銷對帳系統:根據輸入之hi_deal_product id取出資料並依hi_deal_product guid 統計已成檔之憑證檔次之銷售數量
        /// </summary>
        /// <param name="pids"></param>
        /// <returns>VendorDealSalesCount.StoreGuid does not return null, but Guid.Empty instead.</returns>
        List<VendorDealSalesCount> GetHiDealVerificationSummary(IEnumerable<int> pids);

        /// <summary>
        /// 商家後臺_核銷對帳系統:根據輸入之hi_deal_product id取出資料並依hi_deal_product guid及store_guid 統計已成檔之憑證檔次之銷售數量
        /// </summary>
        /// <param name="pids"></param>
        /// <returns>VendorDealSalesCount.StoreGuid does not return null, but Guid.Empty instead.</returns>
        List<VendorDealSalesCount> GetHiDealVerificationSummaryGroupByStore(IEnumerable<int> pids);

        /// <summary>
        /// 商家後臺_出貨管理:根據輸入之bids取出已成檔之宅配檔次之銷售總數、未出貨、已出貨
        /// 及退貨(兌換開始後退貨)數量
        /// </summary>
        /// <param name="pids">多筆hi_deal_product id</param>
        /// <returns></returns>
        List<VendorDealSalesCount> GetHidealToHouseDealSummary(IEnumerable<int> pids);
       
        #endregion view_hideal_verification_info
        
        #endregion HiDeal

        #region 核銷列表

        DataTable GetVerifiedListWithPpon(bool isStore, Guid bGuid, Guid sellerGuid, Guid storeGuid,
            DateTime startTime, DateTime endTime, VerificationQueryStatus enumQueryStatus);
        DataTable GetVerifiedListWithPiinlife(bool isStore, int dealId, int productId, Guid sellerGuid, Guid storeGuid,
            DateTime startTime, DateTime endTime, VerificationQueryStatus enumQueryStatus);

        /// <summary>
        /// 取出P好康憑證核銷紀錄資料 可限制讀取特定檔次或分店下之憑證資料
        /// couponSequence 和trustId 為擇其一使用
        /// </summary>
        /// <param name="couponSequence">string coupon sequence number 後四碼</param>
        /// <param name="couponCode">string coupon code</param>
        /// <param name="trustId">cash_trust_log.trust_id</param>
        /// <param name="isPezEvent"></param>
        /// <returns></returns>
        List<VerifiedCouponData> GetVerifiedCouponDataWithPpon(string couponSequence, string couponCode, Guid trustId,bool isPezEvent = false);

        /// <summary>
        /// 取出品生活憑證核銷紀錄資料 可限制讀取特定檔次或分店下之憑證資料
        /// couponSequence 和trustId 為擇其一使用
        /// </summary>
        /// <param name="couponSequence">string coupon sequence number 後四碼</param>
        /// <param name="couponCode">string coupon code</param>
        /// <param name="trustId">cash_trust_log.trust_id</param>
        /// <param name="isPezEvent"></param>
        /// <returns></returns>
        List<VerifiedCouponData> GetVerifiedCouponDataWithPiinLife(string couponSequence, string couponCode, Guid trustId, bool isPezEvent = false);
        
        #endregion

        #region WPF AP
        StoreAcctInfoCollection GetAcctInfoByAcctForWPF(string acctcode, string acctno, string password);
        #endregion

        List<Guid> StoreGuidsGetListByAccountId(string accountId);
        List<VendorBillingProductModel> DealGuidsGetListByAccountId(string accountId);

        #region 成套商品

        bool GroupCouponLogSet(GroupCouponLog log);
        bool GroupCouponCheckExist(Guid orderGuid, string timestamp);

        #endregion
    }

    public class VendorBillingProductModel
    {
        public Guid Guid { get; set; }
        public OrderClassification OrderClassification { get; set; }
    }

    public class VerifiedCouponData
    {
        public Guid TrustId { get; set; }
        public string CouponSequence { get; set; }
        public string CouponCode { get; set; }
        public int Status { get; set; }
        public int SpecialStatus { get; set; }
        public DateTime ModifyTime { get; set; }
        public string MemberName { get; set; }
        public DateTime UseStartTime { get; set; }
        public DateTime UseEndTime { get; set; }
        public int DealId { get; set; }
        public Guid DealGuid { get; set; }
        public string DealName { get; set; }
        public string ItemName { get; set; }
        public Guid? StoreGuid { get; set; }
        public bool NoRestrictedStore { get; set; }
    }

    public class VendorDealSalesCount
    {
        public Guid MerchandiseGuid { get; set; }

        public Guid? StoreGuid { get; set; }

        public int UnverifiedCount { get; set; }

        public int VerifiedCount { get; set; }

        public int ReturnedCount { get; set; }

        public int BeforeDealEndReturnedCount { get; set; }

        public int ForceReturnCount { get; set; }

        public int ForceVerifyCount { get; set; }

        public bool AnyData()
        {
            return UnverifiedCount + VerifiedCount + ReturnedCount +
                BeforeDealEndReturnedCount + ForceReturnCount + ForceVerifyCount > 0;
        }
    }
}

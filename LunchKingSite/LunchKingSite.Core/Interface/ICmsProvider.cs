using LunchKingSite.DataOrm;
using System.Data;
using System;
using System.Collections.Generic;

namespace LunchKingSite.Core
{
    public interface ICmsProvider : IProvider
    {
        #region CmsContent
        CmsContent CmsContentGet(string name);
        CmsContent CmsContentGet(int id);
        void CmsContentDelete(int id);
        void CmsContentSet(CmsContent content);
        CmsContentCollection CmsContentGetList(int pageStart, int pageSize, string orderBy, params string[] filter);
        #endregion

        #region Audit
        Audit AuditGet(int id);
        AuditCollection AuditGetList(string referenceId, bool showAll);
        AuditCollection AuditGetListByIdAndType(string referenceId, AuditType auditType);

        bool AuditSet(Audit record);
        #endregion

        #region Schema
        string SchemaGetVersion();
        int SchemaGetBranchVersion();
        #endregion

        #region UserTrack
        bool UserTrackSet(UserTrack track);
        bool UserTrackSet(string sessionId, string displayName, string url, string queryString, string context);
        #endregion
         

        #region CmsRandomContent
        void CmsRandomContentSet(CmsRandomContent content);
        void CmsRandomCitySet(CmsRandomCity city);
        void CmsRandomContentDelete(int id);
        void CmsRandomContentUpdate(CmsRandomContent content);
        void CmsRandomCityUpdate(CmsRandomCity city);
        void CmsRandomSortUpdate(CmsRandomCity city);
        void CmsRandomCityDelete(int id);
        void UpdateCmsRandomContentStatus(int brandId, bool status);
        DataTable CmsRandomContentGetTitleNId(string contentname, RandomCmsType type);
        CmsRandomContent CmsRandomContentGetById(int id);
        CmsRandomContent CmsRandomContentGetByEventPromoId(int eventId);
        CmsRandomContent CmsRandomContentGetByBrandId(int brandId);
        CmsRandomCityCollection CmsRandomCityGetByPid(int pid, RandomCmsType type);
        CmsRandomCity CmsRandomCityGetByPidNType(int pid, int type);
        CmsRandomCity CmsRandomCityGetById(int id);
        ViewCmsRandomCollection GetViewCmsRandomCollection(string contentname, RandomCmsType type);
        ViewCmsRandomCollection GetViewCmsRandomCollection(string contentname, RandomCmsType type, int addminute);
        ViewCmsRandomCollection GetViewCmsRandomCollection(RandomCmsType type);
        ViewCmsRandomCollection GetViewCmsRandomCollectionAll(RandomCmsType type);
        void CmsRandomCityDelete(int pid, int type);
        ViewCmsRandomCollection GetViewCmsRandomCollectionByTitleAndDate(string title, DateTime? startdate, DateTime? enddate, RandomCmsType type);
        ViewCmsRandomCollection GetViewCmsRandomCollectionByPid(int pid, RandomCmsType type);
        ViewCmsRandomCollection GetViewCmsRandomCollectionByCityIdDate(DateTime deliverydate, int cityid, RandomCmsType type);
        ViewCmsRandomCollection GetViewCmsRandomCollectionByCityIdDateNoPast(string contentname, DateTime deliverydate, int cityid, RandomCmsType type);
        ViewCmsRandomCollection ViewCmsRandomGetCollectionByEventPromoId(int eventPromoId);
        ViewCmsRandomCollection ViewCmsRandomGetCollectionByBrandId(int brandId);
        List<int> GetBrandListBySearch(string searchWord, string searchCityId, RandomCmsType type);
        #endregion
    }
}

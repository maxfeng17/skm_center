﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Core.Interface
{
    public interface IFamiportProvider : IProvider
    {
        #region FamiportPincodeTranEntity

        int AddFamilyNetPincodeTran(FamilyNetPincodeTran entity);
        bool UpdateFamilyNetPincodeTran(FamilyNetPincodeTran trans);
        FamilyNetPincodeTran GetFamilyNetPincodeTrans(int id);
        FamilyNetPincodeTran GetFamilyNetPincodeTrans(Guid orderGuid);
        List<FamilyNetPincodeTran> GetFamilyNetPincodeTransRetry();

        #endregion

        #region Peztemp
        Peztemp GetPeztemp(int pezId);
        Peztemp GetPeztemp(string pezCode);
        bool UpdatePeztemp(Peztemp entity);

        #endregion

        #region Famiport

        Famiport GetFamiport(Guid bid);

        #endregion

        #region FamilyNetEvent

        FamilyNetEvent GetFamilyNetEventById(int? id);
        FamilyNetEvent GetFamilyNetEvent(Guid bid);
        List<FamilyNetEvent> GetFamilyNetEventMainList();
        List<FamilyNetEvent> GetFamilyNetEventDealById(int? id);
        List<FamilyNetEvent> GetFamilyNetEventDealSonListByMainId(int comboDealMainId);
        List<FamilyNetEvent> GetFamilyNetEventDealList(Guid bid);
        List<FamilyNetEvent> GetFamilyNetEventDealAllListByMainId(int comboDealMainId);
        FamilyNetEvent AddFamilyNetEvent(FamilyNetEvent entity);
        bool UpdateFamilyNetEvent(FamilyNetEvent entity);
        void DeleteFamilyNetEventComboDealByItems(List<FamilyNetEvent> items);
        void AddFamilyNetEventComboDealList(List<FamilyNetEvent> items);
        void SetFamilyNetEventComboDealList(List<FamilyNetEvent> items);
        #endregion

        #region FamilyNetPincode

        FamilyNetPincode GetFamilyNetPincodeByCouponId(int couponId);
        FamilyNetPincode AddFamilyNetPincode(Peztemp pez, FamilyNetEvent famiEvent, Guid orderGuid, int couponId);
        FamilyNetPincode GetFamilyNetPincode(int orderNo);
        FamilyNetPincode GetFamilyNetPincodeByPeztempId(int peztempId);
        FamilyNetPincode GetVerifiedFamilyNetPincodeByPeztempId(int peztempId);
        FamilyNetPincode GetFamilyNetPincode(int peztempId, string actCode);
        FamilyNetPincode GetFamilyNetPincode(int peztempId, string actCode, Guid orderGuid);
        List<FamilyNetPincode> GetFamilyNetPincodeRetryData(int perday, int retryLimit, int timespan);
        List<FamilyNetPincode> GetFamilyNetPincodeRegetBarcode();
        List<FamilyNetPincode> GetFamilyNetPincodeReturning(DateTime returnTime);
        List<FamilyNetPincode> GetFamilyNetListPincode(Guid orderGuid);
        bool UpdateFamilyNetPincode(FamilyNetPincode entity);
        int SetFamilyNetPincodeLock(Guid orderGuid, List<int> ids);
        int SetFamilyNetPincodeUnlock(int unlockSec);
        bool UpdateFamilyNetPincodeReturnStatus(List<FamilyNetPincode> list);
        FamilyNetPincode GetFamilyNetPincodeByPezCode(string pezCode);

        #endregion

        #region FamilyNetPincodeDetail

        bool AddFamilyNetPincodeDetail(List<FamilyNetPincodeDetail> entities);
        FamilyNetPincodeDetail GetFamilyNetPincodeDetail(int orderNo, int serialNo, string itemCode);
        bool UpdateFamilyNetPincodeDetail(FamilyNetPincodeDetail entity);
        FamilyNetPincodeDetail GetFamilyNetPincodeDetail(int orderNo, int serialNo);

        #endregion

        #region FamilyNetVerifyLog

        FamilyNetVerifyLog AddFamilyNetVerifyLog(FamilyNetVerifyLog entity);
        bool UpdateFamilyNetVerifyLog(FamilyNetVerifyLog entity);
        FamilyNetVerifyLog GetFamilyNetVerifyLog(int id);

        #endregion

        #region FamilyStoreInfo

        bool UpdateOrInsertFamilyStoreInfoList(List<FamilyStoreInfo> familyStoreInfoList);
        List<FamilyStoreInfo> GetFamilyStoreInfoList();

        FamilyStoreInfo GetFamilyStoreInfoByStoreCode(string storeCode);

        #endregion

        #region FamilyNetVerifyLog

        FamilyNetReturnLog AddFamilyNetReturnLog(FamilyNetReturnLog entity);
        List<FamilyNetReturnLog> AddFamilyNetReturnLog(List<FamilyNetReturnLog> entity);
        bool UpdateFamilyNetReturnLog(FamilyNetReturnLog entity);
        bool UpdateFamilyNetVerifyLog(List<FamilyNetVerifyLog> logCol);
        List<FamilyNetVerifyLog> GetFamilyNetVerifyLogRetryData(int preDays, int retryLimit, int timespan);

        #endregion

        #region FamilyNetGetBarcodeLog

        bool AddFamilyNetGetBarcodeLog(List<FamiBarcodeData> data, int userId, Guid orderGuid, int requestQty);

        #endregion

        #region FamilyNetLockLog

        bool AddFamilyNetLockLog(List<int> couponIds, int userId, Guid orderGuid);

        #endregion

        #region FamilyNetOrderExgGate

        FamilyNetOrderExgGate AddFamilyNetOrderExgGate(FamilyNetOrderExgGate entity);
        bool CheckFamilyNetOrderExgProcessing(int peztempId);
        bool SetFamilyNetOrderExgGateExpired(int peztempId);

        #endregion FamilyNetOrderExgGate

        #region Join (混合)

        FamiBarcodeData GetFamilyNetPincodeInfo(Guid orderGuid, int couponId);
        List<FamiBarcodeData> GetFamilNetVerifyBarcode(Guid orderGuid, int cnt, List<int> excludeCouponId);
        List<FamiBarcodeData> GetFamilNetVerifyBarcodeByGift(Guid orderGuid, int cnt, List<int> couponIds);
        List<FamiUserCouponBarcodeInfo> GetUserCouponBarcodeInfoListByOrderGuid(Guid orderGuid);
        int GetFamiCouponLockedCount(Guid orderGuid);
        List<FamilyNetPincode> GetFamiCouponsByLocked(Guid orderGuid);

        #endregion

        #region ViewFamilyNetVerifyRetryEntities

        List<ViewFamilyNetVerifyRetry> GetViewFamilyNetVerifyRetry();

        #endregion

        #region FamilyNetOrderTicket

        int GetFamilyNetOrderTicket(FamilyNetApi api);

        List<FamiOrderExgParam> GetFamiOrderExgParams(Guid orderGuid);

        #endregion
    }
}

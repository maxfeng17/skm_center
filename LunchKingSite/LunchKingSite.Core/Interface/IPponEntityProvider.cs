﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Models.PponEntities;

namespace LunchKingSite.Core.Interface
{
    public interface IPponEntityProvider : IProvider
    {
        void GoogleShoppingProductDeleteAll();
        void GoogleShoppingProductSet(List<GoogleShoppingProduct> items);
        Dictionary<Guid, DealGoogleMetadata> DealGoogleMetadataGetDict();
        DealGoogleMetadata DealGoogleMetadataGet(Guid bid);
        void DealGoogleMetadataSet(DealGoogleMetadata metadata);
        int OrderedQuantityGet(Guid bid);

        #region DealDistinctProperty

        DealDiscountProperty GetDealDiscountProperty(Guid bid);
        void SetDealDiscountProperty(DealDiscountProperty item);
        void DeleteDealDiscountProperty(Guid bid);

        #endregion

        /// <summary>
        /// 取得目前的檔次折扣均價相關資料
        /// </summary>
        /// <returns></returns>
        List<DealDiscountProperty> GeOnlineDealDiscountProperties();
        /// <summary>
        /// 取得會員最近的瀏覽記錄，排除重複，取最近的最多10筆資料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<Guid> GetDealPageViewBidsTop10(int userId);
        /// <summary>
        /// 取得特定日期的所有DealPageView (檔次頁的瀏覽記錄)
        /// </summary>
        /// <param name="theDate"></param>
        /// <returns></returns>
        List<DealPageView> GetDealPageViews(DateTime startTime, DateTime endTime);

        #region

        List<DealDiscountPriceBlacklist> GetAllDealDiscountPriceBlacklist();
        bool InsertDealDiscountPriceBlacklist(DealDiscountPriceBlacklist dealDiscountPriceBlacklist);
        void DeleteDealDiscountPriceBlacklist(Guid bid);
        DealDiscountPriceBlacklist GetDealDiscountPriceBlacklist(Guid bid);

        #endregion

        #region 成效報表

        List<FrontDealsDailyStat> GetFrontDealsDailyStatListByRange(DateTime start, DateTime end);
        List<FrontDealOrder> GetFrontDealOrderByRange(DateTime start, DateTime end);
        void ImportFrontDealsDailyState(DateTime startDateTime, DateTime endDateTime, List<FrontDealsDailyStat> frontDealsDailyStats);
        bool InsertFrontViewCount(FrontViewCount frontViewCount);
        List<FrontViewCount> GetFrontViewCountByRange(DateTime start, DateTime end);

        #endregion
        /// <summary>
        /// 新增大量 SoleEdmMain 要發送給哪些Email
        /// </summary>
        /// <param name="sendEmails"></param>
        void SaveSoloEdmSendEmails(List<SoloEdmSendEmail> sendEmails);
        /// <summary>
        /// 依據 mainId 刪除設定發送的Email
        /// </summary>
        /// <param name="mainId">SoloEdmMain的Id</param>
        void DeleteSoleEdmSendEmailsByMainId(int mainId);
        /// <summary>
        /// 取得特定SoloEdm發送名單數目
        /// </summary>
        /// <param name="mainId">SoloEdmMain的Id</param>
        /// <returns></returns>
        int GetSoloEdmSendEmailCount(int mainId);
        /// <summary>
        /// 取得特定SoloEdm發送名單
        /// </summary>
        /// <param name="mainId">SoloEdmMain的Id</param>
        /// <returns>回傳 emails</returns>
        List<string> GetSoloEdmSendEmails(int mainId);
        void DeleteDealDiscountPropertyExpired();

        #region 限時優惠

        List<LimitedTimeSelectionDeal> GetEffectiveLimitedTimeSelectionDeals(int mainId);
        /// <summary>
        /// 取得 限時優惠 日期主題設定與檔次設定數
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        Dictionary<DateTime, int> GetLimitedTimeSelectionMainSummary(DateTime startDate, DateTime endDate);
        List<LimitedTimeSelectionDeal> GetLimitedTimeSelectionAllDeals(int mainId);
        List<LimitedTimeSelectionDeal> GetLimitedTimeSelectionValidDeals(int mainId);
        LimitedTimeSelectionMain GetLimitedTimeSelectionMain(int mainId);

        LimitedTimeSelectionMain GetLimitedTimeSelectionMain(DateTime theDate);
        /// <summary>
        /// 更新限時優惠的主檔
        /// </summary>
        /// <param name="main"></param>
        void SaveLimitedTimeSelectionMain(LimitedTimeSelectionMain main);
        int SaveLimitedTimeSelectionDeal(LimitedTimeSelectionDeal deal);
        int SetLimitedTimeSelectionDealDisabled(int mainId, Guid bid, string userId, DateTime now);
        List<LimitedTimeSelectionLog> GetLimitedTimeSelectionLogs(int mainId);
        void SaveLimitedTimeSelectionLog(LimitedTimeSelectionLog log);

        #endregion

        #region 每日頁面瀏覽數/銷售數資料
        /// <summary>
        /// 
        /// </summary>
        /// <param name="theDate"></param>
        void DeleteDealDailySalesInfo(DateTime theDate);

        int GetDealDailySalesInfoCount(DateTime theDate);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dealDailySalesInfos"></param>
        void BulkInsertDealDailySalesInfo(List<DealDailySalesInfo> dealDailySalesInfos);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        Dictionary<Guid, List<DealDailySalesInfo>> GetPriorDealDailySalesInfos(DateTime startDate, DateTime endDate);
        /// <summary>
        /// 取得特定日期的轉換率
        /// </summary>
        /// <param name="theDate"></param>
        /// <returns></returns>
        Dictionary<Guid, decimal> GetDealWeeklyConverationRateDict(DateTime theDate);

        #endregion

        #region Google ADS Log

        void SaveADSLog(AdsLog adsLog);

        #endregion


        #region 推播

        List<QueuedMemberPushMessage> GetQueuedMemberPushMessagePendingList();
        void QueuedMemberPushMessageSet(QueuedMemberPushMessage entity);

        #endregion


        #region Line

        void LineUserPageViewSet(LineUserPageView entity);

        #endregion

        #region HomepageBlock

        List<HomepageBlock> GetLatestHomepageBlocks();
        bool SaveHomepageBlocks(HomepageBlock homepageBlock);

        #endregion

        #region EnterpriseMember 企業帳號購物金記錄
        List<EnterpriseMember> GetEnterpriseMembers();
        EnterpriseMember GetEnterpriseMember(int userId);
        EnterpriseMember GetEnterpriseMemberByUserId(int userId);
        bool SaveEnterpriseMember(EnterpriseMember member);
        void MarkEnterpriseMemberDeleted(int eemId, string deletedBy);
        void SaveEnterpriseDepisitOrder(int eemId, int amount, int userId, string createId);
        void SaveEnterpriseWithdrawalOrder(int eemId, int amount, int userId, string createId);
        List<EnterpriseDepositOrder> GetEnterpriseDepisitOrders(int eemId);
        List<EnterpriseWithdrawalOrder> GetEnterpriseWithdrawalOrders(int depositId);
        #endregion
    }
}
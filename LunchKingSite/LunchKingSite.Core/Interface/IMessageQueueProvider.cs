﻿using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using System;
using Apache.NMS;
using NPOI.HSSF.Record.Formula.Functions;

namespace LunchKingSite.Core
{
    public interface IMessageQueueProvider : IProvider
    {
        void Send(string queueName, string queueText, string categoryName = "", MsgPriority priority = MsgPriority.Normal);
        void Send(string queueName, object obj, string categoryName = "", MsgPriority priority = MsgPriority.Normal);
        void ResetConnect();
        void Disconnect();
        IMessageConsumer GetConsumer(string queueName);
        int GetMessageCount(string queueName);
    }
}
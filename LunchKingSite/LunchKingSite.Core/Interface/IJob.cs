
namespace LunchKingSite.Core
{
    public interface IJob
    {
        void Execute(Configuration.JobSetting js);
    }
}
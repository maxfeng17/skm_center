﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core
{
    public interface IMarketingProvider : IProvider
    {
        #region CreditCardPremium

        CreditCardPremiumCollection CreditCardPremiumGetListByActivityCodeAndEnabled(string activityCode);
        #endregion  CreditCardPremium

        #region MemberReferral

        bool MemberReferralSet(MemberReferral data);
        MemberReferral MemberReferralGet(Guid id);
        MemberReferralCollection MemberReferralGetList();

        #endregion MemberReferral

        #region ReferralCampaign
        ReferralCampaign ReferralCampaignGet(Guid id);
        bool ReferralCampaignSet(ReferralCampaign data);
        ReferralCampaignCollection ReferralCampaignGetListByReferralGuid(Guid rGuid);
        ReferralCampaign ReferralCampaignGetByReferralGuidAndCode(Guid rGuid, string code);
        ReferralCampaign ReferralCampaignGetByRsrc(string fullCode);

        CpaTracking CpaTrackingGetLast(string cpaKey);
        void CpaTrackingSet(CpaTracking tracking);
        void CpaOrderSet(CpaOrder cpaOrder);
        /// <summary>
        /// 取得符合cpakey的CpaTracking列表，但只取頭跟尾，只有1個符合的資料也會回傳2筆，此時頭跟尾都是同一筆
        /// </summary>
        /// <param name="cpaKey"></param>
        /// <returns></returns>
        List<CpaTracking> CpaTrackingGetHeadAndTail(string cpaKey);

        List<CpaTracking> CpaTrackingGetListByCpaKey(string[] cpaKeys);

        #endregion ReferralCampaign

        #region PezEdmUpload
        void PezEdmUploadSet(PezEdmUpload uploadObject);
        void PezEdmUploadDelete(int id);
        PezEdmUpload PezEdmUploadGet(int id);
        PezEdmUpload PezEdmUploadGet(string colName, object value);
        PezEdmUploadCollection PezEdmUploadGetListTop50();
        #endregion

        #region ReferredAction
        bool ReferredActionSet(ReferralAction data);
        ReferralActionCollection ReferralActionGetListByReferenceSourceId(string srcId);
        ReferralActionCollection ReferralActionGetListByReferenceSourceAndActionType(string srcId, ReferrerActionType atype);
        ReferralActionCollection ReferralActionGetListByReferenceSourceAndTime(
            ReferrerActionType? actionType,
            string srcId, DateTime starttime, DateTime endtime);
        #endregion

        #region GA

        bool GaOverviewSet(GaOverviewCollection data);

        GaOverviewCollection GaOverviewGetList(DateTime s, DateTime e, int vid);

        #endregion

        #region BiTraffic


        bool BiTrafficCollectionSet(BiTrafficCollection BitrafficList);


        #endregion BiTraffic


        #region GaDeal
        /// <summary>
        /// 建立Ga紀錄
        /// </summary>
        /// <param name="data"></param>
        void GaDealSet(GaDealCollection data);
        /// <summary>
        /// 建立Ga紀錄
        /// </summary>
        /// <param name="data"></param>
        void GaDealSet(GaDeal data);
        /// <summary>
        /// 建立網站Ga資料
        /// </summary>
        /// <param name="gid">網站或APP GA ID</param>
        /// <param name="start_date">追蹤起始時間</param>
        /// <param name="end_date">追蹤結束時間</param>
        /// <returns></returns>
        GaDealCollection GetGaDealCollectionBySiteDate(int gid, DateTime start_date, DateTime end_date);
        /// <summary>
        /// 建立Ga排程中檔次的Ga資料
        /// </summary>
        /// <param name="gid"></param>
        /// <returns></returns>
        GaDealCollection GetGaDealCollectionByGid(int gid);
        /// <summary>
        /// 刪除相關Ga資料
        /// </summary>
        /// <param name="gid"></param>
        void DeleteGaDeal(int gid);
        #endregion

        #region GaDealList
        /// <summary>
        /// 設定Ga檔次排程
        /// </summary>
        /// <param name="data"></param>
        void SetGaDealList(GaDealList data);
        /// <summary>
        /// 啟用或停用Ga檔次排成
        /// </summary>
        /// <param name="id">Ga檔次排程id</param>
        /// <param name="enabled"></param>
        void EnableGaDealList(int id, bool enabled);
        /// <summary>
        /// 取得位於追蹤日Ga排程的檔次
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        GaDealListCollection GetGaDealListByDate(DateTime date);
        /// <summary>
        /// 依id取得Ga檔次排程
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        GaDealList GetGaDealListById(int id);
        void DeleteGaDealList(int id);
        #endregion

        #region ViewGaDealList
        /// <summary>
        /// 取得Ga檔次排程詳細資料
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="type">手動或排程</param>
        /// <returns></returns>
        ViewGaDealListCollection GetViewGaDealList(Guid? bid, int type);
        /// <summary>
        /// 依id取得Ga檔次排程詳細資料
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ViewGaDealList GetViewGaDealList(int id);
        /// <summary>
        /// 取得Ga檔次排程詳細資料
        /// </summary>
        /// <param name="only_enabled"></param>
        /// <returns></returns>
        ViewGaDealListCollection GetViewGaDealList(bool only_enabled);
        #endregion

        #region DiscountCategoryDeal

        bool IsDiscountCategoryCampaignExist(Guid bid, int campaignId);
        bool IsDiscountEventPromoLinkCampaignExist(Guid bid, List<int> mainIds);
        bool IsDiscountBrandLinkCampaignExist(Guid bid, List<int> brandIds);

        #endregion DiscountCategoryDeal

        CpaTracking CpaTrackingGetListExternalByCpaKey(string cpaKey);
    }
}

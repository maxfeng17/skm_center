﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;


namespace LunchKingSite.Core
{
    public interface IElmahProvider : IProvider
    {
        ElmahError ElmahGet(DateTime startedate, DateTime enddate);
        ElmahErrorCollection ElmahGetLogBySource(string source, DateTime? startdate, DateTime? enddate, int pageStart, int pageLengh);
        int ElmahGetLogCountBySource(string source, DateTime? startdate, DateTime? enddate);
    }
}

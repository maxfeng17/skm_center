﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System.Data;

namespace LunchKingSite.Core.Interface
{
    public interface IPCPProvider : IProvider
    {

        #region identity_code

        void IdentityCodeSetStatus(long identityCodeId, IdentityCodeStatus status);
        IdentityCode IdentityCodeGet(long id);

        #endregion identity_code

        #region membership_card
        MembershipCard MembershipCardGet(int membershipCardId);
        MembershipCard MembershipCardGet(int cardGroupId, DateTime openTime, int level);
        bool MembershipCardSet(MembershipCard membershipCard);
        bool MembershipCardSet(MembershipCardCollection membershipCard);
        MembershipCard MembershipCardGetForLevelUp(int userMembershipCardId, int versionId, PcpLevelChangeType changelType);
        MembershipCardCollection MembershipCardGetByGroupAndVersion(int cardGroupId, int versionId);
        MembershipCardCollection MembershipCardGetByGroupId(int cardGroupId);
        int MembershipCardUpdateSynchronizationData(int cardGroupId, int versionId, AvailableDateType type, bool combineUse);

        #endregion membership_card

        #region user_membership_card

        int UserMembershipCardSet(UserMembershipCard u);

        int UserMembershipCardSet(UserMembershipCardCollection userMembershipCardCollection);

        void UserMembershipCardUpdateOrderCount(int cardId, double amount, PcpLevelChangeType pcpPosLevelType = PcpLevelChangeType.Up);
        void UserMembershipCardLevelUp(int userMembershipCardId, int memberCardId);

        UserMembershipCard UserMembershipCardGet(int userMembershipCardId);
        UserMembershipCard UserMembershipCardGetByCardGroupIdAndUserId(int cardGroupId, int userId);
        int UserMembershipCardUpdateLevelZeroToOne(int cardGroupId, int cardId);
        UserMembershipCardCollection UserMembershipCardGetByCardId(int cardId);

        string UserMembershipCardNoGet(DateTime d);

        UserMembershipCardCollection UserMembershipCardGetByUserId(int userId);

        int UserMembershipCardRemove(List<int> userCardIdList, int userId);

        UserMembershipCard UserMembershipCardGetByCardGroupIDAndUserId(int cardGroupId, int userId);

        #endregion

        #region membership_card_group_store

        bool MembershipCardGroupStoreGuidIsExists(int groupId, Guid storeGuid);
        MembershipCardGroupStoreCollection MembershipCardGroupStoreGet(int groupId);
        MembershipCardGroupStoreCollection MembershipCardGroupStoreGet(List<Guid> storeGuids);
        MembershipCardGroupStore MembershipCardGroupStoreGetBySellerGuid(Guid sellerGuid);
        void MembershipCardGroupStoreDel(int cardGroupId);
        void MembershipCardGroupStoreDelByStoreGuid(Guid storeGuid);
        SellerCollection MembershipCardGroupStoreCanAddedBySellGuid(string parentSellerGuid, int cardGroupId);
        void MembershipCardGroupStoreSet(MembershipCardGroupStoreCollection stores);

        #endregion membership_card_group_store

        #region membership_card_group
        MembershipCardGroup MembershipCardGroupGetSet(Guid sellerGuid, bool isNullInsertNew);//Table Schema調整後會改成用store_guid取group_id
        int MembershipCardGroupSet(MembershipCardGroup m);
        MembershipCardGroup MembershipCardGroupGet(int groupId);
        MembershipCardGroup MembershipCardGroupGetBySellerGuid(Guid sellerGuid);
        MembershipCardGroupCollection MembershipCardGroupGetBySellerGuids(List<Guid> sellers);
        #endregion membership_card_group

        #region membership_card_group_version

        MembershipCardGroupVersion MembershipCardGroupVersionGet(int versionId);
        /// <summary>
        /// 取出CardGroupId相同的資料中
        /// </summary>
        /// <param name="cardGroupId"></param>
        /// <returns></returns>
        MembershipCardGroupVersion MembershipCardGroupVersionGetCardGroupLastData(int cardGroupId);

        bool MembershipCardGroupVersionSet(MembershipCardGroupVersion version);

        #endregion membership_card_group_version

        #region ViewMembershipCard

        ViewMembershipCard ViewMembershipCardGet(int membershipCardId);

        /// <summary>
        /// 取得會員卡資料
        /// </summary>
        /// <param name="accountId">vbs_member.account_id</param>
        /// <returns></returns>
        ViewMembershipCardCollection ViewMembershipCardGetListByAccountId(string accountId);

        /// <summary>
        /// 取得會員卡資料
        /// </summary>
        /// <param name="sellerGuid">商家會員GUID</param>
        /// <returns></returns>
        ViewMembershipCardCollection ViewMembershipCardGetListByGuid(Guid sellerGuid);

        /// <summary>
        /// 取得會員卡資料
        /// </summary>
        /// <param name="sellerUserId">商家會員帳號</param>
        /// <returns></returns>
        ViewMembershipCardCollection ViewMembershipCardGetListByUserId(int sellerUserId);

        int ViewMembershipCardGetCount(params string[] filter);

        ViewMembershipCardCollection ViewMembershipCardGetListByDatetime(int groupId, DateTime d);

        ViewMembershipCardCollection ViewMembershipCardGetList(string orderBy, params string[] filter);

        ViewMembershipCardCollection ViewMembershipCardGetListByCardIds(List<int> cardIds);
        ViewMembershipCardCollection ViewMembershipCardGetListByEnableDatetime(int status, DateTime s, DateTime e);

        ViewMembershipCardCollection ViewMembershipCardGetListBySellerUserId(List<int> ids);

        #endregion

        #region seller_member
        /// <summary>
        /// 依據生日月分撈取資料
        /// </summary>
        /// <param name="sellerUserId">商家使用者編號</param>
        /// <param name="month">月份數字</param>
        /// <returns></returns>
        SellerMemberCollection SellerMemberGetByBirthMonth(int sellerUserId, int month);
        SellerMemberCollection SellerMemberGet(params string[] filter);
        void SellerMemberSet(SellerMember m);
        int SellerMemberSet(SellerMemberCollection m);
        SellerMember SellerMemberGet(int sellerMemberId);
        SellerMember SellerMemberGet(int sellerUserId, string mobile);
        bool SellerMemberCheck(int sellerUserId, string mobile);

        #endregion seller_member

        #region view_user_membership_card

        ViewUserMembershipCardCollection ViewUserMembershipCardGet(string sql);
        ViewUserMembershipCardCollection ViewUserMembershipCardGetList(string orderBy, params string[] filter);
        ViewUserMembershipCard ViewUserMembershipCardGet(int userMembershipCardId);
        ViewUserMembershipCardCollection ViewUserMembershipCardGet(int userId, int groupId);
        #endregion

        #region view_membership_card_group
        ViewMembershipCardGroupCollection ViewMembershipCardGroupGetList(List<Guid> sellers);
        ViewMembershipCardGroupCollection ViewMembershipCardGroupGetList(List<int> groupIds);
        ViewMembershipCardGroupCollection ViewMembershipCardGroupGetListBySellerUserId(int sellerUserId);
        List<string> ViewMembershipCarOnlineVendorGet();
        #endregion

        #region view_user_membership_card_store
        ViewUserMembershipCardStoreCollection ViewUserMembershipCardStoreGetList(string orderBy, params string[] filter);
        #endregion view_user_membership_card_store

        #region view_membership_card_group_version
        ViewMembershipCardGroupVersionCollection ViewMembershipCardGroupVersionGetByQuarter(int cardGroupId, int year, Quarter q);

        #endregion view_membership_card_group_version

        #region 舊的熟客卡優惠集點相關
        #region pcp_point_deposit
        int PcpPointDepositSet(PcpPointDeposit pcpPointDeposit);
        #endregion

        #region view_pcp_point_deposit

        ViewPcpPointDepositCollection ViewPcpPointDepositGetList(string column, object value);
        ViewPcpPointDepositCollection ViewPcpPointDepositGetOnTimeData(int userId);

        ViewPcpPointDepositCollection ViewPcpPointDepositGetByDatetime(DateTime d);

        #endregion

        #region pcp_point_deposit

        PcpPointDepositCollection PcpPointDepositGetByDatetime(DateTime start, DateTime end, int type);

        #endregion

        #region pcp_point_withdrawal

        void PcpPointWithdrawalSet(PcpPointWithdrawal point);

        #endregion

        #region pcp_point_withdrawal_overall

        void PcpPointWithdrawalOverallSet(PcpPointWithdrawalOverall point);

        PcpPointWithdrawalOverall PcpPointWithdrawalOverallGet(int id);

        PcpPointWithdrawalOverallCollection PcpPointWithdrawalOverallGetByDatetime(DateTime start, DateTime end, int type);

        #endregion

        #region pcp_lock_point

        void PcpLockPointSet(PcpLockPoint point);
        PcpLockPointCollection PcpLockPointGetList(string column, object value);
        PcpLockPointCollection PcpLockPointGetOnTime(int userId, PcpPointType type);

        #endregion 

        #region  pcp_point_transaction_order

        Guid PcpPointTransactionOrderSet(PcpPointTransactionOrder pcpPointTransactionOrder);
        PcpPointTransactionOrderCollection PcpPointTransactionOrderListGet(int sellerUserId);

        PcpPointTransactionOrder PcpPointTransactionOrderGet(int pcpPointTransactionOrderId);

        #endregion pcp_point_transaction_order

        #region  pcp_point_transaction_refund

        void PcpPointTransactionRefundSet(PcpPointTransactionRefund pcpPointTransactionRefund);
        PcpPointTransactionRefundCollection PcpPointTransactionRefundListGet(int pcpPointTransactionRefundId);

        #endregion pcp_point_transaction_refund

        #region  pcp_point_transaction_exchange_order

        void PcpPointTransactionExchangeOrderSet(PcpPointTransactionExchangeOrder pcpPointTransactionExchangeOrder);
        PcpPointTransactionExchangeOrderCollection PcpPointTransactionExchangeOrderListGet(int pcpPointTransactionExchangeOrderId);
        PcpPointTransactionExchangeOrderCollection PcpPointTransactionExchangeOrderListGetBySellerUserId(int sellerUserId);


        #endregion pcp_point_transaction_exchange_order

        #region view_pcp_point_transaction_order_record

        ViewPcpPointTransactionOrderRecordCollection ViewPcpPointTransactionOrderRecordListGetByUserId(int userId, PcpPointType pcpPointType);

        #endregion view_pcp_point_transaction_order_record

        #endregion

        #region view_pcp_order_detail
        ViewPcpOrderInfo ViewPcpOrderInfoCollectionGet(int cardGroupId);
        ViewPcpOrderInfoCollection ViewPcpOrderInfoCollectionGet(int userId, int cardGroupId);
        ViewPcpOrderInfoCollection ViewPcpOrderInfoCollectionGet(int cardGroupId, Guid? sellerGuid, DateTime? queryTime, int pageNumber, int pageSize);
        #endregion

        #region pcp_order

        void PcpOrderSet(PcpOrder order);
        PcpOrder PcpOrderGetByGuid(Guid orderGuid);
        PcpOrderCollection PcpOrderGetByUserId(int userId);

        #endregion

        SellerCollection AllowStoreGetList(int userId);

        string VbsAccounGet(int userId);

        #region discount_store

        void DiscountStoreSet(DiscountTemplateStore ds);

        void DiscountTemplateStoreDelete(int templateId);

        #endregion

        #region discount_template

        DiscountTemplate DiscountTemplateGet(int id);

        void DiscountTemplateSet(DiscountTemplate template);

        ViewPcpDiscountTemplateCollection ViewPcpDiscountTemplateGetList(int sellerUserId, int status, DateTime? beginTime,
            DateTime? endTime);

        ViewPcpDiscountTemplateCollection ViewPcpDiscountTemplateGetList(int templateId, int sellerUserId);

        ViewPcpDiscountTemplateCollection ViewPcpDiscountTemplateGetList(params string[] filter);
        #endregion

        #region discount_template_detail_campaign

        ViewPcpAssignmentDiscountCampaignCollection ViewPcpAssignmentDiscountCampaignGet(int templateId);

        #endregion

        #region ViewPcpOrderDiscountLog

        ViewPcpOrderDiscountLogCollection ViewPcpOrderDiscountLogGetList(string accountId, DiscountCampaignType t);

        #endregion

        #region ViewPcpMemberPromotion

        ViewPcpMemberPromotionCollection ViewPcpMemberPromotionSuperBonuxGetList(int userId);

        ViewPcpMemberPromotionCollection ViewPcpMemberPromotionSuperBonusGetList(int userId);

        #endregion

        #region ViewPcpPointRecord

        ViewPcpPointRecordCollection ViewPcpPointRecordGetList(string column, object value);
        ViewPcpPointRecordCollection PcpPointUsageLog(int userId, PcpPointType type);

        /// <summary>
        /// 取得使用者最新的點數餘額
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        ViewPcpPointRecordCollection PcpPointUsageLog(List<int> userId, PcpPointType type);

        #endregion

        #region ViewPcpDiscountCampaignLockPoint

        ViewPcpDiscountCampaignLockPointCollection ViewPcpDiscountCampaignLockPointGetList(string column, object value);

        #endregion ViewPcpDiscountCampaignLockPoint

        ViewDiscountDetailCollection ViewDiscountDetailByCampiagnId(int p1, int p2);

        #region PcpPosTransLog

        PcpPosTransLog PcpPosTransLogSet(PcpPosTransLog pcpPosTransLog);

        #endregion

        #region PcpQueueTicket

        int PcpQueueTicketGetNumber(string character, DateTime ticketDate);

        #endregion

        #region user_get_card_daily_report
        UserGetCardDailyReportCollection UserGetCardDailyReportGetList(DateTime d);

        UserGetCardDailyReportCollection UserGetCardDailyReportGetList(string orderBy, params string[] filter);

        int UserGetCardDailyReportSet(UserGetCardDailyReportCollection data);
        UserGetCardDailyReportCollection UserGetCardDailyReportGet(DateTime d);
        #endregion

        #region vbs_company_detail

        VbsCompanyDetail VbsCompanyDetailGet(Guid sellerGuid);
        VbsCompanyDetail VbsCompanyDetailSet(VbsCompanyDetail data);
        #endregion

        #region ViewMemberShipCardStore
        ViewMembershipCardStoreCollection ViewMembershipCardStoreCollectionGet(int regionId, int category, double latitude, double longitude, MembershipCardGroupOrderby orderBy, int membershipCardId = 0);
        ViewMembershipCardStoreCollection ViewMembershipCardStoreGetByLastGroupId(int groupId);
        ViewMembershipCardStoreCollection ViewMembershipCardStoreCollectionGetGroupIds(int cardGroupId);
        ViewMembershipCardStoreCollection ViewMembershipCardStoreCollectionGetByPromo();
        /// <summary>
        /// 取得熱門的卡片資料
        /// </summary>
        /// <returns></returns>
        ViewMembershipCardStoreCollection ViewMembershipCardStoreGetByHotTenGroup();
        #endregion

        #region view_vbs_pcp_detail
        ViewVbsPcpDetail ViewVbsPcpDetailGet(Guid sellerGuid);
        #endregion

        #region MembershipCardReferee membership_card_referee

        bool MembershipCardRefereeSet(MembershipCardReferee m);

        #endregion

        #region pcp_image

        bool PcpImageSet(PcpImage image);
        bool PcpImageSet(PcpImageCollection images);
        bool PcpImageDelete(int imageId, int userId);
        bool PcpImageDelete(int sellerUserId, int editUserId, PcpImageType imgType, int seq = 1);
        bool PcpImageDelete(int groupId, int editUserId, PcpImageType imgType, List<int> ids); //新
        int PcpImageDelete(int[] ids);
        PcpImage PcpImageGet(int imageId);
        PcpImageCollection PcpImageGet();
        PcpImageCollection PcpImageGet(List<int> sellerUserId, PcpImageType imageType);
        PcpImageCollection PcpImageGetByCardGroupId(int cardGroupId);//新
        PcpImageCollection PcpImageGetByCardGroupId(int cardGroupId, PcpImageType imageType);//新
        PcpImageCollection PcpImageGetByCardGroupId(int cardGroupId, PcpImageType imageType, int seq);//新
        PcpImageCollection PcpImageGetByCardGroupId(int cardGroupId, PcpImageType imageType, List<int> ids); //新
        PcpImageCollection PcpImageGet(int sellerUserId, PcpImageType imageType);//舊 (為了不影響尚未修改的PCPController中的API，先保留不移除)
        PcpImageCollection PcpImageGet(int sellerUserId, PcpImageType imageType, int seq);
        PcpImageCollection PcpImageGetWithNonCompressFile(List<int> compressType);
        PcpImageCollection PcpImageGetIsDelete(DateTime d);
        PcpImageCollection PcpImageGetUnCompress(int[] types);

        #endregion

        #region pcp_intro

        bool PcpIntroSet(PcpIntro intro);

        bool PcpIntroSet(PcpIntroCollection introsCol);
        bool PcpOrderDetialCollectionSet(PcpOrderDetailCollection pcpOrderDetailCollection);
        bool PcpOrderDetialCheckDataIsExistByPk(int pcpOrderPk);
        bool PcpIntroDeleteByCardGroupId(PcpIntroType type, int cardGroupId);//新
        bool PcpIntroDelete(PcpIntroType type, int cardGroupId);//舊 (為了不影響尚未修改的PCPController中的API，先保留不移除)
        PcpIntroCollection PcpIntroGetByCardGroupId(PcpIntroType type, int cardGroupId);//新
        PcpIntroCollection PcpIntroGet(PcpIntroType type, int groupId);//舊 (為了不影響尚未修改的PCPController中的API，先保留不移除)

        #endregion

        #region pcp_marquee
        PcpMarquee PcpMarqueeGet(int cardGroupId, int id);
        PcpMarqueeCollection PcpMarqueeGetListByCardGroupId(int cardGroupId);
        bool PcpMarqueeSet(PcpMarquee marquee);
        bool PcpMarqueeDelete(int cardGroupId, int id);
        #endregion

        #region pcp_point_collect_rule
        PcpPointCollectRule PcpPointCollectRuleGet(int cardGroupId, int id);
        PcpPointCollectRuleCollection PcpPointCollectRuleGetListByCardGroupId(int cardGroupId);
        bool PcpPointCollectRuleSet(PcpPointCollectRule rule);
        bool PcpPointCollectRuleDelete(int cardGroupId, int id);
        #endregion

        #region pcp_point_exchange_rule
        PcpPointExchangeRule PcpPointExchangeRuleGet(int cardGroupId, int id);
        PcpPointExchangeRuleCollection PcpPointExchangeRuleGetListByCardGroupId(int cardGroupId);
        bool PcpPointExchangeRuleSet(PcpPointExchangeRule rule);
        bool PcpPointExchangeRuleDelete(int cardGroupId, int id);
        #endregion

        #region pcp_point_rule_change_log
        PcpPointRuleChangeLogCollection PcpPointRuleChangeLogGetListByCardGroupId(int cardGroupId);
        bool PcpPointRuleChangeLogSet(PcpPointRuleChangeLog log);
        #endregion

        #region pcp_point_remark
        PcpPointRemark PcpPointRemarkGet(int cardGroupId, int id);
        PcpPointRemarkCollection PcpPointRemarkGetListByCardGroupId(int cardGroupId);
        bool PcpPointRemarkSet(PcpPointRemark rule);
        bool PcpPointRemarkDelete(int cardGroupId, int id);
        #endregion

        #region pcp_user_point
        bool PcpUserPointSet(PcpUserPoint point);
        PcpUserPointCollection PcpUserPointListGetByCardGroupId(int cardGroupId, int userId);
        PcpUserPointCollection PcpUserRemainPointListGetByGroupIdAndUserId(int cardGroupId, int userId);
        #endregion

        #region pcp_user_point_alter_pair
        bool PcpUserPointAlterPairSet(PcpUserPointAlterPair point);
        #endregion

        #region pcp_user_token

        bool PcpCheckUserTokenIsExistById(int userId, ref PcpUserToken userToken);
        PcpUserToken PcpUserTokenGetByToken(string userToken);
        bool PcpUserTokenSet(PcpUserToken userToken);
        #endregion

        #region pcp_polling_user_status

        bool PcpPollingUserStatusSet(PcpPollingUserStatus pcpPollingUserStatus);
        PcpPollingUserStatus PcpPollingStatusGetByToken(string userToken, bool needCheckAlive);
        PcpPollingUserStatusCollection PcpPollingStatusCollectionGetByToken(string userToken);
        PcpPollingUserStatus PcpPollingStatusGetById(int userId);
        PcpPollingUserStatusCollection PcpPollingStatusGetAllById(int userId);
        bool PcpPollingStatusCollectionSet(PcpPollingUserStatusCollection pcpUserStatusCollection);
        #endregion

        #region pcp_user_agree_status

        PcpUserAgreeStatus PcpAgreeStatusGetById(int userId);
        bool PcpUserAgreeStatusSet(PcpUserAgreeStatus pcpUserAgreeStatus);

        #endregion

        #region pcp_deposit_menu
        PcpDepositMenu PcpDepositMenuGet(int menuId);
        bool PcpDepositMenuSet(PcpDepositMenu menu);
        bool PcpDepositMenuDelete(int cardGroupId, int menuId);
        bool PcpDepositMenuEnable(int cardGroupId, int menuId, int enable);
        PcpDepositMenuCollection PcpDepositMenuGetByGroupId(int groupId);
        #endregion

        #region pcp_deposit_item
        PcpDepositItem PcpDepositItemGet(int itemId);
        bool PcpDepositItemSet(PcpDepositItem item);
        bool PcpDepositItemCollectionSet(PcpDepositItemCollection pcpDepositItemCollection);
        bool PcpDepositItemDelete(int itemId);
        #endregion

        #region pcp_deposit_template
        PcpDepositTemplate PcpDepositTemplateGet(int cardGroupId, int id);
        bool PcpDepositTemplateSet(PcpDepositTemplate template);
        int PcpDepositTemplateGetCurrentSeq(int cardGroupId, int type);
        List<PcpDepositTemplate> PcpDepositTemplateGetListByCardGroupId(int cardGroupId);
        bool PcpDepositTemplateDelete(int cardGroupId, int id);
        #endregion

        #region pcp_user_deposit

        bool PcpUserDepositCollectionSet(PcpUserDepositCollection pcpUserDepositCollection);
        bool PcpUserDepositSet(PcpUserDeposit pcpUserDeposit);

        PcpUserDeposit UserDepositItemGetByUserAndId(int userId, int userDepositIds);
        #endregion

        #region pcp_user_deposit_coupon

        PcpDepositCouponCollection PcpDepositCouponCollectionGet(int depositId);
        bool PcpDepositCouponCollectionSet(PcpDepositCouponCollection pcpDepositCouponCollection);
        #endregion

        #region pcp_user_deposit_log

        PcpUserDepositLogCollection PcpUserDepositLogDetailGetByUSerId(int userId);
        PcpUserDepositLog PcpUserDepositLogGetByUSerIdOrderByTime(int userId);
        bool PcpUserDepositLogSet(PcpUserDepositLog pcpUserDepositLog);
        #endregion

        #region view_pcp_seller_menu_item
        ViewPcpSellerMenuItemCollection ViewPcpSellerMenuItemCollectionGetByCardGroupId(int cardGroupId);
        ViewPcpSellerMenuItemCollection ViewPcpSellerMenuItemCollectionGetByCardGroupIdAndMenuId(int cardGroupId, int menuId);
        ViewPcpSellerMenuItemCollection ViewPcpSellerMenuItemCollectionGetByCardGroupIdInEffective(int cardGroupId);
        #endregion

        #region view_pcp_user_deposit
        ViewPcpUserDepositCollection ViewPcpUserDepositGetDepositTotalAndRemain(int cardGroupId);
        ViewPcpUserDeposit ViewPcpUserDepositGetDepositTotalAndRemain(int cardGroupId, int menuId);
        ViewPcpUserDepositCollection ViewPcpUserDepositCollectionGetByUserAndCardGroup(int userId, int cardGroupId);
        ViewPcpUserDeposit ViewPcpUserDepositCollectionGetByUserAndDepositId(int userId, int depositId);
        #endregion

        #region view_pcp_user_deposit_coupon
        ViewPcpUserDepositCouponCollection ViewPcpUserDepositCouponCollectionGetByUserIdAndCardGroupId(int userId, int cardGroupId);
        #endregion

        #region view_pcp_user_deposit_log
        ViewPcpUserDepositLogCollection ViewPcpUserDepositLogCollectionGetByUserIdAndCardGroupId(int userId, int cardGroupId);
        ViewPcpUserDepositLogCollection ViewPcpUserDepositLogCollectionGetByUserId(int userId);
        ViewPcpUserDepositLogCollection ViewPcpUserDepositLogCollectionGetByCardGroupId(int cardGroupId);
        #endregion

        #region view_pcp_user_point

        ViewPcpUserPointCollection ViewPcpUserPointCollectionGet(int userId, int cardGroupId);
        ViewPcpUserPointCollection ViewPcpUserPointCollectionGetByUserId(int userId);
        ViewPcpUserPointCollection ViewPcpUserPointCollectionGetByCardGroupId(int cardGroupId, Guid? sellerGuid, DateTime? queryTime, int pageNumber, int pageSize);

        #endregion


        #region MembershipGroupPermission

        bool MembershipCardGroupPermissionSet(MembershipCardGroupPermissionCollection mpc);
        MembershipCardGroupPermissionCollection MembershipCardGroupPermissionGetList(int cardGroupId);
        MembershipCardGroupPermission MembershipCardGroupPermissionGet(int cardGroupId, MembershipService service);
        void MembershipCardGroupPermissionDel(MembershipCardGroupPermission p);
        void MembershipCardGroupPermissionDelAll(int cardGroupId);
        MembershipCardGroupPermissionCollection MembershipCardGroupPermissionGetList(List<int> cardGroupIds);

        #endregion

        #region user_get_card_daily_report
        UserGetCardDailyReportCollection ImmediatelyCountUserGetCard(int cardGroupId, DateTime start, DateTime end);
        #endregion

        #region pcp_category_image

        PcpCategoryImageCollection PcpCategoryImageCollectionGet();

        #endregion

        #region
        ViewMembershipCardAvailableServiceCollection PcpViewMembershipCardAvailableServiceCollectionGet();
        #endregion
    }
}

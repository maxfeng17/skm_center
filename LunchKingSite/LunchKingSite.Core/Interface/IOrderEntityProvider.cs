﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core.Models.OrderEntities;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core
{
    public interface IOrderEntityProvider : IProvider
    {
        bool InsertShopBackInfo(ShopbackInfo info);
        IEnumerable<ViewShopbackInfo> GetCompleteOrderViewShopbackInfoList(DateTime startTime, DateTime endTime);
        IEnumerable<ViewShopbackInfo> GetCompleteOrderViewShopbackInfoList(Guid orderGuid);
        IEnumerable<ViewShopbackValidationInfo> GetValidationOrderViewShopbackInfoList(DateTime startTime,
            DateTime endTime, DateTime contractShipTimeStart, DateTime contractShipTimeEnd,
            DateTime contractOrderTimeStart, DateTime contractOrderTimeEnd);
        ShopbackInfo ShopBackInfoGetById(int id);
        void SaveShopBackInfo(ShopbackInfo info);
        void SaveShopBackInfo(Guid orderId, int shopbackSendType, string commissionType, string responseMessage, DateTime now);

        /// <summary>
        /// 依時間區間，取得狀態為成功的 ViewOrderEntity 列表 (即部份[Order]資料的View)
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        List<ViewOrderEntity> GetSuccessfulViewOrderEntities(DateTime startTime, DateTime endTime);
        List<ViewOrderEntity> GetUserSuccessfulOrders(int userId, DateTime startTime, DateTime endTime);

        /// <summary>
        /// BulkInsert Line導購訂單
        /// </summary>
        /// <param name="Lineshop"></param>
        /// <returns></returns>
        bool BulkInsertLineShopInfo(List<LineshopInfo> Lineshop);
        /// <summary>
        /// 更新Line導購訂單回拋狀態
        /// </summary>
        /// <param name="orderguid"></param>
        /// <param name="SendType"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        bool UpdateLineShopInfoSendType(Guid orderguid, int SendType, DateTime now, string responseMsg);
        /// <summary>
        /// Line導購 取得首次回拋訂單
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        List<ViewLineshopOrderinfo> GetOrderInfoForLineShop(DateTime startTime, DateTime endTime);
        List<ViewLineshopOrderinfo> GetViewLineshopOrderinfoByOrderGuid(Guid guid);
        List<LineshopInfo> GetLineShopInfosByOrderGuid(Guid guid);
        /// <summary>
        /// Line導購 取得請款/發點回拋訂單
        /// </summary>
        /// <param name="OrderStartDate"></param>
        /// <param name="ShipStartDate"></param>
        /// <returns></returns>
        List<ViewLineshopOrderfinish> GetOrderFinishForLineShop(DateTime OrderStartDate, DateTime ShipStartDate);
       
        bool InsertAffiliatesInfo(AffiliatesInfo info);
        AffiliatesInfo AffiliatesGetById(int id);
        void SaveAffiliatesInfo(AffiliatesInfo info);
        IEnumerable<ViewAffiliatesInfo> GetCompleteOrderViewAffiliatesInfoList(DateTime startTime, DateTime endTime);
        void SaveAffiliatesInfo(Guid orderId, int affiliatesSendType, string responseMessage, DateTime now);
    }
}
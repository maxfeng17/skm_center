﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core
{
    public interface IHumanProvider : IProvider
    {
        #region seller_Mapping_employee
        ViewSellerMappingEmployee SellerMappingEmployeeGetBySeller(Guid sellerGuid);
        SellerMappingEmployee SellerMappingEmployeeGet(Guid seller_Guid);
        bool SellerMappingEmployeeSet(SellerMappingEmployee entity);
        SellerMappingEmployeeCollection SellerMappingEmployeeByEmpId(string empId);
        #endregion
        #region Employee & ViewEmployee

        bool EmployeeSet(Employee emp);
        Employee EmployeeGet(string empNo);
        Employee EmployeeGet(string column, object value);
        Employee EmployeeGetByUserId(int userId);
        string EmployeeGetLastEmpId();
        EmployeeCollection EmployeeCollectionGetByFilter(string column, object value);
        EmployeeCollection EmployeeCollectionGetByDepartment(EmployeeDept dept);
        ViewEmployee ViewEmployeeGet(string column, object value);
        ViewEmployee ViewEmployeeByUserIdAndDept(int userId, string dept_id);
        ViewEmployeeCollection ViewEmployeeCollectionGetByFilter(string column, object value, bool? isInvisible = null);
        ViewEmployeeCollection ViewEmployeeCollectionGetByDepartment(EmployeeDept dept);
        ViewEmployeeCollection ViewEmployeeCollectionGetAll();
        ViewEmployeeCollection ViewEmployeeByLikeUserName(string username);
        ViewEmployeeCollection ViewEmployeeByRoleName(string roleName);
        List<string> ViewEmployeeGetColumnListByLike(string column, string partValue);
        #endregion

        #region Department
        Department DepartmentGet(string deptId);
        DepartmentCollection DepartmentGetParentDeptListByEnabled(bool enabled);
        DepartmentCollection DepartmentGetAllSubDepartmentsByEnabled(bool enabled);
        DepartmentCollection DepartmentGetListByEnabled(bool enabled, EmployeeDept parentDeptId = EmployeeDept.S000);
        DepartmentCollection DepartmentGetListBySalesAndMarketing(bool enabled);
        #endregion Department

        #region organization
        void SetOrganization(Organization organization);
        void DeleteOrganization(string name);
        Organization GetOrganization(string name);
        OrganizationCollection GetOrganizationListByOrgNameList(List<string> orgNames);
        OrganizationCollection GetOrganizationListByParentOrgName(string parentOrgName);
        OrganizationCollection GetOrganizationList(string orgName);
        #endregion

        #region Privilege & ViewPrivilege
        void SetPrivilege(Privilege privilege);
        void DeletePrivilege(int privilegeId);
        void PrivilegeDeleteByUser(int userId);
        Privilege GetPrivilege(string email, int funcId);
        PrivilegeCollection GetPrivilegeListByEmail(string email);
        PrivilegeCollection GetPrivilegeListByFuncId(int funcId);
        ViewPrivilegeCollection ViewPrivilegeGetList(string userName);
        ViewPrivilegeCollection ViewPrivilegeGetListByFuncId(int funcId);
        ViewPrivilegeCollection ViewPrivilegeGetListByFuncType(string link, SystemFunctionType type);
        void PrivilegeSetList(PrivilegeCollection pc);
        void PrivilegeSetListByOrgName(int userId, string createId, string OrgName);
        void PrivilegeSetListByOrgName(string orgName, List<int> funcIds, string createId);
        void OrgPrivilegeDeleteList(string orgName, List<int> funcIds);
        #endregion

        #region OrgPrivilege
        void SetOrgPrivilege(OrgPrivilege orgPrivilege);
        void DeleteOrgPrivilege(int id);
        void DeleteOrgPrivilegeByOrgName(string orgName);
        OrgPrivilege GetOrgPrivilege(string orgName, int funcId);
        OrgPrivilegeCollection GetOrgPrivilegeListByOrgName(string orgName);
        OrgPrivilegeCollection GetOrgPrivilegeListByFuncId(int funcId);
        void OrgPrivilegeSetListByOrgName(string orgName, List<int> funcIds, string createId);
        #endregion

        #region SalesVolumeMonth
        SalesVolumeMonthCollection GetSalesVolumeMonthList(int userId);
        int SalesVolumeMonthSet(SalesVolumeMonth s);
        int SalesVolumeMonthUpdate(SalesVolumeMonth s);
        #endregion

        int updateEmployeeIsOfficial(int OfficialMonth, bool isOfficial);
    }
}

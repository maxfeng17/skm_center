﻿using System.Collections.Generic;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Core.Interface
{
    public interface IMailLogProvider : IProvider
    {
        void MailLogSet(string sender, List<string> receivers, string subject, string content, bool result, 
            int category, string lastError);

        void MailLogResendSet(int mailLogId, bool result, string lastError);
    }
}

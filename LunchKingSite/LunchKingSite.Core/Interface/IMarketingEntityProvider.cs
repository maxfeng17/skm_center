﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.MarketingEntities;

namespace LunchKingSite.Core.Interface
{
    public interface IMarketingEntityProvider : IProvider
    {
        RushbuyTracking RushBuyTrackingGetLast(string cpaKey, string bid);
        IList<RushbuyTracking> RushBuyTrackingsByDateTime(DateTime start, DateTime end);
        void SaveRushBuyTracking(RushbuyTracking rushBuyTracking);
    }
}

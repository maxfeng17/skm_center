﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Core
{
    public interface IChannelEventProvider : IProvider
    {
        #region PrizeDrawEvent
        int InsertPrizeDrawEvent(PrizeDrawEvent events);
        bool UpdatePrizeDrawEvent(PrizeDrawEvent events);
        bool UpdatePrizeDrawEventStatus(int prizeDrawEventId, int newStatus);
        PrizeDrawEventModel GetPrizeDrawEvent(int prizeDrawEventId);
        PrizeDrawEvent GetPrizeDrawEventByEventId(int prizeDrawEventId);
        PrizeDrawEventModel GetPrizeDrawEvent(string token);
        PrizeDrawEventModel GetIndexPrizeDrawEvent();
        List<PrizeDrawEventModel> GetPrizeDrawEventList(SkmPrizeDrawEventOrderField orderField, int sort = 0);
        bool CheckIsOtherEventOnline(DateTime sd, DateTime ed, int eventId);
        PrizeDrawEvent GetPrizeDrawEventByToken(string token);
        #endregion

        #region PrizeDrawItem

        PrizeDrawItem GetPrizeDrawItem(int prizeDrawEventId, Guid bid, int prizeDrawitemId);
        /// <summary>
        /// 依照檔期GUID取得抽獎獎品資訊
        /// </summary>
        /// <param name="bid">檔期GUID</param>
        /// <returns>抽獎獎品資訊</returns>
        PrizeDrawItem GetPrizeDrawItem(Guid externalDealId);
        /// <summary>
        /// 依照獎品編號取得獎品資訊
        /// </summary>
        /// <param name="id">獎品編號</param>
        /// <returns>獎品資訊</returns>
        PrizeDrawItem GetPrizeDrawItem(int id);
        bool InsertPrizeDrawItemList(List<PrizeDrawItem> itemList);
        bool UpdatePrizeDrawItemList(List<PrizeDrawItem> itemList);
        bool UpdatePrizeDrawItemListWithoutIsUse(List<PrizeDrawItem> itemList);
        List<PrizeDrawEditItemModel> GetPrizeDrawItemList(int prizeDrawEventId);
        List<PrizeDrawItem> GetPrizeDrawItemsWithNoLock(int eventId, bool getBetweenToday = true);
        List<PrizeDrawItem> GetPrizeDrawItems(int eventId, bool getBetweenToday = true);

        #endregion

        #region PrizeDrawItemPeriod
        /// <summary>
        /// 依抽獎活動編號取得抽獎活動階段資料清單
        /// </summary>
        /// <param name="prizeDrawEventId">抽獎活動編號</param>
        /// <returns>抽獎活動階段資料清單</returns>
        List<PrizeDrawItemPeriod> GetPrizeDrawItemPeriodByEventId(int prizeDrawEventId);
        /// <summary>
        /// 依獎品編號刪除抽獎活動階段資料清單
        /// </summary>
        /// <param name="prizeDrawItemId">抽獎活動編號</param>
        /// <returns>是否刪除成功</returns>
        bool DeletePrizeDrawItemPeriods(int prizeDrawItemId);
        /// <summary>
        /// 依獎品編號清單刪除活動階段資料清單
        /// </summary>
        /// <param name="prizeDrawItemIds">抽獎活動編號清單</param>
        /// <returns>是否刪除成功</returns>
        bool DeletePrizeDrawItemPeriods(List<int> prizeDrawItemIds);
        /// <summary>
        /// 新抽獎增活動階段資料清單
        /// </summary>
        /// <param name="prizeDrawItemPeriods"></param>
        /// <returns>是否新增成功</returns>
        bool InsertPrizeDrawItemPeriods(List<PrizeDrawItemPeriod> prizeDrawItemPeriods);
        /// <summary>
        /// 依獎品編號取得目前可以抽獎的數量
        /// </summary>
        /// <param name="prizeDrawItemId">獎品編號</param>
        /// <returns>目前可以抽獎的數量</returns>
        int GetCanDrawOutItemCountsByItemId(int prizeDrawItemId);
        #endregion

        #region

        ViewPrizeExternalDeal GetExternalDealRemainingQuantity(Guid bid);
        List<ViewPrizeExternalDeal> GetViewPrizeExternalDealList(List<Guid?> bidList);
        List<PrizeDrawEventDDLModel> GetPrizeDrawEventDDLList(DateTime sd, DateTime ed, int joinfee);

        #endregion

        #region PrizeDrawRecord
        int GetPrizeDrawRecordCount(int eventId, int itemId, PrizeDrawRecordStatus status);
        List<int> GetPrizeDrawRecordItemIds(int eventId, PrizeDrawRecordStatus status);
        bool InsertPrizeDrawRecord(PrizeDrawRecord record);
        bool UpdatePrizeDrawRecord(PrizeDrawRecord record);
        PrizeDrawRecord GetPrizeDrawRecord(int recordId);
        PrizeDrawRecord GetPrizeDrawRecord(int eventId, int userId);
        PrizeDrawRecord GetPrizeDrawRecord(int eventId, int userId, int recordId);
        PrizeDrawRecord GetPrizeDrawRecordWithNoLock(int recordId);
        int GetPrizeDrawRecordCount(int itemId, PrizeDrawRecordStatus status);
        List<PrizeDrawRecord> GetPrizeDrawRecordByItemId(int itemId, PrizeDrawRecordStatus status);
        List<PrizeDrawRecord> GetPrizeDrawRecordByEventId(int eventId, PrizeDrawRecordStatus status);
        List<PrizeDrawRecord> GetPrizeDrawRecordByUserId(int eventId, int userId, PrizeDrawRecordStatus status, bool getBetweenToday);
        int GetPrizeDrawRecordQty(int itemId, PrizeDrawRecordStatus status);
        List<ViewPrizeDrawRecord> GetViewPrizeDrawRecord(int eventId, int userId);

        #endregion

        #region  PrizeDrawRecordLog

        bool InsertOrUpdatePrizeDrawRecordLog(PrizeDrawRecordLog log);
        /// <summary>
        /// 依活動編號取得需要重新發送的活動資料清單
        /// </summary>
        /// <param name="prizeDrawEventId">活動編號</param>
        /// <returns>活動資料清單</returns>
        List<PrizeDrawRecordLog> GetNeedResendPrizeDrawRecordLogsByEventId(int prizeDrawEventId);
        #endregion

        #region PrizePreventingOverdraw

        bool InsertPrizePreventingOverdraw(PrizePreventingOverdraw od);
        bool SetPreventingOversellExpired(int itemId, int userId, int qty);
        bool SetPreventingOversellExpired(int preventingOverdrawId);
        int InsertPrizeDrawProcessingAndGetQty(int itemId, int qty, int userId);
        int PrizeDrawGetQty(int itemId, int qty);

        #endregion PrizePreventingOverdraw

        #region ExternalDealIcon
        /// <summary>
        /// 取得標籤設定資料清單
        /// </summary>
        /// <returns>標籤設定資料清單</returns>
        List<ExternalDealIcon> GetExternalDealIcons();
        /// <summary>
        /// 新增標籤設定資料
        /// </summary>
        /// <param name="externalDealIcon">標籤設定資料</param>
        /// <returns>是否新增成功</returns>
        bool InsertExternalDealIcon(ExternalDealIcon externalDealIcon);
        /// <summary>
        /// 編輯標籤設定資料
        /// </summary>
        /// <param name="externalDealIcon">標籤設定資料</param>
        /// <returns>是否編輯成功</returns>
        bool SetExternalDealIcon(ExternalDealIcon externalDealIcon);
        /// <summary>
        /// 刪除標籤設定資料
        /// </summary>
        /// <param name="externalDealIcon">標籤設定資料</param>
        /// <returns>是否刪除成功</returns>
        bool DeleteExternalDealIcon(ExternalDealIcon externalDealIcon);
        #endregion

        #region External_deal_Exhibition_code

        ExternalDealExhibitionCode GetExternalDealExhibitionCode(int codeId);
        List<ExternalDealExhibitionCode> GetExternalDealExhibitionCode(Guid externalDealGuid);
        List<ExternalDealExhibitionCode> GetExternalDealExhibitionCode(Guid externalDealGuid, long code);
        bool AddExternalDealExhibitionCodeList(Guid externalDealGuid, List<long> codes);
        bool SetExternalDealExhibitionCode(Guid externalDealGuid, long code);
        bool DeleteExternalDealExhibitionCode(Guid externalDealGuid, long code);
        bool DeleteExternalDealExhibitionCode(int codeId);
        bool DeleteExternalDealExhibitionCode(Guid externalDealGuid);
        /// <summary>
        /// 檢查策展代號是否已被使用
        /// </summary>
        /// <param name="id">策展代號</param>
        /// <returns>是否已被使用</returns>
        bool ChechExhibitionCodeIsUsed(long id);
        #endregion External_deal_Exhibition_code

        #region external_deal_relation_store_display_name
        /// <summary>
        /// 依檔次編號刪除APP顯示領取櫃位資訊清單
        /// </summary>
        /// <param name="dealGuid">檔次編號</param>
        /// <returns>是否刪除成功</returns>
        bool DeleteExternalDealRelationStoreDisplayNameByDealGuid(Guid dealGuid);
        /// <summary>
        /// 新增APP顯示領取櫃位資訊清單
        /// </summary>
        /// <param name="storeDisplayNames">APP顯示領取櫃位資訊清單</param>
        /// <returns>是否新增成功</returns>
        bool InsertExternalDealRelationStoreDisplayNameByDealGuid(IEnumerable<ExternalDealRelationStoreDisplayName> storeDisplayNames);

        #endregion
        #region View_External_deal_Exhibition_code

        List<ViewExternalDealExhibitionCode> GetViewExternalDealExhibitionCode(Guid externalDealGuid);
        List<ViewExternalDealExhibitionCode> GetViewExternalDealExhibitionCode(Guid externalDealGuid, long code);

        #endregion View_External_deal_Exhibition_code
    }
}

using System;

namespace LunchKingSite.Core.Constant
{
    public class ConfigSection
    {
        public const string SECTION_NAME = "lkconfig";
        public const string JOBS = "jobs";
        public const string JOB = "job";
    }

    public class ConfigProperty
    {
        public const string NAME = "name";
        public const string TYPE = "type";
        public const string DESCRIPTION = "desc";
        public const string ENABLED = "enabled";
        public const string SECONDS = "seconds";
        public const string START_TIME = "starttime";
        public const string END_TIME = "endtime";
        public const string ADV_SCHEDULE = "schedule";
        public const string DELAY = "delay";
    }

    public class GlobalProperty
    {
        public static readonly DateTime TimeBase = new DateTime(1900, 1, 1);
        public static readonly Guid LkBonusPromotionGuid = new Guid("486A17A1-3A86-4821-86CA-FFE9FBC6E674");
        public static readonly Guid SystemItemGuid = new Guid("042140A4-AE6A-46B8-BAFD-C0896BE73466");
        public static readonly Guid PartialRefundGuid = new Guid("3316a014-d981-4d6d-b3ad-03adedb9db87");
        public static readonly int DefaultDeliveryGapInMinutes = 10;
    }
}

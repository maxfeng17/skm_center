using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewEinvoiceAllowance class.
    /// </summary>
    [Serializable]
    public partial class ViewEinvoiceAllowanceCollection : ReadOnlyList<ViewEinvoiceAllowance, ViewEinvoiceAllowanceCollection>
    {
        public ViewEinvoiceAllowanceCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_einvoice_allowance view.
    /// </summary>
    [Serializable]
    public partial class ViewEinvoiceAllowance : ReadOnlyRecord<ViewEinvoiceAllowance>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_einvoice_allowance", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarPaymentTransId = new TableSchema.TableColumn(schema);
                colvarPaymentTransId.ColumnName = "payment_trans_id";
                colvarPaymentTransId.DataType = DbType.Int32;
                colvarPaymentTransId.MaxLength = 0;
                colvarPaymentTransId.AutoIncrement = false;
                colvarPaymentTransId.IsNullable = false;
                colvarPaymentTransId.IsPrimaryKey = false;
                colvarPaymentTransId.IsForeignKey = false;
                colvarPaymentTransId.IsReadOnly = false;

                schema.Columns.Add(colvarPaymentTransId);

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarTransId = new TableSchema.TableColumn(schema);
                colvarTransId.ColumnName = "trans_id";
                colvarTransId.DataType = DbType.AnsiString;
                colvarTransId.MaxLength = 40;
                colvarTransId.AutoIncrement = false;
                colvarTransId.IsNullable = false;
                colvarTransId.IsPrimaryKey = false;
                colvarTransId.IsForeignKey = false;
                colvarTransId.IsReadOnly = false;

                schema.Columns.Add(colvarTransId);

                TableSchema.TableColumn colvarTransTime = new TableSchema.TableColumn(schema);
                colvarTransTime.ColumnName = "trans_time";
                colvarTransTime.DataType = DbType.DateTime;
                colvarTransTime.MaxLength = 0;
                colvarTransTime.AutoIncrement = false;
                colvarTransTime.IsNullable = true;
                colvarTransTime.IsPrimaryKey = false;
                colvarTransTime.IsForeignKey = false;
                colvarTransTime.IsReadOnly = false;

                schema.Columns.Add(colvarTransTime);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;

                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;

                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 4000;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;

                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarOrderTime = new TableSchema.TableColumn(schema);
                colvarOrderTime.ColumnName = "order_time";
                colvarOrderTime.DataType = DbType.DateTime;
                colvarOrderTime.MaxLength = 0;
                colvarOrderTime.AutoIncrement = false;
                colvarOrderTime.IsNullable = false;
                colvarOrderTime.IsPrimaryKey = false;
                colvarOrderTime.IsForeignKey = false;
                colvarOrderTime.IsReadOnly = false;

                schema.Columns.Add(colvarOrderTime);

                TableSchema.TableColumn colvarInvoiceSumAmount = new TableSchema.TableColumn(schema);
                colvarInvoiceSumAmount.ColumnName = "invoice_sum_amount";
                colvarInvoiceSumAmount.DataType = DbType.Currency;
                colvarInvoiceSumAmount.MaxLength = 0;
                colvarInvoiceSumAmount.AutoIncrement = false;
                colvarInvoiceSumAmount.IsNullable = false;
                colvarInvoiceSumAmount.IsPrimaryKey = false;
                colvarInvoiceSumAmount.IsForeignKey = false;
                colvarInvoiceSumAmount.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceSumAmount);

                TableSchema.TableColumn colvarInvoiceMode = new TableSchema.TableColumn(schema);
                colvarInvoiceMode.ColumnName = "invoice_mode";
                colvarInvoiceMode.DataType = DbType.Int32;
                colvarInvoiceMode.MaxLength = 0;
                colvarInvoiceMode.AutoIncrement = false;
                colvarInvoiceMode.IsNullable = false;
                colvarInvoiceMode.IsPrimaryKey = false;
                colvarInvoiceMode.IsForeignKey = false;
                colvarInvoiceMode.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceMode);

                TableSchema.TableColumn colvarInvoiceNumberTime = new TableSchema.TableColumn(schema);
                colvarInvoiceNumberTime.ColumnName = "invoice_number_time";
                colvarInvoiceNumberTime.DataType = DbType.DateTime;
                colvarInvoiceNumberTime.MaxLength = 0;
                colvarInvoiceNumberTime.AutoIncrement = false;
                colvarInvoiceNumberTime.IsNullable = true;
                colvarInvoiceNumberTime.IsPrimaryKey = false;
                colvarInvoiceNumberTime.IsForeignKey = false;
                colvarInvoiceNumberTime.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceNumberTime);

                TableSchema.TableColumn colvarOrderAmount = new TableSchema.TableColumn(schema);
                colvarOrderAmount.ColumnName = "order_amount";
                colvarOrderAmount.DataType = DbType.Currency;
                colvarOrderAmount.MaxLength = 0;
                colvarOrderAmount.AutoIncrement = false;
                colvarOrderAmount.IsNullable = false;
                colvarOrderAmount.IsPrimaryKey = false;
                colvarOrderAmount.IsForeignKey = false;
                colvarOrderAmount.IsReadOnly = false;

                schema.Columns.Add(colvarOrderAmount);

                TableSchema.TableColumn colvarInvoiceNumber = new TableSchema.TableColumn(schema);
                colvarInvoiceNumber.ColumnName = "invoice_number";
                colvarInvoiceNumber.DataType = DbType.String;
                colvarInvoiceNumber.MaxLength = 10;
                colvarInvoiceNumber.AutoIncrement = false;
                colvarInvoiceNumber.IsNullable = true;
                colvarInvoiceNumber.IsPrimaryKey = false;
                colvarInvoiceNumber.IsForeignKey = false;
                colvarInvoiceNumber.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceNumber);

                TableSchema.TableColumn colvarOrderIsPponitem = new TableSchema.TableColumn(schema);
                colvarOrderIsPponitem.ColumnName = "order_is_pponitem";
                colvarOrderIsPponitem.DataType = DbType.Boolean;
                colvarOrderIsPponitem.MaxLength = 0;
                colvarOrderIsPponitem.AutoIncrement = false;
                colvarOrderIsPponitem.IsNullable = false;
                colvarOrderIsPponitem.IsPrimaryKey = false;
                colvarOrderIsPponitem.IsForeignKey = false;
                colvarOrderIsPponitem.IsReadOnly = false;

                schema.Columns.Add(colvarOrderIsPponitem);

                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourStatus);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_einvoice_allowance", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewEinvoiceAllowance()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEinvoiceAllowance(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewEinvoiceAllowance(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewEinvoiceAllowance(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("PaymentTransId")]
        [Bindable(true)]
        public int PaymentTransId
        {
            get
            {
                return GetColumnValue<int>("payment_trans_id");
            }
            set
            {
                SetColumnValue("payment_trans_id", value);
            }
        }

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("TransId")]
        [Bindable(true)]
        public string TransId
        {
            get
            {
                return GetColumnValue<string>("trans_id");
            }
            set
            {
                SetColumnValue("trans_id", value);
            }
        }

        [XmlAttribute("TransTime")]
        [Bindable(true)]
        public DateTime? TransTime
        {
            get
            {
                return GetColumnValue<DateTime?>("trans_time");
            }
            set
            {
                SetColumnValue("trans_time", value);
            }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal Amount
        {
            get
            {
                return GetColumnValue<decimal>("amount");
            }
            set
            {
                SetColumnValue("amount", value);
            }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get
            {
                return GetColumnValue<string>("create_id");
            }
            set
            {
                SetColumnValue("create_id", value);
            }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get
            {
                return GetColumnValue<string>("message");
            }
            set
            {
                SetColumnValue("message", value);
            }
        }

        [XmlAttribute("OrderTime")]
        [Bindable(true)]
        public DateTime OrderTime
        {
            get
            {
                return GetColumnValue<DateTime>("order_time");
            }
            set
            {
                SetColumnValue("order_time", value);
            }
        }

        [XmlAttribute("InvoiceSumAmount")]
        [Bindable(true)]
        public decimal InvoiceSumAmount
        {
            get
            {
                return GetColumnValue<decimal>("invoice_sum_amount");
            }
            set
            {
                SetColumnValue("invoice_sum_amount", value);
            }
        }

        [XmlAttribute("InvoiceMode")]
        [Bindable(true)]
        public int InvoiceMode
        {
            get
            {
                return GetColumnValue<int>("invoice_mode");
            }
            set
            {
                SetColumnValue("invoice_mode", value);
            }
        }

        [XmlAttribute("InvoiceNumberTime")]
        [Bindable(true)]
        public DateTime? InvoiceNumberTime
        {
            get
            {
                return GetColumnValue<DateTime?>("invoice_number_time");
            }
            set
            {
                SetColumnValue("invoice_number_time", value);
            }
        }

        [XmlAttribute("OrderAmount")]
        [Bindable(true)]
        public decimal OrderAmount
        {
            get
            {
                return GetColumnValue<decimal>("order_amount");
            }
            set
            {
                SetColumnValue("order_amount", value);
            }
        }

        [XmlAttribute("InvoiceNumber")]
        [Bindable(true)]
        public string InvoiceNumber
        {
            get
            {
                return GetColumnValue<string>("invoice_number");
            }
            set
            {
                SetColumnValue("invoice_number", value);
            }
        }

        [XmlAttribute("OrderIsPponitem")]
        [Bindable(true)]
        public bool OrderIsPponitem
        {
            get
            {
                return GetColumnValue<bool>("order_is_pponitem");
            }
            set
            {
                SetColumnValue("order_is_pponitem", value);
            }
        }

        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus
        {
            get
            {
                return GetColumnValue<int>("business_hour_status");
            }
            set
            {
                SetColumnValue("business_hour_status", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string PaymentTransId = @"payment_trans_id";

            public static string Id = @"id";

            public static string TransId = @"trans_id";

            public static string TransTime = @"trans_time";

            public static string Amount = @"amount";

            public static string CreateId = @"create_id";

            public static string Message = @"message";

            public static string OrderTime = @"order_time";

            public static string InvoiceSumAmount = @"invoice_sum_amount";

            public static string InvoiceMode = @"invoice_mode";

            public static string InvoiceNumberTime = @"invoice_number_time";

            public static string OrderAmount = @"order_amount";

            public static string InvoiceNumber = @"invoice_number";

            public static string OrderIsPponitem = @"order_is_pponitem";

            public static string BusinessHourStatus = @"business_hour_status";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

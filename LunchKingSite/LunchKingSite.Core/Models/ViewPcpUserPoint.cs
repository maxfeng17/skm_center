using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpUserPoint class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpUserPointCollection : ReadOnlyList<ViewPcpUserPoint, ViewPcpUserPointCollection>
    {
        public ViewPcpUserPointCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_user_point view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpUserPoint : ReadOnlyRecord<ViewPcpUserPoint>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_user_point", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarUserPointId = new TableSchema.TableColumn(schema);
                colvarUserPointId.ColumnName = "user_point_id";
                colvarUserPointId.DataType = DbType.Int32;
                colvarUserPointId.MaxLength = 0;
                colvarUserPointId.AutoIncrement = false;
                colvarUserPointId.IsNullable = false;
                colvarUserPointId.IsPrimaryKey = false;
                colvarUserPointId.IsForeignKey = false;
                colvarUserPointId.IsReadOnly = false;

                schema.Columns.Add(colvarUserPointId);

                TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
                colvarCardGroupId.ColumnName = "card_group_id";
                colvarCardGroupId.DataType = DbType.Int32;
                colvarCardGroupId.MaxLength = 0;
                colvarCardGroupId.AutoIncrement = false;
                colvarCardGroupId.IsNullable = false;
                colvarCardGroupId.IsPrimaryKey = false;
                colvarCardGroupId.IsForeignKey = false;
                colvarCardGroupId.IsReadOnly = false;

                schema.Columns.Add(colvarCardGroupId);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;

                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarTotalPoint = new TableSchema.TableColumn(schema);
                colvarTotalPoint.ColumnName = "total_point";
                colvarTotalPoint.DataType = DbType.Int32;
                colvarTotalPoint.MaxLength = 0;
                colvarTotalPoint.AutoIncrement = false;
                colvarTotalPoint.IsNullable = false;
                colvarTotalPoint.IsPrimaryKey = false;
                colvarTotalPoint.IsForeignKey = false;
                colvarTotalPoint.IsReadOnly = false;

                schema.Columns.Add(colvarTotalPoint);

                TableSchema.TableColumn colvarRemainPoint = new TableSchema.TableColumn(schema);
                colvarRemainPoint.ColumnName = "remain_point";
                colvarRemainPoint.DataType = DbType.Int32;
                colvarRemainPoint.MaxLength = 0;
                colvarRemainPoint.AutoIncrement = false;
                colvarRemainPoint.IsNullable = false;
                colvarRemainPoint.IsPrimaryKey = false;
                colvarRemainPoint.IsForeignKey = false;
                colvarRemainPoint.IsReadOnly = false;

                schema.Columns.Add(colvarRemainPoint);

                TableSchema.TableColumn colvarFinalPoint = new TableSchema.TableColumn(schema);
                colvarFinalPoint.ColumnName = "final_point";
                colvarFinalPoint.DataType = DbType.Int32;
                colvarFinalPoint.MaxLength = 0;
                colvarFinalPoint.AutoIncrement = false;
                colvarFinalPoint.IsNullable = false;
                colvarFinalPoint.IsPrimaryKey = false;
                colvarFinalPoint.IsForeignKey = false;
                colvarFinalPoint.IsReadOnly = false;

                schema.Columns.Add(colvarFinalPoint);

                TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
                colvarPrice.ColumnName = "price";
                colvarPrice.DataType = DbType.Currency;
                colvarPrice.MaxLength = 0;
                colvarPrice.AutoIncrement = false;
                colvarPrice.IsNullable = true;
                colvarPrice.IsPrimaryKey = false;
                colvarPrice.IsForeignKey = false;
                colvarPrice.IsReadOnly = false;

                schema.Columns.Add(colvarPrice);

                TableSchema.TableColumn colvarSetType = new TableSchema.TableColumn(schema);
                colvarSetType.ColumnName = "set_type";
                colvarSetType.DataType = DbType.Int32;
                colvarSetType.MaxLength = 0;
                colvarSetType.AutoIncrement = false;
                colvarSetType.IsNullable = false;
                colvarSetType.IsPrimaryKey = false;
                colvarSetType.IsForeignKey = false;
                colvarSetType.IsReadOnly = false;

                schema.Columns.Add(colvarSetType);

                TableSchema.TableColumn colvarPointRuleId = new TableSchema.TableColumn(schema);
                colvarPointRuleId.ColumnName = "point_rule_id";
                colvarPointRuleId.DataType = DbType.Int32;
                colvarPointRuleId.MaxLength = 0;
                colvarPointRuleId.AutoIncrement = false;
                colvarPointRuleId.IsNullable = true;
                colvarPointRuleId.IsPrimaryKey = false;
                colvarPointRuleId.IsForeignKey = false;
                colvarPointRuleId.IsReadOnly = false;

                schema.Columns.Add(colvarPointRuleId);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 200;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;

                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarCount = new TableSchema.TableColumn(schema);
                colvarCount.ColumnName = "count";
                colvarCount.DataType = DbType.Int32;
                colvarCount.MaxLength = 0;
                colvarCount.AutoIncrement = false;
                colvarCount.IsNullable = false;
                colvarCount.IsPrimaryKey = false;
                colvarCount.IsForeignKey = false;
                colvarCount.IsReadOnly = false;

                schema.Columns.Add(colvarCount);

                TableSchema.TableColumn colvarPcpOrderGuid = new TableSchema.TableColumn(schema);
                colvarPcpOrderGuid.ColumnName = "pcp_order_guid";
                colvarPcpOrderGuid.DataType = DbType.Guid;
                colvarPcpOrderGuid.MaxLength = 0;
                colvarPcpOrderGuid.AutoIncrement = false;
                colvarPcpOrderGuid.IsNullable = false;
                colvarPcpOrderGuid.IsPrimaryKey = false;
                colvarPcpOrderGuid.IsForeignKey = false;
                colvarPcpOrderGuid.IsReadOnly = false;

                schema.Columns.Add(colvarPcpOrderGuid);

                TableSchema.TableColumn colvarExpireTime = new TableSchema.TableColumn(schema);
                colvarExpireTime.ColumnName = "expire_time";
                colvarExpireTime.DataType = DbType.DateTime;
                colvarExpireTime.MaxLength = 0;
                colvarExpireTime.AutoIncrement = false;
                colvarExpireTime.IsNullable = true;
                colvarExpireTime.IsPrimaryKey = false;
                colvarExpireTime.IsForeignKey = false;
                colvarExpireTime.IsReadOnly = false;

                schema.Columns.Add(colvarExpireTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;

                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;

                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 100;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;

                schema.Columns.Add(colvarStoreName);

                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;

                schema.Columns.Add(colvarSellerName);

                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = false;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;

                schema.Columns.Add(colvarAccountId);

                TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
                colvarAccountName.ColumnName = "account_name";
                colvarAccountName.DataType = DbType.String;
                colvarAccountName.MaxLength = 100;
                colvarAccountName.AutoIncrement = false;
                colvarAccountName.IsNullable = false;
                colvarAccountName.IsPrimaryKey = false;
                colvarAccountName.IsForeignKey = false;
                colvarAccountName.IsReadOnly = false;

                schema.Columns.Add(colvarAccountName);

                TableSchema.TableColumn colvarMemberLastName = new TableSchema.TableColumn(schema);
                colvarMemberLastName.ColumnName = "member_last_name";
                colvarMemberLastName.DataType = DbType.String;
                colvarMemberLastName.MaxLength = 50;
                colvarMemberLastName.AutoIncrement = false;
                colvarMemberLastName.IsNullable = true;
                colvarMemberLastName.IsPrimaryKey = false;
                colvarMemberLastName.IsForeignKey = false;
                colvarMemberLastName.IsReadOnly = false;

                schema.Columns.Add(colvarMemberLastName);

                TableSchema.TableColumn colvarMemberFirstName = new TableSchema.TableColumn(schema);
                colvarMemberFirstName.ColumnName = "member_first_name";
                colvarMemberFirstName.DataType = DbType.String;
                colvarMemberFirstName.MaxLength = 50;
                colvarMemberFirstName.AutoIncrement = false;
                colvarMemberFirstName.IsNullable = true;
                colvarMemberFirstName.IsPrimaryKey = false;
                colvarMemberFirstName.IsForeignKey = false;
                colvarMemberFirstName.IsReadOnly = false;

                schema.Columns.Add(colvarMemberFirstName);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_user_point", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewPcpUserPoint()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpUserPoint(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewPcpUserPoint(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewPcpUserPoint(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("UserPointId")]
        [Bindable(true)]
        public int UserPointId
        {
            get
            {
                return GetColumnValue<int>("user_point_id");
            }
            set
            {
                SetColumnValue("user_point_id", value);
            }
        }

        [XmlAttribute("CardGroupId")]
        [Bindable(true)]
        public int CardGroupId
        {
            get
            {
                return GetColumnValue<int>("card_group_id");
            }
            set
            {
                SetColumnValue("card_group_id", value);
            }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get
            {
                return GetColumnValue<int>("user_id");
            }
            set
            {
                SetColumnValue("user_id", value);
            }
        }

        [XmlAttribute("TotalPoint")]
        [Bindable(true)]
        public int TotalPoint
        {
            get
            {
                return GetColumnValue<int>("total_point");
            }
            set
            {
                SetColumnValue("total_point", value);
            }
        }

        [XmlAttribute("RemainPoint")]
        [Bindable(true)]
        public int RemainPoint
        {
            get
            {
                return GetColumnValue<int>("remain_point");
            }
            set
            {
                SetColumnValue("remain_point", value);
            }
        }

        [XmlAttribute("FinalPoint")]
        [Bindable(true)]
        public int FinalPoint
        {
            get
            {
                return GetColumnValue<int>("final_point");
            }
            set
            {
                SetColumnValue("final_point", value);
            }
        }

        [XmlAttribute("Price")]
        [Bindable(true)]
        public decimal? Price
        {
            get
            {
                return GetColumnValue<decimal?>("price");
            }
            set
            {
                SetColumnValue("price", value);
            }
        }

        [XmlAttribute("SetType")]
        [Bindable(true)]
        public int SetType
        {
            get
            {
                return GetColumnValue<int>("set_type");
            }
            set
            {
                SetColumnValue("set_type", value);
            }
        }

        [XmlAttribute("PointRuleId")]
        [Bindable(true)]
        public int? PointRuleId
        {
            get
            {
                return GetColumnValue<int?>("point_rule_id");
            }
            set
            {
                SetColumnValue("point_rule_id", value);
            }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get
            {
                return GetColumnValue<string>("item_name");
            }
            set
            {
                SetColumnValue("item_name", value);
            }
        }

        [XmlAttribute("Count")]
        [Bindable(true)]
        public int Count
        {
            get
            {
                return GetColumnValue<int>("count");
            }
            set
            {
                SetColumnValue("count", value);
            }
        }

        [XmlAttribute("PcpOrderGuid")]
        [Bindable(true)]
        public Guid PcpOrderGuid
        {
            get
            {
                return GetColumnValue<Guid>("pcp_order_guid");
            }
            set
            {
                SetColumnValue("pcp_order_guid", value);
            }
        }

        [XmlAttribute("ExpireTime")]
        [Bindable(true)]
        public DateTime? ExpireTime
        {
            get
            {
                return GetColumnValue<DateTime?>("expire_time");
            }
            set
            {
                SetColumnValue("expire_time", value);
            }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId
        {
            get
            {
                return GetColumnValue<int>("create_id");
            }
            set
            {
                SetColumnValue("create_id", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid
        {
            get
            {
                return GetColumnValue<Guid>("seller_guid");
            }
            set
            {
                SetColumnValue("seller_guid", value);
            }
        }

        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName
        {
            get
            {
                return GetColumnValue<string>("store_name");
            }
            set
            {
                SetColumnValue("store_name", value);
            }
        }

        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName
        {
            get
            {
                return GetColumnValue<string>("seller_name");
            }
            set
            {
                SetColumnValue("seller_name", value);
            }
        }

        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId
        {
            get
            {
                return GetColumnValue<string>("account_id");
            }
            set
            {
                SetColumnValue("account_id", value);
            }
        }

        [XmlAttribute("AccountName")]
        [Bindable(true)]
        public string AccountName
        {
            get
            {
                return GetColumnValue<string>("account_name");
            }
            set
            {
                SetColumnValue("account_name", value);
            }
        }

        [XmlAttribute("MemberLastName")]
        [Bindable(true)]
        public string MemberLastName
        {
            get
            {
                return GetColumnValue<string>("member_last_name");
            }
            set
            {
                SetColumnValue("member_last_name", value);
            }
        }

        [XmlAttribute("MemberFirstName")]
        [Bindable(true)]
        public string MemberFirstName
        {
            get
            {
                return GetColumnValue<string>("member_first_name");
            }
            set
            {
                SetColumnValue("member_first_name", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string UserPointId = @"user_point_id";

            public static string CardGroupId = @"card_group_id";

            public static string UserId = @"user_id";

            public static string TotalPoint = @"total_point";

            public static string RemainPoint = @"remain_point";

            public static string FinalPoint = @"final_point";

            public static string Price = @"price";

            public static string SetType = @"set_type";

            public static string PointRuleId = @"point_rule_id";

            public static string ItemName = @"item_name";

            public static string Count = @"count";

            public static string PcpOrderGuid = @"pcp_order_guid";

            public static string ExpireTime = @"expire_time";

            public static string CreateId = @"create_id";

            public static string CreateTime = @"create_time";

            public static string SellerGuid = @"seller_guid";

            public static string StoreName = @"store_name";

            public static string SellerName = @"seller_name";

            public static string AccountId = @"account_id";

            public static string AccountName = @"account_name";

            public static string MemberLastName = @"member_last_name";

            public static string MemberFirstName = @"member_first_name";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

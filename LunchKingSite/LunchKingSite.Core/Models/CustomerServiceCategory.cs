using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CustomerServiceCategory class.
	/// </summary>
    [Serializable]
	public partial class CustomerServiceCategoryCollection : RepositoryList<CustomerServiceCategory, CustomerServiceCategoryCollection>
	{	   
		public CustomerServiceCategoryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CustomerServiceCategoryCollection</returns>
		public CustomerServiceCategoryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CustomerServiceCategory o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the customer_service_category table.
	/// </summary>
	
	[Serializable]
	public partial class CustomerServiceCategory : RepositoryRecord<CustomerServiceCategory>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CustomerServiceCategory()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CustomerServiceCategory(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("customer_service_category", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
				colvarCategoryId.ColumnName = "category_id";
				colvarCategoryId.DataType = DbType.Int32;
				colvarCategoryId.MaxLength = 0;
				colvarCategoryId.AutoIncrement = true;
				colvarCategoryId.IsNullable = false;
				colvarCategoryId.IsPrimaryKey = true;
				colvarCategoryId.IsForeignKey = false;
				colvarCategoryId.IsReadOnly = false;
				colvarCategoryId.DefaultSetting = @"";
				colvarCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryId);
				
				TableSchema.TableColumn colvarParentCategoryId = new TableSchema.TableColumn(schema);
				colvarParentCategoryId.ColumnName = "parent_category_id";
				colvarParentCategoryId.DataType = DbType.Int32;
				colvarParentCategoryId.MaxLength = 0;
				colvarParentCategoryId.AutoIncrement = false;
				colvarParentCategoryId.IsNullable = true;
				colvarParentCategoryId.IsPrimaryKey = false;
				colvarParentCategoryId.IsForeignKey = false;
				colvarParentCategoryId.IsReadOnly = false;
				colvarParentCategoryId.DefaultSetting = @"";
				colvarParentCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentCategoryId);
				
				TableSchema.TableColumn colvarCategoryName = new TableSchema.TableColumn(schema);
				colvarCategoryName.ColumnName = "category_name";
				colvarCategoryName.DataType = DbType.String;
				colvarCategoryName.MaxLength = 100;
				colvarCategoryName.AutoIncrement = false;
				colvarCategoryName.IsNullable = true;
				colvarCategoryName.IsPrimaryKey = false;
				colvarCategoryName.IsForeignKey = false;
				colvarCategoryName.IsReadOnly = false;
				colvarCategoryName.DefaultSetting = @"";
				colvarCategoryName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryName);
				
				TableSchema.TableColumn colvarCategoryOrder = new TableSchema.TableColumn(schema);
				colvarCategoryOrder.ColumnName = "category_order";
				colvarCategoryOrder.DataType = DbType.Int32;
				colvarCategoryOrder.MaxLength = 0;
				colvarCategoryOrder.AutoIncrement = false;
				colvarCategoryOrder.IsNullable = false;
				colvarCategoryOrder.IsPrimaryKey = false;
				colvarCategoryOrder.IsForeignKey = false;
				colvarCategoryOrder.IsReadOnly = false;
				colvarCategoryOrder.DefaultSetting = @"";
				colvarCategoryOrder.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryOrder);
				
				TableSchema.TableColumn colvarIsNeedOrderId = new TableSchema.TableColumn(schema);
				colvarIsNeedOrderId.ColumnName = "is_need_order_id";
				colvarIsNeedOrderId.DataType = DbType.Boolean;
				colvarIsNeedOrderId.MaxLength = 0;
				colvarIsNeedOrderId.AutoIncrement = false;
				colvarIsNeedOrderId.IsNullable = false;
				colvarIsNeedOrderId.IsPrimaryKey = false;
				colvarIsNeedOrderId.IsForeignKey = false;
				colvarIsNeedOrderId.IsReadOnly = false;
				
						colvarIsNeedOrderId.DefaultSetting = @"((0))";
				colvarIsNeedOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsNeedOrderId);
				
				TableSchema.TableColumn colvarIsShowFrontEnd = new TableSchema.TableColumn(schema);
				colvarIsShowFrontEnd.ColumnName = "is_show_front_end";
				colvarIsShowFrontEnd.DataType = DbType.Boolean;
				colvarIsShowFrontEnd.MaxLength = 0;
				colvarIsShowFrontEnd.AutoIncrement = false;
				colvarIsShowFrontEnd.IsNullable = false;
				colvarIsShowFrontEnd.IsPrimaryKey = false;
				colvarIsShowFrontEnd.IsForeignKey = false;
				colvarIsShowFrontEnd.IsReadOnly = false;
				
						colvarIsShowFrontEnd.DefaultSetting = @"((0))";
				colvarIsShowFrontEnd.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsShowFrontEnd);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.Int32;
				colvarModifyId.MaxLength = 0;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("customer_service_category",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("CategoryId")]
		[Bindable(true)]
		public int CategoryId 
		{
			get { return GetColumnValue<int>(Columns.CategoryId); }
			set { SetColumnValue(Columns.CategoryId, value); }
		}
		
		[XmlAttribute("ParentCategoryId")]
		[Bindable(true)]
		public int? ParentCategoryId 
		{
			get { return GetColumnValue<int?>(Columns.ParentCategoryId); }
			set { SetColumnValue(Columns.ParentCategoryId, value); }
		}
		
		[XmlAttribute("CategoryName")]
		[Bindable(true)]
		public string CategoryName 
		{
			get { return GetColumnValue<string>(Columns.CategoryName); }
			set { SetColumnValue(Columns.CategoryName, value); }
		}
		
		[XmlAttribute("CategoryOrder")]
		[Bindable(true)]
		public int CategoryOrder 
		{
			get { return GetColumnValue<int>(Columns.CategoryOrder); }
			set { SetColumnValue(Columns.CategoryOrder, value); }
		}
		
		[XmlAttribute("IsNeedOrderId")]
		[Bindable(true)]
		public bool IsNeedOrderId 
		{
			get { return GetColumnValue<bool>(Columns.IsNeedOrderId); }
			set { SetColumnValue(Columns.IsNeedOrderId, value); }
		}
		
		[XmlAttribute("IsShowFrontEnd")]
		[Bindable(true)]
		public bool IsShowFrontEnd 
		{
			get { return GetColumnValue<bool>(Columns.IsShowFrontEnd); }
			set { SetColumnValue(Columns.IsShowFrontEnd, value); }
		}
		
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public int? ModifyId 
		{
			get { return GetColumnValue<int?>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn CategoryIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ParentCategoryIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryOrderColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IsNeedOrderIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IsShowFrontEndColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string CategoryId = @"category_id";
			 public static string ParentCategoryId = @"parent_category_id";
			 public static string CategoryName = @"category_name";
			 public static string CategoryOrder = @"category_order";
			 public static string IsNeedOrderId = @"is_need_order_id";
			 public static string IsShowFrontEnd = @"is_show_front_end";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

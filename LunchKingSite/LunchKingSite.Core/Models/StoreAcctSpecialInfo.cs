using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the StoreAcctSpecialInfo class.
	/// </summary>
    [Serializable]
	public partial class StoreAcctSpecialInfoCollection : RepositoryList<StoreAcctSpecialInfo, StoreAcctSpecialInfoCollection>
	{	   
		public StoreAcctSpecialInfoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>StoreAcctSpecialInfoCollection</returns>
		public StoreAcctSpecialInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                StoreAcctSpecialInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the store_acct_special_info table.
	/// </summary>
	[Serializable]
	public partial class StoreAcctSpecialInfo : RepositoryRecord<StoreAcctSpecialInfo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public StoreAcctSpecialInfo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public StoreAcctSpecialInfo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("store_acct_special_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarAcct = new TableSchema.TableColumn(schema);
				colvarAcct.ColumnName = "acct";
				colvarAcct.DataType = DbType.AnsiString;
				colvarAcct.MaxLength = 20;
				colvarAcct.AutoIncrement = false;
				colvarAcct.IsNullable = true;
				colvarAcct.IsPrimaryKey = false;
				colvarAcct.IsForeignKey = false;
				colvarAcct.IsReadOnly = false;
				colvarAcct.DefaultSetting = @"";
				colvarAcct.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAcct);
				
				TableSchema.TableColumn colvarAcctPw = new TableSchema.TableColumn(schema);
				colvarAcctPw.ColumnName = "acct_pw";
				colvarAcctPw.DataType = DbType.AnsiString;
				colvarAcctPw.MaxLength = 10;
				colvarAcctPw.AutoIncrement = false;
				colvarAcctPw.IsNullable = true;
				colvarAcctPw.IsPrimaryKey = false;
				colvarAcctPw.IsForeignKey = false;
				colvarAcctPw.IsReadOnly = false;
				colvarAcctPw.DefaultSetting = @"";
				colvarAcctPw.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAcctPw);
				
				TableSchema.TableColumn colvarAcctName = new TableSchema.TableColumn(schema);
				colvarAcctName.ColumnName = "acct_name";
				colvarAcctName.DataType = DbType.AnsiString;
				colvarAcctName.MaxLength = 10;
				colvarAcctName.AutoIncrement = false;
				colvarAcctName.IsNullable = true;
				colvarAcctName.IsPrimaryKey = false;
				colvarAcctName.IsForeignKey = false;
				colvarAcctName.IsReadOnly = false;
				colvarAcctName.DefaultSetting = @"";
				colvarAcctName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAcctName);
				
				TableSchema.TableColumn colvarAcctEmail = new TableSchema.TableColumn(schema);
				colvarAcctEmail.ColumnName = "acct_email";
				colvarAcctEmail.DataType = DbType.AnsiString;
				colvarAcctEmail.MaxLength = 50;
				colvarAcctEmail.AutoIncrement = false;
				colvarAcctEmail.IsNullable = true;
				colvarAcctEmail.IsPrimaryKey = false;
				colvarAcctEmail.IsForeignKey = false;
				colvarAcctEmail.IsReadOnly = false;
				colvarAcctEmail.DefaultSetting = @"";
				colvarAcctEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAcctEmail);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = 1000;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("store_acct_special_info",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Acct")]
		[Bindable(true)]
		public string Acct 
		{
			get { return GetColumnValue<string>(Columns.Acct); }
			set { SetColumnValue(Columns.Acct, value); }
		}
		  
		[XmlAttribute("AcctPw")]
		[Bindable(true)]
		public string AcctPw 
		{
			get { return GetColumnValue<string>(Columns.AcctPw); }
			set { SetColumnValue(Columns.AcctPw, value); }
		}
		  
		[XmlAttribute("AcctName")]
		[Bindable(true)]
		public string AcctName 
		{
			get { return GetColumnValue<string>(Columns.AcctName); }
			set { SetColumnValue(Columns.AcctName, value); }
		}
		  
		[XmlAttribute("AcctEmail")]
		[Bindable(true)]
		public string AcctEmail 
		{
			get { return GetColumnValue<string>(Columns.AcctEmail); }
			set { SetColumnValue(Columns.AcctEmail, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AcctColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AcctPwColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AcctNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AcctEmailColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Acct = @"acct";
			 public static string AcctPw = @"acct_pw";
			 public static string AcctName = @"acct_name";
			 public static string AcctEmail = @"acct_email";
			 public static string Status = @"status";
			 public static string Description = @"description";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

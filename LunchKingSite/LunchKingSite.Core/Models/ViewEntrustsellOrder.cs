using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEntrustsellOrder class.
    /// </summary>
    [Serializable]
    public partial class ViewEntrustsellOrderCollection : ReadOnlyList<ViewEntrustsellOrder, ViewEntrustsellOrderCollection>
    {        
        public ViewEntrustsellOrderCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_entrustsell_order view.
    /// </summary>
    [Serializable]
    public partial class ViewEntrustsellOrder : ReadOnlyRecord<ViewEntrustsellOrder>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_entrustsell_order", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarVerifiedTime = new TableSchema.TableColumn(schema);
                colvarVerifiedTime.ColumnName = "verified_time";
                colvarVerifiedTime.DataType = DbType.DateTime;
                colvarVerifiedTime.MaxLength = 0;
                colvarVerifiedTime.AutoIncrement = false;
                colvarVerifiedTime.IsNullable = true;
                colvarVerifiedTime.IsPrimaryKey = false;
                colvarVerifiedTime.IsForeignKey = false;
                colvarVerifiedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifiedTime);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 45;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarMemberId = new TableSchema.TableColumn(schema);
                colvarMemberId.ColumnName = "member_id";
                colvarMemberId.DataType = DbType.AnsiString;
                colvarMemberId.MaxLength = 50;
                colvarMemberId.AutoIncrement = false;
                colvarMemberId.IsNullable = false;
                colvarMemberId.IsPrimaryKey = false;
                colvarMemberId.IsForeignKey = false;
                colvarMemberId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberId);
                
                TableSchema.TableColumn colvarProductQc = new TableSchema.TableColumn(schema);
                colvarProductQc.ColumnName = "product_qc";
                colvarProductQc.DataType = DbType.AnsiString;
                colvarProductQc.MaxLength = 36;
                colvarProductQc.AutoIncrement = false;
                colvarProductQc.IsNullable = true;
                colvarProductQc.IsPrimaryKey = false;
                colvarProductQc.IsForeignKey = false;
                colvarProductQc.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductQc);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 300;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
                colvarItemQuantity.ColumnName = "item_quantity";
                colvarItemQuantity.DataType = DbType.Int32;
                colvarItemQuantity.MaxLength = 0;
                colvarItemQuantity.AutoIncrement = false;
                colvarItemQuantity.IsNullable = false;
                colvarItemQuantity.IsPrimaryKey = false;
                colvarItemQuantity.IsForeignKey = false;
                colvarItemQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemQuantity);
                
                TableSchema.TableColumn colvarPcash = new TableSchema.TableColumn(schema);
                colvarPcash.ColumnName = "pcash";
                colvarPcash.DataType = DbType.Int32;
                colvarPcash.MaxLength = 0;
                colvarPcash.AutoIncrement = false;
                colvarPcash.IsNullable = false;
                colvarPcash.IsPrimaryKey = false;
                colvarPcash.IsForeignKey = false;
                colvarPcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcash);
                
                TableSchema.TableColumn colvarScash = new TableSchema.TableColumn(schema);
                colvarScash.ColumnName = "scash";
                colvarScash.DataType = DbType.Int32;
                colvarScash.MaxLength = 0;
                colvarScash.AutoIncrement = false;
                colvarScash.IsNullable = false;
                colvarScash.IsPrimaryKey = false;
                colvarScash.IsForeignKey = false;
                colvarScash.IsReadOnly = false;
                
                schema.Columns.Add(colvarScash);
                
                TableSchema.TableColumn colvarBcash = new TableSchema.TableColumn(schema);
                colvarBcash.ColumnName = "bcash";
                colvarBcash.DataType = DbType.Int32;
                colvarBcash.MaxLength = 0;
                colvarBcash.AutoIncrement = false;
                colvarBcash.IsNullable = false;
                colvarBcash.IsPrimaryKey = false;
                colvarBcash.IsForeignKey = false;
                colvarBcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarBcash);
                
                TableSchema.TableColumn colvarCreditCard = new TableSchema.TableColumn(schema);
                colvarCreditCard.ColumnName = "credit_card";
                colvarCreditCard.DataType = DbType.Int32;
                colvarCreditCard.MaxLength = 0;
                colvarCreditCard.AutoIncrement = false;
                colvarCreditCard.IsNullable = false;
                colvarCreditCard.IsPrimaryKey = false;
                colvarCreditCard.IsForeignKey = false;
                colvarCreditCard.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreditCard);
                
                TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
                colvarDiscountAmount.ColumnName = "discount_amount";
                colvarDiscountAmount.DataType = DbType.Int32;
                colvarDiscountAmount.MaxLength = 0;
                colvarDiscountAmount.AutoIncrement = false;
                colvarDiscountAmount.IsNullable = false;
                colvarDiscountAmount.IsPrimaryKey = false;
                colvarDiscountAmount.IsForeignKey = false;
                colvarDiscountAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountAmount);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Int32;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Currency;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = false;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarCost);
                
                TableSchema.TableColumn colvarProfit = new TableSchema.TableColumn(schema);
                colvarProfit.ColumnName = "profit";
                colvarProfit.DataType = DbType.Int32;
                colvarProfit.MaxLength = 0;
                colvarProfit.AutoIncrement = false;
                colvarProfit.IsNullable = true;
                colvarProfit.IsPrimaryKey = false;
                colvarProfit.IsForeignKey = false;
                colvarProfit.IsReadOnly = false;
                
                schema.Columns.Add(colvarProfit);
                
                TableSchema.TableColumn colvarVerifiedStatus = new TableSchema.TableColumn(schema);
                colvarVerifiedStatus.ColumnName = "verified_status";
                colvarVerifiedStatus.DataType = DbType.Int32;
                colvarVerifiedStatus.MaxLength = 0;
                colvarVerifiedStatus.AutoIncrement = false;
                colvarVerifiedStatus.IsNullable = false;
                colvarVerifiedStatus.IsPrimaryKey = false;
                colvarVerifiedStatus.IsForeignKey = false;
                colvarVerifiedStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifiedStatus);
                
                TableSchema.TableColumn colvarVerifiedSpecialStatus = new TableSchema.TableColumn(schema);
                colvarVerifiedSpecialStatus.ColumnName = "verified_special_status";
                colvarVerifiedSpecialStatus.DataType = DbType.Int32;
                colvarVerifiedSpecialStatus.MaxLength = 0;
                colvarVerifiedSpecialStatus.AutoIncrement = false;
                colvarVerifiedSpecialStatus.IsNullable = false;
                colvarVerifiedSpecialStatus.IsPrimaryKey = false;
                colvarVerifiedSpecialStatus.IsForeignKey = false;
                colvarVerifiedSpecialStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifiedSpecialStatus);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarUndoFlag = new TableSchema.TableColumn(schema);
                colvarUndoFlag.ColumnName = "undo_flag";
                colvarUndoFlag.DataType = DbType.Int32;
                colvarUndoFlag.MaxLength = 0;
                colvarUndoFlag.AutoIncrement = false;
                colvarUndoFlag.IsNullable = false;
                colvarUndoFlag.IsPrimaryKey = false;
                colvarUndoFlag.IsForeignKey = false;
                colvarUndoFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarUndoFlag);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_entrustsell_order",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewEntrustsellOrder()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEntrustsellOrder(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewEntrustsellOrder(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewEntrustsellOrder(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("VerifiedTime")]
        [Bindable(true)]
        public DateTime? VerifiedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("verified_time");
		    }
            set 
		    {
			    SetColumnValue("verified_time", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("MemberId")]
        [Bindable(true)]
        public string MemberId 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_id");
		    }
            set 
		    {
			    SetColumnValue("member_id", value);
            }
        }
	      
        [XmlAttribute("ProductQc")]
        [Bindable(true)]
        public string ProductQc 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_qc");
		    }
            set 
		    {
			    SetColumnValue("product_qc", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("ItemQuantity")]
        [Bindable(true)]
        public int ItemQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_quantity");
		    }
            set 
		    {
			    SetColumnValue("item_quantity", value);
            }
        }
	      
        [XmlAttribute("Pcash")]
        [Bindable(true)]
        public int Pcash 
	    {
		    get
		    {
			    return GetColumnValue<int>("pcash");
		    }
            set 
		    {
			    SetColumnValue("pcash", value);
            }
        }
	      
        [XmlAttribute("Scash")]
        [Bindable(true)]
        public int Scash 
	    {
		    get
		    {
			    return GetColumnValue<int>("scash");
		    }
            set 
		    {
			    SetColumnValue("scash", value);
            }
        }
	      
        [XmlAttribute("Bcash")]
        [Bindable(true)]
        public int Bcash 
	    {
		    get
		    {
			    return GetColumnValue<int>("bcash");
		    }
            set 
		    {
			    SetColumnValue("bcash", value);
            }
        }
	      
        [XmlAttribute("CreditCard")]
        [Bindable(true)]
        public int CreditCard 
	    {
		    get
		    {
			    return GetColumnValue<int>("credit_card");
		    }
            set 
		    {
			    SetColumnValue("credit_card", value);
            }
        }
	      
        [XmlAttribute("DiscountAmount")]
        [Bindable(true)]
        public int DiscountAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("discount_amount");
		    }
            set 
		    {
			    SetColumnValue("discount_amount", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public int Amount 
	    {
		    get
		    {
			    return GetColumnValue<int>("amount");
		    }
            set 
		    {
			    SetColumnValue("amount", value);
            }
        }
	      
        [XmlAttribute("Cost")]
        [Bindable(true)]
        public decimal Cost 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("cost");
		    }
            set 
		    {
			    SetColumnValue("cost", value);
            }
        }
	      
        [XmlAttribute("Profit")]
        [Bindable(true)]
        public int? Profit 
	    {
		    get
		    {
			    return GetColumnValue<int?>("profit");
		    }
            set 
		    {
			    SetColumnValue("profit", value);
            }
        }
	      
        [XmlAttribute("VerifiedStatus")]
        [Bindable(true)]
        public int VerifiedStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("verified_status");
		    }
            set 
		    {
			    SetColumnValue("verified_status", value);
            }
        }
	      
        [XmlAttribute("VerifiedSpecialStatus")]
        [Bindable(true)]
        public int VerifiedSpecialStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("verified_special_status");
		    }
            set 
		    {
			    SetColumnValue("verified_special_status", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("UndoFlag")]
        [Bindable(true)]
        public int UndoFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("undo_flag");
		    }
            set 
		    {
			    SetColumnValue("undo_flag", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string VerifiedTime = @"verified_time";
            
            public static string OrderId = @"order_id";
            
            public static string MemberName = @"member_name";
            
            public static string MemberId = @"member_id";
            
            public static string ProductQc = @"product_qc";
            
            public static string ItemName = @"item_name";
            
            public static string ItemPrice = @"item_price";
            
            public static string ItemQuantity = @"item_quantity";
            
            public static string Pcash = @"pcash";
            
            public static string Scash = @"scash";
            
            public static string Bcash = @"bcash";
            
            public static string CreditCard = @"credit_card";
            
            public static string DiscountAmount = @"discount_amount";
            
            public static string Amount = @"amount";
            
            public static string Cost = @"cost";
            
            public static string Profit = @"profit";
            
            public static string VerifiedStatus = @"verified_status";
            
            public static string VerifiedSpecialStatus = @"verified_special_status";
            
            public static string SellerName = @"seller_name";
            
            public static string CreateTime = @"create_time";
            
            public static string UndoFlag = @"undo_flag";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

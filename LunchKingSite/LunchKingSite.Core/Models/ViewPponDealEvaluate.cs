using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponDealEvaluate class.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealEvaluateCollection : ReadOnlyList<ViewPponDealEvaluate, ViewPponDealEvaluateCollection>
    {        
        public ViewPponDealEvaluateCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_deal_evaluate view.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealEvaluate : ReadOnlyRecord<ViewPponDealEvaluate>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_deal_evaluate", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarEvaluateCount = new TableSchema.TableColumn(schema);
                colvarEvaluateCount.ColumnName = "evaluate_count";
                colvarEvaluateCount.DataType = DbType.Int32;
                colvarEvaluateCount.MaxLength = 0;
                colvarEvaluateCount.AutoIncrement = false;
                colvarEvaluateCount.IsNullable = false;
                colvarEvaluateCount.IsPrimaryKey = false;
                colvarEvaluateCount.IsForeignKey = false;
                colvarEvaluateCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarEvaluateCount);
                
                TableSchema.TableColumn colvarEvaluateStar = new TableSchema.TableColumn(schema);
                colvarEvaluateStar.ColumnName = "evaluate_star";
                colvarEvaluateStar.DataType = DbType.Decimal;
                colvarEvaluateStar.MaxLength = 0;
                colvarEvaluateStar.AutoIncrement = false;
                colvarEvaluateStar.IsNullable = false;
                colvarEvaluateStar.IsPrimaryKey = false;
                colvarEvaluateStar.IsForeignKey = false;
                colvarEvaluateStar.IsReadOnly = false;
                
                schema.Columns.Add(colvarEvaluateStar);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_deal_evaluate",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponDealEvaluate()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponDealEvaluate(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponDealEvaluate(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponDealEvaluate(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("EvaluateCount")]
        [Bindable(true)]
        public int EvaluateCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("evaluate_count");
		    }
            set 
		    {
			    SetColumnValue("evaluate_count", value);
            }
        }
	      
        [XmlAttribute("EvaluateStar")]
        [Bindable(true)]
        public decimal EvaluateStar 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("evaluate_star");
		    }
            set 
		    {
			    SetColumnValue("evaluate_star", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string EvaluateCount = @"evaluate_count";
            
            public static string EvaluateStar = @"evaluate_star";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

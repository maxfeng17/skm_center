﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Models
{
    public partial class ProposalAssignLogDealModel
    {
        public string Pid { get; set; }
        public string BrandName { get; set; }//品牌名稱
        public string OrderTimeS { get; set; }//上檔日期
        public string Setting { get; set; }//檔次設定
        public string CopyWriter { get; set; }//圖片設計
        public string ImageDesign { get; set; }//圖片設計
        public string ART { get; set; }//大圖ART
        public string Listing { get; set; }//上架確認
    }
    public enum ChkAssignType
    {
        Bid = 1,
        Personal = 2
    }
}

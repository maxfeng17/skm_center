using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewPezChannelCollection : ReadOnlyList<ViewPezChannel, ViewPezChannelCollection>
	{
			public ViewPezChannelCollection() {}

	}

	[Serializable]
	public partial class ViewPezChannel : ReadOnlyRecord<ViewPezChannel>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewPezChannel()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewPezChannel(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewPezChannel(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewPezChannel(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_pez_channel", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = false;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = false;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);

				TableSchema.TableColumn colvarGid = new TableSchema.TableColumn(schema);
				colvarGid.ColumnName = "gid";
				colvarGid.DataType = DbType.AnsiString;
				colvarGid.MaxLength = 100;
				colvarGid.AutoIncrement = false;
				colvarGid.IsNullable = false;
				colvarGid.IsPrimaryKey = false;
				colvarGid.IsForeignKey = false;
				colvarGid.IsReadOnly = false;
				colvarGid.DefaultSetting = @"";
				colvarGid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGid);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);

				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = true;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);

				TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
				colvarOrderStatus.ColumnName = "order_status";
				colvarOrderStatus.DataType = DbType.Int32;
				colvarOrderStatus.MaxLength = 0;
				colvarOrderStatus.AutoIncrement = false;
				colvarOrderStatus.IsNullable = false;
				colvarOrderStatus.IsPrimaryKey = false;
				colvarOrderStatus.IsForeignKey = false;
				colvarOrderStatus.IsReadOnly = false;
				colvarOrderStatus.DefaultSetting = @"";
				colvarOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStatus);

				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);

				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "coupon_id";
				colvarCouponId.DataType = DbType.Int32;
				colvarCouponId.MaxLength = 0;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = true;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = false;
				colvarCouponId.IsReadOnly = false;
				colvarCouponId.DefaultSetting = @"";
				colvarCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponId);

				TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
				colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
				colvarCouponSequenceNumber.DataType = DbType.String;
				colvarCouponSequenceNumber.MaxLength = 50;
				colvarCouponSequenceNumber.AutoIncrement = false;
				colvarCouponSequenceNumber.IsNullable = true;
				colvarCouponSequenceNumber.IsPrimaryKey = false;
				colvarCouponSequenceNumber.IsForeignKey = false;
				colvarCouponSequenceNumber.IsReadOnly = false;
				colvarCouponSequenceNumber.DefaultSetting = @"";
				colvarCouponSequenceNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponSequenceNumber);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = false;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);

				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Currency;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = false;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);

				TableSchema.TableColumn colvarCashTrustLogStatus = new TableSchema.TableColumn(schema);
				colvarCashTrustLogStatus.ColumnName = "cash_trust_log_status";
				colvarCashTrustLogStatus.DataType = DbType.Int32;
				colvarCashTrustLogStatus.MaxLength = 0;
				colvarCashTrustLogStatus.AutoIncrement = false;
				colvarCashTrustLogStatus.IsNullable = false;
				colvarCashTrustLogStatus.IsPrimaryKey = false;
				colvarCashTrustLogStatus.IsForeignKey = false;
				colvarCashTrustLogStatus.IsReadOnly = false;
				colvarCashTrustLogStatus.DefaultSetting = @"";
				colvarCashTrustLogStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCashTrustLogStatus);

				TableSchema.TableColumn colvarOrderTime = new TableSchema.TableColumn(schema);
				colvarOrderTime.ColumnName = "order_time";
				colvarOrderTime.DataType = DbType.DateTime;
				colvarOrderTime.MaxLength = 0;
				colvarOrderTime.AutoIncrement = false;
				colvarOrderTime.IsNullable = false;
				colvarOrderTime.IsPrimaryKey = false;
				colvarOrderTime.IsForeignKey = false;
				colvarOrderTime.IsReadOnly = false;
				colvarOrderTime.DefaultSetting = @"";
				colvarOrderTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderTime);

				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 2147483647;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = false;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);

				TableSchema.TableColumn colvarSendType = new TableSchema.TableColumn(schema);
				colvarSendType.ColumnName = "send_type";
				colvarSendType.DataType = DbType.Int32;
				colvarSendType.MaxLength = 0;
				colvarSendType.AutoIncrement = false;
				colvarSendType.IsNullable = false;
				colvarSendType.IsPrimaryKey = false;
				colvarSendType.IsForeignKey = false;
				colvarSendType.IsReadOnly = false;
				colvarSendType.DefaultSetting = @"";
				colvarSendType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendType);

				TableSchema.TableColumn colvarFirstRsrc = new TableSchema.TableColumn(schema);
				colvarFirstRsrc.ColumnName = "first_rsrc";
				colvarFirstRsrc.DataType = DbType.String;
				colvarFirstRsrc.MaxLength = 50;
				colvarFirstRsrc.AutoIncrement = false;
				colvarFirstRsrc.IsNullable = false;
				colvarFirstRsrc.IsPrimaryKey = false;
				colvarFirstRsrc.IsForeignKey = false;
				colvarFirstRsrc.IsReadOnly = false;
				colvarFirstRsrc.DefaultSetting = @"";
				colvarFirstRsrc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFirstRsrc);

				TableSchema.TableColumn colvarLastRsrc = new TableSchema.TableColumn(schema);
				colvarLastRsrc.ColumnName = "last_rsrc";
				colvarLastRsrc.DataType = DbType.String;
				colvarLastRsrc.MaxLength = 50;
				colvarLastRsrc.AutoIncrement = false;
				colvarLastRsrc.IsNullable = false;
				colvarLastRsrc.IsPrimaryKey = false;
				colvarLastRsrc.IsForeignKey = false;
				colvarLastRsrc.IsReadOnly = false;
				colvarLastRsrc.DefaultSetting = @"";
				colvarLastRsrc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastRsrc);

				TableSchema.TableColumn colvarLastExternalRsrc = new TableSchema.TableColumn(schema);
				colvarLastExternalRsrc.ColumnName = "last_external_rsrc";
				colvarLastExternalRsrc.DataType = DbType.String;
				colvarLastExternalRsrc.MaxLength = 50;
				colvarLastExternalRsrc.AutoIncrement = false;
				colvarLastExternalRsrc.IsNullable = true;
				colvarLastExternalRsrc.IsPrimaryKey = false;
				colvarLastExternalRsrc.IsForeignKey = false;
				colvarLastExternalRsrc.IsReadOnly = false;
				colvarLastExternalRsrc.DefaultSetting = @"";
				colvarLastExternalRsrc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastExternalRsrc);

				TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
				colvarDiscountAmount.ColumnName = "discount_amount";
				colvarDiscountAmount.DataType = DbType.Int32;
				colvarDiscountAmount.MaxLength = 0;
				colvarDiscountAmount.AutoIncrement = false;
				colvarDiscountAmount.IsNullable = false;
				colvarDiscountAmount.IsPrimaryKey = false;
				colvarDiscountAmount.IsForeignKey = false;
				colvarDiscountAmount.IsReadOnly = false;
				colvarDiscountAmount.DefaultSetting = @"";
				colvarDiscountAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountAmount);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_pez_channel",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}

		[XmlAttribute("Gid")]
		[Bindable(true)]
		public string Gid
		{
			get { return GetColumnValue<string>(Columns.Gid); }
			set { SetColumnValue(Columns.Gid, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}

		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}

		[XmlAttribute("OrderStatus")]
		[Bindable(true)]
		public int OrderStatus
		{
			get { return GetColumnValue<int>(Columns.OrderStatus); }
			set { SetColumnValue(Columns.OrderStatus, value); }
		}

		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}

		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public int? CouponId
		{
			get { return GetColumnValue<int?>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}

		[XmlAttribute("CouponSequenceNumber")]
		[Bindable(true)]
		public string CouponSequenceNumber
		{
			get { return GetColumnValue<string>(Columns.CouponSequenceNumber); }
			set { SetColumnValue(Columns.CouponSequenceNumber, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int DeliveryType
		{
			get { return GetColumnValue<int>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}

		[XmlAttribute("Amount")]
		[Bindable(true)]
		public decimal Amount
		{
			get { return GetColumnValue<decimal>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}

		[XmlAttribute("CashTrustLogStatus")]
		[Bindable(true)]
		public int CashTrustLogStatus
		{
			get { return GetColumnValue<int>(Columns.CashTrustLogStatus); }
			set { SetColumnValue(Columns.CashTrustLogStatus, value); }
		}

		[XmlAttribute("OrderTime")]
		[Bindable(true)]
		public DateTime OrderTime
		{
			get { return GetColumnValue<DateTime>(Columns.OrderTime); }
			set { SetColumnValue(Columns.OrderTime, value); }
		}

		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}

		[XmlAttribute("SendType")]
		[Bindable(true)]
		public int SendType
		{
			get { return GetColumnValue<int>(Columns.SendType); }
			set { SetColumnValue(Columns.SendType, value); }
		}

		[XmlAttribute("FirstRsrc")]
		[Bindable(true)]
		public string FirstRsrc
		{
			get { return GetColumnValue<string>(Columns.FirstRsrc); }
			set { SetColumnValue(Columns.FirstRsrc, value); }
		}

		[XmlAttribute("LastRsrc")]
		[Bindable(true)]
		public string LastRsrc
		{
			get { return GetColumnValue<string>(Columns.LastRsrc); }
			set { SetColumnValue(Columns.LastRsrc, value); }
		}

		[XmlAttribute("LastExternalRsrc")]
		[Bindable(true)]
		public string LastExternalRsrc
		{
			get { return GetColumnValue<string>(Columns.LastExternalRsrc); }
			set { SetColumnValue(Columns.LastExternalRsrc, value); }
		}

		[XmlAttribute("DiscountAmount")]
		[Bindable(true)]
		public int DiscountAmount
		{
			get { return GetColumnValue<int>(Columns.DiscountAmount); }
			set { SetColumnValue(Columns.DiscountAmount, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn BidColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn GidColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn GuidColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn OrderIdColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn OrderStatusColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn TrustIdColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn CouponIdColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn CouponSequenceNumberColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn DeliveryTypeColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn AmountColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn CashTrustLogStatusColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn OrderTimeColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn ItemNameColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn SendTypeColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn FirstRsrcColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn LastRsrcColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn LastExternalRsrcColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn DiscountAmountColumn
		{
			get { return Schema.Columns[22]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string OrderGuid = @"order_guid";
			public static string Bid = @"bid";
			public static string Gid = @"gid";
			public static string ModifyTime = @"modify_time";
			public static string CreateTime = @"create_time";
			public static string Guid = @"GUID";
			public static string OrderId = @"order_id";
			public static string OrderStatus = @"order_status";
			public static string TrustId = @"trust_id";
			public static string CouponId = @"coupon_id";
			public static string CouponSequenceNumber = @"coupon_sequence_number";
			public static string UserId = @"user_id";
			public static string DeliveryType = @"delivery_type";
			public static string Amount = @"amount";
			public static string CashTrustLogStatus = @"cash_trust_log_status";
			public static string OrderTime = @"order_time";
			public static string ItemName = @"item_name";
			public static string SendType = @"send_type";
			public static string FirstRsrc = @"first_rsrc";
			public static string LastRsrc = @"last_rsrc";
			public static string LastExternalRsrc = @"last_external_rsrc";
			public static string DiscountAmount = @"discount_amount";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OrderDetailItem class.
	/// </summary>
    [Serializable]
	public partial class OrderDetailItemCollection : RepositoryList<OrderDetailItem, OrderDetailItemCollection>
	{	   
		public OrderDetailItemCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OrderDetailItemCollection</returns>
		public OrderDetailItemCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OrderDetailItem o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the order_detail_item table.
	/// </summary>
	[Serializable]
	public partial class OrderDetailItem : RepositoryRecord<OrderDetailItem>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OrderDetailItem()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OrderDetailItem(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order_detail_item", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
				colvarOrderDetailGuid.ColumnName = "order_detail_guid";
				colvarOrderDetailGuid.DataType = DbType.Guid;
				colvarOrderDetailGuid.MaxLength = 0;
				colvarOrderDetailGuid.AutoIncrement = false;
				colvarOrderDetailGuid.IsNullable = false;
				colvarOrderDetailGuid.IsPrimaryKey = false;
				colvarOrderDetailGuid.IsForeignKey = true;
				colvarOrderDetailGuid.IsReadOnly = false;
				colvarOrderDetailGuid.DefaultSetting = @"";
				
					colvarOrderDetailGuid.ForeignKeyTableName = "order_detail";
				schema.Columns.Add(colvarOrderDetailGuid);
				
				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 200;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = true;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);
				
				TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
				colvarQuantity.ColumnName = "quantity";
				colvarQuantity.DataType = DbType.Int32;
				colvarQuantity.MaxLength = 0;
				colvarQuantity.AutoIncrement = false;
				colvarQuantity.IsNullable = false;
				colvarQuantity.IsPrimaryKey = false;
				colvarQuantity.IsForeignKey = false;
				colvarQuantity.IsReadOnly = false;
				colvarQuantity.DefaultSetting = @"";
				colvarQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQuantity);
				
				TableSchema.TableColumn colvarOptions = new TableSchema.TableColumn(schema);
				colvarOptions.ColumnName = "options";
				colvarOptions.DataType = DbType.String;
				colvarOptions.MaxLength = 1000;
				colvarOptions.AutoIncrement = false;
				colvarOptions.IsNullable = true;
				colvarOptions.IsPrimaryKey = false;
				colvarOptions.IsForeignKey = false;
				colvarOptions.IsReadOnly = false;
				colvarOptions.DefaultSetting = @"";
				colvarOptions.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOptions);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order_detail_item",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderDetailGuid")]
		[Bindable(true)]
		public Guid OrderDetailGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderDetailGuid); }
			set { SetColumnValue(Columns.OrderDetailGuid, value); }
		}
		  
		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName 
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}
		  
		[XmlAttribute("Quantity")]
		[Bindable(true)]
		public int Quantity 
		{
			get { return GetColumnValue<int>(Columns.Quantity); }
			set { SetColumnValue(Columns.Quantity, value); }
		}
		  
		[XmlAttribute("Options")]
		[Bindable(true)]
		public string Options 
		{
			get { return GetColumnValue<string>(Columns.Options); }
			set { SetColumnValue(Columns.Options, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderDetailGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn QuantityColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OptionsColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderDetailGuid = @"order_detail_guid";
			 public static string ItemName = @"item_name";
			 public static string Quantity = @"quantity";
			 public static string Options = @"options";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

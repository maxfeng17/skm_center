using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the MgmGift class.
    /// </summary>
    [Serializable]
    public partial class MgmGiftCollection : RepositoryList<MgmGift, MgmGiftCollection>
    {
        public MgmGiftCollection() {}

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MgmGiftCollection</returns>
        public MgmGiftCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MgmGift o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the mgm_gift table.
    /// </summary>
    [Serializable]
    public partial class MgmGift : RepositoryRecord<MgmGift>, IRecordBase
    {
        #region .ctors and Default Settings

        public MgmGift()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public MgmGift(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("mgm_gift", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarIsUsed = new TableSchema.TableColumn(schema);
                colvarIsUsed.ColumnName = "is_used";
                colvarIsUsed.DataType = DbType.Boolean;
                colvarIsUsed.MaxLength = 0;
                colvarIsUsed.AutoIncrement = false;
                colvarIsUsed.IsNullable = false;
                colvarIsUsed.IsPrimaryKey = false;
                colvarIsUsed.IsForeignKey = false;
                colvarIsUsed.IsReadOnly = false;
                colvarIsUsed.DefaultSetting = @"";
                colvarIsUsed.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsUsed);

                TableSchema.TableColumn colvarUsedTime = new TableSchema.TableColumn(schema);
                colvarUsedTime.ColumnName = "used_time";
                colvarUsedTime.DataType = DbType.DateTime;
                colvarUsedTime.MaxLength = 0;
                colvarUsedTime.AutoIncrement = false;
                colvarUsedTime.IsNullable = true;
                colvarUsedTime.IsPrimaryKey = false;
                colvarUsedTime.IsForeignKey = false;
                colvarUsedTime.IsReadOnly = false;
                colvarUsedTime.DefaultSetting = @"";
                colvarUsedTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUsedTime);

                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;
                colvarTrustId.DefaultSetting = @"";
                colvarTrustId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustId);

                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                colvarBid.DefaultSetting = @"";
                colvarBid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBid);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarAccessCode = new TableSchema.TableColumn(schema);
                colvarAccessCode.ColumnName = "access_code";
                colvarAccessCode.DataType = DbType.Guid;
                colvarAccessCode.MaxLength = 0;
                colvarAccessCode.AutoIncrement = false;
                colvarAccessCode.IsNullable = true;
                colvarAccessCode.IsPrimaryKey = false;
                colvarAccessCode.IsForeignKey = false;
                colvarAccessCode.IsReadOnly = false;
                colvarAccessCode.DefaultSetting = @"";
                colvarAccessCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAccessCode);

                TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
                colvarSequenceNumber.ColumnName = "sequence_number";
                colvarSequenceNumber.DataType = DbType.AnsiString;
                colvarSequenceNumber.MaxLength = 5;
                colvarSequenceNumber.AutoIncrement = false;
                colvarSequenceNumber.IsNullable = true;
                colvarSequenceNumber.IsPrimaryKey = false;
                colvarSequenceNumber.IsForeignKey = false;
                colvarSequenceNumber.IsReadOnly = false;

                colvarSequenceNumber.DefaultSetting = @"('')";
                colvarSequenceNumber.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSequenceNumber);

                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = false;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                colvarCouponId.DefaultSetting = @"";
                colvarCouponId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCouponId);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                colvarOrderGuid.DefaultSetting = @"";
                colvarOrderGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderGuid);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("mgm_gift",schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("IsUsed")]
        [Bindable(true)]
        public bool IsUsed
        {
            get { return GetColumnValue<bool>(Columns.IsUsed); }
            set { SetColumnValue(Columns.IsUsed, value); }
        }

        [XmlAttribute("UsedTime")]
        [Bindable(true)]
        public DateTime? UsedTime
        {
            get { return GetColumnValue<DateTime?>(Columns.UsedTime); }
            set { SetColumnValue(Columns.UsedTime, value); }
        }

        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId
        {
            get { return GetColumnValue<Guid>(Columns.TrustId); }
            set { SetColumnValue(Columns.TrustId, value); }
        }

        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid
        {
            get { return GetColumnValue<Guid>(Columns.Bid); }
            set { SetColumnValue(Columns.Bid, value); }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("AccessCode")]
        [Bindable(true)]
        public Guid? AccessCode
        {
            get { return GetColumnValue<Guid?>(Columns.AccessCode); }
            set { SetColumnValue(Columns.AccessCode, value); }
        }

        [XmlAttribute("SequenceNumber")]
        [Bindable(true)]
        public string SequenceNumber
        {
            get { return GetColumnValue<string>(Columns.SequenceNumber); }
            set { SetColumnValue(Columns.SequenceNumber, value); }
        }

        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int CouponId
        {
            get { return GetColumnValue<int>(Columns.CouponId); }
            set { SetColumnValue(Columns.CouponId, value); }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid
        {
            get { return GetColumnValue<Guid>(Columns.OrderGuid); }
            set { SetColumnValue(Columns.OrderGuid, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn IsUsedColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn UsedTimeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn AccessCodeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn SequenceNumberColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[9]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string IsUsed = @"is_used";
            public static string UsedTime = @"used_time";
            public static string TrustId = @"trust_id";
            public static string Bid = @"bid";
            public static string Guid = @"guid";
            public static string AccessCode = @"access_code";
            public static string SequenceNumber = @"sequence_number";
            public static string CouponId = @"coupon_id";
            public static string OrderGuid = @"order_guid";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

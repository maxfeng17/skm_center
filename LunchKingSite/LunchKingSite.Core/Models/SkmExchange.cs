using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmExchange class.
	/// </summary>
    [Serializable]
	public partial class SkmExchangeCollection : RepositoryList<SkmExchange, SkmExchangeCollection>
	{	   
		public SkmExchangeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmExchangeCollection</returns>
		public SkmExchangeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmExchange o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_exchange table.
	/// </summary>
	
	[Serializable]
	public partial class SkmExchange : RepositoryRecord<SkmExchange>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmExchange()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmExchange(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_exchange", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarShopCode = new TableSchema.TableColumn(schema);
				colvarShopCode.ColumnName = "shop_code";
				colvarShopCode.DataType = DbType.String;
				colvarShopCode.MaxLength = 4;
				colvarShopCode.AutoIncrement = false;
				colvarShopCode.IsNullable = false;
				colvarShopCode.IsPrimaryKey = true;
				colvarShopCode.IsForeignKey = false;
				colvarShopCode.IsReadOnly = false;
				colvarShopCode.DefaultSetting = @"";
				colvarShopCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShopCode);
				
				TableSchema.TableColumn colvarFloor = new TableSchema.TableColumn(schema);
				colvarFloor.ColumnName = "floor";
				colvarFloor.DataType = DbType.String;
				colvarFloor.MaxLength = 50;
				colvarFloor.AutoIncrement = false;
				colvarFloor.IsNullable = false;
				colvarFloor.IsPrimaryKey = true;
				colvarFloor.IsForeignKey = false;
				colvarFloor.IsReadOnly = false;
				colvarFloor.DefaultSetting = @"";
				colvarFloor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFloor);
				
				TableSchema.TableColumn colvarBrandCounterCode = new TableSchema.TableColumn(schema);
				colvarBrandCounterCode.ColumnName = "brand_counter_code";
				colvarBrandCounterCode.DataType = DbType.AnsiString;
				colvarBrandCounterCode.MaxLength = 10;
				colvarBrandCounterCode.AutoIncrement = false;
				colvarBrandCounterCode.IsNullable = true;
				colvarBrandCounterCode.IsPrimaryKey = false;
				colvarBrandCounterCode.IsForeignKey = false;
				colvarBrandCounterCode.IsReadOnly = false;
				colvarBrandCounterCode.DefaultSetting = @"";
				colvarBrandCounterCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandCounterCode);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_exchange",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("ShopCode")]
		[Bindable(true)]
		public string ShopCode 
		{
			get { return GetColumnValue<string>(Columns.ShopCode); }
			set { SetColumnValue(Columns.ShopCode, value); }
		}
		
		[XmlAttribute("Floor")]
		[Bindable(true)]
		public string Floor 
		{
			get { return GetColumnValue<string>(Columns.Floor); }
			set { SetColumnValue(Columns.Floor, value); }
		}
		
		[XmlAttribute("BrandCounterCode")]
		[Bindable(true)]
		public string BrandCounterCode 
		{
			get { return GetColumnValue<string>(Columns.BrandCounterCode); }
			set { SetColumnValue(Columns.BrandCounterCode, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn ShopCodeColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn FloorColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BrandCounterCodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string ShopCode = @"shop_code";
			 public static string Floor = @"floor";
			 public static string BrandCounterCode = @"brand_counter_code";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

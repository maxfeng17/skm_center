using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OpendataWeather class.
	/// </summary>
    [Serializable]
	public partial class OpendataWeatherCollection : RepositoryList<OpendataWeather, OpendataWeatherCollection>
	{	   
		public OpendataWeatherCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OpendataWeatherCollection</returns>
		public OpendataWeatherCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OpendataWeather o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the opendata_weather table.
	/// </summary>
	[Serializable]
	public partial class OpendataWeather : RepositoryRecord<OpendataWeather>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OpendataWeather()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OpendataWeather(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("opendata_weather", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCity = new TableSchema.TableColumn(schema);
				colvarCity.ColumnName = "city";
				colvarCity.DataType = DbType.String;
				colvarCity.MaxLength = 12;
				colvarCity.AutoIncrement = false;
				colvarCity.IsNullable = true;
				colvarCity.IsPrimaryKey = false;
				colvarCity.IsForeignKey = false;
				colvarCity.IsReadOnly = false;
				colvarCity.DefaultSetting = @"";
				colvarCity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCity);
				
				TableSchema.TableColumn colvarStime = new TableSchema.TableColumn(schema);
				colvarStime.ColumnName = "stime";
				colvarStime.DataType = DbType.DateTime;
				colvarStime.MaxLength = 0;
				colvarStime.AutoIncrement = false;
				colvarStime.IsNullable = true;
				colvarStime.IsPrimaryKey = false;
				colvarStime.IsForeignKey = false;
				colvarStime.IsReadOnly = false;
				colvarStime.DefaultSetting = @"";
				colvarStime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStime);
				
				TableSchema.TableColumn colvarEtime = new TableSchema.TableColumn(schema);
				colvarEtime.ColumnName = "etime";
				colvarEtime.DataType = DbType.DateTime;
				colvarEtime.MaxLength = 0;
				colvarEtime.AutoIncrement = false;
				colvarEtime.IsNullable = true;
				colvarEtime.IsPrimaryKey = false;
				colvarEtime.IsForeignKey = false;
				colvarEtime.IsReadOnly = false;
				colvarEtime.DefaultSetting = @"";
				colvarEtime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEtime);
				
				TableSchema.TableColumn colvarWx = new TableSchema.TableColumn(schema);
				colvarWx.ColumnName = "Wx";
				colvarWx.DataType = DbType.String;
				colvarWx.MaxLength = 40;
				colvarWx.AutoIncrement = false;
				colvarWx.IsNullable = true;
				colvarWx.IsPrimaryKey = false;
				colvarWx.IsForeignKey = false;
				colvarWx.IsReadOnly = false;
				colvarWx.DefaultSetting = @"";
				colvarWx.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWx);
				
				TableSchema.TableColumn colvarMaxT = new TableSchema.TableColumn(schema);
				colvarMaxT.ColumnName = "MaxT";
				colvarMaxT.DataType = DbType.Int32;
				colvarMaxT.MaxLength = 0;
				colvarMaxT.AutoIncrement = false;
				colvarMaxT.IsNullable = true;
				colvarMaxT.IsPrimaryKey = false;
				colvarMaxT.IsForeignKey = false;
				colvarMaxT.IsReadOnly = false;
				colvarMaxT.DefaultSetting = @"";
				colvarMaxT.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMaxT);
				
				TableSchema.TableColumn colvarMinT = new TableSchema.TableColumn(schema);
				colvarMinT.ColumnName = "MinT";
				colvarMinT.DataType = DbType.Int32;
				colvarMinT.MaxLength = 0;
				colvarMinT.AutoIncrement = false;
				colvarMinT.IsNullable = true;
				colvarMinT.IsPrimaryKey = false;
				colvarMinT.IsForeignKey = false;
				colvarMinT.IsReadOnly = false;
				colvarMinT.DefaultSetting = @"";
				colvarMinT.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMinT);
				
				TableSchema.TableColumn colvarCi = new TableSchema.TableColumn(schema);
				colvarCi.ColumnName = "CI";
				colvarCi.DataType = DbType.String;
				colvarCi.MaxLength = 10;
				colvarCi.AutoIncrement = false;
				colvarCi.IsNullable = true;
				colvarCi.IsPrimaryKey = false;
				colvarCi.IsForeignKey = false;
				colvarCi.IsReadOnly = false;
				colvarCi.DefaultSetting = @"";
				colvarCi.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCi);
				
				TableSchema.TableColumn colvarPoP = new TableSchema.TableColumn(schema);
				colvarPoP.ColumnName = "PoP";
				colvarPoP.DataType = DbType.String;
				colvarPoP.MaxLength = 10;
				colvarPoP.AutoIncrement = false;
				colvarPoP.IsNullable = true;
				colvarPoP.IsPrimaryKey = false;
				colvarPoP.IsForeignKey = false;
				colvarPoP.IsReadOnly = false;
				colvarPoP.DefaultSetting = @"";
				colvarPoP.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPoP);
				
				TableSchema.TableColumn colvarCreatetime = new TableSchema.TableColumn(schema);
				colvarCreatetime.ColumnName = "createtime";
				colvarCreatetime.DataType = DbType.DateTime;
				colvarCreatetime.MaxLength = 0;
				colvarCreatetime.AutoIncrement = false;
				colvarCreatetime.IsNullable = true;
				colvarCreatetime.IsPrimaryKey = false;
				colvarCreatetime.IsForeignKey = false;
				colvarCreatetime.IsReadOnly = false;
				
						colvarCreatetime.DefaultSetting = @"(getdate())";
				colvarCreatetime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatetime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("opendata_weather",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("City")]
		[Bindable(true)]
		public string City 
		{
			get { return GetColumnValue<string>(Columns.City); }
			set { SetColumnValue(Columns.City, value); }
		}
		  
		[XmlAttribute("Stime")]
		[Bindable(true)]
		public DateTime? Stime 
		{
			get { return GetColumnValue<DateTime?>(Columns.Stime); }
			set { SetColumnValue(Columns.Stime, value); }
		}
		  
		[XmlAttribute("Etime")]
		[Bindable(true)]
		public DateTime? Etime 
		{
			get { return GetColumnValue<DateTime?>(Columns.Etime); }
			set { SetColumnValue(Columns.Etime, value); }
		}
		  
		[XmlAttribute("Wx")]
		[Bindable(true)]
		public string Wx 
		{
			get { return GetColumnValue<string>(Columns.Wx); }
			set { SetColumnValue(Columns.Wx, value); }
		}
		  
		[XmlAttribute("MaxT")]
		[Bindable(true)]
		public int? MaxT 
		{
			get { return GetColumnValue<int?>(Columns.MaxT); }
			set { SetColumnValue(Columns.MaxT, value); }
		}
		  
		[XmlAttribute("MinT")]
		[Bindable(true)]
		public int? MinT 
		{
			get { return GetColumnValue<int?>(Columns.MinT); }
			set { SetColumnValue(Columns.MinT, value); }
		}
		  
		[XmlAttribute("Ci")]
		[Bindable(true)]
		public string Ci 
		{
			get { return GetColumnValue<string>(Columns.Ci); }
			set { SetColumnValue(Columns.Ci, value); }
		}
		  
		[XmlAttribute("PoP")]
		[Bindable(true)]
		public string PoP 
		{
			get { return GetColumnValue<string>(Columns.PoP); }
			set { SetColumnValue(Columns.PoP, value); }
		}
		  
		[XmlAttribute("Createtime")]
		[Bindable(true)]
		public DateTime? Createtime 
		{
			get { return GetColumnValue<DateTime?>(Columns.Createtime); }
			set { SetColumnValue(Columns.Createtime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CityColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StimeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn EtimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn WxColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MaxTColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MinTColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CiColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn PoPColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatetimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string City = @"city";
			 public static string Stime = @"stime";
			 public static string Etime = @"etime";
			 public static string Wx = @"Wx";
			 public static string MaxT = @"MaxT";
			 public static string MinT = @"MinT";
			 public static string Ci = @"CI";
			 public static string PoP = @"PoP";
			 public static string Createtime = @"createtime";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

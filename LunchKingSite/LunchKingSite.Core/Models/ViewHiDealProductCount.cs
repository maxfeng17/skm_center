using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealProductCount class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealProductCountCollection : ReadOnlyList<ViewHiDealProductCount, ViewHiDealProductCountCollection>
    {        
        public ViewHiDealProductCountCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_product_count view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealProductCount : ReadOnlyRecord<ViewHiDealProductCount>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_product_count", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = true;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarDealGuid = new TableSchema.TableColumn(schema);
                colvarDealGuid.ColumnName = "deal_guid";
                colvarDealGuid.DataType = DbType.Guid;
                colvarDealGuid.MaxLength = 0;
                colvarDealGuid.AutoIncrement = false;
                colvarDealGuid.IsNullable = false;
                colvarDealGuid.IsPrimaryKey = false;
                colvarDealGuid.IsForeignKey = false;
                colvarDealGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealGuid);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 50;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "product_name";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 250;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = true;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductName);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarDealPromoShortDesc = new TableSchema.TableColumn(schema);
                colvarDealPromoShortDesc.ColumnName = "deal_promo_short_desc";
                colvarDealPromoShortDesc.DataType = DbType.String;
                colvarDealPromoShortDesc.MaxLength = 100;
                colvarDealPromoShortDesc.AutoIncrement = false;
                colvarDealPromoShortDesc.IsNullable = true;
                colvarDealPromoShortDesc.IsPrimaryKey = false;
                colvarDealPromoShortDesc.IsForeignKey = false;
                colvarDealPromoShortDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealPromoShortDesc);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
                colvarItemQuantity.ColumnName = "item_quantity";
                colvarItemQuantity.DataType = DbType.Int32;
                colvarItemQuantity.MaxLength = 0;
                colvarItemQuantity.AutoIncrement = false;
                colvarItemQuantity.IsNullable = true;
                colvarItemQuantity.IsPrimaryKey = false;
                colvarItemQuantity.IsForeignKey = false;
                colvarItemQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemQuantity);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_product_count",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealProductCount()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealProductCount(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealProductCount(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealProductCount(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int? ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("DealGuid")]
        [Bindable(true)]
        public Guid DealGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("deal_guid");
		    }
            set 
		    {
			    SetColumnValue("deal_guid", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_name");
		    }
            set 
		    {
			    SetColumnValue("deal_name", value);
            }
        }
	      
        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_name");
		    }
            set 
		    {
			    SetColumnValue("product_name", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("DealPromoShortDesc")]
        [Bindable(true)]
        public string DealPromoShortDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_promo_short_desc");
		    }
            set 
		    {
			    SetColumnValue("deal_promo_short_desc", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_start_time", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("ItemQuantity")]
        [Bindable(true)]
        public int? ItemQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("item_quantity");
		    }
            set 
		    {
			    SetColumnValue("item_quantity", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ProductId = @"product_id";
            
            public static string DealId = @"deal_id";
            
            public static string DealGuid = @"deal_guid";
            
            public static string DealName = @"deal_name";
            
            public static string ProductName = @"product_name";
            
            public static string SellerName = @"seller_name";
            
            public static string DealPromoShortDesc = @"deal_promo_short_desc";
            
            public static string DealStartTime = @"deal_start_time";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string ItemQuantity = @"item_quantity";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

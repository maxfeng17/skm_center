using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ElmahError class.
	/// </summary>
    [Serializable]
	public partial class ElmahErrorCollection : RepositoryList<ElmahError, ElmahErrorCollection>
	{	   
		public ElmahErrorCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ElmahErrorCollection</returns>
		public ElmahErrorCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ElmahError o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the ELMAH_Error table.
	/// </summary>
	[Serializable]
	public partial class ElmahError : RepositoryRecord<ElmahError>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ElmahError()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ElmahError(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("ELMAH_Error", TableType.Table, DataService.GetInstance("elmah"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarErrorId = new TableSchema.TableColumn(schema);
				colvarErrorId.ColumnName = "ErrorId";
				colvarErrorId.DataType = DbType.Guid;
				colvarErrorId.MaxLength = 0;
				colvarErrorId.AutoIncrement = false;
				colvarErrorId.IsNullable = false;
				colvarErrorId.IsPrimaryKey = true;
				colvarErrorId.IsForeignKey = false;
				colvarErrorId.IsReadOnly = false;
				colvarErrorId.DefaultSetting = @"";
				colvarErrorId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarErrorId);
				
				TableSchema.TableColumn colvarApplication = new TableSchema.TableColumn(schema);
				colvarApplication.ColumnName = "Application";
				colvarApplication.DataType = DbType.String;
				colvarApplication.MaxLength = 60;
				colvarApplication.AutoIncrement = false;
				colvarApplication.IsNullable = false;
				colvarApplication.IsPrimaryKey = false;
				colvarApplication.IsForeignKey = false;
				colvarApplication.IsReadOnly = false;
				colvarApplication.DefaultSetting = @"";
				colvarApplication.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApplication);
				
				TableSchema.TableColumn colvarHost = new TableSchema.TableColumn(schema);
				colvarHost.ColumnName = "Host";
				colvarHost.DataType = DbType.String;
				colvarHost.MaxLength = 50;
				colvarHost.AutoIncrement = false;
				colvarHost.IsNullable = false;
				colvarHost.IsPrimaryKey = false;
				colvarHost.IsForeignKey = false;
				colvarHost.IsReadOnly = false;
				colvarHost.DefaultSetting = @"";
				colvarHost.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHost);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "Type";
				colvarType.DataType = DbType.String;
				colvarType.MaxLength = 100;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarSource = new TableSchema.TableColumn(schema);
				colvarSource.ColumnName = "Source";
				colvarSource.DataType = DbType.String;
				colvarSource.MaxLength = 60;
				colvarSource.AutoIncrement = false;
				colvarSource.IsNullable = false;
				colvarSource.IsPrimaryKey = false;
				colvarSource.IsForeignKey = false;
				colvarSource.IsReadOnly = false;
				colvarSource.DefaultSetting = @"";
				colvarSource.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSource);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "Message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 500;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = false;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarUser = new TableSchema.TableColumn(schema);
				colvarUser.ColumnName = "User";
				colvarUser.DataType = DbType.String;
				colvarUser.MaxLength = 50;
				colvarUser.AutoIncrement = false;
				colvarUser.IsNullable = false;
				colvarUser.IsPrimaryKey = false;
				colvarUser.IsForeignKey = false;
				colvarUser.IsReadOnly = false;
				colvarUser.DefaultSetting = @"";
				colvarUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUser);
				
				TableSchema.TableColumn colvarStatusCode = new TableSchema.TableColumn(schema);
				colvarStatusCode.ColumnName = "StatusCode";
				colvarStatusCode.DataType = DbType.Int32;
				colvarStatusCode.MaxLength = 0;
				colvarStatusCode.AutoIncrement = false;
				colvarStatusCode.IsNullable = false;
				colvarStatusCode.IsPrimaryKey = false;
				colvarStatusCode.IsForeignKey = false;
				colvarStatusCode.IsReadOnly = false;
				colvarStatusCode.DefaultSetting = @"";
				colvarStatusCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatusCode);
				
				TableSchema.TableColumn colvarTimeUtc = new TableSchema.TableColumn(schema);
				colvarTimeUtc.ColumnName = "TimeUtc";
				colvarTimeUtc.DataType = DbType.DateTime;
				colvarTimeUtc.MaxLength = 0;
				colvarTimeUtc.AutoIncrement = false;
				colvarTimeUtc.IsNullable = false;
				colvarTimeUtc.IsPrimaryKey = false;
				colvarTimeUtc.IsForeignKey = false;
				colvarTimeUtc.IsReadOnly = false;
				colvarTimeUtc.DefaultSetting = @"";
				colvarTimeUtc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTimeUtc);
				
				TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
				colvarSequence.ColumnName = "Sequence";
				colvarSequence.DataType = DbType.Int32;
				colvarSequence.MaxLength = 0;
				colvarSequence.AutoIncrement = false;
				colvarSequence.IsNullable = false;
				colvarSequence.IsPrimaryKey = false;
				colvarSequence.IsForeignKey = false;
				colvarSequence.IsReadOnly = false;
				colvarSequence.DefaultSetting = @"";
				colvarSequence.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSequence);
				
				TableSchema.TableColumn colvarAllXml = new TableSchema.TableColumn(schema);
				colvarAllXml.ColumnName = "AllXml";
				colvarAllXml.DataType = DbType.String;
				colvarAllXml.MaxLength = 1073741823;
				colvarAllXml.AutoIncrement = false;
				colvarAllXml.IsNullable = false;
				colvarAllXml.IsPrimaryKey = false;
				colvarAllXml.IsForeignKey = false;
				colvarAllXml.IsReadOnly = false;
				colvarAllXml.DefaultSetting = @"";
				colvarAllXml.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAllXml);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["elmah"].AddSchema("ELMAH_Error",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("ErrorId")]
		[Bindable(true)]
		public Guid ErrorId 
		{
			get { return GetColumnValue<Guid>(Columns.ErrorId); }
			set { SetColumnValue(Columns.ErrorId, value); }
		}
		  
		[XmlAttribute("Application")]
		[Bindable(true)]
		public string Application 
		{
			get { return GetColumnValue<string>(Columns.Application); }
			set { SetColumnValue(Columns.Application, value); }
		}
		  
		[XmlAttribute("Host")]
		[Bindable(true)]
		public string Host 
		{
			get { return GetColumnValue<string>(Columns.Host); }
			set { SetColumnValue(Columns.Host, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public string Type 
		{
			get { return GetColumnValue<string>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("Source")]
		[Bindable(true)]
		public string Source 
		{
			get { return GetColumnValue<string>(Columns.Source); }
			set { SetColumnValue(Columns.Source, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("User")]
		[Bindable(true)]
		public string User 
		{
			get { return GetColumnValue<string>(Columns.User); }
			set { SetColumnValue(Columns.User, value); }
		}
		  
		[XmlAttribute("StatusCode")]
		[Bindable(true)]
		public int StatusCode 
		{
			get { return GetColumnValue<int>(Columns.StatusCode); }
			set { SetColumnValue(Columns.StatusCode, value); }
		}
		  
		[XmlAttribute("TimeUtc")]
		[Bindable(true)]
		public DateTime TimeUtc 
		{
			get { return GetColumnValue<DateTime>(Columns.TimeUtc); }
			set { SetColumnValue(Columns.TimeUtc, value); }
		}
		  
		[XmlAttribute("Sequence")]
		[Bindable(true)]
		public int Sequence 
		{
			get { return GetColumnValue<int>(Columns.Sequence); }
			set { SetColumnValue(Columns.Sequence, value); }
		}
		  
		[XmlAttribute("AllXml")]
		[Bindable(true)]
		public string AllXml 
		{
			get { return GetColumnValue<string>(Columns.AllXml); }
			set { SetColumnValue(Columns.AllXml, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn ErrorIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ApplicationColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn HostColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SourceColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn UserColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusCodeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn TimeUtcColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn AllXmlColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string ErrorId = @"ErrorId";
			 public static string Application = @"Application";
			 public static string Host = @"Host";
			 public static string Type = @"Type";
			 public static string Source = @"Source";
			 public static string Message = @"Message";
			 public static string User = @"User";
			 public static string StatusCode = @"StatusCode";
			 public static string TimeUtc = @"TimeUtc";
			 public static string Sequence = @"Sequence";
			 public static string AllXml = @"AllXml";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

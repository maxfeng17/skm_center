using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealWeeklyPayReport class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealWeeklyPayReportCollection : ReadOnlyList<ViewHiDealWeeklyPayReport, ViewHiDealWeeklyPayReportCollection>
    {
        public ViewHiDealWeeklyPayReportCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_weekly_pay_report view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealWeeklyPayReport : ReadOnlyRecord<ViewHiDealWeeklyPayReport>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_weekly_pay_report", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;

                schema.Columns.Add(colvarDealId);

                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = false;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;

                schema.Columns.Add(colvarProductId);

                TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
                colvarEventName.ColumnName = "event_name";
                colvarEventName.DataType = DbType.String;
                colvarEventName.MaxLength = 500;
                colvarEventName.AutoIncrement = false;
                colvarEventName.IsNullable = false;
                colvarEventName.IsPrimaryKey = false;
                colvarEventName.IsForeignKey = false;
                colvarEventName.IsReadOnly = false;

                schema.Columns.Add(colvarEventName);

                TableSchema.TableColumn colvarIntervalStart = new TableSchema.TableColumn(schema);
                colvarIntervalStart.ColumnName = "interval_start";
                colvarIntervalStart.DataType = DbType.DateTime;
                colvarIntervalStart.MaxLength = 0;
                colvarIntervalStart.AutoIncrement = false;
                colvarIntervalStart.IsNullable = false;
                colvarIntervalStart.IsPrimaryKey = false;
                colvarIntervalStart.IsForeignKey = false;
                colvarIntervalStart.IsReadOnly = false;

                schema.Columns.Add(colvarIntervalStart);

                TableSchema.TableColumn colvarIntervalEnd = new TableSchema.TableColumn(schema);
                colvarIntervalEnd.ColumnName = "interval_end";
                colvarIntervalEnd.DataType = DbType.DateTime;
                colvarIntervalEnd.MaxLength = 0;
                colvarIntervalEnd.AutoIncrement = false;
                colvarIntervalEnd.IsNullable = false;
                colvarIntervalEnd.IsPrimaryKey = false;
                colvarIntervalEnd.IsForeignKey = false;
                colvarIntervalEnd.IsReadOnly = false;

                schema.Columns.Add(colvarIntervalEnd);

                TableSchema.TableColumn colvarCreditCard = new TableSchema.TableColumn(schema);
                colvarCreditCard.ColumnName = "credit_card";
                colvarCreditCard.DataType = DbType.Int32;
                colvarCreditCard.MaxLength = 0;
                colvarCreditCard.AutoIncrement = false;
                colvarCreditCard.IsNullable = false;
                colvarCreditCard.IsPrimaryKey = false;
                colvarCreditCard.IsForeignKey = false;
                colvarCreditCard.IsReadOnly = false;

                schema.Columns.Add(colvarCreditCard);

                TableSchema.TableColumn colvarAtm = new TableSchema.TableColumn(schema);
                colvarAtm.ColumnName = "atm";
                colvarAtm.DataType = DbType.Int32;
                colvarAtm.MaxLength = 0;
                colvarAtm.AutoIncrement = false;
                colvarAtm.IsNullable = false;
                colvarAtm.IsPrimaryKey = false;
                colvarAtm.IsForeignKey = false;
                colvarAtm.IsReadOnly = false;

                schema.Columns.Add(colvarAtm);

                TableSchema.TableColumn colvarIsSummary = new TableSchema.TableColumn(schema);
                colvarIsSummary.ColumnName = "is_summary";
                colvarIsSummary.DataType = DbType.Boolean;
                colvarIsSummary.MaxLength = 0;
                colvarIsSummary.AutoIncrement = false;
                colvarIsSummary.IsNullable = false;
                colvarIsSummary.IsPrimaryKey = false;
                colvarIsSummary.IsForeignKey = false;
                colvarIsSummary.IsReadOnly = false;

                schema.Columns.Add(colvarIsSummary);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = true;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;

                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;

                schema.Columns.Add(colvarStoreGuid);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
                colvarCreator.ColumnName = "creator";
                colvarCreator.DataType = DbType.String;
                colvarCreator.MaxLength = 100;
                colvarCreator.AutoIncrement = false;
                colvarCreator.IsNullable = false;
                colvarCreator.IsPrimaryKey = false;
                colvarCreator.IsForeignKey = false;
                colvarCreator.IsReadOnly = false;

                schema.Columns.Add(colvarCreator);

                TableSchema.TableColumn colvarErrorOutSum = new TableSchema.TableColumn(schema);
                colvarErrorOutSum.ColumnName = "error_out_sum";
                colvarErrorOutSum.DataType = DbType.Int32;
                colvarErrorOutSum.MaxLength = 0;
                colvarErrorOutSum.AutoIncrement = false;
                colvarErrorOutSum.IsNullable = false;
                colvarErrorOutSum.IsPrimaryKey = false;
                colvarErrorOutSum.IsForeignKey = false;
                colvarErrorOutSum.IsReadOnly = false;

                schema.Columns.Add(colvarErrorOutSum);

                TableSchema.TableColumn colvarErrorInSum = new TableSchema.TableColumn(schema);
                colvarErrorInSum.ColumnName = "error_in_sum";
                colvarErrorInSum.DataType = DbType.Int32;
                colvarErrorInSum.MaxLength = 0;
                colvarErrorInSum.AutoIncrement = false;
                colvarErrorInSum.IsNullable = false;
                colvarErrorInSum.IsPrimaryKey = false;
                colvarErrorInSum.IsForeignKey = false;
                colvarErrorInSum.IsReadOnly = false;

                schema.Columns.Add(colvarErrorInSum);

                TableSchema.TableColumn colvarErrorOut = new TableSchema.TableColumn(schema);
                colvarErrorOut.ColumnName = "error_out";
                colvarErrorOut.DataType = DbType.Decimal;
                colvarErrorOut.MaxLength = 0;
                colvarErrorOut.AutoIncrement = false;
                colvarErrorOut.IsNullable = false;
                colvarErrorOut.IsPrimaryKey = false;
                colvarErrorOut.IsForeignKey = false;
                colvarErrorOut.IsReadOnly = false;

                schema.Columns.Add(colvarErrorOut);

                TableSchema.TableColumn colvarErrorIn = new TableSchema.TableColumn(schema);
                colvarErrorIn.ColumnName = "error_in";
                colvarErrorIn.DataType = DbType.Decimal;
                colvarErrorIn.MaxLength = 0;
                colvarErrorIn.AutoIncrement = false;
                colvarErrorIn.IsNullable = false;
                colvarErrorIn.IsPrimaryKey = false;
                colvarErrorIn.IsForeignKey = false;
                colvarErrorIn.IsReadOnly = false;

                schema.Columns.Add(colvarErrorIn);

                TableSchema.TableColumn colvarTotalSum = new TableSchema.TableColumn(schema);
                colvarTotalSum.ColumnName = "total_sum";
                colvarTotalSum.DataType = DbType.Decimal;
                colvarTotalSum.MaxLength = 0;
                colvarTotalSum.AutoIncrement = false;
                colvarTotalSum.IsNullable = false;
                colvarTotalSum.IsPrimaryKey = false;
                colvarTotalSum.IsForeignKey = false;
                colvarTotalSum.IsReadOnly = false;

                schema.Columns.Add(colvarTotalSum);

                TableSchema.TableColumn colvarTotalCount = new TableSchema.TableColumn(schema);
                colvarTotalCount.ColumnName = "total_count";
                colvarTotalCount.DataType = DbType.Int32;
                colvarTotalCount.MaxLength = 0;
                colvarTotalCount.AutoIncrement = false;
                colvarTotalCount.IsNullable = false;
                colvarTotalCount.IsPrimaryKey = false;
                colvarTotalCount.IsForeignKey = false;
                colvarTotalCount.IsReadOnly = false;

                schema.Columns.Add(colvarTotalCount);

                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Decimal;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = false;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;

                schema.Columns.Add(colvarCost);

                TableSchema.TableColumn colvarIsLastWeek = new TableSchema.TableColumn(schema);
                colvarIsLastWeek.ColumnName = "is_last_week";
                colvarIsLastWeek.DataType = DbType.Boolean;
                colvarIsLastWeek.MaxLength = 0;
                colvarIsLastWeek.AutoIncrement = false;
                colvarIsLastWeek.IsNullable = false;
                colvarIsLastWeek.IsPrimaryKey = false;
                colvarIsLastWeek.IsForeignKey = false;
                colvarIsLastWeek.IsReadOnly = false;

                schema.Columns.Add(colvarIsLastWeek);

                TableSchema.TableColumn colvarReportGuid = new TableSchema.TableColumn(schema);
                colvarReportGuid.ColumnName = "report_guid";
                colvarReportGuid.DataType = DbType.Guid;
                colvarReportGuid.MaxLength = 0;
                colvarReportGuid.AutoIncrement = false;
                colvarReportGuid.IsNullable = false;
                colvarReportGuid.IsPrimaryKey = false;
                colvarReportGuid.IsForeignKey = false;
                colvarReportGuid.IsReadOnly = false;

                schema.Columns.Add(colvarReportGuid);

                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 100;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;

                schema.Columns.Add(colvarStoreName);

                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "company_name";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 200;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;

                schema.Columns.Add(colvarCompanyName);

                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;

                schema.Columns.Add(colvarSellerName);

                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.AnsiString;
                colvarAccountId.MaxLength = 10;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = true;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;

                schema.Columns.Add(colvarAccountId);

                TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
                colvarAccountName.ColumnName = "account_name";
                colvarAccountName.DataType = DbType.String;
                colvarAccountName.MaxLength = 100;
                colvarAccountName.AutoIncrement = false;
                colvarAccountName.IsNullable = true;
                colvarAccountName.IsPrimaryKey = false;
                colvarAccountName.IsForeignKey = false;
                colvarAccountName.IsReadOnly = false;

                schema.Columns.Add(colvarAccountName);

                TableSchema.TableColumn colvarAccountNo = new TableSchema.TableColumn(schema);
                colvarAccountNo.ColumnName = "account_no";
                colvarAccountNo.DataType = DbType.AnsiString;
                colvarAccountNo.MaxLength = 50;
                colvarAccountNo.AutoIncrement = false;
                colvarAccountNo.IsNullable = true;
                colvarAccountNo.IsPrimaryKey = false;
                colvarAccountNo.IsForeignKey = false;
                colvarAccountNo.IsReadOnly = false;

                schema.Columns.Add(colvarAccountNo);

                TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
                colvarEmail.ColumnName = "email";
                colvarEmail.DataType = DbType.AnsiString;
                colvarEmail.MaxLength = 100;
                colvarEmail.AutoIncrement = false;
                colvarEmail.IsNullable = true;
                colvarEmail.IsPrimaryKey = false;
                colvarEmail.IsForeignKey = false;
                colvarEmail.IsReadOnly = false;

                schema.Columns.Add(colvarEmail);

                TableSchema.TableColumn colvarBankNo = new TableSchema.TableColumn(schema);
                colvarBankNo.ColumnName = "bank_no";
                colvarBankNo.DataType = DbType.AnsiString;
                colvarBankNo.MaxLength = 20;
                colvarBankNo.AutoIncrement = false;
                colvarBankNo.IsNullable = true;
                colvarBankNo.IsPrimaryKey = false;
                colvarBankNo.IsForeignKey = false;
                colvarBankNo.IsReadOnly = false;

                schema.Columns.Add(colvarBankNo);

                TableSchema.TableColumn colvarBranchNo = new TableSchema.TableColumn(schema);
                colvarBranchNo.ColumnName = "branch_no";
                colvarBranchNo.DataType = DbType.AnsiString;
                colvarBranchNo.MaxLength = 20;
                colvarBranchNo.AutoIncrement = false;
                colvarBranchNo.IsNullable = true;
                colvarBranchNo.IsPrimaryKey = false;
                colvarBranchNo.IsForeignKey = false;
                colvarBranchNo.IsReadOnly = false;

                schema.Columns.Add(colvarBranchNo);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 1000;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;

                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarSignCompanyId = new TableSchema.TableColumn(schema);
                colvarSignCompanyId.ColumnName = "sign_company_id";
                colvarSignCompanyId.DataType = DbType.AnsiString;
                colvarSignCompanyId.MaxLength = 20;
                colvarSignCompanyId.AutoIncrement = false;
                colvarSignCompanyId.IsNullable = true;
                colvarSignCompanyId.IsPrimaryKey = false;
                colvarSignCompanyId.IsForeignKey = false;
                colvarSignCompanyId.IsReadOnly = false;

                schema.Columns.Add(colvarSignCompanyId);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_weekly_pay_report", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewHiDealWeeklyPayReport()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealWeeklyPayReport(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewHiDealWeeklyPayReport(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewHiDealWeeklyPayReport(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId
        {
            get
            {
                return GetColumnValue<int>("deal_id");
            }
            set
            {
                SetColumnValue("deal_id", value);
            }
        }

        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int ProductId
        {
            get
            {
                return GetColumnValue<int>("product_id");
            }
            set
            {
                SetColumnValue("product_id", value);
            }
        }

        [XmlAttribute("EventName")]
        [Bindable(true)]
        public string EventName
        {
            get
            {
                return GetColumnValue<string>("event_name");
            }
            set
            {
                SetColumnValue("event_name", value);
            }
        }

        [XmlAttribute("IntervalStart")]
        [Bindable(true)]
        public DateTime IntervalStart
        {
            get
            {
                return GetColumnValue<DateTime>("interval_start");
            }
            set
            {
                SetColumnValue("interval_start", value);
            }
        }

        [XmlAttribute("IntervalEnd")]
        [Bindable(true)]
        public DateTime IntervalEnd
        {
            get
            {
                return GetColumnValue<DateTime>("interval_end");
            }
            set
            {
                SetColumnValue("interval_end", value);
            }
        }

        [XmlAttribute("CreditCard")]
        [Bindable(true)]
        public int CreditCard
        {
            get
            {
                return GetColumnValue<int>("credit_card");
            }
            set
            {
                SetColumnValue("credit_card", value);
            }
        }

        [XmlAttribute("Atm")]
        [Bindable(true)]
        public int Atm
        {
            get
            {
                return GetColumnValue<int>("atm");
            }
            set
            {
                SetColumnValue("atm", value);
            }
        }

        [XmlAttribute("IsSummary")]
        [Bindable(true)]
        public bool IsSummary
        {
            get
            {
                return GetColumnValue<bool>("is_summary");
            }
            set
            {
                SetColumnValue("is_summary", value);
            }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid? SellerGuid
        {
            get
            {
                return GetColumnValue<Guid?>("seller_guid");
            }
            set
            {
                SetColumnValue("seller_guid", value);
            }
        }

        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid
        {
            get
            {
                return GetColumnValue<Guid?>("store_guid");
            }
            set
            {
                SetColumnValue("store_guid", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("Creator")]
        [Bindable(true)]
        public string Creator
        {
            get
            {
                return GetColumnValue<string>("creator");
            }
            set
            {
                SetColumnValue("creator", value);
            }
        }

        [XmlAttribute("ErrorOutSum")]
        [Bindable(true)]
        public int ErrorOutSum
        {
            get
            {
                return GetColumnValue<int>("error_out_sum");
            }
            set
            {
                SetColumnValue("error_out_sum", value);
            }
        }

        [XmlAttribute("ErrorInSum")]
        [Bindable(true)]
        public int ErrorInSum
        {
            get
            {
                return GetColumnValue<int>("error_in_sum");
            }
            set
            {
                SetColumnValue("error_in_sum", value);
            }
        }

        [XmlAttribute("ErrorOut")]
        [Bindable(true)]
        public decimal ErrorOut
        {
            get
            {
                return GetColumnValue<decimal>("error_out");
            }
            set
            {
                SetColumnValue("error_out", value);
            }
        }

        [XmlAttribute("ErrorIn")]
        [Bindable(true)]
        public decimal ErrorIn
        {
            get
            {
                return GetColumnValue<decimal>("error_in");
            }
            set
            {
                SetColumnValue("error_in", value);
            }
        }

        [XmlAttribute("TotalSum")]
        [Bindable(true)]
        public decimal TotalSum
        {
            get
            {
                return GetColumnValue<decimal>("total_sum");
            }
            set
            {
                SetColumnValue("total_sum", value);
            }
        }

        [XmlAttribute("TotalCount")]
        [Bindable(true)]
        public int TotalCount
        {
            get
            {
                return GetColumnValue<int>("total_count");
            }
            set
            {
                SetColumnValue("total_count", value);
            }
        }

        [XmlAttribute("Cost")]
        [Bindable(true)]
        public decimal Cost
        {
            get
            {
                return GetColumnValue<decimal>("cost");
            }
            set
            {
                SetColumnValue("cost", value);
            }
        }

        [XmlAttribute("IsLastWeek")]
        [Bindable(true)]
        public bool IsLastWeek
        {
            get
            {
                return GetColumnValue<bool>("is_last_week");
            }
            set
            {
                SetColumnValue("is_last_week", value);
            }
        }

        [XmlAttribute("ReportGuid")]
        [Bindable(true)]
        public Guid ReportGuid
        {
            get
            {
                return GetColumnValue<Guid>("report_guid");
            }
            set
            {
                SetColumnValue("report_guid", value);
            }
        }

        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName
        {
            get
            {
                return GetColumnValue<string>("store_name");
            }
            set
            {
                SetColumnValue("store_name", value);
            }
        }

        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName
        {
            get
            {
                return GetColumnValue<string>("company_name");
            }
            set
            {
                SetColumnValue("company_name", value);
            }
        }

        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName
        {
            get
            {
                return GetColumnValue<string>("seller_name");
            }
            set
            {
                SetColumnValue("seller_name", value);
            }
        }

        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId
        {
            get
            {
                return GetColumnValue<string>("account_id");
            }
            set
            {
                SetColumnValue("account_id", value);
            }
        }

        [XmlAttribute("AccountName")]
        [Bindable(true)]
        public string AccountName
        {
            get
            {
                return GetColumnValue<string>("account_name");
            }
            set
            {
                SetColumnValue("account_name", value);
            }
        }

        [XmlAttribute("AccountNo")]
        [Bindable(true)]
        public string AccountNo
        {
            get
            {
                return GetColumnValue<string>("account_no");
            }
            set
            {
                SetColumnValue("account_no", value);
            }
        }

        [XmlAttribute("Email")]
        [Bindable(true)]
        public string Email
        {
            get
            {
                return GetColumnValue<string>("email");
            }
            set
            {
                SetColumnValue("email", value);
            }
        }

        [XmlAttribute("BankNo")]
        [Bindable(true)]
        public string BankNo
        {
            get
            {
                return GetColumnValue<string>("bank_no");
            }
            set
            {
                SetColumnValue("bank_no", value);
            }
        }

        [XmlAttribute("BranchNo")]
        [Bindable(true)]
        public string BranchNo
        {
            get
            {
                return GetColumnValue<string>("branch_no");
            }
            set
            {
                SetColumnValue("branch_no", value);
            }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get
            {
                return GetColumnValue<string>("message");
            }
            set
            {
                SetColumnValue("message", value);
            }
        }

        [XmlAttribute("SignCompanyId")]
        [Bindable(true)]
        public string SignCompanyId
        {
            get
            {
                return GetColumnValue<string>("sign_company_id");
            }
            set
            {
                SetColumnValue("sign_company_id", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Id = @"id";

            public static string DealId = @"deal_id";

            public static string ProductId = @"product_id";

            public static string EventName = @"event_name";

            public static string IntervalStart = @"interval_start";

            public static string IntervalEnd = @"interval_end";

            public static string CreditCard = @"credit_card";

            public static string Atm = @"atm";

            public static string IsSummary = @"is_summary";

            public static string SellerGuid = @"seller_guid";

            public static string StoreGuid = @"store_guid";

            public static string CreateTime = @"create_time";

            public static string Creator = @"creator";

            public static string ErrorOutSum = @"error_out_sum";

            public static string ErrorInSum = @"error_in_sum";

            public static string ErrorOut = @"error_out";

            public static string ErrorIn = @"error_in";

            public static string TotalSum = @"total_sum";

            public static string TotalCount = @"total_count";

            public static string Cost = @"cost";

            public static string IsLastWeek = @"is_last_week";

            public static string ReportGuid = @"report_guid";

            public static string StoreName = @"store_name";

            public static string CompanyName = @"company_name";

            public static string SellerName = @"seller_name";

            public static string AccountId = @"account_id";

            public static string AccountName = @"account_name";

            public static string AccountNo = @"account_no";

            public static string Email = @"email";

            public static string BankNo = @"bank_no";

            public static string BranchNo = @"branch_no";

            public static string Message = @"message";

            public static string SignCompanyId = @"sign_company_id";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

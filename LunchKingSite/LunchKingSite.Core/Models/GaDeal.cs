using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the GaDeal class.
	/// </summary>
    [Serializable]
	public partial class GaDealCollection : RepositoryList<GaDeal, GaDealCollection>
	{	   
		public GaDealCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>GaDealCollection</returns>
		public GaDealCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                GaDeal o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the ga_deal table.
	/// </summary>
	[Serializable]
	public partial class GaDeal : RepositoryRecord<GaDeal>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public GaDeal()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public GaDeal(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("ga_deal", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarUsers = new TableSchema.TableColumn(schema);
				colvarUsers.ColumnName = "users";
				colvarUsers.DataType = DbType.Int32;
				colvarUsers.MaxLength = 0;
				colvarUsers.AutoIncrement = false;
				colvarUsers.IsNullable = false;
				colvarUsers.IsPrimaryKey = false;
				colvarUsers.IsForeignKey = false;
				colvarUsers.IsReadOnly = false;
				colvarUsers.DefaultSetting = @"";
				colvarUsers.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUsers);
				
				TableSchema.TableColumn colvarEntrances = new TableSchema.TableColumn(schema);
				colvarEntrances.ColumnName = "entrances";
				colvarEntrances.DataType = DbType.Int32;
				colvarEntrances.MaxLength = 0;
				colvarEntrances.AutoIncrement = false;
				colvarEntrances.IsNullable = false;
				colvarEntrances.IsPrimaryKey = false;
				colvarEntrances.IsForeignKey = false;
				colvarEntrances.IsReadOnly = false;
				colvarEntrances.DefaultSetting = @"";
				colvarEntrances.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEntrances);
				
				TableSchema.TableColumn colvarEntranceRate = new TableSchema.TableColumn(schema);
				colvarEntranceRate.ColumnName = "entrance_rate";
				colvarEntranceRate.DataType = DbType.Decimal;
				colvarEntranceRate.MaxLength = 0;
				colvarEntranceRate.AutoIncrement = false;
				colvarEntranceRate.IsNullable = false;
				colvarEntranceRate.IsPrimaryKey = false;
				colvarEntranceRate.IsForeignKey = false;
				colvarEntranceRate.IsReadOnly = false;
				colvarEntranceRate.DefaultSetting = @"";
				colvarEntranceRate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEntranceRate);
				
				TableSchema.TableColumn colvarPageViews = new TableSchema.TableColumn(schema);
				colvarPageViews.ColumnName = "page_views";
				colvarPageViews.DataType = DbType.Int32;
				colvarPageViews.MaxLength = 0;
				colvarPageViews.AutoIncrement = false;
				colvarPageViews.IsNullable = false;
				colvarPageViews.IsPrimaryKey = false;
				colvarPageViews.IsForeignKey = false;
				colvarPageViews.IsReadOnly = false;
				colvarPageViews.DefaultSetting = @"";
				colvarPageViews.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPageViews);
				
				TableSchema.TableColumn colvarUniquePageViews = new TableSchema.TableColumn(schema);
				colvarUniquePageViews.ColumnName = "unique_page_views";
				colvarUniquePageViews.DataType = DbType.Int32;
				colvarUniquePageViews.MaxLength = 0;
				colvarUniquePageViews.AutoIncrement = false;
				colvarUniquePageViews.IsNullable = false;
				colvarUniquePageViews.IsPrimaryKey = false;
				colvarUniquePageViews.IsForeignKey = false;
				colvarUniquePageViews.IsReadOnly = false;
				colvarUniquePageViews.DefaultSetting = @"";
				colvarUniquePageViews.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniquePageViews);
				
				TableSchema.TableColumn colvarAvgTimeOnPage = new TableSchema.TableColumn(schema);
				colvarAvgTimeOnPage.ColumnName = "avg_time_on_page";
				colvarAvgTimeOnPage.DataType = DbType.Decimal;
				colvarAvgTimeOnPage.MaxLength = 0;
				colvarAvgTimeOnPage.AutoIncrement = false;
				colvarAvgTimeOnPage.IsNullable = false;
				colvarAvgTimeOnPage.IsPrimaryKey = false;
				colvarAvgTimeOnPage.IsForeignKey = false;
				colvarAvgTimeOnPage.IsReadOnly = false;
				colvarAvgTimeOnPage.DefaultSetting = @"";
				colvarAvgTimeOnPage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAvgTimeOnPage);
				
				TableSchema.TableColumn colvarExits = new TableSchema.TableColumn(schema);
				colvarExits.ColumnName = "exits";
				colvarExits.DataType = DbType.Int32;
				colvarExits.MaxLength = 0;
				colvarExits.AutoIncrement = false;
				colvarExits.IsNullable = false;
				colvarExits.IsPrimaryKey = false;
				colvarExits.IsForeignKey = false;
				colvarExits.IsReadOnly = false;
				colvarExits.DefaultSetting = @"";
				colvarExits.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExits);
				
				TableSchema.TableColumn colvarExitRate = new TableSchema.TableColumn(schema);
				colvarExitRate.ColumnName = "exit_rate";
				colvarExitRate.DataType = DbType.Decimal;
				colvarExitRate.MaxLength = 0;
				colvarExitRate.AutoIncrement = false;
				colvarExitRate.IsNullable = false;
				colvarExitRate.IsPrimaryKey = false;
				colvarExitRate.IsForeignKey = false;
				colvarExitRate.IsReadOnly = false;
				colvarExitRate.DefaultSetting = @"";
				colvarExitRate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExitRate);
				
				TableSchema.TableColumn colvarPagePath = new TableSchema.TableColumn(schema);
				colvarPagePath.ColumnName = "page_path";
				colvarPagePath.DataType = DbType.String;
				colvarPagePath.MaxLength = 500;
				colvarPagePath.AutoIncrement = false;
				colvarPagePath.IsNullable = false;
				colvarPagePath.IsPrimaryKey = false;
				colvarPagePath.IsForeignKey = false;
				colvarPagePath.IsReadOnly = false;
				colvarPagePath.DefaultSetting = @"";
				colvarPagePath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPagePath);
				
				TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
				colvarStartDate.ColumnName = "start_date";
				colvarStartDate.DataType = DbType.DateTime;
				colvarStartDate.MaxLength = 0;
				colvarStartDate.AutoIncrement = false;
				colvarStartDate.IsNullable = false;
				colvarStartDate.IsPrimaryKey = false;
				colvarStartDate.IsForeignKey = false;
				colvarStartDate.IsReadOnly = false;
				colvarStartDate.DefaultSetting = @"";
				colvarStartDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartDate);
				
				TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
				colvarEndDate.ColumnName = "end_date";
				colvarEndDate.DataType = DbType.DateTime;
				colvarEndDate.MaxLength = 0;
				colvarEndDate.AutoIncrement = false;
				colvarEndDate.IsNullable = false;
				colvarEndDate.IsPrimaryKey = false;
				colvarEndDate.IsForeignKey = false;
				colvarEndDate.IsReadOnly = false;
				colvarEndDate.DefaultSetting = @"";
				colvarEndDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndDate);
				
				TableSchema.TableColumn colvarSessions = new TableSchema.TableColumn(schema);
				colvarSessions.ColumnName = "sessions";
				colvarSessions.DataType = DbType.Int32;
				colvarSessions.MaxLength = 0;
				colvarSessions.AutoIncrement = false;
				colvarSessions.IsNullable = false;
				colvarSessions.IsPrimaryKey = false;
				colvarSessions.IsForeignKey = false;
				colvarSessions.IsReadOnly = false;
				colvarSessions.DefaultSetting = @"";
				colvarSessions.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSessions);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarGid = new TableSchema.TableColumn(schema);
				colvarGid.ColumnName = "gid";
				colvarGid.DataType = DbType.Int32;
				colvarGid.MaxLength = 0;
				colvarGid.AutoIncrement = false;
				colvarGid.IsNullable = false;
				colvarGid.IsPrimaryKey = false;
				colvarGid.IsForeignKey = false;
				colvarGid.IsReadOnly = false;
				colvarGid.DefaultSetting = @"";
				colvarGid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGid);
				
				TableSchema.TableColumn colvarSort = new TableSchema.TableColumn(schema);
				colvarSort.ColumnName = "sort";
				colvarSort.DataType = DbType.Int32;
				colvarSort.MaxLength = 0;
				colvarSort.AutoIncrement = false;
				colvarSort.IsNullable = false;
				colvarSort.IsPrimaryKey = false;
				colvarSort.IsForeignKey = false;
				colvarSort.IsReadOnly = false;
				
						colvarSort.DefaultSetting = @"((0))";
				colvarSort.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSort);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("ga_deal",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("Users")]
		[Bindable(true)]
		public int Users 
		{
			get { return GetColumnValue<int>(Columns.Users); }
			set { SetColumnValue(Columns.Users, value); }
		}
		  
		[XmlAttribute("Entrances")]
		[Bindable(true)]
		public int Entrances 
		{
			get { return GetColumnValue<int>(Columns.Entrances); }
			set { SetColumnValue(Columns.Entrances, value); }
		}
		  
		[XmlAttribute("EntranceRate")]
		[Bindable(true)]
		public decimal EntranceRate 
		{
			get { return GetColumnValue<decimal>(Columns.EntranceRate); }
			set { SetColumnValue(Columns.EntranceRate, value); }
		}
		  
		[XmlAttribute("PageViews")]
		[Bindable(true)]
		public int PageViews 
		{
			get { return GetColumnValue<int>(Columns.PageViews); }
			set { SetColumnValue(Columns.PageViews, value); }
		}
		  
		[XmlAttribute("UniquePageViews")]
		[Bindable(true)]
		public int UniquePageViews 
		{
			get { return GetColumnValue<int>(Columns.UniquePageViews); }
			set { SetColumnValue(Columns.UniquePageViews, value); }
		}
		  
		[XmlAttribute("AvgTimeOnPage")]
		[Bindable(true)]
		public decimal AvgTimeOnPage 
		{
			get { return GetColumnValue<decimal>(Columns.AvgTimeOnPage); }
			set { SetColumnValue(Columns.AvgTimeOnPage, value); }
		}
		  
		[XmlAttribute("Exits")]
		[Bindable(true)]
		public int Exits 
		{
			get { return GetColumnValue<int>(Columns.Exits); }
			set { SetColumnValue(Columns.Exits, value); }
		}
		  
		[XmlAttribute("ExitRate")]
		[Bindable(true)]
		public decimal ExitRate 
		{
			get { return GetColumnValue<decimal>(Columns.ExitRate); }
			set { SetColumnValue(Columns.ExitRate, value); }
		}
		  
		[XmlAttribute("PagePath")]
		[Bindable(true)]
		public string PagePath 
		{
			get { return GetColumnValue<string>(Columns.PagePath); }
			set { SetColumnValue(Columns.PagePath, value); }
		}
		  
		[XmlAttribute("StartDate")]
		[Bindable(true)]
		public DateTime StartDate 
		{
			get { return GetColumnValue<DateTime>(Columns.StartDate); }
			set { SetColumnValue(Columns.StartDate, value); }
		}
		  
		[XmlAttribute("EndDate")]
		[Bindable(true)]
		public DateTime EndDate 
		{
			get { return GetColumnValue<DateTime>(Columns.EndDate); }
			set { SetColumnValue(Columns.EndDate, value); }
		}
		  
		[XmlAttribute("Sessions")]
		[Bindable(true)]
		public int Sessions 
		{
			get { return GetColumnValue<int>(Columns.Sessions); }
			set { SetColumnValue(Columns.Sessions, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("Gid")]
		[Bindable(true)]
		public int Gid 
		{
			get { return GetColumnValue<int>(Columns.Gid); }
			set { SetColumnValue(Columns.Gid, value); }
		}
		  
		[XmlAttribute("Sort")]
		[Bindable(true)]
		public int Sort 
		{
			get { return GetColumnValue<int>(Columns.Sort); }
			set { SetColumnValue(Columns.Sort, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UsersColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn EntrancesColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EntranceRateColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PageViewsColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn UniquePageViewsColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn AvgTimeOnPageColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ExitsColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ExitRateColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn PagePathColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn StartDateColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn EndDateColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn SessionsColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn GidColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn SortColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Bid = @"bid";
			 public static string Users = @"users";
			 public static string Entrances = @"entrances";
			 public static string EntranceRate = @"entrance_rate";
			 public static string PageViews = @"page_views";
			 public static string UniquePageViews = @"unique_page_views";
			 public static string AvgTimeOnPage = @"avg_time_on_page";
			 public static string Exits = @"exits";
			 public static string ExitRate = @"exit_rate";
			 public static string PagePath = @"page_path";
			 public static string StartDate = @"start_date";
			 public static string EndDate = @"end_date";
			 public static string Sessions = @"sessions";
			 public static string Type = @"type";
			 public static string Gid = @"gid";
			 public static string Sort = @"sort";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

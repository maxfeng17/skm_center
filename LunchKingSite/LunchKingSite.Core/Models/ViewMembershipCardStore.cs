using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMembershipCardStore class.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardStoreCollection : ReadOnlyList<ViewMembershipCardStore, ViewMembershipCardStoreCollection>
    {        
        public ViewMembershipCardStoreCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_membership_card_store view.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardStore : ReadOnlyRecord<ViewMembershipCardStore>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_membership_card_store", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
                colvarCardGroupId.ColumnName = "card_group_id";
                colvarCardGroupId.DataType = DbType.Int32;
                colvarCardGroupId.MaxLength = 0;
                colvarCardGroupId.AutoIncrement = false;
                colvarCardGroupId.IsNullable = false;
                colvarCardGroupId.IsPrimaryKey = false;
                colvarCardGroupId.IsForeignKey = false;
                colvarCardGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardGroupId);
                
                TableSchema.TableColumn colvarVersionId = new TableSchema.TableColumn(schema);
                colvarVersionId.ColumnName = "version_id";
                colvarVersionId.DataType = DbType.Int32;
                colvarVersionId.MaxLength = 0;
                colvarVersionId.AutoIncrement = false;
                colvarVersionId.IsNullable = false;
                colvarVersionId.IsPrimaryKey = false;
                colvarVersionId.IsForeignKey = false;
                colvarVersionId.IsReadOnly = false;
                
                schema.Columns.Add(colvarVersionId);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = false;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarTownshipId = new TableSchema.TableColumn(schema);
                colvarTownshipId.ColumnName = "township_id";
                colvarTownshipId.DataType = DbType.Int32;
                colvarTownshipId.MaxLength = 0;
                colvarTownshipId.AutoIncrement = false;
                colvarTownshipId.IsNullable = true;
                colvarTownshipId.IsPrimaryKey = false;
                colvarTownshipId.IsForeignKey = false;
                colvarTownshipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTownshipId);
                
                TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
                colvarCoordinate.ColumnName = "coordinate";
                colvarCoordinate.DataType = DbType.String;
                colvarCoordinate.MaxLength = -1;
                colvarCoordinate.AutoIncrement = false;
                colvarCoordinate.IsNullable = true;
                colvarCoordinate.IsPrimaryKey = false;
                colvarCoordinate.IsForeignKey = false;
                colvarCoordinate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCoordinate);
                
                TableSchema.TableColumn colvarGeographic = new TableSchema.TableColumn(schema);
                colvarGeographic.ColumnName = "geographic";
                colvarGeographic.DataType = DbType.AnsiString;
                colvarGeographic.MaxLength = -1;
                colvarGeographic.AutoIncrement = false;
                colvarGeographic.IsNullable = true;
                colvarGeographic.IsPrimaryKey = false;
                colvarGeographic.IsForeignKey = false;
                colvarGeographic.IsReadOnly = false;
                
                schema.Columns.Add(colvarGeographic);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 100;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = false;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarCardLastModifyTime = new TableSchema.TableColumn(schema);
                colvarCardLastModifyTime.ColumnName = "card_last_modify_time";
                colvarCardLastModifyTime.DataType = DbType.DateTime;
                colvarCardLastModifyTime.MaxLength = 0;
                colvarCardLastModifyTime.AutoIncrement = false;
                colvarCardLastModifyTime.IsNullable = false;
                colvarCardLastModifyTime.IsPrimaryKey = false;
                colvarCardLastModifyTime.IsForeignKey = false;
                colvarCardLastModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardLastModifyTime);
                
                TableSchema.TableColumn colvarCategoryList = new TableSchema.TableColumn(schema);
                colvarCategoryList.ColumnName = "category_list";
                colvarCategoryList.DataType = DbType.AnsiString;
                colvarCategoryList.MaxLength = -1;
                colvarCategoryList.AutoIncrement = false;
                colvarCategoryList.IsNullable = false;
                colvarCategoryList.IsPrimaryKey = false;
                colvarCategoryList.IsForeignKey = false;
                colvarCategoryList.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryList);
                
                TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
                colvarOpenTime.ColumnName = "open_time";
                colvarOpenTime.DataType = DbType.DateTime;
                colvarOpenTime.MaxLength = 0;
                colvarOpenTime.AutoIncrement = false;
                colvarOpenTime.IsNullable = false;
                colvarOpenTime.IsPrimaryKey = false;
                colvarOpenTime.IsForeignKey = false;
                colvarOpenTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpenTime);
                
                TableSchema.TableColumn colvarCloseTime = new TableSchema.TableColumn(schema);
                colvarCloseTime.ColumnName = "close_time";
                colvarCloseTime.DataType = DbType.DateTime;
                colvarCloseTime.MaxLength = 0;
                colvarCloseTime.AutoIncrement = false;
                colvarCloseTime.IsNullable = false;
                colvarCloseTime.IsPrimaryKey = false;
                colvarCloseTime.IsForeignKey = false;
                colvarCloseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCloseTime);
                
                TableSchema.TableColumn colvarReleaseTime = new TableSchema.TableColumn(schema);
                colvarReleaseTime.ColumnName = "release_time";
                colvarReleaseTime.DataType = DbType.DateTime;
                colvarReleaseTime.MaxLength = 0;
                colvarReleaseTime.AutoIncrement = false;
                colvarReleaseTime.IsNullable = true;
                colvarReleaseTime.IsPrimaryKey = false;
                colvarReleaseTime.IsForeignKey = false;
                colvarReleaseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReleaseTime);
                
                TableSchema.TableColumn colvarCardLevel = new TableSchema.TableColumn(schema);
                colvarCardLevel.ColumnName = "card_level";
                colvarCardLevel.DataType = DbType.Int32;
                colvarCardLevel.MaxLength = 0;
                colvarCardLevel.AutoIncrement = false;
                colvarCardLevel.IsNullable = false;
                colvarCardLevel.IsPrimaryKey = false;
                colvarCardLevel.IsForeignKey = false;
                colvarCardLevel.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardLevel);
                
                TableSchema.TableColumn colvarCardId = new TableSchema.TableColumn(schema);
                colvarCardId.ColumnName = "card_id";
                colvarCardId.DataType = DbType.Int32;
                colvarCardId.MaxLength = 0;
                colvarCardId.AutoIncrement = false;
                colvarCardId.IsNullable = false;
                colvarCardId.IsPrimaryKey = false;
                colvarCardId.IsForeignKey = false;
                colvarCardId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardId);
                
                TableSchema.TableColumn colvarHotPoint = new TableSchema.TableColumn(schema);
                colvarHotPoint.ColumnName = "hot_point";
                colvarHotPoint.DataType = DbType.Int32;
                colvarHotPoint.MaxLength = 0;
                colvarHotPoint.AutoIncrement = false;
                colvarHotPoint.IsNullable = false;
                colvarHotPoint.IsPrimaryKey = false;
                colvarHotPoint.IsForeignKey = false;
                colvarHotPoint.IsReadOnly = false;
                
                schema.Columns.Add(colvarHotPoint);
                
                TableSchema.TableColumn colvarInContractWith = new TableSchema.TableColumn(schema);
                colvarInContractWith.ColumnName = "in_contract_with";
                colvarInContractWith.DataType = DbType.String;
                colvarInContractWith.MaxLength = 80;
                colvarInContractWith.AutoIncrement = false;
                colvarInContractWith.IsNullable = true;
                colvarInContractWith.IsPrimaryKey = false;
                colvarInContractWith.IsForeignKey = false;
                colvarInContractWith.IsReadOnly = false;
                
                schema.Columns.Add(colvarInContractWith);
                
                TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
                colvarSellerEmail.ColumnName = "seller_email";
                colvarSellerEmail.DataType = DbType.String;
                colvarSellerEmail.MaxLength = 256;
                colvarSellerEmail.AutoIncrement = false;
                colvarSellerEmail.IsNullable = true;
                colvarSellerEmail.IsPrimaryKey = false;
                colvarSellerEmail.IsForeignKey = false;
                colvarSellerEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerEmail);
                
                TableSchema.TableColumn colvarIsPromo = new TableSchema.TableColumn(schema);
                colvarIsPromo.ColumnName = "is_promo";
                colvarIsPromo.DataType = DbType.Boolean;
                colvarIsPromo.MaxLength = 0;
                colvarIsPromo.AutoIncrement = false;
                colvarIsPromo.IsNullable = false;
                colvarIsPromo.IsPrimaryKey = false;
                colvarIsPromo.IsForeignKey = false;
                colvarIsPromo.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsPromo);
                
                TableSchema.TableColumn colvarSellerMobile = new TableSchema.TableColumn(schema);
                colvarSellerMobile.ColumnName = "seller_mobile";
                colvarSellerMobile.DataType = DbType.AnsiString;
                colvarSellerMobile.MaxLength = 20;
                colvarSellerMobile.AutoIncrement = false;
                colvarSellerMobile.IsNullable = true;
                colvarSellerMobile.IsPrimaryKey = false;
                colvarSellerMobile.IsForeignKey = false;
                colvarSellerMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMobile);
                
                TableSchema.TableColumn colvarContractValidDate = new TableSchema.TableColumn(schema);
                colvarContractValidDate.ColumnName = "contract_valid_date";
                colvarContractValidDate.DataType = DbType.DateTime;
                colvarContractValidDate.MaxLength = 0;
                colvarContractValidDate.AutoIncrement = false;
                colvarContractValidDate.IsNullable = true;
                colvarContractValidDate.IsPrimaryKey = false;
                colvarContractValidDate.IsForeignKey = false;
                colvarContractValidDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarContractValidDate);
                
                TableSchema.TableColumn colvarPublishTime = new TableSchema.TableColumn(schema);
                colvarPublishTime.ColumnName = "publish_time";
                colvarPublishTime.DataType = DbType.DateTime;
                colvarPublishTime.MaxLength = 0;
                colvarPublishTime.AutoIncrement = false;
                colvarPublishTime.IsNullable = true;
                colvarPublishTime.IsPrimaryKey = false;
                colvarPublishTime.IsForeignKey = false;
                colvarPublishTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarPublishTime);
                
                TableSchema.TableColumn colvarBackgroundColorValue = new TableSchema.TableColumn(schema);
                colvarBackgroundColorValue.ColumnName = "background_color_value";
                colvarBackgroundColorValue.DataType = DbType.String;
                colvarBackgroundColorValue.MaxLength = -1;
                colvarBackgroundColorValue.AutoIncrement = false;
                colvarBackgroundColorValue.IsNullable = false;
                colvarBackgroundColorValue.IsPrimaryKey = false;
                colvarBackgroundColorValue.IsForeignKey = false;
                colvarBackgroundColorValue.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundColorValue);
                
                TableSchema.TableColumn colvarBackgroundImageValue = new TableSchema.TableColumn(schema);
                colvarBackgroundImageValue.ColumnName = "background_image_value";
                colvarBackgroundImageValue.DataType = DbType.String;
                colvarBackgroundImageValue.MaxLength = -1;
                colvarBackgroundImageValue.AutoIncrement = false;
                colvarBackgroundImageValue.IsNullable = false;
                colvarBackgroundImageValue.IsPrimaryKey = false;
                colvarBackgroundImageValue.IsForeignKey = false;
                colvarBackgroundImageValue.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundImageValue);
                
                TableSchema.TableColumn colvarIconImageValue = new TableSchema.TableColumn(schema);
                colvarIconImageValue.ColumnName = "icon_image_value";
                colvarIconImageValue.DataType = DbType.String;
                colvarIconImageValue.MaxLength = -1;
                colvarIconImageValue.AutoIncrement = false;
                colvarIconImageValue.IsNullable = false;
                colvarIconImageValue.IsPrimaryKey = false;
                colvarIconImageValue.IsForeignKey = false;
                colvarIconImageValue.IsReadOnly = false;
                
                schema.Columns.Add(colvarIconImageValue);
                
                TableSchema.TableColumn colvarStoreCount = new TableSchema.TableColumn(schema);
                colvarStoreCount.ColumnName = "store_count";
                colvarStoreCount.DataType = DbType.Int32;
                colvarStoreCount.MaxLength = 0;
                colvarStoreCount.AutoIncrement = false;
                colvarStoreCount.IsNullable = true;
                colvarStoreCount.IsPrimaryKey = false;
                colvarStoreCount.IsForeignKey = false;
                colvarStoreCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreCount);
                
                TableSchema.TableColumn colvarPaymentPercent = new TableSchema.TableColumn(schema);
                colvarPaymentPercent.ColumnName = "payment_percent";
                colvarPaymentPercent.DataType = DbType.Double;
                colvarPaymentPercent.MaxLength = 0;
                colvarPaymentPercent.AutoIncrement = false;
                colvarPaymentPercent.IsNullable = false;
                colvarPaymentPercent.IsPrimaryKey = false;
                colvarPaymentPercent.IsForeignKey = false;
                colvarPaymentPercent.IsReadOnly = false;
                
                schema.Columns.Add(colvarPaymentPercent);
                
                TableSchema.TableColumn colvarCardModifyTime = new TableSchema.TableColumn(schema);
                colvarCardModifyTime.ColumnName = "card_modify_time";
                colvarCardModifyTime.DataType = DbType.DateTime;
                colvarCardModifyTime.MaxLength = 0;
                colvarCardModifyTime.AutoIncrement = false;
                colvarCardModifyTime.IsNullable = true;
                colvarCardModifyTime.IsPrimaryKey = false;
                colvarCardModifyTime.IsForeignKey = false;
                colvarCardModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardModifyTime);
                
                TableSchema.TableColumn colvarCardCreateTime = new TableSchema.TableColumn(schema);
                colvarCardCreateTime.ColumnName = "card_create_time";
                colvarCardCreateTime.DataType = DbType.DateTime;
                colvarCardCreateTime.MaxLength = 0;
                colvarCardCreateTime.AutoIncrement = false;
                colvarCardCreateTime.IsNullable = false;
                colvarCardCreateTime.IsPrimaryKey = false;
                colvarCardCreateTime.IsForeignKey = false;
                colvarCardCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardCreateTime);
                
                TableSchema.TableColumn colvarOthers = new TableSchema.TableColumn(schema);
                colvarOthers.ColumnName = "others";
                colvarOthers.DataType = DbType.String;
                colvarOthers.MaxLength = -1;
                colvarOthers.AutoIncrement = false;
                colvarOthers.IsNullable = true;
                colvarOthers.IsPrimaryKey = false;
                colvarOthers.IsForeignKey = false;
                colvarOthers.IsReadOnly = false;
                
                schema.Columns.Add(colvarOthers);
                
                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = false;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerUserId);
                
                TableSchema.TableColumn colvarCardType = new TableSchema.TableColumn(schema);
                colvarCardType.ColumnName = "card_type";
                colvarCardType.DataType = DbType.Int32;
                colvarCardType.MaxLength = 0;
                colvarCardType.AutoIncrement = false;
                colvarCardType.IsNullable = false;
                colvarCardType.IsPrimaryKey = false;
                colvarCardType.IsForeignKey = false;
                colvarCardType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardType);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Byte;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_membership_card_store",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMembershipCardStore()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMembershipCardStore(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMembershipCardStore(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMembershipCardStore(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("CardGroupId")]
        [Bindable(true)]
        public int CardGroupId 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_group_id");
		    }
            set 
		    {
			    SetColumnValue("card_group_id", value);
            }
        }
	      
        [XmlAttribute("VersionId")]
        [Bindable(true)]
        public int VersionId 
	    {
		    get
		    {
			    return GetColumnValue<int>("version_id");
		    }
            set 
		    {
			    SetColumnValue("version_id", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("TownshipId")]
        [Bindable(true)]
        public int? TownshipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("township_id");
		    }
            set 
		    {
			    SetColumnValue("township_id", value);
            }
        }
	      
        [XmlAttribute("Coordinate")]
        [Bindable(true)]
        public string Coordinate 
	    {
		    get
		    {
			    return GetColumnValue<string>("coordinate");
		    }
            set 
		    {
			    SetColumnValue("coordinate", value);
            }
        }
	      
        [XmlAttribute("Geographic")]
        [Bindable(true)]
        public string Geographic 
	    {
		    get
		    {
			    return GetColumnValue<string>("geographic");
		    }
            set 
		    {
			    SetColumnValue("geographic", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("CardLastModifyTime")]
        [Bindable(true)]
        public DateTime CardLastModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("card_last_modify_time");
		    }
            set 
		    {
			    SetColumnValue("card_last_modify_time", value);
            }
        }
	      
        [XmlAttribute("CategoryList")]
        [Bindable(true)]
        public string CategoryList 
	    {
		    get
		    {
			    return GetColumnValue<string>("category_list");
		    }
            set 
		    {
			    SetColumnValue("category_list", value);
            }
        }
	      
        [XmlAttribute("OpenTime")]
        [Bindable(true)]
        public DateTime OpenTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("open_time");
		    }
            set 
		    {
			    SetColumnValue("open_time", value);
            }
        }
	      
        [XmlAttribute("CloseTime")]
        [Bindable(true)]
        public DateTime CloseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("close_time");
		    }
            set 
		    {
			    SetColumnValue("close_time", value);
            }
        }
	      
        [XmlAttribute("ReleaseTime")]
        [Bindable(true)]
        public DateTime? ReleaseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("release_time");
		    }
            set 
		    {
			    SetColumnValue("release_time", value);
            }
        }
	      
        [XmlAttribute("CardLevel")]
        [Bindable(true)]
        public int CardLevel 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_level");
		    }
            set 
		    {
			    SetColumnValue("card_level", value);
            }
        }
	      
        [XmlAttribute("CardId")]
        [Bindable(true)]
        public int CardId 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_id");
		    }
            set 
		    {
			    SetColumnValue("card_id", value);
            }
        }
	      
        [XmlAttribute("HotPoint")]
        [Bindable(true)]
        public int HotPoint 
	    {
		    get
		    {
			    return GetColumnValue<int>("hot_point");
		    }
            set 
		    {
			    SetColumnValue("hot_point", value);
            }
        }
	      
        [XmlAttribute("InContractWith")]
        [Bindable(true)]
        public string InContractWith 
	    {
		    get
		    {
			    return GetColumnValue<string>("in_contract_with");
		    }
            set 
		    {
			    SetColumnValue("in_contract_with", value);
            }
        }
	      
        [XmlAttribute("SellerEmail")]
        [Bindable(true)]
        public string SellerEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_email");
		    }
            set 
		    {
			    SetColumnValue("seller_email", value);
            }
        }
	      
        [XmlAttribute("IsPromo")]
        [Bindable(true)]
        public bool IsPromo 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_promo");
		    }
            set 
		    {
			    SetColumnValue("is_promo", value);
            }
        }
	      
        [XmlAttribute("SellerMobile")]
        [Bindable(true)]
        public string SellerMobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_mobile");
		    }
            set 
		    {
			    SetColumnValue("seller_mobile", value);
            }
        }
	      
        [XmlAttribute("ContractValidDate")]
        [Bindable(true)]
        public DateTime? ContractValidDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("contract_valid_date");
		    }
            set 
		    {
			    SetColumnValue("contract_valid_date", value);
            }
        }
	      
        [XmlAttribute("PublishTime")]
        [Bindable(true)]
        public DateTime? PublishTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("publish_time");
		    }
            set 
		    {
			    SetColumnValue("publish_time", value);
            }
        }
	      
        [XmlAttribute("BackgroundColorValue")]
        [Bindable(true)]
        public string BackgroundColorValue 
	    {
		    get
		    {
			    return GetColumnValue<string>("background_color_value");
		    }
            set 
		    {
			    SetColumnValue("background_color_value", value);
            }
        }
	      
        [XmlAttribute("BackgroundImageValue")]
        [Bindable(true)]
        public string BackgroundImageValue 
	    {
		    get
		    {
			    return GetColumnValue<string>("background_image_value");
		    }
            set 
		    {
			    SetColumnValue("background_image_value", value);
            }
        }
	      
        [XmlAttribute("IconImageValue")]
        [Bindable(true)]
        public string IconImageValue 
	    {
		    get
		    {
			    return GetColumnValue<string>("icon_image_value");
		    }
            set 
		    {
			    SetColumnValue("icon_image_value", value);
            }
        }
	      
        [XmlAttribute("StoreCount")]
        [Bindable(true)]
        public int? StoreCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("store_count");
		    }
            set 
		    {
			    SetColumnValue("store_count", value);
            }
        }
	      
        [XmlAttribute("PaymentPercent")]
        [Bindable(true)]
        public double PaymentPercent 
	    {
		    get
		    {
			    return GetColumnValue<double>("payment_percent");
		    }
            set 
		    {
			    SetColumnValue("payment_percent", value);
            }
        }
	      
        [XmlAttribute("CardModifyTime")]
        [Bindable(true)]
        public DateTime? CardModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("card_modify_time");
		    }
            set 
		    {
			    SetColumnValue("card_modify_time", value);
            }
        }
	      
        [XmlAttribute("CardCreateTime")]
        [Bindable(true)]
        public DateTime CardCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("card_create_time");
		    }
            set 
		    {
			    SetColumnValue("card_create_time", value);
            }
        }
	      
        [XmlAttribute("Others")]
        [Bindable(true)]
        public string Others 
	    {
		    get
		    {
			    return GetColumnValue<string>("others");
		    }
            set 
		    {
			    SetColumnValue("others", value);
            }
        }
	      
        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int SellerUserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_user_id");
		    }
            set 
		    {
			    SetColumnValue("seller_user_id", value);
            }
        }
	      
        [XmlAttribute("CardType")]
        [Bindable(true)]
        public int CardType 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_type");
		    }
            set 
		    {
			    SetColumnValue("card_type", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public byte Status 
	    {
		    get
		    {
			    return GetColumnValue<byte>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string SellerName = @"seller_name";
            
            public static string CardGroupId = @"card_group_id";
            
            public static string VersionId = @"version_id";
            
            public static string StoreGuid = @"store_guid";
            
            public static string TownshipId = @"township_id";
            
            public static string Coordinate = @"coordinate";
            
            public static string Geographic = @"geographic";
            
            public static string StoreName = @"store_name";
            
            public static string CardLastModifyTime = @"card_last_modify_time";
            
            public static string CategoryList = @"category_list";
            
            public static string OpenTime = @"open_time";
            
            public static string CloseTime = @"close_time";
            
            public static string ReleaseTime = @"release_time";
            
            public static string CardLevel = @"card_level";
            
            public static string CardId = @"card_id";
            
            public static string HotPoint = @"hot_point";
            
            public static string InContractWith = @"in_contract_with";
            
            public static string SellerEmail = @"seller_email";
            
            public static string IsPromo = @"is_promo";
            
            public static string SellerMobile = @"seller_mobile";
            
            public static string ContractValidDate = @"contract_valid_date";
            
            public static string PublishTime = @"publish_time";
            
            public static string BackgroundColorValue = @"background_color_value";
            
            public static string BackgroundImageValue = @"background_image_value";
            
            public static string IconImageValue = @"icon_image_value";
            
            public static string StoreCount = @"store_count";
            
            public static string PaymentPercent = @"payment_percent";
            
            public static string CardModifyTime = @"card_modify_time";
            
            public static string CardCreateTime = @"card_create_time";
            
            public static string Others = @"others";
            
            public static string SellerUserId = @"seller_user_id";
            
            public static string CardType = @"card_type";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string Status = @"status";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

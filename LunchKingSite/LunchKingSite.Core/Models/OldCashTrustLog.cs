using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OldCashTrustLog class.
	/// </summary>
    [Serializable]
	public partial class OldCashTrustLogCollection : RepositoryList<OldCashTrustLog, OldCashTrustLogCollection>
	{	   
		public OldCashTrustLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OldCashTrustLogCollection</returns>
		public OldCashTrustLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OldCashTrustLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the old_cash_trust_log table.
	/// </summary>
	[Serializable]
	public partial class OldCashTrustLog : RepositoryRecord<OldCashTrustLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OldCashTrustLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OldCashTrustLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("old_cash_trust_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "coupon_id";
				colvarCouponId.DataType = DbType.Int32;
				colvarCouponId.MaxLength = 0;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = false;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = false;
				colvarCouponId.IsReadOnly = false;
				colvarCouponId.DefaultSetting = @"";
				colvarCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponId);
				
				TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
				colvarSequenceNumber.ColumnName = "sequence_number";
				colvarSequenceNumber.DataType = DbType.AnsiString;
				colvarSequenceNumber.MaxLength = 50;
				colvarSequenceNumber.AutoIncrement = false;
				colvarSequenceNumber.IsNullable = false;
				colvarSequenceNumber.IsPrimaryKey = false;
				colvarSequenceNumber.IsForeignKey = false;
				colvarSequenceNumber.IsReadOnly = false;
				colvarSequenceNumber.DefaultSetting = @"";
				colvarSequenceNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSequenceNumber);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
				colvarOrderDetailGuid.ColumnName = "order_detail_guid";
				colvarOrderDetailGuid.DataType = DbType.Guid;
				colvarOrderDetailGuid.MaxLength = 0;
				colvarOrderDetailGuid.AutoIncrement = false;
				colvarOrderDetailGuid.IsNullable = false;
				colvarOrderDetailGuid.IsPrimaryKey = false;
				colvarOrderDetailGuid.IsForeignKey = false;
				colvarOrderDetailGuid.IsReadOnly = false;
				colvarOrderDetailGuid.DefaultSetting = @"";
				colvarOrderDetailGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderDetailGuid);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarOrderTime = new TableSchema.TableColumn(schema);
				colvarOrderTime.ColumnName = "order_time";
				colvarOrderTime.DataType = DbType.DateTime;
				colvarOrderTime.MaxLength = 0;
				colvarOrderTime.AutoIncrement = false;
				colvarOrderTime.IsNullable = false;
				colvarOrderTime.IsPrimaryKey = false;
				colvarOrderTime.IsForeignKey = false;
				colvarOrderTime.IsReadOnly = false;
				colvarOrderTime.DefaultSetting = @"";
				colvarOrderTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderTime);
				
				TableSchema.TableColumn colvarVerifyTime = new TableSchema.TableColumn(schema);
				colvarVerifyTime.ColumnName = "verify_time";
				colvarVerifyTime.DataType = DbType.DateTime;
				colvarVerifyTime.MaxLength = 0;
				colvarVerifyTime.AutoIncrement = false;
				colvarVerifyTime.IsNullable = true;
				colvarVerifyTime.IsPrimaryKey = false;
				colvarVerifyTime.IsForeignKey = false;
				colvarVerifyTime.IsReadOnly = false;
				colvarVerifyTime.DefaultSetting = @"";
				colvarVerifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifyTime);
				
				TableSchema.TableColumn colvarVerifyType = new TableSchema.TableColumn(schema);
				colvarVerifyType.ColumnName = "verify_type";
				colvarVerifyType.DataType = DbType.Int32;
				colvarVerifyType.MaxLength = 0;
				colvarVerifyType.AutoIncrement = false;
				colvarVerifyType.IsNullable = false;
				colvarVerifyType.IsPrimaryKey = false;
				colvarVerifyType.IsForeignKey = false;
				colvarVerifyType.IsReadOnly = false;
				
						colvarVerifyType.DefaultSetting = @"((0))";
				colvarVerifyType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifyType);
				
				TableSchema.TableColumn colvarVerifyBy = new TableSchema.TableColumn(schema);
				colvarVerifyBy.ColumnName = "verify_by";
				colvarVerifyBy.DataType = DbType.AnsiString;
				colvarVerifyBy.MaxLength = 100;
				colvarVerifyBy.AutoIncrement = false;
				colvarVerifyBy.IsNullable = true;
				colvarVerifyBy.IsPrimaryKey = false;
				colvarVerifyBy.IsForeignKey = false;
				colvarVerifyBy.IsReadOnly = false;
				colvarVerifyBy.DefaultSetting = @"";
				colvarVerifyBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifyBy);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("old_cash_trust_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public int CouponId 
		{
			get { return GetColumnValue<int>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}
		  
		[XmlAttribute("SequenceNumber")]
		[Bindable(true)]
		public string SequenceNumber 
		{
			get { return GetColumnValue<string>(Columns.SequenceNumber); }
			set { SetColumnValue(Columns.SequenceNumber, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("OrderDetailGuid")]
		[Bindable(true)]
		public Guid OrderDetailGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderDetailGuid); }
			set { SetColumnValue(Columns.OrderDetailGuid, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("OrderTime")]
		[Bindable(true)]
		public DateTime OrderTime 
		{
			get { return GetColumnValue<DateTime>(Columns.OrderTime); }
			set { SetColumnValue(Columns.OrderTime, value); }
		}
		
		
		//private DateTime? propVerifyTime;
		[XmlAttribute("VerifyTime")]
		[Bindable(true)]
		public DateTime VerifyTime 
		{
			get
			{
			DateTime? propVerifyTime = GetColumnValue<DateTime?>(Columns.VerifyTime);
				if (!propVerifyTime.HasValue)
					return new DateTime(1900, 01, 01);
				return propVerifyTime.Value;
			}
		   set { SetColumnValue(Columns.VerifyTime, value); }
		}
		
		[XmlIgnore]
		public bool VerifyTimeHasValue
		{
			get { return GetColumnValue<DateTime?>("verify_time") != null; }
			set
			{
				DateTime? propVerifyTime = GetColumnValue<DateTime?>(Columns.VerifyTime);
				if (!value)
					SetColumnValue(Columns.VerifyTime, null);
				else if (value && !propVerifyTime.HasValue)
					SetColumnValue(Columns.VerifyTime, new DateTime(1900, 01, 01));
		   }
		}
		
		  
		[XmlAttribute("VerifyType")]
		[Bindable(true)]
		public int VerifyType 
		{
			get { return GetColumnValue<int>(Columns.VerifyType); }
			set { SetColumnValue(Columns.VerifyType, value); }
		}
		  
		[XmlAttribute("VerifyBy")]
		[Bindable(true)]
		public string VerifyBy 
		{
			get { return GetColumnValue<string>(Columns.VerifyBy); }
			set { SetColumnValue(Columns.VerifyBy, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SequenceNumberColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderDetailGuidColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn VerifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn VerifyTypeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn VerifyByColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CouponId = @"coupon_id";
			 public static string SequenceNumber = @"sequence_number";
			 public static string Status = @"status";
			 public static string OrderDetailGuid = @"order_detail_guid";
			 public static string OrderGuid = @"order_guid";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string OrderTime = @"order_time";
			 public static string VerifyTime = @"verify_time";
			 public static string VerifyType = @"verify_type";
			 public static string VerifyBy = @"verify_by";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

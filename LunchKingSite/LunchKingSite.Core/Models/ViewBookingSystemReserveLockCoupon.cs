using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBookingSystemReserveLockCoupon class.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemReserveLockCouponCollection : ReadOnlyList<ViewBookingSystemReserveLockCoupon, ViewBookingSystemReserveLockCouponCollection>
    {        
        public ViewBookingSystemReserveLockCouponCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_booking_system_reserve_lock_coupon view.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemReserveLockCoupon : ReadOnlyRecord<ViewBookingSystemReserveLockCoupon>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_booking_system_reserve_lock_coupon", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = false;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponId);
                
                TableSchema.TableColumn colvarReserveLockStatus = new TableSchema.TableColumn(schema);
                colvarReserveLockStatus.ColumnName = "reserve_lock_status";
                colvarReserveLockStatus.DataType = DbType.Int32;
                colvarReserveLockStatus.MaxLength = 0;
                colvarReserveLockStatus.AutoIncrement = false;
                colvarReserveLockStatus.IsNullable = false;
                colvarReserveLockStatus.IsPrimaryKey = false;
                colvarReserveLockStatus.IsForeignKey = false;
                colvarReserveLockStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarReserveLockStatus);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarCouponReservationDate = new TableSchema.TableColumn(schema);
                colvarCouponReservationDate.ColumnName = "coupon_reservation_date";
                colvarCouponReservationDate.DataType = DbType.DateTime;
                colvarCouponReservationDate.MaxLength = 0;
                colvarCouponReservationDate.AutoIncrement = false;
                colvarCouponReservationDate.IsNullable = true;
                colvarCouponReservationDate.IsPrimaryKey = false;
                colvarCouponReservationDate.IsForeignKey = false;
                colvarCouponReservationDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponReservationDate);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_booking_system_reserve_lock_coupon",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBookingSystemReserveLockCoupon()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBookingSystemReserveLockCoupon(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBookingSystemReserveLockCoupon(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBookingSystemReserveLockCoupon(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int CouponId 
	    {
		    get
		    {
			    return GetColumnValue<int>("coupon_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_id", value);
            }
        }
	      
        [XmlAttribute("ReserveLockStatus")]
        [Bindable(true)]
        public int ReserveLockStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("reserve_lock_status");
		    }
            set 
		    {
			    SetColumnValue("reserve_lock_status", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("CouponReservationDate")]
        [Bindable(true)]
        public DateTime? CouponReservationDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("coupon_reservation_date");
		    }
            set 
		    {
			    SetColumnValue("coupon_reservation_date", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string CouponId = @"coupon_id";
            
            public static string ReserveLockStatus = @"reserve_lock_status";
            
            public static string ModifyTime = @"modify_time";
            
            public static string CouponReservationDate = @"coupon_reservation_date";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

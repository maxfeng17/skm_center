using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the DevicePushRecord class.
    /// </summary>
    [Serializable]
    public partial class DevicePushRecordCollection : RepositoryList<DevicePushRecord, DevicePushRecordCollection>
    {
        public DevicePushRecordCollection() {}

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DevicePushRecordCollection</returns>
        public DevicePushRecordCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DevicePushRecord o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the device_push_record table.
    /// </summary>
    [Serializable]
    public partial class DevicePushRecord : RepositoryRecord<DevicePushRecord>, IRecordBase
    {
        #region .ctors and Default Settings

        public DevicePushRecord()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public DevicePushRecord(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("device_push_record", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarDeviceId = new TableSchema.TableColumn(schema);
                colvarDeviceId.ColumnName = "device_id";
                colvarDeviceId.DataType = DbType.Int32;
                colvarDeviceId.MaxLength = 0;
                colvarDeviceId.AutoIncrement = false;
                colvarDeviceId.IsNullable = false;
                colvarDeviceId.IsPrimaryKey = false;
                colvarDeviceId.IsForeignKey = false;
                colvarDeviceId.IsReadOnly = false;
                colvarDeviceId.DefaultSetting = @"";
                colvarDeviceId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDeviceId);

                TableSchema.TableColumn colvarMemberId = new TableSchema.TableColumn(schema);
                colvarMemberId.ColumnName = "member_id";
                colvarMemberId.DataType = DbType.Int32;
                colvarMemberId.MaxLength = 0;
                colvarMemberId.AutoIncrement = false;
                colvarMemberId.IsNullable = true;
                colvarMemberId.IsPrimaryKey = false;
                colvarMemberId.IsForeignKey = false;
                colvarMemberId.IsReadOnly = false;
                colvarMemberId.DefaultSetting = @"";
                colvarMemberId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMemberId);

                TableSchema.TableColumn colvarIdentifierType = new TableSchema.TableColumn(schema);
                colvarIdentifierType.ColumnName = "identifier_type";
                colvarIdentifierType.DataType = DbType.Int32;
                colvarIdentifierType.MaxLength = 0;
                colvarIdentifierType.AutoIncrement = false;
                colvarIdentifierType.IsNullable = false;
                colvarIdentifierType.IsPrimaryKey = false;
                colvarIdentifierType.IsForeignKey = false;
                colvarIdentifierType.IsReadOnly = false;
                colvarIdentifierType.DefaultSetting = @"";
                colvarIdentifierType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIdentifierType);

                TableSchema.TableColumn colvarActionId = new TableSchema.TableColumn(schema);
                colvarActionId.ColumnName = "action_id";
                colvarActionId.DataType = DbType.Int32;
                colvarActionId.MaxLength = 0;
                colvarActionId.AutoIncrement = false;
                colvarActionId.IsNullable = false;
                colvarActionId.IsPrimaryKey = false;
                colvarActionId.IsForeignKey = false;
                colvarActionId.IsReadOnly = false;
                colvarActionId.DefaultSetting = @"";
                colvarActionId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarActionId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarIsRead = new TableSchema.TableColumn(schema);
                colvarIsRead.ColumnName = "is_read";
                colvarIsRead.DataType = DbType.Boolean;
                colvarIsRead.MaxLength = 0;
                colvarIsRead.AutoIncrement = false;
                colvarIsRead.IsNullable = false;
                colvarIsRead.IsPrimaryKey = false;
                colvarIsRead.IsForeignKey = false;
                colvarIsRead.IsReadOnly = false;

                colvarIsRead.DefaultSetting = @"((0))";
                colvarIsRead.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsRead);

                TableSchema.TableColumn colvarReadTime = new TableSchema.TableColumn(schema);
                colvarReadTime.ColumnName = "read_time";
                colvarReadTime.DataType = DbType.DateTime;
                colvarReadTime.MaxLength = 0;
                colvarReadTime.AutoIncrement = false;
                colvarReadTime.IsNullable = true;
                colvarReadTime.IsPrimaryKey = false;
                colvarReadTime.IsForeignKey = false;
                colvarReadTime.IsReadOnly = false;
                colvarReadTime.DefaultSetting = @"";
                colvarReadTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReadTime);

                TableSchema.TableColumn colvarIsRemove = new TableSchema.TableColumn(schema);
                colvarIsRemove.ColumnName = "is_remove";
                colvarIsRemove.DataType = DbType.Boolean;
                colvarIsRemove.MaxLength = 0;
                colvarIsRemove.AutoIncrement = false;
                colvarIsRemove.IsNullable = false;
                colvarIsRemove.IsPrimaryKey = false;
                colvarIsRemove.IsForeignKey = false;
                colvarIsRemove.IsReadOnly = false;

                colvarIsRemove.DefaultSetting = @"((0))";
                colvarIsRemove.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsRemove);

                TableSchema.TableColumn colvarRemoveTime = new TableSchema.TableColumn(schema);
                colvarRemoveTime.ColumnName = "remove_time";
                colvarRemoveTime.DataType = DbType.DateTime;
                colvarRemoveTime.MaxLength = 0;
                colvarRemoveTime.AutoIncrement = false;
                colvarRemoveTime.IsNullable = true;
                colvarRemoveTime.IsPrimaryKey = false;
                colvarRemoveTime.IsForeignKey = false;
                colvarRemoveTime.IsReadOnly = false;
                colvarRemoveTime.DefaultSetting = @"";
                colvarRemoveTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRemoveTime);

                TableSchema.TableColumn colvarTriggerType = new TableSchema.TableColumn(schema);
                colvarTriggerType.ColumnName = "trigger_type";
                colvarTriggerType.DataType = DbType.Int32;
                colvarTriggerType.MaxLength = 0;
                colvarTriggerType.AutoIncrement = false;
                colvarTriggerType.IsNullable = false;
                colvarTriggerType.IsPrimaryKey = false;
                colvarTriggerType.IsForeignKey = false;
                colvarTriggerType.IsReadOnly = false;

                colvarTriggerType.DefaultSetting = @"((0))";
                colvarTriggerType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTriggerType);

                TableSchema.TableColumn colvarIsDeal = new TableSchema.TableColumn(schema);
                colvarIsDeal.ColumnName = "is_deal";
                colvarIsDeal.DataType = DbType.Boolean;
                colvarIsDeal.MaxLength = 0;
                colvarIsDeal.AutoIncrement = false;
                colvarIsDeal.IsNullable = false;
                colvarIsDeal.IsPrimaryKey = false;
                colvarIsDeal.IsForeignKey = false;
                colvarIsDeal.IsReadOnly = false;

                colvarIsDeal.DefaultSetting = @"((0))";
                colvarIsDeal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsDeal);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("device_push_record",schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("DeviceId")]
        [Bindable(true)]
        public int DeviceId
        {
            get { return GetColumnValue<int>(Columns.DeviceId); }
            set { SetColumnValue(Columns.DeviceId, value); }
        }

        [XmlAttribute("MemberId")]
        [Bindable(true)]
        public int? MemberId
        {
            get { return GetColumnValue<int?>(Columns.MemberId); }
            set { SetColumnValue(Columns.MemberId, value); }
        }

        [XmlAttribute("IdentifierType")]
        [Bindable(true)]
        public int IdentifierType
        {
            get { return GetColumnValue<int>(Columns.IdentifierType); }
            set { SetColumnValue(Columns.IdentifierType, value); }
        }

        [XmlAttribute("ActionId")]
        [Bindable(true)]
        public int ActionId
        {
            get { return GetColumnValue<int>(Columns.ActionId); }
            set { SetColumnValue(Columns.ActionId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("IsRead")]
        [Bindable(true)]
        public bool IsRead
        {
            get { return GetColumnValue<bool>(Columns.IsRead); }
            set { SetColumnValue(Columns.IsRead, value); }
        }

        [XmlAttribute("ReadTime")]
        [Bindable(true)]
        public DateTime? ReadTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ReadTime); }
            set { SetColumnValue(Columns.ReadTime, value); }
        }

        [XmlAttribute("IsRemove")]
        [Bindable(true)]
        public bool IsRemove
        {
            get { return GetColumnValue<bool>(Columns.IsRemove); }
            set { SetColumnValue(Columns.IsRemove, value); }
        }

        [XmlAttribute("RemoveTime")]
        [Bindable(true)]
        public DateTime? RemoveTime
        {
            get { return GetColumnValue<DateTime?>(Columns.RemoveTime); }
            set { SetColumnValue(Columns.RemoveTime, value); }
        }

        [XmlAttribute("TriggerType")]
        [Bindable(true)]
        public int TriggerType
        {
            get { return GetColumnValue<int>(Columns.TriggerType); }
            set { SetColumnValue(Columns.TriggerType, value); }
        }

        [XmlAttribute("IsDeal")]
        [Bindable(true)]
        public bool IsDeal
        {
            get { return GetColumnValue<bool>(Columns.IsDeal); }
            set { SetColumnValue(Columns.IsDeal, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn DeviceIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn MemberIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn IdentifierTypeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ActionIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn IsReadColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ReadTimeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn IsRemoveColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn RemoveTimeColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn TriggerTypeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn IsDealColumn
        {
            get { return Schema.Columns[11]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string DeviceId = @"device_id";
            public static string MemberId = @"member_id";
            public static string IdentifierType = @"identifier_type";
            public static string ActionId = @"action_id";
            public static string CreateTime = @"create_time";
            public static string IsRead = @"is_read";
            public static string ReadTime = @"read_time";
            public static string IsRemove = @"is_remove";
            public static string RemoveTime = @"remove_time";
            public static string TriggerType = @"trigger_type";
            public static string IsDeal = @"is_deal";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

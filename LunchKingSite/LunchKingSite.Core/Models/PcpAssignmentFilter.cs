using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpAssignmentFilter class.
	/// </summary>
    [Serializable]
	public partial class PcpAssignmentFilterCollection : RepositoryList<PcpAssignmentFilter, PcpAssignmentFilterCollection>
	{	   
		public PcpAssignmentFilterCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpAssignmentFilterCollection</returns>
		public PcpAssignmentFilterCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpAssignmentFilter o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_assignment_filter table.
	/// </summary>
	[Serializable]
	public partial class PcpAssignmentFilter : RepositoryRecord<PcpAssignmentFilter>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpAssignmentFilter()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpAssignmentFilter(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_assignment_filter", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarAssignmentId = new TableSchema.TableColumn(schema);
				colvarAssignmentId.ColumnName = "assignment_id";
				colvarAssignmentId.DataType = DbType.Int32;
				colvarAssignmentId.MaxLength = 0;
				colvarAssignmentId.AutoIncrement = false;
				colvarAssignmentId.IsNullable = false;
				colvarAssignmentId.IsPrimaryKey = false;
				colvarAssignmentId.IsForeignKey = false;
				colvarAssignmentId.IsReadOnly = false;
				
						colvarAssignmentId.DefaultSetting = @"((0))";
				colvarAssignmentId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAssignmentId);
				
				TableSchema.TableColumn colvarFilterType = new TableSchema.TableColumn(schema);
				colvarFilterType.ColumnName = "filter_type";
				colvarFilterType.DataType = DbType.Byte;
				colvarFilterType.MaxLength = 0;
				colvarFilterType.AutoIncrement = false;
				colvarFilterType.IsNullable = false;
				colvarFilterType.IsPrimaryKey = false;
				colvarFilterType.IsForeignKey = false;
				colvarFilterType.IsReadOnly = false;
				
						colvarFilterType.DefaultSetting = @"((0))";
				colvarFilterType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFilterType);
				
				TableSchema.TableColumn colvarOperatorX = new TableSchema.TableColumn(schema);
				colvarOperatorX.ColumnName = "operator_x";
				colvarOperatorX.DataType = DbType.Byte;
				colvarOperatorX.MaxLength = 0;
				colvarOperatorX.AutoIncrement = false;
				colvarOperatorX.IsNullable = false;
				colvarOperatorX.IsPrimaryKey = false;
				colvarOperatorX.IsForeignKey = false;
				colvarOperatorX.IsReadOnly = false;
				
						colvarOperatorX.DefaultSetting = @"((0))";
				colvarOperatorX.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOperatorX);
				
				TableSchema.TableColumn colvarParameterFirst = new TableSchema.TableColumn(schema);
				colvarParameterFirst.ColumnName = "parameter_first";
				colvarParameterFirst.DataType = DbType.String;
				colvarParameterFirst.MaxLength = -1;
				colvarParameterFirst.AutoIncrement = false;
				colvarParameterFirst.IsNullable = false;
				colvarParameterFirst.IsPrimaryKey = false;
				colvarParameterFirst.IsForeignKey = false;
				colvarParameterFirst.IsReadOnly = false;
				
						colvarParameterFirst.DefaultSetting = @"('')";
				colvarParameterFirst.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParameterFirst);
				
				TableSchema.TableColumn colvarParameterSecond = new TableSchema.TableColumn(schema);
				colvarParameterSecond.ColumnName = "parameter_second";
				colvarParameterSecond.DataType = DbType.String;
				colvarParameterSecond.MaxLength = -1;
				colvarParameterSecond.AutoIncrement = false;
				colvarParameterSecond.IsNullable = true;
				colvarParameterSecond.IsPrimaryKey = false;
				colvarParameterSecond.IsForeignKey = false;
				colvarParameterSecond.IsReadOnly = false;
				colvarParameterSecond.DefaultSetting = @"";
				colvarParameterSecond.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParameterSecond);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_assignment_filter",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("AssignmentId")]
		[Bindable(true)]
		public int AssignmentId 
		{
			get { return GetColumnValue<int>(Columns.AssignmentId); }
			set { SetColumnValue(Columns.AssignmentId, value); }
		}
		  
		[XmlAttribute("FilterType")]
		[Bindable(true)]
		public byte FilterType 
		{
			get { return GetColumnValue<byte>(Columns.FilterType); }
			set { SetColumnValue(Columns.FilterType, value); }
		}
		  
		[XmlAttribute("OperatorX")]
		[Bindable(true)]
		public byte OperatorX 
		{
			get { return GetColumnValue<byte>(Columns.OperatorX); }
			set { SetColumnValue(Columns.OperatorX, value); }
		}
		  
		[XmlAttribute("ParameterFirst")]
		[Bindable(true)]
		public string ParameterFirst 
		{
			get { return GetColumnValue<string>(Columns.ParameterFirst); }
			set { SetColumnValue(Columns.ParameterFirst, value); }
		}
		  
		[XmlAttribute("ParameterSecond")]
		[Bindable(true)]
		public string ParameterSecond 
		{
			get { return GetColumnValue<string>(Columns.ParameterSecond); }
			set { SetColumnValue(Columns.ParameterSecond, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AssignmentIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FilterTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OperatorXColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ParameterFirstColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ParameterSecondColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string AssignmentId = @"assignment_id";
			 public static string FilterType = @"filter_type";
			 public static string OperatorX = @"operator_x";
			 public static string ParameterFirst = @"parameter_first";
			 public static string ParameterSecond = @"parameter_second";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

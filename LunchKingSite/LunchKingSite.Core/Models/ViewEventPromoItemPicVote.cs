using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEventPromoItemPicVote class.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromoItemPicVoteCollection : ReadOnlyList<ViewEventPromoItemPicVote, ViewEventPromoItemPicVoteCollection>
    {        
        public ViewEventPromoItemPicVoteCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_event_promo_item_pic_vote view.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromoItemPicVote : ReadOnlyRecord<ViewEventPromoItemPicVote>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_event_promo_item_pic_vote", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "Id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarMainId = new TableSchema.TableColumn(schema);
                colvarMainId.ColumnName = "MainId";
                colvarMainId.DataType = DbType.Int32;
                colvarMainId.MaxLength = 0;
                colvarMainId.AutoIncrement = false;
                colvarMainId.IsNullable = false;
                colvarMainId.IsPrimaryKey = false;
                colvarMainId.IsForeignKey = false;
                colvarMainId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainId);
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "Title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 200;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = false;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "Description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = 300;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = false;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescription);
                
                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "Seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = false;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarSeq);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "Status";
                colvarStatus.DataType = DbType.Boolean;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
                colvarCategory.ColumnName = "category";
                colvarCategory.DataType = DbType.String;
                colvarCategory.MaxLength = 200;
                colvarCategory.AutoIncrement = false;
                colvarCategory.IsNullable = true;
                colvarCategory.IsPrimaryKey = false;
                colvarCategory.IsForeignKey = false;
                colvarCategory.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = false;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarItemPicUrl = new TableSchema.TableColumn(schema);
                colvarItemPicUrl.ColumnName = "item_pic_url";
                colvarItemPicUrl.DataType = DbType.AnsiString;
                colvarItemPicUrl.MaxLength = 500;
                colvarItemPicUrl.AutoIncrement = false;
                colvarItemPicUrl.IsNullable = true;
                colvarItemPicUrl.IsPrimaryKey = false;
                colvarItemPicUrl.IsForeignKey = false;
                colvarItemPicUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPicUrl);
                
                TableSchema.TableColumn colvarLinkItemType = new TableSchema.TableColumn(schema);
                colvarLinkItemType.ColumnName = "link_item_type";
                colvarLinkItemType.DataType = DbType.Int32;
                colvarLinkItemType.MaxLength = 0;
                colvarLinkItemType.AutoIncrement = false;
                colvarLinkItemType.IsNullable = false;
                colvarLinkItemType.IsPrimaryKey = false;
                colvarLinkItemType.IsForeignKey = false;
                colvarLinkItemType.IsReadOnly = false;
                
                schema.Columns.Add(colvarLinkItemType);
                
                TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
                colvarStartDate.ColumnName = "start_date";
                colvarStartDate.DataType = DbType.DateTime;
                colvarStartDate.MaxLength = 0;
                colvarStartDate.AutoIncrement = false;
                colvarStartDate.IsNullable = true;
                colvarStartDate.IsPrimaryKey = false;
                colvarStartDate.IsForeignKey = false;
                colvarStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartDate);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = true;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_event_promo_item_pic_vote",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewEventPromoItemPicVote()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEventPromoItemPicVote(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewEventPromoItemPicVote(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewEventPromoItemPicVote(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("Id");
		    }
            set 
		    {
			    SetColumnValue("Id", value);
            }
        }
	      
        [XmlAttribute("MainId")]
        [Bindable(true)]
        public int MainId 
	    {
		    get
		    {
			    return GetColumnValue<int>("MainId");
		    }
            set 
		    {
			    SetColumnValue("MainId", value);
            }
        }
	      
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("Title");
		    }
            set 
		    {
			    SetColumnValue("Title", value);
            }
        }
	      
        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description 
	    {
		    get
		    {
			    return GetColumnValue<string>("Description");
		    }
            set 
		    {
			    SetColumnValue("Description", value);
            }
        }
	      
        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int Seq 
	    {
		    get
		    {
			    return GetColumnValue<int>("Seq");
		    }
            set 
		    {
			    SetColumnValue("Seq", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public bool Status 
	    {
		    get
		    {
			    return GetColumnValue<bool>("Status");
		    }
            set 
		    {
			    SetColumnValue("Status", value);
            }
        }
	      
        [XmlAttribute("Category")]
        [Bindable(true)]
        public string Category 
	    {
		    get
		    {
			    return GetColumnValue<string>("category");
		    }
            set 
		    {
			    SetColumnValue("category", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int ItemId 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("ItemPicUrl")]
        [Bindable(true)]
        public string ItemPicUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_pic_url");
		    }
            set 
		    {
			    SetColumnValue("item_pic_url", value);
            }
        }
	      
        [XmlAttribute("LinkItemType")]
        [Bindable(true)]
        public int LinkItemType 
	    {
		    get
		    {
			    return GetColumnValue<int>("link_item_type");
		    }
            set 
		    {
			    SetColumnValue("link_item_type", value);
            }
        }
	      
        [XmlAttribute("StartDate")]
        [Bindable(true)]
        public DateTime? StartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_date");
		    }
            set 
		    {
			    SetColumnValue("start_date", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"Id";
            
            public static string MainId = @"MainId";
            
            public static string Title = @"Title";
            
            public static string Description = @"Description";
            
            public static string Seq = @"Seq";
            
            public static string Status = @"Status";
            
            public static string Category = @"category";
            
            public static string ItemId = @"item_id";
            
            public static string ItemPicUrl = @"item_pic_url";
            
            public static string LinkItemType = @"link_item_type";
            
            public static string StartDate = @"start_date";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

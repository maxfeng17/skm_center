using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewServiceMessageDetail class.
    /// </summary>
    [Serializable]
    public partial class ViewServiceMessageDetailCollection : ReadOnlyList<ViewServiceMessageDetail, ViewServiceMessageDetailCollection>
    {
        public ViewServiceMessageDetailCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_service_message_detail view.
    /// </summary>
    [Serializable]
    public partial class ViewServiceMessageDetail : ReadOnlyRecord<ViewServiceMessageDetail>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_service_message_detail", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
                colvarCategory.ColumnName = "category";
                colvarCategory.DataType = DbType.Int32;
                colvarCategory.MaxLength = 0;
                colvarCategory.AutoIncrement = false;
                colvarCategory.IsNullable = true;
                colvarCategory.IsPrimaryKey = false;
                colvarCategory.IsForeignKey = false;
                colvarCategory.IsReadOnly = false;

                schema.Columns.Add(colvarCategory);

                TableSchema.TableColumn colvarSubCategory = new TableSchema.TableColumn(schema);
                colvarSubCategory.ColumnName = "sub_category";
                colvarSubCategory.DataType = DbType.Int32;
                colvarSubCategory.MaxLength = 0;
                colvarSubCategory.AutoIncrement = false;
                colvarSubCategory.IsNullable = true;
                colvarSubCategory.IsPrimaryKey = false;
                colvarSubCategory.IsForeignKey = false;
                colvarSubCategory.IsReadOnly = false;

                schema.Columns.Add(colvarSubCategory);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = -1;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;

                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
                colvarPhone.ColumnName = "phone";
                colvarPhone.DataType = DbType.AnsiString;
                colvarPhone.MaxLength = 50;
                colvarPhone.AutoIncrement = false;
                colvarPhone.IsNullable = true;
                colvarPhone.IsPrimaryKey = false;
                colvarPhone.IsForeignKey = false;
                colvarPhone.IsReadOnly = false;

                schema.Columns.Add(colvarPhone);

                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 50;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;

                schema.Columns.Add(colvarName);

                TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
                colvarEmail.ColumnName = "email";
                colvarEmail.DataType = DbType.String;
                colvarEmail.MaxLength = 100;
                colvarEmail.AutoIncrement = false;
                colvarEmail.IsNullable = true;
                colvarEmail.IsPrimaryKey = false;
                colvarEmail.IsForeignKey = false;
                colvarEmail.IsReadOnly = false;

                schema.Columns.Add(colvarEmail);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.String;
                colvarStatus.MaxLength = 50;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = -1;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = true;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;

                schema.Columns.Add(colvarRemark);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 100;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;

                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 100;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;

                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;

                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;

                schema.Columns.Add(colvarOrderId);

                TableSchema.TableColumn colvarMessageType = new TableSchema.TableColumn(schema);
                colvarMessageType.ColumnName = "message_type";
                colvarMessageType.DataType = DbType.String;
                colvarMessageType.MaxLength = 50;
                colvarMessageType.AutoIncrement = false;
                colvarMessageType.IsNullable = true;
                colvarMessageType.IsPrimaryKey = false;
                colvarMessageType.IsForeignKey = false;
                colvarMessageType.IsReadOnly = false;

                schema.Columns.Add(colvarMessageType);

                TableSchema.TableColumn colvarSubject = new TableSchema.TableColumn(schema);
                colvarSubject.ColumnName = "subject";
                colvarSubject.DataType = DbType.String;
                colvarSubject.MaxLength = 200;
                colvarSubject.AutoIncrement = false;
                colvarSubject.IsNullable = true;
                colvarSubject.IsPrimaryKey = false;
                colvarSubject.IsForeignKey = false;
                colvarSubject.IsReadOnly = false;

                schema.Columns.Add(colvarSubject);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;

                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarWorkType = new TableSchema.TableColumn(schema);
                colvarWorkType.ColumnName = "work_type";
                colvarWorkType.DataType = DbType.Int32;
                colvarWorkType.MaxLength = 0;
                colvarWorkType.AutoIncrement = false;
                colvarWorkType.IsNullable = true;
                colvarWorkType.IsPrimaryKey = false;
                colvarWorkType.IsForeignKey = false;
                colvarWorkType.IsReadOnly = false;

                schema.Columns.Add(colvarWorkType);

                TableSchema.TableColumn colvarWorkUser = new TableSchema.TableColumn(schema);
                colvarWorkUser.ColumnName = "work_user";
                colvarWorkUser.DataType = DbType.String;
                colvarWorkUser.MaxLength = 100;
                colvarWorkUser.AutoIncrement = false;
                colvarWorkUser.IsNullable = true;
                colvarWorkUser.IsPrimaryKey = false;
                colvarWorkUser.IsForeignKey = false;
                colvarWorkUser.IsReadOnly = false;

                schema.Columns.Add(colvarWorkUser);

                TableSchema.TableColumn colvarPriority = new TableSchema.TableColumn(schema);
                colvarPriority.ColumnName = "priority";
                colvarPriority.DataType = DbType.Int32;
                colvarPriority.MaxLength = 0;
                colvarPriority.AutoIncrement = false;
                colvarPriority.IsNullable = true;
                colvarPriority.IsPrimaryKey = false;
                colvarPriority.IsForeignKey = false;
                colvarPriority.IsReadOnly = false;

                schema.Columns.Add(colvarPriority);

                TableSchema.TableColumn colvarRemarkSeller = new TableSchema.TableColumn(schema);
                colvarRemarkSeller.ColumnName = "remark_seller";
                colvarRemarkSeller.DataType = DbType.String;
                colvarRemarkSeller.MaxLength = -1;
                colvarRemarkSeller.AutoIncrement = false;
                colvarRemarkSeller.IsNullable = true;
                colvarRemarkSeller.IsPrimaryKey = false;
                colvarRemarkSeller.IsForeignKey = false;
                colvarRemarkSeller.IsReadOnly = false;

                schema.Columns.Add(colvarRemarkSeller);

                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;

                schema.Columns.Add(colvarSellerName);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 250;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;

                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = true;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;

                schema.Columns.Add(colvarUniqueId);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;

                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = true;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourOrderTimeE);

                TableSchema.TableColumn colvarSellerTel = new TableSchema.TableColumn(schema);
                colvarSellerTel.ColumnName = "seller_tel";
                colvarSellerTel.DataType = DbType.AnsiString;
                colvarSellerTel.MaxLength = 100;
                colvarSellerTel.AutoIncrement = false;
                colvarSellerTel.IsNullable = true;
                colvarSellerTel.IsPrimaryKey = false;
                colvarSellerTel.IsForeignKey = false;
                colvarSellerTel.IsReadOnly = false;

                schema.Columns.Add(colvarSellerTel);

                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;

                schema.Columns.Add(colvarDeliveryAddress);

                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = true;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;

                schema.Columns.Add(colvarMemberName);

                TableSchema.TableColumn colvarReturnedPersonEmail = new TableSchema.TableColumn(schema);
                colvarReturnedPersonEmail.ColumnName = "returned_person_email";
                colvarReturnedPersonEmail.DataType = DbType.AnsiString;
                colvarReturnedPersonEmail.MaxLength = 200;
                colvarReturnedPersonEmail.AutoIncrement = false;
                colvarReturnedPersonEmail.IsNullable = true;
                colvarReturnedPersonEmail.IsPrimaryKey = false;
                colvarReturnedPersonEmail.IsForeignKey = false;
                colvarReturnedPersonEmail.IsReadOnly = false;

                schema.Columns.Add(colvarReturnedPersonEmail);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = true;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;

                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarOrderTimeE = new TableSchema.TableColumn(schema);
                colvarOrderTimeE.ColumnName = "order_time_e";
                colvarOrderTimeE.DataType = DbType.DateTime;
                colvarOrderTimeE.MaxLength = 0;
                colvarOrderTimeE.AutoIncrement = false;
                colvarOrderTimeE.IsNullable = true;
                colvarOrderTimeE.IsPrimaryKey = false;
                colvarOrderTimeE.IsForeignKey = false;
                colvarOrderTimeE.IsReadOnly = false;

                schema.Columns.Add(colvarOrderTimeE);

                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;

                schema.Columns.Add(colvarMobile);

                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "company_name";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 100;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;

                schema.Columns.Add(colvarCompanyName);

                TableSchema.TableColumn colvarShipNo = new TableSchema.TableColumn(schema);
                colvarShipNo.ColumnName = "ship_no";
                colvarShipNo.DataType = DbType.String;
                colvarShipNo.MaxLength = -1;
                colvarShipNo.AutoIncrement = false;
                colvarShipNo.IsNullable = true;
                colvarShipNo.IsPrimaryKey = false;
                colvarShipNo.IsForeignKey = false;
                colvarShipNo.IsReadOnly = false;

                schema.Columns.Add(colvarShipNo);

                TableSchema.TableColumn colvarDealEmpName = new TableSchema.TableColumn(schema);
                colvarDealEmpName.ColumnName = "deal_emp_name";
                colvarDealEmpName.DataType = DbType.String;
                colvarDealEmpName.MaxLength = 50;
                colvarDealEmpName.AutoIncrement = false;
                colvarDealEmpName.IsNullable = true;
                colvarDealEmpName.IsPrimaryKey = false;
                colvarDealEmpName.IsForeignKey = false;
                colvarDealEmpName.IsReadOnly = false;

                schema.Columns.Add(colvarDealEmpName);

                TableSchema.TableColumn colvarLabelIconList = new TableSchema.TableColumn(schema);
                colvarLabelIconList.ColumnName = "label_icon_list";
                colvarLabelIconList.DataType = DbType.AnsiString;
                colvarLabelIconList.MaxLength = 255;
                colvarLabelIconList.AutoIncrement = false;
                colvarLabelIconList.IsNullable = true;
                colvarLabelIconList.IsPrimaryKey = false;
                colvarLabelIconList.IsForeignKey = false;
                colvarLabelIconList.IsReadOnly = false;

                schema.Columns.Add(colvarLabelIconList);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = true;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;

                schema.Columns.Add(colvarGuid);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_service_message_detail", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewServiceMessageDetail()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewServiceMessageDetail(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewServiceMessageDetail(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewServiceMessageDetail(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("Category")]
        [Bindable(true)]
        public int? Category
        {
            get
            {
                return GetColumnValue<int?>("category");
            }
            set
            {
                SetColumnValue("category", value);
            }
        }

        [XmlAttribute("SubCategory")]
        [Bindable(true)]
        public int? SubCategory
        {
            get
            {
                return GetColumnValue<int?>("sub_category");
            }
            set
            {
                SetColumnValue("sub_category", value);
            }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get
            {
                return GetColumnValue<string>("message");
            }
            set
            {
                SetColumnValue("message", value);
            }
        }

        [XmlAttribute("Phone")]
        [Bindable(true)]
        public string Phone
        {
            get
            {
                return GetColumnValue<string>("phone");
            }
            set
            {
                SetColumnValue("phone", value);
            }
        }

        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name
        {
            get
            {
                return GetColumnValue<string>("name");
            }
            set
            {
                SetColumnValue("name", value);
            }
        }

        [XmlAttribute("Email")]
        [Bindable(true)]
        public string Email
        {
            get
            {
                return GetColumnValue<string>("email");
            }
            set
            {
                SetColumnValue("email", value);
            }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public string Status
        {
            get
            {
                return GetColumnValue<string>("status");
            }
            set
            {
                SetColumnValue("status", value);
            }
        }

        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark
        {
            get
            {
                return GetColumnValue<string>("remark");
            }
            set
            {
                SetColumnValue("remark", value);
            }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get
            {
                return GetColumnValue<string>("create_id");
            }
            set
            {
                SetColumnValue("create_id", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get
            {
                return GetColumnValue<DateTime?>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get
            {
                return GetColumnValue<string>("modify_id");
            }
            set
            {
                SetColumnValue("modify_id", value);
            }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get
            {
                return GetColumnValue<DateTime?>("modify_time");
            }
            set
            {
                SetColumnValue("modify_time", value);
            }
        }

        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId
        {
            get
            {
                return GetColumnValue<string>("order_id");
            }
            set
            {
                SetColumnValue("order_id", value);
            }
        }

        [XmlAttribute("MessageType")]
        [Bindable(true)]
        public string MessageType
        {
            get
            {
                return GetColumnValue<string>("message_type");
            }
            set
            {
                SetColumnValue("message_type", value);
            }
        }

        [XmlAttribute("Subject")]
        [Bindable(true)]
        public string Subject
        {
            get
            {
                return GetColumnValue<string>("subject");
            }
            set
            {
                SetColumnValue("subject", value);
            }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get
            {
                return GetColumnValue<int>("type");
            }
            set
            {
                SetColumnValue("type", value);
            }
        }

        [XmlAttribute("WorkType")]
        [Bindable(true)]
        public int? WorkType
        {
            get
            {
                return GetColumnValue<int?>("work_type");
            }
            set
            {
                SetColumnValue("work_type", value);
            }
        }

        [XmlAttribute("WorkUser")]
        [Bindable(true)]
        public string WorkUser
        {
            get
            {
                return GetColumnValue<string>("work_user");
            }
            set
            {
                SetColumnValue("work_user", value);
            }
        }

        [XmlAttribute("Priority")]
        [Bindable(true)]
        public int? Priority
        {
            get
            {
                return GetColumnValue<int?>("priority");
            }
            set
            {
                SetColumnValue("priority", value);
            }
        }

        [XmlAttribute("RemarkSeller")]
        [Bindable(true)]
        public string RemarkSeller
        {
            get
            {
                return GetColumnValue<string>("remark_seller");
            }
            set
            {
                SetColumnValue("remark_seller", value);
            }
        }

        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName
        {
            get
            {
                return GetColumnValue<string>("seller_name");
            }
            set
            {
                SetColumnValue("seller_name", value);
            }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get
            {
                return GetColumnValue<string>("item_name");
            }
            set
            {
                SetColumnValue("item_name", value);
            }
        }

        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int? UniqueId
        {
            get
            {
                return GetColumnValue<int?>("unique_id");
            }
            set
            {
                SetColumnValue("unique_id", value);
            }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid
        {
            get
            {
                return GetColumnValue<Guid?>("order_guid");
            }
            set
            {
                SetColumnValue("order_guid", value);
            }
        }

        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourOrderTimeE
        {
            get
            {
                return GetColumnValue<DateTime?>("business_hour_order_time_e");
            }
            set
            {
                SetColumnValue("business_hour_order_time_e", value);
            }
        }

        [XmlAttribute("SellerTel")]
        [Bindable(true)]
        public string SellerTel
        {
            get
            {
                return GetColumnValue<string>("seller_tel");
            }
            set
            {
                SetColumnValue("seller_tel", value);
            }
        }

        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress
        {
            get
            {
                return GetColumnValue<string>("delivery_address");
            }
            set
            {
                SetColumnValue("delivery_address", value);
            }
        }

        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName
        {
            get
            {
                return GetColumnValue<string>("member_name");
            }
            set
            {
                SetColumnValue("member_name", value);
            }
        }

        [XmlAttribute("ReturnedPersonEmail")]
        [Bindable(true)]
        public string ReturnedPersonEmail
        {
            get
            {
                return GetColumnValue<string>("returned_person_email");
            }
            set
            {
                SetColumnValue("returned_person_email", value);
            }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal? Amount
        {
            get
            {
                return GetColumnValue<decimal?>("amount");
            }
            set
            {
                SetColumnValue("amount", value);
            }
        }

        [XmlAttribute("OrderTimeE")]
        [Bindable(true)]
        public DateTime? OrderTimeE
        {
            get
            {
                return GetColumnValue<DateTime?>("order_time_e");
            }
            set
            {
                SetColumnValue("order_time_e", value);
            }
        }

        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile
        {
            get
            {
                return GetColumnValue<string>("mobile");
            }
            set
            {
                SetColumnValue("mobile", value);
            }
        }

        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName
        {
            get
            {
                return GetColumnValue<string>("company_name");
            }
            set
            {
                SetColumnValue("company_name", value);
            }
        }

        [XmlAttribute("ShipNo")]
        [Bindable(true)]
        public string ShipNo
        {
            get
            {
                return GetColumnValue<string>("ship_no");
            }
            set
            {
                SetColumnValue("ship_no", value);
            }
        }

        [XmlAttribute("DealEmpName")]
        [Bindable(true)]
        public string DealEmpName
        {
            get
            {
                return GetColumnValue<string>("deal_emp_name");
            }
            set
            {
                SetColumnValue("deal_emp_name", value);
            }
        }

        [XmlAttribute("LabelIconList")]
        [Bindable(true)]
        public string LabelIconList
        {
            get
            {
                return GetColumnValue<string>("label_icon_list");
            }
            set
            {
                SetColumnValue("label_icon_list", value);
            }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid? Guid
        {
            get
            {
                return GetColumnValue<Guid?>("GUID");
            }
            set
            {
                SetColumnValue("GUID", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Id = @"id";

            public static string Category = @"category";

            public static string SubCategory = @"sub_category";

            public static string Message = @"message";

            public static string Phone = @"phone";

            public static string Name = @"name";

            public static string Email = @"email";

            public static string Status = @"status";

            public static string Remark = @"remark";

            public static string CreateId = @"create_id";

            public static string CreateTime = @"create_time";

            public static string ModifyId = @"modify_id";

            public static string ModifyTime = @"modify_time";

            public static string OrderId = @"order_id";

            public static string MessageType = @"message_type";

            public static string Subject = @"subject";

            public static string Type = @"type";

            public static string WorkType = @"work_type";

            public static string WorkUser = @"work_user";

            public static string Priority = @"priority";

            public static string RemarkSeller = @"remark_seller";

            public static string SellerName = @"seller_name";

            public static string ItemName = @"item_name";

            public static string UniqueId = @"unique_id";

            public static string OrderGuid = @"order_guid";

            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";

            public static string SellerTel = @"seller_tel";

            public static string DeliveryAddress = @"delivery_address";

            public static string MemberName = @"member_name";

            public static string ReturnedPersonEmail = @"returned_person_email";

            public static string Amount = @"amount";

            public static string OrderTimeE = @"order_time_e";

            public static string Mobile = @"mobile";

            public static string CompanyName = @"company_name";

            public static string ShipNo = @"ship_no";

            public static string DealEmpName = @"deal_emp_name";

            public static string LabelIconList = @"label_icon_list";

            public static string Guid = @"GUID";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

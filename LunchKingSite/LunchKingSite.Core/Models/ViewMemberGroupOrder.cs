using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewMemberGroupOrderCollection : ReadOnlyList<ViewMemberGroupOrder, ViewMemberGroupOrderCollection>
	{
			public ViewMemberGroupOrderCollection() {}

	}

	[Serializable]
	public partial class ViewMemberGroupOrder : ReadOnlyRecord<ViewMemberGroupOrder>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewMemberGroupOrder()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewMemberGroupOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewMemberGroupOrder(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewMemberGroupOrder(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_member_group_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
				colvarUserName.ColumnName = "user_name";
				colvarUserName.DataType = DbType.String;
				colvarUserName.MaxLength = 256;
				colvarUserName.AutoIncrement = false;
				colvarUserName.IsNullable = false;
				colvarUserName.IsPrimaryKey = false;
				colvarUserName.IsForeignKey = false;
				colvarUserName.IsReadOnly = false;
				colvarUserName.DefaultSetting = @"";
				colvarUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserName);

				TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
				colvarLastName.ColumnName = "last_name";
				colvarLastName.DataType = DbType.String;
				colvarLastName.MaxLength = 50;
				colvarLastName.AutoIncrement = false;
				colvarLastName.IsNullable = true;
				colvarLastName.IsPrimaryKey = false;
				colvarLastName.IsForeignKey = false;
				colvarLastName.IsReadOnly = false;
				colvarLastName.DefaultSetting = @"";
				colvarLastName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastName);

				TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
				colvarFirstName.ColumnName = "first_name";
				colvarFirstName.DataType = DbType.String;
				colvarFirstName.MaxLength = 50;
				colvarFirstName.AutoIncrement = false;
				colvarFirstName.IsNullable = true;
				colvarFirstName.IsPrimaryKey = false;
				colvarFirstName.IsForeignKey = false;
				colvarFirstName.IsReadOnly = false;
				colvarFirstName.DefaultSetting = @"";
				colvarFirstName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFirstName);

				TableSchema.TableColumn colvarBuildingGuid = new TableSchema.TableColumn(schema);
				colvarBuildingGuid.ColumnName = "building_GUID";
				colvarBuildingGuid.DataType = DbType.Guid;
				colvarBuildingGuid.MaxLength = 0;
				colvarBuildingGuid.AutoIncrement = false;
				colvarBuildingGuid.IsNullable = true;
				colvarBuildingGuid.IsPrimaryKey = false;
				colvarBuildingGuid.IsForeignKey = false;
				colvarBuildingGuid.IsReadOnly = false;
				colvarBuildingGuid.DefaultSetting = @"";
				colvarBuildingGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuildingGuid);

				TableSchema.TableColumn colvarBirthday = new TableSchema.TableColumn(schema);
				colvarBirthday.ColumnName = "birthday";
				colvarBirthday.DataType = DbType.DateTime;
				colvarBirthday.MaxLength = 0;
				colvarBirthday.AutoIncrement = false;
				colvarBirthday.IsNullable = true;
				colvarBirthday.IsPrimaryKey = false;
				colvarBirthday.IsForeignKey = false;
				colvarBirthday.IsReadOnly = false;
				colvarBirthday.DefaultSetting = @"";
				colvarBirthday.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBirthday);

				TableSchema.TableColumn colvarGender = new TableSchema.TableColumn(schema);
				colvarGender.ColumnName = "gender";
				colvarGender.DataType = DbType.Int32;
				colvarGender.MaxLength = 0;
				colvarGender.AutoIncrement = false;
				colvarGender.IsNullable = true;
				colvarGender.IsPrimaryKey = false;
				colvarGender.IsForeignKey = false;
				colvarGender.IsReadOnly = false;
				colvarGender.DefaultSetting = @"";
				colvarGender.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGender);

				TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
				colvarMobile.ColumnName = "mobile";
				colvarMobile.DataType = DbType.AnsiString;
				colvarMobile.MaxLength = 50;
				colvarMobile.AutoIncrement = false;
				colvarMobile.IsNullable = true;
				colvarMobile.IsPrimaryKey = false;
				colvarMobile.IsForeignKey = false;
				colvarMobile.IsReadOnly = false;
				colvarMobile.DefaultSetting = @"";
				colvarMobile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobile);

				TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
				colvarCompanyName.ColumnName = "company_name";
				colvarCompanyName.DataType = DbType.String;
				colvarCompanyName.MaxLength = 50;
				colvarCompanyName.AutoIncrement = false;
				colvarCompanyName.IsNullable = true;
				colvarCompanyName.IsPrimaryKey = false;
				colvarCompanyName.IsForeignKey = false;
				colvarCompanyName.IsReadOnly = false;
				colvarCompanyName.DefaultSetting = @"";
				colvarCompanyName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyName);

				TableSchema.TableColumn colvarCompanyDepartment = new TableSchema.TableColumn(schema);
				colvarCompanyDepartment.ColumnName = "company_department";
				colvarCompanyDepartment.DataType = DbType.String;
				colvarCompanyDepartment.MaxLength = 50;
				colvarCompanyDepartment.AutoIncrement = false;
				colvarCompanyDepartment.IsNullable = true;
				colvarCompanyDepartment.IsPrimaryKey = false;
				colvarCompanyDepartment.IsForeignKey = false;
				colvarCompanyDepartment.IsReadOnly = false;
				colvarCompanyDepartment.DefaultSetting = @"";
				colvarCompanyDepartment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyDepartment);

				TableSchema.TableColumn colvarCompanyTel = new TableSchema.TableColumn(schema);
				colvarCompanyTel.ColumnName = "company_tel";
				colvarCompanyTel.DataType = DbType.AnsiString;
				colvarCompanyTel.MaxLength = 20;
				colvarCompanyTel.AutoIncrement = false;
				colvarCompanyTel.IsNullable = true;
				colvarCompanyTel.IsPrimaryKey = false;
				colvarCompanyTel.IsForeignKey = false;
				colvarCompanyTel.IsReadOnly = false;
				colvarCompanyTel.DefaultSetting = @"";
				colvarCompanyTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyTel);

				TableSchema.TableColumn colvarCompanyTelExt = new TableSchema.TableColumn(schema);
				colvarCompanyTelExt.ColumnName = "company_tel_ext";
				colvarCompanyTelExt.DataType = DbType.AnsiString;
				colvarCompanyTelExt.MaxLength = 50;
				colvarCompanyTelExt.AutoIncrement = false;
				colvarCompanyTelExt.IsNullable = true;
				colvarCompanyTelExt.IsPrimaryKey = false;
				colvarCompanyTelExt.IsForeignKey = false;
				colvarCompanyTelExt.IsReadOnly = false;
				colvarCompanyTelExt.DefaultSetting = @"";
				colvarCompanyTelExt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyTelExt);

				TableSchema.TableColumn colvarCompanyAddress = new TableSchema.TableColumn(schema);
				colvarCompanyAddress.ColumnName = "company_address";
				colvarCompanyAddress.DataType = DbType.String;
				colvarCompanyAddress.MaxLength = 100;
				colvarCompanyAddress.AutoIncrement = false;
				colvarCompanyAddress.IsNullable = true;
				colvarCompanyAddress.IsPrimaryKey = false;
				colvarCompanyAddress.IsForeignKey = false;
				colvarCompanyAddress.IsReadOnly = false;
				colvarCompanyAddress.DefaultSetting = @"";
				colvarCompanyAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyAddress);

				TableSchema.TableColumn colvarDeliveryMethod = new TableSchema.TableColumn(schema);
				colvarDeliveryMethod.ColumnName = "delivery_method";
				colvarDeliveryMethod.DataType = DbType.String;
				colvarDeliveryMethod.MaxLength = 200;
				colvarDeliveryMethod.AutoIncrement = false;
				colvarDeliveryMethod.IsNullable = true;
				colvarDeliveryMethod.IsPrimaryKey = false;
				colvarDeliveryMethod.IsForeignKey = false;
				colvarDeliveryMethod.IsReadOnly = false;
				colvarDeliveryMethod.DefaultSetting = @"";
				colvarDeliveryMethod.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryMethod);

				TableSchema.TableColumn colvarPrimaryContactMethod = new TableSchema.TableColumn(schema);
				colvarPrimaryContactMethod.ColumnName = "primary_contact_method";
				colvarPrimaryContactMethod.DataType = DbType.Int32;
				colvarPrimaryContactMethod.MaxLength = 0;
				colvarPrimaryContactMethod.AutoIncrement = false;
				colvarPrimaryContactMethod.IsNullable = false;
				colvarPrimaryContactMethod.IsPrimaryKey = false;
				colvarPrimaryContactMethod.IsForeignKey = false;
				colvarPrimaryContactMethod.IsReadOnly = false;
				colvarPrimaryContactMethod.DefaultSetting = @"";
				colvarPrimaryContactMethod.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrimaryContactMethod);

				TableSchema.TableColumn colvarMemberStatus = new TableSchema.TableColumn(schema);
				colvarMemberStatus.ColumnName = "member_status";
				colvarMemberStatus.DataType = DbType.Int32;
				colvarMemberStatus.MaxLength = 0;
				colvarMemberStatus.AutoIncrement = false;
				colvarMemberStatus.IsNullable = false;
				colvarMemberStatus.IsPrimaryKey = false;
				colvarMemberStatus.IsForeignKey = false;
				colvarMemberStatus.IsReadOnly = false;
				colvarMemberStatus.DefaultSetting = @"";
				colvarMemberStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberStatus);

				TableSchema.TableColumn colvarMemberCreateTime = new TableSchema.TableColumn(schema);
				colvarMemberCreateTime.ColumnName = "member_create_time";
				colvarMemberCreateTime.DataType = DbType.DateTime;
				colvarMemberCreateTime.MaxLength = 0;
				colvarMemberCreateTime.AutoIncrement = false;
				colvarMemberCreateTime.IsNullable = false;
				colvarMemberCreateTime.IsPrimaryKey = false;
				colvarMemberCreateTime.IsForeignKey = false;
				colvarMemberCreateTime.IsReadOnly = false;
				colvarMemberCreateTime.DefaultSetting = @"";
				colvarMemberCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberCreateTime);

				TableSchema.TableColumn colvarMemberModifyTime = new TableSchema.TableColumn(schema);
				colvarMemberModifyTime.ColumnName = "member_modify_time";
				colvarMemberModifyTime.DataType = DbType.DateTime;
				colvarMemberModifyTime.MaxLength = 0;
				colvarMemberModifyTime.AutoIncrement = false;
				colvarMemberModifyTime.IsNullable = true;
				colvarMemberModifyTime.IsPrimaryKey = false;
				colvarMemberModifyTime.IsForeignKey = false;
				colvarMemberModifyTime.IsReadOnly = false;
				colvarMemberModifyTime.DefaultSetting = @"";
				colvarMemberModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberModifyTime);

				TableSchema.TableColumn colvarMemberModifyId = new TableSchema.TableColumn(schema);
				colvarMemberModifyId.ColumnName = "member_modify_id";
				colvarMemberModifyId.DataType = DbType.String;
				colvarMemberModifyId.MaxLength = 256;
				colvarMemberModifyId.AutoIncrement = false;
				colvarMemberModifyId.IsNullable = true;
				colvarMemberModifyId.IsPrimaryKey = false;
				colvarMemberModifyId.IsForeignKey = false;
				colvarMemberModifyId.IsReadOnly = false;
				colvarMemberModifyId.DefaultSetting = @"";
				colvarMemberModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberModifyId);

				TableSchema.TableColumn colvarComment = new TableSchema.TableColumn(schema);
				colvarComment.ColumnName = "comment";
				colvarComment.DataType = DbType.String;
				colvarComment.MaxLength = 100;
				colvarComment.AutoIncrement = false;
				colvarComment.IsNullable = true;
				colvarComment.IsPrimaryKey = false;
				colvarComment.IsForeignKey = false;
				colvarComment.IsReadOnly = false;
				colvarComment.DefaultSetting = @"";
				colvarComment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarComment);

				TableSchema.TableColumn colvarGroupOrderGuid = new TableSchema.TableColumn(schema);
				colvarGroupOrderGuid.ColumnName = "group_order_guid";
				colvarGroupOrderGuid.DataType = DbType.Guid;
				colvarGroupOrderGuid.MaxLength = 0;
				colvarGroupOrderGuid.AutoIncrement = false;
				colvarGroupOrderGuid.IsNullable = false;
				colvarGroupOrderGuid.IsPrimaryKey = false;
				colvarGroupOrderGuid.IsForeignKey = false;
				colvarGroupOrderGuid.IsReadOnly = false;
				colvarGroupOrderGuid.DefaultSetting = @"";
				colvarGroupOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupOrderGuid);

				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);

				TableSchema.TableColumn colvarGroupOrderCreateTime = new TableSchema.TableColumn(schema);
				colvarGroupOrderCreateTime.ColumnName = "group_order_create_time";
				colvarGroupOrderCreateTime.DataType = DbType.DateTime;
				colvarGroupOrderCreateTime.MaxLength = 0;
				colvarGroupOrderCreateTime.AutoIncrement = false;
				colvarGroupOrderCreateTime.IsNullable = false;
				colvarGroupOrderCreateTime.IsPrimaryKey = false;
				colvarGroupOrderCreateTime.IsForeignKey = false;
				colvarGroupOrderCreateTime.IsReadOnly = false;
				colvarGroupOrderCreateTime.DefaultSetting = @"";
				colvarGroupOrderCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupOrderCreateTime);

				TableSchema.TableColumn colvarGroupOrderCloseTime = new TableSchema.TableColumn(schema);
				colvarGroupOrderCloseTime.ColumnName = "group_order_close_time";
				colvarGroupOrderCloseTime.DataType = DbType.DateTime;
				colvarGroupOrderCloseTime.MaxLength = 0;
				colvarGroupOrderCloseTime.AutoIncrement = false;
				colvarGroupOrderCloseTime.IsNullable = false;
				colvarGroupOrderCloseTime.IsPrimaryKey = false;
				colvarGroupOrderCloseTime.IsForeignKey = false;
				colvarGroupOrderCloseTime.IsReadOnly = false;
				colvarGroupOrderCloseTime.DefaultSetting = @"";
				colvarGroupOrderCloseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupOrderCloseTime);

				TableSchema.TableColumn colvarGroupOrderStatus = new TableSchema.TableColumn(schema);
				colvarGroupOrderStatus.ColumnName = "group_order_status";
				colvarGroupOrderStatus.DataType = DbType.Int32;
				colvarGroupOrderStatus.MaxLength = 0;
				colvarGroupOrderStatus.AutoIncrement = false;
				colvarGroupOrderStatus.IsNullable = true;
				colvarGroupOrderStatus.IsPrimaryKey = false;
				colvarGroupOrderStatus.IsForeignKey = false;
				colvarGroupOrderStatus.IsReadOnly = false;
				colvarGroupOrderStatus.DefaultSetting = @"";
				colvarGroupOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupOrderStatus);

				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = true;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_GUID";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 50;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);

				TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
				colvarMemberName.ColumnName = "member_name";
				colvarMemberName.DataType = DbType.String;
				colvarMemberName.MaxLength = 50;
				colvarMemberName.AutoIncrement = false;
				colvarMemberName.IsNullable = false;
				colvarMemberName.IsPrimaryKey = false;
				colvarMemberName.IsForeignKey = false;
				colvarMemberName.IsReadOnly = false;
				colvarMemberName.DefaultSetting = @"";
				colvarMemberName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberName);

				TableSchema.TableColumn colvarPhoneNumber = new TableSchema.TableColumn(schema);
				colvarPhoneNumber.ColumnName = "phone_number";
				colvarPhoneNumber.DataType = DbType.AnsiString;
				colvarPhoneNumber.MaxLength = 50;
				colvarPhoneNumber.AutoIncrement = false;
				colvarPhoneNumber.IsNullable = true;
				colvarPhoneNumber.IsPrimaryKey = false;
				colvarPhoneNumber.IsForeignKey = false;
				colvarPhoneNumber.IsReadOnly = false;
				colvarPhoneNumber.DefaultSetting = @"";
				colvarPhoneNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhoneNumber);

				TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
				colvarMobileNumber.ColumnName = "mobile_number";
				colvarMobileNumber.DataType = DbType.AnsiString;
				colvarMobileNumber.MaxLength = 50;
				colvarMobileNumber.AutoIncrement = false;
				colvarMobileNumber.IsNullable = true;
				colvarMobileNumber.IsPrimaryKey = false;
				colvarMobileNumber.IsForeignKey = false;
				colvarMobileNumber.IsReadOnly = false;
				colvarMobileNumber.DefaultSetting = @"";
				colvarMobileNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileNumber);

				TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
				colvarDeliveryAddress.ColumnName = "delivery_address";
				colvarDeliveryAddress.DataType = DbType.String;
				colvarDeliveryAddress.MaxLength = 200;
				colvarDeliveryAddress.AutoIncrement = false;
				colvarDeliveryAddress.IsNullable = true;
				colvarDeliveryAddress.IsPrimaryKey = false;
				colvarDeliveryAddress.IsForeignKey = false;
				colvarDeliveryAddress.IsReadOnly = false;
				colvarDeliveryAddress.DefaultSetting = @"";
				colvarDeliveryAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryAddress);

				TableSchema.TableColumn colvarDeliveryTime = new TableSchema.TableColumn(schema);
				colvarDeliveryTime.ColumnName = "delivery_time";
				colvarDeliveryTime.DataType = DbType.DateTime;
				colvarDeliveryTime.MaxLength = 0;
				colvarDeliveryTime.AutoIncrement = false;
				colvarDeliveryTime.IsNullable = true;
				colvarDeliveryTime.IsPrimaryKey = false;
				colvarDeliveryTime.IsForeignKey = false;
				colvarDeliveryTime.IsReadOnly = false;
				colvarDeliveryTime.DefaultSetting = @"";
				colvarDeliveryTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryTime);

				TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
				colvarSubtotal.ColumnName = "subtotal";
				colvarSubtotal.DataType = DbType.Currency;
				colvarSubtotal.MaxLength = 0;
				colvarSubtotal.AutoIncrement = false;
				colvarSubtotal.IsNullable = false;
				colvarSubtotal.IsPrimaryKey = false;
				colvarSubtotal.IsForeignKey = false;
				colvarSubtotal.IsReadOnly = false;
				colvarSubtotal.DefaultSetting = @"";
				colvarSubtotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubtotal);

				TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
				colvarTotal.ColumnName = "total";
				colvarTotal.DataType = DbType.Currency;
				colvarTotal.MaxLength = 0;
				colvarTotal.AutoIncrement = false;
				colvarTotal.IsNullable = false;
				colvarTotal.IsPrimaryKey = false;
				colvarTotal.IsForeignKey = false;
				colvarTotal.IsReadOnly = false;
				colvarTotal.DefaultSetting = @"";
				colvarTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotal);

				TableSchema.TableColumn colvarUserMemo = new TableSchema.TableColumn(schema);
				colvarUserMemo.ColumnName = "user_memo";
				colvarUserMemo.DataType = DbType.String;
				colvarUserMemo.MaxLength = 1073741823;
				colvarUserMemo.AutoIncrement = false;
				colvarUserMemo.IsNullable = true;
				colvarUserMemo.IsPrimaryKey = false;
				colvarUserMemo.IsForeignKey = false;
				colvarUserMemo.IsReadOnly = false;
				colvarUserMemo.DefaultSetting = @"";
				colvarUserMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserMemo);

				TableSchema.TableColumn colvarOrderMemo = new TableSchema.TableColumn(schema);
				colvarOrderMemo.ColumnName = "order_memo";
				colvarOrderMemo.DataType = DbType.String;
				colvarOrderMemo.MaxLength = 1073741823;
				colvarOrderMemo.AutoIncrement = false;
				colvarOrderMemo.IsNullable = true;
				colvarOrderMemo.IsPrimaryKey = false;
				colvarOrderMemo.IsForeignKey = false;
				colvarOrderMemo.IsReadOnly = false;
				colvarOrderMemo.DefaultSetting = @"";
				colvarOrderMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderMemo);

				TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
				colvarOrderStatus.ColumnName = "order_status";
				colvarOrderStatus.DataType = DbType.Int32;
				colvarOrderStatus.MaxLength = 0;
				colvarOrderStatus.AutoIncrement = false;
				colvarOrderStatus.IsNullable = false;
				colvarOrderStatus.IsPrimaryKey = false;
				colvarOrderStatus.IsForeignKey = false;
				colvarOrderStatus.IsReadOnly = false;
				colvarOrderStatus.DefaultSetting = @"";
				colvarOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStatus);

				TableSchema.TableColumn colvarOrderCreateId = new TableSchema.TableColumn(schema);
				colvarOrderCreateId.ColumnName = "order_create_id";
				colvarOrderCreateId.DataType = DbType.String;
				colvarOrderCreateId.MaxLength = 30;
				colvarOrderCreateId.AutoIncrement = false;
				colvarOrderCreateId.IsNullable = false;
				colvarOrderCreateId.IsPrimaryKey = false;
				colvarOrderCreateId.IsForeignKey = false;
				colvarOrderCreateId.IsReadOnly = false;
				colvarOrderCreateId.DefaultSetting = @"";
				colvarOrderCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderCreateId);

				TableSchema.TableColumn colvarOrderCreateTime = new TableSchema.TableColumn(schema);
				colvarOrderCreateTime.ColumnName = "order_create_time";
				colvarOrderCreateTime.DataType = DbType.DateTime;
				colvarOrderCreateTime.MaxLength = 0;
				colvarOrderCreateTime.AutoIncrement = false;
				colvarOrderCreateTime.IsNullable = false;
				colvarOrderCreateTime.IsPrimaryKey = false;
				colvarOrderCreateTime.IsForeignKey = false;
				colvarOrderCreateTime.IsReadOnly = false;
				colvarOrderCreateTime.DefaultSetting = @"";
				colvarOrderCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderCreateTime);

				TableSchema.TableColumn colvarOrderModifyId = new TableSchema.TableColumn(schema);
				colvarOrderModifyId.ColumnName = "order_modify_id";
				colvarOrderModifyId.DataType = DbType.String;
				colvarOrderModifyId.MaxLength = 30;
				colvarOrderModifyId.AutoIncrement = false;
				colvarOrderModifyId.IsNullable = true;
				colvarOrderModifyId.IsPrimaryKey = false;
				colvarOrderModifyId.IsForeignKey = false;
				colvarOrderModifyId.IsReadOnly = false;
				colvarOrderModifyId.DefaultSetting = @"";
				colvarOrderModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderModifyId);

				TableSchema.TableColumn colvarOrderModifyTime = new TableSchema.TableColumn(schema);
				colvarOrderModifyTime.ColumnName = "order_modify_time";
				colvarOrderModifyTime.DataType = DbType.DateTime;
				colvarOrderModifyTime.MaxLength = 0;
				colvarOrderModifyTime.AutoIncrement = false;
				colvarOrderModifyTime.IsNullable = true;
				colvarOrderModifyTime.IsPrimaryKey = false;
				colvarOrderModifyTime.IsForeignKey = false;
				colvarOrderModifyTime.IsReadOnly = false;
				colvarOrderModifyTime.DefaultSetting = @"";
				colvarOrderModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderModifyTime);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarOrderAccessLock = new TableSchema.TableColumn(schema);
				colvarOrderAccessLock.ColumnName = "order_access_lock";
				colvarOrderAccessLock.DataType = DbType.String;
				colvarOrderAccessLock.MaxLength = 50;
				colvarOrderAccessLock.AutoIncrement = false;
				colvarOrderAccessLock.IsNullable = true;
				colvarOrderAccessLock.IsPrimaryKey = false;
				colvarOrderAccessLock.IsForeignKey = false;
				colvarOrderAccessLock.IsReadOnly = false;
				colvarOrderAccessLock.DefaultSetting = @"";
				colvarOrderAccessLock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderAccessLock);

				TableSchema.TableColumn colvarOrderStage = new TableSchema.TableColumn(schema);
				colvarOrderStage.ColumnName = "order_stage";
				colvarOrderStage.DataType = DbType.Int32;
				colvarOrderStage.MaxLength = 0;
				colvarOrderStage.AutoIncrement = false;
				colvarOrderStage.IsNullable = false;
				colvarOrderStage.IsPrimaryKey = false;
				colvarOrderStage.IsForeignKey = false;
				colvarOrderStage.IsReadOnly = false;
				colvarOrderStage.DefaultSetting = @"";
				colvarOrderStage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStage);

				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);

				TableSchema.TableColumn colvarBusinessHourId = new TableSchema.TableColumn(schema);
				colvarBusinessHourId.ColumnName = "business_hour_id";
				colvarBusinessHourId.DataType = DbType.AnsiString;
				colvarBusinessHourId.MaxLength = 20;
				colvarBusinessHourId.AutoIncrement = false;
				colvarBusinessHourId.IsNullable = true;
				colvarBusinessHourId.IsPrimaryKey = false;
				colvarBusinessHourId.IsForeignKey = false;
				colvarBusinessHourId.IsReadOnly = false;
				colvarBusinessHourId.DefaultSetting = @"";
				colvarBusinessHourId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourId);

				TableSchema.TableColumn colvarBusinessHourTypeId = new TableSchema.TableColumn(schema);
				colvarBusinessHourTypeId.ColumnName = "business_hour_type_id";
				colvarBusinessHourTypeId.DataType = DbType.Int32;
				colvarBusinessHourTypeId.MaxLength = 0;
				colvarBusinessHourTypeId.AutoIncrement = false;
				colvarBusinessHourTypeId.IsNullable = false;
				colvarBusinessHourTypeId.IsPrimaryKey = false;
				colvarBusinessHourTypeId.IsForeignKey = false;
				colvarBusinessHourTypeId.IsReadOnly = false;
				colvarBusinessHourTypeId.DefaultSetting = @"";
				colvarBusinessHourTypeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourTypeId);

				TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
				colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeS.MaxLength = 0;
				colvarBusinessHourOrderTimeS.AutoIncrement = false;
				colvarBusinessHourOrderTimeS.IsNullable = false;
				colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeS.IsForeignKey = false;
				colvarBusinessHourOrderTimeS.IsReadOnly = false;
				colvarBusinessHourOrderTimeS.DefaultSetting = @"";
				colvarBusinessHourOrderTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeS);

				TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
				colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeE.MaxLength = 0;
				colvarBusinessHourOrderTimeE.AutoIncrement = false;
				colvarBusinessHourOrderTimeE.IsNullable = false;
				colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeE.IsForeignKey = false;
				colvarBusinessHourOrderTimeE.IsReadOnly = false;
				colvarBusinessHourOrderTimeE.DefaultSetting = @"";
				colvarBusinessHourOrderTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeE);

				TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
				colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeS.MaxLength = 0;
				colvarBusinessHourDeliverTimeS.AutoIncrement = false;
				colvarBusinessHourDeliverTimeS.IsNullable = true;
				colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeS.IsForeignKey = false;
				colvarBusinessHourDeliverTimeS.IsReadOnly = false;
				colvarBusinessHourDeliverTimeS.DefaultSetting = @"";
				colvarBusinessHourDeliverTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeS);

				TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
				colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeE.MaxLength = 0;
				colvarBusinessHourDeliverTimeE.AutoIncrement = false;
				colvarBusinessHourDeliverTimeE.IsNullable = true;
				colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeE.IsForeignKey = false;
				colvarBusinessHourDeliverTimeE.IsReadOnly = false;
				colvarBusinessHourDeliverTimeE.DefaultSetting = @"";
				colvarBusinessHourDeliverTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeE);

				TableSchema.TableColumn colvarBusinessHourDeliveryCharge = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliveryCharge.ColumnName = "business_hour_delivery_charge";
				colvarBusinessHourDeliveryCharge.DataType = DbType.Currency;
				colvarBusinessHourDeliveryCharge.MaxLength = 0;
				colvarBusinessHourDeliveryCharge.AutoIncrement = false;
				colvarBusinessHourDeliveryCharge.IsNullable = false;
				colvarBusinessHourDeliveryCharge.IsPrimaryKey = false;
				colvarBusinessHourDeliveryCharge.IsForeignKey = false;
				colvarBusinessHourDeliveryCharge.IsReadOnly = false;
				colvarBusinessHourDeliveryCharge.DefaultSetting = @"";
				colvarBusinessHourDeliveryCharge.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliveryCharge);

				TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
				colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
				colvarBusinessHourOrderMinimum.MaxLength = 0;
				colvarBusinessHourOrderMinimum.AutoIncrement = false;
				colvarBusinessHourOrderMinimum.IsNullable = false;
				colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
				colvarBusinessHourOrderMinimum.IsForeignKey = false;
				colvarBusinessHourOrderMinimum.IsReadOnly = false;
				colvarBusinessHourOrderMinimum.DefaultSetting = @"";
				colvarBusinessHourOrderMinimum.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderMinimum);

				TableSchema.TableColumn colvarBusinessHourPreparationTime = new TableSchema.TableColumn(schema);
				colvarBusinessHourPreparationTime.ColumnName = "business_hour_preparation_time";
				colvarBusinessHourPreparationTime.DataType = DbType.Int32;
				colvarBusinessHourPreparationTime.MaxLength = 0;
				colvarBusinessHourPreparationTime.AutoIncrement = false;
				colvarBusinessHourPreparationTime.IsNullable = false;
				colvarBusinessHourPreparationTime.IsPrimaryKey = false;
				colvarBusinessHourPreparationTime.IsForeignKey = false;
				colvarBusinessHourPreparationTime.IsReadOnly = false;
				colvarBusinessHourPreparationTime.DefaultSetting = @"";
				colvarBusinessHourPreparationTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourPreparationTime);

				TableSchema.TableColumn colvarBusinessHourOnline = new TableSchema.TableColumn(schema);
				colvarBusinessHourOnline.ColumnName = "business_hour_online";
				colvarBusinessHourOnline.DataType = DbType.Boolean;
				colvarBusinessHourOnline.MaxLength = 0;
				colvarBusinessHourOnline.AutoIncrement = false;
				colvarBusinessHourOnline.IsNullable = false;
				colvarBusinessHourOnline.IsPrimaryKey = false;
				colvarBusinessHourOnline.IsForeignKey = false;
				colvarBusinessHourOnline.IsReadOnly = false;
				colvarBusinessHourOnline.DefaultSetting = @"";
				colvarBusinessHourOnline.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOnline);

				TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
				colvarBusinessHourStatus.ColumnName = "business_hour_status";
				colvarBusinessHourStatus.DataType = DbType.Int32;
				colvarBusinessHourStatus.MaxLength = 0;
				colvarBusinessHourStatus.AutoIncrement = false;
				colvarBusinessHourStatus.IsNullable = false;
				colvarBusinessHourStatus.IsPrimaryKey = false;
				colvarBusinessHourStatus.IsForeignKey = false;
				colvarBusinessHourStatus.IsReadOnly = false;
				colvarBusinessHourStatus.DefaultSetting = @"";
				colvarBusinessHourStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourStatus);

				TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
				colvarOrderTotalLimit.ColumnName = "order_total_limit";
				colvarOrderTotalLimit.DataType = DbType.Currency;
				colvarOrderTotalLimit.MaxLength = 0;
				colvarOrderTotalLimit.AutoIncrement = false;
				colvarOrderTotalLimit.IsNullable = true;
				colvarOrderTotalLimit.IsPrimaryKey = false;
				colvarOrderTotalLimit.IsForeignKey = false;
				colvarOrderTotalLimit.IsReadOnly = false;
				colvarOrderTotalLimit.DefaultSetting = @"";
				colvarOrderTotalLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderTotalLimit);

				TableSchema.TableColumn colvarDeliveryLimit = new TableSchema.TableColumn(schema);
				colvarDeliveryLimit.ColumnName = "delivery_limit";
				colvarDeliveryLimit.DataType = DbType.Int32;
				colvarDeliveryLimit.MaxLength = 0;
				colvarDeliveryLimit.AutoIncrement = false;
				colvarDeliveryLimit.IsNullable = true;
				colvarDeliveryLimit.IsPrimaryKey = false;
				colvarDeliveryLimit.IsForeignKey = false;
				colvarDeliveryLimit.IsReadOnly = false;
				colvarDeliveryLimit.DefaultSetting = @"";
				colvarDeliveryLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryLimit);

				TableSchema.TableColumn colvarHoliday = new TableSchema.TableColumn(schema);
				colvarHoliday.ColumnName = "holiday";
				colvarHoliday.DataType = DbType.Int32;
				colvarHoliday.MaxLength = 0;
				colvarHoliday.AutoIncrement = false;
				colvarHoliday.IsNullable = false;
				colvarHoliday.IsPrimaryKey = false;
				colvarHoliday.IsForeignKey = false;
				colvarHoliday.IsReadOnly = false;
				colvarHoliday.DefaultSetting = @"";
				colvarHoliday.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHoliday);

				TableSchema.TableColumn colvarBusinessHourCreateId = new TableSchema.TableColumn(schema);
				colvarBusinessHourCreateId.ColumnName = "business_hour_create_id";
				colvarBusinessHourCreateId.DataType = DbType.String;
				colvarBusinessHourCreateId.MaxLength = 30;
				colvarBusinessHourCreateId.AutoIncrement = false;
				colvarBusinessHourCreateId.IsNullable = false;
				colvarBusinessHourCreateId.IsPrimaryKey = false;
				colvarBusinessHourCreateId.IsForeignKey = false;
				colvarBusinessHourCreateId.IsReadOnly = false;
				colvarBusinessHourCreateId.DefaultSetting = @"";
				colvarBusinessHourCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourCreateId);

				TableSchema.TableColumn colvarBusinessHourCreateTime = new TableSchema.TableColumn(schema);
				colvarBusinessHourCreateTime.ColumnName = "business_hour_create_time";
				colvarBusinessHourCreateTime.DataType = DbType.DateTime;
				colvarBusinessHourCreateTime.MaxLength = 0;
				colvarBusinessHourCreateTime.AutoIncrement = false;
				colvarBusinessHourCreateTime.IsNullable = false;
				colvarBusinessHourCreateTime.IsPrimaryKey = false;
				colvarBusinessHourCreateTime.IsForeignKey = false;
				colvarBusinessHourCreateTime.IsReadOnly = false;
				colvarBusinessHourCreateTime.DefaultSetting = @"";
				colvarBusinessHourCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourCreateTime);

				TableSchema.TableColumn colvarBusinessHourModifyTime = new TableSchema.TableColumn(schema);
				colvarBusinessHourModifyTime.ColumnName = "business_hour_modify_time";
				colvarBusinessHourModifyTime.DataType = DbType.DateTime;
				colvarBusinessHourModifyTime.MaxLength = 0;
				colvarBusinessHourModifyTime.AutoIncrement = false;
				colvarBusinessHourModifyTime.IsNullable = true;
				colvarBusinessHourModifyTime.IsPrimaryKey = false;
				colvarBusinessHourModifyTime.IsForeignKey = false;
				colvarBusinessHourModifyTime.IsReadOnly = false;
				colvarBusinessHourModifyTime.DefaultSetting = @"";
				colvarBusinessHourModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourModifyTime);

				TableSchema.TableColumn colvarBusinessHourModifyId = new TableSchema.TableColumn(schema);
				colvarBusinessHourModifyId.ColumnName = "business_hour_modify_id";
				colvarBusinessHourModifyId.DataType = DbType.String;
				colvarBusinessHourModifyId.MaxLength = 30;
				colvarBusinessHourModifyId.AutoIncrement = false;
				colvarBusinessHourModifyId.IsNullable = true;
				colvarBusinessHourModifyId.IsPrimaryKey = false;
				colvarBusinessHourModifyId.IsForeignKey = false;
				colvarBusinessHourModifyId.IsReadOnly = false;
				colvarBusinessHourModifyId.DefaultSetting = @"";
				colvarBusinessHourModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourModifyId);

				TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
				colvarDepartment.ColumnName = "department";
				colvarDepartment.DataType = DbType.Int32;
				colvarDepartment.MaxLength = 0;
				colvarDepartment.AutoIncrement = false;
				colvarDepartment.IsNullable = false;
				colvarDepartment.IsPrimaryKey = false;
				colvarDepartment.IsForeignKey = false;
				colvarDepartment.IsReadOnly = false;
				colvarDepartment.DefaultSetting = @"";
				colvarDepartment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDepartment);

				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = true;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_member_group_order",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("UserName")]
		[Bindable(true)]
		public string UserName
		{
			get { return GetColumnValue<string>(Columns.UserName); }
			set { SetColumnValue(Columns.UserName, value); }
		}

		[XmlAttribute("LastName")]
		[Bindable(true)]
		public string LastName
		{
			get { return GetColumnValue<string>(Columns.LastName); }
			set { SetColumnValue(Columns.LastName, value); }
		}

		[XmlAttribute("FirstName")]
		[Bindable(true)]
		public string FirstName
		{
			get { return GetColumnValue<string>(Columns.FirstName); }
			set { SetColumnValue(Columns.FirstName, value); }
		}

		[XmlAttribute("BuildingGuid")]
		[Bindable(true)]
		public Guid? BuildingGuid
		{
			get { return GetColumnValue<Guid?>(Columns.BuildingGuid); }
			set { SetColumnValue(Columns.BuildingGuid, value); }
		}

		[XmlAttribute("Birthday")]
		[Bindable(true)]
		public DateTime? Birthday
		{
			get { return GetColumnValue<DateTime?>(Columns.Birthday); }
			set { SetColumnValue(Columns.Birthday, value); }
		}

		[XmlAttribute("Gender")]
		[Bindable(true)]
		public int? Gender
		{
			get { return GetColumnValue<int?>(Columns.Gender); }
			set { SetColumnValue(Columns.Gender, value); }
		}

		[XmlAttribute("Mobile")]
		[Bindable(true)]
		public string Mobile
		{
			get { return GetColumnValue<string>(Columns.Mobile); }
			set { SetColumnValue(Columns.Mobile, value); }
		}

		[XmlAttribute("CompanyName")]
		[Bindable(true)]
		public string CompanyName
		{
			get { return GetColumnValue<string>(Columns.CompanyName); }
			set { SetColumnValue(Columns.CompanyName, value); }
		}

		[XmlAttribute("CompanyDepartment")]
		[Bindable(true)]
		public string CompanyDepartment
		{
			get { return GetColumnValue<string>(Columns.CompanyDepartment); }
			set { SetColumnValue(Columns.CompanyDepartment, value); }
		}

		[XmlAttribute("CompanyTel")]
		[Bindable(true)]
		public string CompanyTel
		{
			get { return GetColumnValue<string>(Columns.CompanyTel); }
			set { SetColumnValue(Columns.CompanyTel, value); }
		}

		[XmlAttribute("CompanyTelExt")]
		[Bindable(true)]
		public string CompanyTelExt
		{
			get { return GetColumnValue<string>(Columns.CompanyTelExt); }
			set { SetColumnValue(Columns.CompanyTelExt, value); }
		}

		[XmlAttribute("CompanyAddress")]
		[Bindable(true)]
		public string CompanyAddress
		{
			get { return GetColumnValue<string>(Columns.CompanyAddress); }
			set { SetColumnValue(Columns.CompanyAddress, value); }
		}

		[XmlAttribute("DeliveryMethod")]
		[Bindable(true)]
		public string DeliveryMethod
		{
			get { return GetColumnValue<string>(Columns.DeliveryMethod); }
			set { SetColumnValue(Columns.DeliveryMethod, value); }
		}

		[XmlAttribute("PrimaryContactMethod")]
		[Bindable(true)]
		public int PrimaryContactMethod
		{
			get { return GetColumnValue<int>(Columns.PrimaryContactMethod); }
			set { SetColumnValue(Columns.PrimaryContactMethod, value); }
		}

		[XmlAttribute("MemberStatus")]
		[Bindable(true)]
		public int MemberStatus
		{
			get { return GetColumnValue<int>(Columns.MemberStatus); }
			set { SetColumnValue(Columns.MemberStatus, value); }
		}

		[XmlAttribute("MemberCreateTime")]
		[Bindable(true)]
		public DateTime MemberCreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.MemberCreateTime); }
			set { SetColumnValue(Columns.MemberCreateTime, value); }
		}

		[XmlAttribute("MemberModifyTime")]
		[Bindable(true)]
		public DateTime? MemberModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.MemberModifyTime); }
			set { SetColumnValue(Columns.MemberModifyTime, value); }
		}

		[XmlAttribute("MemberModifyId")]
		[Bindable(true)]
		public string MemberModifyId
		{
			get { return GetColumnValue<string>(Columns.MemberModifyId); }
			set { SetColumnValue(Columns.MemberModifyId, value); }
		}

		[XmlAttribute("Comment")]
		[Bindable(true)]
		public string Comment
		{
			get { return GetColumnValue<string>(Columns.Comment); }
			set { SetColumnValue(Columns.Comment, value); }
		}

		[XmlAttribute("GroupOrderGuid")]
		[Bindable(true)]
		public Guid GroupOrderGuid
		{
			get { return GetColumnValue<Guid>(Columns.GroupOrderGuid); }
			set { SetColumnValue(Columns.GroupOrderGuid, value); }
		}

		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}

		[XmlAttribute("GroupOrderCreateTime")]
		[Bindable(true)]
		public DateTime GroupOrderCreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.GroupOrderCreateTime); }
			set { SetColumnValue(Columns.GroupOrderCreateTime, value); }
		}

		[XmlAttribute("GroupOrderCloseTime")]
		[Bindable(true)]
		public DateTime GroupOrderCloseTime
		{
			get { return GetColumnValue<DateTime>(Columns.GroupOrderCloseTime); }
			set { SetColumnValue(Columns.GroupOrderCloseTime, value); }
		}

		[XmlAttribute("GroupOrderStatus")]
		[Bindable(true)]
		public int? GroupOrderStatus
		{
			get { return GetColumnValue<int?>(Columns.GroupOrderStatus); }
			set { SetColumnValue(Columns.GroupOrderStatus, value); }
		}

		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}

		[XmlAttribute("MemberName")]
		[Bindable(true)]
		public string MemberName
		{
			get { return GetColumnValue<string>(Columns.MemberName); }
			set { SetColumnValue(Columns.MemberName, value); }
		}

		[XmlAttribute("PhoneNumber")]
		[Bindable(true)]
		public string PhoneNumber
		{
			get { return GetColumnValue<string>(Columns.PhoneNumber); }
			set { SetColumnValue(Columns.PhoneNumber, value); }
		}

		[XmlAttribute("MobileNumber")]
		[Bindable(true)]
		public string MobileNumber
		{
			get { return GetColumnValue<string>(Columns.MobileNumber); }
			set { SetColumnValue(Columns.MobileNumber, value); }
		}

		[XmlAttribute("DeliveryAddress")]
		[Bindable(true)]
		public string DeliveryAddress
		{
			get { return GetColumnValue<string>(Columns.DeliveryAddress); }
			set { SetColumnValue(Columns.DeliveryAddress, value); }
		}

		[XmlAttribute("DeliveryTime")]
		[Bindable(true)]
		public DateTime? DeliveryTime
		{
			get { return GetColumnValue<DateTime?>(Columns.DeliveryTime); }
			set { SetColumnValue(Columns.DeliveryTime, value); }
		}

		[XmlAttribute("Subtotal")]
		[Bindable(true)]
		public decimal Subtotal
		{
			get { return GetColumnValue<decimal>(Columns.Subtotal); }
			set { SetColumnValue(Columns.Subtotal, value); }
		}

		[XmlAttribute("Total")]
		[Bindable(true)]
		public decimal Total
		{
			get { return GetColumnValue<decimal>(Columns.Total); }
			set { SetColumnValue(Columns.Total, value); }
		}

		[XmlAttribute("UserMemo")]
		[Bindable(true)]
		public string UserMemo
		{
			get { return GetColumnValue<string>(Columns.UserMemo); }
			set { SetColumnValue(Columns.UserMemo, value); }
		}

		[XmlAttribute("OrderMemo")]
		[Bindable(true)]
		public string OrderMemo
		{
			get { return GetColumnValue<string>(Columns.OrderMemo); }
			set { SetColumnValue(Columns.OrderMemo, value); }
		}

		[XmlAttribute("OrderStatus")]
		[Bindable(true)]
		public int OrderStatus
		{
			get { return GetColumnValue<int>(Columns.OrderStatus); }
			set { SetColumnValue(Columns.OrderStatus, value); }
		}

		[XmlAttribute("OrderCreateId")]
		[Bindable(true)]
		public string OrderCreateId
		{
			get { return GetColumnValue<string>(Columns.OrderCreateId); }
			set { SetColumnValue(Columns.OrderCreateId, value); }
		}

		[XmlAttribute("OrderCreateTime")]
		[Bindable(true)]
		public DateTime OrderCreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.OrderCreateTime); }
			set { SetColumnValue(Columns.OrderCreateTime, value); }
		}

		[XmlAttribute("OrderModifyId")]
		[Bindable(true)]
		public string OrderModifyId
		{
			get { return GetColumnValue<string>(Columns.OrderModifyId); }
			set { SetColumnValue(Columns.OrderModifyId, value); }
		}

		[XmlAttribute("OrderModifyTime")]
		[Bindable(true)]
		public DateTime? OrderModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.OrderModifyTime); }
			set { SetColumnValue(Columns.OrderModifyTime, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("OrderAccessLock")]
		[Bindable(true)]
		public string OrderAccessLock
		{
			get { return GetColumnValue<string>(Columns.OrderAccessLock); }
			set { SetColumnValue(Columns.OrderAccessLock, value); }
		}

		[XmlAttribute("OrderStage")]
		[Bindable(true)]
		public int OrderStage
		{
			get { return GetColumnValue<int>(Columns.OrderStage); }
			set { SetColumnValue(Columns.OrderStage, value); }
		}

		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}

		[XmlAttribute("BusinessHourId")]
		[Bindable(true)]
		public string BusinessHourId
		{
			get { return GetColumnValue<string>(Columns.BusinessHourId); }
			set { SetColumnValue(Columns.BusinessHourId, value); }
		}

		[XmlAttribute("BusinessHourTypeId")]
		[Bindable(true)]
		public int BusinessHourTypeId
		{
			get { return GetColumnValue<int>(Columns.BusinessHourTypeId); }
			set { SetColumnValue(Columns.BusinessHourTypeId, value); }
		}

		[XmlAttribute("BusinessHourOrderTimeS")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeS
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeS); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeS, value); }
		}

		[XmlAttribute("BusinessHourOrderTimeE")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeE
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeE); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeE, value); }
		}

		[XmlAttribute("BusinessHourDeliverTimeS")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeS
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeS); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeS, value); }
		}

		[XmlAttribute("BusinessHourDeliverTimeE")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeE
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeE); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeE, value); }
		}

		[XmlAttribute("BusinessHourDeliveryCharge")]
		[Bindable(true)]
		public decimal BusinessHourDeliveryCharge
		{
			get { return GetColumnValue<decimal>(Columns.BusinessHourDeliveryCharge); }
			set { SetColumnValue(Columns.BusinessHourDeliveryCharge, value); }
		}

		[XmlAttribute("BusinessHourOrderMinimum")]
		[Bindable(true)]
		public decimal BusinessHourOrderMinimum
		{
			get { return GetColumnValue<decimal>(Columns.BusinessHourOrderMinimum); }
			set { SetColumnValue(Columns.BusinessHourOrderMinimum, value); }
		}

		[XmlAttribute("BusinessHourPreparationTime")]
		[Bindable(true)]
		public int BusinessHourPreparationTime
		{
			get { return GetColumnValue<int>(Columns.BusinessHourPreparationTime); }
			set { SetColumnValue(Columns.BusinessHourPreparationTime, value); }
		}

		[XmlAttribute("BusinessHourOnline")]
		[Bindable(true)]
		public bool BusinessHourOnline
		{
			get { return GetColumnValue<bool>(Columns.BusinessHourOnline); }
			set { SetColumnValue(Columns.BusinessHourOnline, value); }
		}

		[XmlAttribute("BusinessHourStatus")]
		[Bindable(true)]
		public int BusinessHourStatus
		{
			get { return GetColumnValue<int>(Columns.BusinessHourStatus); }
			set { SetColumnValue(Columns.BusinessHourStatus, value); }
		}

		[XmlAttribute("OrderTotalLimit")]
		[Bindable(true)]
		public decimal? OrderTotalLimit
		{
			get { return GetColumnValue<decimal?>(Columns.OrderTotalLimit); }
			set { SetColumnValue(Columns.OrderTotalLimit, value); }
		}

		[XmlAttribute("DeliveryLimit")]
		[Bindable(true)]
		public int? DeliveryLimit
		{
			get { return GetColumnValue<int?>(Columns.DeliveryLimit); }
			set { SetColumnValue(Columns.DeliveryLimit, value); }
		}

		[XmlAttribute("Holiday")]
		[Bindable(true)]
		public int Holiday
		{
			get { return GetColumnValue<int>(Columns.Holiday); }
			set { SetColumnValue(Columns.Holiday, value); }
		}

		[XmlAttribute("BusinessHourCreateId")]
		[Bindable(true)]
		public string BusinessHourCreateId
		{
			get { return GetColumnValue<string>(Columns.BusinessHourCreateId); }
			set { SetColumnValue(Columns.BusinessHourCreateId, value); }
		}

		[XmlAttribute("BusinessHourCreateTime")]
		[Bindable(true)]
		public DateTime BusinessHourCreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourCreateTime); }
			set { SetColumnValue(Columns.BusinessHourCreateTime, value); }
		}

		[XmlAttribute("BusinessHourModifyTime")]
		[Bindable(true)]
		public DateTime? BusinessHourModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourModifyTime); }
			set { SetColumnValue(Columns.BusinessHourModifyTime, value); }
		}

		[XmlAttribute("BusinessHourModifyId")]
		[Bindable(true)]
		public string BusinessHourModifyId
		{
			get { return GetColumnValue<string>(Columns.BusinessHourModifyId); }
			set { SetColumnValue(Columns.BusinessHourModifyId, value); }
		}

		[XmlAttribute("Department")]
		[Bindable(true)]
		public int Department
		{
			get { return GetColumnValue<int>(Columns.Department); }
			set { SetColumnValue(Columns.Department, value); }
		}

		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int? CityId
		{
			get { return GetColumnValue<int?>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn UserNameColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn LastNameColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn FirstNameColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn BuildingGuidColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn BirthdayColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn GenderColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn MobileColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn CompanyNameColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn CompanyDepartmentColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn CompanyTelColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn CompanyTelExtColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn CompanyAddressColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn DeliveryMethodColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn PrimaryContactMethodColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn MemberStatusColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn MemberCreateTimeColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn MemberModifyTimeColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn MemberModifyIdColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn CommentColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn GroupOrderGuidColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn BusinessHourGuidColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn GroupOrderCreateTimeColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn GroupOrderCloseTimeColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn GroupOrderStatusColumn
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn OrderIdColumn
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn SellerNameColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn MemberNameColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn PhoneNumberColumn
		{
			get { return Schema.Columns[28]; }
		}

		public static TableSchema.TableColumn MobileNumberColumn
		{
			get { return Schema.Columns[29]; }
		}

		public static TableSchema.TableColumn DeliveryAddressColumn
		{
			get { return Schema.Columns[30]; }
		}

		public static TableSchema.TableColumn DeliveryTimeColumn
		{
			get { return Schema.Columns[31]; }
		}

		public static TableSchema.TableColumn SubtotalColumn
		{
			get { return Schema.Columns[32]; }
		}

		public static TableSchema.TableColumn TotalColumn
		{
			get { return Schema.Columns[33]; }
		}

		public static TableSchema.TableColumn UserMemoColumn
		{
			get { return Schema.Columns[34]; }
		}

		public static TableSchema.TableColumn OrderMemoColumn
		{
			get { return Schema.Columns[35]; }
		}

		public static TableSchema.TableColumn OrderStatusColumn
		{
			get { return Schema.Columns[36]; }
		}

		public static TableSchema.TableColumn OrderCreateIdColumn
		{
			get { return Schema.Columns[37]; }
		}

		public static TableSchema.TableColumn OrderCreateTimeColumn
		{
			get { return Schema.Columns[38]; }
		}

		public static TableSchema.TableColumn OrderModifyIdColumn
		{
			get { return Schema.Columns[39]; }
		}

		public static TableSchema.TableColumn OrderModifyTimeColumn
		{
			get { return Schema.Columns[40]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[41]; }
		}

		public static TableSchema.TableColumn OrderAccessLockColumn
		{
			get { return Schema.Columns[42]; }
		}

		public static TableSchema.TableColumn OrderStageColumn
		{
			get { return Schema.Columns[43]; }
		}

		public static TableSchema.TableColumn GuidColumn
		{
			get { return Schema.Columns[44]; }
		}

		public static TableSchema.TableColumn BusinessHourIdColumn
		{
			get { return Schema.Columns[45]; }
		}

		public static TableSchema.TableColumn BusinessHourTypeIdColumn
		{
			get { return Schema.Columns[46]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderTimeSColumn
		{
			get { return Schema.Columns[47]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderTimeEColumn
		{
			get { return Schema.Columns[48]; }
		}

		public static TableSchema.TableColumn BusinessHourDeliverTimeSColumn
		{
			get { return Schema.Columns[49]; }
		}

		public static TableSchema.TableColumn BusinessHourDeliverTimeEColumn
		{
			get { return Schema.Columns[50]; }
		}

		public static TableSchema.TableColumn BusinessHourDeliveryChargeColumn
		{
			get { return Schema.Columns[51]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderMinimumColumn
		{
			get { return Schema.Columns[52]; }
		}

		public static TableSchema.TableColumn BusinessHourPreparationTimeColumn
		{
			get { return Schema.Columns[53]; }
		}

		public static TableSchema.TableColumn BusinessHourOnlineColumn
		{
			get { return Schema.Columns[54]; }
		}

		public static TableSchema.TableColumn BusinessHourStatusColumn
		{
			get { return Schema.Columns[55]; }
		}

		public static TableSchema.TableColumn OrderTotalLimitColumn
		{
			get { return Schema.Columns[56]; }
		}

		public static TableSchema.TableColumn DeliveryLimitColumn
		{
			get { return Schema.Columns[57]; }
		}

		public static TableSchema.TableColumn HolidayColumn
		{
			get { return Schema.Columns[58]; }
		}

		public static TableSchema.TableColumn BusinessHourCreateIdColumn
		{
			get { return Schema.Columns[59]; }
		}

		public static TableSchema.TableColumn BusinessHourCreateTimeColumn
		{
			get { return Schema.Columns[60]; }
		}

		public static TableSchema.TableColumn BusinessHourModifyTimeColumn
		{
			get { return Schema.Columns[61]; }
		}

		public static TableSchema.TableColumn BusinessHourModifyIdColumn
		{
			get { return Schema.Columns[62]; }
		}

		public static TableSchema.TableColumn DepartmentColumn
		{
			get { return Schema.Columns[63]; }
		}

		public static TableSchema.TableColumn CityIdColumn
		{
			get { return Schema.Columns[64]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string UserName = @"user_name";
			public static string LastName = @"last_name";
			public static string FirstName = @"first_name";
			public static string BuildingGuid = @"building_GUID";
			public static string Birthday = @"birthday";
			public static string Gender = @"gender";
			public static string Mobile = @"mobile";
			public static string CompanyName = @"company_name";
			public static string CompanyDepartment = @"company_department";
			public static string CompanyTel = @"company_tel";
			public static string CompanyTelExt = @"company_tel_ext";
			public static string CompanyAddress = @"company_address";
			public static string DeliveryMethod = @"delivery_method";
			public static string PrimaryContactMethod = @"primary_contact_method";
			public static string MemberStatus = @"member_status";
			public static string MemberCreateTime = @"member_create_time";
			public static string MemberModifyTime = @"member_modify_time";
			public static string MemberModifyId = @"member_modify_id";
			public static string Comment = @"comment";
			public static string GroupOrderGuid = @"group_order_guid";
			public static string BusinessHourGuid = @"business_hour_guid";
			public static string GroupOrderCreateTime = @"group_order_create_time";
			public static string GroupOrderCloseTime = @"group_order_close_time";
			public static string GroupOrderStatus = @"group_order_status";
			public static string OrderId = @"order_id";
			public static string SellerGuid = @"seller_GUID";
			public static string SellerName = @"seller_name";
			public static string MemberName = @"member_name";
			public static string PhoneNumber = @"phone_number";
			public static string MobileNumber = @"mobile_number";
			public static string DeliveryAddress = @"delivery_address";
			public static string DeliveryTime = @"delivery_time";
			public static string Subtotal = @"subtotal";
			public static string Total = @"total";
			public static string UserMemo = @"user_memo";
			public static string OrderMemo = @"order_memo";
			public static string OrderStatus = @"order_status";
			public static string OrderCreateId = @"order_create_id";
			public static string OrderCreateTime = @"order_create_time";
			public static string OrderModifyId = @"order_modify_id";
			public static string OrderModifyTime = @"order_modify_time";
			public static string OrderGuid = @"order_guid";
			public static string OrderAccessLock = @"order_access_lock";
			public static string OrderStage = @"order_stage";
			public static string Guid = @"GUID";
			public static string BusinessHourId = @"business_hour_id";
			public static string BusinessHourTypeId = @"business_hour_type_id";
			public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
			public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
			public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
			public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
			public static string BusinessHourDeliveryCharge = @"business_hour_delivery_charge";
			public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
			public static string BusinessHourPreparationTime = @"business_hour_preparation_time";
			public static string BusinessHourOnline = @"business_hour_online";
			public static string BusinessHourStatus = @"business_hour_status";
			public static string OrderTotalLimit = @"order_total_limit";
			public static string DeliveryLimit = @"delivery_limit";
			public static string Holiday = @"holiday";
			public static string BusinessHourCreateId = @"business_hour_create_id";
			public static string BusinessHourCreateTime = @"business_hour_create_time";
			public static string BusinessHourModifyTime = @"business_hour_modify_time";
			public static string BusinessHourModifyId = @"business_hour_modify_id";
			public static string Department = @"department";
			public static string CityId = @"city_id";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

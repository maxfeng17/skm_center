using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewMgmGiftBacklist class.
    /// </summary>
    [Serializable]
    public partial class ViewMgmGiftBacklistCollection : ReadOnlyList<ViewMgmGiftBacklist, ViewMgmGiftBacklistCollection>
    {
        public ViewMgmGiftBacklistCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_mgm_gift_backlist view.
    /// </summary>
    [Serializable]
    public partial class ViewMgmGiftBacklist : ReadOnlyRecord<ViewMgmGiftBacklist>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_mgm_gift_backlist", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarAccount = new TableSchema.TableColumn(schema);
                colvarAccount.ColumnName = "account";
                colvarAccount.DataType = DbType.String;
                colvarAccount.MaxLength = 256;
                colvarAccount.AutoIncrement = false;
                colvarAccount.IsNullable = false;
                colvarAccount.IsPrimaryKey = false;
                colvarAccount.IsForeignKey = false;
                colvarAccount.IsReadOnly = false;

                schema.Columns.Add(colvarAccount);

                TableSchema.TableColumn colvarSenderId = new TableSchema.TableColumn(schema);
                colvarSenderId.ColumnName = "sender_id";
                colvarSenderId.DataType = DbType.Int32;
                colvarSenderId.MaxLength = 0;
                colvarSenderId.AutoIncrement = false;
                colvarSenderId.IsNullable = false;
                colvarSenderId.IsPrimaryKey = false;
                colvarSenderId.IsForeignKey = false;
                colvarSenderId.IsReadOnly = false;

                schema.Columns.Add(colvarSenderId);

                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "Name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 100;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;

                schema.Columns.Add(colvarName);

                TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
                colvarSequenceNumber.ColumnName = "sequence_number";
                colvarSequenceNumber.DataType = DbType.AnsiString;
                colvarSequenceNumber.MaxLength = 5;
                colvarSequenceNumber.AutoIncrement = false;
                colvarSequenceNumber.IsNullable = false;
                colvarSequenceNumber.IsPrimaryKey = false;
                colvarSequenceNumber.IsForeignKey = false;
                colvarSequenceNumber.IsReadOnly = false;

                schema.Columns.Add(colvarSequenceNumber);

                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = -1;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = false;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;

                schema.Columns.Add(colvarTitle);

                TableSchema.TableColumn colvarCount = new TableSchema.TableColumn(schema);
                colvarCount.ColumnName = "Count";
                colvarCount.DataType = DbType.Int32;
                colvarCount.MaxLength = 0;
                colvarCount.AutoIncrement = false;
                colvarCount.IsNullable = true;
                colvarCount.IsPrimaryKey = false;
                colvarCount.IsForeignKey = false;
                colvarCount.IsReadOnly = false;

                schema.Columns.Add(colvarCount);

                TableSchema.TableColumn colvarSendTime = new TableSchema.TableColumn(schema);
                colvarSendTime.ColumnName = "send_time";
                colvarSendTime.DataType = DbType.DateTime;
                colvarSendTime.MaxLength = 0;
                colvarSendTime.AutoIncrement = false;
                colvarSendTime.IsNullable = false;
                colvarSendTime.IsPrimaryKey = false;
                colvarSendTime.IsForeignKey = false;
                colvarSendTime.IsReadOnly = false;

                schema.Columns.Add(colvarSendTime);

                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;

                schema.Columns.Add(colvarOrderId);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;

                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;

                schema.Columns.Add(colvarMobile);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_mgm_gift_backlist", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewMgmGiftBacklist()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMgmGiftBacklist(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewMgmGiftBacklist(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewMgmGiftBacklist(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Account")]
        [Bindable(true)]
        public string Account
        {
            get
            {
                return GetColumnValue<string>("account");
            }
            set
            {
                SetColumnValue("account", value);
            }
        }

        [XmlAttribute("SenderId")]
        [Bindable(true)]
        public int SenderId
        {
            get
            {
                return GetColumnValue<int>("sender_id");
            }
            set
            {
                SetColumnValue("sender_id", value);
            }
        }

        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name
        {
            get
            {
                return GetColumnValue<string>("Name");
            }
            set
            {
                SetColumnValue("Name", value);
            }
        }

        [XmlAttribute("SequenceNumber")]
        [Bindable(true)]
        public string SequenceNumber
        {
            get
            {
                return GetColumnValue<string>("sequence_number");
            }
            set
            {
                SetColumnValue("sequence_number", value);
            }
        }

        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title
        {
            get
            {
                return GetColumnValue<string>("title");
            }
            set
            {
                SetColumnValue("title", value);
            }
        }

        [XmlAttribute("Count")]
        [Bindable(true)]
        public int? Count
        {
            get
            {
                return GetColumnValue<int?>("Count");
            }
            set
            {
                SetColumnValue("Count", value);
            }
        }

        [XmlAttribute("SendTime")]
        [Bindable(true)]
        public DateTime SendTime
        {
            get
            {
                return GetColumnValue<DateTime>("send_time");
            }
            set
            {
                SetColumnValue("send_time", value);
            }
        }

        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId
        {
            get
            {
                return GetColumnValue<string>("order_id");
            }
            set
            {
                SetColumnValue("order_id", value);
            }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get
            {
                return GetColumnValue<Guid>("GUID");
            }
            set
            {
                SetColumnValue("GUID", value);
            }
        }

        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile
        {
            get
            {
                return GetColumnValue<string>("mobile");
            }
            set
            {
                SetColumnValue("mobile", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Account = @"account";

            public static string SenderId = @"sender_id";

            public static string Name = @"Name";

            public static string SequenceNumber = @"sequence_number";

            public static string Title = @"title";

            public static string Count = @"Count";

            public static string SendTime = @"send_time";

            public static string OrderId = @"order_id";

            public static string Guid = @"GUID";

            public static string Mobile = @"mobile";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

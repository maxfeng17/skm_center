using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpUserPoint class.
	/// </summary>
    [Serializable]
	public partial class PcpUserPointCollection : RepositoryList<PcpUserPoint, PcpUserPointCollection>
	{	   
		public PcpUserPointCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpUserPointCollection</returns>
		public PcpUserPointCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpUserPoint o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_user_point table.
	/// </summary>
	[Serializable]
	public partial class PcpUserPoint : RepositoryRecord<PcpUserPoint>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpUserPoint()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpUserPoint(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_user_point", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
				colvarCardGroupId.ColumnName = "card_group_id";
				colvarCardGroupId.DataType = DbType.Int32;
				colvarCardGroupId.MaxLength = 0;
				colvarCardGroupId.AutoIncrement = false;
				colvarCardGroupId.IsNullable = false;
				colvarCardGroupId.IsPrimaryKey = false;
				colvarCardGroupId.IsForeignKey = false;
				colvarCardGroupId.IsReadOnly = false;
				colvarCardGroupId.DefaultSetting = @"";
				colvarCardGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardGroupId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarTotalPoint = new TableSchema.TableColumn(schema);
				colvarTotalPoint.ColumnName = "total_point";
				colvarTotalPoint.DataType = DbType.Int32;
				colvarTotalPoint.MaxLength = 0;
				colvarTotalPoint.AutoIncrement = false;
				colvarTotalPoint.IsNullable = false;
				colvarTotalPoint.IsPrimaryKey = false;
				colvarTotalPoint.IsForeignKey = false;
				colvarTotalPoint.IsReadOnly = false;
				colvarTotalPoint.DefaultSetting = @"";
				colvarTotalPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalPoint);
				
				TableSchema.TableColumn colvarRemainPoint = new TableSchema.TableColumn(schema);
				colvarRemainPoint.ColumnName = "remain_point";
				colvarRemainPoint.DataType = DbType.Int32;
				colvarRemainPoint.MaxLength = 0;
				colvarRemainPoint.AutoIncrement = false;
				colvarRemainPoint.IsNullable = false;
				colvarRemainPoint.IsPrimaryKey = false;
				colvarRemainPoint.IsForeignKey = false;
				colvarRemainPoint.IsReadOnly = false;
				colvarRemainPoint.DefaultSetting = @"";
				colvarRemainPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemainPoint);
				
				TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
				colvarPrice.ColumnName = "price";
				colvarPrice.DataType = DbType.Currency;
				colvarPrice.MaxLength = 0;
				colvarPrice.AutoIncrement = false;
				colvarPrice.IsNullable = true;
				colvarPrice.IsPrimaryKey = false;
				colvarPrice.IsForeignKey = false;
				colvarPrice.IsReadOnly = false;
				colvarPrice.DefaultSetting = @"";
				colvarPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrice);
				
				TableSchema.TableColumn colvarSetType = new TableSchema.TableColumn(schema);
				colvarSetType.ColumnName = "set_type";
				colvarSetType.DataType = DbType.Int32;
				colvarSetType.MaxLength = 0;
				colvarSetType.AutoIncrement = false;
				colvarSetType.IsNullable = false;
				colvarSetType.IsPrimaryKey = false;
				colvarSetType.IsForeignKey = false;
				colvarSetType.IsReadOnly = false;
				colvarSetType.DefaultSetting = @"";
				colvarSetType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSetType);
				
				TableSchema.TableColumn colvarPointRuleId = new TableSchema.TableColumn(schema);
				colvarPointRuleId.ColumnName = "point_rule_id";
				colvarPointRuleId.DataType = DbType.Int32;
				colvarPointRuleId.MaxLength = 0;
				colvarPointRuleId.AutoIncrement = false;
				colvarPointRuleId.IsNullable = true;
				colvarPointRuleId.IsPrimaryKey = false;
				colvarPointRuleId.IsForeignKey = false;
				colvarPointRuleId.IsReadOnly = false;
				colvarPointRuleId.DefaultSetting = @"";
				colvarPointRuleId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPointRuleId);
				
				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 200;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = true;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);
				
				TableSchema.TableColumn colvarCount = new TableSchema.TableColumn(schema);
				colvarCount.ColumnName = "count";
				colvarCount.DataType = DbType.Int32;
				colvarCount.MaxLength = 0;
				colvarCount.AutoIncrement = false;
				colvarCount.IsNullable = false;
				colvarCount.IsPrimaryKey = false;
				colvarCount.IsForeignKey = false;
				colvarCount.IsReadOnly = false;
				colvarCount.DefaultSetting = @"";
				colvarCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCount);
				
				TableSchema.TableColumn colvarExpireTime = new TableSchema.TableColumn(schema);
				colvarExpireTime.ColumnName = "expire_time";
				colvarExpireTime.DataType = DbType.DateTime;
				colvarExpireTime.MaxLength = 0;
				colvarExpireTime.AutoIncrement = false;
				colvarExpireTime.IsNullable = true;
				colvarExpireTime.IsPrimaryKey = false;
				colvarExpireTime.IsForeignKey = false;
				colvarExpireTime.IsReadOnly = false;
				colvarExpireTime.DefaultSetting = @"";
				colvarExpireTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExpireTime);
				
				TableSchema.TableColumn colvarPcpOrderGuid = new TableSchema.TableColumn(schema);
				colvarPcpOrderGuid.ColumnName = "pcp_order_guid";
				colvarPcpOrderGuid.DataType = DbType.Guid;
				colvarPcpOrderGuid.MaxLength = 0;
				colvarPcpOrderGuid.AutoIncrement = false;
				colvarPcpOrderGuid.IsNullable = false;
				colvarPcpOrderGuid.IsPrimaryKey = false;
				colvarPcpOrderGuid.IsForeignKey = false;
				colvarPcpOrderGuid.IsReadOnly = false;
				colvarPcpOrderGuid.DefaultSetting = @"";
				colvarPcpOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPcpOrderGuid);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarFinalPoint = new TableSchema.TableColumn(schema);
				colvarFinalPoint.ColumnName = "final_point";
				colvarFinalPoint.DataType = DbType.Int32;
				colvarFinalPoint.MaxLength = 0;
				colvarFinalPoint.AutoIncrement = false;
				colvarFinalPoint.IsNullable = false;
				colvarFinalPoint.IsPrimaryKey = false;
				colvarFinalPoint.IsForeignKey = false;
				colvarFinalPoint.IsReadOnly = false;
				
						colvarFinalPoint.DefaultSetting = @"((0))";
				colvarFinalPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFinalPoint);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_user_point",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CardGroupId")]
		[Bindable(true)]
		public int CardGroupId 
		{
			get { return GetColumnValue<int>(Columns.CardGroupId); }
			set { SetColumnValue(Columns.CardGroupId, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("TotalPoint")]
		[Bindable(true)]
		public int TotalPoint 
		{
			get { return GetColumnValue<int>(Columns.TotalPoint); }
			set { SetColumnValue(Columns.TotalPoint, value); }
		}
		  
		[XmlAttribute("RemainPoint")]
		[Bindable(true)]
		public int RemainPoint 
		{
			get { return GetColumnValue<int>(Columns.RemainPoint); }
			set { SetColumnValue(Columns.RemainPoint, value); }
		}
		  
		[XmlAttribute("Price")]
		[Bindable(true)]
		public decimal? Price 
		{
			get { return GetColumnValue<decimal?>(Columns.Price); }
			set { SetColumnValue(Columns.Price, value); }
		}
		  
		[XmlAttribute("SetType")]
		[Bindable(true)]
		public int SetType 
		{
			get { return GetColumnValue<int>(Columns.SetType); }
			set { SetColumnValue(Columns.SetType, value); }
		}
		  
		[XmlAttribute("PointRuleId")]
		[Bindable(true)]
		public int? PointRuleId 
		{
			get { return GetColumnValue<int?>(Columns.PointRuleId); }
			set { SetColumnValue(Columns.PointRuleId, value); }
		}
		  
		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName 
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}
		  
		[XmlAttribute("Count")]
		[Bindable(true)]
		public int Count 
		{
			get { return GetColumnValue<int>(Columns.Count); }
			set { SetColumnValue(Columns.Count, value); }
		}
		  
		[XmlAttribute("ExpireTime")]
		[Bindable(true)]
		public DateTime? ExpireTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ExpireTime); }
			set { SetColumnValue(Columns.ExpireTime, value); }
		}
		  
		[XmlAttribute("PcpOrderGuid")]
		[Bindable(true)]
		public Guid PcpOrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.PcpOrderGuid); }
			set { SetColumnValue(Columns.PcpOrderGuid, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("FinalPoint")]
		[Bindable(true)]
		public int FinalPoint 
		{
			get { return GetColumnValue<int>(Columns.FinalPoint); }
			set { SetColumnValue(Columns.FinalPoint, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CardGroupIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalPointColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn RemainPointColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PriceColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn SetTypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn PointRuleIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CountColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ExpireTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn PcpOrderGuidColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn FinalPointColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CardGroupId = @"card_group_id";
			 public static string UserId = @"user_id";
			 public static string TotalPoint = @"total_point";
			 public static string RemainPoint = @"remain_point";
			 public static string Price = @"price";
			 public static string SetType = @"set_type";
			 public static string PointRuleId = @"point_rule_id";
			 public static string ItemName = @"item_name";
			 public static string Count = @"count";
			 public static string ExpireTime = @"expire_time";
			 public static string PcpOrderGuid = @"pcp_order_guid";
			 public static string SellerGuid = @"seller_guid";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string FinalPoint = @"final_point";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBonusTransaction class.
    /// </summary>
    [Serializable]
    public partial class ViewBonusTransactionCollection : ReadOnlyList<ViewBonusTransaction, ViewBonusTransactionCollection>
    {        
        public ViewBonusTransactionCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_bonus_transaction view.
    /// </summary>
    [Serializable]
    public partial class ViewBonusTransaction : ReadOnlyRecord<ViewBonusTransaction>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_bonus_transaction", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarAction = new TableSchema.TableColumn(schema);
                colvarAction.ColumnName = "action";
                colvarAction.DataType = DbType.String;
                colvarAction.MaxLength = 200;
                colvarAction.AutoIncrement = false;
                colvarAction.IsNullable = true;
                colvarAction.IsPrimaryKey = false;
                colvarAction.IsForeignKey = false;
                colvarAction.IsReadOnly = false;
                
                schema.Columns.Add(colvarAction);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Double;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarRunSum = new TableSchema.TableColumn(schema);
                colvarRunSum.ColumnName = "run_sum";
                colvarRunSum.DataType = DbType.Double;
                colvarRunSum.MaxLength = 0;
                colvarRunSum.AutoIncrement = false;
                colvarRunSum.IsNullable = true;
                colvarRunSum.IsPrimaryKey = false;
                colvarRunSum.IsForeignKey = false;
                colvarRunSum.IsReadOnly = false;
                
                schema.Columns.Add(colvarRunSum);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarTransactionType = new TableSchema.TableColumn(schema);
                colvarTransactionType.ColumnName = "transaction_type";
                colvarTransactionType.DataType = DbType.Int32;
                colvarTransactionType.MaxLength = 0;
                colvarTransactionType.AutoIncrement = false;
                colvarTransactionType.IsNullable = false;
                colvarTransactionType.IsPrimaryKey = false;
                colvarTransactionType.IsForeignKey = false;
                colvarTransactionType.IsReadOnly = false;
                
                schema.Columns.Add(colvarTransactionType);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_GUID";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_bonus_transaction",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBonusTransaction()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBonusTransaction(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBonusTransaction(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBonusTransaction(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("Action")]
        [Bindable(true)]
        public string Action 
	    {
		    get
		    {
			    return GetColumnValue<string>("action");
		    }
            set 
		    {
			    SetColumnValue("action", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public double Amount 
	    {
		    get
		    {
			    return GetColumnValue<double>("amount");
		    }
            set 
		    {
			    SetColumnValue("amount", value);
            }
        }
	      
        [XmlAttribute("RunSum")]
        [Bindable(true)]
        public double? RunSum 
	    {
		    get
		    {
			    return GetColumnValue<double?>("run_sum");
		    }
            set 
		    {
			    SetColumnValue("run_sum", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("TransactionType")]
        [Bindable(true)]
        public int TransactionType 
	    {
		    get
		    {
			    return GetColumnValue<int>("transaction_type");
		    }
            set 
		    {
			    SetColumnValue("transaction_type", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("order_GUID");
		    }
            set 
		    {
			    SetColumnValue("order_GUID", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string UserName = @"user_name";
            
            public static string Action = @"action";
            
            public static string Amount = @"amount";
            
            public static string RunSum = @"run_sum";
            
            public static string CreateTime = @"create_time";
            
            public static string TransactionType = @"transaction_type";
            
            public static string OrderGuid = @"order_GUID";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

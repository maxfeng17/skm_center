using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OauthClient class.
	/// </summary>
    [Serializable]
	public partial class OauthClientCollection : RepositoryList<OauthClient, OauthClientCollection>
	{	   
		public OauthClientCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OauthClientCollection</returns>
		public OauthClientCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OauthClient o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the oauth_client table.
	/// </summary>
	[Serializable]
	public partial class OauthClient : RepositoryRecord<OauthClient>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OauthClient()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OauthClient(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("oauth_client", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarAppId = new TableSchema.TableColumn(schema);
				colvarAppId.ColumnName = "app_id";
				colvarAppId.DataType = DbType.String;
				colvarAppId.MaxLength = 100;
				colvarAppId.AutoIncrement = false;
				colvarAppId.IsNullable = true;
				colvarAppId.IsPrimaryKey = false;
				colvarAppId.IsForeignKey = false;
				colvarAppId.IsReadOnly = false;
				colvarAppId.DefaultSetting = @"";
				colvarAppId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppId);
				
				TableSchema.TableColumn colvarAppSecret = new TableSchema.TableColumn(schema);
				colvarAppSecret.ColumnName = "app_secret";
				colvarAppSecret.DataType = DbType.String;
				colvarAppSecret.MaxLength = 100;
				colvarAppSecret.AutoIncrement = false;
				colvarAppSecret.IsNullable = true;
				colvarAppSecret.IsPrimaryKey = false;
				colvarAppSecret.IsForeignKey = false;
				colvarAppSecret.IsReadOnly = false;
				colvarAppSecret.DefaultSetting = @"";
				colvarAppSecret.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppSecret);
				
				TableSchema.TableColumn colvarReturnUrl = new TableSchema.TableColumn(schema);
				colvarReturnUrl.ColumnName = "return_url";
				colvarReturnUrl.DataType = DbType.String;
				colvarReturnUrl.MaxLength = 200;
				colvarReturnUrl.AutoIncrement = false;
				colvarReturnUrl.IsNullable = true;
				colvarReturnUrl.IsPrimaryKey = false;
				colvarReturnUrl.IsForeignKey = false;
				colvarReturnUrl.IsReadOnly = false;
				colvarReturnUrl.DefaultSetting = @"";
				colvarReturnUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnUrl);
				
				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 200;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = true;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = -1;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarOwner = new TableSchema.TableColumn(schema);
				colvarOwner.ColumnName = "owner";
				colvarOwner.DataType = DbType.String;
				colvarOwner.MaxLength = 200;
				colvarOwner.AutoIncrement = false;
				colvarOwner.IsNullable = true;
				colvarOwner.IsPrimaryKey = false;
				colvarOwner.IsForeignKey = false;
				colvarOwner.IsReadOnly = false;
				colvarOwner.DefaultSetting = @"";
				colvarOwner.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOwner);
				
				TableSchema.TableColumn colvarCreatedTime = new TableSchema.TableColumn(schema);
				colvarCreatedTime.ColumnName = "created_time";
				colvarCreatedTime.DataType = DbType.DateTime;
				colvarCreatedTime.MaxLength = 0;
				colvarCreatedTime.AutoIncrement = false;
				colvarCreatedTime.IsNullable = false;
				colvarCreatedTime.IsPrimaryKey = false;
				colvarCreatedTime.IsForeignKey = false;
				colvarCreatedTime.IsReadOnly = false;
				colvarCreatedTime.DefaultSetting = @"";
				colvarCreatedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedTime);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = false;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				colvarEnabled.DefaultSetting = @"";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("oauth_client",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("AppId")]
		[Bindable(true)]
		public string AppId 
		{
			get { return GetColumnValue<string>(Columns.AppId); }
			set { SetColumnValue(Columns.AppId, value); }
		}
		  
		[XmlAttribute("AppSecret")]
		[Bindable(true)]
		public string AppSecret 
		{
			get { return GetColumnValue<string>(Columns.AppSecret); }
			set { SetColumnValue(Columns.AppSecret, value); }
		}
		  
		[XmlAttribute("ReturnUrl")]
		[Bindable(true)]
		public string ReturnUrl 
		{
			get { return GetColumnValue<string>(Columns.ReturnUrl); }
			set { SetColumnValue(Columns.ReturnUrl, value); }
		}
		  
		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name 
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("Owner")]
		[Bindable(true)]
		public string Owner 
		{
			get { return GetColumnValue<string>(Columns.Owner); }
			set { SetColumnValue(Columns.Owner, value); }
		}
		  
		[XmlAttribute("CreatedTime")]
		[Bindable(true)]
		public DateTime CreatedTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedTime); }
			set { SetColumnValue(Columns.CreatedTime, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool Enabled 
		{
			get { return GetColumnValue<bool>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AppIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AppSecretColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnUrlColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn OwnerColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string AppId = @"app_id";
			 public static string AppSecret = @"app_secret";
			 public static string ReturnUrl = @"return_url";
			 public static string Name = @"name";
			 public static string Description = @"description";
			 public static string Owner = @"owner";
			 public static string CreatedTime = @"created_time";
			 public static string Enabled = @"enabled";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

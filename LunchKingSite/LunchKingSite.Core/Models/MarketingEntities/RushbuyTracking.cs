using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.MarketingEntities
{
	[Table("rushbuy_tracking")]
	public class RushbuyTracking
	{
		public RushbuyTracking()
		{
			CpaKey = string.Empty;
			Url = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("cpa_key")]
		public string CpaKey { get; set; }

		[Column("bid")]
		public Guid Bid { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("user_id")]
		public int? UserId { get; set; }

		[Column("url", TypeName = "varchar")]
		public string Url { get; set; }
	}
}

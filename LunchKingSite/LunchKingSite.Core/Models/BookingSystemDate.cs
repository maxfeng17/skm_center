using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BookingSystemDate class.
	/// </summary>
    [Serializable]
	public partial class BookingSystemDateCollection : RepositoryList<BookingSystemDate, BookingSystemDateCollection>
	{	   
		public BookingSystemDateCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BookingSystemDateCollection</returns>
		public BookingSystemDateCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BookingSystemDate o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the booking_system_date table.
	/// </summary>
	[Serializable]
	public partial class BookingSystemDate : RepositoryRecord<BookingSystemDate>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BookingSystemDate()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BookingSystemDate(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("booking_system_date", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBookingSystemStoreBookingId = new TableSchema.TableColumn(schema);
				colvarBookingSystemStoreBookingId.ColumnName = "booking_system_store_booking_id";
				colvarBookingSystemStoreBookingId.DataType = DbType.Int32;
				colvarBookingSystemStoreBookingId.MaxLength = 0;
				colvarBookingSystemStoreBookingId.AutoIncrement = false;
				colvarBookingSystemStoreBookingId.IsNullable = false;
				colvarBookingSystemStoreBookingId.IsPrimaryKey = false;
				colvarBookingSystemStoreBookingId.IsForeignKey = false;
				colvarBookingSystemStoreBookingId.IsReadOnly = false;
				colvarBookingSystemStoreBookingId.DefaultSetting = @"";
				colvarBookingSystemStoreBookingId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBookingSystemStoreBookingId);
				
				TableSchema.TableColumn colvarDayType = new TableSchema.TableColumn(schema);
				colvarDayType.ColumnName = "day_type";
				colvarDayType.DataType = DbType.Int32;
				colvarDayType.MaxLength = 0;
				colvarDayType.AutoIncrement = false;
				colvarDayType.IsNullable = false;
				colvarDayType.IsPrimaryKey = false;
				colvarDayType.IsForeignKey = false;
				colvarDayType.IsReadOnly = false;
				
						colvarDayType.DefaultSetting = @"((8))";
				colvarDayType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDayType);
				
				TableSchema.TableColumn colvarEffectiveDate = new TableSchema.TableColumn(schema);
				colvarEffectiveDate.ColumnName = "effective_date";
				colvarEffectiveDate.DataType = DbType.DateTime;
				colvarEffectiveDate.MaxLength = 0;
				colvarEffectiveDate.AutoIncrement = false;
				colvarEffectiveDate.IsNullable = true;
				colvarEffectiveDate.IsPrimaryKey = false;
				colvarEffectiveDate.IsForeignKey = false;
				colvarEffectiveDate.IsReadOnly = false;
				colvarEffectiveDate.DefaultSetting = @"";
				colvarEffectiveDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEffectiveDate);
				
				TableSchema.TableColumn colvarIsOpening = new TableSchema.TableColumn(schema);
				colvarIsOpening.ColumnName = "is_opening";
				colvarIsOpening.DataType = DbType.Boolean;
				colvarIsOpening.MaxLength = 0;
				colvarIsOpening.AutoIncrement = false;
				colvarIsOpening.IsNullable = false;
				colvarIsOpening.IsPrimaryKey = false;
				colvarIsOpening.IsForeignKey = false;
				colvarIsOpening.IsReadOnly = false;
				
						colvarIsOpening.DefaultSetting = @"((1))";
				colvarIsOpening.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOpening);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("booking_system_date",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("BookingSystemStoreBookingId")]
		[Bindable(true)]
		public int BookingSystemStoreBookingId 
		{
			get { return GetColumnValue<int>(Columns.BookingSystemStoreBookingId); }
			set { SetColumnValue(Columns.BookingSystemStoreBookingId, value); }
		}
		  
		[XmlAttribute("DayType")]
		[Bindable(true)]
		public int DayType 
		{
			get { return GetColumnValue<int>(Columns.DayType); }
			set { SetColumnValue(Columns.DayType, value); }
		}
		  
		[XmlAttribute("EffectiveDate")]
		[Bindable(true)]
		public DateTime? EffectiveDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.EffectiveDate); }
			set { SetColumnValue(Columns.EffectiveDate, value); }
		}
		  
		[XmlAttribute("IsOpening")]
		[Bindable(true)]
		public bool IsOpening 
		{
			get { return GetColumnValue<bool>(Columns.IsOpening); }
			set { SetColumnValue(Columns.IsOpening, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BookingSystemStoreBookingIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DayTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn EffectiveDateColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOpeningColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BookingSystemStoreBookingId = @"booking_system_store_booking_id";
			 public static string DayType = @"day_type";
			 public static string EffectiveDate = @"effective_date";
			 public static string IsOpening = @"is_opening";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VbsMembershipPermission class.
	/// </summary>
    [Serializable]
	public partial class VbsMembershipPermissionCollection : RepositoryList<VbsMembershipPermission, VbsMembershipPermissionCollection>
	{	   
		public VbsMembershipPermissionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VbsMembershipPermissionCollection</returns>
		public VbsMembershipPermissionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VbsMembershipPermission o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the vbs_membership_permission table.
	/// </summary>
	
	[Serializable]
	public partial class VbsMembershipPermission : RepositoryRecord<VbsMembershipPermission>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VbsMembershipPermission()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VbsMembershipPermission(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("vbs_membership_permission", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarVbsMembershipId = new TableSchema.TableColumn(schema);
				colvarVbsMembershipId.ColumnName = "vbs_membership_id";
				colvarVbsMembershipId.DataType = DbType.Int32;
				colvarVbsMembershipId.MaxLength = 0;
				colvarVbsMembershipId.AutoIncrement = false;
				colvarVbsMembershipId.IsNullable = false;
				colvarVbsMembershipId.IsPrimaryKey = true;
				colvarVbsMembershipId.IsForeignKey = false;
				colvarVbsMembershipId.IsReadOnly = false;
				colvarVbsMembershipId.DefaultSetting = @"";
				colvarVbsMembershipId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVbsMembershipId);
				
				TableSchema.TableColumn colvarScope = new TableSchema.TableColumn(schema);
				colvarScope.ColumnName = "scope";
				colvarScope.DataType = DbType.Int32;
				colvarScope.MaxLength = 0;
				colvarScope.AutoIncrement = false;
				colvarScope.IsNullable = false;
				colvarScope.IsPrimaryKey = true;
				colvarScope.IsForeignKey = false;
				colvarScope.IsReadOnly = false;
				colvarScope.DefaultSetting = @"";
				colvarScope.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScope);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				
						colvarUserId.DefaultSetting = @"((0))";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("vbs_membership_permission",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("VbsMembershipId")]
		[Bindable(true)]
		public int VbsMembershipId 
		{
			get { return GetColumnValue<int>(Columns.VbsMembershipId); }
			set { SetColumnValue(Columns.VbsMembershipId, value); }
		}
		
		[XmlAttribute("Scope")]
		[Bindable(true)]
		public int Scope 
		{
			get { return GetColumnValue<int>(Columns.Scope); }
			set { SetColumnValue(Columns.Scope, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn VbsMembershipIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ScopeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string VbsMembershipId = @"vbs_membership_id";
			 public static string Scope = @"scope";
			 public static string CreateTime = @"create_time";
			 public static string UserId = @"user_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the HiDealWeeklyPayReport class.
    /// </summary>
    [Serializable]
    public partial class HiDealWeeklyPayReportCollection : RepositoryList<HiDealWeeklyPayReport, HiDealWeeklyPayReportCollection>
    {
        public HiDealWeeklyPayReportCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealWeeklyPayReportCollection</returns>
        public HiDealWeeklyPayReportCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealWeeklyPayReport o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the hi_deal_weekly_pay_report table.
    /// </summary>
    [Serializable]
    public partial class HiDealWeeklyPayReport : RepositoryRecord<HiDealWeeklyPayReport>, IRecordBase
    {
        #region .ctors and Default Settings

        public HiDealWeeklyPayReport()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public HiDealWeeklyPayReport(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("hi_deal_weekly_pay_report", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "Id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                colvarDealId.DefaultSetting = @"";
                colvarDealId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDealId);

                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = false;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                colvarProductId.DefaultSetting = @"";
                colvarProductId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProductId);

                TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
                colvarEventName.ColumnName = "event_name";
                colvarEventName.DataType = DbType.String;
                colvarEventName.MaxLength = 500;
                colvarEventName.AutoIncrement = false;
                colvarEventName.IsNullable = false;
                colvarEventName.IsPrimaryKey = false;
                colvarEventName.IsForeignKey = false;
                colvarEventName.IsReadOnly = false;
                colvarEventName.DefaultSetting = @"";
                colvarEventName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventName);

                TableSchema.TableColumn colvarIntervalStart = new TableSchema.TableColumn(schema);
                colvarIntervalStart.ColumnName = "interval_start";
                colvarIntervalStart.DataType = DbType.DateTime;
                colvarIntervalStart.MaxLength = 0;
                colvarIntervalStart.AutoIncrement = false;
                colvarIntervalStart.IsNullable = false;
                colvarIntervalStart.IsPrimaryKey = false;
                colvarIntervalStart.IsForeignKey = false;
                colvarIntervalStart.IsReadOnly = false;
                colvarIntervalStart.DefaultSetting = @"";
                colvarIntervalStart.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIntervalStart);

                TableSchema.TableColumn colvarIntervalEnd = new TableSchema.TableColumn(schema);
                colvarIntervalEnd.ColumnName = "interval_end";
                colvarIntervalEnd.DataType = DbType.DateTime;
                colvarIntervalEnd.MaxLength = 0;
                colvarIntervalEnd.AutoIncrement = false;
                colvarIntervalEnd.IsNullable = false;
                colvarIntervalEnd.IsPrimaryKey = false;
                colvarIntervalEnd.IsForeignKey = false;
                colvarIntervalEnd.IsReadOnly = false;
                colvarIntervalEnd.DefaultSetting = @"";
                colvarIntervalEnd.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIntervalEnd);

                TableSchema.TableColumn colvarCreditCard = new TableSchema.TableColumn(schema);
                colvarCreditCard.ColumnName = "credit_card";
                colvarCreditCard.DataType = DbType.Int32;
                colvarCreditCard.MaxLength = 0;
                colvarCreditCard.AutoIncrement = false;
                colvarCreditCard.IsNullable = false;
                colvarCreditCard.IsPrimaryKey = false;
                colvarCreditCard.IsForeignKey = false;
                colvarCreditCard.IsReadOnly = false;
                colvarCreditCard.DefaultSetting = @"";
                colvarCreditCard.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreditCard);

                TableSchema.TableColumn colvarAtm = new TableSchema.TableColumn(schema);
                colvarAtm.ColumnName = "atm";
                colvarAtm.DataType = DbType.Int32;
                colvarAtm.MaxLength = 0;
                colvarAtm.AutoIncrement = false;
                colvarAtm.IsNullable = false;
                colvarAtm.IsPrimaryKey = false;
                colvarAtm.IsForeignKey = false;
                colvarAtm.IsReadOnly = false;
                colvarAtm.DefaultSetting = @"";
                colvarAtm.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAtm);

                TableSchema.TableColumn colvarReportGuid = new TableSchema.TableColumn(schema);
                colvarReportGuid.ColumnName = "report_guid";
                colvarReportGuid.DataType = DbType.Guid;
                colvarReportGuid.MaxLength = 0;
                colvarReportGuid.AutoIncrement = false;
                colvarReportGuid.IsNullable = false;
                colvarReportGuid.IsPrimaryKey = false;
                colvarReportGuid.IsForeignKey = false;
                colvarReportGuid.IsReadOnly = false;
                colvarReportGuid.DefaultSetting = @"";
                colvarReportGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReportGuid);

                TableSchema.TableColumn colvarIsLastWeek = new TableSchema.TableColumn(schema);
                colvarIsLastWeek.ColumnName = "is_last_week";
                colvarIsLastWeek.DataType = DbType.Boolean;
                colvarIsLastWeek.MaxLength = 0;
                colvarIsLastWeek.AutoIncrement = false;
                colvarIsLastWeek.IsNullable = false;
                colvarIsLastWeek.IsPrimaryKey = false;
                colvarIsLastWeek.IsForeignKey = false;
                colvarIsLastWeek.IsReadOnly = false;
                colvarIsLastWeek.DefaultSetting = @"";
                colvarIsLastWeek.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsLastWeek);

                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Decimal;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = false;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;
                colvarCost.DefaultSetting = @"";
                colvarCost.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCost);

                TableSchema.TableColumn colvarTotalCount = new TableSchema.TableColumn(schema);
                colvarTotalCount.ColumnName = "total_count";
                colvarTotalCount.DataType = DbType.Int32;
                colvarTotalCount.MaxLength = 0;
                colvarTotalCount.AutoIncrement = false;
                colvarTotalCount.IsNullable = false;
                colvarTotalCount.IsPrimaryKey = false;
                colvarTotalCount.IsForeignKey = false;
                colvarTotalCount.IsReadOnly = false;
                colvarTotalCount.DefaultSetting = @"";
                colvarTotalCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTotalCount);

                TableSchema.TableColumn colvarTotalSum = new TableSchema.TableColumn(schema);
                colvarTotalSum.ColumnName = "total_sum";
                colvarTotalSum.DataType = DbType.Decimal;
                colvarTotalSum.MaxLength = 0;
                colvarTotalSum.AutoIncrement = false;
                colvarTotalSum.IsNullable = false;
                colvarTotalSum.IsPrimaryKey = false;
                colvarTotalSum.IsForeignKey = false;
                colvarTotalSum.IsReadOnly = false;
                colvarTotalSum.DefaultSetting = @"";
                colvarTotalSum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTotalSum);

                TableSchema.TableColumn colvarErrorIn = new TableSchema.TableColumn(schema);
                colvarErrorIn.ColumnName = "error_in";
                colvarErrorIn.DataType = DbType.Decimal;
                colvarErrorIn.MaxLength = 0;
                colvarErrorIn.AutoIncrement = false;
                colvarErrorIn.IsNullable = false;
                colvarErrorIn.IsPrimaryKey = false;
                colvarErrorIn.IsForeignKey = false;
                colvarErrorIn.IsReadOnly = false;
                colvarErrorIn.DefaultSetting = @"";
                colvarErrorIn.ForeignKeyTableName = "";
                schema.Columns.Add(colvarErrorIn);

                TableSchema.TableColumn colvarErrorOut = new TableSchema.TableColumn(schema);
                colvarErrorOut.ColumnName = "error_out";
                colvarErrorOut.DataType = DbType.Decimal;
                colvarErrorOut.MaxLength = 0;
                colvarErrorOut.AutoIncrement = false;
                colvarErrorOut.IsNullable = false;
                colvarErrorOut.IsPrimaryKey = false;
                colvarErrorOut.IsForeignKey = false;
                colvarErrorOut.IsReadOnly = false;
                colvarErrorOut.DefaultSetting = @"";
                colvarErrorOut.ForeignKeyTableName = "";
                schema.Columns.Add(colvarErrorOut);

                TableSchema.TableColumn colvarErrorInSum = new TableSchema.TableColumn(schema);
                colvarErrorInSum.ColumnName = "error_in_sum";
                colvarErrorInSum.DataType = DbType.Int32;
                colvarErrorInSum.MaxLength = 0;
                colvarErrorInSum.AutoIncrement = false;
                colvarErrorInSum.IsNullable = false;
                colvarErrorInSum.IsPrimaryKey = false;
                colvarErrorInSum.IsForeignKey = false;
                colvarErrorInSum.IsReadOnly = false;
                colvarErrorInSum.DefaultSetting = @"";
                colvarErrorInSum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarErrorInSum);

                TableSchema.TableColumn colvarErrorOutSum = new TableSchema.TableColumn(schema);
                colvarErrorOutSum.ColumnName = "error_out_sum";
                colvarErrorOutSum.DataType = DbType.Int32;
                colvarErrorOutSum.MaxLength = 0;
                colvarErrorOutSum.AutoIncrement = false;
                colvarErrorOutSum.IsNullable = false;
                colvarErrorOutSum.IsPrimaryKey = false;
                colvarErrorOutSum.IsForeignKey = false;
                colvarErrorOutSum.IsReadOnly = false;
                colvarErrorOutSum.DefaultSetting = @"";
                colvarErrorOutSum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarErrorOutSum);

                TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
                colvarCreator.ColumnName = "creator";
                colvarCreator.DataType = DbType.String;
                colvarCreator.MaxLength = 100;
                colvarCreator.AutoIncrement = false;
                colvarCreator.IsNullable = false;
                colvarCreator.IsPrimaryKey = false;
                colvarCreator.IsForeignKey = false;
                colvarCreator.IsReadOnly = false;
                colvarCreator.DefaultSetting = @"";
                colvarCreator.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreator);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                colvarStoreGuid.DefaultSetting = @"";
                colvarStoreGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreGuid);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = true;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                colvarSellerGuid.DefaultSetting = @"";
                colvarSellerGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarIsSummary = new TableSchema.TableColumn(schema);
                colvarIsSummary.ColumnName = "is_summary";
                colvarIsSummary.DataType = DbType.Boolean;
                colvarIsSummary.MaxLength = 0;
                colvarIsSummary.AutoIncrement = false;
                colvarIsSummary.IsNullable = false;
                colvarIsSummary.IsPrimaryKey = false;
                colvarIsSummary.IsForeignKey = false;
                colvarIsSummary.IsReadOnly = false;
                colvarIsSummary.DefaultSetting = @"";
                colvarIsSummary.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsSummary);

                TableSchema.TableColumn colvarResult = new TableSchema.TableColumn(schema);
                colvarResult.ColumnName = "result";
                colvarResult.DataType = DbType.String;
                colvarResult.MaxLength = 10;
                colvarResult.AutoIncrement = false;
                colvarResult.IsNullable = true;
                colvarResult.IsPrimaryKey = false;
                colvarResult.IsForeignKey = false;
                colvarResult.IsReadOnly = false;
                colvarResult.DefaultSetting = @"";
                colvarResult.ForeignKeyTableName = "";
                schema.Columns.Add(colvarResult);

                TableSchema.TableColumn colvarResponseTime = new TableSchema.TableColumn(schema);
                colvarResponseTime.ColumnName = "response_time";
                colvarResponseTime.DataType = DbType.DateTime;
                colvarResponseTime.MaxLength = 0;
                colvarResponseTime.AutoIncrement = false;
                colvarResponseTime.IsNullable = true;
                colvarResponseTime.IsPrimaryKey = false;
                colvarResponseTime.IsForeignKey = false;
                colvarResponseTime.IsReadOnly = false;
                colvarResponseTime.DefaultSetting = @"";
                colvarResponseTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarResponseTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("hi_deal_weekly_pay_report", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId
        {
            get { return GetColumnValue<int>(Columns.DealId); }
            set { SetColumnValue(Columns.DealId, value); }
        }

        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int ProductId
        {
            get { return GetColumnValue<int>(Columns.ProductId); }
            set { SetColumnValue(Columns.ProductId, value); }
        }

        [XmlAttribute("EventName")]
        [Bindable(true)]
        public string EventName
        {
            get { return GetColumnValue<string>(Columns.EventName); }
            set { SetColumnValue(Columns.EventName, value); }
        }

        [XmlAttribute("IntervalStart")]
        [Bindable(true)]
        public DateTime IntervalStart
        {
            get { return GetColumnValue<DateTime>(Columns.IntervalStart); }
            set { SetColumnValue(Columns.IntervalStart, value); }
        }

        [XmlAttribute("IntervalEnd")]
        [Bindable(true)]
        public DateTime IntervalEnd
        {
            get { return GetColumnValue<DateTime>(Columns.IntervalEnd); }
            set { SetColumnValue(Columns.IntervalEnd, value); }
        }

        [XmlAttribute("CreditCard")]
        [Bindable(true)]
        public int CreditCard
        {
            get { return GetColumnValue<int>(Columns.CreditCard); }
            set { SetColumnValue(Columns.CreditCard, value); }
        }

        [XmlAttribute("Atm")]
        [Bindable(true)]
        public int Atm
        {
            get { return GetColumnValue<int>(Columns.Atm); }
            set { SetColumnValue(Columns.Atm, value); }
        }

        [XmlAttribute("ReportGuid")]
        [Bindable(true)]
        public Guid ReportGuid
        {
            get { return GetColumnValue<Guid>(Columns.ReportGuid); }
            set { SetColumnValue(Columns.ReportGuid, value); }
        }

        [XmlAttribute("IsLastWeek")]
        [Bindable(true)]
        public bool IsLastWeek
        {
            get { return GetColumnValue<bool>(Columns.IsLastWeek); }
            set { SetColumnValue(Columns.IsLastWeek, value); }
        }

        [XmlAttribute("Cost")]
        [Bindable(true)]
        public decimal Cost
        {
            get { return GetColumnValue<decimal>(Columns.Cost); }
            set { SetColumnValue(Columns.Cost, value); }
        }

        [XmlAttribute("TotalCount")]
        [Bindable(true)]
        public int TotalCount
        {
            get { return GetColumnValue<int>(Columns.TotalCount); }
            set { SetColumnValue(Columns.TotalCount, value); }
        }

        [XmlAttribute("TotalSum")]
        [Bindable(true)]
        public decimal TotalSum
        {
            get { return GetColumnValue<decimal>(Columns.TotalSum); }
            set { SetColumnValue(Columns.TotalSum, value); }
        }

        [XmlAttribute("ErrorIn")]
        [Bindable(true)]
        public decimal ErrorIn
        {
            get { return GetColumnValue<decimal>(Columns.ErrorIn); }
            set { SetColumnValue(Columns.ErrorIn, value); }
        }

        [XmlAttribute("ErrorOut")]
        [Bindable(true)]
        public decimal ErrorOut
        {
            get { return GetColumnValue<decimal>(Columns.ErrorOut); }
            set { SetColumnValue(Columns.ErrorOut, value); }
        }

        [XmlAttribute("ErrorInSum")]
        [Bindable(true)]
        public int ErrorInSum
        {
            get { return GetColumnValue<int>(Columns.ErrorInSum); }
            set { SetColumnValue(Columns.ErrorInSum, value); }
        }

        [XmlAttribute("ErrorOutSum")]
        [Bindable(true)]
        public int ErrorOutSum
        {
            get { return GetColumnValue<int>(Columns.ErrorOutSum); }
            set { SetColumnValue(Columns.ErrorOutSum, value); }
        }

        [XmlAttribute("Creator")]
        [Bindable(true)]
        public string Creator
        {
            get { return GetColumnValue<string>(Columns.Creator); }
            set { SetColumnValue(Columns.Creator, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid
        {
            get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
            set { SetColumnValue(Columns.StoreGuid, value); }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid? SellerGuid
        {
            get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
            set { SetColumnValue(Columns.SellerGuid, value); }
        }

        [XmlAttribute("IsSummary")]
        [Bindable(true)]
        public bool IsSummary
        {
            get { return GetColumnValue<bool>(Columns.IsSummary); }
            set { SetColumnValue(Columns.IsSummary, value); }
        }

        [XmlAttribute("Result")]
        [Bindable(true)]
        public string Result
        {
            get { return GetColumnValue<string>(Columns.Result); }
            set { SetColumnValue(Columns.Result, value); }
        }

        [XmlAttribute("ResponseTime")]
        [Bindable(true)]
        public DateTime? ResponseTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ResponseTime); }
            set { SetColumnValue(Columns.ResponseTime, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn DealIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ProductIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn EventNameColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn IntervalStartColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn IntervalEndColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreditCardColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn AtmColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ReportGuidColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn IsLastWeekColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn CostColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn TotalCountColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn TotalSumColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn ErrorInColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn ErrorOutColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn ErrorInSumColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn ErrorOutSumColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn IsSummaryColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn ResultColumn
        {
            get { return Schema.Columns[22]; }
        }



        public static TableSchema.TableColumn ResponseTimeColumn
        {
            get { return Schema.Columns[23]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"Id";
            public static string DealId = @"deal_id";
            public static string ProductId = @"product_id";
            public static string EventName = @"event_name";
            public static string IntervalStart = @"interval_start";
            public static string IntervalEnd = @"interval_end";
            public static string CreditCard = @"credit_card";
            public static string Atm = @"atm";
            public static string ReportGuid = @"report_guid";
            public static string IsLastWeek = @"is_last_week";
            public static string Cost = @"cost";
            public static string TotalCount = @"total_count";
            public static string TotalSum = @"total_sum";
            public static string ErrorIn = @"error_in";
            public static string ErrorOut = @"error_out";
            public static string ErrorInSum = @"error_in_sum";
            public static string ErrorOutSum = @"error_out_sum";
            public static string Creator = @"creator";
            public static string CreateTime = @"create_time";
            public static string StoreGuid = @"store_guid";
            public static string SellerGuid = @"seller_guid";
            public static string IsSummary = @"is_summary";
            public static string Result = @"result";
            public static string ResponseTime = @"response_time";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

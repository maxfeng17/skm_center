using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealProduct class.
	/// </summary>
    [Serializable]
	public partial class HiDealProductCollection : RepositoryList<HiDealProduct, HiDealProductCollection>
	{	   
		public HiDealProductCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealProductCollection</returns>
		public HiDealProductCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealProduct o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_product table.
	/// </summary>
	[Serializable]
	public partial class HiDealProduct : RepositoryRecord<HiDealProduct>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealProduct()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealProduct(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_product", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
				colvarDealId.ColumnName = "deal_id";
				colvarDealId.DataType = DbType.Int32;
				colvarDealId.MaxLength = 0;
				colvarDealId.AutoIncrement = false;
				colvarDealId.IsNullable = false;
				colvarDealId.IsPrimaryKey = false;
				colvarDealId.IsForeignKey = true;
				colvarDealId.IsReadOnly = false;
				colvarDealId.DefaultSetting = @"";
				
					colvarDealId.ForeignKeyTableName = "hi_deal_deal";
				schema.Columns.Add(colvarDealId);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = true;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				colvarSeq.DefaultSetting = @"";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = true;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				
					colvarSellerGuid.ForeignKeyTableName = "seller";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 250;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = true;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = -1;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarIsOnline = new TableSchema.TableColumn(schema);
				colvarIsOnline.ColumnName = "is_online";
				colvarIsOnline.DataType = DbType.Boolean;
				colvarIsOnline.MaxLength = 0;
				colvarIsOnline.AutoIncrement = false;
				colvarIsOnline.IsNullable = true;
				colvarIsOnline.IsPrimaryKey = false;
				colvarIsOnline.IsForeignKey = false;
				colvarIsOnline.IsReadOnly = false;
				colvarIsOnline.DefaultSetting = @"";
				colvarIsOnline.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOnline);
				
				TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
				colvarUseStartTime.ColumnName = "use_start_time";
				colvarUseStartTime.DataType = DbType.DateTime;
				colvarUseStartTime.MaxLength = 0;
				colvarUseStartTime.AutoIncrement = false;
				colvarUseStartTime.IsNullable = true;
				colvarUseStartTime.IsPrimaryKey = false;
				colvarUseStartTime.IsForeignKey = false;
				colvarUseStartTime.IsReadOnly = false;
				colvarUseStartTime.DefaultSetting = @"";
				colvarUseStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUseStartTime);
				
				TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
				colvarUseEndTime.ColumnName = "use_end_time";
				colvarUseEndTime.DataType = DbType.DateTime;
				colvarUseEndTime.MaxLength = 0;
				colvarUseEndTime.AutoIncrement = false;
				colvarUseEndTime.IsNullable = true;
				colvarUseEndTime.IsPrimaryKey = false;
				colvarUseEndTime.IsForeignKey = false;
				colvarUseEndTime.IsReadOnly = false;
				colvarUseEndTime.DefaultSetting = @"";
				colvarUseEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUseEndTime);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = true;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarOriginalPrice = new TableSchema.TableColumn(schema);
				colvarOriginalPrice.ColumnName = "original_price";
				colvarOriginalPrice.DataType = DbType.Currency;
				colvarOriginalPrice.MaxLength = 0;
				colvarOriginalPrice.AutoIncrement = false;
				colvarOriginalPrice.IsNullable = true;
				colvarOriginalPrice.IsPrimaryKey = false;
				colvarOriginalPrice.IsForeignKey = false;
				colvarOriginalPrice.IsReadOnly = false;
				colvarOriginalPrice.DefaultSetting = @"";
				colvarOriginalPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOriginalPrice);
				
				TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
				colvarPrice.ColumnName = "price";
				colvarPrice.DataType = DbType.Currency;
				colvarPrice.MaxLength = 0;
				colvarPrice.AutoIncrement = false;
				colvarPrice.IsNullable = true;
				colvarPrice.IsPrimaryKey = false;
				colvarPrice.IsForeignKey = false;
				colvarPrice.IsReadOnly = false;
				colvarPrice.DefaultSetting = @"";
				colvarPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrice);
				
				TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
				colvarQuantity.ColumnName = "quantity";
				colvarQuantity.DataType = DbType.Int32;
				colvarQuantity.MaxLength = 0;
				colvarQuantity.AutoIncrement = false;
				colvarQuantity.IsNullable = true;
				colvarQuantity.IsPrimaryKey = false;
				colvarQuantity.IsForeignKey = false;
				colvarQuantity.IsReadOnly = false;
				colvarQuantity.DefaultSetting = @"";
				colvarQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQuantity);
				
				TableSchema.TableColumn colvarOrderLimit = new TableSchema.TableColumn(schema);
				colvarOrderLimit.ColumnName = "order_limit";
				colvarOrderLimit.DataType = DbType.Int32;
				colvarOrderLimit.MaxLength = 0;
				colvarOrderLimit.AutoIncrement = false;
				colvarOrderLimit.IsNullable = true;
				colvarOrderLimit.IsPrimaryKey = false;
				colvarOrderLimit.IsForeignKey = false;
				colvarOrderLimit.IsReadOnly = false;
				colvarOrderLimit.DefaultSetting = @"";
				colvarOrderLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderLimit);
				
				TableSchema.TableColumn colvarOrderLimitUser = new TableSchema.TableColumn(schema);
				colvarOrderLimitUser.ColumnName = "order_limit_user";
				colvarOrderLimitUser.DataType = DbType.Int32;
				colvarOrderLimitUser.MaxLength = 0;
				colvarOrderLimitUser.AutoIncrement = false;
				colvarOrderLimitUser.IsNullable = true;
				colvarOrderLimitUser.IsPrimaryKey = false;
				colvarOrderLimitUser.IsForeignKey = false;
				colvarOrderLimitUser.IsReadOnly = false;
				colvarOrderLimitUser.DefaultSetting = @"";
				colvarOrderLimitUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderLimitUser);
				
				TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
				colvarOrderCount.ColumnName = "order_count";
				colvarOrderCount.DataType = DbType.Int32;
				colvarOrderCount.MaxLength = 0;
				colvarOrderCount.AutoIncrement = false;
				colvarOrderCount.IsNullable = true;
				colvarOrderCount.IsPrimaryKey = false;
				colvarOrderCount.IsForeignKey = false;
				colvarOrderCount.IsReadOnly = false;
				colvarOrderCount.DefaultSetting = @"";
				colvarOrderCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderCount);
				
				TableSchema.TableColumn colvarIsNoRefund = new TableSchema.TableColumn(schema);
				colvarIsNoRefund.ColumnName = "is_no_refund";
				colvarIsNoRefund.DataType = DbType.Boolean;
				colvarIsNoRefund.MaxLength = 0;
				colvarIsNoRefund.AutoIncrement = false;
				colvarIsNoRefund.IsNullable = false;
				colvarIsNoRefund.IsPrimaryKey = false;
				colvarIsNoRefund.IsForeignKey = false;
				colvarIsNoRefund.IsReadOnly = false;
				
						colvarIsNoRefund.DefaultSetting = @"((0))";
				colvarIsNoRefund.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsNoRefund);
				
				TableSchema.TableColumn colvarIsExpireNoRefund = new TableSchema.TableColumn(schema);
				colvarIsExpireNoRefund.ColumnName = "is_expire_no_refund";
				colvarIsExpireNoRefund.DataType = DbType.Boolean;
				colvarIsExpireNoRefund.MaxLength = 0;
				colvarIsExpireNoRefund.AutoIncrement = false;
				colvarIsExpireNoRefund.IsNullable = false;
				colvarIsExpireNoRefund.IsPrimaryKey = false;
				colvarIsExpireNoRefund.IsForeignKey = false;
				colvarIsExpireNoRefund.IsReadOnly = false;
				
						colvarIsExpireNoRefund.DefaultSetting = @"((0))";
				colvarIsExpireNoRefund.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsExpireNoRefund);
				
				TableSchema.TableColumn colvarIsTaxFree = new TableSchema.TableColumn(schema);
				colvarIsTaxFree.ColumnName = "is_tax_free";
				colvarIsTaxFree.DataType = DbType.Boolean;
				colvarIsTaxFree.MaxLength = 0;
				colvarIsTaxFree.AutoIncrement = false;
				colvarIsTaxFree.IsNullable = false;
				colvarIsTaxFree.IsPrimaryKey = false;
				colvarIsTaxFree.IsForeignKey = false;
				colvarIsTaxFree.IsReadOnly = false;
				
						colvarIsTaxFree.DefaultSetting = @"((0))";
				colvarIsTaxFree.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTaxFree);
				
				TableSchema.TableColumn colvarIsSoldout = new TableSchema.TableColumn(schema);
				colvarIsSoldout.ColumnName = "is_soldout";
				colvarIsSoldout.DataType = DbType.Boolean;
				colvarIsSoldout.MaxLength = 0;
				colvarIsSoldout.AutoIncrement = false;
				colvarIsSoldout.IsNullable = true;
				colvarIsSoldout.IsPrimaryKey = false;
				colvarIsSoldout.IsForeignKey = false;
				colvarIsSoldout.IsReadOnly = false;
				colvarIsSoldout.DefaultSetting = @"";
				colvarIsSoldout.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSoldout);
				
				TableSchema.TableColumn colvarIsInStore = new TableSchema.TableColumn(schema);
				colvarIsInStore.ColumnName = "is_in_store";
				colvarIsInStore.DataType = DbType.Boolean;
				colvarIsInStore.MaxLength = 0;
				colvarIsInStore.AutoIncrement = false;
				colvarIsInStore.IsNullable = false;
				colvarIsInStore.IsPrimaryKey = false;
				colvarIsInStore.IsForeignKey = false;
				colvarIsInStore.IsReadOnly = false;
				
						colvarIsInStore.DefaultSetting = @"((0))";
				colvarIsInStore.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsInStore);
				
				TableSchema.TableColumn colvarInStoreDesc = new TableSchema.TableColumn(schema);
				colvarInStoreDesc.ColumnName = "in_store_desc";
				colvarInStoreDesc.DataType = DbType.String;
				colvarInStoreDesc.MaxLength = 100;
				colvarInStoreDesc.AutoIncrement = false;
				colvarInStoreDesc.IsNullable = true;
				colvarInStoreDesc.IsPrimaryKey = false;
				colvarInStoreDesc.IsForeignKey = false;
				colvarInStoreDesc.IsReadOnly = false;
				colvarInStoreDesc.DefaultSetting = @"";
				colvarInStoreDesc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInStoreDesc);
				
				TableSchema.TableColumn colvarIsHomeDelivery = new TableSchema.TableColumn(schema);
				colvarIsHomeDelivery.ColumnName = "is_home_delivery";
				colvarIsHomeDelivery.DataType = DbType.Boolean;
				colvarIsHomeDelivery.MaxLength = 0;
				colvarIsHomeDelivery.AutoIncrement = false;
				colvarIsHomeDelivery.IsNullable = false;
				colvarIsHomeDelivery.IsPrimaryKey = false;
				colvarIsHomeDelivery.IsForeignKey = false;
				colvarIsHomeDelivery.IsReadOnly = false;
				
						colvarIsHomeDelivery.DefaultSetting = @"((0))";
				colvarIsHomeDelivery.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsHomeDelivery);
				
				TableSchema.TableColumn colvarHomeDeliveryDesc = new TableSchema.TableColumn(schema);
				colvarHomeDeliveryDesc.ColumnName = "home_delivery_desc";
				colvarHomeDeliveryDesc.DataType = DbType.String;
				colvarHomeDeliveryDesc.MaxLength = 100;
				colvarHomeDeliveryDesc.AutoIncrement = false;
				colvarHomeDeliveryDesc.IsNullable = true;
				colvarHomeDeliveryDesc.IsPrimaryKey = false;
				colvarHomeDeliveryDesc.IsForeignKey = false;
				colvarHomeDeliveryDesc.IsReadOnly = false;
				colvarHomeDeliveryDesc.DefaultSetting = @"";
				colvarHomeDeliveryDesc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHomeDeliveryDesc);
				
				TableSchema.TableColumn colvarSms = new TableSchema.TableColumn(schema);
				colvarSms.ColumnName = "sms";
				colvarSms.DataType = DbType.String;
				colvarSms.MaxLength = 150;
				colvarSms.AutoIncrement = false;
				colvarSms.IsNullable = true;
				colvarSms.IsPrimaryKey = false;
				colvarSms.IsForeignKey = false;
				colvarSms.IsReadOnly = false;
				colvarSms.DefaultSetting = @"";
				colvarSms.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSms);
				
				TableSchema.TableColumn colvarIsSmsClose = new TableSchema.TableColumn(schema);
				colvarIsSmsClose.ColumnName = "is_sms_close";
				colvarIsSmsClose.DataType = DbType.Boolean;
				colvarIsSmsClose.MaxLength = 0;
				colvarIsSmsClose.AutoIncrement = false;
				colvarIsSmsClose.IsNullable = false;
				colvarIsSmsClose.IsPrimaryKey = false;
				colvarIsSmsClose.IsForeignKey = false;
				colvarIsSmsClose.IsReadOnly = false;
				
						colvarIsSmsClose.DefaultSetting = @"((0))";
				colvarIsSmsClose.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSmsClose);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 100;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 100;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
				colvarStartTime.ColumnName = "start_time";
				colvarStartTime.DataType = DbType.DateTime;
				colvarStartTime.MaxLength = 0;
				colvarStartTime.AutoIncrement = false;
				colvarStartTime.IsNullable = true;
				colvarStartTime.IsPrimaryKey = false;
				colvarStartTime.IsForeignKey = false;
				colvarStartTime.IsReadOnly = false;
				colvarStartTime.DefaultSetting = @"";
				colvarStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartTime);
				
				TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
				colvarEndTime.ColumnName = "end_time";
				colvarEndTime.DataType = DbType.DateTime;
				colvarEndTime.MaxLength = 0;
				colvarEndTime.AutoIncrement = false;
				colvarEndTime.IsNullable = true;
				colvarEndTime.IsPrimaryKey = false;
				colvarEndTime.IsForeignKey = false;
				colvarEndTime.IsReadOnly = false;
				colvarEndTime.DefaultSetting = @"";
				colvarEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndTime);
				
				TableSchema.TableColumn colvarPic = new TableSchema.TableColumn(schema);
				colvarPic.ColumnName = "pic";
				colvarPic.DataType = DbType.String;
				colvarPic.MaxLength = -1;
				colvarPic.AutoIncrement = false;
				colvarPic.IsNullable = true;
				colvarPic.IsPrimaryKey = false;
				colvarPic.IsForeignKey = false;
				colvarPic.IsReadOnly = false;
				colvarPic.DefaultSetting = @"";
				colvarPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPic);
				
				TableSchema.TableColumn colvarHasBranchStoreQuantityLimit = new TableSchema.TableColumn(schema);
				colvarHasBranchStoreQuantityLimit.ColumnName = "has_branch_store_quantity_limit";
				colvarHasBranchStoreQuantityLimit.DataType = DbType.Boolean;
				colvarHasBranchStoreQuantityLimit.MaxLength = 0;
				colvarHasBranchStoreQuantityLimit.AutoIncrement = false;
				colvarHasBranchStoreQuantityLimit.IsNullable = false;
				colvarHasBranchStoreQuantityLimit.IsPrimaryKey = false;
				colvarHasBranchStoreQuantityLimit.IsForeignKey = false;
				colvarHasBranchStoreQuantityLimit.IsReadOnly = false;
				
						colvarHasBranchStoreQuantityLimit.DefaultSetting = @"((0))";
				colvarHasBranchStoreQuantityLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHasBranchStoreQuantityLimit);
				
				TableSchema.TableColumn colvarHomeDeliveryQuantity = new TableSchema.TableColumn(schema);
				colvarHomeDeliveryQuantity.ColumnName = "home_delivery_quantity";
				colvarHomeDeliveryQuantity.DataType = DbType.Int32;
				colvarHomeDeliveryQuantity.MaxLength = 0;
				colvarHomeDeliveryQuantity.AutoIncrement = false;
				colvarHomeDeliveryQuantity.IsNullable = true;
				colvarHomeDeliveryQuantity.IsPrimaryKey = false;
				colvarHomeDeliveryQuantity.IsForeignKey = false;
				colvarHomeDeliveryQuantity.IsReadOnly = false;
				colvarHomeDeliveryQuantity.DefaultSetting = @"";
				colvarHomeDeliveryQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHomeDeliveryQuantity);
				
				TableSchema.TableColumn colvarDeliveryIslands = new TableSchema.TableColumn(schema);
				colvarDeliveryIslands.ColumnName = "deliveryIslands";
				colvarDeliveryIslands.DataType = DbType.Boolean;
				colvarDeliveryIslands.MaxLength = 0;
				colvarDeliveryIslands.AutoIncrement = false;
				colvarDeliveryIslands.IsNullable = false;
				colvarDeliveryIslands.IsPrimaryKey = false;
				colvarDeliveryIslands.IsForeignKey = false;
				colvarDeliveryIslands.IsReadOnly = false;
				
						colvarDeliveryIslands.DefaultSetting = @"('1')";
				colvarDeliveryIslands.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryIslands);
				
				TableSchema.TableColumn colvarShippedDate = new TableSchema.TableColumn(schema);
				colvarShippedDate.ColumnName = "shipped_date";
				colvarShippedDate.DataType = DbType.DateTime;
				colvarShippedDate.MaxLength = 0;
				colvarShippedDate.AutoIncrement = false;
				colvarShippedDate.IsNullable = true;
				colvarShippedDate.IsPrimaryKey = false;
				colvarShippedDate.IsForeignKey = false;
				colvarShippedDate.IsReadOnly = false;
				colvarShippedDate.DefaultSetting = @"";
				colvarShippedDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShippedDate);
				
				TableSchema.TableColumn colvarTrustType = new TableSchema.TableColumn(schema);
				colvarTrustType.ColumnName = "trust_type";
				colvarTrustType.DataType = DbType.Int32;
				colvarTrustType.MaxLength = 0;
				colvarTrustType.AutoIncrement = false;
				colvarTrustType.IsNullable = false;
				colvarTrustType.IsPrimaryKey = false;
				colvarTrustType.IsForeignKey = false;
				colvarTrustType.IsReadOnly = false;
				
						colvarTrustType.DefaultSetting = @"((0))";
				colvarTrustType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustType);
				
				TableSchema.TableColumn colvarIsShowPriceDiscount = new TableSchema.TableColumn(schema);
				colvarIsShowPriceDiscount.ColumnName = "is_show_Price_discount";
				colvarIsShowPriceDiscount.DataType = DbType.Boolean;
				colvarIsShowPriceDiscount.MaxLength = 0;
				colvarIsShowPriceDiscount.AutoIncrement = false;
				colvarIsShowPriceDiscount.IsNullable = false;
				colvarIsShowPriceDiscount.IsPrimaryKey = false;
				colvarIsShowPriceDiscount.IsForeignKey = false;
				colvarIsShowPriceDiscount.IsReadOnly = false;
				
						colvarIsShowPriceDiscount.DefaultSetting = @"((1))";
				colvarIsShowPriceDiscount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsShowPriceDiscount);
				
				TableSchema.TableColumn colvarIsInvoiceCreate = new TableSchema.TableColumn(schema);
				colvarIsInvoiceCreate.ColumnName = "Is_Invoice_Create";
				colvarIsInvoiceCreate.DataType = DbType.Boolean;
				colvarIsInvoiceCreate.MaxLength = 0;
				colvarIsInvoiceCreate.AutoIncrement = false;
				colvarIsInvoiceCreate.IsNullable = false;
				colvarIsInvoiceCreate.IsPrimaryKey = false;
				colvarIsInvoiceCreate.IsForeignKey = false;
				colvarIsInvoiceCreate.IsReadOnly = false;
				
						colvarIsInvoiceCreate.DefaultSetting = @"('1')";
				colvarIsInvoiceCreate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsInvoiceCreate);
				
				TableSchema.TableColumn colvarPayToCompany = new TableSchema.TableColumn(schema);
				colvarPayToCompany.ColumnName = "pay_to_company";
				colvarPayToCompany.DataType = DbType.Boolean;
				colvarPayToCompany.MaxLength = 0;
				colvarPayToCompany.AutoIncrement = false;
				colvarPayToCompany.IsNullable = false;
				colvarPayToCompany.IsPrimaryKey = false;
				colvarPayToCompany.IsForeignKey = false;
				colvarPayToCompany.IsReadOnly = false;
				
						colvarPayToCompany.DefaultSetting = @"((1))";
				colvarPayToCompany.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayToCompany);
				
				TableSchema.TableColumn colvarIsCombo = new TableSchema.TableColumn(schema);
				colvarIsCombo.ColumnName = "is_combo";
				colvarIsCombo.DataType = DbType.Boolean;
				colvarIsCombo.MaxLength = 0;
				colvarIsCombo.AutoIncrement = false;
				colvarIsCombo.IsNullable = false;
				colvarIsCombo.IsPrimaryKey = false;
				colvarIsCombo.IsForeignKey = false;
				colvarIsCombo.IsReadOnly = false;
				
						colvarIsCombo.DefaultSetting = @"((0))";
				colvarIsCombo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCombo);
				
				TableSchema.TableColumn colvarIsDaysNoRefund = new TableSchema.TableColumn(schema);
				colvarIsDaysNoRefund.ColumnName = "is_days_no_refund";
				colvarIsDaysNoRefund.DataType = DbType.Boolean;
				colvarIsDaysNoRefund.MaxLength = 0;
				colvarIsDaysNoRefund.AutoIncrement = false;
				colvarIsDaysNoRefund.IsNullable = false;
				colvarIsDaysNoRefund.IsPrimaryKey = false;
				colvarIsDaysNoRefund.IsForeignKey = false;
				colvarIsDaysNoRefund.IsReadOnly = false;
				
						colvarIsDaysNoRefund.DefaultSetting = @"((0))";
				colvarIsDaysNoRefund.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDaysNoRefund);
				
				TableSchema.TableColumn colvarVendorBillingModel = new TableSchema.TableColumn(schema);
				colvarVendorBillingModel.ColumnName = "vendor_billing_model";
				colvarVendorBillingModel.DataType = DbType.Int32;
				colvarVendorBillingModel.MaxLength = 0;
				colvarVendorBillingModel.AutoIncrement = false;
				colvarVendorBillingModel.IsNullable = false;
				colvarVendorBillingModel.IsPrimaryKey = false;
				colvarVendorBillingModel.IsForeignKey = false;
				colvarVendorBillingModel.IsReadOnly = false;
				
						colvarVendorBillingModel.DefaultSetting = @"((0))";
				colvarVendorBillingModel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorBillingModel);
				
				TableSchema.TableColumn colvarVendorReceiptType = new TableSchema.TableColumn(schema);
				colvarVendorReceiptType.ColumnName = "vendor_receipt_type";
				colvarVendorReceiptType.DataType = DbType.Int32;
				colvarVendorReceiptType.MaxLength = 0;
				colvarVendorReceiptType.AutoIncrement = false;
				colvarVendorReceiptType.IsNullable = false;
				colvarVendorReceiptType.IsPrimaryKey = false;
				colvarVendorReceiptType.IsForeignKey = false;
				colvarVendorReceiptType.IsReadOnly = false;
				
						colvarVendorReceiptType.DefaultSetting = @"((0))";
				colvarVendorReceiptType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorReceiptType);
				
				TableSchema.TableColumn colvarChangedExpireTime = new TableSchema.TableColumn(schema);
				colvarChangedExpireTime.ColumnName = "changed_expire_time";
				colvarChangedExpireTime.DataType = DbType.DateTime;
				colvarChangedExpireTime.MaxLength = 0;
				colvarChangedExpireTime.AutoIncrement = false;
				colvarChangedExpireTime.IsNullable = true;
				colvarChangedExpireTime.IsPrimaryKey = false;
				colvarChangedExpireTime.IsForeignKey = false;
				colvarChangedExpireTime.IsReadOnly = false;
				colvarChangedExpireTime.DefaultSetting = @"";
				colvarChangedExpireTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChangedExpireTime);
				
				TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
				colvarRemittanceType.ColumnName = "remittance_type";
				colvarRemittanceType.DataType = DbType.Int32;
				colvarRemittanceType.MaxLength = 0;
				colvarRemittanceType.AutoIncrement = false;
				colvarRemittanceType.IsNullable = false;
				colvarRemittanceType.IsPrimaryKey = false;
				colvarRemittanceType.IsForeignKey = false;
				colvarRemittanceType.IsReadOnly = false;
				
						colvarRemittanceType.DefaultSetting = @"((0))";
				colvarRemittanceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemittanceType);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 50;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarIsInputTaxFree = new TableSchema.TableColumn(schema);
				colvarIsInputTaxFree.ColumnName = "is_input_tax_free";
				colvarIsInputTaxFree.DataType = DbType.Boolean;
				colvarIsInputTaxFree.MaxLength = 0;
				colvarIsInputTaxFree.AutoIncrement = false;
				colvarIsInputTaxFree.IsNullable = false;
				colvarIsInputTaxFree.IsPrimaryKey = false;
				colvarIsInputTaxFree.IsForeignKey = false;
				colvarIsInputTaxFree.IsReadOnly = false;
				
						colvarIsInputTaxFree.DefaultSetting = @"((0))";
				colvarIsInputTaxFree.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsInputTaxFree);
				
				TableSchema.TableColumn colvarBookingSystemType = new TableSchema.TableColumn(schema);
				colvarBookingSystemType.ColumnName = "booking_system_type";
				colvarBookingSystemType.DataType = DbType.Int32;
				colvarBookingSystemType.MaxLength = 0;
				colvarBookingSystemType.AutoIncrement = false;
				colvarBookingSystemType.IsNullable = false;
				colvarBookingSystemType.IsPrimaryKey = false;
				colvarBookingSystemType.IsForeignKey = false;
				colvarBookingSystemType.IsReadOnly = false;
				
						colvarBookingSystemType.DefaultSetting = @"((0))";
				colvarBookingSystemType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBookingSystemType);
				
				TableSchema.TableColumn colvarAdvanceReservationDays = new TableSchema.TableColumn(schema);
				colvarAdvanceReservationDays.ColumnName = "advance_reservation_days";
				colvarAdvanceReservationDays.DataType = DbType.Int32;
				colvarAdvanceReservationDays.MaxLength = 0;
				colvarAdvanceReservationDays.AutoIncrement = false;
				colvarAdvanceReservationDays.IsNullable = false;
				colvarAdvanceReservationDays.IsPrimaryKey = false;
				colvarAdvanceReservationDays.IsForeignKey = false;
				colvarAdvanceReservationDays.IsReadOnly = false;
				
						colvarAdvanceReservationDays.DefaultSetting = @"((0))";
				colvarAdvanceReservationDays.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAdvanceReservationDays);
				
				TableSchema.TableColumn colvarCouponUsers = new TableSchema.TableColumn(schema);
				colvarCouponUsers.ColumnName = "coupon_users";
				colvarCouponUsers.DataType = DbType.Int32;
				colvarCouponUsers.MaxLength = 0;
				colvarCouponUsers.AutoIncrement = false;
				colvarCouponUsers.IsNullable = false;
				colvarCouponUsers.IsPrimaryKey = false;
				colvarCouponUsers.IsForeignKey = false;
				colvarCouponUsers.IsReadOnly = false;
				
						colvarCouponUsers.DefaultSetting = @"((0))";
				colvarCouponUsers.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponUsers);
				
				TableSchema.TableColumn colvarIsReserveLock = new TableSchema.TableColumn(schema);
				colvarIsReserveLock.ColumnName = "is_reserve_lock";
				colvarIsReserveLock.DataType = DbType.Boolean;
				colvarIsReserveLock.MaxLength = 0;
				colvarIsReserveLock.AutoIncrement = false;
				colvarIsReserveLock.IsNullable = false;
				colvarIsReserveLock.IsPrimaryKey = false;
				colvarIsReserveLock.IsForeignKey = false;
				colvarIsReserveLock.IsReadOnly = false;
				
						colvarIsReserveLock.DefaultSetting = @"((0))";
				colvarIsReserveLock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReserveLock);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_product",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("DealId")]
		[Bindable(true)]
		public int DealId 
		{
			get { return GetColumnValue<int>(Columns.DealId); }
			set { SetColumnValue(Columns.DealId, value); }
		}
		  
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int? Seq 
		{
			get { return GetColumnValue<int?>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name 
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("IsOnline")]
		[Bindable(true)]
		public bool? IsOnline 
		{
			get { return GetColumnValue<bool?>(Columns.IsOnline); }
			set { SetColumnValue(Columns.IsOnline, value); }
		}
		  
		[XmlAttribute("UseStartTime")]
		[Bindable(true)]
		public DateTime? UseStartTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UseStartTime); }
			set { SetColumnValue(Columns.UseStartTime, value); }
		}
		  
		[XmlAttribute("UseEndTime")]
		[Bindable(true)]
		public DateTime? UseEndTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UseEndTime); }
			set { SetColumnValue(Columns.UseEndTime, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int? Type 
		{
			get { return GetColumnValue<int?>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("OriginalPrice")]
		[Bindable(true)]
		public decimal? OriginalPrice 
		{
			get { return GetColumnValue<decimal?>(Columns.OriginalPrice); }
			set { SetColumnValue(Columns.OriginalPrice, value); }
		}
		  
		[XmlAttribute("Price")]
		[Bindable(true)]
		public decimal? Price 
		{
			get { return GetColumnValue<decimal?>(Columns.Price); }
			set { SetColumnValue(Columns.Price, value); }
		}
		  
		[XmlAttribute("Quantity")]
		[Bindable(true)]
		public int? Quantity 
		{
			get { return GetColumnValue<int?>(Columns.Quantity); }
			set { SetColumnValue(Columns.Quantity, value); }
		}
		  
		[XmlAttribute("OrderLimit")]
		[Bindable(true)]
		public int? OrderLimit 
		{
			get { return GetColumnValue<int?>(Columns.OrderLimit); }
			set { SetColumnValue(Columns.OrderLimit, value); }
		}
		  
		[XmlAttribute("OrderLimitUser")]
		[Bindable(true)]
		public int? OrderLimitUser 
		{
			get { return GetColumnValue<int?>(Columns.OrderLimitUser); }
			set { SetColumnValue(Columns.OrderLimitUser, value); }
		}
		  
		[XmlAttribute("OrderCount")]
		[Bindable(true)]
		public int? OrderCount 
		{
			get { return GetColumnValue<int?>(Columns.OrderCount); }
			set { SetColumnValue(Columns.OrderCount, value); }
		}
		  
		[XmlAttribute("IsNoRefund")]
		[Bindable(true)]
		public bool IsNoRefund 
		{
			get { return GetColumnValue<bool>(Columns.IsNoRefund); }
			set { SetColumnValue(Columns.IsNoRefund, value); }
		}
		  
		[XmlAttribute("IsExpireNoRefund")]
		[Bindable(true)]
		public bool IsExpireNoRefund 
		{
			get { return GetColumnValue<bool>(Columns.IsExpireNoRefund); }
			set { SetColumnValue(Columns.IsExpireNoRefund, value); }
		}
		  
		[XmlAttribute("IsTaxFree")]
		[Bindable(true)]
		public bool IsTaxFree 
		{
			get { return GetColumnValue<bool>(Columns.IsTaxFree); }
			set { SetColumnValue(Columns.IsTaxFree, value); }
		}
		  
		[XmlAttribute("IsSoldout")]
		[Bindable(true)]
		public bool? IsSoldout 
		{
			get { return GetColumnValue<bool?>(Columns.IsSoldout); }
			set { SetColumnValue(Columns.IsSoldout, value); }
		}
		  
		[XmlAttribute("IsInStore")]
		[Bindable(true)]
		public bool IsInStore 
		{
			get { return GetColumnValue<bool>(Columns.IsInStore); }
			set { SetColumnValue(Columns.IsInStore, value); }
		}
		  
		[XmlAttribute("InStoreDesc")]
		[Bindable(true)]
		public string InStoreDesc 
		{
			get { return GetColumnValue<string>(Columns.InStoreDesc); }
			set { SetColumnValue(Columns.InStoreDesc, value); }
		}
		  
		[XmlAttribute("IsHomeDelivery")]
		[Bindable(true)]
		public bool IsHomeDelivery 
		{
			get { return GetColumnValue<bool>(Columns.IsHomeDelivery); }
			set { SetColumnValue(Columns.IsHomeDelivery, value); }
		}
		  
		[XmlAttribute("HomeDeliveryDesc")]
		[Bindable(true)]
		public string HomeDeliveryDesc 
		{
			get { return GetColumnValue<string>(Columns.HomeDeliveryDesc); }
			set { SetColumnValue(Columns.HomeDeliveryDesc, value); }
		}
		  
		[XmlAttribute("Sms")]
		[Bindable(true)]
		public string Sms 
		{
			get { return GetColumnValue<string>(Columns.Sms); }
			set { SetColumnValue(Columns.Sms, value); }
		}
		  
		[XmlAttribute("IsSmsClose")]
		[Bindable(true)]
		public bool IsSmsClose 
		{
			get { return GetColumnValue<bool>(Columns.IsSmsClose); }
			set { SetColumnValue(Columns.IsSmsClose, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("StartTime")]
		[Bindable(true)]
		public DateTime? StartTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.StartTime); }
			set { SetColumnValue(Columns.StartTime, value); }
		}
		  
		[XmlAttribute("EndTime")]
		[Bindable(true)]
		public DateTime? EndTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.EndTime); }
			set { SetColumnValue(Columns.EndTime, value); }
		}
		  
		[XmlAttribute("Pic")]
		[Bindable(true)]
		public string Pic 
		{
			get { return GetColumnValue<string>(Columns.Pic); }
			set { SetColumnValue(Columns.Pic, value); }
		}
		  
		[XmlAttribute("HasBranchStoreQuantityLimit")]
		[Bindable(true)]
		public bool HasBranchStoreQuantityLimit 
		{
			get { return GetColumnValue<bool>(Columns.HasBranchStoreQuantityLimit); }
			set { SetColumnValue(Columns.HasBranchStoreQuantityLimit, value); }
		}
		  
		[XmlAttribute("HomeDeliveryQuantity")]
		[Bindable(true)]
		public int? HomeDeliveryQuantity 
		{
			get { return GetColumnValue<int?>(Columns.HomeDeliveryQuantity); }
			set { SetColumnValue(Columns.HomeDeliveryQuantity, value); }
		}
		  
		[XmlAttribute("DeliveryIslands")]
		[Bindable(true)]
		public bool DeliveryIslands 
		{
			get { return GetColumnValue<bool>(Columns.DeliveryIslands); }
			set { SetColumnValue(Columns.DeliveryIslands, value); }
		}
		  
		[XmlAttribute("ShippedDate")]
		[Bindable(true)]
		public DateTime? ShippedDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ShippedDate); }
			set { SetColumnValue(Columns.ShippedDate, value); }
		}
		  
		[XmlAttribute("TrustType")]
		[Bindable(true)]
		public int TrustType 
		{
			get { return GetColumnValue<int>(Columns.TrustType); }
			set { SetColumnValue(Columns.TrustType, value); }
		}
		  
		[XmlAttribute("IsShowPriceDiscount")]
		[Bindable(true)]
		public bool IsShowPriceDiscount 
		{
			get { return GetColumnValue<bool>(Columns.IsShowPriceDiscount); }
			set { SetColumnValue(Columns.IsShowPriceDiscount, value); }
		}
		  
		[XmlAttribute("IsInvoiceCreate")]
		[Bindable(true)]
		public bool IsInvoiceCreate 
		{
			get { return GetColumnValue<bool>(Columns.IsInvoiceCreate); }
			set { SetColumnValue(Columns.IsInvoiceCreate, value); }
		}
		  
		[XmlAttribute("PayToCompany")]
		[Bindable(true)]
		public bool PayToCompany 
		{
			get { return GetColumnValue<bool>(Columns.PayToCompany); }
			set { SetColumnValue(Columns.PayToCompany, value); }
		}
		  
		[XmlAttribute("IsCombo")]
		[Bindable(true)]
		public bool IsCombo 
		{
			get { return GetColumnValue<bool>(Columns.IsCombo); }
			set { SetColumnValue(Columns.IsCombo, value); }
		}
		  
		[XmlAttribute("IsDaysNoRefund")]
		[Bindable(true)]
		public bool IsDaysNoRefund 
		{
			get { return GetColumnValue<bool>(Columns.IsDaysNoRefund); }
			set { SetColumnValue(Columns.IsDaysNoRefund, value); }
		}
		  
		[XmlAttribute("VendorBillingModel")]
		[Bindable(true)]
		public int VendorBillingModel 
		{
			get { return GetColumnValue<int>(Columns.VendorBillingModel); }
			set { SetColumnValue(Columns.VendorBillingModel, value); }
		}
		  
		[XmlAttribute("VendorReceiptType")]
		[Bindable(true)]
		public int VendorReceiptType 
		{
			get { return GetColumnValue<int>(Columns.VendorReceiptType); }
			set { SetColumnValue(Columns.VendorReceiptType, value); }
		}
		  
		[XmlAttribute("ChangedExpireTime")]
		[Bindable(true)]
		public DateTime? ChangedExpireTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ChangedExpireTime); }
			set { SetColumnValue(Columns.ChangedExpireTime, value); }
		}
		  
		[XmlAttribute("RemittanceType")]
		[Bindable(true)]
		public int RemittanceType 
		{
			get { return GetColumnValue<int>(Columns.RemittanceType); }
			set { SetColumnValue(Columns.RemittanceType, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("IsInputTaxFree")]
		[Bindable(true)]
		public bool IsInputTaxFree 
		{
			get { return GetColumnValue<bool>(Columns.IsInputTaxFree); }
			set { SetColumnValue(Columns.IsInputTaxFree, value); }
		}
		  
		[XmlAttribute("BookingSystemType")]
		[Bindable(true)]
		public int BookingSystemType 
		{
			get { return GetColumnValue<int>(Columns.BookingSystemType); }
			set { SetColumnValue(Columns.BookingSystemType, value); }
		}
		  
		[XmlAttribute("AdvanceReservationDays")]
		[Bindable(true)]
		public int AdvanceReservationDays 
		{
			get { return GetColumnValue<int>(Columns.AdvanceReservationDays); }
			set { SetColumnValue(Columns.AdvanceReservationDays, value); }
		}
		  
		[XmlAttribute("CouponUsers")]
		[Bindable(true)]
		public int CouponUsers 
		{
			get { return GetColumnValue<int>(Columns.CouponUsers); }
			set { SetColumnValue(Columns.CouponUsers, value); }
		}
		  
		[XmlAttribute("IsReserveLock")]
		[Bindable(true)]
		public bool IsReserveLock 
		{
			get { return GetColumnValue<bool>(Columns.IsReserveLock); }
			set { SetColumnValue(Columns.IsReserveLock, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DealIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOnlineColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn UseStartTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn UseEndTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn OriginalPriceColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn PriceColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn QuantityColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderLimitColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderLimitUserColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderCountColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn IsNoRefundColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn IsExpireNoRefundColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn IsTaxFreeColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn IsSoldoutColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn IsInStoreColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn InStoreDescColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn IsHomeDeliveryColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn HomeDeliveryDescColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn SmsColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn IsSmsCloseColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn StartTimeColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn EndTimeColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn PicColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn HasBranchStoreQuantityLimitColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn HomeDeliveryQuantityColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryIslandsColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn ShippedDateColumn
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustTypeColumn
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn IsShowPriceDiscountColumn
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        public static TableSchema.TableColumn IsInvoiceCreateColumn
        {
            get { return Schema.Columns[40]; }
        }
        
        
        
        public static TableSchema.TableColumn PayToCompanyColumn
        {
            get { return Schema.Columns[41]; }
        }
        
        
        
        public static TableSchema.TableColumn IsComboColumn
        {
            get { return Schema.Columns[42]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDaysNoRefundColumn
        {
            get { return Schema.Columns[43]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorBillingModelColumn
        {
            get { return Schema.Columns[44]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorReceiptTypeColumn
        {
            get { return Schema.Columns[45]; }
        }
        
        
        
        public static TableSchema.TableColumn ChangedExpireTimeColumn
        {
            get { return Schema.Columns[46]; }
        }
        
        
        
        public static TableSchema.TableColumn RemittanceTypeColumn
        {
            get { return Schema.Columns[47]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[48]; }
        }
        
        
        
        public static TableSchema.TableColumn IsInputTaxFreeColumn
        {
            get { return Schema.Columns[49]; }
        }
        
        
        
        public static TableSchema.TableColumn BookingSystemTypeColumn
        {
            get { return Schema.Columns[50]; }
        }
        
        
        
        public static TableSchema.TableColumn AdvanceReservationDaysColumn
        {
            get { return Schema.Columns[51]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponUsersColumn
        {
            get { return Schema.Columns[52]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReserveLockColumn
        {
            get { return Schema.Columns[53]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Guid = @"guid";
			 public static string DealId = @"deal_id";
			 public static string Seq = @"seq";
			 public static string SellerGuid = @"seller_guid";
			 public static string Name = @"name";
			 public static string Description = @"description";
			 public static string IsOnline = @"is_online";
			 public static string UseStartTime = @"use_start_time";
			 public static string UseEndTime = @"use_end_time";
			 public static string Type = @"type";
			 public static string OriginalPrice = @"original_price";
			 public static string Price = @"price";
			 public static string Quantity = @"quantity";
			 public static string OrderLimit = @"order_limit";
			 public static string OrderLimitUser = @"order_limit_user";
			 public static string OrderCount = @"order_count";
			 public static string IsNoRefund = @"is_no_refund";
			 public static string IsExpireNoRefund = @"is_expire_no_refund";
			 public static string IsTaxFree = @"is_tax_free";
			 public static string IsSoldout = @"is_soldout";
			 public static string IsInStore = @"is_in_store";
			 public static string InStoreDesc = @"in_store_desc";
			 public static string IsHomeDelivery = @"is_home_delivery";
			 public static string HomeDeliveryDesc = @"home_delivery_desc";
			 public static string Sms = @"sms";
			 public static string IsSmsClose = @"is_sms_close";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string StartTime = @"start_time";
			 public static string EndTime = @"end_time";
			 public static string Pic = @"pic";
			 public static string HasBranchStoreQuantityLimit = @"has_branch_store_quantity_limit";
			 public static string HomeDeliveryQuantity = @"home_delivery_quantity";
			 public static string DeliveryIslands = @"deliveryIslands";
			 public static string ShippedDate = @"shipped_date";
			 public static string TrustType = @"trust_type";
			 public static string IsShowPriceDiscount = @"is_show_Price_discount";
			 public static string IsInvoiceCreate = @"Is_Invoice_Create";
			 public static string PayToCompany = @"pay_to_company";
			 public static string IsCombo = @"is_combo";
			 public static string IsDaysNoRefund = @"is_days_no_refund";
			 public static string VendorBillingModel = @"vendor_billing_model";
			 public static string VendorReceiptType = @"vendor_receipt_type";
			 public static string ChangedExpireTime = @"changed_expire_time";
			 public static string RemittanceType = @"remittance_type";
			 public static string Message = @"message";
			 public static string IsInputTaxFree = @"is_input_tax_free";
			 public static string BookingSystemType = @"booking_system_type";
			 public static string AdvanceReservationDays = @"advance_reservation_days";
			 public static string CouponUsers = @"coupon_users";
			 public static string IsReserveLock = @"is_reserve_lock";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVbsMemberDetail class.
    /// </summary>
    [Serializable]
    public partial class ViewVbsMemberDetailCollection : ReadOnlyList<ViewVbsMemberDetail, ViewVbsMemberDetailCollection>
    {        
        public ViewVbsMemberDetailCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vbs_member_detail view.
    /// </summary>
    [Serializable]
    public partial class ViewVbsMemberDetail : ReadOnlyRecord<ViewVbsMemberDetail>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vbs_member_detail", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = true;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarLoginId = new TableSchema.TableColumn(schema);
                colvarLoginId.ColumnName = "login_id";
                colvarLoginId.DataType = DbType.String;
                colvarLoginId.MaxLength = 256;
                colvarLoginId.AutoIncrement = false;
                colvarLoginId.IsNullable = false;
                colvarLoginId.IsPrimaryKey = false;
                colvarLoginId.IsForeignKey = false;
                colvarLoginId.IsReadOnly = false;
                
                schema.Columns.Add(colvarLoginId);
                
                TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
                colvarEmail.ColumnName = "email";
                colvarEmail.DataType = DbType.String;
                colvarEmail.MaxLength = 256;
                colvarEmail.AutoIncrement = false;
                colvarEmail.IsNullable = true;
                colvarEmail.IsPrimaryKey = false;
                colvarEmail.IsForeignKey = false;
                colvarEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarEmail);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 20;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                TableSchema.TableColumn colvarTownshipId = new TableSchema.TableColumn(schema);
                colvarTownshipId.ColumnName = "township_id";
                colvarTownshipId.DataType = DbType.Int32;
                colvarTownshipId.MaxLength = 0;
                colvarTownshipId.AutoIncrement = false;
                colvarTownshipId.IsNullable = true;
                colvarTownshipId.IsPrimaryKey = false;
                colvarTownshipId.IsForeignKey = false;
                colvarTownshipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTownshipId);
                
                TableSchema.TableColumn colvarInvoiceTitle = new TableSchema.TableColumn(schema);
                colvarInvoiceTitle.ColumnName = "invoice_title";
                colvarInvoiceTitle.DataType = DbType.String;
                colvarInvoiceTitle.MaxLength = 250;
                colvarInvoiceTitle.AutoIncrement = false;
                colvarInvoiceTitle.IsNullable = true;
                colvarInvoiceTitle.IsPrimaryKey = false;
                colvarInvoiceTitle.IsForeignKey = false;
                colvarInvoiceTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceTitle);
                
                TableSchema.TableColumn colvarInvoiceComId = new TableSchema.TableColumn(schema);
                colvarInvoiceComId.ColumnName = "invoice_com_id";
                colvarInvoiceComId.DataType = DbType.AnsiString;
                colvarInvoiceComId.MaxLength = 10;
                colvarInvoiceComId.AutoIncrement = false;
                colvarInvoiceComId.IsNullable = true;
                colvarInvoiceComId.IsPrimaryKey = false;
                colvarInvoiceComId.IsForeignKey = false;
                colvarInvoiceComId.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceComId);
                
                TableSchema.TableColumn colvarInvoiceName = new TableSchema.TableColumn(schema);
                colvarInvoiceName.ColumnName = "invoice_name";
                colvarInvoiceName.DataType = DbType.String;
                colvarInvoiceName.MaxLength = 50;
                colvarInvoiceName.AutoIncrement = false;
                colvarInvoiceName.IsNullable = true;
                colvarInvoiceName.IsPrimaryKey = false;
                colvarInvoiceName.IsForeignKey = false;
                colvarInvoiceName.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceName);
                
                TableSchema.TableColumn colvarInvoiceAddress = new TableSchema.TableColumn(schema);
                colvarInvoiceAddress.ColumnName = "invoice_address";
                colvarInvoiceAddress.DataType = DbType.String;
                colvarInvoiceAddress.MaxLength = 250;
                colvarInvoiceAddress.AutoIncrement = false;
                colvarInvoiceAddress.IsNullable = true;
                colvarInvoiceAddress.IsPrimaryKey = false;
                colvarInvoiceAddress.IsForeignKey = false;
                colvarInvoiceAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceAddress);
                
                TableSchema.TableColumn colvarAccountBankCode = new TableSchema.TableColumn(schema);
                colvarAccountBankCode.ColumnName = "account_bank_code";
                colvarAccountBankCode.DataType = DbType.AnsiString;
                colvarAccountBankCode.MaxLength = 10;
                colvarAccountBankCode.AutoIncrement = false;
                colvarAccountBankCode.IsNullable = true;
                colvarAccountBankCode.IsPrimaryKey = false;
                colvarAccountBankCode.IsForeignKey = false;
                colvarAccountBankCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountBankCode);
                
                TableSchema.TableColumn colvarAccountBranchCode = new TableSchema.TableColumn(schema);
                colvarAccountBranchCode.ColumnName = "account_branch_code";
                colvarAccountBranchCode.DataType = DbType.AnsiString;
                colvarAccountBranchCode.MaxLength = 10;
                colvarAccountBranchCode.AutoIncrement = false;
                colvarAccountBranchCode.IsNullable = true;
                colvarAccountBranchCode.IsPrimaryKey = false;
                colvarAccountBranchCode.IsForeignKey = false;
                colvarAccountBranchCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountBranchCode);
                
                TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
                colvarAccountName.ColumnName = "account_name";
                colvarAccountName.DataType = DbType.String;
                colvarAccountName.MaxLength = 50;
                colvarAccountName.AutoIncrement = false;
                colvarAccountName.IsNullable = true;
                colvarAccountName.IsPrimaryKey = false;
                colvarAccountName.IsForeignKey = false;
                colvarAccountName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountName);
                
                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.AnsiString;
                colvarAccountId.MaxLength = 20;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = true;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountId);
                
                TableSchema.TableColumn colvarAccountNo = new TableSchema.TableColumn(schema);
                colvarAccountNo.ColumnName = "account_no";
                colvarAccountNo.DataType = DbType.AnsiString;
                colvarAccountNo.MaxLength = 20;
                colvarAccountNo.AutoIncrement = false;
                colvarAccountNo.IsNullable = true;
                colvarAccountNo.IsPrimaryKey = false;
                colvarAccountNo.IsForeignKey = false;
                colvarAccountNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountNo);
                
                TableSchema.TableColumn colvarLastPasswordChangedDate = new TableSchema.TableColumn(schema);
                colvarLastPasswordChangedDate.ColumnName = "last_password_changed_date";
                colvarLastPasswordChangedDate.DataType = DbType.DateTime;
                colvarLastPasswordChangedDate.MaxLength = 0;
                colvarLastPasswordChangedDate.AutoIncrement = false;
                colvarLastPasswordChangedDate.IsNullable = true;
                colvarLastPasswordChangedDate.IsPrimaryKey = false;
                colvarLastPasswordChangedDate.IsForeignKey = false;
                colvarLastPasswordChangedDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastPasswordChangedDate);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vbs_member_detail",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVbsMemberDetail()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVbsMemberDetail(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVbsMemberDetail(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVbsMemberDetail(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int? UserId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("LoginId")]
        [Bindable(true)]
        public string LoginId 
	    {
		    get
		    {
			    return GetColumnValue<string>("login_id");
		    }
            set 
		    {
			    SetColumnValue("login_id", value);
            }
        }
	      
        [XmlAttribute("Email")]
        [Bindable(true)]
        public string Email 
	    {
		    get
		    {
			    return GetColumnValue<string>("email");
		    }
            set 
		    {
			    SetColumnValue("email", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	      
        [XmlAttribute("TownshipId")]
        [Bindable(true)]
        public int? TownshipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("township_id");
		    }
            set 
		    {
			    SetColumnValue("township_id", value);
            }
        }
	      
        [XmlAttribute("InvoiceTitle")]
        [Bindable(true)]
        public string InvoiceTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_title");
		    }
            set 
		    {
			    SetColumnValue("invoice_title", value);
            }
        }
	      
        [XmlAttribute("InvoiceComId")]
        [Bindable(true)]
        public string InvoiceComId 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_com_id");
		    }
            set 
		    {
			    SetColumnValue("invoice_com_id", value);
            }
        }
	      
        [XmlAttribute("InvoiceName")]
        [Bindable(true)]
        public string InvoiceName 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_name");
		    }
            set 
		    {
			    SetColumnValue("invoice_name", value);
            }
        }
	      
        [XmlAttribute("InvoiceAddress")]
        [Bindable(true)]
        public string InvoiceAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_address");
		    }
            set 
		    {
			    SetColumnValue("invoice_address", value);
            }
        }
	      
        [XmlAttribute("AccountBankCode")]
        [Bindable(true)]
        public string AccountBankCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_bank_code");
		    }
            set 
		    {
			    SetColumnValue("account_bank_code", value);
            }
        }
	      
        [XmlAttribute("AccountBranchCode")]
        [Bindable(true)]
        public string AccountBranchCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_branch_code");
		    }
            set 
		    {
			    SetColumnValue("account_branch_code", value);
            }
        }
	      
        [XmlAttribute("AccountName")]
        [Bindable(true)]
        public string AccountName 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_name");
		    }
            set 
		    {
			    SetColumnValue("account_name", value);
            }
        }
	      
        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_id");
		    }
            set 
		    {
			    SetColumnValue("account_id", value);
            }
        }
	      
        [XmlAttribute("AccountNo")]
        [Bindable(true)]
        public string AccountNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_no");
		    }
            set 
		    {
			    SetColumnValue("account_no", value);
            }
        }
	      
        [XmlAttribute("LastPasswordChangedDate")]
        [Bindable(true)]
        public DateTime? LastPasswordChangedDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("last_password_changed_date");
		    }
            set 
		    {
			    SetColumnValue("last_password_changed_date", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string UserId = @"user_id";
            
            public static string LoginId = @"login_id";
            
            public static string Email = @"email";
            
            public static string Mobile = @"mobile";
            
            public static string TownshipId = @"township_id";
            
            public static string InvoiceTitle = @"invoice_title";
            
            public static string InvoiceComId = @"invoice_com_id";
            
            public static string InvoiceName = @"invoice_name";
            
            public static string InvoiceAddress = @"invoice_address";
            
            public static string AccountBankCode = @"account_bank_code";
            
            public static string AccountBranchCode = @"account_branch_code";
            
            public static string AccountName = @"account_name";
            
            public static string AccountId = @"account_id";
            
            public static string AccountNo = @"account_no";
            
            public static string LastPasswordChangedDate = @"last_password_changed_date";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

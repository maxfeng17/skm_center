using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class WmsPurchaseOrderCollection : RepositoryList<WmsPurchaseOrder, WmsPurchaseOrderCollection>
	{
			public WmsPurchaseOrderCollection() {}

			public WmsPurchaseOrderCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							WmsPurchaseOrder o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class WmsPurchaseOrder : RepositoryRecord<WmsPurchaseOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		public WmsPurchaseOrder()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public WmsPurchaseOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("wms_purchase_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);

				TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
				colvarItemGuid.ColumnName = "item_guid";
				colvarItemGuid.DataType = DbType.Guid;
				colvarItemGuid.MaxLength = 0;
				colvarItemGuid.AutoIncrement = false;
				colvarItemGuid.IsNullable = false;
				colvarItemGuid.IsPrimaryKey = false;
				colvarItemGuid.IsForeignKey = false;
				colvarItemGuid.IsReadOnly = false;
				colvarItemGuid.DefaultSetting = @"";
				colvarItemGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemGuid);

				TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
				colvarQty.ColumnName = "qty";
				colvarQty.DataType = DbType.Int32;
				colvarQty.MaxLength = 0;
				colvarQty.AutoIncrement = false;
				colvarQty.IsNullable = false;
				colvarQty.IsPrimaryKey = false;
				colvarQty.IsForeignKey = false;
				colvarQty.IsReadOnly = false;
				colvarQty.DefaultSetting = @"";
				colvarQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQty);

				TableSchema.TableColumn colvarSalesId = new TableSchema.TableColumn(schema);
				colvarSalesId.ColumnName = "sales_id";
				colvarSalesId.DataType = DbType.Int32;
				colvarSalesId.MaxLength = 0;
				colvarSalesId.AutoIncrement = false;
				colvarSalesId.IsNullable = false;
				colvarSalesId.IsPrimaryKey = false;
				colvarSalesId.IsForeignKey = false;
				colvarSalesId.IsReadOnly = false;
				colvarSalesId.DefaultSetting = @"";
				colvarSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesId);

				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);

				TableSchema.TableColumn colvarPchomePurchaseOrderId = new TableSchema.TableColumn(schema);
				colvarPchomePurchaseOrderId.ColumnName = "pchome_purchase_order_id";
				colvarPchomePurchaseOrderId.DataType = DbType.AnsiString;
				colvarPchomePurchaseOrderId.MaxLength = 50;
				colvarPchomePurchaseOrderId.AutoIncrement = false;
				colvarPchomePurchaseOrderId.IsNullable = true;
				colvarPchomePurchaseOrderId.IsPrimaryKey = false;
				colvarPchomePurchaseOrderId.IsForeignKey = false;
				colvarPchomePurchaseOrderId.IsReadOnly = false;
				colvarPchomePurchaseOrderId.DefaultSetting = @"";
				colvarPchomePurchaseOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPchomePurchaseOrderId);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.AnsiString;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.AnsiString;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarActualQty = new TableSchema.TableColumn(schema);
				colvarActualQty.ColumnName = "actual_qty";
				colvarActualQty.DataType = DbType.Int32;
				colvarActualQty.MaxLength = 0;
				colvarActualQty.AutoIncrement = false;
				colvarActualQty.IsNullable = true;
				colvarActualQty.IsPrimaryKey = false;
				colvarActualQty.IsForeignKey = false;
				colvarActualQty.IsReadOnly = false;
				colvarActualQty.DefaultSetting = @"";
				colvarActualQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActualQty);

				TableSchema.TableColumn colvarRefundQty = new TableSchema.TableColumn(schema);
				colvarRefundQty.ColumnName = "refund_qty";
				colvarRefundQty.DataType = DbType.Int32;
				colvarRefundQty.MaxLength = 0;
				colvarRefundQty.AutoIncrement = false;
				colvarRefundQty.IsNullable = true;
				colvarRefundQty.IsPrimaryKey = false;
				colvarRefundQty.IsForeignKey = false;
				colvarRefundQty.IsReadOnly = false;
				colvarRefundQty.DefaultSetting = @"";
				colvarRefundQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundQty);

				TableSchema.TableColumn colvarInvalidation = new TableSchema.TableColumn(schema);
				colvarInvalidation.ColumnName = "invalidation";
				colvarInvalidation.DataType = DbType.Int32;
				colvarInvalidation.MaxLength = 0;
				colvarInvalidation.AutoIncrement = false;
				colvarInvalidation.IsNullable = true;
				colvarInvalidation.IsPrimaryKey = false;
				colvarInvalidation.IsForeignKey = false;
				colvarInvalidation.IsReadOnly = false;
				colvarInvalidation.DefaultSetting = @"((0))";
				colvarInvalidation.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvalidation);

				TableSchema.TableColumn colvarExpireDate = new TableSchema.TableColumn(schema);
				colvarExpireDate.ColumnName = "expire_date";
				colvarExpireDate.DataType = DbType.DateTime;
				colvarExpireDate.MaxLength = 0;
				colvarExpireDate.AutoIncrement = false;
				colvarExpireDate.IsNullable = true;
				colvarExpireDate.IsPrimaryKey = false;
				colvarExpireDate.IsForeignKey = false;
				colvarExpireDate.IsReadOnly = false;
				colvarExpireDate.DefaultSetting = @"";
				colvarExpireDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExpireDate);

				TableSchema.TableColumn colvarArrivalDate = new TableSchema.TableColumn(schema);
				colvarArrivalDate.ColumnName = "arrival_date";
				colvarArrivalDate.DataType = DbType.DateTime;
				colvarArrivalDate.MaxLength = 0;
				colvarArrivalDate.AutoIncrement = false;
				colvarArrivalDate.IsNullable = true;
				colvarArrivalDate.IsPrimaryKey = false;
				colvarArrivalDate.IsForeignKey = false;
				colvarArrivalDate.IsReadOnly = false;
				colvarArrivalDate.DefaultSetting = @"";
				colvarArrivalDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarArrivalDate);

				TableSchema.TableColumn colvarEntryDate = new TableSchema.TableColumn(schema);
				colvarEntryDate.ColumnName = "entry_date";
				colvarEntryDate.DataType = DbType.DateTime;
				colvarEntryDate.MaxLength = 0;
				colvarEntryDate.AutoIncrement = false;
				colvarEntryDate.IsNullable = true;
				colvarEntryDate.IsPrimaryKey = false;
				colvarEntryDate.IsForeignKey = false;
				colvarEntryDate.IsReadOnly = false;
				colvarEntryDate.DefaultSetting = @"";
				colvarEntryDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEntryDate);

				TableSchema.TableColumn colvarApplyDate = new TableSchema.TableColumn(schema);
				colvarApplyDate.ColumnName = "apply_date";
				colvarApplyDate.DataType = DbType.DateTime;
				colvarApplyDate.MaxLength = 0;
				colvarApplyDate.AutoIncrement = false;
				colvarApplyDate.IsNullable = true;
				colvarApplyDate.IsPrimaryKey = false;
				colvarApplyDate.IsForeignKey = false;
				colvarApplyDate.IsReadOnly = false;
				colvarApplyDate.DefaultSetting = @"";
				colvarApplyDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApplyDate);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("wms_purchase_order",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}

		[XmlAttribute("ItemGuid")]
		[Bindable(true)]
		public Guid ItemGuid
		{
			get { return GetColumnValue<Guid>(Columns.ItemGuid); }
			set { SetColumnValue(Columns.ItemGuid, value); }
		}

		[XmlAttribute("Qty")]
		[Bindable(true)]
		public int Qty
		{
			get { return GetColumnValue<int>(Columns.Qty); }
			set { SetColumnValue(Columns.Qty, value); }
		}

		[XmlAttribute("SalesId")]
		[Bindable(true)]
		public int SalesId
		{
			get { return GetColumnValue<int>(Columns.SalesId); }
			set { SetColumnValue(Columns.SalesId, value); }
		}

		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}

		[XmlAttribute("PchomePurchaseOrderId")]
		[Bindable(true)]
		public string PchomePurchaseOrderId
		{
			get { return GetColumnValue<string>(Columns.PchomePurchaseOrderId); }
			set { SetColumnValue(Columns.PchomePurchaseOrderId, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("ActualQty")]
		[Bindable(true)]
		public int? ActualQty
		{
			get { return GetColumnValue<int?>(Columns.ActualQty); }
			set { SetColumnValue(Columns.ActualQty, value); }
		}

		[XmlAttribute("RefundQty")]
		[Bindable(true)]
		public int? RefundQty
		{
			get { return GetColumnValue<int?>(Columns.RefundQty); }
			set { SetColumnValue(Columns.RefundQty, value); }
		}

		[XmlAttribute("Invalidation")]
		[Bindable(true)]
		public int? Invalidation
		{
			get { return GetColumnValue<int?>(Columns.Invalidation); }
			set { SetColumnValue(Columns.Invalidation, value); }
		}

		[XmlAttribute("ExpireDate")]
		[Bindable(true)]
		public DateTime? ExpireDate
		{
			get { return GetColumnValue<DateTime?>(Columns.ExpireDate); }
			set { SetColumnValue(Columns.ExpireDate, value); }
		}

		[XmlAttribute("ArrivalDate")]
		[Bindable(true)]
		public DateTime? ArrivalDate
		{
			get { return GetColumnValue<DateTime?>(Columns.ArrivalDate); }
			set { SetColumnValue(Columns.ArrivalDate, value); }
		}

		[XmlAttribute("EntryDate")]
		[Bindable(true)]
		public DateTime? EntryDate
		{
			get { return GetColumnValue<DateTime?>(Columns.EntryDate); }
			set { SetColumnValue(Columns.EntryDate, value); }
		}

		[XmlAttribute("ApplyDate")]
		[Bindable(true)]
		public DateTime? ApplyDate
		{
			get { return GetColumnValue<DateTime?>(Columns.ApplyDate); }
			set { SetColumnValue(Columns.ApplyDate, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn GuidColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn ItemGuidColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn QtyColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn SalesIdColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn StatusColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn PchomePurchaseOrderIdColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn ModifyIdColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn ActualQtyColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn RefundQtyColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn InvalidationColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn ExpireDateColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn ArrivalDateColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn EntryDateColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn ApplyDateColumn
		{
			get { return Schema.Columns[16]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Guid = @"guid";
			public static string ItemGuid = @"item_guid";
			public static string Qty = @"qty";
			public static string SalesId = @"sales_id";
			public static string Status = @"status";
			public static string PchomePurchaseOrderId = @"pchome_purchase_order_id";
			public static string CreateId = @"create_id";
			public static string CreateTime = @"create_time";
			public static string ModifyId = @"modify_id";
			public static string ModifyTime = @"modify_time";
			public static string ActualQty = @"actual_qty";
			public static string RefundQty = @"refund_qty";
			public static string Invalidation = @"invalidation";
			public static string ExpireDate = @"expire_date";
			public static string ArrivalDate = @"arrival_date";
			public static string EntryDate = @"entry_date";
			public static string ApplyDate = @"apply_date";
		}

		#endregion

	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HamiCategory class.
	/// </summary>
    [Serializable]
	public partial class HamiCategoryCollection : RepositoryList<HamiCategory, HamiCategoryCollection>
	{	   
		public HamiCategoryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HamiCategoryCollection</returns>
		public HamiCategoryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HamiCategory o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hami_category table.
	/// </summary>
	[Serializable]
	public partial class HamiCategory : RepositoryRecord<HamiCategory>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HamiCategory()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HamiCategory(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hami_category", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarDealTypeCodeId = new TableSchema.TableColumn(schema);
				colvarDealTypeCodeId.ColumnName = "deal_type_code_id";
				colvarDealTypeCodeId.DataType = DbType.Int32;
				colvarDealTypeCodeId.MaxLength = 0;
				colvarDealTypeCodeId.AutoIncrement = false;
				colvarDealTypeCodeId.IsNullable = false;
				colvarDealTypeCodeId.IsPrimaryKey = true;
				colvarDealTypeCodeId.IsForeignKey = false;
				colvarDealTypeCodeId.IsReadOnly = false;
				colvarDealTypeCodeId.DefaultSetting = @"";
				colvarDealTypeCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealTypeCodeId);
				
				TableSchema.TableColumn colvarHamiCategoryX = new TableSchema.TableColumn(schema);
				colvarHamiCategoryX.ColumnName = "hami_category";
				colvarHamiCategoryX.DataType = DbType.AnsiString;
				colvarHamiCategoryX.MaxLength = 4;
				colvarHamiCategoryX.AutoIncrement = false;
				colvarHamiCategoryX.IsNullable = true;
				colvarHamiCategoryX.IsPrimaryKey = false;
				colvarHamiCategoryX.IsForeignKey = false;
				colvarHamiCategoryX.IsReadOnly = false;
				colvarHamiCategoryX.DefaultSetting = @"";
				colvarHamiCategoryX.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHamiCategoryX);
				
				TableSchema.TableColumn colvarHamiCategoryDesc = new TableSchema.TableColumn(schema);
				colvarHamiCategoryDesc.ColumnName = "hami_category_desc";
				colvarHamiCategoryDesc.DataType = DbType.String;
				colvarHamiCategoryDesc.MaxLength = 100;
				colvarHamiCategoryDesc.AutoIncrement = false;
				colvarHamiCategoryDesc.IsNullable = true;
				colvarHamiCategoryDesc.IsPrimaryKey = false;
				colvarHamiCategoryDesc.IsForeignKey = false;
				colvarHamiCategoryDesc.IsReadOnly = false;
				colvarHamiCategoryDesc.DefaultSetting = @"";
				colvarHamiCategoryDesc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHamiCategoryDesc);
				
				TableSchema.TableColumn colvarHtcCategoryDesc = new TableSchema.TableColumn(schema);
				colvarHtcCategoryDesc.ColumnName = "htc_category_desc";
				colvarHtcCategoryDesc.DataType = DbType.String;
				colvarHtcCategoryDesc.MaxLength = 100;
				colvarHtcCategoryDesc.AutoIncrement = false;
				colvarHtcCategoryDesc.IsNullable = true;
				colvarHtcCategoryDesc.IsPrimaryKey = false;
				colvarHtcCategoryDesc.IsForeignKey = false;
				colvarHtcCategoryDesc.IsReadOnly = false;
				colvarHtcCategoryDesc.DefaultSetting = @"";
				colvarHtcCategoryDesc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHtcCategoryDesc);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hami_category",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("DealTypeCodeId")]
		[Bindable(true)]
		public int DealTypeCodeId 
		{
			get { return GetColumnValue<int>(Columns.DealTypeCodeId); }
			set { SetColumnValue(Columns.DealTypeCodeId, value); }
		}
		  
		[XmlAttribute("HamiCategoryX")]
		[Bindable(true)]
		public string HamiCategoryX 
		{
			get { return GetColumnValue<string>(Columns.HamiCategoryX); }
			set { SetColumnValue(Columns.HamiCategoryX, value); }
		}
		  
		[XmlAttribute("HamiCategoryDesc")]
		[Bindable(true)]
		public string HamiCategoryDesc 
		{
			get { return GetColumnValue<string>(Columns.HamiCategoryDesc); }
			set { SetColumnValue(Columns.HamiCategoryDesc, value); }
		}
		  
		[XmlAttribute("HtcCategoryDesc")]
		[Bindable(true)]
		public string HtcCategoryDesc 
		{
			get { return GetColumnValue<string>(Columns.HtcCategoryDesc); }
			set { SetColumnValue(Columns.HtcCategoryDesc, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn DealTypeCodeIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn HamiCategoryXColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn HamiCategoryDescColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn HtcCategoryDescColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string DealTypeCodeId = @"deal_type_code_id";
			 public static string HamiCategoryX = @"hami_category";
			 public static string HamiCategoryDesc = @"hami_category_desc";
			 public static string HtcCategoryDesc = @"htc_category_desc";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

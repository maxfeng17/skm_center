using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewOrderMemberBuildingSeller class.
    /// </summary>
    [Serializable]
    public partial class ViewOrderMemberBuildingSellerCollection : ReadOnlyList<ViewOrderMemberBuildingSeller, ViewOrderMemberBuildingSellerCollection>
    {        
        public ViewOrderMemberBuildingSellerCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_order_member_building_seller view.
    /// </summary>
    [Serializable]
    public partial class ViewOrderMemberBuildingSeller : ReadOnlyRecord<ViewOrderMemberBuildingSeller>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_order_member_building_seller", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBuildingId = new TableSchema.TableColumn(schema);
                colvarBuildingId.ColumnName = "building_id";
                colvarBuildingId.DataType = DbType.AnsiString;
                colvarBuildingId.MaxLength = 20;
                colvarBuildingId.AutoIncrement = false;
                colvarBuildingId.IsNullable = true;
                colvarBuildingId.IsPrimaryKey = false;
                colvarBuildingId.IsForeignKey = false;
                colvarBuildingId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingId);
                
                TableSchema.TableColumn colvarBuildingName = new TableSchema.TableColumn(schema);
                colvarBuildingName.ColumnName = "building_name";
                colvarBuildingName.DataType = DbType.String;
                colvarBuildingName.MaxLength = 50;
                colvarBuildingName.AutoIncrement = false;
                colvarBuildingName.IsNullable = false;
                colvarBuildingName.IsPrimaryKey = false;
                colvarBuildingName.IsForeignKey = false;
                colvarBuildingName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingName);
                
                TableSchema.TableColumn colvarBuildingStreetName = new TableSchema.TableColumn(schema);
                colvarBuildingStreetName.ColumnName = "building_street_name";
                colvarBuildingStreetName.DataType = DbType.String;
                colvarBuildingStreetName.MaxLength = 100;
                colvarBuildingStreetName.AutoIncrement = false;
                colvarBuildingStreetName.IsNullable = false;
                colvarBuildingStreetName.IsPrimaryKey = false;
                colvarBuildingStreetName.IsForeignKey = false;
                colvarBuildingStreetName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingStreetName);
                
                TableSchema.TableColumn colvarBuildingAddressNumber = new TableSchema.TableColumn(schema);
                colvarBuildingAddressNumber.ColumnName = "building_address_number";
                colvarBuildingAddressNumber.DataType = DbType.String;
                colvarBuildingAddressNumber.MaxLength = 100;
                colvarBuildingAddressNumber.AutoIncrement = false;
                colvarBuildingAddressNumber.IsNullable = true;
                colvarBuildingAddressNumber.IsPrimaryKey = false;
                colvarBuildingAddressNumber.IsForeignKey = false;
                colvarBuildingAddressNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingAddressNumber);
                
                TableSchema.TableColumn colvarBuildingOnline = new TableSchema.TableColumn(schema);
                colvarBuildingOnline.ColumnName = "building_online";
                colvarBuildingOnline.DataType = DbType.Boolean;
                colvarBuildingOnline.MaxLength = 0;
                colvarBuildingOnline.AutoIncrement = false;
                colvarBuildingOnline.IsNullable = false;
                colvarBuildingOnline.IsPrimaryKey = false;
                colvarBuildingOnline.IsForeignKey = false;
                colvarBuildingOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingOnline);
                
                TableSchema.TableColumn colvarBuildingRank = new TableSchema.TableColumn(schema);
                colvarBuildingRank.ColumnName = "building_rank";
                colvarBuildingRank.DataType = DbType.Int32;
                colvarBuildingRank.MaxLength = 0;
                colvarBuildingRank.AutoIncrement = false;
                colvarBuildingRank.IsNullable = false;
                colvarBuildingRank.IsPrimaryKey = false;
                colvarBuildingRank.IsForeignKey = false;
                colvarBuildingRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingRank);
                
                TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
                colvarLastName.ColumnName = "last_name";
                colvarLastName.DataType = DbType.String;
                colvarLastName.MaxLength = 50;
                colvarLastName.AutoIncrement = false;
                colvarLastName.IsNullable = true;
                colvarLastName.IsPrimaryKey = false;
                colvarLastName.IsForeignKey = false;
                colvarLastName.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastName);
                
                TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
                colvarFirstName.ColumnName = "first_name";
                colvarFirstName.DataType = DbType.String;
                colvarFirstName.MaxLength = 50;
                colvarFirstName.AutoIncrement = false;
                colvarFirstName.IsNullable = true;
                colvarFirstName.IsPrimaryKey = false;
                colvarFirstName.IsForeignKey = false;
                colvarFirstName.IsReadOnly = false;
                
                schema.Columns.Add(colvarFirstName);
                
                TableSchema.TableColumn colvarBuildingGuid = new TableSchema.TableColumn(schema);
                colvarBuildingGuid.ColumnName = "building_GUID";
                colvarBuildingGuid.DataType = DbType.Guid;
                colvarBuildingGuid.MaxLength = 0;
                colvarBuildingGuid.AutoIncrement = false;
                colvarBuildingGuid.IsNullable = true;
                colvarBuildingGuid.IsPrimaryKey = false;
                colvarBuildingGuid.IsForeignKey = false;
                colvarBuildingGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingGuid);
                
                TableSchema.TableColumn colvarBirthday = new TableSchema.TableColumn(schema);
                colvarBirthday.ColumnName = "birthday";
                colvarBirthday.DataType = DbType.DateTime;
                colvarBirthday.MaxLength = 0;
                colvarBirthday.AutoIncrement = false;
                colvarBirthday.IsNullable = true;
                colvarBirthday.IsPrimaryKey = false;
                colvarBirthday.IsForeignKey = false;
                colvarBirthday.IsReadOnly = false;
                
                schema.Columns.Add(colvarBirthday);
                
                TableSchema.TableColumn colvarGender = new TableSchema.TableColumn(schema);
                colvarGender.ColumnName = "gender";
                colvarGender.DataType = DbType.Int32;
                colvarGender.MaxLength = 0;
                colvarGender.AutoIncrement = false;
                colvarGender.IsNullable = true;
                colvarGender.IsPrimaryKey = false;
                colvarGender.IsForeignKey = false;
                colvarGender.IsReadOnly = false;
                
                schema.Columns.Add(colvarGender);
                
                TableSchema.TableColumn colvarMemberMobile = new TableSchema.TableColumn(schema);
                colvarMemberMobile.ColumnName = "member_mobile";
                colvarMemberMobile.DataType = DbType.AnsiString;
                colvarMemberMobile.MaxLength = 50;
                colvarMemberMobile.AutoIncrement = false;
                colvarMemberMobile.IsNullable = true;
                colvarMemberMobile.IsPrimaryKey = false;
                colvarMemberMobile.IsForeignKey = false;
                colvarMemberMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberMobile);
                
                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "company_name";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 50;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyName);
                
                TableSchema.TableColumn colvarCompanyDepartment = new TableSchema.TableColumn(schema);
                colvarCompanyDepartment.ColumnName = "company_department";
                colvarCompanyDepartment.DataType = DbType.String;
                colvarCompanyDepartment.MaxLength = 50;
                colvarCompanyDepartment.AutoIncrement = false;
                colvarCompanyDepartment.IsNullable = true;
                colvarCompanyDepartment.IsPrimaryKey = false;
                colvarCompanyDepartment.IsForeignKey = false;
                colvarCompanyDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyDepartment);
                
                TableSchema.TableColumn colvarCompanyTel = new TableSchema.TableColumn(schema);
                colvarCompanyTel.ColumnName = "company_tel";
                colvarCompanyTel.DataType = DbType.AnsiString;
                colvarCompanyTel.MaxLength = 20;
                colvarCompanyTel.AutoIncrement = false;
                colvarCompanyTel.IsNullable = true;
                colvarCompanyTel.IsPrimaryKey = false;
                colvarCompanyTel.IsForeignKey = false;
                colvarCompanyTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyTel);
                
                TableSchema.TableColumn colvarCompanyTelExt = new TableSchema.TableColumn(schema);
                colvarCompanyTelExt.ColumnName = "company_tel_ext";
                colvarCompanyTelExt.DataType = DbType.AnsiString;
                colvarCompanyTelExt.MaxLength = 50;
                colvarCompanyTelExt.AutoIncrement = false;
                colvarCompanyTelExt.IsNullable = true;
                colvarCompanyTelExt.IsPrimaryKey = false;
                colvarCompanyTelExt.IsForeignKey = false;
                colvarCompanyTelExt.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyTelExt);
                
                TableSchema.TableColumn colvarCompanyAddress = new TableSchema.TableColumn(schema);
                colvarCompanyAddress.ColumnName = "company_address";
                colvarCompanyAddress.DataType = DbType.String;
                colvarCompanyAddress.MaxLength = 100;
                colvarCompanyAddress.AutoIncrement = false;
                colvarCompanyAddress.IsNullable = true;
                colvarCompanyAddress.IsPrimaryKey = false;
                colvarCompanyAddress.IsForeignKey = false;
                colvarCompanyAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAddress);
                
                TableSchema.TableColumn colvarDeliveryMethod = new TableSchema.TableColumn(schema);
                colvarDeliveryMethod.ColumnName = "delivery_method";
                colvarDeliveryMethod.DataType = DbType.String;
                colvarDeliveryMethod.MaxLength = 200;
                colvarDeliveryMethod.AutoIncrement = false;
                colvarDeliveryMethod.IsNullable = true;
                colvarDeliveryMethod.IsPrimaryKey = false;
                colvarDeliveryMethod.IsForeignKey = false;
                colvarDeliveryMethod.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryMethod);
                
                TableSchema.TableColumn colvarPrimaryContactMethod = new TableSchema.TableColumn(schema);
                colvarPrimaryContactMethod.ColumnName = "primary_contact_method";
                colvarPrimaryContactMethod.DataType = DbType.Int32;
                colvarPrimaryContactMethod.MaxLength = 0;
                colvarPrimaryContactMethod.AutoIncrement = false;
                colvarPrimaryContactMethod.IsNullable = true;
                colvarPrimaryContactMethod.IsPrimaryKey = false;
                colvarPrimaryContactMethod.IsForeignKey = false;
                colvarPrimaryContactMethod.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrimaryContactMethod);
                
                TableSchema.TableColumn colvarMemberStatus = new TableSchema.TableColumn(schema);
                colvarMemberStatus.ColumnName = "member_status";
                colvarMemberStatus.DataType = DbType.Int32;
                colvarMemberStatus.MaxLength = 0;
                colvarMemberStatus.AutoIncrement = false;
                colvarMemberStatus.IsNullable = true;
                colvarMemberStatus.IsPrimaryKey = false;
                colvarMemberStatus.IsForeignKey = false;
                colvarMemberStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberStatus);
                
                TableSchema.TableColumn colvarComment = new TableSchema.TableColumn(schema);
                colvarComment.ColumnName = "comment";
                colvarComment.DataType = DbType.String;
                colvarComment.MaxLength = 100;
                colvarComment.AutoIncrement = false;
                colvarComment.IsNullable = true;
                colvarComment.IsPrimaryKey = false;
                colvarComment.IsForeignKey = false;
                colvarComment.IsReadOnly = false;
                
                schema.Columns.Add(colvarComment);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = true;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarSellerBossName = new TableSchema.TableColumn(schema);
                colvarSellerBossName.ColumnName = "seller_boss_name";
                colvarSellerBossName.DataType = DbType.String;
                colvarSellerBossName.MaxLength = 30;
                colvarSellerBossName.AutoIncrement = false;
                colvarSellerBossName.IsNullable = true;
                colvarSellerBossName.IsPrimaryKey = false;
                colvarSellerBossName.IsForeignKey = false;
                colvarSellerBossName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerBossName);
                
                TableSchema.TableColumn colvarSellerTel = new TableSchema.TableColumn(schema);
                colvarSellerTel.ColumnName = "seller_tel";
                colvarSellerTel.DataType = DbType.AnsiString;
                colvarSellerTel.MaxLength = 100;
                colvarSellerTel.AutoIncrement = false;
                colvarSellerTel.IsNullable = true;
                colvarSellerTel.IsPrimaryKey = false;
                colvarSellerTel.IsForeignKey = false;
                colvarSellerTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerTel);
                
                TableSchema.TableColumn colvarSellerTel2 = new TableSchema.TableColumn(schema);
                colvarSellerTel2.ColumnName = "seller_tel2";
                colvarSellerTel2.DataType = DbType.AnsiString;
                colvarSellerTel2.MaxLength = 100;
                colvarSellerTel2.AutoIncrement = false;
                colvarSellerTel2.IsNullable = true;
                colvarSellerTel2.IsPrimaryKey = false;
                colvarSellerTel2.IsForeignKey = false;
                colvarSellerTel2.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerTel2);
                
                TableSchema.TableColumn colvarSellerFax = new TableSchema.TableColumn(schema);
                colvarSellerFax.ColumnName = "seller_fax";
                colvarSellerFax.DataType = DbType.AnsiString;
                colvarSellerFax.MaxLength = 20;
                colvarSellerFax.AutoIncrement = false;
                colvarSellerFax.IsNullable = true;
                colvarSellerFax.IsPrimaryKey = false;
                colvarSellerFax.IsForeignKey = false;
                colvarSellerFax.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerFax);
                
                TableSchema.TableColumn colvarSellerMobile = new TableSchema.TableColumn(schema);
                colvarSellerMobile.ColumnName = "seller_mobile";
                colvarSellerMobile.DataType = DbType.AnsiString;
                colvarSellerMobile.MaxLength = 100;
                colvarSellerMobile.AutoIncrement = false;
                colvarSellerMobile.IsNullable = true;
                colvarSellerMobile.IsPrimaryKey = false;
                colvarSellerMobile.IsForeignKey = false;
                colvarSellerMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMobile);
                
                TableSchema.TableColumn colvarSellerAddress = new TableSchema.TableColumn(schema);
                colvarSellerAddress.ColumnName = "seller_address";
                colvarSellerAddress.DataType = DbType.String;
                colvarSellerAddress.MaxLength = 100;
                colvarSellerAddress.AutoIncrement = false;
                colvarSellerAddress.IsNullable = true;
                colvarSellerAddress.IsPrimaryKey = false;
                colvarSellerAddress.IsForeignKey = false;
                colvarSellerAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerAddress);
                
                TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
                colvarSellerEmail.ColumnName = "seller_email";
                colvarSellerEmail.DataType = DbType.AnsiString;
                colvarSellerEmail.MaxLength = 200;
                colvarSellerEmail.AutoIncrement = false;
                colvarSellerEmail.IsNullable = true;
                colvarSellerEmail.IsPrimaryKey = false;
                colvarSellerEmail.IsForeignKey = false;
                colvarSellerEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerEmail);
                
                TableSchema.TableColumn colvarSellerBlog = new TableSchema.TableColumn(schema);
                colvarSellerBlog.ColumnName = "seller_blog";
                colvarSellerBlog.DataType = DbType.AnsiString;
                colvarSellerBlog.MaxLength = 100;
                colvarSellerBlog.AutoIncrement = false;
                colvarSellerBlog.IsNullable = true;
                colvarSellerBlog.IsPrimaryKey = false;
                colvarSellerBlog.IsForeignKey = false;
                colvarSellerBlog.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerBlog);
                
                TableSchema.TableColumn colvarSellerInvoice = new TableSchema.TableColumn(schema);
                colvarSellerInvoice.ColumnName = "seller_invoice";
                colvarSellerInvoice.DataType = DbType.String;
                colvarSellerInvoice.MaxLength = 50;
                colvarSellerInvoice.AutoIncrement = false;
                colvarSellerInvoice.IsNullable = true;
                colvarSellerInvoice.IsPrimaryKey = false;
                colvarSellerInvoice.IsForeignKey = false;
                colvarSellerInvoice.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerInvoice);
                
                TableSchema.TableColumn colvarSellerDescription = new TableSchema.TableColumn(schema);
                colvarSellerDescription.ColumnName = "seller_description";
                colvarSellerDescription.DataType = DbType.String;
                colvarSellerDescription.MaxLength = 1073741823;
                colvarSellerDescription.AutoIncrement = false;
                colvarSellerDescription.IsNullable = true;
                colvarSellerDescription.IsPrimaryKey = false;
                colvarSellerDescription.IsForeignKey = false;
                colvarSellerDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerDescription);
                
                TableSchema.TableColumn colvarSellerStatus = new TableSchema.TableColumn(schema);
                colvarSellerStatus.ColumnName = "seller_status";
                colvarSellerStatus.DataType = DbType.Int32;
                colvarSellerStatus.MaxLength = 0;
                colvarSellerStatus.AutoIncrement = false;
                colvarSellerStatus.IsNullable = true;
                colvarSellerStatus.IsPrimaryKey = false;
                colvarSellerStatus.IsForeignKey = false;
                colvarSellerStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerStatus);
                
                TableSchema.TableColumn colvarSellerRemark = new TableSchema.TableColumn(schema);
                colvarSellerRemark.ColumnName = "seller_remark";
                colvarSellerRemark.DataType = DbType.String;
                colvarSellerRemark.MaxLength = 200;
                colvarSellerRemark.AutoIncrement = false;
                colvarSellerRemark.IsNullable = true;
                colvarSellerRemark.IsPrimaryKey = false;
                colvarSellerRemark.IsForeignKey = false;
                colvarSellerRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerRemark);
                
                TableSchema.TableColumn colvarSellerSales = new TableSchema.TableColumn(schema);
                colvarSellerSales.ColumnName = "seller_sales";
                colvarSellerSales.DataType = DbType.String;
                colvarSellerSales.MaxLength = 50;
                colvarSellerSales.AutoIncrement = false;
                colvarSellerSales.IsNullable = true;
                colvarSellerSales.IsPrimaryKey = false;
                colvarSellerSales.IsForeignKey = false;
                colvarSellerSales.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerSales);
                
                TableSchema.TableColumn colvarSellerLogoimgPath = new TableSchema.TableColumn(schema);
                colvarSellerLogoimgPath.ColumnName = "seller_logoimg_path";
                colvarSellerLogoimgPath.DataType = DbType.AnsiString;
                colvarSellerLogoimgPath.MaxLength = 500;
                colvarSellerLogoimgPath.AutoIncrement = false;
                colvarSellerLogoimgPath.IsNullable = true;
                colvarSellerLogoimgPath.IsPrimaryKey = false;
                colvarSellerLogoimgPath.IsForeignKey = false;
                colvarSellerLogoimgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerLogoimgPath);
                
                TableSchema.TableColumn colvarSellerVideoPath = new TableSchema.TableColumn(schema);
                colvarSellerVideoPath.ColumnName = "seller_video_path";
                colvarSellerVideoPath.DataType = DbType.AnsiString;
                colvarSellerVideoPath.MaxLength = 100;
                colvarSellerVideoPath.AutoIncrement = false;
                colvarSellerVideoPath.IsNullable = true;
                colvarSellerVideoPath.IsPrimaryKey = false;
                colvarSellerVideoPath.IsForeignKey = false;
                colvarSellerVideoPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerVideoPath);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarOrderSellerName = new TableSchema.TableColumn(schema);
                colvarOrderSellerName.ColumnName = "order_seller_name";
                colvarOrderSellerName.DataType = DbType.String;
                colvarOrderSellerName.MaxLength = 50;
                colvarOrderSellerName.AutoIncrement = false;
                colvarOrderSellerName.IsNullable = false;
                colvarOrderSellerName.IsPrimaryKey = false;
                colvarOrderSellerName.IsForeignKey = false;
                colvarOrderSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderSellerName);
                
                TableSchema.TableColumn colvarMemberEmail = new TableSchema.TableColumn(schema);
                colvarMemberEmail.ColumnName = "member_email";
                colvarMemberEmail.DataType = DbType.String;
                colvarMemberEmail.MaxLength = 256;
                colvarMemberEmail.AutoIncrement = false;
                colvarMemberEmail.IsNullable = true;
                colvarMemberEmail.IsPrimaryKey = false;
                colvarMemberEmail.IsForeignKey = false;
                colvarMemberEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberEmail);
                
                TableSchema.TableColumn colvarUserEmail = new TableSchema.TableColumn(schema);
                colvarUserEmail.ColumnName = "user_email";
                colvarUserEmail.DataType = DbType.String;
                colvarUserEmail.MaxLength = 256;
                colvarUserEmail.AutoIncrement = false;
                colvarUserEmail.IsNullable = true;
                colvarUserEmail.IsPrimaryKey = false;
                colvarUserEmail.IsForeignKey = false;
                colvarUserEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserEmail);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarOrderPhone = new TableSchema.TableColumn(schema);
                colvarOrderPhone.ColumnName = "order_phone";
                colvarOrderPhone.DataType = DbType.AnsiString;
                colvarOrderPhone.MaxLength = 50;
                colvarOrderPhone.AutoIncrement = false;
                colvarOrderPhone.IsNullable = true;
                colvarOrderPhone.IsPrimaryKey = false;
                colvarOrderPhone.IsForeignKey = false;
                colvarOrderPhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderPhone);
                
                TableSchema.TableColumn colvarOrderMobile = new TableSchema.TableColumn(schema);
                colvarOrderMobile.ColumnName = "order_mobile";
                colvarOrderMobile.DataType = DbType.AnsiString;
                colvarOrderMobile.MaxLength = 50;
                colvarOrderMobile.AutoIncrement = false;
                colvarOrderMobile.IsNullable = true;
                colvarOrderMobile.IsPrimaryKey = false;
                colvarOrderMobile.IsForeignKey = false;
                colvarOrderMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderMobile);
                
                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryAddress);
                
                TableSchema.TableColumn colvarDeliveryTime = new TableSchema.TableColumn(schema);
                colvarDeliveryTime.ColumnName = "delivery_time";
                colvarDeliveryTime.DataType = DbType.DateTime;
                colvarDeliveryTime.MaxLength = 0;
                colvarDeliveryTime.AutoIncrement = false;
                colvarDeliveryTime.IsNullable = true;
                colvarDeliveryTime.IsPrimaryKey = false;
                colvarDeliveryTime.IsForeignKey = false;
                colvarDeliveryTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryTime);
                
                TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
                colvarSubtotal.ColumnName = "subtotal";
                colvarSubtotal.DataType = DbType.Currency;
                colvarSubtotal.MaxLength = 0;
                colvarSubtotal.AutoIncrement = false;
                colvarSubtotal.IsNullable = false;
                colvarSubtotal.IsPrimaryKey = false;
                colvarSubtotal.IsForeignKey = false;
                colvarSubtotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubtotal);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = false;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarUserMemo = new TableSchema.TableColumn(schema);
                colvarUserMemo.ColumnName = "user_memo";
                colvarUserMemo.DataType = DbType.String;
                colvarUserMemo.MaxLength = 1073741823;
                colvarUserMemo.AutoIncrement = false;
                colvarUserMemo.IsNullable = true;
                colvarUserMemo.IsPrimaryKey = false;
                colvarUserMemo.IsForeignKey = false;
                colvarUserMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserMemo);
                
                TableSchema.TableColumn colvarOrderMemo = new TableSchema.TableColumn(schema);
                colvarOrderMemo.ColumnName = "order_memo";
                colvarOrderMemo.DataType = DbType.String;
                colvarOrderMemo.MaxLength = 1073741823;
                colvarOrderMemo.AutoIncrement = false;
                colvarOrderMemo.IsNullable = true;
                colvarOrderMemo.IsPrimaryKey = false;
                colvarOrderMemo.IsForeignKey = false;
                colvarOrderMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderMemo);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarOrderCreateTime = new TableSchema.TableColumn(schema);
                colvarOrderCreateTime.ColumnName = "order_create_time";
                colvarOrderCreateTime.DataType = DbType.DateTime;
                colvarOrderCreateTime.MaxLength = 0;
                colvarOrderCreateTime.AutoIncrement = false;
                colvarOrderCreateTime.IsNullable = false;
                colvarOrderCreateTime.IsPrimaryKey = false;
                colvarOrderCreateTime.IsForeignKey = false;
                colvarOrderCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCreateTime);
                
                TableSchema.TableColumn colvarOrderCreateId = new TableSchema.TableColumn(schema);
                colvarOrderCreateId.ColumnName = "order_create_id";
                colvarOrderCreateId.DataType = DbType.String;
                colvarOrderCreateId.MaxLength = 30;
                colvarOrderCreateId.AutoIncrement = false;
                colvarOrderCreateId.IsNullable = false;
                colvarOrderCreateId.IsPrimaryKey = false;
                colvarOrderCreateId.IsForeignKey = false;
                colvarOrderCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCreateId);
                
                TableSchema.TableColumn colvarOrderModifyId = new TableSchema.TableColumn(schema);
                colvarOrderModifyId.ColumnName = "order_modify_id";
                colvarOrderModifyId.DataType = DbType.String;
                colvarOrderModifyId.MaxLength = 30;
                colvarOrderModifyId.AutoIncrement = false;
                colvarOrderModifyId.IsNullable = true;
                colvarOrderModifyId.IsPrimaryKey = false;
                colvarOrderModifyId.IsForeignKey = false;
                colvarOrderModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderModifyId);
                
                TableSchema.TableColumn colvarOrderModifyTime = new TableSchema.TableColumn(schema);
                colvarOrderModifyTime.ColumnName = "order_modify_time";
                colvarOrderModifyTime.DataType = DbType.DateTime;
                colvarOrderModifyTime.MaxLength = 0;
                colvarOrderModifyTime.AutoIncrement = false;
                colvarOrderModifyTime.IsNullable = true;
                colvarOrderModifyTime.IsPrimaryKey = false;
                colvarOrderModifyTime.IsForeignKey = false;
                colvarOrderModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderModifyTime);
                
                TableSchema.TableColumn colvarOrderAccessLock = new TableSchema.TableColumn(schema);
                colvarOrderAccessLock.ColumnName = "order_access_lock";
                colvarOrderAccessLock.DataType = DbType.String;
                colvarOrderAccessLock.MaxLength = 50;
                colvarOrderAccessLock.AutoIncrement = false;
                colvarOrderAccessLock.IsNullable = true;
                colvarOrderAccessLock.IsPrimaryKey = false;
                colvarOrderAccessLock.IsForeignKey = false;
                colvarOrderAccessLock.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderAccessLock);
                
                TableSchema.TableColumn colvarOrderStage = new TableSchema.TableColumn(schema);
                colvarOrderStage.ColumnName = "order_stage";
                colvarOrderStage.DataType = DbType.Int32;
                colvarOrderStage.MaxLength = 0;
                colvarOrderStage.AutoIncrement = false;
                colvarOrderStage.IsNullable = false;
                colvarOrderStage.IsPrimaryKey = false;
                colvarOrderStage.IsForeignKey = false;
                colvarOrderStage.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStage);
                
                TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
                colvarDepartment.ColumnName = "department";
                colvarDepartment.DataType = DbType.Int32;
                colvarDepartment.MaxLength = 0;
                colvarDepartment.AutoIncrement = false;
                colvarDepartment.IsNullable = true;
                colvarDepartment.IsPrimaryKey = false;
                colvarDepartment.IsForeignKey = false;
                colvarDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepartment);
                
                TableSchema.TableColumn colvarPostCkoutAction = new TableSchema.TableColumn(schema);
                colvarPostCkoutAction.ColumnName = "post_ckout_action";
                colvarPostCkoutAction.DataType = DbType.Int32;
                colvarPostCkoutAction.MaxLength = 0;
                colvarPostCkoutAction.AutoIncrement = false;
                colvarPostCkoutAction.IsNullable = true;
                colvarPostCkoutAction.IsPrimaryKey = false;
                colvarPostCkoutAction.IsForeignKey = false;
                colvarPostCkoutAction.IsReadOnly = false;
                
                schema.Columns.Add(colvarPostCkoutAction);
                
                TableSchema.TableColumn colvarPostCkoutArgs = new TableSchema.TableColumn(schema);
                colvarPostCkoutArgs.ColumnName = "post_ckout_args";
                colvarPostCkoutArgs.DataType = DbType.String;
                colvarPostCkoutArgs.MaxLength = 150;
                colvarPostCkoutArgs.AutoIncrement = false;
                colvarPostCkoutArgs.IsNullable = true;
                colvarPostCkoutArgs.IsPrimaryKey = false;
                colvarPostCkoutArgs.IsForeignKey = false;
                colvarPostCkoutArgs.IsReadOnly = false;
                
                schema.Columns.Add(colvarPostCkoutArgs);
                
                TableSchema.TableColumn colvarSellerCityId = new TableSchema.TableColumn(schema);
                colvarSellerCityId.ColumnName = "seller_city_id";
                colvarSellerCityId.DataType = DbType.Int32;
                colvarSellerCityId.MaxLength = 0;
                colvarSellerCityId.AutoIncrement = false;
                colvarSellerCityId.IsNullable = true;
                colvarSellerCityId.IsPrimaryKey = false;
                colvarSellerCityId.IsForeignKey = false;
                colvarSellerCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCityId);
                
                TableSchema.TableColumn colvarOrderCityId = new TableSchema.TableColumn(schema);
                colvarOrderCityId.ColumnName = "order_city_id";
                colvarOrderCityId.DataType = DbType.Int32;
                colvarOrderCityId.MaxLength = 0;
                colvarOrderCityId.AutoIncrement = false;
                colvarOrderCityId.IsNullable = true;
                colvarOrderCityId.IsPrimaryKey = false;
                colvarOrderCityId.IsForeignKey = false;
                colvarOrderCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCityId);
                
                TableSchema.TableColumn colvarBonusPoint = new TableSchema.TableColumn(schema);
                colvarBonusPoint.ColumnName = "bonus_point";
                colvarBonusPoint.DataType = DbType.AnsiString;
                colvarBonusPoint.MaxLength = 10;
                colvarBonusPoint.AutoIncrement = false;
                colvarBonusPoint.IsNullable = true;
                colvarBonusPoint.IsPrimaryKey = false;
                colvarBonusPoint.IsForeignKey = false;
                colvarBonusPoint.IsReadOnly = false;
                
                schema.Columns.Add(colvarBonusPoint);
                
                TableSchema.TableColumn colvarWithdrawalPromotionValue = new TableSchema.TableColumn(schema);
                colvarWithdrawalPromotionValue.ColumnName = "withdrawal_promotion_value";
                colvarWithdrawalPromotionValue.DataType = DbType.Double;
                colvarWithdrawalPromotionValue.MaxLength = 0;
                colvarWithdrawalPromotionValue.AutoIncrement = false;
                colvarWithdrawalPromotionValue.IsNullable = true;
                colvarWithdrawalPromotionValue.IsPrimaryKey = false;
                colvarWithdrawalPromotionValue.IsForeignKey = false;
                colvarWithdrawalPromotionValue.IsReadOnly = false;
                
                schema.Columns.Add(colvarWithdrawalPromotionValue);
                
                TableSchema.TableColumn colvarExportLog = new TableSchema.TableColumn(schema);
                colvarExportLog.ColumnName = "export_log";
                colvarExportLog.DataType = DbType.String;
                colvarExportLog.MaxLength = -1;
                colvarExportLog.AutoIncrement = false;
                colvarExportLog.IsNullable = true;
                colvarExportLog.IsPrimaryKey = false;
                colvarExportLog.IsForeignKey = false;
                colvarExportLog.IsReadOnly = false;
                
                schema.Columns.Add(colvarExportLog);
                
                TableSchema.TableColumn colvarIp = new TableSchema.TableColumn(schema);
                colvarIp.ColumnName = "ip";
                colvarIp.DataType = DbType.String;
                colvarIp.MaxLength = 50;
                colvarIp.AutoIncrement = false;
                colvarIp.IsNullable = true;
                colvarIp.IsPrimaryKey = false;
                colvarIp.IsForeignKey = false;
                colvarIp.IsReadOnly = false;
                
                schema.Columns.Add(colvarIp);
                
                TableSchema.TableColumn colvarReturnPaper = new TableSchema.TableColumn(schema);
                colvarReturnPaper.ColumnName = "return_paper";
                colvarReturnPaper.DataType = DbType.Boolean;
                colvarReturnPaper.MaxLength = 0;
                colvarReturnPaper.AutoIncrement = false;
                colvarReturnPaper.IsNullable = false;
                colvarReturnPaper.IsPrimaryKey = false;
                colvarReturnPaper.IsForeignKey = false;
                colvarReturnPaper.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnPaper);
                
                TableSchema.TableColumn colvarCartGuid = new TableSchema.TableColumn(schema);
                colvarCartGuid.ColumnName = "cart_guid";
                colvarCartGuid.DataType = DbType.Guid;
                colvarCartGuid.MaxLength = 0;
                colvarCartGuid.AutoIncrement = false;
                colvarCartGuid.IsNullable = true;
                colvarCartGuid.IsPrimaryKey = false;
                colvarCartGuid.IsForeignKey = false;
                colvarCartGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarCartGuid);
                
                TableSchema.TableColumn colvarIsFreightsOrder = new TableSchema.TableColumn(schema);
                colvarIsFreightsOrder.ColumnName = "is_freights_order";
                colvarIsFreightsOrder.DataType = DbType.Boolean;
                colvarIsFreightsOrder.MaxLength = 0;
                colvarIsFreightsOrder.AutoIncrement = false;
                colvarIsFreightsOrder.IsNullable = true;
                colvarIsFreightsOrder.IsPrimaryKey = false;
                colvarIsFreightsOrder.IsForeignKey = false;
                colvarIsFreightsOrder.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsFreightsOrder);
                
                TableSchema.TableColumn colvarIsCanceling = new TableSchema.TableColumn(schema);
                colvarIsCanceling.ColumnName = "is_canceling";
                colvarIsCanceling.DataType = DbType.Boolean;
                colvarIsCanceling.MaxLength = 0;
                colvarIsCanceling.AutoIncrement = false;
                colvarIsCanceling.IsNullable = false;
                colvarIsCanceling.IsPrimaryKey = false;
                colvarIsCanceling.IsForeignKey = false;
                colvarIsCanceling.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCanceling);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarCityList = new TableSchema.TableColumn(schema);
                colvarCityList.ColumnName = "city_list";
                colvarCityList.DataType = DbType.AnsiString;
                colvarCityList.MaxLength = 255;
                colvarCityList.AutoIncrement = false;
                colvarCityList.IsNullable = true;
                colvarCityList.IsPrimaryKey = false;
                colvarCityList.IsForeignKey = false;
                colvarCityList.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityList);
                
                TableSchema.TableColumn colvarFreezeCount = new TableSchema.TableColumn(schema);
                colvarFreezeCount.ColumnName = "freeze_count";
                colvarFreezeCount.DataType = DbType.Int32;
                colvarFreezeCount.MaxLength = 0;
                colvarFreezeCount.AutoIncrement = false;
                colvarFreezeCount.IsNullable = true;
                colvarFreezeCount.IsPrimaryKey = false;
                colvarFreezeCount.IsForeignKey = false;
                colvarFreezeCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarFreezeCount);
                
                TableSchema.TableColumn colvarAccountingFlag = new TableSchema.TableColumn(schema);
                colvarAccountingFlag.ColumnName = "accounting_flag";
                colvarAccountingFlag.DataType = DbType.Int32;
                colvarAccountingFlag.MaxLength = 0;
                colvarAccountingFlag.AutoIncrement = false;
                colvarAccountingFlag.IsNullable = true;
                colvarAccountingFlag.IsPrimaryKey = false;
                colvarAccountingFlag.IsForeignKey = false;
                colvarAccountingFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountingFlag);
                
                TableSchema.TableColumn colvarProductDeliveryType = new TableSchema.TableColumn(schema);
                colvarProductDeliveryType.ColumnName = "product_delivery_type";
                colvarProductDeliveryType.DataType = DbType.Int32;
                colvarProductDeliveryType.MaxLength = 0;
                colvarProductDeliveryType.AutoIncrement = false;
                colvarProductDeliveryType.IsNullable = false;
                colvarProductDeliveryType.IsPrimaryKey = false;
                colvarProductDeliveryType.IsForeignKey = false;
                colvarProductDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductDeliveryType);
                
                TableSchema.TableColumn colvarGoodsStatus = new TableSchema.TableColumn(schema);
                colvarGoodsStatus.ColumnName = "goods_status";
                colvarGoodsStatus.DataType = DbType.Int32;
                colvarGoodsStatus.MaxLength = 0;
                colvarGoodsStatus.AutoIncrement = false;
                colvarGoodsStatus.IsNullable = true;
                colvarGoodsStatus.IsPrimaryKey = false;
                colvarGoodsStatus.IsForeignKey = false;
                colvarGoodsStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarGoodsStatus);
                
                TableSchema.TableColumn colvarGoodsStatusMessage = new TableSchema.TableColumn(schema);
                colvarGoodsStatusMessage.ColumnName = "goods_status_message";
                colvarGoodsStatusMessage.DataType = DbType.String;
                colvarGoodsStatusMessage.MaxLength = 200;
                colvarGoodsStatusMessage.AutoIncrement = false;
                colvarGoodsStatusMessage.IsNullable = true;
                colvarGoodsStatusMessage.IsPrimaryKey = false;
                colvarGoodsStatusMessage.IsForeignKey = false;
                colvarGoodsStatusMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarGoodsStatusMessage);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = true;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarShipTime = new TableSchema.TableColumn(schema);
                colvarShipTime.ColumnName = "ship_time";
                colvarShipTime.DataType = DbType.DateTime;
                colvarShipTime.MaxLength = 0;
                colvarShipTime.AutoIncrement = false;
                colvarShipTime.IsNullable = true;
                colvarShipTime.IsPrimaryKey = false;
                colvarShipTime.IsForeignKey = false;
                colvarShipTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipTime);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarDcReceiveFail = new TableSchema.TableColumn(schema);
                colvarDcReceiveFail.ColumnName = "dc_receive_fail";
                colvarDcReceiveFail.DataType = DbType.Int32;
                colvarDcReceiveFail.MaxLength = 0;
                colvarDcReceiveFail.AutoIncrement = false;
                colvarDcReceiveFail.IsNullable = true;
                colvarDcReceiveFail.IsPrimaryKey = false;
                colvarDcReceiveFail.IsForeignKey = false;
                colvarDcReceiveFail.IsReadOnly = false;
                
                schema.Columns.Add(colvarDcReceiveFail);
                
                TableSchema.TableColumn colvarDcReceiveFailReason = new TableSchema.TableColumn(schema);
                colvarDcReceiveFailReason.ColumnName = "dc_receive_fail_reason";
                colvarDcReceiveFailReason.DataType = DbType.String;
                colvarDcReceiveFailReason.MaxLength = 50;
                colvarDcReceiveFailReason.AutoIncrement = false;
                colvarDcReceiveFailReason.IsNullable = true;
                colvarDcReceiveFailReason.IsPrimaryKey = false;
                colvarDcReceiveFailReason.IsForeignKey = false;
                colvarDcReceiveFailReason.IsReadOnly = false;
                
                schema.Columns.Add(colvarDcReceiveFailReason);
                
                TableSchema.TableColumn colvarDcReceiveFailTime = new TableSchema.TableColumn(schema);
                colvarDcReceiveFailTime.ColumnName = "dc_receive_fail_time";
                colvarDcReceiveFailTime.DataType = DbType.DateTime;
                colvarDcReceiveFailTime.MaxLength = 0;
                colvarDcReceiveFailTime.AutoIncrement = false;
                colvarDcReceiveFailTime.IsNullable = true;
                colvarDcReceiveFailTime.IsPrimaryKey = false;
                colvarDcReceiveFailTime.IsForeignKey = false;
                colvarDcReceiveFailTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDcReceiveFailTime);
                
                TableSchema.TableColumn colvarFamilyStoreName = new TableSchema.TableColumn(schema);
                colvarFamilyStoreName.ColumnName = "family_store_name";
                colvarFamilyStoreName.DataType = DbType.String;
                colvarFamilyStoreName.MaxLength = 50;
                colvarFamilyStoreName.AutoIncrement = false;
                colvarFamilyStoreName.IsNullable = true;
                colvarFamilyStoreName.IsPrimaryKey = false;
                colvarFamilyStoreName.IsForeignKey = false;
                colvarFamilyStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarFamilyStoreName);
                
                TableSchema.TableColumn colvarSevenStoreName = new TableSchema.TableColumn(schema);
                colvarSevenStoreName.ColumnName = "seven_store_name";
                colvarSevenStoreName.DataType = DbType.String;
                colvarSevenStoreName.MaxLength = 50;
                colvarSevenStoreName.AutoIncrement = false;
                colvarSevenStoreName.IsNullable = true;
                colvarSevenStoreName.IsPrimaryKey = false;
                colvarSevenStoreName.IsForeignKey = false;
                colvarSevenStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSevenStoreName);
                
                TableSchema.TableColumn colvarDcReturn = new TableSchema.TableColumn(schema);
                colvarDcReturn.ColumnName = "dc_return";
                colvarDcReturn.DataType = DbType.Int32;
                colvarDcReturn.MaxLength = 0;
                colvarDcReturn.AutoIncrement = false;
                colvarDcReturn.IsNullable = true;
                colvarDcReturn.IsPrimaryKey = false;
                colvarDcReturn.IsForeignKey = false;
                colvarDcReturn.IsReadOnly = false;
                
                schema.Columns.Add(colvarDcReturn);
                
                TableSchema.TableColumn colvarDcReturnTime = new TableSchema.TableColumn(schema);
                colvarDcReturnTime.ColumnName = "dc_return_time";
                colvarDcReturnTime.DataType = DbType.DateTime;
                colvarDcReturnTime.MaxLength = 0;
                colvarDcReturnTime.AutoIncrement = false;
                colvarDcReturnTime.IsNullable = true;
                colvarDcReturnTime.IsPrimaryKey = false;
                colvarDcReturnTime.IsForeignKey = false;
                colvarDcReturnTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDcReturnTime);
                
                TableSchema.TableColumn colvarDcReturnReason = new TableSchema.TableColumn(schema);
                colvarDcReturnReason.ColumnName = "dc_return_reason";
                colvarDcReturnReason.DataType = DbType.String;
                colvarDcReturnReason.MaxLength = 50;
                colvarDcReturnReason.AutoIncrement = false;
                colvarDcReturnReason.IsNullable = true;
                colvarDcReturnReason.IsPrimaryKey = false;
                colvarDcReturnReason.IsForeignKey = false;
                colvarDcReturnReason.IsReadOnly = false;
                
                schema.Columns.Add(colvarDcReturnReason);
                
                TableSchema.TableColumn colvarShipNo = new TableSchema.TableColumn(schema);
                colvarShipNo.ColumnName = "ship_no";
                colvarShipNo.DataType = DbType.String;
                colvarShipNo.MaxLength = -1;
                colvarShipNo.AutoIncrement = false;
                colvarShipNo.IsNullable = true;
                colvarShipNo.IsPrimaryKey = false;
                colvarShipNo.IsForeignKey = false;
                colvarShipNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipNo);
                
                TableSchema.TableColumn colvarDcAcceptStatus = new TableSchema.TableColumn(schema);
                colvarDcAcceptStatus.ColumnName = "dc_accept_status";
                colvarDcAcceptStatus.DataType = DbType.Int32;
                colvarDcAcceptStatus.MaxLength = 0;
                colvarDcAcceptStatus.AutoIncrement = false;
                colvarDcAcceptStatus.IsNullable = true;
                colvarDcAcceptStatus.IsPrimaryKey = false;
                colvarDcAcceptStatus.IsForeignKey = false;
                colvarDcAcceptStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarDcAcceptStatus);
                
                TableSchema.TableColumn colvarShipToStockTime = new TableSchema.TableColumn(schema);
                colvarShipToStockTime.ColumnName = "ship_to_stock_time";
                colvarShipToStockTime.DataType = DbType.DateTime;
                colvarShipToStockTime.MaxLength = 0;
                colvarShipToStockTime.AutoIncrement = false;
                colvarShipToStockTime.IsNullable = true;
                colvarShipToStockTime.IsPrimaryKey = false;
                colvarShipToStockTime.IsForeignKey = false;
                colvarShipToStockTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipToStockTime);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_order_member_building_seller",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewOrderMemberBuildingSeller()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewOrderMemberBuildingSeller(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewOrderMemberBuildingSeller(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewOrderMemberBuildingSeller(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BuildingId")]
        [Bindable(true)]
        public string BuildingId 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_id");
		    }
            set 
		    {
			    SetColumnValue("building_id", value);
            }
        }
	      
        [XmlAttribute("BuildingName")]
        [Bindable(true)]
        public string BuildingName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_name");
		    }
            set 
		    {
			    SetColumnValue("building_name", value);
            }
        }
	      
        [XmlAttribute("BuildingStreetName")]
        [Bindable(true)]
        public string BuildingStreetName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_street_name");
		    }
            set 
		    {
			    SetColumnValue("building_street_name", value);
            }
        }
	      
        [XmlAttribute("BuildingAddressNumber")]
        [Bindable(true)]
        public string BuildingAddressNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_address_number");
		    }
            set 
		    {
			    SetColumnValue("building_address_number", value);
            }
        }
	      
        [XmlAttribute("BuildingOnline")]
        [Bindable(true)]
        public bool BuildingOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool>("building_online");
		    }
            set 
		    {
			    SetColumnValue("building_online", value);
            }
        }
	      
        [XmlAttribute("BuildingRank")]
        [Bindable(true)]
        public int BuildingRank 
	    {
		    get
		    {
			    return GetColumnValue<int>("building_rank");
		    }
            set 
		    {
			    SetColumnValue("building_rank", value);
            }
        }
	      
        [XmlAttribute("LastName")]
        [Bindable(true)]
        public string LastName 
	    {
		    get
		    {
			    return GetColumnValue<string>("last_name");
		    }
            set 
		    {
			    SetColumnValue("last_name", value);
            }
        }
	      
        [XmlAttribute("FirstName")]
        [Bindable(true)]
        public string FirstName 
	    {
		    get
		    {
			    return GetColumnValue<string>("first_name");
		    }
            set 
		    {
			    SetColumnValue("first_name", value);
            }
        }
	      
        [XmlAttribute("BuildingGuid")]
        [Bindable(true)]
        public Guid? BuildingGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("building_GUID");
		    }
            set 
		    {
			    SetColumnValue("building_GUID", value);
            }
        }
	      
        [XmlAttribute("Birthday")]
        [Bindable(true)]
        public DateTime? Birthday 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("birthday");
		    }
            set 
		    {
			    SetColumnValue("birthday", value);
            }
        }
	      
        [XmlAttribute("Gender")]
        [Bindable(true)]
        public int? Gender 
	    {
		    get
		    {
			    return GetColumnValue<int?>("gender");
		    }
            set 
		    {
			    SetColumnValue("gender", value);
            }
        }
	      
        [XmlAttribute("MemberMobile")]
        [Bindable(true)]
        public string MemberMobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_mobile");
		    }
            set 
		    {
			    SetColumnValue("member_mobile", value);
            }
        }
	      
        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_name");
		    }
            set 
		    {
			    SetColumnValue("company_name", value);
            }
        }
	      
        [XmlAttribute("CompanyDepartment")]
        [Bindable(true)]
        public string CompanyDepartment 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_department");
		    }
            set 
		    {
			    SetColumnValue("company_department", value);
            }
        }
	      
        [XmlAttribute("CompanyTel")]
        [Bindable(true)]
        public string CompanyTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_tel");
		    }
            set 
		    {
			    SetColumnValue("company_tel", value);
            }
        }
	      
        [XmlAttribute("CompanyTelExt")]
        [Bindable(true)]
        public string CompanyTelExt 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_tel_ext");
		    }
            set 
		    {
			    SetColumnValue("company_tel_ext", value);
            }
        }
	      
        [XmlAttribute("CompanyAddress")]
        [Bindable(true)]
        public string CompanyAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_address");
		    }
            set 
		    {
			    SetColumnValue("company_address", value);
            }
        }
	      
        [XmlAttribute("DeliveryMethod")]
        [Bindable(true)]
        public string DeliveryMethod 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_method");
		    }
            set 
		    {
			    SetColumnValue("delivery_method", value);
            }
        }
	      
        [XmlAttribute("PrimaryContactMethod")]
        [Bindable(true)]
        public int? PrimaryContactMethod 
	    {
		    get
		    {
			    return GetColumnValue<int?>("primary_contact_method");
		    }
            set 
		    {
			    SetColumnValue("primary_contact_method", value);
            }
        }
	      
        [XmlAttribute("MemberStatus")]
        [Bindable(true)]
        public int? MemberStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("member_status");
		    }
            set 
		    {
			    SetColumnValue("member_status", value);
            }
        }
	      
        [XmlAttribute("Comment")]
        [Bindable(true)]
        public string Comment 
	    {
		    get
		    {
			    return GetColumnValue<string>("comment");
		    }
            set 
		    {
			    SetColumnValue("comment", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int? UserId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("SellerBossName")]
        [Bindable(true)]
        public string SellerBossName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_boss_name");
		    }
            set 
		    {
			    SetColumnValue("seller_boss_name", value);
            }
        }
	      
        [XmlAttribute("SellerTel")]
        [Bindable(true)]
        public string SellerTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_tel");
		    }
            set 
		    {
			    SetColumnValue("seller_tel", value);
            }
        }
	      
        [XmlAttribute("SellerTel2")]
        [Bindable(true)]
        public string SellerTel2 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_tel2");
		    }
            set 
		    {
			    SetColumnValue("seller_tel2", value);
            }
        }
	      
        [XmlAttribute("SellerFax")]
        [Bindable(true)]
        public string SellerFax 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_fax");
		    }
            set 
		    {
			    SetColumnValue("seller_fax", value);
            }
        }
	      
        [XmlAttribute("SellerMobile")]
        [Bindable(true)]
        public string SellerMobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_mobile");
		    }
            set 
		    {
			    SetColumnValue("seller_mobile", value);
            }
        }
	      
        [XmlAttribute("SellerAddress")]
        [Bindable(true)]
        public string SellerAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_address");
		    }
            set 
		    {
			    SetColumnValue("seller_address", value);
            }
        }
	      
        [XmlAttribute("SellerEmail")]
        [Bindable(true)]
        public string SellerEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_email");
		    }
            set 
		    {
			    SetColumnValue("seller_email", value);
            }
        }
	      
        [XmlAttribute("SellerBlog")]
        [Bindable(true)]
        public string SellerBlog 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_blog");
		    }
            set 
		    {
			    SetColumnValue("seller_blog", value);
            }
        }
	      
        [XmlAttribute("SellerInvoice")]
        [Bindable(true)]
        public string SellerInvoice 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_invoice");
		    }
            set 
		    {
			    SetColumnValue("seller_invoice", value);
            }
        }
	      
        [XmlAttribute("SellerDescription")]
        [Bindable(true)]
        public string SellerDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_description");
		    }
            set 
		    {
			    SetColumnValue("seller_description", value);
            }
        }
	      
        [XmlAttribute("SellerStatus")]
        [Bindable(true)]
        public int? SellerStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seller_status");
		    }
            set 
		    {
			    SetColumnValue("seller_status", value);
            }
        }
	      
        [XmlAttribute("SellerRemark")]
        [Bindable(true)]
        public string SellerRemark 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_remark");
		    }
            set 
		    {
			    SetColumnValue("seller_remark", value);
            }
        }
	      
        [XmlAttribute("SellerSales")]
        [Bindable(true)]
        public string SellerSales 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_sales");
		    }
            set 
		    {
			    SetColumnValue("seller_sales", value);
            }
        }
	      
        [XmlAttribute("SellerLogoimgPath")]
        [Bindable(true)]
        public string SellerLogoimgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_logoimg_path");
		    }
            set 
		    {
			    SetColumnValue("seller_logoimg_path", value);
            }
        }
	      
        [XmlAttribute("SellerVideoPath")]
        [Bindable(true)]
        public string SellerVideoPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_video_path");
		    }
            set 
		    {
			    SetColumnValue("seller_video_path", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("OrderSellerName")]
        [Bindable(true)]
        public string OrderSellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_seller_name");
		    }
            set 
		    {
			    SetColumnValue("order_seller_name", value);
            }
        }
	      
        [XmlAttribute("MemberEmail")]
        [Bindable(true)]
        public string MemberEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_email");
		    }
            set 
		    {
			    SetColumnValue("member_email", value);
            }
        }
	      
        [XmlAttribute("UserEmail")]
        [Bindable(true)]
        public string UserEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_email");
		    }
            set 
		    {
			    SetColumnValue("user_email", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("OrderPhone")]
        [Bindable(true)]
        public string OrderPhone 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_phone");
		    }
            set 
		    {
			    SetColumnValue("order_phone", value);
            }
        }
	      
        [XmlAttribute("OrderMobile")]
        [Bindable(true)]
        public string OrderMobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_mobile");
		    }
            set 
		    {
			    SetColumnValue("order_mobile", value);
            }
        }
	      
        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_address");
		    }
            set 
		    {
			    SetColumnValue("delivery_address", value);
            }
        }
	      
        [XmlAttribute("DeliveryTime")]
        [Bindable(true)]
        public DateTime? DeliveryTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("delivery_time");
		    }
            set 
		    {
			    SetColumnValue("delivery_time", value);
            }
        }
	      
        [XmlAttribute("Subtotal")]
        [Bindable(true)]
        public decimal Subtotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("subtotal");
		    }
            set 
		    {
			    SetColumnValue("subtotal", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal Total 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("UserMemo")]
        [Bindable(true)]
        public string UserMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_memo");
		    }
            set 
		    {
			    SetColumnValue("user_memo", value);
            }
        }
	      
        [XmlAttribute("OrderMemo")]
        [Bindable(true)]
        public string OrderMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_memo");
		    }
            set 
		    {
			    SetColumnValue("order_memo", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("OrderCreateTime")]
        [Bindable(true)]
        public DateTime OrderCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("order_create_time");
		    }
            set 
		    {
			    SetColumnValue("order_create_time", value);
            }
        }
	      
        [XmlAttribute("OrderCreateId")]
        [Bindable(true)]
        public string OrderCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_create_id");
		    }
            set 
		    {
			    SetColumnValue("order_create_id", value);
            }
        }
	      
        [XmlAttribute("OrderModifyId")]
        [Bindable(true)]
        public string OrderModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_modify_id");
		    }
            set 
		    {
			    SetColumnValue("order_modify_id", value);
            }
        }
	      
        [XmlAttribute("OrderModifyTime")]
        [Bindable(true)]
        public DateTime? OrderModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("order_modify_time");
		    }
            set 
		    {
			    SetColumnValue("order_modify_time", value);
            }
        }
	      
        [XmlAttribute("OrderAccessLock")]
        [Bindable(true)]
        public string OrderAccessLock 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_access_lock");
		    }
            set 
		    {
			    SetColumnValue("order_access_lock", value);
            }
        }
	      
        [XmlAttribute("OrderStage")]
        [Bindable(true)]
        public int OrderStage 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_stage");
		    }
            set 
		    {
			    SetColumnValue("order_stage", value);
            }
        }
	      
        [XmlAttribute("Department")]
        [Bindable(true)]
        public int? Department 
	    {
		    get
		    {
			    return GetColumnValue<int?>("department");
		    }
            set 
		    {
			    SetColumnValue("department", value);
            }
        }
	      
        [XmlAttribute("PostCkoutAction")]
        [Bindable(true)]
        public int? PostCkoutAction 
	    {
		    get
		    {
			    return GetColumnValue<int?>("post_ckout_action");
		    }
            set 
		    {
			    SetColumnValue("post_ckout_action", value);
            }
        }
	      
        [XmlAttribute("PostCkoutArgs")]
        [Bindable(true)]
        public string PostCkoutArgs 
	    {
		    get
		    {
			    return GetColumnValue<string>("post_ckout_args");
		    }
            set 
		    {
			    SetColumnValue("post_ckout_args", value);
            }
        }
	      
        [XmlAttribute("SellerCityId")]
        [Bindable(true)]
        public int? SellerCityId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seller_city_id");
		    }
            set 
		    {
			    SetColumnValue("seller_city_id", value);
            }
        }
	      
        [XmlAttribute("OrderCityId")]
        [Bindable(true)]
        public int? OrderCityId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_city_id");
		    }
            set 
		    {
			    SetColumnValue("order_city_id", value);
            }
        }
	      
        [XmlAttribute("BonusPoint")]
        [Bindable(true)]
        public string BonusPoint 
	    {
		    get
		    {
			    return GetColumnValue<string>("bonus_point");
		    }
            set 
		    {
			    SetColumnValue("bonus_point", value);
            }
        }
	      
        [XmlAttribute("WithdrawalPromotionValue")]
        [Bindable(true)]
        public double? WithdrawalPromotionValue 
	    {
		    get
		    {
			    return GetColumnValue<double?>("withdrawal_promotion_value");
		    }
            set 
		    {
			    SetColumnValue("withdrawal_promotion_value", value);
            }
        }
	      
        [XmlAttribute("ExportLog")]
        [Bindable(true)]
        public string ExportLog 
	    {
		    get
		    {
			    return GetColumnValue<string>("export_log");
		    }
            set 
		    {
			    SetColumnValue("export_log", value);
            }
        }
	      
        [XmlAttribute("Ip")]
        [Bindable(true)]
        public string Ip 
	    {
		    get
		    {
			    return GetColumnValue<string>("ip");
		    }
            set 
		    {
			    SetColumnValue("ip", value);
            }
        }
	      
        [XmlAttribute("ReturnPaper")]
        [Bindable(true)]
        public bool ReturnPaper 
	    {
		    get
		    {
			    return GetColumnValue<bool>("return_paper");
		    }
            set 
		    {
			    SetColumnValue("return_paper", value);
            }
        }
	      
        [XmlAttribute("CartGuid")]
        [Bindable(true)]
        public Guid? CartGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("cart_guid");
		    }
            set 
		    {
			    SetColumnValue("cart_guid", value);
            }
        }
	      
        [XmlAttribute("IsFreightsOrder")]
        [Bindable(true)]
        public bool? IsFreightsOrder 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_freights_order");
		    }
            set 
		    {
			    SetColumnValue("is_freights_order", value);
            }
        }
	      
        [XmlAttribute("IsCanceling")]
        [Bindable(true)]
        public bool IsCanceling 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_canceling");
		    }
            set 
		    {
			    SetColumnValue("is_canceling", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("CityList")]
        [Bindable(true)]
        public string CityList 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_list");
		    }
            set 
		    {
			    SetColumnValue("city_list", value);
            }
        }
	      
        [XmlAttribute("FreezeCount")]
        [Bindable(true)]
        public int? FreezeCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("freeze_count");
		    }
            set 
		    {
			    SetColumnValue("freeze_count", value);
            }
        }
	      
        [XmlAttribute("AccountingFlag")]
        [Bindable(true)]
        public int? AccountingFlag 
	    {
		    get
		    {
			    return GetColumnValue<int?>("accounting_flag");
		    }
            set 
		    {
			    SetColumnValue("accounting_flag", value);
            }
        }
	      
        [XmlAttribute("ProductDeliveryType")]
        [Bindable(true)]
        public int ProductDeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_delivery_type");
		    }
            set 
		    {
			    SetColumnValue("product_delivery_type", value);
            }
        }
	      
        [XmlAttribute("GoodsStatus")]
        [Bindable(true)]
        public int? GoodsStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("goods_status");
		    }
            set 
		    {
			    SetColumnValue("goods_status", value);
            }
        }
	      
        [XmlAttribute("GoodsStatusMessage")]
        [Bindable(true)]
        public string GoodsStatusMessage 
	    {
		    get
		    {
			    return GetColumnValue<string>("goods_status_message");
		    }
            set 
		    {
			    SetColumnValue("goods_status_message", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int? DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("ShipTime")]
        [Bindable(true)]
        public DateTime? ShipTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ship_time");
		    }
            set 
		    {
			    SetColumnValue("ship_time", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("DcReceiveFail")]
        [Bindable(true)]
        public int? DcReceiveFail 
	    {
		    get
		    {
			    return GetColumnValue<int?>("dc_receive_fail");
		    }
            set 
		    {
			    SetColumnValue("dc_receive_fail", value);
            }
        }
	      
        [XmlAttribute("DcReceiveFailReason")]
        [Bindable(true)]
        public string DcReceiveFailReason 
	    {
		    get
		    {
			    return GetColumnValue<string>("dc_receive_fail_reason");
		    }
            set 
		    {
			    SetColumnValue("dc_receive_fail_reason", value);
            }
        }
	      
        [XmlAttribute("DcReceiveFailTime")]
        [Bindable(true)]
        public DateTime? DcReceiveFailTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("dc_receive_fail_time");
		    }
            set 
		    {
			    SetColumnValue("dc_receive_fail_time", value);
            }
        }
	      
        [XmlAttribute("FamilyStoreName")]
        [Bindable(true)]
        public string FamilyStoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("family_store_name");
		    }
            set 
		    {
			    SetColumnValue("family_store_name", value);
            }
        }
	      
        [XmlAttribute("SevenStoreName")]
        [Bindable(true)]
        public string SevenStoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seven_store_name");
		    }
            set 
		    {
			    SetColumnValue("seven_store_name", value);
            }
        }
	      
        [XmlAttribute("DcReturn")]
        [Bindable(true)]
        public int? DcReturn 
	    {
		    get
		    {
			    return GetColumnValue<int?>("dc_return");
		    }
            set 
		    {
			    SetColumnValue("dc_return", value);
            }
        }
	      
        [XmlAttribute("DcReturnTime")]
        [Bindable(true)]
        public DateTime? DcReturnTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("dc_return_time");
		    }
            set 
		    {
			    SetColumnValue("dc_return_time", value);
            }
        }
	      
        [XmlAttribute("DcReturnReason")]
        [Bindable(true)]
        public string DcReturnReason 
	    {
		    get
		    {
			    return GetColumnValue<string>("dc_return_reason");
		    }
            set 
		    {
			    SetColumnValue("dc_return_reason", value);
            }
        }
	      
        [XmlAttribute("ShipNo")]
        [Bindable(true)]
        public string ShipNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("ship_no");
		    }
            set 
		    {
			    SetColumnValue("ship_no", value);
            }
        }
	      
        [XmlAttribute("DcAcceptStatus")]
        [Bindable(true)]
        public int? DcAcceptStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("dc_accept_status");
		    }
            set 
		    {
			    SetColumnValue("dc_accept_status", value);
            }
        }
	      
        [XmlAttribute("ShipToStockTime")]
        [Bindable(true)]
        public DateTime? ShipToStockTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ship_to_stock_time");
		    }
            set 
		    {
			    SetColumnValue("ship_to_stock_time", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BuildingId = @"building_id";
            
            public static string BuildingName = @"building_name";
            
            public static string BuildingStreetName = @"building_street_name";
            
            public static string BuildingAddressNumber = @"building_address_number";
            
            public static string BuildingOnline = @"building_online";
            
            public static string BuildingRank = @"building_rank";
            
            public static string LastName = @"last_name";
            
            public static string FirstName = @"first_name";
            
            public static string BuildingGuid = @"building_GUID";
            
            public static string Birthday = @"birthday";
            
            public static string Gender = @"gender";
            
            public static string MemberMobile = @"member_mobile";
            
            public static string CompanyName = @"company_name";
            
            public static string CompanyDepartment = @"company_department";
            
            public static string CompanyTel = @"company_tel";
            
            public static string CompanyTelExt = @"company_tel_ext";
            
            public static string CompanyAddress = @"company_address";
            
            public static string DeliveryMethod = @"delivery_method";
            
            public static string PrimaryContactMethod = @"primary_contact_method";
            
            public static string MemberStatus = @"member_status";
            
            public static string Comment = @"comment";
            
            public static string UserId = @"user_id";
            
            public static string SellerId = @"seller_id";
            
            public static string SellerBossName = @"seller_boss_name";
            
            public static string SellerTel = @"seller_tel";
            
            public static string SellerTel2 = @"seller_tel2";
            
            public static string SellerFax = @"seller_fax";
            
            public static string SellerMobile = @"seller_mobile";
            
            public static string SellerAddress = @"seller_address";
            
            public static string SellerEmail = @"seller_email";
            
            public static string SellerBlog = @"seller_blog";
            
            public static string SellerInvoice = @"seller_invoice";
            
            public static string SellerDescription = @"seller_description";
            
            public static string SellerStatus = @"seller_status";
            
            public static string SellerRemark = @"seller_remark";
            
            public static string SellerSales = @"seller_sales";
            
            public static string SellerLogoimgPath = @"seller_logoimg_path";
            
            public static string SellerVideoPath = @"seller_video_path";
            
            public static string OrderGuid = @"order_guid";
            
            public static string OrderId = @"order_id";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string SellerName = @"seller_name";
            
            public static string OrderSellerName = @"order_seller_name";
            
            public static string MemberEmail = @"member_email";
            
            public static string UserEmail = @"user_email";
            
            public static string MemberName = @"member_name";
            
            public static string OrderPhone = @"order_phone";
            
            public static string OrderMobile = @"order_mobile";
            
            public static string DeliveryAddress = @"delivery_address";
            
            public static string DeliveryTime = @"delivery_time";
            
            public static string Subtotal = @"subtotal";
            
            public static string Total = @"total";
            
            public static string UserMemo = @"user_memo";
            
            public static string OrderMemo = @"order_memo";
            
            public static string OrderStatus = @"order_status";
            
            public static string OrderCreateTime = @"order_create_time";
            
            public static string OrderCreateId = @"order_create_id";
            
            public static string OrderModifyId = @"order_modify_id";
            
            public static string OrderModifyTime = @"order_modify_time";
            
            public static string OrderAccessLock = @"order_access_lock";
            
            public static string OrderStage = @"order_stage";
            
            public static string Department = @"department";
            
            public static string PostCkoutAction = @"post_ckout_action";
            
            public static string PostCkoutArgs = @"post_ckout_args";
            
            public static string SellerCityId = @"seller_city_id";
            
            public static string OrderCityId = @"order_city_id";
            
            public static string BonusPoint = @"bonus_point";
            
            public static string WithdrawalPromotionValue = @"withdrawal_promotion_value";
            
            public static string ExportLog = @"export_log";
            
            public static string Ip = @"ip";
            
            public static string ReturnPaper = @"return_paper";
            
            public static string CartGuid = @"cart_guid";
            
            public static string IsFreightsOrder = @"is_freights_order";
            
            public static string IsCanceling = @"is_canceling";
            
            public static string UniqueId = @"unique_id";
            
            public static string CityList = @"city_list";
            
            public static string FreezeCount = @"freeze_count";
            
            public static string AccountingFlag = @"accounting_flag";
            
            public static string ProductDeliveryType = @"product_delivery_type";
            
            public static string GoodsStatus = @"goods_status";
            
            public static string GoodsStatusMessage = @"goods_status_message";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string ShipTime = @"ship_time";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string DcReceiveFail = @"dc_receive_fail";
            
            public static string DcReceiveFailReason = @"dc_receive_fail_reason";
            
            public static string DcReceiveFailTime = @"dc_receive_fail_time";
            
            public static string FamilyStoreName = @"family_store_name";
            
            public static string SevenStoreName = @"seven_store_name";
            
            public static string DcReturn = @"dc_return";
            
            public static string DcReturnTime = @"dc_return_time";
            
            public static string DcReturnReason = @"dc_return_reason";
            
            public static string ShipNo = @"ship_no";
            
            public static string DcAcceptStatus = @"dc_accept_status";
            
            public static string ShipToStockTime = @"ship_to_stock_time";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

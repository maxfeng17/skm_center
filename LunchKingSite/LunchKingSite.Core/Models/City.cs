using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the City class.
	/// </summary>
    [Serializable]
	public partial class CityCollection : RepositoryList<City, CityCollection>
	{	   
		public CityCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CityCollection</returns>
		public CityCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                City o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the city table.
	/// </summary>
	[Serializable]
	public partial class City : RepositoryRecord<City>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public City()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public City(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("city", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarParentId = new TableSchema.TableColumn(schema);
				colvarParentId.ColumnName = "parent_id";
				colvarParentId.DataType = DbType.Int32;
				colvarParentId.MaxLength = 0;
				colvarParentId.AutoIncrement = false;
				colvarParentId.IsNullable = true;
				colvarParentId.IsPrimaryKey = false;
				colvarParentId.IsForeignKey = true;
				colvarParentId.IsReadOnly = false;
				colvarParentId.DefaultSetting = @"";
				
					colvarParentId.ForeignKeyTableName = "city";
				schema.Columns.Add(colvarParentId);
				
				TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
				colvarCityName.ColumnName = "city_name";
				colvarCityName.DataType = DbType.String;
				colvarCityName.MaxLength = 20;
				colvarCityName.AutoIncrement = false;
				colvarCityName.IsNullable = false;
				colvarCityName.IsPrimaryKey = false;
				colvarCityName.IsForeignKey = false;
				colvarCityName.IsReadOnly = false;
				colvarCityName.DefaultSetting = @"";
				colvarCityName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityName);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "code";
				colvarCode.DataType = DbType.AnsiString;
				colvarCode.MaxLength = 10;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = true;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarImgPath = new TableSchema.TableColumn(schema);
				colvarImgPath.ColumnName = "img_path";
				colvarImgPath.DataType = DbType.String;
				colvarImgPath.MaxLength = 500;
				colvarImgPath.AutoIncrement = false;
				colvarImgPath.IsNullable = true;
				colvarImgPath.IsPrimaryKey = false;
				colvarImgPath.IsForeignKey = false;
				colvarImgPath.IsReadOnly = false;
				colvarImgPath.DefaultSetting = @"";
				colvarImgPath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImgPath);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarRank = new TableSchema.TableColumn(schema);
				colvarRank.ColumnName = "rank";
				colvarRank.DataType = DbType.Int32;
				colvarRank.MaxLength = 0;
				colvarRank.AutoIncrement = false;
				colvarRank.IsNullable = true;
				colvarRank.IsPrimaryKey = false;
				colvarRank.IsForeignKey = false;
				colvarRank.IsReadOnly = false;
				colvarRank.DefaultSetting = @"";
				colvarRank.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRank);
				
				TableSchema.TableColumn colvarZipCode = new TableSchema.TableColumn(schema);
				colvarZipCode.ColumnName = "zip_code";
				colvarZipCode.DataType = DbType.AnsiString;
				colvarZipCode.MaxLength = 5;
				colvarZipCode.AutoIncrement = false;
				colvarZipCode.IsNullable = true;
				colvarZipCode.IsPrimaryKey = false;
				colvarZipCode.IsForeignKey = false;
				colvarZipCode.IsReadOnly = false;
				colvarZipCode.DefaultSetting = @"";
				colvarZipCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarZipCode);
				
				TableSchema.TableColumn colvarVisible = new TableSchema.TableColumn(schema);
				colvarVisible.ColumnName = "visible";
				colvarVisible.DataType = DbType.Boolean;
				colvarVisible.MaxLength = 0;
				colvarVisible.AutoIncrement = false;
				colvarVisible.IsNullable = false;
				colvarVisible.IsPrimaryKey = false;
				colvarVisible.IsForeignKey = false;
				colvarVisible.IsReadOnly = false;
				
						colvarVisible.DefaultSetting = @"('1')";
				colvarVisible.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVisible);
				
				TableSchema.TableColumn colvarBitNumber = new TableSchema.TableColumn(schema);
				colvarBitNumber.ColumnName = "bit_number";
				colvarBitNumber.DataType = DbType.Int32;
				colvarBitNumber.MaxLength = 0;
				colvarBitNumber.AutoIncrement = false;
				colvarBitNumber.IsNullable = false;
				colvarBitNumber.IsPrimaryKey = false;
				colvarBitNumber.IsForeignKey = false;
				colvarBitNumber.IsReadOnly = false;
				
						colvarBitNumber.DefaultSetting = @"((0))";
				colvarBitNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBitNumber);
				
				TableSchema.TableColumn colvarShortName = new TableSchema.TableColumn(schema);
				colvarShortName.ColumnName = "short_name";
				colvarShortName.DataType = DbType.String;
				colvarShortName.MaxLength = 10;
				colvarShortName.AutoIncrement = false;
				colvarShortName.IsNullable = true;
				colvarShortName.IsPrimaryKey = false;
				colvarShortName.IsForeignKey = false;
				colvarShortName.IsReadOnly = false;
				colvarShortName.DefaultSetting = @"";
				colvarShortName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShortName);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("city",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ParentId")]
		[Bindable(true)]
		public int? ParentId 
		{
			get { return GetColumnValue<int?>(Columns.ParentId); }
			set { SetColumnValue(Columns.ParentId, value); }
		}
		  
		[XmlAttribute("CityName")]
		[Bindable(true)]
		public string CityName 
		{
			get { return GetColumnValue<string>(Columns.CityName); }
			set { SetColumnValue(Columns.CityName, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public string Code 
		{
			get { return GetColumnValue<string>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("ImgPath")]
		[Bindable(true)]
		public string ImgPath 
		{
			get { return GetColumnValue<string>(Columns.ImgPath); }
			set { SetColumnValue(Columns.ImgPath, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status 
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Rank")]
		[Bindable(true)]
		public int? Rank 
		{
			get { return GetColumnValue<int?>(Columns.Rank); }
			set { SetColumnValue(Columns.Rank, value); }
		}
		  
		[XmlAttribute("ZipCode")]
		[Bindable(true)]
		public string ZipCode 
		{
			get { return GetColumnValue<string>(Columns.ZipCode); }
			set { SetColumnValue(Columns.ZipCode, value); }
		}
		  
		[XmlAttribute("Visible")]
		[Bindable(true)]
		public bool Visible 
		{
			get { return GetColumnValue<bool>(Columns.Visible); }
			set { SetColumnValue(Columns.Visible, value); }
		}
		  
		[XmlAttribute("BitNumber")]
		[Bindable(true)]
		public int BitNumber 
		{
			get { return GetColumnValue<int>(Columns.BitNumber); }
			set { SetColumnValue(Columns.BitNumber, value); }
		}
		  
		[XmlAttribute("ShortName")]
		[Bindable(true)]
		public string ShortName 
		{
			get { return GetColumnValue<string>(Columns.ShortName); }
			set { SetColumnValue(Columns.ShortName, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (1)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ParentIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CityNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ImgPathColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn RankColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ZipCodeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn VisibleColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn BitNumberColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ShortNameColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ParentId = @"parent_id";
			 public static string CityName = @"city_name";
			 public static string Code = @"code";
			 public static string ImgPath = @"img_path";
			 public static string Status = @"status";
			 public static string Rank = @"rank";
			 public static string ZipCode = @"zip_code";
			 public static string Visible = @"visible";
			 public static string BitNumber = @"bit_number";
			 public static string ShortName = @"short_name";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

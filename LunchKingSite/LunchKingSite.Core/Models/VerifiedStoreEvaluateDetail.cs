using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VerifiedStoreEvaluateDetail class.
	/// </summary>
    [Serializable]
	public partial class VerifiedStoreEvaluateDetailCollection : RepositoryList<VerifiedStoreEvaluateDetail, VerifiedStoreEvaluateDetailCollection>
	{	   
		public VerifiedStoreEvaluateDetailCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VerifiedStoreEvaluateDetailCollection</returns>
		public VerifiedStoreEvaluateDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VerifiedStoreEvaluateDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the verified_store_evaluate_detail table.
	/// </summary>
	[Serializable]
	public partial class VerifiedStoreEvaluateDetail : RepositoryRecord<VerifiedStoreEvaluateDetail>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VerifiedStoreEvaluateDetail()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VerifiedStoreEvaluateDetail(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("verified_store_evaluate_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarVerifiedStoreGuid = new TableSchema.TableColumn(schema);
				colvarVerifiedStoreGuid.ColumnName = "verified_store_guid";
				colvarVerifiedStoreGuid.DataType = DbType.Guid;
				colvarVerifiedStoreGuid.MaxLength = 0;
				colvarVerifiedStoreGuid.AutoIncrement = false;
				colvarVerifiedStoreGuid.IsNullable = false;
				colvarVerifiedStoreGuid.IsPrimaryKey = false;
				colvarVerifiedStoreGuid.IsForeignKey = false;
				colvarVerifiedStoreGuid.IsReadOnly = false;
				colvarVerifiedStoreGuid.DefaultSetting = @"";
				colvarVerifiedStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifiedStoreGuid);
				
				TableSchema.TableColumn colvarEvaluateMainId = new TableSchema.TableColumn(schema);
				colvarEvaluateMainId.ColumnName = "evaluate_main_id";
				colvarEvaluateMainId.DataType = DbType.Int32;
				colvarEvaluateMainId.MaxLength = 0;
				colvarEvaluateMainId.AutoIncrement = false;
				colvarEvaluateMainId.IsNullable = false;
				colvarEvaluateMainId.IsPrimaryKey = true;
				colvarEvaluateMainId.IsForeignKey = false;
				colvarEvaluateMainId.IsReadOnly = false;
				colvarEvaluateMainId.DefaultSetting = @"";
				colvarEvaluateMainId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEvaluateMainId);
				
				TableSchema.TableColumn colvarStar = new TableSchema.TableColumn(schema);
				colvarStar.ColumnName = "star";
				colvarStar.DataType = DbType.Decimal;
				colvarStar.MaxLength = 0;
				colvarStar.AutoIncrement = false;
				colvarStar.IsNullable = false;
				colvarStar.IsPrimaryKey = false;
				colvarStar.IsForeignKey = false;
				colvarStar.IsReadOnly = false;
				colvarStar.DefaultSetting = @"";
				colvarStar.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStar);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("verified_store_evaluate_detail",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("VerifiedStoreGuid")]
		[Bindable(true)]
		public Guid VerifiedStoreGuid 
		{
			get { return GetColumnValue<Guid>(Columns.VerifiedStoreGuid); }
			set { SetColumnValue(Columns.VerifiedStoreGuid, value); }
		}
		  
		[XmlAttribute("EvaluateMainId")]
		[Bindable(true)]
		public int EvaluateMainId 
		{
			get { return GetColumnValue<int>(Columns.EvaluateMainId); }
			set { SetColumnValue(Columns.EvaluateMainId, value); }
		}
		  
		[XmlAttribute("Star")]
		[Bindable(true)]
		public decimal Star 
		{
			get { return GetColumnValue<decimal>(Columns.Star); }
			set { SetColumnValue(Columns.Star, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn VerifiedStoreGuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EvaluateMainIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StarColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string VerifiedStoreGuid = @"verified_store_guid";
			 public static string EvaluateMainId = @"evaluate_main_id";
			 public static string Star = @"star";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

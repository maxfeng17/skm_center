using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the EventPremiumPromo class.
	/// </summary>
    [Serializable]
	public partial class EventPremiumPromoCollection : RepositoryList<EventPremiumPromo, EventPremiumPromoCollection>
	{	   
		public EventPremiumPromoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EventPremiumPromoCollection</returns>
		public EventPremiumPromoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EventPremiumPromo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the event_premium_promo table.
	/// </summary>
	[Serializable]
	public partial class EventPremiumPromo : RepositoryRecord<EventPremiumPromo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public EventPremiumPromo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public EventPremiumPromo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("event_premium_promo", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 500;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
				colvarUrl.ColumnName = "url";
				colvarUrl.DataType = DbType.AnsiString;
				colvarUrl.MaxLength = 100;
				colvarUrl.AutoIncrement = false;
				colvarUrl.IsNullable = false;
				colvarUrl.IsPrimaryKey = false;
				colvarUrl.IsForeignKey = false;
				colvarUrl.IsReadOnly = false;
				colvarUrl.DefaultSetting = @"";
				colvarUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUrl);
				
				TableSchema.TableColumn colvarCpa = new TableSchema.TableColumn(schema);
				colvarCpa.ColumnName = "cpa";
				colvarCpa.DataType = DbType.AnsiString;
				colvarCpa.MaxLength = 100;
				colvarCpa.AutoIncrement = false;
				colvarCpa.IsNullable = true;
				colvarCpa.IsPrimaryKey = false;
				colvarCpa.IsForeignKey = false;
				colvarCpa.IsReadOnly = false;
				colvarCpa.DefaultSetting = @"";
				colvarCpa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCpa);
				
				TableSchema.TableColumn colvarPremiumName = new TableSchema.TableColumn(schema);
				colvarPremiumName.ColumnName = "premium_name";
				colvarPremiumName.DataType = DbType.String;
				colvarPremiumName.MaxLength = 500;
				colvarPremiumName.AutoIncrement = false;
				colvarPremiumName.IsNullable = false;
				colvarPremiumName.IsPrimaryKey = false;
				colvarPremiumName.IsForeignKey = false;
				colvarPremiumName.IsReadOnly = false;
				colvarPremiumName.DefaultSetting = @"";
				colvarPremiumName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPremiumName);
				
				TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
				colvarStartDate.ColumnName = "start_date";
				colvarStartDate.DataType = DbType.DateTime;
				colvarStartDate.MaxLength = 0;
				colvarStartDate.AutoIncrement = false;
				colvarStartDate.IsNullable = false;
				colvarStartDate.IsPrimaryKey = false;
				colvarStartDate.IsForeignKey = false;
				colvarStartDate.IsReadOnly = false;
				colvarStartDate.DefaultSetting = @"";
				colvarStartDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartDate);
				
				TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
				colvarEndDate.ColumnName = "end_date";
				colvarEndDate.DataType = DbType.DateTime;
				colvarEndDate.MaxLength = 0;
				colvarEndDate.AutoIncrement = false;
				colvarEndDate.IsNullable = false;
				colvarEndDate.IsPrimaryKey = false;
				colvarEndDate.IsForeignKey = false;
				colvarEndDate.IsReadOnly = false;
				colvarEndDate.DefaultSetting = @"";
				colvarEndDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndDate);
				
				TableSchema.TableColumn colvarTemplateType = new TableSchema.TableColumn(schema);
				colvarTemplateType.ColumnName = "template_type";
				colvarTemplateType.DataType = DbType.Int32;
				colvarTemplateType.MaxLength = 0;
				colvarTemplateType.AutoIncrement = false;
				colvarTemplateType.IsNullable = false;
				colvarTemplateType.IsPrimaryKey = false;
				colvarTemplateType.IsForeignKey = false;
				colvarTemplateType.IsReadOnly = false;
				
						colvarTemplateType.DefaultSetting = @"((0))";
				colvarTemplateType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTemplateType);
				
				TableSchema.TableColumn colvarPremiumAmount = new TableSchema.TableColumn(schema);
				colvarPremiumAmount.ColumnName = "premium_amount";
				colvarPremiumAmount.DataType = DbType.Int32;
				colvarPremiumAmount.MaxLength = 0;
				colvarPremiumAmount.AutoIncrement = false;
				colvarPremiumAmount.IsNullable = false;
				colvarPremiumAmount.IsPrimaryKey = false;
				colvarPremiumAmount.IsForeignKey = false;
				colvarPremiumAmount.IsReadOnly = false;
				
						colvarPremiumAmount.DefaultSetting = @"((0))";
				colvarPremiumAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPremiumAmount);
				
				TableSchema.TableColumn colvarMainBanner = new TableSchema.TableColumn(schema);
				colvarMainBanner.ColumnName = "main_banner";
				colvarMainBanner.DataType = DbType.String;
				colvarMainBanner.MaxLength = 1073741823;
				colvarMainBanner.AutoIncrement = false;
				colvarMainBanner.IsNullable = false;
				colvarMainBanner.IsPrimaryKey = false;
				colvarMainBanner.IsForeignKey = false;
				colvarMainBanner.IsReadOnly = false;
				colvarMainBanner.DefaultSetting = @"";
				colvarMainBanner.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainBanner);
				
				TableSchema.TableColumn colvarBackgroundPicture = new TableSchema.TableColumn(schema);
				colvarBackgroundPicture.ColumnName = "background_picture";
				colvarBackgroundPicture.DataType = DbType.AnsiString;
				colvarBackgroundPicture.MaxLength = 200;
				colvarBackgroundPicture.AutoIncrement = false;
				colvarBackgroundPicture.IsNullable = true;
				colvarBackgroundPicture.IsPrimaryKey = false;
				colvarBackgroundPicture.IsForeignKey = false;
				colvarBackgroundPicture.IsReadOnly = false;
				colvarBackgroundPicture.DefaultSetting = @"";
				colvarBackgroundPicture.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBackgroundPicture);
				
				TableSchema.TableColumn colvarBackgroundColor = new TableSchema.TableColumn(schema);
				colvarBackgroundColor.ColumnName = "background_color";
				colvarBackgroundColor.DataType = DbType.AnsiString;
				colvarBackgroundColor.MaxLength = 20;
				colvarBackgroundColor.AutoIncrement = false;
				colvarBackgroundColor.IsNullable = true;
				colvarBackgroundColor.IsPrimaryKey = false;
				colvarBackgroundColor.IsForeignKey = false;
				colvarBackgroundColor.IsReadOnly = false;
				colvarBackgroundColor.DefaultSetting = @"";
				colvarBackgroundColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBackgroundColor);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = 1073741823;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarSecretRule = new TableSchema.TableColumn(schema);
				colvarSecretRule.ColumnName = "secret_rule";
				colvarSecretRule.DataType = DbType.String;
				colvarSecretRule.MaxLength = 1073741823;
				colvarSecretRule.AutoIncrement = false;
				colvarSecretRule.IsNullable = true;
				colvarSecretRule.IsPrimaryKey = false;
				colvarSecretRule.IsForeignKey = false;
				colvarSecretRule.IsReadOnly = false;
				colvarSecretRule.DefaultSetting = @"";
				colvarSecretRule.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSecretRule);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Boolean;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((1))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreateUniqueId = new TableSchema.TableColumn(schema);
				colvarCreateUniqueId.ColumnName = "create_unique_id";
				colvarCreateUniqueId.DataType = DbType.Int32;
				colvarCreateUniqueId.MaxLength = 0;
				colvarCreateUniqueId.AutoIncrement = false;
				colvarCreateUniqueId.IsNullable = false;
				colvarCreateUniqueId.IsPrimaryKey = false;
				colvarCreateUniqueId.IsForeignKey = false;
				colvarCreateUniqueId.IsReadOnly = false;
				colvarCreateUniqueId.DefaultSetting = @"";
				colvarCreateUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateUniqueId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarApplySuccessContent = new TableSchema.TableColumn(schema);
				colvarApplySuccessContent.ColumnName = "apply_success_content";
				colvarApplySuccessContent.DataType = DbType.String;
				colvarApplySuccessContent.MaxLength = 1073741823;
				colvarApplySuccessContent.AutoIncrement = false;
				colvarApplySuccessContent.IsNullable = false;
				colvarApplySuccessContent.IsPrimaryKey = false;
				colvarApplySuccessContent.IsForeignKey = false;
				colvarApplySuccessContent.IsReadOnly = false;
				colvarApplySuccessContent.DefaultSetting = @"";
				colvarApplySuccessContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApplySuccessContent);
				
				TableSchema.TableColumn colvarChangeLog = new TableSchema.TableColumn(schema);
				colvarChangeLog.ColumnName = "change_log";
				colvarChangeLog.DataType = DbType.String;
				colvarChangeLog.MaxLength = 1073741823;
				colvarChangeLog.AutoIncrement = false;
				colvarChangeLog.IsNullable = false;
				colvarChangeLog.IsPrimaryKey = false;
				colvarChangeLog.IsForeignKey = false;
				colvarChangeLog.IsReadOnly = false;
				colvarChangeLog.DefaultSetting = @"";
				colvarChangeLog.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChangeLog);
				
				TableSchema.TableColumn colvarBannerFontColor = new TableSchema.TableColumn(schema);
				colvarBannerFontColor.ColumnName = "banner_font_color";
				colvarBannerFontColor.DataType = DbType.AnsiString;
				colvarBannerFontColor.MaxLength = 20;
				colvarBannerFontColor.AutoIncrement = false;
				colvarBannerFontColor.IsNullable = true;
				colvarBannerFontColor.IsPrimaryKey = false;
				colvarBannerFontColor.IsForeignKey = false;
				colvarBannerFontColor.IsReadOnly = false;
				colvarBannerFontColor.DefaultSetting = @"";
				colvarBannerFontColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBannerFontColor);
				
				TableSchema.TableColumn colvarButtonBackgroundColor = new TableSchema.TableColumn(schema);
				colvarButtonBackgroundColor.ColumnName = "button_background_color";
				colvarButtonBackgroundColor.DataType = DbType.AnsiString;
				colvarButtonBackgroundColor.MaxLength = 20;
				colvarButtonBackgroundColor.AutoIncrement = false;
				colvarButtonBackgroundColor.IsNullable = true;
				colvarButtonBackgroundColor.IsPrimaryKey = false;
				colvarButtonBackgroundColor.IsForeignKey = false;
				colvarButtonBackgroundColor.IsReadOnly = false;
				colvarButtonBackgroundColor.DefaultSetting = @"";
				colvarButtonBackgroundColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarButtonBackgroundColor);
				
				TableSchema.TableColumn colvarButtonFontColor = new TableSchema.TableColumn(schema);
				colvarButtonFontColor.ColumnName = "button_font_color";
				colvarButtonFontColor.DataType = DbType.AnsiString;
				colvarButtonFontColor.MaxLength = 20;
				colvarButtonFontColor.AutoIncrement = false;
				colvarButtonFontColor.IsNullable = true;
				colvarButtonFontColor.IsPrimaryKey = false;
				colvarButtonFontColor.IsForeignKey = false;
				colvarButtonFontColor.IsReadOnly = false;
				colvarButtonFontColor.DefaultSetting = @"";
				colvarButtonFontColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarButtonFontColor);
				
				TableSchema.TableColumn colvarButtonBackgroundHoverColor = new TableSchema.TableColumn(schema);
				colvarButtonBackgroundHoverColor.ColumnName = "button_background_hover_color";
				colvarButtonBackgroundHoverColor.DataType = DbType.AnsiString;
				colvarButtonBackgroundHoverColor.MaxLength = 20;
				colvarButtonBackgroundHoverColor.AutoIncrement = false;
				colvarButtonBackgroundHoverColor.IsNullable = true;
				colvarButtonBackgroundHoverColor.IsPrimaryKey = false;
				colvarButtonBackgroundHoverColor.IsForeignKey = false;
				colvarButtonBackgroundHoverColor.IsReadOnly = false;
				colvarButtonBackgroundHoverColor.DefaultSetting = @"";
				colvarButtonBackgroundHoverColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarButtonBackgroundHoverColor);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("event_premium_promo",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		  
		[XmlAttribute("Url")]
		[Bindable(true)]
		public string Url 
		{
			get { return GetColumnValue<string>(Columns.Url); }
			set { SetColumnValue(Columns.Url, value); }
		}
		  
		[XmlAttribute("Cpa")]
		[Bindable(true)]
		public string Cpa 
		{
			get { return GetColumnValue<string>(Columns.Cpa); }
			set { SetColumnValue(Columns.Cpa, value); }
		}
		  
		[XmlAttribute("PremiumName")]
		[Bindable(true)]
		public string PremiumName 
		{
			get { return GetColumnValue<string>(Columns.PremiumName); }
			set { SetColumnValue(Columns.PremiumName, value); }
		}
		  
		[XmlAttribute("StartDate")]
		[Bindable(true)]
		public DateTime StartDate 
		{
			get { return GetColumnValue<DateTime>(Columns.StartDate); }
			set { SetColumnValue(Columns.StartDate, value); }
		}
		  
		[XmlAttribute("EndDate")]
		[Bindable(true)]
		public DateTime EndDate 
		{
			get { return GetColumnValue<DateTime>(Columns.EndDate); }
			set { SetColumnValue(Columns.EndDate, value); }
		}
		  
		[XmlAttribute("TemplateType")]
		[Bindable(true)]
		public int TemplateType 
		{
			get { return GetColumnValue<int>(Columns.TemplateType); }
			set { SetColumnValue(Columns.TemplateType, value); }
		}
		  
		[XmlAttribute("PremiumAmount")]
		[Bindable(true)]
		public int PremiumAmount 
		{
			get { return GetColumnValue<int>(Columns.PremiumAmount); }
			set { SetColumnValue(Columns.PremiumAmount, value); }
		}
		  
		[XmlAttribute("MainBanner")]
		[Bindable(true)]
		public string MainBanner 
		{
			get { return GetColumnValue<string>(Columns.MainBanner); }
			set { SetColumnValue(Columns.MainBanner, value); }
		}
		  
		[XmlAttribute("BackgroundPicture")]
		[Bindable(true)]
		public string BackgroundPicture 
		{
			get { return GetColumnValue<string>(Columns.BackgroundPicture); }
			set { SetColumnValue(Columns.BackgroundPicture, value); }
		}
		  
		[XmlAttribute("BackgroundColor")]
		[Bindable(true)]
		public string BackgroundColor 
		{
			get { return GetColumnValue<string>(Columns.BackgroundColor); }
			set { SetColumnValue(Columns.BackgroundColor, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("SecretRule")]
		[Bindable(true)]
		public string SecretRule 
		{
			get { return GetColumnValue<string>(Columns.SecretRule); }
			set { SetColumnValue(Columns.SecretRule, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public bool Status 
		{
			get { return GetColumnValue<bool>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreateUniqueId")]
		[Bindable(true)]
		public int CreateUniqueId 
		{
			get { return GetColumnValue<int>(Columns.CreateUniqueId); }
			set { SetColumnValue(Columns.CreateUniqueId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ApplySuccessContent")]
		[Bindable(true)]
		public string ApplySuccessContent 
		{
			get { return GetColumnValue<string>(Columns.ApplySuccessContent); }
			set { SetColumnValue(Columns.ApplySuccessContent, value); }
		}
		  
		[XmlAttribute("ChangeLog")]
		[Bindable(true)]
		public string ChangeLog 
		{
			get { return GetColumnValue<string>(Columns.ChangeLog); }
			set { SetColumnValue(Columns.ChangeLog, value); }
		}
		  
		[XmlAttribute("BannerFontColor")]
		[Bindable(true)]
		public string BannerFontColor 
		{
			get { return GetColumnValue<string>(Columns.BannerFontColor); }
			set { SetColumnValue(Columns.BannerFontColor, value); }
		}
		  
		[XmlAttribute("ButtonBackgroundColor")]
		[Bindable(true)]
		public string ButtonBackgroundColor 
		{
			get { return GetColumnValue<string>(Columns.ButtonBackgroundColor); }
			set { SetColumnValue(Columns.ButtonBackgroundColor, value); }
		}
		  
		[XmlAttribute("ButtonFontColor")]
		[Bindable(true)]
		public string ButtonFontColor 
		{
			get { return GetColumnValue<string>(Columns.ButtonFontColor); }
			set { SetColumnValue(Columns.ButtonFontColor, value); }
		}
		  
		[XmlAttribute("ButtonBackgroundHoverColor")]
		[Bindable(true)]
		public string ButtonBackgroundHoverColor 
		{
			get { return GetColumnValue<string>(Columns.ButtonBackgroundHoverColor); }
			set { SetColumnValue(Columns.ButtonBackgroundHoverColor, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UrlColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CpaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PremiumNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StartDateColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn EndDateColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn TemplateTypeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn PremiumAmountColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn MainBannerColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn BackgroundPictureColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn BackgroundColorColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn SecretRuleColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateUniqueIdColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ApplySuccessContentColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn ChangeLogColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn BannerFontColorColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn ButtonBackgroundColorColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn ButtonFontColorColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn ButtonBackgroundHoverColorColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string Title = @"title";
			 public static string Url = @"url";
			 public static string Cpa = @"cpa";
			 public static string PremiumName = @"premium_name";
			 public static string StartDate = @"start_date";
			 public static string EndDate = @"end_date";
			 public static string TemplateType = @"template_type";
			 public static string PremiumAmount = @"premium_amount";
			 public static string MainBanner = @"main_banner";
			 public static string BackgroundPicture = @"background_picture";
			 public static string BackgroundColor = @"background_color";
			 public static string Description = @"description";
			 public static string SecretRule = @"secret_rule";
			 public static string Status = @"status";
			 public static string CreateUniqueId = @"create_unique_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyTime = @"modify_time";
			 public static string ApplySuccessContent = @"apply_success_content";
			 public static string ChangeLog = @"change_log";
			 public static string BannerFontColor = @"banner_font_color";
			 public static string ButtonBackgroundColor = @"button_background_color";
			 public static string ButtonFontColor = @"button_font_color";
			 public static string ButtonBackgroundHoverColor = @"button_background_hover_color";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

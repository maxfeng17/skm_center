using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewSellerStore class.
    /// </summary>
    [Serializable]
    public partial class ViewSellerStoreCollection : ReadOnlyList<ViewSellerStore, ViewSellerStoreCollection>
    {        
        public ViewSellerStoreCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_seller_store view.
    /// </summary>
    [Serializable]
    public partial class ViewSellerStore : ReadOnlyRecord<ViewSellerStore>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_seller_store", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "Guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 100;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = false;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
                colvarPhone.ColumnName = "phone";
                colvarPhone.DataType = DbType.AnsiString;
                colvarPhone.MaxLength = 100;
                colvarPhone.AutoIncrement = false;
                colvarPhone.IsNullable = true;
                colvarPhone.IsPrimaryKey = false;
                colvarPhone.IsForeignKey = false;
                colvarPhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhone);
                
                TableSchema.TableColumn colvarTownshipId = new TableSchema.TableColumn(schema);
                colvarTownshipId.ColumnName = "township_id";
                colvarTownshipId.DataType = DbType.Int32;
                colvarTownshipId.MaxLength = 0;
                colvarTownshipId.AutoIncrement = false;
                colvarTownshipId.IsNullable = true;
                colvarTownshipId.IsPrimaryKey = false;
                colvarTownshipId.IsForeignKey = false;
                colvarTownshipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTownshipId);
                
                TableSchema.TableColumn colvarAddressString = new TableSchema.TableColumn(schema);
                colvarAddressString.ColumnName = "address_string";
                colvarAddressString.DataType = DbType.String;
                colvarAddressString.MaxLength = 100;
                colvarAddressString.AutoIncrement = false;
                colvarAddressString.IsNullable = true;
                colvarAddressString.IsPrimaryKey = false;
                colvarAddressString.IsForeignKey = false;
                colvarAddressString.IsReadOnly = false;
                
                schema.Columns.Add(colvarAddressString);
                
                TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
                colvarOpenTime.ColumnName = "open_time";
                colvarOpenTime.DataType = DbType.String;
                colvarOpenTime.MaxLength = 256;
                colvarOpenTime.AutoIncrement = false;
                colvarOpenTime.IsNullable = true;
                colvarOpenTime.IsPrimaryKey = false;
                colvarOpenTime.IsForeignKey = false;
                colvarOpenTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpenTime);
                
                TableSchema.TableColumn colvarCloseDate = new TableSchema.TableColumn(schema);
                colvarCloseDate.ColumnName = "close_date";
                colvarCloseDate.DataType = DbType.String;
                colvarCloseDate.MaxLength = 256;
                colvarCloseDate.AutoIncrement = false;
                colvarCloseDate.IsNullable = true;
                colvarCloseDate.IsPrimaryKey = false;
                colvarCloseDate.IsForeignKey = false;
                colvarCloseDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCloseDate);
                
                TableSchema.TableColumn colvarRemarks = new TableSchema.TableColumn(schema);
                colvarRemarks.ColumnName = "remarks";
                colvarRemarks.DataType = DbType.String;
                colvarRemarks.MaxLength = 256;
                colvarRemarks.AutoIncrement = false;
                colvarRemarks.IsNullable = true;
                colvarRemarks.IsPrimaryKey = false;
                colvarRemarks.IsForeignKey = false;
                colvarRemarks.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemarks);
                
                TableSchema.TableColumn colvarMrt = new TableSchema.TableColumn(schema);
                colvarMrt.ColumnName = "mrt";
                colvarMrt.DataType = DbType.String;
                colvarMrt.MaxLength = 256;
                colvarMrt.AutoIncrement = false;
                colvarMrt.IsNullable = true;
                colvarMrt.IsPrimaryKey = false;
                colvarMrt.IsForeignKey = false;
                colvarMrt.IsReadOnly = false;
                
                schema.Columns.Add(colvarMrt);
                
                TableSchema.TableColumn colvarCar = new TableSchema.TableColumn(schema);
                colvarCar.ColumnName = "car";
                colvarCar.DataType = DbType.String;
                colvarCar.MaxLength = 256;
                colvarCar.AutoIncrement = false;
                colvarCar.IsNullable = true;
                colvarCar.IsPrimaryKey = false;
                colvarCar.IsForeignKey = false;
                colvarCar.IsReadOnly = false;
                
                schema.Columns.Add(colvarCar);
                
                TableSchema.TableColumn colvarBus = new TableSchema.TableColumn(schema);
                colvarBus.ColumnName = "bus";
                colvarBus.DataType = DbType.String;
                colvarBus.MaxLength = 256;
                colvarBus.AutoIncrement = false;
                colvarBus.IsNullable = true;
                colvarBus.IsPrimaryKey = false;
                colvarBus.IsForeignKey = false;
                colvarBus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBus);
                
                TableSchema.TableColumn colvarOtherVehicles = new TableSchema.TableColumn(schema);
                colvarOtherVehicles.ColumnName = "other_vehicles";
                colvarOtherVehicles.DataType = DbType.String;
                colvarOtherVehicles.MaxLength = 256;
                colvarOtherVehicles.AutoIncrement = false;
                colvarOtherVehicles.IsNullable = true;
                colvarOtherVehicles.IsPrimaryKey = false;
                colvarOtherVehicles.IsForeignKey = false;
                colvarOtherVehicles.IsReadOnly = false;
                
                schema.Columns.Add(colvarOtherVehicles);
                
                TableSchema.TableColumn colvarWebUrl = new TableSchema.TableColumn(schema);
                colvarWebUrl.ColumnName = "web_url";
                colvarWebUrl.DataType = DbType.AnsiString;
                colvarWebUrl.MaxLength = 4000;
                colvarWebUrl.AutoIncrement = false;
                colvarWebUrl.IsNullable = true;
                colvarWebUrl.IsPrimaryKey = false;
                colvarWebUrl.IsForeignKey = false;
                colvarWebUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarWebUrl);
                
                TableSchema.TableColumn colvarFacebookUrl = new TableSchema.TableColumn(schema);
                colvarFacebookUrl.ColumnName = "facebook_url";
                colvarFacebookUrl.DataType = DbType.AnsiString;
                colvarFacebookUrl.MaxLength = 4000;
                colvarFacebookUrl.AutoIncrement = false;
                colvarFacebookUrl.IsNullable = true;
                colvarFacebookUrl.IsPrimaryKey = false;
                colvarFacebookUrl.IsForeignKey = false;
                colvarFacebookUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarFacebookUrl);
                
                TableSchema.TableColumn colvarPlurkUrl = new TableSchema.TableColumn(schema);
                colvarPlurkUrl.ColumnName = "plurk_url";
                colvarPlurkUrl.DataType = DbType.Int32;
                colvarPlurkUrl.MaxLength = 0;
                colvarPlurkUrl.AutoIncrement = false;
                colvarPlurkUrl.IsNullable = true;
                colvarPlurkUrl.IsPrimaryKey = false;
                colvarPlurkUrl.IsForeignKey = false;
                colvarPlurkUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarPlurkUrl);
                
                TableSchema.TableColumn colvarBlogUrl = new TableSchema.TableColumn(schema);
                colvarBlogUrl.ColumnName = "blog_url";
                colvarBlogUrl.DataType = DbType.AnsiString;
                colvarBlogUrl.MaxLength = 4000;
                colvarBlogUrl.AutoIncrement = false;
                colvarBlogUrl.IsNullable = true;
                colvarBlogUrl.IsPrimaryKey = false;
                colvarBlogUrl.IsForeignKey = false;
                colvarBlogUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarBlogUrl);
                
                TableSchema.TableColumn colvarOtherUrl = new TableSchema.TableColumn(schema);
                colvarOtherUrl.ColumnName = "other_url";
                colvarOtherUrl.DataType = DbType.AnsiString;
                colvarOtherUrl.MaxLength = 4000;
                colvarOtherUrl.AutoIncrement = false;
                colvarOtherUrl.IsNullable = true;
                colvarOtherUrl.IsPrimaryKey = false;
                colvarOtherUrl.IsForeignKey = false;
                colvarOtherUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarOtherUrl);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 30;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 30;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "CompanyName";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 50;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyName);
                
                TableSchema.TableColumn colvarCompanyBossName = new TableSchema.TableColumn(schema);
                colvarCompanyBossName.ColumnName = "CompanyBossName";
                colvarCompanyBossName.DataType = DbType.String;
                colvarCompanyBossName.MaxLength = 30;
                colvarCompanyBossName.AutoIncrement = false;
                colvarCompanyBossName.IsNullable = true;
                colvarCompanyBossName.IsPrimaryKey = false;
                colvarCompanyBossName.IsForeignKey = false;
                colvarCompanyBossName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBossName);
                
                TableSchema.TableColumn colvarCompanyID = new TableSchema.TableColumn(schema);
                colvarCompanyID.ColumnName = "CompanyID";
                colvarCompanyID.DataType = DbType.AnsiString;
                colvarCompanyID.MaxLength = 20;
                colvarCompanyID.AutoIncrement = false;
                colvarCompanyID.IsNullable = true;
                colvarCompanyID.IsPrimaryKey = false;
                colvarCompanyID.IsForeignKey = false;
                colvarCompanyID.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyID);
                
                TableSchema.TableColumn colvarCompanyBankCode = new TableSchema.TableColumn(schema);
                colvarCompanyBankCode.ColumnName = "CompanyBankCode";
                colvarCompanyBankCode.DataType = DbType.AnsiString;
                colvarCompanyBankCode.MaxLength = 10;
                colvarCompanyBankCode.AutoIncrement = false;
                colvarCompanyBankCode.IsNullable = true;
                colvarCompanyBankCode.IsPrimaryKey = false;
                colvarCompanyBankCode.IsForeignKey = false;
                colvarCompanyBankCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBankCode);
                
                TableSchema.TableColumn colvarCompanyBranchCode = new TableSchema.TableColumn(schema);
                colvarCompanyBranchCode.ColumnName = "CompanyBranchCode";
                colvarCompanyBranchCode.DataType = DbType.AnsiString;
                colvarCompanyBranchCode.MaxLength = 10;
                colvarCompanyBranchCode.AutoIncrement = false;
                colvarCompanyBranchCode.IsNullable = true;
                colvarCompanyBranchCode.IsPrimaryKey = false;
                colvarCompanyBranchCode.IsForeignKey = false;
                colvarCompanyBranchCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBranchCode);
                
                TableSchema.TableColumn colvarCompanyAccount = new TableSchema.TableColumn(schema);
                colvarCompanyAccount.ColumnName = "CompanyAccount";
                colvarCompanyAccount.DataType = DbType.AnsiString;
                colvarCompanyAccount.MaxLength = 20;
                colvarCompanyAccount.AutoIncrement = false;
                colvarCompanyAccount.IsNullable = true;
                colvarCompanyAccount.IsPrimaryKey = false;
                colvarCompanyAccount.IsForeignKey = false;
                colvarCompanyAccount.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAccount);
                
                TableSchema.TableColumn colvarCompanyAccountName = new TableSchema.TableColumn(schema);
                colvarCompanyAccountName.ColumnName = "CompanyAccountName";
                colvarCompanyAccountName.DataType = DbType.String;
                colvarCompanyAccountName.MaxLength = 50;
                colvarCompanyAccountName.AutoIncrement = false;
                colvarCompanyAccountName.IsNullable = true;
                colvarCompanyAccountName.IsPrimaryKey = false;
                colvarCompanyAccountName.IsForeignKey = false;
                colvarCompanyAccountName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAccountName);
                
                TableSchema.TableColumn colvarCompanyEmail = new TableSchema.TableColumn(schema);
                colvarCompanyEmail.ColumnName = "CompanyEmail";
                colvarCompanyEmail.DataType = DbType.String;
                colvarCompanyEmail.MaxLength = 250;
                colvarCompanyEmail.AutoIncrement = false;
                colvarCompanyEmail.IsNullable = true;
                colvarCompanyEmail.IsPrimaryKey = false;
                colvarCompanyEmail.IsForeignKey = false;
                colvarCompanyEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyEmail);
                
                TableSchema.TableColumn colvarCompanyNotice = new TableSchema.TableColumn(schema);
                colvarCompanyNotice.ColumnName = "CompanyNotice";
                colvarCompanyNotice.DataType = DbType.String;
                colvarCompanyNotice.MaxLength = 250;
                colvarCompanyNotice.AutoIncrement = false;
                colvarCompanyNotice.IsNullable = true;
                colvarCompanyNotice.IsPrimaryKey = false;
                colvarCompanyNotice.IsForeignKey = false;
                colvarCompanyNotice.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyNotice);
                
                TableSchema.TableColumn colvarIsCloseDown = new TableSchema.TableColumn(schema);
                colvarIsCloseDown.ColumnName = "is_close_down";
                colvarIsCloseDown.DataType = DbType.Boolean;
                colvarIsCloseDown.MaxLength = 0;
                colvarIsCloseDown.AutoIncrement = false;
                colvarIsCloseDown.IsNullable = false;
                colvarIsCloseDown.IsPrimaryKey = false;
                colvarIsCloseDown.IsForeignKey = false;
                colvarIsCloseDown.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCloseDown);
                
                TableSchema.TableColumn colvarCloseDownDate = new TableSchema.TableColumn(schema);
                colvarCloseDownDate.ColumnName = "close_down_date";
                colvarCloseDownDate.DataType = DbType.DateTime;
                colvarCloseDownDate.MaxLength = 0;
                colvarCloseDownDate.AutoIncrement = false;
                colvarCloseDownDate.IsNullable = true;
                colvarCloseDownDate.IsPrimaryKey = false;
                colvarCloseDownDate.IsForeignKey = false;
                colvarCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCloseDownDate);
                
                TableSchema.TableColumn colvarSignCompanyID = new TableSchema.TableColumn(schema);
                colvarSignCompanyID.ColumnName = "SignCompanyID";
                colvarSignCompanyID.DataType = DbType.AnsiString;
                colvarSignCompanyID.MaxLength = 20;
                colvarSignCompanyID.AutoIncrement = false;
                colvarSignCompanyID.IsNullable = true;
                colvarSignCompanyID.IsPrimaryKey = false;
                colvarSignCompanyID.IsForeignKey = false;
                colvarSignCompanyID.IsReadOnly = false;
                
                schema.Columns.Add(colvarSignCompanyID);
                
                TableSchema.TableColumn colvarAccountantName = new TableSchema.TableColumn(schema);
                colvarAccountantName.ColumnName = "accountant_name";
                colvarAccountantName.DataType = DbType.String;
                colvarAccountantName.MaxLength = 20;
                colvarAccountantName.AutoIncrement = false;
                colvarAccountantName.IsNullable = true;
                colvarAccountantName.IsPrimaryKey = false;
                colvarAccountantName.IsForeignKey = false;
                colvarAccountantName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountantName);
                
                TableSchema.TableColumn colvarAccountantTel = new TableSchema.TableColumn(schema);
                colvarAccountantTel.ColumnName = "accountant_tel";
                colvarAccountantTel.DataType = DbType.AnsiString;
                colvarAccountantTel.MaxLength = 100;
                colvarAccountantTel.AutoIncrement = false;
                colvarAccountantTel.IsNullable = true;
                colvarAccountantTel.IsPrimaryKey = false;
                colvarAccountantTel.IsForeignKey = false;
                colvarAccountantTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountantTel);
                
                TableSchema.TableColumn colvarCreditcardAvailable = new TableSchema.TableColumn(schema);
                colvarCreditcardAvailable.ColumnName = "creditcard_available";
                colvarCreditcardAvailable.DataType = DbType.Boolean;
                colvarCreditcardAvailable.MaxLength = 0;
                colvarCreditcardAvailable.AutoIncrement = false;
                colvarCreditcardAvailable.IsNullable = false;
                colvarCreditcardAvailable.IsPrimaryKey = false;
                colvarCreditcardAvailable.IsForeignKey = false;
                colvarCreditcardAvailable.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreditcardAvailable);
                
                TableSchema.TableColumn colvarTempStatus = new TableSchema.TableColumn(schema);
                colvarTempStatus.ColumnName = "temp_status";
                colvarTempStatus.DataType = DbType.Int32;
                colvarTempStatus.MaxLength = 0;
                colvarTempStatus.AutoIncrement = false;
                colvarTempStatus.IsNullable = false;
                colvarTempStatus.IsPrimaryKey = false;
                colvarTempStatus.IsForeignKey = false;
                colvarTempStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarTempStatus);
                
                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 1073741823;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage);
                
                TableSchema.TableColumn colvarNewCreated = new TableSchema.TableColumn(schema);
                colvarNewCreated.ColumnName = "new_created";
                colvarNewCreated.DataType = DbType.Boolean;
                colvarNewCreated.MaxLength = 0;
                colvarNewCreated.AutoIncrement = false;
                colvarNewCreated.IsNullable = false;
                colvarNewCreated.IsPrimaryKey = false;
                colvarNewCreated.IsForeignKey = false;
                colvarNewCreated.IsReadOnly = false;
                
                schema.Columns.Add(colvarNewCreated);
                
                TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
                colvarCoordinate.ColumnName = "coordinate";
                colvarCoordinate.DataType = DbType.AnsiString;
                colvarCoordinate.MaxLength = -1;
                colvarCoordinate.AutoIncrement = false;
                colvarCoordinate.IsNullable = true;
                colvarCoordinate.IsPrimaryKey = false;
                colvarCoordinate.IsForeignKey = false;
                colvarCoordinate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCoordinate);
                
                TableSchema.TableColumn colvarApplyId = new TableSchema.TableColumn(schema);
                colvarApplyId.ColumnName = "apply_id";
                colvarApplyId.DataType = DbType.String;
                colvarApplyId.MaxLength = 100;
                colvarApplyId.AutoIncrement = false;
                colvarApplyId.IsNullable = true;
                colvarApplyId.IsPrimaryKey = false;
                colvarApplyId.IsForeignKey = false;
                colvarApplyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarApplyId);
                
                TableSchema.TableColumn colvarApplyTime = new TableSchema.TableColumn(schema);
                colvarApplyTime.ColumnName = "apply_time";
                colvarApplyTime.DataType = DbType.DateTime;
                colvarApplyTime.MaxLength = 0;
                colvarApplyTime.AutoIncrement = false;
                colvarApplyTime.IsNullable = true;
                colvarApplyTime.IsPrimaryKey = false;
                colvarApplyTime.IsForeignKey = false;
                colvarApplyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarApplyTime);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarReturnTime = new TableSchema.TableColumn(schema);
                colvarReturnTime.ColumnName = "return_time";
                colvarReturnTime.DataType = DbType.DateTime;
                colvarReturnTime.MaxLength = 0;
                colvarReturnTime.AutoIncrement = false;
                colvarReturnTime.IsNullable = true;
                colvarReturnTime.IsPrimaryKey = false;
                colvarReturnTime.IsForeignKey = false;
                colvarReturnTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnTime);
                
                TableSchema.TableColumn colvarApproveTime = new TableSchema.TableColumn(schema);
                colvarApproveTime.ColumnName = "approve_time";
                colvarApproveTime.DataType = DbType.DateTime;
                colvarApproveTime.MaxLength = 0;
                colvarApproveTime.AutoIncrement = false;
                colvarApproveTime.IsNullable = true;
                colvarApproveTime.IsPrimaryKey = false;
                colvarApproveTime.IsForeignKey = false;
                colvarApproveTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarApproveTime);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_seller_store",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewSellerStore()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewSellerStore(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewSellerStore(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewSellerStore(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("Guid");
		    }
            set 
		    {
			    SetColumnValue("Guid", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("Phone")]
        [Bindable(true)]
        public string Phone 
	    {
		    get
		    {
			    return GetColumnValue<string>("phone");
		    }
            set 
		    {
			    SetColumnValue("phone", value);
            }
        }
	      
        [XmlAttribute("TownshipId")]
        [Bindable(true)]
        public int? TownshipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("township_id");
		    }
            set 
		    {
			    SetColumnValue("township_id", value);
            }
        }
	      
        [XmlAttribute("AddressString")]
        [Bindable(true)]
        public string AddressString 
	    {
		    get
		    {
			    return GetColumnValue<string>("address_string");
		    }
            set 
		    {
			    SetColumnValue("address_string", value);
            }
        }
	      
        [XmlAttribute("OpenTime")]
        [Bindable(true)]
        public string OpenTime 
	    {
		    get
		    {
			    return GetColumnValue<string>("open_time");
		    }
            set 
		    {
			    SetColumnValue("open_time", value);
            }
        }
	      
        [XmlAttribute("CloseDate")]
        [Bindable(true)]
        public string CloseDate 
	    {
		    get
		    {
			    return GetColumnValue<string>("close_date");
		    }
            set 
		    {
			    SetColumnValue("close_date", value);
            }
        }
	      
        [XmlAttribute("Remarks")]
        [Bindable(true)]
        public string Remarks 
	    {
		    get
		    {
			    return GetColumnValue<string>("remarks");
		    }
            set 
		    {
			    SetColumnValue("remarks", value);
            }
        }
	      
        [XmlAttribute("Mrt")]
        [Bindable(true)]
        public string Mrt 
	    {
		    get
		    {
			    return GetColumnValue<string>("mrt");
		    }
            set 
		    {
			    SetColumnValue("mrt", value);
            }
        }
	      
        [XmlAttribute("Car")]
        [Bindable(true)]
        public string Car 
	    {
		    get
		    {
			    return GetColumnValue<string>("car");
		    }
            set 
		    {
			    SetColumnValue("car", value);
            }
        }
	      
        [XmlAttribute("Bus")]
        [Bindable(true)]
        public string Bus 
	    {
		    get
		    {
			    return GetColumnValue<string>("bus");
		    }
            set 
		    {
			    SetColumnValue("bus", value);
            }
        }
	      
        [XmlAttribute("OtherVehicles")]
        [Bindable(true)]
        public string OtherVehicles 
	    {
		    get
		    {
			    return GetColumnValue<string>("other_vehicles");
		    }
            set 
		    {
			    SetColumnValue("other_vehicles", value);
            }
        }
	      
        [XmlAttribute("WebUrl")]
        [Bindable(true)]
        public string WebUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("web_url");
		    }
            set 
		    {
			    SetColumnValue("web_url", value);
            }
        }
	      
        [XmlAttribute("FacebookUrl")]
        [Bindable(true)]
        public string FacebookUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("facebook_url");
		    }
            set 
		    {
			    SetColumnValue("facebook_url", value);
            }
        }
	      
        [XmlAttribute("PlurkUrl")]
        [Bindable(true)]
        public int? PlurkUrl 
	    {
		    get
		    {
			    return GetColumnValue<int?>("plurk_url");
		    }
            set 
		    {
			    SetColumnValue("plurk_url", value);
            }
        }
	      
        [XmlAttribute("BlogUrl")]
        [Bindable(true)]
        public string BlogUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("blog_url");
		    }
            set 
		    {
			    SetColumnValue("blog_url", value);
            }
        }
	      
        [XmlAttribute("OtherUrl")]
        [Bindable(true)]
        public string OtherUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("other_url");
		    }
            set 
		    {
			    SetColumnValue("other_url", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyName");
		    }
            set 
		    {
			    SetColumnValue("CompanyName", value);
            }
        }
	      
        [XmlAttribute("CompanyBossName")]
        [Bindable(true)]
        public string CompanyBossName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBossName");
		    }
            set 
		    {
			    SetColumnValue("CompanyBossName", value);
            }
        }
	      
        [XmlAttribute("CompanyID")]
        [Bindable(true)]
        public string CompanyID 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyID");
		    }
            set 
		    {
			    SetColumnValue("CompanyID", value);
            }
        }
	      
        [XmlAttribute("CompanyBankCode")]
        [Bindable(true)]
        public string CompanyBankCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBankCode");
		    }
            set 
		    {
			    SetColumnValue("CompanyBankCode", value);
            }
        }
	      
        [XmlAttribute("CompanyBranchCode")]
        [Bindable(true)]
        public string CompanyBranchCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBranchCode");
		    }
            set 
		    {
			    SetColumnValue("CompanyBranchCode", value);
            }
        }
	      
        [XmlAttribute("CompanyAccount")]
        [Bindable(true)]
        public string CompanyAccount 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyAccount");
		    }
            set 
		    {
			    SetColumnValue("CompanyAccount", value);
            }
        }
	      
        [XmlAttribute("CompanyAccountName")]
        [Bindable(true)]
        public string CompanyAccountName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyAccountName");
		    }
            set 
		    {
			    SetColumnValue("CompanyAccountName", value);
            }
        }
	      
        [XmlAttribute("CompanyEmail")]
        [Bindable(true)]
        public string CompanyEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyEmail");
		    }
            set 
		    {
			    SetColumnValue("CompanyEmail", value);
            }
        }
	      
        [XmlAttribute("CompanyNotice")]
        [Bindable(true)]
        public string CompanyNotice 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyNotice");
		    }
            set 
		    {
			    SetColumnValue("CompanyNotice", value);
            }
        }
	      
        [XmlAttribute("IsCloseDown")]
        [Bindable(true)]
        public bool IsCloseDown 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_close_down");
		    }
            set 
		    {
			    SetColumnValue("is_close_down", value);
            }
        }
	      
        [XmlAttribute("CloseDownDate")]
        [Bindable(true)]
        public DateTime? CloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("close_down_date");
		    }
            set 
		    {
			    SetColumnValue("close_down_date", value);
            }
        }
	      
        [XmlAttribute("SignCompanyID")]
        [Bindable(true)]
        public string SignCompanyID 
	    {
		    get
		    {
			    return GetColumnValue<string>("SignCompanyID");
		    }
            set 
		    {
			    SetColumnValue("SignCompanyID", value);
            }
        }
	      
        [XmlAttribute("AccountantName")]
        [Bindable(true)]
        public string AccountantName 
	    {
		    get
		    {
			    return GetColumnValue<string>("accountant_name");
		    }
            set 
		    {
			    SetColumnValue("accountant_name", value);
            }
        }
	      
        [XmlAttribute("AccountantTel")]
        [Bindable(true)]
        public string AccountantTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("accountant_tel");
		    }
            set 
		    {
			    SetColumnValue("accountant_tel", value);
            }
        }
	      
        [XmlAttribute("CreditcardAvailable")]
        [Bindable(true)]
        public bool CreditcardAvailable 
	    {
		    get
		    {
			    return GetColumnValue<bool>("creditcard_available");
		    }
            set 
		    {
			    SetColumnValue("creditcard_available", value);
            }
        }
	      
        [XmlAttribute("TempStatus")]
        [Bindable(true)]
        public int TempStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("temp_status");
		    }
            set 
		    {
			    SetColumnValue("temp_status", value);
            }
        }
	      
        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message 
	    {
		    get
		    {
			    return GetColumnValue<string>("message");
		    }
            set 
		    {
			    SetColumnValue("message", value);
            }
        }
	      
        [XmlAttribute("NewCreated")]
        [Bindable(true)]
        public bool NewCreated 
	    {
		    get
		    {
			    return GetColumnValue<bool>("new_created");
		    }
            set 
		    {
			    SetColumnValue("new_created", value);
            }
        }
	      
        [XmlAttribute("Coordinate")]
        [Bindable(true)]
        public string Coordinate 
	    {
		    get
		    {
			    return GetColumnValue<string>("coordinate");
		    }
            set 
		    {
			    SetColumnValue("coordinate", value);
            }
        }
	      
        [XmlAttribute("ApplyId")]
        [Bindable(true)]
        public string ApplyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("apply_id");
		    }
            set 
		    {
			    SetColumnValue("apply_id", value);
            }
        }
	      
        [XmlAttribute("ApplyTime")]
        [Bindable(true)]
        public DateTime? ApplyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("apply_time");
		    }
            set 
		    {
			    SetColumnValue("apply_time", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("ReturnTime")]
        [Bindable(true)]
        public DateTime? ReturnTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("return_time");
		    }
            set 
		    {
			    SetColumnValue("return_time", value);
            }
        }
	      
        [XmlAttribute("ApproveTime")]
        [Bindable(true)]
        public DateTime? ApproveTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("approve_time");
		    }
            set 
		    {
			    SetColumnValue("approve_time", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Guid = @"Guid";
            
            public static string StoreName = @"store_name";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string Phone = @"phone";
            
            public static string TownshipId = @"township_id";
            
            public static string AddressString = @"address_string";
            
            public static string OpenTime = @"open_time";
            
            public static string CloseDate = @"close_date";
            
            public static string Remarks = @"remarks";
            
            public static string Mrt = @"mrt";
            
            public static string Car = @"car";
            
            public static string Bus = @"bus";
            
            public static string OtherVehicles = @"other_vehicles";
            
            public static string WebUrl = @"web_url";
            
            public static string FacebookUrl = @"facebook_url";
            
            public static string PlurkUrl = @"plurk_url";
            
            public static string BlogUrl = @"blog_url";
            
            public static string OtherUrl = @"other_url";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string ModifyTime = @"modify_time";
            
            public static string Status = @"status";
            
            public static string CompanyName = @"CompanyName";
            
            public static string CompanyBossName = @"CompanyBossName";
            
            public static string CompanyID = @"CompanyID";
            
            public static string CompanyBankCode = @"CompanyBankCode";
            
            public static string CompanyBranchCode = @"CompanyBranchCode";
            
            public static string CompanyAccount = @"CompanyAccount";
            
            public static string CompanyAccountName = @"CompanyAccountName";
            
            public static string CompanyEmail = @"CompanyEmail";
            
            public static string CompanyNotice = @"CompanyNotice";
            
            public static string IsCloseDown = @"is_close_down";
            
            public static string CloseDownDate = @"close_down_date";
            
            public static string SignCompanyID = @"SignCompanyID";
            
            public static string AccountantName = @"accountant_name";
            
            public static string AccountantTel = @"accountant_tel";
            
            public static string CreditcardAvailable = @"creditcard_available";
            
            public static string TempStatus = @"temp_status";
            
            public static string Message = @"message";
            
            public static string NewCreated = @"new_created";
            
            public static string Coordinate = @"coordinate";
            
            public static string ApplyId = @"apply_id";
            
            public static string ApplyTime = @"apply_time";
            
            public static string CityId = @"city_id";
            
            public static string ReturnTime = @"return_time";
            
            public static string ApproveTime = @"approve_time";
            
            public static string SellerName = @"seller_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewCashTrustLogRetrnFormList class.
    /// </summary>
    [Serializable]
    public partial class ViewCashTrustLogRetrnFormListCollection : ReadOnlyList<ViewCashTrustLogRetrnFormList, ViewCashTrustLogRetrnFormListCollection>
    {        
        public ViewCashTrustLogRetrnFormListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_cash_trust_log_retrn_form_list view.
    /// </summary>
    [Serializable]
    public partial class ViewCashTrustLogRetrnFormList : ReadOnlyRecord<ViewCashTrustLogRetrnFormList>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_cash_trust_log_retrn_form_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
                colvarReturnFormId.ColumnName = "return_form_id";
                colvarReturnFormId.DataType = DbType.Int32;
                colvarReturnFormId.MaxLength = 0;
                colvarReturnFormId.AutoIncrement = false;
                colvarReturnFormId.IsNullable = false;
                colvarReturnFormId.IsPrimaryKey = false;
                colvarReturnFormId.IsForeignKey = false;
                colvarReturnFormId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnFormId);
                
                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustId);
                
                TableSchema.TableColumn colvarVendorNoRefund = new TableSchema.TableColumn(schema);
                colvarVendorNoRefund.ColumnName = "vendor_no_refund";
                colvarVendorNoRefund.DataType = DbType.Boolean;
                colvarVendorNoRefund.MaxLength = 0;
                colvarVendorNoRefund.AutoIncrement = false;
                colvarVendorNoRefund.IsNullable = false;
                colvarVendorNoRefund.IsPrimaryKey = false;
                colvarVendorNoRefund.IsForeignKey = false;
                colvarVendorNoRefund.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorNoRefund);
                
                TableSchema.TableColumn colvarIsRefunded = new TableSchema.TableColumn(schema);
                colvarIsRefunded.ColumnName = "is_refunded";
                colvarIsRefunded.DataType = DbType.Boolean;
                colvarIsRefunded.MaxLength = 0;
                colvarIsRefunded.AutoIncrement = false;
                colvarIsRefunded.IsNullable = true;
                colvarIsRefunded.IsPrimaryKey = false;
                colvarIsRefunded.IsForeignKey = false;
                colvarIsRefunded.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsRefunded);
                
                TableSchema.TableColumn colvarFailReason = new TableSchema.TableColumn(schema);
                colvarFailReason.ColumnName = "fail_reason";
                colvarFailReason.DataType = DbType.String;
                colvarFailReason.MaxLength = 200;
                colvarFailReason.AutoIncrement = false;
                colvarFailReason.IsNullable = true;
                colvarFailReason.IsPrimaryKey = false;
                colvarFailReason.IsForeignKey = false;
                colvarFailReason.IsReadOnly = false;
                
                schema.Columns.Add(colvarFailReason);
                
                TableSchema.TableColumn colvarIsFreight = new TableSchema.TableColumn(schema);
                colvarIsFreight.ColumnName = "is_freight";
                colvarIsFreight.DataType = DbType.Boolean;
                colvarIsFreight.MaxLength = 0;
                colvarIsFreight.AutoIncrement = false;
                colvarIsFreight.IsNullable = false;
                colvarIsFreight.IsPrimaryKey = false;
                colvarIsFreight.IsForeignKey = false;
                colvarIsFreight.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsFreight);
                
                TableSchema.TableColumn colvarIsInRetry = new TableSchema.TableColumn(schema);
                colvarIsInRetry.ColumnName = "is_in_retry";
                colvarIsInRetry.DataType = DbType.Boolean;
                colvarIsInRetry.MaxLength = 0;
                colvarIsInRetry.AutoIncrement = false;
                colvarIsInRetry.IsNullable = false;
                colvarIsInRetry.IsPrimaryKey = false;
                colvarIsInRetry.IsForeignKey = false;
                colvarIsInRetry.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsInRetry);
                
                TableSchema.TableColumn colvarUsageVerifiedTime = new TableSchema.TableColumn(schema);
                colvarUsageVerifiedTime.ColumnName = "usage_verified_time";
                colvarUsageVerifiedTime.DataType = DbType.DateTime;
                colvarUsageVerifiedTime.MaxLength = 0;
                colvarUsageVerifiedTime.AutoIncrement = false;
                colvarUsageVerifiedTime.IsNullable = true;
                colvarUsageVerifiedTime.IsPrimaryKey = false;
                colvarUsageVerifiedTime.IsForeignKey = false;
                colvarUsageVerifiedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUsageVerifiedTime);
                
                TableSchema.TableColumn colvarTrustStatus = new TableSchema.TableColumn(schema);
                colvarTrustStatus.ColumnName = "trust_status";
                colvarTrustStatus.DataType = DbType.Int32;
                colvarTrustStatus.MaxLength = 0;
                colvarTrustStatus.AutoIncrement = false;
                colvarTrustStatus.IsNullable = false;
                colvarTrustStatus.IsPrimaryKey = false;
                colvarTrustStatus.IsForeignKey = false;
                colvarTrustStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustStatus);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = false;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_cash_trust_log_retrn_form_list",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewCashTrustLogRetrnFormList()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewCashTrustLogRetrnFormList(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewCashTrustLogRetrnFormList(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewCashTrustLogRetrnFormList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ReturnFormId")]
        [Bindable(true)]
        public int ReturnFormId 
	    {
		    get
		    {
			    return GetColumnValue<int>("return_form_id");
		    }
            set 
		    {
			    SetColumnValue("return_form_id", value);
            }
        }
	      
        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("trust_id");
		    }
            set 
		    {
			    SetColumnValue("trust_id", value);
            }
        }
	      
        [XmlAttribute("VendorNoRefund")]
        [Bindable(true)]
        public bool VendorNoRefund 
	    {
		    get
		    {
			    return GetColumnValue<bool>("vendor_no_refund");
		    }
            set 
		    {
			    SetColumnValue("vendor_no_refund", value);
            }
        }
	      
        [XmlAttribute("IsRefunded")]
        [Bindable(true)]
        public bool? IsRefunded 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_refunded");
		    }
            set 
		    {
			    SetColumnValue("is_refunded", value);
            }
        }
	      
        [XmlAttribute("FailReason")]
        [Bindable(true)]
        public string FailReason 
	    {
		    get
		    {
			    return GetColumnValue<string>("fail_reason");
		    }
            set 
		    {
			    SetColumnValue("fail_reason", value);
            }
        }
	      
        [XmlAttribute("IsFreight")]
        [Bindable(true)]
        public bool IsFreight 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_freight");
		    }
            set 
		    {
			    SetColumnValue("is_freight", value);
            }
        }
	      
        [XmlAttribute("IsInRetry")]
        [Bindable(true)]
        public bool IsInRetry 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_in_retry");
		    }
            set 
		    {
			    SetColumnValue("is_in_retry", value);
            }
        }
	      
        [XmlAttribute("UsageVerifiedTime")]
        [Bindable(true)]
        public DateTime? UsageVerifiedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("usage_verified_time");
		    }
            set 
		    {
			    SetColumnValue("usage_verified_time", value);
            }
        }
	      
        [XmlAttribute("TrustStatus")]
        [Bindable(true)]
        public int TrustStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("trust_status");
		    }
            set 
		    {
			    SetColumnValue("trust_status", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ReturnFormId = @"return_form_id";
            
            public static string TrustId = @"trust_id";
            
            public static string VendorNoRefund = @"vendor_no_refund";
            
            public static string IsRefunded = @"is_refunded";
            
            public static string FailReason = @"fail_reason";
            
            public static string IsFreight = @"is_freight";
            
            public static string IsInRetry = @"is_in_retry";
            
            public static string UsageVerifiedTime = @"usage_verified_time";
            
            public static string TrustStatus = @"trust_status";
            
            public static string DeliveryType = @"delivery_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

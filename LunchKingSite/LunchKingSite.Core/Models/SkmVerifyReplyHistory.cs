using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmVerifyReplyHistory class.
	/// </summary>
    [Serializable]
	public partial class SkmVerifyReplyHistoryCollection : RepositoryList<SkmVerifyReplyHistory, SkmVerifyReplyHistoryCollection>
	{	   
		public SkmVerifyReplyHistoryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmVerifyReplyHistoryCollection</returns>
		public SkmVerifyReplyHistoryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmVerifyReplyHistory o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_verify_reply_history table.
	/// </summary>
	
	[Serializable]
	public partial class SkmVerifyReplyHistory : RepositoryRecord<SkmVerifyReplyHistory>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmVerifyReplyHistory()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmVerifyReplyHistory(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_verify_reply_history", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);
				
				TableSchema.TableColumn colvarSkmTmCode = new TableSchema.TableColumn(schema);
				colvarSkmTmCode.ColumnName = "skm_tm_code";
				colvarSkmTmCode.DataType = DbType.AnsiString;
				colvarSkmTmCode.MaxLength = 20;
				colvarSkmTmCode.AutoIncrement = false;
				colvarSkmTmCode.IsNullable = false;
				colvarSkmTmCode.IsPrimaryKey = false;
				colvarSkmTmCode.IsForeignKey = false;
				colvarSkmTmCode.IsReadOnly = false;
				colvarSkmTmCode.DefaultSetting = @"";
				colvarSkmTmCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmTmCode);
				
				TableSchema.TableColumn colvarSkmReplySn = new TableSchema.TableColumn(schema);
				colvarSkmReplySn.ColumnName = "skm_reply_sn";
				colvarSkmReplySn.DataType = DbType.AnsiString;
				colvarSkmReplySn.MaxLength = 10;
				colvarSkmReplySn.AutoIncrement = false;
				colvarSkmReplySn.IsNullable = false;
				colvarSkmReplySn.IsPrimaryKey = false;
				colvarSkmReplySn.IsForeignKey = false;
				colvarSkmReplySn.IsReadOnly = false;
				colvarSkmReplySn.DefaultSetting = @"";
				colvarSkmReplySn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmReplySn);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarIsCancel = new TableSchema.TableColumn(schema);
				colvarIsCancel.ColumnName = "is_cancel";
				colvarIsCancel.DataType = DbType.Boolean;
				colvarIsCancel.MaxLength = 0;
				colvarIsCancel.AutoIncrement = false;
				colvarIsCancel.IsNullable = false;
				colvarIsCancel.IsPrimaryKey = false;
				colvarIsCancel.IsForeignKey = false;
				colvarIsCancel.IsReadOnly = false;
				
						colvarIsCancel.DefaultSetting = @"((0))";
				colvarIsCancel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCancel);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_verify_reply_history",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		
		[XmlAttribute("SkmTmCode")]
		[Bindable(true)]
		public string SkmTmCode 
		{
			get { return GetColumnValue<string>(Columns.SkmTmCode); }
			set { SetColumnValue(Columns.SkmTmCode, value); }
		}
		
		[XmlAttribute("SkmReplySn")]
		[Bindable(true)]
		public string SkmReplySn 
		{
			get { return GetColumnValue<string>(Columns.SkmReplySn); }
			set { SetColumnValue(Columns.SkmReplySn, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("IsCancel")]
		[Bindable(true)]
		public bool IsCancel 
		{
			get { return GetColumnValue<bool>(Columns.IsCancel); }
			set { SetColumnValue(Columns.IsCancel, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmTmCodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmReplySnColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCancelColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TrustId = @"trust_id";
			 public static string SkmTmCode = @"skm_tm_code";
			 public static string SkmReplySn = @"skm_reply_sn";
			 public static string CreateTime = @"create_time";
			 public static string IsCancel = @"is_cancel";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBalanceSheetList class.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetListCollection : ReadOnlyList<ViewBalanceSheetList, ViewBalanceSheetListCollection>
    {        
        public ViewBalanceSheetListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_balance_sheet_list view.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetList : ReadOnlyRecord<ViewBalanceSheetList>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_balance_sheet_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
                colvarProductGuid.ColumnName = "product_guid";
                colvarProductGuid.DataType = DbType.Guid;
                colvarProductGuid.MaxLength = 0;
                colvarProductGuid.AutoIncrement = false;
                colvarProductGuid.IsNullable = false;
                colvarProductGuid.IsPrimaryKey = false;
                colvarProductGuid.IsForeignKey = false;
                colvarProductGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductGuid);
                
                TableSchema.TableColumn colvarProductType = new TableSchema.TableColumn(schema);
                colvarProductType.ColumnName = "product_type";
                colvarProductType.DataType = DbType.Int32;
                colvarProductType.MaxLength = 0;
                colvarProductType.AutoIncrement = false;
                colvarProductType.IsNullable = false;
                colvarProductType.IsPrimaryKey = false;
                colvarProductType.IsForeignKey = false;
                colvarProductType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductType);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarYear = new TableSchema.TableColumn(schema);
                colvarYear.ColumnName = "year";
                colvarYear.DataType = DbType.Int32;
                colvarYear.MaxLength = 0;
                colvarYear.AutoIncrement = false;
                colvarYear.IsNullable = true;
                colvarYear.IsPrimaryKey = false;
                colvarYear.IsForeignKey = false;
                colvarYear.IsReadOnly = false;
                
                schema.Columns.Add(colvarYear);
                
                TableSchema.TableColumn colvarMonth = new TableSchema.TableColumn(schema);
                colvarMonth.ColumnName = "month";
                colvarMonth.DataType = DbType.Int32;
                colvarMonth.MaxLength = 0;
                colvarMonth.AutoIncrement = false;
                colvarMonth.IsNullable = true;
                colvarMonth.IsPrimaryKey = false;
                colvarMonth.IsForeignKey = false;
                colvarMonth.IsReadOnly = false;
                
                schema.Columns.Add(colvarMonth);
                
                TableSchema.TableColumn colvarGenerationFrequency = new TableSchema.TableColumn(schema);
                colvarGenerationFrequency.ColumnName = "generation_frequency";
                colvarGenerationFrequency.DataType = DbType.Int32;
                colvarGenerationFrequency.MaxLength = 0;
                colvarGenerationFrequency.AutoIncrement = false;
                colvarGenerationFrequency.IsNullable = false;
                colvarGenerationFrequency.IsPrimaryKey = false;
                colvarGenerationFrequency.IsForeignKey = false;
                colvarGenerationFrequency.IsReadOnly = false;
                
                schema.Columns.Add(colvarGenerationFrequency);
                
                TableSchema.TableColumn colvarIsConfirmedReadyToPay = new TableSchema.TableColumn(schema);
                colvarIsConfirmedReadyToPay.ColumnName = "is_confirmed_ready_to_pay";
                colvarIsConfirmedReadyToPay.DataType = DbType.Boolean;
                colvarIsConfirmedReadyToPay.MaxLength = 0;
                colvarIsConfirmedReadyToPay.AutoIncrement = false;
                colvarIsConfirmedReadyToPay.IsNullable = false;
                colvarIsConfirmedReadyToPay.IsPrimaryKey = false;
                colvarIsConfirmedReadyToPay.IsForeignKey = false;
                colvarIsConfirmedReadyToPay.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsConfirmedReadyToPay);
                
                TableSchema.TableColumn colvarConfirmedUserName = new TableSchema.TableColumn(schema);
                colvarConfirmedUserName.ColumnName = "confirmed_user_name";
                colvarConfirmedUserName.DataType = DbType.String;
                colvarConfirmedUserName.MaxLength = 256;
                colvarConfirmedUserName.AutoIncrement = false;
                colvarConfirmedUserName.IsNullable = true;
                colvarConfirmedUserName.IsPrimaryKey = false;
                colvarConfirmedUserName.IsForeignKey = false;
                colvarConfirmedUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarConfirmedUserName);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 500;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
                colvarAppTitle.ColumnName = "app_title";
                colvarAppTitle.DataType = DbType.String;
                colvarAppTitle.MaxLength = 40;
                colvarAppTitle.AutoIncrement = false;
                colvarAppTitle.IsNullable = true;
                colvarAppTitle.IsPrimaryKey = false;
                colvarAppTitle.IsForeignKey = false;
                colvarAppTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarAppTitle);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarVendorReceiptType = new TableSchema.TableColumn(schema);
                colvarVendorReceiptType.ColumnName = "vendor_receipt_type";
                colvarVendorReceiptType.DataType = DbType.Int32;
                colvarVendorReceiptType.MaxLength = 0;
                colvarVendorReceiptType.AutoIncrement = false;
                colvarVendorReceiptType.IsNullable = false;
                colvarVendorReceiptType.IsPrimaryKey = false;
                colvarVendorReceiptType.IsForeignKey = false;
                colvarVendorReceiptType.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorReceiptType);
                
                TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
                colvarRemittanceType.ColumnName = "remittance_type";
                colvarRemittanceType.DataType = DbType.Int32;
                colvarRemittanceType.MaxLength = 0;
                colvarRemittanceType.AutoIncrement = false;
                colvarRemittanceType.IsNullable = false;
                colvarRemittanceType.IsPrimaryKey = false;
                colvarRemittanceType.IsForeignKey = false;
                colvarRemittanceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemittanceType);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = true;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = true;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
                colvarUseStartTime.ColumnName = "use_start_time";
                colvarUseStartTime.DataType = DbType.DateTime;
                colvarUseStartTime.MaxLength = 0;
                colvarUseStartTime.AutoIncrement = false;
                colvarUseStartTime.IsNullable = true;
                colvarUseStartTime.IsPrimaryKey = false;
                colvarUseStartTime.IsForeignKey = false;
                colvarUseStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseStartTime);
                
                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.DateTime;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseEndTime);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarVerificationStatisticsLogId = new TableSchema.TableColumn(schema);
                colvarVerificationStatisticsLogId.ColumnName = "verification_statistics_log_id";
                colvarVerificationStatisticsLogId.DataType = DbType.Int32;
                colvarVerificationStatisticsLogId.MaxLength = 0;
                colvarVerificationStatisticsLogId.AutoIncrement = false;
                colvarVerificationStatisticsLogId.IsNullable = false;
                colvarVerificationStatisticsLogId.IsPrimaryKey = false;
                colvarVerificationStatisticsLogId.IsForeignKey = false;
                colvarVerificationStatisticsLogId.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerificationStatisticsLogId);
                
                TableSchema.TableColumn colvarIntervalStart = new TableSchema.TableColumn(schema);
                colvarIntervalStart.ColumnName = "interval_start";
                colvarIntervalStart.DataType = DbType.DateTime;
                colvarIntervalStart.MaxLength = 0;
                colvarIntervalStart.AutoIncrement = false;
                colvarIntervalStart.IsNullable = false;
                colvarIntervalStart.IsPrimaryKey = false;
                colvarIntervalStart.IsForeignKey = false;
                colvarIntervalStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarIntervalStart);
                
                TableSchema.TableColumn colvarIntervalEnd = new TableSchema.TableColumn(schema);
                colvarIntervalEnd.ColumnName = "interval_end";
                colvarIntervalEnd.DataType = DbType.DateTime;
                colvarIntervalEnd.MaxLength = 0;
                colvarIntervalEnd.AutoIncrement = false;
                colvarIntervalEnd.IsNullable = false;
                colvarIntervalEnd.IsPrimaryKey = false;
                colvarIntervalEnd.IsForeignKey = false;
                colvarIntervalEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarIntervalEnd);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = true;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarIsReceiptReceived = new TableSchema.TableColumn(schema);
                colvarIsReceiptReceived.ColumnName = "is_receipt_received";
                colvarIsReceiptReceived.DataType = DbType.Boolean;
                colvarIsReceiptReceived.MaxLength = 0;
                colvarIsReceiptReceived.AutoIncrement = false;
                colvarIsReceiptReceived.IsNullable = false;
                colvarIsReceiptReceived.IsPrimaryKey = false;
                colvarIsReceiptReceived.IsForeignKey = false;
                colvarIsReceiptReceived.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsReceiptReceived);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarEstAmount = new TableSchema.TableColumn(schema);
                colvarEstAmount.ColumnName = "est_amount";
                colvarEstAmount.DataType = DbType.Int32;
                colvarEstAmount.MaxLength = 0;
                colvarEstAmount.AutoIncrement = false;
                colvarEstAmount.IsNullable = true;
                colvarEstAmount.IsPrimaryKey = false;
                colvarEstAmount.IsForeignKey = false;
                colvarEstAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarEstAmount);
                
                TableSchema.TableColumn colvarIspFamilyAmount = new TableSchema.TableColumn(schema);
                colvarIspFamilyAmount.ColumnName = "isp_family_amount";
                colvarIspFamilyAmount.DataType = DbType.Int32;
                colvarIspFamilyAmount.MaxLength = 0;
                colvarIspFamilyAmount.AutoIncrement = false;
                colvarIspFamilyAmount.IsNullable = false;
                colvarIspFamilyAmount.IsPrimaryKey = false;
                colvarIspFamilyAmount.IsForeignKey = false;
                colvarIspFamilyAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarIspFamilyAmount);
                
                TableSchema.TableColumn colvarIspSevenAmount = new TableSchema.TableColumn(schema);
                colvarIspSevenAmount.ColumnName = "isp_seven_amount";
                colvarIspSevenAmount.DataType = DbType.Int32;
                colvarIspSevenAmount.MaxLength = 0;
                colvarIspSevenAmount.AutoIncrement = false;
                colvarIspSevenAmount.IsNullable = false;
                colvarIspSevenAmount.IsPrimaryKey = false;
                colvarIspSevenAmount.IsForeignKey = false;
                colvarIspSevenAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarIspSevenAmount);
                
                TableSchema.TableColumn colvarOverdueAmount = new TableSchema.TableColumn(schema);
                colvarOverdueAmount.ColumnName = "overdue_amount";
                colvarOverdueAmount.DataType = DbType.Int32;
                colvarOverdueAmount.MaxLength = 0;
                colvarOverdueAmount.AutoIncrement = false;
                colvarOverdueAmount.IsNullable = false;
                colvarOverdueAmount.IsPrimaryKey = false;
                colvarOverdueAmount.IsForeignKey = false;
                colvarOverdueAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOverdueAmount);
                
                TableSchema.TableColumn colvarWmsOrderAmount = new TableSchema.TableColumn(schema);
                colvarWmsOrderAmount.ColumnName = "wms_order_amount";
                colvarWmsOrderAmount.DataType = DbType.Int32;
                colvarWmsOrderAmount.MaxLength = 0;
                colvarWmsOrderAmount.AutoIncrement = false;
                colvarWmsOrderAmount.IsNullable = false;
                colvarWmsOrderAmount.IsPrimaryKey = false;
                colvarWmsOrderAmount.IsForeignKey = false;
                colvarWmsOrderAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarWmsOrderAmount);
                
                TableSchema.TableColumn colvarTransferAmount = new TableSchema.TableColumn(schema);
                colvarTransferAmount.ColumnName = "transfer_amount";
                colvarTransferAmount.DataType = DbType.Decimal;
                colvarTransferAmount.MaxLength = 0;
                colvarTransferAmount.AutoIncrement = false;
                colvarTransferAmount.IsNullable = true;
                colvarTransferAmount.IsPrimaryKey = false;
                colvarTransferAmount.IsForeignKey = false;
                colvarTransferAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarTransferAmount);
                
                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Currency;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = true;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarCost);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarBalanceSheetType = new TableSchema.TableColumn(schema);
                colvarBalanceSheetType.ColumnName = "balance_sheet_type";
                colvarBalanceSheetType.DataType = DbType.Int32;
                colvarBalanceSheetType.MaxLength = 0;
                colvarBalanceSheetType.AutoIncrement = false;
                colvarBalanceSheetType.IsNullable = false;
                colvarBalanceSheetType.IsPrimaryKey = false;
                colvarBalanceSheetType.IsForeignKey = false;
                colvarBalanceSheetType.IsReadOnly = false;
                
                schema.Columns.Add(colvarBalanceSheetType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_balance_sheet_list",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBalanceSheetList()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBalanceSheetList(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBalanceSheetList(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBalanceSheetList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("ProductGuid")]
        [Bindable(true)]
        public Guid ProductGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("product_guid");
		    }
            set 
		    {
			    SetColumnValue("product_guid", value);
            }
        }
	      
        [XmlAttribute("ProductType")]
        [Bindable(true)]
        public int ProductType 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_type");
		    }
            set 
		    {
			    SetColumnValue("product_type", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("Year")]
        [Bindable(true)]
        public int? Year 
	    {
		    get
		    {
			    return GetColumnValue<int?>("year");
		    }
            set 
		    {
			    SetColumnValue("year", value);
            }
        }
	      
        [XmlAttribute("Month")]
        [Bindable(true)]
        public int? Month 
	    {
		    get
		    {
			    return GetColumnValue<int?>("month");
		    }
            set 
		    {
			    SetColumnValue("month", value);
            }
        }
	      
        [XmlAttribute("GenerationFrequency")]
        [Bindable(true)]
        public int GenerationFrequency 
	    {
		    get
		    {
			    return GetColumnValue<int>("generation_frequency");
		    }
            set 
		    {
			    SetColumnValue("generation_frequency", value);
            }
        }
	      
        [XmlAttribute("IsConfirmedReadyToPay")]
        [Bindable(true)]
        public bool IsConfirmedReadyToPay 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_confirmed_ready_to_pay");
		    }
            set 
		    {
			    SetColumnValue("is_confirmed_ready_to_pay", value);
            }
        }
	      
        [XmlAttribute("ConfirmedUserName")]
        [Bindable(true)]
        public string ConfirmedUserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("confirmed_user_name");
		    }
            set 
		    {
			    SetColumnValue("confirmed_user_name", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("AppTitle")]
        [Bindable(true)]
        public string AppTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("app_title");
		    }
            set 
		    {
			    SetColumnValue("app_title", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("VendorReceiptType")]
        [Bindable(true)]
        public int VendorReceiptType 
	    {
		    get
		    {
			    return GetColumnValue<int>("vendor_receipt_type");
		    }
            set 
		    {
			    SetColumnValue("vendor_receipt_type", value);
            }
        }
	      
        [XmlAttribute("RemittanceType")]
        [Bindable(true)]
        public int RemittanceType 
	    {
		    get
		    {
			    return GetColumnValue<int>("remittance_type");
		    }
            set 
		    {
			    SetColumnValue("remittance_type", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("UseStartTime")]
        [Bindable(true)]
        public DateTime? UseStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_start_time");
		    }
            set 
		    {
			    SetColumnValue("use_start_time", value);
            }
        }
	      
        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public DateTime? UseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_end_time");
		    }
            set 
		    {
			    SetColumnValue("use_end_time", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("VerificationStatisticsLogId")]
        [Bindable(true)]
        public int VerificationStatisticsLogId 
	    {
		    get
		    {
			    return GetColumnValue<int>("verification_statistics_log_id");
		    }
            set 
		    {
			    SetColumnValue("verification_statistics_log_id", value);
            }
        }
	      
        [XmlAttribute("IntervalStart")]
        [Bindable(true)]
        public DateTime IntervalStart 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("interval_start");
		    }
            set 
		    {
			    SetColumnValue("interval_start", value);
            }
        }
	      
        [XmlAttribute("IntervalEnd")]
        [Bindable(true)]
        public DateTime IntervalEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("interval_end");
		    }
            set 
		    {
			    SetColumnValue("interval_end", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int? DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("IsReceiptReceived")]
        [Bindable(true)]
        public bool IsReceiptReceived 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_receipt_received");
		    }
            set 
		    {
			    SetColumnValue("is_receipt_received", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("EstAmount")]
        [Bindable(true)]
        public int? EstAmount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("est_amount");
		    }
            set 
		    {
			    SetColumnValue("est_amount", value);
            }
        }
	      
        [XmlAttribute("IspFamilyAmount")]
        [Bindable(true)]
        public int IspFamilyAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("isp_family_amount");
		    }
            set 
		    {
			    SetColumnValue("isp_family_amount", value);
            }
        }
	      
        [XmlAttribute("IspSevenAmount")]
        [Bindable(true)]
        public int IspSevenAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("isp_seven_amount");
		    }
            set 
		    {
			    SetColumnValue("isp_seven_amount", value);
            }
        }
	      
        [XmlAttribute("OverdueAmount")]
        [Bindable(true)]
        public int OverdueAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("overdue_amount");
		    }
            set 
		    {
			    SetColumnValue("overdue_amount", value);
            }
        }
	      
        [XmlAttribute("WmsOrderAmount")]
        [Bindable(true)]
        public int WmsOrderAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("wms_order_amount");
		    }
            set 
		    {
			    SetColumnValue("wms_order_amount", value);
            }
        }
	      
        [XmlAttribute("TransferAmount")]
        [Bindable(true)]
        public decimal? TransferAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("transfer_amount");
		    }
            set 
		    {
			    SetColumnValue("transfer_amount", value);
            }
        }
	      
        [XmlAttribute("Cost")]
        [Bindable(true)]
        public decimal? Cost 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("cost");
		    }
            set 
		    {
			    SetColumnValue("cost", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("BalanceSheetType")]
        [Bindable(true)]
        public int BalanceSheetType 
	    {
		    get
		    {
			    return GetColumnValue<int>("balance_sheet_type");
		    }
            set 
		    {
			    SetColumnValue("balance_sheet_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string ProductGuid = @"product_guid";
            
            public static string ProductType = @"product_type";
            
            public static string StoreGuid = @"store_guid";
            
            public static string Year = @"year";
            
            public static string Month = @"month";
            
            public static string GenerationFrequency = @"generation_frequency";
            
            public static string IsConfirmedReadyToPay = @"is_confirmed_ready_to_pay";
            
            public static string ConfirmedUserName = @"confirmed_user_name";
            
            public static string Name = @"name";
            
            public static string AppTitle = @"app_title";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string StoreName = @"store_name";
            
            public static string VendorReceiptType = @"vendor_receipt_type";
            
            public static string RemittanceType = @"remittance_type";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string UseStartTime = @"use_start_time";
            
            public static string UseEndTime = @"use_end_time";
            
            public static string UniqueId = @"unique_id";
            
            public static string VerificationStatisticsLogId = @"verification_statistics_log_id";
            
            public static string IntervalStart = @"interval_start";
            
            public static string IntervalEnd = @"interval_end";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string IsReceiptReceived = @"is_receipt_received";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerId = @"seller_id";
            
            public static string EstAmount = @"est_amount";
            
            public static string IspFamilyAmount = @"isp_family_amount";
            
            public static string IspSevenAmount = @"isp_seven_amount";
            
            public static string OverdueAmount = @"overdue_amount";
            
            public static string WmsOrderAmount = @"wms_order_amount";
            
            public static string TransferAmount = @"transfer_amount";
            
            public static string Cost = @"cost";
            
            public static string CreateTime = @"create_time";
            
            public static string BalanceSheetType = @"balance_sheet_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealCoupon class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealCouponCollection : ReadOnlyList<ViewHiDealCoupon, ViewHiDealCouponCollection>
    {        
        public ViewHiDealCouponCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_coupon view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealCoupon : ReadOnlyRecord<ViewHiDealCoupon>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_coupon", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int64;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponId);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarOrderPk = new TableSchema.TableColumn(schema);
                colvarOrderPk.ColumnName = "order_pk";
                colvarOrderPk.DataType = DbType.Int32;
                colvarOrderPk.MaxLength = 0;
                colvarOrderPk.AutoIncrement = false;
                colvarOrderPk.IsNullable = true;
                colvarOrderPk.IsPrimaryKey = false;
                colvarOrderPk.IsForeignKey = false;
                colvarOrderPk.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderPk);
                
                TableSchema.TableColumn colvarPrefix = new TableSchema.TableColumn(schema);
                colvarPrefix.ColumnName = "prefix";
                colvarPrefix.DataType = DbType.AnsiString;
                colvarPrefix.MaxLength = 10;
                colvarPrefix.AutoIncrement = false;
                colvarPrefix.IsNullable = true;
                colvarPrefix.IsPrimaryKey = false;
                colvarPrefix.IsForeignKey = false;
                colvarPrefix.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrefix);
                
                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.AnsiString;
                colvarSequence.MaxLength = 20;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = true;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequence);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 10;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCreateDate = new TableSchema.TableColumn(schema);
                colvarCreateDate.ColumnName = "create_date";
                colvarCreateDate.DataType = DbType.DateTime;
                colvarCreateDate.MaxLength = 0;
                colvarCreateDate.AutoIncrement = false;
                colvarCreateDate.IsNullable = true;
                colvarCreateDate.IsPrimaryKey = false;
                colvarCreateDate.IsForeignKey = false;
                colvarCreateDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateDate);
                
                TableSchema.TableColumn colvarBoughtDate = new TableSchema.TableColumn(schema);
                colvarBoughtDate.ColumnName = "bought_date";
                colvarBoughtDate.DataType = DbType.DateTime;
                colvarBoughtDate.MaxLength = 0;
                colvarBoughtDate.AutoIncrement = false;
                colvarBoughtDate.IsNullable = true;
                colvarBoughtDate.IsPrimaryKey = false;
                colvarBoughtDate.IsForeignKey = false;
                colvarBoughtDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarBoughtDate);
                
                TableSchema.TableColumn colvarUsedDate = new TableSchema.TableColumn(schema);
                colvarUsedDate.ColumnName = "used_date";
                colvarUsedDate.DataType = DbType.DateTime;
                colvarUsedDate.MaxLength = 0;
                colvarUsedDate.AutoIncrement = false;
                colvarUsedDate.IsNullable = true;
                colvarUsedDate.IsPrimaryKey = false;
                colvarUsedDate.IsForeignKey = false;
                colvarUsedDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarUsedDate);
                
                TableSchema.TableColumn colvarRefundDate = new TableSchema.TableColumn(schema);
                colvarRefundDate.ColumnName = "refund_date";
                colvarRefundDate.DataType = DbType.DateTime;
                colvarRefundDate.MaxLength = 0;
                colvarRefundDate.AutoIncrement = false;
                colvarRefundDate.IsNullable = true;
                colvarRefundDate.IsPrimaryKey = false;
                colvarRefundDate.IsForeignKey = false;
                colvarRefundDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarRefundDate);
                
                TableSchema.TableColumn colvarHiDealOrderDetailGuid = new TableSchema.TableColumn(schema);
                colvarHiDealOrderDetailGuid.ColumnName = "hi_deal_order_detail_guid";
                colvarHiDealOrderDetailGuid.DataType = DbType.Guid;
                colvarHiDealOrderDetailGuid.MaxLength = 0;
                colvarHiDealOrderDetailGuid.AutoIncrement = false;
                colvarHiDealOrderDetailGuid.IsNullable = false;
                colvarHiDealOrderDetailGuid.IsPrimaryKey = false;
                colvarHiDealOrderDetailGuid.IsForeignKey = false;
                colvarHiDealOrderDetailGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealOrderDetailGuid);
                
                TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
                colvarItemQuantity.ColumnName = "item_quantity";
                colvarItemQuantity.DataType = DbType.Int32;
                colvarItemQuantity.MaxLength = 0;
                colvarItemQuantity.AutoIncrement = false;
                colvarItemQuantity.IsNullable = false;
                colvarItemQuantity.IsPrimaryKey = false;
                colvarItemQuantity.IsForeignKey = false;
                colvarItemQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemQuantity);
                
                TableSchema.TableColumn colvarUnitPrice = new TableSchema.TableColumn(schema);
                colvarUnitPrice.ColumnName = "unit_price";
                colvarUnitPrice.DataType = DbType.Currency;
                colvarUnitPrice.MaxLength = 0;
                colvarUnitPrice.AutoIncrement = false;
                colvarUnitPrice.IsNullable = false;
                colvarUnitPrice.IsPrimaryKey = false;
                colvarUnitPrice.IsForeignKey = false;
                colvarUnitPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarUnitPrice);
                
                TableSchema.TableColumn colvarDetailTotalAmt = new TableSchema.TableColumn(schema);
                colvarDetailTotalAmt.ColumnName = "detail_total_amt";
                colvarDetailTotalAmt.DataType = DbType.Currency;
                colvarDetailTotalAmt.MaxLength = 0;
                colvarDetailTotalAmt.AutoIncrement = false;
                colvarDetailTotalAmt.IsNullable = false;
                colvarDetailTotalAmt.IsPrimaryKey = false;
                colvarDetailTotalAmt.IsForeignKey = false;
                colvarDetailTotalAmt.IsReadOnly = false;
                
                schema.Columns.Add(colvarDetailTotalAmt);
                
                TableSchema.TableColumn colvarUnitCost = new TableSchema.TableColumn(schema);
                colvarUnitCost.ColumnName = "unit_cost";
                colvarUnitCost.DataType = DbType.Currency;
                colvarUnitCost.MaxLength = 0;
                colvarUnitCost.AutoIncrement = false;
                colvarUnitCost.IsNullable = false;
                colvarUnitCost.IsPrimaryKey = false;
                colvarUnitCost.IsForeignKey = false;
                colvarUnitCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarUnitCost);
                
                TableSchema.TableColumn colvarTotalCost = new TableSchema.TableColumn(schema);
                colvarTotalCost.ColumnName = "total_cost";
                colvarTotalCost.DataType = DbType.Currency;
                colvarTotalCost.MaxLength = 0;
                colvarTotalCost.AutoIncrement = false;
                colvarTotalCost.IsNullable = false;
                colvarTotalCost.IsPrimaryKey = false;
                colvarTotalCost.IsForeignKey = false;
                colvarTotalCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalCost);
                
                TableSchema.TableColumn colvarCategory1 = new TableSchema.TableColumn(schema);
                colvarCategory1.ColumnName = "category1";
                colvarCategory1.DataType = DbType.Int32;
                colvarCategory1.MaxLength = 0;
                colvarCategory1.AutoIncrement = false;
                colvarCategory1.IsNullable = true;
                colvarCategory1.IsPrimaryKey = false;
                colvarCategory1.IsForeignKey = false;
                colvarCategory1.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory1);
                
                TableSchema.TableColumn colvarCatgName1 = new TableSchema.TableColumn(schema);
                colvarCatgName1.ColumnName = "catg_name1";
                colvarCatgName1.DataType = DbType.String;
                colvarCatgName1.MaxLength = 25;
                colvarCatgName1.AutoIncrement = false;
                colvarCatgName1.IsNullable = true;
                colvarCatgName1.IsPrimaryKey = false;
                colvarCatgName1.IsForeignKey = false;
                colvarCatgName1.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgName1);
                
                TableSchema.TableColumn colvarOption1 = new TableSchema.TableColumn(schema);
                colvarOption1.ColumnName = "option1";
                colvarOption1.DataType = DbType.Int32;
                colvarOption1.MaxLength = 0;
                colvarOption1.AutoIncrement = false;
                colvarOption1.IsNullable = true;
                colvarOption1.IsPrimaryKey = false;
                colvarOption1.IsForeignKey = false;
                colvarOption1.IsReadOnly = false;
                
                schema.Columns.Add(colvarOption1);
                
                TableSchema.TableColumn colvarOptionName1 = new TableSchema.TableColumn(schema);
                colvarOptionName1.ColumnName = "option_name1";
                colvarOptionName1.DataType = DbType.String;
                colvarOptionName1.MaxLength = 50;
                colvarOptionName1.AutoIncrement = false;
                colvarOptionName1.IsNullable = true;
                colvarOptionName1.IsPrimaryKey = false;
                colvarOptionName1.IsForeignKey = false;
                colvarOptionName1.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName1);
                
                TableSchema.TableColumn colvarCategory2 = new TableSchema.TableColumn(schema);
                colvarCategory2.ColumnName = "category2";
                colvarCategory2.DataType = DbType.Int32;
                colvarCategory2.MaxLength = 0;
                colvarCategory2.AutoIncrement = false;
                colvarCategory2.IsNullable = true;
                colvarCategory2.IsPrimaryKey = false;
                colvarCategory2.IsForeignKey = false;
                colvarCategory2.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory2);
                
                TableSchema.TableColumn colvarCatgName2 = new TableSchema.TableColumn(schema);
                colvarCatgName2.ColumnName = "catg_name2";
                colvarCatgName2.DataType = DbType.String;
                colvarCatgName2.MaxLength = 25;
                colvarCatgName2.AutoIncrement = false;
                colvarCatgName2.IsNullable = true;
                colvarCatgName2.IsPrimaryKey = false;
                colvarCatgName2.IsForeignKey = false;
                colvarCatgName2.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgName2);
                
                TableSchema.TableColumn colvarOption2 = new TableSchema.TableColumn(schema);
                colvarOption2.ColumnName = "option2";
                colvarOption2.DataType = DbType.Int32;
                colvarOption2.MaxLength = 0;
                colvarOption2.AutoIncrement = false;
                colvarOption2.IsNullable = true;
                colvarOption2.IsPrimaryKey = false;
                colvarOption2.IsForeignKey = false;
                colvarOption2.IsReadOnly = false;
                
                schema.Columns.Add(colvarOption2);
                
                TableSchema.TableColumn colvarOptionName2 = new TableSchema.TableColumn(schema);
                colvarOptionName2.ColumnName = "option_name2";
                colvarOptionName2.DataType = DbType.String;
                colvarOptionName2.MaxLength = 50;
                colvarOptionName2.AutoIncrement = false;
                colvarOptionName2.IsNullable = true;
                colvarOptionName2.IsPrimaryKey = false;
                colvarOptionName2.IsForeignKey = false;
                colvarOptionName2.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName2);
                
                TableSchema.TableColumn colvarCategory3 = new TableSchema.TableColumn(schema);
                colvarCategory3.ColumnName = "category3";
                colvarCategory3.DataType = DbType.Int32;
                colvarCategory3.MaxLength = 0;
                colvarCategory3.AutoIncrement = false;
                colvarCategory3.IsNullable = true;
                colvarCategory3.IsPrimaryKey = false;
                colvarCategory3.IsForeignKey = false;
                colvarCategory3.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory3);
                
                TableSchema.TableColumn colvarCatgName3 = new TableSchema.TableColumn(schema);
                colvarCatgName3.ColumnName = "catg_name3";
                colvarCatgName3.DataType = DbType.String;
                colvarCatgName3.MaxLength = 25;
                colvarCatgName3.AutoIncrement = false;
                colvarCatgName3.IsNullable = true;
                colvarCatgName3.IsPrimaryKey = false;
                colvarCatgName3.IsForeignKey = false;
                colvarCatgName3.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgName3);
                
                TableSchema.TableColumn colvarOption3 = new TableSchema.TableColumn(schema);
                colvarOption3.ColumnName = "option3";
                colvarOption3.DataType = DbType.Int32;
                colvarOption3.MaxLength = 0;
                colvarOption3.AutoIncrement = false;
                colvarOption3.IsNullable = true;
                colvarOption3.IsPrimaryKey = false;
                colvarOption3.IsForeignKey = false;
                colvarOption3.IsReadOnly = false;
                
                schema.Columns.Add(colvarOption3);
                
                TableSchema.TableColumn colvarOptionName3 = new TableSchema.TableColumn(schema);
                colvarOptionName3.ColumnName = "option_name3";
                colvarOptionName3.DataType = DbType.String;
                colvarOptionName3.MaxLength = 50;
                colvarOptionName3.AutoIncrement = false;
                colvarOptionName3.IsNullable = true;
                colvarOptionName3.IsPrimaryKey = false;
                colvarOptionName3.IsForeignKey = false;
                colvarOptionName3.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName3);
                
                TableSchema.TableColumn colvarCategory4 = new TableSchema.TableColumn(schema);
                colvarCategory4.ColumnName = "category4";
                colvarCategory4.DataType = DbType.Int32;
                colvarCategory4.MaxLength = 0;
                colvarCategory4.AutoIncrement = false;
                colvarCategory4.IsNullable = true;
                colvarCategory4.IsPrimaryKey = false;
                colvarCategory4.IsForeignKey = false;
                colvarCategory4.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory4);
                
                TableSchema.TableColumn colvarCatgName4 = new TableSchema.TableColumn(schema);
                colvarCatgName4.ColumnName = "catg_name4";
                colvarCatgName4.DataType = DbType.String;
                colvarCatgName4.MaxLength = 25;
                colvarCatgName4.AutoIncrement = false;
                colvarCatgName4.IsNullable = true;
                colvarCatgName4.IsPrimaryKey = false;
                colvarCatgName4.IsForeignKey = false;
                colvarCatgName4.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgName4);
                
                TableSchema.TableColumn colvarOption4 = new TableSchema.TableColumn(schema);
                colvarOption4.ColumnName = "option4";
                colvarOption4.DataType = DbType.Int32;
                colvarOption4.MaxLength = 0;
                colvarOption4.AutoIncrement = false;
                colvarOption4.IsNullable = true;
                colvarOption4.IsPrimaryKey = false;
                colvarOption4.IsForeignKey = false;
                colvarOption4.IsReadOnly = false;
                
                schema.Columns.Add(colvarOption4);
                
                TableSchema.TableColumn colvarOptionName4 = new TableSchema.TableColumn(schema);
                colvarOptionName4.ColumnName = "option_name4";
                colvarOptionName4.DataType = DbType.String;
                colvarOptionName4.MaxLength = 50;
                colvarOptionName4.AutoIncrement = false;
                colvarOptionName4.IsNullable = true;
                colvarOptionName4.IsPrimaryKey = false;
                colvarOptionName4.IsForeignKey = false;
                colvarOptionName4.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName4);
                
                TableSchema.TableColumn colvarCategory5 = new TableSchema.TableColumn(schema);
                colvarCategory5.ColumnName = "category5";
                colvarCategory5.DataType = DbType.Int32;
                colvarCategory5.MaxLength = 0;
                colvarCategory5.AutoIncrement = false;
                colvarCategory5.IsNullable = true;
                colvarCategory5.IsPrimaryKey = false;
                colvarCategory5.IsForeignKey = false;
                colvarCategory5.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory5);
                
                TableSchema.TableColumn colvarCatgName5 = new TableSchema.TableColumn(schema);
                colvarCatgName5.ColumnName = "catg_name5";
                colvarCatgName5.DataType = DbType.String;
                colvarCatgName5.MaxLength = 25;
                colvarCatgName5.AutoIncrement = false;
                colvarCatgName5.IsNullable = true;
                colvarCatgName5.IsPrimaryKey = false;
                colvarCatgName5.IsForeignKey = false;
                colvarCatgName5.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgName5);
                
                TableSchema.TableColumn colvarOption5 = new TableSchema.TableColumn(schema);
                colvarOption5.ColumnName = "option5";
                colvarOption5.DataType = DbType.Int32;
                colvarOption5.MaxLength = 0;
                colvarOption5.AutoIncrement = false;
                colvarOption5.IsNullable = true;
                colvarOption5.IsPrimaryKey = false;
                colvarOption5.IsForeignKey = false;
                colvarOption5.IsReadOnly = false;
                
                schema.Columns.Add(colvarOption5);
                
                TableSchema.TableColumn colvarOptionName5 = new TableSchema.TableColumn(schema);
                colvarOptionName5.ColumnName = "option_name5";
                colvarOptionName5.DataType = DbType.String;
                colvarOptionName5.MaxLength = 50;
                colvarOptionName5.AutoIncrement = false;
                colvarOptionName5.IsNullable = true;
                colvarOptionName5.IsPrimaryKey = false;
                colvarOptionName5.IsForeignKey = false;
                colvarOptionName5.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName5);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = false;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarAddresseeName = new TableSchema.TableColumn(schema);
                colvarAddresseeName.ColumnName = "addressee_name";
                colvarAddresseeName.DataType = DbType.String;
                colvarAddresseeName.MaxLength = 50;
                colvarAddresseeName.AutoIncrement = false;
                colvarAddresseeName.IsNullable = true;
                colvarAddresseeName.IsPrimaryKey = false;
                colvarAddresseeName.IsForeignKey = false;
                colvarAddresseeName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAddresseeName);
                
                TableSchema.TableColumn colvarAddresseePhone = new TableSchema.TableColumn(schema);
                colvarAddresseePhone.ColumnName = "addressee_phone";
                colvarAddresseePhone.DataType = DbType.String;
                colvarAddresseePhone.MaxLength = 50;
                colvarAddresseePhone.AutoIncrement = false;
                colvarAddresseePhone.IsNullable = true;
                colvarAddresseePhone.IsPrimaryKey = false;
                colvarAddresseePhone.IsForeignKey = false;
                colvarAddresseePhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarAddresseePhone);
                
                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryAddress);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 256;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarProductType = new TableSchema.TableColumn(schema);
                colvarProductType.ColumnName = "product_type";
                colvarProductType.DataType = DbType.Int32;
                colvarProductType.MaxLength = 0;
                colvarProductType.AutoIncrement = false;
                colvarProductType.IsNullable = false;
                colvarProductType.IsPrimaryKey = false;
                colvarProductType.IsForeignKey = false;
                colvarProductType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductType);
                
                TableSchema.TableColumn colvarPk = new TableSchema.TableColumn(schema);
                colvarPk.ColumnName = "pk";
                colvarPk.DataType = DbType.Int32;
                colvarPk.MaxLength = 0;
                colvarPk.AutoIncrement = false;
                colvarPk.IsNullable = false;
                colvarPk.IsPrimaryKey = false;
                colvarPk.IsForeignKey = false;
                colvarPk.IsReadOnly = false;
                
                schema.Columns.Add(colvarPk);
                
                TableSchema.TableColumn colvarHiDealOrderGuid = new TableSchema.TableColumn(schema);
                colvarHiDealOrderGuid.ColumnName = "hi_deal_order_guid";
                colvarHiDealOrderGuid.DataType = DbType.Guid;
                colvarHiDealOrderGuid.MaxLength = 0;
                colvarHiDealOrderGuid.AutoIncrement = false;
                colvarHiDealOrderGuid.IsNullable = false;
                colvarHiDealOrderGuid.IsPrimaryKey = false;
                colvarHiDealOrderGuid.IsForeignKey = false;
                colvarHiDealOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealOrderGuid);
                
                TableSchema.TableColumn colvarHiDealOrderId = new TableSchema.TableColumn(schema);
                colvarHiDealOrderId.ColumnName = "hi_deal_order_id";
                colvarHiDealOrderId.DataType = DbType.AnsiString;
                colvarHiDealOrderId.MaxLength = 30;
                colvarHiDealOrderId.AutoIncrement = false;
                colvarHiDealOrderId.IsNullable = false;
                colvarHiDealOrderId.IsPrimaryKey = false;
                colvarHiDealOrderId.IsForeignKey = false;
                colvarHiDealOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealOrderId);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarTotalAmount = new TableSchema.TableColumn(schema);
                colvarTotalAmount.ColumnName = "total_amount";
                colvarTotalAmount.DataType = DbType.Currency;
                colvarTotalAmount.MaxLength = 0;
                colvarTotalAmount.AutoIncrement = false;
                colvarTotalAmount.IsNullable = false;
                colvarTotalAmount.IsPrimaryKey = false;
                colvarTotalAmount.IsForeignKey = false;
                colvarTotalAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalAmount);
                
                TableSchema.TableColumn colvarPcash = new TableSchema.TableColumn(schema);
                colvarPcash.ColumnName = "pcash";
                colvarPcash.DataType = DbType.Currency;
                colvarPcash.MaxLength = 0;
                colvarPcash.AutoIncrement = false;
                colvarPcash.IsNullable = false;
                colvarPcash.IsPrimaryKey = false;
                colvarPcash.IsForeignKey = false;
                colvarPcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcash);
                
                TableSchema.TableColumn colvarScash = new TableSchema.TableColumn(schema);
                colvarScash.ColumnName = "scash";
                colvarScash.DataType = DbType.Currency;
                colvarScash.MaxLength = 0;
                colvarScash.AutoIncrement = false;
                colvarScash.IsNullable = false;
                colvarScash.IsPrimaryKey = false;
                colvarScash.IsForeignKey = false;
                colvarScash.IsReadOnly = false;
                
                schema.Columns.Add(colvarScash);
                
                TableSchema.TableColumn colvarBcash = new TableSchema.TableColumn(schema);
                colvarBcash.ColumnName = "bcash";
                colvarBcash.DataType = DbType.Currency;
                colvarBcash.MaxLength = 0;
                colvarBcash.AutoIncrement = false;
                colvarBcash.IsNullable = false;
                colvarBcash.IsPrimaryKey = false;
                colvarBcash.IsForeignKey = false;
                colvarBcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarBcash);
                
                TableSchema.TableColumn colvarCreditCardAmt = new TableSchema.TableColumn(schema);
                colvarCreditCardAmt.ColumnName = "credit_card_amt";
                colvarCreditCardAmt.DataType = DbType.Currency;
                colvarCreditCardAmt.MaxLength = 0;
                colvarCreditCardAmt.AutoIncrement = false;
                colvarCreditCardAmt.IsNullable = false;
                colvarCreditCardAmt.IsPrimaryKey = false;
                colvarCreditCardAmt.IsForeignKey = false;
                colvarCreditCardAmt.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreditCardAmt);
                
                TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
                colvarDiscountAmount.ColumnName = "discount_amount";
                colvarDiscountAmount.DataType = DbType.Currency;
                colvarDiscountAmount.MaxLength = 0;
                colvarDiscountAmount.AutoIncrement = false;
                colvarDiscountAmount.IsNullable = false;
                colvarDiscountAmount.IsPrimaryKey = false;
                colvarDiscountAmount.IsForeignKey = false;
                colvarDiscountAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountAmount);
                
                TableSchema.TableColumn colvarAtmAmount = new TableSchema.TableColumn(schema);
                colvarAtmAmount.ColumnName = "atm_amount";
                colvarAtmAmount.DataType = DbType.Currency;
                colvarAtmAmount.MaxLength = 0;
                colvarAtmAmount.AutoIncrement = false;
                colvarAtmAmount.IsNullable = false;
                colvarAtmAmount.IsPrimaryKey = false;
                colvarAtmAmount.IsForeignKey = false;
                colvarAtmAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAtmAmount);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarHiDealOrderCreateId = new TableSchema.TableColumn(schema);
                colvarHiDealOrderCreateId.ColumnName = "hi_deal_order_create_id";
                colvarHiDealOrderCreateId.DataType = DbType.String;
                colvarHiDealOrderCreateId.MaxLength = 256;
                colvarHiDealOrderCreateId.AutoIncrement = false;
                colvarHiDealOrderCreateId.IsNullable = false;
                colvarHiDealOrderCreateId.IsPrimaryKey = false;
                colvarHiDealOrderCreateId.IsForeignKey = false;
                colvarHiDealOrderCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealOrderCreateId);
                
                TableSchema.TableColumn colvarHiDealOrderCreateTime = new TableSchema.TableColumn(schema);
                colvarHiDealOrderCreateTime.ColumnName = "hi_deal_order_create_time";
                colvarHiDealOrderCreateTime.DataType = DbType.DateTime;
                colvarHiDealOrderCreateTime.MaxLength = 0;
                colvarHiDealOrderCreateTime.AutoIncrement = false;
                colvarHiDealOrderCreateTime.IsNullable = false;
                colvarHiDealOrderCreateTime.IsPrimaryKey = false;
                colvarHiDealOrderCreateTime.IsForeignKey = false;
                colvarHiDealOrderCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealOrderCreateTime);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = false;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
                colvarProductGuid.ColumnName = "product_guid";
                colvarProductGuid.DataType = DbType.Guid;
                colvarProductGuid.MaxLength = 0;
                colvarProductGuid.AutoIncrement = false;
                colvarProductGuid.IsNullable = false;
                colvarProductGuid.IsPrimaryKey = false;
                colvarProductGuid.IsForeignKey = false;
                colvarProductGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductGuid);
                
                TableSchema.TableColumn colvarProductSeq = new TableSchema.TableColumn(schema);
                colvarProductSeq.ColumnName = "product_seq";
                colvarProductSeq.DataType = DbType.Int32;
                colvarProductSeq.MaxLength = 0;
                colvarProductSeq.AutoIncrement = false;
                colvarProductSeq.IsNullable = true;
                colvarProductSeq.IsPrimaryKey = false;
                colvarProductSeq.IsForeignKey = false;
                colvarProductSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductSeq);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 250;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = -1;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = true;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescription);
                
                TableSchema.TableColumn colvarIsOnline = new TableSchema.TableColumn(schema);
                colvarIsOnline.ColumnName = "is_online";
                colvarIsOnline.DataType = DbType.Boolean;
                colvarIsOnline.MaxLength = 0;
                colvarIsOnline.AutoIncrement = false;
                colvarIsOnline.IsNullable = true;
                colvarIsOnline.IsPrimaryKey = false;
                colvarIsOnline.IsForeignKey = false;
                colvarIsOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsOnline);
                
                TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
                colvarUseStartTime.ColumnName = "use_start_time";
                colvarUseStartTime.DataType = DbType.DateTime;
                colvarUseStartTime.MaxLength = 0;
                colvarUseStartTime.AutoIncrement = false;
                colvarUseStartTime.IsNullable = true;
                colvarUseStartTime.IsPrimaryKey = false;
                colvarUseStartTime.IsForeignKey = false;
                colvarUseStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseStartTime);
                
                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.DateTime;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseEndTime);
                
                TableSchema.TableColumn colvarChangedExpireTime = new TableSchema.TableColumn(schema);
                colvarChangedExpireTime.ColumnName = "changed_expire_time";
                colvarChangedExpireTime.DataType = DbType.DateTime;
                colvarChangedExpireTime.MaxLength = 0;
                colvarChangedExpireTime.AutoIncrement = false;
                colvarChangedExpireTime.IsNullable = true;
                colvarChangedExpireTime.IsPrimaryKey = false;
                colvarChangedExpireTime.IsForeignKey = false;
                colvarChangedExpireTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarChangedExpireTime);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = true;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarOriginalPrice = new TableSchema.TableColumn(schema);
                colvarOriginalPrice.ColumnName = "original_price";
                colvarOriginalPrice.DataType = DbType.Currency;
                colvarOriginalPrice.MaxLength = 0;
                colvarOriginalPrice.AutoIncrement = false;
                colvarOriginalPrice.IsNullable = true;
                colvarOriginalPrice.IsPrimaryKey = false;
                colvarOriginalPrice.IsForeignKey = false;
                colvarOriginalPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarOriginalPrice);
                
                TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
                colvarPrice.ColumnName = "price";
                colvarPrice.DataType = DbType.Currency;
                colvarPrice.MaxLength = 0;
                colvarPrice.AutoIncrement = false;
                colvarPrice.IsNullable = true;
                colvarPrice.IsPrimaryKey = false;
                colvarPrice.IsForeignKey = false;
                colvarPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrice);
                
                TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
                colvarQuantity.ColumnName = "quantity";
                colvarQuantity.DataType = DbType.Int32;
                colvarQuantity.MaxLength = 0;
                colvarQuantity.AutoIncrement = false;
                colvarQuantity.IsNullable = true;
                colvarQuantity.IsPrimaryKey = false;
                colvarQuantity.IsForeignKey = false;
                colvarQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarQuantity);
                
                TableSchema.TableColumn colvarOrderLimit = new TableSchema.TableColumn(schema);
                colvarOrderLimit.ColumnName = "order_limit";
                colvarOrderLimit.DataType = DbType.Int32;
                colvarOrderLimit.MaxLength = 0;
                colvarOrderLimit.AutoIncrement = false;
                colvarOrderLimit.IsNullable = true;
                colvarOrderLimit.IsPrimaryKey = false;
                colvarOrderLimit.IsForeignKey = false;
                colvarOrderLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderLimit);
                
                TableSchema.TableColumn colvarOrderLimitUser = new TableSchema.TableColumn(schema);
                colvarOrderLimitUser.ColumnName = "order_limit_user";
                colvarOrderLimitUser.DataType = DbType.Int32;
                colvarOrderLimitUser.MaxLength = 0;
                colvarOrderLimitUser.AutoIncrement = false;
                colvarOrderLimitUser.IsNullable = true;
                colvarOrderLimitUser.IsPrimaryKey = false;
                colvarOrderLimitUser.IsForeignKey = false;
                colvarOrderLimitUser.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderLimitUser);
                
                TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
                colvarOrderCount.ColumnName = "order_count";
                colvarOrderCount.DataType = DbType.Int32;
                colvarOrderCount.MaxLength = 0;
                colvarOrderCount.AutoIncrement = false;
                colvarOrderCount.IsNullable = true;
                colvarOrderCount.IsPrimaryKey = false;
                colvarOrderCount.IsForeignKey = false;
                colvarOrderCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCount);
                
                TableSchema.TableColumn colvarIsNoRefund = new TableSchema.TableColumn(schema);
                colvarIsNoRefund.ColumnName = "is_no_refund";
                colvarIsNoRefund.DataType = DbType.Boolean;
                colvarIsNoRefund.MaxLength = 0;
                colvarIsNoRefund.AutoIncrement = false;
                colvarIsNoRefund.IsNullable = false;
                colvarIsNoRefund.IsPrimaryKey = false;
                colvarIsNoRefund.IsForeignKey = false;
                colvarIsNoRefund.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsNoRefund);
                
                TableSchema.TableColumn colvarIsExpireNoRefund = new TableSchema.TableColumn(schema);
                colvarIsExpireNoRefund.ColumnName = "is_expire_no_refund";
                colvarIsExpireNoRefund.DataType = DbType.Boolean;
                colvarIsExpireNoRefund.MaxLength = 0;
                colvarIsExpireNoRefund.AutoIncrement = false;
                colvarIsExpireNoRefund.IsNullable = false;
                colvarIsExpireNoRefund.IsPrimaryKey = false;
                colvarIsExpireNoRefund.IsForeignKey = false;
                colvarIsExpireNoRefund.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsExpireNoRefund);
                
                TableSchema.TableColumn colvarIsTaxFree = new TableSchema.TableColumn(schema);
                colvarIsTaxFree.ColumnName = "is_tax_free";
                colvarIsTaxFree.DataType = DbType.Boolean;
                colvarIsTaxFree.MaxLength = 0;
                colvarIsTaxFree.AutoIncrement = false;
                colvarIsTaxFree.IsNullable = false;
                colvarIsTaxFree.IsPrimaryKey = false;
                colvarIsTaxFree.IsForeignKey = false;
                colvarIsTaxFree.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsTaxFree);
                
                TableSchema.TableColumn colvarIsSoldout = new TableSchema.TableColumn(schema);
                colvarIsSoldout.ColumnName = "is_soldout";
                colvarIsSoldout.DataType = DbType.Boolean;
                colvarIsSoldout.MaxLength = 0;
                colvarIsSoldout.AutoIncrement = false;
                colvarIsSoldout.IsNullable = true;
                colvarIsSoldout.IsPrimaryKey = false;
                colvarIsSoldout.IsForeignKey = false;
                colvarIsSoldout.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsSoldout);
                
                TableSchema.TableColumn colvarIsInStore = new TableSchema.TableColumn(schema);
                colvarIsInStore.ColumnName = "is_in_store";
                colvarIsInStore.DataType = DbType.Boolean;
                colvarIsInStore.MaxLength = 0;
                colvarIsInStore.AutoIncrement = false;
                colvarIsInStore.IsNullable = false;
                colvarIsInStore.IsPrimaryKey = false;
                colvarIsInStore.IsForeignKey = false;
                colvarIsInStore.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsInStore);
                
                TableSchema.TableColumn colvarInStoreDesc = new TableSchema.TableColumn(schema);
                colvarInStoreDesc.ColumnName = "in_store_desc";
                colvarInStoreDesc.DataType = DbType.String;
                colvarInStoreDesc.MaxLength = 100;
                colvarInStoreDesc.AutoIncrement = false;
                colvarInStoreDesc.IsNullable = true;
                colvarInStoreDesc.IsPrimaryKey = false;
                colvarInStoreDesc.IsForeignKey = false;
                colvarInStoreDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarInStoreDesc);
                
                TableSchema.TableColumn colvarIsHomeDelivery = new TableSchema.TableColumn(schema);
                colvarIsHomeDelivery.ColumnName = "is_home_delivery";
                colvarIsHomeDelivery.DataType = DbType.Boolean;
                colvarIsHomeDelivery.MaxLength = 0;
                colvarIsHomeDelivery.AutoIncrement = false;
                colvarIsHomeDelivery.IsNullable = false;
                colvarIsHomeDelivery.IsPrimaryKey = false;
                colvarIsHomeDelivery.IsForeignKey = false;
                colvarIsHomeDelivery.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsHomeDelivery);
                
                TableSchema.TableColumn colvarHomeDeliveryDesc = new TableSchema.TableColumn(schema);
                colvarHomeDeliveryDesc.ColumnName = "home_delivery_desc";
                colvarHomeDeliveryDesc.DataType = DbType.String;
                colvarHomeDeliveryDesc.MaxLength = 100;
                colvarHomeDeliveryDesc.AutoIncrement = false;
                colvarHomeDeliveryDesc.IsNullable = true;
                colvarHomeDeliveryDesc.IsPrimaryKey = false;
                colvarHomeDeliveryDesc.IsForeignKey = false;
                colvarHomeDeliveryDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarHomeDeliveryDesc);
                
                TableSchema.TableColumn colvarSms = new TableSchema.TableColumn(schema);
                colvarSms.ColumnName = "sms";
                colvarSms.DataType = DbType.String;
                colvarSms.MaxLength = 150;
                colvarSms.AutoIncrement = false;
                colvarSms.IsNullable = true;
                colvarSms.IsPrimaryKey = false;
                colvarSms.IsForeignKey = false;
                colvarSms.IsReadOnly = false;
                
                schema.Columns.Add(colvarSms);
                
                TableSchema.TableColumn colvarIsSmsClose = new TableSchema.TableColumn(schema);
                colvarIsSmsClose.ColumnName = "is_sms_close";
                colvarIsSmsClose.DataType = DbType.Boolean;
                colvarIsSmsClose.MaxLength = 0;
                colvarIsSmsClose.AutoIncrement = false;
                colvarIsSmsClose.IsNullable = false;
                colvarIsSmsClose.IsPrimaryKey = false;
                colvarIsSmsClose.IsForeignKey = false;
                colvarIsSmsClose.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsSmsClose);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = true;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                TableSchema.TableColumn colvarPic = new TableSchema.TableColumn(schema);
                colvarPic.ColumnName = "pic";
                colvarPic.DataType = DbType.String;
                colvarPic.MaxLength = -1;
                colvarPic.AutoIncrement = false;
                colvarPic.IsNullable = true;
                colvarPic.IsPrimaryKey = false;
                colvarPic.IsForeignKey = false;
                colvarPic.IsReadOnly = false;
                
                schema.Columns.Add(colvarPic);
                
                TableSchema.TableColumn colvarHasBranchStoreQuantityLimit = new TableSchema.TableColumn(schema);
                colvarHasBranchStoreQuantityLimit.ColumnName = "has_branch_store_quantity_limit";
                colvarHasBranchStoreQuantityLimit.DataType = DbType.Boolean;
                colvarHasBranchStoreQuantityLimit.MaxLength = 0;
                colvarHasBranchStoreQuantityLimit.AutoIncrement = false;
                colvarHasBranchStoreQuantityLimit.IsNullable = false;
                colvarHasBranchStoreQuantityLimit.IsPrimaryKey = false;
                colvarHasBranchStoreQuantityLimit.IsForeignKey = false;
                colvarHasBranchStoreQuantityLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarHasBranchStoreQuantityLimit);
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarDealGuid = new TableSchema.TableColumn(schema);
                colvarDealGuid.ColumnName = "deal_guid";
                colvarDealGuid.DataType = DbType.Guid;
                colvarDealGuid.MaxLength = 0;
                colvarDealGuid.AutoIncrement = false;
                colvarDealGuid.IsNullable = false;
                colvarDealGuid.IsPrimaryKey = false;
                colvarDealGuid.IsForeignKey = false;
                colvarDealGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealGuid);
                
                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = true;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 50;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarPromoLongDesc = new TableSchema.TableColumn(schema);
                colvarPromoLongDesc.ColumnName = "promo_long_desc";
                colvarPromoLongDesc.DataType = DbType.String;
                colvarPromoLongDesc.MaxLength = 255;
                colvarPromoLongDesc.AutoIncrement = false;
                colvarPromoLongDesc.IsNullable = true;
                colvarPromoLongDesc.IsPrimaryKey = false;
                colvarPromoLongDesc.IsForeignKey = false;
                colvarPromoLongDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoLongDesc);
                
                TableSchema.TableColumn colvarPromoShortDesc = new TableSchema.TableColumn(schema);
                colvarPromoShortDesc.ColumnName = "promo_short_desc";
                colvarPromoShortDesc.DataType = DbType.String;
                colvarPromoShortDesc.MaxLength = 100;
                colvarPromoShortDesc.AutoIncrement = false;
                colvarPromoShortDesc.IsNullable = true;
                colvarPromoShortDesc.IsPrimaryKey = false;
                colvarPromoShortDesc.IsForeignKey = false;
                colvarPromoShortDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoShortDesc);
                
                TableSchema.TableColumn colvarPicture = new TableSchema.TableColumn(schema);
                colvarPicture.ColumnName = "picture";
                colvarPicture.DataType = DbType.String;
                colvarPicture.MaxLength = -1;
                colvarPicture.AutoIncrement = false;
                colvarPicture.IsNullable = true;
                colvarPicture.IsPrimaryKey = false;
                colvarPicture.IsForeignKey = false;
                colvarPicture.IsReadOnly = false;
                
                schema.Columns.Add(colvarPicture);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarIsOpen = new TableSchema.TableColumn(schema);
                colvarIsOpen.ColumnName = "IsOpen";
                colvarIsOpen.DataType = DbType.Boolean;
                colvarIsOpen.MaxLength = 0;
                colvarIsOpen.AutoIncrement = false;
                colvarIsOpen.IsNullable = false;
                colvarIsOpen.IsPrimaryKey = false;
                colvarIsOpen.IsForeignKey = false;
                colvarIsOpen.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsOpen);
                
                TableSchema.TableColumn colvarIsShow = new TableSchema.TableColumn(schema);
                colvarIsShow.ColumnName = "IsShow";
                colvarIsShow.DataType = DbType.Boolean;
                colvarIsShow.MaxLength = 0;
                colvarIsShow.AutoIncrement = false;
                colvarIsShow.IsNullable = false;
                colvarIsShow.IsPrimaryKey = false;
                colvarIsShow.IsForeignKey = false;
                colvarIsShow.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsShow);
                
                TableSchema.TableColumn colvarIsAlwaysMain = new TableSchema.TableColumn(schema);
                colvarIsAlwaysMain.ColumnName = "is_always_main";
                colvarIsAlwaysMain.DataType = DbType.Boolean;
                colvarIsAlwaysMain.MaxLength = 0;
                colvarIsAlwaysMain.AutoIncrement = false;
                colvarIsAlwaysMain.IsNullable = false;
                colvarIsAlwaysMain.IsPrimaryKey = false;
                colvarIsAlwaysMain.IsForeignKey = false;
                colvarIsAlwaysMain.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsAlwaysMain);
                
                TableSchema.TableColumn colvarIsVerifyByList = new TableSchema.TableColumn(schema);
                colvarIsVerifyByList.ColumnName = "is_verify_by_list";
                colvarIsVerifyByList.DataType = DbType.Boolean;
                colvarIsVerifyByList.MaxLength = 0;
                colvarIsVerifyByList.AutoIncrement = false;
                colvarIsVerifyByList.IsNullable = false;
                colvarIsVerifyByList.IsPrimaryKey = false;
                colvarIsVerifyByList.IsForeignKey = false;
                colvarIsVerifyByList.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsVerifyByList);
                
                TableSchema.TableColumn colvarIsVerifyByPad = new TableSchema.TableColumn(schema);
                colvarIsVerifyByPad.ColumnName = "is_verify_by_pad";
                colvarIsVerifyByPad.DataType = DbType.Boolean;
                colvarIsVerifyByPad.MaxLength = 0;
                colvarIsVerifyByPad.AutoIncrement = false;
                colvarIsVerifyByPad.IsNullable = false;
                colvarIsVerifyByPad.IsPrimaryKey = false;
                colvarIsVerifyByPad.IsForeignKey = false;
                colvarIsVerifyByPad.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsVerifyByPad);
                
                TableSchema.TableColumn colvarSales = new TableSchema.TableColumn(schema);
                colvarSales.ColumnName = "sales";
                colvarSales.DataType = DbType.String;
                colvarSales.MaxLength = 100;
                colvarSales.AutoIncrement = false;
                colvarSales.IsNullable = true;
                colvarSales.IsPrimaryKey = false;
                colvarSales.IsForeignKey = false;
                colvarSales.IsReadOnly = false;
                
                schema.Columns.Add(colvarSales);
                
                TableSchema.TableColumn colvarSalesGroupId = new TableSchema.TableColumn(schema);
                colvarSalesGroupId.ColumnName = "sales_group_id";
                colvarSalesGroupId.DataType = DbType.Int32;
                colvarSalesGroupId.MaxLength = 0;
                colvarSalesGroupId.AutoIncrement = false;
                colvarSalesGroupId.IsNullable = true;
                colvarSalesGroupId.IsPrimaryKey = false;
                colvarSalesGroupId.IsForeignKey = false;
                colvarSalesGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSalesGroupId);
                
                TableSchema.TableColumn colvarSalesGroupName = new TableSchema.TableColumn(schema);
                colvarSalesGroupName.ColumnName = "sales_group_name";
                colvarSalesGroupName.DataType = DbType.String;
                colvarSalesGroupName.MaxLength = 255;
                colvarSalesGroupName.AutoIncrement = false;
                colvarSalesGroupName.IsNullable = true;
                colvarSalesGroupName.IsPrimaryKey = false;
                colvarSalesGroupName.IsForeignKey = false;
                colvarSalesGroupName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSalesGroupName);
                
                TableSchema.TableColumn colvarSalesDeptId = new TableSchema.TableColumn(schema);
                colvarSalesDeptId.ColumnName = "sales_dept_id";
                colvarSalesDeptId.DataType = DbType.AnsiString;
                colvarSalesDeptId.MaxLength = 10;
                colvarSalesDeptId.AutoIncrement = false;
                colvarSalesDeptId.IsNullable = true;
                colvarSalesDeptId.IsPrimaryKey = false;
                colvarSalesDeptId.IsForeignKey = false;
                colvarSalesDeptId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSalesDeptId);
                
                TableSchema.TableColumn colvarSalesDept = new TableSchema.TableColumn(schema);
                colvarSalesDept.ColumnName = "sales_dept";
                colvarSalesDept.DataType = DbType.String;
                colvarSalesDept.MaxLength = 100;
                colvarSalesDept.AutoIncrement = false;
                colvarSalesDept.IsNullable = true;
                colvarSalesDept.IsPrimaryKey = false;
                colvarSalesDept.IsForeignKey = false;
                colvarSalesDept.IsReadOnly = false;
                
                schema.Columns.Add(colvarSalesDept);
                
                TableSchema.TableColumn colvarSalesLocationId = new TableSchema.TableColumn(schema);
                colvarSalesLocationId.ColumnName = "sales_location_id";
                colvarSalesLocationId.DataType = DbType.Int32;
                colvarSalesLocationId.MaxLength = 0;
                colvarSalesLocationId.AutoIncrement = false;
                colvarSalesLocationId.IsNullable = true;
                colvarSalesLocationId.IsPrimaryKey = false;
                colvarSalesLocationId.IsForeignKey = false;
                colvarSalesLocationId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSalesLocationId);
                
                TableSchema.TableColumn colvarSalesLocation = new TableSchema.TableColumn(schema);
                colvarSalesLocation.ColumnName = "sales_location";
                colvarSalesLocation.DataType = DbType.String;
                colvarSalesLocation.MaxLength = 25;
                colvarSalesLocation.AutoIncrement = false;
                colvarSalesLocation.IsNullable = true;
                colvarSalesLocation.IsPrimaryKey = false;
                colvarSalesLocation.IsForeignKey = false;
                colvarSalesLocation.IsReadOnly = false;
                
                schema.Columns.Add(colvarSalesLocation);
                
                TableSchema.TableColumn colvarSalesCatgCodeGroup = new TableSchema.TableColumn(schema);
                colvarSalesCatgCodeGroup.ColumnName = "sales_catg_code_group";
                colvarSalesCatgCodeGroup.DataType = DbType.AnsiString;
                colvarSalesCatgCodeGroup.MaxLength = 50;
                colvarSalesCatgCodeGroup.AutoIncrement = false;
                colvarSalesCatgCodeGroup.IsNullable = true;
                colvarSalesCatgCodeGroup.IsPrimaryKey = false;
                colvarSalesCatgCodeGroup.IsForeignKey = false;
                colvarSalesCatgCodeGroup.IsReadOnly = false;
                
                schema.Columns.Add(colvarSalesCatgCodeGroup);
                
                TableSchema.TableColumn colvarSalesCatgCodeId = new TableSchema.TableColumn(schema);
                colvarSalesCatgCodeId.ColumnName = "sales_catg_code_id";
                colvarSalesCatgCodeId.DataType = DbType.Int32;
                colvarSalesCatgCodeId.MaxLength = 0;
                colvarSalesCatgCodeId.AutoIncrement = false;
                colvarSalesCatgCodeId.IsNullable = true;
                colvarSalesCatgCodeId.IsPrimaryKey = false;
                colvarSalesCatgCodeId.IsForeignKey = false;
                colvarSalesCatgCodeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSalesCatgCodeId);
                
                TableSchema.TableColumn colvarSalesCatgCodeName = new TableSchema.TableColumn(schema);
                colvarSalesCatgCodeName.ColumnName = "sales_catg_code_name";
                colvarSalesCatgCodeName.DataType = DbType.String;
                colvarSalesCatgCodeName.MaxLength = 50;
                colvarSalesCatgCodeName.AutoIncrement = false;
                colvarSalesCatgCodeName.IsNullable = true;
                colvarSalesCatgCodeName.IsPrimaryKey = false;
                colvarSalesCatgCodeName.IsForeignKey = false;
                colvarSalesCatgCodeName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSalesCatgCodeName);
                
                TableSchema.TableColumn colvarSettleDay1 = new TableSchema.TableColumn(schema);
                colvarSettleDay1.ColumnName = "settle_day_1";
                colvarSettleDay1.DataType = DbType.Int32;
                colvarSettleDay1.MaxLength = 0;
                colvarSettleDay1.AutoIncrement = false;
                colvarSettleDay1.IsNullable = true;
                colvarSettleDay1.IsPrimaryKey = false;
                colvarSettleDay1.IsForeignKey = false;
                colvarSettleDay1.IsReadOnly = false;
                
                schema.Columns.Add(colvarSettleDay1);
                
                TableSchema.TableColumn colvarSettleDay2 = new TableSchema.TableColumn(schema);
                colvarSettleDay2.ColumnName = "settle_day_2";
                colvarSettleDay2.DataType = DbType.Int32;
                colvarSettleDay2.MaxLength = 0;
                colvarSettleDay2.AutoIncrement = false;
                colvarSettleDay2.IsNullable = true;
                colvarSettleDay2.IsPrimaryKey = false;
                colvarSettleDay2.IsForeignKey = false;
                colvarSettleDay2.IsReadOnly = false;
                
                schema.Columns.Add(colvarSettleDay2);
                
                TableSchema.TableColumn colvarSettleDay3 = new TableSchema.TableColumn(schema);
                colvarSettleDay3.ColumnName = "settle_day_3";
                colvarSettleDay3.DataType = DbType.Int32;
                colvarSettleDay3.MaxLength = 0;
                colvarSettleDay3.AutoIncrement = false;
                colvarSettleDay3.IsNullable = true;
                colvarSettleDay3.IsPrimaryKey = false;
                colvarSettleDay3.IsForeignKey = false;
                colvarSettleDay3.IsReadOnly = false;
                
                schema.Columns.Add(colvarSettleDay3);
                
                TableSchema.TableColumn colvarCities = new TableSchema.TableColumn(schema);
                colvarCities.ColumnName = "cities";
                colvarCities.DataType = DbType.String;
                colvarCities.MaxLength = 100;
                colvarCities.AutoIncrement = false;
                colvarCities.IsNullable = true;
                colvarCities.IsPrimaryKey = false;
                colvarCities.IsForeignKey = false;
                colvarCities.IsReadOnly = false;
                
                schema.Columns.Add(colvarCities);
                
                TableSchema.TableColumn colvarIsVisa = new TableSchema.TableColumn(schema);
                colvarIsVisa.ColumnName = "is_visa";
                colvarIsVisa.DataType = DbType.Boolean;
                colvarIsVisa.MaxLength = 0;
                colvarIsVisa.AutoIncrement = false;
                colvarIsVisa.IsNullable = true;
                colvarIsVisa.IsPrimaryKey = false;
                colvarIsVisa.IsForeignKey = false;
                colvarIsVisa.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsVisa);
                
                TableSchema.TableColumn colvarEdmTitle = new TableSchema.TableColumn(schema);
                colvarEdmTitle.ColumnName = "edm_title";
                colvarEdmTitle.DataType = DbType.String;
                colvarEdmTitle.MaxLength = 40;
                colvarEdmTitle.AutoIncrement = false;
                colvarEdmTitle.IsNullable = true;
                colvarEdmTitle.IsPrimaryKey = false;
                colvarEdmTitle.IsForeignKey = false;
                colvarEdmTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarEdmTitle);
                
                TableSchema.TableColumn colvarEdmSubject = new TableSchema.TableColumn(schema);
                colvarEdmSubject.ColumnName = "edm_subject";
                colvarEdmSubject.DataType = DbType.String;
                colvarEdmSubject.MaxLength = 40;
                colvarEdmSubject.AutoIncrement = false;
                colvarEdmSubject.IsNullable = true;
                colvarEdmSubject.IsPrimaryKey = false;
                colvarEdmSubject.IsForeignKey = false;
                colvarEdmSubject.IsReadOnly = false;
                
                schema.Columns.Add(colvarEdmSubject);
                
                TableSchema.TableColumn colvarPrimaryBigPicture = new TableSchema.TableColumn(schema);
                colvarPrimaryBigPicture.ColumnName = "primary_big_picture";
                colvarPrimaryBigPicture.DataType = DbType.String;
                colvarPrimaryBigPicture.MaxLength = 200;
                colvarPrimaryBigPicture.AutoIncrement = false;
                colvarPrimaryBigPicture.IsNullable = true;
                colvarPrimaryBigPicture.IsPrimaryKey = false;
                colvarPrimaryBigPicture.IsForeignKey = false;
                colvarPrimaryBigPicture.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrimaryBigPicture);
                
                TableSchema.TableColumn colvarPrimarySmallPicture = new TableSchema.TableColumn(schema);
                colvarPrimarySmallPicture.ColumnName = "primary_small_picture";
                colvarPrimarySmallPicture.DataType = DbType.String;
                colvarPrimarySmallPicture.MaxLength = 200;
                colvarPrimarySmallPicture.AutoIncrement = false;
                colvarPrimarySmallPicture.IsNullable = true;
                colvarPrimarySmallPicture.IsPrimaryKey = false;
                colvarPrimarySmallPicture.IsForeignKey = false;
                colvarPrimarySmallPicture.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrimarySmallPicture);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerBossName = new TableSchema.TableColumn(schema);
                colvarSellerBossName.ColumnName = "seller_boss_name";
                colvarSellerBossName.DataType = DbType.String;
                colvarSellerBossName.MaxLength = 30;
                colvarSellerBossName.AutoIncrement = false;
                colvarSellerBossName.IsNullable = true;
                colvarSellerBossName.IsPrimaryKey = false;
                colvarSellerBossName.IsForeignKey = false;
                colvarSellerBossName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerBossName);
                
                TableSchema.TableColumn colvarSellerTel = new TableSchema.TableColumn(schema);
                colvarSellerTel.ColumnName = "seller_tel";
                colvarSellerTel.DataType = DbType.AnsiString;
                colvarSellerTel.MaxLength = 100;
                colvarSellerTel.AutoIncrement = false;
                colvarSellerTel.IsNullable = true;
                colvarSellerTel.IsPrimaryKey = false;
                colvarSellerTel.IsForeignKey = false;
                colvarSellerTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerTel);
                
                TableSchema.TableColumn colvarSellerTel2 = new TableSchema.TableColumn(schema);
                colvarSellerTel2.ColumnName = "seller_tel2";
                colvarSellerTel2.DataType = DbType.AnsiString;
                colvarSellerTel2.MaxLength = 100;
                colvarSellerTel2.AutoIncrement = false;
                colvarSellerTel2.IsNullable = true;
                colvarSellerTel2.IsPrimaryKey = false;
                colvarSellerTel2.IsForeignKey = false;
                colvarSellerTel2.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerTel2);
                
                TableSchema.TableColumn colvarSellerFax = new TableSchema.TableColumn(schema);
                colvarSellerFax.ColumnName = "seller_fax";
                colvarSellerFax.DataType = DbType.AnsiString;
                colvarSellerFax.MaxLength = 20;
                colvarSellerFax.AutoIncrement = false;
                colvarSellerFax.IsNullable = true;
                colvarSellerFax.IsPrimaryKey = false;
                colvarSellerFax.IsForeignKey = false;
                colvarSellerFax.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerFax);
                
                TableSchema.TableColumn colvarSellerMobile = new TableSchema.TableColumn(schema);
                colvarSellerMobile.ColumnName = "seller_mobile";
                colvarSellerMobile.DataType = DbType.AnsiString;
                colvarSellerMobile.MaxLength = 100;
                colvarSellerMobile.AutoIncrement = false;
                colvarSellerMobile.IsNullable = true;
                colvarSellerMobile.IsPrimaryKey = false;
                colvarSellerMobile.IsForeignKey = false;
                colvarSellerMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMobile);
                
                TableSchema.TableColumn colvarSellerAddress = new TableSchema.TableColumn(schema);
                colvarSellerAddress.ColumnName = "seller_address";
                colvarSellerAddress.DataType = DbType.String;
                colvarSellerAddress.MaxLength = 100;
                colvarSellerAddress.AutoIncrement = false;
                colvarSellerAddress.IsNullable = true;
                colvarSellerAddress.IsPrimaryKey = false;
                colvarSellerAddress.IsForeignKey = false;
                colvarSellerAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerAddress);
                
                TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
                colvarSellerEmail.ColumnName = "seller_email";
                colvarSellerEmail.DataType = DbType.AnsiString;
                colvarSellerEmail.MaxLength = 200;
                colvarSellerEmail.AutoIncrement = false;
                colvarSellerEmail.IsNullable = true;
                colvarSellerEmail.IsPrimaryKey = false;
                colvarSellerEmail.IsForeignKey = false;
                colvarSellerEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerEmail);
                
                TableSchema.TableColumn colvarSellerBlog = new TableSchema.TableColumn(schema);
                colvarSellerBlog.ColumnName = "seller_blog";
                colvarSellerBlog.DataType = DbType.AnsiString;
                colvarSellerBlog.MaxLength = 100;
                colvarSellerBlog.AutoIncrement = false;
                colvarSellerBlog.IsNullable = true;
                colvarSellerBlog.IsPrimaryKey = false;
                colvarSellerBlog.IsForeignKey = false;
                colvarSellerBlog.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerBlog);
                
                TableSchema.TableColumn colvarSellerInvoice = new TableSchema.TableColumn(schema);
                colvarSellerInvoice.ColumnName = "seller_invoice";
                colvarSellerInvoice.DataType = DbType.String;
                colvarSellerInvoice.MaxLength = 50;
                colvarSellerInvoice.AutoIncrement = false;
                colvarSellerInvoice.IsNullable = true;
                colvarSellerInvoice.IsPrimaryKey = false;
                colvarSellerInvoice.IsForeignKey = false;
                colvarSellerInvoice.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerInvoice);
                
                TableSchema.TableColumn colvarSellerDescription = new TableSchema.TableColumn(schema);
                colvarSellerDescription.ColumnName = "seller_description";
                colvarSellerDescription.DataType = DbType.String;
                colvarSellerDescription.MaxLength = 1073741823;
                colvarSellerDescription.AutoIncrement = false;
                colvarSellerDescription.IsNullable = true;
                colvarSellerDescription.IsPrimaryKey = false;
                colvarSellerDescription.IsForeignKey = false;
                colvarSellerDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerDescription);
                
                TableSchema.TableColumn colvarSellerStatus = new TableSchema.TableColumn(schema);
                colvarSellerStatus.ColumnName = "seller_status";
                colvarSellerStatus.DataType = DbType.Int32;
                colvarSellerStatus.MaxLength = 0;
                colvarSellerStatus.AutoIncrement = false;
                colvarSellerStatus.IsNullable = false;
                colvarSellerStatus.IsPrimaryKey = false;
                colvarSellerStatus.IsForeignKey = false;
                colvarSellerStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerStatus);
                
                TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
                colvarDepartment.ColumnName = "department";
                colvarDepartment.DataType = DbType.Int32;
                colvarDepartment.MaxLength = 0;
                colvarDepartment.AutoIncrement = false;
                colvarDepartment.IsNullable = false;
                colvarDepartment.IsPrimaryKey = false;
                colvarDepartment.IsForeignKey = false;
                colvarDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepartment);
                
                TableSchema.TableColumn colvarSellerRemark = new TableSchema.TableColumn(schema);
                colvarSellerRemark.ColumnName = "seller_remark";
                colvarSellerRemark.DataType = DbType.String;
                colvarSellerRemark.MaxLength = 200;
                colvarSellerRemark.AutoIncrement = false;
                colvarSellerRemark.IsNullable = true;
                colvarSellerRemark.IsPrimaryKey = false;
                colvarSellerRemark.IsForeignKey = false;
                colvarSellerRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerRemark);
                
                TableSchema.TableColumn colvarSellerSales = new TableSchema.TableColumn(schema);
                colvarSellerSales.ColumnName = "seller_sales";
                colvarSellerSales.DataType = DbType.String;
                colvarSellerSales.MaxLength = 50;
                colvarSellerSales.AutoIncrement = false;
                colvarSellerSales.IsNullable = true;
                colvarSellerSales.IsPrimaryKey = false;
                colvarSellerSales.IsForeignKey = false;
                colvarSellerSales.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerSales);
                
                TableSchema.TableColumn colvarSellerLogoimgPath = new TableSchema.TableColumn(schema);
                colvarSellerLogoimgPath.ColumnName = "seller_logoimg_path";
                colvarSellerLogoimgPath.DataType = DbType.AnsiString;
                colvarSellerLogoimgPath.MaxLength = 500;
                colvarSellerLogoimgPath.AutoIncrement = false;
                colvarSellerLogoimgPath.IsNullable = true;
                colvarSellerLogoimgPath.IsPrimaryKey = false;
                colvarSellerLogoimgPath.IsForeignKey = false;
                colvarSellerLogoimgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerLogoimgPath);
                
                TableSchema.TableColumn colvarSellerVideoPath = new TableSchema.TableColumn(schema);
                colvarSellerVideoPath.ColumnName = "seller_video_path";
                colvarSellerVideoPath.DataType = DbType.AnsiString;
                colvarSellerVideoPath.MaxLength = 100;
                colvarSellerVideoPath.AutoIncrement = false;
                colvarSellerVideoPath.IsNullable = true;
                colvarSellerVideoPath.IsPrimaryKey = false;
                colvarSellerVideoPath.IsForeignKey = false;
                colvarSellerVideoPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerVideoPath);
                
                TableSchema.TableColumn colvarDefaultCommissionRate = new TableSchema.TableColumn(schema);
                colvarDefaultCommissionRate.ColumnName = "default_commission_rate";
                colvarDefaultCommissionRate.DataType = DbType.Double;
                colvarDefaultCommissionRate.MaxLength = 0;
                colvarDefaultCommissionRate.AutoIncrement = false;
                colvarDefaultCommissionRate.IsNullable = true;
                colvarDefaultCommissionRate.IsPrimaryKey = false;
                colvarDefaultCommissionRate.IsForeignKey = false;
                colvarDefaultCommissionRate.IsReadOnly = false;
                
                schema.Columns.Add(colvarDefaultCommissionRate);
                
                TableSchema.TableColumn colvarBillingCode = new TableSchema.TableColumn(schema);
                colvarBillingCode.ColumnName = "billing_code";
                colvarBillingCode.DataType = DbType.String;
                colvarBillingCode.MaxLength = 50;
                colvarBillingCode.AutoIncrement = false;
                colvarBillingCode.IsNullable = true;
                colvarBillingCode.IsPrimaryKey = false;
                colvarBillingCode.IsForeignKey = false;
                colvarBillingCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillingCode);
                
                TableSchema.TableColumn colvarWeight = new TableSchema.TableColumn(schema);
                colvarWeight.ColumnName = "weight";
                colvarWeight.DataType = DbType.Int32;
                colvarWeight.MaxLength = 0;
                colvarWeight.AutoIncrement = false;
                colvarWeight.IsNullable = true;
                colvarWeight.IsPrimaryKey = false;
                colvarWeight.IsForeignKey = false;
                colvarWeight.IsReadOnly = false;
                
                schema.Columns.Add(colvarWeight);
                
                TableSchema.TableColumn colvarPostCkoutAction = new TableSchema.TableColumn(schema);
                colvarPostCkoutAction.ColumnName = "post_ckout_action";
                colvarPostCkoutAction.DataType = DbType.Int32;
                colvarPostCkoutAction.MaxLength = 0;
                colvarPostCkoutAction.AutoIncrement = false;
                colvarPostCkoutAction.IsNullable = false;
                colvarPostCkoutAction.IsPrimaryKey = false;
                colvarPostCkoutAction.IsForeignKey = false;
                colvarPostCkoutAction.IsReadOnly = false;
                
                schema.Columns.Add(colvarPostCkoutAction);
                
                TableSchema.TableColumn colvarPostCkoutArgs = new TableSchema.TableColumn(schema);
                colvarPostCkoutArgs.ColumnName = "post_ckout_args";
                colvarPostCkoutArgs.DataType = DbType.String;
                colvarPostCkoutArgs.MaxLength = 150;
                colvarPostCkoutArgs.AutoIncrement = false;
                colvarPostCkoutArgs.IsNullable = true;
                colvarPostCkoutArgs.IsPrimaryKey = false;
                colvarPostCkoutArgs.IsForeignKey = false;
                colvarPostCkoutArgs.IsReadOnly = false;
                
                schema.Columns.Add(colvarPostCkoutArgs);
                
                TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
                colvarCoordinate.ColumnName = "coordinate";
                colvarCoordinate.DataType = DbType.AnsiString;
                colvarCoordinate.MaxLength = -1;
                colvarCoordinate.AutoIncrement = false;
                colvarCoordinate.IsNullable = true;
                colvarCoordinate.IsPrimaryKey = false;
                colvarCoordinate.IsForeignKey = false;
                colvarCoordinate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCoordinate);
                
                TableSchema.TableColumn colvarDeliveryMinuteGap = new TableSchema.TableColumn(schema);
                colvarDeliveryMinuteGap.ColumnName = "delivery_minute_gap";
                colvarDeliveryMinuteGap.DataType = DbType.Int32;
                colvarDeliveryMinuteGap.MaxLength = 0;
                colvarDeliveryMinuteGap.AutoIncrement = false;
                colvarDeliveryMinuteGap.IsNullable = true;
                colvarDeliveryMinuteGap.IsPrimaryKey = false;
                colvarDeliveryMinuteGap.IsForeignKey = false;
                colvarDeliveryMinuteGap.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryMinuteGap);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_coupon",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealCoupon()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealCoupon(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealCoupon(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealCoupon(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public long? CouponId 
	    {
		    get
		    {
			    return GetColumnValue<long?>("coupon_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_id", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("OrderPk")]
        [Bindable(true)]
        public int? OrderPk 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_pk");
		    }
            set 
		    {
			    SetColumnValue("order_pk", value);
            }
        }
	      
        [XmlAttribute("Prefix")]
        [Bindable(true)]
        public string Prefix 
	    {
		    get
		    {
			    return GetColumnValue<string>("prefix");
		    }
            set 
		    {
			    SetColumnValue("prefix", value);
            }
        }
	      
        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public string Sequence 
	    {
		    get
		    {
			    return GetColumnValue<string>("sequence");
		    }
            set 
		    {
			    SetColumnValue("sequence", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int? Status 
	    {
		    get
		    {
			    return GetColumnValue<int?>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("CreateDate")]
        [Bindable(true)]
        public DateTime? CreateDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("create_date");
		    }
            set 
		    {
			    SetColumnValue("create_date", value);
            }
        }
	      
        [XmlAttribute("BoughtDate")]
        [Bindable(true)]
        public DateTime? BoughtDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bought_date");
		    }
            set 
		    {
			    SetColumnValue("bought_date", value);
            }
        }
	      
        [XmlAttribute("UsedDate")]
        [Bindable(true)]
        public DateTime? UsedDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("used_date");
		    }
            set 
		    {
			    SetColumnValue("used_date", value);
            }
        }
	      
        [XmlAttribute("RefundDate")]
        [Bindable(true)]
        public DateTime? RefundDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("refund_date");
		    }
            set 
		    {
			    SetColumnValue("refund_date", value);
            }
        }
	      
        [XmlAttribute("HiDealOrderDetailGuid")]
        [Bindable(true)]
        public Guid HiDealOrderDetailGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("hi_deal_order_detail_guid");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_order_detail_guid", value);
            }
        }
	      
        [XmlAttribute("ItemQuantity")]
        [Bindable(true)]
        public int ItemQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_quantity");
		    }
            set 
		    {
			    SetColumnValue("item_quantity", value);
            }
        }
	      
        [XmlAttribute("UnitPrice")]
        [Bindable(true)]
        public decimal UnitPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("unit_price");
		    }
            set 
		    {
			    SetColumnValue("unit_price", value);
            }
        }
	      
        [XmlAttribute("DetailTotalAmt")]
        [Bindable(true)]
        public decimal DetailTotalAmt 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("detail_total_amt");
		    }
            set 
		    {
			    SetColumnValue("detail_total_amt", value);
            }
        }
	      
        [XmlAttribute("UnitCost")]
        [Bindable(true)]
        public decimal UnitCost 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("unit_cost");
		    }
            set 
		    {
			    SetColumnValue("unit_cost", value);
            }
        }
	      
        [XmlAttribute("TotalCost")]
        [Bindable(true)]
        public decimal TotalCost 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total_cost");
		    }
            set 
		    {
			    SetColumnValue("total_cost", value);
            }
        }
	      
        [XmlAttribute("Category1")]
        [Bindable(true)]
        public int? Category1 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category1");
		    }
            set 
		    {
			    SetColumnValue("category1", value);
            }
        }
	      
        [XmlAttribute("CatgName1")]
        [Bindable(true)]
        public string CatgName1 
	    {
		    get
		    {
			    return GetColumnValue<string>("catg_name1");
		    }
            set 
		    {
			    SetColumnValue("catg_name1", value);
            }
        }
	      
        [XmlAttribute("Option1")]
        [Bindable(true)]
        public int? Option1 
	    {
		    get
		    {
			    return GetColumnValue<int?>("option1");
		    }
            set 
		    {
			    SetColumnValue("option1", value);
            }
        }
	      
        [XmlAttribute("OptionName1")]
        [Bindable(true)]
        public string OptionName1 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name1");
		    }
            set 
		    {
			    SetColumnValue("option_name1", value);
            }
        }
	      
        [XmlAttribute("Category2")]
        [Bindable(true)]
        public int? Category2 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category2");
		    }
            set 
		    {
			    SetColumnValue("category2", value);
            }
        }
	      
        [XmlAttribute("CatgName2")]
        [Bindable(true)]
        public string CatgName2 
	    {
		    get
		    {
			    return GetColumnValue<string>("catg_name2");
		    }
            set 
		    {
			    SetColumnValue("catg_name2", value);
            }
        }
	      
        [XmlAttribute("Option2")]
        [Bindable(true)]
        public int? Option2 
	    {
		    get
		    {
			    return GetColumnValue<int?>("option2");
		    }
            set 
		    {
			    SetColumnValue("option2", value);
            }
        }
	      
        [XmlAttribute("OptionName2")]
        [Bindable(true)]
        public string OptionName2 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name2");
		    }
            set 
		    {
			    SetColumnValue("option_name2", value);
            }
        }
	      
        [XmlAttribute("Category3")]
        [Bindable(true)]
        public int? Category3 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category3");
		    }
            set 
		    {
			    SetColumnValue("category3", value);
            }
        }
	      
        [XmlAttribute("CatgName3")]
        [Bindable(true)]
        public string CatgName3 
	    {
		    get
		    {
			    return GetColumnValue<string>("catg_name3");
		    }
            set 
		    {
			    SetColumnValue("catg_name3", value);
            }
        }
	      
        [XmlAttribute("Option3")]
        [Bindable(true)]
        public int? Option3 
	    {
		    get
		    {
			    return GetColumnValue<int?>("option3");
		    }
            set 
		    {
			    SetColumnValue("option3", value);
            }
        }
	      
        [XmlAttribute("OptionName3")]
        [Bindable(true)]
        public string OptionName3 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name3");
		    }
            set 
		    {
			    SetColumnValue("option_name3", value);
            }
        }
	      
        [XmlAttribute("Category4")]
        [Bindable(true)]
        public int? Category4 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category4");
		    }
            set 
		    {
			    SetColumnValue("category4", value);
            }
        }
	      
        [XmlAttribute("CatgName4")]
        [Bindable(true)]
        public string CatgName4 
	    {
		    get
		    {
			    return GetColumnValue<string>("catg_name4");
		    }
            set 
		    {
			    SetColumnValue("catg_name4", value);
            }
        }
	      
        [XmlAttribute("Option4")]
        [Bindable(true)]
        public int? Option4 
	    {
		    get
		    {
			    return GetColumnValue<int?>("option4");
		    }
            set 
		    {
			    SetColumnValue("option4", value);
            }
        }
	      
        [XmlAttribute("OptionName4")]
        [Bindable(true)]
        public string OptionName4 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name4");
		    }
            set 
		    {
			    SetColumnValue("option_name4", value);
            }
        }
	      
        [XmlAttribute("Category5")]
        [Bindable(true)]
        public int? Category5 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category5");
		    }
            set 
		    {
			    SetColumnValue("category5", value);
            }
        }
	      
        [XmlAttribute("CatgName5")]
        [Bindable(true)]
        public string CatgName5 
	    {
		    get
		    {
			    return GetColumnValue<string>("catg_name5");
		    }
            set 
		    {
			    SetColumnValue("catg_name5", value);
            }
        }
	      
        [XmlAttribute("Option5")]
        [Bindable(true)]
        public int? Option5 
	    {
		    get
		    {
			    return GetColumnValue<int?>("option5");
		    }
            set 
		    {
			    SetColumnValue("option5", value);
            }
        }
	      
        [XmlAttribute("OptionName5")]
        [Bindable(true)]
        public string OptionName5 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name5");
		    }
            set 
		    {
			    SetColumnValue("option_name5", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("AddresseeName")]
        [Bindable(true)]
        public string AddresseeName 
	    {
		    get
		    {
			    return GetColumnValue<string>("addressee_name");
		    }
            set 
		    {
			    SetColumnValue("addressee_name", value);
            }
        }
	      
        [XmlAttribute("AddresseePhone")]
        [Bindable(true)]
        public string AddresseePhone 
	    {
		    get
		    {
			    return GetColumnValue<string>("addressee_phone");
		    }
            set 
		    {
			    SetColumnValue("addressee_phone", value);
            }
        }
	      
        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_address");
		    }
            set 
		    {
			    SetColumnValue("delivery_address", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("ProductType")]
        [Bindable(true)]
        public int ProductType 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_type");
		    }
            set 
		    {
			    SetColumnValue("product_type", value);
            }
        }
	      
        [XmlAttribute("Pk")]
        [Bindable(true)]
        public int Pk 
	    {
		    get
		    {
			    return GetColumnValue<int>("pk");
		    }
            set 
		    {
			    SetColumnValue("pk", value);
            }
        }
	      
        [XmlAttribute("HiDealOrderGuid")]
        [Bindable(true)]
        public Guid HiDealOrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("hi_deal_order_guid");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_order_guid", value);
            }
        }
	      
        [XmlAttribute("HiDealOrderId")]
        [Bindable(true)]
        public string HiDealOrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("hi_deal_order_id");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_order_id", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("TotalAmount")]
        [Bindable(true)]
        public decimal TotalAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total_amount");
		    }
            set 
		    {
			    SetColumnValue("total_amount", value);
            }
        }
	      
        [XmlAttribute("Pcash")]
        [Bindable(true)]
        public decimal Pcash 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("pcash");
		    }
            set 
		    {
			    SetColumnValue("pcash", value);
            }
        }
	      
        [XmlAttribute("Scash")]
        [Bindable(true)]
        public decimal Scash 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("scash");
		    }
            set 
		    {
			    SetColumnValue("scash", value);
            }
        }
	      
        [XmlAttribute("Bcash")]
        [Bindable(true)]
        public decimal Bcash 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("bcash");
		    }
            set 
		    {
			    SetColumnValue("bcash", value);
            }
        }
	      
        [XmlAttribute("CreditCardAmt")]
        [Bindable(true)]
        public decimal CreditCardAmt 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("credit_card_amt");
		    }
            set 
		    {
			    SetColumnValue("credit_card_amt", value);
            }
        }
	      
        [XmlAttribute("DiscountAmount")]
        [Bindable(true)]
        public decimal DiscountAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("discount_amount");
		    }
            set 
		    {
			    SetColumnValue("discount_amount", value);
            }
        }
	      
        [XmlAttribute("AtmAmount")]
        [Bindable(true)]
        public decimal AtmAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("atm_amount");
		    }
            set 
		    {
			    SetColumnValue("atm_amount", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("HiDealOrderCreateId")]
        [Bindable(true)]
        public string HiDealOrderCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("hi_deal_order_create_id");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_order_create_id", value);
            }
        }
	      
        [XmlAttribute("HiDealOrderCreateTime")]
        [Bindable(true)]
        public DateTime HiDealOrderCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("hi_deal_order_create_time");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_order_create_time", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("ProductGuid")]
        [Bindable(true)]
        public Guid ProductGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("product_guid");
		    }
            set 
		    {
			    SetColumnValue("product_guid", value);
            }
        }
	      
        [XmlAttribute("ProductSeq")]
        [Bindable(true)]
        public int? ProductSeq 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_seq");
		    }
            set 
		    {
			    SetColumnValue("product_seq", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description 
	    {
		    get
		    {
			    return GetColumnValue<string>("description");
		    }
            set 
		    {
			    SetColumnValue("description", value);
            }
        }
	      
        [XmlAttribute("IsOnline")]
        [Bindable(true)]
        public bool? IsOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_online");
		    }
            set 
		    {
			    SetColumnValue("is_online", value);
            }
        }
	      
        [XmlAttribute("UseStartTime")]
        [Bindable(true)]
        public DateTime? UseStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_start_time");
		    }
            set 
		    {
			    SetColumnValue("use_start_time", value);
            }
        }
	      
        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public DateTime? UseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_end_time");
		    }
            set 
		    {
			    SetColumnValue("use_end_time", value);
            }
        }
	      
        [XmlAttribute("ChangedExpireTime")]
        [Bindable(true)]
        public DateTime? ChangedExpireTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("changed_expire_time");
		    }
            set 
		    {
			    SetColumnValue("changed_expire_time", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int? Type 
	    {
		    get
		    {
			    return GetColumnValue<int?>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("OriginalPrice")]
        [Bindable(true)]
        public decimal? OriginalPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("original_price");
		    }
            set 
		    {
			    SetColumnValue("original_price", value);
            }
        }
	      
        [XmlAttribute("Price")]
        [Bindable(true)]
        public decimal? Price 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("price");
		    }
            set 
		    {
			    SetColumnValue("price", value);
            }
        }
	      
        [XmlAttribute("Quantity")]
        [Bindable(true)]
        public int? Quantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("quantity");
		    }
            set 
		    {
			    SetColumnValue("quantity", value);
            }
        }
	      
        [XmlAttribute("OrderLimit")]
        [Bindable(true)]
        public int? OrderLimit 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_limit");
		    }
            set 
		    {
			    SetColumnValue("order_limit", value);
            }
        }
	      
        [XmlAttribute("OrderLimitUser")]
        [Bindable(true)]
        public int? OrderLimitUser 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_limit_user");
		    }
            set 
		    {
			    SetColumnValue("order_limit_user", value);
            }
        }
	      
        [XmlAttribute("OrderCount")]
        [Bindable(true)]
        public int? OrderCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_count");
		    }
            set 
		    {
			    SetColumnValue("order_count", value);
            }
        }
	      
        [XmlAttribute("IsNoRefund")]
        [Bindable(true)]
        public bool IsNoRefund 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_no_refund");
		    }
            set 
		    {
			    SetColumnValue("is_no_refund", value);
            }
        }
	      
        [XmlAttribute("IsExpireNoRefund")]
        [Bindable(true)]
        public bool IsExpireNoRefund 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_expire_no_refund");
		    }
            set 
		    {
			    SetColumnValue("is_expire_no_refund", value);
            }
        }
	      
        [XmlAttribute("IsTaxFree")]
        [Bindable(true)]
        public bool IsTaxFree 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_tax_free");
		    }
            set 
		    {
			    SetColumnValue("is_tax_free", value);
            }
        }
	      
        [XmlAttribute("IsSoldout")]
        [Bindable(true)]
        public bool? IsSoldout 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_soldout");
		    }
            set 
		    {
			    SetColumnValue("is_soldout", value);
            }
        }
	      
        [XmlAttribute("IsInStore")]
        [Bindable(true)]
        public bool IsInStore 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_in_store");
		    }
            set 
		    {
			    SetColumnValue("is_in_store", value);
            }
        }
	      
        [XmlAttribute("InStoreDesc")]
        [Bindable(true)]
        public string InStoreDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("in_store_desc");
		    }
            set 
		    {
			    SetColumnValue("in_store_desc", value);
            }
        }
	      
        [XmlAttribute("IsHomeDelivery")]
        [Bindable(true)]
        public bool IsHomeDelivery 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_home_delivery");
		    }
            set 
		    {
			    SetColumnValue("is_home_delivery", value);
            }
        }
	      
        [XmlAttribute("HomeDeliveryDesc")]
        [Bindable(true)]
        public string HomeDeliveryDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("home_delivery_desc");
		    }
            set 
		    {
			    SetColumnValue("home_delivery_desc", value);
            }
        }
	      
        [XmlAttribute("Sms")]
        [Bindable(true)]
        public string Sms 
	    {
		    get
		    {
			    return GetColumnValue<string>("sms");
		    }
            set 
		    {
			    SetColumnValue("sms", value);
            }
        }
	      
        [XmlAttribute("IsSmsClose")]
        [Bindable(true)]
        public bool IsSmsClose 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_sms_close");
		    }
            set 
		    {
			    SetColumnValue("is_sms_close", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime? StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	      
        [XmlAttribute("Pic")]
        [Bindable(true)]
        public string Pic 
	    {
		    get
		    {
			    return GetColumnValue<string>("pic");
		    }
            set 
		    {
			    SetColumnValue("pic", value);
            }
        }
	      
        [XmlAttribute("HasBranchStoreQuantityLimit")]
        [Bindable(true)]
        public bool HasBranchStoreQuantityLimit 
	    {
		    get
		    {
			    return GetColumnValue<bool>("has_branch_store_quantity_limit");
		    }
            set 
		    {
			    SetColumnValue("has_branch_store_quantity_limit", value);
            }
        }
	      
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("DealGuid")]
        [Bindable(true)]
        public Guid DealGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("deal_guid");
		    }
            set 
		    {
			    SetColumnValue("deal_guid", value);
            }
        }
	      
        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int? DealType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_type");
		    }
            set 
		    {
			    SetColumnValue("deal_type", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_name");
		    }
            set 
		    {
			    SetColumnValue("deal_name", value);
            }
        }
	      
        [XmlAttribute("PromoLongDesc")]
        [Bindable(true)]
        public string PromoLongDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("promo_long_desc");
		    }
            set 
		    {
			    SetColumnValue("promo_long_desc", value);
            }
        }
	      
        [XmlAttribute("PromoShortDesc")]
        [Bindable(true)]
        public string PromoShortDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("promo_short_desc");
		    }
            set 
		    {
			    SetColumnValue("promo_short_desc", value);
            }
        }
	      
        [XmlAttribute("Picture")]
        [Bindable(true)]
        public string Picture 
	    {
		    get
		    {
			    return GetColumnValue<string>("picture");
		    }
            set 
		    {
			    SetColumnValue("picture", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_start_time", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("IsOpen")]
        [Bindable(true)]
        public bool IsOpen 
	    {
		    get
		    {
			    return GetColumnValue<bool>("IsOpen");
		    }
            set 
		    {
			    SetColumnValue("IsOpen", value);
            }
        }
	      
        [XmlAttribute("IsShow")]
        [Bindable(true)]
        public bool IsShow 
	    {
		    get
		    {
			    return GetColumnValue<bool>("IsShow");
		    }
            set 
		    {
			    SetColumnValue("IsShow", value);
            }
        }
	      
        [XmlAttribute("IsAlwaysMain")]
        [Bindable(true)]
        public bool IsAlwaysMain 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_always_main");
		    }
            set 
		    {
			    SetColumnValue("is_always_main", value);
            }
        }
	      
        [XmlAttribute("IsVerifyByList")]
        [Bindable(true)]
        public bool IsVerifyByList 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_verify_by_list");
		    }
            set 
		    {
			    SetColumnValue("is_verify_by_list", value);
            }
        }
	      
        [XmlAttribute("IsVerifyByPad")]
        [Bindable(true)]
        public bool IsVerifyByPad 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_verify_by_pad");
		    }
            set 
		    {
			    SetColumnValue("is_verify_by_pad", value);
            }
        }
	      
        [XmlAttribute("Sales")]
        [Bindable(true)]
        public string Sales 
	    {
		    get
		    {
			    return GetColumnValue<string>("sales");
		    }
            set 
		    {
			    SetColumnValue("sales", value);
            }
        }
	      
        [XmlAttribute("SalesGroupId")]
        [Bindable(true)]
        public int? SalesGroupId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("sales_group_id");
		    }
            set 
		    {
			    SetColumnValue("sales_group_id", value);
            }
        }
	      
        [XmlAttribute("SalesGroupName")]
        [Bindable(true)]
        public string SalesGroupName 
	    {
		    get
		    {
			    return GetColumnValue<string>("sales_group_name");
		    }
            set 
		    {
			    SetColumnValue("sales_group_name", value);
            }
        }
	      
        [XmlAttribute("SalesDeptId")]
        [Bindable(true)]
        public string SalesDeptId 
	    {
		    get
		    {
			    return GetColumnValue<string>("sales_dept_id");
		    }
            set 
		    {
			    SetColumnValue("sales_dept_id", value);
            }
        }
	      
        [XmlAttribute("SalesDept")]
        [Bindable(true)]
        public string SalesDept 
	    {
		    get
		    {
			    return GetColumnValue<string>("sales_dept");
		    }
            set 
		    {
			    SetColumnValue("sales_dept", value);
            }
        }
	      
        [XmlAttribute("SalesLocationId")]
        [Bindable(true)]
        public int? SalesLocationId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("sales_location_id");
		    }
            set 
		    {
			    SetColumnValue("sales_location_id", value);
            }
        }
	      
        [XmlAttribute("SalesLocation")]
        [Bindable(true)]
        public string SalesLocation 
	    {
		    get
		    {
			    return GetColumnValue<string>("sales_location");
		    }
            set 
		    {
			    SetColumnValue("sales_location", value);
            }
        }
	      
        [XmlAttribute("SalesCatgCodeGroup")]
        [Bindable(true)]
        public string SalesCatgCodeGroup 
	    {
		    get
		    {
			    return GetColumnValue<string>("sales_catg_code_group");
		    }
            set 
		    {
			    SetColumnValue("sales_catg_code_group", value);
            }
        }
	      
        [XmlAttribute("SalesCatgCodeId")]
        [Bindable(true)]
        public int? SalesCatgCodeId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("sales_catg_code_id");
		    }
            set 
		    {
			    SetColumnValue("sales_catg_code_id", value);
            }
        }
	      
        [XmlAttribute("SalesCatgCodeName")]
        [Bindable(true)]
        public string SalesCatgCodeName 
	    {
		    get
		    {
			    return GetColumnValue<string>("sales_catg_code_name");
		    }
            set 
		    {
			    SetColumnValue("sales_catg_code_name", value);
            }
        }
	      
        [XmlAttribute("SettleDay1")]
        [Bindable(true)]
        public int? SettleDay1 
	    {
		    get
		    {
			    return GetColumnValue<int?>("settle_day_1");
		    }
            set 
		    {
			    SetColumnValue("settle_day_1", value);
            }
        }
	      
        [XmlAttribute("SettleDay2")]
        [Bindable(true)]
        public int? SettleDay2 
	    {
		    get
		    {
			    return GetColumnValue<int?>("settle_day_2");
		    }
            set 
		    {
			    SetColumnValue("settle_day_2", value);
            }
        }
	      
        [XmlAttribute("SettleDay3")]
        [Bindable(true)]
        public int? SettleDay3 
	    {
		    get
		    {
			    return GetColumnValue<int?>("settle_day_3");
		    }
            set 
		    {
			    SetColumnValue("settle_day_3", value);
            }
        }
	      
        [XmlAttribute("Cities")]
        [Bindable(true)]
        public string Cities 
	    {
		    get
		    {
			    return GetColumnValue<string>("cities");
		    }
            set 
		    {
			    SetColumnValue("cities", value);
            }
        }
	      
        [XmlAttribute("IsVisa")]
        [Bindable(true)]
        public bool? IsVisa 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_visa");
		    }
            set 
		    {
			    SetColumnValue("is_visa", value);
            }
        }
	      
        [XmlAttribute("EdmTitle")]
        [Bindable(true)]
        public string EdmTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("edm_title");
		    }
            set 
		    {
			    SetColumnValue("edm_title", value);
            }
        }
	      
        [XmlAttribute("EdmSubject")]
        [Bindable(true)]
        public string EdmSubject 
	    {
		    get
		    {
			    return GetColumnValue<string>("edm_subject");
		    }
            set 
		    {
			    SetColumnValue("edm_subject", value);
            }
        }
	      
        [XmlAttribute("PrimaryBigPicture")]
        [Bindable(true)]
        public string PrimaryBigPicture 
	    {
		    get
		    {
			    return GetColumnValue<string>("primary_big_picture");
		    }
            set 
		    {
			    SetColumnValue("primary_big_picture", value);
            }
        }
	      
        [XmlAttribute("PrimarySmallPicture")]
        [Bindable(true)]
        public string PrimarySmallPicture 
	    {
		    get
		    {
			    return GetColumnValue<string>("primary_small_picture");
		    }
            set 
		    {
			    SetColumnValue("primary_small_picture", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerBossName")]
        [Bindable(true)]
        public string SellerBossName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_boss_name");
		    }
            set 
		    {
			    SetColumnValue("seller_boss_name", value);
            }
        }
	      
        [XmlAttribute("SellerTel")]
        [Bindable(true)]
        public string SellerTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_tel");
		    }
            set 
		    {
			    SetColumnValue("seller_tel", value);
            }
        }
	      
        [XmlAttribute("SellerTel2")]
        [Bindable(true)]
        public string SellerTel2 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_tel2");
		    }
            set 
		    {
			    SetColumnValue("seller_tel2", value);
            }
        }
	      
        [XmlAttribute("SellerFax")]
        [Bindable(true)]
        public string SellerFax 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_fax");
		    }
            set 
		    {
			    SetColumnValue("seller_fax", value);
            }
        }
	      
        [XmlAttribute("SellerMobile")]
        [Bindable(true)]
        public string SellerMobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_mobile");
		    }
            set 
		    {
			    SetColumnValue("seller_mobile", value);
            }
        }
	      
        [XmlAttribute("SellerAddress")]
        [Bindable(true)]
        public string SellerAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_address");
		    }
            set 
		    {
			    SetColumnValue("seller_address", value);
            }
        }
	      
        [XmlAttribute("SellerEmail")]
        [Bindable(true)]
        public string SellerEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_email");
		    }
            set 
		    {
			    SetColumnValue("seller_email", value);
            }
        }
	      
        [XmlAttribute("SellerBlog")]
        [Bindable(true)]
        public string SellerBlog 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_blog");
		    }
            set 
		    {
			    SetColumnValue("seller_blog", value);
            }
        }
	      
        [XmlAttribute("SellerInvoice")]
        [Bindable(true)]
        public string SellerInvoice 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_invoice");
		    }
            set 
		    {
			    SetColumnValue("seller_invoice", value);
            }
        }
	      
        [XmlAttribute("SellerDescription")]
        [Bindable(true)]
        public string SellerDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_description");
		    }
            set 
		    {
			    SetColumnValue("seller_description", value);
            }
        }
	      
        [XmlAttribute("SellerStatus")]
        [Bindable(true)]
        public int SellerStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_status");
		    }
            set 
		    {
			    SetColumnValue("seller_status", value);
            }
        }
	      
        [XmlAttribute("Department")]
        [Bindable(true)]
        public int Department 
	    {
		    get
		    {
			    return GetColumnValue<int>("department");
		    }
            set 
		    {
			    SetColumnValue("department", value);
            }
        }
	      
        [XmlAttribute("SellerRemark")]
        [Bindable(true)]
        public string SellerRemark 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_remark");
		    }
            set 
		    {
			    SetColumnValue("seller_remark", value);
            }
        }
	      
        [XmlAttribute("SellerSales")]
        [Bindable(true)]
        public string SellerSales 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_sales");
		    }
            set 
		    {
			    SetColumnValue("seller_sales", value);
            }
        }
	      
        [XmlAttribute("SellerLogoimgPath")]
        [Bindable(true)]
        public string SellerLogoimgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_logoimg_path");
		    }
            set 
		    {
			    SetColumnValue("seller_logoimg_path", value);
            }
        }
	      
        [XmlAttribute("SellerVideoPath")]
        [Bindable(true)]
        public string SellerVideoPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_video_path");
		    }
            set 
		    {
			    SetColumnValue("seller_video_path", value);
            }
        }
	      
        [XmlAttribute("DefaultCommissionRate")]
        [Bindable(true)]
        public double? DefaultCommissionRate 
	    {
		    get
		    {
			    return GetColumnValue<double?>("default_commission_rate");
		    }
            set 
		    {
			    SetColumnValue("default_commission_rate", value);
            }
        }
	      
        [XmlAttribute("BillingCode")]
        [Bindable(true)]
        public string BillingCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("billing_code");
		    }
            set 
		    {
			    SetColumnValue("billing_code", value);
            }
        }
	      
        [XmlAttribute("Weight")]
        [Bindable(true)]
        public int? Weight 
	    {
		    get
		    {
			    return GetColumnValue<int?>("weight");
		    }
            set 
		    {
			    SetColumnValue("weight", value);
            }
        }
	      
        [XmlAttribute("PostCkoutAction")]
        [Bindable(true)]
        public int PostCkoutAction 
	    {
		    get
		    {
			    return GetColumnValue<int>("post_ckout_action");
		    }
            set 
		    {
			    SetColumnValue("post_ckout_action", value);
            }
        }
	      
        [XmlAttribute("PostCkoutArgs")]
        [Bindable(true)]
        public string PostCkoutArgs 
	    {
		    get
		    {
			    return GetColumnValue<string>("post_ckout_args");
		    }
            set 
		    {
			    SetColumnValue("post_ckout_args", value);
            }
        }
	      
        [XmlAttribute("Coordinate")]
        [Bindable(true)]
        public string Coordinate 
	    {
		    get
		    {
			    return GetColumnValue<string>("coordinate");
		    }
            set 
		    {
			    SetColumnValue("coordinate", value);
            }
        }
	      
        [XmlAttribute("DeliveryMinuteGap")]
        [Bindable(true)]
        public int? DeliveryMinuteGap 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_minute_gap");
		    }
            set 
		    {
			    SetColumnValue("delivery_minute_gap", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CouponId = @"coupon_id";
            
            public static string StoreGuid = @"store_guid";
            
            public static string OrderPk = @"order_pk";
            
            public static string Prefix = @"prefix";
            
            public static string Sequence = @"sequence";
            
            public static string Code = @"code";
            
            public static string Status = @"status";
            
            public static string CreateDate = @"create_date";
            
            public static string BoughtDate = @"bought_date";
            
            public static string UsedDate = @"used_date";
            
            public static string RefundDate = @"refund_date";
            
            public static string HiDealOrderDetailGuid = @"hi_deal_order_detail_guid";
            
            public static string ItemQuantity = @"item_quantity";
            
            public static string UnitPrice = @"unit_price";
            
            public static string DetailTotalAmt = @"detail_total_amt";
            
            public static string UnitCost = @"unit_cost";
            
            public static string TotalCost = @"total_cost";
            
            public static string Category1 = @"category1";
            
            public static string CatgName1 = @"catg_name1";
            
            public static string Option1 = @"option1";
            
            public static string OptionName1 = @"option_name1";
            
            public static string Category2 = @"category2";
            
            public static string CatgName2 = @"catg_name2";
            
            public static string Option2 = @"option2";
            
            public static string OptionName2 = @"option_name2";
            
            public static string Category3 = @"category3";
            
            public static string CatgName3 = @"catg_name3";
            
            public static string Option3 = @"option3";
            
            public static string OptionName3 = @"option_name3";
            
            public static string Category4 = @"category4";
            
            public static string CatgName4 = @"catg_name4";
            
            public static string Option4 = @"option4";
            
            public static string OptionName4 = @"option_name4";
            
            public static string Category5 = @"category5";
            
            public static string CatgName5 = @"catg_name5";
            
            public static string Option5 = @"option5";
            
            public static string OptionName5 = @"option_name5";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string AddresseeName = @"addressee_name";
            
            public static string AddresseePhone = @"addressee_phone";
            
            public static string DeliveryAddress = @"delivery_address";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string ModifyTime = @"modify_time";
            
            public static string ProductType = @"product_type";
            
            public static string Pk = @"pk";
            
            public static string HiDealOrderGuid = @"hi_deal_order_guid";
            
            public static string HiDealOrderId = @"hi_deal_order_id";
            
            public static string UserId = @"user_id";
            
            public static string UserName = @"user_name";
            
            public static string TotalAmount = @"total_amount";
            
            public static string Pcash = @"pcash";
            
            public static string Scash = @"scash";
            
            public static string Bcash = @"bcash";
            
            public static string CreditCardAmt = @"credit_card_amt";
            
            public static string DiscountAmount = @"discount_amount";
            
            public static string AtmAmount = @"atm_amount";
            
            public static string OrderStatus = @"order_status";
            
            public static string HiDealOrderCreateId = @"hi_deal_order_create_id";
            
            public static string HiDealOrderCreateTime = @"hi_deal_order_create_time";
            
            public static string ProductId = @"product_id";
            
            public static string ProductGuid = @"product_guid";
            
            public static string ProductSeq = @"product_seq";
            
            public static string Name = @"name";
            
            public static string Description = @"description";
            
            public static string IsOnline = @"is_online";
            
            public static string UseStartTime = @"use_start_time";
            
            public static string UseEndTime = @"use_end_time";
            
            public static string ChangedExpireTime = @"changed_expire_time";
            
            public static string Type = @"type";
            
            public static string OriginalPrice = @"original_price";
            
            public static string Price = @"price";
            
            public static string Quantity = @"quantity";
            
            public static string OrderLimit = @"order_limit";
            
            public static string OrderLimitUser = @"order_limit_user";
            
            public static string OrderCount = @"order_count";
            
            public static string IsNoRefund = @"is_no_refund";
            
            public static string IsExpireNoRefund = @"is_expire_no_refund";
            
            public static string IsTaxFree = @"is_tax_free";
            
            public static string IsSoldout = @"is_soldout";
            
            public static string IsInStore = @"is_in_store";
            
            public static string InStoreDesc = @"in_store_desc";
            
            public static string IsHomeDelivery = @"is_home_delivery";
            
            public static string HomeDeliveryDesc = @"home_delivery_desc";
            
            public static string Sms = @"sms";
            
            public static string IsSmsClose = @"is_sms_close";
            
            public static string StartTime = @"start_time";
            
            public static string EndTime = @"end_time";
            
            public static string Pic = @"pic";
            
            public static string HasBranchStoreQuantityLimit = @"has_branch_store_quantity_limit";
            
            public static string DealId = @"deal_id";
            
            public static string DealGuid = @"deal_guid";
            
            public static string DealType = @"deal_type";
            
            public static string DealName = @"deal_name";
            
            public static string PromoLongDesc = @"promo_long_desc";
            
            public static string PromoShortDesc = @"promo_short_desc";
            
            public static string Picture = @"picture";
            
            public static string DealStartTime = @"deal_start_time";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string IsOpen = @"IsOpen";
            
            public static string IsShow = @"IsShow";
            
            public static string IsAlwaysMain = @"is_always_main";
            
            public static string IsVerifyByList = @"is_verify_by_list";
            
            public static string IsVerifyByPad = @"is_verify_by_pad";
            
            public static string Sales = @"sales";
            
            public static string SalesGroupId = @"sales_group_id";
            
            public static string SalesGroupName = @"sales_group_name";
            
            public static string SalesDeptId = @"sales_dept_id";
            
            public static string SalesDept = @"sales_dept";
            
            public static string SalesLocationId = @"sales_location_id";
            
            public static string SalesLocation = @"sales_location";
            
            public static string SalesCatgCodeGroup = @"sales_catg_code_group";
            
            public static string SalesCatgCodeId = @"sales_catg_code_id";
            
            public static string SalesCatgCodeName = @"sales_catg_code_name";
            
            public static string SettleDay1 = @"settle_day_1";
            
            public static string SettleDay2 = @"settle_day_2";
            
            public static string SettleDay3 = @"settle_day_3";
            
            public static string Cities = @"cities";
            
            public static string IsVisa = @"is_visa";
            
            public static string EdmTitle = @"edm_title";
            
            public static string EdmSubject = @"edm_subject";
            
            public static string PrimaryBigPicture = @"primary_big_picture";
            
            public static string PrimarySmallPicture = @"primary_small_picture";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string SellerId = @"seller_id";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerBossName = @"seller_boss_name";
            
            public static string SellerTel = @"seller_tel";
            
            public static string SellerTel2 = @"seller_tel2";
            
            public static string SellerFax = @"seller_fax";
            
            public static string SellerMobile = @"seller_mobile";
            
            public static string SellerAddress = @"seller_address";
            
            public static string SellerEmail = @"seller_email";
            
            public static string SellerBlog = @"seller_blog";
            
            public static string SellerInvoice = @"seller_invoice";
            
            public static string SellerDescription = @"seller_description";
            
            public static string SellerStatus = @"seller_status";
            
            public static string Department = @"department";
            
            public static string SellerRemark = @"seller_remark";
            
            public static string SellerSales = @"seller_sales";
            
            public static string SellerLogoimgPath = @"seller_logoimg_path";
            
            public static string SellerVideoPath = @"seller_video_path";
            
            public static string DefaultCommissionRate = @"default_commission_rate";
            
            public static string BillingCode = @"billing_code";
            
            public static string Weight = @"weight";
            
            public static string PostCkoutAction = @"post_ckout_action";
            
            public static string PostCkoutArgs = @"post_ckout_args";
            
            public static string Coordinate = @"coordinate";
            
            public static string DeliveryMinuteGap = @"delivery_minute_gap";
            
            public static string CityId = @"city_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEventPromoVourcher class.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromoVourcherCollection : ReadOnlyList<ViewEventPromoVourcher, ViewEventPromoVourcherCollection>
    {        
        public ViewEventPromoVourcherCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_event_promo_vourcher view.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromoVourcher : ReadOnlyRecord<ViewEventPromoVourcher>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_event_promo_vourcher", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "Id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
                colvarUrl.ColumnName = "Url";
                colvarUrl.DataType = DbType.AnsiString;
                colvarUrl.MaxLength = 100;
                colvarUrl.AutoIncrement = false;
                colvarUrl.IsNullable = false;
                colvarUrl.IsPrimaryKey = false;
                colvarUrl.IsForeignKey = false;
                colvarUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarUrl);
                
                TableSchema.TableColumn colvarPromoStartDate = new TableSchema.TableColumn(schema);
                colvarPromoStartDate.ColumnName = "promo_start_date";
                colvarPromoStartDate.DataType = DbType.DateTime;
                colvarPromoStartDate.MaxLength = 0;
                colvarPromoStartDate.AutoIncrement = false;
                colvarPromoStartDate.IsNullable = false;
                colvarPromoStartDate.IsPrimaryKey = false;
                colvarPromoStartDate.IsForeignKey = false;
                colvarPromoStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoStartDate);
                
                TableSchema.TableColumn colvarPromoEndDate = new TableSchema.TableColumn(schema);
                colvarPromoEndDate.ColumnName = "promo_end_date";
                colvarPromoEndDate.DataType = DbType.DateTime;
                colvarPromoEndDate.MaxLength = 0;
                colvarPromoEndDate.AutoIncrement = false;
                colvarPromoEndDate.IsNullable = false;
                colvarPromoEndDate.IsPrimaryKey = false;
                colvarPromoEndDate.IsForeignKey = false;
                colvarPromoEndDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoEndDate);
                
                TableSchema.TableColumn colvarPromoType = new TableSchema.TableColumn(schema);
                colvarPromoType.ColumnName = "promo_type";
                colvarPromoType.DataType = DbType.Int32;
                colvarPromoType.MaxLength = 0;
                colvarPromoType.AutoIncrement = false;
                colvarPromoType.IsNullable = false;
                colvarPromoType.IsPrimaryKey = false;
                colvarPromoType.IsForeignKey = false;
                colvarPromoType.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoType);
                
                TableSchema.TableColumn colvarPromoStatus = new TableSchema.TableColumn(schema);
                colvarPromoStatus.ColumnName = "promo_status";
                colvarPromoStatus.DataType = DbType.Boolean;
                colvarPromoStatus.MaxLength = 0;
                colvarPromoStatus.AutoIncrement = false;
                colvarPromoStatus.IsNullable = false;
                colvarPromoStatus.IsPrimaryKey = false;
                colvarPromoStatus.IsForeignKey = false;
                colvarPromoStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoStatus);
                
                TableSchema.TableColumn colvarTemplateType = new TableSchema.TableColumn(schema);
                colvarTemplateType.ColumnName = "template_type";
                colvarTemplateType.DataType = DbType.Int32;
                colvarTemplateType.MaxLength = 0;
                colvarTemplateType.AutoIncrement = false;
                colvarTemplateType.IsNullable = false;
                colvarTemplateType.IsPrimaryKey = false;
                colvarTemplateType.IsForeignKey = false;
                colvarTemplateType.IsReadOnly = false;
                
                schema.Columns.Add(colvarTemplateType);
                
                TableSchema.TableColumn colvarParentId = new TableSchema.TableColumn(schema);
                colvarParentId.ColumnName = "parent_id";
                colvarParentId.DataType = DbType.Int32;
                colvarParentId.MaxLength = 0;
                colvarParentId.AutoIncrement = false;
                colvarParentId.IsNullable = true;
                colvarParentId.IsPrimaryKey = false;
                colvarParentId.IsForeignKey = false;
                colvarParentId.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentId);
                
                TableSchema.TableColumn colvarShowInApp = new TableSchema.TableColumn(schema);
                colvarShowInApp.ColumnName = "show_in_app";
                colvarShowInApp.DataType = DbType.Boolean;
                colvarShowInApp.MaxLength = 0;
                colvarShowInApp.AutoIncrement = false;
                colvarShowInApp.IsNullable = false;
                colvarShowInApp.IsPrimaryKey = false;
                colvarShowInApp.IsForeignKey = false;
                colvarShowInApp.IsReadOnly = false;
                
                schema.Columns.Add(colvarShowInApp);
                
                TableSchema.TableColumn colvarShowInIdeas = new TableSchema.TableColumn(schema);
                colvarShowInIdeas.ColumnName = "show_in_ideas";
                colvarShowInIdeas.DataType = DbType.Boolean;
                colvarShowInIdeas.MaxLength = 0;
                colvarShowInIdeas.AutoIncrement = false;
                colvarShowInIdeas.IsNullable = false;
                colvarShowInIdeas.IsPrimaryKey = false;
                colvarShowInIdeas.IsForeignKey = false;
                colvarShowInIdeas.IsReadOnly = false;
                
                schema.Columns.Add(colvarShowInIdeas);
                
                TableSchema.TableColumn colvarShowInWeb = new TableSchema.TableColumn(schema);
                colvarShowInWeb.ColumnName = "show_in_web";
                colvarShowInWeb.DataType = DbType.Boolean;
                colvarShowInWeb.MaxLength = 0;
                colvarShowInWeb.AutoIncrement = false;
                colvarShowInWeb.IsNullable = false;
                colvarShowInWeb.IsPrimaryKey = false;
                colvarShowInWeb.IsForeignKey = false;
                colvarShowInWeb.IsReadOnly = false;
                
                schema.Columns.Add(colvarShowInWeb);
                
                TableSchema.TableColumn colvarEventPromoItemId = new TableSchema.TableColumn(schema);
                colvarEventPromoItemId.ColumnName = "event_promo_item_id";
                colvarEventPromoItemId.DataType = DbType.Int32;
                colvarEventPromoItemId.MaxLength = 0;
                colvarEventPromoItemId.AutoIncrement = false;
                colvarEventPromoItemId.IsNullable = false;
                colvarEventPromoItemId.IsPrimaryKey = false;
                colvarEventPromoItemId.IsForeignKey = false;
                colvarEventPromoItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventPromoItemId);
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "Title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 200;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = false;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "Description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = 300;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = false;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescription);
                
                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "Seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = false;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarSeq);
                
                TableSchema.TableColumn colvarPromoItemStatus = new TableSchema.TableColumn(schema);
                colvarPromoItemStatus.ColumnName = "promo_item_status";
                colvarPromoItemStatus.DataType = DbType.Boolean;
                colvarPromoItemStatus.MaxLength = 0;
                colvarPromoItemStatus.AutoIncrement = false;
                colvarPromoItemStatus.IsNullable = false;
                colvarPromoItemStatus.IsPrimaryKey = false;
                colvarPromoItemStatus.IsForeignKey = false;
                colvarPromoItemStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoItemStatus);
                
                TableSchema.TableColumn colvarVourcherEventId = new TableSchema.TableColumn(schema);
                colvarVourcherEventId.ColumnName = "vourcher_event_id";
                colvarVourcherEventId.DataType = DbType.Int32;
                colvarVourcherEventId.MaxLength = 0;
                colvarVourcherEventId.AutoIncrement = false;
                colvarVourcherEventId.IsNullable = false;
                colvarVourcherEventId.IsPrimaryKey = false;
                colvarVourcherEventId.IsForeignKey = false;
                colvarVourcherEventId.IsReadOnly = false;
                
                schema.Columns.Add(colvarVourcherEventId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarVourcherEventStatus = new TableSchema.TableColumn(schema);
                colvarVourcherEventStatus.ColumnName = "vourcher_event_status";
                colvarVourcherEventStatus.DataType = DbType.Int32;
                colvarVourcherEventStatus.MaxLength = 0;
                colvarVourcherEventStatus.AutoIncrement = false;
                colvarVourcherEventStatus.IsNullable = false;
                colvarVourcherEventStatus.IsPrimaryKey = false;
                colvarVourcherEventStatus.IsForeignKey = false;
                colvarVourcherEventStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarVourcherEventStatus);
                
                TableSchema.TableColumn colvarVourcherEventType = new TableSchema.TableColumn(schema);
                colvarVourcherEventType.ColumnName = "vourcher_event_type";
                colvarVourcherEventType.DataType = DbType.Int32;
                colvarVourcherEventType.MaxLength = 0;
                colvarVourcherEventType.AutoIncrement = false;
                colvarVourcherEventType.IsNullable = false;
                colvarVourcherEventType.IsPrimaryKey = false;
                colvarVourcherEventType.IsForeignKey = false;
                colvarVourcherEventType.IsReadOnly = false;
                
                schema.Columns.Add(colvarVourcherEventType);
                
                TableSchema.TableColumn colvarVourcherStartDate = new TableSchema.TableColumn(schema);
                colvarVourcherStartDate.ColumnName = "vourcher_start_date";
                colvarVourcherStartDate.DataType = DbType.DateTime;
                colvarVourcherStartDate.MaxLength = 0;
                colvarVourcherStartDate.AutoIncrement = false;
                colvarVourcherStartDate.IsNullable = true;
                colvarVourcherStartDate.IsPrimaryKey = false;
                colvarVourcherStartDate.IsForeignKey = false;
                colvarVourcherStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarVourcherStartDate);
                
                TableSchema.TableColumn colvarVourcherEndDate = new TableSchema.TableColumn(schema);
                colvarVourcherEndDate.ColumnName = "vourcher_end_Date";
                colvarVourcherEndDate.DataType = DbType.DateTime;
                colvarVourcherEndDate.MaxLength = 0;
                colvarVourcherEndDate.AutoIncrement = false;
                colvarVourcherEndDate.IsNullable = true;
                colvarVourcherEndDate.IsPrimaryKey = false;
                colvarVourcherEndDate.IsForeignKey = false;
                colvarVourcherEndDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarVourcherEndDate);
                
                TableSchema.TableColumn colvarItemStartDate = new TableSchema.TableColumn(schema);
                colvarItemStartDate.ColumnName = "item_start_date";
                colvarItemStartDate.DataType = DbType.DateTime;
                colvarItemStartDate.MaxLength = 0;
                colvarItemStartDate.AutoIncrement = false;
                colvarItemStartDate.IsNullable = true;
                colvarItemStartDate.IsPrimaryKey = false;
                colvarItemStartDate.IsForeignKey = false;
                colvarItemStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemStartDate);
                
                TableSchema.TableColumn colvarItemEndDate = new TableSchema.TableColumn(schema);
                colvarItemEndDate.ColumnName = "item_end_date";
                colvarItemEndDate.DataType = DbType.DateTime;
                colvarItemEndDate.MaxLength = 0;
                colvarItemEndDate.AutoIncrement = false;
                colvarItemEndDate.IsNullable = true;
                colvarItemEndDate.IsPrimaryKey = false;
                colvarItemEndDate.IsForeignKey = false;
                colvarItemEndDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemEndDate);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_event_promo_vourcher",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewEventPromoVourcher()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEventPromoVourcher(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewEventPromoVourcher(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewEventPromoVourcher(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("Id");
		    }
            set 
		    {
			    SetColumnValue("Id", value);
            }
        }
	      
        [XmlAttribute("Url")]
        [Bindable(true)]
        public string Url 
	    {
		    get
		    {
			    return GetColumnValue<string>("Url");
		    }
            set 
		    {
			    SetColumnValue("Url", value);
            }
        }
	      
        [XmlAttribute("PromoStartDate")]
        [Bindable(true)]
        public DateTime PromoStartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("promo_start_date");
		    }
            set 
		    {
			    SetColumnValue("promo_start_date", value);
            }
        }
	      
        [XmlAttribute("PromoEndDate")]
        [Bindable(true)]
        public DateTime PromoEndDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("promo_end_date");
		    }
            set 
		    {
			    SetColumnValue("promo_end_date", value);
            }
        }
	      
        [XmlAttribute("PromoType")]
        [Bindable(true)]
        public int PromoType 
	    {
		    get
		    {
			    return GetColumnValue<int>("promo_type");
		    }
            set 
		    {
			    SetColumnValue("promo_type", value);
            }
        }
	      
        [XmlAttribute("PromoStatus")]
        [Bindable(true)]
        public bool PromoStatus 
	    {
		    get
		    {
			    return GetColumnValue<bool>("promo_status");
		    }
            set 
		    {
			    SetColumnValue("promo_status", value);
            }
        }
	      
        [XmlAttribute("TemplateType")]
        [Bindable(true)]
        public int TemplateType 
	    {
		    get
		    {
			    return GetColumnValue<int>("template_type");
		    }
            set 
		    {
			    SetColumnValue("template_type", value);
            }
        }
	      
        [XmlAttribute("ParentId")]
        [Bindable(true)]
        public int? ParentId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("parent_id");
		    }
            set 
		    {
			    SetColumnValue("parent_id", value);
            }
        }
	      
        [XmlAttribute("ShowInApp")]
        [Bindable(true)]
        public bool ShowInApp 
	    {
		    get
		    {
			    return GetColumnValue<bool>("show_in_app");
		    }
            set 
		    {
			    SetColumnValue("show_in_app", value);
            }
        }
	      
        [XmlAttribute("ShowInIdeas")]
        [Bindable(true)]
        public bool ShowInIdeas 
	    {
		    get
		    {
			    return GetColumnValue<bool>("show_in_ideas");
		    }
            set 
		    {
			    SetColumnValue("show_in_ideas", value);
            }
        }
	      
        [XmlAttribute("ShowInWeb")]
        [Bindable(true)]
        public bool ShowInWeb 
	    {
		    get
		    {
			    return GetColumnValue<bool>("show_in_web");
		    }
            set 
		    {
			    SetColumnValue("show_in_web", value);
            }
        }
	      
        [XmlAttribute("EventPromoItemId")]
        [Bindable(true)]
        public int EventPromoItemId 
	    {
		    get
		    {
			    return GetColumnValue<int>("event_promo_item_id");
		    }
            set 
		    {
			    SetColumnValue("event_promo_item_id", value);
            }
        }
	      
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("Title");
		    }
            set 
		    {
			    SetColumnValue("Title", value);
            }
        }
	      
        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description 
	    {
		    get
		    {
			    return GetColumnValue<string>("Description");
		    }
            set 
		    {
			    SetColumnValue("Description", value);
            }
        }
	      
        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int Seq 
	    {
		    get
		    {
			    return GetColumnValue<int>("Seq");
		    }
            set 
		    {
			    SetColumnValue("Seq", value);
            }
        }
	      
        [XmlAttribute("PromoItemStatus")]
        [Bindable(true)]
        public bool PromoItemStatus 
	    {
		    get
		    {
			    return GetColumnValue<bool>("promo_item_status");
		    }
            set 
		    {
			    SetColumnValue("promo_item_status", value);
            }
        }
	      
        [XmlAttribute("VourcherEventId")]
        [Bindable(true)]
        public int VourcherEventId 
	    {
		    get
		    {
			    return GetColumnValue<int>("vourcher_event_id");
		    }
            set 
		    {
			    SetColumnValue("vourcher_event_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("VourcherEventStatus")]
        [Bindable(true)]
        public int VourcherEventStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("vourcher_event_status");
		    }
            set 
		    {
			    SetColumnValue("vourcher_event_status", value);
            }
        }
	      
        [XmlAttribute("VourcherEventType")]
        [Bindable(true)]
        public int VourcherEventType 
	    {
		    get
		    {
			    return GetColumnValue<int>("vourcher_event_type");
		    }
            set 
		    {
			    SetColumnValue("vourcher_event_type", value);
            }
        }
	      
        [XmlAttribute("VourcherStartDate")]
        [Bindable(true)]
        public DateTime? VourcherStartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("vourcher_start_date");
		    }
            set 
		    {
			    SetColumnValue("vourcher_start_date", value);
            }
        }
	      
        [XmlAttribute("VourcherEndDate")]
        [Bindable(true)]
        public DateTime? VourcherEndDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("vourcher_end_Date");
		    }
            set 
		    {
			    SetColumnValue("vourcher_end_Date", value);
            }
        }
	      
        [XmlAttribute("ItemStartDate")]
        [Bindable(true)]
        public DateTime? ItemStartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("item_start_date");
		    }
            set 
		    {
			    SetColumnValue("item_start_date", value);
            }
        }
	      
        [XmlAttribute("ItemEndDate")]
        [Bindable(true)]
        public DateTime? ItemEndDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("item_end_date");
		    }
            set 
		    {
			    SetColumnValue("item_end_date", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"Id";
            
            public static string Url = @"Url";
            
            public static string PromoStartDate = @"promo_start_date";
            
            public static string PromoEndDate = @"promo_end_date";
            
            public static string PromoType = @"promo_type";
            
            public static string PromoStatus = @"promo_status";
            
            public static string TemplateType = @"template_type";
            
            public static string ParentId = @"parent_id";
            
            public static string ShowInApp = @"show_in_app";
            
            public static string ShowInIdeas = @"show_in_ideas";
            
            public static string ShowInWeb = @"show_in_web";
            
            public static string EventPromoItemId = @"event_promo_item_id";
            
            public static string Title = @"Title";
            
            public static string Description = @"Description";
            
            public static string Seq = @"Seq";
            
            public static string PromoItemStatus = @"promo_item_status";
            
            public static string VourcherEventId = @"vourcher_event_id";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string VourcherEventStatus = @"vourcher_event_status";
            
            public static string VourcherEventType = @"vourcher_event_type";
            
            public static string VourcherStartDate = @"vourcher_start_date";
            
            public static string VourcherEndDate = @"vourcher_end_Date";
            
            public static string ItemStartDate = @"item_start_date";
            
            public static string ItemEndDate = @"item_end_date";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

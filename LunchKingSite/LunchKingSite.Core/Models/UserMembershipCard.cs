using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the UserMembershipCard class.
	/// </summary>
    [Serializable]
	public partial class UserMembershipCardCollection : RepositoryList<UserMembershipCard, UserMembershipCardCollection>
	{	   
		public UserMembershipCardCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>UserMembershipCardCollection</returns>
		public UserMembershipCardCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                UserMembershipCard o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the user_membership_card table.
	/// </summary>
	
	[Serializable]
	public partial class UserMembershipCard : RepositoryRecord<UserMembershipCard>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public UserMembershipCard()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public UserMembershipCard(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("user_membership_card", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
				colvarCardGroupId.ColumnName = "card_group_id";
				colvarCardGroupId.DataType = DbType.Int32;
				colvarCardGroupId.MaxLength = 0;
				colvarCardGroupId.AutoIncrement = false;
				colvarCardGroupId.IsNullable = false;
				colvarCardGroupId.IsPrimaryKey = false;
				colvarCardGroupId.IsForeignKey = false;
				colvarCardGroupId.IsReadOnly = false;
				colvarCardGroupId.DefaultSetting = @"";
				colvarCardGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardGroupId);
				
				TableSchema.TableColumn colvarCardId = new TableSchema.TableColumn(schema);
				colvarCardId.ColumnName = "card_id";
				colvarCardId.DataType = DbType.Int32;
				colvarCardId.MaxLength = 0;
				colvarCardId.AutoIncrement = false;
				colvarCardId.IsNullable = false;
				colvarCardId.IsPrimaryKey = false;
				colvarCardId.IsForeignKey = false;
				colvarCardId.IsReadOnly = false;
				colvarCardId.DefaultSetting = @"";
				colvarCardId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardId);
				
				TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
				colvarOrderCount.ColumnName = "order_count";
				colvarOrderCount.DataType = DbType.Int32;
				colvarOrderCount.MaxLength = 0;
				colvarOrderCount.AutoIncrement = false;
				colvarOrderCount.IsNullable = false;
				colvarOrderCount.IsPrimaryKey = false;
				colvarOrderCount.IsForeignKey = false;
				colvarOrderCount.IsReadOnly = false;
				
						colvarOrderCount.DefaultSetting = @"((0))";
				colvarOrderCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderCount);
				
				TableSchema.TableColumn colvarAmountTotal = new TableSchema.TableColumn(schema);
				colvarAmountTotal.ColumnName = "amount_total";
				colvarAmountTotal.DataType = DbType.Currency;
				colvarAmountTotal.MaxLength = 0;
				colvarAmountTotal.AutoIncrement = false;
				colvarAmountTotal.IsNullable = false;
				colvarAmountTotal.IsPrimaryKey = false;
				colvarAmountTotal.IsForeignKey = false;
				colvarAmountTotal.IsReadOnly = false;
				
						colvarAmountTotal.DefaultSetting = @"((0))";
				colvarAmountTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmountTotal);
				
				TableSchema.TableColumn colvarLastUseTime = new TableSchema.TableColumn(schema);
				colvarLastUseTime.ColumnName = "last_use_time";
				colvarLastUseTime.DataType = DbType.DateTime;
				colvarLastUseTime.MaxLength = 0;
				colvarLastUseTime.AutoIncrement = false;
				colvarLastUseTime.IsNullable = true;
				colvarLastUseTime.IsPrimaryKey = false;
				colvarLastUseTime.IsForeignKey = false;
				colvarLastUseTime.IsReadOnly = false;
				colvarLastUseTime.DefaultSetting = @"";
				colvarLastUseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastUseTime);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = false;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				
						colvarEnabled.DefaultSetting = @"((1))";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
				colvarStartTime.ColumnName = "start_time";
				colvarStartTime.DataType = DbType.DateTime;
				colvarStartTime.MaxLength = 0;
				colvarStartTime.AutoIncrement = false;
				colvarStartTime.IsNullable = false;
				colvarStartTime.IsPrimaryKey = false;
				colvarStartTime.IsForeignKey = false;
				colvarStartTime.IsReadOnly = false;
				colvarStartTime.DefaultSetting = @"";
				colvarStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartTime);
				
				TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
				colvarEndTime.ColumnName = "end_time";
				colvarEndTime.DataType = DbType.DateTime;
				colvarEndTime.MaxLength = 0;
				colvarEndTime.AutoIncrement = false;
				colvarEndTime.IsNullable = false;
				colvarEndTime.IsPrimaryKey = false;
				colvarEndTime.IsForeignKey = false;
				colvarEndTime.IsReadOnly = false;
				colvarEndTime.DefaultSetting = @"";
				colvarEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndTime);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.Int32;
				colvarModifyId.MaxLength = 0;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarRequestTime = new TableSchema.TableColumn(schema);
				colvarRequestTime.ColumnName = "request_time";
				colvarRequestTime.DataType = DbType.DateTime;
				colvarRequestTime.MaxLength = 0;
				colvarRequestTime.AutoIncrement = false;
				colvarRequestTime.IsNullable = false;
				colvarRequestTime.IsPrimaryKey = false;
				colvarRequestTime.IsForeignKey = false;
				colvarRequestTime.IsReadOnly = false;
				
						colvarRequestTime.DefaultSetting = @"(getdate())";
				colvarRequestTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRequestTime);
				
				TableSchema.TableColumn colvarCardNo = new TableSchema.TableColumn(schema);
				colvarCardNo.ColumnName = "card_no";
				colvarCardNo.DataType = DbType.AnsiString;
				colvarCardNo.MaxLength = 15;
				colvarCardNo.AutoIncrement = false;
				colvarCardNo.IsNullable = false;
				colvarCardNo.IsPrimaryKey = false;
				colvarCardNo.IsForeignKey = false;
				colvarCardNo.IsReadOnly = false;
				
						colvarCardNo.DefaultSetting = @"('')";
				colvarCardNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardNo);
				
				TableSchema.TableColumn colvarUserSeq = new TableSchema.TableColumn(schema);
				colvarUserSeq.ColumnName = "user_seq";
				colvarUserSeq.DataType = DbType.Int32;
				colvarUserSeq.MaxLength = 0;
				colvarUserSeq.AutoIncrement = false;
				colvarUserSeq.IsNullable = false;
				colvarUserSeq.IsPrimaryKey = false;
				colvarUserSeq.IsForeignKey = false;
				colvarUserSeq.IsReadOnly = false;
				
						colvarUserSeq.DefaultSetting = @"((0))";
				colvarUserSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserSeq);
				
				TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
				colvarRemark.ColumnName = "remark";
				colvarRemark.DataType = DbType.String;
				colvarRemark.MaxLength = 256;
				colvarRemark.AutoIncrement = false;
				colvarRemark.IsNullable = true;
				colvarRemark.IsPrimaryKey = false;
				colvarRemark.IsForeignKey = false;
				colvarRemark.IsReadOnly = false;
				colvarRemark.DefaultSetting = @"";
				colvarRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemark);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("user_membership_card",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		[XmlAttribute("CardGroupId")]
		[Bindable(true)]
		public int CardGroupId 
		{
			get { return GetColumnValue<int>(Columns.CardGroupId); }
			set { SetColumnValue(Columns.CardGroupId, value); }
		}
		
		[XmlAttribute("CardId")]
		[Bindable(true)]
		public int CardId 
		{
			get { return GetColumnValue<int>(Columns.CardId); }
			set { SetColumnValue(Columns.CardId, value); }
		}
		
		[XmlAttribute("OrderCount")]
		[Bindable(true)]
		public int OrderCount 
		{
			get { return GetColumnValue<int>(Columns.OrderCount); }
			set { SetColumnValue(Columns.OrderCount, value); }
		}
		
		[XmlAttribute("AmountTotal")]
		[Bindable(true)]
		public decimal AmountTotal 
		{
			get { return GetColumnValue<decimal>(Columns.AmountTotal); }
			set { SetColumnValue(Columns.AmountTotal, value); }
		}
		
		[XmlAttribute("LastUseTime")]
		[Bindable(true)]
		public DateTime? LastUseTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastUseTime); }
			set { SetColumnValue(Columns.LastUseTime, value); }
		}
		
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool Enabled 
		{
			get { return GetColumnValue<bool>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		
		[XmlAttribute("StartTime")]
		[Bindable(true)]
		public DateTime StartTime 
		{
			get { return GetColumnValue<DateTime>(Columns.StartTime); }
			set { SetColumnValue(Columns.StartTime, value); }
		}
		
		[XmlAttribute("EndTime")]
		[Bindable(true)]
		public DateTime EndTime 
		{
			get { return GetColumnValue<DateTime>(Columns.EndTime); }
			set { SetColumnValue(Columns.EndTime, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public int? ModifyId 
		{
			get { return GetColumnValue<int?>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		
		[XmlAttribute("RequestTime")]
		[Bindable(true)]
		public DateTime RequestTime 
		{
			get { return GetColumnValue<DateTime>(Columns.RequestTime); }
			set { SetColumnValue(Columns.RequestTime, value); }
		}
		
		[XmlAttribute("CardNo")]
		[Bindable(true)]
		public string CardNo 
		{
			get { return GetColumnValue<string>(Columns.CardNo); }
			set { SetColumnValue(Columns.CardNo, value); }
		}
		
		[XmlAttribute("UserSeq")]
		[Bindable(true)]
		public int UserSeq 
		{
			get { return GetColumnValue<int>(Columns.UserSeq); }
			set { SetColumnValue(Columns.UserSeq, value); }
		}
		
		[XmlAttribute("Remark")]
		[Bindable(true)]
		public string Remark 
		{
			get { return GetColumnValue<string>(Columns.Remark); }
			set { SetColumnValue(Columns.Remark, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CardGroupIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CardIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderCountColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountTotalColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn LastUseTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn StartTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn EndTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn RequestTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CardNoColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn UserSeqColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string UserId = @"user_id";
			 public static string CardGroupId = @"card_group_id";
			 public static string CardId = @"card_id";
			 public static string OrderCount = @"order_count";
			 public static string AmountTotal = @"amount_total";
			 public static string LastUseTime = @"last_use_time";
			 public static string Enabled = @"enabled";
			 public static string StartTime = @"start_time";
			 public static string EndTime = @"end_time";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
			 public static string ModifyTime = @"modify_time";
			 public static string ModifyId = @"modify_id";
			 public static string RequestTime = @"request_time";
			 public static string CardNo = @"card_no";
			 public static string UserSeq = @"user_seq";
			 public static string Remark = @"remark";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

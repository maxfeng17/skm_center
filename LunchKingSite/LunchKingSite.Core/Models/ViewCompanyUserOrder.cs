using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewCompanyUserOrder class.
    /// </summary>
    [Serializable]
    public partial class ViewCompanyUserOrderCollection : ReadOnlyList<ViewCompanyUserOrder, ViewCompanyUserOrderCollection>
    {        
        public ViewCompanyUserOrderCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_company_user_order view.
    /// </summary>
    [Serializable]
    public partial class ViewCompanyUserOrder : ReadOnlyRecord<ViewCompanyUserOrder>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_company_user_order", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCompanyUserId = new TableSchema.TableColumn(schema);
                colvarCompanyUserId.ColumnName = "company_user_id";
                colvarCompanyUserId.DataType = DbType.Int32;
                colvarCompanyUserId.MaxLength = 0;
                colvarCompanyUserId.AutoIncrement = false;
                colvarCompanyUserId.IsNullable = false;
                colvarCompanyUserId.IsPrimaryKey = false;
                colvarCompanyUserId.IsForeignKey = false;
                colvarCompanyUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyUserId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarPurchaserEmail = new TableSchema.TableColumn(schema);
                colvarPurchaserEmail.ColumnName = "purchaser_email";
                colvarPurchaserEmail.DataType = DbType.String;
                colvarPurchaserEmail.MaxLength = 255;
                colvarPurchaserEmail.AutoIncrement = false;
                colvarPurchaserEmail.IsNullable = true;
                colvarPurchaserEmail.IsPrimaryKey = false;
                colvarPurchaserEmail.IsForeignKey = false;
                colvarPurchaserEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarPurchaserEmail);
                
                TableSchema.TableColumn colvarPurchaserName = new TableSchema.TableColumn(schema);
                colvarPurchaserName.ColumnName = "purchaser_name";
                colvarPurchaserName.DataType = DbType.String;
                colvarPurchaserName.MaxLength = 255;
                colvarPurchaserName.AutoIncrement = false;
                colvarPurchaserName.IsNullable = true;
                colvarPurchaserName.IsPrimaryKey = false;
                colvarPurchaserName.IsForeignKey = false;
                colvarPurchaserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarPurchaserName);
                
                TableSchema.TableColumn colvarPurchaserPhone = new TableSchema.TableColumn(schema);
                colvarPurchaserPhone.ColumnName = "purchaser_phone";
                colvarPurchaserPhone.DataType = DbType.String;
                colvarPurchaserPhone.MaxLength = 20;
                colvarPurchaserPhone.AutoIncrement = false;
                colvarPurchaserPhone.IsNullable = true;
                colvarPurchaserPhone.IsPrimaryKey = false;
                colvarPurchaserPhone.IsForeignKey = false;
                colvarPurchaserPhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarPurchaserPhone);
                
                TableSchema.TableColumn colvarPurchaserAddress = new TableSchema.TableColumn(schema);
                colvarPurchaserAddress.ColumnName = "purchaser_address";
                colvarPurchaserAddress.DataType = DbType.String;
                colvarPurchaserAddress.MaxLength = 100;
                colvarPurchaserAddress.AutoIncrement = false;
                colvarPurchaserAddress.IsNullable = true;
                colvarPurchaserAddress.IsPrimaryKey = false;
                colvarPurchaserAddress.IsForeignKey = false;
                colvarPurchaserAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarPurchaserAddress);
                
                TableSchema.TableColumn colvarWaitUpdate = new TableSchema.TableColumn(schema);
                colvarWaitUpdate.ColumnName = "wait_update";
                colvarWaitUpdate.DataType = DbType.Boolean;
                colvarWaitUpdate.MaxLength = 0;
                colvarWaitUpdate.AutoIncrement = false;
                colvarWaitUpdate.IsNullable = false;
                colvarWaitUpdate.IsPrimaryKey = false;
                colvarWaitUpdate.IsForeignKey = false;
                colvarWaitUpdate.IsReadOnly = false;
                
                schema.Columns.Add(colvarWaitUpdate);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = true;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
                colvarSubtotal.ColumnName = "subtotal";
                colvarSubtotal.DataType = DbType.Currency;
                colvarSubtotal.MaxLength = 0;
                colvarSubtotal.AutoIncrement = false;
                colvarSubtotal.IsNullable = true;
                colvarSubtotal.IsPrimaryKey = false;
                colvarSubtotal.IsForeignKey = false;
                colvarSubtotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubtotal);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = true;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = true;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarTransId = new TableSchema.TableColumn(schema);
                colvarTransId.ColumnName = "trans_id";
                colvarTransId.DataType = DbType.AnsiString;
                colvarTransId.MaxLength = 40;
                colvarTransId.AutoIncrement = false;
                colvarTransId.IsNullable = true;
                colvarTransId.IsPrimaryKey = false;
                colvarTransId.IsForeignKey = false;
                colvarTransId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTransId);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = true;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarLogId = new TableSchema.TableColumn(schema);
                colvarLogId.ColumnName = "log_id";
                colvarLogId.DataType = DbType.Int32;
                colvarLogId.MaxLength = 0;
                colvarLogId.AutoIncrement = false;
                colvarLogId.IsNullable = true;
                colvarLogId.IsPrimaryKey = false;
                colvarLogId.IsForeignKey = false;
                colvarLogId.IsReadOnly = false;
                
                schema.Columns.Add(colvarLogId);
                
                TableSchema.TableColumn colvarLogStatus = new TableSchema.TableColumn(schema);
                colvarLogStatus.ColumnName = "log_status";
                colvarLogStatus.DataType = DbType.Int32;
                colvarLogStatus.MaxLength = 0;
                colvarLogStatus.AutoIncrement = false;
                colvarLogStatus.IsNullable = true;
                colvarLogStatus.IsPrimaryKey = false;
                colvarLogStatus.IsForeignKey = false;
                colvarLogStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarLogStatus);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                TableSchema.TableColumn colvarBhChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarBhChangedExpireDate.ColumnName = "bh_changed_expire_date";
                colvarBhChangedExpireDate.DataType = DbType.DateTime;
                colvarBhChangedExpireDate.MaxLength = 0;
                colvarBhChangedExpireDate.AutoIncrement = false;
                colvarBhChangedExpireDate.IsNullable = true;
                colvarBhChangedExpireDate.IsPrimaryKey = false;
                colvarBhChangedExpireDate.IsForeignKey = false;
                colvarBhChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarBhChangedExpireDate);
                
                TableSchema.TableColumn colvarStoreChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarStoreChangedExpireDate.ColumnName = "store_changed_expire_date";
                colvarStoreChangedExpireDate.DataType = DbType.DateTime;
                colvarStoreChangedExpireDate.MaxLength = 0;
                colvarStoreChangedExpireDate.AutoIncrement = false;
                colvarStoreChangedExpireDate.IsNullable = true;
                colvarStoreChangedExpireDate.IsPrimaryKey = false;
                colvarStoreChangedExpireDate.IsForeignKey = false;
                colvarStoreChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreChangedExpireDate);
                
                TableSchema.TableColumn colvarSellerCloseDownDate = new TableSchema.TableColumn(schema);
                colvarSellerCloseDownDate.ColumnName = "seller_close_down_date";
                colvarSellerCloseDownDate.DataType = DbType.DateTime;
                colvarSellerCloseDownDate.MaxLength = 0;
                colvarSellerCloseDownDate.AutoIncrement = false;
                colvarSellerCloseDownDate.IsNullable = true;
                colvarSellerCloseDownDate.IsPrimaryKey = false;
                colvarSellerCloseDownDate.IsForeignKey = false;
                colvarSellerCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCloseDownDate);
                
                TableSchema.TableColumn colvarStoreCloseDownDate = new TableSchema.TableColumn(schema);
                colvarStoreCloseDownDate.ColumnName = "store_close_down_date";
                colvarStoreCloseDownDate.DataType = DbType.DateTime;
                colvarStoreCloseDownDate.MaxLength = 0;
                colvarStoreCloseDownDate.AutoIncrement = false;
                colvarStoreCloseDownDate.IsNullable = true;
                colvarStoreCloseDownDate.IsPrimaryKey = false;
                colvarStoreCloseDownDate.IsForeignKey = false;
                colvarStoreCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreCloseDownDate);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_company_user_order",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewCompanyUserOrder()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewCompanyUserOrder(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewCompanyUserOrder(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewCompanyUserOrder(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CompanyUserId")]
        [Bindable(true)]
        public int CompanyUserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("company_user_id");
		    }
            set 
		    {
			    SetColumnValue("company_user_id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("PurchaserEmail")]
        [Bindable(true)]
        public string PurchaserEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("purchaser_email");
		    }
            set 
		    {
			    SetColumnValue("purchaser_email", value);
            }
        }
	      
        [XmlAttribute("PurchaserName")]
        [Bindable(true)]
        public string PurchaserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("purchaser_name");
		    }
            set 
		    {
			    SetColumnValue("purchaser_name", value);
            }
        }
	      
        [XmlAttribute("PurchaserPhone")]
        [Bindable(true)]
        public string PurchaserPhone 
	    {
		    get
		    {
			    return GetColumnValue<string>("purchaser_phone");
		    }
            set 
		    {
			    SetColumnValue("purchaser_phone", value);
            }
        }
	      
        [XmlAttribute("PurchaserAddress")]
        [Bindable(true)]
        public string PurchaserAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("purchaser_address");
		    }
            set 
		    {
			    SetColumnValue("purchaser_address", value);
            }
        }
	      
        [XmlAttribute("WaitUpdate")]
        [Bindable(true)]
        public bool WaitUpdate 
	    {
		    get
		    {
			    return GetColumnValue<bool>("wait_update");
		    }
            set 
		    {
			    SetColumnValue("wait_update", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid? SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("Subtotal")]
        [Bindable(true)]
        public decimal? Subtotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("subtotal");
		    }
            set 
		    {
			    SetColumnValue("subtotal", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("TransId")]
        [Bindable(true)]
        public string TransId 
	    {
		    get
		    {
			    return GetColumnValue<string>("trans_id");
		    }
            set 
		    {
			    SetColumnValue("trans_id", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int? OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("LogId")]
        [Bindable(true)]
        public int? LogId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("log_id");
		    }
            set 
		    {
			    SetColumnValue("log_id", value);
            }
        }
	      
        [XmlAttribute("LogStatus")]
        [Bindable(true)]
        public int? LogStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("log_status");
		    }
            set 
		    {
			    SetColumnValue("log_status", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("BhChangedExpireDate")]
        [Bindable(true)]
        public DateTime? BhChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bh_changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("bh_changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("StoreChangedExpireDate")]
        [Bindable(true)]
        public DateTime? StoreChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("store_changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("store_changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("SellerCloseDownDate")]
        [Bindable(true)]
        public DateTime? SellerCloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("seller_close_down_date");
		    }
            set 
		    {
			    SetColumnValue("seller_close_down_date", value);
            }
        }
	      
        [XmlAttribute("StoreCloseDownDate")]
        [Bindable(true)]
        public DateTime? StoreCloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("store_close_down_date");
		    }
            set 
		    {
			    SetColumnValue("store_close_down_date", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CompanyUserId = @"company_user_id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string PurchaserEmail = @"purchaser_email";
            
            public static string PurchaserName = @"purchaser_name";
            
            public static string PurchaserPhone = @"purchaser_phone";
            
            public static string PurchaserAddress = @"purchaser_address";
            
            public static string WaitUpdate = @"wait_update";
            
            public static string OrderId = @"order_id";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string CreateTime = @"create_time";
            
            public static string Subtotal = @"subtotal";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string TransId = @"trans_id";
            
            public static string OrderStatus = @"order_status";
            
            public static string LogId = @"log_id";
            
            public static string LogStatus = @"log_status";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
            public static string BhChangedExpireDate = @"bh_changed_expire_date";
            
            public static string StoreChangedExpireDate = @"store_changed_expire_date";
            
            public static string SellerCloseDownDate = @"seller_close_down_date";
            
            public static string StoreCloseDownDate = @"store_close_down_date";
            
            public static string StoreName = @"store_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

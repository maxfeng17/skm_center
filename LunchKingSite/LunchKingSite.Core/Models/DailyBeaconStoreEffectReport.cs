using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DailyBeaconStoreEffectReport class.
	/// </summary>
    [Serializable]
	public partial class DailyBeaconStoreEffectReportCollection : RepositoryList<DailyBeaconStoreEffectReport, DailyBeaconStoreEffectReportCollection>
	{	   
		public DailyBeaconStoreEffectReportCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DailyBeaconStoreEffectReportCollection</returns>
		public DailyBeaconStoreEffectReportCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DailyBeaconStoreEffectReport o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the daily_beacon_store_effect_report table.
	/// </summary>
	[Serializable]
	public partial class DailyBeaconStoreEffectReport : RepositoryRecord<DailyBeaconStoreEffectReport>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DailyBeaconStoreEffectReport()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DailyBeaconStoreEffectReport(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("daily_beacon_store_effect_report", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarStoreTenCode = new TableSchema.TableColumn(schema);
				colvarStoreTenCode.ColumnName = "store_ten_code";
				colvarStoreTenCode.DataType = DbType.AnsiString;
				colvarStoreTenCode.MaxLength = 6;
				colvarStoreTenCode.AutoIncrement = false;
				colvarStoreTenCode.IsNullable = true;
				colvarStoreTenCode.IsPrimaryKey = false;
				colvarStoreTenCode.IsForeignKey = false;
				colvarStoreTenCode.IsReadOnly = false;
				colvarStoreTenCode.DefaultSetting = @"";
				colvarStoreTenCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreTenCode);
				
				TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
				colvarStoreName.ColumnName = "store_name";
				colvarStoreName.DataType = DbType.String;
				colvarStoreName.MaxLength = 15;
				colvarStoreName.AutoIncrement = false;
				colvarStoreName.IsNullable = true;
				colvarStoreName.IsPrimaryKey = false;
				colvarStoreName.IsForeignKey = false;
				colvarStoreName.IsReadOnly = false;
				colvarStoreName.DefaultSetting = @"";
				colvarStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreName);
				
				TableSchema.TableColumn colvarReportStartDate = new TableSchema.TableColumn(schema);
				colvarReportStartDate.ColumnName = "report_start_date";
				colvarReportStartDate.DataType = DbType.DateTime;
				colvarReportStartDate.MaxLength = 0;
				colvarReportStartDate.AutoIncrement = false;
				colvarReportStartDate.IsNullable = false;
				colvarReportStartDate.IsPrimaryKey = false;
				colvarReportStartDate.IsForeignKey = false;
				colvarReportStartDate.IsReadOnly = false;
				colvarReportStartDate.DefaultSetting = @"";
				colvarReportStartDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReportStartDate);
				
				TableSchema.TableColumn colvarReportEndDate = new TableSchema.TableColumn(schema);
				colvarReportEndDate.ColumnName = "report_end_date";
				colvarReportEndDate.DataType = DbType.DateTime;
				colvarReportEndDate.MaxLength = 0;
				colvarReportEndDate.AutoIncrement = false;
				colvarReportEndDate.IsNullable = false;
				colvarReportEndDate.IsPrimaryKey = false;
				colvarReportEndDate.IsForeignKey = false;
				colvarReportEndDate.IsReadOnly = false;
				colvarReportEndDate.DefaultSetting = @"";
				colvarReportEndDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReportEndDate);
				
				TableSchema.TableColumn colvarBeaconTriggerAmount = new TableSchema.TableColumn(schema);
				colvarBeaconTriggerAmount.ColumnName = "beacon_trigger_amount";
				colvarBeaconTriggerAmount.DataType = DbType.Int32;
				colvarBeaconTriggerAmount.MaxLength = 0;
				colvarBeaconTriggerAmount.AutoIncrement = false;
				colvarBeaconTriggerAmount.IsNullable = false;
				colvarBeaconTriggerAmount.IsPrimaryKey = false;
				colvarBeaconTriggerAmount.IsForeignKey = false;
				colvarBeaconTriggerAmount.IsReadOnly = false;
				
						colvarBeaconTriggerAmount.DefaultSetting = @"((0))";
				colvarBeaconTriggerAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBeaconTriggerAmount);
				
				TableSchema.TableColumn colvarSoonTriggerAmount = new TableSchema.TableColumn(schema);
				colvarSoonTriggerAmount.ColumnName = "soon_trigger_amount";
				colvarSoonTriggerAmount.DataType = DbType.Int32;
				colvarSoonTriggerAmount.MaxLength = 0;
				colvarSoonTriggerAmount.AutoIncrement = false;
				colvarSoonTriggerAmount.IsNullable = false;
				colvarSoonTriggerAmount.IsPrimaryKey = false;
				colvarSoonTriggerAmount.IsForeignKey = false;
				colvarSoonTriggerAmount.IsReadOnly = false;
				
						colvarSoonTriggerAmount.DefaultSetting = @"((0))";
				colvarSoonTriggerAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSoonTriggerAmount);
				
				TableSchema.TableColumn colvarPayAmount = new TableSchema.TableColumn(schema);
				colvarPayAmount.ColumnName = "pay_amount";
				colvarPayAmount.DataType = DbType.Int32;
				colvarPayAmount.MaxLength = 0;
				colvarPayAmount.AutoIncrement = false;
				colvarPayAmount.IsNullable = false;
				colvarPayAmount.IsPrimaryKey = false;
				colvarPayAmount.IsForeignKey = false;
				colvarPayAmount.IsReadOnly = false;
				
						colvarPayAmount.DefaultSetting = @"((0))";
				colvarPayAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayAmount);
				
				TableSchema.TableColumn colvarItemCount = new TableSchema.TableColumn(schema);
				colvarItemCount.ColumnName = "item_count";
				colvarItemCount.DataType = DbType.Int32;
				colvarItemCount.MaxLength = 0;
				colvarItemCount.AutoIncrement = false;
				colvarItemCount.IsNullable = false;
				colvarItemCount.IsPrimaryKey = false;
				colvarItemCount.IsForeignKey = false;
				colvarItemCount.IsReadOnly = false;
				
						colvarItemCount.DefaultSetting = @"((0))";
				colvarItemCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemCount);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("daily_beacon_store_effect_report",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("StoreTenCode")]
		[Bindable(true)]
		public string StoreTenCode 
		{
			get { return GetColumnValue<string>(Columns.StoreTenCode); }
			set { SetColumnValue(Columns.StoreTenCode, value); }
		}
		  
		[XmlAttribute("StoreName")]
		[Bindable(true)]
		public string StoreName 
		{
			get { return GetColumnValue<string>(Columns.StoreName); }
			set { SetColumnValue(Columns.StoreName, value); }
		}
		  
		[XmlAttribute("ReportStartDate")]
		[Bindable(true)]
		public DateTime ReportStartDate 
		{
			get { return GetColumnValue<DateTime>(Columns.ReportStartDate); }
			set { SetColumnValue(Columns.ReportStartDate, value); }
		}
		  
		[XmlAttribute("ReportEndDate")]
		[Bindable(true)]
		public DateTime ReportEndDate 
		{
			get { return GetColumnValue<DateTime>(Columns.ReportEndDate); }
			set { SetColumnValue(Columns.ReportEndDate, value); }
		}
		  
		[XmlAttribute("BeaconTriggerAmount")]
		[Bindable(true)]
		public int BeaconTriggerAmount 
		{
			get { return GetColumnValue<int>(Columns.BeaconTriggerAmount); }
			set { SetColumnValue(Columns.BeaconTriggerAmount, value); }
		}
		  
		[XmlAttribute("SoonTriggerAmount")]
		[Bindable(true)]
		public int SoonTriggerAmount 
		{
			get { return GetColumnValue<int>(Columns.SoonTriggerAmount); }
			set { SetColumnValue(Columns.SoonTriggerAmount, value); }
		}
		  
		[XmlAttribute("PayAmount")]
		[Bindable(true)]
		public int PayAmount 
		{
			get { return GetColumnValue<int>(Columns.PayAmount); }
			set { SetColumnValue(Columns.PayAmount, value); }
		}
		  
		[XmlAttribute("ItemCount")]
		[Bindable(true)]
		public int ItemCount 
		{
			get { return GetColumnValue<int>(Columns.ItemCount); }
			set { SetColumnValue(Columns.ItemCount, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreTenCodeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ReportStartDateColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ReportEndDateColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn BeaconTriggerAmountColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn SoonTriggerAmountColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn PayAmountColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemCountColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string StoreTenCode = @"store_ten_code";
			 public static string StoreName = @"store_name";
			 public static string ReportStartDate = @"report_start_date";
			 public static string ReportEndDate = @"report_end_date";
			 public static string BeaconTriggerAmount = @"beacon_trigger_amount";
			 public static string SoonTriggerAmount = @"soon_trigger_amount";
			 public static string PayAmount = @"pay_amount";
			 public static string ItemCount = @"item_count";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

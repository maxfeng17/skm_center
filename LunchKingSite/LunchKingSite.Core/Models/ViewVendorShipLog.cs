using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVendorShipLog class.
    /// </summary>
    [Serializable]
    public partial class ViewVendorShipLogCollection : ReadOnlyList<ViewVendorShipLog, ViewVendorShipLogCollection>
    {        
        public ViewVendorShipLogCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vendor_ship_log view.
    /// </summary>
    [Serializable]
    public partial class ViewVendorShipLog : ReadOnlyRecord<ViewVendorShipLog>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vendor_ship_log", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarVendorProcessTime = new TableSchema.TableColumn(schema);
                colvarVendorProcessTime.ColumnName = "vendor_process_time";
                colvarVendorProcessTime.DataType = DbType.DateTime;
                colvarVendorProcessTime.MaxLength = 0;
                colvarVendorProcessTime.AutoIncrement = false;
                colvarVendorProcessTime.IsNullable = true;
                colvarVendorProcessTime.IsPrimaryKey = false;
                colvarVendorProcessTime.IsForeignKey = false;
                colvarVendorProcessTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorProcessTime);
                
                TableSchema.TableColumn colvarVendorMemo = new TableSchema.TableColumn(schema);
                colvarVendorMemo.ColumnName = "vendor_memo";
                colvarVendorMemo.DataType = DbType.String;
                colvarVendorMemo.MaxLength = 500;
                colvarVendorMemo.AutoIncrement = false;
                colvarVendorMemo.IsNullable = false;
                colvarVendorMemo.IsPrimaryKey = false;
                colvarVendorMemo.IsForeignKey = false;
                colvarVendorMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorMemo);
                
                TableSchema.TableColumn colvarShipCompanyName = new TableSchema.TableColumn(schema);
                colvarShipCompanyName.ColumnName = "ship_company_name";
                colvarShipCompanyName.DataType = DbType.String;
                colvarShipCompanyName.MaxLength = 100;
                colvarShipCompanyName.AutoIncrement = false;
                colvarShipCompanyName.IsNullable = true;
                colvarShipCompanyName.IsPrimaryKey = false;
                colvarShipCompanyName.IsForeignKey = false;
                colvarShipCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipCompanyName);
                
                TableSchema.TableColumn colvarShipNo = new TableSchema.TableColumn(schema);
                colvarShipNo.ColumnName = "ship_no";
                colvarShipNo.DataType = DbType.String;
                colvarShipNo.MaxLength = -1;
                colvarShipNo.AutoIncrement = false;
                colvarShipNo.IsNullable = true;
                colvarShipNo.IsPrimaryKey = false;
                colvarShipNo.IsForeignKey = false;
                colvarShipNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipNo);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 256;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarVendorProgressStatus = new TableSchema.TableColumn(schema);
                colvarVendorProgressStatus.ColumnName = "vendor_progress_status";
                colvarVendorProgressStatus.DataType = DbType.Int32;
                colvarVendorProgressStatus.MaxLength = 0;
                colvarVendorProgressStatus.AutoIncrement = false;
                colvarVendorProgressStatus.IsNullable = true;
                colvarVendorProgressStatus.IsPrimaryKey = false;
                colvarVendorProgressStatus.IsForeignKey = false;
                colvarVendorProgressStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorProgressStatus);
                
                TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
                colvarShipType.ColumnName = "ship_type";
                colvarShipType.DataType = DbType.Int32;
                colvarShipType.MaxLength = 0;
                colvarShipType.AutoIncrement = false;
                colvarShipType.IsNullable = false;
                colvarShipType.IsPrimaryKey = false;
                colvarShipType.IsForeignKey = false;
                colvarShipType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vendor_ship_log",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVendorShipLog()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVendorShipLog(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVendorShipLog(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVendorShipLog(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("VendorProcessTime")]
        [Bindable(true)]
        public DateTime? VendorProcessTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("vendor_process_time");
		    }
            set 
		    {
			    SetColumnValue("vendor_process_time", value);
            }
        }
	      
        [XmlAttribute("VendorMemo")]
        [Bindable(true)]
        public string VendorMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("vendor_memo");
		    }
            set 
		    {
			    SetColumnValue("vendor_memo", value);
            }
        }
	      
        [XmlAttribute("ShipCompanyName")]
        [Bindable(true)]
        public string ShipCompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("ship_company_name");
		    }
            set 
		    {
			    SetColumnValue("ship_company_name", value);
            }
        }
	      
        [XmlAttribute("ShipNo")]
        [Bindable(true)]
        public string ShipNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("ship_no");
		    }
            set 
		    {
			    SetColumnValue("ship_no", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("VendorProgressStatus")]
        [Bindable(true)]
        public int? VendorProgressStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vendor_progress_status");
		    }
            set 
		    {
			    SetColumnValue("vendor_progress_status", value);
            }
        }
	      
        [XmlAttribute("ShipType")]
        [Bindable(true)]
        public int ShipType 
	    {
		    get
		    {
			    return GetColumnValue<int>("ship_type");
		    }
            set 
		    {
			    SetColumnValue("ship_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderGuid = @"order_guid";
            
            public static string VendorProcessTime = @"vendor_process_time";
            
            public static string VendorMemo = @"vendor_memo";
            
            public static string ShipCompanyName = @"ship_company_name";
            
            public static string ShipNo = @"ship_no";
            
            public static string ModifyId = @"modify_id";
            
            public static string VendorProgressStatus = @"vendor_progress_status";
            
            public static string ShipType = @"ship_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

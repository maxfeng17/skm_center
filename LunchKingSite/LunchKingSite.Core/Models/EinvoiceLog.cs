using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the EinvoiceLog class.
    /// </summary>
    [Serializable]
    public partial class EinvoiceLogCollection : RepositoryList<EinvoiceLog, EinvoiceLogCollection>
    {
        public EinvoiceLogCollection() {}

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EinvoiceLogCollection</returns>
        public EinvoiceLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EinvoiceLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the einvoice_log table.
    /// </summary>
    [Serializable]
    public partial class EinvoiceLog : RepositoryRecord<EinvoiceLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public EinvoiceLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public EinvoiceLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("einvoice_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarDateCode = new TableSchema.TableColumn(schema);
                colvarDateCode.ColumnName = "date_code";
                colvarDateCode.DataType = DbType.AnsiString;
                colvarDateCode.MaxLength = 6;
                colvarDateCode.AutoIncrement = false;
                colvarDateCode.IsNullable = false;
                colvarDateCode.IsPrimaryKey = false;
                colvarDateCode.IsForeignKey = false;
                colvarDateCode.IsReadOnly = false;
                colvarDateCode.DefaultSetting = @"";
                colvarDateCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDateCode);

                TableSchema.TableColumn colvarInvoiceDateStart = new TableSchema.TableColumn(schema);
                colvarInvoiceDateStart.ColumnName = "invoice_date_start";
                colvarInvoiceDateStart.DataType = DbType.DateTime;
                colvarInvoiceDateStart.MaxLength = 0;
                colvarInvoiceDateStart.AutoIncrement = false;
                colvarInvoiceDateStart.IsNullable = true;
                colvarInvoiceDateStart.IsPrimaryKey = false;
                colvarInvoiceDateStart.IsForeignKey = false;
                colvarInvoiceDateStart.IsReadOnly = false;
                colvarInvoiceDateStart.DefaultSetting = @"";
                colvarInvoiceDateStart.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceDateStart);

                TableSchema.TableColumn colvarInvoiceDateEnd = new TableSchema.TableColumn(schema);
                colvarInvoiceDateEnd.ColumnName = "invoice_date_end";
                colvarInvoiceDateEnd.DataType = DbType.DateTime;
                colvarInvoiceDateEnd.MaxLength = 0;
                colvarInvoiceDateEnd.AutoIncrement = false;
                colvarInvoiceDateEnd.IsNullable = true;
                colvarInvoiceDateEnd.IsPrimaryKey = false;
                colvarInvoiceDateEnd.IsForeignKey = false;
                colvarInvoiceDateEnd.IsReadOnly = false;
                colvarInvoiceDateEnd.DefaultSetting = @"";
                colvarInvoiceDateEnd.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceDateEnd);

                TableSchema.TableColumn colvarInvoiceCount = new TableSchema.TableColumn(schema);
                colvarInvoiceCount.ColumnName = "invoice_count";
                colvarInvoiceCount.DataType = DbType.Int32;
                colvarInvoiceCount.MaxLength = 0;
                colvarInvoiceCount.AutoIncrement = false;
                colvarInvoiceCount.IsNullable = true;
                colvarInvoiceCount.IsPrimaryKey = false;
                colvarInvoiceCount.IsForeignKey = false;
                colvarInvoiceCount.IsReadOnly = false;
                colvarInvoiceCount.DefaultSetting = @"";
                colvarInvoiceCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceCount);

                TableSchema.TableColumn colvarInvoiceSum = new TableSchema.TableColumn(schema);
                colvarInvoiceSum.ColumnName = "invoice_sum";
                colvarInvoiceSum.DataType = DbType.Int32;
                colvarInvoiceSum.MaxLength = 0;
                colvarInvoiceSum.AutoIncrement = false;
                colvarInvoiceSum.IsNullable = true;
                colvarInvoiceSum.IsPrimaryKey = false;
                colvarInvoiceSum.IsForeignKey = false;
                colvarInvoiceSum.IsReadOnly = false;
                colvarInvoiceSum.DefaultSetting = @"";
                colvarInvoiceSum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceSum);

                TableSchema.TableColumn colvarReferenceSum = new TableSchema.TableColumn(schema);
                colvarReferenceSum.ColumnName = "reference_sum";
                colvarReferenceSum.DataType = DbType.Int32;
                colvarReferenceSum.MaxLength = 0;
                colvarReferenceSum.AutoIncrement = false;
                colvarReferenceSum.IsNullable = true;
                colvarReferenceSum.IsPrimaryKey = false;
                colvarReferenceSum.IsForeignKey = false;
                colvarReferenceSum.IsReadOnly = false;
                colvarReferenceSum.DefaultSetting = @"";
                colvarReferenceSum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReferenceSum);

                TableSchema.TableColumn colvarInvoiceMode1Total = new TableSchema.TableColumn(schema);
                colvarInvoiceMode1Total.ColumnName = "invoice_mode1_total";
                colvarInvoiceMode1Total.DataType = DbType.Int32;
                colvarInvoiceMode1Total.MaxLength = 0;
                colvarInvoiceMode1Total.AutoIncrement = false;
                colvarInvoiceMode1Total.IsNullable = true;
                colvarInvoiceMode1Total.IsPrimaryKey = false;
                colvarInvoiceMode1Total.IsForeignKey = false;
                colvarInvoiceMode1Total.IsReadOnly = false;
                colvarInvoiceMode1Total.DefaultSetting = @"";
                colvarInvoiceMode1Total.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode1Total);

                TableSchema.TableColumn colvarInvoiceMode1Sum = new TableSchema.TableColumn(schema);
                colvarInvoiceMode1Sum.ColumnName = "invoice_mode1_sum";
                colvarInvoiceMode1Sum.DataType = DbType.Int32;
                colvarInvoiceMode1Sum.MaxLength = 0;
                colvarInvoiceMode1Sum.AutoIncrement = false;
                colvarInvoiceMode1Sum.IsNullable = true;
                colvarInvoiceMode1Sum.IsPrimaryKey = false;
                colvarInvoiceMode1Sum.IsForeignKey = false;
                colvarInvoiceMode1Sum.IsReadOnly = false;
                colvarInvoiceMode1Sum.DefaultSetting = @"";
                colvarInvoiceMode1Sum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode1Sum);

                TableSchema.TableColumn colvarInvoiceMode2Total = new TableSchema.TableColumn(schema);
                colvarInvoiceMode2Total.ColumnName = "invoice_mode2_total";
                colvarInvoiceMode2Total.DataType = DbType.Int32;
                colvarInvoiceMode2Total.MaxLength = 0;
                colvarInvoiceMode2Total.AutoIncrement = false;
                colvarInvoiceMode2Total.IsNullable = true;
                colvarInvoiceMode2Total.IsPrimaryKey = false;
                colvarInvoiceMode2Total.IsForeignKey = false;
                colvarInvoiceMode2Total.IsReadOnly = false;
                colvarInvoiceMode2Total.DefaultSetting = @"";
                colvarInvoiceMode2Total.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode2Total);

                TableSchema.TableColumn colvarInvoiceMode2Sum = new TableSchema.TableColumn(schema);
                colvarInvoiceMode2Sum.ColumnName = "invoice_mode2_sum";
                colvarInvoiceMode2Sum.DataType = DbType.Int32;
                colvarInvoiceMode2Sum.MaxLength = 0;
                colvarInvoiceMode2Sum.AutoIncrement = false;
                colvarInvoiceMode2Sum.IsNullable = true;
                colvarInvoiceMode2Sum.IsPrimaryKey = false;
                colvarInvoiceMode2Sum.IsForeignKey = false;
                colvarInvoiceMode2Sum.IsReadOnly = false;
                colvarInvoiceMode2Sum.DefaultSetting = @"";
                colvarInvoiceMode2Sum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode2Sum);

                TableSchema.TableColumn colvarInvoiceMode17Total = new TableSchema.TableColumn(schema);
                colvarInvoiceMode17Total.ColumnName = "invoice_mode17_total";
                colvarInvoiceMode17Total.DataType = DbType.Int32;
                colvarInvoiceMode17Total.MaxLength = 0;
                colvarInvoiceMode17Total.AutoIncrement = false;
                colvarInvoiceMode17Total.IsNullable = true;
                colvarInvoiceMode17Total.IsPrimaryKey = false;
                colvarInvoiceMode17Total.IsForeignKey = false;
                colvarInvoiceMode17Total.IsReadOnly = false;
                colvarInvoiceMode17Total.DefaultSetting = @"";
                colvarInvoiceMode17Total.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode17Total);

                TableSchema.TableColumn colvarInvoiceMode17Sum = new TableSchema.TableColumn(schema);
                colvarInvoiceMode17Sum.ColumnName = "invoice_mode17_sum";
                colvarInvoiceMode17Sum.DataType = DbType.Int32;
                colvarInvoiceMode17Sum.MaxLength = 0;
                colvarInvoiceMode17Sum.AutoIncrement = false;
                colvarInvoiceMode17Sum.IsNullable = true;
                colvarInvoiceMode17Sum.IsPrimaryKey = false;
                colvarInvoiceMode17Sum.IsForeignKey = false;
                colvarInvoiceMode17Sum.IsReadOnly = false;
                colvarInvoiceMode17Sum.DefaultSetting = @"";
                colvarInvoiceMode17Sum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode17Sum);

                TableSchema.TableColumn colvarInvoiceMode18Total = new TableSchema.TableColumn(schema);
                colvarInvoiceMode18Total.ColumnName = "invoice_mode18_total";
                colvarInvoiceMode18Total.DataType = DbType.Int32;
                colvarInvoiceMode18Total.MaxLength = 0;
                colvarInvoiceMode18Total.AutoIncrement = false;
                colvarInvoiceMode18Total.IsNullable = true;
                colvarInvoiceMode18Total.IsPrimaryKey = false;
                colvarInvoiceMode18Total.IsForeignKey = false;
                colvarInvoiceMode18Total.IsReadOnly = false;
                colvarInvoiceMode18Total.DefaultSetting = @"";
                colvarInvoiceMode18Total.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode18Total);

                TableSchema.TableColumn colvarInvoiceMode18Sum = new TableSchema.TableColumn(schema);
                colvarInvoiceMode18Sum.ColumnName = "invoice_mode18_sum";
                colvarInvoiceMode18Sum.DataType = DbType.Int32;
                colvarInvoiceMode18Sum.MaxLength = 0;
                colvarInvoiceMode18Sum.AutoIncrement = false;
                colvarInvoiceMode18Sum.IsNullable = true;
                colvarInvoiceMode18Sum.IsPrimaryKey = false;
                colvarInvoiceMode18Sum.IsForeignKey = false;
                colvarInvoiceMode18Sum.IsReadOnly = false;
                colvarInvoiceMode18Sum.DefaultSetting = @"";
                colvarInvoiceMode18Sum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode18Sum);

                TableSchema.TableColumn colvarInvoiceMode21Total = new TableSchema.TableColumn(schema);
                colvarInvoiceMode21Total.ColumnName = "invoice_mode21_total";
                colvarInvoiceMode21Total.DataType = DbType.Int32;
                colvarInvoiceMode21Total.MaxLength = 0;
                colvarInvoiceMode21Total.AutoIncrement = false;
                colvarInvoiceMode21Total.IsNullable = true;
                colvarInvoiceMode21Total.IsPrimaryKey = false;
                colvarInvoiceMode21Total.IsForeignKey = false;
                colvarInvoiceMode21Total.IsReadOnly = false;
                colvarInvoiceMode21Total.DefaultSetting = @"";
                colvarInvoiceMode21Total.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode21Total);

                TableSchema.TableColumn colvarInvoiceMode21Sum = new TableSchema.TableColumn(schema);
                colvarInvoiceMode21Sum.ColumnName = "invoice_mode21_sum";
                colvarInvoiceMode21Sum.DataType = DbType.Int32;
                colvarInvoiceMode21Sum.MaxLength = 0;
                colvarInvoiceMode21Sum.AutoIncrement = false;
                colvarInvoiceMode21Sum.IsNullable = true;
                colvarInvoiceMode21Sum.IsPrimaryKey = false;
                colvarInvoiceMode21Sum.IsForeignKey = false;
                colvarInvoiceMode21Sum.IsReadOnly = false;
                colvarInvoiceMode21Sum.DefaultSetting = @"";
                colvarInvoiceMode21Sum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode21Sum);

                TableSchema.TableColumn colvarInvoiceMode22Total = new TableSchema.TableColumn(schema);
                colvarInvoiceMode22Total.ColumnName = "invoice_mode22_total";
                colvarInvoiceMode22Total.DataType = DbType.Int32;
                colvarInvoiceMode22Total.MaxLength = 0;
                colvarInvoiceMode22Total.AutoIncrement = false;
                colvarInvoiceMode22Total.IsNullable = true;
                colvarInvoiceMode22Total.IsPrimaryKey = false;
                colvarInvoiceMode22Total.IsForeignKey = false;
                colvarInvoiceMode22Total.IsReadOnly = false;
                colvarInvoiceMode22Total.DefaultSetting = @"";
                colvarInvoiceMode22Total.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode22Total);

                TableSchema.TableColumn colvarInvoiceMode22Sum = new TableSchema.TableColumn(schema);
                colvarInvoiceMode22Sum.ColumnName = "invoice_mode22_sum";
                colvarInvoiceMode22Sum.DataType = DbType.Int32;
                colvarInvoiceMode22Sum.MaxLength = 0;
                colvarInvoiceMode22Sum.AutoIncrement = false;
                colvarInvoiceMode22Sum.IsNullable = true;
                colvarInvoiceMode22Sum.IsPrimaryKey = false;
                colvarInvoiceMode22Sum.IsForeignKey = false;
                colvarInvoiceMode22Sum.IsReadOnly = false;
                colvarInvoiceMode22Sum.DefaultSetting = @"";
                colvarInvoiceMode22Sum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceMode22Sum);

                TableSchema.TableColumn colvarInvoiceNumber = new TableSchema.TableColumn(schema);
                colvarInvoiceNumber.ColumnName = "invoice_number";
                colvarInvoiceNumber.DataType = DbType.String;
                colvarInvoiceNumber.MaxLength = 10;
                colvarInvoiceNumber.AutoIncrement = false;
                colvarInvoiceNumber.IsNullable = true;
                colvarInvoiceNumber.IsPrimaryKey = false;
                colvarInvoiceNumber.IsForeignKey = false;
                colvarInvoiceNumber.IsReadOnly = false;
                colvarInvoiceNumber.DefaultSetting = @"";
                colvarInvoiceNumber.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceNumber);

                TableSchema.TableColumn colvarLogMessage = new TableSchema.TableColumn(schema);
                colvarLogMessage.ColumnName = "log_message";
                colvarLogMessage.DataType = DbType.String;
                colvarLogMessage.MaxLength = 1073741823;
                colvarLogMessage.AutoIncrement = false;
                colvarLogMessage.IsNullable = true;
                colvarLogMessage.IsPrimaryKey = false;
                colvarLogMessage.IsForeignKey = false;
                colvarLogMessage.IsReadOnly = false;
                colvarLogMessage.DefaultSetting = @"";
                colvarLogMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLogMessage);

                TableSchema.TableColumn colvarInvoiceType = new TableSchema.TableColumn(schema);
                colvarInvoiceType.ColumnName = "invoice_type";
                colvarInvoiceType.DataType = DbType.Int32;
                colvarInvoiceType.MaxLength = 0;
                colvarInvoiceType.AutoIncrement = false;
                colvarInvoiceType.IsNullable = false;
                colvarInvoiceType.IsPrimaryKey = false;
                colvarInvoiceType.IsForeignKey = false;
                colvarInvoiceType.IsReadOnly = false;
                colvarInvoiceType.DefaultSetting = @"";
                colvarInvoiceType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceType);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 100;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarFileName = new TableSchema.TableColumn(schema);
                colvarFileName.ColumnName = "file_name";
                colvarFileName.DataType = DbType.String;
                colvarFileName.MaxLength = 50;
                colvarFileName.AutoIncrement = false;
                colvarFileName.IsNullable = true;
                colvarFileName.IsPrimaryKey = false;
                colvarFileName.IsForeignKey = false;
                colvarFileName.IsReadOnly = false;
                colvarFileName.DefaultSetting = @"";
                colvarFileName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFileName);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("einvoice_log",schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("DateCode")]
        [Bindable(true)]
        public string DateCode
        {
            get { return GetColumnValue<string>(Columns.DateCode); }
            set { SetColumnValue(Columns.DateCode, value); }
        }

        [XmlAttribute("InvoiceDateStart")]
        [Bindable(true)]
        public DateTime? InvoiceDateStart
        {
            get { return GetColumnValue<DateTime?>(Columns.InvoiceDateStart); }
            set { SetColumnValue(Columns.InvoiceDateStart, value); }
        }

        [XmlAttribute("InvoiceDateEnd")]
        [Bindable(true)]
        public DateTime? InvoiceDateEnd
        {
            get { return GetColumnValue<DateTime?>(Columns.InvoiceDateEnd); }
            set { SetColumnValue(Columns.InvoiceDateEnd, value); }
        }

        [XmlAttribute("InvoiceCount")]
        [Bindable(true)]
        public int? InvoiceCount
        {
            get { return GetColumnValue<int?>(Columns.InvoiceCount); }
            set { SetColumnValue(Columns.InvoiceCount, value); }
        }

        [XmlAttribute("InvoiceSum")]
        [Bindable(true)]
        public int? InvoiceSum
        {
            get { return GetColumnValue<int?>(Columns.InvoiceSum); }
            set { SetColumnValue(Columns.InvoiceSum, value); }
        }

        [XmlAttribute("ReferenceSum")]
        [Bindable(true)]
        public int? ReferenceSum
        {
            get { return GetColumnValue<int?>(Columns.ReferenceSum); }
            set { SetColumnValue(Columns.ReferenceSum, value); }
        }

        [XmlAttribute("InvoiceMode1Total")]
        [Bindable(true)]
        public int? InvoiceMode1Total
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode1Total); }
            set { SetColumnValue(Columns.InvoiceMode1Total, value); }
        }

        [XmlAttribute("InvoiceMode1Sum")]
        [Bindable(true)]
        public int? InvoiceMode1Sum
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode1Sum); }
            set { SetColumnValue(Columns.InvoiceMode1Sum, value); }
        }

        [XmlAttribute("InvoiceMode2Total")]
        [Bindable(true)]
        public int? InvoiceMode2Total
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode2Total); }
            set { SetColumnValue(Columns.InvoiceMode2Total, value); }
        }

        [XmlAttribute("InvoiceMode2Sum")]
        [Bindable(true)]
        public int? InvoiceMode2Sum
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode2Sum); }
            set { SetColumnValue(Columns.InvoiceMode2Sum, value); }
        }

        [XmlAttribute("InvoiceMode17Total")]
        [Bindable(true)]
        public int? InvoiceMode17Total
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode17Total); }
            set { SetColumnValue(Columns.InvoiceMode17Total, value); }
        }

        [XmlAttribute("InvoiceMode17Sum")]
        [Bindable(true)]
        public int? InvoiceMode17Sum
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode17Sum); }
            set { SetColumnValue(Columns.InvoiceMode17Sum, value); }
        }

        [XmlAttribute("InvoiceMode18Total")]
        [Bindable(true)]
        public int? InvoiceMode18Total
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode18Total); }
            set { SetColumnValue(Columns.InvoiceMode18Total, value); }
        }

        [XmlAttribute("InvoiceMode18Sum")]
        [Bindable(true)]
        public int? InvoiceMode18Sum
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode18Sum); }
            set { SetColumnValue(Columns.InvoiceMode18Sum, value); }
        }

        [XmlAttribute("InvoiceMode21Total")]
        [Bindable(true)]
        public int? InvoiceMode21Total
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode21Total); }
            set { SetColumnValue(Columns.InvoiceMode21Total, value); }
        }

        [XmlAttribute("InvoiceMode21Sum")]
        [Bindable(true)]
        public int? InvoiceMode21Sum
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode21Sum); }
            set { SetColumnValue(Columns.InvoiceMode21Sum, value); }
        }

        [XmlAttribute("InvoiceMode22Total")]
        [Bindable(true)]
        public int? InvoiceMode22Total
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode22Total); }
            set { SetColumnValue(Columns.InvoiceMode22Total, value); }
        }

        [XmlAttribute("InvoiceMode22Sum")]
        [Bindable(true)]
        public int? InvoiceMode22Sum
        {
            get { return GetColumnValue<int?>(Columns.InvoiceMode22Sum); }
            set { SetColumnValue(Columns.InvoiceMode22Sum, value); }
        }

        [XmlAttribute("InvoiceNumber")]
        [Bindable(true)]
        public string InvoiceNumber
        {
            get { return GetColumnValue<string>(Columns.InvoiceNumber); }
            set { SetColumnValue(Columns.InvoiceNumber, value); }
        }

        [XmlAttribute("LogMessage")]
        [Bindable(true)]
        public string LogMessage
        {
            get { return GetColumnValue<string>(Columns.LogMessage); }
            set { SetColumnValue(Columns.LogMessage, value); }
        }

        [XmlAttribute("InvoiceType")]
        [Bindable(true)]
        public int InvoiceType
        {
            get { return GetColumnValue<int>(Columns.InvoiceType); }
            set { SetColumnValue(Columns.InvoiceType, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("FileName")]
        [Bindable(true)]
        public string FileName
        {
            get { return GetColumnValue<string>(Columns.FileName); }
            set { SetColumnValue(Columns.FileName, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn DateCodeColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn InvoiceDateStartColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn InvoiceDateEndColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn InvoiceCountColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn InvoiceSumColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ReferenceSumColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn InvoiceMode1TotalColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn InvoiceMode1SumColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn InvoiceMode2TotalColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn InvoiceMode2SumColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn InvoiceMode17TotalColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn InvoiceMode17SumColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn InvoiceMode18TotalColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn InvoiceMode18SumColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn InvoiceMode21TotalColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn InvoiceMode21SumColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn InvoiceMode22TotalColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn InvoiceMode22SumColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn InvoiceNumberColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn LogMessageColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn InvoiceTypeColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[22]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[23]; }
        }



        public static TableSchema.TableColumn FileNameColumn
        {
            get { return Schema.Columns[24]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string DateCode = @"date_code";
            public static string InvoiceDateStart = @"invoice_date_start";
            public static string InvoiceDateEnd = @"invoice_date_end";
            public static string InvoiceCount = @"invoice_count";
            public static string InvoiceSum = @"invoice_sum";
            public static string ReferenceSum = @"reference_sum";
            public static string InvoiceMode1Total = @"invoice_mode1_total";
            public static string InvoiceMode1Sum = @"invoice_mode1_sum";
            public static string InvoiceMode2Total = @"invoice_mode2_total";
            public static string InvoiceMode2Sum = @"invoice_mode2_sum";
            public static string InvoiceMode17Total = @"invoice_mode17_total";
            public static string InvoiceMode17Sum = @"invoice_mode17_sum";
            public static string InvoiceMode18Total = @"invoice_mode18_total";
            public static string InvoiceMode18Sum = @"invoice_mode18_sum";
            public static string InvoiceMode21Total = @"invoice_mode21_total";
            public static string InvoiceMode21Sum = @"invoice_mode21_sum";
            public static string InvoiceMode22Total = @"invoice_mode22_total";
            public static string InvoiceMode22Sum = @"invoice_mode22_sum";
            public static string InvoiceNumber = @"invoice_number";
            public static string LogMessage = @"log_message";
            public static string InvoiceType = @"invoice_type";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string FileName = @"file_name";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

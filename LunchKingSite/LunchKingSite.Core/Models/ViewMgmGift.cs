using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewMgmGift class.
    /// </summary>
    [Serializable]
    public partial class ViewMgmGiftCollection : ReadOnlyList<ViewMgmGift, ViewMgmGiftCollection>
    {
        public ViewMgmGiftCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_mgm_gift view.
    /// </summary>
    [Serializable]
    public partial class ViewMgmGift : ReadOnlyRecord<ViewMgmGift>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_mgm_gift", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarIsUsed = new TableSchema.TableColumn(schema);
                colvarIsUsed.ColumnName = "is_used";
                colvarIsUsed.DataType = DbType.Boolean;
                colvarIsUsed.MaxLength = 0;
                colvarIsUsed.AutoIncrement = false;
                colvarIsUsed.IsNullable = false;
                colvarIsUsed.IsPrimaryKey = false;
                colvarIsUsed.IsForeignKey = false;
                colvarIsUsed.IsReadOnly = false;

                schema.Columns.Add(colvarIsUsed);

                TableSchema.TableColumn colvarUsedTime = new TableSchema.TableColumn(schema);
                colvarUsedTime.ColumnName = "used_time";
                colvarUsedTime.DataType = DbType.DateTime;
                colvarUsedTime.MaxLength = 0;
                colvarUsedTime.AutoIncrement = false;
                colvarUsedTime.IsNullable = true;
                colvarUsedTime.IsPrimaryKey = false;
                colvarUsedTime.IsForeignKey = false;
                colvarUsedTime.IsReadOnly = false;

                schema.Columns.Add(colvarUsedTime);

                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;

                schema.Columns.Add(colvarBid);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;

                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
                colvarSequenceNumber.ColumnName = "sequence_number";
                colvarSequenceNumber.DataType = DbType.AnsiString;
                colvarSequenceNumber.MaxLength = 5;
                colvarSequenceNumber.AutoIncrement = false;
                colvarSequenceNumber.IsNullable = false;
                colvarSequenceNumber.IsPrimaryKey = false;
                colvarSequenceNumber.IsForeignKey = false;
                colvarSequenceNumber.IsReadOnly = false;

                schema.Columns.Add(colvarSequenceNumber);

                TableSchema.TableColumn colvarMgmMatchId = new TableSchema.TableColumn(schema);
                colvarMgmMatchId.ColumnName = "mgm_match_id";
                colvarMgmMatchId.DataType = DbType.Int32;
                colvarMgmMatchId.MaxLength = 0;
                colvarMgmMatchId.AutoIncrement = false;
                colvarMgmMatchId.IsNullable = false;
                colvarMgmMatchId.IsPrimaryKey = false;
                colvarMgmMatchId.IsForeignKey = false;
                colvarMgmMatchId.IsReadOnly = false;

                schema.Columns.Add(colvarMgmMatchId);

                TableSchema.TableColumn colvarSendMessageId = new TableSchema.TableColumn(schema);
                colvarSendMessageId.ColumnName = "send_message_id";
                colvarSendMessageId.DataType = DbType.Int32;
                colvarSendMessageId.MaxLength = 0;
                colvarSendMessageId.AutoIncrement = false;
                colvarSendMessageId.IsNullable = false;
                colvarSendMessageId.IsPrimaryKey = false;
                colvarSendMessageId.IsForeignKey = false;
                colvarSendMessageId.IsReadOnly = false;

                schema.Columns.Add(colvarSendMessageId);

                TableSchema.TableColumn colvarReplyMessageId = new TableSchema.TableColumn(schema);
                colvarReplyMessageId.ColumnName = "reply_message_id";
                colvarReplyMessageId.DataType = DbType.Int32;
                colvarReplyMessageId.MaxLength = 0;
                colvarReplyMessageId.AutoIncrement = false;
                colvarReplyMessageId.IsNullable = true;
                colvarReplyMessageId.IsPrimaryKey = false;
                colvarReplyMessageId.IsForeignKey = false;
                colvarReplyMessageId.IsReadOnly = false;

                schema.Columns.Add(colvarReplyMessageId);

                TableSchema.TableColumn colvarSenderId = new TableSchema.TableColumn(schema);
                colvarSenderId.ColumnName = "sender_id";
                colvarSenderId.DataType = DbType.Int32;
                colvarSenderId.MaxLength = 0;
                colvarSenderId.AutoIncrement = false;
                colvarSenderId.IsNullable = false;
                colvarSenderId.IsPrimaryKey = false;
                colvarSenderId.IsForeignKey = false;
                colvarSenderId.IsReadOnly = false;

                schema.Columns.Add(colvarSenderId);

                TableSchema.TableColumn colvarSendTime = new TableSchema.TableColumn(schema);
                colvarSendTime.ColumnName = "send_time";
                colvarSendTime.DataType = DbType.DateTime;
                colvarSendTime.MaxLength = 0;
                colvarSendTime.AutoIncrement = false;
                colvarSendTime.IsNullable = false;
                colvarSendTime.IsPrimaryKey = false;
                colvarSendTime.IsForeignKey = false;
                colvarSendTime.IsReadOnly = false;

                schema.Columns.Add(colvarSendTime);

                TableSchema.TableColumn colvarReceiverId = new TableSchema.TableColumn(schema);
                colvarReceiverId.ColumnName = "receiver_id";
                colvarReceiverId.DataType = DbType.Int32;
                colvarReceiverId.MaxLength = 0;
                colvarReceiverId.AutoIncrement = false;
                colvarReceiverId.IsNullable = true;
                colvarReceiverId.IsPrimaryKey = false;
                colvarReceiverId.IsForeignKey = false;
                colvarReceiverId.IsReadOnly = false;

                schema.Columns.Add(colvarReceiverId);

                TableSchema.TableColumn colvarReceiveTime = new TableSchema.TableColumn(schema);
                colvarReceiveTime.ColumnName = "receive_time";
                colvarReceiveTime.DataType = DbType.DateTime;
                colvarReceiveTime.MaxLength = 0;
                colvarReceiveTime.AutoIncrement = false;
                colvarReceiveTime.IsNullable = true;
                colvarReceiveTime.IsPrimaryKey = false;
                colvarReceiveTime.IsForeignKey = false;
                colvarReceiveTime.IsReadOnly = false;

                schema.Columns.Add(colvarReceiveTime);

                TableSchema.TableColumn colvarAccept = new TableSchema.TableColumn(schema);
                colvarAccept.ColumnName = "accept";
                colvarAccept.DataType = DbType.Int32;
                colvarAccept.MaxLength = 0;
                colvarAccept.AutoIncrement = false;
                colvarAccept.IsNullable = false;
                colvarAccept.IsPrimaryKey = false;
                colvarAccept.IsForeignKey = false;
                colvarAccept.IsReadOnly = false;

                schema.Columns.Add(colvarAccept);

                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;

                schema.Columns.Add(colvarCouponId);

                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.String;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = true;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;

                schema.Columns.Add(colvarCouponSequenceNumber);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = -1;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;

                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
                colvarLastName.ColumnName = "last_name";
                colvarLastName.DataType = DbType.String;
                colvarLastName.MaxLength = 50;
                colvarLastName.AutoIncrement = false;
                colvarLastName.IsNullable = true;
                colvarLastName.IsPrimaryKey = false;
                colvarLastName.IsForeignKey = false;
                colvarLastName.IsReadOnly = false;

                schema.Columns.Add(colvarLastName);

                TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
                colvarFirstName.ColumnName = "first_name";
                colvarFirstName.DataType = DbType.String;
                colvarFirstName.MaxLength = 50;
                colvarFirstName.AutoIncrement = false;
                colvarFirstName.IsNullable = true;
                colvarFirstName.IsPrimaryKey = false;
                colvarFirstName.IsForeignKey = false;
                colvarFirstName.IsReadOnly = false;

                schema.Columns.Add(colvarFirstName);

                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourStatus);

                TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarChangedExpireDate.ColumnName = "changed_expire_date";
                colvarChangedExpireDate.DataType = DbType.DateTime;
                colvarChangedExpireDate.MaxLength = 0;
                colvarChangedExpireDate.AutoIncrement = false;
                colvarChangedExpireDate.IsNullable = true;
                colvarChangedExpireDate.IsPrimaryKey = false;
                colvarChangedExpireDate.IsForeignKey = false;
                colvarChangedExpireDate.IsReadOnly = false;

                schema.Columns.Add(colvarChangedExpireDate);

                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourOrderTimeE);

                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourDeliverTimeE);

                TableSchema.TableColumn colvarTrustedTime = new TableSchema.TableColumn(schema);
                colvarTrustedTime.ColumnName = "trusted_time";
                colvarTrustedTime.DataType = DbType.DateTime;
                colvarTrustedTime.MaxLength = 0;
                colvarTrustedTime.AutoIncrement = false;
                colvarTrustedTime.IsNullable = true;
                colvarTrustedTime.IsPrimaryKey = false;
                colvarTrustedTime.IsForeignKey = false;
                colvarTrustedTime.IsReadOnly = false;

                schema.Columns.Add(colvarTrustedTime);

                TableSchema.TableColumn colvarIsActive = new TableSchema.TableColumn(schema);
                colvarIsActive.ColumnName = "is_active";
                colvarIsActive.DataType = DbType.Boolean;
                colvarIsActive.MaxLength = 0;
                colvarIsActive.AutoIncrement = false;
                colvarIsActive.IsNullable = false;
                colvarIsActive.IsPrimaryKey = false;
                colvarIsActive.IsForeignKey = false;
                colvarIsActive.IsReadOnly = false;

                schema.Columns.Add(colvarIsActive);

                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 120;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = true;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;

                schema.Columns.Add(colvarTitle);

                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 400;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;

                schema.Columns.Add(colvarName);

                TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
                colvarImagePath.ColumnName = "image_path";
                colvarImagePath.DataType = DbType.String;
                colvarImagePath.MaxLength = 1000;
                colvarImagePath.AutoIncrement = false;
                colvarImagePath.IsNullable = true;
                colvarImagePath.IsPrimaryKey = false;
                colvarImagePath.IsForeignKey = false;
                colvarImagePath.IsReadOnly = false;

                schema.Columns.Add(colvarImagePath);

                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 50;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = false;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;

                schema.Columns.Add(colvarCode);

                TableSchema.TableColumn colvarAccessCode = new TableSchema.TableColumn(schema);
                colvarAccessCode.ColumnName = "access_code";
                colvarAccessCode.DataType = DbType.Guid;
                colvarAccessCode.MaxLength = 0;
                colvarAccessCode.AutoIncrement = false;
                colvarAccessCode.IsNullable = true;
                colvarAccessCode.IsPrimaryKey = false;
                colvarAccessCode.IsForeignKey = false;
                colvarAccessCode.IsReadOnly = false;

                schema.Columns.Add(colvarAccessCode);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;

                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarReceiverName = new TableSchema.TableColumn(schema);
                colvarReceiverName.ColumnName = "receiver_name";
                colvarReceiverName.DataType = DbType.String;
                colvarReceiverName.MaxLength = 250;
                colvarReceiverName.AutoIncrement = false;
                colvarReceiverName.IsNullable = true;
                colvarReceiverName.IsPrimaryKey = false;
                colvarReceiverName.IsForeignKey = false;
                colvarReceiverName.IsReadOnly = false;

                schema.Columns.Add(colvarReceiverName);

                TableSchema.TableColumn colvarReceiveName = new TableSchema.TableColumn(schema);
                colvarReceiveName.ColumnName = "receive_name";
                colvarReceiveName.DataType = DbType.String;
                colvarReceiveName.MaxLength = 100;
                colvarReceiveName.AutoIncrement = false;
                colvarReceiveName.IsNullable = true;
                colvarReceiveName.IsPrimaryKey = false;
                colvarReceiveName.IsForeignKey = false;
                colvarReceiveName.IsReadOnly = false;

                schema.Columns.Add(colvarReceiveName);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;

                schema.Columns.Add(colvarSellerGuid);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_mgm_gift", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewMgmGift()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMgmGift(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewMgmGift(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewMgmGift(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("IsUsed")]
        [Bindable(true)]
        public bool IsUsed
        {
            get
            {
                return GetColumnValue<bool>("is_used");
            }
            set
            {
                SetColumnValue("is_used", value);
            }
        }

        [XmlAttribute("UsedTime")]
        [Bindable(true)]
        public DateTime? UsedTime
        {
            get
            {
                return GetColumnValue<DateTime?>("used_time");
            }
            set
            {
                SetColumnValue("used_time", value);
            }
        }

        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid
        {
            get
            {
                return GetColumnValue<Guid>("bid");
            }
            set
            {
                SetColumnValue("bid", value);
            }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get
            {
                return GetColumnValue<Guid>("guid");
            }
            set
            {
                SetColumnValue("guid", value);
            }
        }

        [XmlAttribute("SequenceNumber")]
        [Bindable(true)]
        public string SequenceNumber
        {
            get
            {
                return GetColumnValue<string>("sequence_number");
            }
            set
            {
                SetColumnValue("sequence_number", value);
            }
        }

        [XmlAttribute("MgmMatchId")]
        [Bindable(true)]
        public int MgmMatchId
        {
            get
            {
                return GetColumnValue<int>("mgm_match_id");
            }
            set
            {
                SetColumnValue("mgm_match_id", value);
            }
        }

        [XmlAttribute("SendMessageId")]
        [Bindable(true)]
        public int SendMessageId
        {
            get
            {
                return GetColumnValue<int>("send_message_id");
            }
            set
            {
                SetColumnValue("send_message_id", value);
            }
        }

        [XmlAttribute("ReplyMessageId")]
        [Bindable(true)]
        public int? ReplyMessageId
        {
            get
            {
                return GetColumnValue<int?>("reply_message_id");
            }
            set
            {
                SetColumnValue("reply_message_id", value);
            }
        }

        [XmlAttribute("SenderId")]
        [Bindable(true)]
        public int SenderId
        {
            get
            {
                return GetColumnValue<int>("sender_id");
            }
            set
            {
                SetColumnValue("sender_id", value);
            }
        }

        [XmlAttribute("SendTime")]
        [Bindable(true)]
        public DateTime SendTime
        {
            get
            {
                return GetColumnValue<DateTime>("send_time");
            }
            set
            {
                SetColumnValue("send_time", value);
            }
        }

        [XmlAttribute("ReceiverId")]
        [Bindable(true)]
        public int? ReceiverId
        {
            get
            {
                return GetColumnValue<int?>("receiver_id");
            }
            set
            {
                SetColumnValue("receiver_id", value);
            }
        }

        [XmlAttribute("ReceiveTime")]
        [Bindable(true)]
        public DateTime? ReceiveTime
        {
            get
            {
                return GetColumnValue<DateTime?>("receive_time");
            }
            set
            {
                SetColumnValue("receive_time", value);
            }
        }

        [XmlAttribute("Accept")]
        [Bindable(true)]
        public int Accept
        {
            get
            {
                return GetColumnValue<int>("accept");
            }
            set
            {
                SetColumnValue("accept", value);
            }
        }

        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId
        {
            get
            {
                return GetColumnValue<int?>("coupon_id");
            }
            set
            {
                SetColumnValue("coupon_id", value);
            }
        }

        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber
        {
            get
            {
                return GetColumnValue<string>("coupon_sequence_number");
            }
            set
            {
                SetColumnValue("coupon_sequence_number", value);
            }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get
            {
                return GetColumnValue<string>("item_name");
            }
            set
            {
                SetColumnValue("item_name", value);
            }
        }

        [XmlAttribute("LastName")]
        [Bindable(true)]
        public string LastName
        {
            get
            {
                return GetColumnValue<string>("last_name");
            }
            set
            {
                SetColumnValue("last_name", value);
            }
        }

        [XmlAttribute("FirstName")]
        [Bindable(true)]
        public string FirstName
        {
            get
            {
                return GetColumnValue<string>("first_name");
            }
            set
            {
                SetColumnValue("first_name", value);
            }
        }

        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus
        {
            get
            {
                return GetColumnValue<int>("business_hour_status");
            }
            set
            {
                SetColumnValue("business_hour_status", value);
            }
        }

        [XmlAttribute("ChangedExpireDate")]
        [Bindable(true)]
        public DateTime? ChangedExpireDate
        {
            get
            {
                return GetColumnValue<DateTime?>("changed_expire_date");
            }
            set
            {
                SetColumnValue("changed_expire_date", value);
            }
        }

        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE
        {
            get
            {
                return GetColumnValue<DateTime>("business_hour_order_time_e");
            }
            set
            {
                SetColumnValue("business_hour_order_time_e", value);
            }
        }

        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE
        {
            get
            {
                return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
            }
            set
            {
                SetColumnValue("business_hour_deliver_time_e", value);
            }
        }

        [XmlAttribute("TrustedTime")]
        [Bindable(true)]
        public DateTime? TrustedTime
        {
            get
            {
                return GetColumnValue<DateTime?>("trusted_time");
            }
            set
            {
                SetColumnValue("trusted_time", value);
            }
        }

        [XmlAttribute("IsActive")]
        [Bindable(true)]
        public bool IsActive
        {
            get
            {
                return GetColumnValue<bool>("is_active");
            }
            set
            {
                SetColumnValue("is_active", value);
            }
        }

        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title
        {
            get
            {
                return GetColumnValue<string>("title");
            }
            set
            {
                SetColumnValue("title", value);
            }
        }

        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name
        {
            get
            {
                return GetColumnValue<string>("name");
            }
            set
            {
                SetColumnValue("name", value);
            }
        }

        [XmlAttribute("ImagePath")]
        [Bindable(true)]
        public string ImagePath
        {
            get
            {
                return GetColumnValue<string>("image_path");
            }
            set
            {
                SetColumnValue("image_path", value);
            }
        }

        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code
        {
            get
            {
                return GetColumnValue<string>("code");
            }
            set
            {
                SetColumnValue("code", value);
            }
        }

        [XmlAttribute("AccessCode")]
        [Bindable(true)]
        public Guid? AccessCode
        {
            get
            {
                return GetColumnValue<Guid?>("access_code");
            }
            set
            {
                SetColumnValue("access_code", value);
            }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid
        {
            get
            {
                return GetColumnValue<Guid>("order_guid");
            }
            set
            {
                SetColumnValue("order_guid", value);
            }
        }

        [XmlAttribute("ReceiverName")]
        [Bindable(true)]
        public string ReceiverName
        {
            get
            {
                return GetColumnValue<string>("receiver_name");
            }
            set
            {
                SetColumnValue("receiver_name", value);
            }
        }

        [XmlAttribute("ReceiveName")]
        [Bindable(true)]
        public string ReceiveName
        {
            get
            {
                return GetColumnValue<string>("receive_name");
            }
            set
            {
                SetColumnValue("receive_name", value);
            }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid
        {
            get
            {
                return GetColumnValue<Guid>("seller_GUID");
            }
            set
            {
                SetColumnValue("seller_GUID", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Id = @"id";

            public static string IsUsed = @"is_used";

            public static string UsedTime = @"used_time";

            public static string Bid = @"bid";

            public static string Guid = @"guid";

            public static string SequenceNumber = @"sequence_number";

            public static string MgmMatchId = @"mgm_match_id";

            public static string SendMessageId = @"send_message_id";

            public static string ReplyMessageId = @"reply_message_id";

            public static string SenderId = @"sender_id";

            public static string SendTime = @"send_time";

            public static string ReceiverId = @"receiver_id";

            public static string ReceiveTime = @"receive_time";

            public static string Accept = @"accept";

            public static string CouponId = @"coupon_id";

            public static string CouponSequenceNumber = @"coupon_sequence_number";

            public static string ItemName = @"item_name";

            public static string LastName = @"last_name";

            public static string FirstName = @"first_name";

            public static string BusinessHourStatus = @"business_hour_status";

            public static string ChangedExpireDate = @"changed_expire_date";

            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";

            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";

            public static string TrustedTime = @"trusted_time";

            public static string IsActive = @"is_active";

            public static string Title = @"title";

            public static string Name = @"name";

            public static string ImagePath = @"image_path";

            public static string Code = @"code";

            public static string AccessCode = @"access_code";

            public static string OrderGuid = @"order_guid";

            public static string ReceiverName = @"receiver_name";

            public static string ReceiveName = @"receive_name";

            public static string SellerGuid = @"seller_GUID";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

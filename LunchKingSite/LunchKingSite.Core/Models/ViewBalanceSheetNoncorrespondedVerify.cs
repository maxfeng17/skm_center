using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBalanceSheetNoncorrespondedVerify class.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetNoncorrespondedVerifyCollection : ReadOnlyList<ViewBalanceSheetNoncorrespondedVerify, ViewBalanceSheetNoncorrespondedVerifyCollection>
    {        
        public ViewBalanceSheetNoncorrespondedVerifyCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_balance_sheet_noncorresponded_verifies view.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetNoncorrespondedVerify : ReadOnlyRecord<ViewBalanceSheetNoncorrespondedVerify>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_balance_sheet_noncorresponded_verifies", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustId);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarSpecialStatus = new TableSchema.TableColumn(schema);
                colvarSpecialStatus.ColumnName = "special_status";
                colvarSpecialStatus.DataType = DbType.Int32;
                colvarSpecialStatus.MaxLength = 0;
                colvarSpecialStatus.AutoIncrement = false;
                colvarSpecialStatus.IsNullable = false;
                colvarSpecialStatus.IsPrimaryKey = false;
                colvarSpecialStatus.IsForeignKey = false;
                colvarSpecialStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarSpecialStatus);
                
                TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
                colvarOrderClassification.ColumnName = "order_classification";
                colvarOrderClassification.DataType = DbType.Int32;
                colvarOrderClassification.MaxLength = 0;
                colvarOrderClassification.AutoIncrement = false;
                colvarOrderClassification.IsNullable = false;
                colvarOrderClassification.IsPrimaryKey = false;
                colvarOrderClassification.IsForeignKey = false;
                colvarOrderClassification.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderClassification);
                
                TableSchema.TableColumn colvarMerchandiseGuid = new TableSchema.TableColumn(schema);
                colvarMerchandiseGuid.ColumnName = "merchandise_guid";
                colvarMerchandiseGuid.DataType = DbType.Guid;
                colvarMerchandiseGuid.MaxLength = 0;
                colvarMerchandiseGuid.AutoIncrement = false;
                colvarMerchandiseGuid.IsNullable = true;
                colvarMerchandiseGuid.IsPrimaryKey = false;
                colvarMerchandiseGuid.IsForeignKey = false;
                colvarMerchandiseGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMerchandiseGuid);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.String;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = true;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponSequenceNumber);
                
                TableSchema.TableColumn colvarVerifyUser = new TableSchema.TableColumn(schema);
                colvarVerifyUser.ColumnName = "verify_user";
                colvarVerifyUser.DataType = DbType.String;
                colvarVerifyUser.MaxLength = 256;
                colvarVerifyUser.AutoIncrement = false;
                colvarVerifyUser.IsNullable = true;
                colvarVerifyUser.IsPrimaryKey = false;
                colvarVerifyUser.IsForeignKey = false;
                colvarVerifyUser.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifyUser);
                
                TableSchema.TableColumn colvarVerifyTime = new TableSchema.TableColumn(schema);
                colvarVerifyTime.ColumnName = "verify_time";
                colvarVerifyTime.DataType = DbType.DateTime;
                colvarVerifyTime.MaxLength = 0;
                colvarVerifyTime.AutoIncrement = false;
                colvarVerifyTime.IsNullable = true;
                colvarVerifyTime.IsPrimaryKey = false;
                colvarVerifyTime.IsForeignKey = false;
                colvarVerifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifyTime);
                
                TableSchema.TableColumn colvarPponCost = new TableSchema.TableColumn(schema);
                colvarPponCost.ColumnName = "ppon_cost";
                colvarPponCost.DataType = DbType.Currency;
                colvarPponCost.MaxLength = 0;
                colvarPponCost.AutoIncrement = false;
                colvarPponCost.IsNullable = true;
                colvarPponCost.IsPrimaryKey = false;
                colvarPponCost.IsForeignKey = false;
                colvarPponCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarPponCost);
                
                TableSchema.TableColumn colvarPiinCost = new TableSchema.TableColumn(schema);
                colvarPiinCost.ColumnName = "piin_cost";
                colvarPiinCost.DataType = DbType.Currency;
                colvarPiinCost.MaxLength = 0;
                colvarPiinCost.AutoIncrement = false;
                colvarPiinCost.IsNullable = true;
                colvarPiinCost.IsPrimaryKey = false;
                colvarPiinCost.IsForeignKey = false;
                colvarPiinCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarPiinCost);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_balance_sheet_noncorresponded_verifies",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBalanceSheetNoncorrespondedVerify()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBalanceSheetNoncorrespondedVerify(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBalanceSheetNoncorrespondedVerify(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBalanceSheetNoncorrespondedVerify(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("trust_id");
		    }
            set 
		    {
			    SetColumnValue("trust_id", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("SpecialStatus")]
        [Bindable(true)]
        public int SpecialStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("special_status");
		    }
            set 
		    {
			    SetColumnValue("special_status", value);
            }
        }
	      
        [XmlAttribute("OrderClassification")]
        [Bindable(true)]
        public int OrderClassification 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_classification");
		    }
            set 
		    {
			    SetColumnValue("order_classification", value);
            }
        }
	      
        [XmlAttribute("MerchandiseGuid")]
        [Bindable(true)]
        public Guid? MerchandiseGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("merchandise_guid");
		    }
            set 
		    {
			    SetColumnValue("merchandise_guid", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_sequence_number");
		    }
            set 
		    {
			    SetColumnValue("coupon_sequence_number", value);
            }
        }
	      
        [XmlAttribute("VerifyUser")]
        [Bindable(true)]
        public string VerifyUser 
	    {
		    get
		    {
			    return GetColumnValue<string>("verify_user");
		    }
            set 
		    {
			    SetColumnValue("verify_user", value);
            }
        }
	      
        [XmlAttribute("VerifyTime")]
        [Bindable(true)]
        public DateTime? VerifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("verify_time");
		    }
            set 
		    {
			    SetColumnValue("verify_time", value);
            }
        }
	      
        [XmlAttribute("PponCost")]
        [Bindable(true)]
        public decimal? PponCost 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("ppon_cost");
		    }
            set 
		    {
			    SetColumnValue("ppon_cost", value);
            }
        }
	      
        [XmlAttribute("PiinCost")]
        [Bindable(true)]
        public decimal? PiinCost 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("piin_cost");
		    }
            set 
		    {
			    SetColumnValue("piin_cost", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string TrustId = @"trust_id";
            
            public static string Status = @"status";
            
            public static string SpecialStatus = @"special_status";
            
            public static string OrderClassification = @"order_classification";
            
            public static string MerchandiseGuid = @"merchandise_guid";
            
            public static string StoreGuid = @"store_guid";
            
            public static string StoreName = @"store_name";
            
            public static string CouponSequenceNumber = @"coupon_sequence_number";
            
            public static string VerifyUser = @"verify_user";
            
            public static string VerifyTime = @"verify_time";
            
            public static string PponCost = @"ppon_cost";
            
            public static string PiinCost = @"piin_cost";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

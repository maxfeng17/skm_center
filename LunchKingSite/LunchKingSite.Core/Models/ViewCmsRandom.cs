using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewCmsRandom class.
    /// </summary>
    [Serializable]
    public partial class ViewCmsRandomCollection : ReadOnlyList<ViewCmsRandom, ViewCmsRandomCollection>
    {        
        public ViewCmsRandomCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_cms_random view.
    /// </summary>
    [Serializable]
    public partial class ViewCmsRandom : ReadOnlyRecord<ViewCmsRandom>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_cms_random", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 500;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = false;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarContentTitle = new TableSchema.TableColumn(schema);
                colvarContentTitle.ColumnName = "content_title";
                colvarContentTitle.DataType = DbType.String;
                colvarContentTitle.MaxLength = 200;
                colvarContentTitle.AutoIncrement = false;
                colvarContentTitle.IsNullable = false;
                colvarContentTitle.IsPrimaryKey = false;
                colvarContentTitle.IsForeignKey = false;
                colvarContentTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarContentTitle);
                
                TableSchema.TableColumn colvarContentName = new TableSchema.TableColumn(schema);
                colvarContentName.ColumnName = "content_name";
                colvarContentName.DataType = DbType.String;
                colvarContentName.MaxLength = 250;
                colvarContentName.AutoIncrement = false;
                colvarContentName.IsNullable = false;
                colvarContentName.IsPrimaryKey = false;
                colvarContentName.IsForeignKey = false;
                colvarContentName.IsReadOnly = false;
                
                schema.Columns.Add(colvarContentName);
                
                TableSchema.TableColumn colvarBody = new TableSchema.TableColumn(schema);
                colvarBody.ColumnName = "body";
                colvarBody.DataType = DbType.String;
                colvarBody.MaxLength = -1;
                colvarBody.AutoIncrement = false;
                colvarBody.IsNullable = true;
                colvarBody.IsPrimaryKey = false;
                colvarBody.IsForeignKey = false;
                colvarBody.IsReadOnly = false;
                
                schema.Columns.Add(colvarBody);
                
                TableSchema.TableColumn colvarBannerStatus = new TableSchema.TableColumn(schema);
                colvarBannerStatus.ColumnName = "banner_status";
                colvarBannerStatus.DataType = DbType.Boolean;
                colvarBannerStatus.MaxLength = 0;
                colvarBannerStatus.AutoIncrement = false;
                colvarBannerStatus.IsNullable = false;
                colvarBannerStatus.IsPrimaryKey = false;
                colvarBannerStatus.IsForeignKey = false;
                colvarBannerStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBannerStatus);
                
                TableSchema.TableColumn colvarLocale = new TableSchema.TableColumn(schema);
                colvarLocale.ColumnName = "locale";
                colvarLocale.DataType = DbType.String;
                colvarLocale.MaxLength = 50;
                colvarLocale.AutoIncrement = false;
                colvarLocale.IsNullable = false;
                colvarLocale.IsPrimaryKey = false;
                colvarLocale.IsForeignKey = false;
                colvarLocale.IsReadOnly = false;
                
                schema.Columns.Add(colvarLocale);
                
                TableSchema.TableColumn colvarEventPromoId = new TableSchema.TableColumn(schema);
                colvarEventPromoId.ColumnName = "event_promo_id";
                colvarEventPromoId.DataType = DbType.Int32;
                colvarEventPromoId.MaxLength = 0;
                colvarEventPromoId.AutoIncrement = false;
                colvarEventPromoId.IsNullable = true;
                colvarEventPromoId.IsPrimaryKey = false;
                colvarEventPromoId.IsForeignKey = false;
                colvarEventPromoId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventPromoId);

                TableSchema.TableColumn colvarBrandId = new TableSchema.TableColumn(schema);
                colvarBrandId.ColumnName = "brand_id";
                colvarBrandId.DataType = DbType.Int32;
                colvarBrandId.MaxLength = 0;
                colvarBrandId.AutoIncrement = false;
                colvarBrandId.IsNullable = true;
                colvarBrandId.IsPrimaryKey = false;
                colvarBrandId.IsForeignKey = false;
                colvarBrandId.IsReadOnly = false;

                schema.Columns.Add(colvarBrandId);

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarPid = new TableSchema.TableColumn(schema);
                colvarPid.ColumnName = "pid";
                colvarPid.DataType = DbType.Int32;
                colvarPid.MaxLength = 0;
                colvarPid.AutoIncrement = false;
                colvarPid.IsNullable = false;
                colvarPid.IsPrimaryKey = false;
                colvarPid.IsForeignKey = false;
                colvarPid.IsReadOnly = false;
                
                schema.Columns.Add(colvarPid);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 50;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = true;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = false;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = false;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Boolean;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarRatio = new TableSchema.TableColumn(schema);
                colvarRatio.ColumnName = "ratio";
                colvarRatio.DataType = DbType.Int32;
                colvarRatio.MaxLength = 0;
                colvarRatio.AutoIncrement = false;
                colvarRatio.IsNullable = false;
                colvarRatio.IsPrimaryKey = false;
                colvarRatio.IsForeignKey = false;
                colvarRatio.IsReadOnly = false;
                
                schema.Columns.Add(colvarRatio);
                
                TableSchema.TableColumn colvarCreatedBy = new TableSchema.TableColumn(schema);
                colvarCreatedBy.ColumnName = "created_by";
                colvarCreatedBy.DataType = DbType.String;
                colvarCreatedBy.MaxLength = 50;
                colvarCreatedBy.AutoIncrement = false;
                colvarCreatedBy.IsNullable = true;
                colvarCreatedBy.IsPrimaryKey = false;
                colvarCreatedBy.IsForeignKey = false;
                colvarCreatedBy.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreatedBy);
                
                TableSchema.TableColumn colvarCreatedOn = new TableSchema.TableColumn(schema);
                colvarCreatedOn.ColumnName = "created_on";
                colvarCreatedOn.DataType = DbType.DateTime;
                colvarCreatedOn.MaxLength = 0;
                colvarCreatedOn.AutoIncrement = false;
                colvarCreatedOn.IsNullable = true;
                colvarCreatedOn.IsPrimaryKey = false;
                colvarCreatedOn.IsForeignKey = false;
                colvarCreatedOn.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreatedOn);
                
                TableSchema.TableColumn colvarModifideBy = new TableSchema.TableColumn(schema);
                colvarModifideBy.ColumnName = "modifide_by";
                colvarModifideBy.DataType = DbType.String;
                colvarModifideBy.MaxLength = 50;
                colvarModifideBy.AutoIncrement = false;
                colvarModifideBy.IsNullable = true;
                colvarModifideBy.IsPrimaryKey = false;
                colvarModifideBy.IsForeignKey = false;
                colvarModifideBy.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifideBy);
                
                TableSchema.TableColumn colvarModifiedOn = new TableSchema.TableColumn(schema);
                colvarModifiedOn.ColumnName = "modified_on";
                colvarModifiedOn.DataType = DbType.DateTime;
                colvarModifiedOn.MaxLength = 0;
                colvarModifiedOn.AutoIncrement = false;
                colvarModifiedOn.IsNullable = true;
                colvarModifiedOn.IsPrimaryKey = false;
                colvarModifiedOn.IsForeignKey = false;
                colvarModifiedOn.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifiedOn);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = true;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarSeq);
                
                TableSchema.TableColumn colvarShowInApp = new TableSchema.TableColumn(schema);
                colvarShowInApp.ColumnName = "show_in_app";
                colvarShowInApp.DataType = DbType.Boolean;
                colvarShowInApp.MaxLength = 0;
                colvarShowInApp.AutoIncrement = false;
                colvarShowInApp.IsNullable = true;
                colvarShowInApp.IsPrimaryKey = false;
                colvarShowInApp.IsForeignKey = false;
                colvarShowInApp.IsReadOnly = false;
                
                schema.Columns.Add(colvarShowInApp);
                
                TableSchema.TableColumn colvarShowInWeb = new TableSchema.TableColumn(schema);
                colvarShowInWeb.ColumnName = "show_in_web";
                colvarShowInWeb.DataType = DbType.Boolean;
                colvarShowInWeb.MaxLength = 0;
                colvarShowInWeb.AutoIncrement = false;
                colvarShowInWeb.IsNullable = true;
                colvarShowInWeb.IsPrimaryKey = false;
                colvarShowInWeb.IsForeignKey = false;
                colvarShowInWeb.IsReadOnly = false;
                
                schema.Columns.Add(colvarShowInWeb);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_cms_random",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewCmsRandom()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewCmsRandom(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewCmsRandom(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewCmsRandom(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("title");
		    }
            set 
		    {
			    SetColumnValue("title", value);
            }
        }
	      
        [XmlAttribute("ContentTitle")]
        [Bindable(true)]
        public string ContentTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("content_title");
		    }
            set 
		    {
			    SetColumnValue("content_title", value);
            }
        }
	      
        [XmlAttribute("ContentName")]
        [Bindable(true)]
        public string ContentName 
	    {
		    get
		    {
			    return GetColumnValue<string>("content_name");
		    }
            set 
		    {
			    SetColumnValue("content_name", value);
            }
        }
	      
        [XmlAttribute("Body")]
        [Bindable(true)]
        public string Body 
	    {
		    get
		    {
			    return GetColumnValue<string>("body");
		    }
            set 
		    {
			    SetColumnValue("body", value);
            }
        }
	      
        [XmlAttribute("BannerStatus")]
        [Bindable(true)]
        public bool BannerStatus 
	    {
		    get
		    {
			    return GetColumnValue<bool>("banner_status");
		    }
            set 
		    {
			    SetColumnValue("banner_status", value);
            }
        }
	      
        [XmlAttribute("Locale")]
        [Bindable(true)]
        public string Locale 
	    {
		    get
		    {
			    return GetColumnValue<string>("locale");
		    }
            set 
		    {
			    SetColumnValue("locale", value);
            }
        }
	      
        [XmlAttribute("EventPromoId")]
        [Bindable(true)]
        public int? EventPromoId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("event_promo_id");
		    }
            set 
		    {
			    SetColumnValue("event_promo_id", value);
            }
        }

        [XmlAttribute("BrandId")]
        [Bindable(true)]
        public int? BrandId
        {
            get
            {
                return GetColumnValue<int?>("brand_id");
            }
            set
            {
                SetColumnValue("brand_id", value);
            }
        }

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("Pid")]
        [Bindable(true)]
        public int Pid 
	    {
		    get
		    {
			    return GetColumnValue<int>("pid");
		    }
            set 
		    {
			    SetColumnValue("pid", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public bool Status 
	    {
		    get
		    {
			    return GetColumnValue<bool>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("Ratio")]
        [Bindable(true)]
        public int Ratio 
	    {
		    get
		    {
			    return GetColumnValue<int>("ratio");
		    }
            set 
		    {
			    SetColumnValue("ratio", value);
            }
        }
	      
        [XmlAttribute("CreatedBy")]
        [Bindable(true)]
        public string CreatedBy 
	    {
		    get
		    {
			    return GetColumnValue<string>("created_by");
		    }
            set 
		    {
			    SetColumnValue("created_by", value);
            }
        }
	      
        [XmlAttribute("CreatedOn")]
        [Bindable(true)]
        public DateTime? CreatedOn 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("created_on");
		    }
            set 
		    {
			    SetColumnValue("created_on", value);
            }
        }
	      
        [XmlAttribute("ModifideBy")]
        [Bindable(true)]
        public string ModifideBy 
	    {
		    get
		    {
			    return GetColumnValue<string>("modifide_by");
		    }
            set 
		    {
			    SetColumnValue("modifide_by", value);
            }
        }
	      
        [XmlAttribute("ModifiedOn")]
        [Bindable(true)]
        public DateTime? ModifiedOn 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modified_on");
		    }
            set 
		    {
			    SetColumnValue("modified_on", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int? Seq 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seq");
		    }
            set 
		    {
			    SetColumnValue("seq", value);
            }
        }
	      
        [XmlAttribute("ShowInApp")]
        [Bindable(true)]
        public bool? ShowInApp 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("show_in_app");
		    }
            set 
		    {
			    SetColumnValue("show_in_app", value);
            }
        }
	      
        [XmlAttribute("ShowInWeb")]
        [Bindable(true)]
        public bool? ShowInWeb 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("show_in_web");
		    }
            set 
		    {
			    SetColumnValue("show_in_web", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Title = @"title";
            
            public static string ContentTitle = @"content_title";
            
            public static string ContentName = @"content_name";
            
            public static string Body = @"body";
            
            public static string BannerStatus = @"banner_status";
            
            public static string Locale = @"locale";
            
            public static string EventPromoId = @"event_promo_id";

            public static string BrandId = @"brand_id";

            public static string Id = @"id";
            
            public static string Pid = @"pid";
            
            public static string CityId = @"city_id";
            
            public static string CityName = @"city_name";
            
            public static string StartTime = @"start_time";
            
            public static string EndTime = @"end_time";
            
            public static string Status = @"status";
            
            public static string Ratio = @"ratio";
            
            public static string CreatedBy = @"created_by";
            
            public static string CreatedOn = @"created_on";
            
            public static string ModifideBy = @"modifide_by";
            
            public static string ModifiedOn = @"modified_on";
            
            public static string Type = @"type";
            
            public static string Seq = @"seq";
            
            public static string ShowInApp = @"show_in_app";
            
            public static string ShowInWeb = @"show_in_web";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewYahooVoucherPromo class.
    /// </summary>
    [Serializable]
    public partial class ViewYahooVoucherPromoCollection : ReadOnlyList<ViewYahooVoucherPromo, ViewYahooVoucherPromoCollection>
    {        
        public ViewYahooVoucherPromoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_yahoo_voucher_promo view.
    /// </summary>
    [Serializable]
    public partial class ViewYahooVoucherPromo : ReadOnlyRecord<ViewYahooVoucherPromo>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_yahoo_voucher_promo", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
                colvarEventId.ColumnName = "event_id";
                colvarEventId.DataType = DbType.Int32;
                colvarEventId.MaxLength = 0;
                colvarEventId.AutoIncrement = false;
                colvarEventId.IsNullable = true;
                colvarEventId.IsPrimaryKey = false;
                colvarEventId.IsForeignKey = false;
                colvarEventId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventId);
                
                TableSchema.TableColumn colvarTag = new TableSchema.TableColumn(schema);
                colvarTag.ColumnName = "tag";
                colvarTag.DataType = DbType.String;
                colvarTag.MaxLength = 50;
                colvarTag.AutoIncrement = false;
                colvarTag.IsNullable = true;
                colvarTag.IsPrimaryKey = false;
                colvarTag.IsForeignKey = false;
                colvarTag.IsReadOnly = false;
                
                schema.Columns.Add(colvarTag);
                
                TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
                colvarContent.ColumnName = "content";
                colvarContent.DataType = DbType.String;
                colvarContent.MaxLength = 1073741823;
                colvarContent.AutoIncrement = false;
                colvarContent.IsNullable = true;
                colvarContent.IsPrimaryKey = false;
                colvarContent.IsForeignKey = false;
                colvarContent.IsReadOnly = false;
                
                schema.Columns.Add(colvarContent);
                
                TableSchema.TableColumn colvarLink = new TableSchema.TableColumn(schema);
                colvarLink.ColumnName = "link";
                colvarLink.DataType = DbType.String;
                colvarLink.MaxLength = 100;
                colvarLink.AutoIncrement = false;
                colvarLink.IsNullable = true;
                colvarLink.IsPrimaryKey = false;
                colvarLink.IsForeignKey = false;
                colvarLink.IsReadOnly = false;
                
                schema.Columns.Add(colvarLink);
                
                TableSchema.TableColumn colvarRank = new TableSchema.TableColumn(schema);
                colvarRank.ColumnName = "rank";
                colvarRank.DataType = DbType.Int32;
                colvarRank.MaxLength = 0;
                colvarRank.AutoIncrement = false;
                colvarRank.IsNullable = false;
                colvarRank.IsPrimaryKey = false;
                colvarRank.IsForeignKey = false;
                colvarRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarRank);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarPromoStartDate = new TableSchema.TableColumn(schema);
                colvarPromoStartDate.ColumnName = "promo_start_date";
                colvarPromoStartDate.DataType = DbType.DateTime;
                colvarPromoStartDate.MaxLength = 0;
                colvarPromoStartDate.AutoIncrement = false;
                colvarPromoStartDate.IsNullable = true;
                colvarPromoStartDate.IsPrimaryKey = false;
                colvarPromoStartDate.IsForeignKey = false;
                colvarPromoStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoStartDate);
                
                TableSchema.TableColumn colvarPromoEndDate = new TableSchema.TableColumn(schema);
                colvarPromoEndDate.ColumnName = "promo_end_date";
                colvarPromoEndDate.DataType = DbType.DateTime;
                colvarPromoEndDate.MaxLength = 0;
                colvarPromoEndDate.AutoIncrement = false;
                colvarPromoEndDate.IsNullable = true;
                colvarPromoEndDate.IsPrimaryKey = false;
                colvarPromoEndDate.IsForeignKey = false;
                colvarPromoEndDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoEndDate);
                
                TableSchema.TableColumn colvarPageCount = new TableSchema.TableColumn(schema);
                colvarPageCount.ColumnName = "page_count";
                colvarPageCount.DataType = DbType.Int32;
                colvarPageCount.MaxLength = 0;
                colvarPageCount.AutoIncrement = false;
                colvarPageCount.IsNullable = true;
                colvarPageCount.IsPrimaryKey = false;
                colvarPageCount.IsForeignKey = false;
                colvarPageCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarPageCount);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarEnable = new TableSchema.TableColumn(schema);
                colvarEnable.ColumnName = "enable";
                colvarEnable.DataType = DbType.Boolean;
                colvarEnable.MaxLength = 0;
                colvarEnable.AutoIncrement = false;
                colvarEnable.IsNullable = true;
                colvarEnable.IsPrimaryKey = false;
                colvarEnable.IsForeignKey = false;
                colvarEnable.IsReadOnly = false;
                
                schema.Columns.Add(colvarEnable);
                
                TableSchema.TableColumn colvarEventType = new TableSchema.TableColumn(schema);
                colvarEventType.ColumnName = "event_type";
                colvarEventType.DataType = DbType.Int32;
                colvarEventType.MaxLength = 0;
                colvarEventType.AutoIncrement = false;
                colvarEventType.IsNullable = true;
                colvarEventType.IsPrimaryKey = false;
                colvarEventType.IsForeignKey = false;
                colvarEventType.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventType);
                
                TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
                colvarStartDate.ColumnName = "start_date";
                colvarStartDate.DataType = DbType.DateTime;
                colvarStartDate.MaxLength = 0;
                colvarStartDate.AutoIncrement = false;
                colvarStartDate.IsNullable = true;
                colvarStartDate.IsPrimaryKey = false;
                colvarStartDate.IsForeignKey = false;
                colvarStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartDate);
                
                TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
                colvarEndDate.ColumnName = "end_date";
                colvarEndDate.DataType = DbType.DateTime;
                colvarEndDate.MaxLength = 0;
                colvarEndDate.AutoIncrement = false;
                colvarEndDate.IsNullable = true;
                colvarEndDate.IsPrimaryKey = false;
                colvarEndDate.IsForeignKey = false;
                colvarEndDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndDate);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarContents = new TableSchema.TableColumn(schema);
                colvarContents.ColumnName = "contents";
                colvarContents.DataType = DbType.String;
                colvarContents.MaxLength = 1000;
                colvarContents.AutoIncrement = false;
                colvarContents.IsNullable = true;
                colvarContents.IsPrimaryKey = false;
                colvarContents.IsForeignKey = false;
                colvarContents.IsReadOnly = false;
                
                schema.Columns.Add(colvarContents);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_yahoo_voucher_promo",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewYahooVoucherPromo()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewYahooVoucherPromo(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewYahooVoucherPromo(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewYahooVoucherPromo(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("EventId")]
        [Bindable(true)]
        public int? EventId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("event_id");
		    }
            set 
		    {
			    SetColumnValue("event_id", value);
            }
        }
	      
        [XmlAttribute("Tag")]
        [Bindable(true)]
        public string Tag 
	    {
		    get
		    {
			    return GetColumnValue<string>("tag");
		    }
            set 
		    {
			    SetColumnValue("tag", value);
            }
        }
	      
        [XmlAttribute("Content")]
        [Bindable(true)]
        public string Content 
	    {
		    get
		    {
			    return GetColumnValue<string>("content");
		    }
            set 
		    {
			    SetColumnValue("content", value);
            }
        }
	      
        [XmlAttribute("Link")]
        [Bindable(true)]
        public string Link 
	    {
		    get
		    {
			    return GetColumnValue<string>("link");
		    }
            set 
		    {
			    SetColumnValue("link", value);
            }
        }
	      
        [XmlAttribute("Rank")]
        [Bindable(true)]
        public int Rank 
	    {
		    get
		    {
			    return GetColumnValue<int>("rank");
		    }
            set 
		    {
			    SetColumnValue("rank", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("PromoStartDate")]
        [Bindable(true)]
        public DateTime? PromoStartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("promo_start_date");
		    }
            set 
		    {
			    SetColumnValue("promo_start_date", value);
            }
        }
	      
        [XmlAttribute("PromoEndDate")]
        [Bindable(true)]
        public DateTime? PromoEndDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("promo_end_date");
		    }
            set 
		    {
			    SetColumnValue("promo_end_date", value);
            }
        }
	      
        [XmlAttribute("PageCount")]
        [Bindable(true)]
        public int? PageCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("page_count");
		    }
            set 
		    {
			    SetColumnValue("page_count", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int? Status 
	    {
		    get
		    {
			    return GetColumnValue<int?>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("Enable")]
        [Bindable(true)]
        public bool? Enable 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("enable");
		    }
            set 
		    {
			    SetColumnValue("enable", value);
            }
        }
	      
        [XmlAttribute("EventType")]
        [Bindable(true)]
        public int? EventType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("event_type");
		    }
            set 
		    {
			    SetColumnValue("event_type", value);
            }
        }
	      
        [XmlAttribute("StartDate")]
        [Bindable(true)]
        public DateTime? StartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_date");
		    }
            set 
		    {
			    SetColumnValue("start_date", value);
            }
        }
	      
        [XmlAttribute("EndDate")]
        [Bindable(true)]
        public DateTime? EndDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_date");
		    }
            set 
		    {
			    SetColumnValue("end_date", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("Contents")]
        [Bindable(true)]
        public string Contents 
	    {
		    get
		    {
			    return GetColumnValue<string>("contents");
		    }
            set 
		    {
			    SetColumnValue("contents", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string EventId = @"event_id";
            
            public static string Tag = @"tag";
            
            public static string Content = @"content";
            
            public static string Link = @"link";
            
            public static string Rank = @"rank";
            
            public static string Type = @"type";
            
            public static string PromoStartDate = @"promo_start_date";
            
            public static string PromoEndDate = @"promo_end_date";
            
            public static string PageCount = @"page_count";
            
            public static string Status = @"status";
            
            public static string Enable = @"enable";
            
            public static string EventType = @"event_type";
            
            public static string StartDate = @"start_date";
            
            public static string EndDate = @"end_date";
            
            public static string SellerName = @"seller_name";
            
            public static string Contents = @"contents";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

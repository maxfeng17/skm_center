using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Brand class.
	/// </summary>
    [Serializable]
	public partial class BrandCollection : RepositoryList<Brand, BrandCollection>
	{	   
		public BrandCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BrandCollection</returns>
		public BrandCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Brand o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the brand table.
	/// </summary>
	[Serializable]
	public partial class Brand : RepositoryRecord<Brand>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Brand()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Brand(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("brand", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBrandName = new TableSchema.TableColumn(schema);
				colvarBrandName.ColumnName = "brand_name";
				colvarBrandName.DataType = DbType.String;
				colvarBrandName.MaxLength = 200;
				colvarBrandName.AutoIncrement = false;
				colvarBrandName.IsNullable = false;
				colvarBrandName.IsPrimaryKey = false;
				colvarBrandName.IsForeignKey = false;
				colvarBrandName.IsReadOnly = false;
				colvarBrandName.DefaultSetting = @"";
				colvarBrandName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandName);
				
				TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
				colvarUrl.ColumnName = "url";
				colvarUrl.DataType = DbType.String;
				colvarUrl.MaxLength = 100;
				colvarUrl.AutoIncrement = false;
				colvarUrl.IsNullable = false;
				colvarUrl.IsPrimaryKey = false;
				colvarUrl.IsForeignKey = false;
				colvarUrl.IsReadOnly = false;
				colvarUrl.DefaultSetting = @"";
				colvarUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUrl);
				
				TableSchema.TableColumn colvarCpa = new TableSchema.TableColumn(schema);
				colvarCpa.ColumnName = "cpa";
				colvarCpa.DataType = DbType.String;
				colvarCpa.MaxLength = 100;
				colvarCpa.AutoIncrement = false;
				colvarCpa.IsNullable = true;
				colvarCpa.IsPrimaryKey = false;
				colvarCpa.IsForeignKey = false;
				colvarCpa.IsReadOnly = false;
				colvarCpa.DefaultSetting = @"";
				colvarCpa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCpa);
				
				TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
				colvarStartTime.ColumnName = "start_time";
				colvarStartTime.DataType = DbType.DateTime;
				colvarStartTime.MaxLength = 0;
				colvarStartTime.AutoIncrement = false;
				colvarStartTime.IsNullable = false;
				colvarStartTime.IsPrimaryKey = false;
				colvarStartTime.IsForeignKey = false;
				colvarStartTime.IsReadOnly = false;
				colvarStartTime.DefaultSetting = @"";
				colvarStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartTime);
				
				TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
				colvarEndTime.ColumnName = "end_time";
				colvarEndTime.DataType = DbType.DateTime;
				colvarEndTime.MaxLength = 0;
				colvarEndTime.AutoIncrement = false;
				colvarEndTime.IsNullable = false;
				colvarEndTime.IsPrimaryKey = false;
				colvarEndTime.IsForeignKey = false;
				colvarEndTime.IsReadOnly = false;
				colvarEndTime.DefaultSetting = @"";
				colvarEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndTime);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Boolean;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((1))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
				colvarCreator.ColumnName = "creator";
				colvarCreator.DataType = DbType.String;
				colvarCreator.MaxLength = 100;
				colvarCreator.AutoIncrement = false;
				colvarCreator.IsNullable = false;
				colvarCreator.IsPrimaryKey = false;
				colvarCreator.IsForeignKey = false;
				colvarCreator.IsReadOnly = false;
				colvarCreator.DefaultSetting = @"";
				colvarCreator.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreator);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarMainPic = new TableSchema.TableColumn(schema);
				colvarMainPic.ColumnName = "main_pic";
				colvarMainPic.DataType = DbType.String;
				colvarMainPic.MaxLength = 1073741823;
				colvarMainPic.AutoIncrement = false;
				colvarMainPic.IsNullable = false;
				colvarMainPic.IsPrimaryKey = false;
				colvarMainPic.IsForeignKey = false;
				colvarMainPic.IsReadOnly = false;
				colvarMainPic.DefaultSetting = @"";
				colvarMainPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainPic);
				
				TableSchema.TableColumn colvarMobileMainPic = new TableSchema.TableColumn(schema);
				colvarMobileMainPic.ColumnName = "mobile_main_pic";
				colvarMobileMainPic.DataType = DbType.String;
				colvarMobileMainPic.MaxLength = 1073741823;
				colvarMobileMainPic.AutoIncrement = false;
				colvarMobileMainPic.IsNullable = false;
				colvarMobileMainPic.IsPrimaryKey = false;
				colvarMobileMainPic.IsForeignKey = false;
				colvarMobileMainPic.IsReadOnly = false;
				colvarMobileMainPic.DefaultSetting = @"";
				colvarMobileMainPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileMainPic);
				
				TableSchema.TableColumn colvarShowInWeb = new TableSchema.TableColumn(schema);
				colvarShowInWeb.ColumnName = "show_in_web";
				colvarShowInWeb.DataType = DbType.Boolean;
				colvarShowInWeb.MaxLength = 0;
				colvarShowInWeb.AutoIncrement = false;
				colvarShowInWeb.IsNullable = false;
				colvarShowInWeb.IsPrimaryKey = false;
				colvarShowInWeb.IsForeignKey = false;
				colvarShowInWeb.IsReadOnly = false;
				
						colvarShowInWeb.DefaultSetting = @"((1))";
				colvarShowInWeb.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShowInWeb);
				
				TableSchema.TableColumn colvarShowInApp = new TableSchema.TableColumn(schema);
				colvarShowInApp.ColumnName = "show_in_app";
				colvarShowInApp.DataType = DbType.Boolean;
				colvarShowInApp.MaxLength = 0;
				colvarShowInApp.AutoIncrement = false;
				colvarShowInApp.IsNullable = false;
				colvarShowInApp.IsPrimaryKey = false;
				colvarShowInApp.IsForeignKey = false;
				colvarShowInApp.IsReadOnly = false;
				
						colvarShowInApp.DefaultSetting = @"((1))";
				colvarShowInApp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShowInApp);
				
				TableSchema.TableColumn colvarBgpic = new TableSchema.TableColumn(schema);
				colvarBgpic.ColumnName = "bgpic";
				colvarBgpic.DataType = DbType.String;
				colvarBgpic.MaxLength = 200;
				colvarBgpic.AutoIncrement = false;
				colvarBgpic.IsNullable = true;
				colvarBgpic.IsPrimaryKey = false;
				colvarBgpic.IsForeignKey = false;
				colvarBgpic.IsReadOnly = false;
				colvarBgpic.DefaultSetting = @"";
				colvarBgpic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBgpic);
				
				TableSchema.TableColumn colvarBgcolor = new TableSchema.TableColumn(schema);
				colvarBgcolor.ColumnName = "bgcolor";
				colvarBgcolor.DataType = DbType.String;
				colvarBgcolor.MaxLength = 6;
				colvarBgcolor.AutoIncrement = false;
				colvarBgcolor.IsNullable = true;
				colvarBgcolor.IsPrimaryKey = false;
				colvarBgcolor.IsForeignKey = false;
				colvarBgcolor.IsReadOnly = false;
				colvarBgcolor.DefaultSetting = @"";
				colvarBgcolor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBgcolor);
				
				TableSchema.TableColumn colvarAppBannerImage = new TableSchema.TableColumn(schema);
				colvarAppBannerImage.ColumnName = "app_banner_image";
				colvarAppBannerImage.DataType = DbType.String;
				colvarAppBannerImage.MaxLength = 200;
				colvarAppBannerImage.AutoIncrement = false;
				colvarAppBannerImage.IsNullable = true;
				colvarAppBannerImage.IsPrimaryKey = false;
				colvarAppBannerImage.IsForeignKey = false;
				colvarAppBannerImage.IsReadOnly = false;
				colvarAppBannerImage.DefaultSetting = @"";
				colvarAppBannerImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppBannerImage);
				
				TableSchema.TableColumn colvarAppPromoImage = new TableSchema.TableColumn(schema);
				colvarAppPromoImage.ColumnName = "app_promo_Image";
				colvarAppPromoImage.DataType = DbType.String;
				colvarAppPromoImage.MaxLength = 200;
				colvarAppPromoImage.AutoIncrement = false;
				colvarAppPromoImage.IsNullable = true;
				colvarAppPromoImage.IsPrimaryKey = false;
				colvarAppPromoImage.IsForeignKey = false;
				colvarAppPromoImage.IsReadOnly = false;
				colvarAppPromoImage.DefaultSetting = @"";
				colvarAppPromoImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppPromoImage);
				
				TableSchema.TableColumn colvarMk1ActName = new TableSchema.TableColumn(schema);
				colvarMk1ActName.ColumnName = "mk1_act_name";
				colvarMk1ActName.DataType = DbType.String;
				colvarMk1ActName.MaxLength = 50;
				colvarMk1ActName.AutoIncrement = false;
				colvarMk1ActName.IsNullable = true;
				colvarMk1ActName.IsPrimaryKey = false;
				colvarMk1ActName.IsForeignKey = false;
				colvarMk1ActName.IsReadOnly = false;
				colvarMk1ActName.DefaultSetting = @"";
				colvarMk1ActName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk1ActName);
				
				TableSchema.TableColumn colvarMk1Url = new TableSchema.TableColumn(schema);
				colvarMk1Url.ColumnName = "mk1_url";
				colvarMk1Url.DataType = DbType.String;
				colvarMk1Url.MaxLength = 200;
				colvarMk1Url.AutoIncrement = false;
				colvarMk1Url.IsNullable = true;
				colvarMk1Url.IsPrimaryKey = false;
				colvarMk1Url.IsForeignKey = false;
				colvarMk1Url.IsReadOnly = false;
				colvarMk1Url.DefaultSetting = @"";
				colvarMk1Url.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk1Url);
				
				TableSchema.TableColumn colvarMk1WebImage = new TableSchema.TableColumn(schema);
				colvarMk1WebImage.ColumnName = "mk1_web_image";
				colvarMk1WebImage.DataType = DbType.String;
				colvarMk1WebImage.MaxLength = 200;
				colvarMk1WebImage.AutoIncrement = false;
				colvarMk1WebImage.IsNullable = true;
				colvarMk1WebImage.IsPrimaryKey = false;
				colvarMk1WebImage.IsForeignKey = false;
				colvarMk1WebImage.IsReadOnly = false;
				colvarMk1WebImage.DefaultSetting = @"";
				colvarMk1WebImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk1WebImage);
				
				TableSchema.TableColumn colvarMk1AppImage = new TableSchema.TableColumn(schema);
				colvarMk1AppImage.ColumnName = "mk1_app_image";
				colvarMk1AppImage.DataType = DbType.String;
				colvarMk1AppImage.MaxLength = 200;
				colvarMk1AppImage.AutoIncrement = false;
				colvarMk1AppImage.IsNullable = true;
				colvarMk1AppImage.IsPrimaryKey = false;
				colvarMk1AppImage.IsForeignKey = false;
				colvarMk1AppImage.IsReadOnly = false;
				colvarMk1AppImage.DefaultSetting = @"";
				colvarMk1AppImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk1AppImage);
				
				TableSchema.TableColumn colvarMk2ActName = new TableSchema.TableColumn(schema);
				colvarMk2ActName.ColumnName = "mk2_act_name";
				colvarMk2ActName.DataType = DbType.String;
				colvarMk2ActName.MaxLength = 50;
				colvarMk2ActName.AutoIncrement = false;
				colvarMk2ActName.IsNullable = true;
				colvarMk2ActName.IsPrimaryKey = false;
				colvarMk2ActName.IsForeignKey = false;
				colvarMk2ActName.IsReadOnly = false;
				colvarMk2ActName.DefaultSetting = @"";
				colvarMk2ActName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk2ActName);
				
				TableSchema.TableColumn colvarMk2Url = new TableSchema.TableColumn(schema);
				colvarMk2Url.ColumnName = "mk2_url";
				colvarMk2Url.DataType = DbType.String;
				colvarMk2Url.MaxLength = 200;
				colvarMk2Url.AutoIncrement = false;
				colvarMk2Url.IsNullable = true;
				colvarMk2Url.IsPrimaryKey = false;
				colvarMk2Url.IsForeignKey = false;
				colvarMk2Url.IsReadOnly = false;
				colvarMk2Url.DefaultSetting = @"";
				colvarMk2Url.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk2Url);
				
				TableSchema.TableColumn colvarMk2WebImage = new TableSchema.TableColumn(schema);
				colvarMk2WebImage.ColumnName = "mk2_web_image";
				colvarMk2WebImage.DataType = DbType.String;
				colvarMk2WebImage.MaxLength = 200;
				colvarMk2WebImage.AutoIncrement = false;
				colvarMk2WebImage.IsNullable = true;
				colvarMk2WebImage.IsPrimaryKey = false;
				colvarMk2WebImage.IsForeignKey = false;
				colvarMk2WebImage.IsReadOnly = false;
				colvarMk2WebImage.DefaultSetting = @"";
				colvarMk2WebImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk2WebImage);
				
				TableSchema.TableColumn colvarMk2AppImage = new TableSchema.TableColumn(schema);
				colvarMk2AppImage.ColumnName = "mk2_app_image";
				colvarMk2AppImage.DataType = DbType.String;
				colvarMk2AppImage.MaxLength = 200;
				colvarMk2AppImage.AutoIncrement = false;
				colvarMk2AppImage.IsNullable = true;
				colvarMk2AppImage.IsPrimaryKey = false;
				colvarMk2AppImage.IsForeignKey = false;
				colvarMk2AppImage.IsReadOnly = false;
				colvarMk2AppImage.DefaultSetting = @"";
				colvarMk2AppImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk2AppImage);
				
				TableSchema.TableColumn colvarMk3ActName = new TableSchema.TableColumn(schema);
				colvarMk3ActName.ColumnName = "mk3_act_name";
				colvarMk3ActName.DataType = DbType.String;
				colvarMk3ActName.MaxLength = 50;
				colvarMk3ActName.AutoIncrement = false;
				colvarMk3ActName.IsNullable = true;
				colvarMk3ActName.IsPrimaryKey = false;
				colvarMk3ActName.IsForeignKey = false;
				colvarMk3ActName.IsReadOnly = false;
				colvarMk3ActName.DefaultSetting = @"";
				colvarMk3ActName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk3ActName);
				
				TableSchema.TableColumn colvarMk3Url = new TableSchema.TableColumn(schema);
				colvarMk3Url.ColumnName = "mk3_url";
				colvarMk3Url.DataType = DbType.String;
				colvarMk3Url.MaxLength = 200;
				colvarMk3Url.AutoIncrement = false;
				colvarMk3Url.IsNullable = true;
				colvarMk3Url.IsPrimaryKey = false;
				colvarMk3Url.IsForeignKey = false;
				colvarMk3Url.IsReadOnly = false;
				colvarMk3Url.DefaultSetting = @"";
				colvarMk3Url.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk3Url);
				
				TableSchema.TableColumn colvarMk3WebImage = new TableSchema.TableColumn(schema);
				colvarMk3WebImage.ColumnName = "mk3_web_image";
				colvarMk3WebImage.DataType = DbType.String;
				colvarMk3WebImage.MaxLength = 200;
				colvarMk3WebImage.AutoIncrement = false;
				colvarMk3WebImage.IsNullable = true;
				colvarMk3WebImage.IsPrimaryKey = false;
				colvarMk3WebImage.IsForeignKey = false;
				colvarMk3WebImage.IsReadOnly = false;
				colvarMk3WebImage.DefaultSetting = @"";
				colvarMk3WebImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk3WebImage);
				
				TableSchema.TableColumn colvarMk3AppImage = new TableSchema.TableColumn(schema);
				colvarMk3AppImage.ColumnName = "mk3_app_image";
				colvarMk3AppImage.DataType = DbType.String;
				colvarMk3AppImage.MaxLength = 200;
				colvarMk3AppImage.AutoIncrement = false;
				colvarMk3AppImage.IsNullable = true;
				colvarMk3AppImage.IsPrimaryKey = false;
				colvarMk3AppImage.IsForeignKey = false;
				colvarMk3AppImage.IsReadOnly = false;
				colvarMk3AppImage.DefaultSetting = @"";
				colvarMk3AppImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk3AppImage);
				
				TableSchema.TableColumn colvarMk4ActName = new TableSchema.TableColumn(schema);
				colvarMk4ActName.ColumnName = "mk4_act_name";
				colvarMk4ActName.DataType = DbType.String;
				colvarMk4ActName.MaxLength = 50;
				colvarMk4ActName.AutoIncrement = false;
				colvarMk4ActName.IsNullable = true;
				colvarMk4ActName.IsPrimaryKey = false;
				colvarMk4ActName.IsForeignKey = false;
				colvarMk4ActName.IsReadOnly = false;
				colvarMk4ActName.DefaultSetting = @"";
				colvarMk4ActName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk4ActName);
				
				TableSchema.TableColumn colvarMk4Url = new TableSchema.TableColumn(schema);
				colvarMk4Url.ColumnName = "mk4_url";
				colvarMk4Url.DataType = DbType.String;
				colvarMk4Url.MaxLength = 200;
				colvarMk4Url.AutoIncrement = false;
				colvarMk4Url.IsNullable = true;
				colvarMk4Url.IsPrimaryKey = false;
				colvarMk4Url.IsForeignKey = false;
				colvarMk4Url.IsReadOnly = false;
				colvarMk4Url.DefaultSetting = @"";
				colvarMk4Url.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk4Url);
				
				TableSchema.TableColumn colvarMk4WebImage = new TableSchema.TableColumn(schema);
				colvarMk4WebImage.ColumnName = "mk4_web_image";
				colvarMk4WebImage.DataType = DbType.String;
				colvarMk4WebImage.MaxLength = 200;
				colvarMk4WebImage.AutoIncrement = false;
				colvarMk4WebImage.IsNullable = true;
				colvarMk4WebImage.IsPrimaryKey = false;
				colvarMk4WebImage.IsForeignKey = false;
				colvarMk4WebImage.IsReadOnly = false;
				colvarMk4WebImage.DefaultSetting = @"";
				colvarMk4WebImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk4WebImage);
				
				TableSchema.TableColumn colvarMk4AppImage = new TableSchema.TableColumn(schema);
				colvarMk4AppImage.ColumnName = "mk4_app_image";
				colvarMk4AppImage.DataType = DbType.String;
				colvarMk4AppImage.MaxLength = 200;
				colvarMk4AppImage.AutoIncrement = false;
				colvarMk4AppImage.IsNullable = true;
				colvarMk4AppImage.IsPrimaryKey = false;
				colvarMk4AppImage.IsForeignKey = false;
				colvarMk4AppImage.IsReadOnly = false;
				colvarMk4AppImage.DefaultSetting = @"";
				colvarMk4AppImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk4AppImage);
				
				TableSchema.TableColumn colvarMk5ActName = new TableSchema.TableColumn(schema);
				colvarMk5ActName.ColumnName = "mk5_act_name";
				colvarMk5ActName.DataType = DbType.String;
				colvarMk5ActName.MaxLength = 50;
				colvarMk5ActName.AutoIncrement = false;
				colvarMk5ActName.IsNullable = true;
				colvarMk5ActName.IsPrimaryKey = false;
				colvarMk5ActName.IsForeignKey = false;
				colvarMk5ActName.IsReadOnly = false;
				colvarMk5ActName.DefaultSetting = @"";
				colvarMk5ActName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk5ActName);
				
				TableSchema.TableColumn colvarMk5Url = new TableSchema.TableColumn(schema);
				colvarMk5Url.ColumnName = "mk5_url";
				colvarMk5Url.DataType = DbType.String;
				colvarMk5Url.MaxLength = 200;
				colvarMk5Url.AutoIncrement = false;
				colvarMk5Url.IsNullable = true;
				colvarMk5Url.IsPrimaryKey = false;
				colvarMk5Url.IsForeignKey = false;
				colvarMk5Url.IsReadOnly = false;
				colvarMk5Url.DefaultSetting = @"";
				colvarMk5Url.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk5Url);
				
				TableSchema.TableColumn colvarMk5WebImage = new TableSchema.TableColumn(schema);
				colvarMk5WebImage.ColumnName = "mk5_web_image";
				colvarMk5WebImage.DataType = DbType.String;
				colvarMk5WebImage.MaxLength = 200;
				colvarMk5WebImage.AutoIncrement = false;
				colvarMk5WebImage.IsNullable = true;
				colvarMk5WebImage.IsPrimaryKey = false;
				colvarMk5WebImage.IsForeignKey = false;
				colvarMk5WebImage.IsReadOnly = false;
				colvarMk5WebImage.DefaultSetting = @"";
				colvarMk5WebImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk5WebImage);
				
				TableSchema.TableColumn colvarMk5AppImage = new TableSchema.TableColumn(schema);
				colvarMk5AppImage.ColumnName = "mk5_app_image";
				colvarMk5AppImage.DataType = DbType.String;
				colvarMk5AppImage.MaxLength = 200;
				colvarMk5AppImage.AutoIncrement = false;
				colvarMk5AppImage.IsNullable = true;
				colvarMk5AppImage.IsPrimaryKey = false;
				colvarMk5AppImage.IsForeignKey = false;
				colvarMk5AppImage.IsReadOnly = false;
				colvarMk5AppImage.DefaultSetting = @"";
				colvarMk5AppImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMk5AppImage);
				
				TableSchema.TableColumn colvarHotItemBid1 = new TableSchema.TableColumn(schema);
				colvarHotItemBid1.ColumnName = "hot_item_bid1";
				colvarHotItemBid1.DataType = DbType.Guid;
				colvarHotItemBid1.MaxLength = 0;
				colvarHotItemBid1.AutoIncrement = false;
				colvarHotItemBid1.IsNullable = true;
				colvarHotItemBid1.IsPrimaryKey = false;
				colvarHotItemBid1.IsForeignKey = false;
				colvarHotItemBid1.IsReadOnly = false;
				colvarHotItemBid1.DefaultSetting = @"";
				colvarHotItemBid1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHotItemBid1);
				
				TableSchema.TableColumn colvarHotItemBid2 = new TableSchema.TableColumn(schema);
				colvarHotItemBid2.ColumnName = "hot_item_bid2";
				colvarHotItemBid2.DataType = DbType.Guid;
				colvarHotItemBid2.MaxLength = 0;
				colvarHotItemBid2.AutoIncrement = false;
				colvarHotItemBid2.IsNullable = true;
				colvarHotItemBid2.IsPrimaryKey = false;
				colvarHotItemBid2.IsForeignKey = false;
				colvarHotItemBid2.IsReadOnly = false;
				colvarHotItemBid2.DefaultSetting = @"";
				colvarHotItemBid2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHotItemBid2);
				
				TableSchema.TableColumn colvarHotItemBid3 = new TableSchema.TableColumn(schema);
				colvarHotItemBid3.ColumnName = "hot_item_bid3";
				colvarHotItemBid3.DataType = DbType.Guid;
				colvarHotItemBid3.MaxLength = 0;
				colvarHotItemBid3.AutoIncrement = false;
				colvarHotItemBid3.IsNullable = true;
				colvarHotItemBid3.IsPrimaryKey = false;
				colvarHotItemBid3.IsForeignKey = false;
				colvarHotItemBid3.IsReadOnly = false;
				colvarHotItemBid3.DefaultSetting = @"";
				colvarHotItemBid3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHotItemBid3);
				
				TableSchema.TableColumn colvarHotItemBid4 = new TableSchema.TableColumn(schema);
				colvarHotItemBid4.ColumnName = "hot_item_bid4";
				colvarHotItemBid4.DataType = DbType.Guid;
				colvarHotItemBid4.MaxLength = 0;
				colvarHotItemBid4.AutoIncrement = false;
				colvarHotItemBid4.IsNullable = true;
				colvarHotItemBid4.IsPrimaryKey = false;
				colvarHotItemBid4.IsForeignKey = false;
				colvarHotItemBid4.IsReadOnly = false;
				colvarHotItemBid4.DefaultSetting = @"";
				colvarHotItemBid4.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHotItemBid4);
				
				TableSchema.TableColumn colvarBtnOriginalUrl = new TableSchema.TableColumn(schema);
				colvarBtnOriginalUrl.ColumnName = "btn_original_url";
				colvarBtnOriginalUrl.DataType = DbType.String;
				colvarBtnOriginalUrl.MaxLength = 200;
				colvarBtnOriginalUrl.AutoIncrement = false;
				colvarBtnOriginalUrl.IsNullable = true;
				colvarBtnOriginalUrl.IsPrimaryKey = false;
				colvarBtnOriginalUrl.IsForeignKey = false;
				colvarBtnOriginalUrl.IsReadOnly = false;
				colvarBtnOriginalUrl.DefaultSetting = @"";
				colvarBtnOriginalUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnOriginalUrl);
				
				TableSchema.TableColumn colvarBtnOriginalFontColor = new TableSchema.TableColumn(schema);
				colvarBtnOriginalFontColor.ColumnName = "btn_original_font_color";
				colvarBtnOriginalFontColor.DataType = DbType.String;
				colvarBtnOriginalFontColor.MaxLength = 6;
				colvarBtnOriginalFontColor.AutoIncrement = false;
				colvarBtnOriginalFontColor.IsNullable = true;
				colvarBtnOriginalFontColor.IsPrimaryKey = false;
				colvarBtnOriginalFontColor.IsForeignKey = false;
				colvarBtnOriginalFontColor.IsReadOnly = false;
				colvarBtnOriginalFontColor.DefaultSetting = @"";
				colvarBtnOriginalFontColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnOriginalFontColor);
				
				TableSchema.TableColumn colvarBtnHoverUrl = new TableSchema.TableColumn(schema);
				colvarBtnHoverUrl.ColumnName = "btn_hover_url";
				colvarBtnHoverUrl.DataType = DbType.String;
				colvarBtnHoverUrl.MaxLength = 200;
				colvarBtnHoverUrl.AutoIncrement = false;
				colvarBtnHoverUrl.IsNullable = true;
				colvarBtnHoverUrl.IsPrimaryKey = false;
				colvarBtnHoverUrl.IsForeignKey = false;
				colvarBtnHoverUrl.IsReadOnly = false;
				colvarBtnHoverUrl.DefaultSetting = @"";
				colvarBtnHoverUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnHoverUrl);
				
				TableSchema.TableColumn colvarBtnHoverFontColor = new TableSchema.TableColumn(schema);
				colvarBtnHoverFontColor.ColumnName = "btn_hover_font_color";
				colvarBtnHoverFontColor.DataType = DbType.String;
				colvarBtnHoverFontColor.MaxLength = 6;
				colvarBtnHoverFontColor.AutoIncrement = false;
				colvarBtnHoverFontColor.IsNullable = true;
				colvarBtnHoverFontColor.IsPrimaryKey = false;
				colvarBtnHoverFontColor.IsForeignKey = false;
				colvarBtnHoverFontColor.IsReadOnly = false;
				colvarBtnHoverFontColor.DefaultSetting = @"";
				colvarBtnHoverFontColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnHoverFontColor);
				
				TableSchema.TableColumn colvarBtnActiveUrl = new TableSchema.TableColumn(schema);
				colvarBtnActiveUrl.ColumnName = "btn_active_url";
				colvarBtnActiveUrl.DataType = DbType.String;
				colvarBtnActiveUrl.MaxLength = 200;
				colvarBtnActiveUrl.AutoIncrement = false;
				colvarBtnActiveUrl.IsNullable = true;
				colvarBtnActiveUrl.IsPrimaryKey = false;
				colvarBtnActiveUrl.IsForeignKey = false;
				colvarBtnActiveUrl.IsReadOnly = false;
				colvarBtnActiveUrl.DefaultSetting = @"";
				colvarBtnActiveUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnActiveUrl);
				
				TableSchema.TableColumn colvarBtnActiveFontColor = new TableSchema.TableColumn(schema);
				colvarBtnActiveFontColor.ColumnName = "btn_active_font_color";
				colvarBtnActiveFontColor.DataType = DbType.String;
				colvarBtnActiveFontColor.MaxLength = 6;
				colvarBtnActiveFontColor.AutoIncrement = false;
				colvarBtnActiveFontColor.IsNullable = true;
				colvarBtnActiveFontColor.IsPrimaryKey = false;
				colvarBtnActiveFontColor.IsForeignKey = false;
				colvarBtnActiveFontColor.IsReadOnly = false;
				colvarBtnActiveFontColor.DefaultSetting = @"";
				colvarBtnActiveFontColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnActiveFontColor);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = -1;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarDealPromoTitle = new TableSchema.TableColumn(schema);
				colvarDealPromoTitle.ColumnName = "deal_promo_title";
				colvarDealPromoTitle.DataType = DbType.String;
				colvarDealPromoTitle.MaxLength = 50;
				colvarDealPromoTitle.AutoIncrement = false;
				colvarDealPromoTitle.IsNullable = true;
				colvarDealPromoTitle.IsPrimaryKey = false;
				colvarDealPromoTitle.IsForeignKey = false;
				colvarDealPromoTitle.IsReadOnly = false;
				colvarDealPromoTitle.DefaultSetting = @"";
				colvarDealPromoTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealPromoTitle);
				
				TableSchema.TableColumn colvarDealPromoImage = new TableSchema.TableColumn(schema);
				colvarDealPromoImage.ColumnName = "deal_promo_image";
				colvarDealPromoImage.DataType = DbType.String;
				colvarDealPromoImage.MaxLength = 120;
				colvarDealPromoImage.AutoIncrement = false;
				colvarDealPromoImage.IsNullable = true;
				colvarDealPromoImage.IsPrimaryKey = false;
				colvarDealPromoImage.IsForeignKey = false;
				colvarDealPromoImage.IsReadOnly = false;
				colvarDealPromoImage.DefaultSetting = @"";
				colvarDealPromoImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealPromoImage);
				
				TableSchema.TableColumn colvarDealPromoDescription = new TableSchema.TableColumn(schema);
				colvarDealPromoDescription.ColumnName = "deal_promo_description";
				colvarDealPromoDescription.DataType = DbType.String;
				colvarDealPromoDescription.MaxLength = -1;
				colvarDealPromoDescription.AutoIncrement = false;
				colvarDealPromoDescription.IsNullable = true;
				colvarDealPromoDescription.IsPrimaryKey = false;
				colvarDealPromoDescription.IsForeignKey = false;
				colvarDealPromoDescription.IsReadOnly = false;
				colvarDealPromoDescription.DefaultSetting = @"";
				colvarDealPromoDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealPromoDescription);
				
				TableSchema.TableColumn colvarBannerType = new TableSchema.TableColumn(schema);
				colvarBannerType.ColumnName = "banner_type";
				colvarBannerType.DataType = DbType.Int32;
				colvarBannerType.MaxLength = 0;
				colvarBannerType.AutoIncrement = false;
				colvarBannerType.IsNullable = false;
				colvarBannerType.IsPrimaryKey = false;
				colvarBannerType.IsForeignKey = false;
				colvarBannerType.IsReadOnly = false;
				
						colvarBannerType.DefaultSetting = @"((0))";
				colvarBannerType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBannerType);
				
				TableSchema.TableColumn colvarDiscountList = new TableSchema.TableColumn(schema);
				colvarDiscountList.ColumnName = "discount_list";
				colvarDiscountList.DataType = DbType.AnsiString;
				colvarDiscountList.MaxLength = 200;
				colvarDiscountList.AutoIncrement = false;
				colvarDiscountList.IsNullable = true;
				colvarDiscountList.IsPrimaryKey = false;
				colvarDiscountList.IsForeignKey = false;
				colvarDiscountList.IsReadOnly = false;
				colvarDiscountList.DefaultSetting = @"";
				colvarDiscountList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountList);
				
				TableSchema.TableColumn colvarDiscountBannerImage = new TableSchema.TableColumn(schema);
				colvarDiscountBannerImage.ColumnName = "discount_banner_image";
				colvarDiscountBannerImage.DataType = DbType.String;
				colvarDiscountBannerImage.MaxLength = 200;
				colvarDiscountBannerImage.AutoIncrement = false;
				colvarDiscountBannerImage.IsNullable = true;
				colvarDiscountBannerImage.IsPrimaryKey = false;
				colvarDiscountBannerImage.IsForeignKey = false;
				colvarDiscountBannerImage.IsReadOnly = false;
				colvarDiscountBannerImage.DefaultSetting = @"";
				colvarDiscountBannerImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountBannerImage);
				
				TableSchema.TableColumn colvarEventListImage = new TableSchema.TableColumn(schema);
				colvarEventListImage.ColumnName = "event_list_image";
				colvarEventListImage.DataType = DbType.String;
				colvarEventListImage.MaxLength = 200;
				colvarEventListImage.AutoIncrement = false;
				colvarEventListImage.IsNullable = true;
				colvarEventListImage.IsPrimaryKey = false;
				colvarEventListImage.IsForeignKey = false;
				colvarEventListImage.IsReadOnly = false;
				colvarEventListImage.DefaultSetting = @"";
				colvarEventListImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventListImage);
				
				TableSchema.TableColumn colvarFbShareImage = new TableSchema.TableColumn(schema);
				colvarFbShareImage.ColumnName = "fb_share_image";
				colvarFbShareImage.DataType = DbType.String;
				colvarFbShareImage.MaxLength = 200;
				colvarFbShareImage.AutoIncrement = false;
				colvarFbShareImage.IsNullable = true;
				colvarFbShareImage.IsPrimaryKey = false;
				colvarFbShareImage.IsForeignKey = false;
				colvarFbShareImage.IsReadOnly = false;
				colvarFbShareImage.DefaultSetting = @"";
				colvarFbShareImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFbShareImage);
				
				TableSchema.TableColumn colvarChannelListImage = new TableSchema.TableColumn(schema);
				colvarChannelListImage.ColumnName = "channel_list_image";
				colvarChannelListImage.DataType = DbType.String;
				colvarChannelListImage.MaxLength = 200;
				colvarChannelListImage.AutoIncrement = false;
				colvarChannelListImage.IsNullable = true;
				colvarChannelListImage.IsPrimaryKey = false;
				colvarChannelListImage.IsForeignKey = false;
				colvarChannelListImage.IsReadOnly = false;
				colvarChannelListImage.DefaultSetting = @"";
				colvarChannelListImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChannelListImage);
				
				TableSchema.TableColumn colvarWebRelayImage = new TableSchema.TableColumn(schema);
				colvarWebRelayImage.ColumnName = "web_relay_image";
				colvarWebRelayImage.DataType = DbType.String;
				colvarWebRelayImage.MaxLength = 200;
				colvarWebRelayImage.AutoIncrement = false;
				colvarWebRelayImage.IsNullable = true;
				colvarWebRelayImage.IsPrimaryKey = false;
				colvarWebRelayImage.IsForeignKey = false;
				colvarWebRelayImage.IsReadOnly = false;
				colvarWebRelayImage.DefaultSetting = @"";
				colvarWebRelayImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWebRelayImage);
				
				TableSchema.TableColumn colvarMobileRelayImage = new TableSchema.TableColumn(schema);
				colvarMobileRelayImage.ColumnName = "mobile_relay_image";
				colvarMobileRelayImage.DataType = DbType.String;
				colvarMobileRelayImage.MaxLength = 200;
				colvarMobileRelayImage.AutoIncrement = false;
				colvarMobileRelayImage.IsNullable = true;
				colvarMobileRelayImage.IsPrimaryKey = false;
				colvarMobileRelayImage.IsForeignKey = false;
				colvarMobileRelayImage.IsReadOnly = false;
				colvarMobileRelayImage.DefaultSetting = @"";
				colvarMobileRelayImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileRelayImage);
				
				TableSchema.TableColumn colvarMainBackgroundPic = new TableSchema.TableColumn(schema);
				colvarMainBackgroundPic.ColumnName = "main_background_pic";
				colvarMainBackgroundPic.DataType = DbType.String;
				colvarMainBackgroundPic.MaxLength = 200;
				colvarMainBackgroundPic.AutoIncrement = false;
				colvarMainBackgroundPic.IsNullable = true;
				colvarMainBackgroundPic.IsPrimaryKey = false;
				colvarMainBackgroundPic.IsForeignKey = false;
				colvarMainBackgroundPic.IsReadOnly = false;
				colvarMainBackgroundPic.DefaultSetting = @"";
				colvarMainBackgroundPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainBackgroundPic);

			    TableSchema.TableColumn colvarMetaDescription = new TableSchema.TableColumn(schema);
			    colvarMetaDescription.ColumnName = "meta_description";
			    colvarMetaDescription.DataType = DbType.String;
			    colvarMetaDescription.MaxLength = 200;
			    colvarMetaDescription.AutoIncrement = false;
			    colvarMetaDescription.IsNullable = true;
			    colvarMetaDescription.IsPrimaryKey = false;
			    colvarMetaDescription.IsForeignKey = false;
			    colvarMetaDescription.IsReadOnly = false;
			    colvarMetaDescription.DefaultSetting = @"";
                colvarMetaDescription.ForeignKeyTableName = "";
			    schema.Columns.Add(colvarMetaDescription);

                BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("brand",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("BrandName")]
		[Bindable(true)]
		public string BrandName 
		{
			get { return GetColumnValue<string>(Columns.BrandName); }
			set { SetColumnValue(Columns.BrandName, value); }
		}
		  
		[XmlAttribute("Url")]
		[Bindable(true)]
		public string Url 
		{
			get { return GetColumnValue<string>(Columns.Url); }
			set { SetColumnValue(Columns.Url, value); }
		}
		  
		[XmlAttribute("Cpa")]
		[Bindable(true)]
		public string Cpa 
		{
			get { return GetColumnValue<string>(Columns.Cpa); }
			set { SetColumnValue(Columns.Cpa, value); }
		}
		  
		[XmlAttribute("StartTime")]
		[Bindable(true)]
		public DateTime StartTime 
		{
			get { return GetColumnValue<DateTime>(Columns.StartTime); }
			set { SetColumnValue(Columns.StartTime, value); }
		}
		  
		[XmlAttribute("EndTime")]
		[Bindable(true)]
		public DateTime EndTime 
		{
			get { return GetColumnValue<DateTime>(Columns.EndTime); }
			set { SetColumnValue(Columns.EndTime, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public bool Status 
		{
			get { return GetColumnValue<bool>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Creator")]
		[Bindable(true)]
		public string Creator 
		{
			get { return GetColumnValue<string>(Columns.Creator); }
			set { SetColumnValue(Columns.Creator, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("MainPic")]
		[Bindable(true)]
		public string MainPic 
		{
			get { return GetColumnValue<string>(Columns.MainPic); }
			set { SetColumnValue(Columns.MainPic, value); }
		}
		  
		[XmlAttribute("MobileMainPic")]
		[Bindable(true)]
		public string MobileMainPic 
		{
			get { return GetColumnValue<string>(Columns.MobileMainPic); }
			set { SetColumnValue(Columns.MobileMainPic, value); }
		}
		  
		[XmlAttribute("ShowInWeb")]
		[Bindable(true)]
		public bool ShowInWeb 
		{
			get { return GetColumnValue<bool>(Columns.ShowInWeb); }
			set { SetColumnValue(Columns.ShowInWeb, value); }
		}
		  
		[XmlAttribute("ShowInApp")]
		[Bindable(true)]
		public bool ShowInApp 
		{
			get { return GetColumnValue<bool>(Columns.ShowInApp); }
			set { SetColumnValue(Columns.ShowInApp, value); }
		}
		  
		[XmlAttribute("Bgpic")]
		[Bindable(true)]
		public string Bgpic 
		{
			get { return GetColumnValue<string>(Columns.Bgpic); }
			set { SetColumnValue(Columns.Bgpic, value); }
		}
		  
		[XmlAttribute("Bgcolor")]
		[Bindable(true)]
		public string Bgcolor 
		{
			get { return GetColumnValue<string>(Columns.Bgcolor); }
			set { SetColumnValue(Columns.Bgcolor, value); }
		}
		  
		[XmlAttribute("AppBannerImage")]
		[Bindable(true)]
		public string AppBannerImage 
		{
			get { return GetColumnValue<string>(Columns.AppBannerImage); }
			set { SetColumnValue(Columns.AppBannerImage, value); }
		}
		  
		[XmlAttribute("AppPromoImage")]
		[Bindable(true)]
		public string AppPromoImage 
		{
			get { return GetColumnValue<string>(Columns.AppPromoImage); }
			set { SetColumnValue(Columns.AppPromoImage, value); }
		}
		  
		[XmlAttribute("Mk1ActName")]
		[Bindable(true)]
		public string Mk1ActName 
		{
			get { return GetColumnValue<string>(Columns.Mk1ActName); }
			set { SetColumnValue(Columns.Mk1ActName, value); }
		}
		  
		[XmlAttribute("Mk1Url")]
		[Bindable(true)]
		public string Mk1Url 
		{
			get { return GetColumnValue<string>(Columns.Mk1Url); }
			set { SetColumnValue(Columns.Mk1Url, value); }
		}
		  
		[XmlAttribute("Mk1WebImage")]
		[Bindable(true)]
		public string Mk1WebImage 
		{
			get { return GetColumnValue<string>(Columns.Mk1WebImage); }
			set { SetColumnValue(Columns.Mk1WebImage, value); }
		}
		  
		[XmlAttribute("Mk1AppImage")]
		[Bindable(true)]
		public string Mk1AppImage 
		{
			get { return GetColumnValue<string>(Columns.Mk1AppImage); }
			set { SetColumnValue(Columns.Mk1AppImage, value); }
		}
		  
		[XmlAttribute("Mk2ActName")]
		[Bindable(true)]
		public string Mk2ActName 
		{
			get { return GetColumnValue<string>(Columns.Mk2ActName); }
			set { SetColumnValue(Columns.Mk2ActName, value); }
		}
		  
		[XmlAttribute("Mk2Url")]
		[Bindable(true)]
		public string Mk2Url 
		{
			get { return GetColumnValue<string>(Columns.Mk2Url); }
			set { SetColumnValue(Columns.Mk2Url, value); }
		}
		  
		[XmlAttribute("Mk2WebImage")]
		[Bindable(true)]
		public string Mk2WebImage 
		{
			get { return GetColumnValue<string>(Columns.Mk2WebImage); }
			set { SetColumnValue(Columns.Mk2WebImage, value); }
		}
		  
		[XmlAttribute("Mk2AppImage")]
		[Bindable(true)]
		public string Mk2AppImage 
		{
			get { return GetColumnValue<string>(Columns.Mk2AppImage); }
			set { SetColumnValue(Columns.Mk2AppImage, value); }
		}
		  
		[XmlAttribute("Mk3ActName")]
		[Bindable(true)]
		public string Mk3ActName 
		{
			get { return GetColumnValue<string>(Columns.Mk3ActName); }
			set { SetColumnValue(Columns.Mk3ActName, value); }
		}
		  
		[XmlAttribute("Mk3Url")]
		[Bindable(true)]
		public string Mk3Url 
		{
			get { return GetColumnValue<string>(Columns.Mk3Url); }
			set { SetColumnValue(Columns.Mk3Url, value); }
		}
		  
		[XmlAttribute("Mk3WebImage")]
		[Bindable(true)]
		public string Mk3WebImage 
		{
			get { return GetColumnValue<string>(Columns.Mk3WebImage); }
			set { SetColumnValue(Columns.Mk3WebImage, value); }
		}
		  
		[XmlAttribute("Mk3AppImage")]
		[Bindable(true)]
		public string Mk3AppImage 
		{
			get { return GetColumnValue<string>(Columns.Mk3AppImage); }
			set { SetColumnValue(Columns.Mk3AppImage, value); }
		}
		  
		[XmlAttribute("Mk4ActName")]
		[Bindable(true)]
		public string Mk4ActName 
		{
			get { return GetColumnValue<string>(Columns.Mk4ActName); }
			set { SetColumnValue(Columns.Mk4ActName, value); }
		}
		  
		[XmlAttribute("Mk4Url")]
		[Bindable(true)]
		public string Mk4Url 
		{
			get { return GetColumnValue<string>(Columns.Mk4Url); }
			set { SetColumnValue(Columns.Mk4Url, value); }
		}
		  
		[XmlAttribute("Mk4WebImage")]
		[Bindable(true)]
		public string Mk4WebImage 
		{
			get { return GetColumnValue<string>(Columns.Mk4WebImage); }
			set { SetColumnValue(Columns.Mk4WebImage, value); }
		}
		  
		[XmlAttribute("Mk4AppImage")]
		[Bindable(true)]
		public string Mk4AppImage 
		{
			get { return GetColumnValue<string>(Columns.Mk4AppImage); }
			set { SetColumnValue(Columns.Mk4AppImage, value); }
		}
		  
		[XmlAttribute("Mk5ActName")]
		[Bindable(true)]
		public string Mk5ActName 
		{
			get { return GetColumnValue<string>(Columns.Mk5ActName); }
			set { SetColumnValue(Columns.Mk5ActName, value); }
		}
		  
		[XmlAttribute("Mk5Url")]
		[Bindable(true)]
		public string Mk5Url 
		{
			get { return GetColumnValue<string>(Columns.Mk5Url); }
			set { SetColumnValue(Columns.Mk5Url, value); }
		}
		  
		[XmlAttribute("Mk5WebImage")]
		[Bindable(true)]
		public string Mk5WebImage 
		{
			get { return GetColumnValue<string>(Columns.Mk5WebImage); }
			set { SetColumnValue(Columns.Mk5WebImage, value); }
		}
		  
		[XmlAttribute("Mk5AppImage")]
		[Bindable(true)]
		public string Mk5AppImage 
		{
			get { return GetColumnValue<string>(Columns.Mk5AppImage); }
			set { SetColumnValue(Columns.Mk5AppImage, value); }
		}
		  
		[XmlAttribute("HotItemBid1")]
		[Bindable(true)]
		public Guid? HotItemBid1 
		{
			get { return GetColumnValue<Guid?>(Columns.HotItemBid1); }
			set { SetColumnValue(Columns.HotItemBid1, value); }
		}
		  
		[XmlAttribute("HotItemBid2")]
		[Bindable(true)]
		public Guid? HotItemBid2 
		{
			get { return GetColumnValue<Guid?>(Columns.HotItemBid2); }
			set { SetColumnValue(Columns.HotItemBid2, value); }
		}
		  
		[XmlAttribute("HotItemBid3")]
		[Bindable(true)]
		public Guid? HotItemBid3 
		{
			get { return GetColumnValue<Guid?>(Columns.HotItemBid3); }
			set { SetColumnValue(Columns.HotItemBid3, value); }
		}
		  
		[XmlAttribute("HotItemBid4")]
		[Bindable(true)]
		public Guid? HotItemBid4 
		{
			get { return GetColumnValue<Guid?>(Columns.HotItemBid4); }
			set { SetColumnValue(Columns.HotItemBid4, value); }
		}
		  
		[XmlAttribute("BtnOriginalUrl")]
		[Bindable(true)]
		public string BtnOriginalUrl 
		{
			get { return GetColumnValue<string>(Columns.BtnOriginalUrl); }
			set { SetColumnValue(Columns.BtnOriginalUrl, value); }
		}
		  
		[XmlAttribute("BtnOriginalFontColor")]
		[Bindable(true)]
		public string BtnOriginalFontColor 
		{
			get { return GetColumnValue<string>(Columns.BtnOriginalFontColor); }
			set { SetColumnValue(Columns.BtnOriginalFontColor, value); }
		}
		  
		[XmlAttribute("BtnHoverUrl")]
		[Bindable(true)]
		public string BtnHoverUrl 
		{
			get { return GetColumnValue<string>(Columns.BtnHoverUrl); }
			set { SetColumnValue(Columns.BtnHoverUrl, value); }
		}
		  
		[XmlAttribute("BtnHoverFontColor")]
		[Bindable(true)]
		public string BtnHoverFontColor 
		{
			get { return GetColumnValue<string>(Columns.BtnHoverFontColor); }
			set { SetColumnValue(Columns.BtnHoverFontColor, value); }
		}
		  
		[XmlAttribute("BtnActiveUrl")]
		[Bindable(true)]
		public string BtnActiveUrl 
		{
			get { return GetColumnValue<string>(Columns.BtnActiveUrl); }
			set { SetColumnValue(Columns.BtnActiveUrl, value); }
		}
		  
		[XmlAttribute("BtnActiveFontColor")]
		[Bindable(true)]
		public string BtnActiveFontColor 
		{
			get { return GetColumnValue<string>(Columns.BtnActiveFontColor); }
			set { SetColumnValue(Columns.BtnActiveFontColor, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("DealPromoTitle")]
		[Bindable(true)]
		public string DealPromoTitle 
		{
			get { return GetColumnValue<string>(Columns.DealPromoTitle); }
			set { SetColumnValue(Columns.DealPromoTitle, value); }
		}
		  
		[XmlAttribute("DealPromoImage")]
		[Bindable(true)]
		public string DealPromoImage 
		{
			get { return GetColumnValue<string>(Columns.DealPromoImage); }
			set { SetColumnValue(Columns.DealPromoImage, value); }
		}
		  
		[XmlAttribute("DealPromoDescription")]
		[Bindable(true)]
		public string DealPromoDescription 
		{
			get { return GetColumnValue<string>(Columns.DealPromoDescription); }
			set { SetColumnValue(Columns.DealPromoDescription, value); }
		}
		  
		[XmlAttribute("BannerType")]
		[Bindable(true)]
		public int BannerType 
		{
			get { return GetColumnValue<int>(Columns.BannerType); }
			set { SetColumnValue(Columns.BannerType, value); }
		}
		  
		[XmlAttribute("DiscountList")]
		[Bindable(true)]
		public string DiscountList 
		{
			get { return GetColumnValue<string>(Columns.DiscountList); }
			set { SetColumnValue(Columns.DiscountList, value); }
		}
		  
		[XmlAttribute("DiscountBannerImage")]
		[Bindable(true)]
		public string DiscountBannerImage 
		{
			get { return GetColumnValue<string>(Columns.DiscountBannerImage); }
			set { SetColumnValue(Columns.DiscountBannerImage, value); }
		}
		  
		[XmlAttribute("EventListImage")]
		[Bindable(true)]
		public string EventListImage 
		{
			get { return GetColumnValue<string>(Columns.EventListImage); }
			set { SetColumnValue(Columns.EventListImage, value); }
		}
		  
		[XmlAttribute("FbShareImage")]
		[Bindable(true)]
		public string FbShareImage 
		{
			get { return GetColumnValue<string>(Columns.FbShareImage); }
			set { SetColumnValue(Columns.FbShareImage, value); }
		}
		  
		[XmlAttribute("ChannelListImage")]
		[Bindable(true)]
		public string ChannelListImage 
		{
			get { return GetColumnValue<string>(Columns.ChannelListImage); }
			set { SetColumnValue(Columns.ChannelListImage, value); }
		}
		  
		[XmlAttribute("WebRelayImage")]
		[Bindable(true)]
		public string WebRelayImage 
		{
			get { return GetColumnValue<string>(Columns.WebRelayImage); }
			set { SetColumnValue(Columns.WebRelayImage, value); }
		}
		  
		[XmlAttribute("MobileRelayImage")]
		[Bindable(true)]
		public string MobileRelayImage 
		{
			get { return GetColumnValue<string>(Columns.MobileRelayImage); }
			set { SetColumnValue(Columns.MobileRelayImage, value); }
		}
		  
		[XmlAttribute("MainBackgroundPic")]
		[Bindable(true)]
		public string MainBackgroundPic 
		{
			get { return GetColumnValue<string>(Columns.MainBackgroundPic); }
			set { SetColumnValue(Columns.MainBackgroundPic, value); }
		}

	    [XmlAttribute("MetaDescription")]
	    [Bindable(true)]
	    public string MetaDescription
        {
	        get { return GetColumnValue<string>(Columns.MetaDescription); }
	        set { SetColumnValue(Columns.MetaDescription, value); }
	    }
        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BrandNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UrlColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CpaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn StartTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn EndTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn MainPicColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn MobileMainPicColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ShowInWebColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ShowInAppColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn BgpicColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn BgcolorColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn AppBannerImageColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn AppPromoImageColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk1ActNameColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk1UrlColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk1WebImageColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk1AppImageColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk2ActNameColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk2UrlColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk2WebImageColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk2AppImageColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk3ActNameColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk3UrlColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk3WebImageColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk3AppImageColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk4ActNameColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk4UrlColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk4WebImageColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk4AppImageColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk5ActNameColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk5UrlColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk5WebImageColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn Mk5AppImageColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn HotItemBid1Column
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn HotItemBid2Column
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn HotItemBid3Column
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        public static TableSchema.TableColumn HotItemBid4Column
        {
            get { return Schema.Columns[40]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnOriginalUrlColumn
        {
            get { return Schema.Columns[41]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnOriginalFontColorColumn
        {
            get { return Schema.Columns[42]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnHoverUrlColumn
        {
            get { return Schema.Columns[43]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnHoverFontColorColumn
        {
            get { return Schema.Columns[44]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnActiveUrlColumn
        {
            get { return Schema.Columns[45]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnActiveFontColorColumn
        {
            get { return Schema.Columns[46]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[47]; }
        }
        
        
        
        public static TableSchema.TableColumn DealPromoTitleColumn
        {
            get { return Schema.Columns[48]; }
        }
        
        
        
        public static TableSchema.TableColumn DealPromoImageColumn
        {
            get { return Schema.Columns[49]; }
        }
        
        
        
        public static TableSchema.TableColumn DealPromoDescriptionColumn
        {
            get { return Schema.Columns[50]; }
        }
        
        
        
        public static TableSchema.TableColumn BannerTypeColumn
        {
            get { return Schema.Columns[51]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountListColumn
        {
            get { return Schema.Columns[52]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountBannerImageColumn
        {
            get { return Schema.Columns[53]; }
        }
        
        
        
        public static TableSchema.TableColumn EventListImageColumn
        {
            get { return Schema.Columns[54]; }
        }
        
        
        
        public static TableSchema.TableColumn FbShareImageColumn
        {
            get { return Schema.Columns[55]; }
        }
        
        
        
        public static TableSchema.TableColumn ChannelListImageColumn
        {
            get { return Schema.Columns[56]; }
        }
        
        
        
        public static TableSchema.TableColumn WebRelayImageColumn
        {
            get { return Schema.Columns[57]; }
        }
        
        
        
        public static TableSchema.TableColumn MobileRelayImageColumn
        {
            get { return Schema.Columns[58]; }
        }
        
        
        
        public static TableSchema.TableColumn MainBackgroundPicColumn
        {
            get { return Schema.Columns[59]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BrandName = @"brand_name";
			 public static string Url = @"url";
			 public static string Cpa = @"cpa";
			 public static string StartTime = @"start_time";
			 public static string EndTime = @"end_time";
			 public static string Status = @"status";
			 public static string Creator = @"creator";
			 public static string CreateTime = @"create_time";
			 public static string MainPic = @"main_pic";
			 public static string MobileMainPic = @"mobile_main_pic";
			 public static string ShowInWeb = @"show_in_web";
			 public static string ShowInApp = @"show_in_app";
			 public static string Bgpic = @"bgpic";
			 public static string Bgcolor = @"bgcolor";
			 public static string AppBannerImage = @"app_banner_image";
			 public static string AppPromoImage = @"app_promo_Image";
			 public static string Mk1ActName = @"mk1_act_name";
			 public static string Mk1Url = @"mk1_url";
			 public static string Mk1WebImage = @"mk1_web_image";
			 public static string Mk1AppImage = @"mk1_app_image";
			 public static string Mk2ActName = @"mk2_act_name";
			 public static string Mk2Url = @"mk2_url";
			 public static string Mk2WebImage = @"mk2_web_image";
			 public static string Mk2AppImage = @"mk2_app_image";
			 public static string Mk3ActName = @"mk3_act_name";
			 public static string Mk3Url = @"mk3_url";
			 public static string Mk3WebImage = @"mk3_web_image";
			 public static string Mk3AppImage = @"mk3_app_image";
			 public static string Mk4ActName = @"mk4_act_name";
			 public static string Mk4Url = @"mk4_url";
			 public static string Mk4WebImage = @"mk4_web_image";
			 public static string Mk4AppImage = @"mk4_app_image";
			 public static string Mk5ActName = @"mk5_act_name";
			 public static string Mk5Url = @"mk5_url";
			 public static string Mk5WebImage = @"mk5_web_image";
			 public static string Mk5AppImage = @"mk5_app_image";
			 public static string HotItemBid1 = @"hot_item_bid1";
			 public static string HotItemBid2 = @"hot_item_bid2";
			 public static string HotItemBid3 = @"hot_item_bid3";
			 public static string HotItemBid4 = @"hot_item_bid4";
			 public static string BtnOriginalUrl = @"btn_original_url";
			 public static string BtnOriginalFontColor = @"btn_original_font_color";
			 public static string BtnHoverUrl = @"btn_hover_url";
			 public static string BtnHoverFontColor = @"btn_hover_font_color";
			 public static string BtnActiveUrl = @"btn_active_url";
			 public static string BtnActiveFontColor = @"btn_active_font_color";
			 public static string Description = @"description";
			 public static string DealPromoTitle = @"deal_promo_title";
			 public static string DealPromoImage = @"deal_promo_image";
			 public static string DealPromoDescription = @"deal_promo_description";
			 public static string BannerType = @"banner_type";
			 public static string DiscountList = @"discount_list";
			 public static string DiscountBannerImage = @"discount_banner_image";
			 public static string EventListImage = @"event_list_image";
			 public static string FbShareImage = @"fb_share_image";
			 public static string ChannelListImage = @"channel_list_image";
			 public static string WebRelayImage = @"web_relay_image";
			 public static string MobileRelayImage = @"mobile_relay_image";
			 public static string MainBackgroundPic = @"main_background_pic";
		     public static string MetaDescription = @"meta_description";


		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

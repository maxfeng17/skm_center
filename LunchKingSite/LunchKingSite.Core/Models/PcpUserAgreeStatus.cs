using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpUserAgreeStatus class.
	/// </summary>
    [Serializable]
	public partial class PcpUserAgreeStatusCollection : RepositoryList<PcpUserAgreeStatus, PcpUserAgreeStatusCollection>
	{	   
		public PcpUserAgreeStatusCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpUserAgreeStatusCollection</returns>
		public PcpUserAgreeStatusCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpUserAgreeStatus o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_user_agree_status table.
	/// </summary>
	[Serializable]
	public partial class PcpUserAgreeStatus : RepositoryRecord<PcpUserAgreeStatus>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpUserAgreeStatus()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpUserAgreeStatus(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_user_agree_status", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarAgreeTime = new TableSchema.TableColumn(schema);
				colvarAgreeTime.ColumnName = "agree_time";
				colvarAgreeTime.DataType = DbType.DateTime;
				colvarAgreeTime.MaxLength = 0;
				colvarAgreeTime.AutoIncrement = false;
				colvarAgreeTime.IsNullable = false;
				colvarAgreeTime.IsPrimaryKey = false;
				colvarAgreeTime.IsForeignKey = false;
				colvarAgreeTime.IsReadOnly = false;
				colvarAgreeTime.DefaultSetting = @"";
				colvarAgreeTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAgreeTime);
				
				TableSchema.TableColumn colvarSystemDataName = new TableSchema.TableColumn(schema);
				colvarSystemDataName.ColumnName = "system_data_name";
				colvarSystemDataName.DataType = DbType.String;
				colvarSystemDataName.MaxLength = 100;
				colvarSystemDataName.AutoIncrement = false;
				colvarSystemDataName.IsNullable = true;
				colvarSystemDataName.IsPrimaryKey = false;
				colvarSystemDataName.IsForeignKey = false;
				colvarSystemDataName.IsReadOnly = false;
				colvarSystemDataName.DefaultSetting = @"";
				colvarSystemDataName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSystemDataName);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_user_agree_status",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("AgreeTime")]
		[Bindable(true)]
		public DateTime AgreeTime 
		{
			get { return GetColumnValue<DateTime>(Columns.AgreeTime); }
			set { SetColumnValue(Columns.AgreeTime, value); }
		}
		  
		[XmlAttribute("SystemDataName")]
		[Bindable(true)]
		public string SystemDataName 
		{
			get { return GetColumnValue<string>(Columns.SystemDataName); }
			set { SetColumnValue(Columns.SystemDataName, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AgreeTimeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SystemDataNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string UserId = @"user_id";
			 public static string AgreeTime = @"agree_time";
			 public static string SystemDataName = @"system_data_name";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealTimeSlotSortElementRank class.
	/// </summary>
    [Serializable]
	public partial class DealTimeSlotSortElementRankCollection : RepositoryList<DealTimeSlotSortElementRank, DealTimeSlotSortElementRankCollection>
	{	   
		public DealTimeSlotSortElementRankCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealTimeSlotSortElementRankCollection</returns>
		public DealTimeSlotSortElementRankCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealTimeSlotSortElementRank o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_time_slot_sort_element_rank table.
	/// </summary>
	[Serializable]
	public partial class DealTimeSlotSortElementRank : RepositoryRecord<DealTimeSlotSortElementRank>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealTimeSlotSortElementRank()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DealTimeSlotSortElementRank(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("deal_time_slot_sort_element_rank", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarElementId = new TableSchema.TableColumn(schema);
				colvarElementId.ColumnName = "element_id";
				colvarElementId.DataType = DbType.Int32;
				colvarElementId.MaxLength = 0;
				colvarElementId.AutoIncrement = false;
				colvarElementId.IsNullable = false;
				colvarElementId.IsPrimaryKey = false;
				colvarElementId.IsForeignKey = false;
				colvarElementId.IsReadOnly = false;
				colvarElementId.DefaultSetting = @"";
				colvarElementId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarElementId);
				
				TableSchema.TableColumn colvarRankStart = new TableSchema.TableColumn(schema);
				colvarRankStart.ColumnName = "rank_start";
				colvarRankStart.DataType = DbType.Decimal;
				colvarRankStart.MaxLength = 0;
				colvarRankStart.AutoIncrement = false;
				colvarRankStart.IsNullable = true;
				colvarRankStart.IsPrimaryKey = false;
				colvarRankStart.IsForeignKey = false;
				colvarRankStart.IsReadOnly = false;
				colvarRankStart.DefaultSetting = @"";
				colvarRankStart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRankStart);
				
				TableSchema.TableColumn colvarRankEnd = new TableSchema.TableColumn(schema);
				colvarRankEnd.ColumnName = "rank_end";
				colvarRankEnd.DataType = DbType.Decimal;
				colvarRankEnd.MaxLength = 0;
				colvarRankEnd.AutoIncrement = false;
				colvarRankEnd.IsNullable = false;
				colvarRankEnd.IsPrimaryKey = false;
				colvarRankEnd.IsForeignKey = false;
				colvarRankEnd.IsReadOnly = false;
				colvarRankEnd.DefaultSetting = @"";
				colvarRankEnd.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRankEnd);
				
				TableSchema.TableColumn colvarScore = new TableSchema.TableColumn(schema);
				colvarScore.ColumnName = "score";
				colvarScore.DataType = DbType.Decimal;
				colvarScore.MaxLength = 0;
				colvarScore.AutoIncrement = false;
				colvarScore.IsNullable = false;
				colvarScore.IsPrimaryKey = false;
				colvarScore.IsForeignKey = false;
				colvarScore.IsReadOnly = false;
				colvarScore.DefaultSetting = @"";
				colvarScore.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScore);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("deal_time_slot_sort_element_rank",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ElementId")]
		[Bindable(true)]
		public int ElementId 
		{
			get { return GetColumnValue<int>(Columns.ElementId); }
			set { SetColumnValue(Columns.ElementId, value); }
		}
		  
		[XmlAttribute("RankStart")]
		[Bindable(true)]
		public decimal? RankStart 
		{
			get { return GetColumnValue<decimal?>(Columns.RankStart); }
			set { SetColumnValue(Columns.RankStart, value); }
		}
		  
		[XmlAttribute("RankEnd")]
		[Bindable(true)]
		public decimal RankEnd 
		{
			get { return GetColumnValue<decimal>(Columns.RankEnd); }
			set { SetColumnValue(Columns.RankEnd, value); }
		}
		  
		[XmlAttribute("Score")]
		[Bindable(true)]
		public decimal Score 
		{
			get { return GetColumnValue<decimal>(Columns.Score); }
			set { SetColumnValue(Columns.Score, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ElementIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn RankStartColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RankEndColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ScoreColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ElementId = @"element_id";
			 public static string RankStart = @"rank_start";
			 public static string RankEnd = @"rank_end";
			 public static string Score = @"score";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewCustomerServiceListCollection : ReadOnlyList<ViewCustomerServiceList, ViewCustomerServiceListCollection>
	{
			public ViewCustomerServiceListCollection() {}

	}

	[Serializable]
	public partial class ViewCustomerServiceList : ReadOnlyRecord<ViewCustomerServiceList>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewCustomerServiceList()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewCustomerServiceList(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewCustomerServiceList(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewCustomerServiceList(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_customer_service_list", TableType.View, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarServiceNo = new TableSchema.TableColumn(schema);
				colvarServiceNo.ColumnName = "service_no";
				colvarServiceNo.DataType = DbType.AnsiString;
				colvarServiceNo.MaxLength = 50;
				colvarServiceNo.AutoIncrement = false;
				colvarServiceNo.IsNullable = false;
				colvarServiceNo.IsPrimaryKey = false;
				colvarServiceNo.IsForeignKey = false;
				colvarServiceNo.IsReadOnly = false;
				colvarServiceNo.DefaultSetting = @"";
				colvarServiceNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceNo);

				TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
				colvarCategory.ColumnName = "category";
				colvarCategory.DataType = DbType.Int32;
				colvarCategory.MaxLength = 0;
				colvarCategory.AutoIncrement = false;
				colvarCategory.IsNullable = false;
				colvarCategory.IsPrimaryKey = false;
				colvarCategory.IsForeignKey = false;
				colvarCategory.IsReadOnly = false;
				colvarCategory.DefaultSetting = @"";
				colvarCategory.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategory);

				TableSchema.TableColumn colvarMainCategoryName = new TableSchema.TableColumn(schema);
				colvarMainCategoryName.ColumnName = "main_category_name";
				colvarMainCategoryName.DataType = DbType.String;
				colvarMainCategoryName.MaxLength = 100;
				colvarMainCategoryName.AutoIncrement = false;
				colvarMainCategoryName.IsNullable = true;
				colvarMainCategoryName.IsPrimaryKey = false;
				colvarMainCategoryName.IsForeignKey = false;
				colvarMainCategoryName.IsReadOnly = false;
				colvarMainCategoryName.DefaultSetting = @"";
				colvarMainCategoryName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainCategoryName);

				TableSchema.TableColumn colvarSubCategory = new TableSchema.TableColumn(schema);
				colvarSubCategory.ColumnName = "sub_category";
				colvarSubCategory.DataType = DbType.Int32;
				colvarSubCategory.MaxLength = 0;
				colvarSubCategory.AutoIncrement = false;
				colvarSubCategory.IsNullable = false;
				colvarSubCategory.IsPrimaryKey = false;
				colvarSubCategory.IsForeignKey = false;
				colvarSubCategory.IsReadOnly = false;
				colvarSubCategory.DefaultSetting = @"";
				colvarSubCategory.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategory);

				TableSchema.TableColumn colvarSubCategoryName = new TableSchema.TableColumn(schema);
				colvarSubCategoryName.ColumnName = "sub_category_name";
				colvarSubCategoryName.DataType = DbType.String;
				colvarSubCategoryName.MaxLength = 100;
				colvarSubCategoryName.AutoIncrement = false;
				colvarSubCategoryName.IsNullable = true;
				colvarSubCategoryName.IsPrimaryKey = false;
				colvarSubCategoryName.IsForeignKey = false;
				colvarSubCategoryName.IsReadOnly = false;
				colvarSubCategoryName.DefaultSetting = @"";
				colvarSubCategoryName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryName);

				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = true;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarCustomerServiceStatus = new TableSchema.TableColumn(schema);
				colvarCustomerServiceStatus.ColumnName = "customer_service_status";
				colvarCustomerServiceStatus.DataType = DbType.Int32;
				colvarCustomerServiceStatus.MaxLength = 0;
				colvarCustomerServiceStatus.AutoIncrement = false;
				colvarCustomerServiceStatus.IsNullable = false;
				colvarCustomerServiceStatus.IsPrimaryKey = false;
				colvarCustomerServiceStatus.IsForeignKey = false;
				colvarCustomerServiceStatus.IsReadOnly = false;
				colvarCustomerServiceStatus.DefaultSetting = @"";
				colvarCustomerServiceStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCustomerServiceStatus);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_GUID";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = true;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 50;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = true;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);

				TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
				colvarUniqueId.ColumnName = "unique_id";
				colvarUniqueId.DataType = DbType.Int32;
				colvarUniqueId.MaxLength = 0;
				colvarUniqueId.AutoIncrement = false;
				colvarUniqueId.IsNullable = true;
				colvarUniqueId.IsPrimaryKey = false;
				colvarUniqueId.IsForeignKey = false;
				colvarUniqueId.IsReadOnly = false;
				colvarUniqueId.DefaultSetting = @"";
				colvarUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniqueId);

				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = true;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);

				TableSchema.TableColumn colvarIsWms = new TableSchema.TableColumn(schema);
				colvarIsWms.ColumnName = "is_wms";
				colvarIsWms.DataType = DbType.Boolean;
				colvarIsWms.MaxLength = 0;
				colvarIsWms.AutoIncrement = false;
				colvarIsWms.IsNullable = true;
				colvarIsWms.IsPrimaryKey = false;
				colvarIsWms.IsForeignKey = false;
				colvarIsWms.IsReadOnly = false;
				colvarIsWms.DefaultSetting = @"";
				colvarIsWms.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsWms);

				TableSchema.TableColumn colvarMainBid = new TableSchema.TableColumn(schema);
				colvarMainBid.ColumnName = "mainBid";
				colvarMainBid.DataType = DbType.Guid;
				colvarMainBid.MaxLength = 0;
				colvarMainBid.AutoIncrement = false;
				colvarMainBid.IsNullable = true;
				colvarMainBid.IsPrimaryKey = false;
				colvarMainBid.IsForeignKey = false;
				colvarMainBid.IsReadOnly = false;
				colvarMainBid.DefaultSetting = @"";
				colvarMainBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainBid);

				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 750;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = true;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);

				TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
				colvarMemberName.ColumnName = "member_name";
				colvarMemberName.DataType = DbType.String;
				colvarMemberName.MaxLength = 50;
				colvarMemberName.AutoIncrement = false;
				colvarMemberName.IsNullable = true;
				colvarMemberName.IsPrimaryKey = false;
				colvarMemberName.IsForeignKey = false;
				colvarMemberName.IsReadOnly = false;
				colvarMemberName.DefaultSetting = @"";
				colvarMemberName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberName);

				TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
				colvarMobileNumber.ColumnName = "mobile_number";
				colvarMobileNumber.DataType = DbType.AnsiString;
				colvarMobileNumber.MaxLength = 50;
				colvarMobileNumber.AutoIncrement = false;
				colvarMobileNumber.IsNullable = true;
				colvarMobileNumber.IsPrimaryKey = false;
				colvarMobileNumber.IsForeignKey = false;
				colvarMobileNumber.IsReadOnly = false;
				colvarMobileNumber.DefaultSetting = @"";
				colvarMobileNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileNumber);

				TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
				colvarDeliveryAddress.ColumnName = "delivery_address";
				colvarDeliveryAddress.DataType = DbType.String;
				colvarDeliveryAddress.MaxLength = 200;
				colvarDeliveryAddress.AutoIncrement = false;
				colvarDeliveryAddress.IsNullable = true;
				colvarDeliveryAddress.IsPrimaryKey = false;
				colvarDeliveryAddress.IsForeignKey = false;
				colvarDeliveryAddress.IsReadOnly = false;
				colvarDeliveryAddress.DefaultSetting = @"";
				colvarDeliveryAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryAddress);

				TableSchema.TableColumn colvarCasePriority = new TableSchema.TableColumn(schema);
				colvarCasePriority.ColumnName = "case_priority";
				colvarCasePriority.DataType = DbType.Int32;
				colvarCasePriority.MaxLength = 0;
				colvarCasePriority.AutoIncrement = false;
				colvarCasePriority.IsNullable = false;
				colvarCasePriority.IsPrimaryKey = false;
				colvarCasePriority.IsForeignKey = false;
				colvarCasePriority.IsReadOnly = false;
				colvarCasePriority.DefaultSetting = @"";
				colvarCasePriority.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCasePriority);

				TableSchema.TableColumn colvarMessageType = new TableSchema.TableColumn(schema);
				colvarMessageType.ColumnName = "message_type";
				colvarMessageType.DataType = DbType.Int32;
				colvarMessageType.MaxLength = 0;
				colvarMessageType.AutoIncrement = false;
				colvarMessageType.IsNullable = false;
				colvarMessageType.IsPrimaryKey = false;
				colvarMessageType.IsForeignKey = false;
				colvarMessageType.IsReadOnly = false;
				colvarMessageType.DefaultSetting = @"";
				colvarMessageType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessageType);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarServicePeopleId = new TableSchema.TableColumn(schema);
				colvarServicePeopleId.ColumnName = "service_people_id";
				colvarServicePeopleId.DataType = DbType.Int32;
				colvarServicePeopleId.MaxLength = 0;
				colvarServicePeopleId.AutoIncrement = false;
				colvarServicePeopleId.IsNullable = true;
				colvarServicePeopleId.IsPrimaryKey = false;
				colvarServicePeopleId.IsForeignKey = false;
				colvarServicePeopleId.IsReadOnly = false;
				colvarServicePeopleId.DefaultSetting = @"";
				colvarServicePeopleId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServicePeopleId);

				TableSchema.TableColumn colvarServicePeopleName = new TableSchema.TableColumn(schema);
				colvarServicePeopleName.ColumnName = "service_people_name";
				colvarServicePeopleName.DataType = DbType.String;
				colvarServicePeopleName.MaxLength = 50;
				colvarServicePeopleName.AutoIncrement = false;
				colvarServicePeopleName.IsNullable = true;
				colvarServicePeopleName.IsPrimaryKey = false;
				colvarServicePeopleName.IsForeignKey = false;
				colvarServicePeopleName.IsReadOnly = false;
				colvarServicePeopleName.DefaultSetting = @"";
				colvarServicePeopleName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServicePeopleName);

				TableSchema.TableColumn colvarSecServicePeopleId = new TableSchema.TableColumn(schema);
				colvarSecServicePeopleId.ColumnName = "sec_service_people_id";
				colvarSecServicePeopleId.DataType = DbType.Int32;
				colvarSecServicePeopleId.MaxLength = 0;
				colvarSecServicePeopleId.AutoIncrement = false;
				colvarSecServicePeopleId.IsNullable = true;
				colvarSecServicePeopleId.IsPrimaryKey = false;
				colvarSecServicePeopleId.IsForeignKey = false;
				colvarSecServicePeopleId.IsReadOnly = false;
				colvarSecServicePeopleId.DefaultSetting = @"";
				colvarSecServicePeopleId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSecServicePeopleId);

				TableSchema.TableColumn colvarSecServicePeopleName = new TableSchema.TableColumn(schema);
				colvarSecServicePeopleName.ColumnName = "sec_service_people_name";
				colvarSecServicePeopleName.DataType = DbType.String;
				colvarSecServicePeopleName.MaxLength = 50;
				colvarSecServicePeopleName.AutoIncrement = false;
				colvarSecServicePeopleName.IsNullable = true;
				colvarSecServicePeopleName.IsPrimaryKey = false;
				colvarSecServicePeopleName.IsForeignKey = false;
				colvarSecServicePeopleName.IsReadOnly = false;
				colvarSecServicePeopleName.DefaultSetting = @"";
				colvarSecServicePeopleName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSecServicePeopleName);

				TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
				colvarEmail.ColumnName = "email";
				colvarEmail.DataType = DbType.AnsiString;
				colvarEmail.MaxLength = 100;
				colvarEmail.AutoIncrement = false;
				colvarEmail.IsNullable = true;
				colvarEmail.IsPrimaryKey = false;
				colvarEmail.IsForeignKey = false;
				colvarEmail.IsReadOnly = false;
				colvarEmail.DefaultSetting = @"";
				colvarEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmail);

				TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
				colvarPhone.ColumnName = "phone";
				colvarPhone.DataType = DbType.AnsiString;
				colvarPhone.MaxLength = 50;
				colvarPhone.AutoIncrement = false;
				colvarPhone.IsNullable = true;
				colvarPhone.IsPrimaryKey = false;
				colvarPhone.IsForeignKey = false;
				colvarPhone.IsReadOnly = false;
				colvarPhone.DefaultSetting = @"";
				colvarPhone.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhone);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = true;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
				colvarBusinessHourStatus.ColumnName = "business_hour_status";
				colvarBusinessHourStatus.DataType = DbType.Int32;
				colvarBusinessHourStatus.MaxLength = 0;
				colvarBusinessHourStatus.AutoIncrement = false;
				colvarBusinessHourStatus.IsNullable = true;
				colvarBusinessHourStatus.IsPrimaryKey = false;
				colvarBusinessHourStatus.IsForeignKey = false;
				colvarBusinessHourStatus.IsReadOnly = false;
				colvarBusinessHourStatus.DefaultSetting = @"";
				colvarBusinessHourStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourStatus);

				TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
				colvarImagePath.ColumnName = "image_path";
				colvarImagePath.DataType = DbType.String;
				colvarImagePath.MaxLength = 1000;
				colvarImagePath.AutoIncrement = false;
				colvarImagePath.IsNullable = true;
				colvarImagePath.IsPrimaryKey = false;
				colvarImagePath.IsForeignKey = false;
				colvarImagePath.IsReadOnly = false;
				colvarImagePath.DefaultSetting = @"";
				colvarImagePath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImagePath);

				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 50;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = true;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);

				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = true;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);

				TableSchema.TableColumn colvarSource = new TableSchema.TableColumn(schema);
				colvarSource.ColumnName = "source";
				colvarSource.DataType = DbType.Int32;
				colvarSource.MaxLength = 0;
				colvarSource.AutoIncrement = false;
				colvarSource.IsNullable = false;
				colvarSource.IsPrimaryKey = false;
				colvarSource.IsForeignKey = false;
				colvarSource.IsReadOnly = false;
				colvarSource.DefaultSetting = @"";
				colvarSource.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSource);

				TableSchema.TableColumn colvarParentServiceNo = new TableSchema.TableColumn(schema);
				colvarParentServiceNo.ColumnName = "parent_service_no";
				colvarParentServiceNo.DataType = DbType.AnsiString;
				colvarParentServiceNo.MaxLength = 50;
				colvarParentServiceNo.AutoIncrement = false;
				colvarParentServiceNo.IsNullable = true;
				colvarParentServiceNo.IsPrimaryKey = false;
				colvarParentServiceNo.IsForeignKey = false;
				colvarParentServiceNo.IsReadOnly = false;
				colvarParentServiceNo.DefaultSetting = @"";
				colvarParentServiceNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentServiceNo);

				TableSchema.TableColumn colvarProductDeliveryType = new TableSchema.TableColumn(schema);
				colvarProductDeliveryType.ColumnName = "product_delivery_type";
				colvarProductDeliveryType.DataType = DbType.Int32;
				colvarProductDeliveryType.MaxLength = 0;
				colvarProductDeliveryType.AutoIncrement = false;
				colvarProductDeliveryType.IsNullable = true;
				colvarProductDeliveryType.IsPrimaryKey = false;
				colvarProductDeliveryType.IsForeignKey = false;
				colvarProductDeliveryType.IsReadOnly = false;
				colvarProductDeliveryType.DefaultSetting = @"";
				colvarProductDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductDeliveryType);

				TableSchema.TableColumn colvarIspOrderType = new TableSchema.TableColumn(schema);
				colvarIspOrderType.ColumnName = "isp_order_type";
				colvarIspOrderType.DataType = DbType.Int32;
				colvarIspOrderType.MaxLength = 0;
				colvarIspOrderType.AutoIncrement = false;
				colvarIspOrderType.IsNullable = true;
				colvarIspOrderType.IsPrimaryKey = false;
				colvarIspOrderType.IsForeignKey = false;
				colvarIspOrderType.IsReadOnly = false;
				colvarIspOrderType.DefaultSetting = @"";
				colvarIspOrderType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIspOrderType);

				TableSchema.TableColumn colvarIssueFromType = new TableSchema.TableColumn(schema);
				colvarIssueFromType.ColumnName = "issue_from_type";
				colvarIssueFromType.DataType = DbType.Int32;
				colvarIssueFromType.MaxLength = 0;
				colvarIssueFromType.AutoIncrement = false;
				colvarIssueFromType.IsNullable = false;
				colvarIssueFromType.IsPrimaryKey = false;
				colvarIssueFromType.IsForeignKey = false;
				colvarIssueFromType.IsReadOnly = false;
				colvarIssueFromType.DefaultSetting = @"";
				colvarIssueFromType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIssueFromType);

				TableSchema.TableColumn colvarTsMemberNo = new TableSchema.TableColumn(schema);
				colvarTsMemberNo.ColumnName = "ts_member_no";
				colvarTsMemberNo.DataType = DbType.AnsiString;
				colvarTsMemberNo.MaxLength = 80;
				colvarTsMemberNo.AutoIncrement = false;
				colvarTsMemberNo.IsNullable = true;
				colvarTsMemberNo.IsPrimaryKey = false;
				colvarTsMemberNo.IsForeignKey = false;
				colvarTsMemberNo.IsReadOnly = false;
				colvarTsMemberNo.DefaultSetting = @"";
				colvarTsMemberNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTsMemberNo);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_customer_service_list",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("ServiceNo")]
		[Bindable(true)]
		public string ServiceNo
		{
			get { return GetColumnValue<string>(Columns.ServiceNo); }
			set { SetColumnValue(Columns.ServiceNo, value); }
		}

		[XmlAttribute("Category")]
		[Bindable(true)]
		public int Category
		{
			get { return GetColumnValue<int>(Columns.Category); }
			set { SetColumnValue(Columns.Category, value); }
		}

		[XmlAttribute("MainCategoryName")]
		[Bindable(true)]
		public string MainCategoryName
		{
			get { return GetColumnValue<string>(Columns.MainCategoryName); }
			set { SetColumnValue(Columns.MainCategoryName, value); }
		}

		[XmlAttribute("SubCategory")]
		[Bindable(true)]
		public int SubCategory
		{
			get { return GetColumnValue<int>(Columns.SubCategory); }
			set { SetColumnValue(Columns.SubCategory, value); }
		}

		[XmlAttribute("SubCategoryName")]
		[Bindable(true)]
		public string SubCategoryName
		{
			get { return GetColumnValue<string>(Columns.SubCategoryName); }
			set { SetColumnValue(Columns.SubCategoryName, value); }
		}

		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("CustomerServiceStatus")]
		[Bindable(true)]
		public int CustomerServiceStatus
		{
			get { return GetColumnValue<int>(Columns.CustomerServiceStatus); }
			set { SetColumnValue(Columns.CustomerServiceStatus, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid? SellerGuid
		{
			get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}

		[XmlAttribute("UniqueId")]
		[Bindable(true)]
		public int? UniqueId
		{
			get { return GetColumnValue<int?>(Columns.UniqueId); }
			set { SetColumnValue(Columns.UniqueId, value); }
		}

		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid? BusinessHourGuid
		{
			get { return GetColumnValue<Guid?>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}

		[XmlAttribute("IsWms")]
		[Bindable(true)]
		public bool? IsWms
		{
			get { return GetColumnValue<bool?>(Columns.IsWms); }
			set { SetColumnValue(Columns.IsWms, value); }
		}

		[XmlAttribute("MainBid")]
		[Bindable(true)]
		public Guid? MainBid
		{
			get { return GetColumnValue<Guid?>(Columns.MainBid); }
			set { SetColumnValue(Columns.MainBid, value); }
		}

		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}

		[XmlAttribute("MemberName")]
		[Bindable(true)]
		public string MemberName
		{
			get { return GetColumnValue<string>(Columns.MemberName); }
			set { SetColumnValue(Columns.MemberName, value); }
		}

		[XmlAttribute("MobileNumber")]
		[Bindable(true)]
		public string MobileNumber
		{
			get { return GetColumnValue<string>(Columns.MobileNumber); }
			set { SetColumnValue(Columns.MobileNumber, value); }
		}

		[XmlAttribute("DeliveryAddress")]
		[Bindable(true)]
		public string DeliveryAddress
		{
			get { return GetColumnValue<string>(Columns.DeliveryAddress); }
			set { SetColumnValue(Columns.DeliveryAddress, value); }
		}

		[XmlAttribute("CasePriority")]
		[Bindable(true)]
		public int CasePriority
		{
			get { return GetColumnValue<int>(Columns.CasePriority); }
			set { SetColumnValue(Columns.CasePriority, value); }
		}

		[XmlAttribute("MessageType")]
		[Bindable(true)]
		public int MessageType
		{
			get { return GetColumnValue<int>(Columns.MessageType); }
			set { SetColumnValue(Columns.MessageType, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("ServicePeopleId")]
		[Bindable(true)]
		public int? ServicePeopleId
		{
			get { return GetColumnValue<int?>(Columns.ServicePeopleId); }
			set { SetColumnValue(Columns.ServicePeopleId, value); }
		}

		[XmlAttribute("ServicePeopleName")]
		[Bindable(true)]
		public string ServicePeopleName
		{
			get { return GetColumnValue<string>(Columns.ServicePeopleName); }
			set { SetColumnValue(Columns.ServicePeopleName, value); }
		}

		[XmlAttribute("SecServicePeopleId")]
		[Bindable(true)]
		public int? SecServicePeopleId
		{
			get { return GetColumnValue<int?>(Columns.SecServicePeopleId); }
			set { SetColumnValue(Columns.SecServicePeopleId, value); }
		}

		[XmlAttribute("SecServicePeopleName")]
		[Bindable(true)]
		public string SecServicePeopleName
		{
			get { return GetColumnValue<string>(Columns.SecServicePeopleName); }
			set { SetColumnValue(Columns.SecServicePeopleName, value); }
		}

		[XmlAttribute("Email")]
		[Bindable(true)]
		public string Email
		{
			get { return GetColumnValue<string>(Columns.Email); }
			set { SetColumnValue(Columns.Email, value); }
		}

		[XmlAttribute("Phone")]
		[Bindable(true)]
		public string Phone
		{
			get { return GetColumnValue<string>(Columns.Phone); }
			set { SetColumnValue(Columns.Phone, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int? UserId
		{
			get { return GetColumnValue<int?>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		[XmlAttribute("BusinessHourStatus")]
		[Bindable(true)]
		public int? BusinessHourStatus
		{
			get { return GetColumnValue<int?>(Columns.BusinessHourStatus); }
			set { SetColumnValue(Columns.BusinessHourStatus, value); }
		}

		[XmlAttribute("ImagePath")]
		[Bindable(true)]
		public string ImagePath
		{
			get { return GetColumnValue<string>(Columns.ImagePath); }
			set { SetColumnValue(Columns.ImagePath, value); }
		}

		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}

		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int? DeliveryType
		{
			get { return GetColumnValue<int?>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}

		[XmlAttribute("Source")]
		[Bindable(true)]
		public int Source
		{
			get { return GetColumnValue<int>(Columns.Source); }
			set { SetColumnValue(Columns.Source, value); }
		}

		[XmlAttribute("ParentServiceNo")]
		[Bindable(true)]
		public string ParentServiceNo
		{
			get { return GetColumnValue<string>(Columns.ParentServiceNo); }
			set { SetColumnValue(Columns.ParentServiceNo, value); }
		}

		[XmlAttribute("ProductDeliveryType")]
		[Bindable(true)]
		public int? ProductDeliveryType
		{
			get { return GetColumnValue<int?>(Columns.ProductDeliveryType); }
			set { SetColumnValue(Columns.ProductDeliveryType, value); }
		}

		[XmlAttribute("IspOrderType")]
		[Bindable(true)]
		public int? IspOrderType
		{
			get { return GetColumnValue<int?>(Columns.IspOrderType); }
			set { SetColumnValue(Columns.IspOrderType, value); }
		}

		[XmlAttribute("IssueFromType")]
		[Bindable(true)]
		public int IssueFromType
		{
			get { return GetColumnValue<int>(Columns.IssueFromType); }
			set { SetColumnValue(Columns.IssueFromType, value); }
		}

		[XmlAttribute("TsMemberNo")]
		[Bindable(true)]
		public string TsMemberNo
		{
			get { return GetColumnValue<string>(Columns.TsMemberNo); }
			set { SetColumnValue(Columns.TsMemberNo, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn ServiceNoColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn CategoryColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn MainCategoryNameColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn SubCategoryColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn SubCategoryNameColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn OrderIdColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn CustomerServiceStatusColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn SellerNameColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn UniqueIdColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn BusinessHourGuidColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn IsWmsColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn MainBidColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn ItemNameColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn MemberNameColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn MobileNumberColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn DeliveryAddressColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn CasePriorityColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn MessageTypeColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn ServicePeopleIdColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn ServicePeopleNameColumn
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn SecServicePeopleIdColumn
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn SecServicePeopleNameColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn EmailColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn PhoneColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[28]; }
		}

		public static TableSchema.TableColumn BusinessHourStatusColumn
		{
			get { return Schema.Columns[29]; }
		}

		public static TableSchema.TableColumn ImagePathColumn
		{
			get { return Schema.Columns[30]; }
		}

		public static TableSchema.TableColumn NameColumn
		{
			get { return Schema.Columns[31]; }
		}

		public static TableSchema.TableColumn DeliveryTypeColumn
		{
			get { return Schema.Columns[32]; }
		}

		public static TableSchema.TableColumn SourceColumn
		{
			get { return Schema.Columns[33]; }
		}

		public static TableSchema.TableColumn ParentServiceNoColumn
		{
			get { return Schema.Columns[34]; }
		}

		public static TableSchema.TableColumn ProductDeliveryTypeColumn
		{
			get { return Schema.Columns[35]; }
		}

		public static TableSchema.TableColumn IspOrderTypeColumn
		{
			get { return Schema.Columns[36]; }
		}

		public static TableSchema.TableColumn IssueFromTypeColumn
		{
			get { return Schema.Columns[37]; }
		}

		public static TableSchema.TableColumn TsMemberNoColumn
		{
			get { return Schema.Columns[38]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string ServiceNo = @"service_no";
			public static string Category = @"category";
			public static string MainCategoryName = @"main_category_name";
			public static string SubCategory = @"sub_category";
			public static string SubCategoryName = @"sub_category_name";
			public static string OrderId = @"order_id";
			public static string OrderGuid = @"order_guid";
			public static string CustomerServiceStatus = @"customer_service_status";
			public static string SellerGuid = @"seller_GUID";
			public static string SellerName = @"seller_name";
			public static string UniqueId = @"unique_id";
			public static string BusinessHourGuid = @"business_hour_guid";
			public static string IsWms = @"is_wms";
			public static string MainBid = @"mainBid";
			public static string ItemName = @"item_name";
			public static string MemberName = @"member_name";
			public static string MobileNumber = @"mobile_number";
			public static string DeliveryAddress = @"delivery_address";
			public static string CasePriority = @"case_priority";
			public static string MessageType = @"message_type";
			public static string CreateTime = @"create_time";
			public static string ModifyTime = @"modify_time";
			public static string ServicePeopleId = @"service_people_id";
			public static string ServicePeopleName = @"service_people_name";
			public static string SecServicePeopleId = @"sec_service_people_id";
			public static string SecServicePeopleName = @"sec_service_people_name";
			public static string Email = @"email";
			public static string Phone = @"phone";
			public static string UserId = @"user_id";
			public static string BusinessHourStatus = @"business_hour_status";
			public static string ImagePath = @"image_path";
			public static string Name = @"name";
			public static string DeliveryType = @"delivery_type";
			public static string Source = @"source";
			public static string ParentServiceNo = @"parent_service_no";
			public static string ProductDeliveryType = @"product_delivery_type";
			public static string IspOrderType = @"isp_order_type";
			public static string IssueFromType = @"issue_from_type";
			public static string TsMemberNo = @"ts_member_no";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

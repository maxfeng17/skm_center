﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the SkmAppStyleSetup class.
    /// </summary>
    [Serializable]
    public partial class SkmAppStyleSetupCollection : RepositoryList<SkmAppStyleSetup, SkmAppStyleSetupCollection>
    {
        public SkmAppStyleSetupCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmAppStyleSetupCollection</returns>
        public SkmAppStyleSetupCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmAppStyleSetup o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the skm_app_style_setup table.
    /// </summary>
    [Serializable]
    public partial class SkmAppStyleSetup : RepositoryRecord<SkmAppStyleSetup>, IRecordBase
    {
        #region .ctors and Default Settings

        public SkmAppStyleSetup()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SkmAppStyleSetup(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("skm_app_style_setup", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarStyleGuid = new TableSchema.TableColumn(schema);
                colvarStyleGuid.ColumnName = "style_guid";
                colvarStyleGuid.DataType = DbType.Guid;
                colvarStyleGuid.MaxLength = 0;
                colvarStyleGuid.AutoIncrement = false;
                colvarStyleGuid.IsNullable = false;
                colvarStyleGuid.IsPrimaryKey = false;
                colvarStyleGuid.IsForeignKey = false;
                colvarStyleGuid.IsReadOnly = false;
                colvarStyleGuid.DefaultSetting = @"";
                colvarStyleGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStyleGuid);

                TableSchema.TableColumn colvarBeginDate = new TableSchema.TableColumn(schema);
                colvarBeginDate.ColumnName = "begin_date";
                colvarBeginDate.DataType = DbType.AnsiString;
                colvarBeginDate.MaxLength = 10;
                colvarBeginDate.AutoIncrement = false;
                colvarBeginDate.IsNullable = false;
                colvarBeginDate.IsPrimaryKey = false;
                colvarBeginDate.IsForeignKey = false;
                colvarBeginDate.IsReadOnly = false;
                colvarBeginDate.DefaultSetting = @"";
                colvarBeginDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBeginDate);

                TableSchema.TableColumn colvarBeginTime = new TableSchema.TableColumn(schema);
                colvarBeginTime.ColumnName = "begin_time";
                colvarBeginTime.DataType = DbType.AnsiString;
                colvarBeginTime.MaxLength = 5;
                colvarBeginTime.AutoIncrement = false;
                colvarBeginTime.IsNullable = false;
                colvarBeginTime.IsPrimaryKey = false;
                colvarBeginTime.IsForeignKey = false;
                colvarBeginTime.IsReadOnly = false;
                colvarBeginTime.DefaultSetting = @"";
                colvarBeginTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBeginTime);

                TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
                colvarEndDate.ColumnName = "end_date";
                colvarEndDate.DataType = DbType.AnsiString;
                colvarEndDate.MaxLength = 10;
                colvarEndDate.AutoIncrement = false;
                colvarEndDate.IsNullable = false;
                colvarEndDate.IsPrimaryKey = false;
                colvarEndDate.IsForeignKey = false;
                colvarEndDate.IsReadOnly = false;
                colvarEndDate.DefaultSetting = @"";
                colvarEndDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEndDate);

                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.AnsiString;
                colvarEndTime.MaxLength = 5;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = false;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                colvarEndTime.DefaultSetting = @"";
                colvarEndTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEndTime);

                TableSchema.TableColumn colvarStyle = new TableSchema.TableColumn(schema);
                colvarStyle.ColumnName = "style";
                colvarStyle.DataType = DbType.Int32;
                colvarStyle.MaxLength = 0;
                colvarStyle.AutoIncrement = false;
                colvarStyle.IsNullable = false;
                colvarStyle.IsPrimaryKey = false;
                colvarStyle.IsForeignKey = false;
                colvarStyle.IsReadOnly = false;
                colvarStyle.DefaultSetting = @"";
                colvarStyle.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStyle);

                TableSchema.TableColumn colvarDealImage = new TableSchema.TableColumn(schema);
                colvarDealImage.ColumnName = "deal_image";
                colvarDealImage.DataType = DbType.AnsiString;
                colvarDealImage.MaxLength = -1;
                colvarDealImage.AutoIncrement = false;
                colvarDealImage.IsNullable = false;
                colvarDealImage.IsPrimaryKey = false;
                colvarDealImage.IsForeignKey = false;
                colvarDealImage.IsReadOnly = false;
                colvarDealImage.DefaultSetting = @"";
                colvarDealImage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDealImage);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateUser = new TableSchema.TableColumn(schema);
                colvarCreateUser.ColumnName = "create_user";
                colvarCreateUser.DataType = DbType.AnsiString;
                colvarCreateUser.MaxLength = 100;
                colvarCreateUser.AutoIncrement = false;
                colvarCreateUser.IsNullable = false;
                colvarCreateUser.IsPrimaryKey = false;
                colvarCreateUser.IsForeignKey = false;
                colvarCreateUser.IsReadOnly = false;
                colvarCreateUser.DefaultSetting = @"";
                colvarCreateUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateUser);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarModifyUser = new TableSchema.TableColumn(schema);
                colvarModifyUser.ColumnName = "modify_user";
                colvarModifyUser.DataType = DbType.AnsiString;
                colvarModifyUser.MaxLength = 100;
                colvarModifyUser.AutoIncrement = false;
                colvarModifyUser.IsNullable = false;
                colvarModifyUser.IsPrimaryKey = false;
                colvarModifyUser.IsForeignKey = false;
                colvarModifyUser.IsReadOnly = false;
                colvarModifyUser.DefaultSetting = @"";
                colvarModifyUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyUser);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("skm_app_style_setup", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("StyleGuid")]
        [Bindable(true)]
        public Guid StyleGuid
        {
            get { return GetColumnValue<Guid>(Columns.StyleGuid); }
            set { SetColumnValue(Columns.StyleGuid, value); }
        }

        [XmlAttribute("BeginDate")]
        [Bindable(true)]
        public string BeginDate
        {
            get { return GetColumnValue<string>(Columns.BeginDate); }
            set { SetColumnValue(Columns.BeginDate, value); }
        }

        [XmlAttribute("BeginTime")]
        [Bindable(true)]
        public string BeginTime
        {
            get { return GetColumnValue<string>(Columns.BeginTime); }
            set { SetColumnValue(Columns.BeginTime, value); }
        }

        [XmlAttribute("EndDate")]
        [Bindable(true)]
        public string EndDate
        {
            get { return GetColumnValue<string>(Columns.EndDate); }
            set { SetColumnValue(Columns.EndDate, value); }
        }

        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public string EndTime
        {
            get { return GetColumnValue<string>(Columns.EndTime); }
            set { SetColumnValue(Columns.EndTime, value); }
        }

        [XmlAttribute("Style")]
        [Bindable(true)]
        public int Style
        {
            get { return GetColumnValue<int>(Columns.Style); }
            set { SetColumnValue(Columns.Style, value); }
        }

        [XmlAttribute("DealImage")]
        [Bindable(true)]
        public string DealImage
        {
            get { return GetColumnValue<string>(Columns.DealImage); }
            set { SetColumnValue(Columns.DealImage, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateUser")]
        [Bindable(true)]
        public string CreateUser
        {
            get { return GetColumnValue<string>(Columns.CreateUser); }
            set { SetColumnValue(Columns.CreateUser, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime
        {
            get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ModifyUser")]
        [Bindable(true)]
        public string ModifyUser
        {
            get { return GetColumnValue<string>(Columns.ModifyUser); }
            set { SetColumnValue(Columns.ModifyUser, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn StyleGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn BeginDateColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn BeginTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn EndDateColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn EndTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn StyleColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn DealImageColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CreateUserColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn ModifyUserColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[12]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"guid";
            public static string StyleGuid = @"style_guid";
            public static string BeginDate = @"begin_date";
            public static string BeginTime = @"begin_time";
            public static string EndDate = @"end_date";
            public static string EndTime = @"end_time";
            public static string Style = @"style";
            public static string DealImage = @"deal_image";
            public static string CreateTime = @"create_time";
            public static string CreateUser = @"create_user";
            public static string ModifyTime = @"modify_time";
            public static string ModifyUser = @"modify_user";
            public static string Status = @"status";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("enterprise_deposit_order_scash_record")]
	public class EnterpriseDepositOrderScashRecord
	{
		public EnterpriseDepositOrderScashRecord()
		{
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("enterprise_deposit_order_id")]
		public int EnterpriseDepositOrderId { get; set; }

		[Column("scash_deposit_id")]
		public int ScashDepositId { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}

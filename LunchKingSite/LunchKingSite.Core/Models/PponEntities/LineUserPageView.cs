using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
    [Table("line_user_page_view")]
    public class LineUserPageView
    {
        public LineUserPageView()
        {
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("deal_guid")]
        public Guid DealGuid { get; set; }

        [Column("line_user_id")]
        public string LineUserId { get; set; }

        [Column("topic")]
        public string Topic { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }
    }
}

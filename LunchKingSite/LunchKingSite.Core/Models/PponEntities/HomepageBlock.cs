﻿using LunchKingSite.Core.Component;
using LunchKingSite.Core.ModelCustom;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace LunchKingSite.Core.Models.PponEntities
{
    [Table("homepage_block")]
    public class HomepageBlock
    {
        public HomepageBlock()
        {
            CreateId = string.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("doc_guid")]
        public Guid DocGuid { get; set; }

        [Column("type_id")]
        public int TypeId { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("content")]
        public string Content { get; set; }

        [Column("latest")]
        public bool Latest { get; set; }

        [Column("enabled")]
        public bool Enabled { get; set; }

        [Column("sequence")]
        public int Sequence { get; set; }

        [Column("create_id")]
        public string CreateId { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }


        public override string ToString()
        {
            return this.Title + " - " + Sequence;
        }
    }
}

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
    [Table("queued_member_push_message")]
    public class QueuedMemberPushMessage
    {
        public QueuedMemberPushMessage()
        {
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("message")]
        public string Message { get; set; }

        [Column("message_data")]
        public string MessageData { get; set; }

        [Column("status")]
        public int Status { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("acceptable_start_hour")]
        public int AcceptableStartHour { get; set; }

        [Column("acceptable_end_hour")]
        public int AcceptableEndHour { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }

        [Column("modify_time")]
        public DateTime? ModifyTime { get; set; }

        [Column("message_type")]
        public ActionEventType MessageType { get; set; }
    }
}

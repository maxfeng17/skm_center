﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("deal_daily_sales_info")]
	public class DealDailySalesInfo
	{
		public DealDailySalesInfo()
		{
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("main_deal_id")]
		public Guid MainDealId { get; set; }

		[Column("the_date")]
		public DateTime TheDate { get; set; }

		[Column("view_count")]
		public int ViewCount { get; set; }

		[Column("web_view_count")]
		public int WebViewCount { get; set; }

		[Column("app_view_count")]
		public int AppViewCount { get; set; }

		[Column("mweb_view_count")]
		public int MwebViewCount { get; set; }

		[Column("ios_app_view_count")]
		public int IosAppViewCount { get; set; }

		[Column("android_app_view_count")]
		public int AndroidAppViewCount { get; set; }

		[Column("ios_view_count")]
		public int IosViewCount { get; set; }

		[Column("android_view_count")]
		public int AndroidViewCount { get; set; }

		[Column("order_count")]
		public int OrderCount { get; set; }

		[Column("web_order_count")]
		public int WebOrderCount { get; set; }

		[Column("app_order_count")]
		public int AppOrderCount { get; set; }

		[Column("mweb_order_count")]
		public int MwebOrderCount { get; set; }

		[Column("ios_app_order_count")]
		public int IosAppOrderCount { get; set; }

		[Column("android_app_order_count")]
		public int AndroidAppOrderCount { get; set; }

		[Column("ios_order_count")]
		public int IosOrderCount { get; set; }

		[Column("android_order_count")]
		public int AndroidOrderCount { get; set; }

		[Column("turnover")]
		public int Turnover { get; set; }

		[Column("web_turnover")]
		public int WebTurnover { get; set; }

		[Column("app_turnover")]
		public int AppTurnover { get; set; }

		[Column("mweb_turnover")]
		public int MwebTurnover { get; set; }

		[Column("ios_app_turnover")]
		public int IosAppTurnover { get; set; }

		[Column("android_app_turnover")]
		public int AndroidAppTurnover { get; set; }

		[Column("ios_turnover")]
		public int IosTurnover { get; set; }

		[Column("android_turnover")]
		public int AndroidTurnover { get; set; }

		[Column("conversion_rate")]
		public decimal ConversionRate { get; set; }

		[Column("web_conversion_rate")]
		public decimal WebConversionRate { get; set; }

		[Column("app_conversion_rate")]
		public decimal AppConversionRate { get; set; }

		[Column("mweb_conversion_rate")]
		public decimal MwebConversionRate { get; set; }

		[Column("ios_app_conversion_rate")]
		public decimal IosAppConversionRate { get; set; }

		[Column("android_app_conversion_rate")]
		public decimal AndroidAppConversionRate { get; set; }

		[Column("ios_conversion_rate")]
		public decimal IosConversionRate { get; set; }

		[Column("android_conversion_rate")]
		public decimal AndroidConversionRate { get; set; }

		[Column("weekly_conversion_rate")]
		public decimal WeeklyConversionRate { get; set; }

		[Column("web_weekly_conversion_rate")]
		public decimal WebWeeklyConversionRate { get; set; }

		[Column("app_weekly_conversion_rate")]
		public decimal AppWeeklyConversionRate { get; set; }

		[Column("mweb_weekly_conversion_rate")]
		public decimal MwebWeeklyConversionRate { get; set; }

		[Column("ios_app_weekly_conversion_rate")]
		public decimal IosAppWeeklyConversionRate { get; set; }

		[Column("android_app_weekly_conversion_rate")]
		public decimal AndroidAppWeeklyConversionRate { get; set; }

		[Column("ios_weekly_conversion_rate")]
		public decimal IosWeeklyConversionRate { get; set; }

		[Column("android_weekly_conversion_rate")]
		public decimal AndroidWeeklyConversionRate { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}

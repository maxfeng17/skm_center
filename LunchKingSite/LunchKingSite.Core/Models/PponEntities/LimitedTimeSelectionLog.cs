﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
    [Table("limited_time_selection_log")]
	public class LimitedTimeSelectionLog
	{
		public LimitedTimeSelectionLog()
		{
			CreateId = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("main_id")]
		public int MainId { get; set; }

		[Column("action")]
		public string Action { get; set; }

		[Column("content")]
		public string Content { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}

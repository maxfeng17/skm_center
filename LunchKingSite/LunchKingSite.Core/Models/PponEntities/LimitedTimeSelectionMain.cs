﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("limited_time_selection_main")]
	public partial class LimitedTimeSelectionMain
	{
		public LimitedTimeSelectionMain()
		{
			CreateId = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("the_date")]
		public DateTime TheDate { get; set; }

		[Column("preview_code")]
		public string PreviewCode { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_id")]
		public string ModifyId { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }        
	}

    public partial class LimitedTimeSelectionMain
    {
        public static bool IsReadOnly(LimitedTimeSelectionMain main)
        {
            if (main == null)
            {
                return false;
            }
            return IsReadOnly(main.TheDate);
        }
        public static bool IsReadOnly(DateTime theDate)
        {
            return theDate.AddHours(12) < DateTime.Now.AddDays(-1);
        }

        public static bool IsRunning(LimitedTimeSelectionMain main)
        {
            if (main == null)
            {
                throw new Exception("IsNeedCodeToView can't allow null value.");
            }
            //main.TheDate.AddHours(12) < now < main.TheDate.AddHours(36)
            DateTime now = DateTime.Now;
            if (now >= main.TheDate.AddHours(12) &&  now <= main.TheDate.AddHours(36))
            {
                return true;
            }
            return false;
        }

    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("solo_edm_send_email")]
	public class SoloEdmSendEmail
	{
		public SoloEdmSendEmail()
		{
			Email = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("main_id")]
		public int MainId { get; set; }

		[Column("email")]
		public string Email { get; set; }
	}
}

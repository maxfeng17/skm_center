using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("event_activity_hit_log")]
	public class EventActivityHitLog
	{
		public EventActivityHitLog()
		{
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("event_activity_id")]
		public int EventActivityId { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("stay_seconds")]
		public decimal StaySeconds { get; set; }

		[Column("open_event")]
		public bool OpenEvent { get; set; }

		[Column("trigger_type")]
		public int TriggerType { get; set; }

		[Column("device_type")]
		public int DeviceType { get; set; }
	}
}

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("ads_log")]
	public class AdsLog
	{
		public AdsLog()
		{
			ItemName = string.Empty;
			SendContent = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("item_name")]
		public string ItemName { get; set; }

		[Column("total_amount")]
		public int TotalAmount { get; set; }

		[Column("send_content")]
		public string SendContent { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}

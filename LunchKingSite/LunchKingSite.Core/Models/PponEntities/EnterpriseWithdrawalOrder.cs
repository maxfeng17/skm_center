using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("enterprise_withdrawal_order")]
	public class EnterpriseWithdrawalOrder
	{
		public EnterpriseWithdrawalOrder()
		{
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("deposit_id")]
		public int DepositId { get; set; }

		[Column("amount")]
		public int? Amount { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
    [Table("deal_page_view")]
    public class DealPageView
    {
        public DealPageView()
        {
            UserIp = string.Empty;
            ReferrerSourceId = string.Empty;
            UserAgent = string.Empty;
            ServerBy = string.Empty;
            RawUrl = string.Empty;
            UrlReference = string.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("deal_id")]
        public Guid DealId { get; set; }

        [Column("main_deal_id")]
        public Guid MainDealId { get; set; }

        [Column("visitor_identity")]
        public Guid VisitorIdentity { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("user_ip")]
        public string UserIp { get; set; }

        [Column("referrer_source_id")]
        public string ReferrerSourceId { get; set; }

        [Column("device_type")]
        public int DeviceType { get; set; }

        [Column("user_agent")]
        public string UserAgent { get; set; }

        [Column("server_by")]
        public string ServerBy { get; set; }

        [Column("start_time")]
        public DateTime StartTime { get; set; }

        [Column("raw_url")]
        public string RawUrl { get; set; }

        [Column("url_reference")]
        public string UrlReference { get; set; }
    }
}

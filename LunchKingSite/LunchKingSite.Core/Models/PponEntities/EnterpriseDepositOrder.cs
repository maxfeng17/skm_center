using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("enterprise_deposit_order")]
	public class EnterpriseDepositOrder
	{
		public EnterpriseDepositOrder()
		{
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("guid")]
		public Guid Guid { get; set; }

		[Column("amount")]
		public int? Amount { get; set; }

		[Column("enterprise_member_id")]
		public int EnterpriseMemberId { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}

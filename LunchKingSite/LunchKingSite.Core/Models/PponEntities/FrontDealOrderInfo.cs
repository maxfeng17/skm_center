﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Models.PponEntities
{
    public class FrontDealOrderInfo
    {
        public int UserId { get; set; }
        public DateTime Date { get; set; }
        public int AppOrderCount { get; set; }
        public int AppOrderTurnoverTotal { get; set; }
        public decimal AppOrderGrossProfitTotal { get; set; }
    }
}

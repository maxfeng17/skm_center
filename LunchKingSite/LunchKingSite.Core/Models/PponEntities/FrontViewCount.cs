using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("front_view_count")]
	public class FrontViewCount
	{
		public FrontViewCount()
		{
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("date")]
		public DateTime Date { get; set; }

		[Column("device_ios")]
		public bool DeviceIos { get; set; }

		[Column("device_android")]
		public bool DeviceAndroid { get; set; }

		[Column("app_frontpage_view_count")]
		public int AppFrontpageViewCount { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}

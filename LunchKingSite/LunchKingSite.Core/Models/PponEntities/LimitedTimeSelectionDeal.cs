using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("limited_time_selection_deals")]
	public class LimitedTimeSelectionDeal
	{
		public LimitedTimeSelectionDeal()
		{
			CreateId = string.Empty;
			ModifyId = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("main_id")]
		public int MainId { get; set; }

		[Column("bid")]
		public Guid Bid { get; set; }

		[Column("sequence")]
		public short Sequence { get; set; }

		[Column("enabled")]
		public bool Enabled { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_id")]
		public string ModifyId { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }
	}
}

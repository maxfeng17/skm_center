﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace LunchKingSite.Core.Models.PponEntities
{
    public class FrontDealOrder
    {
        [Key]
        public Guid id { get; set; }

        public int UserId { get; set; }

        public DateTime CreateTime { get; set; }

        public int Amount { get; set; }

        public int DiscountAmount { get; set; }

        public decimal BaseCost { get; set; }
    }
}

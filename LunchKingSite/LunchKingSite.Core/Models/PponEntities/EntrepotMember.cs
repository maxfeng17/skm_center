using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("entrepot_member")]
	public class EntrepotMember
	{
		public EntrepotMember()
		{
			EntrepotToken = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("entrepot_token")]
		public string EntrepotToken { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("oauth_client_id")]
		public int OauthClientId { get; set; }
	}
}

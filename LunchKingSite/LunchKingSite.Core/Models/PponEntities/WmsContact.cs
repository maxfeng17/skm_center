using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
    [Table("wms_contact")]
    public class WmsContact
    {
        public WmsContact()
        {
            AccountId = string.Empty;
            CreateId = string.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("account_id")]
        public string AccountId { get; set; }

        [Column("returner_name")]
        public string ReturnerName { get; set; }

        [Column("returner_tel")]
        public string ReturnerTel { get; set; }

        [Column("returner_mobile")]
        public string ReturnerMobile { get; set; }

        [Column("returner_email")]
        public string ReturnerEmail { get; set; }

        [Column("returner_zip")]
        public string ReturnerZip { get; set; }

        [Column("returner_address")]
        public string ReturnerAddress { get; set; }

        [Column("warehousing_name")]
        public string WarehousingName { get; set; }

        [Column("warehousing_tel")]
        public string WarehousingTel { get; set; }

        [Column("warehousing_mobile")]
        public string WarehousingMobile { get; set; }

        [Column("warehousing_email")]
        public string WarehousingEmail { get; set; }

        [Column("warehousing_zip")]
        public string WarehousingZip { get; set; }

        [Column("warehousing_address")]
        public string WarehousingAddress { get; set; }

        [Column("create_id")]
        public string CreateId { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }

        [Column("modify_id")]
        public string ModifyId { get; set; }

        [Column("modify_time")]
        public DateTime? ModifyTime { get; set; }
    }
}

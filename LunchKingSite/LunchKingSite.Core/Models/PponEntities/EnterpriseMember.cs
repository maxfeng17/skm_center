using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("enterprise_member")]
	public class EnterpriseMember
	{
		public EnterpriseMember()
		{
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("is_deleted")]
		public bool IsDeleted { get; set; }

		[Column("deleted_time")]
		public DateTime? DeletedTime { get; set; }

		[Column("deleted_by")]
		public string DeletedBy { get; set; }
	}
}

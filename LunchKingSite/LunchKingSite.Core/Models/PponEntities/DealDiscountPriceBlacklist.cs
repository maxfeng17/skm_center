using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
	[Table("deal_discount_price_blacklist")]
	public class DealDiscountPriceBlacklist
	{
		public DealDiscountPriceBlacklist()
		{
			CreateId = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("bid")]
		public Guid Bid { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("created_time")]
		public DateTime CreatedTime { get; set; }
	}
}

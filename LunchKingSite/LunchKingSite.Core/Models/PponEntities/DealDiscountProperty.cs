﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.PponEntities
{
    [Table("deal_discount_property")]
    public class DealDiscountProperty
    {
        public DealDiscountProperty()
        {
            ItemName = string.Empty;
            DiscountCampaignName = string.Empty;
            ApplicableDealItemName = string.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("bid")]
        public Guid Bid { get; set; }

        [Column("deal_id")]
        public int DealId { get; set; }

        [Column("item_name")]
        public string ItemName { get; set; }

        [Column("item_price")]
        public decimal ItemPrice { get; set; }

        [Column("discount_price")]
        public decimal DiscountPrice { get; set; }

        [Column("discount_campaign_id")]
        public int DiscountCampaignId { get; set; }

        [Column("discount_campaign_name")]
        public string DiscountCampaignName { get; set; }

        [Column("applicable_deal_bid")]
        public Guid ApplicableDealBid { get; set; }

        [Column("applicable_deal_id")]
        public int ApplicableDealId { get; set; }

        [Column("applicable_deal_item_name")]
        public string ApplicableDealItemName { get; set; }

        [Column("created_time")]
        public DateTime CreatedTime { get; set; }

        [Column("modified_time")]
        public DateTime ModifiedTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Models.PponEntities
{
    public class AppFrontPageView
    {
        public AppFrontPageView()
        {
        }

        public int UserId { get; set; }

        public DateTime Date { get; set; }

        public bool DeviceIos { get; set; }

        public bool DeviceAndroid { get; set; }
        
        public int AppFrontpageViewCount { get; set; }

        public int AppFrontpageViewTotal { get; set; }

    }
}

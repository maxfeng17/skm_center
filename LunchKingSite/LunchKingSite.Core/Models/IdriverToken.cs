using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the IdriverToken class.
	/// </summary>
    [Serializable]
	public partial class IdriverTokenCollection : RepositoryList<IdriverToken, IdriverTokenCollection>
	{	   
		public IdriverTokenCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>IdriverTokenCollection</returns>
		public IdriverTokenCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                IdriverToken o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the idriver_token table.
	/// </summary>
	[Serializable]
	public partial class IdriverToken : RepositoryRecord<IdriverToken>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public IdriverToken()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public IdriverToken(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("idriver_token", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarToken = new TableSchema.TableColumn(schema);
				colvarToken.ColumnName = "token";
				colvarToken.DataType = DbType.String;
				colvarToken.MaxLength = 256;
				colvarToken.AutoIncrement = false;
				colvarToken.IsNullable = false;
				colvarToken.IsPrimaryKey = true;
				colvarToken.IsForeignKey = false;
				colvarToken.IsReadOnly = false;
				colvarToken.DefaultSetting = @"";
				colvarToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarToken);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = false;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				colvarEnabled.DefaultSetting = @"";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = false;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarMobileOsType = new TableSchema.TableColumn(schema);
				colvarMobileOsType.ColumnName = "mobile_os_type";
				colvarMobileOsType.DataType = DbType.Int32;
				colvarMobileOsType.MaxLength = 0;
				colvarMobileOsType.AutoIncrement = false;
				colvarMobileOsType.IsNullable = false;
				colvarMobileOsType.IsPrimaryKey = false;
				colvarMobileOsType.IsForeignKey = false;
				colvarMobileOsType.IsReadOnly = false;
				colvarMobileOsType.DefaultSetting = @"";
				colvarMobileOsType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileOsType);
				
				TableSchema.TableColumn colvarDeviceId = new TableSchema.TableColumn(schema);
				colvarDeviceId.ColumnName = "device_id";
				colvarDeviceId.DataType = DbType.AnsiString;
				colvarDeviceId.MaxLength = 50;
				colvarDeviceId.AutoIncrement = false;
				colvarDeviceId.IsNullable = true;
				colvarDeviceId.IsPrimaryKey = false;
				colvarDeviceId.IsForeignKey = false;
				colvarDeviceId.IsReadOnly = false;
				colvarDeviceId.DefaultSetting = @"";
				colvarDeviceId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeviceId);
				
				TableSchema.TableColumn colvarDeviceTokenStatus = new TableSchema.TableColumn(schema);
				colvarDeviceTokenStatus.ColumnName = "device_token_status";
				colvarDeviceTokenStatus.DataType = DbType.Int32;
				colvarDeviceTokenStatus.MaxLength = 0;
				colvarDeviceTokenStatus.AutoIncrement = false;
				colvarDeviceTokenStatus.IsNullable = false;
				colvarDeviceTokenStatus.IsPrimaryKey = false;
				colvarDeviceTokenStatus.IsForeignKey = false;
				colvarDeviceTokenStatus.IsReadOnly = false;
				
						colvarDeviceTokenStatus.DefaultSetting = @"((1))";
				colvarDeviceTokenStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeviceTokenStatus);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("idriver_token",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Token")]
		[Bindable(true)]
		public string Token 
		{
			get { return GetColumnValue<string>(Columns.Token); }
			set { SetColumnValue(Columns.Token, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool Enabled 
		{
			get { return GetColumnValue<bool>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int CityId 
		{
			get { return GetColumnValue<int>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("MobileOsType")]
		[Bindable(true)]
		public int MobileOsType 
		{
			get { return GetColumnValue<int>(Columns.MobileOsType); }
			set { SetColumnValue(Columns.MobileOsType, value); }
		}
		  
		[XmlAttribute("DeviceId")]
		[Bindable(true)]
		public string DeviceId 
		{
			get { return GetColumnValue<string>(Columns.DeviceId); }
			set { SetColumnValue(Columns.DeviceId, value); }
		}
		  
		[XmlAttribute("DeviceTokenStatus")]
		[Bindable(true)]
		public int DeviceTokenStatus 
		{
			get { return GetColumnValue<int>(Columns.DeviceTokenStatus); }
			set { SetColumnValue(Columns.DeviceTokenStatus, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn TokenColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MobileOsTypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn DeviceIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn DeviceTokenStatusColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Token = @"token";
			 public static string Enabled = @"enabled";
			 public static string CreateTime = @"create_time";
			 public static string CityId = @"city_id";
			 public static string ModifyTime = @"modify_time";
			 public static string MobileOsType = @"mobile_os_type";
			 public static string DeviceId = @"device_id";
			 public static string DeviceTokenStatus = @"device_token_status";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

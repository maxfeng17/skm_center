using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the EventContent class.
    /// </summary>
    [Serializable]
    public partial class EventContentCollection : RepositoryList<EventContent, EventContentCollection>
    {
        public EventContentCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EventContentCollection</returns>
        public EventContentCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EventContent o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the event_content table.
    /// </summary>
    [Serializable]
    public partial class EventContent : RepositoryRecord<EventContent>, IRecordBase
    {
        #region .ctors and Default Settings

        public EventContent()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public EventContent(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("event_content", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;

                colvarGuid.DefaultSetting = @"(newid())";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarEnable = new TableSchema.TableColumn(schema);
                colvarEnable.ColumnName = "Enable";
                colvarEnable.DataType = DbType.Byte;
                colvarEnable.MaxLength = 0;
                colvarEnable.AutoIncrement = false;
                colvarEnable.IsNullable = true;
                colvarEnable.IsPrimaryKey = false;
                colvarEnable.IsForeignKey = false;
                colvarEnable.IsReadOnly = false;
                colvarEnable.DefaultSetting = @"";
                colvarEnable.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEnable);

                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "Title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 50;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = false;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                colvarTitle.DefaultSetting = @"";
                colvarTitle.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTitle);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "Type";
                colvarType.DataType = DbType.Byte;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = true;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                colvarType.DefaultSetting = @"";
                colvarType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
                colvarUrl.ColumnName = "Url";
                colvarUrl.DataType = DbType.AnsiString;
                colvarUrl.MaxLength = 50;
                colvarUrl.AutoIncrement = false;
                colvarUrl.IsNullable = false;
                colvarUrl.IsPrimaryKey = false;
                colvarUrl.IsForeignKey = false;
                colvarUrl.IsReadOnly = false;
                colvarUrl.DefaultSetting = @"";
                colvarUrl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUrl);

                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "StartTime";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = true;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                colvarStartTime.DefaultSetting = @"";
                colvarStartTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStartTime);

                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "EndTime";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                colvarEndTime.DefaultSetting = @"";
                colvarEndTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEndTime);

                TableSchema.TableColumn colvarTextImg = new TableSchema.TableColumn(schema);
                colvarTextImg.ColumnName = "TextImg";
                colvarTextImg.DataType = DbType.AnsiString;
                colvarTextImg.MaxLength = 150;
                colvarTextImg.AutoIncrement = false;
                colvarTextImg.IsNullable = true;
                colvarTextImg.IsPrimaryKey = false;
                colvarTextImg.IsForeignKey = false;
                colvarTextImg.IsReadOnly = false;
                colvarTextImg.DefaultSetting = @"";
                colvarTextImg.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTextImg);

                TableSchema.TableColumn colvarTextLink = new TableSchema.TableColumn(schema);
                colvarTextLink.ColumnName = "TextLink";
                colvarTextLink.DataType = DbType.AnsiString;
                colvarTextLink.MaxLength = 150;
                colvarTextLink.AutoIncrement = false;
                colvarTextLink.IsNullable = true;
                colvarTextLink.IsPrimaryKey = false;
                colvarTextLink.IsForeignKey = false;
                colvarTextLink.IsReadOnly = false;
                colvarTextLink.DefaultSetting = @"";
                colvarTextLink.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTextLink);

                TableSchema.TableColumn colvarViewImg = new TableSchema.TableColumn(schema);
                colvarViewImg.ColumnName = "ViewImg";
                colvarViewImg.DataType = DbType.AnsiString;
                colvarViewImg.MaxLength = 150;
                colvarViewImg.AutoIncrement = false;
                colvarViewImg.IsNullable = true;
                colvarViewImg.IsPrimaryKey = false;
                colvarViewImg.IsForeignKey = false;
                colvarViewImg.IsReadOnly = false;
                colvarViewImg.DefaultSetting = @"";
                colvarViewImg.ForeignKeyTableName = "";
                schema.Columns.Add(colvarViewImg);

                TableSchema.TableColumn colvarViewLink = new TableSchema.TableColumn(schema);
                colvarViewLink.ColumnName = "ViewLink";
                colvarViewLink.DataType = DbType.AnsiString;
                colvarViewLink.MaxLength = 150;
                colvarViewLink.AutoIncrement = false;
                colvarViewLink.IsNullable = true;
                colvarViewLink.IsPrimaryKey = false;
                colvarViewLink.IsForeignKey = false;
                colvarViewLink.IsReadOnly = false;
                colvarViewLink.DefaultSetting = @"";
                colvarViewLink.ForeignKeyTableName = "";
                schema.Columns.Add(colvarViewLink);

                TableSchema.TableColumn colvarButtonImg = new TableSchema.TableColumn(schema);
                colvarButtonImg.ColumnName = "ButtonImg";
                colvarButtonImg.DataType = DbType.AnsiString;
                colvarButtonImg.MaxLength = 150;
                colvarButtonImg.AutoIncrement = false;
                colvarButtonImg.IsNullable = true;
                colvarButtonImg.IsPrimaryKey = false;
                colvarButtonImg.IsForeignKey = false;
                colvarButtonImg.IsReadOnly = false;
                colvarButtonImg.DefaultSetting = @"";
                colvarButtonImg.ForeignKeyTableName = "";
                schema.Columns.Add(colvarButtonImg);

                TableSchema.TableColumn colvarButtomLink = new TableSchema.TableColumn(schema);
                colvarButtomLink.ColumnName = "ButtomLink";
                colvarButtomLink.DataType = DbType.AnsiString;
                colvarButtomLink.MaxLength = 150;
                colvarButtomLink.AutoIncrement = false;
                colvarButtomLink.IsNullable = true;
                colvarButtomLink.IsPrimaryKey = false;
                colvarButtomLink.IsForeignKey = false;
                colvarButtomLink.IsReadOnly = false;
                colvarButtomLink.DefaultSetting = @"";
                colvarButtomLink.ForeignKeyTableName = "";
                schema.Columns.Add(colvarButtomLink);

                TableSchema.TableColumn colvarEmailImg = new TableSchema.TableColumn(schema);
                colvarEmailImg.ColumnName = "EmailImg";
                colvarEmailImg.DataType = DbType.AnsiString;
                colvarEmailImg.MaxLength = 150;
                colvarEmailImg.AutoIncrement = false;
                colvarEmailImg.IsNullable = true;
                colvarEmailImg.IsPrimaryKey = false;
                colvarEmailImg.IsForeignKey = false;
                colvarEmailImg.IsReadOnly = false;
                colvarEmailImg.DefaultSetting = @"";
                colvarEmailImg.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEmailImg);

                TableSchema.TableColumn colvarEmailLink = new TableSchema.TableColumn(schema);
                colvarEmailLink.ColumnName = "EmailLink";
                colvarEmailLink.DataType = DbType.AnsiString;
                colvarEmailLink.MaxLength = 150;
                colvarEmailLink.AutoIncrement = false;
                colvarEmailLink.IsNullable = true;
                colvarEmailLink.IsPrimaryKey = false;
                colvarEmailLink.IsForeignKey = false;
                colvarEmailLink.IsReadOnly = false;
                colvarEmailLink.DefaultSetting = @"";
                colvarEmailLink.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEmailLink);

                TableSchema.TableColumn colvarFbLikeEnable = new TableSchema.TableColumn(schema);
                colvarFbLikeEnable.ColumnName = "fbLikeEnable";
                colvarFbLikeEnable.DataType = DbType.Byte;
                colvarFbLikeEnable.MaxLength = 0;
                colvarFbLikeEnable.AutoIncrement = false;
                colvarFbLikeEnable.IsNullable = true;
                colvarFbLikeEnable.IsPrimaryKey = false;
                colvarFbLikeEnable.IsForeignKey = false;
                colvarFbLikeEnable.IsReadOnly = false;

                colvarFbLikeEnable.DefaultSetting = @"((1))";
                colvarFbLikeEnable.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFbLikeEnable);

                TableSchema.TableColumn colvarFbShareEnable = new TableSchema.TableColumn(schema);
                colvarFbShareEnable.ColumnName = "fbShareEnable";
                colvarFbShareEnable.DataType = DbType.Byte;
                colvarFbShareEnable.MaxLength = 0;
                colvarFbShareEnable.AutoIncrement = false;
                colvarFbShareEnable.IsNullable = true;
                colvarFbShareEnable.IsPrimaryKey = false;
                colvarFbShareEnable.IsForeignKey = false;
                colvarFbShareEnable.IsReadOnly = false;

                colvarFbShareEnable.DefaultSetting = @"((1))";
                colvarFbShareEnable.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFbShareEnable);

                TableSchema.TableColumn colvarFbSendEnable = new TableSchema.TableColumn(schema);
                colvarFbSendEnable.ColumnName = "fbSendEnable";
                colvarFbSendEnable.DataType = DbType.Byte;
                colvarFbSendEnable.MaxLength = 0;
                colvarFbSendEnable.AutoIncrement = false;
                colvarFbSendEnable.IsNullable = true;
                colvarFbSendEnable.IsPrimaryKey = false;
                colvarFbSendEnable.IsForeignKey = false;
                colvarFbSendEnable.IsReadOnly = false;

                colvarFbSendEnable.DefaultSetting = @"((1))";
                colvarFbSendEnable.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFbSendEnable);

                TableSchema.TableColumn colvarFbLikeText = new TableSchema.TableColumn(schema);
                colvarFbLikeText.ColumnName = "fbLikeText";
                colvarFbLikeText.DataType = DbType.String;
                colvarFbLikeText.MaxLength = 15;
                colvarFbLikeText.AutoIncrement = false;
                colvarFbLikeText.IsNullable = true;
                colvarFbLikeText.IsPrimaryKey = false;
                colvarFbLikeText.IsForeignKey = false;
                colvarFbLikeText.IsReadOnly = false;
                colvarFbLikeText.DefaultSetting = @"";
                colvarFbLikeText.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFbLikeText);

                TableSchema.TableColumn colvarFbShareText = new TableSchema.TableColumn(schema);
                colvarFbShareText.ColumnName = "fbShareText";
                colvarFbShareText.DataType = DbType.String;
                colvarFbShareText.MaxLength = 15;
                colvarFbShareText.AutoIncrement = false;
                colvarFbShareText.IsNullable = true;
                colvarFbShareText.IsPrimaryKey = false;
                colvarFbShareText.IsForeignKey = false;
                colvarFbShareText.IsReadOnly = false;
                colvarFbShareText.DefaultSetting = @"";
                colvarFbShareText.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFbShareText);

                TableSchema.TableColumn colvarFbSendText = new TableSchema.TableColumn(schema);
                colvarFbSendText.ColumnName = "fbSendText";
                colvarFbSendText.DataType = DbType.String;
                colvarFbSendText.MaxLength = 15;
                colvarFbSendText.AutoIncrement = false;
                colvarFbSendText.IsNullable = true;
                colvarFbSendText.IsPrimaryKey = false;
                colvarFbSendText.IsForeignKey = false;
                colvarFbSendText.IsReadOnly = false;
                colvarFbSendText.DefaultSetting = @"";
                colvarFbSendText.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFbSendText);

                TableSchema.TableColumn colvarFbShareCaption = new TableSchema.TableColumn(schema);
                colvarFbShareCaption.ColumnName = "fbShareCaption";
                colvarFbShareCaption.DataType = DbType.String;
                colvarFbShareCaption.MaxLength = 50;
                colvarFbShareCaption.AutoIncrement = false;
                colvarFbShareCaption.IsNullable = true;
                colvarFbShareCaption.IsPrimaryKey = false;
                colvarFbShareCaption.IsForeignKey = false;
                colvarFbShareCaption.IsReadOnly = false;
                colvarFbShareCaption.DefaultSetting = @"";
                colvarFbShareCaption.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFbShareCaption);

                TableSchema.TableColumn colvarFbShareDesc = new TableSchema.TableColumn(schema);
                colvarFbShareDesc.ColumnName = "fbShareDesc";
                colvarFbShareDesc.DataType = DbType.String;
                colvarFbShareDesc.MaxLength = 300;
                colvarFbShareDesc.AutoIncrement = false;
                colvarFbShareDesc.IsNullable = true;
                colvarFbShareDesc.IsPrimaryKey = false;
                colvarFbShareDesc.IsForeignKey = false;
                colvarFbShareDesc.IsReadOnly = false;
                colvarFbShareDesc.DefaultSetting = @"";
                colvarFbShareDesc.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFbShareDesc);

                TableSchema.TableColumn colvarFbShareImg = new TableSchema.TableColumn(schema);
                colvarFbShareImg.ColumnName = "fbShareImg";
                colvarFbShareImg.DataType = DbType.AnsiString;
                colvarFbShareImg.MaxLength = 150;
                colvarFbShareImg.AutoIncrement = false;
                colvarFbShareImg.IsNullable = true;
                colvarFbShareImg.IsPrimaryKey = false;
                colvarFbShareImg.IsForeignKey = false;
                colvarFbShareImg.IsReadOnly = false;
                colvarFbShareImg.DefaultSetting = @"";
                colvarFbShareImg.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFbShareImg);

                TableSchema.TableColumn colvarEventRule = new TableSchema.TableColumn(schema);
                colvarEventRule.ColumnName = "EventRule";
                colvarEventRule.DataType = DbType.String;
                colvarEventRule.MaxLength = 1073741823;
                colvarEventRule.AutoIncrement = false;
                colvarEventRule.IsNullable = true;
                colvarEventRule.IsPrimaryKey = false;
                colvarEventRule.IsForeignKey = false;
                colvarEventRule.IsReadOnly = false;
                colvarEventRule.DefaultSetting = @"";
                colvarEventRule.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventRule);

                TableSchema.TableColumn colvarRewardEnable = new TableSchema.TableColumn(schema);
                colvarRewardEnable.ColumnName = "RewardEnable";
                colvarRewardEnable.DataType = DbType.Byte;
                colvarRewardEnable.MaxLength = 0;
                colvarRewardEnable.AutoIncrement = false;
                colvarRewardEnable.IsNullable = true;
                colvarRewardEnable.IsPrimaryKey = false;
                colvarRewardEnable.IsForeignKey = false;
                colvarRewardEnable.IsReadOnly = false;

                colvarRewardEnable.DefaultSetting = @"((0))";
                colvarRewardEnable.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRewardEnable);

                TableSchema.TableColumn colvarRewardList = new TableSchema.TableColumn(schema);
                colvarRewardList.ColumnName = "RewardList";
                colvarRewardList.DataType = DbType.AnsiString;
                colvarRewardList.MaxLength = 2147483647;
                colvarRewardList.AutoIncrement = false;
                colvarRewardList.IsNullable = true;
                colvarRewardList.IsPrimaryKey = false;
                colvarRewardList.IsForeignKey = false;
                colvarRewardList.IsReadOnly = false;
                colvarRewardList.DefaultSetting = @"";
                colvarRewardList.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRewardList);

                TableSchema.TableColumn colvarCreatTime = new TableSchema.TableColumn(schema);
                colvarCreatTime.ColumnName = "CreatTime";
                colvarCreatTime.DataType = DbType.DateTime;
                colvarCreatTime.MaxLength = 0;
                colvarCreatTime.AutoIncrement = false;
                colvarCreatTime.IsNullable = true;
                colvarCreatTime.IsPrimaryKey = false;
                colvarCreatTime.IsForeignKey = false;
                colvarCreatTime.IsReadOnly = false;

                colvarCreatTime.DefaultSetting = @"(getdate())";
                colvarCreatTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreatTime);

                TableSchema.TableColumn colvarCreatUser = new TableSchema.TableColumn(schema);
                colvarCreatUser.ColumnName = "CreatUser";
                colvarCreatUser.DataType = DbType.AnsiString;
                colvarCreatUser.MaxLength = 50;
                colvarCreatUser.AutoIncrement = false;
                colvarCreatUser.IsNullable = true;
                colvarCreatUser.IsPrimaryKey = false;
                colvarCreatUser.IsForeignKey = false;
                colvarCreatUser.IsReadOnly = false;
                colvarCreatUser.DefaultSetting = @"";
                colvarCreatUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreatUser);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "ModifyTime";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarModifyUser = new TableSchema.TableColumn(schema);
                colvarModifyUser.ColumnName = "ModifyUser";
                colvarModifyUser.DataType = DbType.AnsiString;
                colvarModifyUser.MaxLength = 50;
                colvarModifyUser.AutoIncrement = false;
                colvarModifyUser.IsNullable = true;
                colvarModifyUser.IsPrimaryKey = false;
                colvarModifyUser.IsForeignKey = false;
                colvarModifyUser.IsReadOnly = false;
                colvarModifyUser.DefaultSetting = @"";
                colvarModifyUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyUser);

                TableSchema.TableColumn colvarFbEventUrl = new TableSchema.TableColumn(schema);
                colvarFbEventUrl.ColumnName = "fbEventUrl";
                colvarFbEventUrl.DataType = DbType.AnsiString;
                colvarFbEventUrl.MaxLength = 200;
                colvarFbEventUrl.AutoIncrement = false;
                colvarFbEventUrl.IsNullable = true;
                colvarFbEventUrl.IsPrimaryKey = false;
                colvarFbEventUrl.IsForeignKey = false;
                colvarFbEventUrl.IsReadOnly = false;
                colvarFbEventUrl.DefaultSetting = @"";
                colvarFbEventUrl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFbEventUrl);

                TableSchema.TableColumn colvarCPAurl = new TableSchema.TableColumn(schema);
                colvarCPAurl.ColumnName = "CPAurl";
                colvarCPAurl.DataType = DbType.AnsiString;
                colvarCPAurl.MaxLength = 50;
                colvarCPAurl.AutoIncrement = false;
                colvarCPAurl.IsNullable = true;
                colvarCPAurl.IsPrimaryKey = false;
                colvarCPAurl.IsForeignKey = false;
                colvarCPAurl.IsReadOnly = false;
                colvarCPAurl.DefaultSetting = @"";
                colvarCPAurl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCPAurl);

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "Id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("event_content", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("Enable")]
        [Bindable(true)]
        public byte? Enable
        {
            get { return GetColumnValue<byte?>(Columns.Enable); }
            set { SetColumnValue(Columns.Enable, value); }
        }

        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title
        {
            get { return GetColumnValue<string>(Columns.Title); }
            set { SetColumnValue(Columns.Title, value); }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public byte? Type
        {
            get { return GetColumnValue<byte?>(Columns.Type); }
            set { SetColumnValue(Columns.Type, value); }
        }

        [XmlAttribute("Url")]
        [Bindable(true)]
        public string Url
        {
            get { return GetColumnValue<string>(Columns.Url); }
            set { SetColumnValue(Columns.Url, value); }
        }

        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime? StartTime
        {
            get { return GetColumnValue<DateTime?>(Columns.StartTime); }
            set { SetColumnValue(Columns.StartTime, value); }
        }

        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime
        {
            get { return GetColumnValue<DateTime?>(Columns.EndTime); }
            set { SetColumnValue(Columns.EndTime, value); }
        }

        [XmlAttribute("TextImg")]
        [Bindable(true)]
        public string TextImg
        {
            get { return GetColumnValue<string>(Columns.TextImg); }
            set { SetColumnValue(Columns.TextImg, value); }
        }

        [XmlAttribute("TextLink")]
        [Bindable(true)]
        public string TextLink
        {
            get { return GetColumnValue<string>(Columns.TextLink); }
            set { SetColumnValue(Columns.TextLink, value); }
        }

        [XmlAttribute("ViewImg")]
        [Bindable(true)]
        public string ViewImg
        {
            get { return GetColumnValue<string>(Columns.ViewImg); }
            set { SetColumnValue(Columns.ViewImg, value); }
        }

        [XmlAttribute("ViewLink")]
        [Bindable(true)]
        public string ViewLink
        {
            get { return GetColumnValue<string>(Columns.ViewLink); }
            set { SetColumnValue(Columns.ViewLink, value); }
        }

        [XmlAttribute("ButtonImg")]
        [Bindable(true)]
        public string ButtonImg
        {
            get { return GetColumnValue<string>(Columns.ButtonImg); }
            set { SetColumnValue(Columns.ButtonImg, value); }
        }

        [XmlAttribute("ButtomLink")]
        [Bindable(true)]
        public string ButtomLink
        {
            get { return GetColumnValue<string>(Columns.ButtomLink); }
            set { SetColumnValue(Columns.ButtomLink, value); }
        }

        [XmlAttribute("EmailImg")]
        [Bindable(true)]
        public string EmailImg
        {
            get { return GetColumnValue<string>(Columns.EmailImg); }
            set { SetColumnValue(Columns.EmailImg, value); }
        }

        [XmlAttribute("EmailLink")]
        [Bindable(true)]
        public string EmailLink
        {
            get { return GetColumnValue<string>(Columns.EmailLink); }
            set { SetColumnValue(Columns.EmailLink, value); }
        }

        [XmlAttribute("FbLikeEnable")]
        [Bindable(true)]
        public byte? FbLikeEnable
        {
            get { return GetColumnValue<byte?>(Columns.FbLikeEnable); }
            set { SetColumnValue(Columns.FbLikeEnable, value); }
        }

        [XmlAttribute("FbShareEnable")]
        [Bindable(true)]
        public byte? FbShareEnable
        {
            get { return GetColumnValue<byte?>(Columns.FbShareEnable); }
            set { SetColumnValue(Columns.FbShareEnable, value); }
        }

        [XmlAttribute("FbSendEnable")]
        [Bindable(true)]
        public byte? FbSendEnable
        {
            get { return GetColumnValue<byte?>(Columns.FbSendEnable); }
            set { SetColumnValue(Columns.FbSendEnable, value); }
        }

        [XmlAttribute("FbLikeText")]
        [Bindable(true)]
        public string FbLikeText
        {
            get { return GetColumnValue<string>(Columns.FbLikeText); }
            set { SetColumnValue(Columns.FbLikeText, value); }
        }

        [XmlAttribute("FbShareText")]
        [Bindable(true)]
        public string FbShareText
        {
            get { return GetColumnValue<string>(Columns.FbShareText); }
            set { SetColumnValue(Columns.FbShareText, value); }
        }

        [XmlAttribute("FbSendText")]
        [Bindable(true)]
        public string FbSendText
        {
            get { return GetColumnValue<string>(Columns.FbSendText); }
            set { SetColumnValue(Columns.FbSendText, value); }
        }

        [XmlAttribute("FbShareCaption")]
        [Bindable(true)]
        public string FbShareCaption
        {
            get { return GetColumnValue<string>(Columns.FbShareCaption); }
            set { SetColumnValue(Columns.FbShareCaption, value); }
        }

        [XmlAttribute("FbShareDesc")]
        [Bindable(true)]
        public string FbShareDesc
        {
            get { return GetColumnValue<string>(Columns.FbShareDesc); }
            set { SetColumnValue(Columns.FbShareDesc, value); }
        }

        [XmlAttribute("FbShareImg")]
        [Bindable(true)]
        public string FbShareImg
        {
            get { return GetColumnValue<string>(Columns.FbShareImg); }
            set { SetColumnValue(Columns.FbShareImg, value); }
        }

        [XmlAttribute("EventRule")]
        [Bindable(true)]
        public string EventRule
        {
            get { return GetColumnValue<string>(Columns.EventRule); }
            set { SetColumnValue(Columns.EventRule, value); }
        }

        [XmlAttribute("RewardEnable")]
        [Bindable(true)]
        public byte? RewardEnable
        {
            get { return GetColumnValue<byte?>(Columns.RewardEnable); }
            set { SetColumnValue(Columns.RewardEnable, value); }
        }

        [XmlAttribute("RewardList")]
        [Bindable(true)]
        public string RewardList
        {
            get { return GetColumnValue<string>(Columns.RewardList); }
            set { SetColumnValue(Columns.RewardList, value); }
        }

        [XmlAttribute("CreatTime")]
        [Bindable(true)]
        public DateTime? CreatTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreatTime); }
            set { SetColumnValue(Columns.CreatTime, value); }
        }

        [XmlAttribute("CreatUser")]
        [Bindable(true)]
        public string CreatUser
        {
            get { return GetColumnValue<string>(Columns.CreatUser); }
            set { SetColumnValue(Columns.CreatUser, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ModifyUser")]
        [Bindable(true)]
        public string ModifyUser
        {
            get { return GetColumnValue<string>(Columns.ModifyUser); }
            set { SetColumnValue(Columns.ModifyUser, value); }
        }

        [XmlAttribute("FbEventUrl")]
        [Bindable(true)]
        public string FbEventUrl
        {
            get { return GetColumnValue<string>(Columns.FbEventUrl); }
            set { SetColumnValue(Columns.FbEventUrl, value); }
        }

        [XmlAttribute("CPAurl")]
        [Bindable(true)]
        public string CPAurl
        {
            get { return GetColumnValue<string>(Columns.CPAurl); }
            set { SetColumnValue(Columns.CPAurl, value); }
        }

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn EnableColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn UrlColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn StartTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn EndTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn TextImgColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn TextLinkColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn ViewImgColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn ViewLinkColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn ButtonImgColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn ButtomLinkColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn EmailImgColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn EmailLinkColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn FbLikeEnableColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn FbShareEnableColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn FbSendEnableColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn FbLikeTextColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn FbShareTextColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn FbSendTextColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn FbShareCaptionColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn FbShareDescColumn
        {
            get { return Schema.Columns[22]; }
        }



        public static TableSchema.TableColumn FbShareImgColumn
        {
            get { return Schema.Columns[23]; }
        }



        public static TableSchema.TableColumn EventRuleColumn
        {
            get { return Schema.Columns[24]; }
        }



        public static TableSchema.TableColumn RewardEnableColumn
        {
            get { return Schema.Columns[25]; }
        }



        public static TableSchema.TableColumn RewardListColumn
        {
            get { return Schema.Columns[26]; }
        }



        public static TableSchema.TableColumn CreatTimeColumn
        {
            get { return Schema.Columns[27]; }
        }



        public static TableSchema.TableColumn CreatUserColumn
        {
            get { return Schema.Columns[28]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[29]; }
        }



        public static TableSchema.TableColumn ModifyUserColumn
        {
            get { return Schema.Columns[30]; }
        }



        public static TableSchema.TableColumn FbEventUrlColumn
        {
            get { return Schema.Columns[31]; }
        }



        public static TableSchema.TableColumn CPAurlColumn
        {
            get { return Schema.Columns[32]; }
        }



        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[33]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"GUID";
            public static string Enable = @"Enable";
            public static string Title = @"Title";
            public static string Type = @"Type";
            public static string Url = @"Url";
            public static string StartTime = @"StartTime";
            public static string EndTime = @"EndTime";
            public static string TextImg = @"TextImg";
            public static string TextLink = @"TextLink";
            public static string ViewImg = @"ViewImg";
            public static string ViewLink = @"ViewLink";
            public static string ButtonImg = @"ButtonImg";
            public static string ButtomLink = @"ButtomLink";
            public static string EmailImg = @"EmailImg";
            public static string EmailLink = @"EmailLink";
            public static string FbLikeEnable = @"fbLikeEnable";
            public static string FbShareEnable = @"fbShareEnable";
            public static string FbSendEnable = @"fbSendEnable";
            public static string FbLikeText = @"fbLikeText";
            public static string FbShareText = @"fbShareText";
            public static string FbSendText = @"fbSendText";
            public static string FbShareCaption = @"fbShareCaption";
            public static string FbShareDesc = @"fbShareDesc";
            public static string FbShareImg = @"fbShareImg";
            public static string EventRule = @"EventRule";
            public static string RewardEnable = @"RewardEnable";
            public static string RewardList = @"RewardList";
            public static string CreatTime = @"CreatTime";
            public static string CreatUser = @"CreatUser";
            public static string ModifyTime = @"ModifyTime";
            public static string ModifyUser = @"ModifyUser";
            public static string FbEventUrl = @"fbEventUrl";
            public static string CPAurl = @"CPAurl";
            public static string Id = @"Id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

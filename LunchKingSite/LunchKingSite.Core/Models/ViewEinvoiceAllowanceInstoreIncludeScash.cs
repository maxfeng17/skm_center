using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEinvoiceAllowanceInstoreIncludeScash class.
    /// </summary>
    [Serializable]
    public partial class ViewEinvoiceAllowanceInstoreIncludeScashCollection : ReadOnlyList<ViewEinvoiceAllowanceInstoreIncludeScash, ViewEinvoiceAllowanceInstoreIncludeScashCollection>
    {
        public ViewEinvoiceAllowanceInstoreIncludeScashCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_einvoice_allowance_instore_includeScash view.
    /// </summary>
    [Serializable]
    public partial class ViewEinvoiceAllowanceInstoreIncludeScash : ReadOnlyRecord<ViewEinvoiceAllowanceInstoreIncludeScash>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_einvoice_allowance_instore_includeScash", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarMainId = new TableSchema.TableColumn(schema);
                colvarMainId.ColumnName = "main_id";
                colvarMainId.DataType = DbType.Int32;
                colvarMainId.MaxLength = 0;
                colvarMainId.AutoIncrement = false;
                colvarMainId.IsNullable = false;
                colvarMainId.IsPrimaryKey = false;
                colvarMainId.IsForeignKey = false;
                colvarMainId.IsReadOnly = false;

                schema.Columns.Add(colvarMainId);

                TableSchema.TableColumn colvarInvoiceNumber = new TableSchema.TableColumn(schema);
                colvarInvoiceNumber.ColumnName = "invoice_number";
                colvarInvoiceNumber.DataType = DbType.String;
                colvarInvoiceNumber.MaxLength = 10;
                colvarInvoiceNumber.AutoIncrement = false;
                colvarInvoiceNumber.IsNullable = true;
                colvarInvoiceNumber.IsPrimaryKey = false;
                colvarInvoiceNumber.IsForeignKey = false;
                colvarInvoiceNumber.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceNumber);

                TableSchema.TableColumn colvarInvoiceNumberTime = new TableSchema.TableColumn(schema);
                colvarInvoiceNumberTime.ColumnName = "invoice_number_time";
                colvarInvoiceNumberTime.DataType = DbType.DateTime;
                colvarInvoiceNumberTime.MaxLength = 0;
                colvarInvoiceNumberTime.AutoIncrement = false;
                colvarInvoiceNumberTime.IsNullable = true;
                colvarInvoiceNumberTime.IsPrimaryKey = false;
                colvarInvoiceNumberTime.IsForeignKey = false;
                colvarInvoiceNumberTime.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceNumberTime);

                TableSchema.TableColumn colvarOrderTime = new TableSchema.TableColumn(schema);
                colvarOrderTime.ColumnName = "order_time";
                colvarOrderTime.DataType = DbType.DateTime;
                colvarOrderTime.MaxLength = 0;
                colvarOrderTime.AutoIncrement = false;
                colvarOrderTime.IsNullable = false;
                colvarOrderTime.IsPrimaryKey = false;
                colvarOrderTime.IsForeignKey = false;
                colvarOrderTime.IsReadOnly = false;

                schema.Columns.Add(colvarOrderTime);

                TableSchema.TableColumn colvarOrderAmount = new TableSchema.TableColumn(schema);
                colvarOrderAmount.ColumnName = "order_amount";
                colvarOrderAmount.DataType = DbType.Currency;
                colvarOrderAmount.MaxLength = 0;
                colvarOrderAmount.AutoIncrement = false;
                colvarOrderAmount.IsNullable = false;
                colvarOrderAmount.IsPrimaryKey = false;
                colvarOrderAmount.IsForeignKey = false;
                colvarOrderAmount.IsReadOnly = false;

                schema.Columns.Add(colvarOrderAmount);

                TableSchema.TableColumn colvarInvoiceSumAmount = new TableSchema.TableColumn(schema);
                colvarInvoiceSumAmount.ColumnName = "invoice_sum_amount";
                colvarInvoiceSumAmount.DataType = DbType.Currency;
                colvarInvoiceSumAmount.MaxLength = 0;
                colvarInvoiceSumAmount.AutoIncrement = false;
                colvarInvoiceSumAmount.IsNullable = false;
                colvarInvoiceSumAmount.IsPrimaryKey = false;
                colvarInvoiceSumAmount.IsForeignKey = false;
                colvarInvoiceSumAmount.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceSumAmount);

                TableSchema.TableColumn colvarInvoiceMode = new TableSchema.TableColumn(schema);
                colvarInvoiceMode.ColumnName = "invoice_mode";
                colvarInvoiceMode.DataType = DbType.Int32;
                colvarInvoiceMode.MaxLength = 0;
                colvarInvoiceMode.AutoIncrement = false;
                colvarInvoiceMode.IsNullable = false;
                colvarInvoiceMode.IsPrimaryKey = false;
                colvarInvoiceMode.IsForeignKey = false;
                colvarInvoiceMode.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceMode);

                TableSchema.TableColumn colvarTransId = new TableSchema.TableColumn(schema);
                colvarTransId.ColumnName = "trans_id";
                colvarTransId.DataType = DbType.AnsiString;
                colvarTransId.MaxLength = 40;
                colvarTransId.AutoIncrement = false;
                colvarTransId.IsNullable = false;
                colvarTransId.IsPrimaryKey = false;
                colvarTransId.IsForeignKey = false;
                colvarTransId.IsReadOnly = false;

                schema.Columns.Add(colvarTransId);

                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;

                schema.Columns.Add(colvarTrustId);

                TableSchema.TableColumn colvarRefundTime = new TableSchema.TableColumn(schema);
                colvarRefundTime.ColumnName = "refund_time";
                colvarRefundTime.DataType = DbType.DateTime;
                colvarRefundTime.MaxLength = 0;
                colvarRefundTime.AutoIncrement = false;
                colvarRefundTime.IsNullable = false;
                colvarRefundTime.IsPrimaryKey = false;
                colvarRefundTime.IsForeignKey = false;
                colvarRefundTime.IsReadOnly = false;

                schema.Columns.Add(colvarRefundTime);

                TableSchema.TableColumn colvarCashAmount = new TableSchema.TableColumn(schema);
                colvarCashAmount.ColumnName = "cash_amount";
                colvarCashAmount.DataType = DbType.Int32;
                colvarCashAmount.MaxLength = 0;
                colvarCashAmount.AutoIncrement = false;
                colvarCashAmount.IsNullable = true;
                colvarCashAmount.IsPrimaryKey = false;
                colvarCashAmount.IsForeignKey = false;
                colvarCashAmount.IsReadOnly = false;

                schema.Columns.Add(colvarCashAmount);

                TableSchema.TableColumn colvarAllowanceStatus = new TableSchema.TableColumn(schema);
                colvarAllowanceStatus.ColumnName = "allowance_status";
                colvarAllowanceStatus.DataType = DbType.Int32;
                colvarAllowanceStatus.MaxLength = 0;
                colvarAllowanceStatus.AutoIncrement = false;
                colvarAllowanceStatus.IsNullable = true;
                colvarAllowanceStatus.IsPrimaryKey = false;
                colvarAllowanceStatus.IsForeignKey = false;
                colvarAllowanceStatus.IsReadOnly = false;

                schema.Columns.Add(colvarAllowanceStatus);

                TableSchema.TableColumn colvarInvoicePapered = new TableSchema.TableColumn(schema);
                colvarInvoicePapered.ColumnName = "invoice_papered";
                colvarInvoicePapered.DataType = DbType.Boolean;
                colvarInvoicePapered.MaxLength = 0;
                colvarInvoicePapered.AutoIncrement = false;
                colvarInvoicePapered.IsNullable = false;
                colvarInvoicePapered.IsPrimaryKey = false;
                colvarInvoicePapered.IsForeignKey = false;
                colvarInvoicePapered.IsReadOnly = false;

                schema.Columns.Add(colvarInvoicePapered);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_einvoice_allowance_instore_includeScash",schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewEinvoiceAllowanceInstoreIncludeScash()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEinvoiceAllowanceInstoreIncludeScash(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewEinvoiceAllowanceInstoreIncludeScash(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewEinvoiceAllowanceInstoreIncludeScash(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("MainId")]
        [Bindable(true)]
        public int MainId
        {
            get
            {
                return GetColumnValue<int>("main_id");
            }
            set
            {
                SetColumnValue("main_id", value);
            }
        }

        [XmlAttribute("InvoiceNumber")]
        [Bindable(true)]
        public string InvoiceNumber
        {
            get
            {
                return GetColumnValue<string>("invoice_number");
            }
            set
            {
                SetColumnValue("invoice_number", value);
            }
        }

        [XmlAttribute("InvoiceNumberTime")]
        [Bindable(true)]
        public DateTime? InvoiceNumberTime
        {
            get
            {
                return GetColumnValue<DateTime?>("invoice_number_time");
            }
            set
            {
                SetColumnValue("invoice_number_time", value);
            }
        }

        [XmlAttribute("OrderTime")]
        [Bindable(true)]
        public DateTime OrderTime
        {
            get
            {
                return GetColumnValue<DateTime>("order_time");
            }
            set
            {
                SetColumnValue("order_time", value);
            }
        }

        [XmlAttribute("OrderAmount")]
        [Bindable(true)]
        public decimal OrderAmount
        {
            get
            {
                return GetColumnValue<decimal>("order_amount");
            }
            set
            {
                SetColumnValue("order_amount", value);
            }
        }

        [XmlAttribute("InvoiceSumAmount")]
        [Bindable(true)]
        public decimal InvoiceSumAmount
        {
            get
            {
                return GetColumnValue<decimal>("invoice_sum_amount");
            }
            set
            {
                SetColumnValue("invoice_sum_amount", value);
            }
        }

        [XmlAttribute("InvoiceMode")]
        [Bindable(true)]
        public int InvoiceMode
        {
            get
            {
                return GetColumnValue<int>("invoice_mode");
            }
            set
            {
                SetColumnValue("invoice_mode", value);
            }
        }

        [XmlAttribute("TransId")]
        [Bindable(true)]
        public string TransId
        {
            get
            {
                return GetColumnValue<string>("trans_id");
            }
            set
            {
                SetColumnValue("trans_id", value);
            }
        }

        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId
        {
            get
            {
                return GetColumnValue<Guid>("trust_id");
            }
            set
            {
                SetColumnValue("trust_id", value);
            }
        }

        [XmlAttribute("RefundTime")]
        [Bindable(true)]
        public DateTime RefundTime
        {
            get
            {
                return GetColumnValue<DateTime>("refund_time");
            }
            set
            {
                SetColumnValue("refund_time", value);
            }
        }

        [XmlAttribute("CashAmount")]
        [Bindable(true)]
        public int? CashAmount
        {
            get
            {
                return GetColumnValue<int?>("cash_amount");
            }
            set
            {
                SetColumnValue("cash_amount", value);
            }
        }

        [XmlAttribute("AllowanceStatus")]
        [Bindable(true)]
        public int? AllowanceStatus
        {
            get
            {
                return GetColumnValue<int?>("allowance_status");
            }
            set
            {
                SetColumnValue("allowance_status", value);
            }
        }

        [XmlAttribute("InvoicePapered")]
        [Bindable(true)]
        public bool InvoicePapered
        {
            get
            {
                return GetColumnValue<bool>("invoice_papered");
            }
            set
            {
                SetColumnValue("invoice_papered", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string MainId = @"main_id";

            public static string InvoiceNumber = @"invoice_number";

            public static string InvoiceNumberTime = @"invoice_number_time";

            public static string OrderTime = @"order_time";

            public static string OrderAmount = @"order_amount";

            public static string InvoiceSumAmount = @"invoice_sum_amount";

            public static string InvoiceMode = @"invoice_mode";

            public static string TransId = @"trans_id";

            public static string TrustId = @"trust_id";

            public static string RefundTime = @"refund_time";

            public static string CashAmount = @"cash_amount";

            public static string AllowanceStatus = @"allowance_status";

            public static string InvoicePapered = @"invoice_papered";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

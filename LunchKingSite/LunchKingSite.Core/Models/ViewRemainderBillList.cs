using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewRemainderBillList class.
    /// </summary>
    [Serializable]
    public partial class ViewRemainderBillListCollection : ReadOnlyList<ViewRemainderBillList, ViewRemainderBillListCollection>
    {        
        public ViewRemainderBillListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_remainder_bill_list view.
    /// </summary>
    [Serializable]
    public partial class ViewRemainderBillList : ReadOnlyRecord<ViewRemainderBillList>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_remainder_bill_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarBillNumber = new TableSchema.TableColumn(schema);
                colvarBillNumber.ColumnName = "bill_number";
                colvarBillNumber.DataType = DbType.String;
                colvarBillNumber.MaxLength = 10;
                colvarBillNumber.AutoIncrement = false;
                colvarBillNumber.IsNullable = false;
                colvarBillNumber.IsPrimaryKey = false;
                colvarBillNumber.IsForeignKey = false;
                colvarBillNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillNumber);
                
                TableSchema.TableColumn colvarRemainder = new TableSchema.TableColumn(schema);
                colvarRemainder.ColumnName = "remainder";
                colvarRemainder.DataType = DbType.Int32;
                colvarRemainder.MaxLength = 0;
                colvarRemainder.AutoIncrement = false;
                colvarRemainder.IsNullable = true;
                colvarRemainder.IsPrimaryKey = false;
                colvarRemainder.IsForeignKey = false;
                colvarRemainder.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemainder);
                
                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 250;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = true;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemark);
                
                TableSchema.TableColumn colvarInvoiceComId = new TableSchema.TableColumn(schema);
                colvarInvoiceComId.ColumnName = "invoice_com_id";
                colvarInvoiceComId.DataType = DbType.String;
                colvarInvoiceComId.MaxLength = 10;
                colvarInvoiceComId.AutoIncrement = false;
                colvarInvoiceComId.IsNullable = true;
                colvarInvoiceComId.IsPrimaryKey = false;
                colvarInvoiceComId.IsForeignKey = false;
                colvarInvoiceComId.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceComId);
                
                TableSchema.TableColumn colvarReceiptType = new TableSchema.TableColumn(schema);
                colvarReceiptType.ColumnName = "receiptType";
                colvarReceiptType.DataType = DbType.Int32;
                colvarReceiptType.MaxLength = 0;
                colvarReceiptType.AutoIncrement = false;
                colvarReceiptType.IsNullable = false;
                colvarReceiptType.IsPrimaryKey = false;
                colvarReceiptType.IsForeignKey = false;
                colvarReceiptType.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiptType);
                
                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 50;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarDeptId = new TableSchema.TableColumn(schema);
                colvarDeptId.ColumnName = "dept_id";
                colvarDeptId.DataType = DbType.AnsiString;
                colvarDeptId.MaxLength = 50;
                colvarDeptId.AutoIncrement = false;
                colvarDeptId.IsNullable = true;
                colvarDeptId.IsPrimaryKey = false;
                colvarDeptId.IsForeignKey = false;
                colvarDeptId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeptId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_remainder_bill_list",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewRemainderBillList()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewRemainderBillList(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewRemainderBillList(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewRemainderBillList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("BillNumber")]
        [Bindable(true)]
        public string BillNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("bill_number");
		    }
            set 
		    {
			    SetColumnValue("bill_number", value);
            }
        }
	      
        [XmlAttribute("Remainder")]
        [Bindable(true)]
        public int? Remainder 
	    {
		    get
		    {
			    return GetColumnValue<int?>("remainder");
		    }
            set 
		    {
			    SetColumnValue("remainder", value);
            }
        }
	      
        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark 
	    {
		    get
		    {
			    return GetColumnValue<string>("remark");
		    }
            set 
		    {
			    SetColumnValue("remark", value);
            }
        }
	      
        [XmlAttribute("InvoiceComId")]
        [Bindable(true)]
        public string InvoiceComId 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_com_id");
		    }
            set 
		    {
			    SetColumnValue("invoice_com_id", value);
            }
        }
	      
        [XmlAttribute("ReceiptType")]
        [Bindable(true)]
        public int ReceiptType 
	    {
		    get
		    {
			    return GetColumnValue<int>("receiptType");
		    }
            set 
		    {
			    SetColumnValue("receiptType", value);
            }
        }
	      
        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message 
	    {
		    get
		    {
			    return GetColumnValue<string>("message");
		    }
            set 
		    {
			    SetColumnValue("message", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("DeptId")]
        [Bindable(true)]
        public string DeptId 
	    {
		    get
		    {
			    return GetColumnValue<string>("dept_id");
		    }
            set 
		    {
			    SetColumnValue("dept_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string CreateTime = @"create_time";
            
            public static string BillNumber = @"bill_number";
            
            public static string Remainder = @"remainder";
            
            public static string Remark = @"remark";
            
            public static string InvoiceComId = @"invoice_com_id";
            
            public static string ReceiptType = @"receiptType";
            
            public static string Message = @"message";
            
            public static string SellerName = @"seller_name";
            
            public static string DeptId = @"dept_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

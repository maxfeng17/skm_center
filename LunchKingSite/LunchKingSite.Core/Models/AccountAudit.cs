using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the AccountAudit class.
	/// </summary>
    [Serializable]
	public partial class AccountAuditCollection : RepositoryList<AccountAudit, AccountAuditCollection>
	{	   
		public AccountAuditCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>AccountAuditCollection</returns>
		public AccountAuditCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                AccountAudit o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the account_audit table.
	/// </summary>
	[Serializable]
	public partial class AccountAudit : RepositoryRecord<AccountAudit>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public AccountAudit()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public AccountAudit(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("account_audit", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int64;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarAction = new TableSchema.TableColumn(schema);
				colvarAction.ColumnName = "action";
				colvarAction.DataType = DbType.Int32;
				colvarAction.MaxLength = 0;
				colvarAction.AutoIncrement = false;
				colvarAction.IsNullable = false;
				colvarAction.IsPrimaryKey = false;
				colvarAction.IsForeignKey = false;
				colvarAction.IsReadOnly = false;
				colvarAction.DefaultSetting = @"";
				colvarAction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAction);
				
				TableSchema.TableColumn colvarIsSuccess = new TableSchema.TableColumn(schema);
				colvarIsSuccess.ColumnName = "is_success";
				colvarIsSuccess.DataType = DbType.Boolean;
				colvarIsSuccess.MaxLength = 0;
				colvarIsSuccess.AutoIncrement = false;
				colvarIsSuccess.IsNullable = false;
				colvarIsSuccess.IsPrimaryKey = false;
				colvarIsSuccess.IsForeignKey = false;
				colvarIsSuccess.IsReadOnly = false;
				colvarIsSuccess.DefaultSetting = @"";
				colvarIsSuccess.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSuccess);
				
				TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
				colvarAccountId.ColumnName = "account_id";
				colvarAccountId.DataType = DbType.String;
				colvarAccountId.MaxLength = 256;
				colvarAccountId.AutoIncrement = false;
				colvarAccountId.IsNullable = false;
				colvarAccountId.IsPrimaryKey = false;
				colvarAccountId.IsForeignKey = false;
				colvarAccountId.IsReadOnly = false;
				colvarAccountId.DefaultSetting = @"";
				colvarAccountId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountId);
				
				TableSchema.TableColumn colvarSourceIp = new TableSchema.TableColumn(schema);
				colvarSourceIp.ColumnName = "source_ip";
				colvarSourceIp.DataType = DbType.AnsiString;
				colvarSourceIp.MaxLength = 39;
				colvarSourceIp.AutoIncrement = false;
				colvarSourceIp.IsNullable = false;
				colvarSourceIp.IsPrimaryKey = false;
				colvarSourceIp.IsForeignKey = false;
				colvarSourceIp.IsReadOnly = false;
				colvarSourceIp.DefaultSetting = @"";
				colvarSourceIp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSourceIp);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 100;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = true;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = true;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = true;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				
					colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarHost = new TableSchema.TableColumn(schema);
				colvarHost.ColumnName = "host";
				colvarHost.DataType = DbType.String;
				colvarHost.MaxLength = 50;
				colvarHost.AutoIncrement = false;
				colvarHost.IsNullable = true;
				colvarHost.IsPrimaryKey = false;
				colvarHost.IsForeignKey = false;
				colvarHost.IsReadOnly = false;
				colvarHost.DefaultSetting = @"";
				colvarHost.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHost);
				
				TableSchema.TableColumn colvarDeviceType = new TableSchema.TableColumn(schema);
				colvarDeviceType.ColumnName = "device_type";
				colvarDeviceType.DataType = DbType.Int32;
				colvarDeviceType.MaxLength = 0;
				colvarDeviceType.AutoIncrement = false;
				colvarDeviceType.IsNullable = true;
				colvarDeviceType.IsPrimaryKey = false;
				colvarDeviceType.IsForeignKey = false;
				colvarDeviceType.IsReadOnly = false;
				colvarDeviceType.DefaultSetting = @"";
				colvarDeviceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeviceType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("account_audit",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public long Id 
		{
			get { return GetColumnValue<long>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Action")]
		[Bindable(true)]
		public int Action 
		{
			get { return GetColumnValue<int>(Columns.Action); }
			set { SetColumnValue(Columns.Action, value); }
		}
		  
		[XmlAttribute("IsSuccess")]
		[Bindable(true)]
		public bool IsSuccess 
		{
			get { return GetColumnValue<bool>(Columns.IsSuccess); }
			set { SetColumnValue(Columns.IsSuccess, value); }
		}
		  
		[XmlAttribute("AccountId")]
		[Bindable(true)]
		public string AccountId 
		{
			get { return GetColumnValue<string>(Columns.AccountId); }
			set { SetColumnValue(Columns.AccountId, value); }
		}
		  
		[XmlAttribute("SourceIp")]
		[Bindable(true)]
		public string SourceIp 
		{
			get { return GetColumnValue<string>(Columns.SourceIp); }
			set { SetColumnValue(Columns.SourceIp, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int? UserId 
		{
			get { return GetColumnValue<int?>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("Host")]
		[Bindable(true)]
		public string Host 
		{
			get { return GetColumnValue<string>(Columns.Host); }
			set { SetColumnValue(Columns.Host, value); }
		}
		  
		[XmlAttribute("DeviceType")]
		[Bindable(true)]
		public int? DeviceType 
		{
			get { return GetColumnValue<int?>(Columns.DeviceType); }
			set { SetColumnValue(Columns.DeviceType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IsSuccessColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SourceIpColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn HostColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn DeviceTypeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Action = @"action";
			 public static string IsSuccess = @"is_success";
			 public static string AccountId = @"account_id";
			 public static string SourceIp = @"source_ip";
			 public static string CreateTime = @"create_time";
			 public static string Memo = @"memo";
			 public static string UserId = @"user_id";
			 public static string CreateId = @"create_id";
			 public static string Host = @"host";
			 public static string DeviceType = @"device_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

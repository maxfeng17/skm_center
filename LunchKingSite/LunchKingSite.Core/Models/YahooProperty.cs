using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the YahooProperty class.
	/// </summary>
    [Serializable]
	public partial class YahooPropertyCollection : RepositoryList<YahooProperty, YahooPropertyCollection>
	{	   
		public YahooPropertyCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>YahooPropertyCollection</returns>
		public YahooPropertyCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                YahooProperty o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the yahoo_property table.
	/// </summary>
	[Serializable]
	public partial class YahooProperty : RepositoryRecord<YahooProperty>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public YahooProperty()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public YahooProperty(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("yahoo_property", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = true;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarPid = new TableSchema.TableColumn(schema);
				colvarPid.ColumnName = "pid";
				colvarPid.DataType = DbType.Int32;
				colvarPid.MaxLength = 0;
				colvarPid.AutoIncrement = false;
				colvarPid.IsNullable = true;
				colvarPid.IsPrimaryKey = false;
				colvarPid.IsForeignKey = false;
				colvarPid.IsReadOnly = false;
				colvarPid.DefaultSetting = @"";
				colvarPid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPid);
				
				TableSchema.TableColumn colvarAction = new TableSchema.TableColumn(schema);
				colvarAction.ColumnName = "action";
				colvarAction.DataType = DbType.Int32;
				colvarAction.MaxLength = 0;
				colvarAction.AutoIncrement = false;
				colvarAction.IsNullable = false;
				colvarAction.IsPrimaryKey = false;
				colvarAction.IsForeignKey = false;
				colvarAction.IsReadOnly = false;
				
						colvarAction.DefaultSetting = @"((0))";
				colvarAction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAction);
				
				TableSchema.TableColumn colvarImgUrl = new TableSchema.TableColumn(schema);
				colvarImgUrl.ColumnName = "img_url";
				colvarImgUrl.DataType = DbType.String;
				colvarImgUrl.MaxLength = -1;
				colvarImgUrl.AutoIncrement = false;
				colvarImgUrl.IsNullable = true;
				colvarImgUrl.IsPrimaryKey = false;
				colvarImgUrl.IsForeignKey = false;
				colvarImgUrl.IsReadOnly = false;
				colvarImgUrl.DefaultSetting = @"";
				colvarImgUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImgUrl);
				
				TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
				colvarCategory.ColumnName = "category";
				colvarCategory.DataType = DbType.Int32;
				colvarCategory.MaxLength = 0;
				colvarCategory.AutoIncrement = false;
				colvarCategory.IsNullable = true;
				colvarCategory.IsPrimaryKey = false;
				colvarCategory.IsForeignKey = false;
				colvarCategory.IsReadOnly = false;
				colvarCategory.DefaultSetting = @"";
				colvarCategory.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategory);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 100;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarDealDesc = new TableSchema.TableColumn(schema);
				colvarDealDesc.ColumnName = "deal_desc";
				colvarDealDesc.DataType = DbType.String;
				colvarDealDesc.MaxLength = 200;
				colvarDealDesc.AutoIncrement = false;
				colvarDealDesc.IsNullable = false;
				colvarDealDesc.IsPrimaryKey = false;
				colvarDealDesc.IsForeignKey = false;
				colvarDealDesc.IsReadOnly = false;
				colvarDealDesc.DefaultSetting = @"";
				colvarDealDesc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealDesc);
				
				TableSchema.TableColumn colvarAreaN = new TableSchema.TableColumn(schema);
				colvarAreaN.ColumnName = "area_n";
				colvarAreaN.DataType = DbType.Int32;
				colvarAreaN.MaxLength = 0;
				colvarAreaN.AutoIncrement = false;
				colvarAreaN.IsNullable = true;
				colvarAreaN.IsPrimaryKey = false;
				colvarAreaN.IsForeignKey = false;
				colvarAreaN.IsReadOnly = false;
				colvarAreaN.DefaultSetting = @"";
				colvarAreaN.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAreaN);
				
				TableSchema.TableColumn colvarAreaC = new TableSchema.TableColumn(schema);
				colvarAreaC.ColumnName = "area_c";
				colvarAreaC.DataType = DbType.Int32;
				colvarAreaC.MaxLength = 0;
				colvarAreaC.AutoIncrement = false;
				colvarAreaC.IsNullable = true;
				colvarAreaC.IsPrimaryKey = false;
				colvarAreaC.IsForeignKey = false;
				colvarAreaC.IsReadOnly = false;
				colvarAreaC.DefaultSetting = @"";
				colvarAreaC.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAreaC);
				
				TableSchema.TableColumn colvarAreaS = new TableSchema.TableColumn(schema);
				colvarAreaS.ColumnName = "area_s";
				colvarAreaS.DataType = DbType.Int32;
				colvarAreaS.MaxLength = 0;
				colvarAreaS.AutoIncrement = false;
				colvarAreaS.IsNullable = true;
				colvarAreaS.IsPrimaryKey = false;
				colvarAreaS.IsForeignKey = false;
				colvarAreaS.IsReadOnly = false;
				colvarAreaS.DefaultSetting = @"";
				colvarAreaS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAreaS);
				
				TableSchema.TableColumn colvarSortN = new TableSchema.TableColumn(schema);
				colvarSortN.ColumnName = "sort_n";
				colvarSortN.DataType = DbType.Int32;
				colvarSortN.MaxLength = 0;
				colvarSortN.AutoIncrement = false;
				colvarSortN.IsNullable = true;
				colvarSortN.IsPrimaryKey = false;
				colvarSortN.IsForeignKey = false;
				colvarSortN.IsReadOnly = false;
				colvarSortN.DefaultSetting = @"";
				colvarSortN.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSortN);
				
				TableSchema.TableColumn colvarSortC = new TableSchema.TableColumn(schema);
				colvarSortC.ColumnName = "sort_c";
				colvarSortC.DataType = DbType.Int32;
				colvarSortC.MaxLength = 0;
				colvarSortC.AutoIncrement = false;
				colvarSortC.IsNullable = true;
				colvarSortC.IsPrimaryKey = false;
				colvarSortC.IsForeignKey = false;
				colvarSortC.IsReadOnly = false;
				colvarSortC.DefaultSetting = @"";
				colvarSortC.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSortC);
				
				TableSchema.TableColumn colvarSortS = new TableSchema.TableColumn(schema);
				colvarSortS.ColumnName = "sort_s";
				colvarSortS.DataType = DbType.Int32;
				colvarSortS.MaxLength = 0;
				colvarSortS.AutoIncrement = false;
				colvarSortS.IsNullable = true;
				colvarSortS.IsPrimaryKey = false;
				colvarSortS.IsForeignKey = false;
				colvarSortS.IsReadOnly = false;
				colvarSortS.DefaultSetting = @"";
				colvarSortS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSortS);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 100;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 100;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarRequestTime = new TableSchema.TableColumn(schema);
				colvarRequestTime.ColumnName = "request_time";
				colvarRequestTime.DataType = DbType.DateTime;
				colvarRequestTime.MaxLength = 0;
				colvarRequestTime.AutoIncrement = false;
				colvarRequestTime.IsNullable = true;
				colvarRequestTime.IsPrimaryKey = false;
				colvarRequestTime.IsForeignKey = false;
				colvarRequestTime.IsReadOnly = false;
				colvarRequestTime.DefaultSetting = @"";
				colvarRequestTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRequestTime);
				
				TableSchema.TableColumn colvarAreaFlag = new TableSchema.TableColumn(schema);
				colvarAreaFlag.ColumnName = "area_flag";
				colvarAreaFlag.DataType = DbType.Int32;
				colvarAreaFlag.MaxLength = 0;
				colvarAreaFlag.AutoIncrement = false;
				colvarAreaFlag.IsNullable = true;
				colvarAreaFlag.IsPrimaryKey = false;
				colvarAreaFlag.IsForeignKey = false;
				colvarAreaFlag.IsReadOnly = false;
				colvarAreaFlag.DefaultSetting = @"";
				colvarAreaFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAreaFlag);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("yahoo_property",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid? Bid 
		{
			get { return GetColumnValue<Guid?>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("Pid")]
		[Bindable(true)]
		public int? Pid 
		{
			get { return GetColumnValue<int?>(Columns.Pid); }
			set { SetColumnValue(Columns.Pid, value); }
		}
		  
		[XmlAttribute("Action")]
		[Bindable(true)]
		public int Action 
		{
			get { return GetColumnValue<int>(Columns.Action); }
			set { SetColumnValue(Columns.Action, value); }
		}
		  
		[XmlAttribute("ImgUrl")]
		[Bindable(true)]
		public string ImgUrl 
		{
			get { return GetColumnValue<string>(Columns.ImgUrl); }
			set { SetColumnValue(Columns.ImgUrl, value); }
		}
		  
		[XmlAttribute("Category")]
		[Bindable(true)]
		public int? Category 
		{
			get { return GetColumnValue<int?>(Columns.Category); }
			set { SetColumnValue(Columns.Category, value); }
		}
		  
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		  
		[XmlAttribute("DealDesc")]
		[Bindable(true)]
		public string DealDesc 
		{
			get { return GetColumnValue<string>(Columns.DealDesc); }
			set { SetColumnValue(Columns.DealDesc, value); }
		}
		  
		[XmlAttribute("AreaN")]
		[Bindable(true)]
		public int? AreaN 
		{
			get { return GetColumnValue<int?>(Columns.AreaN); }
			set { SetColumnValue(Columns.AreaN, value); }
		}
		  
		[XmlAttribute("AreaC")]
		[Bindable(true)]
		public int? AreaC 
		{
			get { return GetColumnValue<int?>(Columns.AreaC); }
			set { SetColumnValue(Columns.AreaC, value); }
		}
		  
		[XmlAttribute("AreaS")]
		[Bindable(true)]
		public int? AreaS 
		{
			get { return GetColumnValue<int?>(Columns.AreaS); }
			set { SetColumnValue(Columns.AreaS, value); }
		}
		  
		[XmlAttribute("SortN")]
		[Bindable(true)]
		public int? SortN 
		{
			get { return GetColumnValue<int?>(Columns.SortN); }
			set { SetColumnValue(Columns.SortN, value); }
		}
		  
		[XmlAttribute("SortC")]
		[Bindable(true)]
		public int? SortC 
		{
			get { return GetColumnValue<int?>(Columns.SortC); }
			set { SetColumnValue(Columns.SortC, value); }
		}
		  
		[XmlAttribute("SortS")]
		[Bindable(true)]
		public int? SortS 
		{
			get { return GetColumnValue<int?>(Columns.SortS); }
			set { SetColumnValue(Columns.SortS, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("RequestTime")]
		[Bindable(true)]
		public DateTime? RequestTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.RequestTime); }
			set { SetColumnValue(Columns.RequestTime, value); }
		}
		  
		[XmlAttribute("AreaFlag")]
		[Bindable(true)]
		public int? AreaFlag 
		{
			get { return GetColumnValue<int?>(Columns.AreaFlag); }
			set { SetColumnValue(Columns.AreaFlag, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ImgUrlColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn DealDescColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn AreaNColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn AreaCColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn AreaSColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn SortNColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn SortCColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn SortSColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn RequestTimeColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn AreaFlagColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Type = @"type";
			 public static string Bid = @"bid";
			 public static string Pid = @"pid";
			 public static string Action = @"action";
			 public static string ImgUrl = @"img_url";
			 public static string Category = @"category";
			 public static string Title = @"title";
			 public static string DealDesc = @"deal_desc";
			 public static string AreaN = @"area_n";
			 public static string AreaC = @"area_c";
			 public static string AreaS = @"area_s";
			 public static string SortN = @"sort_n";
			 public static string SortC = @"sort_c";
			 public static string SortS = @"sort_s";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
			 public static string ModifyTime = @"modify_time";
			 public static string ModifyId = @"modify_id";
			 public static string RequestTime = @"request_time";
			 public static string AreaFlag = @"area_flag";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

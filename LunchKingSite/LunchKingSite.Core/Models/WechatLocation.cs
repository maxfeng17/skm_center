using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using Microsoft.SqlServer.Types;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the WechatLocation class.
	/// </summary>
    [Serializable]
	public partial class WechatLocationCollection : RepositoryList<WechatLocation, WechatLocationCollection>
	{	   
		public WechatLocationCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>WechatLocationCollection</returns>
		public WechatLocationCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                WechatLocation o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the wechat_location table.
	/// </summary>
	[Serializable]
	public partial class WechatLocation : RepositoryRecord<WechatLocation>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public WechatLocation()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public WechatLocation(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("wechat_location", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOpenId = new TableSchema.TableColumn(schema);
				colvarOpenId.ColumnName = "open_id";
				colvarOpenId.DataType = DbType.AnsiString;
				colvarOpenId.MaxLength = 100;
				colvarOpenId.AutoIncrement = false;
				colvarOpenId.IsNullable = false;
				colvarOpenId.IsPrimaryKey = false;
				colvarOpenId.IsForeignKey = false;
				colvarOpenId.IsReadOnly = false;
				colvarOpenId.DefaultSetting = @"";
				colvarOpenId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOpenId);
				
				TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
				colvarCoordinate.ColumnName = "coordinate";
				colvarCoordinate.DataType = DbType.AnsiString;
				colvarCoordinate.MaxLength = -1;
				colvarCoordinate.AutoIncrement = false;
				colvarCoordinate.IsNullable = false;
				colvarCoordinate.IsPrimaryKey = false;
				colvarCoordinate.IsForeignKey = false;
				colvarCoordinate.IsReadOnly = false;
				colvarCoordinate.DefaultSetting = @"";
				colvarCoordinate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCoordinate);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = false;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
				colvarCityName.ColumnName = "city_name";
				colvarCityName.DataType = DbType.String;
				colvarCityName.MaxLength = 50;
				colvarCityName.AutoIncrement = false;
				colvarCityName.IsNullable = false;
				colvarCityName.IsPrimaryKey = false;
				colvarCityName.IsForeignKey = false;
				colvarCityName.IsReadOnly = false;
				colvarCityName.DefaultSetting = @"";
				colvarCityName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityName);
				
				TableSchema.TableColumn colvarAreaCityId = new TableSchema.TableColumn(schema);
				colvarAreaCityId.ColumnName = "area_city_id";
				colvarAreaCityId.DataType = DbType.Int32;
				colvarAreaCityId.MaxLength = 0;
				colvarAreaCityId.AutoIncrement = false;
				colvarAreaCityId.IsNullable = false;
				colvarAreaCityId.IsPrimaryKey = false;
				colvarAreaCityId.IsForeignKey = false;
				colvarAreaCityId.IsReadOnly = false;
				colvarAreaCityId.DefaultSetting = @"";
				colvarAreaCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAreaCityId);
				
				TableSchema.TableColumn colvarAreaCityName = new TableSchema.TableColumn(schema);
				colvarAreaCityName.ColumnName = "area_city_name";
				colvarAreaCityName.DataType = DbType.String;
				colvarAreaCityName.MaxLength = 50;
				colvarAreaCityName.AutoIncrement = false;
				colvarAreaCityName.IsNullable = false;
				colvarAreaCityName.IsPrimaryKey = false;
				colvarAreaCityName.IsForeignKey = false;
				colvarAreaCityName.IsReadOnly = false;
				colvarAreaCityName.DefaultSetting = @"";
				colvarAreaCityName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAreaCityName);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("wechat_location",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OpenId")]
		[Bindable(true)]
		public string OpenId 
		{
			get { return GetColumnValue<string>(Columns.OpenId); }
			set { SetColumnValue(Columns.OpenId, value); }
		}

        [XmlAttribute("Coordinate")]
        [Bindable(true)]
        public string Coordinate
        {
            get { return GetColumnValue<object>(Columns.Coordinate) != null ? GetColumnValue<object>(Columns.Coordinate).ToString() : string.Empty; }
            set { SetColumnValue(Columns.Coordinate, value); }
        }
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int CityId 
		{
			get { return GetColumnValue<int>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("CityName")]
		[Bindable(true)]
		public string CityName 
		{
			get { return GetColumnValue<string>(Columns.CityName); }
			set { SetColumnValue(Columns.CityName, value); }
		}
		  
		[XmlAttribute("AreaCityId")]
		[Bindable(true)]
		public int AreaCityId 
		{
			get { return GetColumnValue<int>(Columns.AreaCityId); }
			set { SetColumnValue(Columns.AreaCityId, value); }
		}
		  
		[XmlAttribute("AreaCityName")]
		[Bindable(true)]
		public string AreaCityName 
		{
			get { return GetColumnValue<string>(Columns.AreaCityName); }
			set { SetColumnValue(Columns.AreaCityName, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OpenIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CoordinateColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CityNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AreaCityIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn AreaCityNameColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OpenId = @"open_id";
			 public static string Coordinate = @"coordinate";
			 public static string CityId = @"city_id";
			 public static string CityName = @"city_name";
			 public static string AreaCityId = @"area_city_id";
			 public static string AreaCityName = @"area_city_name";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

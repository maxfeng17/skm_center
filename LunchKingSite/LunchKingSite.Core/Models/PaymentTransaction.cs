using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    [Serializable]
    public partial class PaymentTransactionCollection : RepositoryList<PaymentTransaction, PaymentTransactionCollection>
    {
        public PaymentTransactionCollection() { }

        public PaymentTransactionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PaymentTransaction o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }

    [Serializable]
    public partial class PaymentTransaction : RepositoryRecord<PaymentTransaction>, IRecordBase
    {
        #region .ctors and Default Settings
        public PaymentTransaction()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public PaymentTransaction(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("payment_transaction", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarTransId = new TableSchema.TableColumn(schema);
                colvarTransId.ColumnName = "trans_id";
                colvarTransId.DataType = DbType.AnsiString;
                colvarTransId.MaxLength = 40;
                colvarTransId.AutoIncrement = false;
                colvarTransId.IsNullable = false;
                colvarTransId.IsPrimaryKey = false;
                colvarTransId.IsForeignKey = false;
                colvarTransId.IsReadOnly = false;
                colvarTransId.DefaultSetting = @"";
                colvarTransId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTransId);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                colvarOrderGuid.DefaultSetting = @"";
                colvarOrderGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarPaymentType = new TableSchema.TableColumn(schema);
                colvarPaymentType.ColumnName = "payment_type";
                colvarPaymentType.DataType = DbType.Int32;
                colvarPaymentType.MaxLength = 0;
                colvarPaymentType.AutoIncrement = false;
                colvarPaymentType.IsNullable = false;
                colvarPaymentType.IsPrimaryKey = false;
                colvarPaymentType.IsForeignKey = false;
                colvarPaymentType.IsReadOnly = false;
                colvarPaymentType.DefaultSetting = @"((1))";
                colvarPaymentType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPaymentType);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                colvarAmount.DefaultSetting = @"";
                colvarAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarAuthCode = new TableSchema.TableColumn(schema);
                colvarAuthCode.ColumnName = "auth_code";
                colvarAuthCode.DataType = DbType.AnsiString;
                colvarAuthCode.MaxLength = 20;
                colvarAuthCode.AutoIncrement = false;
                colvarAuthCode.IsNullable = true;
                colvarAuthCode.IsPrimaryKey = false;
                colvarAuthCode.IsForeignKey = false;
                colvarAuthCode.IsReadOnly = false;
                colvarAuthCode.DefaultSetting = @"";
                colvarAuthCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAuthCode);

                TableSchema.TableColumn colvarTransType = new TableSchema.TableColumn(schema);
                colvarTransType.ColumnName = "trans_type";
                colvarTransType.DataType = DbType.Int32;
                colvarTransType.MaxLength = 0;
                colvarTransType.AutoIncrement = false;
                colvarTransType.IsNullable = true;
                colvarTransType.IsPrimaryKey = false;
                colvarTransType.IsForeignKey = false;
                colvarTransType.IsReadOnly = false;
                colvarTransType.DefaultSetting = @"";
                colvarTransType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTransType);

                TableSchema.TableColumn colvarTransTime = new TableSchema.TableColumn(schema);
                colvarTransTime.ColumnName = "trans_time";
                colvarTransTime.DataType = DbType.DateTime;
                colvarTransTime.MaxLength = 0;
                colvarTransTime.AutoIncrement = false;
                colvarTransTime.IsNullable = true;
                colvarTransTime.IsPrimaryKey = false;
                colvarTransTime.IsForeignKey = false;
                colvarTransTime.IsReadOnly = false;
                colvarTransTime.DefaultSetting = @"";
                colvarTransTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTransTime);

                TableSchema.TableColumn colvarResult = new TableSchema.TableColumn(schema);
                colvarResult.ColumnName = "result";
                colvarResult.DataType = DbType.Int32;
                colvarResult.MaxLength = 0;
                colvarResult.AutoIncrement = false;
                colvarResult.IsNullable = true;
                colvarResult.IsPrimaryKey = false;
                colvarResult.IsForeignKey = false;
                colvarResult.IsReadOnly = false;
                colvarResult.DefaultSetting = @"";
                colvarResult.ForeignKeyTableName = "";
                schema.Columns.Add(colvarResult);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 4000;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                colvarMessage.DefaultSetting = @"";
                colvarMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
                colvarOrderClassification.ColumnName = "order_classification";
                colvarOrderClassification.DataType = DbType.Int32;
                colvarOrderClassification.MaxLength = 0;
                colvarOrderClassification.AutoIncrement = false;
                colvarOrderClassification.IsNullable = true;
                colvarOrderClassification.IsPrimaryKey = false;
                colvarOrderClassification.IsForeignKey = false;
                colvarOrderClassification.IsReadOnly = false;
                colvarOrderClassification.DefaultSetting = @"";
                colvarOrderClassification.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderClassification);

                TableSchema.TableColumn colvarReceivableId = new TableSchema.TableColumn(schema);
                colvarReceivableId.ColumnName = "receivable_id";
                colvarReceivableId.DataType = DbType.Int32;
                colvarReceivableId.MaxLength = 0;
                colvarReceivableId.AutoIncrement = false;
                colvarReceivableId.IsNullable = true;
                colvarReceivableId.IsPrimaryKey = false;
                colvarReceivableId.IsForeignKey = false;
                colvarReceivableId.IsReadOnly = false;
                colvarReceivableId.DefaultSetting = @"";
                colvarReceivableId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReceivableId);

                TableSchema.TableColumn colvarApiProvider = new TableSchema.TableColumn(schema);
                colvarApiProvider.ColumnName = "api_provider";
                colvarApiProvider.DataType = DbType.Int32;
                colvarApiProvider.MaxLength = 0;
                colvarApiProvider.AutoIncrement = false;
                colvarApiProvider.IsNullable = true;
                colvarApiProvider.IsPrimaryKey = false;
                colvarApiProvider.IsForeignKey = false;
                colvarApiProvider.IsReadOnly = false;
                colvarApiProvider.DefaultSetting = @"";
                colvarApiProvider.ForeignKeyTableName = "";
                schema.Columns.Add(colvarApiProvider);

                TableSchema.TableColumn colvarInstallment = new TableSchema.TableColumn(schema);
                colvarInstallment.ColumnName = "installment";
                colvarInstallment.DataType = DbType.Int32;
                colvarInstallment.MaxLength = 0;
                colvarInstallment.AutoIncrement = false;
                colvarInstallment.IsNullable = true;
                colvarInstallment.IsPrimaryKey = false;
                colvarInstallment.IsForeignKey = false;
                colvarInstallment.IsReadOnly = false;
                colvarInstallment.DefaultSetting = @"";
                colvarInstallment.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInstallment);

                TableSchema.TableColumn colvarBankId = new TableSchema.TableColumn(schema);
                colvarBankId.ColumnName = "bank_id";
                colvarBankId.DataType = DbType.Int32;
                colvarBankId.MaxLength = 0;
                colvarBankId.AutoIncrement = false;
                colvarBankId.IsNullable = true;
                colvarBankId.IsPrimaryKey = false;
                colvarBankId.IsForeignKey = false;
                colvarBankId.IsReadOnly = false;
                colvarBankId.DefaultSetting = @"";
                colvarBankId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBankId);

                TableSchema.TableColumn colvarCartTransId = new TableSchema.TableColumn(schema);
                colvarCartTransId.ColumnName = "cart_trans_id";
                colvarCartTransId.DataType = DbType.AnsiString;
                colvarCartTransId.MaxLength = 40;
                colvarCartTransId.AutoIncrement = false;
                colvarCartTransId.IsNullable = true;
                colvarCartTransId.IsPrimaryKey = false;
                colvarCartTransId.IsForeignKey = false;
                colvarCartTransId.IsReadOnly = false;
                colvarCartTransId.DefaultSetting = @"";
                colvarCartTransId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCartTransId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("payment_transaction", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("TransId")]
        [Bindable(true)]
        public string TransId
        {
            get { return GetColumnValue<string>(Columns.TransId); }
            set { SetColumnValue(Columns.TransId, value); }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid
        {
            get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
            set { SetColumnValue(Columns.OrderGuid, value); }
        }

        [XmlAttribute("PaymentType")]
        [Bindable(true)]
        public int PaymentType
        {
            get { return GetColumnValue<int>(Columns.PaymentType); }
            set { SetColumnValue(Columns.PaymentType, value); }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal Amount
        {
            get { return GetColumnValue<decimal>(Columns.Amount); }
            set { SetColumnValue(Columns.Amount, value); }
        }

        [XmlAttribute("AuthCode")]
        [Bindable(true)]
        public string AuthCode
        {
            get { return GetColumnValue<string>(Columns.AuthCode); }
            set { SetColumnValue(Columns.AuthCode, value); }
        }

        [XmlAttribute("TransType")]
        [Bindable(true)]
        public int? TransType
        {
            get { return GetColumnValue<int?>(Columns.TransType); }
            set { SetColumnValue(Columns.TransType, value); }
        }

        [XmlAttribute("TransTime")]
        [Bindable(true)]
        public DateTime? TransTime
        {
            get { return GetColumnValue<DateTime?>(Columns.TransTime); }
            set { SetColumnValue(Columns.TransTime, value); }
        }

        [XmlAttribute("Result")]
        [Bindable(true)]
        public int? Result
        {
            get { return GetColumnValue<int?>(Columns.Result); }
            set { SetColumnValue(Columns.Result, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get { return GetColumnValue<string>(Columns.Message); }
            set { SetColumnValue(Columns.Message, value); }
        }

        [XmlAttribute("OrderClassification")]
        [Bindable(true)]
        public int? OrderClassification
        {
            get { return GetColumnValue<int?>(Columns.OrderClassification); }
            set { SetColumnValue(Columns.OrderClassification, value); }
        }

        [XmlAttribute("ReceivableId")]
        [Bindable(true)]
        public int? ReceivableId
        {
            get { return GetColumnValue<int?>(Columns.ReceivableId); }
            set { SetColumnValue(Columns.ReceivableId, value); }
        }

        [XmlAttribute("ApiProvider")]
        [Bindable(true)]
        public int? ApiProvider
        {
            get { return GetColumnValue<int?>(Columns.ApiProvider); }
            set { SetColumnValue(Columns.ApiProvider, value); }
        }

        [XmlAttribute("Installment")]
        [Bindable(true)]
        public int? Installment
        {
            get { return GetColumnValue<int?>(Columns.Installment); }
            set { SetColumnValue(Columns.Installment, value); }
        }

        [XmlAttribute("BankId")]
        [Bindable(true)]
        public int? BankId
        {
            get { return GetColumnValue<int?>(Columns.BankId); }
            set { SetColumnValue(Columns.BankId, value); }
        }

        [XmlAttribute("CartTransId")]
        [Bindable(true)]
        public string CartTransId
        {
            get { return GetColumnValue<string>(Columns.CartTransId); }
            set { SetColumnValue(Columns.CartTransId, value); }
        }

        #endregion

        #region Typed Columns

        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }

        public static TableSchema.TableColumn TransIdColumn
        {
            get { return Schema.Columns[1]; }
        }

        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[2]; }
        }

        public static TableSchema.TableColumn PaymentTypeColumn
        {
            get { return Schema.Columns[3]; }
        }

        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[4]; }
        }

        public static TableSchema.TableColumn AuthCodeColumn
        {
            get { return Schema.Columns[5]; }
        }

        public static TableSchema.TableColumn TransTypeColumn
        {
            get { return Schema.Columns[6]; }
        }

        public static TableSchema.TableColumn TransTimeColumn
        {
            get { return Schema.Columns[7]; }
        }

        public static TableSchema.TableColumn ResultColumn
        {
            get { return Schema.Columns[8]; }
        }

        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[9]; }
        }

        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[10]; }
        }

        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[11]; }
        }

        public static TableSchema.TableColumn OrderClassificationColumn
        {
            get { return Schema.Columns[12]; }
        }

        public static TableSchema.TableColumn ReceivableIdColumn
        {
            get { return Schema.Columns[13]; }
        }

        public static TableSchema.TableColumn ApiProviderColumn
        {
            get { return Schema.Columns[14]; }
        }

        public static TableSchema.TableColumn InstallmentColumn
        {
            get { return Schema.Columns[15]; }
        }

        public static TableSchema.TableColumn BankIdColumn
        {
            get { return Schema.Columns[16]; }
        }

        public static TableSchema.TableColumn CartTransIdColumn
        {
            get { return Schema.Columns[17]; }
        }

        #endregion

        #region Columns Struct

        public struct Columns
        {
            public static string Id = @"id";
            public static string TransId = @"trans_id";
            public static string OrderGuid = @"order_guid";
            public static string PaymentType = @"payment_type";
            public static string Amount = @"amount";
            public static string AuthCode = @"auth_code";
            public static string TransType = @"trans_type";
            public static string TransTime = @"trans_time";
            public static string Result = @"result";
            public static string Status = @"status";
            public static string CreateId = @"create_id";
            public static string Message = @"message";
            public static string OrderClassification = @"order_classification";
            public static string ReceivableId = @"receivable_id";
            public static string ApiProvider = @"api_provider";
            public static string Installment = @"installment";
            public static string BankId = @"bank_id";
            public static string CartTransId = @"cart_trans_id";
        }

        #endregion

    }
}

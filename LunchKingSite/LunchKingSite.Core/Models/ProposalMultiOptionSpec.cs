﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProposalMultiOptionSpec class.
    /// </summary>
    [Serializable]
    public partial class ProposalMultiOptionSpecCollection : RepositoryList<ProposalMultiOptionSpec, ProposalMultiOptionSpecCollection>
    {
        public ProposalMultiOptionSpecCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalMultiOptionSpecCollection</returns>
        public ProposalMultiOptionSpecCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProposalMultiOptionSpec o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the proposal_multi_option_spec table.
    /// </summary>
    [Serializable]
    public partial class ProposalMultiOptionSpec : RepositoryRecord<ProposalMultiOptionSpec>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProposalMultiOptionSpec()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProposalMultiOptionSpec(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("proposal_multi_option_spec", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarMultiOptionId = new TableSchema.TableColumn(schema);
                colvarMultiOptionId.ColumnName = "multi_option_id";
                colvarMultiOptionId.DataType = DbType.Int32;
                colvarMultiOptionId.MaxLength = 0;
                colvarMultiOptionId.AutoIncrement = false;
                colvarMultiOptionId.IsNullable = false;
                colvarMultiOptionId.IsPrimaryKey = true;
                colvarMultiOptionId.IsForeignKey = false;
                colvarMultiOptionId.IsReadOnly = false;
                colvarMultiOptionId.DefaultSetting = @"";
                colvarMultiOptionId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMultiOptionId);

                TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
                colvarProductGuid.ColumnName = "product_guid";
                colvarProductGuid.DataType = DbType.Guid;
                colvarProductGuid.MaxLength = 0;
                colvarProductGuid.AutoIncrement = false;
                colvarProductGuid.IsNullable = false;
                colvarProductGuid.IsPrimaryKey = true;
                colvarProductGuid.IsForeignKey = false;
                colvarProductGuid.IsReadOnly = false;
                colvarProductGuid.DefaultSetting = @"";
                colvarProductGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProductGuid);

                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_guid";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = false;
                colvarItemGuid.IsPrimaryKey = true;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;
                colvarItemGuid.DefaultSetting = @"";
                colvarItemGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemGuid);

                TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
                colvarGroupId.ColumnName = "group_id";
                colvarGroupId.DataType = DbType.Int32;
                colvarGroupId.MaxLength = 0;
                colvarGroupId.AutoIncrement = false;
                colvarGroupId.IsNullable = false;
                colvarGroupId.IsPrimaryKey = false;
                colvarGroupId.IsForeignKey = false;
                colvarGroupId.IsReadOnly = false;

                colvarGroupId.DefaultSetting = @"((1))";
                colvarGroupId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGroupId);

                TableSchema.TableColumn colvarSort = new TableSchema.TableColumn(schema);
                colvarSort.ColumnName = "sort";
                colvarSort.DataType = DbType.Int32;
                colvarSort.MaxLength = 0;
                colvarSort.AutoIncrement = false;
                colvarSort.IsNullable = false;
                colvarSort.IsPrimaryKey = false;
                colvarSort.IsForeignKey = false;
                colvarSort.IsReadOnly = false;

                colvarSort.DefaultSetting = @"((1))";
                colvarSort.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSort);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("proposal_multi_option_spec", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("MultiOptionId")]
        [Bindable(true)]
        public int MultiOptionId
        {
            get { return GetColumnValue<int>(Columns.MultiOptionId); }
            set { SetColumnValue(Columns.MultiOptionId, value); }
        }

        [XmlAttribute("ProductGuid")]
        [Bindable(true)]
        public Guid ProductGuid
        {
            get { return GetColumnValue<Guid>(Columns.ProductGuid); }
            set { SetColumnValue(Columns.ProductGuid, value); }
        }

        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid ItemGuid
        {
            get { return GetColumnValue<Guid>(Columns.ItemGuid); }
            set { SetColumnValue(Columns.ItemGuid, value); }
        }

        [XmlAttribute("GroupId")]
        [Bindable(true)]
        public int GroupId
        {
            get { return GetColumnValue<int>(Columns.GroupId); }
            set { SetColumnValue(Columns.GroupId, value); }
        }

        [XmlAttribute("Sort")]
        [Bindable(true)]
        public int Sort
        {
            get { return GetColumnValue<int>(Columns.Sort); }
            set { SetColumnValue(Columns.Sort, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid
        {
            get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn MultiOptionIdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ProductGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ItemGuidColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn GroupIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn SortColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[5]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string MultiOptionId = @"multi_option_id";
            public static string ProductGuid = @"product_guid";
            public static string ItemGuid = @"item_guid";
            public static string GroupId = @"group_id";
            public static string Sort = @"sort";
            public static string BusinessHourGuid = @"business_hour_guid";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

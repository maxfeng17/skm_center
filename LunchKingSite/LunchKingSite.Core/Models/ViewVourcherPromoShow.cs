using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVourcherPromoShow class.
    /// </summary>
    [Serializable]
    public partial class ViewVourcherPromoShowCollection : ReadOnlyList<ViewVourcherPromoShow, ViewVourcherPromoShowCollection>
    {        
        public ViewVourcherPromoShowCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vourcher_promo_show view.
    /// </summary>
    [Serializable]
    public partial class ViewVourcherPromoShow : ReadOnlyRecord<ViewVourcherPromoShow>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vourcher_promo_show", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarPromoId = new TableSchema.TableColumn(schema);
                colvarPromoId.ColumnName = "promo_id";
                colvarPromoId.DataType = DbType.Int32;
                colvarPromoId.MaxLength = 0;
                colvarPromoId.AutoIncrement = false;
                colvarPromoId.IsNullable = false;
                colvarPromoId.IsPrimaryKey = false;
                colvarPromoId.IsForeignKey = false;
                colvarPromoId.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoId);
                
                TableSchema.TableColumn colvarPromoType = new TableSchema.TableColumn(schema);
                colvarPromoType.ColumnName = "promo_type";
                colvarPromoType.DataType = DbType.Int32;
                colvarPromoType.MaxLength = 0;
                colvarPromoType.AutoIncrement = false;
                colvarPromoType.IsNullable = false;
                colvarPromoType.IsPrimaryKey = false;
                colvarPromoType.IsForeignKey = false;
                colvarPromoType.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoType);
                
                TableSchema.TableColumn colvarRank = new TableSchema.TableColumn(schema);
                colvarRank.ColumnName = "rank";
                colvarRank.DataType = DbType.Int32;
                colvarRank.MaxLength = 0;
                colvarRank.AutoIncrement = false;
                colvarRank.IsNullable = false;
                colvarRank.IsPrimaryKey = false;
                colvarRank.IsForeignKey = false;
                colvarRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarRank);
                
                TableSchema.TableColumn colvarRatio = new TableSchema.TableColumn(schema);
                colvarRatio.ColumnName = "ratio";
                colvarRatio.DataType = DbType.Int32;
                colvarRatio.MaxLength = 0;
                colvarRatio.AutoIncrement = false;
                colvarRatio.IsNullable = false;
                colvarRatio.IsPrimaryKey = false;
                colvarRatio.IsForeignKey = false;
                colvarRatio.IsReadOnly = false;
                
                schema.Columns.Add(colvarRatio);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerLogoimgPath = new TableSchema.TableColumn(schema);
                colvarSellerLogoimgPath.ColumnName = "seller_logoimg_path";
                colvarSellerLogoimgPath.DataType = DbType.AnsiString;
                colvarSellerLogoimgPath.MaxLength = 500;
                colvarSellerLogoimgPath.AutoIncrement = false;
                colvarSellerLogoimgPath.IsNullable = true;
                colvarSellerLogoimgPath.IsPrimaryKey = false;
                colvarSellerLogoimgPath.IsForeignKey = false;
                colvarSellerLogoimgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerLogoimgPath);
                
                TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
                colvarEventId.ColumnName = "event_id";
                colvarEventId.DataType = DbType.Int32;
                colvarEventId.MaxLength = 0;
                colvarEventId.AutoIncrement = false;
                colvarEventId.IsNullable = true;
                colvarEventId.IsPrimaryKey = false;
                colvarEventId.IsForeignKey = false;
                colvarEventId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventId);
                
                TableSchema.TableColumn colvarPageCount = new TableSchema.TableColumn(schema);
                colvarPageCount.ColumnName = "page_count";
                colvarPageCount.DataType = DbType.Int32;
                colvarPageCount.MaxLength = 0;
                colvarPageCount.AutoIncrement = false;
                colvarPageCount.IsNullable = true;
                colvarPageCount.IsPrimaryKey = false;
                colvarPageCount.IsForeignKey = false;
                colvarPageCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarPageCount);
                
                TableSchema.TableColumn colvarContents = new TableSchema.TableColumn(schema);
                colvarContents.ColumnName = "contents";
                colvarContents.DataType = DbType.String;
                colvarContents.MaxLength = 1000;
                colvarContents.AutoIncrement = false;
                colvarContents.IsNullable = true;
                colvarContents.IsPrimaryKey = false;
                colvarContents.IsForeignKey = false;
                colvarContents.IsReadOnly = false;
                
                schema.Columns.Add(colvarContents);
                
                TableSchema.TableColumn colvarPicUrl = new TableSchema.TableColumn(schema);
                colvarPicUrl.ColumnName = "pic_url";
                colvarPicUrl.DataType = DbType.AnsiString;
                colvarPicUrl.MaxLength = 500;
                colvarPicUrl.AutoIncrement = false;
                colvarPicUrl.IsNullable = true;
                colvarPicUrl.IsPrimaryKey = false;
                colvarPicUrl.IsForeignKey = false;
                colvarPicUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarPicUrl);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vourcher_promo_show",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVourcherPromoShow()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVourcherPromoShow(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVourcherPromoShow(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVourcherPromoShow(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("PromoId")]
        [Bindable(true)]
        public int PromoId 
	    {
		    get
		    {
			    return GetColumnValue<int>("promo_id");
		    }
            set 
		    {
			    SetColumnValue("promo_id", value);
            }
        }
	      
        [XmlAttribute("PromoType")]
        [Bindable(true)]
        public int PromoType 
	    {
		    get
		    {
			    return GetColumnValue<int>("promo_type");
		    }
            set 
		    {
			    SetColumnValue("promo_type", value);
            }
        }
	      
        [XmlAttribute("Rank")]
        [Bindable(true)]
        public int Rank 
	    {
		    get
		    {
			    return GetColumnValue<int>("rank");
		    }
            set 
		    {
			    SetColumnValue("rank", value);
            }
        }
	      
        [XmlAttribute("Ratio")]
        [Bindable(true)]
        public int Ratio 
	    {
		    get
		    {
			    return GetColumnValue<int>("ratio");
		    }
            set 
		    {
			    SetColumnValue("ratio", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerLogoimgPath")]
        [Bindable(true)]
        public string SellerLogoimgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_logoimg_path");
		    }
            set 
		    {
			    SetColumnValue("seller_logoimg_path", value);
            }
        }
	      
        [XmlAttribute("EventId")]
        [Bindable(true)]
        public int? EventId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("event_id");
		    }
            set 
		    {
			    SetColumnValue("event_id", value);
            }
        }
	      
        [XmlAttribute("PageCount")]
        [Bindable(true)]
        public int? PageCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("page_count");
		    }
            set 
		    {
			    SetColumnValue("page_count", value);
            }
        }
	      
        [XmlAttribute("Contents")]
        [Bindable(true)]
        public string Contents 
	    {
		    get
		    {
			    return GetColumnValue<string>("contents");
		    }
            set 
		    {
			    SetColumnValue("contents", value);
            }
        }
	      
        [XmlAttribute("PicUrl")]
        [Bindable(true)]
        public string PicUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("pic_url");
		    }
            set 
		    {
			    SetColumnValue("pic_url", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string PromoId = @"promo_id";
            
            public static string PromoType = @"promo_type";
            
            public static string Rank = @"rank";
            
            public static string Ratio = @"ratio";
            
            public static string SellerId = @"seller_id";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerLogoimgPath = @"seller_logoimg_path";
            
            public static string EventId = @"event_id";
            
            public static string PageCount = @"page_count";
            
            public static string Contents = @"contents";
            
            public static string PicUrl = @"pic_url";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the SalesCalendarEvent class.
    /// </summary>
    [Serializable]
    public partial class SalesCalendarEventCollection : RepositoryList<SalesCalendarEvent, SalesCalendarEventCollection>
    {
        public SalesCalendarEventCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SalesCalendarEventCollection</returns>
        public SalesCalendarEventCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SalesCalendarEvent o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the sales_calendar_event table.
    /// </summary>
    [Serializable]
    public partial class SalesCalendarEvent : RepositoryRecord<SalesCalendarEvent>, IRecordBase
    {
        #region .ctors and Default Settings

        public SalesCalendarEvent()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SalesCalendarEvent(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("sales_calendar_event", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                colvarType.DefaultSetting = @"";
                colvarType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarGid = new TableSchema.TableColumn(schema);
                colvarGid.ColumnName = "gid";
                colvarGid.DataType = DbType.AnsiString;
                colvarGid.MaxLength = 50;
                colvarGid.AutoIncrement = false;
                colvarGid.IsNullable = false;
                colvarGid.IsPrimaryKey = false;
                colvarGid.IsForeignKey = false;
                colvarGid.IsReadOnly = false;
                colvarGid.DefaultSetting = @"";
                colvarGid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGid);

                TableSchema.TableColumn colvarEventContent = new TableSchema.TableColumn(schema);
                colvarEventContent.ColumnName = "event_content";
                colvarEventContent.DataType = DbType.String;
                colvarEventContent.MaxLength = 50;
                colvarEventContent.AutoIncrement = false;
                colvarEventContent.IsNullable = false;
                colvarEventContent.IsPrimaryKey = false;
                colvarEventContent.IsForeignKey = false;
                colvarEventContent.IsReadOnly = false;
                colvarEventContent.DefaultSetting = @"";
                colvarEventContent.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventContent);

                TableSchema.TableColumn colvarEventDateS = new TableSchema.TableColumn(schema);
                colvarEventDateS.ColumnName = "event_date_s";
                colvarEventDateS.DataType = DbType.DateTime;
                colvarEventDateS.MaxLength = 0;
                colvarEventDateS.AutoIncrement = false;
                colvarEventDateS.IsNullable = true;
                colvarEventDateS.IsPrimaryKey = false;
                colvarEventDateS.IsForeignKey = false;
                colvarEventDateS.IsReadOnly = false;
                colvarEventDateS.DefaultSetting = @"";
                colvarEventDateS.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventDateS);

                TableSchema.TableColumn colvarEventDateE = new TableSchema.TableColumn(schema);
                colvarEventDateE.ColumnName = "event_date_e";
                colvarEventDateE.DataType = DbType.DateTime;
                colvarEventDateE.MaxLength = 0;
                colvarEventDateE.AutoIncrement = false;
                colvarEventDateE.IsNullable = true;
                colvarEventDateE.IsPrimaryKey = false;
                colvarEventDateE.IsForeignKey = false;
                colvarEventDateE.IsReadOnly = false;
                colvarEventDateE.DefaultSetting = @"";
                colvarEventDateE.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventDateE);

                TableSchema.TableColumn colvarIsWholeDay = new TableSchema.TableColumn(schema);
                colvarIsWholeDay.ColumnName = "is_whole_day";
                colvarIsWholeDay.DataType = DbType.Boolean;
                colvarIsWholeDay.MaxLength = 0;
                colvarIsWholeDay.AutoIncrement = false;
                colvarIsWholeDay.IsNullable = false;
                colvarIsWholeDay.IsPrimaryKey = false;
                colvarIsWholeDay.IsForeignKey = false;
                colvarIsWholeDay.IsReadOnly = false;

                colvarIsWholeDay.DefaultSetting = @"((0))";
                colvarIsWholeDay.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsWholeDay);

                TableSchema.TableColumn colvarIsRepeat = new TableSchema.TableColumn(schema);
                colvarIsRepeat.ColumnName = "is_repeat";
                colvarIsRepeat.DataType = DbType.Boolean;
                colvarIsRepeat.MaxLength = 0;
                colvarIsRepeat.AutoIncrement = false;
                colvarIsRepeat.IsNullable = false;
                colvarIsRepeat.IsPrimaryKey = false;
                colvarIsRepeat.IsForeignKey = false;
                colvarIsRepeat.IsReadOnly = false;

                colvarIsRepeat.DefaultSetting = @"((0))";
                colvarIsRepeat.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsRepeat);

                TableSchema.TableColumn colvarLocation = new TableSchema.TableColumn(schema);
                colvarLocation.ColumnName = "location";
                colvarLocation.DataType = DbType.String;
                colvarLocation.MaxLength = 50;
                colvarLocation.AutoIncrement = false;
                colvarLocation.IsNullable = true;
                colvarLocation.IsPrimaryKey = false;
                colvarLocation.IsForeignKey = false;
                colvarLocation.IsReadOnly = false;
                colvarLocation.DefaultSetting = @"";
                colvarLocation.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLocation);

                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                colvarSellerName.DefaultSetting = @"";
                colvarSellerName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerName);

                TableSchema.TableColumn colvarContact = new TableSchema.TableColumn(schema);
                colvarContact.ColumnName = "contact";
                colvarContact.DataType = DbType.String;
                colvarContact.MaxLength = 50;
                colvarContact.AutoIncrement = false;
                colvarContact.IsNullable = true;
                colvarContact.IsPrimaryKey = false;
                colvarContact.IsForeignKey = false;
                colvarContact.IsReadOnly = false;
                colvarContact.DefaultSetting = @"";
                colvarContact.ForeignKeyTableName = "";
                schema.Columns.Add(colvarContact);

                TableSchema.TableColumn colvarEventType = new TableSchema.TableColumn(schema);
                colvarEventType.ColumnName = "event_type";
                colvarEventType.DataType = DbType.Int32;
                colvarEventType.MaxLength = 0;
                colvarEventType.AutoIncrement = false;
                colvarEventType.IsNullable = true;
                colvarEventType.IsPrimaryKey = false;
                colvarEventType.IsForeignKey = false;
                colvarEventType.IsReadOnly = false;
                colvarEventType.DefaultSetting = @"";
                colvarEventType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventType);

                TableSchema.TableColumn colvarIsWakeup = new TableSchema.TableColumn(schema);
                colvarIsWakeup.ColumnName = "is_wakeup";
                colvarIsWakeup.DataType = DbType.Boolean;
                colvarIsWakeup.MaxLength = 0;
                colvarIsWakeup.AutoIncrement = false;
                colvarIsWakeup.IsNullable = false;
                colvarIsWakeup.IsPrimaryKey = false;
                colvarIsWakeup.IsForeignKey = false;
                colvarIsWakeup.IsReadOnly = false;

                colvarIsWakeup.DefaultSetting = @"((0))";
                colvarIsWakeup.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsWakeup);

                TableSchema.TableColumn colvarWakeupMinutes = new TableSchema.TableColumn(schema);
                colvarWakeupMinutes.ColumnName = "wakeup_minutes";
                colvarWakeupMinutes.DataType = DbType.Int32;
                colvarWakeupMinutes.MaxLength = 0;
                colvarWakeupMinutes.AutoIncrement = false;
                colvarWakeupMinutes.IsNullable = true;
                colvarWakeupMinutes.IsPrimaryKey = false;
                colvarWakeupMinutes.IsForeignKey = false;
                colvarWakeupMinutes.IsReadOnly = false;
                colvarWakeupMinutes.DefaultSetting = @"";
                colvarWakeupMinutes.ForeignKeyTableName = "";
                schema.Columns.Add(colvarWakeupMinutes);

                TableSchema.TableColumn colvarWakeupCycle = new TableSchema.TableColumn(schema);
                colvarWakeupCycle.ColumnName = "wakeup_cycle";
                colvarWakeupCycle.DataType = DbType.Int32;
                colvarWakeupCycle.MaxLength = 0;
                colvarWakeupCycle.AutoIncrement = false;
                colvarWakeupCycle.IsNullable = true;
                colvarWakeupCycle.IsPrimaryKey = false;
                colvarWakeupCycle.IsForeignKey = false;
                colvarWakeupCycle.IsReadOnly = false;
                colvarWakeupCycle.DefaultSetting = @"";
                colvarWakeupCycle.ForeignKeyTableName = "";
                schema.Columns.Add(colvarWakeupCycle);

                TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
                colvarMemo.ColumnName = "memo";
                colvarMemo.DataType = DbType.String;
                colvarMemo.MaxLength = 2000;
                colvarMemo.AutoIncrement = false;
                colvarMemo.IsNullable = true;
                colvarMemo.IsPrimaryKey = false;
                colvarMemo.IsForeignKey = false;
                colvarMemo.IsReadOnly = false;
                colvarMemo.DefaultSetting = @"";
                colvarMemo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMemo);

                TableSchema.TableColumn colvarFrequency = new TableSchema.TableColumn(schema);
                colvarFrequency.ColumnName = "frequency";
                colvarFrequency.DataType = DbType.Int32;
                colvarFrequency.MaxLength = 0;
                colvarFrequency.AutoIncrement = false;
                colvarFrequency.IsNullable = true;
                colvarFrequency.IsPrimaryKey = false;
                colvarFrequency.IsForeignKey = false;
                colvarFrequency.IsReadOnly = false;
                colvarFrequency.DefaultSetting = @"";
                colvarFrequency.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFrequency);

                TableSchema.TableColumn colvarInterval = new TableSchema.TableColumn(schema);
                colvarInterval.ColumnName = "interval";
                colvarInterval.DataType = DbType.Int32;
                colvarInterval.MaxLength = 0;
                colvarInterval.AutoIncrement = false;
                colvarInterval.IsNullable = true;
                colvarInterval.IsPrimaryKey = false;
                colvarInterval.IsForeignKey = false;
                colvarInterval.IsReadOnly = false;
                colvarInterval.DefaultSetting = @"";
                colvarInterval.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInterval);

                TableSchema.TableColumn colvarWeek = new TableSchema.TableColumn(schema);
                colvarWeek.ColumnName = "week";
                colvarWeek.DataType = DbType.AnsiString;
                colvarWeek.MaxLength = 20;
                colvarWeek.AutoIncrement = false;
                colvarWeek.IsNullable = true;
                colvarWeek.IsPrimaryKey = false;
                colvarWeek.IsForeignKey = false;
                colvarWeek.IsReadOnly = false;
                colvarWeek.DefaultSetting = @"";
                colvarWeek.ForeignKeyTableName = "";
                schema.Columns.Add(colvarWeek);

                TableSchema.TableColumn colvarWakeupDateS = new TableSchema.TableColumn(schema);
                colvarWakeupDateS.ColumnName = "wakeup_date_s";
                colvarWakeupDateS.DataType = DbType.AnsiString;
                colvarWakeupDateS.MaxLength = 10;
                colvarWakeupDateS.AutoIncrement = false;
                colvarWakeupDateS.IsNullable = true;
                colvarWakeupDateS.IsPrimaryKey = false;
                colvarWakeupDateS.IsForeignKey = false;
                colvarWakeupDateS.IsReadOnly = false;
                colvarWakeupDateS.DefaultSetting = @"";
                colvarWakeupDateS.ForeignKeyTableName = "";
                schema.Columns.Add(colvarWakeupDateS);

                TableSchema.TableColumn colvarEndson = new TableSchema.TableColumn(schema);
                colvarEndson.ColumnName = "endson";
                colvarEndson.DataType = DbType.Int32;
                colvarEndson.MaxLength = 0;
                colvarEndson.AutoIncrement = false;
                colvarEndson.IsNullable = true;
                colvarEndson.IsPrimaryKey = false;
                colvarEndson.IsForeignKey = false;
                colvarEndson.IsReadOnly = false;
                colvarEndson.DefaultSetting = @"";
                colvarEndson.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEndson);

                TableSchema.TableColumn colvarEndsonData = new TableSchema.TableColumn(schema);
                colvarEndsonData.ColumnName = "endson_data";
                colvarEndsonData.DataType = DbType.AnsiString;
                colvarEndsonData.MaxLength = 10;
                colvarEndsonData.AutoIncrement = false;
                colvarEndsonData.IsNullable = true;
                colvarEndsonData.IsPrimaryKey = false;
                colvarEndsonData.IsForeignKey = false;
                colvarEndsonData.IsReadOnly = false;
                colvarEndsonData.DefaultSetting = @"";
                colvarEndsonData.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEndsonData);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateUser = new TableSchema.TableColumn(schema);
                colvarCreateUser.ColumnName = "create_user";
                colvarCreateUser.DataType = DbType.AnsiString;
                colvarCreateUser.MaxLength = 50;
                colvarCreateUser.AutoIncrement = false;
                colvarCreateUser.IsNullable = false;
                colvarCreateUser.IsPrimaryKey = false;
                colvarCreateUser.IsForeignKey = false;
                colvarCreateUser.IsReadOnly = false;
                colvarCreateUser.DefaultSetting = @"";
                colvarCreateUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateUser);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarModiftUser = new TableSchema.TableColumn(schema);
                colvarModiftUser.ColumnName = "modift_user";
                colvarModiftUser.DataType = DbType.AnsiString;
                colvarModiftUser.MaxLength = 50;
                colvarModiftUser.AutoIncrement = false;
                colvarModiftUser.IsNullable = true;
                colvarModiftUser.IsPrimaryKey = false;
                colvarModiftUser.IsForeignKey = false;
                colvarModiftUser.IsReadOnly = false;
                colvarModiftUser.DefaultSetting = @"";
                colvarModiftUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModiftUser);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("sales_calendar_event", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get { return GetColumnValue<int>(Columns.Type); }
            set { SetColumnValue(Columns.Type, value); }
        }

        [XmlAttribute("Gid")]
        [Bindable(true)]
        public string Gid
        {
            get { return GetColumnValue<string>(Columns.Gid); }
            set { SetColumnValue(Columns.Gid, value); }
        }

        [XmlAttribute("EventContent")]
        [Bindable(true)]
        public string EventContent
        {
            get { return GetColumnValue<string>(Columns.EventContent); }
            set { SetColumnValue(Columns.EventContent, value); }
        }

        [XmlAttribute("EventDateS")]
        [Bindable(true)]
        public DateTime? EventDateS
        {
            get { return GetColumnValue<DateTime?>(Columns.EventDateS); }
            set { SetColumnValue(Columns.EventDateS, value); }
        }

        [XmlAttribute("EventDateE")]
        [Bindable(true)]
        public DateTime? EventDateE
        {
            get { return GetColumnValue<DateTime?>(Columns.EventDateE); }
            set { SetColumnValue(Columns.EventDateE, value); }
        }

        [XmlAttribute("IsWholeDay")]
        [Bindable(true)]
        public bool IsWholeDay
        {
            get { return GetColumnValue<bool>(Columns.IsWholeDay); }
            set { SetColumnValue(Columns.IsWholeDay, value); }
        }

        [XmlAttribute("IsRepeat")]
        [Bindable(true)]
        public bool IsRepeat
        {
            get { return GetColumnValue<bool>(Columns.IsRepeat); }
            set { SetColumnValue(Columns.IsRepeat, value); }
        }

        [XmlAttribute("Location")]
        [Bindable(true)]
        public string Location
        {
            get { return GetColumnValue<string>(Columns.Location); }
            set { SetColumnValue(Columns.Location, value); }
        }

        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName
        {
            get { return GetColumnValue<string>(Columns.SellerName); }
            set { SetColumnValue(Columns.SellerName, value); }
        }

        [XmlAttribute("Contact")]
        [Bindable(true)]
        public string Contact
        {
            get { return GetColumnValue<string>(Columns.Contact); }
            set { SetColumnValue(Columns.Contact, value); }
        }

        [XmlAttribute("EventType")]
        [Bindable(true)]
        public int? EventType
        {
            get { return GetColumnValue<int?>(Columns.EventType); }
            set { SetColumnValue(Columns.EventType, value); }
        }

        [XmlAttribute("IsWakeup")]
        [Bindable(true)]
        public bool IsWakeup
        {
            get { return GetColumnValue<bool>(Columns.IsWakeup); }
            set { SetColumnValue(Columns.IsWakeup, value); }
        }

        [XmlAttribute("WakeupMinutes")]
        [Bindable(true)]
        public int? WakeupMinutes
        {
            get { return GetColumnValue<int?>(Columns.WakeupMinutes); }
            set { SetColumnValue(Columns.WakeupMinutes, value); }
        }

        [XmlAttribute("WakeupCycle")]
        [Bindable(true)]
        public int? WakeupCycle
        {
            get { return GetColumnValue<int?>(Columns.WakeupCycle); }
            set { SetColumnValue(Columns.WakeupCycle, value); }
        }

        [XmlAttribute("Memo")]
        [Bindable(true)]
        public string Memo
        {
            get { return GetColumnValue<string>(Columns.Memo); }
            set { SetColumnValue(Columns.Memo, value); }
        }

        [XmlAttribute("Frequency")]
        [Bindable(true)]
        public int? Frequency
        {
            get { return GetColumnValue<int?>(Columns.Frequency); }
            set { SetColumnValue(Columns.Frequency, value); }
        }

        [XmlAttribute("Interval")]
        [Bindable(true)]
        public int? Interval
        {
            get { return GetColumnValue<int?>(Columns.Interval); }
            set { SetColumnValue(Columns.Interval, value); }
        }

        [XmlAttribute("Week")]
        [Bindable(true)]
        public string Week
        {
            get { return GetColumnValue<string>(Columns.Week); }
            set { SetColumnValue(Columns.Week, value); }
        }

        [XmlAttribute("WakeupDateS")]
        [Bindable(true)]
        public string WakeupDateS
        {
            get { return GetColumnValue<string>(Columns.WakeupDateS); }
            set { SetColumnValue(Columns.WakeupDateS, value); }
        }

        [XmlAttribute("Endson")]
        [Bindable(true)]
        public int? Endson
        {
            get { return GetColumnValue<int?>(Columns.Endson); }
            set { SetColumnValue(Columns.Endson, value); }
        }

        [XmlAttribute("EndsonData")]
        [Bindable(true)]
        public string EndsonData
        {
            get { return GetColumnValue<string>(Columns.EndsonData); }
            set { SetColumnValue(Columns.EndsonData, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateUser")]
        [Bindable(true)]
        public string CreateUser
        {
            get { return GetColumnValue<string>(Columns.CreateUser); }
            set { SetColumnValue(Columns.CreateUser, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ModiftUser")]
        [Bindable(true)]
        public string ModiftUser
        {
            get { return GetColumnValue<string>(Columns.ModiftUser); }
            set { SetColumnValue(Columns.ModiftUser, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn GidColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn EventContentColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn EventDateSColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn EventDateEColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn IsWholeDayColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn IsRepeatColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn LocationColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn SellerNameColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn ContactColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn EventTypeColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn IsWakeupColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn WakeupMinutesColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn WakeupCycleColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn FrequencyColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn IntervalColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn WeekColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn WakeupDateSColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn EndsonColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn EndsonDataColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[22]; }
        }



        public static TableSchema.TableColumn CreateUserColumn
        {
            get { return Schema.Columns[23]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[24]; }
        }



        public static TableSchema.TableColumn ModiftUserColumn
        {
            get { return Schema.Columns[25]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Type = @"type";
            public static string Gid = @"gid";
            public static string EventContent = @"event_content";
            public static string EventDateS = @"event_date_s";
            public static string EventDateE = @"event_date_e";
            public static string IsWholeDay = @"is_whole_day";
            public static string IsRepeat = @"is_repeat";
            public static string Location = @"location";
            public static string SellerName = @"seller_name";
            public static string Contact = @"contact";
            public static string EventType = @"event_type";
            public static string IsWakeup = @"is_wakeup";
            public static string WakeupMinutes = @"wakeup_minutes";
            public static string WakeupCycle = @"wakeup_cycle";
            public static string Memo = @"memo";
            public static string Frequency = @"frequency";
            public static string Interval = @"interval";
            public static string Week = @"week";
            public static string WakeupDateS = @"wakeup_date_s";
            public static string Endson = @"endson";
            public static string EndsonData = @"endson_data";
            public static string CreateTime = @"create_time";
            public static string CreateUser = @"create_user";
            public static string ModifyTime = @"modify_time";
            public static string ModiftUser = @"modift_user";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

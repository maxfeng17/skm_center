using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the OrderProductDelivery class.
    /// </summary>
    [Serializable]
    public partial class OrderProductDeliveryCollection : RepositoryList<OrderProductDelivery, OrderProductDeliveryCollection>
    {
        public OrderProductDeliveryCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OrderProductDeliveryCollection</returns>
        public OrderProductDeliveryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OrderProductDelivery o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }


    /// <summary>
    /// This is an ActiveRecord class which wraps the order_product_delivery table.
    /// </summary>

    [Serializable]
    public partial class OrderProductDelivery : RepositoryRecord<OrderProductDelivery>, IRecordBase
    {
        #region .ctors and Default Settings

        public OrderProductDelivery()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public OrderProductDelivery(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("order_product_delivery", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                colvarOrderGuid.DefaultSetting = @"";
                colvarOrderGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarProductDeliveryType = new TableSchema.TableColumn(schema);
                colvarProductDeliveryType.ColumnName = "product_delivery_type";
                colvarProductDeliveryType.DataType = DbType.Int32;
                colvarProductDeliveryType.MaxLength = 0;
                colvarProductDeliveryType.AutoIncrement = false;
                colvarProductDeliveryType.IsNullable = false;
                colvarProductDeliveryType.IsPrimaryKey = false;
                colvarProductDeliveryType.IsForeignKey = false;
                colvarProductDeliveryType.IsReadOnly = false;

                colvarProductDeliveryType.DefaultSetting = @"((0))";
                colvarProductDeliveryType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProductDeliveryType);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarLastShipDate = new TableSchema.TableColumn(schema);
                colvarLastShipDate.ColumnName = "last_ship_date";
                colvarLastShipDate.DataType = DbType.DateTime;
                colvarLastShipDate.MaxLength = 0;
                colvarLastShipDate.AutoIncrement = false;
                colvarLastShipDate.IsNullable = true;
                colvarLastShipDate.IsPrimaryKey = false;
                colvarLastShipDate.IsForeignKey = false;
                colvarLastShipDate.IsReadOnly = false;
                colvarLastShipDate.DefaultSetting = @"";
                colvarLastShipDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastShipDate);

                TableSchema.TableColumn colvarDeliveryId = new TableSchema.TableColumn(schema);
                colvarDeliveryId.ColumnName = "delivery_id";
                colvarDeliveryId.DataType = DbType.Int32;
                colvarDeliveryId.MaxLength = 0;
                colvarDeliveryId.AutoIncrement = false;
                colvarDeliveryId.IsNullable = true;
                colvarDeliveryId.IsPrimaryKey = false;
                colvarDeliveryId.IsForeignKey = false;
                colvarDeliveryId.IsReadOnly = false;
                colvarDeliveryId.DefaultSetting = @"";
                colvarDeliveryId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDeliveryId);

                TableSchema.TableColumn colvarFamilyStoreId = new TableSchema.TableColumn(schema);
                colvarFamilyStoreId.ColumnName = "family_store_id";
                colvarFamilyStoreId.DataType = DbType.String;
                colvarFamilyStoreId.MaxLength = 10;
                colvarFamilyStoreId.AutoIncrement = false;
                colvarFamilyStoreId.IsNullable = true;
                colvarFamilyStoreId.IsPrimaryKey = false;
                colvarFamilyStoreId.IsForeignKey = false;
                colvarFamilyStoreId.IsReadOnly = false;
                colvarFamilyStoreId.DefaultSetting = @"";
                colvarFamilyStoreId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFamilyStoreId);

                TableSchema.TableColumn colvarFamilyStoreName = new TableSchema.TableColumn(schema);
                colvarFamilyStoreName.ColumnName = "family_store_name";
                colvarFamilyStoreName.DataType = DbType.String;
                colvarFamilyStoreName.MaxLength = 50;
                colvarFamilyStoreName.AutoIncrement = false;
                colvarFamilyStoreName.IsNullable = true;
                colvarFamilyStoreName.IsPrimaryKey = false;
                colvarFamilyStoreName.IsForeignKey = false;
                colvarFamilyStoreName.IsReadOnly = false;
                colvarFamilyStoreName.DefaultSetting = @"";
                colvarFamilyStoreName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFamilyStoreName);

                TableSchema.TableColumn colvarFamilyStoreTel = new TableSchema.TableColumn(schema);
                colvarFamilyStoreTel.ColumnName = "family_store_tel";
                colvarFamilyStoreTel.DataType = DbType.String;
                colvarFamilyStoreTel.MaxLength = 50;
                colvarFamilyStoreTel.AutoIncrement = false;
                colvarFamilyStoreTel.IsNullable = true;
                colvarFamilyStoreTel.IsPrimaryKey = false;
                colvarFamilyStoreTel.IsForeignKey = false;
                colvarFamilyStoreTel.IsReadOnly = false;
                colvarFamilyStoreTel.DefaultSetting = @"";
                colvarFamilyStoreTel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFamilyStoreTel);

                TableSchema.TableColumn colvarFamilyStoreAddr = new TableSchema.TableColumn(schema);
                colvarFamilyStoreAddr.ColumnName = "family_store_addr";
                colvarFamilyStoreAddr.DataType = DbType.String;
                colvarFamilyStoreAddr.MaxLength = 200;
                colvarFamilyStoreAddr.AutoIncrement = false;
                colvarFamilyStoreAddr.IsNullable = true;
                colvarFamilyStoreAddr.IsPrimaryKey = false;
                colvarFamilyStoreAddr.IsForeignKey = false;
                colvarFamilyStoreAddr.IsReadOnly = false;
                colvarFamilyStoreAddr.DefaultSetting = @"";
                colvarFamilyStoreAddr.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFamilyStoreAddr);

                TableSchema.TableColumn colvarSevenStoreId = new TableSchema.TableColumn(schema);
                colvarSevenStoreId.ColumnName = "seven_store_id";
                colvarSevenStoreId.DataType = DbType.String;
                colvarSevenStoreId.MaxLength = 10;
                colvarSevenStoreId.AutoIncrement = false;
                colvarSevenStoreId.IsNullable = true;
                colvarSevenStoreId.IsPrimaryKey = false;
                colvarSevenStoreId.IsForeignKey = false;
                colvarSevenStoreId.IsReadOnly = false;
                colvarSevenStoreId.DefaultSetting = @"";
                colvarSevenStoreId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSevenStoreId);

                TableSchema.TableColumn colvarSevenStoreName = new TableSchema.TableColumn(schema);
                colvarSevenStoreName.ColumnName = "seven_store_name";
                colvarSevenStoreName.DataType = DbType.String;
                colvarSevenStoreName.MaxLength = 50;
                colvarSevenStoreName.AutoIncrement = false;
                colvarSevenStoreName.IsNullable = true;
                colvarSevenStoreName.IsPrimaryKey = false;
                colvarSevenStoreName.IsForeignKey = false;
                colvarSevenStoreName.IsReadOnly = false;
                colvarSevenStoreName.DefaultSetting = @"";
                colvarSevenStoreName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSevenStoreName);

                TableSchema.TableColumn colvarSevenStoreTel = new TableSchema.TableColumn(schema);
                colvarSevenStoreTel.ColumnName = "seven_store_tel";
                colvarSevenStoreTel.DataType = DbType.String;
                colvarSevenStoreTel.MaxLength = 50;
                colvarSevenStoreTel.AutoIncrement = false;
                colvarSevenStoreTel.IsNullable = true;
                colvarSevenStoreTel.IsPrimaryKey = false;
                colvarSevenStoreTel.IsForeignKey = false;
                colvarSevenStoreTel.IsReadOnly = false;
                colvarSevenStoreTel.DefaultSetting = @"";
                colvarSevenStoreTel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSevenStoreTel);

                TableSchema.TableColumn colvarSevenStoreAddr = new TableSchema.TableColumn(schema);
                colvarSevenStoreAddr.ColumnName = "seven_store_addr";
                colvarSevenStoreAddr.DataType = DbType.String;
                colvarSevenStoreAddr.MaxLength = 200;
                colvarSevenStoreAddr.AutoIncrement = false;
                colvarSevenStoreAddr.IsNullable = true;
                colvarSevenStoreAddr.IsPrimaryKey = false;
                colvarSevenStoreAddr.IsForeignKey = false;
                colvarSevenStoreAddr.IsReadOnly = false;
                colvarSevenStoreAddr.DefaultSetting = @"";
                colvarSevenStoreAddr.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSevenStoreAddr);

                TableSchema.TableColumn colvarGoodsStatus = new TableSchema.TableColumn(schema);
                colvarGoodsStatus.ColumnName = "goods_status";
                colvarGoodsStatus.DataType = DbType.Int32;
                colvarGoodsStatus.MaxLength = 0;
                colvarGoodsStatus.AutoIncrement = false;
                colvarGoodsStatus.IsNullable = false;
                colvarGoodsStatus.IsPrimaryKey = false;
                colvarGoodsStatus.IsForeignKey = false;
                colvarGoodsStatus.IsReadOnly = false;

                colvarGoodsStatus.DefaultSetting = @"((0))";
                colvarGoodsStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGoodsStatus);

                TableSchema.TableColumn colvarGoodsStatusMessage = new TableSchema.TableColumn(schema);
                colvarGoodsStatusMessage.ColumnName = "goods_status_message";
                colvarGoodsStatusMessage.DataType = DbType.String;
                colvarGoodsStatusMessage.MaxLength = 200;
                colvarGoodsStatusMessage.AutoIncrement = false;
                colvarGoodsStatusMessage.IsNullable = true;
                colvarGoodsStatusMessage.IsPrimaryKey = false;
                colvarGoodsStatusMessage.IsForeignKey = false;
                colvarGoodsStatusMessage.IsReadOnly = false;
                colvarGoodsStatusMessage.DefaultSetting = @"";
                colvarGoodsStatusMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGoodsStatusMessage);

                TableSchema.TableColumn colvarIsApplicationServerConnected = new TableSchema.TableColumn(schema);
                colvarIsApplicationServerConnected.ColumnName = "is_application_server_connected";
                colvarIsApplicationServerConnected.DataType = DbType.Int32;
                colvarIsApplicationServerConnected.MaxLength = 0;
                colvarIsApplicationServerConnected.AutoIncrement = false;
                colvarIsApplicationServerConnected.IsNullable = false;
                colvarIsApplicationServerConnected.IsPrimaryKey = false;
                colvarIsApplicationServerConnected.IsForeignKey = false;
                colvarIsApplicationServerConnected.IsReadOnly = false;

                colvarIsApplicationServerConnected.DefaultSetting = @"((0))";
                colvarIsApplicationServerConnected.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsApplicationServerConnected);

                TableSchema.TableColumn colvarSellerGoodsStatus = new TableSchema.TableColumn(schema);
                colvarSellerGoodsStatus.ColumnName = "seller_goods_status";
                colvarSellerGoodsStatus.DataType = DbType.Int32;
                colvarSellerGoodsStatus.MaxLength = 0;
                colvarSellerGoodsStatus.AutoIncrement = false;
                colvarSellerGoodsStatus.IsNullable = false;
                colvarSellerGoodsStatus.IsPrimaryKey = false;
                colvarSellerGoodsStatus.IsForeignKey = false;
                colvarSellerGoodsStatus.IsReadOnly = false;

                colvarSellerGoodsStatus.DefaultSetting = @"((0))";
                colvarSellerGoodsStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerGoodsStatus);

                TableSchema.TableColumn colvarSellerGoodsStatusMessage = new TableSchema.TableColumn(schema);
                colvarSellerGoodsStatusMessage.ColumnName = "seller_goods_status_message";
                colvarSellerGoodsStatusMessage.DataType = DbType.String;
                colvarSellerGoodsStatusMessage.MaxLength = 200;
                colvarSellerGoodsStatusMessage.AutoIncrement = false;
                colvarSellerGoodsStatusMessage.IsNullable = true;
                colvarSellerGoodsStatusMessage.IsPrimaryKey = false;
                colvarSellerGoodsStatusMessage.IsForeignKey = false;
                colvarSellerGoodsStatusMessage.IsReadOnly = false;
                colvarSellerGoodsStatusMessage.DefaultSetting = @"";
                colvarSellerGoodsStatusMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerGoodsStatusMessage);

                TableSchema.TableColumn colvarIsCancel = new TableSchema.TableColumn(schema);
                colvarIsCancel.ColumnName = "is_cancel";
                colvarIsCancel.DataType = DbType.Boolean;
                colvarIsCancel.MaxLength = 0;
                colvarIsCancel.AutoIncrement = false;
                colvarIsCancel.IsNullable = false;
                colvarIsCancel.IsPrimaryKey = false;
                colvarIsCancel.IsForeignKey = false;
                colvarIsCancel.IsReadOnly = false;

                colvarIsCancel.DefaultSetting = @"((0))";
                colvarIsCancel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsCancel);

                TableSchema.TableColumn colvarIsCompleted = new TableSchema.TableColumn(schema);
                colvarIsCompleted.ColumnName = "is_completed";
                colvarIsCompleted.DataType = DbType.Boolean;
                colvarIsCompleted.MaxLength = 0;
                colvarIsCompleted.AutoIncrement = false;
                colvarIsCompleted.IsNullable = false;
                colvarIsCompleted.IsPrimaryKey = false;
                colvarIsCompleted.IsForeignKey = false;
                colvarIsCompleted.IsReadOnly = false;

                colvarIsCompleted.DefaultSetting = @"((0))";
                colvarIsCompleted.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsCompleted);

                TableSchema.TableColumn colvarDcReturn = new TableSchema.TableColumn(schema);
                colvarDcReturn.ColumnName = "dc_return";
                colvarDcReturn.DataType = DbType.Int32;
                colvarDcReturn.MaxLength = 0;
                colvarDcReturn.AutoIncrement = false;
                colvarDcReturn.IsNullable = false;
                colvarDcReturn.IsPrimaryKey = false;
                colvarDcReturn.IsForeignKey = false;
                colvarDcReturn.IsReadOnly = false;

                colvarDcReturn.DefaultSetting = @"((0))";
                colvarDcReturn.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDcReturn);

                TableSchema.TableColumn colvarDcReturnTime = new TableSchema.TableColumn(schema);
                colvarDcReturnTime.ColumnName = "dc_return_time";
                colvarDcReturnTime.DataType = DbType.DateTime;
                colvarDcReturnTime.MaxLength = 0;
                colvarDcReturnTime.AutoIncrement = false;
                colvarDcReturnTime.IsNullable = true;
                colvarDcReturnTime.IsPrimaryKey = false;
                colvarDcReturnTime.IsForeignKey = false;
                colvarDcReturnTime.IsReadOnly = false;
                colvarDcReturnTime.DefaultSetting = @"";
                colvarDcReturnTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDcReturnTime);

                TableSchema.TableColumn colvarDcReceiveFail = new TableSchema.TableColumn(schema);
                colvarDcReceiveFail.ColumnName = "dc_receive_fail";
                colvarDcReceiveFail.DataType = DbType.Int32;
                colvarDcReceiveFail.MaxLength = 0;
                colvarDcReceiveFail.AutoIncrement = false;
                colvarDcReceiveFail.IsNullable = false;
                colvarDcReceiveFail.IsPrimaryKey = false;
                colvarDcReceiveFail.IsForeignKey = false;
                colvarDcReceiveFail.IsReadOnly = false;

                colvarDcReceiveFail.DefaultSetting = @"((0))";
                colvarDcReceiveFail.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDcReceiveFail);

                TableSchema.TableColumn colvarDcReceiveFailTime = new TableSchema.TableColumn(schema);
                colvarDcReceiveFailTime.ColumnName = "dc_receive_fail_time";
                colvarDcReceiveFailTime.DataType = DbType.DateTime;
                colvarDcReceiveFailTime.MaxLength = 0;
                colvarDcReceiveFailTime.AutoIncrement = false;
                colvarDcReceiveFailTime.IsNullable = true;
                colvarDcReceiveFailTime.IsPrimaryKey = false;
                colvarDcReceiveFailTime.IsForeignKey = false;
                colvarDcReceiveFailTime.IsReadOnly = false;
                colvarDcReceiveFailTime.DefaultSetting = @"";
                colvarDcReceiveFailTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDcReceiveFailTime);

                TableSchema.TableColumn colvarReturnConfirmId = new TableSchema.TableColumn(schema);
                colvarReturnConfirmId.ColumnName = "return_confirm_id";
                colvarReturnConfirmId.DataType = DbType.AnsiString;
                colvarReturnConfirmId.MaxLength = 50;
                colvarReturnConfirmId.AutoIncrement = false;
                colvarReturnConfirmId.IsNullable = true;
                colvarReturnConfirmId.IsPrimaryKey = false;
                colvarReturnConfirmId.IsForeignKey = false;
                colvarReturnConfirmId.IsReadOnly = false;
                colvarReturnConfirmId.DefaultSetting = @"";
                colvarReturnConfirmId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReturnConfirmId);

                TableSchema.TableColumn colvarReturnConfirmTime = new TableSchema.TableColumn(schema);
                colvarReturnConfirmTime.ColumnName = "return_confirm_time";
                colvarReturnConfirmTime.DataType = DbType.DateTime;
                colvarReturnConfirmTime.MaxLength = 0;
                colvarReturnConfirmTime.AutoIncrement = false;
                colvarReturnConfirmTime.IsNullable = true;
                colvarReturnConfirmTime.IsPrimaryKey = false;
                colvarReturnConfirmTime.IsForeignKey = false;
                colvarReturnConfirmTime.IsReadOnly = false;
                colvarReturnConfirmTime.DefaultSetting = @"";
                colvarReturnConfirmTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReturnConfirmTime);

                TableSchema.TableColumn colvarIsShipmentPdf = new TableSchema.TableColumn(schema);
                colvarIsShipmentPdf.ColumnName = "is_shipment_pdf";
                colvarIsShipmentPdf.DataType = DbType.Boolean;
                colvarIsShipmentPdf.MaxLength = 0;
                colvarIsShipmentPdf.AutoIncrement = false;
                colvarIsShipmentPdf.IsNullable = false;
                colvarIsShipmentPdf.IsPrimaryKey = false;
                colvarIsShipmentPdf.IsForeignKey = false;
                colvarIsShipmentPdf.IsReadOnly = false;

                colvarIsShipmentPdf.DefaultSetting = @"((0))";
                colvarIsShipmentPdf.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsShipmentPdf);

                TableSchema.TableColumn colvarDcReceiveFailReason = new TableSchema.TableColumn(schema);
                colvarDcReceiveFailReason.ColumnName = "dc_receive_fail_reason";
                colvarDcReceiveFailReason.DataType = DbType.String;
                colvarDcReceiveFailReason.MaxLength = 50;
                colvarDcReceiveFailReason.AutoIncrement = false;
                colvarDcReceiveFailReason.IsNullable = false;
                colvarDcReceiveFailReason.IsPrimaryKey = false;
                colvarDcReceiveFailReason.IsForeignKey = false;
                colvarDcReceiveFailReason.IsReadOnly = false;

                colvarDcReceiveFailReason.DefaultSetting = @"('')";
                colvarDcReceiveFailReason.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDcReceiveFailReason);

                TableSchema.TableColumn colvarDcReturnReason = new TableSchema.TableColumn(schema);
                colvarDcReturnReason.ColumnName = "dc_return_reason";
                colvarDcReturnReason.DataType = DbType.String;
                colvarDcReturnReason.MaxLength = 50;
                colvarDcReturnReason.AutoIncrement = false;
                colvarDcReturnReason.IsNullable = false;
                colvarDcReturnReason.IsPrimaryKey = false;
                colvarDcReturnReason.IsForeignKey = false;
                colvarDcReturnReason.IsReadOnly = false;

                colvarDcReturnReason.DefaultSetting = @"('')";
                colvarDcReturnReason.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDcReturnReason);

                TableSchema.TableColumn colvarCancelReason = new TableSchema.TableColumn(schema);
                colvarCancelReason.ColumnName = "cancel_reason";
                colvarCancelReason.DataType = DbType.String;
                colvarCancelReason.MaxLength = 50;
                colvarCancelReason.AutoIncrement = false;
                colvarCancelReason.IsNullable = false;
                colvarCancelReason.IsPrimaryKey = false;
                colvarCancelReason.IsForeignKey = false;
                colvarCancelReason.IsReadOnly = false;

                colvarCancelReason.DefaultSetting = @"('')";
                colvarCancelReason.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCancelReason);

                TableSchema.TableColumn colvarPreShipNo = new TableSchema.TableColumn(schema);
                colvarPreShipNo.ColumnName = "pre_ship_no";
                colvarPreShipNo.DataType = DbType.AnsiString;
                colvarPreShipNo.MaxLength = 50;
                colvarPreShipNo.AutoIncrement = false;
                colvarPreShipNo.IsNullable = true;
                colvarPreShipNo.IsPrimaryKey = false;
                colvarPreShipNo.IsForeignKey = false;
                colvarPreShipNo.IsReadOnly = false;
                colvarPreShipNo.DefaultSetting = @"";
                colvarPreShipNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPreShipNo);

                TableSchema.TableColumn colvarGoodStatusHistory = new TableSchema.TableColumn(schema);
                colvarGoodStatusHistory.ColumnName = "good_status_history";
                colvarGoodStatusHistory.DataType = DbType.String;
                colvarGoodStatusHistory.MaxLength = -1;
                colvarGoodStatusHistory.AutoIncrement = false;
                colvarGoodStatusHistory.IsNullable = true;
                colvarGoodStatusHistory.IsPrimaryKey = false;
                colvarGoodStatusHistory.IsForeignKey = false;
                colvarGoodStatusHistory.IsReadOnly = false;
                colvarGoodStatusHistory.DefaultSetting = @"";
                colvarGoodStatusHistory.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGoodStatusHistory);

                TableSchema.TableColumn colvarSellerOperatingMode = new TableSchema.TableColumn(schema);
                colvarSellerOperatingMode.ColumnName = "seller_operating_mode";
                colvarSellerOperatingMode.DataType = DbType.Int32;
                colvarSellerOperatingMode.MaxLength = 0;
                colvarSellerOperatingMode.AutoIncrement = false;
                colvarSellerOperatingMode.IsNullable = false;
                colvarSellerOperatingMode.IsPrimaryKey = false;
                colvarSellerOperatingMode.IsForeignKey = false;
                colvarSellerOperatingMode.IsReadOnly = false;
                colvarSellerOperatingMode.DefaultSetting = @"";
                colvarSellerOperatingMode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerOperatingMode);

                TableSchema.TableColumn colvarIspOrderType = new TableSchema.TableColumn(schema);
                colvarIspOrderType.ColumnName = "isp_order_type";
                colvarIspOrderType.DataType = DbType.Int32;
                colvarIspOrderType.MaxLength = 0;
                colvarIspOrderType.AutoIncrement = false;
                colvarIspOrderType.IsNullable = false;
                colvarIspOrderType.IsPrimaryKey = false;
                colvarIspOrderType.IsForeignKey = false;
                colvarIspOrderType.IsReadOnly = false;
                colvarIspOrderType.DefaultSetting = @"";
                colvarIspOrderType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIspOrderType);

                TableSchema.TableColumn colvarDcAcceptStatus = new TableSchema.TableColumn(schema);
                colvarDcAcceptStatus.ColumnName = "dc_accept_status";
                colvarDcAcceptStatus.DataType = DbType.Int32;
                colvarDcAcceptStatus.MaxLength = 0;
                colvarDcAcceptStatus.AutoIncrement = false;
                colvarDcAcceptStatus.IsNullable = false;
                colvarDcAcceptStatus.IsPrimaryKey = false;
                colvarDcAcceptStatus.IsForeignKey = false;
                colvarDcAcceptStatus.IsReadOnly = false;

                colvarDcAcceptStatus.DefaultSetting = @"((0))";
                colvarDcAcceptStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDcAcceptStatus);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("order_product_delivery", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid
        {
            get { return GetColumnValue<Guid>(Columns.OrderGuid); }
            set { SetColumnValue(Columns.OrderGuid, value); }
        }

        [XmlAttribute("ProductDeliveryType")]
        [Bindable(true)]
        public int ProductDeliveryType
        {
            get { return GetColumnValue<int>(Columns.ProductDeliveryType); }
            set { SetColumnValue(Columns.ProductDeliveryType, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("LastShipDate")]
        [Bindable(true)]
        public DateTime? LastShipDate
        {
            get { return GetColumnValue<DateTime?>(Columns.LastShipDate); }
            set { SetColumnValue(Columns.LastShipDate, value); }
        }

        [XmlAttribute("DeliveryId")]
        [Bindable(true)]
        public int? DeliveryId
        {
            get { return GetColumnValue<int?>(Columns.DeliveryId); }
            set { SetColumnValue(Columns.DeliveryId, value); }
        }

        [XmlAttribute("FamilyStoreId")]
        [Bindable(true)]
        public string FamilyStoreId
        {
            get { return GetColumnValue<string>(Columns.FamilyStoreId); }
            set { SetColumnValue(Columns.FamilyStoreId, value); }
        }

        [XmlAttribute("FamilyStoreName")]
        [Bindable(true)]
        public string FamilyStoreName
        {
            get { return GetColumnValue<string>(Columns.FamilyStoreName); }
            set { SetColumnValue(Columns.FamilyStoreName, value); }
        }

        [XmlAttribute("FamilyStoreTel")]
        [Bindable(true)]
        public string FamilyStoreTel
        {
            get { return GetColumnValue<string>(Columns.FamilyStoreTel); }
            set { SetColumnValue(Columns.FamilyStoreTel, value); }
        }

        [XmlAttribute("FamilyStoreAddr")]
        [Bindable(true)]
        public string FamilyStoreAddr
        {
            get { return GetColumnValue<string>(Columns.FamilyStoreAddr); }
            set { SetColumnValue(Columns.FamilyStoreAddr, value); }
        }

        [XmlAttribute("SevenStoreId")]
        [Bindable(true)]
        public string SevenStoreId
        {
            get { return GetColumnValue<string>(Columns.SevenStoreId); }
            set { SetColumnValue(Columns.SevenStoreId, value); }
        }

        [XmlAttribute("SevenStoreName")]
        [Bindable(true)]
        public string SevenStoreName
        {
            get { return GetColumnValue<string>(Columns.SevenStoreName); }
            set { SetColumnValue(Columns.SevenStoreName, value); }
        }

        [XmlAttribute("SevenStoreTel")]
        [Bindable(true)]
        public string SevenStoreTel
        {
            get { return GetColumnValue<string>(Columns.SevenStoreTel); }
            set { SetColumnValue(Columns.SevenStoreTel, value); }
        }

        [XmlAttribute("SevenStoreAddr")]
        [Bindable(true)]
        public string SevenStoreAddr
        {
            get { return GetColumnValue<string>(Columns.SevenStoreAddr); }
            set { SetColumnValue(Columns.SevenStoreAddr, value); }
        }

        [XmlAttribute("GoodsStatus")]
        [Bindable(true)]
        public int GoodsStatus
        {
            get { return GetColumnValue<int>(Columns.GoodsStatus); }
            set { SetColumnValue(Columns.GoodsStatus, value); }
        }

        [XmlAttribute("GoodsStatusMessage")]
        [Bindable(true)]
        public string GoodsStatusMessage
        {
            get { return GetColumnValue<string>(Columns.GoodsStatusMessage); }
            set { SetColumnValue(Columns.GoodsStatusMessage, value); }
        }

        [XmlAttribute("IsApplicationServerConnected")]
        [Bindable(true)]
        public int IsApplicationServerConnected
        {
            get { return GetColumnValue<int>(Columns.IsApplicationServerConnected); }
            set { SetColumnValue(Columns.IsApplicationServerConnected, value); }
        }

        [XmlAttribute("SellerGoodsStatus")]
        [Bindable(true)]
        public int SellerGoodsStatus
        {
            get { return GetColumnValue<int>(Columns.SellerGoodsStatus); }
            set { SetColumnValue(Columns.SellerGoodsStatus, value); }
        }

        [XmlAttribute("SellerGoodsStatusMessage")]
        [Bindable(true)]
        public string SellerGoodsStatusMessage
        {
            get { return GetColumnValue<string>(Columns.SellerGoodsStatusMessage); }
            set { SetColumnValue(Columns.SellerGoodsStatusMessage, value); }
        }

        [XmlAttribute("IsCancel")]
        [Bindable(true)]
        public bool IsCancel
        {
            get { return GetColumnValue<bool>(Columns.IsCancel); }
            set { SetColumnValue(Columns.IsCancel, value); }
        }

        [XmlAttribute("IsCompleted")]
        [Bindable(true)]
        public bool IsCompleted
        {
            get { return GetColumnValue<bool>(Columns.IsCompleted); }
            set { SetColumnValue(Columns.IsCompleted, value); }
        }

        [XmlAttribute("DcReturn")]
        [Bindable(true)]
        public int DcReturn
        {
            get { return GetColumnValue<int>(Columns.DcReturn); }
            set { SetColumnValue(Columns.DcReturn, value); }
        }

        [XmlAttribute("DcReturnTime")]
        [Bindable(true)]
        public DateTime? DcReturnTime
        {
            get { return GetColumnValue<DateTime?>(Columns.DcReturnTime); }
            set { SetColumnValue(Columns.DcReturnTime, value); }
        }

        [XmlAttribute("DcReceiveFail")]
        [Bindable(true)]
        public int DcReceiveFail
        {
            get { return GetColumnValue<int>(Columns.DcReceiveFail); }
            set { SetColumnValue(Columns.DcReceiveFail, value); }
        }

        [XmlAttribute("DcReceiveFailTime")]
        [Bindable(true)]
        public DateTime? DcReceiveFailTime
        {
            get { return GetColumnValue<DateTime?>(Columns.DcReceiveFailTime); }
            set { SetColumnValue(Columns.DcReceiveFailTime, value); }
        }

        [XmlAttribute("ReturnConfirmId")]
        [Bindable(true)]
        public string ReturnConfirmId
        {
            get { return GetColumnValue<string>(Columns.ReturnConfirmId); }
            set { SetColumnValue(Columns.ReturnConfirmId, value); }
        }

        [XmlAttribute("ReturnConfirmTime")]
        [Bindable(true)]
        public DateTime? ReturnConfirmTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ReturnConfirmTime); }
            set { SetColumnValue(Columns.ReturnConfirmTime, value); }
        }

        [XmlAttribute("IsShipmentPdf")]
        [Bindable(true)]
        public bool IsShipmentPdf
        {
            get { return GetColumnValue<bool>(Columns.IsShipmentPdf); }
            set { SetColumnValue(Columns.IsShipmentPdf, value); }
        }

        [XmlAttribute("DcReceiveFailReason")]
        [Bindable(true)]
        public string DcReceiveFailReason
        {
            get { return GetColumnValue<string>(Columns.DcReceiveFailReason); }
            set { SetColumnValue(Columns.DcReceiveFailReason, value); }
        }

        [XmlAttribute("DcReturnReason")]
        [Bindable(true)]
        public string DcReturnReason
        {
            get { return GetColumnValue<string>(Columns.DcReturnReason); }
            set { SetColumnValue(Columns.DcReturnReason, value); }
        }

        [XmlAttribute("CancelReason")]
        [Bindable(true)]
        public string CancelReason
        {
            get { return GetColumnValue<string>(Columns.CancelReason); }
            set { SetColumnValue(Columns.CancelReason, value); }
        }

        [XmlAttribute("PreShipNo")]
        [Bindable(true)]
        public string PreShipNo
        {
            get { return GetColumnValue<string>(Columns.PreShipNo); }
            set { SetColumnValue(Columns.PreShipNo, value); }
        }

        [XmlAttribute("GoodStatusHistory")]
        [Bindable(true)]
        public string GoodStatusHistory
        {
            get { return GetColumnValue<string>(Columns.GoodStatusHistory); }
            set { SetColumnValue(Columns.GoodStatusHistory, value); }
        }

        [XmlAttribute("SellerOperatingMode")]
        [Bindable(true)]
        public int SellerOperatingMode
        {
            get { return GetColumnValue<int>(Columns.SellerOperatingMode); }
            set { SetColumnValue(Columns.SellerOperatingMode, value); }
        }

        [XmlAttribute("IspOrderType")]
        [Bindable(true)]
        public int IspOrderType
        {
            get { return GetColumnValue<int>(Columns.IspOrderType); }
            set { SetColumnValue(Columns.IspOrderType, value); }
        }

        [XmlAttribute("DcAcceptStatus")]
        [Bindable(true)]
        public int DcAcceptStatus
        {
            get { return GetColumnValue<int>(Columns.DcAcceptStatus); }
            set { SetColumnValue(Columns.DcAcceptStatus, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ProductDeliveryTypeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn LastShipDateColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn DeliveryIdColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn FamilyStoreIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn FamilyStoreNameColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn FamilyStoreTelColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn FamilyStoreAddrColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn SevenStoreIdColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn SevenStoreNameColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn SevenStoreTelColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn SevenStoreAddrColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn GoodsStatusColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn GoodsStatusMessageColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn IsApplicationServerConnectedColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn SellerGoodsStatusColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn SellerGoodsStatusMessageColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn IsCancelColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn IsCompletedColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn DcReturnColumn
        {
            get { return Schema.Columns[22]; }
        }



        public static TableSchema.TableColumn DcReturnTimeColumn
        {
            get { return Schema.Columns[23]; }
        }



        public static TableSchema.TableColumn DcReceiveFailColumn
        {
            get { return Schema.Columns[24]; }
        }



        public static TableSchema.TableColumn DcReceiveFailTimeColumn
        {
            get { return Schema.Columns[25]; }
        }



        public static TableSchema.TableColumn ReturnConfirmIdColumn
        {
            get { return Schema.Columns[26]; }
        }



        public static TableSchema.TableColumn ReturnConfirmTimeColumn
        {
            get { return Schema.Columns[27]; }
        }



        public static TableSchema.TableColumn IsShipmentPdfColumn
        {
            get { return Schema.Columns[28]; }
        }



        public static TableSchema.TableColumn DcReceiveFailReasonColumn
        {
            get { return Schema.Columns[29]; }
        }



        public static TableSchema.TableColumn DcReturnReasonColumn
        {
            get { return Schema.Columns[30]; }
        }



        public static TableSchema.TableColumn CancelReasonColumn
        {
            get { return Schema.Columns[31]; }
        }



        public static TableSchema.TableColumn PreShipNoColumn
        {
            get { return Schema.Columns[32]; }
        }



        public static TableSchema.TableColumn GoodStatusHistoryColumn
        {
            get { return Schema.Columns[33]; }
        }



        public static TableSchema.TableColumn SellerOperatingModeColumn
        {
            get { return Schema.Columns[34]; }
        }



        public static TableSchema.TableColumn IspOrderTypeColumn
        {
            get { return Schema.Columns[35]; }
        }



        public static TableSchema.TableColumn DcAcceptStatusColumn
        {
            get { return Schema.Columns[36]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string OrderGuid = @"order_guid";
            public static string ProductDeliveryType = @"product_delivery_type";
            public static string CreateTime = @"create_time";
            public static string ModifyTime = @"modify_time";
            public static string LastShipDate = @"last_ship_date";
            public static string DeliveryId = @"delivery_id";
            public static string FamilyStoreId = @"family_store_id";
            public static string FamilyStoreName = @"family_store_name";
            public static string FamilyStoreTel = @"family_store_tel";
            public static string FamilyStoreAddr = @"family_store_addr";
            public static string SevenStoreId = @"seven_store_id";
            public static string SevenStoreName = @"seven_store_name";
            public static string SevenStoreTel = @"seven_store_tel";
            public static string SevenStoreAddr = @"seven_store_addr";
            public static string GoodsStatus = @"goods_status";
            public static string GoodsStatusMessage = @"goods_status_message";
            public static string IsApplicationServerConnected = @"is_application_server_connected";
            public static string SellerGoodsStatus = @"seller_goods_status";
            public static string SellerGoodsStatusMessage = @"seller_goods_status_message";
            public static string IsCancel = @"is_cancel";
            public static string IsCompleted = @"is_completed";
            public static string DcReturn = @"dc_return";
            public static string DcReturnTime = @"dc_return_time";
            public static string DcReceiveFail = @"dc_receive_fail";
            public static string DcReceiveFailTime = @"dc_receive_fail_time";
            public static string ReturnConfirmId = @"return_confirm_id";
            public static string ReturnConfirmTime = @"return_confirm_time";
            public static string IsShipmentPdf = @"is_shipment_pdf";
            public static string DcReceiveFailReason = @"dc_receive_fail_reason";
            public static string DcReturnReason = @"dc_return_reason";
            public static string CancelReason = @"cancel_reason";
            public static string PreShipNo = @"pre_ship_no";
            public static string GoodStatusHistory = @"good_status_history";
            public static string SellerOperatingMode = @"seller_operating_mode";
            public static string IspOrderType = @"isp_order_type";
            public static string DcAcceptStatus = @"dc_accept_status";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

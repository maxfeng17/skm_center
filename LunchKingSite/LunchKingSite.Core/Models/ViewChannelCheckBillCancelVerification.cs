using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewChannelCheckBillCancelVerification class.
    /// </summary>
    [Serializable]
    public partial class ViewChannelCheckBillCancelVerificationCollection : ReadOnlyList<ViewChannelCheckBillCancelVerification, ViewChannelCheckBillCancelVerificationCollection>
    {        
        public ViewChannelCheckBillCancelVerificationCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_channel_check_bill_cancel_verification view.
    /// </summary>
    [Serializable]
    public partial class ViewChannelCheckBillCancelVerification : ReadOnlyRecord<ViewChannelCheckBillCancelVerification>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_channel_check_bill_cancel_verification", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.String;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = true;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponSequenceNumber);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Currency;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = true;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarCost);
                
                TableSchema.TableColumn colvarUsageVerifiedTime = new TableSchema.TableColumn(schema);
                colvarUsageVerifiedTime.ColumnName = "usage_verified_time";
                colvarUsageVerifiedTime.DataType = DbType.DateTime;
                colvarUsageVerifiedTime.MaxLength = 0;
                colvarUsageVerifiedTime.AutoIncrement = false;
                colvarUsageVerifiedTime.IsNullable = true;
                colvarUsageVerifiedTime.IsPrimaryKey = false;
                colvarUsageVerifiedTime.IsForeignKey = false;
                colvarUsageVerifiedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUsageVerifiedTime);
                
                TableSchema.TableColumn colvarRelatedOrderId = new TableSchema.TableColumn(schema);
                colvarRelatedOrderId.ColumnName = "related_order_id";
                colvarRelatedOrderId.DataType = DbType.AnsiString;
                colvarRelatedOrderId.MaxLength = 50;
                colvarRelatedOrderId.AutoIncrement = false;
                colvarRelatedOrderId.IsNullable = false;
                colvarRelatedOrderId.IsPrimaryKey = false;
                colvarRelatedOrderId.IsForeignKey = false;
                colvarRelatedOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarRelatedOrderId);
                
                TableSchema.TableColumn colvarUndoTime = new TableSchema.TableColumn(schema);
                colvarUndoTime.ColumnName = "undo_time";
                colvarUndoTime.DataType = DbType.DateTime;
                colvarUndoTime.MaxLength = 0;
                colvarUndoTime.AutoIncrement = false;
                colvarUndoTime.IsNullable = true;
                colvarUndoTime.IsPrimaryKey = false;
                colvarUndoTime.IsForeignKey = false;
                colvarUndoTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUndoTime);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "BID";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = true;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBid);
                
                TableSchema.TableColumn colvarGrossMargin = new TableSchema.TableColumn(schema);
                colvarGrossMargin.ColumnName = "gross_margin";
                colvarGrossMargin.DataType = DbType.Decimal;
                colvarGrossMargin.MaxLength = 0;
                colvarGrossMargin.AutoIncrement = false;
                colvarGrossMargin.IsNullable = true;
                colvarGrossMargin.IsPrimaryKey = false;
                colvarGrossMargin.IsForeignKey = false;
                colvarGrossMargin.IsReadOnly = false;
                
                schema.Columns.Add(colvarGrossMargin);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_channel_check_bill_cancel_verification",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewChannelCheckBillCancelVerification()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewChannelCheckBillCancelVerification(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewChannelCheckBillCancelVerification(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewChannelCheckBillCancelVerification(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_sequence_number");
		    }
            set 
		    {
			    SetColumnValue("coupon_sequence_number", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("Cost")]
        [Bindable(true)]
        public decimal? Cost 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("cost");
		    }
            set 
		    {
			    SetColumnValue("cost", value);
            }
        }
	      
        [XmlAttribute("UsageVerifiedTime")]
        [Bindable(true)]
        public DateTime? UsageVerifiedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("usage_verified_time");
		    }
            set 
		    {
			    SetColumnValue("usage_verified_time", value);
            }
        }
	      
        [XmlAttribute("RelatedOrderId")]
        [Bindable(true)]
        public string RelatedOrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("related_order_id");
		    }
            set 
		    {
			    SetColumnValue("related_order_id", value);
            }
        }
	      
        [XmlAttribute("UndoTime")]
        [Bindable(true)]
        public DateTime? UndoTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("undo_time");
		    }
            set 
		    {
			    SetColumnValue("undo_time", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid? Bid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("BID");
		    }
            set 
		    {
			    SetColumnValue("BID", value);
            }
        }
	      
        [XmlAttribute("GrossMargin")]
        [Bindable(true)]
        public decimal? GrossMargin 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("gross_margin");
		    }
            set 
		    {
			    SetColumnValue("gross_margin", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CouponSequenceNumber = @"coupon_sequence_number";
            
            public static string ItemPrice = @"item_price";
            
            public static string Cost = @"cost";
            
            public static string UsageVerifiedTime = @"usage_verified_time";
            
            public static string RelatedOrderId = @"related_order_id";
            
            public static string UndoTime = @"undo_time";
            
            public static string CouponUsage = @"coupon_usage";
            
            public static string StoreName = @"store_name";
            
            public static string OrderGuid = @"order_guid";
            
            public static string OrderId = @"order_id";
            
            public static string Type = @"type";
            
            public static string Bid = @"BID";
            
            public static string GrossMargin = @"gross_margin";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

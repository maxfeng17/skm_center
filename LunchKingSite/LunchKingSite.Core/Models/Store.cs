using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Store class.
	/// </summary>
    [Serializable]
	public partial class StoreCollection : RepositoryList<Store, StoreCollection>
	{	   
		public StoreCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>StoreCollection</returns>
		public StoreCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Store o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the store table.
	/// </summary>
	[Serializable]
	public partial class Store : RepositoryRecord<Store>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Store()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Store(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("store", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "Guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
				colvarStoreName.ColumnName = "store_name";
				colvarStoreName.DataType = DbType.String;
				colvarStoreName.MaxLength = 256;
				colvarStoreName.AutoIncrement = false;
				colvarStoreName.IsNullable = true;
				colvarStoreName.IsPrimaryKey = false;
				colvarStoreName.IsForeignKey = false;
				colvarStoreName.IsReadOnly = false;
				colvarStoreName.DefaultSetting = @"";
				colvarStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreName);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = true;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				
					colvarSellerGuid.ForeignKeyTableName = "seller";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
				colvarPhone.ColumnName = "phone";
				colvarPhone.DataType = DbType.AnsiString;
				colvarPhone.MaxLength = 100;
				colvarPhone.AutoIncrement = false;
				colvarPhone.IsNullable = true;
				colvarPhone.IsPrimaryKey = false;
				colvarPhone.IsForeignKey = false;
				colvarPhone.IsReadOnly = false;
				colvarPhone.DefaultSetting = @"";
				colvarPhone.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhone);
				
				TableSchema.TableColumn colvarTownshipId = new TableSchema.TableColumn(schema);
				colvarTownshipId.ColumnName = "township_id";
				colvarTownshipId.DataType = DbType.Int32;
				colvarTownshipId.MaxLength = 0;
				colvarTownshipId.AutoIncrement = false;
				colvarTownshipId.IsNullable = true;
				colvarTownshipId.IsPrimaryKey = false;
				colvarTownshipId.IsForeignKey = false;
				colvarTownshipId.IsReadOnly = false;
				colvarTownshipId.DefaultSetting = @"";
				colvarTownshipId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTownshipId);
				
				TableSchema.TableColumn colvarAddressString = new TableSchema.TableColumn(schema);
				colvarAddressString.ColumnName = "address_string";
				colvarAddressString.DataType = DbType.String;
				colvarAddressString.MaxLength = 100;
				colvarAddressString.AutoIncrement = false;
				colvarAddressString.IsNullable = true;
				colvarAddressString.IsPrimaryKey = false;
				colvarAddressString.IsForeignKey = false;
				colvarAddressString.IsReadOnly = false;
				colvarAddressString.DefaultSetting = @"";
				colvarAddressString.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAddressString);
				
				TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
				colvarOpenTime.ColumnName = "open_time";
				colvarOpenTime.DataType = DbType.String;
				colvarOpenTime.MaxLength = 256;
				colvarOpenTime.AutoIncrement = false;
				colvarOpenTime.IsNullable = true;
				colvarOpenTime.IsPrimaryKey = false;
				colvarOpenTime.IsForeignKey = false;
				colvarOpenTime.IsReadOnly = false;
				colvarOpenTime.DefaultSetting = @"";
				colvarOpenTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOpenTime);
				
				TableSchema.TableColumn colvarCloseDate = new TableSchema.TableColumn(schema);
				colvarCloseDate.ColumnName = "close_date";
				colvarCloseDate.DataType = DbType.String;
				colvarCloseDate.MaxLength = 256;
				colvarCloseDate.AutoIncrement = false;
				colvarCloseDate.IsNullable = true;
				colvarCloseDate.IsPrimaryKey = false;
				colvarCloseDate.IsForeignKey = false;
				colvarCloseDate.IsReadOnly = false;
				colvarCloseDate.DefaultSetting = @"";
				colvarCloseDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCloseDate);
				
				TableSchema.TableColumn colvarRemarks = new TableSchema.TableColumn(schema);
				colvarRemarks.ColumnName = "remarks";
				colvarRemarks.DataType = DbType.String;
				colvarRemarks.MaxLength = 256;
				colvarRemarks.AutoIncrement = false;
				colvarRemarks.IsNullable = true;
				colvarRemarks.IsPrimaryKey = false;
				colvarRemarks.IsForeignKey = false;
				colvarRemarks.IsReadOnly = false;
				colvarRemarks.DefaultSetting = @"";
				colvarRemarks.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemarks);
				
				TableSchema.TableColumn colvarMrt = new TableSchema.TableColumn(schema);
				colvarMrt.ColumnName = "mrt";
				colvarMrt.DataType = DbType.String;
				colvarMrt.MaxLength = 256;
				colvarMrt.AutoIncrement = false;
				colvarMrt.IsNullable = true;
				colvarMrt.IsPrimaryKey = false;
				colvarMrt.IsForeignKey = false;
				colvarMrt.IsReadOnly = false;
				colvarMrt.DefaultSetting = @"";
				colvarMrt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMrt);
				
				TableSchema.TableColumn colvarCar = new TableSchema.TableColumn(schema);
				colvarCar.ColumnName = "car";
				colvarCar.DataType = DbType.String;
				colvarCar.MaxLength = 256;
				colvarCar.AutoIncrement = false;
				colvarCar.IsNullable = true;
				colvarCar.IsPrimaryKey = false;
				colvarCar.IsForeignKey = false;
				colvarCar.IsReadOnly = false;
				colvarCar.DefaultSetting = @"";
				colvarCar.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCar);
				
				TableSchema.TableColumn colvarBus = new TableSchema.TableColumn(schema);
				colvarBus.ColumnName = "bus";
				colvarBus.DataType = DbType.String;
				colvarBus.MaxLength = 256;
				colvarBus.AutoIncrement = false;
				colvarBus.IsNullable = true;
				colvarBus.IsPrimaryKey = false;
				colvarBus.IsForeignKey = false;
				colvarBus.IsReadOnly = false;
				colvarBus.DefaultSetting = @"";
				colvarBus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBus);
				
				TableSchema.TableColumn colvarOtherVehicles = new TableSchema.TableColumn(schema);
				colvarOtherVehicles.ColumnName = "other_vehicles";
				colvarOtherVehicles.DataType = DbType.String;
				colvarOtherVehicles.MaxLength = 256;
				colvarOtherVehicles.AutoIncrement = false;
				colvarOtherVehicles.IsNullable = true;
				colvarOtherVehicles.IsPrimaryKey = false;
				colvarOtherVehicles.IsForeignKey = false;
				colvarOtherVehicles.IsReadOnly = false;
				colvarOtherVehicles.DefaultSetting = @"";
				colvarOtherVehicles.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOtherVehicles);
				
				TableSchema.TableColumn colvarWebUrl = new TableSchema.TableColumn(schema);
				colvarWebUrl.ColumnName = "web_url";
				colvarWebUrl.DataType = DbType.AnsiString;
				colvarWebUrl.MaxLength = 4000;
				colvarWebUrl.AutoIncrement = false;
				colvarWebUrl.IsNullable = true;
				colvarWebUrl.IsPrimaryKey = false;
				colvarWebUrl.IsForeignKey = false;
				colvarWebUrl.IsReadOnly = false;
				colvarWebUrl.DefaultSetting = @"";
				colvarWebUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWebUrl);
				
				TableSchema.TableColumn colvarFacebookUrl = new TableSchema.TableColumn(schema);
				colvarFacebookUrl.ColumnName = "facebook_url";
				colvarFacebookUrl.DataType = DbType.AnsiString;
				colvarFacebookUrl.MaxLength = 4000;
				colvarFacebookUrl.AutoIncrement = false;
				colvarFacebookUrl.IsNullable = true;
				colvarFacebookUrl.IsPrimaryKey = false;
				colvarFacebookUrl.IsForeignKey = false;
				colvarFacebookUrl.IsReadOnly = false;
				colvarFacebookUrl.DefaultSetting = @"";
				colvarFacebookUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFacebookUrl);
				
				TableSchema.TableColumn colvarPlurkUrl = new TableSchema.TableColumn(schema);
				colvarPlurkUrl.ColumnName = "plurk_url";
				colvarPlurkUrl.DataType = DbType.AnsiString;
				colvarPlurkUrl.MaxLength = 4000;
				colvarPlurkUrl.AutoIncrement = false;
				colvarPlurkUrl.IsNullable = true;
				colvarPlurkUrl.IsPrimaryKey = false;
				colvarPlurkUrl.IsForeignKey = false;
				colvarPlurkUrl.IsReadOnly = false;
				colvarPlurkUrl.DefaultSetting = @"";
				colvarPlurkUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPlurkUrl);
				
				TableSchema.TableColumn colvarBlogUrl = new TableSchema.TableColumn(schema);
				colvarBlogUrl.ColumnName = "blog_url";
				colvarBlogUrl.DataType = DbType.AnsiString;
				colvarBlogUrl.MaxLength = 4000;
				colvarBlogUrl.AutoIncrement = false;
				colvarBlogUrl.IsNullable = true;
				colvarBlogUrl.IsPrimaryKey = false;
				colvarBlogUrl.IsForeignKey = false;
				colvarBlogUrl.IsReadOnly = false;
				colvarBlogUrl.DefaultSetting = @"";
				colvarBlogUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBlogUrl);
				
				TableSchema.TableColumn colvarOtherUrl = new TableSchema.TableColumn(schema);
				colvarOtherUrl.ColumnName = "other_url";
				colvarOtherUrl.DataType = DbType.AnsiString;
				colvarOtherUrl.MaxLength = 4000;
				colvarOtherUrl.AutoIncrement = false;
				colvarOtherUrl.IsNullable = true;
				colvarOtherUrl.IsPrimaryKey = false;
				colvarOtherUrl.IsForeignKey = false;
				colvarOtherUrl.IsReadOnly = false;
				colvarOtherUrl.DefaultSetting = @"";
				colvarOtherUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOtherUrl);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
				colvarCompanyName.ColumnName = "CompanyName";
				colvarCompanyName.DataType = DbType.String;
				colvarCompanyName.MaxLength = 50;
				colvarCompanyName.AutoIncrement = false;
				colvarCompanyName.IsNullable = true;
				colvarCompanyName.IsPrimaryKey = false;
				colvarCompanyName.IsForeignKey = false;
				colvarCompanyName.IsReadOnly = false;
				colvarCompanyName.DefaultSetting = @"";
				colvarCompanyName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyName);
				
				TableSchema.TableColumn colvarCompanyBossName = new TableSchema.TableColumn(schema);
				colvarCompanyBossName.ColumnName = "CompanyBossName";
				colvarCompanyBossName.DataType = DbType.String;
				colvarCompanyBossName.MaxLength = 30;
				colvarCompanyBossName.AutoIncrement = false;
				colvarCompanyBossName.IsNullable = true;
				colvarCompanyBossName.IsPrimaryKey = false;
				colvarCompanyBossName.IsForeignKey = false;
				colvarCompanyBossName.IsReadOnly = false;
				colvarCompanyBossName.DefaultSetting = @"";
				colvarCompanyBossName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyBossName);
				
				TableSchema.TableColumn colvarCompanyID = new TableSchema.TableColumn(schema);
				colvarCompanyID.ColumnName = "CompanyID";
				colvarCompanyID.DataType = DbType.AnsiString;
				colvarCompanyID.MaxLength = 20;
				colvarCompanyID.AutoIncrement = false;
				colvarCompanyID.IsNullable = true;
				colvarCompanyID.IsPrimaryKey = false;
				colvarCompanyID.IsForeignKey = false;
				colvarCompanyID.IsReadOnly = false;
				colvarCompanyID.DefaultSetting = @"";
				colvarCompanyID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyID);
				
				TableSchema.TableColumn colvarCompanyBankCode = new TableSchema.TableColumn(schema);
				colvarCompanyBankCode.ColumnName = "CompanyBankCode";
				colvarCompanyBankCode.DataType = DbType.AnsiString;
				colvarCompanyBankCode.MaxLength = 10;
				colvarCompanyBankCode.AutoIncrement = false;
				colvarCompanyBankCode.IsNullable = true;
				colvarCompanyBankCode.IsPrimaryKey = false;
				colvarCompanyBankCode.IsForeignKey = false;
				colvarCompanyBankCode.IsReadOnly = false;
				colvarCompanyBankCode.DefaultSetting = @"";
				colvarCompanyBankCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyBankCode);
				
				TableSchema.TableColumn colvarCompanyBranchCode = new TableSchema.TableColumn(schema);
				colvarCompanyBranchCode.ColumnName = "CompanyBranchCode";
				colvarCompanyBranchCode.DataType = DbType.AnsiString;
				colvarCompanyBranchCode.MaxLength = 10;
				colvarCompanyBranchCode.AutoIncrement = false;
				colvarCompanyBranchCode.IsNullable = true;
				colvarCompanyBranchCode.IsPrimaryKey = false;
				colvarCompanyBranchCode.IsForeignKey = false;
				colvarCompanyBranchCode.IsReadOnly = false;
				colvarCompanyBranchCode.DefaultSetting = @"";
				colvarCompanyBranchCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyBranchCode);
				
				TableSchema.TableColumn colvarCompanyAccount = new TableSchema.TableColumn(schema);
				colvarCompanyAccount.ColumnName = "CompanyAccount";
				colvarCompanyAccount.DataType = DbType.AnsiString;
				colvarCompanyAccount.MaxLength = 20;
				colvarCompanyAccount.AutoIncrement = false;
				colvarCompanyAccount.IsNullable = true;
				colvarCompanyAccount.IsPrimaryKey = false;
				colvarCompanyAccount.IsForeignKey = false;
				colvarCompanyAccount.IsReadOnly = false;
				colvarCompanyAccount.DefaultSetting = @"";
				colvarCompanyAccount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyAccount);
				
				TableSchema.TableColumn colvarCompanyAccountName = new TableSchema.TableColumn(schema);
				colvarCompanyAccountName.ColumnName = "CompanyAccountName";
				colvarCompanyAccountName.DataType = DbType.String;
				colvarCompanyAccountName.MaxLength = 50;
				colvarCompanyAccountName.AutoIncrement = false;
				colvarCompanyAccountName.IsNullable = true;
				colvarCompanyAccountName.IsPrimaryKey = false;
				colvarCompanyAccountName.IsForeignKey = false;
				colvarCompanyAccountName.IsReadOnly = false;
				colvarCompanyAccountName.DefaultSetting = @"";
				colvarCompanyAccountName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyAccountName);
				
				TableSchema.TableColumn colvarCompanyEmail = new TableSchema.TableColumn(schema);
				colvarCompanyEmail.ColumnName = "CompanyEmail";
				colvarCompanyEmail.DataType = DbType.String;
				colvarCompanyEmail.MaxLength = 250;
				colvarCompanyEmail.AutoIncrement = false;
				colvarCompanyEmail.IsNullable = true;
				colvarCompanyEmail.IsPrimaryKey = false;
				colvarCompanyEmail.IsForeignKey = false;
				colvarCompanyEmail.IsReadOnly = false;
				colvarCompanyEmail.DefaultSetting = @"";
				colvarCompanyEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyEmail);
				
				TableSchema.TableColumn colvarCompanyNotice = new TableSchema.TableColumn(schema);
				colvarCompanyNotice.ColumnName = "CompanyNotice";
				colvarCompanyNotice.DataType = DbType.String;
				colvarCompanyNotice.MaxLength = 250;
				colvarCompanyNotice.AutoIncrement = false;
				colvarCompanyNotice.IsNullable = true;
				colvarCompanyNotice.IsPrimaryKey = false;
				colvarCompanyNotice.IsForeignKey = false;
				colvarCompanyNotice.IsReadOnly = false;
				colvarCompanyNotice.DefaultSetting = @"";
				colvarCompanyNotice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyNotice);
				
				TableSchema.TableColumn colvarIsCloseDown = new TableSchema.TableColumn(schema);
				colvarIsCloseDown.ColumnName = "is_close_down";
				colvarIsCloseDown.DataType = DbType.Boolean;
				colvarIsCloseDown.MaxLength = 0;
				colvarIsCloseDown.AutoIncrement = false;
				colvarIsCloseDown.IsNullable = false;
				colvarIsCloseDown.IsPrimaryKey = false;
				colvarIsCloseDown.IsForeignKey = false;
				colvarIsCloseDown.IsReadOnly = false;
				colvarIsCloseDown.DefaultSetting = @"";
				colvarIsCloseDown.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCloseDown);
				
				TableSchema.TableColumn colvarCloseDownDate = new TableSchema.TableColumn(schema);
				colvarCloseDownDate.ColumnName = "close_down_date";
				colvarCloseDownDate.DataType = DbType.DateTime;
				colvarCloseDownDate.MaxLength = 0;
				colvarCloseDownDate.AutoIncrement = false;
				colvarCloseDownDate.IsNullable = true;
				colvarCloseDownDate.IsPrimaryKey = false;
				colvarCloseDownDate.IsForeignKey = false;
				colvarCloseDownDate.IsReadOnly = false;
				colvarCloseDownDate.DefaultSetting = @"";
				colvarCloseDownDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCloseDownDate);
				
				TableSchema.TableColumn colvarSignCompanyID = new TableSchema.TableColumn(schema);
				colvarSignCompanyID.ColumnName = "SignCompanyID";
				colvarSignCompanyID.DataType = DbType.AnsiString;
				colvarSignCompanyID.MaxLength = 20;
				colvarSignCompanyID.AutoIncrement = false;
				colvarSignCompanyID.IsNullable = true;
				colvarSignCompanyID.IsPrimaryKey = false;
				colvarSignCompanyID.IsForeignKey = false;
				colvarSignCompanyID.IsReadOnly = false;
				colvarSignCompanyID.DefaultSetting = @"";
				colvarSignCompanyID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSignCompanyID);
				
				TableSchema.TableColumn colvarAccountantName = new TableSchema.TableColumn(schema);
				colvarAccountantName.ColumnName = "accountant_name";
				colvarAccountantName.DataType = DbType.String;
				colvarAccountantName.MaxLength = 20;
				colvarAccountantName.AutoIncrement = false;
				colvarAccountantName.IsNullable = true;
				colvarAccountantName.IsPrimaryKey = false;
				colvarAccountantName.IsForeignKey = false;
				colvarAccountantName.IsReadOnly = false;
				colvarAccountantName.DefaultSetting = @"";
				colvarAccountantName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountantName);
				
				TableSchema.TableColumn colvarAccountantTel = new TableSchema.TableColumn(schema);
				colvarAccountantTel.ColumnName = "accountant_tel";
				colvarAccountantTel.DataType = DbType.AnsiString;
				colvarAccountantTel.MaxLength = 100;
				colvarAccountantTel.AutoIncrement = false;
				colvarAccountantTel.IsNullable = true;
				colvarAccountantTel.IsPrimaryKey = false;
				colvarAccountantTel.IsForeignKey = false;
				colvarAccountantTel.IsReadOnly = false;
				colvarAccountantTel.DefaultSetting = @"";
				colvarAccountantTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountantTel);
				
				TableSchema.TableColumn colvarCreditcardAvailable = new TableSchema.TableColumn(schema);
				colvarCreditcardAvailable.ColumnName = "creditcard_available";
				colvarCreditcardAvailable.DataType = DbType.Boolean;
				colvarCreditcardAvailable.MaxLength = 0;
				colvarCreditcardAvailable.AutoIncrement = false;
				colvarCreditcardAvailable.IsNullable = false;
				colvarCreditcardAvailable.IsPrimaryKey = false;
				colvarCreditcardAvailable.IsForeignKey = false;
				colvarCreditcardAvailable.IsReadOnly = false;
				
						colvarCreditcardAvailable.DefaultSetting = @"((0))";
				colvarCreditcardAvailable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditcardAvailable);
				
				TableSchema.TableColumn colvarTempStatus = new TableSchema.TableColumn(schema);
				colvarTempStatus.ColumnName = "temp_status";
				colvarTempStatus.DataType = DbType.Int32;
				colvarTempStatus.MaxLength = 0;
				colvarTempStatus.AutoIncrement = false;
				colvarTempStatus.IsNullable = false;
				colvarTempStatus.IsPrimaryKey = false;
				colvarTempStatus.IsForeignKey = false;
				colvarTempStatus.IsReadOnly = false;
				
						colvarTempStatus.DefaultSetting = @"((0))";
				colvarTempStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTempStatus);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 1073741823;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarNewCreated = new TableSchema.TableColumn(schema);
				colvarNewCreated.ColumnName = "new_created";
				colvarNewCreated.DataType = DbType.Boolean;
				colvarNewCreated.MaxLength = 0;
				colvarNewCreated.AutoIncrement = false;
				colvarNewCreated.IsNullable = false;
				colvarNewCreated.IsPrimaryKey = false;
				colvarNewCreated.IsForeignKey = false;
				colvarNewCreated.IsReadOnly = false;
				
						colvarNewCreated.DefaultSetting = @"((0))";
				colvarNewCreated.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNewCreated);
				
				TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
				colvarCoordinate.ColumnName = "coordinate";
				colvarCoordinate.DataType = DbType.AnsiString;
				colvarCoordinate.MaxLength = -1;
				colvarCoordinate.AutoIncrement = false;
				colvarCoordinate.IsNullable = true;
				colvarCoordinate.IsPrimaryKey = false;
				colvarCoordinate.IsForeignKey = false;
				colvarCoordinate.IsReadOnly = false;
				colvarCoordinate.DefaultSetting = @"";
				colvarCoordinate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCoordinate);
				
				TableSchema.TableColumn colvarApplyId = new TableSchema.TableColumn(schema);
				colvarApplyId.ColumnName = "apply_id";
				colvarApplyId.DataType = DbType.String;
				colvarApplyId.MaxLength = 100;
				colvarApplyId.AutoIncrement = false;
				colvarApplyId.IsNullable = true;
				colvarApplyId.IsPrimaryKey = false;
				colvarApplyId.IsForeignKey = false;
				colvarApplyId.IsReadOnly = false;
				colvarApplyId.DefaultSetting = @"";
				colvarApplyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApplyId);
				
				TableSchema.TableColumn colvarApplyTime = new TableSchema.TableColumn(schema);
				colvarApplyTime.ColumnName = "apply_time";
				colvarApplyTime.DataType = DbType.DateTime;
				colvarApplyTime.MaxLength = 0;
				colvarApplyTime.AutoIncrement = false;
				colvarApplyTime.IsNullable = true;
				colvarApplyTime.IsPrimaryKey = false;
				colvarApplyTime.IsForeignKey = false;
				colvarApplyTime.IsReadOnly = false;
				colvarApplyTime.DefaultSetting = @"";
				colvarApplyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApplyTime);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = true;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarReturnTime = new TableSchema.TableColumn(schema);
				colvarReturnTime.ColumnName = "return_time";
				colvarReturnTime.DataType = DbType.DateTime;
				colvarReturnTime.MaxLength = 0;
				colvarReturnTime.AutoIncrement = false;
				colvarReturnTime.IsNullable = true;
				colvarReturnTime.IsPrimaryKey = false;
				colvarReturnTime.IsForeignKey = false;
				colvarReturnTime.IsReadOnly = false;
				colvarReturnTime.DefaultSetting = @"";
				colvarReturnTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnTime);
				
				TableSchema.TableColumn colvarApproveTime = new TableSchema.TableColumn(schema);
				colvarApproveTime.ColumnName = "approve_time";
				colvarApproveTime.DataType = DbType.DateTime;
				colvarApproveTime.MaxLength = 0;
				colvarApproveTime.AutoIncrement = false;
				colvarApproveTime.IsNullable = true;
				colvarApproveTime.IsPrimaryKey = false;
				colvarApproveTime.IsForeignKey = false;
				colvarApproveTime.IsReadOnly = false;
				colvarApproveTime.DefaultSetting = @"";
				colvarApproveTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApproveTime);
				
				TableSchema.TableColumn colvarIsOpenReservationSetting = new TableSchema.TableColumn(schema);
				colvarIsOpenReservationSetting.ColumnName = "is_open_reservation_setting";
				colvarIsOpenReservationSetting.DataType = DbType.Boolean;
				colvarIsOpenReservationSetting.MaxLength = 0;
				colvarIsOpenReservationSetting.AutoIncrement = false;
				colvarIsOpenReservationSetting.IsNullable = false;
				colvarIsOpenReservationSetting.IsPrimaryKey = false;
				colvarIsOpenReservationSetting.IsForeignKey = false;
				colvarIsOpenReservationSetting.IsReadOnly = false;
				
						colvarIsOpenReservationSetting.DefaultSetting = @"((0))";
				colvarIsOpenReservationSetting.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOpenReservationSetting);
				
				TableSchema.TableColumn colvarIsOpenBooking = new TableSchema.TableColumn(schema);
				colvarIsOpenBooking.ColumnName = "is_open_booking";
				colvarIsOpenBooking.DataType = DbType.Boolean;
				colvarIsOpenBooking.MaxLength = 0;
				colvarIsOpenBooking.AutoIncrement = false;
				colvarIsOpenBooking.IsNullable = false;
				colvarIsOpenBooking.IsPrimaryKey = false;
				colvarIsOpenBooking.IsForeignKey = false;
				colvarIsOpenBooking.IsReadOnly = false;
				
						colvarIsOpenBooking.DefaultSetting = @"((0))";
				colvarIsOpenBooking.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOpenBooking);
			
			
				TableSchema.TableColumn colvarStoreRelationCode = new TableSchema.TableColumn(schema);
				colvarStoreRelationCode.ColumnName = "store_relation_code";
				colvarStoreRelationCode.DataType = DbType.AnsiString;
				colvarStoreRelationCode.MaxLength = 50;
				colvarStoreRelationCode.AutoIncrement = false;
				colvarStoreRelationCode.IsNullable = true;
				colvarStoreRelationCode.IsPrimaryKey = false;
				colvarStoreRelationCode.IsForeignKey = false;
				colvarStoreRelationCode.IsReadOnly = false;
				colvarStoreRelationCode.DefaultSetting = @"";
				colvarStoreRelationCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreRelationCode);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("store",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("StoreName")]
		[Bindable(true)]
		public string StoreName 
		{
			get { return GetColumnValue<string>(Columns.StoreName); }
			set { SetColumnValue(Columns.StoreName, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("Phone")]
		[Bindable(true)]
		public string Phone 
		{
			get { return GetColumnValue<string>(Columns.Phone); }
			set { SetColumnValue(Columns.Phone, value); }
		}
		  
		[XmlAttribute("TownshipId")]
		[Bindable(true)]
		public int? TownshipId 
		{
			get { return GetColumnValue<int?>(Columns.TownshipId); }
			set { SetColumnValue(Columns.TownshipId, value); }
		}
		  
		[XmlAttribute("AddressString")]
		[Bindable(true)]
		public string AddressString 
		{
			get { return GetColumnValue<string>(Columns.AddressString); }
			set { SetColumnValue(Columns.AddressString, value); }
		}
		  
		[XmlAttribute("OpenTime")]
		[Bindable(true)]
		public string OpenTime 
		{
			get { return GetColumnValue<string>(Columns.OpenTime); }
			set { SetColumnValue(Columns.OpenTime, value); }
		}
		  
		[XmlAttribute("CloseDate")]
		[Bindable(true)]
		public string CloseDate 
		{
			get { return GetColumnValue<string>(Columns.CloseDate); }
			set { SetColumnValue(Columns.CloseDate, value); }
		}
		  
		[XmlAttribute("Remarks")]
		[Bindable(true)]
		public string Remarks 
		{
			get { return GetColumnValue<string>(Columns.Remarks); }
			set { SetColumnValue(Columns.Remarks, value); }
		}
		  
		[XmlAttribute("Mrt")]
		[Bindable(true)]
		public string Mrt 
		{
			get { return GetColumnValue<string>(Columns.Mrt); }
			set { SetColumnValue(Columns.Mrt, value); }
		}
		  
		[XmlAttribute("Car")]
		[Bindable(true)]
		public string Car 
		{
			get { return GetColumnValue<string>(Columns.Car); }
			set { SetColumnValue(Columns.Car, value); }
		}
		  
		[XmlAttribute("Bus")]
		[Bindable(true)]
		public string Bus 
		{
			get { return GetColumnValue<string>(Columns.Bus); }
			set { SetColumnValue(Columns.Bus, value); }
		}
		  
		[XmlAttribute("OtherVehicles")]
		[Bindable(true)]
		public string OtherVehicles 
		{
			get { return GetColumnValue<string>(Columns.OtherVehicles); }
			set { SetColumnValue(Columns.OtherVehicles, value); }
		}
		  
		[XmlAttribute("WebUrl")]
		[Bindable(true)]
		public string WebUrl 
		{
			get { return GetColumnValue<string>(Columns.WebUrl); }
			set { SetColumnValue(Columns.WebUrl, value); }
		}
		  
		[XmlAttribute("FacebookUrl")]
		[Bindable(true)]
		public string FacebookUrl 
		{
			get { return GetColumnValue<string>(Columns.FacebookUrl); }
			set { SetColumnValue(Columns.FacebookUrl, value); }
		}
		  
		[XmlAttribute("PlurkUrl")]
		[Bindable(true)]
		public string PlurkUrl 
		{
			get { return GetColumnValue<string>(Columns.PlurkUrl); }
			set { SetColumnValue(Columns.PlurkUrl, value); }
		}
		  
		[XmlAttribute("BlogUrl")]
		[Bindable(true)]
		public string BlogUrl 
		{
			get { return GetColumnValue<string>(Columns.BlogUrl); }
			set { SetColumnValue(Columns.BlogUrl, value); }
		}
		  
		[XmlAttribute("OtherUrl")]
		[Bindable(true)]
		public string OtherUrl 
		{
			get { return GetColumnValue<string>(Columns.OtherUrl); }
			set { SetColumnValue(Columns.OtherUrl, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CompanyName")]
		[Bindable(true)]
		public string CompanyName 
		{
			get { return GetColumnValue<string>(Columns.CompanyName); }
			set { SetColumnValue(Columns.CompanyName, value); }
		}
		  
		[XmlAttribute("CompanyBossName")]
		[Bindable(true)]
		public string CompanyBossName 
		{
			get { return GetColumnValue<string>(Columns.CompanyBossName); }
			set { SetColumnValue(Columns.CompanyBossName, value); }
		}
		  
		[XmlAttribute("CompanyID")]
		[Bindable(true)]
		public string CompanyID 
		{
			get { return GetColumnValue<string>(Columns.CompanyID); }
			set { SetColumnValue(Columns.CompanyID, value); }
		}
		  
		[XmlAttribute("CompanyBankCode")]
		[Bindable(true)]
		public string CompanyBankCode 
		{
			get { return GetColumnValue<string>(Columns.CompanyBankCode); }
			set { SetColumnValue(Columns.CompanyBankCode, value); }
		}
		  
		[XmlAttribute("CompanyBranchCode")]
		[Bindable(true)]
		public string CompanyBranchCode 
		{
			get { return GetColumnValue<string>(Columns.CompanyBranchCode); }
			set { SetColumnValue(Columns.CompanyBranchCode, value); }
		}
		  
		[XmlAttribute("CompanyAccount")]
		[Bindable(true)]
		public string CompanyAccount 
		{
			get { return GetColumnValue<string>(Columns.CompanyAccount); }
			set { SetColumnValue(Columns.CompanyAccount, value); }
		}
		  
		[XmlAttribute("CompanyAccountName")]
		[Bindable(true)]
		public string CompanyAccountName 
		{
			get { return GetColumnValue<string>(Columns.CompanyAccountName); }
			set { SetColumnValue(Columns.CompanyAccountName, value); }
		}
		  
		[XmlAttribute("CompanyEmail")]
		[Bindable(true)]
		public string CompanyEmail 
		{
			get { return GetColumnValue<string>(Columns.CompanyEmail); }
			set { SetColumnValue(Columns.CompanyEmail, value); }
		}
		  
		[XmlAttribute("CompanyNotice")]
		[Bindable(true)]
		public string CompanyNotice 
		{
			get { return GetColumnValue<string>(Columns.CompanyNotice); }
			set { SetColumnValue(Columns.CompanyNotice, value); }
		}
		  
		[XmlAttribute("IsCloseDown")]
		[Bindable(true)]
		public bool IsCloseDown 
		{
			get { return GetColumnValue<bool>(Columns.IsCloseDown); }
			set { SetColumnValue(Columns.IsCloseDown, value); }
		}
		  
		[XmlAttribute("CloseDownDate")]
		[Bindable(true)]
		public DateTime? CloseDownDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.CloseDownDate); }
			set { SetColumnValue(Columns.CloseDownDate, value); }
		}
		  
		[XmlAttribute("SignCompanyID")]
		[Bindable(true)]
		public string SignCompanyID 
		{
			get { return GetColumnValue<string>(Columns.SignCompanyID); }
			set { SetColumnValue(Columns.SignCompanyID, value); }
		}
		  
		[XmlAttribute("AccountantName")]
		[Bindable(true)]
		public string AccountantName 
		{
			get { return GetColumnValue<string>(Columns.AccountantName); }
			set { SetColumnValue(Columns.AccountantName, value); }
		}
		  
		[XmlAttribute("AccountantTel")]
		[Bindable(true)]
		public string AccountantTel 
		{
			get { return GetColumnValue<string>(Columns.AccountantTel); }
			set { SetColumnValue(Columns.AccountantTel, value); }
		}
		  
		[XmlAttribute("CreditcardAvailable")]
		[Bindable(true)]
		public bool CreditcardAvailable 
		{
			get { return GetColumnValue<bool>(Columns.CreditcardAvailable); }
			set { SetColumnValue(Columns.CreditcardAvailable, value); }
		}
		  
		[XmlAttribute("TempStatus")]
		[Bindable(true)]
		public int TempStatus 
		{
			get { return GetColumnValue<int>(Columns.TempStatus); }
			set { SetColumnValue(Columns.TempStatus, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("NewCreated")]
		[Bindable(true)]
		public bool NewCreated 
		{
			get { return GetColumnValue<bool>(Columns.NewCreated); }
			set { SetColumnValue(Columns.NewCreated, value); }
		}
		  
		[XmlAttribute("Coordinate")]
		[Bindable(true)]
		public string Coordinate 
		{
            get { return GetColumnValue<object>(Columns.Coordinate) != null ? GetColumnValue<object>(Columns.Coordinate).ToString() : string.Empty; }
			set { SetColumnValue(Columns.Coordinate, value); }
		}
		  
		[XmlAttribute("ApplyId")]
		[Bindable(true)]
		public string ApplyId 
		{
			get { return GetColumnValue<string>(Columns.ApplyId); }
			set { SetColumnValue(Columns.ApplyId, value); }
		}
		  
		[XmlAttribute("ApplyTime")]
		[Bindable(true)]
		public DateTime? ApplyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ApplyTime); }
			set { SetColumnValue(Columns.ApplyTime, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int? CityId 
		{
			get { return GetColumnValue<int?>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("ReturnTime")]
		[Bindable(true)]
		public DateTime? ReturnTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ReturnTime); }
			set { SetColumnValue(Columns.ReturnTime, value); }
		}
		  
		[XmlAttribute("ApproveTime")]
		[Bindable(true)]
		public DateTime? ApproveTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ApproveTime); }
			set { SetColumnValue(Columns.ApproveTime, value); }
		}
		  
		[XmlAttribute("IsOpenReservationSetting")]
		[Bindable(true)]
		public bool IsOpenReservationSetting 
		{
			get { return GetColumnValue<bool>(Columns.IsOpenReservationSetting); }
			set { SetColumnValue(Columns.IsOpenReservationSetting, value); }
		}
		  
		[XmlAttribute("IsOpenBooking")]
		[Bindable(true)]
		public bool IsOpenBooking 
		{
			get { return GetColumnValue<bool>(Columns.IsOpenBooking); }
			set { SetColumnValue(Columns.IsOpenBooking, value); }
		}
		  
		[XmlAttribute("StoreRelationCode")]
		[Bindable(true)]
		public string StoreRelationCode 
		{
			get { return GetColumnValue<string>(Columns.StoreRelationCode); }
			set { SetColumnValue(Columns.StoreRelationCode, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (1)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PhoneColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn TownshipIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AddressStringColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn OpenTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CloseDateColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn RemarksColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn MrtColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CarColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn BusColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn OtherVehiclesColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn WebUrlColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn FacebookUrlColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn PlurkUrlColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn BlogUrlColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn OtherUrlColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyNameColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyBossNameColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyIDColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyBankCodeColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyBranchCodeColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyAccountColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyAccountNameColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyEmailColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyNoticeColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCloseDownColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn CloseDownDateColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn SignCompanyIDColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountantNameColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountantTelColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn CreditcardAvailableColumn
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn TempStatusColumn
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        public static TableSchema.TableColumn NewCreatedColumn
        {
            get { return Schema.Columns[40]; }
        }
        
        
        
        public static TableSchema.TableColumn CoordinateColumn
        {
            get { return Schema.Columns[41]; }
        }
        
        
        
        public static TableSchema.TableColumn ApplyIdColumn
        {
            get { return Schema.Columns[42]; }
        }
        
        
        
        public static TableSchema.TableColumn ApplyTimeColumn
        {
            get { return Schema.Columns[43]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[44]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnTimeColumn
        {
            get { return Schema.Columns[45]; }
        }
        
        
        
        public static TableSchema.TableColumn ApproveTimeColumn
        {
            get { return Schema.Columns[46]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOpenReservationSettingColumn
        {
            get { return Schema.Columns[47]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOpenBookingColumn
        {
            get { return Schema.Columns[48]; }
        }
        
        public static TableSchema.TableColumn StoreRelationCodeColumn
        {
            get { return Schema.Columns[49]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"Guid";
			 public static string StoreName = @"store_name";
			 public static string SellerGuid = @"seller_guid";
			 public static string Phone = @"phone";
			 public static string TownshipId = @"township_id";
			 public static string AddressString = @"address_string";
			 public static string OpenTime = @"open_time";
			 public static string CloseDate = @"close_date";
			 public static string Remarks = @"remarks";
			 public static string Mrt = @"mrt";
			 public static string Car = @"car";
			 public static string Bus = @"bus";
			 public static string OtherVehicles = @"other_vehicles";
			 public static string WebUrl = @"web_url";
			 public static string FacebookUrl = @"facebook_url";
			 public static string PlurkUrl = @"plurk_url";
			 public static string BlogUrl = @"blog_url";
			 public static string OtherUrl = @"other_url";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string Status = @"status";
			 public static string CompanyName = @"CompanyName";
			 public static string CompanyBossName = @"CompanyBossName";
			 public static string CompanyID = @"CompanyID";
			 public static string CompanyBankCode = @"CompanyBankCode";
			 public static string CompanyBranchCode = @"CompanyBranchCode";
			 public static string CompanyAccount = @"CompanyAccount";
			 public static string CompanyAccountName = @"CompanyAccountName";
			 public static string CompanyEmail = @"CompanyEmail";
			 public static string CompanyNotice = @"CompanyNotice";
			 public static string IsCloseDown = @"is_close_down";
			 public static string CloseDownDate = @"close_down_date";
			 public static string SignCompanyID = @"SignCompanyID";
			 public static string AccountantName = @"accountant_name";
			 public static string AccountantTel = @"accountant_tel";
			 public static string CreditcardAvailable = @"creditcard_available";
			 public static string TempStatus = @"temp_status";
			 public static string Message = @"message";
			 public static string NewCreated = @"new_created";
			 public static string Coordinate = @"coordinate";
			 public static string ApplyId = @"apply_id";
			 public static string ApplyTime = @"apply_time";
			 public static string CityId = @"city_id";
			 public static string ReturnTime = @"return_time";
			 public static string ApproveTime = @"approve_time";
			 public static string IsOpenReservationSetting = @"is_open_reservation_setting";
			 public static string IsOpenBooking = @"is_open_booking";
			 public static string StoreRelationCode = @"store_relation_code";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProposalMultiDeal class.
    /// </summary>
    [Serializable]
    public partial class ProposalMultiDealCollection : RepositoryList<ProposalMultiDeal, ProposalMultiDealCollection>
    {
        public ProposalMultiDealCollection() {}

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalMultiDealCollection</returns>
        public ProposalMultiDealCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProposalMultiDeal o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the proposal_multi_deals table.
    /// </summary>
    [Serializable]
    public partial class ProposalMultiDeal : RepositoryRecord<ProposalMultiDeal>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProposalMultiDeal()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProposalMultiDeal(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("proposal_multi_deals", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarPid = new TableSchema.TableColumn(schema);
                colvarPid.ColumnName = "pid";
                colvarPid.DataType = DbType.Int32;
                colvarPid.MaxLength = 0;
                colvarPid.AutoIncrement = false;
                colvarPid.IsNullable = false;
                colvarPid.IsPrimaryKey = false;
                colvarPid.IsForeignKey = false;
                colvarPid.IsReadOnly = false;
                colvarPid.DefaultSetting = @"";
                colvarPid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPid);

                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = true;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                colvarBid.DefaultSetting = @"";
                colvarBid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBid);

                TableSchema.TableColumn colvarBrandName = new TableSchema.TableColumn(schema);
                colvarBrandName.ColumnName = "brand_name";
                colvarBrandName.DataType = DbType.String;
                colvarBrandName.MaxLength = 100;
                colvarBrandName.AutoIncrement = false;
                colvarBrandName.IsNullable = false;
                colvarBrandName.IsPrimaryKey = false;
                colvarBrandName.IsForeignKey = false;
                colvarBrandName.IsReadOnly = false;
                colvarBrandName.DefaultSetting = @"";
                colvarBrandName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBrandName);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                colvarItemName.DefaultSetting = @"";
                colvarItemName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarItemNameTravel = new TableSchema.TableColumn(schema);
                colvarItemNameTravel.ColumnName = "item_name_travel";
                colvarItemNameTravel.DataType = DbType.String;
                colvarItemNameTravel.MaxLength = 750;
                colvarItemNameTravel.AutoIncrement = false;
                colvarItemNameTravel.IsNullable = false;
                colvarItemNameTravel.IsPrimaryKey = false;
                colvarItemNameTravel.IsForeignKey = false;
                colvarItemNameTravel.IsReadOnly = false;
                colvarItemNameTravel.DefaultSetting = @"";
                colvarItemNameTravel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemNameTravel);

                TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
                colvarContent.ColumnName = "content";
                colvarContent.DataType = DbType.String;
                colvarContent.MaxLength = 400;
                colvarContent.AutoIncrement = false;
                colvarContent.IsNullable = false;
                colvarContent.IsPrimaryKey = false;
                colvarContent.IsForeignKey = false;
                colvarContent.IsReadOnly = false;
                colvarContent.DefaultSetting = @"";
                colvarContent.ForeignKeyTableName = "";
                schema.Columns.Add(colvarContent);

                TableSchema.TableColumn colvarOrigPrice = new TableSchema.TableColumn(schema);
                colvarOrigPrice.ColumnName = "orig_price";
                colvarOrigPrice.DataType = DbType.Currency;
                colvarOrigPrice.MaxLength = 0;
                colvarOrigPrice.AutoIncrement = false;
                colvarOrigPrice.IsNullable = false;
                colvarOrigPrice.IsPrimaryKey = false;
                colvarOrigPrice.IsForeignKey = false;
                colvarOrigPrice.IsReadOnly = false;
                colvarOrigPrice.DefaultSetting = @"";
                colvarOrigPrice.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrigPrice);

                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                colvarItemPrice.DefaultSetting = @"";
                colvarItemPrice.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemPrice);

                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Currency;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = false;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;
                colvarCost.DefaultSetting = @"";
                colvarCost.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCost);

                TableSchema.TableColumn colvarGroupCouponBuy = new TableSchema.TableColumn(schema);
                colvarGroupCouponBuy.ColumnName = "group_coupon_buy";
                colvarGroupCouponBuy.DataType = DbType.Int32;
                colvarGroupCouponBuy.MaxLength = 0;
                colvarGroupCouponBuy.AutoIncrement = false;
                colvarGroupCouponBuy.IsNullable = false;
                colvarGroupCouponBuy.IsPrimaryKey = false;
                colvarGroupCouponBuy.IsForeignKey = false;
                colvarGroupCouponBuy.IsReadOnly = false;
                colvarGroupCouponBuy.DefaultSetting = @"";
                colvarGroupCouponBuy.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGroupCouponBuy);

                TableSchema.TableColumn colvarGroupCouponPresent = new TableSchema.TableColumn(schema);
                colvarGroupCouponPresent.ColumnName = "group_coupon_present";
                colvarGroupCouponPresent.DataType = DbType.Int32;
                colvarGroupCouponPresent.MaxLength = 0;
                colvarGroupCouponPresent.AutoIncrement = false;
                colvarGroupCouponPresent.IsNullable = false;
                colvarGroupCouponPresent.IsPrimaryKey = false;
                colvarGroupCouponPresent.IsForeignKey = false;
                colvarGroupCouponPresent.IsReadOnly = false;
                colvarGroupCouponPresent.DefaultSetting = @"";
                colvarGroupCouponPresent.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGroupCouponPresent);

                TableSchema.TableColumn colvarSlottingFeeQuantity = new TableSchema.TableColumn(schema);
                colvarSlottingFeeQuantity.ColumnName = "slotting_fee_quantity";
                colvarSlottingFeeQuantity.DataType = DbType.Int32;
                colvarSlottingFeeQuantity.MaxLength = 0;
                colvarSlottingFeeQuantity.AutoIncrement = false;
                colvarSlottingFeeQuantity.IsNullable = false;
                colvarSlottingFeeQuantity.IsPrimaryKey = false;
                colvarSlottingFeeQuantity.IsForeignKey = false;
                colvarSlottingFeeQuantity.IsReadOnly = false;
                colvarSlottingFeeQuantity.DefaultSetting = @"";
                colvarSlottingFeeQuantity.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSlottingFeeQuantity);

                TableSchema.TableColumn colvarOrderMaxPersonal = new TableSchema.TableColumn(schema);
                colvarOrderMaxPersonal.ColumnName = "order_max_personal";
                colvarOrderMaxPersonal.DataType = DbType.Int32;
                colvarOrderMaxPersonal.MaxLength = 0;
                colvarOrderMaxPersonal.AutoIncrement = false;
                colvarOrderMaxPersonal.IsNullable = false;
                colvarOrderMaxPersonal.IsPrimaryKey = false;
                colvarOrderMaxPersonal.IsForeignKey = false;
                colvarOrderMaxPersonal.IsReadOnly = false;
                colvarOrderMaxPersonal.DefaultSetting = @"";
                colvarOrderMaxPersonal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderMaxPersonal);

                TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
                colvarOrderTotalLimit.ColumnName = "order_total_limit";
                colvarOrderTotalLimit.DataType = DbType.Int32;
                colvarOrderTotalLimit.MaxLength = 0;
                colvarOrderTotalLimit.AutoIncrement = false;
                colvarOrderTotalLimit.IsNullable = false;
                colvarOrderTotalLimit.IsPrimaryKey = false;
                colvarOrderTotalLimit.IsForeignKey = false;
                colvarOrderTotalLimit.IsReadOnly = false;
                colvarOrderTotalLimit.DefaultSetting = @"";
                colvarOrderTotalLimit.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderTotalLimit);

                TableSchema.TableColumn colvarAtmMaximum = new TableSchema.TableColumn(schema);
                colvarAtmMaximum.ColumnName = "atm_maximum";
                colvarAtmMaximum.DataType = DbType.Int32;
                colvarAtmMaximum.MaxLength = 0;
                colvarAtmMaximum.AutoIncrement = false;
                colvarAtmMaximum.IsNullable = false;
                colvarAtmMaximum.IsPrimaryKey = false;
                colvarAtmMaximum.IsForeignKey = false;
                colvarAtmMaximum.IsReadOnly = false;
                colvarAtmMaximum.DefaultSetting = @"";
                colvarAtmMaximum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAtmMaximum);

                TableSchema.TableColumn colvarFreights = new TableSchema.TableColumn(schema);
                colvarFreights.ColumnName = "freights";
                colvarFreights.DataType = DbType.Currency;
                colvarFreights.MaxLength = 0;
                colvarFreights.AutoIncrement = false;
                colvarFreights.IsNullable = false;
                colvarFreights.IsPrimaryKey = false;
                colvarFreights.IsForeignKey = false;
                colvarFreights.IsReadOnly = false;
                colvarFreights.DefaultSetting = @"";
                colvarFreights.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFreights);

                TableSchema.TableColumn colvarNoFreightLimit = new TableSchema.TableColumn(schema);
                colvarNoFreightLimit.ColumnName = "no_freight_limit";
                colvarNoFreightLimit.DataType = DbType.Int32;
                colvarNoFreightLimit.MaxLength = 0;
                colvarNoFreightLimit.AutoIncrement = false;
                colvarNoFreightLimit.IsNullable = false;
                colvarNoFreightLimit.IsPrimaryKey = false;
                colvarNoFreightLimit.IsForeignKey = false;
                colvarNoFreightLimit.IsReadOnly = false;
                colvarNoFreightLimit.DefaultSetting = @"";
                colvarNoFreightLimit.ForeignKeyTableName = "";
                schema.Columns.Add(colvarNoFreightLimit);

                TableSchema.TableColumn colvarOptions = new TableSchema.TableColumn(schema);
                colvarOptions.ColumnName = "options";
                colvarOptions.DataType = DbType.String;
                colvarOptions.MaxLength = -1;
                colvarOptions.AutoIncrement = false;
                colvarOptions.IsNullable = false;
                colvarOptions.IsPrimaryKey = false;
                colvarOptions.IsForeignKey = false;
                colvarOptions.IsReadOnly = false;
                colvarOptions.DefaultSetting = @"";
                colvarOptions.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOptions);

                TableSchema.TableColumn colvarSellerOptions = new TableSchema.TableColumn(schema);
                colvarSellerOptions.ColumnName = "seller_options";
                colvarSellerOptions.DataType = DbType.String;
                colvarSellerOptions.MaxLength = -1;
                colvarSellerOptions.AutoIncrement = false;
                colvarSellerOptions.IsNullable = false;
                colvarSellerOptions.IsPrimaryKey = false;
                colvarSellerOptions.IsForeignKey = false;
                colvarSellerOptions.IsReadOnly = false;
                colvarSellerOptions.DefaultSetting = @"";
                colvarSellerOptions.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerOptions);

                TableSchema.TableColumn colvarSort = new TableSchema.TableColumn(schema);
                colvarSort.ColumnName = "sort";
                colvarSort.DataType = DbType.Int32;
                colvarSort.MaxLength = 0;
                colvarSort.AutoIncrement = false;
                colvarSort.IsNullable = false;
                colvarSort.IsPrimaryKey = false;
                colvarSort.IsForeignKey = false;
                colvarSort.IsReadOnly = false;
                colvarSort.DefaultSetting = @"";
                colvarSort.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSort);

                TableSchema.TableColumn colvarFreightsId = new TableSchema.TableColumn(schema);
                colvarFreightsId.ColumnName = "freights_id";
                colvarFreightsId.DataType = DbType.Int32;
                colvarFreightsId.MaxLength = 0;
                colvarFreightsId.AutoIncrement = false;
                colvarFreightsId.IsNullable = true;
                colvarFreightsId.IsPrimaryKey = false;
                colvarFreightsId.IsForeignKey = false;
                colvarFreightsId.IsReadOnly = false;
                colvarFreightsId.DefaultSetting = @"";
                colvarFreightsId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFreightsId);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 250;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 50;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarUnit = new TableSchema.TableColumn(schema);
                colvarUnit.ColumnName = "unit";
                colvarUnit.DataType = DbType.String;
                colvarUnit.MaxLength = 50;
                colvarUnit.AutoIncrement = false;
                colvarUnit.IsNullable = true;
                colvarUnit.IsPrimaryKey = false;
                colvarUnit.IsForeignKey = false;
                colvarUnit.IsReadOnly = false;
                colvarUnit.DefaultSetting = @"";
                colvarUnit.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUnit);

                TableSchema.TableColumn colvarQuantityMultiplier = new TableSchema.TableColumn(schema);
                colvarQuantityMultiplier.ColumnName = "quantity_multiplier";
                colvarQuantityMultiplier.DataType = DbType.String;
                colvarQuantityMultiplier.MaxLength = 50;
                colvarQuantityMultiplier.AutoIncrement = false;
                colvarQuantityMultiplier.IsNullable = true;
                colvarQuantityMultiplier.IsPrimaryKey = false;
                colvarQuantityMultiplier.IsForeignKey = false;
                colvarQuantityMultiplier.IsReadOnly = false;
                colvarQuantityMultiplier.DefaultSetting = @"";
                colvarQuantityMultiplier.ForeignKeyTableName = "";
                schema.Columns.Add(colvarQuantityMultiplier);

                TableSchema.TableColumn colvarGifts = new TableSchema.TableColumn(schema);
                colvarGifts.ColumnName = "gifts";
                colvarGifts.DataType = DbType.String;
                colvarGifts.MaxLength = 1000;
                colvarGifts.AutoIncrement = false;
                colvarGifts.IsNullable = true;
                colvarGifts.IsPrimaryKey = false;
                colvarGifts.IsForeignKey = false;
                colvarGifts.IsReadOnly = false;
                colvarGifts.DefaultSetting = @"";
                colvarGifts.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGifts);

                TableSchema.TableColumn colvarComboPackCount = new TableSchema.TableColumn(schema);
                colvarComboPackCount.ColumnName = "combo_pack_count";
                colvarComboPackCount.DataType = DbType.AnsiString;
                colvarComboPackCount.MaxLength = 4;
                colvarComboPackCount.AutoIncrement = false;
                colvarComboPackCount.IsNullable = true;
                colvarComboPackCount.IsPrimaryKey = false;
                colvarComboPackCount.IsForeignKey = false;
                colvarComboPackCount.IsReadOnly = false;
                colvarComboPackCount.DefaultSetting = @"";
                colvarComboPackCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarComboPackCount);

                TableSchema.TableColumn colvarBoxQuantityLimit = new TableSchema.TableColumn(schema);
                colvarBoxQuantityLimit.ColumnName = "box_quantity_limit";
                colvarBoxQuantityLimit.DataType = DbType.AnsiString;
                colvarBoxQuantityLimit.MaxLength = 4;
                colvarBoxQuantityLimit.AutoIncrement = false;
                colvarBoxQuantityLimit.IsNullable = true;
                colvarBoxQuantityLimit.IsPrimaryKey = false;
                colvarBoxQuantityLimit.IsForeignKey = false;
                colvarBoxQuantityLimit.IsReadOnly = false;
                colvarBoxQuantityLimit.DefaultSetting = @"";
                colvarBoxQuantityLimit.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBoxQuantityLimit);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                colvarStatus.DefaultSetting = @"((0))";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("proposal_multi_deals",schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Pid")]
        [Bindable(true)]
        public int Pid
        {
            get { return GetColumnValue<int>(Columns.Pid); }
            set { SetColumnValue(Columns.Pid, value); }
        }

        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid? Bid
        {
            get { return GetColumnValue<Guid?>(Columns.Bid); }
            set { SetColumnValue(Columns.Bid, value); }
        }

        [XmlAttribute("BrandName")]
        [Bindable(true)]
        public string BrandName
        {
            get { return GetColumnValue<string>(Columns.BrandName); }
            set { SetColumnValue(Columns.BrandName, value); }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get { return GetColumnValue<string>(Columns.ItemName); }
            set { SetColumnValue(Columns.ItemName, value); }
        }

        [XmlAttribute("ItemNameTravel")]
        [Bindable(true)]
        public string ItemNameTravel
        {
            get { return GetColumnValue<string>(Columns.ItemNameTravel); }
            set { SetColumnValue(Columns.ItemNameTravel, value); }
        }

        [XmlAttribute("Content")]
        [Bindable(true)]
        public string Content
        {
            get { return GetColumnValue<string>(Columns.Content); }
            set { SetColumnValue(Columns.Content, value); }
        }

        [XmlAttribute("OrigPrice")]
        [Bindable(true)]
        public decimal OrigPrice
        {
            get { return GetColumnValue<decimal>(Columns.OrigPrice); }
            set { SetColumnValue(Columns.OrigPrice, value); }
        }

        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice
        {
            get { return GetColumnValue<decimal>(Columns.ItemPrice); }
            set { SetColumnValue(Columns.ItemPrice, value); }
        }

        [XmlAttribute("Cost")]
        [Bindable(true)]
        public decimal Cost
        {
            get { return GetColumnValue<decimal>(Columns.Cost); }
            set { SetColumnValue(Columns.Cost, value); }
        }

        [XmlAttribute("GroupCouponBuy")]
        [Bindable(true)]
        public int GroupCouponBuy
        {
            get { return GetColumnValue<int>(Columns.GroupCouponBuy); }
            set { SetColumnValue(Columns.GroupCouponBuy, value); }
        }

        [XmlAttribute("GroupCouponPresent")]
        [Bindable(true)]
        public int GroupCouponPresent
        {
            get { return GetColumnValue<int>(Columns.GroupCouponPresent); }
            set { SetColumnValue(Columns.GroupCouponPresent, value); }
        }

        [XmlAttribute("SlottingFeeQuantity")]
        [Bindable(true)]
        public int SlottingFeeQuantity
        {
            get { return GetColumnValue<int>(Columns.SlottingFeeQuantity); }
            set { SetColumnValue(Columns.SlottingFeeQuantity, value); }
        }

        [XmlAttribute("OrderMaxPersonal")]
        [Bindable(true)]
        public int OrderMaxPersonal
        {
            get { return GetColumnValue<int>(Columns.OrderMaxPersonal); }
            set { SetColumnValue(Columns.OrderMaxPersonal, value); }
        }

        [XmlAttribute("OrderTotalLimit")]
        [Bindable(true)]
        public int OrderTotalLimit
        {
            get { return GetColumnValue<int>(Columns.OrderTotalLimit); }
            set { SetColumnValue(Columns.OrderTotalLimit, value); }
        }

        [XmlAttribute("AtmMaximum")]
        [Bindable(true)]
        public int AtmMaximum
        {
            get { return GetColumnValue<int>(Columns.AtmMaximum); }
            set { SetColumnValue(Columns.AtmMaximum, value); }
        }

        [XmlAttribute("Freights")]
        [Bindable(true)]
        public decimal Freights
        {
            get { return GetColumnValue<decimal>(Columns.Freights); }
            set { SetColumnValue(Columns.Freights, value); }
        }

        [XmlAttribute("NoFreightLimit")]
        [Bindable(true)]
        public int NoFreightLimit
        {
            get { return GetColumnValue<int>(Columns.NoFreightLimit); }
            set { SetColumnValue(Columns.NoFreightLimit, value); }
        }

        [XmlAttribute("Options")]
        [Bindable(true)]
        public string Options
        {
            get { return GetColumnValue<string>(Columns.Options); }
            set { SetColumnValue(Columns.Options, value); }
        }

        [XmlAttribute("SellerOptions")]
        [Bindable(true)]
        public string SellerOptions
        {
            get { return GetColumnValue<string>(Columns.SellerOptions); }
            set { SetColumnValue(Columns.SellerOptions, value); }
        }

        [XmlAttribute("Sort")]
        [Bindable(true)]
        public int Sort
        {
            get { return GetColumnValue<int>(Columns.Sort); }
            set { SetColumnValue(Columns.Sort, value); }
        }

        [XmlAttribute("FreightsId")]
        [Bindable(true)]
        public int? FreightsId
        {
            get { return GetColumnValue<int?>(Columns.FreightsId); }
            set { SetColumnValue(Columns.FreightsId, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("Unit")]
        [Bindable(true)]
        public string Unit
        {
            get { return GetColumnValue<string>(Columns.Unit); }
            set { SetColumnValue(Columns.Unit, value); }
        }

        [XmlAttribute("QuantityMultiplier")]
        [Bindable(true)]
        public string QuantityMultiplier
        {
            get { return GetColumnValue<string>(Columns.QuantityMultiplier); }
            set { SetColumnValue(Columns.QuantityMultiplier, value); }
        }

        [XmlAttribute("Gifts")]
        [Bindable(true)]
        public string Gifts
        {
            get { return GetColumnValue<string>(Columns.Gifts); }
            set { SetColumnValue(Columns.Gifts, value); }
        }

        [XmlAttribute("ComboPackCount")]
        [Bindable(true)]
        public string ComboPackCount
        {
            get { return GetColumnValue<string>(Columns.ComboPackCount); }
            set { SetColumnValue(Columns.ComboPackCount, value); }
        }

        [XmlAttribute("BoxQuantityLimit")]
        [Bindable(true)]
        public string BoxQuantityLimit
        {
            get { return GetColumnValue<string>(Columns.BoxQuantityLimit); }
            set { SetColumnValue(Columns.BoxQuantityLimit, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn PidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn BrandNameColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ItemNameTravelColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ContentColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn OrigPriceColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ItemPriceColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CostColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn GroupCouponBuyColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn GroupCouponPresentColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn SlottingFeeQuantityColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn OrderMaxPersonalColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn OrderTotalLimitColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn AtmMaximumColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn FreightsColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn NoFreightLimitColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn OptionsColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn SellerOptionsColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn SortColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn FreightsIdColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[22]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[23]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[24]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[25]; }
        }



        public static TableSchema.TableColumn UnitColumn
        {
            get { return Schema.Columns[26]; }
        }



        public static TableSchema.TableColumn QuantityMultiplierColumn
        {
            get { return Schema.Columns[27]; }
        }



        public static TableSchema.TableColumn GiftsColumn
        {
            get { return Schema.Columns[28]; }
        }



        public static TableSchema.TableColumn ComboPackCountColumn
        {
            get { return Schema.Columns[29]; }
        }



        public static TableSchema.TableColumn BoxQuantityLimitColumn
        {
            get { return Schema.Columns[30]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[31]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Pid = @"pid";
            public static string Bid = @"bid";
            public static string BrandName = @"brand_name";
            public static string ItemName = @"item_name";
            public static string ItemNameTravel = @"item_name_travel";
            public static string Content = @"content";
            public static string OrigPrice = @"orig_price";
            public static string ItemPrice = @"item_price";
            public static string Cost = @"cost";
            public static string GroupCouponBuy = @"group_coupon_buy";
            public static string GroupCouponPresent = @"group_coupon_present";
            public static string SlottingFeeQuantity = @"slotting_fee_quantity";
            public static string OrderMaxPersonal = @"order_max_personal";
            public static string OrderTotalLimit = @"order_total_limit";
            public static string AtmMaximum = @"atm_maximum";
            public static string Freights = @"freights";
            public static string NoFreightLimit = @"no_freight_limit";
            public static string Options = @"options";
            public static string SellerOptions = @"seller_options";
            public static string Sort = @"sort";
            public static string FreightsId = @"freights_id";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string Unit = @"unit";
            public static string QuantityMultiplier = @"quantity_multiplier";
            public static string Gifts = @"gifts";
            public static string ComboPackCount = @"combo_pack_count";
            public static string BoxQuantityLimit = @"box_quantity_limit";
            public static string Status = @"status";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the CustomerServiceCategorySample class.
    /// </summary>
    [Serializable]
    public partial class CustomerServiceCategorySampleCollection : RepositoryList<CustomerServiceCategorySample, CustomerServiceCategorySampleCollection>
    {
        public CustomerServiceCategorySampleCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CustomerServiceCategorySampleCollection</returns>
        public CustomerServiceCategorySampleCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CustomerServiceCategorySample o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }


    /// <summary>
    /// This is an ActiveRecord class which wraps the customer_service_category_sample table.
    /// </summary>

    [Serializable]
    public partial class CustomerServiceCategorySample : RepositoryRecord<CustomerServiceCategorySample>, IRecordBase
    {
        #region .ctors and Default Settings

        public CustomerServiceCategorySample()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public CustomerServiceCategorySample(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("customer_service_category_sample", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
                colvarCategoryId.ColumnName = "category_id";
                colvarCategoryId.DataType = DbType.Int32;
                colvarCategoryId.MaxLength = 0;
                colvarCategoryId.AutoIncrement = false;
                colvarCategoryId.IsNullable = false;
                colvarCategoryId.IsPrimaryKey = false;
                colvarCategoryId.IsForeignKey = false;
                colvarCategoryId.IsReadOnly = false;
                colvarCategoryId.DefaultSetting = @"";
                colvarCategoryId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCategoryId);

                TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
                colvarContent.ColumnName = "content";
                colvarContent.DataType = DbType.String;
                colvarContent.MaxLength = -1;
                colvarContent.AutoIncrement = false;
                colvarContent.IsNullable = true;
                colvarContent.IsPrimaryKey = false;
                colvarContent.IsForeignKey = false;
                colvarContent.IsReadOnly = false;
                colvarContent.DefaultSetting = @"";
                colvarContent.ForeignKeyTableName = "";
                schema.Columns.Add(colvarContent);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.Int32;
                colvarModifyId.MaxLength = 0;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarSubject = new TableSchema.TableColumn(schema);
                colvarSubject.ColumnName = "subject";
                colvarSubject.DataType = DbType.String;
                colvarSubject.MaxLength = 50;
                colvarSubject.AutoIncrement = false;
                colvarSubject.IsNullable = true;
                colvarSubject.IsPrimaryKey = false;
                colvarSubject.IsForeignKey = false;
                colvarSubject.IsReadOnly = false;
                colvarSubject.DefaultSetting = @"";
                colvarSubject.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSubject);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("customer_service_category_sample", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("CategoryId")]
        [Bindable(true)]
        public int CategoryId
        {
            get { return GetColumnValue<int>(Columns.CategoryId); }
            set { SetColumnValue(Columns.CategoryId, value); }
        }

        [XmlAttribute("Content")]
        [Bindable(true)]
        public string Content
        {
            get { return GetColumnValue<string>(Columns.Content); }
            set { SetColumnValue(Columns.Content, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId
        {
            get { return GetColumnValue<int>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public int? ModifyId
        {
            get { return GetColumnValue<int?>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("Subject")]
        [Bindable(true)]
        public string Subject
        {
            get { return GetColumnValue<string>(Columns.Subject); }
            set { SetColumnValue(Columns.Subject, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn CategoryIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ContentColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn SubjectColumn
        {
            get { return Schema.Columns[7]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string CategoryId = @"category_id";
            public static string Content = @"content";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string Subject = @"subject";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

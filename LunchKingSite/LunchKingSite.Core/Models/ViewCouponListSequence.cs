using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewCouponListSequence class.
    /// </summary>
    [Serializable]
    public partial class ViewCouponListSequenceCollection : ReadOnlyList<ViewCouponListSequence, ViewCouponListSequenceCollection>
    {
        public ViewCouponListSequenceCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_coupon_list_sequence view.
    /// </summary>
    [Serializable]
    public partial class ViewCouponListSequence : ReadOnlyRecord<ViewCouponListSequence>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_coupon_list_sequence", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarBankStatus = new TableSchema.TableColumn(schema);
                colvarBankStatus.ColumnName = "bank_status";
                colvarBankStatus.DataType = DbType.Int32;
                colvarBankStatus.MaxLength = 0;
                colvarBankStatus.AutoIncrement = false;
                colvarBankStatus.IsNullable = false;
                colvarBankStatus.IsPrimaryKey = false;
                colvarBankStatus.IsForeignKey = false;
                colvarBankStatus.IsReadOnly = false;

                schema.Columns.Add(colvarBankStatus);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
                colvarSequenceNumber.ColumnName = "sequence_number";
                colvarSequenceNumber.DataType = DbType.AnsiString;
                colvarSequenceNumber.MaxLength = 50;
                colvarSequenceNumber.AutoIncrement = false;
                colvarSequenceNumber.IsNullable = false;
                colvarSequenceNumber.IsPrimaryKey = false;
                colvarSequenceNumber.IsForeignKey = false;
                colvarSequenceNumber.IsReadOnly = false;

                schema.Columns.Add(colvarSequenceNumber);

                TableSchema.TableColumn colvarAvailable = new TableSchema.TableColumn(schema);
                colvarAvailable.ColumnName = "available";
                colvarAvailable.DataType = DbType.Boolean;
                colvarAvailable.MaxLength = 0;
                colvarAvailable.AutoIncrement = false;
                colvarAvailable.IsNullable = true;
                colvarAvailable.IsPrimaryKey = false;
                colvarAvailable.IsForeignKey = false;
                colvarAvailable.IsReadOnly = false;

                schema.Columns.Add(colvarAvailable);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;

                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 4000;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;

                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarOrderDetailCreateTime = new TableSchema.TableColumn(schema);
                colvarOrderDetailCreateTime.ColumnName = "order_detail_create_time";
                colvarOrderDetailCreateTime.DataType = DbType.DateTime;
                colvarOrderDetailCreateTime.MaxLength = 0;
                colvarOrderDetailCreateTime.AutoIncrement = false;
                colvarOrderDetailCreateTime.IsNullable = false;
                colvarOrderDetailCreateTime.IsPrimaryKey = false;
                colvarOrderDetailCreateTime.IsForeignKey = false;
                colvarOrderDetailCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarOrderDetailCreateTime);

                TableSchema.TableColumn colvarSpecialStatus = new TableSchema.TableColumn(schema);
                colvarSpecialStatus.ColumnName = "special_status";
                colvarSpecialStatus.DataType = DbType.Int32;
                colvarSpecialStatus.MaxLength = 0;
                colvarSpecialStatus.AutoIncrement = false;
                colvarSpecialStatus.IsNullable = true;
                colvarSpecialStatus.IsPrimaryKey = false;
                colvarSpecialStatus.IsForeignKey = false;
                colvarSpecialStatus.IsReadOnly = false;

                schema.Columns.Add(colvarSpecialStatus);

                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 50;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = false;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;

                schema.Columns.Add(colvarCode);

                TableSchema.TableColumn colvarStoreSequence = new TableSchema.TableColumn(schema);
                colvarStoreSequence.ColumnName = "store_sequence";
                colvarStoreSequence.DataType = DbType.Int32;
                colvarStoreSequence.MaxLength = 0;
                colvarStoreSequence.AutoIncrement = false;
                colvarStoreSequence.IsNullable = true;
                colvarStoreSequence.IsPrimaryKey = false;
                colvarStoreSequence.IsForeignKey = false;
                colvarStoreSequence.IsReadOnly = false;

                schema.Columns.Add(colvarStoreSequence);

                TableSchema.TableColumn colvarIsUsed = new TableSchema.TableColumn(schema);
                colvarIsUsed.ColumnName = "is_used";
                colvarIsUsed.DataType = DbType.Boolean;
                colvarIsUsed.MaxLength = 0;
                colvarIsUsed.AutoIncrement = false;
                colvarIsUsed.IsNullable = true;
                colvarIsUsed.IsPrimaryKey = false;
                colvarIsUsed.IsForeignKey = false;
                colvarIsUsed.IsReadOnly = false;

                schema.Columns.Add(colvarIsUsed);

                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;

                schema.Columns.Add(colvarStoreName);

                TableSchema.TableColumn colvarStoreCount = new TableSchema.TableColumn(schema);
                colvarStoreCount.ColumnName = "store_count";
                colvarStoreCount.DataType = DbType.Int32;
                colvarStoreCount.MaxLength = 0;
                colvarStoreCount.AutoIncrement = false;
                colvarStoreCount.IsNullable = true;
                colvarStoreCount.IsPrimaryKey = false;
                colvarStoreCount.IsForeignKey = false;
                colvarStoreCount.IsReadOnly = false;

                schema.Columns.Add(colvarStoreCount);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;

                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarCouponStatus = new TableSchema.TableColumn(schema);
                colvarCouponStatus.ColumnName = "coupon_status";
                colvarCouponStatus.DataType = DbType.Int32;
                colvarCouponStatus.MaxLength = 0;
                colvarCouponStatus.AutoIncrement = false;
                colvarCouponStatus.IsNullable = false;
                colvarCouponStatus.IsPrimaryKey = false;
                colvarCouponStatus.IsForeignKey = false;
                colvarCouponStatus.IsReadOnly = false;

                schema.Columns.Add(colvarCouponStatus);

                TableSchema.TableColumn colvarUsageVerifiedTime = new TableSchema.TableColumn(schema);
                colvarUsageVerifiedTime.ColumnName = "usage_verified_time";
                colvarUsageVerifiedTime.DataType = DbType.DateTime;
                colvarUsageVerifiedTime.MaxLength = 0;
                colvarUsageVerifiedTime.AutoIncrement = false;
                colvarUsageVerifiedTime.IsNullable = true;
                colvarUsageVerifiedTime.IsPrimaryKey = false;
                colvarUsageVerifiedTime.IsForeignKey = false;
                colvarUsageVerifiedTime.IsReadOnly = false;

                schema.Columns.Add(colvarUsageVerifiedTime);

                TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
                colvarOrderClassification.ColumnName = "order_classification";
                colvarOrderClassification.DataType = DbType.Int32;
                colvarOrderClassification.MaxLength = 0;
                colvarOrderClassification.AutoIncrement = false;
                colvarOrderClassification.IsNullable = true;
                colvarOrderClassification.IsPrimaryKey = false;
                colvarOrderClassification.IsForeignKey = false;
                colvarOrderClassification.IsReadOnly = false;

                schema.Columns.Add(colvarOrderClassification);

                TableSchema.TableColumn colvarTrustSequenceNumber = new TableSchema.TableColumn(schema);
                colvarTrustSequenceNumber.ColumnName = "trust_sequence_number";
                colvarTrustSequenceNumber.DataType = DbType.String;
                colvarTrustSequenceNumber.MaxLength = 50;
                colvarTrustSequenceNumber.AutoIncrement = false;
                colvarTrustSequenceNumber.IsNullable = true;
                colvarTrustSequenceNumber.IsPrimaryKey = false;
                colvarTrustSequenceNumber.IsForeignKey = false;
                colvarTrustSequenceNumber.IsReadOnly = false;

                schema.Columns.Add(colvarTrustSequenceNumber);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Int32;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = true;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;

                schema.Columns.Add(colvarAmount);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_coupon_list_sequence",schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewCouponListSequence()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewCouponListSequence(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewCouponListSequence(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewCouponListSequence(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("BankStatus")]
        [Bindable(true)]
        public int BankStatus
        {
            get
            {
                return GetColumnValue<int>("bank_status");
            }
            set
            {
                SetColumnValue("bank_status", value);
            }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get
            {
                return GetColumnValue<int>("status");
            }
            set
            {
                SetColumnValue("status", value);
            }
        }

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("SequenceNumber")]
        [Bindable(true)]
        public string SequenceNumber
        {
            get
            {
                return GetColumnValue<string>("sequence_number");
            }
            set
            {
                SetColumnValue("sequence_number", value);
            }
        }

        [XmlAttribute("Available")]
        [Bindable(true)]
        public bool? Available
        {
            get
            {
                return GetColumnValue<bool?>("available");
            }
            set
            {
                SetColumnValue("available", value);
            }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get
            {
                return GetColumnValue<Guid>("GUID");
            }
            set
            {
                SetColumnValue("GUID", value);
            }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get
            {
                return GetColumnValue<string>("item_name");
            }
            set
            {
                SetColumnValue("item_name", value);
            }
        }

        [XmlAttribute("OrderDetailCreateTime")]
        [Bindable(true)]
        public DateTime OrderDetailCreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("order_detail_create_time");
            }
            set
            {
                SetColumnValue("order_detail_create_time", value);
            }
        }

        [XmlAttribute("SpecialStatus")]
        [Bindable(true)]
        public int? SpecialStatus
        {
            get
            {
                return GetColumnValue<int?>("special_status");
            }
            set
            {
                SetColumnValue("special_status", value);
            }
        }

        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code
        {
            get
            {
                return GetColumnValue<string>("code");
            }
            set
            {
                SetColumnValue("code", value);
            }
        }

        [XmlAttribute("StoreSequence")]
        [Bindable(true)]
        public int? StoreSequence
        {
            get
            {
                return GetColumnValue<int?>("store_sequence");
            }
            set
            {
                SetColumnValue("store_sequence", value);
            }
        }

        [XmlAttribute("IsUsed")]
        [Bindable(true)]
        public bool? IsUsed
        {
            get
            {
                return GetColumnValue<bool?>("is_used");
            }
            set
            {
                SetColumnValue("is_used", value);
            }
        }

        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName
        {
            get
            {
                return GetColumnValue<string>("store_name");
            }
            set
            {
                SetColumnValue("store_name", value);
            }
        }

        [XmlAttribute("StoreCount")]
        [Bindable(true)]
        public int? StoreCount
        {
            get
            {
                return GetColumnValue<int?>("store_count");
            }
            set
            {
                SetColumnValue("store_count", value);
            }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get
            {
                return GetColumnValue<DateTime?>("modify_time");
            }
            set
            {
                SetColumnValue("modify_time", value);
            }
        }

        [XmlAttribute("CouponStatus")]
        [Bindable(true)]
        public int CouponStatus
        {
            get
            {
                return GetColumnValue<int>("coupon_status");
            }
            set
            {
                SetColumnValue("coupon_status", value);
            }
        }

        [XmlAttribute("UsageVerifiedTime")]
        [Bindable(true)]
        public DateTime? UsageVerifiedTime
        {
            get
            {
                return GetColumnValue<DateTime?>("usage_verified_time");
            }
            set
            {
                SetColumnValue("usage_verified_time", value);
            }
        }

        [XmlAttribute("OrderClassification")]
        [Bindable(true)]
        public int? OrderClassification
        {
            get
            {
                return GetColumnValue<int?>("order_classification");
            }
            set
            {
                SetColumnValue("order_classification", value);
            }
        }

        [XmlAttribute("TrustSequenceNumber")]
        [Bindable(true)]
        public string TrustSequenceNumber
        {
            get
            {
                return GetColumnValue<string>("trust_sequence_number");
            }
            set
            {
                SetColumnValue("trust_sequence_number", value);
            }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public int? Amount
        {
            get
            {
                return GetColumnValue<int?>("amount");
            }
            set
            {
                SetColumnValue("amount", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string BankStatus = @"bank_status";

            public static string Status = @"status";

            public static string Id = @"id";

            public static string SequenceNumber = @"sequence_number";

            public static string Available = @"available";

            public static string Guid = @"GUID";

            public static string ItemName = @"item_name";

            public static string OrderDetailCreateTime = @"order_detail_create_time";

            public static string SpecialStatus = @"special_status";

            public static string Code = @"code";

            public static string StoreSequence = @"store_sequence";

            public static string IsUsed = @"is_used";

            public static string StoreName = @"store_name";

            public static string StoreCount = @"store_count";

            public static string ModifyTime = @"modify_time";

            public static string CouponStatus = @"coupon_status";

            public static string UsageVerifiedTime = @"usage_verified_time";

            public static string OrderClassification = @"order_classification";

            public static string TrustSequenceNumber = @"trust_sequence_number";

            public static string Amount = @"amount";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

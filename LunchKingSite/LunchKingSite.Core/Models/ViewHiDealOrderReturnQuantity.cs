using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealOrderReturnQuantity class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealOrderReturnQuantityCollection : ReadOnlyList<ViewHiDealOrderReturnQuantity, ViewHiDealOrderReturnQuantityCollection>
    {        
        public ViewHiDealOrderReturnQuantityCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_order_return_quantity view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealOrderReturnQuantity : ReadOnlyRecord<ViewHiDealOrderReturnQuantity>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_order_return_quantity", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarHiDealId = new TableSchema.TableColumn(schema);
                colvarHiDealId.ColumnName = "hi_deal_id";
                colvarHiDealId.DataType = DbType.Int32;
                colvarHiDealId.MaxLength = 0;
                colvarHiDealId.AutoIncrement = false;
                colvarHiDealId.IsNullable = false;
                colvarHiDealId.IsPrimaryKey = false;
                colvarHiDealId.IsForeignKey = false;
                colvarHiDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealId);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 50;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = true;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "product_name";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 250;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = true;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductName);
                
                TableSchema.TableColumn colvarOrderQty = new TableSchema.TableColumn(schema);
                colvarOrderQty.ColumnName = "orderQty";
                colvarOrderQty.DataType = DbType.Int32;
                colvarOrderQty.MaxLength = 0;
                colvarOrderQty.AutoIncrement = false;
                colvarOrderQty.IsNullable = true;
                colvarOrderQty.IsPrimaryKey = false;
                colvarOrderQty.IsForeignKey = false;
                colvarOrderQty.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderQty);
                
                TableSchema.TableColumn colvarOrderprice = new TableSchema.TableColumn(schema);
                colvarOrderprice.ColumnName = "orderprice";
                colvarOrderprice.DataType = DbType.Currency;
                colvarOrderprice.MaxLength = 0;
                colvarOrderprice.AutoIncrement = false;
                colvarOrderprice.IsNullable = true;
                colvarOrderprice.IsPrimaryKey = false;
                colvarOrderprice.IsForeignKey = false;
                colvarOrderprice.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderprice);
                
                TableSchema.TableColumn colvarOrderTotalPrice = new TableSchema.TableColumn(schema);
                colvarOrderTotalPrice.ColumnName = "orderTotalPrice";
                colvarOrderTotalPrice.DataType = DbType.Currency;
                colvarOrderTotalPrice.MaxLength = 0;
                colvarOrderTotalPrice.AutoIncrement = false;
                colvarOrderTotalPrice.IsNullable = true;
                colvarOrderTotalPrice.IsPrimaryKey = false;
                colvarOrderTotalPrice.IsForeignKey = false;
                colvarOrderTotalPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTotalPrice);
                
                TableSchema.TableColumn colvarReturnQty = new TableSchema.TableColumn(schema);
                colvarReturnQty.ColumnName = "returnQty";
                colvarReturnQty.DataType = DbType.Int32;
                colvarReturnQty.MaxLength = 0;
                colvarReturnQty.AutoIncrement = false;
                colvarReturnQty.IsNullable = false;
                colvarReturnQty.IsPrimaryKey = false;
                colvarReturnQty.IsForeignKey = false;
                colvarReturnQty.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnQty);
                
                TableSchema.TableColumn colvarReturnPrice = new TableSchema.TableColumn(schema);
                colvarReturnPrice.ColumnName = "returnPrice";
                colvarReturnPrice.DataType = DbType.Currency;
                colvarReturnPrice.MaxLength = 0;
                colvarReturnPrice.AutoIncrement = false;
                colvarReturnPrice.IsNullable = false;
                colvarReturnPrice.IsPrimaryKey = false;
                colvarReturnPrice.IsForeignKey = false;
                colvarReturnPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnPrice);
                
                TableSchema.TableColumn colvarReturnTotalPrice = new TableSchema.TableColumn(schema);
                colvarReturnTotalPrice.ColumnName = "returnTotalPrice";
                colvarReturnTotalPrice.DataType = DbType.Currency;
                colvarReturnTotalPrice.MaxLength = 0;
                colvarReturnTotalPrice.AutoIncrement = false;
                colvarReturnTotalPrice.IsNullable = false;
                colvarReturnTotalPrice.IsPrimaryKey = false;
                colvarReturnTotalPrice.IsForeignKey = false;
                colvarReturnTotalPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnTotalPrice);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_order_return_quantity",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealOrderReturnQuantity()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealOrderReturnQuantity(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealOrderReturnQuantity(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealOrderReturnQuantity(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("HiDealId")]
        [Bindable(true)]
        public int HiDealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("hi_deal_id");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_id", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int? ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_name");
		    }
            set 
		    {
			    SetColumnValue("product_name", value);
            }
        }
	      
        [XmlAttribute("OrderQty")]
        [Bindable(true)]
        public int? OrderQty 
	    {
		    get
		    {
			    return GetColumnValue<int?>("orderQty");
		    }
            set 
		    {
			    SetColumnValue("orderQty", value);
            }
        }
	      
        [XmlAttribute("Orderprice")]
        [Bindable(true)]
        public decimal? Orderprice 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("orderprice");
		    }
            set 
		    {
			    SetColumnValue("orderprice", value);
            }
        }
	      
        [XmlAttribute("OrderTotalPrice")]
        [Bindable(true)]
        public decimal? OrderTotalPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("orderTotalPrice");
		    }
            set 
		    {
			    SetColumnValue("orderTotalPrice", value);
            }
        }
	      
        [XmlAttribute("ReturnQty")]
        [Bindable(true)]
        public int ReturnQty 
	    {
		    get
		    {
			    return GetColumnValue<int>("returnQty");
		    }
            set 
		    {
			    SetColumnValue("returnQty", value);
            }
        }
	      
        [XmlAttribute("ReturnPrice")]
        [Bindable(true)]
        public decimal ReturnPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("returnPrice");
		    }
            set 
		    {
			    SetColumnValue("returnPrice", value);
            }
        }
	      
        [XmlAttribute("ReturnTotalPrice")]
        [Bindable(true)]
        public decimal ReturnTotalPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("returnTotalPrice");
		    }
            set 
		    {
			    SetColumnValue("returnTotalPrice", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string HiDealId = @"hi_deal_id";
            
            public static string Name = @"name";
            
            public static string ProductId = @"product_id";
            
            public static string ProductName = @"product_name";
            
            public static string OrderQty = @"orderQty";
            
            public static string Orderprice = @"orderprice";
            
            public static string OrderTotalPrice = @"orderTotalPrice";
            
            public static string ReturnQty = @"returnQty";
            
            public static string ReturnPrice = @"returnPrice";
            
            public static string ReturnTotalPrice = @"returnTotalPrice";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

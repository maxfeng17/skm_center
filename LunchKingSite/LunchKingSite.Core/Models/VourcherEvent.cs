using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VourcherEvent class.
	/// </summary>
    [Serializable]
	public partial class VourcherEventCollection : RepositoryList<VourcherEvent, VourcherEventCollection>
	{	   
		public VourcherEventCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VourcherEventCollection</returns>
		public VourcherEventCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VourcherEvent o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the vourcher_event table.
	/// </summary>
	[Serializable]
	public partial class VourcherEvent : RepositoryRecord<VourcherEvent>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VourcherEvent()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VourcherEvent(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("vourcher_event", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
				colvarEventName.ColumnName = "event_name";
				colvarEventName.DataType = DbType.String;
				colvarEventName.MaxLength = 100;
				colvarEventName.AutoIncrement = false;
				colvarEventName.IsNullable = false;
				colvarEventName.IsPrimaryKey = false;
				colvarEventName.IsForeignKey = false;
				colvarEventName.IsReadOnly = false;
				colvarEventName.DefaultSetting = @"";
				colvarEventName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventName);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarContents = new TableSchema.TableColumn(schema);
				colvarContents.ColumnName = "contents";
				colvarContents.DataType = DbType.String;
				colvarContents.MaxLength = 1000;
				colvarContents.AutoIncrement = false;
				colvarContents.IsNullable = true;
				colvarContents.IsPrimaryKey = false;
				colvarContents.IsForeignKey = false;
				colvarContents.IsReadOnly = false;
				colvarContents.DefaultSetting = @"";
				colvarContents.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContents);
				
				TableSchema.TableColumn colvarInstruction = new TableSchema.TableColumn(schema);
				colvarInstruction.ColumnName = "instruction";
				colvarInstruction.DataType = DbType.String;
				colvarInstruction.MaxLength = 1000;
				colvarInstruction.AutoIncrement = false;
				colvarInstruction.IsNullable = true;
				colvarInstruction.IsPrimaryKey = false;
				colvarInstruction.IsForeignKey = false;
				colvarInstruction.IsReadOnly = false;
				colvarInstruction.DefaultSetting = @"";
				colvarInstruction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInstruction);
				
				TableSchema.TableColumn colvarPicUrl = new TableSchema.TableColumn(schema);
				colvarPicUrl.ColumnName = "pic_url";
				colvarPicUrl.DataType = DbType.AnsiString;
				colvarPicUrl.MaxLength = 500;
				colvarPicUrl.AutoIncrement = false;
				colvarPicUrl.IsNullable = true;
				colvarPicUrl.IsPrimaryKey = false;
				colvarPicUrl.IsForeignKey = false;
				colvarPicUrl.IsReadOnly = false;
				colvarPicUrl.DefaultSetting = @"";
				colvarPicUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPicUrl);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
				colvarStartDate.ColumnName = "start_date";
				colvarStartDate.DataType = DbType.DateTime;
				colvarStartDate.MaxLength = 0;
				colvarStartDate.AutoIncrement = false;
				colvarStartDate.IsNullable = true;
				colvarStartDate.IsPrimaryKey = false;
				colvarStartDate.IsForeignKey = false;
				colvarStartDate.IsReadOnly = false;
				colvarStartDate.DefaultSetting = @"";
				colvarStartDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartDate);
				
				TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
				colvarEndDate.ColumnName = "end_date";
				colvarEndDate.DataType = DbType.DateTime;
				colvarEndDate.MaxLength = 0;
				colvarEndDate.AutoIncrement = false;
				colvarEndDate.IsNullable = true;
				colvarEndDate.IsPrimaryKey = false;
				colvarEndDate.IsForeignKey = false;
				colvarEndDate.IsReadOnly = false;
				colvarEndDate.DefaultSetting = @"";
				colvarEndDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndDate);
				
				TableSchema.TableColumn colvarPageCount = new TableSchema.TableColumn(schema);
				colvarPageCount.ColumnName = "page_count";
				colvarPageCount.DataType = DbType.Int32;
				colvarPageCount.MaxLength = 0;
				colvarPageCount.AutoIncrement = false;
				colvarPageCount.IsNullable = false;
				colvarPageCount.IsPrimaryKey = false;
				colvarPageCount.IsForeignKey = false;
				colvarPageCount.IsReadOnly = false;
				
						colvarPageCount.DefaultSetting = @"((0))";
				colvarPageCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPageCount);
				
				TableSchema.TableColumn colvarMaxQuantity = new TableSchema.TableColumn(schema);
				colvarMaxQuantity.ColumnName = "max_quantity";
				colvarMaxQuantity.DataType = DbType.Int32;
				colvarMaxQuantity.MaxLength = 0;
				colvarMaxQuantity.AutoIncrement = false;
				colvarMaxQuantity.IsNullable = false;
				colvarMaxQuantity.IsPrimaryKey = false;
				colvarMaxQuantity.IsForeignKey = false;
				colvarMaxQuantity.IsReadOnly = false;
				
						colvarMaxQuantity.DefaultSetting = @"((0))";
				colvarMaxQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMaxQuantity);
				
				TableSchema.TableColumn colvarCurrentQuantity = new TableSchema.TableColumn(schema);
				colvarCurrentQuantity.ColumnName = "current_quantity";
				colvarCurrentQuantity.DataType = DbType.Int32;
				colvarCurrentQuantity.MaxLength = 0;
				colvarCurrentQuantity.AutoIncrement = false;
				colvarCurrentQuantity.IsNullable = false;
				colvarCurrentQuantity.IsPrimaryKey = false;
				colvarCurrentQuantity.IsForeignKey = false;
				colvarCurrentQuantity.IsReadOnly = false;
				
						colvarCurrentQuantity.DefaultSetting = @"((0))";
				colvarCurrentQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCurrentQuantity);
				
				TableSchema.TableColumn colvarRatio = new TableSchema.TableColumn(schema);
				colvarRatio.ColumnName = "ratio";
				colvarRatio.DataType = DbType.Int32;
				colvarRatio.MaxLength = 0;
				colvarRatio.AutoIncrement = false;
				colvarRatio.IsNullable = false;
				colvarRatio.IsPrimaryKey = false;
				colvarRatio.IsForeignKey = false;
				colvarRatio.IsReadOnly = false;
				
						colvarRatio.DefaultSetting = @"((0))";
				colvarRatio.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRatio);
				
				TableSchema.TableColumn colvarEnable = new TableSchema.TableColumn(schema);
				colvarEnable.ColumnName = "enable";
				colvarEnable.DataType = DbType.Boolean;
				colvarEnable.MaxLength = 0;
				colvarEnable.AutoIncrement = false;
				colvarEnable.IsNullable = false;
				colvarEnable.IsPrimaryKey = false;
				colvarEnable.IsForeignKey = false;
				colvarEnable.IsReadOnly = false;
				
						colvarEnable.DefaultSetting = @"((1))";
				colvarEnable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnable);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 50;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 1073741823;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarMode = new TableSchema.TableColumn(schema);
				colvarMode.ColumnName = "mode";
				colvarMode.DataType = DbType.Int32;
				colvarMode.MaxLength = 0;
				colvarMode.AutoIncrement = false;
				colvarMode.IsNullable = false;
				colvarMode.IsPrimaryKey = false;
				colvarMode.IsForeignKey = false;
				colvarMode.IsReadOnly = false;
				
						colvarMode.DefaultSetting = @"((0))";
				colvarMode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMode);
				
				TableSchema.TableColumn colvarMagazine = new TableSchema.TableColumn(schema);
				colvarMagazine.ColumnName = "magazine";
				colvarMagazine.DataType = DbType.Boolean;
				colvarMagazine.MaxLength = 0;
				colvarMagazine.AutoIncrement = false;
				colvarMagazine.IsNullable = true;
				colvarMagazine.IsPrimaryKey = false;
				colvarMagazine.IsForeignKey = false;
				colvarMagazine.IsReadOnly = false;
				colvarMagazine.DefaultSetting = @"";
				colvarMagazine.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMagazine);
				
				TableSchema.TableColumn colvarDiscount = new TableSchema.TableColumn(schema);
				colvarDiscount.ColumnName = "discount";
				colvarDiscount.DataType = DbType.Decimal;
				colvarDiscount.MaxLength = 0;
				colvarDiscount.AutoIncrement = false;
				colvarDiscount.IsNullable = true;
				colvarDiscount.IsPrimaryKey = false;
				colvarDiscount.IsForeignKey = false;
				colvarDiscount.IsReadOnly = false;
				colvarDiscount.DefaultSetting = @"";
				colvarDiscount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscount);
				
				TableSchema.TableColumn colvarOriginalPrice = new TableSchema.TableColumn(schema);
				colvarOriginalPrice.ColumnName = "original_price";
				colvarOriginalPrice.DataType = DbType.Int32;
				colvarOriginalPrice.MaxLength = 0;
				colvarOriginalPrice.AutoIncrement = false;
				colvarOriginalPrice.IsNullable = true;
				colvarOriginalPrice.IsPrimaryKey = false;
				colvarOriginalPrice.IsForeignKey = false;
				colvarOriginalPrice.IsReadOnly = false;
				colvarOriginalPrice.DefaultSetting = @"";
				colvarOriginalPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOriginalPrice);
				
				TableSchema.TableColumn colvarDiscountPrice = new TableSchema.TableColumn(schema);
				colvarDiscountPrice.ColumnName = "discount_price";
				colvarDiscountPrice.DataType = DbType.Int32;
				colvarDiscountPrice.MaxLength = 0;
				colvarDiscountPrice.AutoIncrement = false;
				colvarDiscountPrice.IsNullable = true;
				colvarDiscountPrice.IsPrimaryKey = false;
				colvarDiscountPrice.IsForeignKey = false;
				colvarDiscountPrice.IsReadOnly = false;
				colvarDiscountPrice.DefaultSetting = @"";
				colvarDiscountPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountPrice);
				
				TableSchema.TableColumn colvarMessage1 = new TableSchema.TableColumn(schema);
				colvarMessage1.ColumnName = "message_1";
				colvarMessage1.DataType = DbType.String;
				colvarMessage1.MaxLength = 500;
				colvarMessage1.AutoIncrement = false;
				colvarMessage1.IsNullable = true;
				colvarMessage1.IsPrimaryKey = false;
				colvarMessage1.IsForeignKey = false;
				colvarMessage1.IsReadOnly = false;
				colvarMessage1.DefaultSetting = @"";
				colvarMessage1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage1);
				
				TableSchema.TableColumn colvarMessage2 = new TableSchema.TableColumn(schema);
				colvarMessage2.ColumnName = "message_2";
				colvarMessage2.DataType = DbType.String;
				colvarMessage2.MaxLength = 500;
				colvarMessage2.AutoIncrement = false;
				colvarMessage2.IsNullable = true;
				colvarMessage2.IsPrimaryKey = false;
				colvarMessage2.IsForeignKey = false;
				colvarMessage2.IsReadOnly = false;
				colvarMessage2.DefaultSetting = @"";
				colvarMessage2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage2);
				
				TableSchema.TableColumn colvarMessage3 = new TableSchema.TableColumn(schema);
				colvarMessage3.ColumnName = "message_3";
				colvarMessage3.DataType = DbType.String;
				colvarMessage3.MaxLength = 500;
				colvarMessage3.AutoIncrement = false;
				colvarMessage3.IsNullable = true;
				colvarMessage3.IsPrimaryKey = false;
				colvarMessage3.IsForeignKey = false;
				colvarMessage3.IsReadOnly = false;
				colvarMessage3.DefaultSetting = @"";
				colvarMessage3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage3);
				
				TableSchema.TableColumn colvarAllTable = new TableSchema.TableColumn(schema);
				colvarAllTable.ColumnName = "all_table";
				colvarAllTable.DataType = DbType.Boolean;
				colvarAllTable.MaxLength = 0;
				colvarAllTable.AutoIncrement = false;
				colvarAllTable.IsNullable = true;
				colvarAllTable.IsPrimaryKey = false;
				colvarAllTable.IsForeignKey = false;
				colvarAllTable.IsReadOnly = false;
				colvarAllTable.DefaultSetting = @"";
				colvarAllTable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAllTable);
				
				TableSchema.TableColumn colvarOneMeal = new TableSchema.TableColumn(schema);
				colvarOneMeal.ColumnName = "one_meal";
				colvarOneMeal.DataType = DbType.Boolean;
				colvarOneMeal.MaxLength = 0;
				colvarOneMeal.AutoIncrement = false;
				colvarOneMeal.IsNullable = true;
				colvarOneMeal.IsPrimaryKey = false;
				colvarOneMeal.IsForeignKey = false;
				colvarOneMeal.IsReadOnly = false;
				colvarOneMeal.DefaultSetting = @"";
				colvarOneMeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOneMeal);
				
				TableSchema.TableColumn colvarMultipleMeals = new TableSchema.TableColumn(schema);
				colvarMultipleMeals.ColumnName = "multiple_meals";
				colvarMultipleMeals.DataType = DbType.Boolean;
				colvarMultipleMeals.MaxLength = 0;
				colvarMultipleMeals.AutoIncrement = false;
				colvarMultipleMeals.IsNullable = true;
				colvarMultipleMeals.IsPrimaryKey = false;
				colvarMultipleMeals.IsForeignKey = false;
				colvarMultipleMeals.IsReadOnly = false;
				colvarMultipleMeals.DefaultSetting = @"";
				colvarMultipleMeals.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMultipleMeals);
				
				TableSchema.TableColumn colvarTheFourthApplied = new TableSchema.TableColumn(schema);
				colvarTheFourthApplied.ColumnName = "the_fourth_applied";
				colvarTheFourthApplied.DataType = DbType.Boolean;
				colvarTheFourthApplied.MaxLength = 0;
				colvarTheFourthApplied.AutoIncrement = false;
				colvarTheFourthApplied.IsNullable = true;
				colvarTheFourthApplied.IsPrimaryKey = false;
				colvarTheFourthApplied.IsForeignKey = false;
				colvarTheFourthApplied.IsReadOnly = false;
				colvarTheFourthApplied.DefaultSetting = @"";
				colvarTheFourthApplied.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTheFourthApplied);
				
				TableSchema.TableColumn colvarGiftAttending = new TableSchema.TableColumn(schema);
				colvarGiftAttending.ColumnName = "gift_attending";
				colvarGiftAttending.DataType = DbType.Boolean;
				colvarGiftAttending.MaxLength = 0;
				colvarGiftAttending.AutoIncrement = false;
				colvarGiftAttending.IsNullable = true;
				colvarGiftAttending.IsPrimaryKey = false;
				colvarGiftAttending.IsForeignKey = false;
				colvarGiftAttending.IsReadOnly = false;
				colvarGiftAttending.DefaultSetting = @"";
				colvarGiftAttending.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGiftAttending);
				
				TableSchema.TableColumn colvarGiftConsumption = new TableSchema.TableColumn(schema);
				colvarGiftConsumption.ColumnName = "gift_consumption";
				colvarGiftConsumption.DataType = DbType.Boolean;
				colvarGiftConsumption.MaxLength = 0;
				colvarGiftConsumption.AutoIncrement = false;
				colvarGiftConsumption.IsNullable = true;
				colvarGiftConsumption.IsPrimaryKey = false;
				colvarGiftConsumption.IsForeignKey = false;
				colvarGiftConsumption.IsReadOnly = false;
				colvarGiftConsumption.DefaultSetting = @"";
				colvarGiftConsumption.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGiftConsumption);
				
				TableSchema.TableColumn colvarGiftReplacable = new TableSchema.TableColumn(schema);
				colvarGiftReplacable.ColumnName = "gift_replacable";
				colvarGiftReplacable.DataType = DbType.Boolean;
				colvarGiftReplacable.MaxLength = 0;
				colvarGiftReplacable.AutoIncrement = false;
				colvarGiftReplacable.IsNullable = true;
				colvarGiftReplacable.IsPrimaryKey = false;
				colvarGiftReplacable.IsForeignKey = false;
				colvarGiftReplacable.IsReadOnly = false;
				colvarGiftReplacable.DefaultSetting = @"";
				colvarGiftReplacable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGiftReplacable);
				
				TableSchema.TableColumn colvarGiftLimited = new TableSchema.TableColumn(schema);
				colvarGiftLimited.ColumnName = "gift_limited";
				colvarGiftLimited.DataType = DbType.Boolean;
				colvarGiftLimited.MaxLength = 0;
				colvarGiftLimited.AutoIncrement = false;
				colvarGiftLimited.IsNullable = true;
				colvarGiftLimited.IsPrimaryKey = false;
				colvarGiftLimited.IsForeignKey = false;
				colvarGiftLimited.IsReadOnly = false;
				colvarGiftLimited.DefaultSetting = @"";
				colvarGiftLimited.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGiftLimited);
				
				TableSchema.TableColumn colvarGiftQuantity = new TableSchema.TableColumn(schema);
				colvarGiftQuantity.ColumnName = "gift_quantity";
				colvarGiftQuantity.DataType = DbType.Int32;
				colvarGiftQuantity.MaxLength = 0;
				colvarGiftQuantity.AutoIncrement = false;
				colvarGiftQuantity.IsNullable = true;
				colvarGiftQuantity.IsPrimaryKey = false;
				colvarGiftQuantity.IsForeignKey = false;
				colvarGiftQuantity.IsReadOnly = false;
				colvarGiftQuantity.DefaultSetting = @"";
				colvarGiftQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGiftQuantity);
				
				TableSchema.TableColumn colvarServiceFee = new TableSchema.TableColumn(schema);
				colvarServiceFee.ColumnName = "service_fee";
				colvarServiceFee.DataType = DbType.Boolean;
				colvarServiceFee.MaxLength = 0;
				colvarServiceFee.AutoIncrement = false;
				colvarServiceFee.IsNullable = true;
				colvarServiceFee.IsPrimaryKey = false;
				colvarServiceFee.IsForeignKey = false;
				colvarServiceFee.IsReadOnly = false;
				colvarServiceFee.DefaultSetting = @"";
				colvarServiceFee.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceFee);
				
				TableSchema.TableColumn colvarServiceFeeAdditional = new TableSchema.TableColumn(schema);
				colvarServiceFeeAdditional.ColumnName = "service_fee_additional";
				colvarServiceFeeAdditional.DataType = DbType.Boolean;
				colvarServiceFeeAdditional.MaxLength = 0;
				colvarServiceFeeAdditional.AutoIncrement = false;
				colvarServiceFeeAdditional.IsNullable = true;
				colvarServiceFeeAdditional.IsPrimaryKey = false;
				colvarServiceFeeAdditional.IsForeignKey = false;
				colvarServiceFeeAdditional.IsReadOnly = false;
				colvarServiceFeeAdditional.DefaultSetting = @"";
				colvarServiceFeeAdditional.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceFeeAdditional);
				
				TableSchema.TableColumn colvarConsumptionApplied = new TableSchema.TableColumn(schema);
				colvarConsumptionApplied.ColumnName = "consumption_applied";
				colvarConsumptionApplied.DataType = DbType.Boolean;
				colvarConsumptionApplied.MaxLength = 0;
				colvarConsumptionApplied.AutoIncrement = false;
				colvarConsumptionApplied.IsNullable = true;
				colvarConsumptionApplied.IsPrimaryKey = false;
				colvarConsumptionApplied.IsForeignKey = false;
				colvarConsumptionApplied.IsReadOnly = false;
				colvarConsumptionApplied.DefaultSetting = @"";
				colvarConsumptionApplied.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsumptionApplied);
				
				TableSchema.TableColumn colvarMinConsumption = new TableSchema.TableColumn(schema);
				colvarMinConsumption.ColumnName = "min_consumption";
				colvarMinConsumption.DataType = DbType.Int32;
				colvarMinConsumption.MaxLength = 0;
				colvarMinConsumption.AutoIncrement = false;
				colvarMinConsumption.IsNullable = true;
				colvarMinConsumption.IsPrimaryKey = false;
				colvarMinConsumption.IsForeignKey = false;
				colvarMinConsumption.IsReadOnly = false;
				colvarMinConsumption.DefaultSetting = @"";
				colvarMinConsumption.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMinConsumption);
				
				TableSchema.TableColumn colvarMinPersons = new TableSchema.TableColumn(schema);
				colvarMinPersons.ColumnName = "min_persons";
				colvarMinPersons.DataType = DbType.Int32;
				colvarMinPersons.MaxLength = 0;
				colvarMinPersons.AutoIncrement = false;
				colvarMinPersons.IsNullable = true;
				colvarMinPersons.IsPrimaryKey = false;
				colvarMinPersons.IsForeignKey = false;
				colvarMinPersons.IsReadOnly = false;
				colvarMinPersons.DefaultSetting = @"";
				colvarMinPersons.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMinPersons);
				
				TableSchema.TableColumn colvarHolidayApplied = new TableSchema.TableColumn(schema);
				colvarHolidayApplied.ColumnName = "holiday_applied";
				colvarHolidayApplied.DataType = DbType.Boolean;
				colvarHolidayApplied.MaxLength = 0;
				colvarHolidayApplied.AutoIncrement = false;
				colvarHolidayApplied.IsNullable = true;
				colvarHolidayApplied.IsPrimaryKey = false;
				colvarHolidayApplied.IsForeignKey = false;
				colvarHolidayApplied.IsReadOnly = false;
				colvarHolidayApplied.DefaultSetting = @"";
				colvarHolidayApplied.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHolidayApplied);
				
				TableSchema.TableColumn colvarWeekendApplied = new TableSchema.TableColumn(schema);
				colvarWeekendApplied.ColumnName = "weekend_applied";
				colvarWeekendApplied.DataType = DbType.Boolean;
				colvarWeekendApplied.MaxLength = 0;
				colvarWeekendApplied.AutoIncrement = false;
				colvarWeekendApplied.IsNullable = true;
				colvarWeekendApplied.IsPrimaryKey = false;
				colvarWeekendApplied.IsForeignKey = false;
				colvarWeekendApplied.IsReadOnly = false;
				colvarWeekendApplied.DefaultSetting = @"";
				colvarWeekendApplied.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWeekendApplied);
				
				TableSchema.TableColumn colvarOtherTimes = new TableSchema.TableColumn(schema);
				colvarOtherTimes.ColumnName = "other_times";
				colvarOtherTimes.DataType = DbType.String;
				colvarOtherTimes.MaxLength = 500;
				colvarOtherTimes.AutoIncrement = false;
				colvarOtherTimes.IsNullable = true;
				colvarOtherTimes.IsPrimaryKey = false;
				colvarOtherTimes.IsForeignKey = false;
				colvarOtherTimes.IsReadOnly = false;
				colvarOtherTimes.DefaultSetting = @"";
				colvarOtherTimes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOtherTimes);
				
				TableSchema.TableColumn colvarOthers = new TableSchema.TableColumn(schema);
				colvarOthers.ColumnName = "others";
				colvarOthers.DataType = DbType.String;
				colvarOthers.MaxLength = 500;
				colvarOthers.AutoIncrement = false;
				colvarOthers.IsNullable = true;
				colvarOthers.IsPrimaryKey = false;
				colvarOthers.IsForeignKey = false;
				colvarOthers.IsReadOnly = false;
				colvarOthers.DefaultSetting = @"";
				colvarOthers.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOthers);
				
				TableSchema.TableColumn colvarRestriction = new TableSchema.TableColumn(schema);
				colvarRestriction.ColumnName = "restriction";
				colvarRestriction.DataType = DbType.String;
				colvarRestriction.MaxLength = 1000;
				colvarRestriction.AutoIncrement = false;
				colvarRestriction.IsNullable = true;
				colvarRestriction.IsPrimaryKey = false;
				colvarRestriction.IsForeignKey = false;
				colvarRestriction.IsReadOnly = false;
				colvarRestriction.DefaultSetting = @"";
				colvarRestriction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRestriction);
				
				TableSchema.TableColumn colvarReturnTime = new TableSchema.TableColumn(schema);
				colvarReturnTime.ColumnName = "return_time";
				colvarReturnTime.DataType = DbType.DateTime;
				colvarReturnTime.MaxLength = 0;
				colvarReturnTime.AutoIncrement = false;
				colvarReturnTime.IsNullable = true;
				colvarReturnTime.IsPrimaryKey = false;
				colvarReturnTime.IsForeignKey = false;
				colvarReturnTime.IsReadOnly = false;
				colvarReturnTime.DefaultSetting = @"";
				colvarReturnTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnTime);
				
				TableSchema.TableColumn colvarApproveTime = new TableSchema.TableColumn(schema);
				colvarApproveTime.ColumnName = "approve_time";
				colvarApproveTime.DataType = DbType.DateTime;
				colvarApproveTime.MaxLength = 0;
				colvarApproveTime.AutoIncrement = false;
				colvarApproveTime.IsNullable = true;
				colvarApproveTime.IsPrimaryKey = false;
				colvarApproveTime.IsForeignKey = false;
				colvarApproveTime.IsReadOnly = false;
				colvarApproveTime.DefaultSetting = @"";
				colvarApproveTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApproveTime);
				
				TableSchema.TableColumn colvarCityBit = new TableSchema.TableColumn(schema);
				colvarCityBit.ColumnName = "city_bit";
				colvarCityBit.DataType = DbType.Int32;
				colvarCityBit.MaxLength = 0;
				colvarCityBit.AutoIncrement = false;
				colvarCityBit.IsNullable = false;
				colvarCityBit.IsPrimaryKey = false;
				colvarCityBit.IsForeignKey = false;
				colvarCityBit.IsReadOnly = false;
				
						colvarCityBit.DefaultSetting = @"((0))";
				colvarCityBit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityBit);
				
				TableSchema.TableColumn colvarContractStatus = new TableSchema.TableColumn(schema);
				colvarContractStatus.ColumnName = "contract_status";
				colvarContractStatus.DataType = DbType.Int32;
				colvarContractStatus.MaxLength = 0;
				colvarContractStatus.AutoIncrement = false;
				colvarContractStatus.IsNullable = false;
				colvarContractStatus.IsPrimaryKey = false;
				colvarContractStatus.IsForeignKey = false;
				colvarContractStatus.IsReadOnly = false;
				
						colvarContractStatus.DefaultSetting = @"((0))";
				colvarContractStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContractStatus);
				
				TableSchema.TableColumn colvarNoContractReason = new TableSchema.TableColumn(schema);
				colvarNoContractReason.ColumnName = "no_contract_reason";
				colvarNoContractReason.DataType = DbType.String;
				colvarNoContractReason.MaxLength = 100;
				colvarNoContractReason.AutoIncrement = false;
				colvarNoContractReason.IsNullable = true;
				colvarNoContractReason.IsPrimaryKey = false;
				colvarNoContractReason.IsForeignKey = false;
				colvarNoContractReason.IsReadOnly = false;
				colvarNoContractReason.DefaultSetting = @"";
				colvarNoContractReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNoContractReason);
				
				TableSchema.TableColumn colvarSalesName = new TableSchema.TableColumn(schema);
				colvarSalesName.ColumnName = "sales_name";
				colvarSalesName.DataType = DbType.String;
				colvarSalesName.MaxLength = 50;
				colvarSalesName.AutoIncrement = false;
				colvarSalesName.IsNullable = true;
				colvarSalesName.IsPrimaryKey = false;
				colvarSalesName.IsForeignKey = false;
				colvarSalesName.IsReadOnly = false;
				colvarSalesName.DefaultSetting = @"";
				colvarSalesName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesName);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("vourcher_event",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("EventName")]
		[Bindable(true)]
		public string EventName 
		{
			get { return GetColumnValue<string>(Columns.EventName); }
			set { SetColumnValue(Columns.EventName, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("Contents")]
		[Bindable(true)]
		public string Contents 
		{
			get { return GetColumnValue<string>(Columns.Contents); }
			set { SetColumnValue(Columns.Contents, value); }
		}
		  
		[XmlAttribute("Instruction")]
		[Bindable(true)]
		public string Instruction 
		{
			get { return GetColumnValue<string>(Columns.Instruction); }
			set { SetColumnValue(Columns.Instruction, value); }
		}
		  
		[XmlAttribute("PicUrl")]
		[Bindable(true)]
		public string PicUrl 
		{
			get { return GetColumnValue<string>(Columns.PicUrl); }
			set { SetColumnValue(Columns.PicUrl, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("StartDate")]
		[Bindable(true)]
		public DateTime? StartDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.StartDate); }
			set { SetColumnValue(Columns.StartDate, value); }
		}
		  
		[XmlAttribute("EndDate")]
		[Bindable(true)]
		public DateTime? EndDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.EndDate); }
			set { SetColumnValue(Columns.EndDate, value); }
		}
		  
		[XmlAttribute("PageCount")]
		[Bindable(true)]
		public int PageCount 
		{
			get { return GetColumnValue<int>(Columns.PageCount); }
			set { SetColumnValue(Columns.PageCount, value); }
		}
		  
		[XmlAttribute("MaxQuantity")]
		[Bindable(true)]
		public int MaxQuantity 
		{
			get { return GetColumnValue<int>(Columns.MaxQuantity); }
			set { SetColumnValue(Columns.MaxQuantity, value); }
		}
		  
		[XmlAttribute("CurrentQuantity")]
		[Bindable(true)]
		public int CurrentQuantity 
		{
			get { return GetColumnValue<int>(Columns.CurrentQuantity); }
			set { SetColumnValue(Columns.CurrentQuantity, value); }
		}
		  
		[XmlAttribute("Ratio")]
		[Bindable(true)]
		public int Ratio 
		{
			get { return GetColumnValue<int>(Columns.Ratio); }
			set { SetColumnValue(Columns.Ratio, value); }
		}
		  
		[XmlAttribute("Enable")]
		[Bindable(true)]
		public bool Enable 
		{
			get { return GetColumnValue<bool>(Columns.Enable); }
			set { SetColumnValue(Columns.Enable, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("Mode")]
		[Bindable(true)]
		public int Mode 
		{
			get { return GetColumnValue<int>(Columns.Mode); }
			set { SetColumnValue(Columns.Mode, value); }
		}
		  
		[XmlAttribute("Magazine")]
		[Bindable(true)]
		public bool? Magazine 
		{
			get { return GetColumnValue<bool?>(Columns.Magazine); }
			set { SetColumnValue(Columns.Magazine, value); }
		}
		  
		[XmlAttribute("Discount")]
		[Bindable(true)]
		public decimal? Discount 
		{
			get { return GetColumnValue<decimal?>(Columns.Discount); }
			set { SetColumnValue(Columns.Discount, value); }
		}
		  
		[XmlAttribute("OriginalPrice")]
		[Bindable(true)]
		public int? OriginalPrice 
		{
			get { return GetColumnValue<int?>(Columns.OriginalPrice); }
			set { SetColumnValue(Columns.OriginalPrice, value); }
		}
		  
		[XmlAttribute("DiscountPrice")]
		[Bindable(true)]
		public int? DiscountPrice 
		{
			get { return GetColumnValue<int?>(Columns.DiscountPrice); }
			set { SetColumnValue(Columns.DiscountPrice, value); }
		}
		  
		[XmlAttribute("Message1")]
		[Bindable(true)]
		public string Message1 
		{
			get { return GetColumnValue<string>(Columns.Message1); }
			set { SetColumnValue(Columns.Message1, value); }
		}
		  
		[XmlAttribute("Message2")]
		[Bindable(true)]
		public string Message2 
		{
			get { return GetColumnValue<string>(Columns.Message2); }
			set { SetColumnValue(Columns.Message2, value); }
		}
		  
		[XmlAttribute("Message3")]
		[Bindable(true)]
		public string Message3 
		{
			get { return GetColumnValue<string>(Columns.Message3); }
			set { SetColumnValue(Columns.Message3, value); }
		}
		  
		[XmlAttribute("AllTable")]
		[Bindable(true)]
		public bool? AllTable 
		{
			get { return GetColumnValue<bool?>(Columns.AllTable); }
			set { SetColumnValue(Columns.AllTable, value); }
		}
		  
		[XmlAttribute("OneMeal")]
		[Bindable(true)]
		public bool? OneMeal 
		{
			get { return GetColumnValue<bool?>(Columns.OneMeal); }
			set { SetColumnValue(Columns.OneMeal, value); }
		}
		  
		[XmlAttribute("MultipleMeals")]
		[Bindable(true)]
		public bool? MultipleMeals 
		{
			get { return GetColumnValue<bool?>(Columns.MultipleMeals); }
			set { SetColumnValue(Columns.MultipleMeals, value); }
		}
		  
		[XmlAttribute("TheFourthApplied")]
		[Bindable(true)]
		public bool? TheFourthApplied 
		{
			get { return GetColumnValue<bool?>(Columns.TheFourthApplied); }
			set { SetColumnValue(Columns.TheFourthApplied, value); }
		}
		  
		[XmlAttribute("GiftAttending")]
		[Bindable(true)]
		public bool? GiftAttending 
		{
			get { return GetColumnValue<bool?>(Columns.GiftAttending); }
			set { SetColumnValue(Columns.GiftAttending, value); }
		}
		  
		[XmlAttribute("GiftConsumption")]
		[Bindable(true)]
		public bool? GiftConsumption 
		{
			get { return GetColumnValue<bool?>(Columns.GiftConsumption); }
			set { SetColumnValue(Columns.GiftConsumption, value); }
		}
		  
		[XmlAttribute("GiftReplacable")]
		[Bindable(true)]
		public bool? GiftReplacable 
		{
			get { return GetColumnValue<bool?>(Columns.GiftReplacable); }
			set { SetColumnValue(Columns.GiftReplacable, value); }
		}
		  
		[XmlAttribute("GiftLimited")]
		[Bindable(true)]
		public bool? GiftLimited 
		{
			get { return GetColumnValue<bool?>(Columns.GiftLimited); }
			set { SetColumnValue(Columns.GiftLimited, value); }
		}
		  
		[XmlAttribute("GiftQuantity")]
		[Bindable(true)]
		public int? GiftQuantity 
		{
			get { return GetColumnValue<int?>(Columns.GiftQuantity); }
			set { SetColumnValue(Columns.GiftQuantity, value); }
		}
		  
		[XmlAttribute("ServiceFee")]
		[Bindable(true)]
		public bool? ServiceFee 
		{
			get { return GetColumnValue<bool?>(Columns.ServiceFee); }
			set { SetColumnValue(Columns.ServiceFee, value); }
		}
		  
		[XmlAttribute("ServiceFeeAdditional")]
		[Bindable(true)]
		public bool? ServiceFeeAdditional 
		{
			get { return GetColumnValue<bool?>(Columns.ServiceFeeAdditional); }
			set { SetColumnValue(Columns.ServiceFeeAdditional, value); }
		}
		  
		[XmlAttribute("ConsumptionApplied")]
		[Bindable(true)]
		public bool? ConsumptionApplied 
		{
			get { return GetColumnValue<bool?>(Columns.ConsumptionApplied); }
			set { SetColumnValue(Columns.ConsumptionApplied, value); }
		}
		  
		[XmlAttribute("MinConsumption")]
		[Bindable(true)]
		public int? MinConsumption 
		{
			get { return GetColumnValue<int?>(Columns.MinConsumption); }
			set { SetColumnValue(Columns.MinConsumption, value); }
		}
		  
		[XmlAttribute("MinPersons")]
		[Bindable(true)]
		public int? MinPersons 
		{
			get { return GetColumnValue<int?>(Columns.MinPersons); }
			set { SetColumnValue(Columns.MinPersons, value); }
		}
		  
		[XmlAttribute("HolidayApplied")]
		[Bindable(true)]
		public bool? HolidayApplied 
		{
			get { return GetColumnValue<bool?>(Columns.HolidayApplied); }
			set { SetColumnValue(Columns.HolidayApplied, value); }
		}
		  
		[XmlAttribute("WeekendApplied")]
		[Bindable(true)]
		public bool? WeekendApplied 
		{
			get { return GetColumnValue<bool?>(Columns.WeekendApplied); }
			set { SetColumnValue(Columns.WeekendApplied, value); }
		}
		  
		[XmlAttribute("OtherTimes")]
		[Bindable(true)]
		public string OtherTimes 
		{
			get { return GetColumnValue<string>(Columns.OtherTimes); }
			set { SetColumnValue(Columns.OtherTimes, value); }
		}
		  
		[XmlAttribute("Others")]
		[Bindable(true)]
		public string Others 
		{
			get { return GetColumnValue<string>(Columns.Others); }
			set { SetColumnValue(Columns.Others, value); }
		}
		  
		[XmlAttribute("Restriction")]
		[Bindable(true)]
		public string Restriction 
		{
			get { return GetColumnValue<string>(Columns.Restriction); }
			set { SetColumnValue(Columns.Restriction, value); }
		}
		  
		[XmlAttribute("ReturnTime")]
		[Bindable(true)]
		public DateTime? ReturnTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ReturnTime); }
			set { SetColumnValue(Columns.ReturnTime, value); }
		}
		  
		[XmlAttribute("ApproveTime")]
		[Bindable(true)]
		public DateTime? ApproveTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ApproveTime); }
			set { SetColumnValue(Columns.ApproveTime, value); }
		}
		  
		[XmlAttribute("CityBit")]
		[Bindable(true)]
		public int CityBit 
		{
			get { return GetColumnValue<int>(Columns.CityBit); }
			set { SetColumnValue(Columns.CityBit, value); }
		}
		  
		[XmlAttribute("ContractStatus")]
		[Bindable(true)]
		public int ContractStatus 
		{
			get { return GetColumnValue<int>(Columns.ContractStatus); }
			set { SetColumnValue(Columns.ContractStatus, value); }
		}
		  
		[XmlAttribute("NoContractReason")]
		[Bindable(true)]
		public string NoContractReason 
		{
			get { return GetColumnValue<string>(Columns.NoContractReason); }
			set { SetColumnValue(Columns.NoContractReason, value); }
		}
		  
		[XmlAttribute("SalesName")]
		[Bindable(true)]
		public string SalesName 
		{
			get { return GetColumnValue<string>(Columns.SalesName); }
			set { SetColumnValue(Columns.SalesName, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EventNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ContentsColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn InstructionColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PicUrlColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn StartDateColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn EndDateColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn PageCountColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn MaxQuantityColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CurrentQuantityColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn RatioColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn EnableColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn ModeColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn MagazineColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn OriginalPriceColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountPriceColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn Message1Column
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn Message2Column
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn Message3Column
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn AllTableColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn OneMealColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn MultipleMealsColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn TheFourthAppliedColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn GiftAttendingColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn GiftConsumptionColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn GiftReplacableColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn GiftLimitedColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn GiftQuantityColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn ServiceFeeColumn
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn ServiceFeeAdditionalColumn
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn ConsumptionAppliedColumn
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        public static TableSchema.TableColumn MinConsumptionColumn
        {
            get { return Schema.Columns[40]; }
        }
        
        
        
        public static TableSchema.TableColumn MinPersonsColumn
        {
            get { return Schema.Columns[41]; }
        }
        
        
        
        public static TableSchema.TableColumn HolidayAppliedColumn
        {
            get { return Schema.Columns[42]; }
        }
        
        
        
        public static TableSchema.TableColumn WeekendAppliedColumn
        {
            get { return Schema.Columns[43]; }
        }
        
        
        
        public static TableSchema.TableColumn OtherTimesColumn
        {
            get { return Schema.Columns[44]; }
        }
        
        
        
        public static TableSchema.TableColumn OthersColumn
        {
            get { return Schema.Columns[45]; }
        }
        
        
        
        public static TableSchema.TableColumn RestrictionColumn
        {
            get { return Schema.Columns[46]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnTimeColumn
        {
            get { return Schema.Columns[47]; }
        }
        
        
        
        public static TableSchema.TableColumn ApproveTimeColumn
        {
            get { return Schema.Columns[48]; }
        }
        
        
        
        public static TableSchema.TableColumn CityBitColumn
        {
            get { return Schema.Columns[49]; }
        }
        
        
        
        public static TableSchema.TableColumn ContractStatusColumn
        {
            get { return Schema.Columns[50]; }
        }
        
        
        
        public static TableSchema.TableColumn NoContractReasonColumn
        {
            get { return Schema.Columns[51]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesNameColumn
        {
            get { return Schema.Columns[52]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string EventName = @"event_name";
			 public static string SellerGuid = @"seller_guid";
			 public static string Contents = @"contents";
			 public static string Instruction = @"instruction";
			 public static string PicUrl = @"pic_url";
			 public static string Status = @"status";
			 public static string Type = @"type";
			 public static string StartDate = @"start_date";
			 public static string EndDate = @"end_date";
			 public static string PageCount = @"page_count";
			 public static string MaxQuantity = @"max_quantity";
			 public static string CurrentQuantity = @"current_quantity";
			 public static string Ratio = @"ratio";
			 public static string Enable = @"enable";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string Message = @"message";
			 public static string Mode = @"mode";
			 public static string Magazine = @"magazine";
			 public static string Discount = @"discount";
			 public static string OriginalPrice = @"original_price";
			 public static string DiscountPrice = @"discount_price";
			 public static string Message1 = @"message_1";
			 public static string Message2 = @"message_2";
			 public static string Message3 = @"message_3";
			 public static string AllTable = @"all_table";
			 public static string OneMeal = @"one_meal";
			 public static string MultipleMeals = @"multiple_meals";
			 public static string TheFourthApplied = @"the_fourth_applied";
			 public static string GiftAttending = @"gift_attending";
			 public static string GiftConsumption = @"gift_consumption";
			 public static string GiftReplacable = @"gift_replacable";
			 public static string GiftLimited = @"gift_limited";
			 public static string GiftQuantity = @"gift_quantity";
			 public static string ServiceFee = @"service_fee";
			 public static string ServiceFeeAdditional = @"service_fee_additional";
			 public static string ConsumptionApplied = @"consumption_applied";
			 public static string MinConsumption = @"min_consumption";
			 public static string MinPersons = @"min_persons";
			 public static string HolidayApplied = @"holiday_applied";
			 public static string WeekendApplied = @"weekend_applied";
			 public static string OtherTimes = @"other_times";
			 public static string Others = @"others";
			 public static string Restriction = @"restriction";
			 public static string ReturnTime = @"return_time";
			 public static string ApproveTime = @"approve_time";
			 public static string CityBit = @"city_bit";
			 public static string ContractStatus = @"contract_status";
			 public static string NoContractReason = @"no_contract_reason";
			 public static string SalesName = @"sales_name";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

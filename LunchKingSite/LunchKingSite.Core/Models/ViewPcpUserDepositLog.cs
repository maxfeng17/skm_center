using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpUserDepositLog class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpUserDepositLogCollection : ReadOnlyList<ViewPcpUserDepositLog, ViewPcpUserDepositLogCollection>
    {        
        public ViewPcpUserDepositLogCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_user_deposit_log view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpUserDepositLog : ReadOnlyRecord<ViewPcpUserDepositLog>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_user_deposit_log", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarLogId = new TableSchema.TableColumn(schema);
                colvarLogId.ColumnName = "log_id";
                colvarLogId.DataType = DbType.Int32;
                colvarLogId.MaxLength = 0;
                colvarLogId.AutoIncrement = false;
                colvarLogId.IsNullable = false;
                colvarLogId.IsPrimaryKey = false;
                colvarLogId.IsForeignKey = false;
                colvarLogId.IsReadOnly = false;
                
                schema.Columns.Add(colvarLogId);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_Id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
                colvarGroupId.ColumnName = "group_id";
                colvarGroupId.DataType = DbType.Int32;
                colvarGroupId.MaxLength = 0;
                colvarGroupId.AutoIncrement = false;
                colvarGroupId.IsNullable = true;
                colvarGroupId.IsPrimaryKey = false;
                colvarGroupId.IsForeignKey = false;
                colvarGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupId);
                
                TableSchema.TableColumn colvarDepositId = new TableSchema.TableColumn(schema);
                colvarDepositId.ColumnName = "deposit_id";
                colvarDepositId.DataType = DbType.Int32;
                colvarDepositId.MaxLength = 0;
                colvarDepositId.AutoIncrement = false;
                colvarDepositId.IsNullable = false;
                colvarDepositId.IsPrimaryKey = false;
                colvarDepositId.IsForeignKey = false;
                colvarDepositId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepositId);
                
                TableSchema.TableColumn colvarMenuId = new TableSchema.TableColumn(schema);
                colvarMenuId.ColumnName = "menu_id";
                colvarMenuId.DataType = DbType.Int32;
                colvarMenuId.MaxLength = 0;
                colvarMenuId.AutoIncrement = false;
                colvarMenuId.IsNullable = true;
                colvarMenuId.IsPrimaryKey = false;
                colvarMenuId.IsForeignKey = false;
                colvarMenuId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMenuId);
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 100;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = true;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = true;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 100;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarDepositType = new TableSchema.TableColumn(schema);
                colvarDepositType.ColumnName = "deposit_type";
                colvarDepositType.DataType = DbType.Int32;
                colvarDepositType.MaxLength = 0;
                colvarDepositType.AutoIncrement = false;
                colvarDepositType.IsNullable = false;
                colvarDepositType.IsPrimaryKey = false;
                colvarDepositType.IsForeignKey = false;
                colvarDepositType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepositType);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Int32;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = 100;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = true;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescription);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = false;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountId);
                
                TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
                colvarAccountName.ColumnName = "account_name";
                colvarAccountName.DataType = DbType.String;
                colvarAccountName.MaxLength = 100;
                colvarAccountName.AutoIncrement = false;
                colvarAccountName.IsNullable = false;
                colvarAccountName.IsPrimaryKey = false;
                colvarAccountName.IsForeignKey = false;
                colvarAccountName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountName);
                
                TableSchema.TableColumn colvarMemberLastName = new TableSchema.TableColumn(schema);
                colvarMemberLastName.ColumnName = "member_last_name";
                colvarMemberLastName.DataType = DbType.String;
                colvarMemberLastName.MaxLength = 50;
                colvarMemberLastName.AutoIncrement = false;
                colvarMemberLastName.IsNullable = true;
                colvarMemberLastName.IsPrimaryKey = false;
                colvarMemberLastName.IsForeignKey = false;
                colvarMemberLastName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberLastName);
                
                TableSchema.TableColumn colvarMemberFirstName = new TableSchema.TableColumn(schema);
                colvarMemberFirstName.ColumnName = "member_first_name";
                colvarMemberFirstName.DataType = DbType.String;
                colvarMemberFirstName.MaxLength = 50;
                colvarMemberFirstName.AutoIncrement = false;
                colvarMemberFirstName.IsNullable = true;
                colvarMemberFirstName.IsPrimaryKey = false;
                colvarMemberFirstName.IsForeignKey = false;
                colvarMemberFirstName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberFirstName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_user_deposit_log",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpUserDepositLog()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpUserDepositLog(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpUserDepositLog(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpUserDepositLog(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("LogId")]
        [Bindable(true)]
        public int LogId 
	    {
		    get
		    {
			    return GetColumnValue<int>("log_id");
		    }
            set 
		    {
			    SetColumnValue("log_id", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_Id");
		    }
            set 
		    {
			    SetColumnValue("user_Id", value);
            }
        }
	      
        [XmlAttribute("GroupId")]
        [Bindable(true)]
        public int? GroupId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_id");
		    }
            set 
		    {
			    SetColumnValue("group_id", value);
            }
        }
	      
        [XmlAttribute("DepositId")]
        [Bindable(true)]
        public int DepositId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deposit_id");
		    }
            set 
		    {
			    SetColumnValue("deposit_id", value);
            }
        }
	      
        [XmlAttribute("MenuId")]
        [Bindable(true)]
        public int? MenuId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("menu_id");
		    }
            set 
		    {
			    SetColumnValue("menu_id", value);
            }
        }
	      
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("title");
		    }
            set 
		    {
			    SetColumnValue("title", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int? ItemId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("DepositType")]
        [Bindable(true)]
        public int DepositType 
	    {
		    get
		    {
			    return GetColumnValue<int>("deposit_type");
		    }
            set 
		    {
			    SetColumnValue("deposit_type", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public int Amount 
	    {
		    get
		    {
			    return GetColumnValue<int>("amount");
		    }
            set 
		    {
			    SetColumnValue("amount", value);
            }
        }
	      
        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description 
	    {
		    get
		    {
			    return GetColumnValue<string>("description");
		    }
            set 
		    {
			    SetColumnValue("description", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId 
	    {
		    get
		    {
			    return GetColumnValue<int>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_id");
		    }
            set 
		    {
			    SetColumnValue("account_id", value);
            }
        }
	      
        [XmlAttribute("AccountName")]
        [Bindable(true)]
        public string AccountName 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_name");
		    }
            set 
		    {
			    SetColumnValue("account_name", value);
            }
        }
	      
        [XmlAttribute("MemberLastName")]
        [Bindable(true)]
        public string MemberLastName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_last_name");
		    }
            set 
		    {
			    SetColumnValue("member_last_name", value);
            }
        }
	      
        [XmlAttribute("MemberFirstName")]
        [Bindable(true)]
        public string MemberFirstName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_first_name");
		    }
            set 
		    {
			    SetColumnValue("member_first_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string LogId = @"log_id";
            
            public static string UserId = @"user_Id";
            
            public static string GroupId = @"group_id";
            
            public static string DepositId = @"deposit_id";
            
            public static string MenuId = @"menu_id";
            
            public static string Title = @"title";
            
            public static string ItemId = @"item_id";
            
            public static string ItemName = @"item_name";
            
            public static string DepositType = @"deposit_type";
            
            public static string Amount = @"amount";
            
            public static string Description = @"description";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string SellerName = @"seller_name";
            
            public static string AccountId = @"account_id";
            
            public static string AccountName = @"account_name";
            
            public static string MemberLastName = @"member_last_name";
            
            public static string MemberFirstName = @"member_first_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

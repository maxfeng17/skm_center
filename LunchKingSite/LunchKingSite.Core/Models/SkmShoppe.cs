using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    [Serializable]
    public partial class SkmShoppeCollection : RepositoryList<SkmShoppe, SkmShoppeCollection>
    {
        public SkmShoppeCollection() { }

        public SkmShoppeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmShoppe o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }

    [Serializable]
    public partial class SkmShoppe : RepositoryRecord<SkmShoppe>, IRecordBase
    {
        #region .ctors and Default Settings
        public SkmShoppe()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SkmShoppe(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("skm_shoppe", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                TableSchema.TableColumn colvarBrandCounterCode = new TableSchema.TableColumn(schema);
                colvarBrandCounterCode.ColumnName = "brand_counter_code";
                colvarBrandCounterCode.DataType = DbType.AnsiString;
                colvarBrandCounterCode.MaxLength = 10;
                colvarBrandCounterCode.AutoIncrement = false;
                colvarBrandCounterCode.IsNullable = false;
                colvarBrandCounterCode.IsPrimaryKey = true;
                colvarBrandCounterCode.IsForeignKey = false;
                colvarBrandCounterCode.IsReadOnly = false;
                colvarBrandCounterCode.DefaultSetting = @"";
                colvarBrandCounterCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBrandCounterCode);

                TableSchema.TableColumn colvarBrandCounterName = new TableSchema.TableColumn(schema);
                colvarBrandCounterName.ColumnName = "brand_counter_name";
                colvarBrandCounterName.DataType = DbType.String;
                colvarBrandCounterName.MaxLength = 50;
                colvarBrandCounterName.AutoIncrement = false;
                colvarBrandCounterName.IsNullable = true;
                colvarBrandCounterName.IsPrimaryKey = false;
                colvarBrandCounterName.IsForeignKey = false;
                colvarBrandCounterName.IsReadOnly = false;
                colvarBrandCounterName.DefaultSetting = @"";
                colvarBrandCounterName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBrandCounterName);

                TableSchema.TableColumn colvarShopCode = new TableSchema.TableColumn(schema);
                colvarShopCode.ColumnName = "shop_code";
                colvarShopCode.DataType = DbType.AnsiString;
                colvarShopCode.MaxLength = 4;
                colvarShopCode.AutoIncrement = false;
                colvarShopCode.IsNullable = false;
                colvarShopCode.IsPrimaryKey = true;
                colvarShopCode.IsForeignKey = false;
                colvarShopCode.IsReadOnly = false;
                colvarShopCode.DefaultSetting = @"";
                colvarShopCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShopCode);

                TableSchema.TableColumn colvarShopName = new TableSchema.TableColumn(schema);
                colvarShopName.ColumnName = "shop_name";
                colvarShopName.DataType = DbType.String;
                colvarShopName.MaxLength = 50;
                colvarShopName.AutoIncrement = false;
                colvarShopName.IsNullable = true;
                colvarShopName.IsPrimaryKey = false;
                colvarShopName.IsForeignKey = false;
                colvarShopName.IsReadOnly = false;
                colvarShopName.DefaultSetting = @"";
                colvarShopName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShopName);

                TableSchema.TableColumn colvarFloor = new TableSchema.TableColumn(schema);
                colvarFloor.ColumnName = "floor";
                colvarFloor.DataType = DbType.String;
                colvarFloor.MaxLength = 10;
                colvarFloor.AutoIncrement = false;
                colvarFloor.IsNullable = true;
                colvarFloor.IsPrimaryKey = false;
                colvarFloor.IsForeignKey = false;
                colvarFloor.IsReadOnly = false;
                colvarFloor.DefaultSetting = @"";
                colvarFloor.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFloor);

                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                colvarStoreGuid.DefaultSetting = @"";
                colvarStoreGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreGuid);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = true;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                colvarSellerGuid.DefaultSetting = @"";
                colvarSellerGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarIsShoppe = new TableSchema.TableColumn(schema);
                colvarIsShoppe.ColumnName = "is_shoppe";
                colvarIsShoppe.DataType = DbType.Boolean;
                colvarIsShoppe.MaxLength = 0;
                colvarIsShoppe.AutoIncrement = false;
                colvarIsShoppe.IsNullable = false;
                colvarIsShoppe.IsPrimaryKey = false;
                colvarIsShoppe.IsForeignKey = false;
                colvarIsShoppe.IsReadOnly = false;
                colvarIsShoppe.DefaultSetting = @"";
                colvarIsShoppe.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsShoppe);

                TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
                colvarCategoryId.ColumnName = "category_id";
                colvarCategoryId.DataType = DbType.AnsiString;
                colvarCategoryId.MaxLength = 36;
                colvarCategoryId.AutoIncrement = false;
                colvarCategoryId.IsNullable = true;
                colvarCategoryId.IsPrimaryKey = false;
                colvarCategoryId.IsForeignKey = false;
                colvarCategoryId.IsReadOnly = false;
                colvarCategoryId.DefaultSetting = @"";
                colvarCategoryId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCategoryId);

                TableSchema.TableColumn colvarCategoryName = new TableSchema.TableColumn(schema);
                colvarCategoryName.ColumnName = "category_name";
                colvarCategoryName.DataType = DbType.String;
                colvarCategoryName.MaxLength = 100;
                colvarCategoryName.AutoIncrement = false;
                colvarCategoryName.IsNullable = true;
                colvarCategoryName.IsPrimaryKey = false;
                colvarCategoryName.IsForeignKey = false;
                colvarCategoryName.IsReadOnly = false;
                colvarCategoryName.DefaultSetting = @"";
                colvarCategoryName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCategoryName);

                TableSchema.TableColumn colvarSubcategoryId = new TableSchema.TableColumn(schema);
                colvarSubcategoryId.ColumnName = "subcategory_id";
                colvarSubcategoryId.DataType = DbType.AnsiString;
                colvarSubcategoryId.MaxLength = 36;
                colvarSubcategoryId.AutoIncrement = false;
                colvarSubcategoryId.IsNullable = true;
                colvarSubcategoryId.IsPrimaryKey = false;
                colvarSubcategoryId.IsForeignKey = false;
                colvarSubcategoryId.IsReadOnly = false;
                colvarSubcategoryId.DefaultSetting = @"";
                colvarSubcategoryId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSubcategoryId);

                TableSchema.TableColumn colvarSubcategoryName = new TableSchema.TableColumn(schema);
                colvarSubcategoryName.ColumnName = "subcategory_name";
                colvarSubcategoryName.DataType = DbType.String;
                colvarSubcategoryName.MaxLength = 100;
                colvarSubcategoryName.AutoIncrement = false;
                colvarSubcategoryName.IsNullable = true;
                colvarSubcategoryName.IsPrimaryKey = false;
                colvarSubcategoryName.IsForeignKey = false;
                colvarSubcategoryName.IsReadOnly = false;
                colvarSubcategoryName.DefaultSetting = @"";
                colvarSubcategoryName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSubcategoryName);

                TableSchema.TableColumn colvarIsAvailable = new TableSchema.TableColumn(schema);
                colvarIsAvailable.ColumnName = "is_available";
                colvarIsAvailable.DataType = DbType.Boolean;
                colvarIsAvailable.MaxLength = 0;
                colvarIsAvailable.AutoIncrement = false;
                colvarIsAvailable.IsNullable = false;
                colvarIsAvailable.IsPrimaryKey = false;
                colvarIsAvailable.IsForeignKey = false;
                colvarIsAvailable.IsReadOnly = false;
                colvarIsAvailable.DefaultSetting = @"((1))";
                colvarIsAvailable.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsAvailable);

                TableSchema.TableColumn colvarParentSellerGuid = new TableSchema.TableColumn(schema);
                colvarParentSellerGuid.ColumnName = "parent_seller_guid";
                colvarParentSellerGuid.DataType = DbType.Guid;
                colvarParentSellerGuid.MaxLength = 0;
                colvarParentSellerGuid.AutoIncrement = false;
                colvarParentSellerGuid.IsNullable = false;
                colvarParentSellerGuid.IsPrimaryKey = false;
                colvarParentSellerGuid.IsForeignKey = false;
                colvarParentSellerGuid.IsReadOnly = false;
                colvarParentSellerGuid.DefaultSetting = @"(CONVERT([uniqueidentifier],CONVERT([binary],(0),(0)),(0)))";
                colvarParentSellerGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarParentSellerGuid);

                TableSchema.TableColumn colvarIsExchange = new TableSchema.TableColumn(schema);
                colvarIsExchange.ColumnName = "is_exchange";
                colvarIsExchange.DataType = DbType.Boolean;
                colvarIsExchange.MaxLength = 0;
                colvarIsExchange.AutoIncrement = false;
                colvarIsExchange.IsNullable = false;
                colvarIsExchange.IsPrimaryKey = false;
                colvarIsExchange.IsForeignKey = false;
                colvarIsExchange.IsReadOnly = false;
                colvarIsExchange.DefaultSetting = @"((0))";
                colvarIsExchange.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsExchange);

                TableSchema.TableColumn colvarSkmStoreId = new TableSchema.TableColumn(schema);
                colvarSkmStoreId.ColumnName = "skm_store_id";
                colvarSkmStoreId.DataType = DbType.AnsiString;
                colvarSkmStoreId.MaxLength = 10;
                colvarSkmStoreId.AutoIncrement = false;
                colvarSkmStoreId.IsNullable = true;
                colvarSkmStoreId.IsPrimaryKey = false;
                colvarSkmStoreId.IsForeignKey = false;
                colvarSkmStoreId.IsReadOnly = false;
                colvarSkmStoreId.DefaultSetting = @"";
                colvarSkmStoreId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSkmStoreId);

                TableSchema.TableColumn colvarModifyDate = new TableSchema.TableColumn(schema);
                colvarModifyDate.ColumnName = "modify_date";
                colvarModifyDate.DataType = DbType.DateTime;
                colvarModifyDate.MaxLength = 0;
                colvarModifyDate.AutoIncrement = false;
                colvarModifyDate.IsNullable = false;
                colvarModifyDate.IsPrimaryKey = false;
                colvarModifyDate.IsForeignKey = false;
                colvarModifyDate.IsReadOnly = false;
                colvarModifyDate.DefaultSetting = @"(getdate())";
                colvarModifyDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyDate);

                TableSchema.TableColumn colvarSkmStoreName = new TableSchema.TableColumn(schema);
                colvarSkmStoreName.ColumnName = "skm_store_name";
                colvarSkmStoreName.DataType = DbType.String;
                colvarSkmStoreName.MaxLength = 200;
                colvarSkmStoreName.AutoIncrement = false;
                colvarSkmStoreName.IsNullable = true;
                colvarSkmStoreName.IsPrimaryKey = false;
                colvarSkmStoreName.IsForeignKey = false;
                colvarSkmStoreName.IsReadOnly = false;
                colvarSkmStoreName.DefaultSetting = @"";
                colvarSkmStoreName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSkmStoreName);

                TableSchema.TableColumn colvarSkmStoreGuid = new TableSchema.TableColumn(schema);
                colvarSkmStoreGuid.ColumnName = "skm_store_guid";
                colvarSkmStoreGuid.DataType = DbType.Guid;
                colvarSkmStoreGuid.MaxLength = 0;
                colvarSkmStoreGuid.AutoIncrement = false;
                colvarSkmStoreGuid.IsNullable = true;
                colvarSkmStoreGuid.IsPrimaryKey = false;
                colvarSkmStoreGuid.IsForeignKey = false;
                colvarSkmStoreGuid.IsReadOnly = false;
                colvarSkmStoreGuid.DefaultSetting = @"";
                colvarSkmStoreGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSkmStoreGuid);

                TableSchema.TableColumn colvarBrandGuid = new TableSchema.TableColumn(schema);
                colvarBrandGuid.ColumnName = "brand_guid";
                colvarBrandGuid.DataType = DbType.Guid;
                colvarBrandGuid.MaxLength = 0;
                colvarBrandGuid.AutoIncrement = false;
                colvarBrandGuid.IsNullable = true;
                colvarBrandGuid.IsPrimaryKey = false;
                colvarBrandGuid.IsForeignKey = false;
                colvarBrandGuid.IsReadOnly = false;
                colvarBrandGuid.DefaultSetting = @"";
                colvarBrandGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBrandGuid);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("skm_shoppe", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("BrandCounterCode")]
        [Bindable(true)]
        public string BrandCounterCode
        {
            get { return GetColumnValue<string>(Columns.BrandCounterCode); }
            set { SetColumnValue(Columns.BrandCounterCode, value); }
        }

        [XmlAttribute("BrandCounterName")]
        [Bindable(true)]
        public string BrandCounterName
        {
            get { return GetColumnValue<string>(Columns.BrandCounterName); }
            set { SetColumnValue(Columns.BrandCounterName, value); }
        }

        [XmlAttribute("ShopCode")]
        [Bindable(true)]
        public string ShopCode
        {
            get { return GetColumnValue<string>(Columns.ShopCode); }
            set { SetColumnValue(Columns.ShopCode, value); }
        }

        [XmlAttribute("ShopName")]
        [Bindable(true)]
        public string ShopName
        {
            get { return GetColumnValue<string>(Columns.ShopName); }
            set { SetColumnValue(Columns.ShopName, value); }
        }

        [XmlAttribute("Floor")]
        [Bindable(true)]
        public string Floor
        {
            get { return GetColumnValue<string>(Columns.Floor); }
            set { SetColumnValue(Columns.Floor, value); }
        }

        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid
        {
            get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
            set { SetColumnValue(Columns.StoreGuid, value); }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid? SellerGuid
        {
            get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
            set { SetColumnValue(Columns.SellerGuid, value); }
        }

        [XmlAttribute("IsShoppe")]
        [Bindable(true)]
        public bool IsShoppe
        {
            get { return GetColumnValue<bool>(Columns.IsShoppe); }
            set { SetColumnValue(Columns.IsShoppe, value); }
        }

        [XmlAttribute("CategoryId")]
        [Bindable(true)]
        public string CategoryId
        {
            get { return GetColumnValue<string>(Columns.CategoryId); }
            set { SetColumnValue(Columns.CategoryId, value); }
        }

        [XmlAttribute("CategoryName")]
        [Bindable(true)]
        public string CategoryName
        {
            get { return GetColumnValue<string>(Columns.CategoryName); }
            set { SetColumnValue(Columns.CategoryName, value); }
        }

        [XmlAttribute("SubcategoryId")]
        [Bindable(true)]
        public string SubcategoryId
        {
            get { return GetColumnValue<string>(Columns.SubcategoryId); }
            set { SetColumnValue(Columns.SubcategoryId, value); }
        }

        [XmlAttribute("SubcategoryName")]
        [Bindable(true)]
        public string SubcategoryName
        {
            get { return GetColumnValue<string>(Columns.SubcategoryName); }
            set { SetColumnValue(Columns.SubcategoryName, value); }
        }

        [XmlAttribute("IsAvailable")]
        [Bindable(true)]
        public bool IsAvailable
        {
            get { return GetColumnValue<bool>(Columns.IsAvailable); }
            set { SetColumnValue(Columns.IsAvailable, value); }
        }

        [XmlAttribute("ParentSellerGuid")]
        [Bindable(true)]
        public Guid ParentSellerGuid
        {
            get { return GetColumnValue<Guid>(Columns.ParentSellerGuid); }
            set { SetColumnValue(Columns.ParentSellerGuid, value); }
        }

        [XmlAttribute("IsExchange")]
        [Bindable(true)]
        public bool IsExchange
        {
            get { return GetColumnValue<bool>(Columns.IsExchange); }
            set { SetColumnValue(Columns.IsExchange, value); }
        }

        [XmlAttribute("SkmStoreId")]
        [Bindable(true)]
        public string SkmStoreId
        {
            get { return GetColumnValue<string>(Columns.SkmStoreId); }
            set { SetColumnValue(Columns.SkmStoreId, value); }
        }

        [XmlAttribute("ModifyDate")]
        [Bindable(true)]
        public DateTime ModifyDate
        {
            get { return GetColumnValue<DateTime>(Columns.ModifyDate); }
            set { SetColumnValue(Columns.ModifyDate, value); }
        }

        [XmlAttribute("SkmStoreName")]
        [Bindable(true)]
        public string SkmStoreName
        {
            get { return GetColumnValue<string>(Columns.SkmStoreName); }
            set { SetColumnValue(Columns.SkmStoreName, value); }
        }

        [XmlAttribute("SkmStoreGuid")]
        [Bindable(true)]
        public Guid? SkmStoreGuid
        {
            get { return GetColumnValue<Guid?>(Columns.SkmStoreGuid); }
            set { SetColumnValue(Columns.SkmStoreGuid, value); }
        }

        [XmlAttribute("BrandGuid")]
        [Bindable(true)]
        public Guid? BrandGuid
        {
            get { return GetColumnValue<Guid?>(Columns.BrandGuid); }
            set { SetColumnValue(Columns.BrandGuid, value); }
        }

        #endregion

        #region Typed Columns

        public static TableSchema.TableColumn BrandCounterCodeColumn
        {
            get { return Schema.Columns[0]; }
        }

        public static TableSchema.TableColumn BrandCounterNameColumn
        {
            get { return Schema.Columns[1]; }
        }

        public static TableSchema.TableColumn ShopCodeColumn
        {
            get { return Schema.Columns[2]; }
        }

        public static TableSchema.TableColumn ShopNameColumn
        {
            get { return Schema.Columns[3]; }
        }

        public static TableSchema.TableColumn FloorColumn
        {
            get { return Schema.Columns[4]; }
        }

        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[5]; }
        }

        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[6]; }
        }

        public static TableSchema.TableColumn IsShoppeColumn
        {
            get { return Schema.Columns[7]; }
        }

        public static TableSchema.TableColumn CategoryIdColumn
        {
            get { return Schema.Columns[8]; }
        }

        public static TableSchema.TableColumn CategoryNameColumn
        {
            get { return Schema.Columns[9]; }
        }

        public static TableSchema.TableColumn SubcategoryIdColumn
        {
            get { return Schema.Columns[10]; }
        }

        public static TableSchema.TableColumn SubcategoryNameColumn
        {
            get { return Schema.Columns[11]; }
        }

        public static TableSchema.TableColumn IsAvailableColumn
        {
            get { return Schema.Columns[12]; }
        }

        public static TableSchema.TableColumn ParentSellerGuidColumn
        {
            get { return Schema.Columns[13]; }
        }

        public static TableSchema.TableColumn IsExchangeColumn
        {
            get { return Schema.Columns[14]; }
        }

        public static TableSchema.TableColumn SkmStoreIdColumn
        {
            get { return Schema.Columns[15]; }
        }

        public static TableSchema.TableColumn ModifyDateColumn
        {
            get { return Schema.Columns[16]; }
        }

        public static TableSchema.TableColumn SkmStoreNameColumn
        {
            get { return Schema.Columns[17]; }
        }

        public static TableSchema.TableColumn SkmStoreGuidColumn
        {
            get { return Schema.Columns[18]; }
        }

        public static TableSchema.TableColumn BrandGuidColumn
        {
            get { return Schema.Columns[19]; }
        }

        #endregion

        #region Columns Struct

        public struct Columns
        {
            public static string BrandCounterCode = @"brand_counter_code";
            public static string BrandCounterName = @"brand_counter_name";
            public static string ShopCode = @"shop_code";
            public static string ShopName = @"shop_name";
            public static string Floor = @"floor";
            public static string StoreGuid = @"store_guid";
            public static string SellerGuid = @"seller_guid";
            public static string IsShoppe = @"is_shoppe";
            public static string CategoryId = @"category_id";
            public static string CategoryName = @"category_name";
            public static string SubcategoryId = @"subcategory_id";
            public static string SubcategoryName = @"subcategory_name";
            public static string IsAvailable = @"is_available";
            public static string ParentSellerGuid = @"parent_seller_guid";
            public static string IsExchange = @"is_exchange";
            public static string SkmStoreId = @"skm_store_id";
            public static string ModifyDate = @"modify_date";
            public static string SkmStoreName = @"skm_store_name";
            public static string SkmStoreGuid = @"skm_store_guid";
            public static string BrandGuid = @"brand_guid";
        }

        #endregion

    }
}

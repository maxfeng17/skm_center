using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VbsCompanyDetail class.
	/// </summary>
    [Serializable]
	public partial class VbsCompanyDetailCollection : RepositoryList<VbsCompanyDetail, VbsCompanyDetailCollection>
	{	   
		public VbsCompanyDetailCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VbsCompanyDetailCollection</returns>
		public VbsCompanyDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VbsCompanyDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the vbs_company_detail table.
	/// </summary>
	
	[Serializable]
	public partial class VbsCompanyDetail : RepositoryRecord<VbsCompanyDetail>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VbsCompanyDetail()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VbsCompanyDetail(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("vbs_company_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
				colvarEmail.ColumnName = "email";
				colvarEmail.DataType = DbType.String;
				colvarEmail.MaxLength = 256;
				colvarEmail.AutoIncrement = false;
				colvarEmail.IsNullable = true;
				colvarEmail.IsPrimaryKey = false;
				colvarEmail.IsForeignKey = false;
				colvarEmail.IsReadOnly = false;
				colvarEmail.DefaultSetting = @"";
				colvarEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmail);
				
				TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
				colvarMobile.ColumnName = "mobile";
				colvarMobile.DataType = DbType.AnsiString;
				colvarMobile.MaxLength = 20;
				colvarMobile.AutoIncrement = false;
				colvarMobile.IsNullable = true;
				colvarMobile.IsPrimaryKey = false;
				colvarMobile.IsForeignKey = false;
				colvarMobile.IsReadOnly = false;
				colvarMobile.DefaultSetting = @"";
				colvarMobile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobile);
				
				TableSchema.TableColumn colvarTownshipId = new TableSchema.TableColumn(schema);
				colvarTownshipId.ColumnName = "township_id";
				colvarTownshipId.DataType = DbType.Int32;
				colvarTownshipId.MaxLength = 0;
				colvarTownshipId.AutoIncrement = false;
				colvarTownshipId.IsNullable = true;
				colvarTownshipId.IsPrimaryKey = false;
				colvarTownshipId.IsForeignKey = false;
				colvarTownshipId.IsReadOnly = false;
				colvarTownshipId.DefaultSetting = @"";
				colvarTownshipId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTownshipId);
				
				TableSchema.TableColumn colvarInvoiceTitle = new TableSchema.TableColumn(schema);
				colvarInvoiceTitle.ColumnName = "invoice_title";
				colvarInvoiceTitle.DataType = DbType.String;
				colvarInvoiceTitle.MaxLength = 250;
				colvarInvoiceTitle.AutoIncrement = false;
				colvarInvoiceTitle.IsNullable = true;
				colvarInvoiceTitle.IsPrimaryKey = false;
				colvarInvoiceTitle.IsForeignKey = false;
				colvarInvoiceTitle.IsReadOnly = false;
				colvarInvoiceTitle.DefaultSetting = @"";
				colvarInvoiceTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceTitle);
				
				TableSchema.TableColumn colvarInvoiceComId = new TableSchema.TableColumn(schema);
				colvarInvoiceComId.ColumnName = "invoice_com_id";
				colvarInvoiceComId.DataType = DbType.AnsiString;
				colvarInvoiceComId.MaxLength = 10;
				colvarInvoiceComId.AutoIncrement = false;
				colvarInvoiceComId.IsNullable = true;
				colvarInvoiceComId.IsPrimaryKey = false;
				colvarInvoiceComId.IsForeignKey = false;
				colvarInvoiceComId.IsReadOnly = false;
				colvarInvoiceComId.DefaultSetting = @"";
				colvarInvoiceComId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceComId);
				
				TableSchema.TableColumn colvarInvoiceName = new TableSchema.TableColumn(schema);
				colvarInvoiceName.ColumnName = "invoice_name";
				colvarInvoiceName.DataType = DbType.String;
				colvarInvoiceName.MaxLength = 50;
				colvarInvoiceName.AutoIncrement = false;
				colvarInvoiceName.IsNullable = true;
				colvarInvoiceName.IsPrimaryKey = false;
				colvarInvoiceName.IsForeignKey = false;
				colvarInvoiceName.IsReadOnly = false;
				colvarInvoiceName.DefaultSetting = @"";
				colvarInvoiceName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceName);
				
				TableSchema.TableColumn colvarInvoiceAddress = new TableSchema.TableColumn(schema);
				colvarInvoiceAddress.ColumnName = "invoice_address";
				colvarInvoiceAddress.DataType = DbType.String;
				colvarInvoiceAddress.MaxLength = 250;
				colvarInvoiceAddress.AutoIncrement = false;
				colvarInvoiceAddress.IsNullable = true;
				colvarInvoiceAddress.IsPrimaryKey = false;
				colvarInvoiceAddress.IsForeignKey = false;
				colvarInvoiceAddress.IsReadOnly = false;
				colvarInvoiceAddress.DefaultSetting = @"";
				colvarInvoiceAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceAddress);
				
				TableSchema.TableColumn colvarAccountNo = new TableSchema.TableColumn(schema);
				colvarAccountNo.ColumnName = "account_no";
				colvarAccountNo.DataType = DbType.AnsiString;
				colvarAccountNo.MaxLength = 20;
				colvarAccountNo.AutoIncrement = false;
				colvarAccountNo.IsNullable = true;
				colvarAccountNo.IsPrimaryKey = false;
				colvarAccountNo.IsForeignKey = false;
				colvarAccountNo.IsReadOnly = false;
				colvarAccountNo.DefaultSetting = @"";
				colvarAccountNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountNo);
				
				TableSchema.TableColumn colvarAccountBankCode = new TableSchema.TableColumn(schema);
				colvarAccountBankCode.ColumnName = "account_bank_code";
				colvarAccountBankCode.DataType = DbType.AnsiString;
				colvarAccountBankCode.MaxLength = 10;
				colvarAccountBankCode.AutoIncrement = false;
				colvarAccountBankCode.IsNullable = true;
				colvarAccountBankCode.IsPrimaryKey = false;
				colvarAccountBankCode.IsForeignKey = false;
				colvarAccountBankCode.IsReadOnly = false;
				colvarAccountBankCode.DefaultSetting = @"";
				colvarAccountBankCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountBankCode);
				
				TableSchema.TableColumn colvarAccountBranchCode = new TableSchema.TableColumn(schema);
				colvarAccountBranchCode.ColumnName = "account_branch_code";
				colvarAccountBranchCode.DataType = DbType.AnsiString;
				colvarAccountBranchCode.MaxLength = 10;
				colvarAccountBranchCode.AutoIncrement = false;
				colvarAccountBranchCode.IsNullable = true;
				colvarAccountBranchCode.IsPrimaryKey = false;
				colvarAccountBranchCode.IsForeignKey = false;
				colvarAccountBranchCode.IsReadOnly = false;
				colvarAccountBranchCode.DefaultSetting = @"";
				colvarAccountBranchCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountBranchCode);
				
				TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
				colvarAccountName.ColumnName = "account_name";
				colvarAccountName.DataType = DbType.String;
				colvarAccountName.MaxLength = 50;
				colvarAccountName.AutoIncrement = false;
				colvarAccountName.IsNullable = true;
				colvarAccountName.IsPrimaryKey = false;
				colvarAccountName.IsForeignKey = false;
				colvarAccountName.IsReadOnly = false;
				colvarAccountName.DefaultSetting = @"";
				colvarAccountName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountName);
				
				TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
				colvarAccountId.ColumnName = "account_id";
				colvarAccountId.DataType = DbType.AnsiString;
				colvarAccountId.MaxLength = 20;
				colvarAccountId.AutoIncrement = false;
				colvarAccountId.IsNullable = true;
				colvarAccountId.IsPrimaryKey = false;
				colvarAccountId.IsForeignKey = false;
				colvarAccountId.IsReadOnly = false;
				colvarAccountId.DefaultSetting = @"";
				colvarAccountId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountId);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.Int32;
				colvarModifyId.MaxLength = 0;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 100;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				
						colvarSellerName.DefaultSetting = @"('')";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);
				
				TableSchema.TableColumn colvarInContractWith = new TableSchema.TableColumn(schema);
				colvarInContractWith.ColumnName = "in_contract_with";
				colvarInContractWith.DataType = DbType.String;
				colvarInContractWith.MaxLength = 80;
				colvarInContractWith.AutoIncrement = false;
				colvarInContractWith.IsNullable = true;
				colvarInContractWith.IsPrimaryKey = false;
				colvarInContractWith.IsForeignKey = false;
				colvarInContractWith.IsReadOnly = false;
				colvarInContractWith.DefaultSetting = @"";
				colvarInContractWith.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInContractWith);
				
				TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
				colvarSellerEmail.ColumnName = "seller_email";
				colvarSellerEmail.DataType = DbType.String;
				colvarSellerEmail.MaxLength = 256;
				colvarSellerEmail.AutoIncrement = false;
				colvarSellerEmail.IsNullable = true;
				colvarSellerEmail.IsPrimaryKey = false;
				colvarSellerEmail.IsForeignKey = false;
				colvarSellerEmail.IsReadOnly = false;
				colvarSellerEmail.DefaultSetting = @"";
				colvarSellerEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerEmail);
				
				TableSchema.TableColumn colvarSellerMobile = new TableSchema.TableColumn(schema);
				colvarSellerMobile.ColumnName = "seller_mobile";
				colvarSellerMobile.DataType = DbType.AnsiString;
				colvarSellerMobile.MaxLength = 20;
				colvarSellerMobile.AutoIncrement = false;
				colvarSellerMobile.IsNullable = true;
				colvarSellerMobile.IsPrimaryKey = false;
				colvarSellerMobile.IsForeignKey = false;
				colvarSellerMobile.IsReadOnly = false;
				colvarSellerMobile.DefaultSetting = @"";
				colvarSellerMobile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerMobile);
				
				TableSchema.TableColumn colvarContractValidDate = new TableSchema.TableColumn(schema);
				colvarContractValidDate.ColumnName = "contract_valid_date";
				colvarContractValidDate.DataType = DbType.DateTime;
				colvarContractValidDate.MaxLength = 0;
				colvarContractValidDate.AutoIncrement = false;
				colvarContractValidDate.IsNullable = true;
				colvarContractValidDate.IsPrimaryKey = false;
				colvarContractValidDate.IsForeignKey = false;
				colvarContractValidDate.IsReadOnly = false;
				colvarContractValidDate.DefaultSetting = @"";
				colvarContractValidDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContractValidDate);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				
						colvarSellerGuid.DefaultSetting = @"(CONVERT([uniqueidentifier],CONVERT([binary],(0),(0)),(0)))";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("vbs_company_detail",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		[XmlAttribute("Email")]
		[Bindable(true)]
		public string Email 
		{
			get { return GetColumnValue<string>(Columns.Email); }
			set { SetColumnValue(Columns.Email, value); }
		}
		
		[XmlAttribute("Mobile")]
		[Bindable(true)]
		public string Mobile 
		{
			get { return GetColumnValue<string>(Columns.Mobile); }
			set { SetColumnValue(Columns.Mobile, value); }
		}
		
		[XmlAttribute("TownshipId")]
		[Bindable(true)]
		public int? TownshipId 
		{
			get { return GetColumnValue<int?>(Columns.TownshipId); }
			set { SetColumnValue(Columns.TownshipId, value); }
		}
		
		[XmlAttribute("InvoiceTitle")]
		[Bindable(true)]
		public string InvoiceTitle 
		{
			get { return GetColumnValue<string>(Columns.InvoiceTitle); }
			set { SetColumnValue(Columns.InvoiceTitle, value); }
		}
		
		[XmlAttribute("InvoiceComId")]
		[Bindable(true)]
		public string InvoiceComId 
		{
			get { return GetColumnValue<string>(Columns.InvoiceComId); }
			set { SetColumnValue(Columns.InvoiceComId, value); }
		}
		
		[XmlAttribute("InvoiceName")]
		[Bindable(true)]
		public string InvoiceName 
		{
			get { return GetColumnValue<string>(Columns.InvoiceName); }
			set { SetColumnValue(Columns.InvoiceName, value); }
		}
		
		[XmlAttribute("InvoiceAddress")]
		[Bindable(true)]
		public string InvoiceAddress 
		{
			get { return GetColumnValue<string>(Columns.InvoiceAddress); }
			set { SetColumnValue(Columns.InvoiceAddress, value); }
		}
		
		[XmlAttribute("AccountNo")]
		[Bindable(true)]
		public string AccountNo 
		{
			get { return GetColumnValue<string>(Columns.AccountNo); }
			set { SetColumnValue(Columns.AccountNo, value); }
		}
		
		[XmlAttribute("AccountBankCode")]
		[Bindable(true)]
		public string AccountBankCode 
		{
			get { return GetColumnValue<string>(Columns.AccountBankCode); }
			set { SetColumnValue(Columns.AccountBankCode, value); }
		}
		
		[XmlAttribute("AccountBranchCode")]
		[Bindable(true)]
		public string AccountBranchCode 
		{
			get { return GetColumnValue<string>(Columns.AccountBranchCode); }
			set { SetColumnValue(Columns.AccountBranchCode, value); }
		}
		
		[XmlAttribute("AccountName")]
		[Bindable(true)]
		public string AccountName 
		{
			get { return GetColumnValue<string>(Columns.AccountName); }
			set { SetColumnValue(Columns.AccountName, value); }
		}
		
		[XmlAttribute("AccountId")]
		[Bindable(true)]
		public string AccountId 
		{
			get { return GetColumnValue<string>(Columns.AccountId); }
			set { SetColumnValue(Columns.AccountId, value); }
		}
		
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public int? ModifyId 
		{
			get { return GetColumnValue<int?>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName 
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}
		
		[XmlAttribute("InContractWith")]
		[Bindable(true)]
		public string InContractWith 
		{
			get { return GetColumnValue<string>(Columns.InContractWith); }
			set { SetColumnValue(Columns.InContractWith, value); }
		}
		
		[XmlAttribute("SellerEmail")]
		[Bindable(true)]
		public string SellerEmail 
		{
			get { return GetColumnValue<string>(Columns.SellerEmail); }
			set { SetColumnValue(Columns.SellerEmail, value); }
		}
		
		[XmlAttribute("SellerMobile")]
		[Bindable(true)]
		public string SellerMobile 
		{
			get { return GetColumnValue<string>(Columns.SellerMobile); }
			set { SetColumnValue(Columns.SellerMobile, value); }
		}
		
		[XmlAttribute("ContractValidDate")]
		[Bindable(true)]
		public DateTime? ContractValidDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ContractValidDate); }
			set { SetColumnValue(Columns.ContractValidDate, value); }
		}
		
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn EmailColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MobileColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn TownshipIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceTitleColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceComIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceNameColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceAddressColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountNoColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountBankCodeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountBranchCodeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountNameColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerNameColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn InContractWithColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerEmailColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerMobileColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn ContractValidDateColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string UserId = @"user_id";
			 public static string Email = @"email";
			 public static string Mobile = @"mobile";
			 public static string TownshipId = @"township_id";
			 public static string InvoiceTitle = @"invoice_title";
			 public static string InvoiceComId = @"invoice_com_id";
			 public static string InvoiceName = @"invoice_name";
			 public static string InvoiceAddress = @"invoice_address";
			 public static string AccountNo = @"account_no";
			 public static string AccountBankCode = @"account_bank_code";
			 public static string AccountBranchCode = @"account_branch_code";
			 public static string AccountName = @"account_name";
			 public static string AccountId = @"account_id";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string SellerName = @"seller_name";
			 public static string InContractWith = @"in_contract_with";
			 public static string SellerEmail = @"seller_email";
			 public static string SellerMobile = @"seller_mobile";
			 public static string ContractValidDate = @"contract_valid_date";
			 public static string SellerGuid = @"seller_guid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponDealTimeSlot class.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealTimeSlotCollection : ReadOnlyList<ViewPponDealTimeSlot, ViewPponDealTimeSlotCollection>
    {
        public ViewPponDealTimeSlotCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_deal_time_slot view.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealTimeSlot : ReadOnlyRecord<ViewPponDealTimeSlot>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_deal_time_slot", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_GUID";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;

                schema.Columns.Add(colvarCityId);

                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = false;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;

                schema.Columns.Add(colvarSequence);

                TableSchema.TableColumn colvarEffectiveStart = new TableSchema.TableColumn(schema);
                colvarEffectiveStart.ColumnName = "effective_start";
                colvarEffectiveStart.DataType = DbType.DateTime;
                colvarEffectiveStart.MaxLength = 0;
                colvarEffectiveStart.AutoIncrement = false;
                colvarEffectiveStart.IsNullable = false;
                colvarEffectiveStart.IsPrimaryKey = false;
                colvarEffectiveStart.IsForeignKey = false;
                colvarEffectiveStart.IsReadOnly = false;

                schema.Columns.Add(colvarEffectiveStart);

                TableSchema.TableColumn colvarEffectiveEnd = new TableSchema.TableColumn(schema);
                colvarEffectiveEnd.ColumnName = "effective_end";
                colvarEffectiveEnd.DataType = DbType.DateTime;
                colvarEffectiveEnd.MaxLength = 0;
                colvarEffectiveEnd.AutoIncrement = false;
                colvarEffectiveEnd.IsNullable = false;
                colvarEffectiveEnd.IsPrimaryKey = false;
                colvarEffectiveEnd.IsForeignKey = false;
                colvarEffectiveEnd.IsReadOnly = false;

                schema.Columns.Add(colvarEffectiveEnd);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
                colvarEventName.ColumnName = "event_name";
                colvarEventName.DataType = DbType.String;
                colvarEventName.MaxLength = 400;
                colvarEventName.AutoIncrement = false;
                colvarEventName.IsNullable = true;
                colvarEventName.IsPrimaryKey = false;
                colvarEventName.IsForeignKey = false;
                colvarEventName.IsReadOnly = false;

                schema.Columns.Add(colvarEventName);

                TableSchema.TableColumn colvarSubjectName = new TableSchema.TableColumn(schema);
                colvarSubjectName.ColumnName = "subject_name";
                colvarSubjectName.DataType = DbType.String;
                colvarSubjectName.MaxLength = 150;
                colvarSubjectName.AutoIncrement = false;
                colvarSubjectName.IsNullable = true;
                colvarSubjectName.IsPrimaryKey = false;
                colvarSubjectName.IsForeignKey = false;
                colvarSubjectName.IsReadOnly = false;

                schema.Columns.Add(colvarSubjectName);

                TableSchema.TableColumn colvarIsInTurn = new TableSchema.TableColumn(schema);
                colvarIsInTurn.ColumnName = "is_in_turn";
                colvarIsInTurn.DataType = DbType.Boolean;
                colvarIsInTurn.MaxLength = 0;
                colvarIsInTurn.AutoIncrement = false;
                colvarIsInTurn.IsNullable = false;
                colvarIsInTurn.IsPrimaryKey = false;
                colvarIsInTurn.IsForeignKey = false;
                colvarIsInTurn.IsReadOnly = false;

                schema.Columns.Add(colvarIsInTurn);

                TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
                colvarDepartment.ColumnName = "department";
                colvarDepartment.DataType = DbType.Int32;
                colvarDepartment.MaxLength = 0;
                colvarDepartment.AutoIncrement = false;
                colvarDepartment.IsNullable = false;
                colvarDepartment.IsPrimaryKey = false;
                colvarDepartment.IsForeignKey = false;
                colvarDepartment.IsReadOnly = false;

                schema.Columns.Add(colvarDepartment);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;

                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
                colvarAppTitle.ColumnName = "app_title";
                colvarAppTitle.DataType = DbType.String;
                colvarAppTitle.MaxLength = 120;
                colvarAppTitle.AutoIncrement = false;
                colvarAppTitle.IsNullable = true;
                colvarAppTitle.IsPrimaryKey = false;
                colvarAppTitle.IsForeignKey = false;
                colvarAppTitle.IsReadOnly = false;

                schema.Columns.Add(colvarAppTitle);

                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourStatus);

                TableSchema.TableColumn colvarIsLockSeq = new TableSchema.TableColumn(schema);
                colvarIsLockSeq.ColumnName = "is_lock_seq";
                colvarIsLockSeq.DataType = DbType.Boolean;
                colvarIsLockSeq.MaxLength = 0;
                colvarIsLockSeq.AutoIncrement = false;
                colvarIsLockSeq.IsNullable = false;
                colvarIsLockSeq.IsPrimaryKey = false;
                colvarIsLockSeq.IsForeignKey = false;
                colvarIsLockSeq.IsReadOnly = false;

                schema.Columns.Add(colvarIsLockSeq);

                TableSchema.TableColumn colvarIsHotDeal = new TableSchema.TableColumn(schema);
                colvarIsHotDeal.ColumnName = "is_hot_deal";
                colvarIsHotDeal.DataType = DbType.Boolean;
                colvarIsHotDeal.MaxLength = 0;
                colvarIsHotDeal.AutoIncrement = false;
                colvarIsHotDeal.IsNullable = false;
                colvarIsHotDeal.IsPrimaryKey = false;
                colvarIsHotDeal.IsForeignKey = false;
                colvarIsHotDeal.IsReadOnly = false;

                schema.Columns.Add(colvarIsHotDeal);

                TableSchema.TableColumn colvarIsWms = new TableSchema.TableColumn(schema);
                colvarIsWms.ColumnName = "is_wms";
                colvarIsWms.DataType = DbType.Boolean;
                colvarIsWms.MaxLength = 0;
                colvarIsWms.AutoIncrement = false;
                colvarIsWms.IsNullable = false;
                colvarIsWms.IsPrimaryKey = false;
                colvarIsWms.IsForeignKey = false;
                colvarIsWms.IsReadOnly = false;

                schema.Columns.Add(colvarIsWms);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_deal_time_slot",schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewPponDealTimeSlot()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponDealTimeSlot(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewPponDealTimeSlot(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewPponDealTimeSlot(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid
        {
            get
            {
                return GetColumnValue<Guid>("business_hour_GUID");
            }
            set
            {
                SetColumnValue("business_hour_GUID", value);
            }
        }

        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId
        {
            get
            {
                return GetColumnValue<int>("city_id");
            }
            set
            {
                SetColumnValue("city_id", value);
            }
        }

        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public int Sequence
        {
            get
            {
                return GetColumnValue<int>("sequence");
            }
            set
            {
                SetColumnValue("sequence", value);
            }
        }

        [XmlAttribute("EffectiveStart")]
        [Bindable(true)]
        public DateTime EffectiveStart
        {
            get
            {
                return GetColumnValue<DateTime>("effective_start");
            }
            set
            {
                SetColumnValue("effective_start", value);
            }
        }

        [XmlAttribute("EffectiveEnd")]
        [Bindable(true)]
        public DateTime EffectiveEnd
        {
            get
            {
                return GetColumnValue<DateTime>("effective_end");
            }
            set
            {
                SetColumnValue("effective_end", value);
            }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get
            {
                return GetColumnValue<int>("status");
            }
            set
            {
                SetColumnValue("status", value);
            }
        }

        [XmlAttribute("EventName")]
        [Bindable(true)]
        public string EventName
        {
            get
            {
                return GetColumnValue<string>("event_name");
            }
            set
            {
                SetColumnValue("event_name", value);
            }
        }

        [XmlAttribute("SubjectName")]
        [Bindable(true)]
        public string SubjectName
        {
            get
            {
                return GetColumnValue<string>("subject_name");
            }
            set
            {
                SetColumnValue("subject_name", value);
            }
        }

        [XmlAttribute("IsInTurn")]
        [Bindable(true)]
        public bool IsInTurn
        {
            get
            {
                return GetColumnValue<bool>("is_in_turn");
            }
            set
            {
                SetColumnValue("is_in_turn", value);
            }
        }

        [XmlAttribute("Department")]
        [Bindable(true)]
        public int Department
        {
            get
            {
                return GetColumnValue<int>("department");
            }
            set
            {
                SetColumnValue("department", value);
            }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get
            {
                return GetColumnValue<string>("item_name");
            }
            set
            {
                SetColumnValue("item_name", value);
            }
        }

        [XmlAttribute("AppTitle")]
        [Bindable(true)]
        public string AppTitle
        {
            get
            {
                return GetColumnValue<string>("app_title");
            }
            set
            {
                SetColumnValue("app_title", value);
            }
        }

        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus
        {
            get
            {
                return GetColumnValue<int>("business_hour_status");
            }
            set
            {
                SetColumnValue("business_hour_status", value);
            }
        }

        [XmlAttribute("IsLockSeq")]
        [Bindable(true)]
        public bool IsLockSeq
        {
            get
            {
                return GetColumnValue<bool>("is_lock_seq");
            }
            set
            {
                SetColumnValue("is_lock_seq", value);
            }
        }

        [XmlAttribute("IsHotDeal")]
        [Bindable(true)]
        public bool IsHotDeal
        {
            get
            {
                return GetColumnValue<bool>("is_hot_deal");
            }
            set
            {
                SetColumnValue("is_hot_deal", value);
            }
        }

        [XmlAttribute("IsWms")]
        [Bindable(true)]
        public bool IsWms
        {
            get
            {
                return GetColumnValue<bool>("is_wms");
            }
            set
            {
                SetColumnValue("is_wms", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string BusinessHourGuid = @"business_hour_GUID";

            public static string CityId = @"city_id";

            public static string Sequence = @"sequence";

            public static string EffectiveStart = @"effective_start";

            public static string EffectiveEnd = @"effective_end";

            public static string Status = @"status";

            public static string EventName = @"event_name";

            public static string SubjectName = @"subject_name";

            public static string IsInTurn = @"is_in_turn";

            public static string Department = @"department";

            public static string ItemName = @"item_name";

            public static string AppTitle = @"app_title";

            public static string BusinessHourStatus = @"business_hour_status";

            public static string IsLockSeq = @"is_lock_seq";

            public static string IsHotDeal = @"is_hot_deal";

            public static string IsWms = @"is_wms";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the UserGetCardDailyReport class.
	/// </summary>
    [Serializable]
	public partial class UserGetCardDailyReportCollection : RepositoryList<UserGetCardDailyReport, UserGetCardDailyReportCollection>
	{	   
		public UserGetCardDailyReportCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>UserGetCardDailyReportCollection</returns>
		public UserGetCardDailyReportCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                UserGetCardDailyReport o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the user_get_card_daily_report table.
	/// </summary>
	[Serializable]
	public partial class UserGetCardDailyReport : RepositoryRecord<UserGetCardDailyReport>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public UserGetCardDailyReport()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public UserGetCardDailyReport(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("user_get_card_daily_report", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
				colvarSellerUserId.ColumnName = "seller_user_id";
				colvarSellerUserId.DataType = DbType.Int32;
				colvarSellerUserId.MaxLength = 0;
				colvarSellerUserId.AutoIncrement = false;
				colvarSellerUserId.IsNullable = false;
				colvarSellerUserId.IsPrimaryKey = false;
				colvarSellerUserId.IsForeignKey = false;
				colvarSellerUserId.IsReadOnly = false;
				colvarSellerUserId.DefaultSetting = @"";
				colvarSellerUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerUserId);
				
				TableSchema.TableColumn colvarReportDate = new TableSchema.TableColumn(schema);
				colvarReportDate.ColumnName = "report_date";
				colvarReportDate.DataType = DbType.DateTime;
				colvarReportDate.MaxLength = 0;
				colvarReportDate.AutoIncrement = false;
				colvarReportDate.IsNullable = false;
				colvarReportDate.IsPrimaryKey = true;
				colvarReportDate.IsForeignKey = false;
				colvarReportDate.IsReadOnly = false;
				colvarReportDate.DefaultSetting = @"";
				colvarReportDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReportDate);
				
				TableSchema.TableColumn colvarTotalMemberCount = new TableSchema.TableColumn(schema);
				colvarTotalMemberCount.ColumnName = "total_member_count";
				colvarTotalMemberCount.DataType = DbType.Int32;
				colvarTotalMemberCount.MaxLength = 0;
				colvarTotalMemberCount.AutoIncrement = false;
				colvarTotalMemberCount.IsNullable = false;
				colvarTotalMemberCount.IsPrimaryKey = false;
				colvarTotalMemberCount.IsForeignKey = false;
				colvarTotalMemberCount.IsReadOnly = false;
				
						colvarTotalMemberCount.DefaultSetting = @"((0))";
				colvarTotalMemberCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalMemberCount);
				
				TableSchema.TableColumn colvarDailyNewMemberCount = new TableSchema.TableColumn(schema);
				colvarDailyNewMemberCount.ColumnName = "daily_new_member_count";
				colvarDailyNewMemberCount.DataType = DbType.Int32;
				colvarDailyNewMemberCount.MaxLength = 0;
				colvarDailyNewMemberCount.AutoIncrement = false;
				colvarDailyNewMemberCount.IsNullable = false;
				colvarDailyNewMemberCount.IsPrimaryKey = false;
				colvarDailyNewMemberCount.IsForeignKey = false;
				colvarDailyNewMemberCount.IsReadOnly = false;
				
						colvarDailyNewMemberCount.DefaultSetting = @"((0))";
				colvarDailyNewMemberCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDailyNewMemberCount);
				
				TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
				colvarCardGroupId.ColumnName = "card_group_id";
				colvarCardGroupId.DataType = DbType.Int32;
				colvarCardGroupId.MaxLength = 0;
				colvarCardGroupId.AutoIncrement = false;
				colvarCardGroupId.IsNullable = false;
				colvarCardGroupId.IsPrimaryKey = true;
				colvarCardGroupId.IsForeignKey = false;
				colvarCardGroupId.IsReadOnly = false;
				
						colvarCardGroupId.DefaultSetting = @"((0))";
				colvarCardGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardGroupId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("user_get_card_daily_report",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("SellerUserId")]
		[Bindable(true)]
		public int SellerUserId 
		{
			get { return GetColumnValue<int>(Columns.SellerUserId); }
			set { SetColumnValue(Columns.SellerUserId, value); }
		}
		  
		[XmlAttribute("ReportDate")]
		[Bindable(true)]
		public DateTime ReportDate 
		{
			get { return GetColumnValue<DateTime>(Columns.ReportDate); }
			set { SetColumnValue(Columns.ReportDate, value); }
		}
		  
		[XmlAttribute("TotalMemberCount")]
		[Bindable(true)]
		public int TotalMemberCount 
		{
			get { return GetColumnValue<int>(Columns.TotalMemberCount); }
			set { SetColumnValue(Columns.TotalMemberCount, value); }
		}
		  
		[XmlAttribute("DailyNewMemberCount")]
		[Bindable(true)]
		public int DailyNewMemberCount 
		{
			get { return GetColumnValue<int>(Columns.DailyNewMemberCount); }
			set { SetColumnValue(Columns.DailyNewMemberCount, value); }
		}
		  
		[XmlAttribute("CardGroupId")]
		[Bindable(true)]
		public int CardGroupId 
		{
			get { return GetColumnValue<int>(Columns.CardGroupId); }
			set { SetColumnValue(Columns.CardGroupId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn SellerUserIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ReportDateColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalMemberCountColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn DailyNewMemberCountColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CardGroupIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string SellerUserId = @"seller_user_id";
			 public static string ReportDate = @"report_date";
			 public static string TotalMemberCount = @"total_member_count";
			 public static string DailyNewMemberCount = @"daily_new_member_count";
			 public static string CardGroupId = @"card_group_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpPointRuleChangeLog class.
	/// </summary>
    [Serializable]
	public partial class PcpPointRuleChangeLogCollection : RepositoryList<PcpPointRuleChangeLog, PcpPointRuleChangeLogCollection>
	{	   
		public PcpPointRuleChangeLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpPointRuleChangeLogCollection</returns>
		public PcpPointRuleChangeLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpPointRuleChangeLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_point_rule_change_log table.
	/// </summary>
	[Serializable]
	public partial class PcpPointRuleChangeLog : RepositoryRecord<PcpPointRuleChangeLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpPointRuleChangeLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpPointRuleChangeLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_point_rule_change_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
				colvarGroupId.ColumnName = "group_id";
				colvarGroupId.DataType = DbType.Int32;
				colvarGroupId.MaxLength = 0;
				colvarGroupId.AutoIncrement = false;
				colvarGroupId.IsNullable = false;
				colvarGroupId.IsPrimaryKey = false;
				colvarGroupId.IsForeignKey = false;
				colvarGroupId.IsReadOnly = false;
				colvarGroupId.DefaultSetting = @"";
				colvarGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupId);
				
				TableSchema.TableColumn colvarRuleId = new TableSchema.TableColumn(schema);
				colvarRuleId.ColumnName = "rule_id";
				colvarRuleId.DataType = DbType.Int32;
				colvarRuleId.MaxLength = 0;
				colvarRuleId.AutoIncrement = false;
				colvarRuleId.IsNullable = false;
				colvarRuleId.IsPrimaryKey = false;
				colvarRuleId.IsForeignKey = false;
				colvarRuleId.IsReadOnly = false;
				colvarRuleId.DefaultSetting = @"";
				colvarRuleId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRuleId);
				
				TableSchema.TableColumn colvarRuleType = new TableSchema.TableColumn(schema);
				colvarRuleType.ColumnName = "rule_type";
				colvarRuleType.DataType = DbType.Int32;
				colvarRuleType.MaxLength = 0;
				colvarRuleType.AutoIncrement = false;
				colvarRuleType.IsNullable = false;
				colvarRuleType.IsPrimaryKey = false;
				colvarRuleType.IsForeignKey = false;
				colvarRuleType.IsReadOnly = false;
				colvarRuleType.DefaultSetting = @"";
				colvarRuleType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRuleType);
				
				TableSchema.TableColumn colvarChangeType = new TableSchema.TableColumn(schema);
				colvarChangeType.ColumnName = "change_type";
				colvarChangeType.DataType = DbType.Int32;
				colvarChangeType.MaxLength = 0;
				colvarChangeType.AutoIncrement = false;
				colvarChangeType.IsNullable = false;
				colvarChangeType.IsPrimaryKey = false;
				colvarChangeType.IsForeignKey = false;
				colvarChangeType.IsReadOnly = false;
				colvarChangeType.DefaultSetting = @"";
				colvarChangeType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChangeType);
				
				TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
				colvarContent.ColumnName = "content";
				colvarContent.DataType = DbType.String;
				colvarContent.MaxLength = 200;
				colvarContent.AutoIncrement = false;
				colvarContent.IsNullable = true;
				colvarContent.IsPrimaryKey = false;
				colvarContent.IsForeignKey = false;
				colvarContent.IsReadOnly = false;
				colvarContent.DefaultSetting = @"";
				colvarContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContent);
				
				TableSchema.TableColumn colvarPreviousContent = new TableSchema.TableColumn(schema);
				colvarPreviousContent.ColumnName = "previous_content";
				colvarPreviousContent.DataType = DbType.String;
				colvarPreviousContent.MaxLength = 200;
				colvarPreviousContent.AutoIncrement = false;
				colvarPreviousContent.IsNullable = true;
				colvarPreviousContent.IsPrimaryKey = false;
				colvarPreviousContent.IsForeignKey = false;
				colvarPreviousContent.IsReadOnly = false;
				colvarPreviousContent.DefaultSetting = @"";
				colvarPreviousContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPreviousContent);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.Int32;
				colvarModifyId.MaxLength = 0;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = false;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_point_rule_change_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("GroupId")]
		[Bindable(true)]
		public int GroupId 
		{
			get { return GetColumnValue<int>(Columns.GroupId); }
			set { SetColumnValue(Columns.GroupId, value); }
		}
		  
		[XmlAttribute("RuleId")]
		[Bindable(true)]
		public int RuleId 
		{
			get { return GetColumnValue<int>(Columns.RuleId); }
			set { SetColumnValue(Columns.RuleId, value); }
		}
		  
		[XmlAttribute("RuleType")]
		[Bindable(true)]
		public int RuleType 
		{
			get { return GetColumnValue<int>(Columns.RuleType); }
			set { SetColumnValue(Columns.RuleType, value); }
		}
		  
		[XmlAttribute("ChangeType")]
		[Bindable(true)]
		public int ChangeType 
		{
			get { return GetColumnValue<int>(Columns.ChangeType); }
			set { SetColumnValue(Columns.ChangeType, value); }
		}
		  
		[XmlAttribute("Content")]
		[Bindable(true)]
		public string Content 
		{
			get { return GetColumnValue<string>(Columns.Content); }
			set { SetColumnValue(Columns.Content, value); }
		}
		  
		[XmlAttribute("PreviousContent")]
		[Bindable(true)]
		public string PreviousContent 
		{
			get { return GetColumnValue<string>(Columns.PreviousContent); }
			set { SetColumnValue(Columns.PreviousContent, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public int ModifyId 
		{
			get { return GetColumnValue<int>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GroupIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn RuleIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RuleTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ChangeTypeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ContentColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn PreviousContentColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string GroupId = @"group_id";
			 public static string RuleId = @"rule_id";
			 public static string RuleType = @"rule_type";
			 public static string ChangeType = @"change_type";
			 public static string Content = @"content";
			 public static string PreviousContent = @"previous_content";
			 public static string ModifyTime = @"modify_time";
			 public static string ModifyId = @"modify_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

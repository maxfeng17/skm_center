using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Bill class.
	/// </summary>
    [Serializable]
	public partial class BillCollection : RepositoryList<Bill, BillCollection>
	{	   
		public BillCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BillCollection</returns>
		public BillCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Bill o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the bill table.
	/// </summary>
	[Serializable]
	public partial class Bill : RepositoryRecord<Bill>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Bill()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Bill(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("bill", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBillNumber = new TableSchema.TableColumn(schema);
				colvarBillNumber.ColumnName = "bill_number";
				colvarBillNumber.DataType = DbType.String;
				colvarBillNumber.MaxLength = 10;
				colvarBillNumber.AutoIncrement = false;
				colvarBillNumber.IsNullable = false;
				colvarBillNumber.IsPrimaryKey = false;
				colvarBillNumber.IsForeignKey = false;
				colvarBillNumber.IsReadOnly = false;
				colvarBillNumber.DefaultSetting = @"";
				colvarBillNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBillNumber);
				
				TableSchema.TableColumn colvarInvoiceDate = new TableSchema.TableColumn(schema);
				colvarInvoiceDate.ColumnName = "invoice_date";
				colvarInvoiceDate.DataType = DbType.DateTime;
				colvarInvoiceDate.MaxLength = 0;
				colvarInvoiceDate.AutoIncrement = false;
				colvarInvoiceDate.IsNullable = false;
				colvarInvoiceDate.IsPrimaryKey = false;
				colvarInvoiceDate.IsForeignKey = false;
				colvarInvoiceDate.IsReadOnly = false;
				colvarInvoiceDate.DefaultSetting = @"";
				colvarInvoiceDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceDate);
				
				TableSchema.TableColumn colvarInvoiceComId = new TableSchema.TableColumn(schema);
				colvarInvoiceComId.ColumnName = "invoice_com_id";
				colvarInvoiceComId.DataType = DbType.String;
				colvarInvoiceComId.MaxLength = 10;
				colvarInvoiceComId.AutoIncrement = false;
				colvarInvoiceComId.IsNullable = true;
				colvarInvoiceComId.IsPrimaryKey = false;
				colvarInvoiceComId.IsForeignKey = false;
				colvarInvoiceComId.IsReadOnly = false;
				colvarInvoiceComId.DefaultSetting = @"";
				colvarInvoiceComId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceComId);
				
				TableSchema.TableColumn colvarBillSum = new TableSchema.TableColumn(schema);
				colvarBillSum.ColumnName = "bill_sum";
				colvarBillSum.DataType = DbType.Int32;
				colvarBillSum.MaxLength = 0;
				colvarBillSum.AutoIncrement = false;
				colvarBillSum.IsNullable = false;
				colvarBillSum.IsPrimaryKey = false;
				colvarBillSum.IsForeignKey = false;
				colvarBillSum.IsReadOnly = false;
				colvarBillSum.DefaultSetting = @"";
				colvarBillSum.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBillSum);
				
				TableSchema.TableColumn colvarBillSumNotaxed = new TableSchema.TableColumn(schema);
				colvarBillSumNotaxed.ColumnName = "bill_sum_notaxed";
				colvarBillSumNotaxed.DataType = DbType.Int32;
				colvarBillSumNotaxed.MaxLength = 0;
				colvarBillSumNotaxed.AutoIncrement = false;
				colvarBillSumNotaxed.IsNullable = false;
				colvarBillSumNotaxed.IsPrimaryKey = false;
				colvarBillSumNotaxed.IsForeignKey = false;
				colvarBillSumNotaxed.IsReadOnly = false;
				colvarBillSumNotaxed.DefaultSetting = @"";
				colvarBillSumNotaxed.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBillSumNotaxed);
				
				TableSchema.TableColumn colvarBillTax = new TableSchema.TableColumn(schema);
				colvarBillTax.ColumnName = "bill_tax";
				colvarBillTax.DataType = DbType.Int32;
				colvarBillTax.MaxLength = 0;
				colvarBillTax.AutoIncrement = false;
				colvarBillTax.IsNullable = false;
				colvarBillTax.IsPrimaryKey = false;
				colvarBillTax.IsForeignKey = false;
				colvarBillTax.IsReadOnly = false;
				colvarBillTax.DefaultSetting = @"";
				colvarBillTax.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBillTax);
				
				TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
				colvarRemark.ColumnName = "remark";
				colvarRemark.DataType = DbType.String;
				colvarRemark.MaxLength = 250;
				colvarRemark.AutoIncrement = false;
				colvarRemark.IsNullable = true;
				colvarRemark.IsPrimaryKey = false;
				colvarRemark.IsForeignKey = false;
				colvarRemark.IsReadOnly = false;
				colvarRemark.DefaultSetting = @"";
				colvarRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemark);
				
				TableSchema.TableColumn colvarBillSentDate = new TableSchema.TableColumn(schema);
				colvarBillSentDate.ColumnName = "bill_sent_date";
				colvarBillSentDate.DataType = DbType.DateTime;
				colvarBillSentDate.MaxLength = 0;
				colvarBillSentDate.AutoIncrement = false;
				colvarBillSentDate.IsNullable = true;
				colvarBillSentDate.IsPrimaryKey = false;
				colvarBillSentDate.IsForeignKey = false;
				colvarBillSentDate.IsReadOnly = false;
				colvarBillSentDate.DefaultSetting = @"";
				colvarBillSentDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBillSentDate);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = false;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarFinanceGetDate = new TableSchema.TableColumn(schema);
				colvarFinanceGetDate.ColumnName = "finance_get_date";
				colvarFinanceGetDate.DataType = DbType.DateTime;
				colvarFinanceGetDate.MaxLength = 0;
				colvarFinanceGetDate.AutoIncrement = false;
				colvarFinanceGetDate.IsNullable = true;
				colvarFinanceGetDate.IsPrimaryKey = false;
				colvarFinanceGetDate.IsForeignKey = false;
				colvarFinanceGetDate.IsReadOnly = false;
				colvarFinanceGetDate.DefaultSetting = @"";
				colvarFinanceGetDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFinanceGetDate);
				
				TableSchema.TableColumn colvarBuyerType = new TableSchema.TableColumn(schema);
				colvarBuyerType.ColumnName = "buyer_type";
				colvarBuyerType.DataType = DbType.Int32;
				colvarBuyerType.MaxLength = 0;
				colvarBuyerType.AutoIncrement = false;
				colvarBuyerType.IsNullable = false;
				colvarBuyerType.IsPrimaryKey = false;
				colvarBuyerType.IsForeignKey = false;
				colvarBuyerType.IsReadOnly = false;
				
						colvarBuyerType.DefaultSetting = @"((0))";
				colvarBuyerType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuyerType);
				
				TableSchema.TableColumn colvarVendorReceiptType = new TableSchema.TableColumn(schema);
				colvarVendorReceiptType.ColumnName = "vendor_receipt_type";
				colvarVendorReceiptType.DataType = DbType.Int32;
				colvarVendorReceiptType.MaxLength = 0;
				colvarVendorReceiptType.AutoIncrement = false;
				colvarVendorReceiptType.IsNullable = false;
				colvarVendorReceiptType.IsPrimaryKey = false;
				colvarVendorReceiptType.IsForeignKey = false;
				colvarVendorReceiptType.IsReadOnly = false;
				colvarVendorReceiptType.DefaultSetting = @"";
				colvarVendorReceiptType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorReceiptType);
				
				TableSchema.TableColumn colvarIsInputTaxRequired = new TableSchema.TableColumn(schema);
				colvarIsInputTaxRequired.ColumnName = "is_input_tax_required";
				colvarIsInputTaxRequired.DataType = DbType.Boolean;
				colvarIsInputTaxRequired.MaxLength = 0;
				colvarIsInputTaxRequired.AutoIncrement = false;
				colvarIsInputTaxRequired.IsNullable = false;
				colvarIsInputTaxRequired.IsPrimaryKey = false;
				colvarIsInputTaxRequired.IsForeignKey = false;
				colvarIsInputTaxRequired.IsReadOnly = false;
				colvarIsInputTaxRequired.DefaultSetting = @"";
				colvarIsInputTaxRequired.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsInputTaxRequired);
				
				TableSchema.TableColumn colvarBillTypeOtherName = new TableSchema.TableColumn(schema);
				colvarBillTypeOtherName.ColumnName = "bill_type_other_name";
				colvarBillTypeOtherName.DataType = DbType.String;
				colvarBillTypeOtherName.MaxLength = 50;
				colvarBillTypeOtherName.AutoIncrement = false;
				colvarBillTypeOtherName.IsNullable = true;
				colvarBillTypeOtherName.IsPrimaryKey = false;
				colvarBillTypeOtherName.IsForeignKey = false;
				colvarBillTypeOtherName.IsReadOnly = false;
				colvarBillTypeOtherName.DefaultSetting = @"";
				colvarBillTypeOtherName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBillTypeOtherName);
				
				TableSchema.TableColumn colvarVendorEinvoiceId = new TableSchema.TableColumn(schema);
				colvarVendorEinvoiceId.ColumnName = "vendor_einvoice_id";
				colvarVendorEinvoiceId.DataType = DbType.Int32;
				colvarVendorEinvoiceId.MaxLength = 0;
				colvarVendorEinvoiceId.AutoIncrement = false;
				colvarVendorEinvoiceId.IsNullable = true;
				colvarVendorEinvoiceId.IsPrimaryKey = false;
				colvarVendorEinvoiceId.IsForeignKey = false;
				colvarVendorEinvoiceId.IsReadOnly = false;
				colvarVendorEinvoiceId.DefaultSetting = @"";
				colvarVendorEinvoiceId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorEinvoiceId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("bill",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("BillNumber")]
		[Bindable(true)]
		public string BillNumber 
		{
			get { return GetColumnValue<string>(Columns.BillNumber); }
			set { SetColumnValue(Columns.BillNumber, value); }
		}
		  
		[XmlAttribute("InvoiceDate")]
		[Bindable(true)]
		public DateTime InvoiceDate 
		{
			get { return GetColumnValue<DateTime>(Columns.InvoiceDate); }
			set { SetColumnValue(Columns.InvoiceDate, value); }
		}
		  
		[XmlAttribute("InvoiceComId")]
		[Bindable(true)]
		public string InvoiceComId 
		{
			get { return GetColumnValue<string>(Columns.InvoiceComId); }
			set { SetColumnValue(Columns.InvoiceComId, value); }
		}
		  
		[XmlAttribute("BillSum")]
		[Bindable(true)]
		public int BillSum 
		{
			get { return GetColumnValue<int>(Columns.BillSum); }
			set { SetColumnValue(Columns.BillSum, value); }
		}
		  
		[XmlAttribute("BillSumNotaxed")]
		[Bindable(true)]
		public int BillSumNotaxed 
		{
			get { return GetColumnValue<int>(Columns.BillSumNotaxed); }
			set { SetColumnValue(Columns.BillSumNotaxed, value); }
		}
		  
		[XmlAttribute("BillTax")]
		[Bindable(true)]
		public int BillTax 
		{
			get { return GetColumnValue<int>(Columns.BillTax); }
			set { SetColumnValue(Columns.BillTax, value); }
		}
		  
		[XmlAttribute("Remark")]
		[Bindable(true)]
		public string Remark 
		{
			get { return GetColumnValue<string>(Columns.Remark); }
			set { SetColumnValue(Columns.Remark, value); }
		}
		  
		[XmlAttribute("BillSentDate")]
		[Bindable(true)]
		public DateTime? BillSentDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.BillSentDate); }
			set { SetColumnValue(Columns.BillSentDate, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("FinanceGetDate")]
		[Bindable(true)]
		public DateTime? FinanceGetDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.FinanceGetDate); }
			set { SetColumnValue(Columns.FinanceGetDate, value); }
		}
		  
		[XmlAttribute("BuyerType")]
		[Bindable(true)]
		public int BuyerType 
		{
			get { return GetColumnValue<int>(Columns.BuyerType); }
			set { SetColumnValue(Columns.BuyerType, value); }
		}
		  
		[XmlAttribute("VendorReceiptType")]
		[Bindable(true)]
		public int VendorReceiptType 
		{
			get { return GetColumnValue<int>(Columns.VendorReceiptType); }
			set { SetColumnValue(Columns.VendorReceiptType, value); }
		}
		  
		[XmlAttribute("IsInputTaxRequired")]
		[Bindable(true)]
		public bool IsInputTaxRequired 
		{
			get { return GetColumnValue<bool>(Columns.IsInputTaxRequired); }
			set { SetColumnValue(Columns.IsInputTaxRequired, value); }
		}
		  
		[XmlAttribute("BillTypeOtherName")]
		[Bindable(true)]
		public string BillTypeOtherName 
		{
			get { return GetColumnValue<string>(Columns.BillTypeOtherName); }
			set { SetColumnValue(Columns.BillTypeOtherName, value); }
		}
		  
		[XmlAttribute("VendorEinvoiceId")]
		[Bindable(true)]
		public int? VendorEinvoiceId 
		{
			get { return GetColumnValue<int?>(Columns.VendorEinvoiceId); }
			set { SetColumnValue(Columns.VendorEinvoiceId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BillNumberColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceDateColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceComIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn BillSumColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn BillSumNotaxedColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BillTaxColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn BillSentDateColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn FinanceGetDateColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn BuyerTypeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorReceiptTypeColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn IsInputTaxRequiredColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn BillTypeOtherNameColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorEinvoiceIdColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BillNumber = @"bill_number";
			 public static string InvoiceDate = @"invoice_date";
			 public static string InvoiceComId = @"invoice_com_id";
			 public static string BillSum = @"bill_sum";
			 public static string BillSumNotaxed = @"bill_sum_notaxed";
			 public static string BillTax = @"bill_tax";
			 public static string Remark = @"remark";
			 public static string BillSentDate = @"bill_sent_date";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string FinanceGetDate = @"finance_get_date";
			 public static string BuyerType = @"buyer_type";
			 public static string VendorReceiptType = @"vendor_receipt_type";
			 public static string IsInputTaxRequired = @"is_input_tax_required";
			 public static string BillTypeOtherName = @"bill_type_other_name";
			 public static string VendorEinvoiceId = @"vendor_einvoice_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

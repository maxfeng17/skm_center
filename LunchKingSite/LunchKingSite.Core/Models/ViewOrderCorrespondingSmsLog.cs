using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewOrderCorrespondingSmsLog class.
    /// </summary>
    [Serializable]
    public partial class ViewOrderCorrespondingSmsLogCollection : ReadOnlyList<ViewOrderCorrespondingSmsLog, ViewOrderCorrespondingSmsLogCollection>
    {        
        public ViewOrderCorrespondingSmsLogCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_order_corresponding_sms_log view.
    /// </summary>
    [Serializable]
    public partial class ViewOrderCorrespondingSmsLog : ReadOnlyRecord<ViewOrderCorrespondingSmsLog>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_order_corresponding_sms_log", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 50;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = false;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarRelatedOrderId = new TableSchema.TableColumn(schema);
                colvarRelatedOrderId.ColumnName = "related_order_id";
                colvarRelatedOrderId.DataType = DbType.AnsiString;
                colvarRelatedOrderId.MaxLength = 50;
                colvarRelatedOrderId.AutoIncrement = false;
                colvarRelatedOrderId.IsNullable = false;
                colvarRelatedOrderId.IsPrimaryKey = false;
                colvarRelatedOrderId.IsForeignKey = false;
                colvarRelatedOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarRelatedOrderId);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = false;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCreatedTime = new TableSchema.TableColumn(schema);
                colvarCreatedTime.ColumnName = "created_time";
                colvarCreatedTime.DataType = DbType.DateTime;
                colvarCreatedTime.MaxLength = 0;
                colvarCreatedTime.AutoIncrement = false;
                colvarCreatedTime.IsNullable = false;
                colvarCreatedTime.IsPrimaryKey = false;
                colvarCreatedTime.IsForeignKey = false;
                colvarCreatedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreatedTime);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
                colvarMemo.ColumnName = "memo";
                colvarMemo.DataType = DbType.String;
                colvarMemo.MaxLength = 100;
                colvarMemo.AutoIncrement = false;
                colvarMemo.IsNullable = true;
                colvarMemo.IsPrimaryKey = false;
                colvarMemo.IsForeignKey = false;
                colvarMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemo);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
                colvarSequenceNumber.ColumnName = "sequence_number";
                colvarSequenceNumber.DataType = DbType.AnsiString;
                colvarSequenceNumber.MaxLength = 50;
                colvarSequenceNumber.AutoIncrement = false;
                colvarSequenceNumber.IsNullable = false;
                colvarSequenceNumber.IsPrimaryKey = false;
                colvarSequenceNumber.IsForeignKey = false;
                colvarSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequenceNumber);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 50;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = false;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = false;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponId);
                
                TableSchema.TableColumn colvarSmsStatus = new TableSchema.TableColumn(schema);
                colvarSmsStatus.ColumnName = "sms_status";
                colvarSmsStatus.DataType = DbType.Int32;
                colvarSmsStatus.MaxLength = 0;
                colvarSmsStatus.AutoIncrement = false;
                colvarSmsStatus.IsNullable = true;
                colvarSmsStatus.IsPrimaryKey = false;
                colvarSmsStatus.IsForeignKey = false;
                colvarSmsStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarSmsStatus);
                
                TableSchema.TableColumn colvarSmsCreateTime = new TableSchema.TableColumn(schema);
                colvarSmsCreateTime.ColumnName = "sms_create_time";
                colvarSmsCreateTime.DataType = DbType.DateTime;
                colvarSmsCreateTime.MaxLength = 0;
                colvarSmsCreateTime.AutoIncrement = false;
                colvarSmsCreateTime.IsNullable = true;
                colvarSmsCreateTime.IsPrimaryKey = false;
                colvarSmsCreateTime.IsForeignKey = false;
                colvarSmsCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarSmsCreateTime);
                
                TableSchema.TableColumn colvarTrustStatus = new TableSchema.TableColumn(schema);
                colvarTrustStatus.ColumnName = "trust_status";
                colvarTrustStatus.DataType = DbType.Int32;
                colvarTrustStatus.MaxLength = 0;
                colvarTrustStatus.AutoIncrement = false;
                colvarTrustStatus.IsNullable = false;
                colvarTrustStatus.IsPrimaryKey = false;
                colvarTrustStatus.IsForeignKey = false;
                colvarTrustStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustStatus);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 250;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_order_corresponding_sms_log",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewOrderCorrespondingSmsLog()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewOrderCorrespondingSmsLog(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewOrderCorrespondingSmsLog(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewOrderCorrespondingSmsLog(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("RelatedOrderId")]
        [Bindable(true)]
        public string RelatedOrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("related_order_id");
		    }
            set 
		    {
			    SetColumnValue("related_order_id", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("CreatedTime")]
        [Bindable(true)]
        public DateTime CreatedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("created_time");
		    }
            set 
		    {
			    SetColumnValue("created_time", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("Memo")]
        [Bindable(true)]
        public string Memo 
	    {
		    get
		    {
			    return GetColumnValue<string>("memo");
		    }
            set 
		    {
			    SetColumnValue("memo", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("SequenceNumber")]
        [Bindable(true)]
        public string SequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("sequence_number");
		    }
            set 
		    {
			    SetColumnValue("sequence_number", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int CouponId 
	    {
		    get
		    {
			    return GetColumnValue<int>("coupon_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_id", value);
            }
        }
	      
        [XmlAttribute("SmsStatus")]
        [Bindable(true)]
        public int? SmsStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("sms_status");
		    }
            set 
		    {
			    SetColumnValue("sms_status", value);
            }
        }
	      
        [XmlAttribute("SmsCreateTime")]
        [Bindable(true)]
        public DateTime? SmsCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("sms_create_time");
		    }
            set 
		    {
			    SetColumnValue("sms_create_time", value);
            }
        }
	      
        [XmlAttribute("TrustStatus")]
        [Bindable(true)]
        public int TrustStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("trust_status");
		    }
            set 
		    {
			    SetColumnValue("trust_status", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string OrderId = @"order_id";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string UniqueId = @"unique_id";
            
            public static string RelatedOrderId = @"related_order_id";
            
            public static string Mobile = @"mobile";
            
            public static string Status = @"status";
            
            public static string CreatedTime = @"created_time";
            
            public static string UserId = @"user_id";
            
            public static string Memo = @"memo";
            
            public static string Type = @"type";
            
            public static string SequenceNumber = @"sequence_number";
            
            public static string Code = @"code";
            
            public static string CouponId = @"coupon_id";
            
            public static string SmsStatus = @"sms_status";
            
            public static string SmsCreateTime = @"sms_create_time";
            
            public static string TrustStatus = @"trust_status";
            
            public static string ItemName = @"item_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

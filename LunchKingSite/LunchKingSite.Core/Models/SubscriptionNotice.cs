using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SubscriptionNotice class.
	/// </summary>
    [Serializable]
	public partial class SubscriptionNoticeCollection : RepositoryList<SubscriptionNotice, SubscriptionNoticeCollection>
	{	   
		public SubscriptionNoticeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SubscriptionNoticeCollection</returns>
		public SubscriptionNoticeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SubscriptionNotice o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the subscription_notice table.
	/// </summary>
	[Serializable]
	public partial class SubscriptionNotice : RepositoryRecord<SubscriptionNotice>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SubscriptionNotice()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SubscriptionNotice(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("subscription_notice", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarDeviceId = new TableSchema.TableColumn(schema);
				colvarDeviceId.ColumnName = "device_id";
				colvarDeviceId.DataType = DbType.Int32;
				colvarDeviceId.MaxLength = 0;
				colvarDeviceId.AutoIncrement = false;
				colvarDeviceId.IsNullable = false;
				colvarDeviceId.IsPrimaryKey = false;
				colvarDeviceId.IsForeignKey = true;
				colvarDeviceId.IsReadOnly = false;
				colvarDeviceId.DefaultSetting = @"";
				
					colvarDeviceId.ForeignKeyTableName = "device_token";
				schema.Columns.Add(colvarDeviceId);
				
				TableSchema.TableColumn colvarChannelCategoryId = new TableSchema.TableColumn(schema);
				colvarChannelCategoryId.ColumnName = "channel_category_id";
				colvarChannelCategoryId.DataType = DbType.Int32;
				colvarChannelCategoryId.MaxLength = 0;
				colvarChannelCategoryId.AutoIncrement = false;
				colvarChannelCategoryId.IsNullable = false;
				colvarChannelCategoryId.IsPrimaryKey = false;
				colvarChannelCategoryId.IsForeignKey = false;
				colvarChannelCategoryId.IsReadOnly = false;
				colvarChannelCategoryId.DefaultSetting = @"";
				colvarChannelCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChannelCategoryId);
				
				TableSchema.TableColumn colvarSubcategoryId = new TableSchema.TableColumn(schema);
				colvarSubcategoryId.ColumnName = "subcategory_id";
				colvarSubcategoryId.DataType = DbType.Int32;
				colvarSubcategoryId.MaxLength = 0;
				colvarSubcategoryId.AutoIncrement = false;
				colvarSubcategoryId.IsNullable = true;
				colvarSubcategoryId.IsPrimaryKey = false;
				colvarSubcategoryId.IsForeignKey = false;
				colvarSubcategoryId.IsReadOnly = false;
				colvarSubcategoryId.DefaultSetting = @"";
				colvarSubcategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubcategoryId);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = false;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				colvarEnabled.DefaultSetting = @"";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("subscription_notice",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("DeviceId")]
		[Bindable(true)]
		public int DeviceId 
		{
			get { return GetColumnValue<int>(Columns.DeviceId); }
			set { SetColumnValue(Columns.DeviceId, value); }
		}
		  
		[XmlAttribute("ChannelCategoryId")]
		[Bindable(true)]
		public int ChannelCategoryId 
		{
			get { return GetColumnValue<int>(Columns.ChannelCategoryId); }
			set { SetColumnValue(Columns.ChannelCategoryId, value); }
		}
		  
		[XmlAttribute("SubcategoryId")]
		[Bindable(true)]
		public int? SubcategoryId 
		{
			get { return GetColumnValue<int?>(Columns.SubcategoryId); }
			set { SetColumnValue(Columns.SubcategoryId, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool Enabled 
		{
			get { return GetColumnValue<bool>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DeviceIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ChannelCategoryIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SubcategoryIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string DeviceId = @"device_id";
			 public static string ChannelCategoryId = @"channel_category_id";
			 public static string SubcategoryId = @"subcategory_id";
			 public static string Enabled = @"enabled";
			 public static string CreateTime = @"create_time";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMembershipCardGroup class.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardGroupCollection : ReadOnlyList<ViewMembershipCardGroup, ViewMembershipCardGroupCollection>
    {        
        public ViewMembershipCardGroupCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_membership_card_group view.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardGroup : ReadOnlyRecord<ViewMembershipCardGroup>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_membership_card_group", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
                colvarCardGroupId.ColumnName = "card_group_id";
                colvarCardGroupId.DataType = DbType.Int32;
                colvarCardGroupId.MaxLength = 0;
                colvarCardGroupId.AutoIncrement = false;
                colvarCardGroupId.IsNullable = false;
                colvarCardGroupId.IsPrimaryKey = false;
                colvarCardGroupId.IsForeignKey = false;
                colvarCardGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardGroupId);
                
                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = false;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerUserId);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCategoryList = new TableSchema.TableColumn(schema);
                colvarCategoryList.ColumnName = "category_list";
                colvarCategoryList.DataType = DbType.AnsiString;
                colvarCategoryList.MaxLength = -1;
                colvarCategoryList.AutoIncrement = false;
                colvarCategoryList.IsNullable = false;
                colvarCategoryList.IsPrimaryKey = false;
                colvarCategoryList.IsForeignKey = false;
                colvarCategoryList.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryList);
                
                TableSchema.TableColumn colvarHotPoint = new TableSchema.TableColumn(schema);
                colvarHotPoint.ColumnName = "hot_point";
                colvarHotPoint.DataType = DbType.Int32;
                colvarHotPoint.MaxLength = 0;
                colvarHotPoint.AutoIncrement = false;
                colvarHotPoint.IsNullable = false;
                colvarHotPoint.IsPrimaryKey = false;
                colvarHotPoint.IsForeignKey = false;
                colvarHotPoint.IsReadOnly = false;
                
                schema.Columns.Add(colvarHotPoint);
                
                TableSchema.TableColumn colvarCardType = new TableSchema.TableColumn(schema);
                colvarCardType.ColumnName = "card_type";
                colvarCardType.DataType = DbType.Int32;
                colvarCardType.MaxLength = 0;
                colvarCardType.AutoIncrement = false;
                colvarCardType.IsNullable = false;
                colvarCardType.IsPrimaryKey = false;
                colvarCardType.IsForeignKey = false;
                colvarCardType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardType);
                
                TableSchema.TableColumn colvarIsPromo = new TableSchema.TableColumn(schema);
                colvarIsPromo.ColumnName = "is_promo";
                colvarIsPromo.DataType = DbType.Boolean;
                colvarIsPromo.MaxLength = 0;
                colvarIsPromo.AutoIncrement = false;
                colvarIsPromo.IsNullable = false;
                colvarIsPromo.IsPrimaryKey = false;
                colvarIsPromo.IsForeignKey = false;
                colvarIsPromo.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsPromo);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarPublishTime = new TableSchema.TableColumn(schema);
                colvarPublishTime.ColumnName = "publish_time";
                colvarPublishTime.DataType = DbType.DateTime;
                colvarPublishTime.MaxLength = 0;
                colvarPublishTime.AutoIncrement = false;
                colvarPublishTime.IsNullable = true;
                colvarPublishTime.IsPrimaryKey = false;
                colvarPublishTime.IsForeignKey = false;
                colvarPublishTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarPublishTime);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_membership_card_group",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMembershipCardGroup()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMembershipCardGroup(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMembershipCardGroup(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMembershipCardGroup(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CardGroupId")]
        [Bindable(true)]
        public int CardGroupId 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_group_id");
		    }
            set 
		    {
			    SetColumnValue("card_group_id", value);
            }
        }
	      
        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int SellerUserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_user_id");
		    }
            set 
		    {
			    SetColumnValue("seller_user_id", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("CategoryList")]
        [Bindable(true)]
        public string CategoryList 
	    {
		    get
		    {
			    return GetColumnValue<string>("category_list");
		    }
            set 
		    {
			    SetColumnValue("category_list", value);
            }
        }
	      
        [XmlAttribute("HotPoint")]
        [Bindable(true)]
        public int HotPoint 
	    {
		    get
		    {
			    return GetColumnValue<int>("hot_point");
		    }
            set 
		    {
			    SetColumnValue("hot_point", value);
            }
        }
	      
        [XmlAttribute("CardType")]
        [Bindable(true)]
        public int CardType 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_type");
		    }
            set 
		    {
			    SetColumnValue("card_type", value);
            }
        }
	      
        [XmlAttribute("IsPromo")]
        [Bindable(true)]
        public bool IsPromo 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_promo");
		    }
            set 
		    {
			    SetColumnValue("is_promo", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("PublishTime")]
        [Bindable(true)]
        public DateTime? PublishTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("publish_time");
		    }
            set 
		    {
			    SetColumnValue("publish_time", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CardGroupId = @"card_group_id";
            
            public static string SellerUserId = @"seller_user_id";
            
            public static string Status = @"status";
            
            public static string CategoryList = @"category_list";
            
            public static string HotPoint = @"hot_point";
            
            public static string CardType = @"card_type";
            
            public static string IsPromo = @"is_promo";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string PublishTime = @"publish_time";
            
            public static string SellerName = @"seller_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

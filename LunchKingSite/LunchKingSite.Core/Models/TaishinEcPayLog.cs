﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the TaishinEcPayLog class.
    /// </summary>
    [Serializable]
    public partial class TaishinEcPayLogCollection : RepositoryList<TaishinEcPayLog, TaishinEcPayLogCollection>
    {
        public TaishinEcPayLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TaishinEcPayLogCollection</returns>
        public TaishinEcPayLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TaishinEcPayLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the taishin_ec_pay_log table.
    /// </summary>
    [Serializable]
    public partial class TaishinEcPayLog : RepositoryRecord<TaishinEcPayLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public TaishinEcPayLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public TaishinEcPayLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("taishin_ec_pay_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 50;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                colvarStoreName.DefaultSetting = @"";
                colvarStoreName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreName);

                TableSchema.TableColumn colvarStoreTel = new TableSchema.TableColumn(schema);
                colvarStoreTel.ColumnName = "store_tel";
                colvarStoreTel.DataType = DbType.AnsiString;
                colvarStoreTel.MaxLength = 50;
                colvarStoreTel.AutoIncrement = false;
                colvarStoreTel.IsNullable = true;
                colvarStoreTel.IsPrimaryKey = false;
                colvarStoreTel.IsForeignKey = false;
                colvarStoreTel.IsReadOnly = false;
                colvarStoreTel.DefaultSetting = @"";
                colvarStoreTel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreTel);

                TableSchema.TableColumn colvarStoreAddress = new TableSchema.TableColumn(schema);
                colvarStoreAddress.ColumnName = "store_address";
                colvarStoreAddress.DataType = DbType.String;
                colvarStoreAddress.MaxLength = 100;
                colvarStoreAddress.AutoIncrement = false;
                colvarStoreAddress.IsNullable = true;
                colvarStoreAddress.IsPrimaryKey = false;
                colvarStoreAddress.IsForeignKey = false;
                colvarStoreAddress.IsReadOnly = false;
                colvarStoreAddress.DefaultSetting = @"";
                colvarStoreAddress.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreAddress);

                TableSchema.TableColumn colvarMerchantTradeDate = new TableSchema.TableColumn(schema);
                colvarMerchantTradeDate.ColumnName = "merchant_trade_date";
                colvarMerchantTradeDate.DataType = DbType.AnsiString;
                colvarMerchantTradeDate.MaxLength = 50;
                colvarMerchantTradeDate.AutoIncrement = false;
                colvarMerchantTradeDate.IsNullable = true;
                colvarMerchantTradeDate.IsPrimaryKey = false;
                colvarMerchantTradeDate.IsForeignKey = false;
                colvarMerchantTradeDate.IsReadOnly = false;
                colvarMerchantTradeDate.DefaultSetting = @"";
                colvarMerchantTradeDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMerchantTradeDate);

                TableSchema.TableColumn colvarTradeAmount = new TableSchema.TableColumn(schema);
                colvarTradeAmount.ColumnName = "trade_amount";
                colvarTradeAmount.DataType = DbType.Int32;
                colvarTradeAmount.MaxLength = 0;
                colvarTradeAmount.AutoIncrement = false;
                colvarTradeAmount.IsNullable = true;
                colvarTradeAmount.IsPrimaryKey = false;
                colvarTradeAmount.IsForeignKey = false;
                colvarTradeAmount.IsReadOnly = false;
                colvarTradeAmount.DefaultSetting = @"";
                colvarTradeAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTradeAmount);

                TableSchema.TableColumn colvarCardNumber = new TableSchema.TableColumn(schema);
                colvarCardNumber.ColumnName = "card_number";
                colvarCardNumber.DataType = DbType.AnsiString;
                colvarCardNumber.MaxLength = 50;
                colvarCardNumber.AutoIncrement = false;
                colvarCardNumber.IsNullable = true;
                colvarCardNumber.IsPrimaryKey = false;
                colvarCardNumber.IsForeignKey = false;
                colvarCardNumber.IsReadOnly = false;
                colvarCardNumber.DefaultSetting = @"";
                colvarCardNumber.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardNumber);

                TableSchema.TableColumn colvarCardName = new TableSchema.TableColumn(schema);
                colvarCardName.ColumnName = "card_name";
                colvarCardName.DataType = DbType.String;
                colvarCardName.MaxLength = 50;
                colvarCardName.AutoIncrement = false;
                colvarCardName.IsNullable = true;
                colvarCardName.IsPrimaryKey = false;
                colvarCardName.IsForeignKey = false;
                colvarCardName.IsReadOnly = false;
                colvarCardName.DefaultSetting = @"";
                colvarCardName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardName);

                TableSchema.TableColumn colvarTradeStatus = new TableSchema.TableColumn(schema);
                colvarTradeStatus.ColumnName = "trade_status";
                colvarTradeStatus.DataType = DbType.AnsiString;
                colvarTradeStatus.MaxLength = 10;
                colvarTradeStatus.AutoIncrement = false;
                colvarTradeStatus.IsNullable = true;
                colvarTradeStatus.IsPrimaryKey = false;
                colvarTradeStatus.IsForeignKey = false;
                colvarTradeStatus.IsReadOnly = false;
                colvarTradeStatus.DefaultSetting = @"";
                colvarTradeStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTradeStatus);

                TableSchema.TableColumn colvarTradeStatusName = new TableSchema.TableColumn(schema);
                colvarTradeStatusName.ColumnName = "trade_status_name";
                colvarTradeStatusName.DataType = DbType.String;
                colvarTradeStatusName.MaxLength = 50;
                colvarTradeStatusName.AutoIncrement = false;
                colvarTradeStatusName.IsNullable = true;
                colvarTradeStatusName.IsPrimaryKey = false;
                colvarTradeStatusName.IsForeignKey = false;
                colvarTradeStatusName.IsReadOnly = false;
                colvarTradeStatusName.DefaultSetting = @"";
                colvarTradeStatusName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTradeStatusName);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = true;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                colvarOrderGuid.DefaultSetting = @"";
                colvarOrderGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderGuid);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("taishin_ec_pay_log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName
        {
            get { return GetColumnValue<string>(Columns.StoreName); }
            set { SetColumnValue(Columns.StoreName, value); }
        }

        [XmlAttribute("StoreTel")]
        [Bindable(true)]
        public string StoreTel
        {
            get { return GetColumnValue<string>(Columns.StoreTel); }
            set { SetColumnValue(Columns.StoreTel, value); }
        }

        [XmlAttribute("StoreAddress")]
        [Bindable(true)]
        public string StoreAddress
        {
            get { return GetColumnValue<string>(Columns.StoreAddress); }
            set { SetColumnValue(Columns.StoreAddress, value); }
        }

        [XmlAttribute("MerchantTradeDate")]
        [Bindable(true)]
        public string MerchantTradeDate
        {
            get { return GetColumnValue<string>(Columns.MerchantTradeDate); }
            set { SetColumnValue(Columns.MerchantTradeDate, value); }
        }

        [XmlAttribute("TradeAmount")]
        [Bindable(true)]
        public int? TradeAmount
        {
            get { return GetColumnValue<int?>(Columns.TradeAmount); }
            set { SetColumnValue(Columns.TradeAmount, value); }
        }

        [XmlAttribute("CardNumber")]
        [Bindable(true)]
        public string CardNumber
        {
            get { return GetColumnValue<string>(Columns.CardNumber); }
            set { SetColumnValue(Columns.CardNumber, value); }
        }

        [XmlAttribute("CardName")]
        [Bindable(true)]
        public string CardName
        {
            get { return GetColumnValue<string>(Columns.CardName); }
            set { SetColumnValue(Columns.CardName, value); }
        }

        [XmlAttribute("TradeStatus")]
        [Bindable(true)]
        public string TradeStatus
        {
            get { return GetColumnValue<string>(Columns.TradeStatus); }
            set { SetColumnValue(Columns.TradeStatus, value); }
        }

        [XmlAttribute("TradeStatusName")]
        [Bindable(true)]
        public string TradeStatusName
        {
            get { return GetColumnValue<string>(Columns.TradeStatusName); }
            set { SetColumnValue(Columns.TradeStatusName, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int? UserId
        {
            get { return GetColumnValue<int?>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid
        {
            get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
            set { SetColumnValue(Columns.OrderGuid, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn StoreNameColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn StoreTelColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn StoreAddressColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn MerchantTradeDateColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn TradeAmountColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CardNumberColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CardNameColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn TradeStatusColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn TradeStatusNameColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[12]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string StoreName = @"store_name";
            public static string StoreTel = @"store_tel";
            public static string StoreAddress = @"store_address";
            public static string MerchantTradeDate = @"merchant_trade_date";
            public static string TradeAmount = @"trade_amount";
            public static string CardNumber = @"card_number";
            public static string CardName = @"card_name";
            public static string TradeStatus = @"trade_status";
            public static string TradeStatusName = @"trade_status_name";
            public static string CreateTime = @"create_time";
            public static string UserId = @"user_id";
            public static string OrderGuid = @"order_guid";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewProposalSeller class.
    /// </summary>
    [Serializable]
    public partial class ViewProposalSellerCollection : ReadOnlyList<ViewProposalSeller, ViewProposalSellerCollection>
    {        
        public ViewProposalSellerCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_proposal_seller view.
    /// </summary>
    [Serializable]
    public partial class ViewProposalSeller : ReadOnlyRecord<ViewProposalSeller>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_proposal_seller", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarReferrerBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarReferrerBusinessHourGuid.ColumnName = "referrer_business_hour_guid";
                colvarReferrerBusinessHourGuid.DataType = DbType.Guid;
                colvarReferrerBusinessHourGuid.MaxLength = 0;
                colvarReferrerBusinessHourGuid.AutoIncrement = false;
                colvarReferrerBusinessHourGuid.IsNullable = true;
                colvarReferrerBusinessHourGuid.IsPrimaryKey = false;
                colvarReferrerBusinessHourGuid.IsForeignKey = false;
                colvarReferrerBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarReferrerBusinessHourGuid);
                
                TableSchema.TableColumn colvarDevelopeSalesId = new TableSchema.TableColumn(schema);
                colvarDevelopeSalesId.ColumnName = "develope_sales_id";
                colvarDevelopeSalesId.DataType = DbType.Int32;
                colvarDevelopeSalesId.MaxLength = 0;
                colvarDevelopeSalesId.AutoIncrement = false;
                colvarDevelopeSalesId.IsNullable = false;
                colvarDevelopeSalesId.IsPrimaryKey = false;
                colvarDevelopeSalesId.IsForeignKey = false;
                colvarDevelopeSalesId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDevelopeSalesId);
                
                TableSchema.TableColumn colvarOperationSalesId = new TableSchema.TableColumn(schema);
                colvarOperationSalesId.ColumnName = "operation_sales_id";
                colvarOperationSalesId.DataType = DbType.Int32;
                colvarOperationSalesId.MaxLength = 0;
                colvarOperationSalesId.AutoIncrement = false;
                colvarOperationSalesId.IsNullable = true;
                colvarOperationSalesId.IsPrimaryKey = false;
                colvarOperationSalesId.IsForeignKey = false;
                colvarOperationSalesId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOperationSalesId);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = false;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarApplyFlag = new TableSchema.TableColumn(schema);
                colvarApplyFlag.ColumnName = "apply_flag";
                colvarApplyFlag.DataType = DbType.Int32;
                colvarApplyFlag.MaxLength = 0;
                colvarApplyFlag.AutoIncrement = false;
                colvarApplyFlag.IsNullable = false;
                colvarApplyFlag.IsPrimaryKey = false;
                colvarApplyFlag.IsForeignKey = false;
                colvarApplyFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarApplyFlag);
                
                TableSchema.TableColumn colvarBusinessCreateFlag = new TableSchema.TableColumn(schema);
                colvarBusinessCreateFlag.ColumnName = "business_create_flag";
                colvarBusinessCreateFlag.DataType = DbType.Int32;
                colvarBusinessCreateFlag.MaxLength = 0;
                colvarBusinessCreateFlag.AutoIncrement = false;
                colvarBusinessCreateFlag.IsNullable = false;
                colvarBusinessCreateFlag.IsPrimaryKey = false;
                colvarBusinessCreateFlag.IsForeignKey = false;
                colvarBusinessCreateFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessCreateFlag);
                
                TableSchema.TableColumn colvarBusinessFlag = new TableSchema.TableColumn(schema);
                colvarBusinessFlag.ColumnName = "business_flag";
                colvarBusinessFlag.DataType = DbType.Int32;
                colvarBusinessFlag.MaxLength = 0;
                colvarBusinessFlag.AutoIncrement = false;
                colvarBusinessFlag.IsNullable = false;
                colvarBusinessFlag.IsPrimaryKey = false;
                colvarBusinessFlag.IsForeignKey = false;
                colvarBusinessFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessFlag);
                
                TableSchema.TableColumn colvarEditFlag = new TableSchema.TableColumn(schema);
                colvarEditFlag.ColumnName = "edit_flag";
                colvarEditFlag.DataType = DbType.Int32;
                colvarEditFlag.MaxLength = 0;
                colvarEditFlag.AutoIncrement = false;
                colvarEditFlag.IsNullable = false;
                colvarEditFlag.IsPrimaryKey = false;
                colvarEditFlag.IsForeignKey = false;
                colvarEditFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarEditFlag);
                
                TableSchema.TableColumn colvarListingFlag = new TableSchema.TableColumn(schema);
                colvarListingFlag.ColumnName = "listing_flag";
                colvarListingFlag.DataType = DbType.Int32;
                colvarListingFlag.MaxLength = 0;
                colvarListingFlag.AutoIncrement = false;
                colvarListingFlag.IsNullable = false;
                colvarListingFlag.IsPrimaryKey = false;
                colvarListingFlag.IsForeignKey = false;
                colvarListingFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarListingFlag);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarProductSpec = new TableSchema.TableColumn(schema);
                colvarProductSpec.ColumnName = "product_spec";
                colvarProductSpec.DataType = DbType.String;
                colvarProductSpec.MaxLength = 4000;
                colvarProductSpec.AutoIncrement = false;
                colvarProductSpec.IsNullable = false;
                colvarProductSpec.IsPrimaryKey = false;
                colvarProductSpec.IsForeignKey = false;
                colvarProductSpec.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductSpec);
                
                TableSchema.TableColumn colvarSpecialFlag = new TableSchema.TableColumn(schema);
                colvarSpecialFlag.ColumnName = "special_flag";
                colvarSpecialFlag.DataType = DbType.Int32;
                colvarSpecialFlag.MaxLength = 0;
                colvarSpecialFlag.AutoIncrement = false;
                colvarSpecialFlag.IsNullable = false;
                colvarSpecialFlag.IsPrimaryKey = false;
                colvarSpecialFlag.IsForeignKey = false;
                colvarSpecialFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarSpecialFlag);
                
                TableSchema.TableColumn colvarMarketAnalysis = new TableSchema.TableColumn(schema);
                colvarMarketAnalysis.ColumnName = "market_analysis";
                colvarMarketAnalysis.DataType = DbType.String;
                colvarMarketAnalysis.MaxLength = 1000;
                colvarMarketAnalysis.AutoIncrement = false;
                colvarMarketAnalysis.IsNullable = false;
                colvarMarketAnalysis.IsPrimaryKey = false;
                colvarMarketAnalysis.IsForeignKey = false;
                colvarMarketAnalysis.IsReadOnly = false;
                
                schema.Columns.Add(colvarMarketAnalysis);
                
                TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
                colvarMemo.ColumnName = "memo";
                colvarMemo.DataType = DbType.String;
                colvarMemo.MaxLength = 1000;
                colvarMemo.AutoIncrement = false;
                colvarMemo.IsNullable = false;
                colvarMemo.IsPrimaryKey = false;
                colvarMemo.IsForeignKey = false;
                colvarMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemo);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 50;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarTempStatus = new TableSchema.TableColumn(schema);
                colvarTempStatus.ColumnName = "temp_status";
                colvarTempStatus.DataType = DbType.Int32;
                colvarTempStatus.MaxLength = 0;
                colvarTempStatus.AutoIncrement = false;
                colvarTempStatus.IsNullable = false;
                colvarTempStatus.IsPrimaryKey = false;
                colvarTempStatus.IsForeignKey = false;
                colvarTempStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarTempStatus);
                
                TableSchema.TableColumn colvarEmpId = new TableSchema.TableColumn(schema);
                colvarEmpId.ColumnName = "emp_id";
                colvarEmpId.DataType = DbType.AnsiString;
                colvarEmpId.MaxLength = 50;
                colvarEmpId.AutoIncrement = false;
                colvarEmpId.IsNullable = false;
                colvarEmpId.IsPrimaryKey = false;
                colvarEmpId.IsForeignKey = false;
                colvarEmpId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEmpId);
                
                TableSchema.TableColumn colvarEmpName = new TableSchema.TableColumn(schema);
                colvarEmpName.ColumnName = "emp_name";
                colvarEmpName.DataType = DbType.String;
                colvarEmpName.MaxLength = 50;
                colvarEmpName.AutoIncrement = false;
                colvarEmpName.IsNullable = true;
                colvarEmpName.IsPrimaryKey = false;
                colvarEmpName.IsForeignKey = false;
                colvarEmpName.IsReadOnly = false;
                
                schema.Columns.Add(colvarEmpName);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarDeptName = new TableSchema.TableColumn(schema);
                colvarDeptName.ColumnName = "dept_name";
                colvarDeptName.DataType = DbType.String;
                colvarDeptName.MaxLength = 100;
                colvarDeptName.AutoIncrement = false;
                colvarDeptName.IsNullable = true;
                colvarDeptName.IsPrimaryKey = false;
                colvarDeptName.IsForeignKey = false;
                colvarDeptName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeptName);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "CompanyName";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 50;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyName);
                
                TableSchema.TableColumn colvarApplyId = new TableSchema.TableColumn(schema);
                colvarApplyId.ColumnName = "apply_id";
                colvarApplyId.DataType = DbType.String;
                colvarApplyId.MaxLength = 100;
                colvarApplyId.AutoIncrement = false;
                colvarApplyId.IsNullable = true;
                colvarApplyId.IsPrimaryKey = false;
                colvarApplyId.IsForeignKey = false;
                colvarApplyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarApplyId);
                
                TableSchema.TableColumn colvarApplyTime = new TableSchema.TableColumn(schema);
                colvarApplyTime.ColumnName = "apply_time";
                colvarApplyTime.DataType = DbType.DateTime;
                colvarApplyTime.MaxLength = 0;
                colvarApplyTime.AutoIncrement = false;
                colvarApplyTime.IsNullable = true;
                colvarApplyTime.IsPrimaryKey = false;
                colvarApplyTime.IsForeignKey = false;
                colvarApplyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarApplyTime);
                
                TableSchema.TableColumn colvarSellerLevel = new TableSchema.TableColumn(schema);
                colvarSellerLevel.ColumnName = "seller_level";
                colvarSellerLevel.DataType = DbType.Int32;
                colvarSellerLevel.MaxLength = 0;
                colvarSellerLevel.AutoIncrement = false;
                colvarSellerLevel.IsNullable = true;
                colvarSellerLevel.IsPrimaryKey = false;
                colvarSellerLevel.IsForeignKey = false;
                colvarSellerLevel.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerLevel);
                
                TableSchema.TableColumn colvarDevelopeDeptId = new TableSchema.TableColumn(schema);
                colvarDevelopeDeptId.ColumnName = "develope_dept_id";
                colvarDevelopeDeptId.DataType = DbType.AnsiString;
                colvarDevelopeDeptId.MaxLength = 10;
                colvarDevelopeDeptId.AutoIncrement = false;
                colvarDevelopeDeptId.IsNullable = false;
                colvarDevelopeDeptId.IsPrimaryKey = false;
                colvarDevelopeDeptId.IsForeignKey = false;
                colvarDevelopeDeptId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDevelopeDeptId);
                
                TableSchema.TableColumn colvarOperationDeptId = new TableSchema.TableColumn(schema);
                colvarOperationDeptId.ColumnName = "operation_dept_id";
                colvarOperationDeptId.DataType = DbType.AnsiString;
                colvarOperationDeptId.MaxLength = 10;
                colvarOperationDeptId.AutoIncrement = false;
                colvarOperationDeptId.IsNullable = true;
                colvarOperationDeptId.IsPrimaryKey = false;
                colvarOperationDeptId.IsForeignKey = false;
                colvarOperationDeptId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOperationDeptId);
                
                TableSchema.TableColumn colvarApproveFlag = new TableSchema.TableColumn(schema);
                colvarApproveFlag.ColumnName = "approve_flag";
                colvarApproveFlag.DataType = DbType.Int32;
                colvarApproveFlag.MaxLength = 0;
                colvarApproveFlag.AutoIncrement = false;
                colvarApproveFlag.IsNullable = false;
                colvarApproveFlag.IsPrimaryKey = false;
                colvarApproveFlag.IsForeignKey = false;
                colvarApproveFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarApproveFlag);
                
                TableSchema.TableColumn colvarPayType = new TableSchema.TableColumn(schema);
                colvarPayType.ColumnName = "pay_type";
                colvarPayType.DataType = DbType.Int32;
                colvarPayType.MaxLength = 0;
                colvarPayType.AutoIncrement = false;
                colvarPayType.IsNullable = false;
                colvarPayType.IsPrimaryKey = false;
                colvarPayType.IsForeignKey = false;
                colvarPayType.IsReadOnly = false;
                
                schema.Columns.Add(colvarPayType);
                
                TableSchema.TableColumn colvarCopyType = new TableSchema.TableColumn(schema);
                colvarCopyType.ColumnName = "copy_type";
                colvarCopyType.DataType = DbType.Int32;
                colvarCopyType.MaxLength = 0;
                colvarCopyType.AutoIncrement = false;
                colvarCopyType.IsNullable = false;
                colvarCopyType.IsPrimaryKey = false;
                colvarCopyType.IsForeignKey = false;
                colvarCopyType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCopyType);
                
                TableSchema.TableColumn colvarSpecialFlagText = new TableSchema.TableColumn(schema);
                colvarSpecialFlagText.ColumnName = "special_flag_text";
                colvarSpecialFlagText.DataType = DbType.String;
                colvarSpecialFlagText.MaxLength = 1073741823;
                colvarSpecialFlagText.AutoIncrement = false;
                colvarSpecialFlagText.IsNullable = false;
                colvarSpecialFlagText.IsPrimaryKey = false;
                colvarSpecialFlagText.IsForeignKey = false;
                colvarSpecialFlagText.IsReadOnly = false;
                
                schema.Columns.Add(colvarSpecialFlagText);
                
                TableSchema.TableColumn colvarOrderTimeS = new TableSchema.TableColumn(schema);
                colvarOrderTimeS.ColumnName = "order_time_s";
                colvarOrderTimeS.DataType = DbType.DateTime;
                colvarOrderTimeS.MaxLength = 0;
                colvarOrderTimeS.AutoIncrement = false;
                colvarOrderTimeS.IsNullable = true;
                colvarOrderTimeS.IsPrimaryKey = false;
                colvarOrderTimeS.IsForeignKey = false;
                colvarOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTimeS);
                
                TableSchema.TableColumn colvarOrderTimeE = new TableSchema.TableColumn(schema);
                colvarOrderTimeE.ColumnName = "order_time_e";
                colvarOrderTimeE.DataType = DbType.DateTime;
                colvarOrderTimeE.MaxLength = 0;
                colvarOrderTimeE.AutoIncrement = false;
                colvarOrderTimeE.IsNullable = true;
                colvarOrderTimeE.IsPrimaryKey = false;
                colvarOrderTimeE.IsForeignKey = false;
                colvarOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTimeE);
                
                TableSchema.TableColumn colvarScheduleExpect = new TableSchema.TableColumn(schema);
                colvarScheduleExpect.ColumnName = "schedule_expect";
                colvarScheduleExpect.DataType = DbType.String;
                colvarScheduleExpect.MaxLength = 150;
                colvarScheduleExpect.AutoIncrement = false;
                colvarScheduleExpect.IsNullable = false;
                colvarScheduleExpect.IsPrimaryKey = false;
                colvarScheduleExpect.IsForeignKey = false;
                colvarScheduleExpect.IsReadOnly = false;
                
                schema.Columns.Add(colvarScheduleExpect);
                
                TableSchema.TableColumn colvarStartUseUnit = new TableSchema.TableColumn(schema);
                colvarStartUseUnit.ColumnName = "start_use_unit";
                colvarStartUseUnit.DataType = DbType.Int32;
                colvarStartUseUnit.MaxLength = 0;
                colvarStartUseUnit.AutoIncrement = false;
                colvarStartUseUnit.IsNullable = false;
                colvarStartUseUnit.IsPrimaryKey = false;
                colvarStartUseUnit.IsForeignKey = false;
                colvarStartUseUnit.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartUseUnit);
                
                TableSchema.TableColumn colvarStartUseText = new TableSchema.TableColumn(schema);
                colvarStartUseText.ColumnName = "start_use_text";
                colvarStartUseText.DataType = DbType.String;
                colvarStartUseText.MaxLength = 500;
                colvarStartUseText.AutoIncrement = false;
                colvarStartUseText.IsNullable = false;
                colvarStartUseText.IsPrimaryKey = false;
                colvarStartUseText.IsForeignKey = false;
                colvarStartUseText.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartUseText);
                
                TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
                colvarShipType.ColumnName = "ship_type";
                colvarShipType.DataType = DbType.Int32;
                colvarShipType.MaxLength = 0;
                colvarShipType.AutoIncrement = false;
                colvarShipType.IsNullable = false;
                colvarShipType.IsPrimaryKey = false;
                colvarShipType.IsForeignKey = false;
                colvarShipType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipType);
                
                TableSchema.TableColumn colvarContractMemo = new TableSchema.TableColumn(schema);
                colvarContractMemo.ColumnName = "contract_memo";
                colvarContractMemo.DataType = DbType.String;
                colvarContractMemo.MaxLength = 1073741823;
                colvarContractMemo.AutoIncrement = false;
                colvarContractMemo.IsNullable = false;
                colvarContractMemo.IsPrimaryKey = false;
                colvarContractMemo.IsForeignKey = false;
                colvarContractMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarContractMemo);
                
                TableSchema.TableColumn colvarDealSubType = new TableSchema.TableColumn(schema);
                colvarDealSubType.ColumnName = "deal_sub_type";
                colvarDealSubType.DataType = DbType.Int32;
                colvarDealSubType.MaxLength = 0;
                colvarDealSubType.AutoIncrement = false;
                colvarDealSubType.IsNullable = false;
                colvarDealSubType.IsPrimaryKey = false;
                colvarDealSubType.IsForeignKey = false;
                colvarDealSubType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealSubType);
                
                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = false;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType);
                
                TableSchema.TableColumn colvarShipText1 = new TableSchema.TableColumn(schema);
                colvarShipText1.ColumnName = "ship_text1";
                colvarShipText1.DataType = DbType.String;
                colvarShipText1.MaxLength = 50;
                colvarShipText1.AutoIncrement = false;
                colvarShipText1.IsNullable = false;
                colvarShipText1.IsPrimaryKey = false;
                colvarShipText1.IsForeignKey = false;
                colvarShipText1.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipText1);
                
                TableSchema.TableColumn colvarShipText2 = new TableSchema.TableColumn(schema);
                colvarShipText2.ColumnName = "ship_text2";
                colvarShipText2.DataType = DbType.String;
                colvarShipText2.MaxLength = 50;
                colvarShipText2.AutoIncrement = false;
                colvarShipText2.IsNullable = false;
                colvarShipText2.IsPrimaryKey = false;
                colvarShipText2.IsForeignKey = false;
                colvarShipText2.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipText2);
                
                TableSchema.TableColumn colvarShipText3 = new TableSchema.TableColumn(schema);
                colvarShipText3.ColumnName = "ship_text3";
                colvarShipText3.DataType = DbType.String;
                colvarShipText3.MaxLength = 50;
                colvarShipText3.AutoIncrement = false;
                colvarShipText3.IsNullable = true;
                colvarShipText3.IsPrimaryKey = false;
                colvarShipText3.IsForeignKey = false;
                colvarShipText3.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipText3);
                
                TableSchema.TableColumn colvarShippingdateType = new TableSchema.TableColumn(schema);
                colvarShippingdateType.ColumnName = "shippingdate_type";
                colvarShippingdateType.DataType = DbType.Int32;
                colvarShippingdateType.MaxLength = 0;
                colvarShippingdateType.AutoIncrement = false;
                colvarShippingdateType.IsNullable = true;
                colvarShippingdateType.IsPrimaryKey = false;
                colvarShippingdateType.IsForeignKey = false;
                colvarShippingdateType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippingdateType);
                
                TableSchema.TableColumn colvarBrandName = new TableSchema.TableColumn(schema);
                colvarBrandName.ColumnName = "brand_name";
                colvarBrandName.DataType = DbType.String;
                colvarBrandName.MaxLength = 100;
                colvarBrandName.AutoIncrement = false;
                colvarBrandName.IsNullable = true;
                colvarBrandName.IsPrimaryKey = false;
                colvarBrandName.IsForeignKey = false;
                colvarBrandName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBrandName);
                
                TableSchema.TableColumn colvarMarketingResource = new TableSchema.TableColumn(schema);
                colvarMarketingResource.ColumnName = "marketing_resource";
                colvarMarketingResource.DataType = DbType.String;
                colvarMarketingResource.MaxLength = 500;
                colvarMarketingResource.AutoIncrement = false;
                colvarMarketingResource.IsNullable = true;
                colvarMarketingResource.IsPrimaryKey = false;
                colvarMarketingResource.IsForeignKey = false;
                colvarMarketingResource.IsReadOnly = false;
                
                schema.Columns.Add(colvarMarketingResource);
                
                TableSchema.TableColumn colvarBusinessPrice = new TableSchema.TableColumn(schema);
                colvarBusinessPrice.ColumnName = "business_price";
                colvarBusinessPrice.DataType = DbType.Int32;
                colvarBusinessPrice.MaxLength = 0;
                colvarBusinessPrice.AutoIncrement = false;
                colvarBusinessPrice.IsNullable = true;
                colvarBusinessPrice.IsPrimaryKey = false;
                colvarBusinessPrice.IsForeignKey = false;
                colvarBusinessPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessPrice);
                
                TableSchema.TableColumn colvarConsumerTime = new TableSchema.TableColumn(schema);
                colvarConsumerTime.ColumnName = "consumer_time";
                colvarConsumerTime.DataType = DbType.Int32;
                colvarConsumerTime.MaxLength = 0;
                colvarConsumerTime.AutoIncrement = false;
                colvarConsumerTime.IsNullable = false;
                colvarConsumerTime.IsPrimaryKey = false;
                colvarConsumerTime.IsForeignKey = false;
                colvarConsumerTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsumerTime);
                
                TableSchema.TableColumn colvarPhotographerAppointFlag = new TableSchema.TableColumn(schema);
                colvarPhotographerAppointFlag.ColumnName = "photographer_appoint_flag";
                colvarPhotographerAppointFlag.DataType = DbType.Int32;
                colvarPhotographerAppointFlag.MaxLength = 0;
                colvarPhotographerAppointFlag.AutoIncrement = false;
                colvarPhotographerAppointFlag.IsNullable = false;
                colvarPhotographerAppointFlag.IsPrimaryKey = false;
                colvarPhotographerAppointFlag.IsForeignKey = false;
                colvarPhotographerAppointFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhotographerAppointFlag);
                
                TableSchema.TableColumn colvarAncestorBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarAncestorBusinessHourGuid.ColumnName = "ancestor_business_hour_guid";
                colvarAncestorBusinessHourGuid.DataType = DbType.Guid;
                colvarAncestorBusinessHourGuid.MaxLength = 0;
                colvarAncestorBusinessHourGuid.AutoIncrement = false;
                colvarAncestorBusinessHourGuid.IsNullable = true;
                colvarAncestorBusinessHourGuid.IsPrimaryKey = false;
                colvarAncestorBusinessHourGuid.IsForeignKey = false;
                colvarAncestorBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarAncestorBusinessHourGuid);
                
                TableSchema.TableColumn colvarAncestorSequenceBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarAncestorSequenceBusinessHourGuid.ColumnName = "ancestor_sequence_business_hour_guid";
                colvarAncestorSequenceBusinessHourGuid.DataType = DbType.Guid;
                colvarAncestorSequenceBusinessHourGuid.MaxLength = 0;
                colvarAncestorSequenceBusinessHourGuid.AutoIncrement = false;
                colvarAncestorSequenceBusinessHourGuid.IsNullable = true;
                colvarAncestorSequenceBusinessHourGuid.IsPrimaryKey = false;
                colvarAncestorSequenceBusinessHourGuid.IsForeignKey = false;
                colvarAncestorSequenceBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarAncestorSequenceBusinessHourGuid);
                
                TableSchema.TableColumn colvarStartUseText2 = new TableSchema.TableColumn(schema);
                colvarStartUseText2.ColumnName = "start_use_text2";
                colvarStartUseText2.DataType = DbType.String;
                colvarStartUseText2.MaxLength = 500;
                colvarStartUseText2.AutoIncrement = false;
                colvarStartUseText2.IsNullable = false;
                colvarStartUseText2.IsPrimaryKey = false;
                colvarStartUseText2.IsForeignKey = false;
                colvarStartUseText2.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartUseText2);
                
                TableSchema.TableColumn colvarSaleMarketAnalysis = new TableSchema.TableColumn(schema);
                colvarSaleMarketAnalysis.ColumnName = "sale_market_analysis";
                colvarSaleMarketAnalysis.DataType = DbType.String;
                colvarSaleMarketAnalysis.MaxLength = -1;
                colvarSaleMarketAnalysis.AutoIncrement = false;
                colvarSaleMarketAnalysis.IsNullable = true;
                colvarSaleMarketAnalysis.IsPrimaryKey = false;
                colvarSaleMarketAnalysis.IsForeignKey = false;
                colvarSaleMarketAnalysis.IsReadOnly = false;
                
                schema.Columns.Add(colvarSaleMarketAnalysis);
                
                TableSchema.TableColumn colvarMediaReportFlag = new TableSchema.TableColumn(schema);
                colvarMediaReportFlag.ColumnName = "media_report_flag";
                colvarMediaReportFlag.DataType = DbType.Int32;
                colvarMediaReportFlag.MaxLength = 0;
                colvarMediaReportFlag.AutoIncrement = false;
                colvarMediaReportFlag.IsNullable = false;
                colvarMediaReportFlag.IsPrimaryKey = false;
                colvarMediaReportFlag.IsForeignKey = false;
                colvarMediaReportFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarMediaReportFlag);
                
                TableSchema.TableColumn colvarMediaReportFlagText = new TableSchema.TableColumn(schema);
                colvarMediaReportFlagText.ColumnName = "media_report_flag_text";
                colvarMediaReportFlagText.DataType = DbType.String;
                colvarMediaReportFlagText.MaxLength = 1073741823;
                colvarMediaReportFlagText.AutoIncrement = false;
                colvarMediaReportFlagText.IsNullable = true;
                colvarMediaReportFlagText.IsPrimaryKey = false;
                colvarMediaReportFlagText.IsForeignKey = false;
                colvarMediaReportFlagText.IsReadOnly = false;
                
                schema.Columns.Add(colvarMediaReportFlagText);
                
                TableSchema.TableColumn colvarDealCharacterFlag = new TableSchema.TableColumn(schema);
                colvarDealCharacterFlag.ColumnName = "deal_character_flag";
                colvarDealCharacterFlag.DataType = DbType.Int32;
                colvarDealCharacterFlag.MaxLength = 0;
                colvarDealCharacterFlag.AutoIncrement = false;
                colvarDealCharacterFlag.IsNullable = false;
                colvarDealCharacterFlag.IsPrimaryKey = false;
                colvarDealCharacterFlag.IsForeignKey = false;
                colvarDealCharacterFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealCharacterFlag);
                
                TableSchema.TableColumn colvarDealCharacterFlagText = new TableSchema.TableColumn(schema);
                colvarDealCharacterFlagText.ColumnName = "deal_character_flag_text";
                colvarDealCharacterFlagText.DataType = DbType.String;
                colvarDealCharacterFlagText.MaxLength = 1073741823;
                colvarDealCharacterFlagText.AutoIncrement = false;
                colvarDealCharacterFlagText.IsNullable = true;
                colvarDealCharacterFlagText.IsPrimaryKey = false;
                colvarDealCharacterFlagText.IsForeignKey = false;
                colvarDealCharacterFlagText.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealCharacterFlagText);
                
                TableSchema.TableColumn colvarPicAlt = new TableSchema.TableColumn(schema);
                colvarPicAlt.ColumnName = "pic_alt";
                colvarPicAlt.DataType = DbType.String;
                colvarPicAlt.MaxLength = 120;
                colvarPicAlt.AutoIncrement = false;
                colvarPicAlt.IsNullable = true;
                colvarPicAlt.IsPrimaryKey = false;
                colvarPicAlt.IsForeignKey = false;
                colvarPicAlt.IsReadOnly = false;
                
                schema.Columns.Add(colvarPicAlt);
                
                TableSchema.TableColumn colvarVendorReceiptType = new TableSchema.TableColumn(schema);
                colvarVendorReceiptType.ColumnName = "vendor_receipt_type";
                colvarVendorReceiptType.DataType = DbType.Int32;
                colvarVendorReceiptType.MaxLength = 0;
                colvarVendorReceiptType.AutoIncrement = false;
                colvarVendorReceiptType.IsNullable = true;
                colvarVendorReceiptType.IsPrimaryKey = false;
                colvarVendorReceiptType.IsForeignKey = false;
                colvarVendorReceiptType.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorReceiptType);
                
                TableSchema.TableColumn colvarOthermessage = new TableSchema.TableColumn(schema);
                colvarOthermessage.ColumnName = "othermessage";
                colvarOthermessage.DataType = DbType.String;
                colvarOthermessage.MaxLength = 50;
                colvarOthermessage.AutoIncrement = false;
                colvarOthermessage.IsNullable = true;
                colvarOthermessage.IsPrimaryKey = false;
                colvarOthermessage.IsForeignKey = false;
                colvarOthermessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarOthermessage);
                
                TableSchema.TableColumn colvarDeliveryMethod = new TableSchema.TableColumn(schema);
                colvarDeliveryMethod.ColumnName = "delivery_method";
                colvarDeliveryMethod.DataType = DbType.Int32;
                colvarDeliveryMethod.MaxLength = 0;
                colvarDeliveryMethod.AutoIncrement = false;
                colvarDeliveryMethod.IsNullable = true;
                colvarDeliveryMethod.IsPrimaryKey = false;
                colvarDeliveryMethod.IsForeignKey = false;
                colvarDeliveryMethod.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryMethod);
                
                TableSchema.TableColumn colvarSellerBossName = new TableSchema.TableColumn(schema);
                colvarSellerBossName.ColumnName = "seller_boss_name";
                colvarSellerBossName.DataType = DbType.String;
                colvarSellerBossName.MaxLength = 30;
                colvarSellerBossName.AutoIncrement = false;
                colvarSellerBossName.IsNullable = true;
                colvarSellerBossName.IsPrimaryKey = false;
                colvarSellerBossName.IsForeignKey = false;
                colvarSellerBossName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerBossName);
                
                TableSchema.TableColumn colvarContacts = new TableSchema.TableColumn(schema);
                colvarContacts.ColumnName = "contacts";
                colvarContacts.DataType = DbType.String;
                colvarContacts.MaxLength = 2000;
                colvarContacts.AutoIncrement = false;
                colvarContacts.IsNullable = true;
                colvarContacts.IsPrimaryKey = false;
                colvarContacts.IsForeignKey = false;
                colvarContacts.IsReadOnly = false;
                
                schema.Columns.Add(colvarContacts);
                
                TableSchema.TableColumn colvarEditPassFlag = new TableSchema.TableColumn(schema);
                colvarEditPassFlag.ColumnName = "edit_pass_flag";
                colvarEditPassFlag.DataType = DbType.Int32;
                colvarEditPassFlag.MaxLength = 0;
                colvarEditPassFlag.AutoIncrement = false;
                colvarEditPassFlag.IsNullable = false;
                colvarEditPassFlag.IsPrimaryKey = false;
                colvarEditPassFlag.IsForeignKey = false;
                colvarEditPassFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarEditPassFlag);
                
                TableSchema.TableColumn colvarSpecialAppointFlagText = new TableSchema.TableColumn(schema);
                colvarSpecialAppointFlagText.ColumnName = "special_appoint_flag_text";
                colvarSpecialAppointFlagText.DataType = DbType.String;
                colvarSpecialAppointFlagText.MaxLength = 1073741823;
                colvarSpecialAppointFlagText.AutoIncrement = false;
                colvarSpecialAppointFlagText.IsNullable = true;
                colvarSpecialAppointFlagText.IsPrimaryKey = false;
                colvarSpecialAppointFlagText.IsForeignKey = false;
                colvarSpecialAppointFlagText.IsReadOnly = false;
                
                schema.Columns.Add(colvarSpecialAppointFlagText);
                
                TableSchema.TableColumn colvarProposalCreatedType = new TableSchema.TableColumn(schema);
                colvarProposalCreatedType.ColumnName = "proposal_created_type";
                colvarProposalCreatedType.DataType = DbType.Int16;
                colvarProposalCreatedType.MaxLength = 0;
                colvarProposalCreatedType.AutoIncrement = false;
                colvarProposalCreatedType.IsNullable = false;
                colvarProposalCreatedType.IsPrimaryKey = false;
                colvarProposalCreatedType.IsForeignKey = false;
                colvarProposalCreatedType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProposalCreatedType);
                
                TableSchema.TableColumn colvarSellerProposalFlag = new TableSchema.TableColumn(schema);
                colvarSellerProposalFlag.ColumnName = "seller_proposal_flag";
                colvarSellerProposalFlag.DataType = DbType.Int32;
                colvarSellerProposalFlag.MaxLength = 0;
                colvarSellerProposalFlag.AutoIncrement = false;
                colvarSellerProposalFlag.IsNullable = false;
                colvarSellerProposalFlag.IsPrimaryKey = false;
                colvarSellerProposalFlag.IsForeignKey = false;
                colvarSellerProposalFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerProposalFlag);
                
                TableSchema.TableColumn colvarTrialPeriod = new TableSchema.TableColumn(schema);
                colvarTrialPeriod.ColumnName = "trial_period";
                colvarTrialPeriod.DataType = DbType.Boolean;
                colvarTrialPeriod.MaxLength = 0;
                colvarTrialPeriod.AutoIncrement = false;
                colvarTrialPeriod.IsNullable = true;
                colvarTrialPeriod.IsPrimaryKey = false;
                colvarTrialPeriod.IsForeignKey = false;
                colvarTrialPeriod.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrialPeriod);
                
                TableSchema.TableColumn colvarMohist = new TableSchema.TableColumn(schema);
                colvarMohist.ColumnName = "mohist";
                colvarMohist.DataType = DbType.Boolean;
                colvarMohist.MaxLength = 0;
                colvarMohist.AutoIncrement = false;
                colvarMohist.IsNullable = true;
                colvarMohist.IsPrimaryKey = false;
                colvarMohist.IsForeignKey = false;
                colvarMohist.IsReadOnly = false;
                
                schema.Columns.Add(colvarMohist);
                
                TableSchema.TableColumn colvarShipOther = new TableSchema.TableColumn(schema);
                colvarShipOther.ColumnName = "ship_other";
                colvarShipOther.DataType = DbType.String;
                colvarShipOther.MaxLength = 250;
                colvarShipOther.AutoIncrement = false;
                colvarShipOther.IsNullable = true;
                colvarShipOther.IsPrimaryKey = false;
                colvarShipOther.IsForeignKey = false;
                colvarShipOther.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipOther);
                
                TableSchema.TableColumn colvarDevelopeTeamNo = new TableSchema.TableColumn(schema);
                colvarDevelopeTeamNo.ColumnName = "develope_team_no";
                colvarDevelopeTeamNo.DataType = DbType.Int32;
                colvarDevelopeTeamNo.MaxLength = 0;
                colvarDevelopeTeamNo.AutoIncrement = false;
                colvarDevelopeTeamNo.IsNullable = true;
                colvarDevelopeTeamNo.IsPrimaryKey = false;
                colvarDevelopeTeamNo.IsForeignKey = false;
                colvarDevelopeTeamNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarDevelopeTeamNo);
                
                TableSchema.TableColumn colvarOperationTeamNo = new TableSchema.TableColumn(schema);
                colvarOperationTeamNo.ColumnName = "operation_team_no";
                colvarOperationTeamNo.DataType = DbType.Int32;
                colvarOperationTeamNo.MaxLength = 0;
                colvarOperationTeamNo.AutoIncrement = false;
                colvarOperationTeamNo.IsNullable = true;
                colvarOperationTeamNo.IsPrimaryKey = false;
                colvarOperationTeamNo.IsForeignKey = false;
                colvarOperationTeamNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarOperationTeamNo);
                
                TableSchema.TableColumn colvarPassSeller = new TableSchema.TableColumn(schema);
                colvarPassSeller.ColumnName = "pass_seller";
                colvarPassSeller.DataType = DbType.Int32;
                colvarPassSeller.MaxLength = 0;
                colvarPassSeller.AutoIncrement = false;
                colvarPassSeller.IsNullable = false;
                colvarPassSeller.IsPrimaryKey = false;
                colvarPassSeller.IsForeignKey = false;
                colvarPassSeller.IsReadOnly = false;
                
                schema.Columns.Add(colvarPassSeller);
                
                TableSchema.TableColumn colvarFilesDone = new TableSchema.TableColumn(schema);
                colvarFilesDone.ColumnName = "files_done";
                colvarFilesDone.DataType = DbType.Boolean;
                colvarFilesDone.MaxLength = 0;
                colvarFilesDone.AutoIncrement = false;
                colvarFilesDone.IsNullable = false;
                colvarFilesDone.IsPrimaryKey = false;
                colvarFilesDone.IsForeignKey = false;
                colvarFilesDone.IsReadOnly = false;
                
                schema.Columns.Add(colvarFilesDone);
                
                TableSchema.TableColumn colvarGroupCouponDealType = new TableSchema.TableColumn(schema);
                colvarGroupCouponDealType.ColumnName = "group_coupon_deal_type";
                colvarGroupCouponDealType.DataType = DbType.Int32;
                colvarGroupCouponDealType.MaxLength = 0;
                colvarGroupCouponDealType.AutoIncrement = false;
                colvarGroupCouponDealType.IsNullable = true;
                colvarGroupCouponDealType.IsPrimaryKey = false;
                colvarGroupCouponDealType.IsForeignKey = false;
                colvarGroupCouponDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupCouponDealType);
                
                TableSchema.TableColumn colvarIsEarlierPageCheck = new TableSchema.TableColumn(schema);
                colvarIsEarlierPageCheck.ColumnName = "is_earlier_page_check";
                colvarIsEarlierPageCheck.DataType = DbType.Boolean;
                colvarIsEarlierPageCheck.MaxLength = 0;
                colvarIsEarlierPageCheck.AutoIncrement = false;
                colvarIsEarlierPageCheck.IsNullable = false;
                colvarIsEarlierPageCheck.IsPrimaryKey = false;
                colvarIsEarlierPageCheck.IsForeignKey = false;
                colvarIsEarlierPageCheck.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsEarlierPageCheck);
                
                TableSchema.TableColumn colvarEarlierPageCheckDay = new TableSchema.TableColumn(schema);
                colvarEarlierPageCheckDay.ColumnName = "earlier_page_check_day";
                colvarEarlierPageCheckDay.DataType = DbType.Int32;
                colvarEarlierPageCheckDay.MaxLength = 0;
                colvarEarlierPageCheckDay.AutoIncrement = false;
                colvarEarlierPageCheckDay.IsNullable = true;
                colvarEarlierPageCheckDay.IsPrimaryKey = false;
                colvarEarlierPageCheckDay.IsForeignKey = false;
                colvarEarlierPageCheckDay.IsReadOnly = false;
                
                schema.Columns.Add(colvarEarlierPageCheckDay);
                
                TableSchema.TableColumn colvarConsignment = new TableSchema.TableColumn(schema);
                colvarConsignment.ColumnName = "consignment";
                colvarConsignment.DataType = DbType.Boolean;
                colvarConsignment.MaxLength = 0;
                colvarConsignment.AutoIncrement = false;
                colvarConsignment.IsNullable = false;
                colvarConsignment.IsPrimaryKey = false;
                colvarConsignment.IsForeignKey = false;
                colvarConsignment.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsignment);
                
                TableSchema.TableColumn colvarSystemPic = new TableSchema.TableColumn(schema);
                colvarSystemPic.ColumnName = "system_pic";
                colvarSystemPic.DataType = DbType.Boolean;
                colvarSystemPic.MaxLength = 0;
                colvarSystemPic.AutoIncrement = false;
                colvarSystemPic.IsNullable = false;
                colvarSystemPic.IsPrimaryKey = false;
                colvarSystemPic.IsForeignKey = false;
                colvarSystemPic.IsReadOnly = false;
                
                schema.Columns.Add(colvarSystemPic);
                
                TableSchema.TableColumn colvarSpecialPrice = new TableSchema.TableColumn(schema);
                colvarSpecialPrice.ColumnName = "special_price";
                colvarSpecialPrice.DataType = DbType.Boolean;
                colvarSpecialPrice.MaxLength = 0;
                colvarSpecialPrice.AutoIncrement = false;
                colvarSpecialPrice.IsNullable = false;
                colvarSpecialPrice.IsPrimaryKey = false;
                colvarSpecialPrice.IsForeignKey = false;
                colvarSpecialPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarSpecialPrice);
                
                TableSchema.TableColumn colvarIsBankDeal = new TableSchema.TableColumn(schema);
                colvarIsBankDeal.ColumnName = "is_bank_deal";
                colvarIsBankDeal.DataType = DbType.Boolean;
                colvarIsBankDeal.MaxLength = 0;
                colvarIsBankDeal.AutoIncrement = false;
                colvarIsBankDeal.IsNullable = false;
                colvarIsBankDeal.IsPrimaryKey = false;
                colvarIsBankDeal.IsForeignKey = false;
                colvarIsBankDeal.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsBankDeal);
                
                TableSchema.TableColumn colvarIsPromotionDeal = new TableSchema.TableColumn(schema);
                colvarIsPromotionDeal.ColumnName = "is_promotion_deal";
                colvarIsPromotionDeal.DataType = DbType.Boolean;
                colvarIsPromotionDeal.MaxLength = 0;
                colvarIsPromotionDeal.AutoIncrement = false;
                colvarIsPromotionDeal.IsNullable = false;
                colvarIsPromotionDeal.IsPrimaryKey = false;
                colvarIsPromotionDeal.IsForeignKey = false;
                colvarIsPromotionDeal.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsPromotionDeal);
                
                TableSchema.TableColumn colvarIsExhibitionDeal = new TableSchema.TableColumn(schema);
                colvarIsExhibitionDeal.ColumnName = "is_exhibition_deal";
                colvarIsExhibitionDeal.DataType = DbType.Boolean;
                colvarIsExhibitionDeal.MaxLength = 0;
                colvarIsExhibitionDeal.AutoIncrement = false;
                colvarIsExhibitionDeal.IsNullable = false;
                colvarIsExhibitionDeal.IsPrimaryKey = false;
                colvarIsExhibitionDeal.IsForeignKey = false;
                colvarIsExhibitionDeal.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsExhibitionDeal);
                
                TableSchema.TableColumn colvarProposalSourceType = new TableSchema.TableColumn(schema);
                colvarProposalSourceType.ColumnName = "proposal_source_type";
                colvarProposalSourceType.DataType = DbType.Int32;
                colvarProposalSourceType.MaxLength = 0;
                colvarProposalSourceType.AutoIncrement = false;
                colvarProposalSourceType.IsNullable = false;
                colvarProposalSourceType.IsPrimaryKey = false;
                colvarProposalSourceType.IsForeignKey = false;
                colvarProposalSourceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProposalSourceType);
                
                TableSchema.TableColumn colvarDealType1 = new TableSchema.TableColumn(schema);
                colvarDealType1.ColumnName = "deal_type1";
                colvarDealType1.DataType = DbType.Int32;
                colvarDealType1.MaxLength = 0;
                colvarDealType1.AutoIncrement = false;
                colvarDealType1.IsNullable = true;
                colvarDealType1.IsPrimaryKey = false;
                colvarDealType1.IsForeignKey = false;
                colvarDealType1.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType1);
                
                TableSchema.TableColumn colvarIsProduction = new TableSchema.TableColumn(schema);
                colvarIsProduction.ColumnName = "is_production";
                colvarIsProduction.DataType = DbType.Boolean;
                colvarIsProduction.MaxLength = 0;
                colvarIsProduction.AutoIncrement = false;
                colvarIsProduction.IsNullable = false;
                colvarIsProduction.IsPrimaryKey = false;
                colvarIsProduction.IsForeignKey = false;
                colvarIsProduction.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsProduction);
                
                TableSchema.TableColumn colvarFlowCompleteTime = new TableSchema.TableColumn(schema);
                colvarFlowCompleteTime.ColumnName = "flow_complete_time";
                colvarFlowCompleteTime.DataType = DbType.DateTime;
                colvarFlowCompleteTime.MaxLength = 0;
                colvarFlowCompleteTime.AutoIncrement = false;
                colvarFlowCompleteTime.IsNullable = true;
                colvarFlowCompleteTime.IsPrimaryKey = false;
                colvarFlowCompleteTime.IsForeignKey = false;
                colvarFlowCompleteTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarFlowCompleteTime);
                
                TableSchema.TableColumn colvarIsGame = new TableSchema.TableColumn(schema);
                colvarIsGame.ColumnName = "is_game";
                colvarIsGame.DataType = DbType.Boolean;
                colvarIsGame.MaxLength = 0;
                colvarIsGame.AutoIncrement = false;
                colvarIsGame.IsNullable = false;
                colvarIsGame.IsPrimaryKey = false;
                colvarIsGame.IsForeignKey = false;
                colvarIsGame.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsGame);
                
                TableSchema.TableColumn colvarIsWms = new TableSchema.TableColumn(schema);
                colvarIsWms.ColumnName = "is_wms";
                colvarIsWms.DataType = DbType.Boolean;
                colvarIsWms.MaxLength = 0;
                colvarIsWms.AutoIncrement = false;
                colvarIsWms.IsNullable = false;
                colvarIsWms.IsPrimaryKey = false;
                colvarIsWms.IsForeignKey = false;
                colvarIsWms.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsWms);
                
                TableSchema.TableColumn colvarAgentChannels = new TableSchema.TableColumn(schema);
                colvarAgentChannels.ColumnName = "agent_channels";
                colvarAgentChannels.DataType = DbType.AnsiString;
                colvarAgentChannels.MaxLength = 50;
                colvarAgentChannels.AutoIncrement = false;
                colvarAgentChannels.IsNullable = false;
                colvarAgentChannels.IsPrimaryKey = false;
                colvarAgentChannels.IsForeignKey = false;
                colvarAgentChannels.IsReadOnly = false;
                
                schema.Columns.Add(colvarAgentChannels);
                
                TableSchema.TableColumn colvarIsChannelGift = new TableSchema.TableColumn(schema);
                colvarIsChannelGift.ColumnName = "is_channel_gift";
                colvarIsChannelGift.DataType = DbType.Boolean;
                colvarIsChannelGift.MaxLength = 0;
                colvarIsChannelGift.AutoIncrement = false;
                colvarIsChannelGift.IsNullable = false;
                colvarIsChannelGift.IsPrimaryKey = false;
                colvarIsChannelGift.IsForeignKey = false;
                colvarIsChannelGift.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsChannelGift);

                TableSchema.TableColumn colvarIsTaishinChosen = new TableSchema.TableColumn(schema);
                colvarIsTaishinChosen.ColumnName = "is_taishin_chosen";
                colvarIsTaishinChosen.DataType = DbType.Boolean;
                colvarIsTaishinChosen.MaxLength = 0;
                colvarIsTaishinChosen.AutoIncrement = false;
                colvarIsTaishinChosen.IsNullable = false;
                colvarIsTaishinChosen.IsPrimaryKey = false;
                colvarIsTaishinChosen.IsForeignKey = false;
                colvarIsTaishinChosen.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsTaishinChosen);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_proposal_seller",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewProposalSeller()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewProposalSeller(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewProposalSeller(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewProposalSeller(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("ReferrerBusinessHourGuid")]
        [Bindable(true)]
        public Guid? ReferrerBusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("referrer_business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("referrer_business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("DevelopeSalesId")]
        [Bindable(true)]
        public int DevelopeSalesId 
	    {
		    get
		    {
			    return GetColumnValue<int>("develope_sales_id");
		    }
            set 
		    {
			    SetColumnValue("develope_sales_id", value);
            }
        }
	      
        [XmlAttribute("OperationSalesId")]
        [Bindable(true)]
        public int? OperationSalesId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("operation_sales_id");
		    }
            set 
		    {
			    SetColumnValue("operation_sales_id", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("ApplyFlag")]
        [Bindable(true)]
        public int ApplyFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("apply_flag");
		    }
            set 
		    {
			    SetColumnValue("apply_flag", value);
            }
        }
	      
        [XmlAttribute("BusinessCreateFlag")]
        [Bindable(true)]
        public int BusinessCreateFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_create_flag");
		    }
            set 
		    {
			    SetColumnValue("business_create_flag", value);
            }
        }
	      
        [XmlAttribute("BusinessFlag")]
        [Bindable(true)]
        public int BusinessFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_flag");
		    }
            set 
		    {
			    SetColumnValue("business_flag", value);
            }
        }
	      
        [XmlAttribute("EditFlag")]
        [Bindable(true)]
        public int EditFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("edit_flag");
		    }
            set 
		    {
			    SetColumnValue("edit_flag", value);
            }
        }
	      
        [XmlAttribute("ListingFlag")]
        [Bindable(true)]
        public int ListingFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("listing_flag");
		    }
            set 
		    {
			    SetColumnValue("listing_flag", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("ProductSpec")]
        [Bindable(true)]
        public string ProductSpec 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_spec");
		    }
            set 
		    {
			    SetColumnValue("product_spec", value);
            }
        }
	      
        [XmlAttribute("SpecialFlag")]
        [Bindable(true)]
        public int SpecialFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("special_flag");
		    }
            set 
		    {
			    SetColumnValue("special_flag", value);
            }
        }
	      
        [XmlAttribute("MarketAnalysis")]
        [Bindable(true)]
        public string MarketAnalysis 
	    {
		    get
		    {
			    return GetColumnValue<string>("market_analysis");
		    }
            set 
		    {
			    SetColumnValue("market_analysis", value);
            }
        }
	      
        [XmlAttribute("Memo")]
        [Bindable(true)]
        public string Memo 
	    {
		    get
		    {
			    return GetColumnValue<string>("memo");
		    }
            set 
		    {
			    SetColumnValue("memo", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("TempStatus")]
        [Bindable(true)]
        public int TempStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("temp_status");
		    }
            set 
		    {
			    SetColumnValue("temp_status", value);
            }
        }
	      
        [XmlAttribute("EmpId")]
        [Bindable(true)]
        public string EmpId 
	    {
		    get
		    {
			    return GetColumnValue<string>("emp_id");
		    }
            set 
		    {
			    SetColumnValue("emp_id", value);
            }
        }
	      
        [XmlAttribute("EmpName")]
        [Bindable(true)]
        public string EmpName 
	    {
		    get
		    {
			    return GetColumnValue<string>("emp_name");
		    }
            set 
		    {
			    SetColumnValue("emp_name", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("DeptName")]
        [Bindable(true)]
        public string DeptName 
	    {
		    get
		    {
			    return GetColumnValue<string>("dept_name");
		    }
            set 
		    {
			    SetColumnValue("dept_name", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyName");
		    }
            set 
		    {
			    SetColumnValue("CompanyName", value);
            }
        }
	      
        [XmlAttribute("ApplyId")]
        [Bindable(true)]
        public string ApplyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("apply_id");
		    }
            set 
		    {
			    SetColumnValue("apply_id", value);
            }
        }
	      
        [XmlAttribute("ApplyTime")]
        [Bindable(true)]
        public DateTime? ApplyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("apply_time");
		    }
            set 
		    {
			    SetColumnValue("apply_time", value);
            }
        }
	      
        [XmlAttribute("SellerLevel")]
        [Bindable(true)]
        public int? SellerLevel 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seller_level");
		    }
            set 
		    {
			    SetColumnValue("seller_level", value);
            }
        }
	      
        [XmlAttribute("DevelopeDeptId")]
        [Bindable(true)]
        public string DevelopeDeptId 
	    {
		    get
		    {
			    return GetColumnValue<string>("develope_dept_id");
		    }
            set 
		    {
			    SetColumnValue("develope_dept_id", value);
            }
        }
	      
        [XmlAttribute("OperationDeptId")]
        [Bindable(true)]
        public string OperationDeptId 
	    {
		    get
		    {
			    return GetColumnValue<string>("operation_dept_id");
		    }
            set 
		    {
			    SetColumnValue("operation_dept_id", value);
            }
        }
	      
        [XmlAttribute("ApproveFlag")]
        [Bindable(true)]
        public int ApproveFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("approve_flag");
		    }
            set 
		    {
			    SetColumnValue("approve_flag", value);
            }
        }
	      
        [XmlAttribute("PayType")]
        [Bindable(true)]
        public int PayType 
	    {
		    get
		    {
			    return GetColumnValue<int>("pay_type");
		    }
            set 
		    {
			    SetColumnValue("pay_type", value);
            }
        }
	      
        [XmlAttribute("CopyType")]
        [Bindable(true)]
        public int CopyType 
	    {
		    get
		    {
			    return GetColumnValue<int>("copy_type");
		    }
            set 
		    {
			    SetColumnValue("copy_type", value);
            }
        }
	      
        [XmlAttribute("SpecialFlagText")]
        [Bindable(true)]
        public string SpecialFlagText 
	    {
		    get
		    {
			    return GetColumnValue<string>("special_flag_text");
		    }
            set 
		    {
			    SetColumnValue("special_flag_text", value);
            }
        }
	      
        [XmlAttribute("OrderTimeS")]
        [Bindable(true)]
        public DateTime? OrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("order_time_s");
		    }
            set 
		    {
			    SetColumnValue("order_time_s", value);
            }
        }
	      
        [XmlAttribute("OrderTimeE")]
        [Bindable(true)]
        public DateTime? OrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("order_time_e");
		    }
            set 
		    {
			    SetColumnValue("order_time_e", value);
            }
        }
	      
        [XmlAttribute("ScheduleExpect")]
        [Bindable(true)]
        public string ScheduleExpect 
	    {
		    get
		    {
			    return GetColumnValue<string>("schedule_expect");
		    }
            set 
		    {
			    SetColumnValue("schedule_expect", value);
            }
        }
	      
        [XmlAttribute("StartUseUnit")]
        [Bindable(true)]
        public int StartUseUnit 
	    {
		    get
		    {
			    return GetColumnValue<int>("start_use_unit");
		    }
            set 
		    {
			    SetColumnValue("start_use_unit", value);
            }
        }
	      
        [XmlAttribute("StartUseText")]
        [Bindable(true)]
        public string StartUseText 
	    {
		    get
		    {
			    return GetColumnValue<string>("start_use_text");
		    }
            set 
		    {
			    SetColumnValue("start_use_text", value);
            }
        }
	      
        [XmlAttribute("ShipType")]
        [Bindable(true)]
        public int ShipType 
	    {
		    get
		    {
			    return GetColumnValue<int>("ship_type");
		    }
            set 
		    {
			    SetColumnValue("ship_type", value);
            }
        }
	      
        [XmlAttribute("ContractMemo")]
        [Bindable(true)]
        public string ContractMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("contract_memo");
		    }
            set 
		    {
			    SetColumnValue("contract_memo", value);
            }
        }
	      
        [XmlAttribute("DealSubType")]
        [Bindable(true)]
        public int DealSubType 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_sub_type");
		    }
            set 
		    {
			    SetColumnValue("deal_sub_type", value);
            }
        }
	      
        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int DealType 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_type");
		    }
            set 
		    {
			    SetColumnValue("deal_type", value);
            }
        }
	      
        [XmlAttribute("ShipText1")]
        [Bindable(true)]
        public string ShipText1 
	    {
		    get
		    {
			    return GetColumnValue<string>("ship_text1");
		    }
            set 
		    {
			    SetColumnValue("ship_text1", value);
            }
        }
	      
        [XmlAttribute("ShipText2")]
        [Bindable(true)]
        public string ShipText2 
	    {
		    get
		    {
			    return GetColumnValue<string>("ship_text2");
		    }
            set 
		    {
			    SetColumnValue("ship_text2", value);
            }
        }
	      
        [XmlAttribute("ShipText3")]
        [Bindable(true)]
        public string ShipText3 
	    {
		    get
		    {
			    return GetColumnValue<string>("ship_text3");
		    }
            set 
		    {
			    SetColumnValue("ship_text3", value);
            }
        }
	      
        [XmlAttribute("ShippingdateType")]
        [Bindable(true)]
        public int? ShippingdateType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("shippingdate_type");
		    }
            set 
		    {
			    SetColumnValue("shippingdate_type", value);
            }
        }
	      
        [XmlAttribute("BrandName")]
        [Bindable(true)]
        public string BrandName 
	    {
		    get
		    {
			    return GetColumnValue<string>("brand_name");
		    }
            set 
		    {
			    SetColumnValue("brand_name", value);
            }
        }
	      
        [XmlAttribute("MarketingResource")]
        [Bindable(true)]
        public string MarketingResource 
	    {
		    get
		    {
			    return GetColumnValue<string>("marketing_resource");
		    }
            set 
		    {
			    SetColumnValue("marketing_resource", value);
            }
        }
	      
        [XmlAttribute("BusinessPrice")]
        [Bindable(true)]
        public int? BusinessPrice 
	    {
		    get
		    {
			    return GetColumnValue<int?>("business_price");
		    }
            set 
		    {
			    SetColumnValue("business_price", value);
            }
        }
	      
        [XmlAttribute("ConsumerTime")]
        [Bindable(true)]
        public int ConsumerTime 
	    {
		    get
		    {
			    return GetColumnValue<int>("consumer_time");
		    }
            set 
		    {
			    SetColumnValue("consumer_time", value);
            }
        }
	      
        [XmlAttribute("PhotographerAppointFlag")]
        [Bindable(true)]
        public int PhotographerAppointFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("photographer_appoint_flag");
		    }
            set 
		    {
			    SetColumnValue("photographer_appoint_flag", value);
            }
        }
	      
        [XmlAttribute("AncestorBusinessHourGuid")]
        [Bindable(true)]
        public Guid? AncestorBusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("ancestor_business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("ancestor_business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("AncestorSequenceBusinessHourGuid")]
        [Bindable(true)]
        public Guid? AncestorSequenceBusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("ancestor_sequence_business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("ancestor_sequence_business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("StartUseText2")]
        [Bindable(true)]
        public string StartUseText2 
	    {
		    get
		    {
			    return GetColumnValue<string>("start_use_text2");
		    }
            set 
		    {
			    SetColumnValue("start_use_text2", value);
            }
        }
	      
        [XmlAttribute("SaleMarketAnalysis")]
        [Bindable(true)]
        public string SaleMarketAnalysis 
	    {
		    get
		    {
			    return GetColumnValue<string>("sale_market_analysis");
		    }
            set 
		    {
			    SetColumnValue("sale_market_analysis", value);
            }
        }
	      
        [XmlAttribute("MediaReportFlag")]
        [Bindable(true)]
        public int MediaReportFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("media_report_flag");
		    }
            set 
		    {
			    SetColumnValue("media_report_flag", value);
            }
        }
	      
        [XmlAttribute("MediaReportFlagText")]
        [Bindable(true)]
        public string MediaReportFlagText 
	    {
		    get
		    {
			    return GetColumnValue<string>("media_report_flag_text");
		    }
            set 
		    {
			    SetColumnValue("media_report_flag_text", value);
            }
        }
	      
        [XmlAttribute("DealCharacterFlag")]
        [Bindable(true)]
        public int DealCharacterFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_character_flag");
		    }
            set 
		    {
			    SetColumnValue("deal_character_flag", value);
            }
        }
	      
        [XmlAttribute("DealCharacterFlagText")]
        [Bindable(true)]
        public string DealCharacterFlagText 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_character_flag_text");
		    }
            set 
		    {
			    SetColumnValue("deal_character_flag_text", value);
            }
        }
	      
        [XmlAttribute("PicAlt")]
        [Bindable(true)]
        public string PicAlt 
	    {
		    get
		    {
			    return GetColumnValue<string>("pic_alt");
		    }
            set 
		    {
			    SetColumnValue("pic_alt", value);
            }
        }
	      
        [XmlAttribute("VendorReceiptType")]
        [Bindable(true)]
        public int? VendorReceiptType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vendor_receipt_type");
		    }
            set 
		    {
			    SetColumnValue("vendor_receipt_type", value);
            }
        }
	      
        [XmlAttribute("Othermessage")]
        [Bindable(true)]
        public string Othermessage 
	    {
		    get
		    {
			    return GetColumnValue<string>("othermessage");
		    }
            set 
		    {
			    SetColumnValue("othermessage", value);
            }
        }
	      
        [XmlAttribute("DeliveryMethod")]
        [Bindable(true)]
        public int? DeliveryMethod 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_method");
		    }
            set 
		    {
			    SetColumnValue("delivery_method", value);
            }
        }
	      
        [XmlAttribute("SellerBossName")]
        [Bindable(true)]
        public string SellerBossName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_boss_name");
		    }
            set 
		    {
			    SetColumnValue("seller_boss_name", value);
            }
        }
	      
        [XmlAttribute("Contacts")]
        [Bindable(true)]
        public string Contacts 
	    {
		    get
		    {
			    return GetColumnValue<string>("contacts");
		    }
            set 
		    {
			    SetColumnValue("contacts", value);
            }
        }
	      
        [XmlAttribute("EditPassFlag")]
        [Bindable(true)]
        public int EditPassFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("edit_pass_flag");
		    }
            set 
		    {
			    SetColumnValue("edit_pass_flag", value);
            }
        }
	      
        [XmlAttribute("SpecialAppointFlagText")]
        [Bindable(true)]
        public string SpecialAppointFlagText 
	    {
		    get
		    {
			    return GetColumnValue<string>("special_appoint_flag_text");
		    }
            set 
		    {
			    SetColumnValue("special_appoint_flag_text", value);
            }
        }
	      
        [XmlAttribute("ProposalCreatedType")]
        [Bindable(true)]
        public short ProposalCreatedType 
	    {
		    get
		    {
			    return GetColumnValue<short>("proposal_created_type");
		    }
            set 
		    {
			    SetColumnValue("proposal_created_type", value);
            }
        }
	      
        [XmlAttribute("SellerProposalFlag")]
        [Bindable(true)]
        public int SellerProposalFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_proposal_flag");
		    }
            set 
		    {
			    SetColumnValue("seller_proposal_flag", value);
            }
        }
	      
        [XmlAttribute("TrialPeriod")]
        [Bindable(true)]
        public bool? TrialPeriod 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("trial_period");
		    }
            set 
		    {
			    SetColumnValue("trial_period", value);
            }
        }
	      
        [XmlAttribute("Mohist")]
        [Bindable(true)]
        public bool? Mohist 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("mohist");
		    }
            set 
		    {
			    SetColumnValue("mohist", value);
            }
        }
	      
        [XmlAttribute("ShipOther")]
        [Bindable(true)]
        public string ShipOther 
	    {
		    get
		    {
			    return GetColumnValue<string>("ship_other");
		    }
            set 
		    {
			    SetColumnValue("ship_other", value);
            }
        }
	      
        [XmlAttribute("DevelopeTeamNo")]
        [Bindable(true)]
        public int? DevelopeTeamNo 
	    {
		    get
		    {
			    return GetColumnValue<int?>("develope_team_no");
		    }
            set 
		    {
			    SetColumnValue("develope_team_no", value);
            }
        }
	      
        [XmlAttribute("OperationTeamNo")]
        [Bindable(true)]
        public int? OperationTeamNo 
	    {
		    get
		    {
			    return GetColumnValue<int?>("operation_team_no");
		    }
            set 
		    {
			    SetColumnValue("operation_team_no", value);
            }
        }
	      
        [XmlAttribute("PassSeller")]
        [Bindable(true)]
        public int PassSeller 
	    {
		    get
		    {
			    return GetColumnValue<int>("pass_seller");
		    }
            set 
		    {
			    SetColumnValue("pass_seller", value);
            }
        }
	      
        [XmlAttribute("FilesDone")]
        [Bindable(true)]
        public bool FilesDone 
	    {
		    get
		    {
			    return GetColumnValue<bool>("files_done");
		    }
            set 
		    {
			    SetColumnValue("files_done", value);
            }
        }
	      
        [XmlAttribute("GroupCouponDealType")]
        [Bindable(true)]
        public int? GroupCouponDealType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_coupon_deal_type");
		    }
            set 
		    {
			    SetColumnValue("group_coupon_deal_type", value);
            }
        }
	      
        [XmlAttribute("IsEarlierPageCheck")]
        [Bindable(true)]
        public bool IsEarlierPageCheck 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_earlier_page_check");
		    }
            set 
		    {
			    SetColumnValue("is_earlier_page_check", value);
            }
        }
	      
        [XmlAttribute("EarlierPageCheckDay")]
        [Bindable(true)]
        public int? EarlierPageCheckDay 
	    {
		    get
		    {
			    return GetColumnValue<int?>("earlier_page_check_day");
		    }
            set 
		    {
			    SetColumnValue("earlier_page_check_day", value);
            }
        }
	      
        [XmlAttribute("Consignment")]
        [Bindable(true)]
        public bool Consignment 
	    {
		    get
		    {
			    return GetColumnValue<bool>("consignment");
		    }
            set 
		    {
			    SetColumnValue("consignment", value);
            }
        }
	      
        [XmlAttribute("SystemPic")]
        [Bindable(true)]
        public bool SystemPic 
	    {
		    get
		    {
			    return GetColumnValue<bool>("system_pic");
		    }
            set 
		    {
			    SetColumnValue("system_pic", value);
            }
        }
	      
        [XmlAttribute("SpecialPrice")]
        [Bindable(true)]
        public bool SpecialPrice 
	    {
		    get
		    {
			    return GetColumnValue<bool>("special_price");
		    }
            set 
		    {
			    SetColumnValue("special_price", value);
            }
        }
	      
        [XmlAttribute("IsBankDeal")]
        [Bindable(true)]
        public bool IsBankDeal 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_bank_deal");
		    }
            set 
		    {
			    SetColumnValue("is_bank_deal", value);
            }
        }
	      
        [XmlAttribute("IsPromotionDeal")]
        [Bindable(true)]
        public bool IsPromotionDeal 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_promotion_deal");
		    }
            set 
		    {
			    SetColumnValue("is_promotion_deal", value);
            }
        }
	      
        [XmlAttribute("IsExhibitionDeal")]
        [Bindable(true)]
        public bool IsExhibitionDeal 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_exhibition_deal");
		    }
            set 
		    {
			    SetColumnValue("is_exhibition_deal", value);
            }
        }
	      
        [XmlAttribute("ProposalSourceType")]
        [Bindable(true)]
        public int ProposalSourceType 
	    {
		    get
		    {
			    return GetColumnValue<int>("proposal_source_type");
		    }
            set 
		    {
			    SetColumnValue("proposal_source_type", value);
            }
        }
	      
        [XmlAttribute("DealType1")]
        [Bindable(true)]
        public int? DealType1 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_type1");
		    }
            set 
		    {
			    SetColumnValue("deal_type1", value);
            }
        }
	      
        [XmlAttribute("IsProduction")]
        [Bindable(true)]
        public bool IsProduction 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_production");
		    }
            set 
		    {
			    SetColumnValue("is_production", value);
            }
        }
	      
        [XmlAttribute("FlowCompleteTime")]
        [Bindable(true)]
        public DateTime? FlowCompleteTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("flow_complete_time");
		    }
            set 
		    {
			    SetColumnValue("flow_complete_time", value);
            }
        }
	      
        [XmlAttribute("IsGame")]
        [Bindable(true)]
        public bool IsGame 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_game");
		    }
            set 
		    {
			    SetColumnValue("is_game", value);
            }
        }
	      
        [XmlAttribute("IsWms")]
        [Bindable(true)]
        public bool IsWms 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_wms");
		    }
            set 
		    {
			    SetColumnValue("is_wms", value);
            }
        }
	      
        [XmlAttribute("AgentChannels")]
        [Bindable(true)]
        public string AgentChannels 
	    {
		    get
		    {
			    return GetColumnValue<string>("agent_channels");
		    }
            set 
		    {
			    SetColumnValue("agent_channels", value);
            }
        }
	      
        [XmlAttribute("IsChannelGift")]
        [Bindable(true)]
        public bool IsChannelGift 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_channel_gift");
		    }
            set 
		    {
			    SetColumnValue("is_channel_gift", value);
            }
        }

        [XmlAttribute("IsTaishinChosen")]
        [Bindable(true)]
        public bool IsTaishinChosen
        {
            get { return GetColumnValue<bool>(Columns.IsTaishinChosen); }
            set { SetColumnValue(Columns.IsTaishinChosen, value); }
        }

        #endregion

        #region Columns Struct
        public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string ReferrerBusinessHourGuid = @"referrer_business_hour_guid";
            
            public static string DevelopeSalesId = @"develope_sales_id";
            
            public static string OperationSalesId = @"operation_sales_id";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string ApplyFlag = @"apply_flag";
            
            public static string BusinessCreateFlag = @"business_create_flag";
            
            public static string BusinessFlag = @"business_flag";
            
            public static string EditFlag = @"edit_flag";
            
            public static string ListingFlag = @"listing_flag";
            
            public static string Status = @"status";
            
            public static string ProductSpec = @"product_spec";
            
            public static string SpecialFlag = @"special_flag";
            
            public static string MarketAnalysis = @"market_analysis";
            
            public static string Memo = @"memo";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string ModifyTime = @"modify_time";
            
            public static string SellerId = @"seller_id";
            
            public static string TempStatus = @"temp_status";
            
            public static string EmpId = @"emp_id";
            
            public static string EmpName = @"emp_name";
            
            public static string UserName = @"user_name";
            
            public static string DeptName = @"dept_name";
            
            public static string SellerName = @"seller_name";
            
            public static string CompanyName = @"CompanyName";
            
            public static string ApplyId = @"apply_id";
            
            public static string ApplyTime = @"apply_time";
            
            public static string SellerLevel = @"seller_level";
            
            public static string DevelopeDeptId = @"develope_dept_id";
            
            public static string OperationDeptId = @"operation_dept_id";
            
            public static string ApproveFlag = @"approve_flag";
            
            public static string PayType = @"pay_type";
            
            public static string CopyType = @"copy_type";
            
            public static string SpecialFlagText = @"special_flag_text";
            
            public static string OrderTimeS = @"order_time_s";
            
            public static string OrderTimeE = @"order_time_e";
            
            public static string ScheduleExpect = @"schedule_expect";
            
            public static string StartUseUnit = @"start_use_unit";
            
            public static string StartUseText = @"start_use_text";
            
            public static string ShipType = @"ship_type";
            
            public static string ContractMemo = @"contract_memo";
            
            public static string DealSubType = @"deal_sub_type";
            
            public static string DealType = @"deal_type";
            
            public static string ShipText1 = @"ship_text1";
            
            public static string ShipText2 = @"ship_text2";
            
            public static string ShipText3 = @"ship_text3";
            
            public static string ShippingdateType = @"shippingdate_type";
            
            public static string BrandName = @"brand_name";
            
            public static string MarketingResource = @"marketing_resource";
            
            public static string BusinessPrice = @"business_price";
            
            public static string ConsumerTime = @"consumer_time";
            
            public static string PhotographerAppointFlag = @"photographer_appoint_flag";
            
            public static string AncestorBusinessHourGuid = @"ancestor_business_hour_guid";
            
            public static string AncestorSequenceBusinessHourGuid = @"ancestor_sequence_business_hour_guid";
            
            public static string StartUseText2 = @"start_use_text2";
            
            public static string SaleMarketAnalysis = @"sale_market_analysis";
            
            public static string MediaReportFlag = @"media_report_flag";
            
            public static string MediaReportFlagText = @"media_report_flag_text";
            
            public static string DealCharacterFlag = @"deal_character_flag";
            
            public static string DealCharacterFlagText = @"deal_character_flag_text";
            
            public static string PicAlt = @"pic_alt";
            
            public static string VendorReceiptType = @"vendor_receipt_type";
            
            public static string Othermessage = @"othermessage";
            
            public static string DeliveryMethod = @"delivery_method";
            
            public static string SellerBossName = @"seller_boss_name";
            
            public static string Contacts = @"contacts";
            
            public static string EditPassFlag = @"edit_pass_flag";
            
            public static string SpecialAppointFlagText = @"special_appoint_flag_text";
            
            public static string ProposalCreatedType = @"proposal_created_type";
            
            public static string SellerProposalFlag = @"seller_proposal_flag";
            
            public static string TrialPeriod = @"trial_period";
            
            public static string Mohist = @"mohist";
            
            public static string ShipOther = @"ship_other";
            
            public static string DevelopeTeamNo = @"develope_team_no";
            
            public static string OperationTeamNo = @"operation_team_no";
            
            public static string PassSeller = @"pass_seller";
            
            public static string FilesDone = @"files_done";
            
            public static string GroupCouponDealType = @"group_coupon_deal_type";
            
            public static string IsEarlierPageCheck = @"is_earlier_page_check";
            
            public static string EarlierPageCheckDay = @"earlier_page_check_day";
            
            public static string Consignment = @"consignment";
            
            public static string SystemPic = @"system_pic";
            
            public static string SpecialPrice = @"special_price";
            
            public static string IsBankDeal = @"is_bank_deal";
            
            public static string IsPromotionDeal = @"is_promotion_deal";
            
            public static string IsExhibitionDeal = @"is_exhibition_deal";
            
            public static string ProposalSourceType = @"proposal_source_type";
            
            public static string DealType1 = @"deal_type1";
            
            public static string IsProduction = @"is_production";
            
            public static string FlowCompleteTime = @"flow_complete_time";
            
            public static string IsGame = @"is_game";
            
            public static string IsWms = @"is_wms";
            
            public static string AgentChannels = @"agent_channels";
            
            public static string IsChannelGift = @"is_channel_gift";

            public static string IsTaishinChosen = @"is_taishin_chosen";

        }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

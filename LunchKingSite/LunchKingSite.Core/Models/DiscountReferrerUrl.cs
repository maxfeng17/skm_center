using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DiscountReferrerUrl class.
	/// </summary>
    [Serializable]
	public partial class DiscountReferrerUrlCollection : RepositoryList<DiscountReferrerUrl, DiscountReferrerUrlCollection>
	{	   
		public DiscountReferrerUrlCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DiscountReferrerUrlCollection</returns>
		public DiscountReferrerUrlCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DiscountReferrerUrl o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the discount_referrer_url table.
	/// </summary>
	[Serializable]
	public partial class DiscountReferrerUrl : RepositoryRecord<DiscountReferrerUrl>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DiscountReferrerUrl()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DiscountReferrerUrl(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("discount_referrer_url", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrigUrl = new TableSchema.TableColumn(schema);
				colvarOrigUrl.ColumnName = "orig_url";
				colvarOrigUrl.DataType = DbType.AnsiString;
				colvarOrigUrl.MaxLength = 200;
				colvarOrigUrl.AutoIncrement = false;
				colvarOrigUrl.IsNullable = false;
				colvarOrigUrl.IsPrimaryKey = false;
				colvarOrigUrl.IsForeignKey = false;
				colvarOrigUrl.IsReadOnly = false;
				colvarOrigUrl.DefaultSetting = @"";
				colvarOrigUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrigUrl);
				
				TableSchema.TableColumn colvarShortUrl = new TableSchema.TableColumn(schema);
				colvarShortUrl.ColumnName = "short_url";
				colvarShortUrl.DataType = DbType.AnsiString;
				colvarShortUrl.MaxLength = 100;
				colvarShortUrl.AutoIncrement = false;
				colvarShortUrl.IsNullable = false;
				colvarShortUrl.IsPrimaryKey = false;
				colvarShortUrl.IsForeignKey = false;
				colvarShortUrl.IsReadOnly = false;
				colvarShortUrl.DefaultSetting = @"";
				colvarShortUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShortUrl);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = true;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarEventActivityId = new TableSchema.TableColumn(schema);
				colvarEventActivityId.ColumnName = "event_activity_id";
				colvarEventActivityId.DataType = DbType.Int32;
				colvarEventActivityId.MaxLength = 0;
				colvarEventActivityId.AutoIncrement = false;
				colvarEventActivityId.IsNullable = false;
				colvarEventActivityId.IsPrimaryKey = false;
				colvarEventActivityId.IsForeignKey = false;
				colvarEventActivityId.IsReadOnly = false;
				
						colvarEventActivityId.DefaultSetting = @"((0))";
				colvarEventActivityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventActivityId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("discount_referrer_url",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrigUrl")]
		[Bindable(true)]
		public string OrigUrl 
		{
			get { return GetColumnValue<string>(Columns.OrigUrl); }
			set { SetColumnValue(Columns.OrigUrl, value); }
		}
		  
		[XmlAttribute("ShortUrl")]
		[Bindable(true)]
		public string ShortUrl 
		{
			get { return GetColumnValue<string>(Columns.ShortUrl); }
			set { SetColumnValue(Columns.ShortUrl, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid? Bid 
		{
			get { return GetColumnValue<Guid?>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("EventActivityId")]
		[Bindable(true)]
		public int EventActivityId 
		{
			get { return GetColumnValue<int>(Columns.EventActivityId); }
			set { SetColumnValue(Columns.EventActivityId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrigUrlColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ShortUrlColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn EventActivityIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrigUrl = @"orig_url";
			 public static string ShortUrl = @"short_url";
			 public static string Type = @"type";
			 public static string UserId = @"user_id";
			 public static string Bid = @"bid";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string EventActivityId = @"event_activity_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

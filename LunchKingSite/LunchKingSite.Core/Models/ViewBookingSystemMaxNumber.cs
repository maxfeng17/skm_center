using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBookingSystemMaxNumber class.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemMaxNumberCollection : ReadOnlyList<ViewBookingSystemMaxNumber, ViewBookingSystemMaxNumberCollection>
    {        
        public ViewBookingSystemMaxNumberCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_booking_system_max_number view.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemMaxNumber : ReadOnlyRecord<ViewBookingSystemMaxNumber>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_booking_system_max_number", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBookingSystemStoreBookingId = new TableSchema.TableColumn(schema);
                colvarBookingSystemStoreBookingId.ColumnName = "booking_system_store_booking_id";
                colvarBookingSystemStoreBookingId.DataType = DbType.Int32;
                colvarBookingSystemStoreBookingId.MaxLength = 0;
                colvarBookingSystemStoreBookingId.AutoIncrement = false;
                colvarBookingSystemStoreBookingId.IsNullable = false;
                colvarBookingSystemStoreBookingId.IsPrimaryKey = false;
                colvarBookingSystemStoreBookingId.IsForeignKey = false;
                colvarBookingSystemStoreBookingId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBookingSystemStoreBookingId);
                
                TableSchema.TableColumn colvarDayType = new TableSchema.TableColumn(schema);
                colvarDayType.ColumnName = "day_type";
                colvarDayType.DataType = DbType.Int32;
                colvarDayType.MaxLength = 0;
                colvarDayType.AutoIncrement = false;
                colvarDayType.IsNullable = false;
                colvarDayType.IsPrimaryKey = false;
                colvarDayType.IsForeignKey = false;
                colvarDayType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDayType);
                
                TableSchema.TableColumn colvarEffectiveDate = new TableSchema.TableColumn(schema);
                colvarEffectiveDate.ColumnName = "effective_date";
                colvarEffectiveDate.DataType = DbType.DateTime;
                colvarEffectiveDate.MaxLength = 0;
                colvarEffectiveDate.AutoIncrement = false;
                colvarEffectiveDate.IsNullable = true;
                colvarEffectiveDate.IsPrimaryKey = false;
                colvarEffectiveDate.IsForeignKey = false;
                colvarEffectiveDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarEffectiveDate);
                
                TableSchema.TableColumn colvarIsOpening = new TableSchema.TableColumn(schema);
                colvarIsOpening.ColumnName = "is_opening";
                colvarIsOpening.DataType = DbType.Boolean;
                colvarIsOpening.MaxLength = 0;
                colvarIsOpening.AutoIncrement = false;
                colvarIsOpening.IsNullable = false;
                colvarIsOpening.IsPrimaryKey = false;
                colvarIsOpening.IsForeignKey = false;
                colvarIsOpening.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsOpening);
                
                TableSchema.TableColumn colvarMaxNumberOfPeople = new TableSchema.TableColumn(schema);
                colvarMaxNumberOfPeople.ColumnName = "max_number_of_people";
                colvarMaxNumberOfPeople.DataType = DbType.Int32;
                colvarMaxNumberOfPeople.MaxLength = 0;
                colvarMaxNumberOfPeople.AutoIncrement = false;
                colvarMaxNumberOfPeople.IsNullable = true;
                colvarMaxNumberOfPeople.IsPrimaryKey = false;
                colvarMaxNumberOfPeople.IsForeignKey = false;
                colvarMaxNumberOfPeople.IsReadOnly = false;
                
                schema.Columns.Add(colvarMaxNumberOfPeople);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_booking_system_max_number",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBookingSystemMaxNumber()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBookingSystemMaxNumber(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBookingSystemMaxNumber(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBookingSystemMaxNumber(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BookingSystemStoreBookingId")]
        [Bindable(true)]
        public int BookingSystemStoreBookingId 
	    {
		    get
		    {
			    return GetColumnValue<int>("booking_system_store_booking_id");
		    }
            set 
		    {
			    SetColumnValue("booking_system_store_booking_id", value);
            }
        }
	      
        [XmlAttribute("DayType")]
        [Bindable(true)]
        public int DayType 
	    {
		    get
		    {
			    return GetColumnValue<int>("day_type");
		    }
            set 
		    {
			    SetColumnValue("day_type", value);
            }
        }
	      
        [XmlAttribute("EffectiveDate")]
        [Bindable(true)]
        public DateTime? EffectiveDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("effective_date");
		    }
            set 
		    {
			    SetColumnValue("effective_date", value);
            }
        }
	      
        [XmlAttribute("IsOpening")]
        [Bindable(true)]
        public bool IsOpening 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_opening");
		    }
            set 
		    {
			    SetColumnValue("is_opening", value);
            }
        }
	      
        [XmlAttribute("MaxNumberOfPeople")]
        [Bindable(true)]
        public int? MaxNumberOfPeople 
	    {
		    get
		    {
			    return GetColumnValue<int?>("max_number_of_people");
		    }
            set 
		    {
			    SetColumnValue("max_number_of_people", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BookingSystemStoreBookingId = @"booking_system_store_booking_id";
            
            public static string DayType = @"day_type";
            
            public static string EffectiveDate = @"effective_date";
            
            public static string IsOpening = @"is_opening";
            
            public static string MaxNumberOfPeople = @"max_number_of_people";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

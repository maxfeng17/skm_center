using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewShoppingCartItem class.
    /// </summary>
    [Serializable]
    public partial class ViewShoppingCartItemCollection : ReadOnlyList<ViewShoppingCartItem, ViewShoppingCartItemCollection>
    {        
        public ViewShoppingCartItemCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_shopping_cart_item view.
    /// </summary>
    [Serializable]
    public partial class ViewShoppingCartItem : ReadOnlyRecord<ViewShoppingCartItem>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_shopping_cart_item", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarTicketId = new TableSchema.TableColumn(schema);
                colvarTicketId.ColumnName = "ticket_id";
                colvarTicketId.DataType = DbType.String;
                colvarTicketId.MaxLength = 100;
                colvarTicketId.AutoIncrement = false;
                colvarTicketId.IsNullable = false;
                colvarTicketId.IsPrimaryKey = false;
                colvarTicketId.IsForeignKey = false;
                colvarTicketId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTicketId);
                
                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_GUID";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = false;
                colvarItemGuid.IsPrimaryKey = false;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemGuid);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 4000;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarItemUnitPrice = new TableSchema.TableColumn(schema);
                colvarItemUnitPrice.ColumnName = "item_unit_price";
                colvarItemUnitPrice.DataType = DbType.Currency;
                colvarItemUnitPrice.MaxLength = 0;
                colvarItemUnitPrice.AutoIncrement = false;
                colvarItemUnitPrice.IsNullable = false;
                colvarItemUnitPrice.IsPrimaryKey = false;
                colvarItemUnitPrice.IsForeignKey = false;
                colvarItemUnitPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemUnitPrice);
                
                TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
                colvarItemQuantity.ColumnName = "item_quantity";
                colvarItemQuantity.DataType = DbType.Int32;
                colvarItemQuantity.MaxLength = 0;
                colvarItemQuantity.AutoIncrement = false;
                colvarItemQuantity.IsNullable = false;
                colvarItemQuantity.IsPrimaryKey = false;
                colvarItemQuantity.IsForeignKey = false;
                colvarItemQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemQuantity);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarMenuGuid = new TableSchema.TableColumn(schema);
                colvarMenuGuid.ColumnName = "menu_GUID";
                colvarMenuGuid.DataType = DbType.Guid;
                colvarMenuGuid.MaxLength = 0;
                colvarMenuGuid.AutoIncrement = false;
                colvarMenuGuid.IsNullable = false;
                colvarMenuGuid.IsPrimaryKey = false;
                colvarMenuGuid.IsForeignKey = false;
                colvarMenuGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMenuGuid);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.AnsiString;
                colvarItemId.MaxLength = 20;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = true;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarItemItemName = new TableSchema.TableColumn(schema);
                colvarItemItemName.ColumnName = "item_item_name";
                colvarItemItemName.DataType = DbType.String;
                colvarItemItemName.MaxLength = 250;
                colvarItemItemName.AutoIncrement = false;
                colvarItemItemName.IsNullable = false;
                colvarItemItemName.IsPrimaryKey = false;
                colvarItemItemName.IsForeignKey = false;
                colvarItemItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemItemName);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarItemDescription = new TableSchema.TableColumn(schema);
                colvarItemDescription.ColumnName = "item_description";
                colvarItemDescription.DataType = DbType.String;
                colvarItemDescription.MaxLength = 1073741823;
                colvarItemDescription.AutoIncrement = false;
                colvarItemDescription.IsNullable = true;
                colvarItemDescription.IsPrimaryKey = false;
                colvarItemDescription.IsForeignKey = false;
                colvarItemDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemDescription);
                
                TableSchema.TableColumn colvarItemDefaultDailyAmount = new TableSchema.TableColumn(schema);
                colvarItemDefaultDailyAmount.ColumnName = "item_default_daily_amount";
                colvarItemDefaultDailyAmount.DataType = DbType.Int32;
                colvarItemDefaultDailyAmount.MaxLength = 0;
                colvarItemDefaultDailyAmount.AutoIncrement = false;
                colvarItemDefaultDailyAmount.IsNullable = true;
                colvarItemDefaultDailyAmount.IsPrimaryKey = false;
                colvarItemDefaultDailyAmount.IsForeignKey = false;
                colvarItemDefaultDailyAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemDefaultDailyAmount);
                
                TableSchema.TableColumn colvarItemImgPath = new TableSchema.TableColumn(schema);
                colvarItemImgPath.ColumnName = "item_img_path";
                colvarItemImgPath.DataType = DbType.String;
                colvarItemImgPath.MaxLength = 500;
                colvarItemImgPath.AutoIncrement = false;
                colvarItemImgPath.IsNullable = true;
                colvarItemImgPath.IsPrimaryKey = false;
                colvarItemImgPath.IsForeignKey = false;
                colvarItemImgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemImgPath);
                
                TableSchema.TableColumn colvarItemUrl = new TableSchema.TableColumn(schema);
                colvarItemUrl.ColumnName = "item_url";
                colvarItemUrl.DataType = DbType.String;
                colvarItemUrl.MaxLength = 500;
                colvarItemUrl.AutoIncrement = false;
                colvarItemUrl.IsNullable = true;
                colvarItemUrl.IsPrimaryKey = false;
                colvarItemUrl.IsForeignKey = false;
                colvarItemUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemUrl);
                
                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = true;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequence);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarItemCreateId = new TableSchema.TableColumn(schema);
                colvarItemCreateId.ColumnName = "item_create_id";
                colvarItemCreateId.DataType = DbType.String;
                colvarItemCreateId.MaxLength = 30;
                colvarItemCreateId.AutoIncrement = false;
                colvarItemCreateId.IsNullable = false;
                colvarItemCreateId.IsPrimaryKey = false;
                colvarItemCreateId.IsForeignKey = false;
                colvarItemCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemCreateId);
                
                TableSchema.TableColumn colvarItemCreateTime = new TableSchema.TableColumn(schema);
                colvarItemCreateTime.ColumnName = "item_create_time";
                colvarItemCreateTime.DataType = DbType.DateTime;
                colvarItemCreateTime.MaxLength = 0;
                colvarItemCreateTime.AutoIncrement = false;
                colvarItemCreateTime.IsNullable = false;
                colvarItemCreateTime.IsPrimaryKey = false;
                colvarItemCreateTime.IsForeignKey = false;
                colvarItemCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemCreateTime);
                
                TableSchema.TableColumn colvarItemModifyId = new TableSchema.TableColumn(schema);
                colvarItemModifyId.ColumnName = "item_modify_id";
                colvarItemModifyId.DataType = DbType.String;
                colvarItemModifyId.MaxLength = 30;
                colvarItemModifyId.AutoIncrement = false;
                colvarItemModifyId.IsNullable = true;
                colvarItemModifyId.IsPrimaryKey = false;
                colvarItemModifyId.IsForeignKey = false;
                colvarItemModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemModifyId);
                
                TableSchema.TableColumn colvarItemModifyTime = new TableSchema.TableColumn(schema);
                colvarItemModifyTime.ColumnName = "item_modify_time";
                colvarItemModifyTime.DataType = DbType.DateTime;
                colvarItemModifyTime.MaxLength = 0;
                colvarItemModifyTime.AutoIncrement = false;
                colvarItemModifyTime.IsNullable = true;
                colvarItemModifyTime.IsPrimaryKey = false;
                colvarItemModifyTime.IsForeignKey = false;
                colvarItemModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemModifyTime);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.String;
                colvarProductId.MaxLength = 50;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = true;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
                colvarItemOrigPrice.ColumnName = "item_orig_price";
                colvarItemOrigPrice.DataType = DbType.Currency;
                colvarItemOrigPrice.MaxLength = 0;
                colvarItemOrigPrice.AutoIncrement = false;
                colvarItemOrigPrice.IsNullable = false;
                colvarItemOrigPrice.IsPrimaryKey = false;
                colvarItemOrigPrice.IsForeignKey = false;
                colvarItemOrigPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemOrigPrice);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarItemData = new TableSchema.TableColumn(schema);
                colvarItemData.ColumnName = "item_data";
                colvarItemData.DataType = DbType.String;
                colvarItemData.MaxLength = -1;
                colvarItemData.AutoIncrement = false;
                colvarItemData.IsNullable = true;
                colvarItemData.IsPrimaryKey = false;
                colvarItemData.IsForeignKey = false;
                colvarItemData.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemData);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_shopping_cart_item",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewShoppingCartItem()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewShoppingCartItem(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewShoppingCartItem(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewShoppingCartItem(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("TicketId")]
        [Bindable(true)]
        public string TicketId 
	    {
		    get
		    {
			    return GetColumnValue<string>("ticket_id");
		    }
            set 
		    {
			    SetColumnValue("ticket_id", value);
            }
        }
	      
        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid ItemGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("item_GUID");
		    }
            set 
		    {
			    SetColumnValue("item_GUID", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("ItemUnitPrice")]
        [Bindable(true)]
        public decimal ItemUnitPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_unit_price");
		    }
            set 
		    {
			    SetColumnValue("item_unit_price", value);
            }
        }
	      
        [XmlAttribute("ItemQuantity")]
        [Bindable(true)]
        public int ItemQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_quantity");
		    }
            set 
		    {
			    SetColumnValue("item_quantity", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal? Total 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("MenuGuid")]
        [Bindable(true)]
        public Guid MenuGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("menu_GUID");
		    }
            set 
		    {
			    SetColumnValue("menu_GUID", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public string ItemId 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("ItemItemName")]
        [Bindable(true)]
        public string ItemItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_item_name");
		    }
            set 
		    {
			    SetColumnValue("item_item_name", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("ItemDescription")]
        [Bindable(true)]
        public string ItemDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_description");
		    }
            set 
		    {
			    SetColumnValue("item_description", value);
            }
        }
	      
        [XmlAttribute("ItemDefaultDailyAmount")]
        [Bindable(true)]
        public int? ItemDefaultDailyAmount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("item_default_daily_amount");
		    }
            set 
		    {
			    SetColumnValue("item_default_daily_amount", value);
            }
        }
	      
        [XmlAttribute("ItemImgPath")]
        [Bindable(true)]
        public string ItemImgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_img_path");
		    }
            set 
		    {
			    SetColumnValue("item_img_path", value);
            }
        }
	      
        [XmlAttribute("ItemUrl")]
        [Bindable(true)]
        public string ItemUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_url");
		    }
            set 
		    {
			    SetColumnValue("item_url", value);
            }
        }
	      
        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public int? Sequence 
	    {
		    get
		    {
			    return GetColumnValue<int?>("sequence");
		    }
            set 
		    {
			    SetColumnValue("sequence", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("ItemCreateId")]
        [Bindable(true)]
        public string ItemCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_create_id");
		    }
            set 
		    {
			    SetColumnValue("item_create_id", value);
            }
        }
	      
        [XmlAttribute("ItemCreateTime")]
        [Bindable(true)]
        public DateTime ItemCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("item_create_time");
		    }
            set 
		    {
			    SetColumnValue("item_create_time", value);
            }
        }
	      
        [XmlAttribute("ItemModifyId")]
        [Bindable(true)]
        public string ItemModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_modify_id");
		    }
            set 
		    {
			    SetColumnValue("item_modify_id", value);
            }
        }
	      
        [XmlAttribute("ItemModifyTime")]
        [Bindable(true)]
        public DateTime? ItemModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("item_modify_time");
		    }
            set 
		    {
			    SetColumnValue("item_modify_time", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public string ProductId 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("ItemOrigPrice")]
        [Bindable(true)]
        public decimal ItemOrigPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_orig_price");
		    }
            set 
		    {
			    SetColumnValue("item_orig_price", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("ItemData")]
        [Bindable(true)]
        public string ItemData 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_data");
		    }
            set 
		    {
			    SetColumnValue("item_data", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string TicketId = @"ticket_id";
            
            public static string ItemGuid = @"item_GUID";
            
            public static string ItemName = @"item_name";
            
            public static string ItemUnitPrice = @"item_unit_price";
            
            public static string ItemQuantity = @"item_quantity";
            
            public static string Total = @"total";
            
            public static string CreateTime = @"create_time";
            
            public static string MenuGuid = @"menu_GUID";
            
            public static string ItemId = @"item_id";
            
            public static string ItemItemName = @"item_item_name";
            
            public static string ItemPrice = @"item_price";
            
            public static string ItemDescription = @"item_description";
            
            public static string ItemDefaultDailyAmount = @"item_default_daily_amount";
            
            public static string ItemImgPath = @"item_img_path";
            
            public static string ItemUrl = @"item_url";
            
            public static string Sequence = @"sequence";
            
            public static string Status = @"status";
            
            public static string ItemCreateId = @"item_create_id";
            
            public static string ItemCreateTime = @"item_create_time";
            
            public static string ItemModifyId = @"item_modify_id";
            
            public static string ItemModifyTime = @"item_modify_time";
            
            public static string ProductId = @"product_id";
            
            public static string ItemOrigPrice = @"item_orig_price";
            
            public static string StoreGuid = @"store_guid";
            
            public static string ItemData = @"item_data";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

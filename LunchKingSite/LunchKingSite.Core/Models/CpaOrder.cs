using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CpaOrder class.
	/// </summary>
    [Serializable]
	public partial class CpaOrderCollection : RepositoryList<CpaOrder, CpaOrderCollection>
	{	   
		public CpaOrderCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CpaOrderCollection</returns>
		public CpaOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CpaOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the cpa_order table.
	/// </summary>
	[Serializable]
	public partial class CpaOrder : RepositoryRecord<CpaOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CpaOrder()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CpaOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("cpa_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCpaKey = new TableSchema.TableColumn(schema);
				colvarCpaKey.ColumnName = "cpa_key";
				colvarCpaKey.DataType = DbType.String;
				colvarCpaKey.MaxLength = 50;
				colvarCpaKey.AutoIncrement = false;
				colvarCpaKey.IsNullable = false;
				colvarCpaKey.IsPrimaryKey = false;
				colvarCpaKey.IsForeignKey = false;
				colvarCpaKey.IsReadOnly = false;
				colvarCpaKey.DefaultSetting = @"";
				colvarCpaKey.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCpaKey);
				
				TableSchema.TableColumn colvarFirstRsrc = new TableSchema.TableColumn(schema);
				colvarFirstRsrc.ColumnName = "first_rsrc";
				colvarFirstRsrc.DataType = DbType.String;
				colvarFirstRsrc.MaxLength = 50;
				colvarFirstRsrc.AutoIncrement = false;
				colvarFirstRsrc.IsNullable = false;
				colvarFirstRsrc.IsPrimaryKey = false;
				colvarFirstRsrc.IsForeignKey = false;
				colvarFirstRsrc.IsReadOnly = false;
				colvarFirstRsrc.DefaultSetting = @"";
				colvarFirstRsrc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFirstRsrc);
				
				TableSchema.TableColumn colvarLastRsrc = new TableSchema.TableColumn(schema);
				colvarLastRsrc.ColumnName = "last_rsrc";
				colvarLastRsrc.DataType = DbType.String;
				colvarLastRsrc.MaxLength = 50;
				colvarLastRsrc.AutoIncrement = false;
				colvarLastRsrc.IsNullable = false;
				colvarLastRsrc.IsPrimaryKey = false;
				colvarLastRsrc.IsForeignKey = false;
				colvarLastRsrc.IsReadOnly = false;
				colvarLastRsrc.DefaultSetting = @"";
				colvarLastRsrc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastRsrc);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

			    TableSchema.TableColumn colvarLastExternalRsrc = new TableSchema.TableColumn(schema);
			    colvarLastExternalRsrc.ColumnName = "last_external_rsrc";
			    colvarLastExternalRsrc.DataType = DbType.String;
			    colvarLastExternalRsrc.MaxLength = 50;
			    colvarLastExternalRsrc.AutoIncrement = false;
			    colvarLastExternalRsrc.IsNullable = true;
			    colvarLastExternalRsrc.IsPrimaryKey = false;
			    colvarLastExternalRsrc.IsForeignKey = false;
			    colvarLastExternalRsrc.IsReadOnly = false;
			    colvarLastExternalRsrc.DefaultSetting = @"";
                colvarLastExternalRsrc.ForeignKeyTableName = "";
			    schema.Columns.Add(colvarLastExternalRsrc);

                BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("cpa_order",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("CpaKey")]
		[Bindable(true)]
		public string CpaKey 
		{
			get { return GetColumnValue<string>(Columns.CpaKey); }
			set { SetColumnValue(Columns.CpaKey, value); }
		}
		
		[XmlAttribute("FirstRsrc")]
		[Bindable(true)]
		public string FirstRsrc 
		{
			get { return GetColumnValue<string>(Columns.FirstRsrc); }
			set { SetColumnValue(Columns.FirstRsrc, value); }
		}
		
		[XmlAttribute("LastRsrc")]
		[Bindable(true)]
		public string LastRsrc 
		{
			get { return GetColumnValue<string>(Columns.LastRsrc); }
			set { SetColumnValue(Columns.LastRsrc, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

	    [XmlAttribute("LastExternalRsrc")]
	    [Bindable(true)]
	    public string LastExternalRsrc
	    {
	        get { return GetColumnValue<string>(Columns.LastExternalRsrc); }
	        set { SetColumnValue(Columns.LastExternalRsrc, value); }
	    }
        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CpaKeyColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FirstRsrcColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn LastRsrcColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[5]; }
        }

	    public static TableSchema.TableColumn LastExternalRsrcColumn
        {
	        get { return Schema.Columns[6]; }
	    }
        #endregion
        #region Columns Struct
        public struct Columns
		{
			 public static string Id = @"id";
			 public static string CpaKey = @"cpa_key";
			 public static string FirstRsrc = @"first_rsrc";
			 public static string LastRsrc = @"last_rsrc";
			 public static string CreateTime = @"create_time";
			 public static string OrderGuid = @"order_guid";
		     public static string LastExternalRsrc = @"last_external_rsrc";
        }
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

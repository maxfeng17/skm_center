using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the MgmGiftVersion class.
    /// </summary>
    [Serializable]
    public partial class MgmGiftVersionCollection : RepositoryList<MgmGiftVersion, MgmGiftVersionCollection>
    {
        public MgmGiftVersionCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MgmGiftVersionCollection</returns>
        public MgmGiftVersionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MgmGiftVersion o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }


    /// <summary>
    /// This is an ActiveRecord class which wraps the mgm_gift_version table.
    /// </summary>

    [Serializable]
    public partial class MgmGiftVersion : RepositoryRecord<MgmGiftVersion>, IRecordBase
    {
        #region .ctors and Default Settings

        public MgmGiftVersion()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public MgmGiftVersion(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("mgm_gift_version", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarMgmGiftId = new TableSchema.TableColumn(schema);
                colvarMgmGiftId.ColumnName = "mgm_gift_id";
                colvarMgmGiftId.DataType = DbType.Int32;
                colvarMgmGiftId.MaxLength = 0;
                colvarMgmGiftId.AutoIncrement = false;
                colvarMgmGiftId.IsNullable = false;
                colvarMgmGiftId.IsPrimaryKey = false;
                colvarMgmGiftId.IsForeignKey = false;
                colvarMgmGiftId.IsReadOnly = false;
                colvarMgmGiftId.DefaultSetting = @"";
                colvarMgmGiftId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMgmGiftId);

                TableSchema.TableColumn colvarMgmMatchId = new TableSchema.TableColumn(schema);
                colvarMgmMatchId.ColumnName = "mgm_match_id";
                colvarMgmMatchId.DataType = DbType.Int32;
                colvarMgmMatchId.MaxLength = 0;
                colvarMgmMatchId.AutoIncrement = false;
                colvarMgmMatchId.IsNullable = false;
                colvarMgmMatchId.IsPrimaryKey = false;
                colvarMgmMatchId.IsForeignKey = false;
                colvarMgmMatchId.IsReadOnly = false;
                colvarMgmMatchId.DefaultSetting = @"";
                colvarMgmMatchId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMgmMatchId);

                TableSchema.TableColumn colvarSendMessageId = new TableSchema.TableColumn(schema);
                colvarSendMessageId.ColumnName = "send_message_id";
                colvarSendMessageId.DataType = DbType.Int32;
                colvarSendMessageId.MaxLength = 0;
                colvarSendMessageId.AutoIncrement = false;
                colvarSendMessageId.IsNullable = false;
                colvarSendMessageId.IsPrimaryKey = false;
                colvarSendMessageId.IsForeignKey = false;
                colvarSendMessageId.IsReadOnly = false;
                colvarSendMessageId.DefaultSetting = @"";
                colvarSendMessageId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSendMessageId);

                TableSchema.TableColumn colvarReplyMessageId = new TableSchema.TableColumn(schema);
                colvarReplyMessageId.ColumnName = "reply_message_id";
                colvarReplyMessageId.DataType = DbType.Int32;
                colvarReplyMessageId.MaxLength = 0;
                colvarReplyMessageId.AutoIncrement = false;
                colvarReplyMessageId.IsNullable = true;
                colvarReplyMessageId.IsPrimaryKey = false;
                colvarReplyMessageId.IsForeignKey = false;
                colvarReplyMessageId.IsReadOnly = false;
                colvarReplyMessageId.DefaultSetting = @"";
                colvarReplyMessageId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReplyMessageId);

                TableSchema.TableColumn colvarSenderId = new TableSchema.TableColumn(schema);
                colvarSenderId.ColumnName = "sender_id";
                colvarSenderId.DataType = DbType.Int32;
                colvarSenderId.MaxLength = 0;
                colvarSenderId.AutoIncrement = false;
                colvarSenderId.IsNullable = false;
                colvarSenderId.IsPrimaryKey = false;
                colvarSenderId.IsForeignKey = false;
                colvarSenderId.IsReadOnly = false;
                colvarSenderId.DefaultSetting = @"";
                colvarSenderId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSenderId);

                TableSchema.TableColumn colvarSendTime = new TableSchema.TableColumn(schema);
                colvarSendTime.ColumnName = "send_time";
                colvarSendTime.DataType = DbType.DateTime;
                colvarSendTime.MaxLength = 0;
                colvarSendTime.AutoIncrement = false;
                colvarSendTime.IsNullable = false;
                colvarSendTime.IsPrimaryKey = false;
                colvarSendTime.IsForeignKey = false;
                colvarSendTime.IsReadOnly = false;
                colvarSendTime.DefaultSetting = @"";
                colvarSendTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSendTime);

                TableSchema.TableColumn colvarReceiverId = new TableSchema.TableColumn(schema);
                colvarReceiverId.ColumnName = "receiver_id";
                colvarReceiverId.DataType = DbType.Int32;
                colvarReceiverId.MaxLength = 0;
                colvarReceiverId.AutoIncrement = false;
                colvarReceiverId.IsNullable = true;
                colvarReceiverId.IsPrimaryKey = false;
                colvarReceiverId.IsForeignKey = false;
                colvarReceiverId.IsReadOnly = false;
                colvarReceiverId.DefaultSetting = @"";
                colvarReceiverId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReceiverId);

                TableSchema.TableColumn colvarReceiveTime = new TableSchema.TableColumn(schema);
                colvarReceiveTime.ColumnName = "receive_time";
                colvarReceiveTime.DataType = DbType.DateTime;
                colvarReceiveTime.MaxLength = 0;
                colvarReceiveTime.AutoIncrement = false;
                colvarReceiveTime.IsNullable = true;
                colvarReceiveTime.IsPrimaryKey = false;
                colvarReceiveTime.IsForeignKey = false;
                colvarReceiveTime.IsReadOnly = false;
                colvarReceiveTime.DefaultSetting = @"";
                colvarReceiveTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReceiveTime);

                TableSchema.TableColumn colvarAccept = new TableSchema.TableColumn(schema);
                colvarAccept.ColumnName = "accept";
                colvarAccept.DataType = DbType.Int32;
                colvarAccept.MaxLength = 0;
                colvarAccept.AutoIncrement = false;
                colvarAccept.IsNullable = false;
                colvarAccept.IsPrimaryKey = false;
                colvarAccept.IsForeignKey = false;
                colvarAccept.IsReadOnly = false;
                colvarAccept.DefaultSetting = @"";
                colvarAccept.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAccept);

                TableSchema.TableColumn colvarIsActive = new TableSchema.TableColumn(schema);
                colvarIsActive.ColumnName = "is_active";
                colvarIsActive.DataType = DbType.Boolean;
                colvarIsActive.MaxLength = 0;
                colvarIsActive.AutoIncrement = false;
                colvarIsActive.IsNullable = false;
                colvarIsActive.IsPrimaryKey = false;
                colvarIsActive.IsForeignKey = false;
                colvarIsActive.IsReadOnly = false;
                colvarIsActive.DefaultSetting = @"";
                colvarIsActive.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsActive);

                TableSchema.TableColumn colvarReceiverName = new TableSchema.TableColumn(schema);
                colvarReceiverName.ColumnName = "receiver_name";
                colvarReceiverName.DataType = DbType.String;
                colvarReceiverName.MaxLength = 250;
                colvarReceiverName.AutoIncrement = false;
                colvarReceiverName.IsNullable = true;
                colvarReceiverName.IsPrimaryKey = false;
                colvarReceiverName.IsForeignKey = false;
                colvarReceiverName.IsReadOnly = false;
                colvarReceiverName.DefaultSetting = @"";
                colvarReceiverName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReceiverName);

                TableSchema.TableColumn colvarQuickInfo = new TableSchema.TableColumn(schema);
                colvarQuickInfo.ColumnName = "quick_info";
                colvarQuickInfo.DataType = DbType.String;
                colvarQuickInfo.MaxLength = 250;
                colvarQuickInfo.AutoIncrement = false;
                colvarQuickInfo.IsNullable = true;
                colvarQuickInfo.IsPrimaryKey = false;
                colvarQuickInfo.IsForeignKey = false;
                colvarQuickInfo.IsReadOnly = false;
                colvarQuickInfo.DefaultSetting = @"";
                colvarQuickInfo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarQuickInfo);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("mgm_gift_version", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("MgmGiftId")]
        [Bindable(true)]
        public int MgmGiftId
        {
            get { return GetColumnValue<int>(Columns.MgmGiftId); }
            set { SetColumnValue(Columns.MgmGiftId, value); }
        }

        [XmlAttribute("MgmMatchId")]
        [Bindable(true)]
        public int MgmMatchId
        {
            get { return GetColumnValue<int>(Columns.MgmMatchId); }
            set { SetColumnValue(Columns.MgmMatchId, value); }
        }

        [XmlAttribute("SendMessageId")]
        [Bindable(true)]
        public int SendMessageId
        {
            get { return GetColumnValue<int>(Columns.SendMessageId); }
            set { SetColumnValue(Columns.SendMessageId, value); }
        }

        [XmlAttribute("ReplyMessageId")]
        [Bindable(true)]
        public int? ReplyMessageId
        {
            get { return GetColumnValue<int?>(Columns.ReplyMessageId); }
            set { SetColumnValue(Columns.ReplyMessageId, value); }
        }

        [XmlAttribute("SenderId")]
        [Bindable(true)]
        public int SenderId
        {
            get { return GetColumnValue<int>(Columns.SenderId); }
            set { SetColumnValue(Columns.SenderId, value); }
        }

        [XmlAttribute("SendTime")]
        [Bindable(true)]
        public DateTime SendTime
        {
            get { return GetColumnValue<DateTime>(Columns.SendTime); }
            set { SetColumnValue(Columns.SendTime, value); }
        }

        [XmlAttribute("ReceiverId")]
        [Bindable(true)]
        public int? ReceiverId
        {
            get { return GetColumnValue<int?>(Columns.ReceiverId); }
            set { SetColumnValue(Columns.ReceiverId, value); }
        }

        [XmlAttribute("ReceiveTime")]
        [Bindable(true)]
        public DateTime? ReceiveTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ReceiveTime); }
            set { SetColumnValue(Columns.ReceiveTime, value); }
        }

        [XmlAttribute("Accept")]
        [Bindable(true)]
        public int Accept
        {
            get { return GetColumnValue<int>(Columns.Accept); }
            set { SetColumnValue(Columns.Accept, value); }
        }

        [XmlAttribute("IsActive")]
        [Bindable(true)]
        public bool IsActive
        {
            get { return GetColumnValue<bool>(Columns.IsActive); }
            set { SetColumnValue(Columns.IsActive, value); }
        }

        [XmlAttribute("ReceiverName")]
        [Bindable(true)]
        public string ReceiverName
        {
            get { return GetColumnValue<string>(Columns.ReceiverName); }
            set { SetColumnValue(Columns.ReceiverName, value); }
        }

        [XmlAttribute("QuickInfo")]
        [Bindable(true)]
        public string QuickInfo
        {
            get { return GetColumnValue<string>(Columns.QuickInfo); }
            set { SetColumnValue(Columns.QuickInfo, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn MgmGiftIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn MgmMatchIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn SendMessageIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ReplyMessageIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn SenderIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn SendTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ReceiverIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ReceiveTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn AcceptColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn IsActiveColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn ReceiverNameColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn QuickInfoColumn
        {
            get { return Schema.Columns[12]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string MgmGiftId = @"mgm_gift_id";
            public static string MgmMatchId = @"mgm_match_id";
            public static string SendMessageId = @"send_message_id";
            public static string ReplyMessageId = @"reply_message_id";
            public static string SenderId = @"sender_id";
            public static string SendTime = @"send_time";
            public static string ReceiverId = @"receiver_id";
            public static string ReceiveTime = @"receive_time";
            public static string Accept = @"accept";
            public static string IsActive = @"is_active";
            public static string ReceiverName = @"receiver_name";
            public static string QuickInfo = @"quick_info";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

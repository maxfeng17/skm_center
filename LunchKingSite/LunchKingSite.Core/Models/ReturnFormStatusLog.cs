using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ReturnFormStatusLog class.
	/// </summary>
    [Serializable]
	public partial class ReturnFormStatusLogCollection : RepositoryList<ReturnFormStatusLog, ReturnFormStatusLogCollection>
	{	   
		public ReturnFormStatusLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ReturnFormStatusLogCollection</returns>
		public ReturnFormStatusLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ReturnFormStatusLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the return_form_status_log table.
	/// </summary>
	[Serializable]
	public partial class ReturnFormStatusLog : RepositoryRecord<ReturnFormStatusLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ReturnFormStatusLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ReturnFormStatusLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("return_form_status_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
				colvarReturnFormId.ColumnName = "return_form_id";
				colvarReturnFormId.DataType = DbType.Int32;
				colvarReturnFormId.MaxLength = 0;
				colvarReturnFormId.AutoIncrement = false;
				colvarReturnFormId.IsNullable = false;
				colvarReturnFormId.IsPrimaryKey = false;
				colvarReturnFormId.IsForeignKey = true;
				colvarReturnFormId.IsReadOnly = false;
				colvarReturnFormId.DefaultSetting = @"";
				
					colvarReturnFormId.ForeignKeyTableName = "return_form";
				schema.Columns.Add(colvarReturnFormId);
				
				TableSchema.TableColumn colvarProgressStatus = new TableSchema.TableColumn(schema);
				colvarProgressStatus.ColumnName = "progress_status";
				colvarProgressStatus.DataType = DbType.Int32;
				colvarProgressStatus.MaxLength = 0;
				colvarProgressStatus.AutoIncrement = false;
				colvarProgressStatus.IsNullable = false;
				colvarProgressStatus.IsPrimaryKey = false;
				colvarProgressStatus.IsForeignKey = false;
				colvarProgressStatus.IsReadOnly = false;
				colvarProgressStatus.DefaultSetting = @"";
				colvarProgressStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProgressStatus);
				
				TableSchema.TableColumn colvarVendorProgressStatus = new TableSchema.TableColumn(schema);
				colvarVendorProgressStatus.ColumnName = "vendor_progress_status";
				colvarVendorProgressStatus.DataType = DbType.Int32;
				colvarVendorProgressStatus.MaxLength = 0;
				colvarVendorProgressStatus.AutoIncrement = false;
				colvarVendorProgressStatus.IsNullable = true;
				colvarVendorProgressStatus.IsPrimaryKey = false;
				colvarVendorProgressStatus.IsForeignKey = false;
				colvarVendorProgressStatus.IsReadOnly = false;
				colvarVendorProgressStatus.DefaultSetting = @"";
				colvarVendorProgressStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorProgressStatus);
				
				TableSchema.TableColumn colvarVendorMemo = new TableSchema.TableColumn(schema);
				colvarVendorMemo.ColumnName = "vendor_memo";
				colvarVendorMemo.DataType = DbType.String;
				colvarVendorMemo.MaxLength = 30;
				colvarVendorMemo.AutoIncrement = false;
				colvarVendorMemo.IsNullable = true;
				colvarVendorMemo.IsPrimaryKey = false;
				colvarVendorMemo.IsForeignKey = false;
				colvarVendorMemo.IsReadOnly = false;
				colvarVendorMemo.DefaultSetting = @"";
				colvarVendorMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorMemo);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = false;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarRefundType = new TableSchema.TableColumn(schema);
				colvarRefundType.ColumnName = "refund_type";
				colvarRefundType.DataType = DbType.Int32;
				colvarRefundType.MaxLength = 0;
				colvarRefundType.AutoIncrement = false;
				colvarRefundType.IsNullable = false;
				colvarRefundType.IsPrimaryKey = false;
				colvarRefundType.IsForeignKey = false;
				colvarRefundType.IsReadOnly = false;
				colvarRefundType.DefaultSetting = @"";
				colvarRefundType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundType);
				
				TableSchema.TableColumn colvarIsCreditNoteReceived = new TableSchema.TableColumn(schema);
				colvarIsCreditNoteReceived.ColumnName = "is_credit_note_received";
				colvarIsCreditNoteReceived.DataType = DbType.Boolean;
				colvarIsCreditNoteReceived.MaxLength = 0;
				colvarIsCreditNoteReceived.AutoIncrement = false;
				colvarIsCreditNoteReceived.IsNullable = false;
				colvarIsCreditNoteReceived.IsPrimaryKey = false;
				colvarIsCreditNoteReceived.IsForeignKey = false;
				colvarIsCreditNoteReceived.IsReadOnly = false;
				colvarIsCreditNoteReceived.DefaultSetting = @"";
				colvarIsCreditNoteReceived.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCreditNoteReceived);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("return_form_status_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ReturnFormId")]
		[Bindable(true)]
		public int ReturnFormId 
		{
			get { return GetColumnValue<int>(Columns.ReturnFormId); }
			set { SetColumnValue(Columns.ReturnFormId, value); }
		}
		  
		[XmlAttribute("ProgressStatus")]
		[Bindable(true)]
		public int ProgressStatus 
		{
			get { return GetColumnValue<int>(Columns.ProgressStatus); }
			set { SetColumnValue(Columns.ProgressStatus, value); }
		}
		  
		[XmlAttribute("VendorProgressStatus")]
		[Bindable(true)]
		public int? VendorProgressStatus 
		{
			get { return GetColumnValue<int?>(Columns.VendorProgressStatus); }
			set { SetColumnValue(Columns.VendorProgressStatus, value); }
		}
		  
		[XmlAttribute("VendorMemo")]
		[Bindable(true)]
		public string VendorMemo 
		{
			get { return GetColumnValue<string>(Columns.VendorMemo); }
			set { SetColumnValue(Columns.VendorMemo, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("RefundType")]
		[Bindable(true)]
		public int RefundType 
		{
			get { return GetColumnValue<int>(Columns.RefundType); }
			set { SetColumnValue(Columns.RefundType, value); }
		}
		  
		[XmlAttribute("IsCreditNoteReceived")]
		[Bindable(true)]
		public bool IsCreditNoteReceived 
		{
			get { return GetColumnValue<bool>(Columns.IsCreditNoteReceived); }
			set { SetColumnValue(Columns.IsCreditNoteReceived, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnFormIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ProgressStatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorProgressStatusColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorMemoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundTypeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCreditNoteReceivedColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ReturnFormId = @"return_form_id";
			 public static string ProgressStatus = @"progress_status";
			 public static string VendorProgressStatus = @"vendor_progress_status";
			 public static string VendorMemo = @"vendor_memo";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string RefundType = @"refund_type";
			 public static string IsCreditNoteReceived = @"is_credit_note_received";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MemberAuthInfo class.
	/// </summary>
    [Serializable]
	public partial class MemberAuthInfoCollection : RepositoryList<MemberAuthInfo, MemberAuthInfoCollection>
	{	   
		public MemberAuthInfoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberAuthInfoCollection</returns>
		public MemberAuthInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberAuthInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the member_auth_info table.
	/// </summary>
	[Serializable]
	public partial class MemberAuthInfo : RepositoryRecord<MemberAuthInfo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MemberAuthInfo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MemberAuthInfo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_auth_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
				colvarUniqueId.ColumnName = "unique_id";
				colvarUniqueId.DataType = DbType.Int32;
				colvarUniqueId.MaxLength = 0;
				colvarUniqueId.AutoIncrement = false;
				colvarUniqueId.IsNullable = false;
				colvarUniqueId.IsPrimaryKey = true;
				colvarUniqueId.IsForeignKey = false;
				colvarUniqueId.IsReadOnly = false;
				colvarUniqueId.DefaultSetting = @"";
				colvarUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniqueId);
				
				TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
				colvarEmail.ColumnName = "email";
				colvarEmail.DataType = DbType.String;
				colvarEmail.MaxLength = 256;
				colvarEmail.AutoIncrement = false;
				colvarEmail.IsNullable = false;
				colvarEmail.IsPrimaryKey = false;
				colvarEmail.IsForeignKey = false;
				colvarEmail.IsReadOnly = false;
				colvarEmail.DefaultSetting = @"";
				colvarEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmail);
				
				TableSchema.TableColumn colvarAuthCode = new TableSchema.TableColumn(schema);
				colvarAuthCode.ColumnName = "auth_code";
				colvarAuthCode.DataType = DbType.String;
				colvarAuthCode.MaxLength = 50;
				colvarAuthCode.AutoIncrement = false;
				colvarAuthCode.IsNullable = true;
				colvarAuthCode.IsPrimaryKey = false;
				colvarAuthCode.IsForeignKey = false;
				colvarAuthCode.IsReadOnly = false;
				colvarAuthCode.DefaultSetting = @"";
				colvarAuthCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAuthCode);
				
				TableSchema.TableColumn colvarAuthKey = new TableSchema.TableColumn(schema);
				colvarAuthKey.ColumnName = "auth_key";
				colvarAuthKey.DataType = DbType.String;
				colvarAuthKey.MaxLength = 50;
				colvarAuthKey.AutoIncrement = false;
				colvarAuthKey.IsNullable = true;
				colvarAuthKey.IsPrimaryKey = false;
				colvarAuthKey.IsForeignKey = false;
				colvarAuthKey.IsReadOnly = false;
				colvarAuthKey.DefaultSetting = @"";
				colvarAuthKey.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAuthKey);
				
				TableSchema.TableColumn colvarAuthDate = new TableSchema.TableColumn(schema);
				colvarAuthDate.ColumnName = "auth_date";
				colvarAuthDate.DataType = DbType.DateTime;
				colvarAuthDate.MaxLength = 0;
				colvarAuthDate.AutoIncrement = false;
				colvarAuthDate.IsNullable = true;
				colvarAuthDate.IsPrimaryKey = false;
				colvarAuthDate.IsForeignKey = false;
				colvarAuthDate.IsReadOnly = false;
				colvarAuthDate.DefaultSetting = @"";
				colvarAuthDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAuthDate);
				
				TableSchema.TableColumn colvarForgetPasswordDate = new TableSchema.TableColumn(schema);
				colvarForgetPasswordDate.ColumnName = "forget_password_date";
				colvarForgetPasswordDate.DataType = DbType.DateTime;
				colvarForgetPasswordDate.MaxLength = 0;
				colvarForgetPasswordDate.AutoIncrement = false;
				colvarForgetPasswordDate.IsNullable = true;
				colvarForgetPasswordDate.IsPrimaryKey = false;
				colvarForgetPasswordDate.IsForeignKey = false;
				colvarForgetPasswordDate.IsReadOnly = false;
				colvarForgetPasswordDate.DefaultSetting = @"";
				colvarForgetPasswordDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarForgetPasswordDate);
				
				TableSchema.TableColumn colvarForgetPasswordKey = new TableSchema.TableColumn(schema);
				colvarForgetPasswordKey.ColumnName = "forget_password_key";
				colvarForgetPasswordKey.DataType = DbType.String;
				colvarForgetPasswordKey.MaxLength = 50;
				colvarForgetPasswordKey.AutoIncrement = false;
				colvarForgetPasswordKey.IsNullable = true;
				colvarForgetPasswordKey.IsPrimaryKey = false;
				colvarForgetPasswordKey.IsForeignKey = false;
				colvarForgetPasswordKey.IsReadOnly = false;
				colvarForgetPasswordKey.DefaultSetting = @"";
				colvarForgetPasswordKey.ForeignKeyTableName = "";
				schema.Columns.Add(colvarForgetPasswordKey);
				
				TableSchema.TableColumn colvarForgetPasswordCode = new TableSchema.TableColumn(schema);
				colvarForgetPasswordCode.ColumnName = "forget_password_code";
				colvarForgetPasswordCode.DataType = DbType.String;
				colvarForgetPasswordCode.MaxLength = 50;
				colvarForgetPasswordCode.AutoIncrement = false;
				colvarForgetPasswordCode.IsNullable = true;
				colvarForgetPasswordCode.IsPrimaryKey = false;
				colvarForgetPasswordCode.IsForeignKey = false;
				colvarForgetPasswordCode.IsReadOnly = false;
				colvarForgetPasswordCode.DefaultSetting = @"";
				colvarForgetPasswordCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarForgetPasswordCode);
				
				TableSchema.TableColumn colvarPasswordQueryTime = new TableSchema.TableColumn(schema);
				colvarPasswordQueryTime.ColumnName = "password_query_time";
				colvarPasswordQueryTime.DataType = DbType.Int32;
				colvarPasswordQueryTime.MaxLength = 0;
				colvarPasswordQueryTime.AutoIncrement = false;
				colvarPasswordQueryTime.IsNullable = true;
				colvarPasswordQueryTime.IsPrimaryKey = false;
				colvarPasswordQueryTime.IsForeignKey = false;
				colvarPasswordQueryTime.IsReadOnly = false;
				colvarPasswordQueryTime.DefaultSetting = @"";
				colvarPasswordQueryTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPasswordQueryTime);
				
				TableSchema.TableColumn colvarLastUrl = new TableSchema.TableColumn(schema);
				colvarLastUrl.ColumnName = "last_url";
				colvarLastUrl.DataType = DbType.String;
				colvarLastUrl.MaxLength = -1;
				colvarLastUrl.AutoIncrement = false;
				colvarLastUrl.IsNullable = true;
				colvarLastUrl.IsPrimaryKey = false;
				colvarLastUrl.IsForeignKey = false;
				colvarLastUrl.IsReadOnly = false;
				colvarLastUrl.DefaultSetting = @"";
				colvarLastUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastUrl);
				
				TableSchema.TableColumn colvarAuthQueryTime = new TableSchema.TableColumn(schema);
				colvarAuthQueryTime.ColumnName = "auth_query_time";
				colvarAuthQueryTime.DataType = DbType.Int32;
				colvarAuthQueryTime.MaxLength = 0;
				colvarAuthQueryTime.AutoIncrement = false;
				colvarAuthQueryTime.IsNullable = true;
				colvarAuthQueryTime.IsPrimaryKey = false;
				colvarAuthQueryTime.IsForeignKey = false;
				colvarAuthQueryTime.IsReadOnly = false;
				colvarAuthQueryTime.DefaultSetting = @"";
				colvarAuthQueryTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAuthQueryTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_auth_info",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("UniqueId")]
		[Bindable(true)]
		public int UniqueId 
		{
			get { return GetColumnValue<int>(Columns.UniqueId); }
			set { SetColumnValue(Columns.UniqueId, value); }
		}
		  
		[XmlAttribute("Email")]
		[Bindable(true)]
		public string Email 
		{
			get { return GetColumnValue<string>(Columns.Email); }
			set { SetColumnValue(Columns.Email, value); }
		}
		  
		[XmlAttribute("AuthCode")]
		[Bindable(true)]
		public string AuthCode 
		{
			get { return GetColumnValue<string>(Columns.AuthCode); }
			set { SetColumnValue(Columns.AuthCode, value); }
		}
		  
		[XmlAttribute("AuthKey")]
		[Bindable(true)]
		public string AuthKey 
		{
			get { return GetColumnValue<string>(Columns.AuthKey); }
			set { SetColumnValue(Columns.AuthKey, value); }
		}
		  
		[XmlAttribute("AuthDate")]
		[Bindable(true)]
		public DateTime? AuthDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.AuthDate); }
			set { SetColumnValue(Columns.AuthDate, value); }
		}
		  
		[XmlAttribute("ForgetPasswordDate")]
		[Bindable(true)]
		public DateTime? ForgetPasswordDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ForgetPasswordDate); }
			set { SetColumnValue(Columns.ForgetPasswordDate, value); }
		}
		  
		[XmlAttribute("ForgetPasswordKey")]
		[Bindable(true)]
		public string ForgetPasswordKey 
		{
			get { return GetColumnValue<string>(Columns.ForgetPasswordKey); }
			set { SetColumnValue(Columns.ForgetPasswordKey, value); }
		}
		  
		[XmlAttribute("ForgetPasswordCode")]
		[Bindable(true)]
		public string ForgetPasswordCode 
		{
			get { return GetColumnValue<string>(Columns.ForgetPasswordCode); }
			set { SetColumnValue(Columns.ForgetPasswordCode, value); }
		}
		  
		[XmlAttribute("PasswordQueryTime")]
		[Bindable(true)]
		public int? PasswordQueryTime 
		{
			get { return GetColumnValue<int?>(Columns.PasswordQueryTime); }
			set { SetColumnValue(Columns.PasswordQueryTime, value); }
		}
		  
		[XmlAttribute("LastUrl")]
		[Bindable(true)]
		public string LastUrl 
		{
			get { return GetColumnValue<string>(Columns.LastUrl); }
			set { SetColumnValue(Columns.LastUrl, value); }
		}
		  
		[XmlAttribute("AuthQueryTime")]
		[Bindable(true)]
		public int? AuthQueryTime 
		{
			get { return GetColumnValue<int?>(Columns.AuthQueryTime); }
			set { SetColumnValue(Columns.AuthQueryTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn UniqueIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EmailColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AuthCodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AuthKeyColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AuthDateColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ForgetPasswordDateColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ForgetPasswordKeyColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ForgetPasswordCodeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordQueryTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn LastUrlColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn AuthQueryTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string UniqueId = @"unique_id";
			 public static string Email = @"email";
			 public static string AuthCode = @"auth_code";
			 public static string AuthKey = @"auth_key";
			 public static string AuthDate = @"auth_date";
			 public static string ForgetPasswordDate = @"forget_password_date";
			 public static string ForgetPasswordKey = @"forget_password_key";
			 public static string ForgetPasswordCode = @"forget_password_code";
			 public static string PasswordQueryTime = @"password_query_time";
			 public static string LastUrl = @"last_url";
			 public static string AuthQueryTime = @"auth_query_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

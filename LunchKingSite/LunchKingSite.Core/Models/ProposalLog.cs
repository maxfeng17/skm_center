using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProposalLog class.
    /// </summary>
    [Serializable]
    public partial class ProposalLogCollection : RepositoryList<ProposalLog, ProposalLogCollection>
    {
        public ProposalLogCollection() {}

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalLogCollection</returns>
        public ProposalLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProposalLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the proposal_log table.
    /// </summary>
    [Serializable]
    public partial class ProposalLog : RepositoryRecord<ProposalLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProposalLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProposalLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("proposal_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarProposalId = new TableSchema.TableColumn(schema);
                colvarProposalId.ColumnName = "proposal_id";
                colvarProposalId.DataType = DbType.Int32;
                colvarProposalId.MaxLength = 0;
                colvarProposalId.AutoIncrement = false;
                colvarProposalId.IsNullable = false;
                colvarProposalId.IsPrimaryKey = false;
                colvarProposalId.IsForeignKey = false;
                colvarProposalId.IsReadOnly = false;
                colvarProposalId.DefaultSetting = @"";
                colvarProposalId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProposalId);

                TableSchema.TableColumn colvarChangeLog = new TableSchema.TableColumn(schema);
                colvarChangeLog.ColumnName = "change_log";
                colvarChangeLog.DataType = DbType.String;
                colvarChangeLog.MaxLength = -1;
                colvarChangeLog.AutoIncrement = false;
                colvarChangeLog.IsNullable = true;
                colvarChangeLog.IsPrimaryKey = false;
                colvarChangeLog.IsForeignKey = false;
                colvarChangeLog.IsReadOnly = false;
                colvarChangeLog.DefaultSetting = @"";
                colvarChangeLog.ForeignKeyTableName = "";
                schema.Columns.Add(colvarChangeLog);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;

                colvarType.DefaultSetting = @"((0))";
                colvarType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarDept = new TableSchema.TableColumn(schema);
                colvarDept.ColumnName = "dept";
                colvarDept.DataType = DbType.String;
                colvarDept.MaxLength = 150;
                colvarDept.AutoIncrement = false;
                colvarDept.IsNullable = true;
                colvarDept.IsPrimaryKey = false;
                colvarDept.IsForeignKey = false;
                colvarDept.IsReadOnly = false;
                colvarDept.DefaultSetting = @"";
                colvarDept.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDept);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 250;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarVbsRead = new TableSchema.TableColumn(schema);
                colvarVbsRead.ColumnName = "vbs_read";
                colvarVbsRead.DataType = DbType.Boolean;
                colvarVbsRead.MaxLength = 0;
                colvarVbsRead.AutoIncrement = false;
                colvarVbsRead.IsNullable = false;
                colvarVbsRead.IsPrimaryKey = false;
                colvarVbsRead.IsForeignKey = false;
                colvarVbsRead.IsReadOnly = false;

                colvarVbsRead.DefaultSetting = @"((0))";
                colvarVbsRead.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVbsRead);

                TableSchema.TableColumn colvarSaleRead = new TableSchema.TableColumn(schema);
                colvarSaleRead.ColumnName = "sale_read";
                colvarSaleRead.DataType = DbType.Boolean;
                colvarSaleRead.MaxLength = 0;
                colvarSaleRead.AutoIncrement = false;
                colvarSaleRead.IsNullable = false;
                colvarSaleRead.IsPrimaryKey = false;
                colvarSaleRead.IsForeignKey = false;
                colvarSaleRead.IsReadOnly = false;

                colvarSaleRead.DefaultSetting = @"((0))";
                colvarSaleRead.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSaleRead);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("proposal_log",schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("ProposalId")]
        [Bindable(true)]
        public int ProposalId
        {
            get { return GetColumnValue<int>(Columns.ProposalId); }
            set { SetColumnValue(Columns.ProposalId, value); }
        }

        [XmlAttribute("ChangeLog")]
        [Bindable(true)]
        public string ChangeLog
        {
            get { return GetColumnValue<string>(Columns.ChangeLog); }
            set { SetColumnValue(Columns.ChangeLog, value); }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get { return GetColumnValue<int>(Columns.Type); }
            set { SetColumnValue(Columns.Type, value); }
        }

        [XmlAttribute("Dept")]
        [Bindable(true)]
        public string Dept
        {
            get { return GetColumnValue<string>(Columns.Dept); }
            set { SetColumnValue(Columns.Dept, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("VbsRead")]
        [Bindable(true)]
        public bool VbsRead
        {
            get { return GetColumnValue<bool>(Columns.VbsRead); }
            set { SetColumnValue(Columns.VbsRead, value); }
        }

        [XmlAttribute("SaleRead")]
        [Bindable(true)]
        public bool SaleRead
        {
            get { return GetColumnValue<bool>(Columns.SaleRead); }
            set { SetColumnValue(Columns.SaleRead, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ProposalIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ChangeLogColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn DeptColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn VbsReadColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn SaleReadColumn
        {
            get { return Schema.Columns[8]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string ProposalId = @"proposal_id";
            public static string ChangeLog = @"change_log";
            public static string Type = @"type";
            public static string Dept = @"dept";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string VbsRead = @"vbs_read";
            public static string SaleRead = @"sale_read";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Receivable class.
	/// </summary>
    [Serializable]
	public partial class ReceivableCollection : RepositoryList<Receivable, ReceivableCollection>
	{	   
		public ReceivableCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ReceivableCollection</returns>
		public ReceivableCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Receivable o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the receivable table.
	/// </summary>
	[Serializable]
	public partial class Receivable : RepositoryRecord<Receivable>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Receivable()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Receivable(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("receivable", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = true;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				
					colvarId.ForeignKeyTableName = "receivable";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarClassificationGuid = new TableSchema.TableColumn(schema);
				colvarClassificationGuid.ColumnName = "classification_guid";
				colvarClassificationGuid.DataType = DbType.Guid;
				colvarClassificationGuid.MaxLength = 0;
				colvarClassificationGuid.AutoIncrement = false;
				colvarClassificationGuid.IsNullable = true;
				colvarClassificationGuid.IsPrimaryKey = false;
				colvarClassificationGuid.IsForeignKey = false;
				colvarClassificationGuid.IsReadOnly = false;
				colvarClassificationGuid.DefaultSetting = @"";
				colvarClassificationGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarClassificationGuid);
				
				TableSchema.TableColumn colvarClassificationId = new TableSchema.TableColumn(schema);
				colvarClassificationId.ColumnName = "classification_id";
				colvarClassificationId.DataType = DbType.Int32;
				colvarClassificationId.MaxLength = 0;
				colvarClassificationId.AutoIncrement = false;
				colvarClassificationId.IsNullable = true;
				colvarClassificationId.IsPrimaryKey = false;
				colvarClassificationId.IsForeignKey = false;
				colvarClassificationId.IsReadOnly = false;
				colvarClassificationId.DefaultSetting = @"";
				colvarClassificationId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarClassificationId);
				
				TableSchema.TableColumn colvarAccountsClassification = new TableSchema.TableColumn(schema);
				colvarAccountsClassification.ColumnName = "accounts_classification";
				colvarAccountsClassification.DataType = DbType.Int32;
				colvarAccountsClassification.MaxLength = 0;
				colvarAccountsClassification.AutoIncrement = false;
				colvarAccountsClassification.IsNullable = false;
				colvarAccountsClassification.IsPrimaryKey = false;
				colvarAccountsClassification.IsForeignKey = false;
				colvarAccountsClassification.IsReadOnly = false;
				colvarAccountsClassification.DefaultSetting = @"";
				colvarAccountsClassification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountsClassification);
				
				TableSchema.TableColumn colvarTotalAmount = new TableSchema.TableColumn(schema);
				colvarTotalAmount.ColumnName = "total_amount";
				colvarTotalAmount.DataType = DbType.Currency;
				colvarTotalAmount.MaxLength = 0;
				colvarTotalAmount.AutoIncrement = false;
				colvarTotalAmount.IsNullable = false;
				colvarTotalAmount.IsPrimaryKey = false;
				colvarTotalAmount.IsForeignKey = false;
				colvarTotalAmount.IsReadOnly = false;
				
						colvarTotalAmount.DefaultSetting = @"((0))";
				colvarTotalAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalAmount);
				
				TableSchema.TableColumn colvarPcash = new TableSchema.TableColumn(schema);
				colvarPcash.ColumnName = "pcash";
				colvarPcash.DataType = DbType.Currency;
				colvarPcash.MaxLength = 0;
				colvarPcash.AutoIncrement = false;
				colvarPcash.IsNullable = false;
				colvarPcash.IsPrimaryKey = false;
				colvarPcash.IsForeignKey = false;
				colvarPcash.IsReadOnly = false;
				
						colvarPcash.DefaultSetting = @"((0))";
				colvarPcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPcash);
				
				TableSchema.TableColumn colvarScash = new TableSchema.TableColumn(schema);
				colvarScash.ColumnName = "scash";
				colvarScash.DataType = DbType.Currency;
				colvarScash.MaxLength = 0;
				colvarScash.AutoIncrement = false;
				colvarScash.IsNullable = false;
				colvarScash.IsPrimaryKey = false;
				colvarScash.IsForeignKey = false;
				colvarScash.IsReadOnly = false;
				
						colvarScash.DefaultSetting = @"((0))";
				colvarScash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScash);
				
				TableSchema.TableColumn colvarBcash = new TableSchema.TableColumn(schema);
				colvarBcash.ColumnName = "bcash";
				colvarBcash.DataType = DbType.Currency;
				colvarBcash.MaxLength = 0;
				colvarBcash.AutoIncrement = false;
				colvarBcash.IsNullable = false;
				colvarBcash.IsPrimaryKey = false;
				colvarBcash.IsForeignKey = false;
				colvarBcash.IsReadOnly = false;
				
						colvarBcash.DefaultSetting = @"((0))";
				colvarBcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBcash);
				
				TableSchema.TableColumn colvarCreditCardAmt = new TableSchema.TableColumn(schema);
				colvarCreditCardAmt.ColumnName = "credit_card_amt";
				colvarCreditCardAmt.DataType = DbType.Currency;
				colvarCreditCardAmt.MaxLength = 0;
				colvarCreditCardAmt.AutoIncrement = false;
				colvarCreditCardAmt.IsNullable = false;
				colvarCreditCardAmt.IsPrimaryKey = false;
				colvarCreditCardAmt.IsForeignKey = false;
				colvarCreditCardAmt.IsReadOnly = false;
				
						colvarCreditCardAmt.DefaultSetting = @"((0))";
				colvarCreditCardAmt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditCardAmt);
				
				TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
				colvarDiscountAmount.ColumnName = "discount_amount";
				colvarDiscountAmount.DataType = DbType.Currency;
				colvarDiscountAmount.MaxLength = 0;
				colvarDiscountAmount.AutoIncrement = false;
				colvarDiscountAmount.IsNullable = false;
				colvarDiscountAmount.IsPrimaryKey = false;
				colvarDiscountAmount.IsForeignKey = false;
				colvarDiscountAmount.IsReadOnly = false;
				
						colvarDiscountAmount.DefaultSetting = @"((0))";
				colvarDiscountAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountAmount);
				
				TableSchema.TableColumn colvarAtmAmount = new TableSchema.TableColumn(schema);
				colvarAtmAmount.ColumnName = "atm_amount";
				colvarAtmAmount.DataType = DbType.Currency;
				colvarAtmAmount.MaxLength = 0;
				colvarAtmAmount.AutoIncrement = false;
				colvarAtmAmount.IsNullable = false;
				colvarAtmAmount.IsPrimaryKey = false;
				colvarAtmAmount.IsForeignKey = false;
				colvarAtmAmount.IsReadOnly = false;
				
						colvarAtmAmount.DefaultSetting = @"((0))";
				colvarAtmAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAtmAmount);
				
				TableSchema.TableColumn colvarCompleteTotalAmount = new TableSchema.TableColumn(schema);
				colvarCompleteTotalAmount.ColumnName = "complete_total_amount";
				colvarCompleteTotalAmount.DataType = DbType.Currency;
				colvarCompleteTotalAmount.MaxLength = 0;
				colvarCompleteTotalAmount.AutoIncrement = false;
				colvarCompleteTotalAmount.IsNullable = false;
				colvarCompleteTotalAmount.IsPrimaryKey = false;
				colvarCompleteTotalAmount.IsForeignKey = false;
				colvarCompleteTotalAmount.IsReadOnly = false;
				
						colvarCompleteTotalAmount.DefaultSetting = @"((0))";
				colvarCompleteTotalAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompleteTotalAmount);
				
				TableSchema.TableColumn colvarCompletePcash = new TableSchema.TableColumn(schema);
				colvarCompletePcash.ColumnName = "complete_pcash";
				colvarCompletePcash.DataType = DbType.Currency;
				colvarCompletePcash.MaxLength = 0;
				colvarCompletePcash.AutoIncrement = false;
				colvarCompletePcash.IsNullable = false;
				colvarCompletePcash.IsPrimaryKey = false;
				colvarCompletePcash.IsForeignKey = false;
				colvarCompletePcash.IsReadOnly = false;
				
						colvarCompletePcash.DefaultSetting = @"((0))";
				colvarCompletePcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompletePcash);
				
				TableSchema.TableColumn colvarCompleteScash = new TableSchema.TableColumn(schema);
				colvarCompleteScash.ColumnName = "complete_scash";
				colvarCompleteScash.DataType = DbType.Currency;
				colvarCompleteScash.MaxLength = 0;
				colvarCompleteScash.AutoIncrement = false;
				colvarCompleteScash.IsNullable = false;
				colvarCompleteScash.IsPrimaryKey = false;
				colvarCompleteScash.IsForeignKey = false;
				colvarCompleteScash.IsReadOnly = false;
				
						colvarCompleteScash.DefaultSetting = @"((0))";
				colvarCompleteScash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompleteScash);
				
				TableSchema.TableColumn colvarCompleteBcash = new TableSchema.TableColumn(schema);
				colvarCompleteBcash.ColumnName = "complete_bcash";
				colvarCompleteBcash.DataType = DbType.Currency;
				colvarCompleteBcash.MaxLength = 0;
				colvarCompleteBcash.AutoIncrement = false;
				colvarCompleteBcash.IsNullable = false;
				colvarCompleteBcash.IsPrimaryKey = false;
				colvarCompleteBcash.IsForeignKey = false;
				colvarCompleteBcash.IsReadOnly = false;
				
						colvarCompleteBcash.DefaultSetting = @"((0))";
				colvarCompleteBcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompleteBcash);
				
				TableSchema.TableColumn colvarCompleteCreditCardAmt = new TableSchema.TableColumn(schema);
				colvarCompleteCreditCardAmt.ColumnName = "complete_credit_card_amt";
				colvarCompleteCreditCardAmt.DataType = DbType.Currency;
				colvarCompleteCreditCardAmt.MaxLength = 0;
				colvarCompleteCreditCardAmt.AutoIncrement = false;
				colvarCompleteCreditCardAmt.IsNullable = false;
				colvarCompleteCreditCardAmt.IsPrimaryKey = false;
				colvarCompleteCreditCardAmt.IsForeignKey = false;
				colvarCompleteCreditCardAmt.IsReadOnly = false;
				
						colvarCompleteCreditCardAmt.DefaultSetting = @"((0))";
				colvarCompleteCreditCardAmt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompleteCreditCardAmt);
				
				TableSchema.TableColumn colvarCompleteDiscountAmount = new TableSchema.TableColumn(schema);
				colvarCompleteDiscountAmount.ColumnName = "complete_discount_amount";
				colvarCompleteDiscountAmount.DataType = DbType.Currency;
				colvarCompleteDiscountAmount.MaxLength = 0;
				colvarCompleteDiscountAmount.AutoIncrement = false;
				colvarCompleteDiscountAmount.IsNullable = false;
				colvarCompleteDiscountAmount.IsPrimaryKey = false;
				colvarCompleteDiscountAmount.IsForeignKey = false;
				colvarCompleteDiscountAmount.IsReadOnly = false;
				
						colvarCompleteDiscountAmount.DefaultSetting = @"((0))";
				colvarCompleteDiscountAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompleteDiscountAmount);
				
				TableSchema.TableColumn colvarCompleteAtmAmount = new TableSchema.TableColumn(schema);
				colvarCompleteAtmAmount.ColumnName = "complete_atm_amount";
				colvarCompleteAtmAmount.DataType = DbType.Currency;
				colvarCompleteAtmAmount.MaxLength = 0;
				colvarCompleteAtmAmount.AutoIncrement = false;
				colvarCompleteAtmAmount.IsNullable = false;
				colvarCompleteAtmAmount.IsPrimaryKey = false;
				colvarCompleteAtmAmount.IsForeignKey = false;
				colvarCompleteAtmAmount.IsReadOnly = false;
				
						colvarCompleteAtmAmount.DefaultSetting = @"((0))";
				colvarCompleteAtmAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompleteAtmAmount);
				
				TableSchema.TableColumn colvarReceivableStatus = new TableSchema.TableColumn(schema);
				colvarReceivableStatus.ColumnName = "receivable_status";
				colvarReceivableStatus.DataType = DbType.Int32;
				colvarReceivableStatus.MaxLength = 0;
				colvarReceivableStatus.AutoIncrement = false;
				colvarReceivableStatus.IsNullable = false;
				colvarReceivableStatus.IsPrimaryKey = false;
				colvarReceivableStatus.IsForeignKey = false;
				colvarReceivableStatus.IsReadOnly = false;
				colvarReceivableStatus.DefaultSetting = @"";
				colvarReceivableStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceivableStatus);
				
				TableSchema.TableColumn colvarInvoiceId = new TableSchema.TableColumn(schema);
				colvarInvoiceId.ColumnName = "invoice_id";
				colvarInvoiceId.DataType = DbType.Int32;
				colvarInvoiceId.MaxLength = 0;
				colvarInvoiceId.AutoIncrement = false;
				colvarInvoiceId.IsNullable = true;
				colvarInvoiceId.IsPrimaryKey = false;
				colvarInvoiceId.IsForeignKey = false;
				colvarInvoiceId.IsReadOnly = false;
				colvarInvoiceId.DefaultSetting = @"";
				colvarInvoiceId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceId);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("receivable",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ClassificationGuid")]
		[Bindable(true)]
		public Guid? ClassificationGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.ClassificationGuid); }
			set { SetColumnValue(Columns.ClassificationGuid, value); }
		}
		  
		[XmlAttribute("ClassificationId")]
		[Bindable(true)]
		public int? ClassificationId 
		{
			get { return GetColumnValue<int?>(Columns.ClassificationId); }
			set { SetColumnValue(Columns.ClassificationId, value); }
		}
		  
		[XmlAttribute("AccountsClassification")]
		[Bindable(true)]
		public int AccountsClassification 
		{
			get { return GetColumnValue<int>(Columns.AccountsClassification); }
			set { SetColumnValue(Columns.AccountsClassification, value); }
		}
		  
		[XmlAttribute("TotalAmount")]
		[Bindable(true)]
		public decimal TotalAmount 
		{
			get { return GetColumnValue<decimal>(Columns.TotalAmount); }
			set { SetColumnValue(Columns.TotalAmount, value); }
		}
		  
		[XmlAttribute("Pcash")]
		[Bindable(true)]
		public decimal Pcash 
		{
			get { return GetColumnValue<decimal>(Columns.Pcash); }
			set { SetColumnValue(Columns.Pcash, value); }
		}
		  
		[XmlAttribute("Scash")]
		[Bindable(true)]
		public decimal Scash 
		{
			get { return GetColumnValue<decimal>(Columns.Scash); }
			set { SetColumnValue(Columns.Scash, value); }
		}
		  
		[XmlAttribute("Bcash")]
		[Bindable(true)]
		public decimal Bcash 
		{
			get { return GetColumnValue<decimal>(Columns.Bcash); }
			set { SetColumnValue(Columns.Bcash, value); }
		}
		  
		[XmlAttribute("CreditCardAmt")]
		[Bindable(true)]
		public decimal CreditCardAmt 
		{
			get { return GetColumnValue<decimal>(Columns.CreditCardAmt); }
			set { SetColumnValue(Columns.CreditCardAmt, value); }
		}
		  
		[XmlAttribute("DiscountAmount")]
		[Bindable(true)]
		public decimal DiscountAmount 
		{
			get { return GetColumnValue<decimal>(Columns.DiscountAmount); }
			set { SetColumnValue(Columns.DiscountAmount, value); }
		}
		  
		[XmlAttribute("AtmAmount")]
		[Bindable(true)]
		public decimal AtmAmount 
		{
			get { return GetColumnValue<decimal>(Columns.AtmAmount); }
			set { SetColumnValue(Columns.AtmAmount, value); }
		}
		  
		[XmlAttribute("CompleteTotalAmount")]
		[Bindable(true)]
		public decimal CompleteTotalAmount 
		{
			get { return GetColumnValue<decimal>(Columns.CompleteTotalAmount); }
			set { SetColumnValue(Columns.CompleteTotalAmount, value); }
		}
		  
		[XmlAttribute("CompletePcash")]
		[Bindable(true)]
		public decimal CompletePcash 
		{
			get { return GetColumnValue<decimal>(Columns.CompletePcash); }
			set { SetColumnValue(Columns.CompletePcash, value); }
		}
		  
		[XmlAttribute("CompleteScash")]
		[Bindable(true)]
		public decimal CompleteScash 
		{
			get { return GetColumnValue<decimal>(Columns.CompleteScash); }
			set { SetColumnValue(Columns.CompleteScash, value); }
		}
		  
		[XmlAttribute("CompleteBcash")]
		[Bindable(true)]
		public decimal CompleteBcash 
		{
			get { return GetColumnValue<decimal>(Columns.CompleteBcash); }
			set { SetColumnValue(Columns.CompleteBcash, value); }
		}
		  
		[XmlAttribute("CompleteCreditCardAmt")]
		[Bindable(true)]
		public decimal CompleteCreditCardAmt 
		{
			get { return GetColumnValue<decimal>(Columns.CompleteCreditCardAmt); }
			set { SetColumnValue(Columns.CompleteCreditCardAmt, value); }
		}
		  
		[XmlAttribute("CompleteDiscountAmount")]
		[Bindable(true)]
		public decimal CompleteDiscountAmount 
		{
			get { return GetColumnValue<decimal>(Columns.CompleteDiscountAmount); }
			set { SetColumnValue(Columns.CompleteDiscountAmount, value); }
		}
		  
		[XmlAttribute("CompleteAtmAmount")]
		[Bindable(true)]
		public decimal CompleteAtmAmount 
		{
			get { return GetColumnValue<decimal>(Columns.CompleteAtmAmount); }
			set { SetColumnValue(Columns.CompleteAtmAmount, value); }
		}
		  
		[XmlAttribute("ReceivableStatus")]
		[Bindable(true)]
		public int ReceivableStatus 
		{
			get { return GetColumnValue<int>(Columns.ReceivableStatus); }
			set { SetColumnValue(Columns.ReceivableStatus, value); }
		}
		  
		[XmlAttribute("InvoiceId")]
		[Bindable(true)]
		public int? InvoiceId 
		{
			get { return GetColumnValue<int?>(Columns.InvoiceId); }
			set { SetColumnValue(Columns.InvoiceId, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ClassificationGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ClassificationIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountsClassificationColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalAmountColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PcashColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ScashColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn BcashColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreditCardAmtColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountAmountColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn AtmAmountColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CompleteTotalAmountColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CompletePcashColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CompleteScashColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CompleteBcashColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CompleteCreditCardAmtColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CompleteDiscountAmountColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn CompleteAtmAmountColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceivableStatusColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceIdColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ClassificationGuid = @"classification_guid";
			 public static string ClassificationId = @"classification_id";
			 public static string AccountsClassification = @"accounts_classification";
			 public static string TotalAmount = @"total_amount";
			 public static string Pcash = @"pcash";
			 public static string Scash = @"scash";
			 public static string Bcash = @"bcash";
			 public static string CreditCardAmt = @"credit_card_amt";
			 public static string DiscountAmount = @"discount_amount";
			 public static string AtmAmount = @"atm_amount";
			 public static string CompleteTotalAmount = @"complete_total_amount";
			 public static string CompletePcash = @"complete_pcash";
			 public static string CompleteScash = @"complete_scash";
			 public static string CompleteBcash = @"complete_bcash";
			 public static string CompleteCreditCardAmt = @"complete_credit_card_amt";
			 public static string CompleteDiscountAmount = @"complete_discount_amount";
			 public static string CompleteAtmAmount = @"complete_atm_amount";
			 public static string ReceivableStatus = @"receivable_status";
			 public static string InvoiceId = @"invoice_id";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

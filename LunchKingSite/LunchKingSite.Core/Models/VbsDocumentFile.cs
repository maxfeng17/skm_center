﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the VbsDocumentFile class.
    /// </summary>
    [Serializable]
    public partial class VbsDocumentFileCollection : RepositoryList<VbsDocumentFile, VbsDocumentFileCollection>
    {
        public VbsDocumentFileCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VbsDocumentFileCollection</returns>
        public VbsDocumentFileCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VbsDocumentFile o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the vbs_document_file table.
    /// </summary>
    [Serializable]
    public partial class VbsDocumentFile : RepositoryRecord<VbsDocumentFile>, IRecordBase
    {
        #region .ctors and Default Settings

        public VbsDocumentFile()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public VbsDocumentFile(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("vbs_document_file", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarFileName = new TableSchema.TableColumn(schema);
                colvarFileName.ColumnName = "file_name";
                colvarFileName.DataType = DbType.String;
                colvarFileName.MaxLength = 50;
                colvarFileName.AutoIncrement = false;
                colvarFileName.IsNullable = false;
                colvarFileName.IsPrimaryKey = false;
                colvarFileName.IsForeignKey = false;
                colvarFileName.IsReadOnly = false;
                colvarFileName.DefaultSetting = @"";
                colvarFileName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFileName);

                TableSchema.TableColumn colvarFilePath = new TableSchema.TableColumn(schema);
                colvarFilePath.ColumnName = "file_path";
                colvarFilePath.DataType = DbType.AnsiString;
                colvarFilePath.MaxLength = 100;
                colvarFilePath.AutoIncrement = false;
                colvarFilePath.IsNullable = false;
                colvarFilePath.IsPrimaryKey = false;
                colvarFilePath.IsForeignKey = false;
                colvarFilePath.IsReadOnly = false;
                colvarFilePath.DefaultSetting = @"";
                colvarFilePath.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFilePath);

                TableSchema.TableColumn colvarDisplayName = new TableSchema.TableColumn(schema);
                colvarDisplayName.ColumnName = "display_name";
                colvarDisplayName.DataType = DbType.String;
                colvarDisplayName.MaxLength = 50;
                colvarDisplayName.AutoIncrement = false;
                colvarDisplayName.IsNullable = false;
                colvarDisplayName.IsPrimaryKey = false;
                colvarDisplayName.IsForeignKey = false;
                colvarDisplayName.IsReadOnly = false;
                colvarDisplayName.DefaultSetting = @"";
                colvarDisplayName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDisplayName);

                TableSchema.TableColumn colvarPageType = new TableSchema.TableColumn(schema);
                colvarPageType.ColumnName = "page_type";
                colvarPageType.DataType = DbType.Int32;
                colvarPageType.MaxLength = 0;
                colvarPageType.AutoIncrement = false;
                colvarPageType.IsNullable = false;
                colvarPageType.IsPrimaryKey = false;
                colvarPageType.IsForeignKey = false;
                colvarPageType.IsReadOnly = false;

                colvarPageType.DefaultSetting = @"((0))";
                colvarPageType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPageType);

                TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
                colvarCategoryId.ColumnName = "category_id";
                colvarCategoryId.DataType = DbType.Int32;
                colvarCategoryId.MaxLength = 0;
                colvarCategoryId.AutoIncrement = false;
                colvarCategoryId.IsNullable = false;
                colvarCategoryId.IsPrimaryKey = false;
                colvarCategoryId.IsForeignKey = false;
                colvarCategoryId.IsReadOnly = false;

                colvarCategoryId.DefaultSetting = @"((0))";
                colvarCategoryId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCategoryId);

                TableSchema.TableColumn colvarIsShop = new TableSchema.TableColumn(schema);
                colvarIsShop.ColumnName = "is_shop";
                colvarIsShop.DataType = DbType.Boolean;
                colvarIsShop.MaxLength = 0;
                colvarIsShop.AutoIncrement = false;
                colvarIsShop.IsNullable = false;
                colvarIsShop.IsPrimaryKey = false;
                colvarIsShop.IsForeignKey = false;
                colvarIsShop.IsReadOnly = false;

                colvarIsShop.DefaultSetting = @"((0))";
                colvarIsShop.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsShop);

                TableSchema.TableColumn colvarIsHouse = new TableSchema.TableColumn(schema);
                colvarIsHouse.ColumnName = "is_house";
                colvarIsHouse.DataType = DbType.Boolean;
                colvarIsHouse.MaxLength = 0;
                colvarIsHouse.AutoIncrement = false;
                colvarIsHouse.IsNullable = false;
                colvarIsHouse.IsPrimaryKey = false;
                colvarIsHouse.IsForeignKey = false;
                colvarIsHouse.IsReadOnly = false;

                colvarIsHouse.DefaultSetting = @"((0))";
                colvarIsHouse.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsHouse);

                TableSchema.TableColumn colvarIsShow = new TableSchema.TableColumn(schema);
                colvarIsShow.ColumnName = "is_show";
                colvarIsShow.DataType = DbType.Boolean;
                colvarIsShow.MaxLength = 0;
                colvarIsShow.AutoIncrement = false;
                colvarIsShow.IsNullable = false;
                colvarIsShow.IsPrimaryKey = false;
                colvarIsShow.IsForeignKey = false;
                colvarIsShow.IsReadOnly = false;

                colvarIsShow.DefaultSetting = @"((0))";
                colvarIsShow.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsShow);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.AnsiString;
                colvarModifyId.MaxLength = 50;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = false;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                colvarStatus.DefaultSetting = @"((0))";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarSort = new TableSchema.TableColumn(schema);
                colvarSort.ColumnName = "sort";
                colvarSort.DataType = DbType.Int32;
                colvarSort.MaxLength = 0;
                colvarSort.AutoIncrement = false;
                colvarSort.IsNullable = false;
                colvarSort.IsPrimaryKey = false;
                colvarSort.IsForeignKey = false;
                colvarSort.IsReadOnly = false;

                colvarSort.DefaultSetting = @"((0))";
                colvarSort.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSort);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("vbs_document_file", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("FileName")]
        [Bindable(true)]
        public string FileName
        {
            get { return GetColumnValue<string>(Columns.FileName); }
            set { SetColumnValue(Columns.FileName, value); }
        }

        [XmlAttribute("FilePath")]
        [Bindable(true)]
        public string FilePath
        {
            get { return GetColumnValue<string>(Columns.FilePath); }
            set { SetColumnValue(Columns.FilePath, value); }
        }

        [XmlAttribute("DisplayName")]
        [Bindable(true)]
        public string DisplayName
        {
            get { return GetColumnValue<string>(Columns.DisplayName); }
            set { SetColumnValue(Columns.DisplayName, value); }
        }

        [XmlAttribute("PageType")]
        [Bindable(true)]
        public int PageType
        {
            get { return GetColumnValue<int>(Columns.PageType); }
            set { SetColumnValue(Columns.PageType, value); }
        }

        [XmlAttribute("CategoryId")]
        [Bindable(true)]
        public int CategoryId
        {
            get { return GetColumnValue<int>(Columns.CategoryId); }
            set { SetColumnValue(Columns.CategoryId, value); }
        }

        [XmlAttribute("IsShop")]
        [Bindable(true)]
        public bool IsShop
        {
            get { return GetColumnValue<bool>(Columns.IsShop); }
            set { SetColumnValue(Columns.IsShop, value); }
        }

        [XmlAttribute("IsHouse")]
        [Bindable(true)]
        public bool IsHouse
        {
            get { return GetColumnValue<bool>(Columns.IsHouse); }
            set { SetColumnValue(Columns.IsHouse, value); }
        }

        [XmlAttribute("IsShow")]
        [Bindable(true)]
        public bool IsShow
        {
            get { return GetColumnValue<bool>(Columns.IsShow); }
            set { SetColumnValue(Columns.IsShow, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime
        {
            get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("Sort")]
        [Bindable(true)]
        public int Sort
        {
            get { return GetColumnValue<int>(Columns.Sort); }
            set { SetColumnValue(Columns.Sort, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn FileNameColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn FilePathColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn DisplayNameColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn PageTypeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CategoryIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn IsShopColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn IsHouseColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn IsShowColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn SortColumn
        {
            get { return Schema.Columns[14]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string FileName = @"file_name";
            public static string FilePath = @"file_path";
            public static string DisplayName = @"display_name";
            public static string PageType = @"page_type";
            public static string CategoryId = @"category_id";
            public static string IsShop = @"is_shop";
            public static string IsHouse = @"is_house";
            public static string IsShow = @"is_show";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string Status = @"status";
            public static string Sort = @"sort";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ApiUser class.
	/// </summary>
    [Serializable]
	public partial class ApiUserCollection : RepositoryList<ApiUser, ApiUserCollection>
	{	   
		public ApiUserCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ApiUserCollection</returns>
		public ApiUserCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ApiUser o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the api_user table.
	/// </summary>
	
	[Serializable]
	public partial class ApiUser : RepositoryRecord<ApiUser>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ApiUser()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ApiUser(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("api_user", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.AnsiString;
				colvarUserId.MaxLength = 50;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = true;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
				colvarUserName.ColumnName = "user_name";
				colvarUserName.DataType = DbType.String;
				colvarUserName.MaxLength = 100;
				colvarUserName.AutoIncrement = false;
				colvarUserName.IsNullable = true;
				colvarUserName.IsPrimaryKey = false;
				colvarUserName.IsForeignKey = false;
				colvarUserName.IsReadOnly = false;
				colvarUserName.DefaultSetting = @"";
				colvarUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserName);
				
				TableSchema.TableColumn colvarCpaCode = new TableSchema.TableColumn(schema);
				colvarCpaCode.ColumnName = "cpa_code";
				colvarCpaCode.DataType = DbType.AnsiString;
				colvarCpaCode.MaxLength = 50;
				colvarCpaCode.AutoIncrement = false;
				colvarCpaCode.IsNullable = true;
				colvarCpaCode.IsPrimaryKey = false;
				colvarCpaCode.IsForeignKey = false;
				colvarCpaCode.IsReadOnly = false;
				colvarCpaCode.DefaultSetting = @"";
				colvarCpaCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCpaCode);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = true;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				colvarEnabled.DefaultSetting = @"";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarUserDeviceType = new TableSchema.TableColumn(schema);
				colvarUserDeviceType.ColumnName = "user_device_type";
				colvarUserDeviceType.DataType = DbType.Int32;
				colvarUserDeviceType.MaxLength = 0;
				colvarUserDeviceType.AutoIncrement = false;
				colvarUserDeviceType.IsNullable = false;
				colvarUserDeviceType.IsPrimaryKey = false;
				colvarUserDeviceType.IsForeignKey = false;
				colvarUserDeviceType.IsReadOnly = false;
                colvarUserDeviceType.DefaultSetting = @"((0))";
                colvarUserDeviceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserDeviceType);
				
				TableSchema.TableColumn colvarGrossMarginLimit = new TableSchema.TableColumn(schema);
				colvarGrossMarginLimit.ColumnName = "gross_margin_limit";
				colvarGrossMarginLimit.DataType = DbType.Decimal;
				colvarGrossMarginLimit.MaxLength = 0;
				colvarGrossMarginLimit.AutoIncrement = false;
				colvarGrossMarginLimit.IsNullable = false;
				colvarGrossMarginLimit.IsPrimaryKey = false;
				colvarGrossMarginLimit.IsForeignKey = false;
				colvarGrossMarginLimit.IsReadOnly = false;
				
						colvarGrossMarginLimit.DefaultSetting = @"((0.1))";
				colvarGrossMarginLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGrossMarginLimit);
				
				TableSchema.TableColumn colvarDeliveryGrossMarginLimit = new TableSchema.TableColumn(schema);
				colvarDeliveryGrossMarginLimit.ColumnName = "delivery_gross_margin_limit";
				colvarDeliveryGrossMarginLimit.DataType = DbType.Decimal;
				colvarDeliveryGrossMarginLimit.MaxLength = 0;
				colvarDeliveryGrossMarginLimit.AutoIncrement = false;
				colvarDeliveryGrossMarginLimit.IsNullable = false;
				colvarDeliveryGrossMarginLimit.IsPrimaryKey = false;
				colvarDeliveryGrossMarginLimit.IsForeignKey = false;
				colvarDeliveryGrossMarginLimit.IsReadOnly = false;
				
						colvarDeliveryGrossMarginLimit.DefaultSetting = @"((0.1))";
				colvarDeliveryGrossMarginLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryGrossMarginLimit);
				
				TableSchema.TableColumn colvarCouponGrossMarginLimit = new TableSchema.TableColumn(schema);
				colvarCouponGrossMarginLimit.ColumnName = "coupon_gross_margin_limit";
				colvarCouponGrossMarginLimit.DataType = DbType.Decimal;
				colvarCouponGrossMarginLimit.MaxLength = 0;
				colvarCouponGrossMarginLimit.AutoIncrement = false;
				colvarCouponGrossMarginLimit.IsNullable = false;
				colvarCouponGrossMarginLimit.IsPrimaryKey = false;
				colvarCouponGrossMarginLimit.IsForeignKey = false;
				colvarCouponGrossMarginLimit.IsReadOnly = false;
				
						colvarCouponGrossMarginLimit.DefaultSetting = @"((0.1))";
				colvarCouponGrossMarginLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponGrossMarginLimit);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("api_user",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public string UserId 
		{
			get { return GetColumnValue<string>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		[XmlAttribute("UserName")]
		[Bindable(true)]
		public string UserName 
		{
			get { return GetColumnValue<string>(Columns.UserName); }
			set { SetColumnValue(Columns.UserName, value); }
		}
		
		[XmlAttribute("CpaCode")]
		[Bindable(true)]
		public string CpaCode 
		{
			get { return GetColumnValue<string>(Columns.CpaCode); }
			set { SetColumnValue(Columns.CpaCode, value); }
		}
		
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool? Enabled 
		{
			get { return GetColumnValue<bool?>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		
		[XmlAttribute("UserDeviceType")]
		[Bindable(true)]
		public int UserDeviceType 
		{
			get { return GetColumnValue<int>(Columns.UserDeviceType); }
			set { SetColumnValue(Columns.UserDeviceType, value); }
		}
		
		[XmlAttribute("GrossMarginLimit")]
		[Bindable(true)]
		public decimal GrossMarginLimit 
		{
			get { return GetColumnValue<decimal>(Columns.GrossMarginLimit); }
			set { SetColumnValue(Columns.GrossMarginLimit, value); }
		}
		
		[XmlAttribute("DeliveryGrossMarginLimit")]
		[Bindable(true)]
		public decimal DeliveryGrossMarginLimit 
		{
			get { return GetColumnValue<decimal>(Columns.DeliveryGrossMarginLimit); }
			set { SetColumnValue(Columns.DeliveryGrossMarginLimit, value); }
		}
		
		[XmlAttribute("CouponGrossMarginLimit")]
		[Bindable(true)]
		public decimal CouponGrossMarginLimit 
		{
			get { return GetColumnValue<decimal>(Columns.CouponGrossMarginLimit); }
			set { SetColumnValue(Columns.CouponGrossMarginLimit, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CpaCodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn UserDeviceTypeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn GrossMarginLimitColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryGrossMarginLimitColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponGrossMarginLimitColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string UserId = @"user_id";
			 public static string UserName = @"user_name";
			 public static string CpaCode = @"cpa_code";
			 public static string Enabled = @"enabled";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
			 public static string ModifyTime = @"modify_time";
			 public static string ModifyId = @"modify_id";
			 public static string UserDeviceType = @"user_device_type";
			 public static string GrossMarginLimit = @"gross_margin_limit";
			 public static string DeliveryGrossMarginLimit = @"delivery_gross_margin_limit";
			 public static string CouponGrossMarginLimit = @"coupon_gross_margin_limit";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

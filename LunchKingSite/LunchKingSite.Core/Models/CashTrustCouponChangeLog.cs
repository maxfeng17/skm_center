using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CashTrustCouponChangeLog class.
	/// </summary>
    [Serializable]
	public partial class CashTrustCouponChangeLogCollection : RepositoryList<CashTrustCouponChangeLog, CashTrustCouponChangeLogCollection>
	{	   
		public CashTrustCouponChangeLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CashTrustCouponChangeLogCollection</returns>
		public CashTrustCouponChangeLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CashTrustCouponChangeLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the cash_trust_coupon_change_log table.
	/// </summary>
	[Serializable]
	public partial class CashTrustCouponChangeLog : RepositoryRecord<CashTrustCouponChangeLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CashTrustCouponChangeLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CashTrustCouponChangeLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("cash_trust_coupon_change_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);
				
				TableSchema.TableColumn colvarOriCouponId = new TableSchema.TableColumn(schema);
				colvarOriCouponId.ColumnName = "ori_coupon_id";
				colvarOriCouponId.DataType = DbType.Int32;
				colvarOriCouponId.MaxLength = 0;
				colvarOriCouponId.AutoIncrement = false;
				colvarOriCouponId.IsNullable = true;
				colvarOriCouponId.IsPrimaryKey = false;
				colvarOriCouponId.IsForeignKey = false;
				colvarOriCouponId.IsReadOnly = false;
				colvarOriCouponId.DefaultSetting = @"";
				colvarOriCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOriCouponId);
				
				TableSchema.TableColumn colvarOriSequenceNumber = new TableSchema.TableColumn(schema);
				colvarOriSequenceNumber.ColumnName = "ori_sequence_number";
				colvarOriSequenceNumber.DataType = DbType.String;
				colvarOriSequenceNumber.MaxLength = 50;
				colvarOriSequenceNumber.AutoIncrement = false;
				colvarOriSequenceNumber.IsNullable = true;
				colvarOriSequenceNumber.IsPrimaryKey = false;
				colvarOriSequenceNumber.IsForeignKey = false;
				colvarOriSequenceNumber.IsReadOnly = false;
				colvarOriSequenceNumber.DefaultSetting = @"";
				colvarOriSequenceNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOriSequenceNumber);
				
				TableSchema.TableColumn colvarNewCouponId = new TableSchema.TableColumn(schema);
				colvarNewCouponId.ColumnName = "new_coupon_id";
				colvarNewCouponId.DataType = DbType.Int32;
				colvarNewCouponId.MaxLength = 0;
				colvarNewCouponId.AutoIncrement = false;
				colvarNewCouponId.IsNullable = true;
				colvarNewCouponId.IsPrimaryKey = false;
				colvarNewCouponId.IsForeignKey = false;
				colvarNewCouponId.IsReadOnly = false;
				colvarNewCouponId.DefaultSetting = @"";
				colvarNewCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNewCouponId);
				
				TableSchema.TableColumn colvarNewSequenceNumber = new TableSchema.TableColumn(schema);
				colvarNewSequenceNumber.ColumnName = "new_sequence_number";
				colvarNewSequenceNumber.DataType = DbType.String;
				colvarNewSequenceNumber.MaxLength = 50;
				colvarNewSequenceNumber.AutoIncrement = false;
				colvarNewSequenceNumber.IsNullable = true;
				colvarNewSequenceNumber.IsPrimaryKey = false;
				colvarNewSequenceNumber.IsForeignKey = false;
				colvarNewSequenceNumber.IsReadOnly = false;
				colvarNewSequenceNumber.DefaultSetting = @"";
				colvarNewSequenceNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNewSequenceNumber);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 50;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = true;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("cash_trust_coupon_change_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		  
		[XmlAttribute("OriCouponId")]
		[Bindable(true)]
		public int? OriCouponId 
		{
			get { return GetColumnValue<int?>(Columns.OriCouponId); }
			set { SetColumnValue(Columns.OriCouponId, value); }
		}
		  
		[XmlAttribute("OriSequenceNumber")]
		[Bindable(true)]
		public string OriSequenceNumber 
		{
			get { return GetColumnValue<string>(Columns.OriSequenceNumber); }
			set { SetColumnValue(Columns.OriSequenceNumber, value); }
		}
		  
		[XmlAttribute("NewCouponId")]
		[Bindable(true)]
		public int? NewCouponId 
		{
			get { return GetColumnValue<int?>(Columns.NewCouponId); }
			set { SetColumnValue(Columns.NewCouponId, value); }
		}
		  
		[XmlAttribute("NewSequenceNumber")]
		[Bindable(true)]
		public string NewSequenceNumber 
		{
			get { return GetColumnValue<string>(Columns.NewSequenceNumber); }
			set { SetColumnValue(Columns.NewSequenceNumber, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OriCouponIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OriSequenceNumberColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn NewCouponIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn NewSequenceNumberColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TrustId = @"trust_id";
			 public static string OriCouponId = @"ori_coupon_id";
			 public static string OriSequenceNumber = @"ori_sequence_number";
			 public static string NewCouponId = @"new_coupon_id";
			 public static string NewSequenceNumber = @"new_sequence_number";
			 public static string OrderGuid = @"order_guid";
			 public static string Memo = @"memo";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

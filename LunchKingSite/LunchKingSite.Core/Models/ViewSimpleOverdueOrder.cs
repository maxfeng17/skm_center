using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewSimpleOverdueOrder class.
    /// </summary>
    [Serializable]
    public partial class ViewSimpleOverdueOrderCollection : ReadOnlyList<ViewSimpleOverdueOrder, ViewSimpleOverdueOrderCollection>
    {        
        public ViewSimpleOverdueOrderCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_simple_overdue_order view.
    /// </summary>
    [Serializable]
    public partial class ViewSimpleOverdueOrder : ReadOnlyRecord<ViewSimpleOverdueOrder>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_simple_overdue_order", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarLastShipDate = new TableSchema.TableColumn(schema);
                colvarLastShipDate.ColumnName = "last_ship_date";
                colvarLastShipDate.DataType = DbType.DateTime;
                colvarLastShipDate.MaxLength = 0;
                colvarLastShipDate.AutoIncrement = false;
                colvarLastShipDate.IsNullable = true;
                colvarLastShipDate.IsPrimaryKey = false;
                colvarLastShipDate.IsForeignKey = false;
                colvarLastShipDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastShipDate);
                
                TableSchema.TableColumn colvarShipTime = new TableSchema.TableColumn(schema);
                colvarShipTime.ColumnName = "ship_time";
                colvarShipTime.DataType = DbType.DateTime;
                colvarShipTime.MaxLength = 0;
                colvarShipTime.AutoIncrement = false;
                colvarShipTime.IsNullable = true;
                colvarShipTime.IsPrimaryKey = false;
                colvarShipTime.IsForeignKey = false;
                colvarShipTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipTime);
                
                TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
                colvarShipType.ColumnName = "ship_type";
                colvarShipType.DataType = DbType.Int32;
                colvarShipType.MaxLength = 0;
                colvarShipType.AutoIncrement = false;
                colvarShipType.IsNullable = true;
                colvarShipType.IsPrimaryKey = false;
                colvarShipType.IsForeignKey = false;
                colvarShipType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipType);
                
                TableSchema.TableColumn colvarShippingdateType = new TableSchema.TableColumn(schema);
                colvarShippingdateType.ColumnName = "shippingdate_type";
                colvarShippingdateType.DataType = DbType.Int32;
                colvarShippingdateType.MaxLength = 0;
                colvarShippingdateType.AutoIncrement = false;
                colvarShippingdateType.IsNullable = true;
                colvarShippingdateType.IsPrimaryKey = false;
                colvarShippingdateType.IsForeignKey = false;
                colvarShippingdateType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippingdateType);
                
                TableSchema.TableColumn colvarShippingdate = new TableSchema.TableColumn(schema);
                colvarShippingdate.ColumnName = "shippingdate";
                colvarShippingdate.DataType = DbType.Int32;
                colvarShippingdate.MaxLength = 0;
                colvarShippingdate.AutoIncrement = false;
                colvarShippingdate.IsNullable = true;
                colvarShippingdate.IsPrimaryKey = false;
                colvarShippingdate.IsForeignKey = false;
                colvarShippingdate.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippingdate);
                
                TableSchema.TableColumn colvarProductUseDateEndSet = new TableSchema.TableColumn(schema);
                colvarProductUseDateEndSet.ColumnName = "product_use_date_end_set";
                colvarProductUseDateEndSet.DataType = DbType.Int32;
                colvarProductUseDateEndSet.MaxLength = 0;
                colvarProductUseDateEndSet.AutoIncrement = false;
                colvarProductUseDateEndSet.IsNullable = true;
                colvarProductUseDateEndSet.IsPrimaryKey = false;
                colvarProductUseDateEndSet.IsForeignKey = false;
                colvarProductUseDateEndSet.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductUseDateEndSet);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_simple_overdue_order",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewSimpleOverdueOrder()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewSimpleOverdueOrder(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewSimpleOverdueOrder(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewSimpleOverdueOrder(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("LastShipDate")]
        [Bindable(true)]
        public DateTime? LastShipDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("last_ship_date");
		    }
            set 
		    {
			    SetColumnValue("last_ship_date", value);
            }
        }
	      
        [XmlAttribute("ShipTime")]
        [Bindable(true)]
        public DateTime? ShipTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ship_time");
		    }
            set 
		    {
			    SetColumnValue("ship_time", value);
            }
        }
	      
        [XmlAttribute("ShipType")]
        [Bindable(true)]
        public int? ShipType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ship_type");
		    }
            set 
		    {
			    SetColumnValue("ship_type", value);
            }
        }
	      
        [XmlAttribute("ShippingdateType")]
        [Bindable(true)]
        public int? ShippingdateType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("shippingdate_type");
		    }
            set 
		    {
			    SetColumnValue("shippingdate_type", value);
            }
        }
	      
        [XmlAttribute("Shippingdate")]
        [Bindable(true)]
        public int? Shippingdate 
	    {
		    get
		    {
			    return GetColumnValue<int?>("shippingdate");
		    }
            set 
		    {
			    SetColumnValue("shippingdate", value);
            }
        }
	      
        [XmlAttribute("ProductUseDateEndSet")]
        [Bindable(true)]
        public int? ProductUseDateEndSet 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_use_date_end_set");
		    }
            set 
		    {
			    SetColumnValue("product_use_date_end_set", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderGuid = @"order_guid";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string LastShipDate = @"last_ship_date";
            
            public static string ShipTime = @"ship_time";
            
            public static string ShipType = @"ship_type";
            
            public static string ShippingdateType = @"shippingdate_type";
            
            public static string Shippingdate = @"shippingdate";
            
            public static string ProductUseDateEndSet = @"product_use_date_end_set";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

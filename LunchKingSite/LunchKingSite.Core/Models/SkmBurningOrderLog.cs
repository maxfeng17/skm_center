using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmBurningOrderLog class.
	/// </summary>
    [Serializable]
	public partial class SkmBurningOrderLogCollection : RepositoryList<SkmBurningOrderLog, SkmBurningOrderLogCollection>
	{	   
		public SkmBurningOrderLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmBurningOrderLogCollection</returns>
		public SkmBurningOrderLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmBurningOrderLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_burning_order_log table.
	/// </summary>
	
	[Serializable]
	public partial class SkmBurningOrderLog : RepositoryRecord<SkmBurningOrderLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmBurningOrderLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmBurningOrderLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_burning_order_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
				colvarEventId.ColumnName = "event_id";
				colvarEventId.DataType = DbType.Int32;
				colvarEventId.MaxLength = 0;
				colvarEventId.AutoIncrement = false;
				colvarEventId.IsNullable = false;
				colvarEventId.IsPrimaryKey = false;
				colvarEventId.IsForeignKey = false;
				colvarEventId.IsReadOnly = false;
				colvarEventId.DefaultSetting = @"";
				colvarEventId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventId);
				
				TableSchema.TableColumn colvarSeqId = new TableSchema.TableColumn(schema);
				colvarSeqId.ColumnName = "seq_id";
				colvarSeqId.DataType = DbType.Int32;
				colvarSeqId.MaxLength = 0;
				colvarSeqId.AutoIncrement = false;
				colvarSeqId.IsNullable = false;
				colvarSeqId.IsPrimaryKey = false;
				colvarSeqId.IsForeignKey = false;
				colvarSeqId.IsReadOnly = false;
				colvarSeqId.DefaultSetting = @"";
				colvarSeqId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeqId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Byte;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				
						colvarModifyTime.DefaultSetting = @"(getdate())";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarRequestLog = new TableSchema.TableColumn(schema);
				colvarRequestLog.ColumnName = "request_log";
				colvarRequestLog.DataType = DbType.String;
				colvarRequestLog.MaxLength = -1;
				colvarRequestLog.AutoIncrement = false;
				colvarRequestLog.IsNullable = false;
				colvarRequestLog.IsPrimaryKey = false;
				colvarRequestLog.IsForeignKey = false;
				colvarRequestLog.IsReadOnly = false;
				
						colvarRequestLog.DefaultSetting = @"('')";
				colvarRequestLog.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRequestLog);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				
						colvarOrderGuid.DefaultSetting = @"('00000000-0000-0000-0000-000000000000')";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "coupon_id";
				colvarCouponId.DataType = DbType.Int32;
				colvarCouponId.MaxLength = 0;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = false;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = false;
				colvarCouponId.IsReadOnly = false;
				
						colvarCouponId.DefaultSetting = @"((0))";
				colvarCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponId);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				
						colvarBid.DefaultSetting = @"('00000000-0000-0000-0000-000000000000')";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarSendData = new TableSchema.TableColumn(schema);
				colvarSendData.ColumnName = "send_data";
				colvarSendData.DataType = DbType.String;
				colvarSendData.MaxLength = -1;
				colvarSendData.AutoIncrement = false;
				colvarSendData.IsNullable = true;
				colvarSendData.IsPrimaryKey = false;
				colvarSendData.IsForeignKey = false;
				colvarSendData.IsReadOnly = false;
				colvarSendData.DefaultSetting = @"";
				colvarSendData.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendData);
				
				TableSchema.TableColumn colvarReceivedData = new TableSchema.TableColumn(schema);
				colvarReceivedData.ColumnName = "received_data";
				colvarReceivedData.DataType = DbType.String;
				colvarReceivedData.MaxLength = -1;
				colvarReceivedData.AutoIncrement = false;
				colvarReceivedData.IsNullable = true;
				colvarReceivedData.IsPrimaryKey = false;
				colvarReceivedData.IsForeignKey = false;
				colvarReceivedData.IsReadOnly = false;
				colvarReceivedData.DefaultSetting = @"";
				colvarReceivedData.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceivedData);
				
				TableSchema.TableColumn colvarReceivedTime = new TableSchema.TableColumn(schema);
				colvarReceivedTime.ColumnName = "received_time";
				colvarReceivedTime.DataType = DbType.DateTime;
				colvarReceivedTime.MaxLength = 0;
				colvarReceivedTime.AutoIncrement = false;
				colvarReceivedTime.IsNullable = true;
				colvarReceivedTime.IsPrimaryKey = false;
				colvarReceivedTime.IsForeignKey = false;
				colvarReceivedTime.IsReadOnly = false;
				colvarReceivedTime.DefaultSetting = @"";
				colvarReceivedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceivedTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_burning_order_log",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("EventId")]
		[Bindable(true)]
		public int EventId 
		{
			get { return GetColumnValue<int>(Columns.EventId); }
			set { SetColumnValue(Columns.EventId, value); }
		}
		
		[XmlAttribute("SeqId")]
		[Bindable(true)]
		public int SeqId 
		{
			get { return GetColumnValue<int>(Columns.SeqId); }
			set { SetColumnValue(Columns.SeqId, value); }
		}
		
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("Status")]
		[Bindable(true)]
		public byte Status 
		{
			get { return GetColumnValue<byte>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		[XmlAttribute("RequestLog")]
		[Bindable(true)]
		public string RequestLog 
		{
			get { return GetColumnValue<string>(Columns.RequestLog); }
			set { SetColumnValue(Columns.RequestLog, value); }
		}
		
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		
		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public int CouponId 
		{
			get { return GetColumnValue<int>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}
		
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		
		[XmlAttribute("SendData")]
		[Bindable(true)]
		public string SendData 
		{
			get { return GetColumnValue<string>(Columns.SendData); }
			set { SetColumnValue(Columns.SendData, value); }
		}
		
		[XmlAttribute("ReceivedData")]
		[Bindable(true)]
		public string ReceivedData 
		{
			get { return GetColumnValue<string>(Columns.ReceivedData); }
			set { SetColumnValue(Columns.ReceivedData, value); }
		}
		
		[XmlAttribute("ReceivedTime")]
		[Bindable(true)]
		public DateTime? ReceivedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ReceivedTime); }
			set { SetColumnValue(Columns.ReceivedTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EventIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn RequestLogColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn SendDataColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceivedDataColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceivedTimeColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string EventId = @"event_id";
			 public static string SeqId = @"seq_id";
			 public static string UserId = @"user_id";
			 public static string CreateTime = @"create_time";
			 public static string Status = @"status";
			 public static string ModifyTime = @"modify_time";
			 public static string RequestLog = @"request_log";
			 public static string OrderGuid = @"order_guid";
			 public static string CouponId = @"coupon_id";
			 public static string Bid = @"bid";
			 public static string SendData = @"send_data";
			 public static string ReceivedData = @"received_data";
			 public static string ReceivedTime = @"received_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

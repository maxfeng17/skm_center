using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewBrandCollection : ReadOnlyList<ViewBrand, ViewBrandCollection>
	{
			public ViewBrandCollection() {}

	}

	[Serializable]
	public partial class ViewBrand : ReadOnlyRecord<ViewBrand>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewBrand()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewBrand(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewBrand(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewBrand(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_brand", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarBrandId = new TableSchema.TableColumn(schema);
				colvarBrandId.ColumnName = "brand_id";
				colvarBrandId.DataType = DbType.Int32;
				colvarBrandId.MaxLength = 0;
				colvarBrandId.AutoIncrement = false;
				colvarBrandId.IsNullable = false;
				colvarBrandId.IsPrimaryKey = false;
				colvarBrandId.IsForeignKey = false;
				colvarBrandId.IsReadOnly = false;
				colvarBrandId.DefaultSetting = @"";
				colvarBrandId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandId);

				TableSchema.TableColumn colvarBrandName = new TableSchema.TableColumn(schema);
				colvarBrandName.ColumnName = "brand_name";
				colvarBrandName.DataType = DbType.String;
				colvarBrandName.MaxLength = 200;
				colvarBrandName.AutoIncrement = false;
				colvarBrandName.IsNullable = false;
				colvarBrandName.IsPrimaryKey = false;
				colvarBrandName.IsForeignKey = false;
				colvarBrandName.IsReadOnly = false;
				colvarBrandName.DefaultSetting = @"";
				colvarBrandName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandName);

				TableSchema.TableColumn colvarAppBannerImage = new TableSchema.TableColumn(schema);
				colvarAppBannerImage.ColumnName = "app_banner_image";
				colvarAppBannerImage.DataType = DbType.String;
				colvarAppBannerImage.MaxLength = 200;
				colvarAppBannerImage.AutoIncrement = false;
				colvarAppBannerImage.IsNullable = true;
				colvarAppBannerImage.IsPrimaryKey = false;
				colvarAppBannerImage.IsForeignKey = false;
				colvarAppBannerImage.IsReadOnly = false;
				colvarAppBannerImage.DefaultSetting = @"";
				colvarAppBannerImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppBannerImage);

				TableSchema.TableColumn colvarAppPromoImage = new TableSchema.TableColumn(schema);
				colvarAppPromoImage.ColumnName = "app_promo_Image";
				colvarAppPromoImage.DataType = DbType.String;
				colvarAppPromoImage.MaxLength = 200;
				colvarAppPromoImage.AutoIncrement = false;
				colvarAppPromoImage.IsNullable = true;
				colvarAppPromoImage.IsPrimaryKey = false;
				colvarAppPromoImage.IsForeignKey = false;
				colvarAppPromoImage.IsReadOnly = false;
				colvarAppPromoImage.DefaultSetting = @"";
				colvarAppPromoImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppPromoImage);

				TableSchema.TableColumn colvarBrandItemId = new TableSchema.TableColumn(schema);
				colvarBrandItemId.ColumnName = "brand_item_id";
				colvarBrandItemId.DataType = DbType.Int32;
				colvarBrandItemId.MaxLength = 0;
				colvarBrandItemId.AutoIncrement = false;
				colvarBrandItemId.IsNullable = false;
				colvarBrandItemId.IsPrimaryKey = false;
				colvarBrandItemId.IsForeignKey = false;
				colvarBrandItemId.IsReadOnly = false;
				colvarBrandItemId.DefaultSetting = @"";
				colvarBrandItemId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandItemId);

				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = false;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				colvarSeq.DefaultSetting = @"";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);

				TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
				colvarItemId.ColumnName = "item_id";
				colvarItemId.DataType = DbType.Int32;
				colvarItemId.MaxLength = 0;
				colvarItemId.AutoIncrement = false;
				colvarItemId.IsNullable = false;
				colvarItemId.IsPrimaryKey = false;
				colvarItemId.IsForeignKey = false;
				colvarItemId.IsReadOnly = false;
				colvarItemId.DefaultSetting = @"";
				colvarItemId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemId);

				TableSchema.TableColumn colvarItemStatus = new TableSchema.TableColumn(schema);
				colvarItemStatus.ColumnName = "item_status";
				colvarItemStatus.DataType = DbType.Boolean;
				colvarItemStatus.MaxLength = 0;
				colvarItemStatus.AutoIncrement = false;
				colvarItemStatus.IsNullable = false;
				colvarItemStatus.IsPrimaryKey = false;
				colvarItemStatus.IsForeignKey = false;
				colvarItemStatus.IsReadOnly = false;
				colvarItemStatus.DefaultSetting = @"";
				colvarItemStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemStatus);

				TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
				colvarCategoryId.ColumnName = "category_id";
				colvarCategoryId.DataType = DbType.Int32;
				colvarCategoryId.MaxLength = 0;
				colvarCategoryId.AutoIncrement = false;
				colvarCategoryId.IsNullable = false;
				colvarCategoryId.IsPrimaryKey = false;
				colvarCategoryId.IsForeignKey = false;
				colvarCategoryId.IsReadOnly = false;
				colvarCategoryId.DefaultSetting = @"";
				colvarCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryId);

				TableSchema.TableColumn colvarCategoryName = new TableSchema.TableColumn(schema);
				colvarCategoryName.ColumnName = "category_name";
				colvarCategoryName.DataType = DbType.String;
				colvarCategoryName.MaxLength = 50;
				colvarCategoryName.AutoIncrement = false;
				colvarCategoryName.IsNullable = true;
				colvarCategoryName.IsPrimaryKey = false;
				colvarCategoryName.IsForeignKey = false;
				colvarCategoryName.IsReadOnly = false;
				colvarCategoryName.DefaultSetting = @"";
				colvarCategoryName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryName);

				TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
				colvarStartTime.ColumnName = "start_time";
				colvarStartTime.DataType = DbType.DateTime;
				colvarStartTime.MaxLength = 0;
				colvarStartTime.AutoIncrement = false;
				colvarStartTime.IsNullable = false;
				colvarStartTime.IsPrimaryKey = false;
				colvarStartTime.IsForeignKey = false;
				colvarStartTime.IsReadOnly = false;
				colvarStartTime.DefaultSetting = @"";
				colvarStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartTime);

				TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
				colvarEndTime.ColumnName = "end_time";
				colvarEndTime.DataType = DbType.DateTime;
				colvarEndTime.MaxLength = 0;
				colvarEndTime.AutoIncrement = false;
				colvarEndTime.IsNullable = false;
				colvarEndTime.IsPrimaryKey = false;
				colvarEndTime.IsForeignKey = false;
				colvarEndTime.IsReadOnly = false;
				colvarEndTime.DefaultSetting = @"";
				colvarEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndTime);

				TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
				colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeS.MaxLength = 0;
				colvarBusinessHourOrderTimeS.AutoIncrement = false;
				colvarBusinessHourOrderTimeS.IsNullable = false;
				colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeS.IsForeignKey = false;
				colvarBusinessHourOrderTimeS.IsReadOnly = false;
				colvarBusinessHourOrderTimeS.DefaultSetting = @"";
				colvarBusinessHourOrderTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeS);

				TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
				colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeE.MaxLength = 0;
				colvarBusinessHourOrderTimeE.AutoIncrement = false;
				colvarBusinessHourOrderTimeE.IsNullable = false;
				colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeE.IsForeignKey = false;
				colvarBusinessHourOrderTimeE.IsReadOnly = false;
				colvarBusinessHourOrderTimeE.DefaultSetting = @"";
				colvarBusinessHourOrderTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeE);

				TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
				colvarBusinessHourStatus.ColumnName = "business_hour_status";
				colvarBusinessHourStatus.DataType = DbType.Int32;
				colvarBusinessHourStatus.MaxLength = 0;
				colvarBusinessHourStatus.AutoIncrement = false;
				colvarBusinessHourStatus.IsNullable = false;
				colvarBusinessHourStatus.IsPrimaryKey = false;
				colvarBusinessHourStatus.IsForeignKey = false;
				colvarBusinessHourStatus.IsReadOnly = false;
				colvarBusinessHourStatus.DefaultSetting = @"";
				colvarBusinessHourStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourStatus);

				TableSchema.TableColumn colvarShowInApp = new TableSchema.TableColumn(schema);
				colvarShowInApp.ColumnName = "show_in_app";
				colvarShowInApp.DataType = DbType.Boolean;
				colvarShowInApp.MaxLength = 0;
				colvarShowInApp.AutoIncrement = false;
				colvarShowInApp.IsNullable = false;
				colvarShowInApp.IsPrimaryKey = false;
				colvarShowInApp.IsForeignKey = false;
				colvarShowInApp.IsReadOnly = false;
				colvarShowInApp.DefaultSetting = @"";
				colvarShowInApp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShowInApp);

				TableSchema.TableColumn colvarShowInWeb = new TableSchema.TableColumn(schema);
				colvarShowInWeb.ColumnName = "show_in_web";
				colvarShowInWeb.DataType = DbType.Boolean;
				colvarShowInWeb.MaxLength = 0;
				colvarShowInWeb.AutoIncrement = false;
				colvarShowInWeb.IsNullable = false;
				colvarShowInWeb.IsPrimaryKey = false;
				colvarShowInWeb.IsForeignKey = false;
				colvarShowInWeb.IsReadOnly = false;
				colvarShowInWeb.DefaultSetting = @"";
				colvarShowInWeb.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShowInWeb);

				TableSchema.TableColumn colvarBrandStatus = new TableSchema.TableColumn(schema);
				colvarBrandStatus.ColumnName = "brand_status";
				colvarBrandStatus.DataType = DbType.Boolean;
				colvarBrandStatus.MaxLength = 0;
				colvarBrandStatus.AutoIncrement = false;
				colvarBrandStatus.IsNullable = false;
				colvarBrandStatus.IsPrimaryKey = false;
				colvarBrandStatus.IsForeignKey = false;
				colvarBrandStatus.IsReadOnly = false;
				colvarBrandStatus.DefaultSetting = @"";
				colvarBrandStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandStatus);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_brand",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("BrandId")]
		[Bindable(true)]
		public int BrandId
		{
			get { return GetColumnValue<int>(Columns.BrandId); }
			set { SetColumnValue(Columns.BrandId, value); }
		}

		[XmlAttribute("BrandName")]
		[Bindable(true)]
		public string BrandName
		{
			get { return GetColumnValue<string>(Columns.BrandName); }
			set { SetColumnValue(Columns.BrandName, value); }
		}

		[XmlAttribute("AppBannerImage")]
		[Bindable(true)]
		public string AppBannerImage
		{
			get { return GetColumnValue<string>(Columns.AppBannerImage); }
			set { SetColumnValue(Columns.AppBannerImage, value); }
		}

		[XmlAttribute("AppPromoImage")]
		[Bindable(true)]
		public string AppPromoImage
		{
			get { return GetColumnValue<string>(Columns.AppPromoImage); }
			set { SetColumnValue(Columns.AppPromoImage, value); }
		}

		[XmlAttribute("BrandItemId")]
		[Bindable(true)]
		public int BrandItemId
		{
			get { return GetColumnValue<int>(Columns.BrandItemId); }
			set { SetColumnValue(Columns.BrandItemId, value); }
		}

		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int Seq
		{
			get { return GetColumnValue<int>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}

		[XmlAttribute("ItemId")]
		[Bindable(true)]
		public int ItemId
		{
			get { return GetColumnValue<int>(Columns.ItemId); }
			set { SetColumnValue(Columns.ItemId, value); }
		}

		[XmlAttribute("ItemStatus")]
		[Bindable(true)]
		public bool ItemStatus
		{
			get { return GetColumnValue<bool>(Columns.ItemStatus); }
			set { SetColumnValue(Columns.ItemStatus, value); }
		}

		[XmlAttribute("CategoryId")]
		[Bindable(true)]
		public int CategoryId
		{
			get { return GetColumnValue<int>(Columns.CategoryId); }
			set { SetColumnValue(Columns.CategoryId, value); }
		}

		[XmlAttribute("CategoryName")]
		[Bindable(true)]
		public string CategoryName
		{
			get { return GetColumnValue<string>(Columns.CategoryName); }
			set { SetColumnValue(Columns.CategoryName, value); }
		}

		[XmlAttribute("StartTime")]
		[Bindable(true)]
		public DateTime StartTime
		{
			get { return GetColumnValue<DateTime>(Columns.StartTime); }
			set { SetColumnValue(Columns.StartTime, value); }
		}

		[XmlAttribute("EndTime")]
		[Bindable(true)]
		public DateTime EndTime
		{
			get { return GetColumnValue<DateTime>(Columns.EndTime); }
			set { SetColumnValue(Columns.EndTime, value); }
		}

		[XmlAttribute("BusinessHourOrderTimeS")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeS
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeS); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeS, value); }
		}

		[XmlAttribute("BusinessHourOrderTimeE")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeE
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeE); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeE, value); }
		}

		[XmlAttribute("BusinessHourStatus")]
		[Bindable(true)]
		public int BusinessHourStatus
		{
			get { return GetColumnValue<int>(Columns.BusinessHourStatus); }
			set { SetColumnValue(Columns.BusinessHourStatus, value); }
		}

		[XmlAttribute("ShowInApp")]
		[Bindable(true)]
		public bool ShowInApp
		{
			get { return GetColumnValue<bool>(Columns.ShowInApp); }
			set { SetColumnValue(Columns.ShowInApp, value); }
		}

		[XmlAttribute("ShowInWeb")]
		[Bindable(true)]
		public bool ShowInWeb
		{
			get { return GetColumnValue<bool>(Columns.ShowInWeb); }
			set { SetColumnValue(Columns.ShowInWeb, value); }
		}

		[XmlAttribute("BrandStatus")]
		[Bindable(true)]
		public bool BrandStatus
		{
			get { return GetColumnValue<bool>(Columns.BrandStatus); }
			set { SetColumnValue(Columns.BrandStatus, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn BrandIdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn BrandNameColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn AppBannerImageColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn AppPromoImageColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn BrandItemIdColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn SeqColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn ItemIdColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn ItemStatusColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn CategoryIdColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn CategoryNameColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn StartTimeColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn EndTimeColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderTimeSColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderTimeEColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn BusinessHourStatusColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn ShowInAppColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn ShowInWebColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn BrandStatusColumn
		{
			get { return Schema.Columns[17]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string BrandId = @"brand_id";
			public static string BrandName = @"brand_name";
			public static string AppBannerImage = @"app_banner_image";
			public static string AppPromoImage = @"app_promo_Image";
			public static string BrandItemId = @"brand_item_id";
			public static string Seq = @"seq";
			public static string ItemId = @"item_id";
			public static string ItemStatus = @"item_status";
			public static string CategoryId = @"category_id";
			public static string CategoryName = @"category_name";
			public static string StartTime = @"start_time";
			public static string EndTime = @"end_time";
			public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
			public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
			public static string BusinessHourStatus = @"business_hour_status";
			public static string ShowInApp = @"show_in_app";
			public static string ShowInWeb = @"show_in_web";
			public static string BrandStatus = @"brand_status";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MemberMessage class.
	/// </summary>
    [Serializable]
	public partial class MemberMessageCollection : RepositoryList<MemberMessage, MemberMessageCollection>
	{	   
		public MemberMessageCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberMessageCollection</returns>
		public MemberMessageCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberMessage o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the member_message table.
	/// </summary>
	[Serializable]
	public partial class MemberMessage : RepositoryRecord<MemberMessage>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MemberMessage()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MemberMessage(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_message", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSenderId = new TableSchema.TableColumn(schema);
				colvarSenderId.ColumnName = "sender_id";
				colvarSenderId.DataType = DbType.Int32;
				colvarSenderId.MaxLength = 0;
				colvarSenderId.AutoIncrement = false;
				colvarSenderId.IsNullable = false;
				colvarSenderId.IsPrimaryKey = false;
				colvarSenderId.IsForeignKey = false;
				colvarSenderId.IsReadOnly = false;
				colvarSenderId.DefaultSetting = @"";
				colvarSenderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSenderId);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = -1;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarIsRead = new TableSchema.TableColumn(schema);
				colvarIsRead.ColumnName = "is_read";
				colvarIsRead.DataType = DbType.Boolean;
				colvarIsRead.MaxLength = 0;
				colvarIsRead.AutoIncrement = false;
				colvarIsRead.IsNullable = false;
				colvarIsRead.IsPrimaryKey = false;
				colvarIsRead.IsForeignKey = false;
				colvarIsRead.IsReadOnly = false;
				
						colvarIsRead.DefaultSetting = @"((0))";
				colvarIsRead.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsRead);
				
				TableSchema.TableColumn colvarSendTime = new TableSchema.TableColumn(schema);
				colvarSendTime.ColumnName = "send_time";
				colvarSendTime.DataType = DbType.DateTime;
				colvarSendTime.MaxLength = 0;
				colvarSendTime.AutoIncrement = false;
				colvarSendTime.IsNullable = false;
				colvarSendTime.IsPrimaryKey = false;
				colvarSendTime.IsForeignKey = false;
				colvarSendTime.IsReadOnly = false;
				colvarSendTime.DefaultSetting = @"";
				colvarSendTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendTime);
				
				TableSchema.TableColumn colvarReadTime = new TableSchema.TableColumn(schema);
				colvarReadTime.ColumnName = "read_time";
				colvarReadTime.DataType = DbType.DateTime;
				colvarReadTime.MaxLength = 0;
				colvarReadTime.AutoIncrement = false;
				colvarReadTime.IsNullable = true;
				colvarReadTime.IsPrimaryKey = false;
				colvarReadTime.IsForeignKey = false;
				colvarReadTime.IsReadOnly = false;
				colvarReadTime.DefaultSetting = @"";
				colvarReadTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReadTime);
				
				TableSchema.TableColumn colvarMessageType = new TableSchema.TableColumn(schema);
				colvarMessageType.ColumnName = "message_type";
				colvarMessageType.DataType = DbType.Int32;
				colvarMessageType.MaxLength = 0;
				colvarMessageType.AutoIncrement = false;
				colvarMessageType.IsNullable = true;
				colvarMessageType.IsPrimaryKey = false;
				colvarMessageType.IsForeignKey = false;
				colvarMessageType.IsReadOnly = false;
				colvarMessageType.DefaultSetting = @"";
				colvarMessageType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessageType);
				
				TableSchema.TableColumn colvarTemplateId = new TableSchema.TableColumn(schema);
				colvarTemplateId.ColumnName = "template_id";
				colvarTemplateId.DataType = DbType.Int32;
				colvarTemplateId.MaxLength = 0;
				colvarTemplateId.AutoIncrement = false;
				colvarTemplateId.IsNullable = true;
				colvarTemplateId.IsPrimaryKey = false;
				colvarTemplateId.IsForeignKey = false;
				colvarTemplateId.IsReadOnly = false;
				colvarTemplateId.DefaultSetting = @"";
				colvarTemplateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTemplateId);
				
				TableSchema.TableColumn colvarReceiverId = new TableSchema.TableColumn(schema);
				colvarReceiverId.ColumnName = "receiver_id";
				colvarReceiverId.DataType = DbType.Int32;
				colvarReceiverId.MaxLength = 0;
				colvarReceiverId.AutoIncrement = false;
				colvarReceiverId.IsNullable = true;
				colvarReceiverId.IsPrimaryKey = false;
				colvarReceiverId.IsForeignKey = false;
				colvarReceiverId.IsReadOnly = false;
				colvarReceiverId.DefaultSetting = @"";
				colvarReceiverId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverId);
				
				TableSchema.TableColumn colvarReceiveTime = new TableSchema.TableColumn(schema);
				colvarReceiveTime.ColumnName = "receive_time";
				colvarReceiveTime.DataType = DbType.DateTime;
				colvarReceiveTime.MaxLength = 0;
				colvarReceiveTime.AutoIncrement = false;
				colvarReceiveTime.IsNullable = true;
				colvarReceiveTime.IsPrimaryKey = false;
				colvarReceiveTime.IsForeignKey = false;
				colvarReceiveTime.IsReadOnly = false;
				colvarReceiveTime.DefaultSetting = @"";
				colvarReceiveTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiveTime);
				
				TableSchema.TableColumn colvarIsDeleted = new TableSchema.TableColumn(schema);
				colvarIsDeleted.ColumnName = "is_deleted";
				colvarIsDeleted.DataType = DbType.Boolean;
				colvarIsDeleted.MaxLength = 0;
				colvarIsDeleted.AutoIncrement = false;
				colvarIsDeleted.IsNullable = true;
				colvarIsDeleted.IsPrimaryKey = false;
				colvarIsDeleted.IsForeignKey = false;
				colvarIsDeleted.IsReadOnly = false;
				colvarIsDeleted.DefaultSetting = @"";
				colvarIsDeleted.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDeleted);
				
				TableSchema.TableColumn colvarReferMessageId = new TableSchema.TableColumn(schema);
				colvarReferMessageId.ColumnName = "refer_message_id";
				colvarReferMessageId.DataType = DbType.Int32;
				colvarReferMessageId.MaxLength = 0;
				colvarReferMessageId.AutoIncrement = false;
				colvarReferMessageId.IsNullable = true;
				colvarReferMessageId.IsPrimaryKey = false;
				colvarReferMessageId.IsForeignKey = false;
				colvarReferMessageId.IsReadOnly = false;
				colvarReferMessageId.DefaultSetting = @"";
				colvarReferMessageId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferMessageId);
				
				TableSchema.TableColumn colvarReferTargetGuid = new TableSchema.TableColumn(schema);
				colvarReferTargetGuid.ColumnName = "refer_target_guid";
				colvarReferTargetGuid.DataType = DbType.Guid;
				colvarReferTargetGuid.MaxLength = 0;
				colvarReferTargetGuid.AutoIncrement = false;
				colvarReferTargetGuid.IsNullable = true;
				colvarReferTargetGuid.IsPrimaryKey = false;
				colvarReferTargetGuid.IsForeignKey = false;
				colvarReferTargetGuid.IsReadOnly = false;
				colvarReferTargetGuid.DefaultSetting = @"";
				colvarReferTargetGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferTargetGuid);
				
				TableSchema.TableColumn colvarSenderName = new TableSchema.TableColumn(schema);
				colvarSenderName.ColumnName = "sender_name";
				colvarSenderName.DataType = DbType.String;
				colvarSenderName.MaxLength = 50;
				colvarSenderName.AutoIncrement = false;
				colvarSenderName.IsNullable = true;
				colvarSenderName.IsPrimaryKey = false;
				colvarSenderName.IsForeignKey = false;
				colvarSenderName.IsReadOnly = false;
				colvarSenderName.DefaultSetting = @"";
				colvarSenderName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSenderName);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_message",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SenderId")]
		[Bindable(true)]
		public int SenderId 
		{
			get { return GetColumnValue<int>(Columns.SenderId); }
			set { SetColumnValue(Columns.SenderId, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("IsRead")]
		[Bindable(true)]
		public bool IsRead 
		{
			get { return GetColumnValue<bool>(Columns.IsRead); }
			set { SetColumnValue(Columns.IsRead, value); }
		}
		  
		[XmlAttribute("SendTime")]
		[Bindable(true)]
		public DateTime SendTime 
		{
			get { return GetColumnValue<DateTime>(Columns.SendTime); }
			set { SetColumnValue(Columns.SendTime, value); }
		}
		  
		[XmlAttribute("ReadTime")]
		[Bindable(true)]
		public DateTime? ReadTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ReadTime); }
			set { SetColumnValue(Columns.ReadTime, value); }
		}
		  
		[XmlAttribute("MessageType")]
		[Bindable(true)]
		public int? MessageType 
		{
			get { return GetColumnValue<int?>(Columns.MessageType); }
			set { SetColumnValue(Columns.MessageType, value); }
		}
		  
		[XmlAttribute("TemplateId")]
		[Bindable(true)]
		public int? TemplateId 
		{
			get { return GetColumnValue<int?>(Columns.TemplateId); }
			set { SetColumnValue(Columns.TemplateId, value); }
		}
		  
		[XmlAttribute("ReceiverId")]
		[Bindable(true)]
		public int? ReceiverId 
		{
			get { return GetColumnValue<int?>(Columns.ReceiverId); }
			set { SetColumnValue(Columns.ReceiverId, value); }
		}
		  
		[XmlAttribute("ReceiveTime")]
		[Bindable(true)]
		public DateTime? ReceiveTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ReceiveTime); }
			set { SetColumnValue(Columns.ReceiveTime, value); }
		}
		  
		[XmlAttribute("IsDeleted")]
		[Bindable(true)]
		public bool? IsDeleted 
		{
			get { return GetColumnValue<bool?>(Columns.IsDeleted); }
			set { SetColumnValue(Columns.IsDeleted, value); }
		}
		  
		[XmlAttribute("ReferMessageId")]
		[Bindable(true)]
		public int? ReferMessageId 
		{
			get { return GetColumnValue<int?>(Columns.ReferMessageId); }
			set { SetColumnValue(Columns.ReferMessageId, value); }
		}
		  
		[XmlAttribute("ReferTargetGuid")]
		[Bindable(true)]
		public Guid? ReferTargetGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.ReferTargetGuid); }
			set { SetColumnValue(Columns.ReferTargetGuid, value); }
		}
		  
		[XmlAttribute("SenderName")]
		[Bindable(true)]
		public string SenderName 
		{
			get { return GetColumnValue<string>(Columns.SenderName); }
			set { SetColumnValue(Columns.SenderName, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SenderIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReadColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SendTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ReadTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageTypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn TemplateIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiveTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDeletedColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferMessageIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferTargetGuidColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn SenderNameColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SenderId = @"sender_id";
			 public static string Message = @"message";
			 public static string IsRead = @"is_read";
			 public static string SendTime = @"send_time";
			 public static string ReadTime = @"read_time";
			 public static string MessageType = @"message_type";
			 public static string TemplateId = @"template_id";
			 public static string ReceiverId = @"receiver_id";
			 public static string ReceiveTime = @"receive_time";
			 public static string IsDeleted = @"is_deleted";
			 public static string ReferMessageId = @"refer_message_id";
			 public static string ReferTargetGuid = @"refer_target_guid";
			 public static string SenderName = @"sender_name";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

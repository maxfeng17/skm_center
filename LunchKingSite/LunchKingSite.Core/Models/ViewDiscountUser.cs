using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewDiscountUser class.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountUserCollection : ReadOnlyList<ViewDiscountUser, ViewDiscountUserCollection>
    {        
        public ViewDiscountUserCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_discount_user view.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountUser : ReadOnlyRecord<ViewDiscountUser>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_discount_user", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCampaignId = new TableSchema.TableColumn(schema);
                colvarCampaignId.ColumnName = "campaign_id";
                colvarCampaignId.DataType = DbType.Int32;
                colvarCampaignId.MaxLength = 0;
                colvarCampaignId.AutoIncrement = false;
                colvarCampaignId.IsNullable = false;
                colvarCampaignId.IsPrimaryKey = false;
                colvarCampaignId.IsForeignKey = false;
                colvarCampaignId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCampaignId);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 100;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = true;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = true;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
                colvarFlag.ColumnName = "flag";
                colvarFlag.DataType = DbType.Int32;
                colvarFlag.MaxLength = 0;
                colvarFlag.AutoIncrement = false;
                colvarFlag.IsNullable = false;
                colvarFlag.IsPrimaryKey = false;
                colvarFlag.IsForeignKey = false;
                colvarFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarFlag);
                
                TableSchema.TableColumn colvarMinimumAmount = new TableSchema.TableColumn(schema);
                colvarMinimumAmount.ColumnName = "minimum_amount";
                colvarMinimumAmount.DataType = DbType.Int32;
                colvarMinimumAmount.MaxLength = 0;
                colvarMinimumAmount.AutoIncrement = false;
                colvarMinimumAmount.IsNullable = true;
                colvarMinimumAmount.IsPrimaryKey = false;
                colvarMinimumAmount.IsForeignKey = false;
                colvarMinimumAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarMinimumAmount);
                
                TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
                colvarEventId.ColumnName = "event_id";
                colvarEventId.DataType = DbType.Int32;
                colvarEventId.MaxLength = 0;
                colvarEventId.AutoIncrement = false;
                colvarEventId.IsNullable = false;
                colvarEventId.IsPrimaryKey = false;
                colvarEventId.IsForeignKey = false;
                colvarEventId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventId);
                
                TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
                colvarEventName.ColumnName = "event_name";
                colvarEventName.DataType = DbType.String;
                colvarEventName.MaxLength = 200;
                colvarEventName.AutoIncrement = false;
                colvarEventName.IsNullable = false;
                colvarEventName.IsPrimaryKey = false;
                colvarEventName.IsForeignKey = false;
                colvarEventName.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventName);
                
                TableSchema.TableColumn colvarEventStartTime = new TableSchema.TableColumn(schema);
                colvarEventStartTime.ColumnName = "event_start_time";
                colvarEventStartTime.DataType = DbType.DateTime;
                colvarEventStartTime.MaxLength = 0;
                colvarEventStartTime.AutoIncrement = false;
                colvarEventStartTime.IsNullable = false;
                colvarEventStartTime.IsPrimaryKey = false;
                colvarEventStartTime.IsForeignKey = false;
                colvarEventStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventStartTime);
                
                TableSchema.TableColumn colvarEventEndTime = new TableSchema.TableColumn(schema);
                colvarEventEndTime.ColumnName = "event_end_time";
                colvarEventEndTime.DataType = DbType.DateTime;
                colvarEventEndTime.MaxLength = 0;
                colvarEventEndTime.AutoIncrement = false;
                colvarEventEndTime.IsNullable = false;
                colvarEventEndTime.IsPrimaryKey = false;
                colvarEventEndTime.IsForeignKey = false;
                colvarEventEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventEndTime);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
                colvarQty.ColumnName = "qty";
                colvarQty.DataType = DbType.Int32;
                colvarQty.MaxLength = 0;
                colvarQty.AutoIncrement = false;
                colvarQty.IsNullable = false;
                colvarQty.IsPrimaryKey = false;
                colvarQty.IsForeignKey = false;
                colvarQty.IsReadOnly = false;
                
                schema.Columns.Add(colvarQty);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarDiscountUserStatus = new TableSchema.TableColumn(schema);
                colvarDiscountUserStatus.ColumnName = "discount_user_status";
                colvarDiscountUserStatus.DataType = DbType.Int32;
                colvarDiscountUserStatus.MaxLength = 0;
                colvarDiscountUserStatus.AutoIncrement = false;
                colvarDiscountUserStatus.IsNullable = false;
                colvarDiscountUserStatus.IsPrimaryKey = false;
                colvarDiscountUserStatus.IsForeignKey = false;
                colvarDiscountUserStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountUserStatus);
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_discount_user",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewDiscountUser()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewDiscountUser(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewDiscountUser(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewDiscountUser(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CampaignId")]
        [Bindable(true)]
        public int CampaignId 
	    {
		    get
		    {
			    return GetColumnValue<int>("campaign_id");
		    }
            set 
		    {
			    SetColumnValue("campaign_id", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal? Amount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("amount");
		    }
            set 
		    {
			    SetColumnValue("amount", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime? StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	      
        [XmlAttribute("Flag")]
        [Bindable(true)]
        public int Flag 
	    {
		    get
		    {
			    return GetColumnValue<int>("flag");
		    }
            set 
		    {
			    SetColumnValue("flag", value);
            }
        }
	      
        [XmlAttribute("MinimumAmount")]
        [Bindable(true)]
        public int? MinimumAmount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("minimum_amount");
		    }
            set 
		    {
			    SetColumnValue("minimum_amount", value);
            }
        }
	      
        [XmlAttribute("EventId")]
        [Bindable(true)]
        public int EventId 
	    {
		    get
		    {
			    return GetColumnValue<int>("event_id");
		    }
            set 
		    {
			    SetColumnValue("event_id", value);
            }
        }
	      
        [XmlAttribute("EventName")]
        [Bindable(true)]
        public string EventName 
	    {
		    get
		    {
			    return GetColumnValue<string>("event_name");
		    }
            set 
		    {
			    SetColumnValue("event_name", value);
            }
        }
	      
        [XmlAttribute("EventStartTime")]
        [Bindable(true)]
        public DateTime EventStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("event_start_time");
		    }
            set 
		    {
			    SetColumnValue("event_start_time", value);
            }
        }
	      
        [XmlAttribute("EventEndTime")]
        [Bindable(true)]
        public DateTime EventEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("event_end_time");
		    }
            set 
		    {
			    SetColumnValue("event_end_time", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("Qty")]
        [Bindable(true)]
        public int Qty 
	    {
		    get
		    {
			    return GetColumnValue<int>("qty");
		    }
            set 
		    {
			    SetColumnValue("qty", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("DiscountUserStatus")]
        [Bindable(true)]
        public int DiscountUserStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("discount_user_status");
		    }
            set 
		    {
			    SetColumnValue("discount_user_status", value);
            }
        }
	      
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CampaignId = @"campaign_id";
            
            public static string Name = @"name";
            
            public static string Amount = @"amount";
            
            public static string StartTime = @"start_time";
            
            public static string EndTime = @"end_time";
            
            public static string Flag = @"flag";
            
            public static string MinimumAmount = @"minimum_amount";
            
            public static string EventId = @"event_id";
            
            public static string EventName = @"event_name";
            
            public static string EventStartTime = @"event_start_time";
            
            public static string EventEndTime = @"event_end_time";
            
            public static string Status = @"status";
            
            public static string Qty = @"qty";
            
            public static string UserId = @"user_id";
            
            public static string DiscountUserStatus = @"discount_user_status";
            
            public static string Id = @"id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

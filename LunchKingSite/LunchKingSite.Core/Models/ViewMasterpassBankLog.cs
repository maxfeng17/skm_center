using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMasterpassBankLog class.
    /// </summary>
    [Serializable]
    public partial class ViewMasterpassBankLogCollection : ReadOnlyList<ViewMasterpassBankLog, ViewMasterpassBankLogCollection>
    {        
        public ViewMasterpassBankLogCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_masterpass_bank_log view.
    /// </summary>
    [Serializable]
    public partial class ViewMasterpassBankLog : ReadOnlyRecord<ViewMasterpassBankLog>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_masterpass_bank_log", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarUniqueid = new TableSchema.TableColumn(schema);
                colvarUniqueid.ColumnName = "uniqueid";
                colvarUniqueid.DataType = DbType.Int32;
                colvarUniqueid.MaxLength = 0;
                colvarUniqueid.AutoIncrement = false;
                colvarUniqueid.IsNullable = false;
                colvarUniqueid.IsPrimaryKey = false;
                colvarUniqueid.IsForeignKey = false;
                colvarUniqueid.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueid);
                
                TableSchema.TableColumn colvarCardBrandId = new TableSchema.TableColumn(schema);
                colvarCardBrandId.ColumnName = "card_brand_id";
                colvarCardBrandId.DataType = DbType.String;
                colvarCardBrandId.MaxLength = 50;
                colvarCardBrandId.AutoIncrement = false;
                colvarCardBrandId.IsNullable = true;
                colvarCardBrandId.IsPrimaryKey = false;
                colvarCardBrandId.IsForeignKey = false;
                colvarCardBrandId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardBrandId);
                
                TableSchema.TableColumn colvarIndicator = new TableSchema.TableColumn(schema);
                colvarIndicator.ColumnName = "indicator";
                colvarIndicator.DataType = DbType.String;
                colvarIndicator.MaxLength = 10;
                colvarIndicator.AutoIncrement = false;
                colvarIndicator.IsNullable = true;
                colvarIndicator.IsPrimaryKey = false;
                colvarIndicator.IsForeignKey = false;
                colvarIndicator.IsReadOnly = false;
                
                schema.Columns.Add(colvarIndicator);
                
                TableSchema.TableColumn colvarCardBrandName = new TableSchema.TableColumn(schema);
                colvarCardBrandName.ColumnName = "card_brand_name";
                colvarCardBrandName.DataType = DbType.String;
                colvarCardBrandName.MaxLength = 50;
                colvarCardBrandName.AutoIncrement = false;
                colvarCardBrandName.IsNullable = true;
                colvarCardBrandName.IsPrimaryKey = false;
                colvarCardBrandName.IsForeignKey = false;
                colvarCardBrandName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardBrandName);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarBankName = new TableSchema.TableColumn(schema);
                colvarBankName.ColumnName = "bank_name";
                colvarBankName.DataType = DbType.String;
                colvarBankName.MaxLength = 255;
                colvarBankName.AutoIncrement = false;
                colvarBankName.IsNullable = false;
                colvarBankName.IsPrimaryKey = false;
                colvarBankName.IsForeignKey = false;
                colvarBankName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBankName);
                
                TableSchema.TableColumn colvarBankId = new TableSchema.TableColumn(schema);
                colvarBankId.ColumnName = "bank_id";
                colvarBankId.DataType = DbType.Int32;
                colvarBankId.MaxLength = 0;
                colvarBankId.AutoIncrement = false;
                colvarBankId.IsNullable = false;
                colvarBankId.IsPrimaryKey = false;
                colvarBankId.IsForeignKey = false;
                colvarBankId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBankId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_masterpass_bank_log",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMasterpassBankLog()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMasterpassBankLog(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMasterpassBankLog(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMasterpassBankLog(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("Uniqueid")]
        [Bindable(true)]
        public int Uniqueid 
	    {
		    get
		    {
			    return GetColumnValue<int>("uniqueid");
		    }
            set 
		    {
			    SetColumnValue("uniqueid", value);
            }
        }
	      
        [XmlAttribute("CardBrandId")]
        [Bindable(true)]
        public string CardBrandId 
	    {
		    get
		    {
			    return GetColumnValue<string>("card_brand_id");
		    }
            set 
		    {
			    SetColumnValue("card_brand_id", value);
            }
        }
	      
        [XmlAttribute("Indicator")]
        [Bindable(true)]
        public string Indicator 
	    {
		    get
		    {
			    return GetColumnValue<string>("indicator");
		    }
            set 
		    {
			    SetColumnValue("indicator", value);
            }
        }
	      
        [XmlAttribute("CardBrandName")]
        [Bindable(true)]
        public string CardBrandName 
	    {
		    get
		    {
			    return GetColumnValue<string>("card_brand_name");
		    }
            set 
		    {
			    SetColumnValue("card_brand_name", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("BankName")]
        [Bindable(true)]
        public string BankName 
	    {
		    get
		    {
			    return GetColumnValue<string>("bank_name");
		    }
            set 
		    {
			    SetColumnValue("bank_name", value);
            }
        }
	      
        [XmlAttribute("BankId")]
        [Bindable(true)]
        public int BankId 
	    {
		    get
		    {
			    return GetColumnValue<int>("bank_id");
		    }
            set 
		    {
			    SetColumnValue("bank_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderGuid = @"order_guid";
            
            public static string Uniqueid = @"uniqueid";
            
            public static string CardBrandId = @"card_brand_id";
            
            public static string Indicator = @"indicator";
            
            public static string CardBrandName = @"card_brand_name";
            
            public static string CreateTime = @"create_time";
            
            public static string BankName = @"bank_name";
            
            public static string BankId = @"bank_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

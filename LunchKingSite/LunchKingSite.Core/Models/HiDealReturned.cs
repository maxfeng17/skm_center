using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealReturned class.
	/// </summary>
    [Serializable]
	public partial class HiDealReturnedCollection : RepositoryList<HiDealReturned, HiDealReturnedCollection>
	{	   
		public HiDealReturnedCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealReturnedCollection</returns>
		public HiDealReturnedCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealReturned o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_returned table.
	/// </summary>
	[Serializable]
	public partial class HiDealReturned : RepositoryRecord<HiDealReturned>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealReturned()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealReturned(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_returned", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderPk = new TableSchema.TableColumn(schema);
				colvarOrderPk.ColumnName = "order_pk";
				colvarOrderPk.DataType = DbType.Int32;
				colvarOrderPk.MaxLength = 0;
				colvarOrderPk.AutoIncrement = false;
				colvarOrderPk.IsNullable = false;
				colvarOrderPk.IsPrimaryKey = false;
				colvarOrderPk.IsForeignKey = true;
				colvarOrderPk.IsReadOnly = false;
				colvarOrderPk.DefaultSetting = @"";
				
					colvarOrderPk.ForeignKeyTableName = "hi_deal_order";
				schema.Columns.Add(colvarOrderPk);
				
				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = false;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);
				
				TableSchema.TableColumn colvarTotalAmount = new TableSchema.TableColumn(schema);
				colvarTotalAmount.ColumnName = "total_amount";
				colvarTotalAmount.DataType = DbType.Currency;
				colvarTotalAmount.MaxLength = 0;
				colvarTotalAmount.AutoIncrement = false;
				colvarTotalAmount.IsNullable = false;
				colvarTotalAmount.IsPrimaryKey = false;
				colvarTotalAmount.IsForeignKey = false;
				colvarTotalAmount.IsReadOnly = false;
				
						colvarTotalAmount.DefaultSetting = @"((0))";
				colvarTotalAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalAmount);
				
				TableSchema.TableColumn colvarPcash = new TableSchema.TableColumn(schema);
				colvarPcash.ColumnName = "pcash";
				colvarPcash.DataType = DbType.Currency;
				colvarPcash.MaxLength = 0;
				colvarPcash.AutoIncrement = false;
				colvarPcash.IsNullable = false;
				colvarPcash.IsPrimaryKey = false;
				colvarPcash.IsForeignKey = false;
				colvarPcash.IsReadOnly = false;
				
						colvarPcash.DefaultSetting = @"((0))";
				colvarPcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPcash);
				
				TableSchema.TableColumn colvarScash = new TableSchema.TableColumn(schema);
				colvarScash.ColumnName = "scash";
				colvarScash.DataType = DbType.Currency;
				colvarScash.MaxLength = 0;
				colvarScash.AutoIncrement = false;
				colvarScash.IsNullable = false;
				colvarScash.IsPrimaryKey = false;
				colvarScash.IsForeignKey = false;
				colvarScash.IsReadOnly = false;
				
						colvarScash.DefaultSetting = @"((0))";
				colvarScash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScash);
				
				TableSchema.TableColumn colvarBcash = new TableSchema.TableColumn(schema);
				colvarBcash.ColumnName = "bcash";
				colvarBcash.DataType = DbType.Currency;
				colvarBcash.MaxLength = 0;
				colvarBcash.AutoIncrement = false;
				colvarBcash.IsNullable = false;
				colvarBcash.IsPrimaryKey = false;
				colvarBcash.IsForeignKey = false;
				colvarBcash.IsReadOnly = false;
				
						colvarBcash.DefaultSetting = @"((0))";
				colvarBcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBcash);
				
				TableSchema.TableColumn colvarCreditCardAmt = new TableSchema.TableColumn(schema);
				colvarCreditCardAmt.ColumnName = "credit_card_amt";
				colvarCreditCardAmt.DataType = DbType.Currency;
				colvarCreditCardAmt.MaxLength = 0;
				colvarCreditCardAmt.AutoIncrement = false;
				colvarCreditCardAmt.IsNullable = false;
				colvarCreditCardAmt.IsPrimaryKey = false;
				colvarCreditCardAmt.IsForeignKey = false;
				colvarCreditCardAmt.IsReadOnly = false;
				
						colvarCreditCardAmt.DefaultSetting = @"((0))";
				colvarCreditCardAmt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditCardAmt);
				
				TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
				colvarDiscountAmount.ColumnName = "discount_amount";
				colvarDiscountAmount.DataType = DbType.Currency;
				colvarDiscountAmount.MaxLength = 0;
				colvarDiscountAmount.AutoIncrement = false;
				colvarDiscountAmount.IsNullable = false;
				colvarDiscountAmount.IsPrimaryKey = false;
				colvarDiscountAmount.IsForeignKey = false;
				colvarDiscountAmount.IsReadOnly = false;
				
						colvarDiscountAmount.DefaultSetting = @"((0))";
				colvarDiscountAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountAmount);
				
				TableSchema.TableColumn colvarAtmAmount = new TableSchema.TableColumn(schema);
				colvarAtmAmount.ColumnName = "atm_amount";
				colvarAtmAmount.DataType = DbType.Currency;
				colvarAtmAmount.MaxLength = 0;
				colvarAtmAmount.AutoIncrement = false;
				colvarAtmAmount.IsNullable = false;
				colvarAtmAmount.IsPrimaryKey = false;
				colvarAtmAmount.IsForeignKey = false;
				colvarAtmAmount.IsReadOnly = false;
				
						colvarAtmAmount.DefaultSetting = @"((0))";
				colvarAtmAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAtmAmount);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarCashBack = new TableSchema.TableColumn(schema);
				colvarCashBack.ColumnName = "cash_back";
				colvarCashBack.DataType = DbType.Boolean;
				colvarCashBack.MaxLength = 0;
				colvarCashBack.AutoIncrement = false;
				colvarCashBack.IsNullable = false;
				colvarCashBack.IsPrimaryKey = false;
				colvarCashBack.IsForeignKey = false;
				colvarCashBack.IsReadOnly = false;
				colvarCashBack.DefaultSetting = @"";
				colvarCashBack.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCashBack);
				
				TableSchema.TableColumn colvarReturnReason = new TableSchema.TableColumn(schema);
				colvarReturnReason.ColumnName = "return_reason";
				colvarReturnReason.DataType = DbType.String;
				colvarReturnReason.MaxLength = -1;
				colvarReturnReason.AutoIncrement = false;
				colvarReturnReason.IsNullable = true;
				colvarReturnReason.IsPrimaryKey = false;
				colvarReturnReason.IsForeignKey = false;
				colvarReturnReason.IsReadOnly = false;
				colvarReturnReason.DefaultSetting = @"";
				colvarReturnReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnReason);
				
				TableSchema.TableColumn colvarReturnTotalAmount = new TableSchema.TableColumn(schema);
				colvarReturnTotalAmount.ColumnName = "return_total_amount";
				colvarReturnTotalAmount.DataType = DbType.Currency;
				colvarReturnTotalAmount.MaxLength = 0;
				colvarReturnTotalAmount.AutoIncrement = false;
				colvarReturnTotalAmount.IsNullable = false;
				colvarReturnTotalAmount.IsPrimaryKey = false;
				colvarReturnTotalAmount.IsForeignKey = false;
				colvarReturnTotalAmount.IsReadOnly = false;
				
						colvarReturnTotalAmount.DefaultSetting = @"((0))";
				colvarReturnTotalAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnTotalAmount);
				
				TableSchema.TableColumn colvarReturnPcash = new TableSchema.TableColumn(schema);
				colvarReturnPcash.ColumnName = "return_pcash";
				colvarReturnPcash.DataType = DbType.Currency;
				colvarReturnPcash.MaxLength = 0;
				colvarReturnPcash.AutoIncrement = false;
				colvarReturnPcash.IsNullable = false;
				colvarReturnPcash.IsPrimaryKey = false;
				colvarReturnPcash.IsForeignKey = false;
				colvarReturnPcash.IsReadOnly = false;
				
						colvarReturnPcash.DefaultSetting = @"((0))";
				colvarReturnPcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnPcash);
				
				TableSchema.TableColumn colvarReturnScash = new TableSchema.TableColumn(schema);
				colvarReturnScash.ColumnName = "return_scash";
				colvarReturnScash.DataType = DbType.Currency;
				colvarReturnScash.MaxLength = 0;
				colvarReturnScash.AutoIncrement = false;
				colvarReturnScash.IsNullable = false;
				colvarReturnScash.IsPrimaryKey = false;
				colvarReturnScash.IsForeignKey = false;
				colvarReturnScash.IsReadOnly = false;
				
						colvarReturnScash.DefaultSetting = @"((0))";
				colvarReturnScash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnScash);
				
				TableSchema.TableColumn colvarReturnBcash = new TableSchema.TableColumn(schema);
				colvarReturnBcash.ColumnName = "return_bcash";
				colvarReturnBcash.DataType = DbType.Currency;
				colvarReturnBcash.MaxLength = 0;
				colvarReturnBcash.AutoIncrement = false;
				colvarReturnBcash.IsNullable = false;
				colvarReturnBcash.IsPrimaryKey = false;
				colvarReturnBcash.IsForeignKey = false;
				colvarReturnBcash.IsReadOnly = false;
				
						colvarReturnBcash.DefaultSetting = @"((0))";
				colvarReturnBcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnBcash);
				
				TableSchema.TableColumn colvarReturnCreditCardAmt = new TableSchema.TableColumn(schema);
				colvarReturnCreditCardAmt.ColumnName = "return_credit_card_amt";
				colvarReturnCreditCardAmt.DataType = DbType.Currency;
				colvarReturnCreditCardAmt.MaxLength = 0;
				colvarReturnCreditCardAmt.AutoIncrement = false;
				colvarReturnCreditCardAmt.IsNullable = false;
				colvarReturnCreditCardAmt.IsPrimaryKey = false;
				colvarReturnCreditCardAmt.IsForeignKey = false;
				colvarReturnCreditCardAmt.IsReadOnly = false;
				
						colvarReturnCreditCardAmt.DefaultSetting = @"((0))";
				colvarReturnCreditCardAmt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnCreditCardAmt);
				
				TableSchema.TableColumn colvarReturnDiscountAmount = new TableSchema.TableColumn(schema);
				colvarReturnDiscountAmount.ColumnName = "return_discount_amount";
				colvarReturnDiscountAmount.DataType = DbType.Currency;
				colvarReturnDiscountAmount.MaxLength = 0;
				colvarReturnDiscountAmount.AutoIncrement = false;
				colvarReturnDiscountAmount.IsNullable = false;
				colvarReturnDiscountAmount.IsPrimaryKey = false;
				colvarReturnDiscountAmount.IsForeignKey = false;
				colvarReturnDiscountAmount.IsReadOnly = false;
				
						colvarReturnDiscountAmount.DefaultSetting = @"((0))";
				colvarReturnDiscountAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnDiscountAmount);
				
				TableSchema.TableColumn colvarReturnAtmAmount = new TableSchema.TableColumn(schema);
				colvarReturnAtmAmount.ColumnName = "return_atm_amount";
				colvarReturnAtmAmount.DataType = DbType.Currency;
				colvarReturnAtmAmount.MaxLength = 0;
				colvarReturnAtmAmount.AutoIncrement = false;
				colvarReturnAtmAmount.IsNullable = false;
				colvarReturnAtmAmount.IsPrimaryKey = false;
				colvarReturnAtmAmount.IsForeignKey = false;
				colvarReturnAtmAmount.IsReadOnly = false;
				
						colvarReturnAtmAmount.DefaultSetting = @"((0))";
				colvarReturnAtmAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnAtmAmount);
				
				TableSchema.TableColumn colvarApplicationTime = new TableSchema.TableColumn(schema);
				colvarApplicationTime.ColumnName = "application_time";
				colvarApplicationTime.DataType = DbType.DateTime;
				colvarApplicationTime.MaxLength = 0;
				colvarApplicationTime.AutoIncrement = false;
				colvarApplicationTime.IsNullable = true;
				colvarApplicationTime.IsPrimaryKey = false;
				colvarApplicationTime.IsForeignKey = false;
				colvarApplicationTime.IsReadOnly = false;
				colvarApplicationTime.DefaultSetting = @"";
				colvarApplicationTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApplicationTime);
				
				TableSchema.TableColumn colvarReviewUserName = new TableSchema.TableColumn(schema);
				colvarReviewUserName.ColumnName = "review_user_name";
				colvarReviewUserName.DataType = DbType.String;
				colvarReviewUserName.MaxLength = 256;
				colvarReviewUserName.AutoIncrement = false;
				colvarReviewUserName.IsNullable = true;
				colvarReviewUserName.IsPrimaryKey = false;
				colvarReviewUserName.IsForeignKey = false;
				colvarReviewUserName.IsReadOnly = false;
				colvarReviewUserName.DefaultSetting = @"";
				colvarReviewUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReviewUserName);
				
				TableSchema.TableColumn colvarCompletedTime = new TableSchema.TableColumn(schema);
				colvarCompletedTime.ColumnName = "completed_time";
				colvarCompletedTime.DataType = DbType.DateTime;
				colvarCompletedTime.MaxLength = 0;
				colvarCompletedTime.AutoIncrement = false;
				colvarCompletedTime.IsNullable = true;
				colvarCompletedTime.IsPrimaryKey = false;
				colvarCompletedTime.IsForeignKey = false;
				colvarCompletedTime.IsReadOnly = false;
				colvarCompletedTime.DefaultSetting = @"";
				colvarCompletedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompletedTime);
				
				TableSchema.TableColumn colvarCancelTime = new TableSchema.TableColumn(schema);
				colvarCancelTime.ColumnName = "cancel_time";
				colvarCancelTime.DataType = DbType.DateTime;
				colvarCancelTime.MaxLength = 0;
				colvarCancelTime.AutoIncrement = false;
				colvarCancelTime.IsNullable = true;
				colvarCancelTime.IsPrimaryKey = false;
				colvarCancelTime.IsForeignKey = false;
				colvarCancelTime.IsReadOnly = false;
				colvarCancelTime.DefaultSetting = @"";
				colvarCancelTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelTime);
				
				TableSchema.TableColumn colvarCancelReason = new TableSchema.TableColumn(schema);
				colvarCancelReason.ColumnName = "cancel_reason";
				colvarCancelReason.DataType = DbType.String;
				colvarCancelReason.MaxLength = -1;
				colvarCancelReason.AutoIncrement = false;
				colvarCancelReason.IsNullable = true;
				colvarCancelReason.IsPrimaryKey = false;
				colvarCancelReason.IsForeignKey = false;
				colvarCancelReason.IsReadOnly = false;
				colvarCancelReason.DefaultSetting = @"";
				colvarCancelReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelReason);
				
				TableSchema.TableColumn colvarCancelUserName = new TableSchema.TableColumn(schema);
				colvarCancelUserName.ColumnName = "cancel_user_name";
				colvarCancelUserName.DataType = DbType.String;
				colvarCancelUserName.MaxLength = 256;
				colvarCancelUserName.AutoIncrement = false;
				colvarCancelUserName.IsNullable = true;
				colvarCancelUserName.IsPrimaryKey = false;
				colvarCancelUserName.IsForeignKey = false;
				colvarCancelUserName.IsReadOnly = false;
				colvarCancelUserName.DefaultSetting = @"";
				colvarCancelUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelUserName);
				
				TableSchema.TableColumn colvarCashBackTime = new TableSchema.TableColumn(schema);
				colvarCashBackTime.ColumnName = "cash_back_time";
				colvarCashBackTime.DataType = DbType.DateTime;
				colvarCashBackTime.MaxLength = 0;
				colvarCashBackTime.AutoIncrement = false;
				colvarCashBackTime.IsNullable = true;
				colvarCashBackTime.IsPrimaryKey = false;
				colvarCashBackTime.IsForeignKey = false;
				colvarCashBackTime.IsReadOnly = false;
				colvarCashBackTime.DefaultSetting = @"";
				colvarCashBackTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCashBackTime);
				
				TableSchema.TableColumn colvarRefundFormBack = new TableSchema.TableColumn(schema);
				colvarRefundFormBack.ColumnName = "refund_form_back";
				colvarRefundFormBack.DataType = DbType.Boolean;
				colvarRefundFormBack.MaxLength = 0;
				colvarRefundFormBack.AutoIncrement = false;
				colvarRefundFormBack.IsNullable = false;
				colvarRefundFormBack.IsPrimaryKey = false;
				colvarRefundFormBack.IsForeignKey = false;
				colvarRefundFormBack.IsReadOnly = false;
				
						colvarRefundFormBack.DefaultSetting = @"((0))";
				colvarRefundFormBack.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundFormBack);
				
				TableSchema.TableColumn colvarReturnCashBack = new TableSchema.TableColumn(schema);
				colvarReturnCashBack.ColumnName = "return_cash_back";
				colvarReturnCashBack.DataType = DbType.Boolean;
				colvarReturnCashBack.MaxLength = 0;
				colvarReturnCashBack.AutoIncrement = false;
				colvarReturnCashBack.IsNullable = false;
				colvarReturnCashBack.IsPrimaryKey = false;
				colvarReturnCashBack.IsForeignKey = false;
				colvarReturnCashBack.IsReadOnly = false;
				
						colvarReturnCashBack.DefaultSetting = @"((0))";
				colvarReturnCashBack.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnCashBack);
				
				TableSchema.TableColumn colvarFailTime = new TableSchema.TableColumn(schema);
				colvarFailTime.ColumnName = "fail_time";
				colvarFailTime.DataType = DbType.DateTime;
				colvarFailTime.MaxLength = 0;
				colvarFailTime.AutoIncrement = false;
				colvarFailTime.IsNullable = true;
				colvarFailTime.IsPrimaryKey = false;
				colvarFailTime.IsForeignKey = false;
				colvarFailTime.IsReadOnly = false;
				colvarFailTime.DefaultSetting = @"";
				colvarFailTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailTime);
				
				TableSchema.TableColumn colvarFailReason = new TableSchema.TableColumn(schema);
				colvarFailReason.ColumnName = "fail_reason";
				colvarFailReason.DataType = DbType.String;
				colvarFailReason.MaxLength = -1;
				colvarFailReason.AutoIncrement = false;
				colvarFailReason.IsNullable = true;
				colvarFailReason.IsPrimaryKey = false;
				colvarFailReason.IsForeignKey = false;
				colvarFailReason.IsReadOnly = false;
				colvarFailReason.DefaultSetting = @"";
				colvarFailReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailReason);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_returned",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderPk")]
		[Bindable(true)]
		public int OrderPk 
		{
			get { return GetColumnValue<int>(Columns.OrderPk); }
			set { SetColumnValue(Columns.OrderPk, value); }
		}
		  
		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId 
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}
		  
		[XmlAttribute("TotalAmount")]
		[Bindable(true)]
		public decimal TotalAmount 
		{
			get { return GetColumnValue<decimal>(Columns.TotalAmount); }
			set { SetColumnValue(Columns.TotalAmount, value); }
		}
		  
		[XmlAttribute("Pcash")]
		[Bindable(true)]
		public decimal Pcash 
		{
			get { return GetColumnValue<decimal>(Columns.Pcash); }
			set { SetColumnValue(Columns.Pcash, value); }
		}
		  
		[XmlAttribute("Scash")]
		[Bindable(true)]
		public decimal Scash 
		{
			get { return GetColumnValue<decimal>(Columns.Scash); }
			set { SetColumnValue(Columns.Scash, value); }
		}
		  
		[XmlAttribute("Bcash")]
		[Bindable(true)]
		public decimal Bcash 
		{
			get { return GetColumnValue<decimal>(Columns.Bcash); }
			set { SetColumnValue(Columns.Bcash, value); }
		}
		  
		[XmlAttribute("CreditCardAmt")]
		[Bindable(true)]
		public decimal CreditCardAmt 
		{
			get { return GetColumnValue<decimal>(Columns.CreditCardAmt); }
			set { SetColumnValue(Columns.CreditCardAmt, value); }
		}
		  
		[XmlAttribute("DiscountAmount")]
		[Bindable(true)]
		public decimal DiscountAmount 
		{
			get { return GetColumnValue<decimal>(Columns.DiscountAmount); }
			set { SetColumnValue(Columns.DiscountAmount, value); }
		}
		  
		[XmlAttribute("AtmAmount")]
		[Bindable(true)]
		public decimal AtmAmount 
		{
			get { return GetColumnValue<decimal>(Columns.AtmAmount); }
			set { SetColumnValue(Columns.AtmAmount, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("CashBack")]
		[Bindable(true)]
		public bool CashBack 
		{
			get { return GetColumnValue<bool>(Columns.CashBack); }
			set { SetColumnValue(Columns.CashBack, value); }
		}
		  
		[XmlAttribute("ReturnReason")]
		[Bindable(true)]
		public string ReturnReason 
		{
			get { return GetColumnValue<string>(Columns.ReturnReason); }
			set { SetColumnValue(Columns.ReturnReason, value); }
		}
		  
		[XmlAttribute("ReturnTotalAmount")]
		[Bindable(true)]
		public decimal ReturnTotalAmount 
		{
			get { return GetColumnValue<decimal>(Columns.ReturnTotalAmount); }
			set { SetColumnValue(Columns.ReturnTotalAmount, value); }
		}
		  
		[XmlAttribute("ReturnPcash")]
		[Bindable(true)]
		public decimal ReturnPcash 
		{
			get { return GetColumnValue<decimal>(Columns.ReturnPcash); }
			set { SetColumnValue(Columns.ReturnPcash, value); }
		}
		  
		[XmlAttribute("ReturnScash")]
		[Bindable(true)]
		public decimal ReturnScash 
		{
			get { return GetColumnValue<decimal>(Columns.ReturnScash); }
			set { SetColumnValue(Columns.ReturnScash, value); }
		}
		  
		[XmlAttribute("ReturnBcash")]
		[Bindable(true)]
		public decimal ReturnBcash 
		{
			get { return GetColumnValue<decimal>(Columns.ReturnBcash); }
			set { SetColumnValue(Columns.ReturnBcash, value); }
		}
		  
		[XmlAttribute("ReturnCreditCardAmt")]
		[Bindable(true)]
		public decimal ReturnCreditCardAmt 
		{
			get { return GetColumnValue<decimal>(Columns.ReturnCreditCardAmt); }
			set { SetColumnValue(Columns.ReturnCreditCardAmt, value); }
		}
		  
		[XmlAttribute("ReturnDiscountAmount")]
		[Bindable(true)]
		public decimal ReturnDiscountAmount 
		{
			get { return GetColumnValue<decimal>(Columns.ReturnDiscountAmount); }
			set { SetColumnValue(Columns.ReturnDiscountAmount, value); }
		}
		  
		[XmlAttribute("ReturnAtmAmount")]
		[Bindable(true)]
		public decimal ReturnAtmAmount 
		{
			get { return GetColumnValue<decimal>(Columns.ReturnAtmAmount); }
			set { SetColumnValue(Columns.ReturnAtmAmount, value); }
		}
		  
		[XmlAttribute("ApplicationTime")]
		[Bindable(true)]
		public DateTime? ApplicationTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ApplicationTime); }
			set { SetColumnValue(Columns.ApplicationTime, value); }
		}
		  
		[XmlAttribute("ReviewUserName")]
		[Bindable(true)]
		public string ReviewUserName 
		{
			get { return GetColumnValue<string>(Columns.ReviewUserName); }
			set { SetColumnValue(Columns.ReviewUserName, value); }
		}
		  
		[XmlAttribute("CompletedTime")]
		[Bindable(true)]
		public DateTime? CompletedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CompletedTime); }
			set { SetColumnValue(Columns.CompletedTime, value); }
		}
		  
		[XmlAttribute("CancelTime")]
		[Bindable(true)]
		public DateTime? CancelTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CancelTime); }
			set { SetColumnValue(Columns.CancelTime, value); }
		}
		  
		[XmlAttribute("CancelReason")]
		[Bindable(true)]
		public string CancelReason 
		{
			get { return GetColumnValue<string>(Columns.CancelReason); }
			set { SetColumnValue(Columns.CancelReason, value); }
		}
		  
		[XmlAttribute("CancelUserName")]
		[Bindable(true)]
		public string CancelUserName 
		{
			get { return GetColumnValue<string>(Columns.CancelUserName); }
			set { SetColumnValue(Columns.CancelUserName, value); }
		}
		  
		[XmlAttribute("CashBackTime")]
		[Bindable(true)]
		public DateTime? CashBackTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CashBackTime); }
			set { SetColumnValue(Columns.CashBackTime, value); }
		}
		  
		[XmlAttribute("RefundFormBack")]
		[Bindable(true)]
		public bool RefundFormBack 
		{
			get { return GetColumnValue<bool>(Columns.RefundFormBack); }
			set { SetColumnValue(Columns.RefundFormBack, value); }
		}
		  
		[XmlAttribute("ReturnCashBack")]
		[Bindable(true)]
		public bool ReturnCashBack 
		{
			get { return GetColumnValue<bool>(Columns.ReturnCashBack); }
			set { SetColumnValue(Columns.ReturnCashBack, value); }
		}
		  
		[XmlAttribute("FailTime")]
		[Bindable(true)]
		public DateTime? FailTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.FailTime); }
			set { SetColumnValue(Columns.FailTime, value); }
		}
		  
		[XmlAttribute("FailReason")]
		[Bindable(true)]
		public string FailReason 
		{
			get { return GetColumnValue<string>(Columns.FailReason); }
			set { SetColumnValue(Columns.FailReason, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderPkColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalAmountColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PcashColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ScashColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BcashColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreditCardAmtColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountAmountColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn AtmAmountColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CashBackColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnReasonColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnTotalAmountColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnPcashColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnScashColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnBcashColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnCreditCardAmtColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnDiscountAmountColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnAtmAmountColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn ApplicationTimeColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn ReviewUserNameColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn CompletedTimeColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelTimeColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelReasonColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelUserNameColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn CashBackTimeColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundFormBackColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnCashBackColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn FailTimeColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn FailReasonColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderPk = @"order_pk";
			 public static string OrderId = @"order_id";
			 public static string TotalAmount = @"total_amount";
			 public static string Pcash = @"pcash";
			 public static string Scash = @"scash";
			 public static string Bcash = @"bcash";
			 public static string CreditCardAmt = @"credit_card_amt";
			 public static string DiscountAmount = @"discount_amount";
			 public static string AtmAmount = @"atm_amount";
			 public static string Status = @"status";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string CashBack = @"cash_back";
			 public static string ReturnReason = @"return_reason";
			 public static string ReturnTotalAmount = @"return_total_amount";
			 public static string ReturnPcash = @"return_pcash";
			 public static string ReturnScash = @"return_scash";
			 public static string ReturnBcash = @"return_bcash";
			 public static string ReturnCreditCardAmt = @"return_credit_card_amt";
			 public static string ReturnDiscountAmount = @"return_discount_amount";
			 public static string ReturnAtmAmount = @"return_atm_amount";
			 public static string ApplicationTime = @"application_time";
			 public static string ReviewUserName = @"review_user_name";
			 public static string CompletedTime = @"completed_time";
			 public static string CancelTime = @"cancel_time";
			 public static string CancelReason = @"cancel_reason";
			 public static string CancelUserName = @"cancel_user_name";
			 public static string CashBackTime = @"cash_back_time";
			 public static string RefundFormBack = @"refund_form_back";
			 public static string ReturnCashBack = @"return_cash_back";
			 public static string FailTime = @"fail_time";
			 public static string FailReason = @"fail_reason";
			 public static string UserId = @"user_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

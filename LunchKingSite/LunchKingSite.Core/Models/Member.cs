using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Member class.
	/// </summary>
    [Serializable]
	public partial class MemberCollection : RepositoryList<Member, MemberCollection>
	{	   
		public MemberCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberCollection</returns>
		public MemberCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Member o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the member table.
	/// </summary>
	[Serializable]
	public partial class Member : RepositoryRecord<Member>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Member()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Member(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
				colvarUserName.ColumnName = "user_name";
				colvarUserName.DataType = DbType.String;
				colvarUserName.MaxLength = 256;
				colvarUserName.AutoIncrement = false;
				colvarUserName.IsNullable = false;
				colvarUserName.IsPrimaryKey = true;
				colvarUserName.IsForeignKey = false;
				colvarUserName.IsReadOnly = false;
				colvarUserName.DefaultSetting = @"";
				colvarUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserName);
				
				TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
				colvarLastName.ColumnName = "last_name";
				colvarLastName.DataType = DbType.String;
				colvarLastName.MaxLength = 50;
				colvarLastName.AutoIncrement = false;
				colvarLastName.IsNullable = true;
				colvarLastName.IsPrimaryKey = false;
				colvarLastName.IsForeignKey = false;
				colvarLastName.IsReadOnly = false;
				colvarLastName.DefaultSetting = @"";
				colvarLastName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastName);
				
				TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
				colvarFirstName.ColumnName = "first_name";
				colvarFirstName.DataType = DbType.String;
				colvarFirstName.MaxLength = 50;
				colvarFirstName.AutoIncrement = false;
				colvarFirstName.IsNullable = true;
				colvarFirstName.IsPrimaryKey = false;
				colvarFirstName.IsForeignKey = false;
				colvarFirstName.IsReadOnly = false;
				colvarFirstName.DefaultSetting = @"";
				colvarFirstName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFirstName);
				
				TableSchema.TableColumn colvarBuildingGuid = new TableSchema.TableColumn(schema);
				colvarBuildingGuid.ColumnName = "building_GUID";
				colvarBuildingGuid.DataType = DbType.Guid;
				colvarBuildingGuid.MaxLength = 0;
				colvarBuildingGuid.AutoIncrement = false;
				colvarBuildingGuid.IsNullable = true;
				colvarBuildingGuid.IsPrimaryKey = false;
				colvarBuildingGuid.IsForeignKey = true;
				colvarBuildingGuid.IsReadOnly = false;
				colvarBuildingGuid.DefaultSetting = @"";
				
					colvarBuildingGuid.ForeignKeyTableName = "building";
				schema.Columns.Add(colvarBuildingGuid);
				
				TableSchema.TableColumn colvarBirthday = new TableSchema.TableColumn(schema);
				colvarBirthday.ColumnName = "birthday";
				colvarBirthday.DataType = DbType.DateTime;
				colvarBirthday.MaxLength = 0;
				colvarBirthday.AutoIncrement = false;
				colvarBirthday.IsNullable = true;
				colvarBirthday.IsPrimaryKey = false;
				colvarBirthday.IsForeignKey = false;
				colvarBirthday.IsReadOnly = false;
				colvarBirthday.DefaultSetting = @"";
				colvarBirthday.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBirthday);
				
				TableSchema.TableColumn colvarGender = new TableSchema.TableColumn(schema);
				colvarGender.ColumnName = "gender";
				colvarGender.DataType = DbType.Int32;
				colvarGender.MaxLength = 0;
				colvarGender.AutoIncrement = false;
				colvarGender.IsNullable = true;
				colvarGender.IsPrimaryKey = false;
				colvarGender.IsForeignKey = false;
				colvarGender.IsReadOnly = false;
				colvarGender.DefaultSetting = @"";
				colvarGender.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGender);
				
				TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
				colvarMobile.ColumnName = "mobile";
				colvarMobile.DataType = DbType.AnsiString;
				colvarMobile.MaxLength = 50;
				colvarMobile.AutoIncrement = false;
				colvarMobile.IsNullable = true;
				colvarMobile.IsPrimaryKey = false;
				colvarMobile.IsForeignKey = false;
				colvarMobile.IsReadOnly = false;
				colvarMobile.DefaultSetting = @"";
				colvarMobile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobile);
				
				TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
				colvarCompanyName.ColumnName = "company_name";
				colvarCompanyName.DataType = DbType.String;
				colvarCompanyName.MaxLength = 50;
				colvarCompanyName.AutoIncrement = false;
				colvarCompanyName.IsNullable = true;
				colvarCompanyName.IsPrimaryKey = false;
				colvarCompanyName.IsForeignKey = false;
				colvarCompanyName.IsReadOnly = false;
				colvarCompanyName.DefaultSetting = @"";
				colvarCompanyName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyName);
				
				TableSchema.TableColumn colvarCompanyDepartment = new TableSchema.TableColumn(schema);
				colvarCompanyDepartment.ColumnName = "company_department";
				colvarCompanyDepartment.DataType = DbType.String;
				colvarCompanyDepartment.MaxLength = 50;
				colvarCompanyDepartment.AutoIncrement = false;
				colvarCompanyDepartment.IsNullable = true;
				colvarCompanyDepartment.IsPrimaryKey = false;
				colvarCompanyDepartment.IsForeignKey = false;
				colvarCompanyDepartment.IsReadOnly = false;
				colvarCompanyDepartment.DefaultSetting = @"";
				colvarCompanyDepartment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyDepartment);
				
				TableSchema.TableColumn colvarCompanyTel = new TableSchema.TableColumn(schema);
				colvarCompanyTel.ColumnName = "company_tel";
				colvarCompanyTel.DataType = DbType.AnsiString;
				colvarCompanyTel.MaxLength = 20;
				colvarCompanyTel.AutoIncrement = false;
				colvarCompanyTel.IsNullable = true;
				colvarCompanyTel.IsPrimaryKey = false;
				colvarCompanyTel.IsForeignKey = false;
				colvarCompanyTel.IsReadOnly = false;
				colvarCompanyTel.DefaultSetting = @"";
				colvarCompanyTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyTel);
				
				TableSchema.TableColumn colvarCompanyTelExt = new TableSchema.TableColumn(schema);
				colvarCompanyTelExt.ColumnName = "company_tel_ext";
				colvarCompanyTelExt.DataType = DbType.AnsiString;
				colvarCompanyTelExt.MaxLength = 50;
				colvarCompanyTelExt.AutoIncrement = false;
				colvarCompanyTelExt.IsNullable = true;
				colvarCompanyTelExt.IsPrimaryKey = false;
				colvarCompanyTelExt.IsForeignKey = false;
				colvarCompanyTelExt.IsReadOnly = false;
				colvarCompanyTelExt.DefaultSetting = @"";
				colvarCompanyTelExt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyTelExt);
				
				TableSchema.TableColumn colvarCompanyAddress = new TableSchema.TableColumn(schema);
				colvarCompanyAddress.ColumnName = "company_address";
				colvarCompanyAddress.DataType = DbType.String;
				colvarCompanyAddress.MaxLength = 100;
				colvarCompanyAddress.AutoIncrement = false;
				colvarCompanyAddress.IsNullable = true;
				colvarCompanyAddress.IsPrimaryKey = false;
				colvarCompanyAddress.IsForeignKey = false;
				colvarCompanyAddress.IsReadOnly = false;
				colvarCompanyAddress.DefaultSetting = @"";
				colvarCompanyAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyAddress);
				
				TableSchema.TableColumn colvarDeliveryMethod = new TableSchema.TableColumn(schema);
				colvarDeliveryMethod.ColumnName = "delivery_method";
				colvarDeliveryMethod.DataType = DbType.String;
				colvarDeliveryMethod.MaxLength = 200;
				colvarDeliveryMethod.AutoIncrement = false;
				colvarDeliveryMethod.IsNullable = true;
				colvarDeliveryMethod.IsPrimaryKey = false;
				colvarDeliveryMethod.IsForeignKey = false;
				colvarDeliveryMethod.IsReadOnly = false;
				colvarDeliveryMethod.DefaultSetting = @"";
				colvarDeliveryMethod.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryMethod);
				
				TableSchema.TableColumn colvarPrimaryContactMethod = new TableSchema.TableColumn(schema);
				colvarPrimaryContactMethod.ColumnName = "primary_contact_method";
				colvarPrimaryContactMethod.DataType = DbType.Int32;
				colvarPrimaryContactMethod.MaxLength = 0;
				colvarPrimaryContactMethod.AutoIncrement = false;
				colvarPrimaryContactMethod.IsNullable = false;
				colvarPrimaryContactMethod.IsPrimaryKey = false;
				colvarPrimaryContactMethod.IsForeignKey = false;
				colvarPrimaryContactMethod.IsReadOnly = false;
				colvarPrimaryContactMethod.DefaultSetting = @"";
				colvarPrimaryContactMethod.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrimaryContactMethod);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarComment = new TableSchema.TableColumn(schema);
				colvarComment.ColumnName = "comment";
				colvarComment.DataType = DbType.String;
				colvarComment.MaxLength = 100;
				colvarComment.AutoIncrement = false;
				colvarComment.IsNullable = true;
				colvarComment.IsPrimaryKey = false;
				colvarComment.IsForeignKey = false;
				colvarComment.IsReadOnly = false;
				colvarComment.DefaultSetting = @"";
				colvarComment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarComment);
				
				TableSchema.TableColumn colvarPic = new TableSchema.TableColumn(schema);
				colvarPic.ColumnName = "pic";
				colvarPic.DataType = DbType.AnsiString;
				colvarPic.MaxLength = 500;
				colvarPic.AutoIncrement = false;
				colvarPic.IsNullable = true;
				colvarPic.IsPrimaryKey = false;
				colvarPic.IsForeignKey = false;
				colvarPic.IsReadOnly = false;
				colvarPic.DefaultSetting = @"";
				colvarPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPic);
				
				TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
				colvarUniqueId.ColumnName = "unique_id";
				colvarUniqueId.DataType = DbType.Int32;
				colvarUniqueId.MaxLength = 0;
				colvarUniqueId.AutoIncrement = true;
				colvarUniqueId.IsNullable = false;
				colvarUniqueId.IsPrimaryKey = false;
				colvarUniqueId.IsForeignKey = false;
				colvarUniqueId.IsReadOnly = false;
				colvarUniqueId.DefaultSetting = @"";
				colvarUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniqueId);
				
				TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
				colvarOrderCount.ColumnName = "order_count";
				colvarOrderCount.DataType = DbType.Int32;
				colvarOrderCount.MaxLength = 0;
				colvarOrderCount.AutoIncrement = false;
				colvarOrderCount.IsNullable = true;
				colvarOrderCount.IsPrimaryKey = false;
				colvarOrderCount.IsForeignKey = false;
				colvarOrderCount.IsReadOnly = false;
				
						colvarOrderCount.DefaultSetting = @"((-1))";
				colvarOrderCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderCount);
				
				TableSchema.TableColumn colvarOrderTotal = new TableSchema.TableColumn(schema);
				colvarOrderTotal.ColumnName = "order_total";
				colvarOrderTotal.DataType = DbType.Int32;
				colvarOrderTotal.MaxLength = 0;
				colvarOrderTotal.AutoIncrement = false;
				colvarOrderTotal.IsNullable = true;
				colvarOrderTotal.IsPrimaryKey = false;
				colvarOrderTotal.IsForeignKey = false;
				colvarOrderTotal.IsReadOnly = false;
				
						colvarOrderTotal.DefaultSetting = @"((-1))";
				colvarOrderTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderTotal);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = true;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarEdmType = new TableSchema.TableColumn(schema);
				colvarEdmType.ColumnName = "edm_type";
				colvarEdmType.DataType = DbType.Int32;
				colvarEdmType.MaxLength = 0;
				colvarEdmType.AutoIncrement = false;
				colvarEdmType.IsNullable = false;
				colvarEdmType.IsPrimaryKey = false;
				colvarEdmType.IsForeignKey = false;
				colvarEdmType.IsReadOnly = false;
				
						colvarEdmType.DefaultSetting = @"((0))";
				colvarEdmType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEdmType);
				
				TableSchema.TableColumn colvarVersion = new TableSchema.TableColumn(schema);
				colvarVersion.ColumnName = "version";
				colvarVersion.DataType = DbType.Int32;
				colvarVersion.MaxLength = 0;
				colvarVersion.AutoIncrement = false;
				colvarVersion.IsNullable = false;
				colvarVersion.IsPrimaryKey = false;
				colvarVersion.IsForeignKey = false;
				colvarVersion.IsReadOnly = false;
				
						colvarVersion.DefaultSetting = @"((0))";
				colvarVersion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVersion);
				
				TableSchema.TableColumn colvarIsLockedOut = new TableSchema.TableColumn(schema);
				colvarIsLockedOut.ColumnName = "is_locked_out";
				colvarIsLockedOut.DataType = DbType.Boolean;
				colvarIsLockedOut.MaxLength = 0;
				colvarIsLockedOut.AutoIncrement = false;
				colvarIsLockedOut.IsNullable = false;
				colvarIsLockedOut.IsPrimaryKey = false;
				colvarIsLockedOut.IsForeignKey = false;
				colvarIsLockedOut.IsReadOnly = false;
				
						colvarIsLockedOut.DefaultSetting = @"((0))";
				colvarIsLockedOut.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsLockedOut);
				
				TableSchema.TableColumn colvarPassword = new TableSchema.TableColumn(schema);
				colvarPassword.ColumnName = "password";
				colvarPassword.DataType = DbType.String;
				colvarPassword.MaxLength = 128;
				colvarPassword.AutoIncrement = false;
				colvarPassword.IsNullable = true;
				colvarPassword.IsPrimaryKey = false;
				colvarPassword.IsForeignKey = false;
				colvarPassword.IsReadOnly = false;
				colvarPassword.DefaultSetting = @"";
				colvarPassword.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPassword);
				
				TableSchema.TableColumn colvarPasswordFormat = new TableSchema.TableColumn(schema);
				colvarPasswordFormat.ColumnName = "password_format";
				colvarPasswordFormat.DataType = DbType.Int32;
				colvarPasswordFormat.MaxLength = 0;
				colvarPasswordFormat.AutoIncrement = false;
				colvarPasswordFormat.IsNullable = false;
				colvarPasswordFormat.IsPrimaryKey = false;
				colvarPasswordFormat.IsForeignKey = false;
				colvarPasswordFormat.IsReadOnly = false;
				
						colvarPasswordFormat.DefaultSetting = @"((0))";
				colvarPasswordFormat.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPasswordFormat);
				
				TableSchema.TableColumn colvarPasswordSalt = new TableSchema.TableColumn(schema);
				colvarPasswordSalt.ColumnName = "password_salt";
				colvarPasswordSalt.DataType = DbType.String;
				colvarPasswordSalt.MaxLength = 128;
				colvarPasswordSalt.AutoIncrement = false;
				colvarPasswordSalt.IsNullable = true;
				colvarPasswordSalt.IsPrimaryKey = false;
				colvarPasswordSalt.IsForeignKey = false;
				colvarPasswordSalt.IsReadOnly = false;
				colvarPasswordSalt.DefaultSetting = @"";
				colvarPasswordSalt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPasswordSalt);
				
				TableSchema.TableColumn colvarFailedPasswordAttemptCount = new TableSchema.TableColumn(schema);
				colvarFailedPasswordAttemptCount.ColumnName = "failed_password_attempt_count";
				colvarFailedPasswordAttemptCount.DataType = DbType.Int32;
				colvarFailedPasswordAttemptCount.MaxLength = 0;
				colvarFailedPasswordAttemptCount.AutoIncrement = false;
				colvarFailedPasswordAttemptCount.IsNullable = false;
				colvarFailedPasswordAttemptCount.IsPrimaryKey = false;
				colvarFailedPasswordAttemptCount.IsForeignKey = false;
				colvarFailedPasswordAttemptCount.IsReadOnly = false;
				
						colvarFailedPasswordAttemptCount.DefaultSetting = @"((0))";
				colvarFailedPasswordAttemptCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailedPasswordAttemptCount);
				
				TableSchema.TableColumn colvarFailedPasswordAttemptWindowStart = new TableSchema.TableColumn(schema);
				colvarFailedPasswordAttemptWindowStart.ColumnName = "failed_password_attempt_window_start";
				colvarFailedPasswordAttemptWindowStart.DataType = DbType.DateTime;
				colvarFailedPasswordAttemptWindowStart.MaxLength = 0;
				colvarFailedPasswordAttemptWindowStart.AutoIncrement = false;
				colvarFailedPasswordAttemptWindowStart.IsNullable = false;
				colvarFailedPasswordAttemptWindowStart.IsPrimaryKey = false;
				colvarFailedPasswordAttemptWindowStart.IsForeignKey = false;
				colvarFailedPasswordAttemptWindowStart.IsReadOnly = false;
				
						colvarFailedPasswordAttemptWindowStart.DefaultSetting = @"('1753-01-01')";
				colvarFailedPasswordAttemptWindowStart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailedPasswordAttemptWindowStart);
				
				TableSchema.TableColumn colvarLastLoginDate = new TableSchema.TableColumn(schema);
				colvarLastLoginDate.ColumnName = "last_login_date";
				colvarLastLoginDate.DataType = DbType.DateTime;
				colvarLastLoginDate.MaxLength = 0;
				colvarLastLoginDate.AutoIncrement = false;
				colvarLastLoginDate.IsNullable = false;
				colvarLastLoginDate.IsPrimaryKey = false;
				colvarLastLoginDate.IsForeignKey = false;
				colvarLastLoginDate.IsReadOnly = false;
				
						colvarLastLoginDate.DefaultSetting = @"('1753-01-01')";
				colvarLastLoginDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastLoginDate);
				
				TableSchema.TableColumn colvarLastPasswordChangedDate = new TableSchema.TableColumn(schema);
				colvarLastPasswordChangedDate.ColumnName = "last_password_changed_date";
				colvarLastPasswordChangedDate.DataType = DbType.DateTime;
				colvarLastPasswordChangedDate.MaxLength = 0;
				colvarLastPasswordChangedDate.AutoIncrement = false;
				colvarLastPasswordChangedDate.IsNullable = false;
				colvarLastPasswordChangedDate.IsPrimaryKey = false;
				colvarLastPasswordChangedDate.IsForeignKey = false;
				colvarLastPasswordChangedDate.IsReadOnly = false;
				
						colvarLastPasswordChangedDate.DefaultSetting = @"('1753-01-01')";
				colvarLastPasswordChangedDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastPasswordChangedDate);
				
				TableSchema.TableColumn colvarIsApproved = new TableSchema.TableColumn(schema);
				colvarIsApproved.ColumnName = "is_approved";
				colvarIsApproved.DataType = DbType.Boolean;
				colvarIsApproved.MaxLength = 0;
				colvarIsApproved.AutoIncrement = false;
				colvarIsApproved.IsNullable = false;
				colvarIsApproved.IsPrimaryKey = false;
				colvarIsApproved.IsForeignKey = false;
				colvarIsApproved.IsReadOnly = false;
				
						colvarIsApproved.DefaultSetting = @"((1))";
				colvarIsApproved.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsApproved);
				
				TableSchema.TableColumn colvarCarrierType = new TableSchema.TableColumn(schema);
				colvarCarrierType.ColumnName = "carrier_type";
				colvarCarrierType.DataType = DbType.Int32;
				colvarCarrierType.MaxLength = 0;
				colvarCarrierType.AutoIncrement = false;
				colvarCarrierType.IsNullable = false;
				colvarCarrierType.IsPrimaryKey = false;
				colvarCarrierType.IsForeignKey = false;
				colvarCarrierType.IsReadOnly = false;
				
						colvarCarrierType.DefaultSetting = @"((1))";
				colvarCarrierType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCarrierType);
				
				TableSchema.TableColumn colvarCarrierId = new TableSchema.TableColumn(schema);
				colvarCarrierId.ColumnName = "carrier_id";
				colvarCarrierId.DataType = DbType.String;
				colvarCarrierId.MaxLength = 64;
				colvarCarrierId.AutoIncrement = false;
				colvarCarrierId.IsNullable = true;
				colvarCarrierId.IsPrimaryKey = false;
				colvarCarrierId.IsForeignKey = false;
				colvarCarrierId.IsReadOnly = false;
				colvarCarrierId.DefaultSetting = @"";
				colvarCarrierId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCarrierId);
				
				TableSchema.TableColumn colvarCollectDealExpireMessage = new TableSchema.TableColumn(schema);
				colvarCollectDealExpireMessage.ColumnName = "collect_deal_expire_message";
				colvarCollectDealExpireMessage.DataType = DbType.Boolean;
				colvarCollectDealExpireMessage.MaxLength = 0;
				colvarCollectDealExpireMessage.AutoIncrement = false;
				colvarCollectDealExpireMessage.IsNullable = false;
				colvarCollectDealExpireMessage.IsPrimaryKey = false;
				colvarCollectDealExpireMessage.IsForeignKey = false;
				colvarCollectDealExpireMessage.IsReadOnly = false;
				
						colvarCollectDealExpireMessage.DefaultSetting = @"((1))";
				colvarCollectDealExpireMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCollectDealExpireMessage);
				
				TableSchema.TableColumn colvarTrueEmail = new TableSchema.TableColumn(schema);
				colvarTrueEmail.ColumnName = "true_email";
				colvarTrueEmail.DataType = DbType.String;
				colvarTrueEmail.MaxLength = 256;
				colvarTrueEmail.AutoIncrement = false;
				colvarTrueEmail.IsNullable = true;
				colvarTrueEmail.IsPrimaryKey = false;
				colvarTrueEmail.IsForeignKey = false;
				colvarTrueEmail.IsReadOnly = false;
				colvarTrueEmail.DefaultSetting = @"";
				colvarTrueEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrueEmail);
				
				TableSchema.TableColumn colvarUserEmail = new TableSchema.TableColumn(schema);
				colvarUserEmail.ColumnName = "user_email";
				colvarUserEmail.DataType = DbType.String;
				colvarUserEmail.MaxLength = 256;
				colvarUserEmail.AutoIncrement = false;
				colvarUserEmail.IsNullable = false;
				colvarUserEmail.IsPrimaryKey = false;
				colvarUserEmail.IsForeignKey = false;
				colvarUserEmail.IsReadOnly = false;
				
						colvarUserEmail.DefaultSetting = @"('')";
				colvarUserEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserEmail);
				
				TableSchema.TableColumn colvarIsGuest = new TableSchema.TableColumn(schema);
				colvarIsGuest.ColumnName = "is_guest";
				colvarIsGuest.DataType = DbType.Boolean;
				colvarIsGuest.MaxLength = 0;
				colvarIsGuest.AutoIncrement = false;
				colvarIsGuest.IsNullable = false;
				colvarIsGuest.IsPrimaryKey = false;
				colvarIsGuest.IsForeignKey = false;
				colvarIsGuest.IsReadOnly = false;
				colvarIsGuest.DefaultSetting = @"";
				colvarIsGuest.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsGuest);
				
				TableSchema.TableColumn colvarUpgradedTime = new TableSchema.TableColumn(schema);
				colvarUpgradedTime.ColumnName = "upgraded_time";
				colvarUpgradedTime.DataType = DbType.DateTime;
				colvarUpgradedTime.MaxLength = 0;
				colvarUpgradedTime.AutoIncrement = false;
				colvarUpgradedTime.IsNullable = true;
				colvarUpgradedTime.IsPrimaryKey = false;
				colvarUpgradedTime.IsForeignKey = false;
				colvarUpgradedTime.IsReadOnly = false;
				colvarUpgradedTime.DefaultSetting = @"";
				colvarUpgradedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUpgradedTime);
                
                TableSchema.TableColumn colvarInvoiceBuyerName = new TableSchema.TableColumn(schema);
                colvarInvoiceBuyerName.ColumnName = "invoice_buyer_name";
                colvarInvoiceBuyerName.DataType = DbType.String;
                colvarInvoiceBuyerName.MaxLength = 50;
                colvarInvoiceBuyerName.AutoIncrement = false;
                colvarInvoiceBuyerName.IsNullable = true;
                colvarInvoiceBuyerName.IsPrimaryKey = false;
                colvarInvoiceBuyerName.IsForeignKey = false;
                colvarInvoiceBuyerName.IsReadOnly = false;
                colvarInvoiceBuyerName.DefaultSetting = @"";
                colvarInvoiceBuyerName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceBuyerName);

                TableSchema.TableColumn colvarInvoiceBuyerAddress = new TableSchema.TableColumn(schema);
                colvarInvoiceBuyerAddress.ColumnName = "invoice_buyer_address";
                colvarInvoiceBuyerAddress.DataType = DbType.String;
                colvarInvoiceBuyerAddress.MaxLength = 250;
                colvarInvoiceBuyerAddress.AutoIncrement = false;
                colvarInvoiceBuyerAddress.IsNullable = true;
                colvarInvoiceBuyerAddress.IsPrimaryKey = false;
                colvarInvoiceBuyerAddress.IsForeignKey = false;
                colvarInvoiceBuyerAddress.IsReadOnly = false;
                colvarInvoiceBuyerAddress.DefaultSetting = @"";
                colvarInvoiceBuyerAddress.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceBuyerAddress);
								
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("UserName")]
		[Bindable(true)]
		public string UserName 
		{
			get { return GetColumnValue<string>(Columns.UserName); }
			set { SetColumnValue(Columns.UserName, value); }
		}
		  
		[XmlAttribute("LastName")]
		[Bindable(true)]
		public string LastName 
		{
			get { return GetColumnValue<string>(Columns.LastName); }
			set { SetColumnValue(Columns.LastName, value); }
		}
		  
		[XmlAttribute("FirstName")]
		[Bindable(true)]
		public string FirstName 
		{
			get { return GetColumnValue<string>(Columns.FirstName); }
			set { SetColumnValue(Columns.FirstName, value); }
		}
		  
		[XmlAttribute("BuildingGuid")]
		[Bindable(true)]
		public Guid? BuildingGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.BuildingGuid); }
			set { SetColumnValue(Columns.BuildingGuid, value); }
		}
		  
		[XmlAttribute("Birthday")]
		[Bindable(true)]
		public DateTime? Birthday 
		{
			get { return GetColumnValue<DateTime?>(Columns.Birthday); }
			set { SetColumnValue(Columns.Birthday, value); }
		}
		  
		[XmlAttribute("Gender")]
		[Bindable(true)]
		public int? Gender 
		{
			get { return GetColumnValue<int?>(Columns.Gender); }
			set { SetColumnValue(Columns.Gender, value); }
		}
		  
		[XmlAttribute("Mobile")]
		[Bindable(true)]
		public string Mobile 
		{
			get { return GetColumnValue<string>(Columns.Mobile); }
			set { SetColumnValue(Columns.Mobile, value); }
		}
		  
		[XmlAttribute("CompanyName")]
		[Bindable(true)]
		public string CompanyName 
		{
			get { return GetColumnValue<string>(Columns.CompanyName); }
			set { SetColumnValue(Columns.CompanyName, value); }
		}
		  
		[XmlAttribute("CompanyDepartment")]
		[Bindable(true)]
		public string CompanyDepartment 
		{
			get { return GetColumnValue<string>(Columns.CompanyDepartment); }
			set { SetColumnValue(Columns.CompanyDepartment, value); }
		}
		  
		[XmlAttribute("CompanyTel")]
		[Bindable(true)]
		public string CompanyTel 
		{
			get { return GetColumnValue<string>(Columns.CompanyTel); }
			set { SetColumnValue(Columns.CompanyTel, value); }
		}
		  
		[XmlAttribute("CompanyTelExt")]
		[Bindable(true)]
		public string CompanyTelExt 
		{
			get { return GetColumnValue<string>(Columns.CompanyTelExt); }
			set { SetColumnValue(Columns.CompanyTelExt, value); }
		}
		  
		[XmlAttribute("CompanyAddress")]
		[Bindable(true)]
		public string CompanyAddress 
		{
			get { return GetColumnValue<string>(Columns.CompanyAddress); }
			set { SetColumnValue(Columns.CompanyAddress, value); }
		}
		  
		[XmlAttribute("DeliveryMethod")]
		[Bindable(true)]
		public string DeliveryMethod 
		{
			get { return GetColumnValue<string>(Columns.DeliveryMethod); }
			set { SetColumnValue(Columns.DeliveryMethod, value); }
		}
		  
		[XmlAttribute("PrimaryContactMethod")]
		[Bindable(true)]
		public int PrimaryContactMethod 
		{
			get { return GetColumnValue<int>(Columns.PrimaryContactMethod); }
			set { SetColumnValue(Columns.PrimaryContactMethod, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("Comment")]
		[Bindable(true)]
		public string Comment 
		{
			get { return GetColumnValue<string>(Columns.Comment); }
			set { SetColumnValue(Columns.Comment, value); }
		}
		  
		[XmlAttribute("Pic")]
		[Bindable(true)]
		public string Pic 
		{
			get { return GetColumnValue<string>(Columns.Pic); }
			set { SetColumnValue(Columns.Pic, value); }
		}
		  
		[XmlAttribute("UniqueId")]
		[Bindable(true)]
		public int UniqueId 
		{
			get { return GetColumnValue<int>(Columns.UniqueId); }
			set { SetColumnValue(Columns.UniqueId, value); }
		}
		  
		[XmlAttribute("OrderCount")]
		[Bindable(true)]
		public int? OrderCount 
		{
			get { return GetColumnValue<int?>(Columns.OrderCount); }
			set { SetColumnValue(Columns.OrderCount, value); }
		}
		  
		[XmlAttribute("OrderTotal")]
		[Bindable(true)]
		public int? OrderTotal 
		{
			get { return GetColumnValue<int?>(Columns.OrderTotal); }
			set { SetColumnValue(Columns.OrderTotal, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int? CityId 
		{
			get { return GetColumnValue<int?>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("EdmType")]
		[Bindable(true)]
		public int EdmType 
		{
			get { return GetColumnValue<int>(Columns.EdmType); }
			set { SetColumnValue(Columns.EdmType, value); }
		}
		  
		[XmlAttribute("Version")]
		[Bindable(true)]
		public int Version 
		{
			get { return GetColumnValue<int>(Columns.Version); }
			set { SetColumnValue(Columns.Version, value); }
		}
		  
		[XmlAttribute("IsLockedOut")]
		[Bindable(true)]
		public bool IsLockedOut 
		{
			get { return GetColumnValue<bool>(Columns.IsLockedOut); }
			set { SetColumnValue(Columns.IsLockedOut, value); }
		}
		  
		[XmlAttribute("Password")]
		[Bindable(true)]
		public string Password 
		{
			get { return GetColumnValue<string>(Columns.Password); }
			set { SetColumnValue(Columns.Password, value); }
		}
		  
		[XmlAttribute("PasswordFormat")]
		[Bindable(true)]
		public int PasswordFormat 
		{
			get { return GetColumnValue<int>(Columns.PasswordFormat); }
			set { SetColumnValue(Columns.PasswordFormat, value); }
		}
		  
		[XmlAttribute("PasswordSalt")]
		[Bindable(true)]
		public string PasswordSalt 
		{
			get { return GetColumnValue<string>(Columns.PasswordSalt); }
			set { SetColumnValue(Columns.PasswordSalt, value); }
		}
		  
		[XmlAttribute("FailedPasswordAttemptCount")]
		[Bindable(true)]
		public int FailedPasswordAttemptCount 
		{
			get { return GetColumnValue<int>(Columns.FailedPasswordAttemptCount); }
			set { SetColumnValue(Columns.FailedPasswordAttemptCount, value); }
		}
		  
		[XmlAttribute("FailedPasswordAttemptWindowStart")]
		[Bindable(true)]
		public DateTime FailedPasswordAttemptWindowStart 
		{
			get { return GetColumnValue<DateTime>(Columns.FailedPasswordAttemptWindowStart); }
			set { SetColumnValue(Columns.FailedPasswordAttemptWindowStart, value); }
		}
		  
		[XmlAttribute("LastLoginDate")]
		[Bindable(true)]
		public DateTime LastLoginDate 
		{
			get { return GetColumnValue<DateTime>(Columns.LastLoginDate); }
			set { SetColumnValue(Columns.LastLoginDate, value); }
		}
		  
		[XmlAttribute("LastPasswordChangedDate")]
		[Bindable(true)]
		public DateTime LastPasswordChangedDate 
		{
			get { return GetColumnValue<DateTime>(Columns.LastPasswordChangedDate); }
			set { SetColumnValue(Columns.LastPasswordChangedDate, value); }
		}
		  
		[XmlAttribute("IsApproved")]
		[Bindable(true)]
		public bool IsApproved 
		{
			get { return GetColumnValue<bool>(Columns.IsApproved); }
			set { SetColumnValue(Columns.IsApproved, value); }
		}
		  
		[XmlAttribute("CarrierType")]
		[Bindable(true)]
		public int CarrierType 
		{
			get { return GetColumnValue<int>(Columns.CarrierType); }
			set { SetColumnValue(Columns.CarrierType, value); }
		}
		  
		[XmlAttribute("CarrierId")]
		[Bindable(true)]
		public string CarrierId 
		{
			get { return GetColumnValue<string>(Columns.CarrierId); }
			set { SetColumnValue(Columns.CarrierId, value); }
		}
		  
		[XmlAttribute("CollectDealExpireMessage")]
		[Bindable(true)]
		public bool CollectDealExpireMessage 
		{
			get { return GetColumnValue<bool>(Columns.CollectDealExpireMessage); }
			set { SetColumnValue(Columns.CollectDealExpireMessage, value); }
		}
		  
		[XmlAttribute("TrueEmail")]
		[Bindable(true)]
		public string TrueEmail 
		{
			get { return GetColumnValue<string>(Columns.TrueEmail); }
			set { SetColumnValue(Columns.TrueEmail, value); }
		}
		  
		[XmlAttribute("UserEmail")]
		[Bindable(true)]
		public string UserEmail 
		{
			get { return GetColumnValue<string>(Columns.UserEmail); }
			set { SetColumnValue(Columns.UserEmail, value); }
		}
		  
		[XmlAttribute("IsGuest")]
		[Bindable(true)]
		public bool IsGuest 
		{
			get { return GetColumnValue<bool>(Columns.IsGuest); }
			set { SetColumnValue(Columns.IsGuest, value); }
		}
		  
		[XmlAttribute("UpgradedTime")]
		[Bindable(true)]
		public DateTime? UpgradedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UpgradedTime); }
			set { SetColumnValue(Columns.UpgradedTime, value); }
        }

        [XmlAttribute("InvoiceBuyerName")]
        [Bindable(true)]
        public string InvoiceBuyerName
        {
            get { return GetColumnValue<string>(Columns.InvoiceBuyerName); }
            set { SetColumnValue(Columns.InvoiceBuyerName, value); }
        }

        [XmlAttribute("InvoiceBuyerAddress")]
        [Bindable(true)]
        public string InvoiceBuyerAddress
        {
            get { return GetColumnValue<string>(Columns.InvoiceBuyerAddress); }
            set { SetColumnValue(Columns.InvoiceBuyerAddress, value); }
        }
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (2)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn UserNameColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn LastNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FirstNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BuildingGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn BirthdayColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn GenderColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MobileColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyNameColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyDepartmentColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyTelColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyTelExtColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyAddressColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryMethodColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn PrimaryContactMethodColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn CommentColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn PicColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn UniqueIdColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderCountColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderTotalColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn EdmTypeColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn VersionColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn IsLockedOutColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordFormatColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordSaltColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn FailedPasswordAttemptCountColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn FailedPasswordAttemptWindowStartColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn LastLoginDateColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn LastPasswordChangedDateColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn IsApprovedColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn CarrierTypeColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn CarrierIdColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn CollectDealExpireMessageColumn
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn TrueEmailColumn
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn UserEmailColumn
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        public static TableSchema.TableColumn IsGuestColumn
        {
            get { return Schema.Columns[40]; }
        }
        
        
        
        public static TableSchema.TableColumn UpgradedTimeColumn
        {
            get { return Schema.Columns[41]; }
        }



        public static TableSchema.TableColumn InvoiceBuyerNameColumn
        {
            get { return Schema.Columns[42]; }
        }



        public static TableSchema.TableColumn InvoiceBuyerAddressColumn
        {
            get { return Schema.Columns[43]; }
        }
        


        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string UserName = @"user_name";
			 public static string LastName = @"last_name";
			 public static string FirstName = @"first_name";
			 public static string BuildingGuid = @"building_GUID";
			 public static string Birthday = @"birthday";
			 public static string Gender = @"gender";
			 public static string Mobile = @"mobile";
			 public static string CompanyName = @"company_name";
			 public static string CompanyDepartment = @"company_department";
			 public static string CompanyTel = @"company_tel";
			 public static string CompanyTelExt = @"company_tel_ext";
			 public static string CompanyAddress = @"company_address";
			 public static string DeliveryMethod = @"delivery_method";
			 public static string PrimaryContactMethod = @"primary_contact_method";
			 public static string Status = @"status";
			 public static string CreateTime = @"create_time";
			 public static string ModifyTime = @"modify_time";
			 public static string ModifyId = @"modify_id";
			 public static string Comment = @"comment";
			 public static string Pic = @"pic";
			 public static string UniqueId = @"unique_id";
			 public static string OrderCount = @"order_count";
			 public static string OrderTotal = @"order_total";
			 public static string CityId = @"city_id";
			 public static string EdmType = @"edm_type";
			 public static string Version = @"version";
			 public static string IsLockedOut = @"is_locked_out";
			 public static string Password = @"password";
			 public static string PasswordFormat = @"password_format";
			 public static string PasswordSalt = @"password_salt";
			 public static string FailedPasswordAttemptCount = @"failed_password_attempt_count";
			 public static string FailedPasswordAttemptWindowStart = @"failed_password_attempt_window_start";
			 public static string LastLoginDate = @"last_login_date";
			 public static string LastPasswordChangedDate = @"last_password_changed_date";
			 public static string IsApproved = @"is_approved";
			 public static string CarrierType = @"carrier_type";
			 public static string CarrierId = @"carrier_id";
			 public static string CollectDealExpireMessage = @"collect_deal_expire_message";
			 public static string TrueEmail = @"true_email";
			 public static string UserEmail = @"user_email";
			 public static string IsGuest = @"is_guest";
             public static string UpgradedTime = @"upgraded_time";
             public static string InvoiceBuyerName = @"invoice_buyer_name";
             public static string InvoiceBuyerAddress = @"invoice_buyer_address";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

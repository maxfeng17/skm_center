using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProposalCouponEventContent class.
    /// </summary>
    [Serializable]
    public partial class ProposalCouponEventContentCollection : RepositoryList<ProposalCouponEventContent, ProposalCouponEventContentCollection>
    {
        public ProposalCouponEventContentCollection() {}

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalCouponEventContentCollection</returns>
        public ProposalCouponEventContentCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProposalCouponEventContent o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the proposal_coupon_event_content table.
    /// </summary>
    [Serializable]
    public partial class ProposalCouponEventContent : RepositoryRecord<ProposalCouponEventContent>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProposalCouponEventContent()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProposalCouponEventContent(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("proposal_coupon_event_content", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarProposalId = new TableSchema.TableColumn(schema);
                colvarProposalId.ColumnName = "proposal_id";
                colvarProposalId.DataType = DbType.Int32;
                colvarProposalId.MaxLength = 0;
                colvarProposalId.AutoIncrement = false;
                colvarProposalId.IsNullable = false;
                colvarProposalId.IsPrimaryKey = false;
                colvarProposalId.IsForeignKey = false;
                colvarProposalId.IsReadOnly = false;
                colvarProposalId.DefaultSetting = @"";
                colvarProposalId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProposalId);

                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 400;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                colvarName.DefaultSetting = @"";
                colvarName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarName);

                TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
                colvarImagePath.ColumnName = "image_path";
                colvarImagePath.DataType = DbType.String;
                colvarImagePath.MaxLength = 1000;
                colvarImagePath.AutoIncrement = false;
                colvarImagePath.IsNullable = true;
                colvarImagePath.IsPrimaryKey = false;
                colvarImagePath.IsForeignKey = false;
                colvarImagePath.IsReadOnly = false;
                colvarImagePath.DefaultSetting = @"";
                colvarImagePath.ForeignKeyTableName = "";
                schema.Columns.Add(colvarImagePath);

                TableSchema.TableColumn colvarIntroduction = new TableSchema.TableColumn(schema);
                colvarIntroduction.ColumnName = "introduction";
                colvarIntroduction.DataType = DbType.String;
                colvarIntroduction.MaxLength = 1073741823;
                colvarIntroduction.AutoIncrement = false;
                colvarIntroduction.IsNullable = true;
                colvarIntroduction.IsPrimaryKey = false;
                colvarIntroduction.IsForeignKey = false;
                colvarIntroduction.IsReadOnly = false;
                colvarIntroduction.DefaultSetting = @"";
                colvarIntroduction.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIntroduction);

                TableSchema.TableColumn colvarRestrictions = new TableSchema.TableColumn(schema);
                colvarRestrictions.ColumnName = "restrictions";
                colvarRestrictions.DataType = DbType.String;
                colvarRestrictions.MaxLength = 1073741823;
                colvarRestrictions.AutoIncrement = false;
                colvarRestrictions.IsNullable = true;
                colvarRestrictions.IsPrimaryKey = false;
                colvarRestrictions.IsForeignKey = false;
                colvarRestrictions.IsReadOnly = false;
                colvarRestrictions.DefaultSetting = @"";
                colvarRestrictions.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRestrictions);

                TableSchema.TableColumn colvarReasons = new TableSchema.TableColumn(schema);
                colvarReasons.ColumnName = "reasons";
                colvarReasons.DataType = DbType.String;
                colvarReasons.MaxLength = 1073741823;
                colvarReasons.AutoIncrement = false;
                colvarReasons.IsNullable = true;
                colvarReasons.IsPrimaryKey = false;
                colvarReasons.IsForeignKey = false;
                colvarReasons.IsReadOnly = false;
                colvarReasons.DefaultSetting = @"";
                colvarReasons.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReasons);

                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = 1073741823;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = true;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                colvarDescription.DefaultSetting = @"";
                colvarDescription.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDescription);

                TableSchema.TableColumn colvarReferenceText = new TableSchema.TableColumn(schema);
                colvarReferenceText.ColumnName = "reference_text";
                colvarReferenceText.DataType = DbType.String;
                colvarReferenceText.MaxLength = 1073741823;
                colvarReferenceText.AutoIncrement = false;
                colvarReferenceText.IsNullable = true;
                colvarReferenceText.IsPrimaryKey = false;
                colvarReferenceText.IsForeignKey = false;
                colvarReferenceText.IsReadOnly = false;
                colvarReferenceText.DefaultSetting = @"";
                colvarReferenceText.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReferenceText);

                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 1073741823;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = true;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;
                colvarRemark.DefaultSetting = @"";
                colvarRemark.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRemark);

                TableSchema.TableColumn colvarAvailability = new TableSchema.TableColumn(schema);
                colvarAvailability.ColumnName = "availability";
                colvarAvailability.DataType = DbType.String;
                colvarAvailability.MaxLength = 1073741823;
                colvarAvailability.AutoIncrement = false;
                colvarAvailability.IsNullable = true;
                colvarAvailability.IsPrimaryKey = false;
                colvarAvailability.IsForeignKey = false;
                colvarAvailability.IsReadOnly = false;
                colvarAvailability.DefaultSetting = @"";
                colvarAvailability.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAvailability);

                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 120;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = true;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                colvarTitle.DefaultSetting = @"";
                colvarTitle.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTitle);

                TableSchema.TableColumn colvarSubjectName = new TableSchema.TableColumn(schema);
                colvarSubjectName.ColumnName = "subject_name";
                colvarSubjectName.DataType = DbType.String;
                colvarSubjectName.MaxLength = 150;
                colvarSubjectName.AutoIncrement = false;
                colvarSubjectName.IsNullable = true;
                colvarSubjectName.IsPrimaryKey = false;
                colvarSubjectName.IsForeignKey = false;
                colvarSubjectName.IsReadOnly = false;
                colvarSubjectName.DefaultSetting = @"";
                colvarSubjectName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSubjectName);

                TableSchema.TableColumn colvarSpecialImagePath = new TableSchema.TableColumn(schema);
                colvarSpecialImagePath.ColumnName = "special_image_path";
                colvarSpecialImagePath.DataType = DbType.String;
                colvarSpecialImagePath.MaxLength = 500;
                colvarSpecialImagePath.AutoIncrement = false;
                colvarSpecialImagePath.IsNullable = true;
                colvarSpecialImagePath.IsPrimaryKey = false;
                colvarSpecialImagePath.IsForeignKey = false;
                colvarSpecialImagePath.IsReadOnly = false;
                colvarSpecialImagePath.DefaultSetting = @"";
                colvarSpecialImagePath.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSpecialImagePath);

                TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
                colvarAppTitle.ColumnName = "app_title";
                colvarAppTitle.DataType = DbType.String;
                colvarAppTitle.MaxLength = 40;
                colvarAppTitle.AutoIncrement = false;
                colvarAppTitle.IsNullable = true;
                colvarAppTitle.IsPrimaryKey = false;
                colvarAppTitle.IsForeignKey = false;
                colvarAppTitle.IsReadOnly = false;
                colvarAppTitle.DefaultSetting = @"";
                colvarAppTitle.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAppTitle);

                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                colvarCouponUsage.DefaultSetting = @"";
                colvarCouponUsage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCouponUsage);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarTravelEdmSpecialImagePath = new TableSchema.TableColumn(schema);
                colvarTravelEdmSpecialImagePath.ColumnName = "travel_edm_special_image_path";
                colvarTravelEdmSpecialImagePath.DataType = DbType.String;
                colvarTravelEdmSpecialImagePath.MaxLength = 500;
                colvarTravelEdmSpecialImagePath.AutoIncrement = false;
                colvarTravelEdmSpecialImagePath.IsNullable = true;
                colvarTravelEdmSpecialImagePath.IsPrimaryKey = false;
                colvarTravelEdmSpecialImagePath.IsForeignKey = false;
                colvarTravelEdmSpecialImagePath.IsReadOnly = false;
                colvarTravelEdmSpecialImagePath.DefaultSetting = @"";
                colvarTravelEdmSpecialImagePath.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTravelEdmSpecialImagePath);

                TableSchema.TableColumn colvarAppDealPic = new TableSchema.TableColumn(schema);
                colvarAppDealPic.ColumnName = "app_deal_pic";
                colvarAppDealPic.DataType = DbType.String;
                colvarAppDealPic.MaxLength = 200;
                colvarAppDealPic.AutoIncrement = false;
                colvarAppDealPic.IsNullable = true;
                colvarAppDealPic.IsPrimaryKey = false;
                colvarAppDealPic.IsForeignKey = false;
                colvarAppDealPic.IsReadOnly = false;
                colvarAppDealPic.DefaultSetting = @"";
                colvarAppDealPic.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAppDealPic);

                TableSchema.TableColumn colvarIntro = new TableSchema.TableColumn(schema);
                colvarIntro.ColumnName = "intro";
                colvarIntro.DataType = DbType.String;
                colvarIntro.MaxLength = 100;
                colvarIntro.AutoIncrement = false;
                colvarIntro.IsNullable = true;
                colvarIntro.IsPrimaryKey = false;
                colvarIntro.IsForeignKey = false;
                colvarIntro.IsReadOnly = false;
                colvarIntro.DefaultSetting = @"";
                colvarIntro.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIntro);

                TableSchema.TableColumn colvarPriceDesc = new TableSchema.TableColumn(schema);
                colvarPriceDesc.ColumnName = "price_desc";
                colvarPriceDesc.DataType = DbType.String;
                colvarPriceDesc.MaxLength = 100;
                colvarPriceDesc.AutoIncrement = false;
                colvarPriceDesc.IsNullable = true;
                colvarPriceDesc.IsPrimaryKey = false;
                colvarPriceDesc.IsForeignKey = false;
                colvarPriceDesc.IsReadOnly = false;
                colvarPriceDesc.DefaultSetting = @"";
                colvarPriceDesc.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPriceDesc);

                TableSchema.TableColumn colvarIsHideContent = new TableSchema.TableColumn(schema);
                colvarIsHideContent.ColumnName = "is_hide_content";
                colvarIsHideContent.DataType = DbType.Boolean;
                colvarIsHideContent.MaxLength = 0;
                colvarIsHideContent.AutoIncrement = false;
                colvarIsHideContent.IsNullable = true;
                colvarIsHideContent.IsPrimaryKey = false;
                colvarIsHideContent.IsForeignKey = false;
                colvarIsHideContent.IsReadOnly = false;
                colvarIsHideContent.DefaultSetting = @"";
                colvarIsHideContent.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsHideContent);

                TableSchema.TableColumn colvarProductSpec = new TableSchema.TableColumn(schema);
                colvarProductSpec.ColumnName = "product_spec";
                colvarProductSpec.DataType = DbType.String;
                colvarProductSpec.MaxLength = -1;
                colvarProductSpec.AutoIncrement = false;
                colvarProductSpec.IsNullable = true;
                colvarProductSpec.IsPrimaryKey = false;
                colvarProductSpec.IsForeignKey = false;
                colvarProductSpec.IsReadOnly = false;
                colvarProductSpec.DefaultSetting = @"";
                colvarProductSpec.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProductSpec);

                TableSchema.TableColumn colvarRemoveBgPic = new TableSchema.TableColumn(schema);
                colvarRemoveBgPic.ColumnName = "remove_bg_pic";
                colvarRemoveBgPic.DataType = DbType.String;
                colvarRemoveBgPic.MaxLength = 200;
                colvarRemoveBgPic.AutoIncrement = false;
                colvarRemoveBgPic.IsNullable = true;
                colvarRemoveBgPic.IsPrimaryKey = false;
                colvarRemoveBgPic.IsForeignKey = false;
                colvarRemoveBgPic.IsReadOnly = false;
                colvarRemoveBgPic.DefaultSetting = @"";
                colvarRemoveBgPic.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRemoveBgPic);

                TableSchema.TableColumn colvarOriImagePath = new TableSchema.TableColumn(schema);
                colvarOriImagePath.ColumnName = "ori_image_path";
                colvarOriImagePath.DataType = DbType.String;
                colvarOriImagePath.MaxLength = 1000;
                colvarOriImagePath.AutoIncrement = false;
                colvarOriImagePath.IsNullable = true;
                colvarOriImagePath.IsPrimaryKey = false;
                colvarOriImagePath.IsForeignKey = false;
                colvarOriImagePath.IsReadOnly = false;
                colvarOriImagePath.DefaultSetting = @"";
                colvarOriImagePath.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOriImagePath);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("proposal_coupon_event_content",schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("ProposalId")]
        [Bindable(true)]
        public int ProposalId
        {
            get { return GetColumnValue<int>(Columns.ProposalId); }
            set { SetColumnValue(Columns.ProposalId, value); }
        }

        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name
        {
            get { return GetColumnValue<string>(Columns.Name); }
            set { SetColumnValue(Columns.Name, value); }
        }

        [XmlAttribute("ImagePath")]
        [Bindable(true)]
        public string ImagePath
        {
            get { return GetColumnValue<string>(Columns.ImagePath); }
            set { SetColumnValue(Columns.ImagePath, value); }
        }

        [XmlAttribute("Introduction")]
        [Bindable(true)]
        public string Introduction
        {
            get { return GetColumnValue<string>(Columns.Introduction); }
            set { SetColumnValue(Columns.Introduction, value); }
        }

        [XmlAttribute("Restrictions")]
        [Bindable(true)]
        public string Restrictions
        {
            get { return GetColumnValue<string>(Columns.Restrictions); }
            set { SetColumnValue(Columns.Restrictions, value); }
        }

        [XmlAttribute("Reasons")]
        [Bindable(true)]
        public string Reasons
        {
            get { return GetColumnValue<string>(Columns.Reasons); }
            set { SetColumnValue(Columns.Reasons, value); }
        }

        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description
        {
            get { return GetColumnValue<string>(Columns.Description); }
            set { SetColumnValue(Columns.Description, value); }
        }

        [XmlAttribute("ReferenceText")]
        [Bindable(true)]
        public string ReferenceText
        {
            get { return GetColumnValue<string>(Columns.ReferenceText); }
            set { SetColumnValue(Columns.ReferenceText, value); }
        }

        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark
        {
            get { return GetColumnValue<string>(Columns.Remark); }
            set { SetColumnValue(Columns.Remark, value); }
        }

        [XmlAttribute("Availability")]
        [Bindable(true)]
        public string Availability
        {
            get { return GetColumnValue<string>(Columns.Availability); }
            set { SetColumnValue(Columns.Availability, value); }
        }

        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title
        {
            get { return GetColumnValue<string>(Columns.Title); }
            set { SetColumnValue(Columns.Title, value); }
        }

        [XmlAttribute("SubjectName")]
        [Bindable(true)]
        public string SubjectName
        {
            get { return GetColumnValue<string>(Columns.SubjectName); }
            set { SetColumnValue(Columns.SubjectName, value); }
        }

        [XmlAttribute("SpecialImagePath")]
        [Bindable(true)]
        public string SpecialImagePath
        {
            get { return GetColumnValue<string>(Columns.SpecialImagePath); }
            set { SetColumnValue(Columns.SpecialImagePath, value); }
        }

        [XmlAttribute("AppTitle")]
        [Bindable(true)]
        public string AppTitle
        {
            get { return GetColumnValue<string>(Columns.AppTitle); }
            set { SetColumnValue(Columns.AppTitle, value); }
        }

        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage
        {
            get { return GetColumnValue<string>(Columns.CouponUsage); }
            set { SetColumnValue(Columns.CouponUsage, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("TravelEdmSpecialImagePath")]
        [Bindable(true)]
        public string TravelEdmSpecialImagePath
        {
            get { return GetColumnValue<string>(Columns.TravelEdmSpecialImagePath); }
            set { SetColumnValue(Columns.TravelEdmSpecialImagePath, value); }
        }

        [XmlAttribute("AppDealPic")]
        [Bindable(true)]
        public string AppDealPic
        {
            get { return GetColumnValue<string>(Columns.AppDealPic); }
            set { SetColumnValue(Columns.AppDealPic, value); }
        }

        [XmlAttribute("Intro")]
        [Bindable(true)]
        public string Intro
        {
            get { return GetColumnValue<string>(Columns.Intro); }
            set { SetColumnValue(Columns.Intro, value); }
        }

        [XmlAttribute("PriceDesc")]
        [Bindable(true)]
        public string PriceDesc
        {
            get { return GetColumnValue<string>(Columns.PriceDesc); }
            set { SetColumnValue(Columns.PriceDesc, value); }
        }

        [XmlAttribute("IsHideContent")]
        [Bindable(true)]
        public bool? IsHideContent
        {
            get { return GetColumnValue<bool?>(Columns.IsHideContent); }
            set { SetColumnValue(Columns.IsHideContent, value); }
        }

        [XmlAttribute("ProductSpec")]
        [Bindable(true)]
        public string ProductSpec
        {
            get { return GetColumnValue<string>(Columns.ProductSpec); }
            set { SetColumnValue(Columns.ProductSpec, value); }
        }

        [XmlAttribute("RemoveBgPic")]
        [Bindable(true)]
        public string RemoveBgPic
        {
            get { return GetColumnValue<string>(Columns.RemoveBgPic); }
            set { SetColumnValue(Columns.RemoveBgPic, value); }
        }

        [XmlAttribute("OriImagePath")]
        [Bindable(true)]
        public string OriImagePath
        {
            get { return GetColumnValue<string>(Columns.OriImagePath); }
            set { SetColumnValue(Columns.OriImagePath, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ProposalIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn ImagePathColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn IntroductionColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn RestrictionsColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ReasonsColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ReferenceTextColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn AvailabilityColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn SubjectNameColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn SpecialImagePathColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn AppTitleColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn CouponUsageColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn TravelEdmSpecialImagePathColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn AppDealPicColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn IntroColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn PriceDescColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn IsHideContentColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn ProductSpecColumn
        {
            get { return Schema.Columns[22]; }
        }



        public static TableSchema.TableColumn RemoveBgPicColumn
        {
            get { return Schema.Columns[23]; }
        }



        public static TableSchema.TableColumn OriImagePathColumn
        {
            get { return Schema.Columns[24]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string ProposalId = @"proposal_id";
            public static string Name = @"name";
            public static string ImagePath = @"image_path";
            public static string Introduction = @"introduction";
            public static string Restrictions = @"restrictions";
            public static string Reasons = @"reasons";
            public static string Description = @"description";
            public static string ReferenceText = @"reference_text";
            public static string Remark = @"remark";
            public static string Availability = @"availability";
            public static string Title = @"title";
            public static string SubjectName = @"subject_name";
            public static string SpecialImagePath = @"special_image_path";
            public static string AppTitle = @"app_title";
            public static string CouponUsage = @"coupon_usage";
            public static string ModifyTime = @"modify_time";
            public static string TravelEdmSpecialImagePath = @"travel_edm_special_image_path";
            public static string AppDealPic = @"app_deal_pic";
            public static string Intro = @"intro";
            public static string PriceDesc = @"price_desc";
            public static string IsHideContent = @"is_hide_content";
            public static string ProductSpec = @"product_spec";
            public static string RemoveBgPic = @"remove_bg_pic";
            public static string OriImagePath = @"ori_image_path";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

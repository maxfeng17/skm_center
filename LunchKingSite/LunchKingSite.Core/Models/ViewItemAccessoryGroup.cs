using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewItemAccessoryGroup class.
    /// </summary>
    [Serializable]
    public partial class ViewItemAccessoryGroupCollection : ReadOnlyList<ViewItemAccessoryGroup, ViewItemAccessoryGroupCollection>
    {        
        public ViewItemAccessoryGroupCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_item_accessory_group view.
    /// </summary>
    [Serializable]
    public partial class ViewItemAccessoryGroup : ReadOnlyRecord<ViewItemAccessoryGroup>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_item_accessory_group", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.AnsiString;
                colvarItemId.MaxLength = 20;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = true;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 100;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarItemDescription = new TableSchema.TableColumn(schema);
                colvarItemDescription.ColumnName = "item_description";
                colvarItemDescription.DataType = DbType.String;
                colvarItemDescription.MaxLength = 1073741823;
                colvarItemDescription.AutoIncrement = false;
                colvarItemDescription.IsNullable = true;
                colvarItemDescription.IsPrimaryKey = false;
                colvarItemDescription.IsForeignKey = false;
                colvarItemDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemDescription);
                
                TableSchema.TableColumn colvarItemImgPath = new TableSchema.TableColumn(schema);
                colvarItemImgPath.ColumnName = "item_img_path";
                colvarItemImgPath.DataType = DbType.String;
                colvarItemImgPath.MaxLength = 500;
                colvarItemImgPath.AutoIncrement = false;
                colvarItemImgPath.IsNullable = true;
                colvarItemImgPath.IsPrimaryKey = false;
                colvarItemImgPath.IsForeignKey = false;
                colvarItemImgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemImgPath);
                
                TableSchema.TableColumn colvarItemUrl = new TableSchema.TableColumn(schema);
                colvarItemUrl.ColumnName = "item_url";
                colvarItemUrl.DataType = DbType.String;
                colvarItemUrl.MaxLength = 500;
                colvarItemUrl.AutoIncrement = false;
                colvarItemUrl.IsNullable = true;
                colvarItemUrl.IsPrimaryKey = false;
                colvarItemUrl.IsForeignKey = false;
                colvarItemUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemUrl);
                
                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = true;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequence);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarItemCreateId = new TableSchema.TableColumn(schema);
                colvarItemCreateId.ColumnName = "item_create_id";
                colvarItemCreateId.DataType = DbType.String;
                colvarItemCreateId.MaxLength = 30;
                colvarItemCreateId.AutoIncrement = false;
                colvarItemCreateId.IsNullable = false;
                colvarItemCreateId.IsPrimaryKey = false;
                colvarItemCreateId.IsForeignKey = false;
                colvarItemCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemCreateId);
                
                TableSchema.TableColumn colvarItemCreateTime = new TableSchema.TableColumn(schema);
                colvarItemCreateTime.ColumnName = "item_create_time";
                colvarItemCreateTime.DataType = DbType.DateTime;
                colvarItemCreateTime.MaxLength = 0;
                colvarItemCreateTime.AutoIncrement = false;
                colvarItemCreateTime.IsNullable = false;
                colvarItemCreateTime.IsPrimaryKey = false;
                colvarItemCreateTime.IsForeignKey = false;
                colvarItemCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemCreateTime);
                
                TableSchema.TableColumn colvarItemModifyId = new TableSchema.TableColumn(schema);
                colvarItemModifyId.ColumnName = "item_modify_id";
                colvarItemModifyId.DataType = DbType.String;
                colvarItemModifyId.MaxLength = 30;
                colvarItemModifyId.AutoIncrement = false;
                colvarItemModifyId.IsNullable = true;
                colvarItemModifyId.IsPrimaryKey = false;
                colvarItemModifyId.IsForeignKey = false;
                colvarItemModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemModifyId);
                
                TableSchema.TableColumn colvarItemModifyTime = new TableSchema.TableColumn(schema);
                colvarItemModifyTime.ColumnName = "item_modify_time";
                colvarItemModifyTime.DataType = DbType.DateTime;
                colvarItemModifyTime.MaxLength = 0;
                colvarItemModifyTime.AutoIncrement = false;
                colvarItemModifyTime.IsNullable = true;
                colvarItemModifyTime.IsPrimaryKey = false;
                colvarItemModifyTime.IsForeignKey = false;
                colvarItemModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemModifyTime);
                
                TableSchema.TableColumn colvarAccessoryGroupGuid = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupGuid.ColumnName = "accessory_group_GUID";
                colvarAccessoryGroupGuid.DataType = DbType.Guid;
                colvarAccessoryGroupGuid.MaxLength = 0;
                colvarAccessoryGroupGuid.AutoIncrement = false;
                colvarAccessoryGroupGuid.IsNullable = false;
                colvarAccessoryGroupGuid.IsPrimaryKey = false;
                colvarAccessoryGroupGuid.IsForeignKey = false;
                colvarAccessoryGroupGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupGuid);
                
                TableSchema.TableColumn colvarItemAccessoryGroupListSequence = new TableSchema.TableColumn(schema);
                colvarItemAccessoryGroupListSequence.ColumnName = "item_accessory_group_list_sequence";
                colvarItemAccessoryGroupListSequence.DataType = DbType.Int32;
                colvarItemAccessoryGroupListSequence.MaxLength = 0;
                colvarItemAccessoryGroupListSequence.AutoIncrement = false;
                colvarItemAccessoryGroupListSequence.IsNullable = true;
                colvarItemAccessoryGroupListSequence.IsPrimaryKey = false;
                colvarItemAccessoryGroupListSequence.IsForeignKey = false;
                colvarItemAccessoryGroupListSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemAccessoryGroupListSequence);
                
                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_GUID";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = false;
                colvarItemGuid.IsPrimaryKey = false;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemGuid);
                
                TableSchema.TableColumn colvarItemAccessoryGroupListGuid = new TableSchema.TableColumn(schema);
                colvarItemAccessoryGroupListGuid.ColumnName = "item_accessory_group_list_GUID";
                colvarItemAccessoryGroupListGuid.DataType = DbType.Guid;
                colvarItemAccessoryGroupListGuid.MaxLength = 0;
                colvarItemAccessoryGroupListGuid.AutoIncrement = false;
                colvarItemAccessoryGroupListGuid.IsNullable = false;
                colvarItemAccessoryGroupListGuid.IsPrimaryKey = false;
                colvarItemAccessoryGroupListGuid.IsForeignKey = false;
                colvarItemAccessoryGroupListGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemAccessoryGroupListGuid);
                
                TableSchema.TableColumn colvarAccessoryGroupId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupId.ColumnName = "accessory_group_id";
                colvarAccessoryGroupId.DataType = DbType.AnsiString;
                colvarAccessoryGroupId.MaxLength = 20;
                colvarAccessoryGroupId.AutoIncrement = false;
                colvarAccessoryGroupId.IsNullable = true;
                colvarAccessoryGroupId.IsPrimaryKey = false;
                colvarAccessoryGroupId.IsForeignKey = false;
                colvarAccessoryGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupId);
                
                TableSchema.TableColumn colvarAccessoryGroupName = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupName.ColumnName = "accessory_group_name";
                colvarAccessoryGroupName.DataType = DbType.String;
                colvarAccessoryGroupName.MaxLength = 50;
                colvarAccessoryGroupName.AutoIncrement = false;
                colvarAccessoryGroupName.IsNullable = true;
                colvarAccessoryGroupName.IsPrimaryKey = false;
                colvarAccessoryGroupName.IsForeignKey = false;
                colvarAccessoryGroupName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupName);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = true;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarAccessoryGroupCreateId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupCreateId.ColumnName = "accessory_group_create_id";
                colvarAccessoryGroupCreateId.DataType = DbType.String;
                colvarAccessoryGroupCreateId.MaxLength = 30;
                colvarAccessoryGroupCreateId.AutoIncrement = false;
                colvarAccessoryGroupCreateId.IsNullable = false;
                colvarAccessoryGroupCreateId.IsPrimaryKey = false;
                colvarAccessoryGroupCreateId.IsForeignKey = false;
                colvarAccessoryGroupCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupCreateId);
                
                TableSchema.TableColumn colvarAccessoryGroupCreateTime = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupCreateTime.ColumnName = "accessory_group_create_time";
                colvarAccessoryGroupCreateTime.DataType = DbType.DateTime;
                colvarAccessoryGroupCreateTime.MaxLength = 0;
                colvarAccessoryGroupCreateTime.AutoIncrement = false;
                colvarAccessoryGroupCreateTime.IsNullable = false;
                colvarAccessoryGroupCreateTime.IsPrimaryKey = false;
                colvarAccessoryGroupCreateTime.IsForeignKey = false;
                colvarAccessoryGroupCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupCreateTime);
                
                TableSchema.TableColumn colvarAccessoryGroupModifyId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupModifyId.ColumnName = "accessory_group_modify_id";
                colvarAccessoryGroupModifyId.DataType = DbType.String;
                colvarAccessoryGroupModifyId.MaxLength = 30;
                colvarAccessoryGroupModifyId.AutoIncrement = false;
                colvarAccessoryGroupModifyId.IsNullable = true;
                colvarAccessoryGroupModifyId.IsPrimaryKey = false;
                colvarAccessoryGroupModifyId.IsForeignKey = false;
                colvarAccessoryGroupModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupModifyId);
                
                TableSchema.TableColumn colvarAccessoryGroupModifyTime = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupModifyTime.ColumnName = "accessory_group_modify_time";
                colvarAccessoryGroupModifyTime.DataType = DbType.DateTime;
                colvarAccessoryGroupModifyTime.MaxLength = 0;
                colvarAccessoryGroupModifyTime.AutoIncrement = false;
                colvarAccessoryGroupModifyTime.IsNullable = true;
                colvarAccessoryGroupModifyTime.IsPrimaryKey = false;
                colvarAccessoryGroupModifyTime.IsForeignKey = false;
                colvarAccessoryGroupModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupModifyTime);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberId.ColumnName = "accessory_group_member_id";
                colvarAccessoryGroupMemberId.DataType = DbType.AnsiString;
                colvarAccessoryGroupMemberId.MaxLength = 20;
                colvarAccessoryGroupMemberId.AutoIncrement = false;
                colvarAccessoryGroupMemberId.IsNullable = true;
                colvarAccessoryGroupMemberId.IsPrimaryKey = false;
                colvarAccessoryGroupMemberId.IsForeignKey = false;
                colvarAccessoryGroupMemberId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberId);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberPrice = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberPrice.ColumnName = "accessory_group_member_price";
                colvarAccessoryGroupMemberPrice.DataType = DbType.Currency;
                colvarAccessoryGroupMemberPrice.MaxLength = 0;
                colvarAccessoryGroupMemberPrice.AutoIncrement = false;
                colvarAccessoryGroupMemberPrice.IsNullable = false;
                colvarAccessoryGroupMemberPrice.IsPrimaryKey = false;
                colvarAccessoryGroupMemberPrice.IsForeignKey = false;
                colvarAccessoryGroupMemberPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberPrice);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberNote = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberNote.ColumnName = "accessory_group_member_note";
                colvarAccessoryGroupMemberNote.DataType = DbType.String;
                colvarAccessoryGroupMemberNote.MaxLength = 50;
                colvarAccessoryGroupMemberNote.AutoIncrement = false;
                colvarAccessoryGroupMemberNote.IsNullable = true;
                colvarAccessoryGroupMemberNote.IsPrimaryKey = false;
                colvarAccessoryGroupMemberNote.IsForeignKey = false;
                colvarAccessoryGroupMemberNote.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberNote);
                
                TableSchema.TableColumn colvarAccessoryGuid = new TableSchema.TableColumn(schema);
                colvarAccessoryGuid.ColumnName = "accessory_GUID";
                colvarAccessoryGuid.DataType = DbType.Guid;
                colvarAccessoryGuid.MaxLength = 0;
                colvarAccessoryGuid.AutoIncrement = false;
                colvarAccessoryGuid.IsNullable = false;
                colvarAccessoryGuid.IsPrimaryKey = false;
                colvarAccessoryGuid.IsForeignKey = false;
                colvarAccessoryGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGuid);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberGuid = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberGuid.ColumnName = "accessory_group_member_GUID";
                colvarAccessoryGroupMemberGuid.DataType = DbType.Guid;
                colvarAccessoryGroupMemberGuid.MaxLength = 0;
                colvarAccessoryGroupMemberGuid.AutoIncrement = false;
                colvarAccessoryGroupMemberGuid.IsNullable = false;
                colvarAccessoryGroupMemberGuid.IsPrimaryKey = false;
                colvarAccessoryGroupMemberGuid.IsForeignKey = false;
                colvarAccessoryGroupMemberGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberGuid);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberSequence = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberSequence.ColumnName = "accessory_group_member_sequence";
                colvarAccessoryGroupMemberSequence.DataType = DbType.Int32;
                colvarAccessoryGroupMemberSequence.MaxLength = 0;
                colvarAccessoryGroupMemberSequence.AutoIncrement = false;
                colvarAccessoryGroupMemberSequence.IsNullable = true;
                colvarAccessoryGroupMemberSequence.IsPrimaryKey = false;
                colvarAccessoryGroupMemberSequence.IsForeignKey = false;
                colvarAccessoryGroupMemberSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberSequence);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberCreateId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberCreateId.ColumnName = "accessory_group_member_create_id";
                colvarAccessoryGroupMemberCreateId.DataType = DbType.String;
                colvarAccessoryGroupMemberCreateId.MaxLength = 30;
                colvarAccessoryGroupMemberCreateId.AutoIncrement = false;
                colvarAccessoryGroupMemberCreateId.IsNullable = false;
                colvarAccessoryGroupMemberCreateId.IsPrimaryKey = false;
                colvarAccessoryGroupMemberCreateId.IsForeignKey = false;
                colvarAccessoryGroupMemberCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberCreateId);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberCreateTime = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberCreateTime.ColumnName = "accessory_group_member_create_time";
                colvarAccessoryGroupMemberCreateTime.DataType = DbType.DateTime;
                colvarAccessoryGroupMemberCreateTime.MaxLength = 0;
                colvarAccessoryGroupMemberCreateTime.AutoIncrement = false;
                colvarAccessoryGroupMemberCreateTime.IsNullable = false;
                colvarAccessoryGroupMemberCreateTime.IsPrimaryKey = false;
                colvarAccessoryGroupMemberCreateTime.IsForeignKey = false;
                colvarAccessoryGroupMemberCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberCreateTime);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberModifyId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberModifyId.ColumnName = "accessory_group_member_modify_id";
                colvarAccessoryGroupMemberModifyId.DataType = DbType.String;
                colvarAccessoryGroupMemberModifyId.MaxLength = 30;
                colvarAccessoryGroupMemberModifyId.AutoIncrement = false;
                colvarAccessoryGroupMemberModifyId.IsNullable = true;
                colvarAccessoryGroupMemberModifyId.IsPrimaryKey = false;
                colvarAccessoryGroupMemberModifyId.IsForeignKey = false;
                colvarAccessoryGroupMemberModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberModifyId);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberModifyTime = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberModifyTime.ColumnName = "accessory_group_member_modify_time";
                colvarAccessoryGroupMemberModifyTime.DataType = DbType.DateTime;
                colvarAccessoryGroupMemberModifyTime.MaxLength = 0;
                colvarAccessoryGroupMemberModifyTime.AutoIncrement = false;
                colvarAccessoryGroupMemberModifyTime.IsNullable = true;
                colvarAccessoryGroupMemberModifyTime.IsPrimaryKey = false;
                colvarAccessoryGroupMemberModifyTime.IsForeignKey = false;
                colvarAccessoryGroupMemberModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberModifyTime);
                
                TableSchema.TableColumn colvarAccessoryId = new TableSchema.TableColumn(schema);
                colvarAccessoryId.ColumnName = "accessory_id";
                colvarAccessoryId.DataType = DbType.AnsiString;
                colvarAccessoryId.MaxLength = 20;
                colvarAccessoryId.AutoIncrement = false;
                colvarAccessoryId.IsNullable = false;
                colvarAccessoryId.IsPrimaryKey = false;
                colvarAccessoryId.IsForeignKey = false;
                colvarAccessoryId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryId);
                
                TableSchema.TableColumn colvarAccessoryName = new TableSchema.TableColumn(schema);
                colvarAccessoryName.ColumnName = "accessory_name";
                colvarAccessoryName.DataType = DbType.String;
                colvarAccessoryName.MaxLength = 50;
                colvarAccessoryName.AutoIncrement = false;
                colvarAccessoryName.IsNullable = false;
                colvarAccessoryName.IsPrimaryKey = false;
                colvarAccessoryName.IsForeignKey = false;
                colvarAccessoryName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryName);
                
                TableSchema.TableColumn colvarAccessoryCategoryGuid = new TableSchema.TableColumn(schema);
                colvarAccessoryCategoryGuid.ColumnName = "accessory_category_GUID";
                colvarAccessoryCategoryGuid.DataType = DbType.Guid;
                colvarAccessoryCategoryGuid.MaxLength = 0;
                colvarAccessoryCategoryGuid.AutoIncrement = false;
                colvarAccessoryCategoryGuid.IsNullable = false;
                colvarAccessoryCategoryGuid.IsPrimaryKey = false;
                colvarAccessoryCategoryGuid.IsForeignKey = false;
                colvarAccessoryCategoryGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCategoryGuid);
                
                TableSchema.TableColumn colvarAccessoryCreateId = new TableSchema.TableColumn(schema);
                colvarAccessoryCreateId.ColumnName = "accessory_create_id";
                colvarAccessoryCreateId.DataType = DbType.String;
                colvarAccessoryCreateId.MaxLength = 30;
                colvarAccessoryCreateId.AutoIncrement = false;
                colvarAccessoryCreateId.IsNullable = false;
                colvarAccessoryCreateId.IsPrimaryKey = false;
                colvarAccessoryCreateId.IsForeignKey = false;
                colvarAccessoryCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCreateId);
                
                TableSchema.TableColumn colvarAccessoryCreateTime = new TableSchema.TableColumn(schema);
                colvarAccessoryCreateTime.ColumnName = "accessory_create_time";
                colvarAccessoryCreateTime.DataType = DbType.DateTime;
                colvarAccessoryCreateTime.MaxLength = 0;
                colvarAccessoryCreateTime.AutoIncrement = false;
                colvarAccessoryCreateTime.IsNullable = false;
                colvarAccessoryCreateTime.IsPrimaryKey = false;
                colvarAccessoryCreateTime.IsForeignKey = false;
                colvarAccessoryCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCreateTime);
                
                TableSchema.TableColumn colvarAccessoryModifyId = new TableSchema.TableColumn(schema);
                colvarAccessoryModifyId.ColumnName = "accessory_modify_id";
                colvarAccessoryModifyId.DataType = DbType.String;
                colvarAccessoryModifyId.MaxLength = 30;
                colvarAccessoryModifyId.AutoIncrement = false;
                colvarAccessoryModifyId.IsNullable = true;
                colvarAccessoryModifyId.IsPrimaryKey = false;
                colvarAccessoryModifyId.IsForeignKey = false;
                colvarAccessoryModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryModifyId);
                
                TableSchema.TableColumn colvarAccessoryModifyTime = new TableSchema.TableColumn(schema);
                colvarAccessoryModifyTime.ColumnName = "accessory_modify_time";
                colvarAccessoryModifyTime.DataType = DbType.DateTime;
                colvarAccessoryModifyTime.MaxLength = 0;
                colvarAccessoryModifyTime.AutoIncrement = false;
                colvarAccessoryModifyTime.IsNullable = true;
                colvarAccessoryModifyTime.IsPrimaryKey = false;
                colvarAccessoryModifyTime.IsForeignKey = false;
                colvarAccessoryModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryModifyTime);
                
                TableSchema.TableColumn colvarMenuGuid = new TableSchema.TableColumn(schema);
                colvarMenuGuid.ColumnName = "menu_GUID";
                colvarMenuGuid.DataType = DbType.Guid;
                colvarMenuGuid.MaxLength = 0;
                colvarMenuGuid.AutoIncrement = false;
                colvarMenuGuid.IsNullable = false;
                colvarMenuGuid.IsPrimaryKey = false;
                colvarMenuGuid.IsForeignKey = false;
                colvarMenuGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMenuGuid);
                
                TableSchema.TableColumn colvarItemDefaultDailyAmount = new TableSchema.TableColumn(schema);
                colvarItemDefaultDailyAmount.ColumnName = "item_default_daily_amount";
                colvarItemDefaultDailyAmount.DataType = DbType.Int32;
                colvarItemDefaultDailyAmount.MaxLength = 0;
                colvarItemDefaultDailyAmount.AutoIncrement = false;
                colvarItemDefaultDailyAmount.IsNullable = true;
                colvarItemDefaultDailyAmount.IsPrimaryKey = false;
                colvarItemDefaultDailyAmount.IsForeignKey = false;
                colvarItemDefaultDailyAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemDefaultDailyAmount);
                
                TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
                colvarItemOrigPrice.ColumnName = "item_orig_price";
                colvarItemOrigPrice.DataType = DbType.Currency;
                colvarItemOrigPrice.MaxLength = 0;
                colvarItemOrigPrice.AutoIncrement = false;
                colvarItemOrigPrice.IsNullable = false;
                colvarItemOrigPrice.IsPrimaryKey = false;
                colvarItemOrigPrice.IsForeignKey = false;
                colvarItemOrigPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemOrigPrice);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.String;
                colvarProductId.MaxLength = 50;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = true;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarAccessoryGroupType = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupType.ColumnName = "accessory_group_type";
                colvarAccessoryGroupType.DataType = DbType.Int32;
                colvarAccessoryGroupType.MaxLength = 0;
                colvarAccessoryGroupType.AutoIncrement = false;
                colvarAccessoryGroupType.IsNullable = false;
                colvarAccessoryGroupType.IsPrimaryKey = false;
                colvarAccessoryGroupType.IsForeignKey = false;
                colvarAccessoryGroupType.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupType);
                
                TableSchema.TableColumn colvarAccessoryGroupStatus = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupStatus.ColumnName = "accessory_group_status";
                colvarAccessoryGroupStatus.DataType = DbType.Int32;
                colvarAccessoryGroupStatus.MaxLength = 0;
                colvarAccessoryGroupStatus.AutoIncrement = false;
                colvarAccessoryGroupStatus.IsNullable = true;
                colvarAccessoryGroupStatus.IsPrimaryKey = false;
                colvarAccessoryGroupStatus.IsForeignKey = false;
                colvarAccessoryGroupStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupStatus);
                
                TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
                colvarQuantity.ColumnName = "quantity";
                colvarQuantity.DataType = DbType.Int32;
                colvarQuantity.MaxLength = 0;
                colvarQuantity.AutoIncrement = false;
                colvarQuantity.IsNullable = true;
                colvarQuantity.IsPrimaryKey = false;
                colvarQuantity.IsForeignKey = false;
                colvarQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarQuantity);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_item_accessory_group",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewItemAccessoryGroup()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewItemAccessoryGroup(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewItemAccessoryGroup(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewItemAccessoryGroup(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public string ItemId 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("ItemDescription")]
        [Bindable(true)]
        public string ItemDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_description");
		    }
            set 
		    {
			    SetColumnValue("item_description", value);
            }
        }
	      
        [XmlAttribute("ItemImgPath")]
        [Bindable(true)]
        public string ItemImgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_img_path");
		    }
            set 
		    {
			    SetColumnValue("item_img_path", value);
            }
        }
	      
        [XmlAttribute("ItemUrl")]
        [Bindable(true)]
        public string ItemUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_url");
		    }
            set 
		    {
			    SetColumnValue("item_url", value);
            }
        }
	      
        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public int? Sequence 
	    {
		    get
		    {
			    return GetColumnValue<int?>("sequence");
		    }
            set 
		    {
			    SetColumnValue("sequence", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("ItemCreateId")]
        [Bindable(true)]
        public string ItemCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_create_id");
		    }
            set 
		    {
			    SetColumnValue("item_create_id", value);
            }
        }
	      
        [XmlAttribute("ItemCreateTime")]
        [Bindable(true)]
        public DateTime ItemCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("item_create_time");
		    }
            set 
		    {
			    SetColumnValue("item_create_time", value);
            }
        }
	      
        [XmlAttribute("ItemModifyId")]
        [Bindable(true)]
        public string ItemModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_modify_id");
		    }
            set 
		    {
			    SetColumnValue("item_modify_id", value);
            }
        }
	      
        [XmlAttribute("ItemModifyTime")]
        [Bindable(true)]
        public DateTime? ItemModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("item_modify_time");
		    }
            set 
		    {
			    SetColumnValue("item_modify_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupGuid")]
        [Bindable(true)]
        public Guid AccessoryGroupGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("accessory_group_GUID");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_GUID", value);
            }
        }
	      
        [XmlAttribute("ItemAccessoryGroupListSequence")]
        [Bindable(true)]
        public int? ItemAccessoryGroupListSequence 
	    {
		    get
		    {
			    return GetColumnValue<int?>("item_accessory_group_list_sequence");
		    }
            set 
		    {
			    SetColumnValue("item_accessory_group_list_sequence", value);
            }
        }
	      
        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid ItemGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("item_GUID");
		    }
            set 
		    {
			    SetColumnValue("item_GUID", value);
            }
        }
	      
        [XmlAttribute("ItemAccessoryGroupListGuid")]
        [Bindable(true)]
        public Guid ItemAccessoryGroupListGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("item_accessory_group_list_GUID");
		    }
            set 
		    {
			    SetColumnValue("item_accessory_group_list_GUID", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupId")]
        [Bindable(true)]
        public string AccessoryGroupId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupName")]
        [Bindable(true)]
        public string AccessoryGroupName 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_name");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_name", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid? SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupCreateId")]
        [Bindable(true)]
        public string AccessoryGroupCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_create_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_create_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupCreateTime")]
        [Bindable(true)]
        public DateTime AccessoryGroupCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("accessory_group_create_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_create_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupModifyId")]
        [Bindable(true)]
        public string AccessoryGroupModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_modify_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_modify_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupModifyTime")]
        [Bindable(true)]
        public DateTime? AccessoryGroupModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("accessory_group_modify_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_modify_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberId")]
        [Bindable(true)]
        public string AccessoryGroupMemberId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_member_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberPrice")]
        [Bindable(true)]
        public decimal AccessoryGroupMemberPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("accessory_group_member_price");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_price", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberNote")]
        [Bindable(true)]
        public string AccessoryGroupMemberNote 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_member_note");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_note", value);
            }
        }
	      
        [XmlAttribute("AccessoryGuid")]
        [Bindable(true)]
        public Guid AccessoryGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("accessory_GUID");
		    }
            set 
		    {
			    SetColumnValue("accessory_GUID", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberGuid")]
        [Bindable(true)]
        public Guid AccessoryGroupMemberGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("accessory_group_member_GUID");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_GUID", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberSequence")]
        [Bindable(true)]
        public int? AccessoryGroupMemberSequence 
	    {
		    get
		    {
			    return GetColumnValue<int?>("accessory_group_member_sequence");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_sequence", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberCreateId")]
        [Bindable(true)]
        public string AccessoryGroupMemberCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_member_create_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_create_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberCreateTime")]
        [Bindable(true)]
        public DateTime AccessoryGroupMemberCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("accessory_group_member_create_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_create_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberModifyId")]
        [Bindable(true)]
        public string AccessoryGroupMemberModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_member_modify_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_modify_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberModifyTime")]
        [Bindable(true)]
        public DateTime? AccessoryGroupMemberModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("accessory_group_member_modify_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_modify_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryId")]
        [Bindable(true)]
        public string AccessoryId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryName")]
        [Bindable(true)]
        public string AccessoryName 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_name");
		    }
            set 
		    {
			    SetColumnValue("accessory_name", value);
            }
        }
	      
        [XmlAttribute("AccessoryCategoryGuid")]
        [Bindable(true)]
        public Guid AccessoryCategoryGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("accessory_category_GUID");
		    }
            set 
		    {
			    SetColumnValue("accessory_category_GUID", value);
            }
        }
	      
        [XmlAttribute("AccessoryCreateId")]
        [Bindable(true)]
        public string AccessoryCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_create_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_create_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryCreateTime")]
        [Bindable(true)]
        public DateTime AccessoryCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("accessory_create_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_create_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryModifyId")]
        [Bindable(true)]
        public string AccessoryModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_modify_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_modify_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryModifyTime")]
        [Bindable(true)]
        public DateTime? AccessoryModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("accessory_modify_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_modify_time", value);
            }
        }
	      
        [XmlAttribute("MenuGuid")]
        [Bindable(true)]
        public Guid MenuGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("menu_GUID");
		    }
            set 
		    {
			    SetColumnValue("menu_GUID", value);
            }
        }
	      
        [XmlAttribute("ItemDefaultDailyAmount")]
        [Bindable(true)]
        public int? ItemDefaultDailyAmount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("item_default_daily_amount");
		    }
            set 
		    {
			    SetColumnValue("item_default_daily_amount", value);
            }
        }
	      
        [XmlAttribute("ItemOrigPrice")]
        [Bindable(true)]
        public decimal ItemOrigPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_orig_price");
		    }
            set 
		    {
			    SetColumnValue("item_orig_price", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public string ProductId 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupType")]
        [Bindable(true)]
        public int AccessoryGroupType 
	    {
		    get
		    {
			    return GetColumnValue<int>("accessory_group_type");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_type", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupStatus")]
        [Bindable(true)]
        public int? AccessoryGroupStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("accessory_group_status");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_status", value);
            }
        }
	      
        [XmlAttribute("Quantity")]
        [Bindable(true)]
        public int? Quantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("quantity");
		    }
            set 
		    {
			    SetColumnValue("quantity", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ItemId = @"item_id";
            
            public static string ItemName = @"item_name";
            
            public static string ItemPrice = @"item_price";
            
            public static string ItemDescription = @"item_description";
            
            public static string ItemImgPath = @"item_img_path";
            
            public static string ItemUrl = @"item_url";
            
            public static string Sequence = @"sequence";
            
            public static string Status = @"status";
            
            public static string ItemCreateId = @"item_create_id";
            
            public static string ItemCreateTime = @"item_create_time";
            
            public static string ItemModifyId = @"item_modify_id";
            
            public static string ItemModifyTime = @"item_modify_time";
            
            public static string AccessoryGroupGuid = @"accessory_group_GUID";
            
            public static string ItemAccessoryGroupListSequence = @"item_accessory_group_list_sequence";
            
            public static string ItemGuid = @"item_GUID";
            
            public static string ItemAccessoryGroupListGuid = @"item_accessory_group_list_GUID";
            
            public static string AccessoryGroupId = @"accessory_group_id";
            
            public static string AccessoryGroupName = @"accessory_group_name";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string AccessoryGroupCreateId = @"accessory_group_create_id";
            
            public static string AccessoryGroupCreateTime = @"accessory_group_create_time";
            
            public static string AccessoryGroupModifyId = @"accessory_group_modify_id";
            
            public static string AccessoryGroupModifyTime = @"accessory_group_modify_time";
            
            public static string AccessoryGroupMemberId = @"accessory_group_member_id";
            
            public static string AccessoryGroupMemberPrice = @"accessory_group_member_price";
            
            public static string AccessoryGroupMemberNote = @"accessory_group_member_note";
            
            public static string AccessoryGuid = @"accessory_GUID";
            
            public static string AccessoryGroupMemberGuid = @"accessory_group_member_GUID";
            
            public static string AccessoryGroupMemberSequence = @"accessory_group_member_sequence";
            
            public static string AccessoryGroupMemberCreateId = @"accessory_group_member_create_id";
            
            public static string AccessoryGroupMemberCreateTime = @"accessory_group_member_create_time";
            
            public static string AccessoryGroupMemberModifyId = @"accessory_group_member_modify_id";
            
            public static string AccessoryGroupMemberModifyTime = @"accessory_group_member_modify_time";
            
            public static string AccessoryId = @"accessory_id";
            
            public static string AccessoryName = @"accessory_name";
            
            public static string AccessoryCategoryGuid = @"accessory_category_GUID";
            
            public static string AccessoryCreateId = @"accessory_create_id";
            
            public static string AccessoryCreateTime = @"accessory_create_time";
            
            public static string AccessoryModifyId = @"accessory_modify_id";
            
            public static string AccessoryModifyTime = @"accessory_modify_time";
            
            public static string MenuGuid = @"menu_GUID";
            
            public static string ItemDefaultDailyAmount = @"item_default_daily_amount";
            
            public static string ItemOrigPrice = @"item_orig_price";
            
            public static string ProductId = @"product_id";
            
            public static string AccessoryGroupType = @"accessory_group_type";
            
            public static string AccessoryGroupStatus = @"accessory_group_status";
            
            public static string Quantity = @"quantity";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

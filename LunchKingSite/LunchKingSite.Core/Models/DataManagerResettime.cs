using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DataManagerResettime class.
	/// </summary>
    [Serializable]
	public partial class DataManagerResettimeCollection : RepositoryList<DataManagerResettime, DataManagerResettimeCollection>
	{	   
		public DataManagerResettimeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DataManagerResettimeCollection</returns>
		public DataManagerResettimeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DataManagerResettime o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the data_manager_resettime table.
	/// </summary>
	[Serializable]
	public partial class DataManagerResettime : RepositoryRecord<DataManagerResettime>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DataManagerResettime()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DataManagerResettime(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("data_manager_resettime", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarManagerName = new TableSchema.TableColumn(schema);
				colvarManagerName.ColumnName = "ManagerName";
				colvarManagerName.DataType = DbType.AnsiString;
				colvarManagerName.MaxLength = 255;
				colvarManagerName.AutoIncrement = false;
				colvarManagerName.IsNullable = false;
				colvarManagerName.IsPrimaryKey = false;
				colvarManagerName.IsForeignKey = false;
				colvarManagerName.IsReadOnly = false;
				colvarManagerName.DefaultSetting = @"";
				colvarManagerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarManagerName);
				
				TableSchema.TableColumn colvarLastRunningTime = new TableSchema.TableColumn(schema);
				colvarLastRunningTime.ColumnName = "LastRunningTime";
				colvarLastRunningTime.DataType = DbType.DateTime;
				colvarLastRunningTime.MaxLength = 0;
				colvarLastRunningTime.AutoIncrement = false;
				colvarLastRunningTime.IsNullable = true;
				colvarLastRunningTime.IsPrimaryKey = false;
				colvarLastRunningTime.IsForeignKey = false;
				colvarLastRunningTime.IsReadOnly = false;
				colvarLastRunningTime.DefaultSetting = @"";
				colvarLastRunningTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastRunningTime);
				
				TableSchema.TableColumn colvarNextRunningTime = new TableSchema.TableColumn(schema);
				colvarNextRunningTime.ColumnName = "NextRunningTime";
				colvarNextRunningTime.DataType = DbType.DateTime;
				colvarNextRunningTime.MaxLength = 0;
				colvarNextRunningTime.AutoIncrement = false;
				colvarNextRunningTime.IsNullable = false;
				colvarNextRunningTime.IsPrimaryKey = false;
				colvarNextRunningTime.IsForeignKey = false;
				colvarNextRunningTime.IsReadOnly = false;
				colvarNextRunningTime.DefaultSetting = @"";
				colvarNextRunningTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNextRunningTime);
				
				TableSchema.TableColumn colvarIntervalMinute = new TableSchema.TableColumn(schema);
				colvarIntervalMinute.ColumnName = "IntervalMinute";
				colvarIntervalMinute.DataType = DbType.Int32;
				colvarIntervalMinute.MaxLength = 0;
				colvarIntervalMinute.AutoIncrement = false;
				colvarIntervalMinute.IsNullable = false;
				colvarIntervalMinute.IsPrimaryKey = false;
				colvarIntervalMinute.IsForeignKey = false;
				colvarIntervalMinute.IsReadOnly = false;
				colvarIntervalMinute.DefaultSetting = @"";
				colvarIntervalMinute.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntervalMinute);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "Enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = false;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				colvarEnabled.DefaultSetting = @"";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				TableSchema.TableColumn colvarJobType = new TableSchema.TableColumn(schema);
				colvarJobType.ColumnName = "job_type";
				colvarJobType.DataType = DbType.Int32;
				colvarJobType.MaxLength = 0;
				colvarJobType.AutoIncrement = false;
				colvarJobType.IsNullable = false;
				colvarJobType.IsPrimaryKey = false;
				colvarJobType.IsForeignKey = false;
				colvarJobType.IsReadOnly = false;
				colvarJobType.DefaultSetting = @"";
				colvarJobType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarJobType);
				
				TableSchema.TableColumn colvarServerName = new TableSchema.TableColumn(schema);
				colvarServerName.ColumnName = "ServerName";
				colvarServerName.DataType = DbType.AnsiString;
				colvarServerName.MaxLength = 50;
				colvarServerName.AutoIncrement = false;
				colvarServerName.IsNullable = true;
				colvarServerName.IsPrimaryKey = false;
				colvarServerName.IsForeignKey = false;
				colvarServerName.IsReadOnly = false;
				colvarServerName.DefaultSetting = @"";
				colvarServerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServerName);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("data_manager_resettime",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ManagerName")]
		[Bindable(true)]
		public string ManagerName 
		{
			get { return GetColumnValue<string>(Columns.ManagerName); }
			set { SetColumnValue(Columns.ManagerName, value); }
		}
		  
		[XmlAttribute("LastRunningTime")]
		[Bindable(true)]
		public DateTime? LastRunningTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastRunningTime); }
			set { SetColumnValue(Columns.LastRunningTime, value); }
		}
		  
		[XmlAttribute("NextRunningTime")]
		[Bindable(true)]
		public DateTime NextRunningTime 
		{
			get { return GetColumnValue<DateTime>(Columns.NextRunningTime); }
			set { SetColumnValue(Columns.NextRunningTime, value); }
		}
		  
		[XmlAttribute("IntervalMinute")]
		[Bindable(true)]
		public int IntervalMinute 
		{
			get { return GetColumnValue<int>(Columns.IntervalMinute); }
			set { SetColumnValue(Columns.IntervalMinute, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool Enabled 
		{
			get { return GetColumnValue<bool>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		  
		[XmlAttribute("JobType")]
		[Bindable(true)]
		public int JobType 
		{
			get { return GetColumnValue<int>(Columns.JobType); }
			set { SetColumnValue(Columns.JobType, value); }
		}
		  
		[XmlAttribute("ServerName")]
		[Bindable(true)]
		public string ServerName 
		{
			get { return GetColumnValue<string>(Columns.ServerName); }
			set { SetColumnValue(Columns.ServerName, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ManagerNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn LastRunningTimeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn NextRunningTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IntervalMinuteColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn JobTypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ServerNameColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ManagerName = @"ManagerName";
			 public static string LastRunningTime = @"LastRunningTime";
			 public static string NextRunningTime = @"NextRunningTime";
			 public static string IntervalMinute = @"IntervalMinute";
			 public static string Enabled = @"Enabled";
			 public static string JobType = @"job_type";
			 public static string ServerName = @"ServerName";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

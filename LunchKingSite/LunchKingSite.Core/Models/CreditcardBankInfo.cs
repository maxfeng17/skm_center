using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the CreditcardBankInfo class.
    /// </summary>
    [Serializable]
    public partial class CreditcardBankInfoCollection : RepositoryList<CreditcardBankInfo, CreditcardBankInfoCollection>
    {
        public CreditcardBankInfoCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CreditcardBankInfoCollection</returns>
        public CreditcardBankInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CreditcardBankInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the creditcard_bank_info table.
    /// </summary>
    [Serializable]
    public partial class CreditcardBankInfo : RepositoryRecord<CreditcardBankInfo>, IRecordBase
    {
        #region .ctors and Default Settings

        public CreditcardBankInfo()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public CreditcardBankInfo(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("creditcard_bank_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarBin = new TableSchema.TableColumn(schema);
                colvarBin.ColumnName = "bin";
                colvarBin.DataType = DbType.String;
                colvarBin.MaxLength = 20;
                colvarBin.AutoIncrement = false;
                colvarBin.IsNullable = false;
                colvarBin.IsPrimaryKey = false;
                colvarBin.IsForeignKey = false;
                colvarBin.IsReadOnly = false;
                colvarBin.DefaultSetting = @"";
                colvarBin.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBin);

                TableSchema.TableColumn colvarBankName = new TableSchema.TableColumn(schema);
                colvarBankName.ColumnName = "bank_name";
                colvarBankName.DataType = DbType.String;
                colvarBankName.MaxLength = 50;
                colvarBankName.AutoIncrement = false;
                colvarBankName.IsNullable = true;
                colvarBankName.IsPrimaryKey = false;
                colvarBankName.IsForeignKey = false;
                colvarBankName.IsReadOnly = false;
                colvarBankName.DefaultSetting = @"";
                colvarBankName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBankName);

                TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
                colvarFlag.ColumnName = "flag";
                colvarFlag.DataType = DbType.Int32;
                colvarFlag.MaxLength = 0;
                colvarFlag.AutoIncrement = false;
                colvarFlag.IsNullable = false;
                colvarFlag.IsPrimaryKey = false;
                colvarFlag.IsForeignKey = false;
                colvarFlag.IsReadOnly = false;

                colvarFlag.DefaultSetting = @"((0))";
                colvarFlag.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFlag);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 256;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarBankId = new TableSchema.TableColumn(schema);
                colvarBankId.ColumnName = "bank_id";
                colvarBankId.DataType = DbType.Int32;
                colvarBankId.MaxLength = 0;
                colvarBankId.AutoIncrement = false;
                colvarBankId.IsNullable = false;
                colvarBankId.IsPrimaryKey = false;
                colvarBankId.IsForeignKey = false;
                colvarBankId.IsReadOnly = false;
                colvarBankId.DefaultSetting = @"";
                colvarBankId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBankId);

                TableSchema.TableColumn colvarCardType = new TableSchema.TableColumn(schema);
                colvarCardType.ColumnName = "card_type";
                colvarCardType.DataType = DbType.Int32;
                colvarCardType.MaxLength = 0;
                colvarCardType.AutoIncrement = false;
                colvarCardType.IsNullable = true;
                colvarCardType.IsPrimaryKey = false;
                colvarCardType.IsForeignKey = false;
                colvarCardType.IsReadOnly = false;
                colvarCardType.DefaultSetting = @"";
                colvarCardType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardType);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("creditcard_bank_info", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Bin")]
        [Bindable(true)]
        public string Bin
        {
            get { return GetColumnValue<string>(Columns.Bin); }
            set { SetColumnValue(Columns.Bin, value); }
        }

        [XmlAttribute("BankName")]
        [Bindable(true)]
        public string BankName
        {
            get { return GetColumnValue<string>(Columns.BankName); }
            set { SetColumnValue(Columns.BankName, value); }
        }

        [XmlAttribute("Flag")]
        [Bindable(true)]
        public int Flag
        {
            get { return GetColumnValue<int>(Columns.Flag); }
            set { SetColumnValue(Columns.Flag, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("BankId")]
        [Bindable(true)]
        public int BankId
        {
            get { return GetColumnValue<int>(Columns.BankId); }
            set { SetColumnValue(Columns.BankId, value); }
        }

        [XmlAttribute("CardType")]
        [Bindable(true)]
        public int? CardType
        {
            get { return GetColumnValue<int?>(Columns.CardType); }
            set { SetColumnValue(Columns.CardType, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn BinColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn BankNameColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn FlagColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn BankIdColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CardTypeColumn
        {
            get { return Schema.Columns[9]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Bin = @"bin";
            public static string BankName = @"bank_name";
            public static string Flag = @"flag";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string BankId = @"bank_id";
            public static string CardType = @"card_type";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the TrustVerificationReport class.
    /// </summary>
    [Serializable]
    public partial class TrustVerificationReportCollection : RepositoryList<TrustVerificationReport, TrustVerificationReportCollection>
    {
        public TrustVerificationReportCollection() {}

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TrustVerificationReportCollection</returns>
        public TrustVerificationReportCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TrustVerificationReport o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the trust_verification_report table.
    /// </summary>
    [Serializable]
    public partial class TrustVerificationReport : RepositoryRecord<TrustVerificationReport>, IRecordBase
    {
        #region .ctors and Default Settings

        public TrustVerificationReport()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public TrustVerificationReport(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("trust_verification_report", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarTrustProvider = new TableSchema.TableColumn(schema);
                colvarTrustProvider.ColumnName = "trust_provider";
                colvarTrustProvider.DataType = DbType.Int32;
                colvarTrustProvider.MaxLength = 0;
                colvarTrustProvider.AutoIncrement = false;
                colvarTrustProvider.IsNullable = false;
                colvarTrustProvider.IsPrimaryKey = false;
                colvarTrustProvider.IsForeignKey = false;
                colvarTrustProvider.IsReadOnly = false;
                colvarTrustProvider.DefaultSetting = @"";
                colvarTrustProvider.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustProvider);

                TableSchema.TableColumn colvarReportDate = new TableSchema.TableColumn(schema);
                colvarReportDate.ColumnName = "report_date";
                colvarReportDate.DataType = DbType.DateTime;
                colvarReportDate.MaxLength = 0;
                colvarReportDate.AutoIncrement = false;
                colvarReportDate.IsNullable = false;
                colvarReportDate.IsPrimaryKey = false;
                colvarReportDate.IsForeignKey = false;
                colvarReportDate.IsReadOnly = false;
                colvarReportDate.DefaultSetting = @"";
                colvarReportDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReportDate);

                TableSchema.TableColumn colvarReportType = new TableSchema.TableColumn(schema);
                colvarReportType.ColumnName = "report_type";
                colvarReportType.DataType = DbType.Int32;
                colvarReportType.MaxLength = 0;
                colvarReportType.AutoIncrement = false;
                colvarReportType.IsNullable = false;
                colvarReportType.IsPrimaryKey = false;
                colvarReportType.IsForeignKey = false;
                colvarReportType.IsReadOnly = false;
                colvarReportType.DefaultSetting = @"";
                colvarReportType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReportType);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.String;
                colvarUserId.MaxLength = -1;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarReportIntervalStart = new TableSchema.TableColumn(schema);
                colvarReportIntervalStart.ColumnName = "report_interval_start";
                colvarReportIntervalStart.DataType = DbType.DateTime;
                colvarReportIntervalStart.MaxLength = 0;
                colvarReportIntervalStart.AutoIncrement = false;
                colvarReportIntervalStart.IsNullable = false;
                colvarReportIntervalStart.IsPrimaryKey = false;
                colvarReportIntervalStart.IsForeignKey = false;
                colvarReportIntervalStart.IsReadOnly = false;
                colvarReportIntervalStart.DefaultSetting = @"";
                colvarReportIntervalStart.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReportIntervalStart);

                TableSchema.TableColumn colvarReportIntervalEnd = new TableSchema.TableColumn(schema);
                colvarReportIntervalEnd.ColumnName = "report_interval_end";
                colvarReportIntervalEnd.DataType = DbType.DateTime;
                colvarReportIntervalEnd.MaxLength = 0;
                colvarReportIntervalEnd.AutoIncrement = false;
                colvarReportIntervalEnd.IsNullable = false;
                colvarReportIntervalEnd.IsPrimaryKey = false;
                colvarReportIntervalEnd.IsForeignKey = false;
                colvarReportIntervalEnd.IsReadOnly = false;
                colvarReportIntervalEnd.DefaultSetting = @"";
                colvarReportIntervalEnd.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReportIntervalEnd);

                TableSchema.TableColumn colvarAmountIncrease = new TableSchema.TableColumn(schema);
                colvarAmountIncrease.ColumnName = "amount_increase";
                colvarAmountIncrease.DataType = DbType.Int32;
                colvarAmountIncrease.MaxLength = 0;
                colvarAmountIncrease.AutoIncrement = false;
                colvarAmountIncrease.IsNullable = false;
                colvarAmountIncrease.IsPrimaryKey = false;
                colvarAmountIncrease.IsForeignKey = false;
                colvarAmountIncrease.IsReadOnly = false;
                colvarAmountIncrease.DefaultSetting = @"";
                colvarAmountIncrease.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAmountIncrease);

                TableSchema.TableColumn colvarAmountDecrease = new TableSchema.TableColumn(schema);
                colvarAmountDecrease.ColumnName = "amount_decrease";
                colvarAmountDecrease.DataType = DbType.Int32;
                colvarAmountDecrease.MaxLength = 0;
                colvarAmountDecrease.AutoIncrement = false;
                colvarAmountDecrease.IsNullable = false;
                colvarAmountDecrease.IsPrimaryKey = false;
                colvarAmountDecrease.IsForeignKey = false;
                colvarAmountDecrease.IsReadOnly = false;
                colvarAmountDecrease.DefaultSetting = @"";
                colvarAmountDecrease.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAmountDecrease);

                TableSchema.TableColumn colvarTotalTrustAmount = new TableSchema.TableColumn(schema);
                colvarTotalTrustAmount.ColumnName = "total_trust_amount";
                colvarTotalTrustAmount.DataType = DbType.Int32;
                colvarTotalTrustAmount.MaxLength = 0;
                colvarTotalTrustAmount.AutoIncrement = false;
                colvarTotalTrustAmount.IsNullable = false;
                colvarTotalTrustAmount.IsPrimaryKey = false;
                colvarTotalTrustAmount.IsForeignKey = false;
                colvarTotalTrustAmount.IsReadOnly = false;
                colvarTotalTrustAmount.DefaultSetting = @"";
                colvarTotalTrustAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTotalTrustAmount);

                TableSchema.TableColumn colvarReportGuid = new TableSchema.TableColumn(schema);
                colvarReportGuid.ColumnName = "report_guid";
                colvarReportGuid.DataType = DbType.Guid;
                colvarReportGuid.MaxLength = 0;
                colvarReportGuid.AutoIncrement = false;
                colvarReportGuid.IsNullable = false;
                colvarReportGuid.IsPrimaryKey = false;
                colvarReportGuid.IsForeignKey = false;
                colvarReportGuid.IsReadOnly = false;
                colvarReportGuid.DefaultSetting = @"";
                colvarReportGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReportGuid);

                TableSchema.TableColumn colvarTotalCount = new TableSchema.TableColumn(schema);
                colvarTotalCount.ColumnName = "total_count";
                colvarTotalCount.DataType = DbType.Int32;
                colvarTotalCount.MaxLength = 0;
                colvarTotalCount.AutoIncrement = false;
                colvarTotalCount.IsNullable = false;
                colvarTotalCount.IsPrimaryKey = false;
                colvarTotalCount.IsForeignKey = false;
                colvarTotalCount.IsReadOnly = false;

                colvarTotalCount.DefaultSetting = @"((0))";
                colvarTotalCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTotalCount);

                TableSchema.TableColumn colvarOverOneYearTotal = new TableSchema.TableColumn(schema);
                colvarOverOneYearTotal.ColumnName = "over_one_year_total";
                colvarOverOneYearTotal.DataType = DbType.Int32;
                colvarOverOneYearTotal.MaxLength = 0;
                colvarOverOneYearTotal.AutoIncrement = false;
                colvarOverOneYearTotal.IsNullable = false;
                colvarOverOneYearTotal.IsPrimaryKey = false;
                colvarOverOneYearTotal.IsForeignKey = false;
                colvarOverOneYearTotal.IsReadOnly = false;

                colvarOverOneYearTotal.DefaultSetting = @"((0))";
                colvarOverOneYearTotal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOverOneYearTotal);

                TableSchema.TableColumn colvarScashIncrease = new TableSchema.TableColumn(schema);
                colvarScashIncrease.ColumnName = "scash_increase";
                colvarScashIncrease.DataType = DbType.Int32;
                colvarScashIncrease.MaxLength = 0;
                colvarScashIncrease.AutoIncrement = false;
                colvarScashIncrease.IsNullable = true;
                colvarScashIncrease.IsPrimaryKey = false;
                colvarScashIncrease.IsForeignKey = false;
                colvarScashIncrease.IsReadOnly = false;
                colvarScashIncrease.DefaultSetting = @"";
                colvarScashIncrease.ForeignKeyTableName = "";
                schema.Columns.Add(colvarScashIncrease);

                TableSchema.TableColumn colvarScashDecrease = new TableSchema.TableColumn(schema);
                colvarScashDecrease.ColumnName = "scash_decrease";
                colvarScashDecrease.DataType = DbType.Int32;
                colvarScashDecrease.MaxLength = 0;
                colvarScashDecrease.AutoIncrement = false;
                colvarScashDecrease.IsNullable = true;
                colvarScashDecrease.IsPrimaryKey = false;
                colvarScashDecrease.IsForeignKey = false;
                colvarScashDecrease.IsReadOnly = false;
                colvarScashDecrease.DefaultSetting = @"";
                colvarScashDecrease.ForeignKeyTableName = "";
                schema.Columns.Add(colvarScashDecrease);

                TableSchema.TableColumn colvarUserIncrease = new TableSchema.TableColumn(schema);
                colvarUserIncrease.ColumnName = "user_increase";
                colvarUserIncrease.DataType = DbType.Int32;
                colvarUserIncrease.MaxLength = 0;
                colvarUserIncrease.AutoIncrement = false;
                colvarUserIncrease.IsNullable = true;
                colvarUserIncrease.IsPrimaryKey = false;
                colvarUserIncrease.IsForeignKey = false;
                colvarUserIncrease.IsReadOnly = false;
                colvarUserIncrease.DefaultSetting = @"";
                colvarUserIncrease.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserIncrease);

                TableSchema.TableColumn colvarUserDecrease = new TableSchema.TableColumn(schema);
                colvarUserDecrease.ColumnName = "user_decrease";
                colvarUserDecrease.DataType = DbType.Int32;
                colvarUserDecrease.MaxLength = 0;
                colvarUserDecrease.AutoIncrement = false;
                colvarUserDecrease.IsNullable = true;
                colvarUserDecrease.IsPrimaryKey = false;
                colvarUserDecrease.IsForeignKey = false;
                colvarUserDecrease.IsReadOnly = false;
                colvarUserDecrease.DefaultSetting = @"";
                colvarUserDecrease.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserDecrease);

                TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
                colvarFlag.ColumnName = "flag";
                colvarFlag.DataType = DbType.Int32;
                colvarFlag.MaxLength = 0;
                colvarFlag.AutoIncrement = false;
                colvarFlag.IsNullable = false;
                colvarFlag.IsPrimaryKey = false;
                colvarFlag.IsForeignKey = false;
                colvarFlag.IsReadOnly = false;

                colvarFlag.DefaultSetting = @"((2))";
                colvarFlag.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFlag);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("trust_verification_report", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("TrustProvider")]
        [Bindable(true)]
        public int TrustProvider
        {
            get { return GetColumnValue<int>(Columns.TrustProvider); }
            set { SetColumnValue(Columns.TrustProvider, value); }
        }

        [XmlAttribute("ReportDate")]
        [Bindable(true)]
        public DateTime ReportDate
        {
            get { return GetColumnValue<DateTime>(Columns.ReportDate); }
            set { SetColumnValue(Columns.ReportDate, value); }
        }

        [XmlAttribute("ReportType")]
        [Bindable(true)]
        public int ReportType
        {
            get { return GetColumnValue<int>(Columns.ReportType); }
            set { SetColumnValue(Columns.ReportType, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public string UserId
        {
            get { return GetColumnValue<string>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("ReportIntervalStart")]
        [Bindable(true)]
        public DateTime ReportIntervalStart
        {
            get { return GetColumnValue<DateTime>(Columns.ReportIntervalStart); }
            set { SetColumnValue(Columns.ReportIntervalStart, value); }
        }

        [XmlAttribute("ReportIntervalEnd")]
        [Bindable(true)]
        public DateTime ReportIntervalEnd
        {
            get { return GetColumnValue<DateTime>(Columns.ReportIntervalEnd); }
            set { SetColumnValue(Columns.ReportIntervalEnd, value); }
        }

        [XmlAttribute("AmountIncrease")]
        [Bindable(true)]
        public int AmountIncrease
        {
            get { return GetColumnValue<int>(Columns.AmountIncrease); }
            set { SetColumnValue(Columns.AmountIncrease, value); }
        }

        [XmlAttribute("AmountDecrease")]
        [Bindable(true)]
        public int AmountDecrease
        {
            get { return GetColumnValue<int>(Columns.AmountDecrease); }
            set { SetColumnValue(Columns.AmountDecrease, value); }
        }

        [XmlAttribute("TotalTrustAmount")]
        [Bindable(true)]
        public int TotalTrustAmount
        {
            get { return GetColumnValue<int>(Columns.TotalTrustAmount); }
            set { SetColumnValue(Columns.TotalTrustAmount, value); }
        }

        [XmlAttribute("ReportGuid")]
        [Bindable(true)]
        public Guid ReportGuid
        {
            get { return GetColumnValue<Guid>(Columns.ReportGuid); }
            set { SetColumnValue(Columns.ReportGuid, value); }
        }

        [XmlAttribute("TotalCount")]
        [Bindable(true)]
        public int TotalCount
        {
            get { return GetColumnValue<int>(Columns.TotalCount); }
            set { SetColumnValue(Columns.TotalCount, value); }
        }

        [XmlAttribute("OverOneYearTotal")]
        [Bindable(true)]
        public int OverOneYearTotal
        {
            get { return GetColumnValue<int>(Columns.OverOneYearTotal); }
            set { SetColumnValue(Columns.OverOneYearTotal, value); }
        }

        [XmlAttribute("ScashIncrease")]
        [Bindable(true)]
        public int? ScashIncrease
        {
            get { return GetColumnValue<int?>(Columns.ScashIncrease); }
            set { SetColumnValue(Columns.ScashIncrease, value); }
        }

        [XmlAttribute("ScashDecrease")]
        [Bindable(true)]
        public int? ScashDecrease
        {
            get { return GetColumnValue<int?>(Columns.ScashDecrease); }
            set { SetColumnValue(Columns.ScashDecrease, value); }
        }

        [XmlAttribute("UserIncrease")]
        [Bindable(true)]
        public int? UserIncrease
        {
            get { return GetColumnValue<int?>(Columns.UserIncrease); }
            set { SetColumnValue(Columns.UserIncrease, value); }
        }

        [XmlAttribute("UserDecrease")]
        [Bindable(true)]
        public int? UserDecrease
        {
            get { return GetColumnValue<int?>(Columns.UserDecrease); }
            set { SetColumnValue(Columns.UserDecrease, value); }
        }

        [XmlAttribute("Flag")]
        [Bindable(true)]
        public int Flag
        {
            get { return GetColumnValue<int>(Columns.Flag); }
            set { SetColumnValue(Columns.Flag, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn TrustProviderColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ReportDateColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn ReportTypeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ReportIntervalStartColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ReportIntervalEndColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn AmountIncreaseColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn AmountDecreaseColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn TotalTrustAmountColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn ReportGuidColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn TotalCountColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn OverOneYearTotalColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn ScashIncreaseColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn ScashDecreaseColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn UserIncreaseColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn UserDecreaseColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn FlagColumn
        {
            get { return Schema.Columns[17]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string TrustProvider = @"trust_provider";
            public static string ReportDate = @"report_date";
            public static string ReportType = @"report_type";
            public static string UserId = @"user_id";
            public static string ReportIntervalStart = @"report_interval_start";
            public static string ReportIntervalEnd = @"report_interval_end";
            public static string AmountIncrease = @"amount_increase";
            public static string AmountDecrease = @"amount_decrease";
            public static string TotalTrustAmount = @"total_trust_amount";
            public static string ReportGuid = @"report_guid";
            public static string TotalCount = @"total_count";
            public static string OverOneYearTotal = @"over_one_year_total";
            public static string ScashIncrease = @"scash_increase";
            public static string ScashDecrease = @"scash_decrease";
            public static string UserIncrease = @"user_increase";
            public static string UserDecrease = @"user_decrease";
            public static string Flag = @"flag";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

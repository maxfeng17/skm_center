using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpLockPoint class.
	/// </summary>
    [Serializable]
	public partial class PcpLockPointCollection : RepositoryList<PcpLockPoint, PcpLockPointCollection>
	{	   
		public PcpLockPointCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpLockPointCollection</returns>
		public PcpLockPointCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpLockPoint o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_lock_point table.
	/// </summary>
	[Serializable]
	public partial class PcpLockPoint : RepositoryRecord<PcpLockPoint>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpLockPoint()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpLockPoint(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_lock_point", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarDiscountCampaignId = new TableSchema.TableColumn(schema);
				colvarDiscountCampaignId.ColumnName = "discount_campaign_id";
				colvarDiscountCampaignId.DataType = DbType.Int32;
				colvarDiscountCampaignId.MaxLength = 0;
				colvarDiscountCampaignId.AutoIncrement = false;
				colvarDiscountCampaignId.IsNullable = false;
				colvarDiscountCampaignId.IsPrimaryKey = false;
				colvarDiscountCampaignId.IsForeignKey = false;
				colvarDiscountCampaignId.IsReadOnly = false;
				colvarDiscountCampaignId.DefaultSetting = @"";
				colvarDiscountCampaignId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountCampaignId);
				
				TableSchema.TableColumn colvarPointType = new TableSchema.TableColumn(schema);
				colvarPointType.ColumnName = "point_type";
				colvarPointType.DataType = DbType.Int32;
				colvarPointType.MaxLength = 0;
				colvarPointType.AutoIncrement = false;
				colvarPointType.IsNullable = false;
				colvarPointType.IsPrimaryKey = false;
				colvarPointType.IsForeignKey = false;
				colvarPointType.IsReadOnly = false;
				colvarPointType.DefaultSetting = @"";
				colvarPointType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPointType);
				
				TableSchema.TableColumn colvarLockPoint = new TableSchema.TableColumn(schema);
				colvarLockPoint.ColumnName = "lock_point";
				colvarLockPoint.DataType = DbType.Int32;
				colvarLockPoint.MaxLength = 0;
				colvarLockPoint.AutoIncrement = false;
				colvarLockPoint.IsNullable = false;
				colvarLockPoint.IsPrimaryKey = false;
				colvarLockPoint.IsForeignKey = false;
				colvarLockPoint.IsReadOnly = false;
				colvarLockPoint.DefaultSetting = @"";
				colvarLockPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLockPoint);
				
				TableSchema.TableColumn colvarVerificationPoint = new TableSchema.TableColumn(schema);
				colvarVerificationPoint.ColumnName = "verification_point";
				colvarVerificationPoint.DataType = DbType.Int32;
				colvarVerificationPoint.MaxLength = 0;
				colvarVerificationPoint.AutoIncrement = false;
				colvarVerificationPoint.IsNullable = false;
				colvarVerificationPoint.IsPrimaryKey = false;
				colvarVerificationPoint.IsForeignKey = false;
				colvarVerificationPoint.IsReadOnly = false;
				colvarVerificationPoint.DefaultSetting = @"";
				colvarVerificationPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerificationPoint);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_lock_point",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("DiscountCampaignId")]
		[Bindable(true)]
		public int DiscountCampaignId 
		{
			get { return GetColumnValue<int>(Columns.DiscountCampaignId); }
			set { SetColumnValue(Columns.DiscountCampaignId, value); }
		}
		  
		[XmlAttribute("PointType")]
		[Bindable(true)]
		public int PointType 
		{
			get { return GetColumnValue<int>(Columns.PointType); }
			set { SetColumnValue(Columns.PointType, value); }
		}
		  
		[XmlAttribute("LockPoint")]
		[Bindable(true)]
		public int LockPoint 
		{
			get { return GetColumnValue<int>(Columns.LockPoint); }
			set { SetColumnValue(Columns.LockPoint, value); }
		}
		  
		[XmlAttribute("VerificationPoint")]
		[Bindable(true)]
		public int VerificationPoint 
		{
			get { return GetColumnValue<int>(Columns.VerificationPoint); }
			set { SetColumnValue(Columns.VerificationPoint, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountCampaignIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PointTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn LockPointColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn VerificationPointColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string DiscountCampaignId = @"discount_campaign_id";
			 public static string PointType = @"point_type";
			 public static string LockPoint = @"lock_point";
			 public static string VerificationPoint = @"verification_point";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string UserId = @"user_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

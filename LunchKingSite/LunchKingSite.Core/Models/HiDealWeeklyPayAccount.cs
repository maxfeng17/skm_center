using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the HiDealWeeklyPayAccount class.
    /// </summary>
    [Serializable]
    public partial class HiDealWeeklyPayAccountCollection : RepositoryList<HiDealWeeklyPayAccount, HiDealWeeklyPayAccountCollection>
    {
        public HiDealWeeklyPayAccountCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealWeeklyPayAccountCollection</returns>
        public HiDealWeeklyPayAccountCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealWeeklyPayAccount o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the hi_deal_weekly_pay_account table.
    /// </summary>
    [Serializable]
    public partial class HiDealWeeklyPayAccount : RepositoryRecord<HiDealWeeklyPayAccount>, IRecordBase
    {
        #region .ctors and Default Settings

        public HiDealWeeklyPayAccount()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public HiDealWeeklyPayAccount(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("hi_deal_weekly_pay_account", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarReportId = new TableSchema.TableColumn(schema);
                colvarReportId.ColumnName = "report_id";
                colvarReportId.DataType = DbType.Int32;
                colvarReportId.MaxLength = 0;
                colvarReportId.AutoIncrement = false;
                colvarReportId.IsNullable = false;
                colvarReportId.IsPrimaryKey = false;
                colvarReportId.IsForeignKey = false;
                colvarReportId.IsReadOnly = false;
                colvarReportId.DefaultSetting = @"";
                colvarReportId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReportId);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = true;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                colvarSellerGuid.DefaultSetting = @"";
                colvarSellerGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "company_name";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 200;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;
                colvarCompanyName.DefaultSetting = @"";
                colvarCompanyName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCompanyName);

                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                colvarSellerName.DefaultSetting = @"";
                colvarSellerName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerName);

                TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
                colvarAccountName.ColumnName = "account_name";
                colvarAccountName.DataType = DbType.String;
                colvarAccountName.MaxLength = 100;
                colvarAccountName.AutoIncrement = false;
                colvarAccountName.IsNullable = true;
                colvarAccountName.IsPrimaryKey = false;
                colvarAccountName.IsForeignKey = false;
                colvarAccountName.IsReadOnly = false;
                colvarAccountName.DefaultSetting = @"";
                colvarAccountName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAccountName);

                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.AnsiString;
                colvarAccountId.MaxLength = 10;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = true;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                colvarAccountId.DefaultSetting = @"";
                colvarAccountId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAccountId);

                TableSchema.TableColumn colvarBankNo = new TableSchema.TableColumn(schema);
                colvarBankNo.ColumnName = "bank_no";
                colvarBankNo.DataType = DbType.AnsiString;
                colvarBankNo.MaxLength = 20;
                colvarBankNo.AutoIncrement = false;
                colvarBankNo.IsNullable = true;
                colvarBankNo.IsPrimaryKey = false;
                colvarBankNo.IsForeignKey = false;
                colvarBankNo.IsReadOnly = false;
                colvarBankNo.DefaultSetting = @"";
                colvarBankNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBankNo);

                TableSchema.TableColumn colvarBranchNo = new TableSchema.TableColumn(schema);
                colvarBranchNo.ColumnName = "branch_no";
                colvarBranchNo.DataType = DbType.AnsiString;
                colvarBranchNo.MaxLength = 20;
                colvarBranchNo.AutoIncrement = false;
                colvarBranchNo.IsNullable = true;
                colvarBranchNo.IsPrimaryKey = false;
                colvarBranchNo.IsForeignKey = false;
                colvarBranchNo.IsReadOnly = false;
                colvarBranchNo.DefaultSetting = @"";
                colvarBranchNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBranchNo);

                TableSchema.TableColumn colvarAccountNo = new TableSchema.TableColumn(schema);
                colvarAccountNo.ColumnName = "account_no";
                colvarAccountNo.DataType = DbType.AnsiString;
                colvarAccountNo.MaxLength = 50;
                colvarAccountNo.AutoIncrement = false;
                colvarAccountNo.IsNullable = true;
                colvarAccountNo.IsPrimaryKey = false;
                colvarAccountNo.IsForeignKey = false;
                colvarAccountNo.IsReadOnly = false;
                colvarAccountNo.DefaultSetting = @"";
                colvarAccountNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAccountNo);

                TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
                colvarEmail.ColumnName = "email";
                colvarEmail.DataType = DbType.AnsiString;
                colvarEmail.MaxLength = 100;
                colvarEmail.AutoIncrement = false;
                colvarEmail.IsNullable = true;
                colvarEmail.IsPrimaryKey = false;
                colvarEmail.IsForeignKey = false;
                colvarEmail.IsReadOnly = false;
                colvarEmail.DefaultSetting = @"";
                colvarEmail.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEmail);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 1000;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                colvarMessage.DefaultSetting = @"";
                colvarMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarSignCompanyId = new TableSchema.TableColumn(schema);
                colvarSignCompanyId.ColumnName = "sign_company_id";
                colvarSignCompanyId.DataType = DbType.AnsiString;
                colvarSignCompanyId.MaxLength = 20;
                colvarSignCompanyId.AutoIncrement = false;
                colvarSignCompanyId.IsNullable = true;
                colvarSignCompanyId.IsPrimaryKey = false;
                colvarSignCompanyId.IsForeignKey = false;
                colvarSignCompanyId.IsReadOnly = false;
                colvarSignCompanyId.DefaultSetting = @"";
                colvarSignCompanyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSignCompanyId);

                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                colvarStoreGuid.DefaultSetting = @"";
                colvarStoreGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreGuid);

                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 100;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                colvarStoreName.DefaultSetting = @"";
                colvarStoreName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreName);

                TableSchema.TableColumn colvarStoreCompanyName = new TableSchema.TableColumn(schema);
                colvarStoreCompanyName.ColumnName = "store_company_name";
                colvarStoreCompanyName.DataType = DbType.String;
                colvarStoreCompanyName.MaxLength = 200;
                colvarStoreCompanyName.AutoIncrement = false;
                colvarStoreCompanyName.IsNullable = true;
                colvarStoreCompanyName.IsPrimaryKey = false;
                colvarStoreCompanyName.IsForeignKey = false;
                colvarStoreCompanyName.IsReadOnly = false;
                colvarStoreCompanyName.DefaultSetting = @"";
                colvarStoreCompanyName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreCompanyName);

                TableSchema.TableColumn colvarStoreSellerName = new TableSchema.TableColumn(schema);
                colvarStoreSellerName.ColumnName = "store_seller_name";
                colvarStoreSellerName.DataType = DbType.String;
                colvarStoreSellerName.MaxLength = 100;
                colvarStoreSellerName.AutoIncrement = false;
                colvarStoreSellerName.IsNullable = true;
                colvarStoreSellerName.IsPrimaryKey = false;
                colvarStoreSellerName.IsForeignKey = false;
                colvarStoreSellerName.IsReadOnly = false;
                colvarStoreSellerName.DefaultSetting = @"";
                colvarStoreSellerName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreSellerName);

                TableSchema.TableColumn colvarStoreAccountName = new TableSchema.TableColumn(schema);
                colvarStoreAccountName.ColumnName = "store_account_name";
                colvarStoreAccountName.DataType = DbType.String;
                colvarStoreAccountName.MaxLength = 100;
                colvarStoreAccountName.AutoIncrement = false;
                colvarStoreAccountName.IsNullable = true;
                colvarStoreAccountName.IsPrimaryKey = false;
                colvarStoreAccountName.IsForeignKey = false;
                colvarStoreAccountName.IsReadOnly = false;
                colvarStoreAccountName.DefaultSetting = @"";
                colvarStoreAccountName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreAccountName);

                TableSchema.TableColumn colvarStoreAccountId = new TableSchema.TableColumn(schema);
                colvarStoreAccountId.ColumnName = "store_account_id";
                colvarStoreAccountId.DataType = DbType.AnsiString;
                colvarStoreAccountId.MaxLength = 10;
                colvarStoreAccountId.AutoIncrement = false;
                colvarStoreAccountId.IsNullable = true;
                colvarStoreAccountId.IsPrimaryKey = false;
                colvarStoreAccountId.IsForeignKey = false;
                colvarStoreAccountId.IsReadOnly = false;
                colvarStoreAccountId.DefaultSetting = @"";
                colvarStoreAccountId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreAccountId);

                TableSchema.TableColumn colvarStoreBankNo = new TableSchema.TableColumn(schema);
                colvarStoreBankNo.ColumnName = "store_bank_no";
                colvarStoreBankNo.DataType = DbType.AnsiString;
                colvarStoreBankNo.MaxLength = 20;
                colvarStoreBankNo.AutoIncrement = false;
                colvarStoreBankNo.IsNullable = true;
                colvarStoreBankNo.IsPrimaryKey = false;
                colvarStoreBankNo.IsForeignKey = false;
                colvarStoreBankNo.IsReadOnly = false;
                colvarStoreBankNo.DefaultSetting = @"";
                colvarStoreBankNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreBankNo);

                TableSchema.TableColumn colvarStoreBranchNo = new TableSchema.TableColumn(schema);
                colvarStoreBranchNo.ColumnName = "store_branch_no";
                colvarStoreBranchNo.DataType = DbType.AnsiString;
                colvarStoreBranchNo.MaxLength = 20;
                colvarStoreBranchNo.AutoIncrement = false;
                colvarStoreBranchNo.IsNullable = true;
                colvarStoreBranchNo.IsPrimaryKey = false;
                colvarStoreBranchNo.IsForeignKey = false;
                colvarStoreBranchNo.IsReadOnly = false;
                colvarStoreBranchNo.DefaultSetting = @"";
                colvarStoreBranchNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreBranchNo);

                TableSchema.TableColumn colvarStoreAccountNo = new TableSchema.TableColumn(schema);
                colvarStoreAccountNo.ColumnName = "store_account_no";
                colvarStoreAccountNo.DataType = DbType.AnsiString;
                colvarStoreAccountNo.MaxLength = 50;
                colvarStoreAccountNo.AutoIncrement = false;
                colvarStoreAccountNo.IsNullable = true;
                colvarStoreAccountNo.IsPrimaryKey = false;
                colvarStoreAccountNo.IsForeignKey = false;
                colvarStoreAccountNo.IsReadOnly = false;
                colvarStoreAccountNo.DefaultSetting = @"";
                colvarStoreAccountNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreAccountNo);

                TableSchema.TableColumn colvarStoreEmail = new TableSchema.TableColumn(schema);
                colvarStoreEmail.ColumnName = "store_email";
                colvarStoreEmail.DataType = DbType.AnsiString;
                colvarStoreEmail.MaxLength = 100;
                colvarStoreEmail.AutoIncrement = false;
                colvarStoreEmail.IsNullable = true;
                colvarStoreEmail.IsPrimaryKey = false;
                colvarStoreEmail.IsForeignKey = false;
                colvarStoreEmail.IsReadOnly = false;
                colvarStoreEmail.DefaultSetting = @"";
                colvarStoreEmail.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreEmail);

                TableSchema.TableColumn colvarStoreMessage = new TableSchema.TableColumn(schema);
                colvarStoreMessage.ColumnName = "store_message";
                colvarStoreMessage.DataType = DbType.String;
                colvarStoreMessage.MaxLength = 1000;
                colvarStoreMessage.AutoIncrement = false;
                colvarStoreMessage.IsNullable = true;
                colvarStoreMessage.IsPrimaryKey = false;
                colvarStoreMessage.IsForeignKey = false;
                colvarStoreMessage.IsReadOnly = false;
                colvarStoreMessage.DefaultSetting = @"";
                colvarStoreMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreMessage);

                TableSchema.TableColumn colvarStoreSignCompanyId = new TableSchema.TableColumn(schema);
                colvarStoreSignCompanyId.ColumnName = "store_sign_company_id";
                colvarStoreSignCompanyId.DataType = DbType.AnsiString;
                colvarStoreSignCompanyId.MaxLength = 20;
                colvarStoreSignCompanyId.AutoIncrement = false;
                colvarStoreSignCompanyId.IsNullable = true;
                colvarStoreSignCompanyId.IsPrimaryKey = false;
                colvarStoreSignCompanyId.IsForeignKey = false;
                colvarStoreSignCompanyId.IsReadOnly = false;
                colvarStoreSignCompanyId.DefaultSetting = @"";
                colvarStoreSignCompanyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreSignCompanyId);

                TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
                colvarCreator.ColumnName = "creator";
                colvarCreator.DataType = DbType.String;
                colvarCreator.MaxLength = 50;
                colvarCreator.AutoIncrement = false;
                colvarCreator.IsNullable = false;
                colvarCreator.IsPrimaryKey = false;
                colvarCreator.IsForeignKey = false;
                colvarCreator.IsReadOnly = false;
                colvarCreator.DefaultSetting = @"";
                colvarCreator.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreator);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarAccountantName = new TableSchema.TableColumn(schema);
                colvarAccountantName.ColumnName = "accountant_name";
                colvarAccountantName.DataType = DbType.String;
                colvarAccountantName.MaxLength = 20;
                colvarAccountantName.AutoIncrement = false;
                colvarAccountantName.IsNullable = true;
                colvarAccountantName.IsPrimaryKey = false;
                colvarAccountantName.IsForeignKey = false;
                colvarAccountantName.IsReadOnly = false;
                colvarAccountantName.DefaultSetting = @"";
                colvarAccountantName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAccountantName);

                TableSchema.TableColumn colvarAccountantTel = new TableSchema.TableColumn(schema);
                colvarAccountantTel.ColumnName = "accountant_tel";
                colvarAccountantTel.DataType = DbType.AnsiString;
                colvarAccountantTel.MaxLength = 20;
                colvarAccountantTel.AutoIncrement = false;
                colvarAccountantTel.IsNullable = true;
                colvarAccountantTel.IsPrimaryKey = false;
                colvarAccountantTel.IsForeignKey = false;
                colvarAccountantTel.IsReadOnly = false;
                colvarAccountantTel.DefaultSetting = @"";
                colvarAccountantTel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAccountantTel);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("hi_deal_weekly_pay_account", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("ReportId")]
        [Bindable(true)]
        public int ReportId
        {
            get { return GetColumnValue<int>(Columns.ReportId); }
            set { SetColumnValue(Columns.ReportId, value); }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid? SellerGuid
        {
            get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
            set { SetColumnValue(Columns.SellerGuid, value); }
        }

        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName
        {
            get { return GetColumnValue<string>(Columns.CompanyName); }
            set { SetColumnValue(Columns.CompanyName, value); }
        }

        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName
        {
            get { return GetColumnValue<string>(Columns.SellerName); }
            set { SetColumnValue(Columns.SellerName, value); }
        }

        [XmlAttribute("AccountName")]
        [Bindable(true)]
        public string AccountName
        {
            get { return GetColumnValue<string>(Columns.AccountName); }
            set { SetColumnValue(Columns.AccountName, value); }
        }

        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId
        {
            get { return GetColumnValue<string>(Columns.AccountId); }
            set { SetColumnValue(Columns.AccountId, value); }
        }

        [XmlAttribute("BankNo")]
        [Bindable(true)]
        public string BankNo
        {
            get { return GetColumnValue<string>(Columns.BankNo); }
            set { SetColumnValue(Columns.BankNo, value); }
        }

        [XmlAttribute("BranchNo")]
        [Bindable(true)]
        public string BranchNo
        {
            get { return GetColumnValue<string>(Columns.BranchNo); }
            set { SetColumnValue(Columns.BranchNo, value); }
        }

        [XmlAttribute("AccountNo")]
        [Bindable(true)]
        public string AccountNo
        {
            get { return GetColumnValue<string>(Columns.AccountNo); }
            set { SetColumnValue(Columns.AccountNo, value); }
        }

        [XmlAttribute("Email")]
        [Bindable(true)]
        public string Email
        {
            get { return GetColumnValue<string>(Columns.Email); }
            set { SetColumnValue(Columns.Email, value); }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get { return GetColumnValue<string>(Columns.Message); }
            set { SetColumnValue(Columns.Message, value); }
        }

        [XmlAttribute("SignCompanyId")]
        [Bindable(true)]
        public string SignCompanyId
        {
            get { return GetColumnValue<string>(Columns.SignCompanyId); }
            set { SetColumnValue(Columns.SignCompanyId, value); }
        }

        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid
        {
            get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
            set { SetColumnValue(Columns.StoreGuid, value); }
        }

        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName
        {
            get { return GetColumnValue<string>(Columns.StoreName); }
            set { SetColumnValue(Columns.StoreName, value); }
        }

        [XmlAttribute("StoreCompanyName")]
        [Bindable(true)]
        public string StoreCompanyName
        {
            get { return GetColumnValue<string>(Columns.StoreCompanyName); }
            set { SetColumnValue(Columns.StoreCompanyName, value); }
        }

        [XmlAttribute("StoreSellerName")]
        [Bindable(true)]
        public string StoreSellerName
        {
            get { return GetColumnValue<string>(Columns.StoreSellerName); }
            set { SetColumnValue(Columns.StoreSellerName, value); }
        }

        [XmlAttribute("StoreAccountName")]
        [Bindable(true)]
        public string StoreAccountName
        {
            get { return GetColumnValue<string>(Columns.StoreAccountName); }
            set { SetColumnValue(Columns.StoreAccountName, value); }
        }

        [XmlAttribute("StoreAccountId")]
        [Bindable(true)]
        public string StoreAccountId
        {
            get { return GetColumnValue<string>(Columns.StoreAccountId); }
            set { SetColumnValue(Columns.StoreAccountId, value); }
        }

        [XmlAttribute("StoreBankNo")]
        [Bindable(true)]
        public string StoreBankNo
        {
            get { return GetColumnValue<string>(Columns.StoreBankNo); }
            set { SetColumnValue(Columns.StoreBankNo, value); }
        }

        [XmlAttribute("StoreBranchNo")]
        [Bindable(true)]
        public string StoreBranchNo
        {
            get { return GetColumnValue<string>(Columns.StoreBranchNo); }
            set { SetColumnValue(Columns.StoreBranchNo, value); }
        }

        [XmlAttribute("StoreAccountNo")]
        [Bindable(true)]
        public string StoreAccountNo
        {
            get { return GetColumnValue<string>(Columns.StoreAccountNo); }
            set { SetColumnValue(Columns.StoreAccountNo, value); }
        }

        [XmlAttribute("StoreEmail")]
        [Bindable(true)]
        public string StoreEmail
        {
            get { return GetColumnValue<string>(Columns.StoreEmail); }
            set { SetColumnValue(Columns.StoreEmail, value); }
        }

        [XmlAttribute("StoreMessage")]
        [Bindable(true)]
        public string StoreMessage
        {
            get { return GetColumnValue<string>(Columns.StoreMessage); }
            set { SetColumnValue(Columns.StoreMessage, value); }
        }

        [XmlAttribute("StoreSignCompanyId")]
        [Bindable(true)]
        public string StoreSignCompanyId
        {
            get { return GetColumnValue<string>(Columns.StoreSignCompanyId); }
            set { SetColumnValue(Columns.StoreSignCompanyId, value); }
        }

        [XmlAttribute("Creator")]
        [Bindable(true)]
        public string Creator
        {
            get { return GetColumnValue<string>(Columns.Creator); }
            set { SetColumnValue(Columns.Creator, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("AccountantName")]
        [Bindable(true)]
        public string AccountantName
        {
            get { return GetColumnValue<string>(Columns.AccountantName); }
            set { SetColumnValue(Columns.AccountantName, value); }
        }

        [XmlAttribute("AccountantTel")]
        [Bindable(true)]
        public string AccountantTel
        {
            get { return GetColumnValue<string>(Columns.AccountantTel); }
            set { SetColumnValue(Columns.AccountantTel, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ReportIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CompanyNameColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn SellerNameColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn AccountNameColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn AccountIdColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn BankNoColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn BranchNoColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn AccountNoColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn EmailColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn SignCompanyIdColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn StoreNameColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn StoreCompanyNameColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn StoreSellerNameColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn StoreAccountNameColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn StoreAccountIdColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn StoreBankNoColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn StoreBranchNoColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn StoreAccountNoColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn StoreEmailColumn
        {
            get { return Schema.Columns[22]; }
        }



        public static TableSchema.TableColumn StoreMessageColumn
        {
            get { return Schema.Columns[23]; }
        }



        public static TableSchema.TableColumn StoreSignCompanyIdColumn
        {
            get { return Schema.Columns[24]; }
        }



        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[25]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[26]; }
        }



        public static TableSchema.TableColumn AccountantNameColumn
        {
            get { return Schema.Columns[27]; }
        }



        public static TableSchema.TableColumn AccountantTelColumn
        {
            get { return Schema.Columns[28]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string ReportId = @"report_id";
            public static string SellerGuid = @"seller_guid";
            public static string CompanyName = @"company_name";
            public static string SellerName = @"seller_name";
            public static string AccountName = @"account_name";
            public static string AccountId = @"account_id";
            public static string BankNo = @"bank_no";
            public static string BranchNo = @"branch_no";
            public static string AccountNo = @"account_no";
            public static string Email = @"email";
            public static string Message = @"message";
            public static string SignCompanyId = @"sign_company_id";
            public static string StoreGuid = @"store_guid";
            public static string StoreName = @"store_name";
            public static string StoreCompanyName = @"store_company_name";
            public static string StoreSellerName = @"store_seller_name";
            public static string StoreAccountName = @"store_account_name";
            public static string StoreAccountId = @"store_account_id";
            public static string StoreBankNo = @"store_bank_no";
            public static string StoreBranchNo = @"store_branch_no";
            public static string StoreAccountNo = @"store_account_no";
            public static string StoreEmail = @"store_email";
            public static string StoreMessage = @"store_message";
            public static string StoreSignCompanyId = @"store_sign_company_id";
            public static string Creator = @"creator";
            public static string CreateTime = @"create_time";
            public static string AccountantName = @"accountant_name";
            public static string AccountantTel = @"accountant_tel";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

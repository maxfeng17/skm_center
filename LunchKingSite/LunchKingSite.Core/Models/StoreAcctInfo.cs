using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the StoreAcctInfo class.
	/// </summary>
    [Serializable]
	public partial class StoreAcctInfoCollection : RepositoryList<StoreAcctInfo, StoreAcctInfoCollection>
	{	   
		public StoreAcctInfoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>StoreAcctInfoCollection</returns>
		public StoreAcctInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                StoreAcctInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the store_acct_info table.
	/// </summary>
	[Serializable]
	public partial class StoreAcctInfo : RepositoryRecord<StoreAcctInfo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public StoreAcctInfo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public StoreAcctInfo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("store_acct_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = true;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarAcctCode = new TableSchema.TableColumn(schema);
				colvarAcctCode.ColumnName = "acct_code";
				colvarAcctCode.DataType = DbType.AnsiString;
				colvarAcctCode.MaxLength = 10;
				colvarAcctCode.AutoIncrement = false;
				colvarAcctCode.IsNullable = true;
				colvarAcctCode.IsPrimaryKey = false;
				colvarAcctCode.IsForeignKey = false;
				colvarAcctCode.IsReadOnly = false;
				colvarAcctCode.DefaultSetting = @"";
				colvarAcctCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAcctCode);
				
				TableSchema.TableColumn colvarAcctNo = new TableSchema.TableColumn(schema);
				colvarAcctNo.ColumnName = "acct_no";
				colvarAcctNo.DataType = DbType.AnsiString;
				colvarAcctNo.MaxLength = 10;
				colvarAcctNo.AutoIncrement = false;
				colvarAcctNo.IsNullable = true;
				colvarAcctNo.IsPrimaryKey = false;
				colvarAcctNo.IsForeignKey = false;
				colvarAcctNo.IsReadOnly = false;
				colvarAcctNo.DefaultSetting = @"";
				colvarAcctNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAcctNo);
				
				TableSchema.TableColumn colvarAcctPw = new TableSchema.TableColumn(schema);
				colvarAcctPw.ColumnName = "acct_pw";
				colvarAcctPw.DataType = DbType.AnsiString;
				colvarAcctPw.MaxLength = 10;
				colvarAcctPw.AutoIncrement = false;
				colvarAcctPw.IsNullable = true;
				colvarAcctPw.IsPrimaryKey = false;
				colvarAcctPw.IsForeignKey = false;
				colvarAcctPw.IsReadOnly = false;
				colvarAcctPw.DefaultSetting = @"";
				colvarAcctPw.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAcctPw);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarImeiCode = new TableSchema.TableColumn(schema);
				colvarImeiCode.ColumnName = "imei_code";
				colvarImeiCode.DataType = DbType.AnsiString;
				colvarImeiCode.MaxLength = 50;
				colvarImeiCode.AutoIncrement = false;
				colvarImeiCode.IsNullable = true;
				colvarImeiCode.IsPrimaryKey = false;
				colvarImeiCode.IsForeignKey = false;
				colvarImeiCode.IsReadOnly = false;
				colvarImeiCode.DefaultSetting = @"";
				colvarImeiCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImeiCode);
				
				TableSchema.TableColumn colvarSimCode = new TableSchema.TableColumn(schema);
				colvarSimCode.ColumnName = "sim_code";
				colvarSimCode.DataType = DbType.AnsiString;
				colvarSimCode.MaxLength = 50;
				colvarSimCode.AutoIncrement = false;
				colvarSimCode.IsNullable = true;
				colvarSimCode.IsPrimaryKey = false;
				colvarSimCode.IsForeignKey = false;
				colvarSimCode.IsReadOnly = false;
				colvarSimCode.DefaultSetting = @"";
				colvarSimCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSimCode);
				
				TableSchema.TableColumn colvarRegisterCode = new TableSchema.TableColumn(schema);
				colvarRegisterCode.ColumnName = "register_code";
				colvarRegisterCode.DataType = DbType.AnsiString;
				colvarRegisterCode.MaxLength = 10;
				colvarRegisterCode.AutoIncrement = false;
				colvarRegisterCode.IsNullable = true;
				colvarRegisterCode.IsPrimaryKey = false;
				colvarRegisterCode.IsForeignKey = false;
				colvarRegisterCode.IsReadOnly = false;
				colvarRegisterCode.DefaultSetting = @"";
				colvarRegisterCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRegisterCode);
				
				TableSchema.TableColumn colvarNewRegisterCode = new TableSchema.TableColumn(schema);
				colvarNewRegisterCode.ColumnName = "new_register_code";
				colvarNewRegisterCode.DataType = DbType.AnsiString;
				colvarNewRegisterCode.MaxLength = 10;
				colvarNewRegisterCode.AutoIncrement = false;
				colvarNewRegisterCode.IsNullable = true;
				colvarNewRegisterCode.IsPrimaryKey = false;
				colvarNewRegisterCode.IsForeignKey = false;
				colvarNewRegisterCode.IsReadOnly = false;
				colvarNewRegisterCode.DefaultSetting = @"";
				colvarNewRegisterCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNewRegisterCode);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("store_acct_info",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid? StoreGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("AcctCode")]
		[Bindable(true)]
		public string AcctCode 
		{
			get { return GetColumnValue<string>(Columns.AcctCode); }
			set { SetColumnValue(Columns.AcctCode, value); }
		}
		  
		[XmlAttribute("AcctNo")]
		[Bindable(true)]
		public string AcctNo 
		{
			get { return GetColumnValue<string>(Columns.AcctNo); }
			set { SetColumnValue(Columns.AcctNo, value); }
		}
		  
		[XmlAttribute("AcctPw")]
		[Bindable(true)]
		public string AcctPw 
		{
			get { return GetColumnValue<string>(Columns.AcctPw); }
			set { SetColumnValue(Columns.AcctPw, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status 
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("ImeiCode")]
		[Bindable(true)]
		public string ImeiCode 
		{
			get { return GetColumnValue<string>(Columns.ImeiCode); }
			set { SetColumnValue(Columns.ImeiCode, value); }
		}
		  
		[XmlAttribute("SimCode")]
		[Bindable(true)]
		public string SimCode 
		{
			get { return GetColumnValue<string>(Columns.SimCode); }
			set { SetColumnValue(Columns.SimCode, value); }
		}
		  
		[XmlAttribute("RegisterCode")]
		[Bindable(true)]
		public string RegisterCode 
		{
			get { return GetColumnValue<string>(Columns.RegisterCode); }
			set { SetColumnValue(Columns.RegisterCode, value); }
		}
		  
		[XmlAttribute("NewRegisterCode")]
		[Bindable(true)]
		public string NewRegisterCode 
		{
			get { return GetColumnValue<string>(Columns.NewRegisterCode); }
			set { SetColumnValue(Columns.NewRegisterCode, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AcctCodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AcctNoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AcctPwColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ImeiCodeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn SimCodeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn RegisterCodeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn NewRegisterCodeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SellerGuid = @"seller_guid";
			 public static string StoreGuid = @"store_guid";
			 public static string AcctCode = @"acct_code";
			 public static string AcctNo = @"acct_no";
			 public static string AcctPw = @"acct_pw";
			 public static string Status = @"status";
			 public static string ImeiCode = @"imei_code";
			 public static string SimCode = @"sim_code";
			 public static string RegisterCode = @"register_code";
			 public static string NewRegisterCode = @"new_register_code";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewCouponListMainCollection : ReadOnlyList<ViewCouponListMain, ViewCouponListMainCollection>
	{
			public ViewCouponListMainCollection() {}

	}

	[Serializable]
	public partial class ViewCouponListMain : ReadOnlyRecord<ViewCouponListMain>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewCouponListMain()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewCouponListMain(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewCouponListMain(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewCouponListMain(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_coupon_list_main", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = true;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);

				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);

				TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
				colvarOrderStatus.ColumnName = "order_status";
				colvarOrderStatus.DataType = DbType.Int32;
				colvarOrderStatus.MaxLength = 0;
				colvarOrderStatus.AutoIncrement = false;
				colvarOrderStatus.IsNullable = false;
				colvarOrderStatus.IsPrimaryKey = false;
				colvarOrderStatus.IsForeignKey = false;
				colvarOrderStatus.IsReadOnly = false;
				colvarOrderStatus.DefaultSetting = @"";
				colvarOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStatus);

				TableSchema.TableColumn colvarMemberEmail = new TableSchema.TableColumn(schema);
				colvarMemberEmail.ColumnName = "member_email";
				colvarMemberEmail.DataType = DbType.String;
				colvarMemberEmail.MaxLength = 256;
				colvarMemberEmail.AutoIncrement = false;
				colvarMemberEmail.IsNullable = false;
				colvarMemberEmail.IsPrimaryKey = false;
				colvarMemberEmail.IsForeignKey = false;
				colvarMemberEmail.IsReadOnly = false;
				colvarMemberEmail.DefaultSetting = @"";
				colvarMemberEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberEmail);

				TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
				colvarUniqueId.ColumnName = "unique_id";
				colvarUniqueId.DataType = DbType.Int32;
				colvarUniqueId.MaxLength = 0;
				colvarUniqueId.AutoIncrement = false;
				colvarUniqueId.IsNullable = false;
				colvarUniqueId.IsPrimaryKey = false;
				colvarUniqueId.IsForeignKey = false;
				colvarUniqueId.IsReadOnly = false;
				colvarUniqueId.DefaultSetting = @"";
				colvarUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniqueId);

				TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
				colvarSubtotal.ColumnName = "subtotal";
				colvarSubtotal.DataType = DbType.Currency;
				colvarSubtotal.MaxLength = 0;
				colvarSubtotal.AutoIncrement = false;
				colvarSubtotal.IsNullable = false;
				colvarSubtotal.IsPrimaryKey = false;
				colvarSubtotal.IsForeignKey = false;
				colvarSubtotal.IsReadOnly = false;
				colvarSubtotal.DefaultSetting = @"";
				colvarSubtotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubtotal);

				TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
				colvarTotal.ColumnName = "total";
				colvarTotal.DataType = DbType.Currency;
				colvarTotal.MaxLength = 0;
				colvarTotal.AutoIncrement = false;
				colvarTotal.IsNullable = false;
				colvarTotal.IsPrimaryKey = false;
				colvarTotal.IsForeignKey = false;
				colvarTotal.IsReadOnly = false;
				colvarTotal.DefaultSetting = @"";
				colvarTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotal);

				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);

				TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
				colvarSlug.ColumnName = "slug";
				colvarSlug.DataType = DbType.String;
				colvarSlug.MaxLength = 100;
				colvarSlug.AutoIncrement = false;
				colvarSlug.IsNullable = false;
				colvarSlug.IsPrimaryKey = false;
				colvarSlug.IsForeignKey = false;
				colvarSlug.IsReadOnly = false;
				colvarSlug.DefaultSetting = @"";
				colvarSlug.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSlug);

				TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
				colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeS.MaxLength = 0;
				colvarBusinessHourOrderTimeS.AutoIncrement = false;
				colvarBusinessHourOrderTimeS.IsNullable = false;
				colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeS.IsForeignKey = false;
				colvarBusinessHourOrderTimeS.IsReadOnly = false;
				colvarBusinessHourOrderTimeS.DefaultSetting = @"";
				colvarBusinessHourOrderTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeS);

				TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
				colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeE.MaxLength = 0;
				colvarBusinessHourOrderTimeE.AutoIncrement = false;
				colvarBusinessHourOrderTimeE.IsNullable = false;
				colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeE.IsForeignKey = false;
				colvarBusinessHourOrderTimeE.IsReadOnly = false;
				colvarBusinessHourOrderTimeE.DefaultSetting = @"";
				colvarBusinessHourOrderTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeE);

				TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
				colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeE.MaxLength = 0;
				colvarBusinessHourDeliverTimeE.AutoIncrement = false;
				colvarBusinessHourDeliverTimeE.IsNullable = true;
				colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeE.IsForeignKey = false;
				colvarBusinessHourDeliverTimeE.IsReadOnly = false;
				colvarBusinessHourDeliverTimeE.DefaultSetting = @"";
				colvarBusinessHourDeliverTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeE);

				TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
				colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
				colvarBusinessHourOrderMinimum.MaxLength = 0;
				colvarBusinessHourOrderMinimum.AutoIncrement = false;
				colvarBusinessHourOrderMinimum.IsNullable = false;
				colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
				colvarBusinessHourOrderMinimum.IsForeignKey = false;
				colvarBusinessHourOrderMinimum.IsReadOnly = false;
				colvarBusinessHourOrderMinimum.DefaultSetting = @"";
				colvarBusinessHourOrderMinimum.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderMinimum);

				TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
				colvarItemOrigPrice.ColumnName = "item_orig_price";
				colvarItemOrigPrice.DataType = DbType.Currency;
				colvarItemOrigPrice.MaxLength = 0;
				colvarItemOrigPrice.AutoIncrement = false;
				colvarItemOrigPrice.IsNullable = false;
				colvarItemOrigPrice.IsPrimaryKey = false;
				colvarItemOrigPrice.IsForeignKey = false;
				colvarItemOrigPrice.IsReadOnly = false;
				colvarItemOrigPrice.DefaultSetting = @"";
				colvarItemOrigPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemOrigPrice);

				TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
				colvarItemPrice.ColumnName = "item_price";
				colvarItemPrice.DataType = DbType.Currency;
				colvarItemPrice.MaxLength = 0;
				colvarItemPrice.AutoIncrement = false;
				colvarItemPrice.IsNullable = false;
				colvarItemPrice.IsPrimaryKey = false;
				colvarItemPrice.IsForeignKey = false;
				colvarItemPrice.IsReadOnly = false;
				colvarItemPrice.DefaultSetting = @"";
				colvarItemPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemPrice);

				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 750;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = false;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
				colvarDepartment.ColumnName = "department";
				colvarDepartment.DataType = DbType.Int32;
				colvarDepartment.MaxLength = 0;
				colvarDepartment.AutoIncrement = false;
				colvarDepartment.IsNullable = false;
				colvarDepartment.IsPrimaryKey = false;
				colvarDepartment.IsForeignKey = false;
				colvarDepartment.IsReadOnly = false;
				colvarDepartment.DefaultSetting = @"";
				colvarDepartment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDepartment);

				TableSchema.TableColumn colvarExchangeStatus = new TableSchema.TableColumn(schema);
				colvarExchangeStatus.ColumnName = "exchange_status";
				colvarExchangeStatus.DataType = DbType.Int32;
				colvarExchangeStatus.MaxLength = 0;
				colvarExchangeStatus.AutoIncrement = false;
				colvarExchangeStatus.IsNullable = false;
				colvarExchangeStatus.IsPrimaryKey = false;
				colvarExchangeStatus.IsForeignKey = false;
				colvarExchangeStatus.IsReadOnly = false;
				colvarExchangeStatus.DefaultSetting = @"";
				colvarExchangeStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExchangeStatus);

				TableSchema.TableColumn colvarExchangeModifyTime = new TableSchema.TableColumn(schema);
				colvarExchangeModifyTime.ColumnName = "exchange_modify_time";
				colvarExchangeModifyTime.DataType = DbType.DateTime;
				colvarExchangeModifyTime.MaxLength = 0;
				colvarExchangeModifyTime.AutoIncrement = false;
				colvarExchangeModifyTime.IsNullable = true;
				colvarExchangeModifyTime.IsPrimaryKey = false;
				colvarExchangeModifyTime.IsForeignKey = false;
				colvarExchangeModifyTime.IsReadOnly = false;
				colvarExchangeModifyTime.DefaultSetting = @"";
				colvarExchangeModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExchangeModifyTime);

				TableSchema.TableColumn colvarRemainCount = new TableSchema.TableColumn(schema);
				colvarRemainCount.ColumnName = "remain_count";
				colvarRemainCount.DataType = DbType.Int32;
				colvarRemainCount.MaxLength = 0;
				colvarRemainCount.AutoIncrement = false;
				colvarRemainCount.IsNullable = true;
				colvarRemainCount.IsPrimaryKey = false;
				colvarRemainCount.IsForeignKey = false;
				colvarRemainCount.IsReadOnly = false;
				colvarRemainCount.DefaultSetting = @"";
				colvarRemainCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemainCount);

				TableSchema.TableColumn colvarTotalCount = new TableSchema.TableColumn(schema);
				colvarTotalCount.ColumnName = "total_count";
				colvarTotalCount.DataType = DbType.Int32;
				colvarTotalCount.MaxLength = 0;
				colvarTotalCount.AutoIncrement = false;
				colvarTotalCount.IsNullable = true;
				colvarTotalCount.IsPrimaryKey = false;
				colvarTotalCount.IsForeignKey = false;
				colvarTotalCount.IsReadOnly = false;
				colvarTotalCount.DefaultSetting = @"";
				colvarTotalCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalCount);

				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);

				TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
				colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeS.MaxLength = 0;
				colvarBusinessHourDeliverTimeS.AutoIncrement = false;
				colvarBusinessHourDeliverTimeS.IsNullable = true;
				colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeS.IsForeignKey = false;
				colvarBusinessHourDeliverTimeS.IsReadOnly = false;
				colvarBusinessHourDeliverTimeS.DefaultSetting = @"";
				colvarBusinessHourDeliverTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeS);

				TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
				colvarBusinessHourStatus.ColumnName = "business_hour_status";
				colvarBusinessHourStatus.DataType = DbType.Int32;
				colvarBusinessHourStatus.MaxLength = 0;
				colvarBusinessHourStatus.AutoIncrement = false;
				colvarBusinessHourStatus.IsNullable = false;
				colvarBusinessHourStatus.IsPrimaryKey = false;
				colvarBusinessHourStatus.IsForeignKey = false;
				colvarBusinessHourStatus.IsReadOnly = false;
				colvarBusinessHourStatus.DefaultSetting = @"";
				colvarBusinessHourStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourStatus);

				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = true;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);

				TableSchema.TableColumn colvarShoppingCart = new TableSchema.TableColumn(schema);
				colvarShoppingCart.ColumnName = "shopping_cart";
				colvarShoppingCart.DataType = DbType.Boolean;
				colvarShoppingCart.MaxLength = 0;
				colvarShoppingCart.AutoIncrement = false;
				colvarShoppingCart.IsNullable = false;
				colvarShoppingCart.IsPrimaryKey = false;
				colvarShoppingCart.IsForeignKey = false;
				colvarShoppingCart.IsReadOnly = false;
				colvarShoppingCart.DefaultSetting = @"";
				colvarShoppingCart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShoppingCart);

				TableSchema.TableColumn colvarComboPackCount = new TableSchema.TableColumn(schema);
				colvarComboPackCount.ColumnName = "combo_pack_count";
				colvarComboPackCount.DataType = DbType.Int32;
				colvarComboPackCount.MaxLength = 0;
				colvarComboPackCount.AutoIncrement = false;
				colvarComboPackCount.IsNullable = false;
				colvarComboPackCount.IsPrimaryKey = false;
				colvarComboPackCount.IsForeignKey = false;
				colvarComboPackCount.IsReadOnly = false;
				colvarComboPackCount.DefaultSetting = @"";
				colvarComboPackCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarComboPackCount);

				TableSchema.TableColumn colvarPrintCount = new TableSchema.TableColumn(schema);
				colvarPrintCount.ColumnName = "print_count";
				colvarPrintCount.DataType = DbType.Int32;
				colvarPrintCount.MaxLength = 0;
				colvarPrintCount.AutoIncrement = false;
				colvarPrintCount.IsNullable = true;
				colvarPrintCount.IsPrimaryKey = false;
				colvarPrintCount.IsForeignKey = false;
				colvarPrintCount.IsReadOnly = false;
				colvarPrintCount.DefaultSetting = @"";
				colvarPrintCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrintCount);

				TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
				colvarCouponUsage.ColumnName = "coupon_usage";
				colvarCouponUsage.DataType = DbType.String;
				colvarCouponUsage.MaxLength = 500;
				colvarCouponUsage.AutoIncrement = false;
				colvarCouponUsage.IsNullable = true;
				colvarCouponUsage.IsPrimaryKey = false;
				colvarCouponUsage.IsForeignKey = false;
				colvarCouponUsage.IsReadOnly = false;
				colvarCouponUsage.DefaultSetting = @"";
				colvarCouponUsage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponUsage);

				TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
				colvarImagePath.ColumnName = "image_path";
				colvarImagePath.DataType = DbType.String;
				colvarImagePath.MaxLength = 1000;
				colvarImagePath.AutoIncrement = false;
				colvarImagePath.IsNullable = true;
				colvarImagePath.IsPrimaryKey = false;
				colvarImagePath.IsForeignKey = false;
				colvarImagePath.IsReadOnly = false;
				colvarImagePath.DefaultSetting = @"";
				colvarImagePath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImagePath);

				TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
				colvarChangedExpireDate.ColumnName = "changed_expire_date";
				colvarChangedExpireDate.DataType = DbType.DateTime;
				colvarChangedExpireDate.MaxLength = 0;
				colvarChangedExpireDate.AutoIncrement = false;
				colvarChangedExpireDate.IsNullable = true;
				colvarChangedExpireDate.IsPrimaryKey = false;
				colvarChangedExpireDate.IsForeignKey = false;
				colvarChangedExpireDate.IsReadOnly = false;
				colvarChangedExpireDate.DefaultSetting = @"";
				colvarChangedExpireDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChangedExpireDate);

				TableSchema.TableColumn colvarCouponCodeType = new TableSchema.TableColumn(schema);
				colvarCouponCodeType.ColumnName = "coupon_code_type";
				colvarCouponCodeType.DataType = DbType.Int32;
				colvarCouponCodeType.MaxLength = 0;
				colvarCouponCodeType.AutoIncrement = false;
				colvarCouponCodeType.IsNullable = false;
				colvarCouponCodeType.IsPrimaryKey = false;
				colvarCouponCodeType.IsForeignKey = false;
				colvarCouponCodeType.IsReadOnly = false;
				colvarCouponCodeType.DefaultSetting = @"";
				colvarCouponCodeType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponCodeType);

				TableSchema.TableColumn colvarPinType = new TableSchema.TableColumn(schema);
				colvarPinType.ColumnName = "pin_type";
				colvarPinType.DataType = DbType.Int32;
				colvarPinType.MaxLength = 0;
				colvarPinType.AutoIncrement = false;
				colvarPinType.IsNullable = false;
				colvarPinType.IsPrimaryKey = false;
				colvarPinType.IsForeignKey = false;
				colvarPinType.IsReadOnly = false;
				colvarPinType.DefaultSetting = @"";
				colvarPinType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPinType);

				TableSchema.TableColumn colvarBookingSystemType = new TableSchema.TableColumn(schema);
				colvarBookingSystemType.ColumnName = "booking_system_type";
				colvarBookingSystemType.DataType = DbType.Int32;
				colvarBookingSystemType.MaxLength = 0;
				colvarBookingSystemType.AutoIncrement = false;
				colvarBookingSystemType.IsNullable = false;
				colvarBookingSystemType.IsPrimaryKey = false;
				colvarBookingSystemType.IsForeignKey = false;
				colvarBookingSystemType.IsReadOnly = false;
				colvarBookingSystemType.DefaultSetting = @"";
				colvarBookingSystemType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBookingSystemType);

				TableSchema.TableColumn colvarReturnStatus = new TableSchema.TableColumn(schema);
				colvarReturnStatus.ColumnName = "return_status";
				colvarReturnStatus.DataType = DbType.Int32;
				colvarReturnStatus.MaxLength = 0;
				colvarReturnStatus.AutoIncrement = false;
				colvarReturnStatus.IsNullable = false;
				colvarReturnStatus.IsPrimaryKey = false;
				colvarReturnStatus.IsForeignKey = false;
				colvarReturnStatus.IsReadOnly = false;
				colvarReturnStatus.DefaultSetting = @"";
				colvarReturnStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnStatus);

				TableSchema.TableColumn colvarReturnModifyTime = new TableSchema.TableColumn(schema);
				colvarReturnModifyTime.ColumnName = "return_modify_time";
				colvarReturnModifyTime.DataType = DbType.DateTime;
				colvarReturnModifyTime.MaxLength = 0;
				colvarReturnModifyTime.AutoIncrement = false;
				colvarReturnModifyTime.IsNullable = true;
				colvarReturnModifyTime.IsPrimaryKey = false;
				colvarReturnModifyTime.IsForeignKey = false;
				colvarReturnModifyTime.IsReadOnly = false;
				colvarReturnModifyTime.DefaultSetting = @"";
				colvarReturnModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnModifyTime);

				TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
				colvarAppTitle.ColumnName = "app_title";
				colvarAppTitle.DataType = DbType.String;
				colvarAppTitle.MaxLength = 120;
				colvarAppTitle.AutoIncrement = false;
				colvarAppTitle.IsNullable = true;
				colvarAppTitle.IsPrimaryKey = false;
				colvarAppTitle.IsForeignKey = false;
				colvarAppTitle.IsReadOnly = false;
				colvarAppTitle.DefaultSetting = @"";
				colvarAppTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppTitle);

				TableSchema.TableColumn colvarShipTime = new TableSchema.TableColumn(schema);
				colvarShipTime.ColumnName = "ship_time";
				colvarShipTime.DataType = DbType.DateTime;
				colvarShipTime.MaxLength = 0;
				colvarShipTime.AutoIncrement = false;
				colvarShipTime.IsNullable = true;
				colvarShipTime.IsPrimaryKey = false;
				colvarShipTime.IsForeignKey = false;
				colvarShipTime.IsReadOnly = false;
				colvarShipTime.DefaultSetting = @"";
				colvarShipTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipTime);

				TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
				colvarShipType.ColumnName = "ship_type";
				colvarShipType.DataType = DbType.Int32;
				colvarShipType.MaxLength = 0;
				colvarShipType.AutoIncrement = false;
				colvarShipType.IsNullable = true;
				colvarShipType.IsPrimaryKey = false;
				colvarShipType.IsForeignKey = false;
				colvarShipType.IsReadOnly = false;
				colvarShipType.DefaultSetting = @"";
				colvarShipType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipType);

				TableSchema.TableColumn colvarShippingdateType = new TableSchema.TableColumn(schema);
				colvarShippingdateType.ColumnName = "shippingdate_type";
				colvarShippingdateType.DataType = DbType.Int32;
				colvarShippingdateType.MaxLength = 0;
				colvarShippingdateType.AutoIncrement = false;
				colvarShippingdateType.IsNullable = true;
				colvarShippingdateType.IsPrimaryKey = false;
				colvarShippingdateType.IsForeignKey = false;
				colvarShippingdateType.IsReadOnly = false;
				colvarShippingdateType.DefaultSetting = @"";
				colvarShippingdateType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShippingdateType);

				TableSchema.TableColumn colvarShippingdate = new TableSchema.TableColumn(schema);
				colvarShippingdate.ColumnName = "shippingdate";
				colvarShippingdate.DataType = DbType.Int32;
				colvarShippingdate.MaxLength = 0;
				colvarShippingdate.AutoIncrement = false;
				colvarShippingdate.IsNullable = true;
				colvarShippingdate.IsPrimaryKey = false;
				colvarShippingdate.IsForeignKey = false;
				colvarShippingdate.IsReadOnly = false;
				colvarShippingdate.DefaultSetting = @"";
				colvarShippingdate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShippingdate);

				TableSchema.TableColumn colvarProductUseDateEndSet = new TableSchema.TableColumn(schema);
				colvarProductUseDateEndSet.ColumnName = "product_use_date_end_set";
				colvarProductUseDateEndSet.DataType = DbType.Int32;
				colvarProductUseDateEndSet.MaxLength = 0;
				colvarProductUseDateEndSet.AutoIncrement = false;
				colvarProductUseDateEndSet.IsNullable = true;
				colvarProductUseDateEndSet.IsPrimaryKey = false;
				colvarProductUseDateEndSet.IsForeignKey = false;
				colvarProductUseDateEndSet.IsReadOnly = false;
				colvarProductUseDateEndSet.DefaultSetting = @"";
				colvarProductUseDateEndSet.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductUseDateEndSet);

				TableSchema.TableColumn colvarGroupProductUnitPrice = new TableSchema.TableColumn(schema);
				colvarGroupProductUnitPrice.ColumnName = "group_product_unit_price";
				colvarGroupProductUnitPrice.DataType = DbType.Int32;
				colvarGroupProductUnitPrice.MaxLength = 0;
				colvarGroupProductUnitPrice.AutoIncrement = false;
				colvarGroupProductUnitPrice.IsNullable = true;
				colvarGroupProductUnitPrice.IsPrimaryKey = false;
				colvarGroupProductUnitPrice.IsForeignKey = false;
				colvarGroupProductUnitPrice.IsReadOnly = false;
				colvarGroupProductUnitPrice.DefaultSetting = @"";
				colvarGroupProductUnitPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupProductUnitPrice);

				TableSchema.TableColumn colvarGroupCouponAppStyle = new TableSchema.TableColumn(schema);
				colvarGroupCouponAppStyle.ColumnName = "group_coupon_app_style";
				colvarGroupCouponAppStyle.DataType = DbType.Int32;
				colvarGroupCouponAppStyle.MaxLength = 0;
				colvarGroupCouponAppStyle.AutoIncrement = false;
				colvarGroupCouponAppStyle.IsNullable = false;
				colvarGroupCouponAppStyle.IsPrimaryKey = false;
				colvarGroupCouponAppStyle.IsForeignKey = false;
				colvarGroupCouponAppStyle.IsReadOnly = false;
				colvarGroupCouponAppStyle.DefaultSetting = @"";
				colvarGroupCouponAppStyle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupCouponAppStyle);

				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 400;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = true;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);

				TableSchema.TableColumn colvarConsignment = new TableSchema.TableColumn(schema);
				colvarConsignment.ColumnName = "consignment";
				colvarConsignment.DataType = DbType.Boolean;
				colvarConsignment.MaxLength = 0;
				colvarConsignment.AutoIncrement = false;
				colvarConsignment.IsNullable = false;
				colvarConsignment.IsPrimaryKey = false;
				colvarConsignment.IsForeignKey = false;
				colvarConsignment.IsReadOnly = false;
				colvarConsignment.DefaultSetting = @"";
				colvarConsignment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsignment);

				TableSchema.TableColumn colvarGroupOrderStatus = new TableSchema.TableColumn(schema);
				colvarGroupOrderStatus.ColumnName = "group_order_status";
				colvarGroupOrderStatus.DataType = DbType.Int32;
				colvarGroupOrderStatus.MaxLength = 0;
				colvarGroupOrderStatus.AutoIncrement = false;
				colvarGroupOrderStatus.IsNullable = true;
				colvarGroupOrderStatus.IsPrimaryKey = false;
				colvarGroupOrderStatus.IsForeignKey = false;
				colvarGroupOrderStatus.IsReadOnly = false;
				colvarGroupOrderStatus.DefaultSetting = @"";
				colvarGroupOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupOrderStatus);

				TableSchema.TableColumn colvarIsDepositCoffee = new TableSchema.TableColumn(schema);
				colvarIsDepositCoffee.ColumnName = "is_deposit_coffee";
				colvarIsDepositCoffee.DataType = DbType.Boolean;
				colvarIsDepositCoffee.MaxLength = 0;
				colvarIsDepositCoffee.AutoIncrement = false;
				colvarIsDepositCoffee.IsNullable = false;
				colvarIsDepositCoffee.IsPrimaryKey = false;
				colvarIsDepositCoffee.IsForeignKey = false;
				colvarIsDepositCoffee.IsReadOnly = false;
				colvarIsDepositCoffee.DefaultSetting = @"";
				colvarIsDepositCoffee.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDepositCoffee);

				TableSchema.TableColumn colvarProductDeliveryType = new TableSchema.TableColumn(schema);
				colvarProductDeliveryType.ColumnName = "product_delivery_type";
				colvarProductDeliveryType.DataType = DbType.Int32;
				colvarProductDeliveryType.MaxLength = 0;
				colvarProductDeliveryType.AutoIncrement = false;
				colvarProductDeliveryType.IsNullable = false;
				colvarProductDeliveryType.IsPrimaryKey = false;
				colvarProductDeliveryType.IsForeignKey = false;
				colvarProductDeliveryType.IsReadOnly = false;
				colvarProductDeliveryType.DefaultSetting = @"";
				colvarProductDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductDeliveryType);

				TableSchema.TableColumn colvarLastShipDate = new TableSchema.TableColumn(schema);
				colvarLastShipDate.ColumnName = "last_ship_date";
				colvarLastShipDate.DataType = DbType.DateTime;
				colvarLastShipDate.MaxLength = 0;
				colvarLastShipDate.AutoIncrement = false;
				colvarLastShipDate.IsNullable = true;
				colvarLastShipDate.IsPrimaryKey = false;
				colvarLastShipDate.IsForeignKey = false;
				colvarLastShipDate.IsReadOnly = false;
				colvarLastShipDate.DefaultSetting = @"";
				colvarLastShipDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastShipDate);

				TableSchema.TableColumn colvarGoodsStatus = new TableSchema.TableColumn(schema);
				colvarGoodsStatus.ColumnName = "goods_status";
				colvarGoodsStatus.DataType = DbType.Int32;
				colvarGoodsStatus.MaxLength = 0;
				colvarGoodsStatus.AutoIncrement = false;
				colvarGoodsStatus.IsNullable = true;
				colvarGoodsStatus.IsPrimaryKey = false;
				colvarGoodsStatus.IsForeignKey = false;
				colvarGoodsStatus.IsReadOnly = false;
				colvarGoodsStatus.DefaultSetting = @"";
				colvarGoodsStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGoodsStatus);

				TableSchema.TableColumn colvarSellerGoodsStatus = new TableSchema.TableColumn(schema);
				colvarSellerGoodsStatus.ColumnName = "seller_goods_status";
				colvarSellerGoodsStatus.DataType = DbType.Int32;
				colvarSellerGoodsStatus.MaxLength = 0;
				colvarSellerGoodsStatus.AutoIncrement = false;
				colvarSellerGoodsStatus.IsNullable = true;
				colvarSellerGoodsStatus.IsPrimaryKey = false;
				colvarSellerGoodsStatus.IsForeignKey = false;
				colvarSellerGoodsStatus.IsReadOnly = false;
				colvarSellerGoodsStatus.DefaultSetting = @"";
				colvarSellerGoodsStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGoodsStatus);

				TableSchema.TableColumn colvarFamilyStoreId = new TableSchema.TableColumn(schema);
				colvarFamilyStoreId.ColumnName = "family_store_id";
				colvarFamilyStoreId.DataType = DbType.String;
				colvarFamilyStoreId.MaxLength = 10;
				colvarFamilyStoreId.AutoIncrement = false;
				colvarFamilyStoreId.IsNullable = true;
				colvarFamilyStoreId.IsPrimaryKey = false;
				colvarFamilyStoreId.IsForeignKey = false;
				colvarFamilyStoreId.IsReadOnly = false;
				colvarFamilyStoreId.DefaultSetting = @"";
				colvarFamilyStoreId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamilyStoreId);

				TableSchema.TableColumn colvarFamilyStoreName = new TableSchema.TableColumn(schema);
				colvarFamilyStoreName.ColumnName = "family_store_name";
				colvarFamilyStoreName.DataType = DbType.String;
				colvarFamilyStoreName.MaxLength = 50;
				colvarFamilyStoreName.AutoIncrement = false;
				colvarFamilyStoreName.IsNullable = true;
				colvarFamilyStoreName.IsPrimaryKey = false;
				colvarFamilyStoreName.IsForeignKey = false;
				colvarFamilyStoreName.IsReadOnly = false;
				colvarFamilyStoreName.DefaultSetting = @"";
				colvarFamilyStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamilyStoreName);

				TableSchema.TableColumn colvarFamilyStoreTel = new TableSchema.TableColumn(schema);
				colvarFamilyStoreTel.ColumnName = "family_store_tel";
				colvarFamilyStoreTel.DataType = DbType.String;
				colvarFamilyStoreTel.MaxLength = 50;
				colvarFamilyStoreTel.AutoIncrement = false;
				colvarFamilyStoreTel.IsNullable = true;
				colvarFamilyStoreTel.IsPrimaryKey = false;
				colvarFamilyStoreTel.IsForeignKey = false;
				colvarFamilyStoreTel.IsReadOnly = false;
				colvarFamilyStoreTel.DefaultSetting = @"";
				colvarFamilyStoreTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamilyStoreTel);

				TableSchema.TableColumn colvarFamilyStoreAddr = new TableSchema.TableColumn(schema);
				colvarFamilyStoreAddr.ColumnName = "family_store_addr";
				colvarFamilyStoreAddr.DataType = DbType.String;
				colvarFamilyStoreAddr.MaxLength = 200;
				colvarFamilyStoreAddr.AutoIncrement = false;
				colvarFamilyStoreAddr.IsNullable = true;
				colvarFamilyStoreAddr.IsPrimaryKey = false;
				colvarFamilyStoreAddr.IsForeignKey = false;
				colvarFamilyStoreAddr.IsReadOnly = false;
				colvarFamilyStoreAddr.DefaultSetting = @"";
				colvarFamilyStoreAddr.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamilyStoreAddr);

				TableSchema.TableColumn colvarSevenStoreId = new TableSchema.TableColumn(schema);
				colvarSevenStoreId.ColumnName = "seven_store_id";
				colvarSevenStoreId.DataType = DbType.String;
				colvarSevenStoreId.MaxLength = 10;
				colvarSevenStoreId.AutoIncrement = false;
				colvarSevenStoreId.IsNullable = true;
				colvarSevenStoreId.IsPrimaryKey = false;
				colvarSevenStoreId.IsForeignKey = false;
				colvarSevenStoreId.IsReadOnly = false;
				colvarSevenStoreId.DefaultSetting = @"";
				colvarSevenStoreId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSevenStoreId);

				TableSchema.TableColumn colvarSevenStoreName = new TableSchema.TableColumn(schema);
				colvarSevenStoreName.ColumnName = "seven_store_name";
				colvarSevenStoreName.DataType = DbType.String;
				colvarSevenStoreName.MaxLength = 50;
				colvarSevenStoreName.AutoIncrement = false;
				colvarSevenStoreName.IsNullable = true;
				colvarSevenStoreName.IsPrimaryKey = false;
				colvarSevenStoreName.IsForeignKey = false;
				colvarSevenStoreName.IsReadOnly = false;
				colvarSevenStoreName.DefaultSetting = @"";
				colvarSevenStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSevenStoreName);

				TableSchema.TableColumn colvarSevenStoreTel = new TableSchema.TableColumn(schema);
				colvarSevenStoreTel.ColumnName = "seven_store_tel";
				colvarSevenStoreTel.DataType = DbType.String;
				colvarSevenStoreTel.MaxLength = 50;
				colvarSevenStoreTel.AutoIncrement = false;
				colvarSevenStoreTel.IsNullable = true;
				colvarSevenStoreTel.IsPrimaryKey = false;
				colvarSevenStoreTel.IsForeignKey = false;
				colvarSevenStoreTel.IsReadOnly = false;
				colvarSevenStoreTel.DefaultSetting = @"";
				colvarSevenStoreTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSevenStoreTel);

				TableSchema.TableColumn colvarSevenStoreAddr = new TableSchema.TableColumn(schema);
				colvarSevenStoreAddr.ColumnName = "seven_store_addr";
				colvarSevenStoreAddr.DataType = DbType.String;
				colvarSevenStoreAddr.MaxLength = 200;
				colvarSevenStoreAddr.AutoIncrement = false;
				colvarSevenStoreAddr.IsNullable = true;
				colvarSevenStoreAddr.IsPrimaryKey = false;
				colvarSevenStoreAddr.IsForeignKey = false;
				colvarSevenStoreAddr.IsReadOnly = false;
				colvarSevenStoreAddr.DefaultSetting = @"";
				colvarSevenStoreAddr.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSevenStoreAddr);

				TableSchema.TableColumn colvarPreShipNo = new TableSchema.TableColumn(schema);
				colvarPreShipNo.ColumnName = "pre_ship_no";
				colvarPreShipNo.DataType = DbType.AnsiString;
				colvarPreShipNo.MaxLength = 50;
				colvarPreShipNo.AutoIncrement = false;
				colvarPreShipNo.IsNullable = false;
				colvarPreShipNo.IsPrimaryKey = false;
				colvarPreShipNo.IsForeignKey = false;
				colvarPreShipNo.IsReadOnly = false;
				colvarPreShipNo.DefaultSetting = @"";
				colvarPreShipNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPreShipNo);

				TableSchema.TableColumn colvarIsReceipt = new TableSchema.TableColumn(schema);
				colvarIsReceipt.ColumnName = "is_receipt";
				colvarIsReceipt.DataType = DbType.Boolean;
				colvarIsReceipt.MaxLength = 0;
				colvarIsReceipt.AutoIncrement = false;
				colvarIsReceipt.IsNullable = true;
				colvarIsReceipt.IsPrimaryKey = false;
				colvarIsReceipt.IsForeignKey = false;
				colvarIsReceipt.IsReadOnly = false;
				colvarIsReceipt.DefaultSetting = @"";
				colvarIsReceipt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReceipt);

				TableSchema.TableColumn colvarAppDealPic = new TableSchema.TableColumn(schema);
				colvarAppDealPic.ColumnName = "app_deal_pic";
				colvarAppDealPic.DataType = DbType.String;
				colvarAppDealPic.MaxLength = 200;
				colvarAppDealPic.AutoIncrement = false;
				colvarAppDealPic.IsNullable = true;
				colvarAppDealPic.IsPrimaryKey = false;
				colvarAppDealPic.IsForeignKey = false;
				colvarAppDealPic.IsReadOnly = false;
				colvarAppDealPic.DefaultSetting = @"";
				colvarAppDealPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppDealPic);

				TableSchema.TableColumn colvarIsCanceling = new TableSchema.TableColumn(schema);
				colvarIsCanceling.ColumnName = "is_canceling";
				colvarIsCanceling.DataType = DbType.Boolean;
				colvarIsCanceling.MaxLength = 0;
				colvarIsCanceling.AutoIncrement = false;
				colvarIsCanceling.IsNullable = false;
				colvarIsCanceling.IsPrimaryKey = false;
				colvarIsCanceling.IsForeignKey = false;
				colvarIsCanceling.IsReadOnly = false;
				colvarIsCanceling.DefaultSetting = @"";
				colvarIsCanceling.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCanceling);

				TableSchema.TableColumn colvarIsWms = new TableSchema.TableColumn(schema);
				colvarIsWms.ColumnName = "is_wms";
				colvarIsWms.DataType = DbType.Boolean;
				colvarIsWms.MaxLength = 0;
				colvarIsWms.AutoIncrement = false;
				colvarIsWms.IsNullable = false;
				colvarIsWms.IsPrimaryKey = false;
				colvarIsWms.IsForeignKey = false;
				colvarIsWms.IsReadOnly = false;
				colvarIsWms.DefaultSetting = @"";
				colvarIsWms.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsWms);

				TableSchema.TableColumn colvarWmsOrderShipStatus = new TableSchema.TableColumn(schema);
				colvarWmsOrderShipStatus.ColumnName = "wms_order_ship_status";
				colvarWmsOrderShipStatus.DataType = DbType.Int32;
				colvarWmsOrderShipStatus.MaxLength = 0;
				colvarWmsOrderShipStatus.AutoIncrement = false;
				colvarWmsOrderShipStatus.IsNullable = false;
				colvarWmsOrderShipStatus.IsPrimaryKey = false;
				colvarWmsOrderShipStatus.IsForeignKey = false;
				colvarWmsOrderShipStatus.IsReadOnly = false;
				colvarWmsOrderShipStatus.DefaultSetting = @"";
				colvarWmsOrderShipStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWmsOrderShipStatus);

				TableSchema.TableColumn colvarWmsOrderShipDate = new TableSchema.TableColumn(schema);
				colvarWmsOrderShipDate.ColumnName = "wms_order_ship_date";
				colvarWmsOrderShipDate.DataType = DbType.DateTime;
				colvarWmsOrderShipDate.MaxLength = 0;
				colvarWmsOrderShipDate.AutoIncrement = false;
				colvarWmsOrderShipDate.IsNullable = true;
				colvarWmsOrderShipDate.IsPrimaryKey = false;
				colvarWmsOrderShipDate.IsForeignKey = false;
				colvarWmsOrderShipDate.IsReadOnly = false;
				colvarWmsOrderShipDate.DefaultSetting = @"";
				colvarWmsOrderShipDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWmsOrderShipDate);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_coupon_list_main",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}

		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}

		[XmlAttribute("OrderStatus")]
		[Bindable(true)]
		public int OrderStatus
		{
			get { return GetColumnValue<int>(Columns.OrderStatus); }
			set { SetColumnValue(Columns.OrderStatus, value); }
		}

		[XmlAttribute("MemberEmail")]
		[Bindable(true)]
		public string MemberEmail
		{
			get { return GetColumnValue<string>(Columns.MemberEmail); }
			set { SetColumnValue(Columns.MemberEmail, value); }
		}

		[XmlAttribute("UniqueId")]
		[Bindable(true)]
		public int UniqueId
		{
			get { return GetColumnValue<int>(Columns.UniqueId); }
			set { SetColumnValue(Columns.UniqueId, value); }
		}

		[XmlAttribute("Subtotal")]
		[Bindable(true)]
		public decimal Subtotal
		{
			get { return GetColumnValue<decimal>(Columns.Subtotal); }
			set { SetColumnValue(Columns.Subtotal, value); }
		}

		[XmlAttribute("Total")]
		[Bindable(true)]
		public decimal Total
		{
			get { return GetColumnValue<decimal>(Columns.Total); }
			set { SetColumnValue(Columns.Total, value); }
		}

		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}

		[XmlAttribute("Slug")]
		[Bindable(true)]
		public string Slug
		{
			get { return GetColumnValue<string>(Columns.Slug); }
			set { SetColumnValue(Columns.Slug, value); }
		}

		[XmlAttribute("BusinessHourOrderTimeS")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeS
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeS); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeS, value); }
		}

		[XmlAttribute("BusinessHourOrderTimeE")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeE
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeE); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeE, value); }
		}

		[XmlAttribute("BusinessHourDeliverTimeE")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeE
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeE); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeE, value); }
		}

		[XmlAttribute("BusinessHourOrderMinimum")]
		[Bindable(true)]
		public decimal BusinessHourOrderMinimum
		{
			get { return GetColumnValue<decimal>(Columns.BusinessHourOrderMinimum); }
			set { SetColumnValue(Columns.BusinessHourOrderMinimum, value); }
		}

		[XmlAttribute("ItemOrigPrice")]
		[Bindable(true)]
		public decimal ItemOrigPrice
		{
			get { return GetColumnValue<decimal>(Columns.ItemOrigPrice); }
			set { SetColumnValue(Columns.ItemOrigPrice, value); }
		}

		[XmlAttribute("ItemPrice")]
		[Bindable(true)]
		public decimal ItemPrice
		{
			get { return GetColumnValue<decimal>(Columns.ItemPrice); }
			set { SetColumnValue(Columns.ItemPrice, value); }
		}

		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("Department")]
		[Bindable(true)]
		public int Department
		{
			get { return GetColumnValue<int>(Columns.Department); }
			set { SetColumnValue(Columns.Department, value); }
		}

		[XmlAttribute("ExchangeStatus")]
		[Bindable(true)]
		public int ExchangeStatus
		{
			get { return GetColumnValue<int>(Columns.ExchangeStatus); }
			set { SetColumnValue(Columns.ExchangeStatus, value); }
		}

		[XmlAttribute("ExchangeModifyTime")]
		[Bindable(true)]
		public DateTime? ExchangeModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ExchangeModifyTime); }
			set { SetColumnValue(Columns.ExchangeModifyTime, value); }
		}

		[XmlAttribute("RemainCount")]
		[Bindable(true)]
		public int? RemainCount
		{
			get { return GetColumnValue<int?>(Columns.RemainCount); }
			set { SetColumnValue(Columns.RemainCount, value); }
		}

		[XmlAttribute("TotalCount")]
		[Bindable(true)]
		public int? TotalCount
		{
			get { return GetColumnValue<int?>(Columns.TotalCount); }
			set { SetColumnValue(Columns.TotalCount, value); }
		}

		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}

		[XmlAttribute("BusinessHourDeliverTimeS")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeS
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeS); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeS, value); }
		}

		[XmlAttribute("BusinessHourStatus")]
		[Bindable(true)]
		public int BusinessHourStatus
		{
			get { return GetColumnValue<int>(Columns.BusinessHourStatus); }
			set { SetColumnValue(Columns.BusinessHourStatus, value); }
		}

		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int? DeliveryType
		{
			get { return GetColumnValue<int?>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}

		[XmlAttribute("ShoppingCart")]
		[Bindable(true)]
		public bool ShoppingCart
		{
			get { return GetColumnValue<bool>(Columns.ShoppingCart); }
			set { SetColumnValue(Columns.ShoppingCart, value); }
		}

		[XmlAttribute("ComboPackCount")]
		[Bindable(true)]
		public int ComboPackCount
		{
			get { return GetColumnValue<int>(Columns.ComboPackCount); }
			set { SetColumnValue(Columns.ComboPackCount, value); }
		}

		[XmlAttribute("PrintCount")]
		[Bindable(true)]
		public int? PrintCount
		{
			get { return GetColumnValue<int?>(Columns.PrintCount); }
			set { SetColumnValue(Columns.PrintCount, value); }
		}

		[XmlAttribute("CouponUsage")]
		[Bindable(true)]
		public string CouponUsage
		{
			get { return GetColumnValue<string>(Columns.CouponUsage); }
			set { SetColumnValue(Columns.CouponUsage, value); }
		}

		[XmlAttribute("ImagePath")]
		[Bindable(true)]
		public string ImagePath
		{
			get { return GetColumnValue<string>(Columns.ImagePath); }
			set { SetColumnValue(Columns.ImagePath, value); }
		}

		[XmlAttribute("ChangedExpireDate")]
		[Bindable(true)]
		public DateTime? ChangedExpireDate
		{
			get { return GetColumnValue<DateTime?>(Columns.ChangedExpireDate); }
			set { SetColumnValue(Columns.ChangedExpireDate, value); }
		}

		[XmlAttribute("CouponCodeType")]
		[Bindable(true)]
		public int CouponCodeType
		{
			get { return GetColumnValue<int>(Columns.CouponCodeType); }
			set { SetColumnValue(Columns.CouponCodeType, value); }
		}

		[XmlAttribute("PinType")]
		[Bindable(true)]
		public int PinType
		{
			get { return GetColumnValue<int>(Columns.PinType); }
			set { SetColumnValue(Columns.PinType, value); }
		}

		[XmlAttribute("BookingSystemType")]
		[Bindable(true)]
		public int BookingSystemType
		{
			get { return GetColumnValue<int>(Columns.BookingSystemType); }
			set { SetColumnValue(Columns.BookingSystemType, value); }
		}

		[XmlAttribute("ReturnStatus")]
		[Bindable(true)]
		public int ReturnStatus
		{
			get { return GetColumnValue<int>(Columns.ReturnStatus); }
			set { SetColumnValue(Columns.ReturnStatus, value); }
		}

		[XmlAttribute("ReturnModifyTime")]
		[Bindable(true)]
		public DateTime? ReturnModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ReturnModifyTime); }
			set { SetColumnValue(Columns.ReturnModifyTime, value); }
		}

		[XmlAttribute("AppTitle")]
		[Bindable(true)]
		public string AppTitle
		{
			get { return GetColumnValue<string>(Columns.AppTitle); }
			set { SetColumnValue(Columns.AppTitle, value); }
		}

		[XmlAttribute("ShipTime")]
		[Bindable(true)]
		public DateTime? ShipTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ShipTime); }
			set { SetColumnValue(Columns.ShipTime, value); }
		}

		[XmlAttribute("ShipType")]
		[Bindable(true)]
		public int? ShipType
		{
			get { return GetColumnValue<int?>(Columns.ShipType); }
			set { SetColumnValue(Columns.ShipType, value); }
		}

		[XmlAttribute("ShippingdateType")]
		[Bindable(true)]
		public int? ShippingdateType
		{
			get { return GetColumnValue<int?>(Columns.ShippingdateType); }
			set { SetColumnValue(Columns.ShippingdateType, value); }
		}

		[XmlAttribute("Shippingdate")]
		[Bindable(true)]
		public int? Shippingdate
		{
			get { return GetColumnValue<int?>(Columns.Shippingdate); }
			set { SetColumnValue(Columns.Shippingdate, value); }
		}

		[XmlAttribute("ProductUseDateEndSet")]
		[Bindable(true)]
		public int? ProductUseDateEndSet
		{
			get { return GetColumnValue<int?>(Columns.ProductUseDateEndSet); }
			set { SetColumnValue(Columns.ProductUseDateEndSet, value); }
		}

		[XmlAttribute("GroupProductUnitPrice")]
		[Bindable(true)]
		public int? GroupProductUnitPrice
		{
			get { return GetColumnValue<int?>(Columns.GroupProductUnitPrice); }
			set { SetColumnValue(Columns.GroupProductUnitPrice, value); }
		}

		[XmlAttribute("GroupCouponAppStyle")]
		[Bindable(true)]
		public int GroupCouponAppStyle
		{
			get { return GetColumnValue<int>(Columns.GroupCouponAppStyle); }
			set { SetColumnValue(Columns.GroupCouponAppStyle, value); }
		}

		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}

		[XmlAttribute("Consignment")]
		[Bindable(true)]
		public bool Consignment
		{
			get { return GetColumnValue<bool>(Columns.Consignment); }
			set { SetColumnValue(Columns.Consignment, value); }
		}

		[XmlAttribute("GroupOrderStatus")]
		[Bindable(true)]
		public int? GroupOrderStatus
		{
			get { return GetColumnValue<int?>(Columns.GroupOrderStatus); }
			set { SetColumnValue(Columns.GroupOrderStatus, value); }
		}

		[XmlAttribute("IsDepositCoffee")]
		[Bindable(true)]
		public bool IsDepositCoffee
		{
			get { return GetColumnValue<bool>(Columns.IsDepositCoffee); }
			set { SetColumnValue(Columns.IsDepositCoffee, value); }
		}

		[XmlAttribute("ProductDeliveryType")]
		[Bindable(true)]
		public int ProductDeliveryType
		{
			get { return GetColumnValue<int>(Columns.ProductDeliveryType); }
			set { SetColumnValue(Columns.ProductDeliveryType, value); }
		}

		[XmlAttribute("LastShipDate")]
		[Bindable(true)]
		public DateTime? LastShipDate
		{
			get { return GetColumnValue<DateTime?>(Columns.LastShipDate); }
			set { SetColumnValue(Columns.LastShipDate, value); }
		}

		[XmlAttribute("GoodsStatus")]
		[Bindable(true)]
		public int? GoodsStatus
		{
			get { return GetColumnValue<int?>(Columns.GoodsStatus); }
			set { SetColumnValue(Columns.GoodsStatus, value); }
		}

		[XmlAttribute("SellerGoodsStatus")]
		[Bindable(true)]
		public int? SellerGoodsStatus
		{
			get { return GetColumnValue<int?>(Columns.SellerGoodsStatus); }
			set { SetColumnValue(Columns.SellerGoodsStatus, value); }
		}

		[XmlAttribute("FamilyStoreId")]
		[Bindable(true)]
		public string FamilyStoreId
		{
			get { return GetColumnValue<string>(Columns.FamilyStoreId); }
			set { SetColumnValue(Columns.FamilyStoreId, value); }
		}

		[XmlAttribute("FamilyStoreName")]
		[Bindable(true)]
		public string FamilyStoreName
		{
			get { return GetColumnValue<string>(Columns.FamilyStoreName); }
			set { SetColumnValue(Columns.FamilyStoreName, value); }
		}

		[XmlAttribute("FamilyStoreTel")]
		[Bindable(true)]
		public string FamilyStoreTel
		{
			get { return GetColumnValue<string>(Columns.FamilyStoreTel); }
			set { SetColumnValue(Columns.FamilyStoreTel, value); }
		}

		[XmlAttribute("FamilyStoreAddr")]
		[Bindable(true)]
		public string FamilyStoreAddr
		{
			get { return GetColumnValue<string>(Columns.FamilyStoreAddr); }
			set { SetColumnValue(Columns.FamilyStoreAddr, value); }
		}

		[XmlAttribute("SevenStoreId")]
		[Bindable(true)]
		public string SevenStoreId
		{
			get { return GetColumnValue<string>(Columns.SevenStoreId); }
			set { SetColumnValue(Columns.SevenStoreId, value); }
		}

		[XmlAttribute("SevenStoreName")]
		[Bindable(true)]
		public string SevenStoreName
		{
			get { return GetColumnValue<string>(Columns.SevenStoreName); }
			set { SetColumnValue(Columns.SevenStoreName, value); }
		}

		[XmlAttribute("SevenStoreTel")]
		[Bindable(true)]
		public string SevenStoreTel
		{
			get { return GetColumnValue<string>(Columns.SevenStoreTel); }
			set { SetColumnValue(Columns.SevenStoreTel, value); }
		}

		[XmlAttribute("SevenStoreAddr")]
		[Bindable(true)]
		public string SevenStoreAddr
		{
			get { return GetColumnValue<string>(Columns.SevenStoreAddr); }
			set { SetColumnValue(Columns.SevenStoreAddr, value); }
		}

		[XmlAttribute("PreShipNo")]
		[Bindable(true)]
		public string PreShipNo
		{
			get { return GetColumnValue<string>(Columns.PreShipNo); }
			set { SetColumnValue(Columns.PreShipNo, value); }
		}

		[XmlAttribute("IsReceipt")]
		[Bindable(true)]
		public bool? IsReceipt
		{
			get { return GetColumnValue<bool?>(Columns.IsReceipt); }
			set { SetColumnValue(Columns.IsReceipt, value); }
		}

		[XmlAttribute("AppDealPic")]
		[Bindable(true)]
		public string AppDealPic
		{
			get { return GetColumnValue<string>(Columns.AppDealPic); }
			set { SetColumnValue(Columns.AppDealPic, value); }
		}

		[XmlAttribute("IsCanceling")]
		[Bindable(true)]
		public bool IsCanceling
		{
			get { return GetColumnValue<bool>(Columns.IsCanceling); }
			set { SetColumnValue(Columns.IsCanceling, value); }
		}

		[XmlAttribute("IsWms")]
		[Bindable(true)]
		public bool IsWms
		{
			get { return GetColumnValue<bool>(Columns.IsWms); }
			set { SetColumnValue(Columns.IsWms, value); }
		}

		[XmlAttribute("WmsOrderShipStatus")]
		[Bindable(true)]
		public int WmsOrderShipStatus
		{
			get { return GetColumnValue<int>(Columns.WmsOrderShipStatus); }
			set { SetColumnValue(Columns.WmsOrderShipStatus, value); }
		}

		[XmlAttribute("WmsOrderShipDate")]
		[Bindable(true)]
		public DateTime? WmsOrderShipDate
		{
			get { return GetColumnValue<DateTime?>(Columns.WmsOrderShipDate); }
			set { SetColumnValue(Columns.WmsOrderShipDate, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn OrderIdColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn GuidColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn OrderStatusColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn MemberEmailColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn UniqueIdColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn SubtotalColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn TotalColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn BusinessHourGuidColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn SlugColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderTimeSColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderTimeEColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn BusinessHourDeliverTimeEColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderMinimumColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn ItemOrigPriceColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn ItemPriceColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn ItemNameColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn DepartmentColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn ExchangeStatusColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn ExchangeModifyTimeColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn RemainCountColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn TotalCountColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn StatusColumn
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn BusinessHourDeliverTimeSColumn
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn BusinessHourStatusColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn DeliveryTypeColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn ShoppingCartColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn ComboPackCountColumn
		{
			get { return Schema.Columns[28]; }
		}

		public static TableSchema.TableColumn PrintCountColumn
		{
			get { return Schema.Columns[29]; }
		}

		public static TableSchema.TableColumn CouponUsageColumn
		{
			get { return Schema.Columns[30]; }
		}

		public static TableSchema.TableColumn ImagePathColumn
		{
			get { return Schema.Columns[31]; }
		}

		public static TableSchema.TableColumn ChangedExpireDateColumn
		{
			get { return Schema.Columns[32]; }
		}

		public static TableSchema.TableColumn CouponCodeTypeColumn
		{
			get { return Schema.Columns[33]; }
		}

		public static TableSchema.TableColumn PinTypeColumn
		{
			get { return Schema.Columns[34]; }
		}

		public static TableSchema.TableColumn BookingSystemTypeColumn
		{
			get { return Schema.Columns[35]; }
		}

		public static TableSchema.TableColumn ReturnStatusColumn
		{
			get { return Schema.Columns[36]; }
		}

		public static TableSchema.TableColumn ReturnModifyTimeColumn
		{
			get { return Schema.Columns[37]; }
		}

		public static TableSchema.TableColumn AppTitleColumn
		{
			get { return Schema.Columns[38]; }
		}

		public static TableSchema.TableColumn ShipTimeColumn
		{
			get { return Schema.Columns[39]; }
		}

		public static TableSchema.TableColumn ShipTypeColumn
		{
			get { return Schema.Columns[40]; }
		}

		public static TableSchema.TableColumn ShippingdateTypeColumn
		{
			get { return Schema.Columns[41]; }
		}

		public static TableSchema.TableColumn ShippingdateColumn
		{
			get { return Schema.Columns[42]; }
		}

		public static TableSchema.TableColumn ProductUseDateEndSetColumn
		{
			get { return Schema.Columns[43]; }
		}

		public static TableSchema.TableColumn GroupProductUnitPriceColumn
		{
			get { return Schema.Columns[44]; }
		}

		public static TableSchema.TableColumn GroupCouponAppStyleColumn
		{
			get { return Schema.Columns[45]; }
		}

		public static TableSchema.TableColumn NameColumn
		{
			get { return Schema.Columns[46]; }
		}

		public static TableSchema.TableColumn ConsignmentColumn
		{
			get { return Schema.Columns[47]; }
		}

		public static TableSchema.TableColumn GroupOrderStatusColumn
		{
			get { return Schema.Columns[48]; }
		}

		public static TableSchema.TableColumn IsDepositCoffeeColumn
		{
			get { return Schema.Columns[49]; }
		}

		public static TableSchema.TableColumn ProductDeliveryTypeColumn
		{
			get { return Schema.Columns[50]; }
		}

		public static TableSchema.TableColumn LastShipDateColumn
		{
			get { return Schema.Columns[51]; }
		}

		public static TableSchema.TableColumn GoodsStatusColumn
		{
			get { return Schema.Columns[52]; }
		}

		public static TableSchema.TableColumn SellerGoodsStatusColumn
		{
			get { return Schema.Columns[53]; }
		}

		public static TableSchema.TableColumn FamilyStoreIdColumn
		{
			get { return Schema.Columns[54]; }
		}

		public static TableSchema.TableColumn FamilyStoreNameColumn
		{
			get { return Schema.Columns[55]; }
		}

		public static TableSchema.TableColumn FamilyStoreTelColumn
		{
			get { return Schema.Columns[56]; }
		}

		public static TableSchema.TableColumn FamilyStoreAddrColumn
		{
			get { return Schema.Columns[57]; }
		}

		public static TableSchema.TableColumn SevenStoreIdColumn
		{
			get { return Schema.Columns[58]; }
		}

		public static TableSchema.TableColumn SevenStoreNameColumn
		{
			get { return Schema.Columns[59]; }
		}

		public static TableSchema.TableColumn SevenStoreTelColumn
		{
			get { return Schema.Columns[60]; }
		}

		public static TableSchema.TableColumn SevenStoreAddrColumn
		{
			get { return Schema.Columns[61]; }
		}

		public static TableSchema.TableColumn PreShipNoColumn
		{
			get { return Schema.Columns[62]; }
		}

		public static TableSchema.TableColumn IsReceiptColumn
		{
			get { return Schema.Columns[63]; }
		}

		public static TableSchema.TableColumn AppDealPicColumn
		{
			get { return Schema.Columns[64]; }
		}

		public static TableSchema.TableColumn IsCancelingColumn
		{
			get { return Schema.Columns[65]; }
		}

		public static TableSchema.TableColumn IsWmsColumn
		{
			get { return Schema.Columns[66]; }
		}

		public static TableSchema.TableColumn WmsOrderShipStatusColumn
		{
			get { return Schema.Columns[67]; }
		}

		public static TableSchema.TableColumn WmsOrderShipDateColumn
		{
			get { return Schema.Columns[68]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string CreateTime = @"create_time";
			public static string OrderId = @"order_id";
			public static string Guid = @"GUID";
			public static string OrderStatus = @"order_status";
			public static string MemberEmail = @"member_email";
			public static string UniqueId = @"unique_id";
			public static string Subtotal = @"subtotal";
			public static string Total = @"total";
			public static string BusinessHourGuid = @"business_hour_guid";
			public static string Slug = @"slug";
			public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
			public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
			public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
			public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
			public static string ItemOrigPrice = @"item_orig_price";
			public static string ItemPrice = @"item_price";
			public static string ItemName = @"item_name";
			public static string SellerGuid = @"seller_guid";
			public static string Department = @"department";
			public static string ExchangeStatus = @"exchange_status";
			public static string ExchangeModifyTime = @"exchange_modify_time";
			public static string RemainCount = @"remain_count";
			public static string TotalCount = @"total_count";
			public static string Status = @"status";
			public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
			public static string BusinessHourStatus = @"business_hour_status";
			public static string DeliveryType = @"delivery_type";
			public static string ShoppingCart = @"shopping_cart";
			public static string ComboPackCount = @"combo_pack_count";
			public static string PrintCount = @"print_count";
			public static string CouponUsage = @"coupon_usage";
			public static string ImagePath = @"image_path";
			public static string ChangedExpireDate = @"changed_expire_date";
			public static string CouponCodeType = @"coupon_code_type";
			public static string PinType = @"pin_type";
			public static string BookingSystemType = @"booking_system_type";
			public static string ReturnStatus = @"return_status";
			public static string ReturnModifyTime = @"return_modify_time";
			public static string AppTitle = @"app_title";
			public static string ShipTime = @"ship_time";
			public static string ShipType = @"ship_type";
			public static string ShippingdateType = @"shippingdate_type";
			public static string Shippingdate = @"shippingdate";
			public static string ProductUseDateEndSet = @"product_use_date_end_set";
			public static string GroupProductUnitPrice = @"group_product_unit_price";
			public static string GroupCouponAppStyle = @"group_coupon_app_style";
			public static string Name = @"name";
			public static string Consignment = @"consignment";
			public static string GroupOrderStatus = @"group_order_status";
			public static string IsDepositCoffee = @"is_deposit_coffee";
			public static string ProductDeliveryType = @"product_delivery_type";
			public static string LastShipDate = @"last_ship_date";
			public static string GoodsStatus = @"goods_status";
			public static string SellerGoodsStatus = @"seller_goods_status";
			public static string FamilyStoreId = @"family_store_id";
			public static string FamilyStoreName = @"family_store_name";
			public static string FamilyStoreTel = @"family_store_tel";
			public static string FamilyStoreAddr = @"family_store_addr";
			public static string SevenStoreId = @"seven_store_id";
			public static string SevenStoreName = @"seven_store_name";
			public static string SevenStoreTel = @"seven_store_tel";
			public static string SevenStoreAddr = @"seven_store_addr";
			public static string PreShipNo = @"pre_ship_no";
			public static string IsReceipt = @"is_receipt";
			public static string AppDealPic = @"app_deal_pic";
			public static string IsCanceling = @"is_canceling";
			public static string IsWms = @"is_wms";
			public static string WmsOrderShipStatus = @"wms_order_ship_status";
			public static string WmsOrderShipDate = @"wms_order_ship_date";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

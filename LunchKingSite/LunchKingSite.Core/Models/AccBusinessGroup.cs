using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the AccBusinessGroup class.
	/// </summary>
    [Serializable]
	public partial class AccBusinessGroupCollection : RepositoryList<AccBusinessGroup, AccBusinessGroupCollection>
	{	   
		public AccBusinessGroupCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>AccBusinessGroupCollection</returns>
		public AccBusinessGroupCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                AccBusinessGroup o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the acc_business_group table.
	/// </summary>
	[Serializable]
	public partial class AccBusinessGroup : RepositoryRecord<AccBusinessGroup>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public AccBusinessGroup()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public AccBusinessGroup(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("acc_business_group", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarAccBusinessGroupId = new TableSchema.TableColumn(schema);
				colvarAccBusinessGroupId.ColumnName = "acc_business_group_id";
				colvarAccBusinessGroupId.DataType = DbType.Int32;
				colvarAccBusinessGroupId.MaxLength = 0;
				colvarAccBusinessGroupId.AutoIncrement = false;
				colvarAccBusinessGroupId.IsNullable = false;
				colvarAccBusinessGroupId.IsPrimaryKey = true;
				colvarAccBusinessGroupId.IsForeignKey = false;
				colvarAccBusinessGroupId.IsReadOnly = false;
				colvarAccBusinessGroupId.DefaultSetting = @"";
				colvarAccBusinessGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccBusinessGroupId);
				
				TableSchema.TableColumn colvarAccBusinessGroupName = new TableSchema.TableColumn(schema);
				colvarAccBusinessGroupName.ColumnName = "acc_business_group_name";
				colvarAccBusinessGroupName.DataType = DbType.String;
				colvarAccBusinessGroupName.MaxLength = 255;
				colvarAccBusinessGroupName.AutoIncrement = false;
				colvarAccBusinessGroupName.IsNullable = true;
				colvarAccBusinessGroupName.IsPrimaryKey = false;
				colvarAccBusinessGroupName.IsForeignKey = false;
				colvarAccBusinessGroupName.IsReadOnly = false;
				colvarAccBusinessGroupName.DefaultSetting = @"";
				colvarAccBusinessGroupName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccBusinessGroupName);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = false;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				
						colvarSeq.DefaultSetting = @"((0))";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("acc_business_group",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("AccBusinessGroupId")]
		[Bindable(true)]
		public int AccBusinessGroupId 
		{
			get { return GetColumnValue<int>(Columns.AccBusinessGroupId); }
			set { SetColumnValue(Columns.AccBusinessGroupId, value); }
		}
		  
		[XmlAttribute("AccBusinessGroupName")]
		[Bindable(true)]
		public string AccBusinessGroupName 
		{
			get { return GetColumnValue<string>(Columns.AccBusinessGroupName); }
			set { SetColumnValue(Columns.AccBusinessGroupName, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status 
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int Seq 
		{
			get { return GetColumnValue<int>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn AccBusinessGroupIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AccBusinessGroupNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string AccBusinessGroupId = @"acc_business_group_id";
			 public static string AccBusinessGroupName = @"acc_business_group_name";
			 public static string Status = @"status";
			 public static string Seq = @"seq";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

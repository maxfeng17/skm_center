using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpPollingUserStatus class.
	/// </summary>
    [Serializable]
	public partial class PcpPollingUserStatusCollection : RepositoryList<PcpPollingUserStatus, PcpPollingUserStatusCollection>
	{	   
		public PcpPollingUserStatusCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpPollingUserStatusCollection</returns>
		public PcpPollingUserStatusCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpPollingUserStatus o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_polling_user_status table.
	/// </summary>
	[Serializable]
	public partial class PcpPollingUserStatus : RepositoryRecord<PcpPollingUserStatus>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpPollingUserStatus()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpPollingUserStatus(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_polling_user_status", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarUserToken = new TableSchema.TableColumn(schema);
				colvarUserToken.ColumnName = "user_token";
				colvarUserToken.DataType = DbType.String;
				colvarUserToken.MaxLength = 100;
				colvarUserToken.AutoIncrement = false;
				colvarUserToken.IsNullable = false;
				colvarUserToken.IsPrimaryKey = false;
				colvarUserToken.IsForeignKey = false;
				colvarUserToken.IsReadOnly = false;
				colvarUserToken.DefaultSetting = @"";
				colvarUserToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserToken);
				
				TableSchema.TableColumn colvarExpiredTime = new TableSchema.TableColumn(schema);
				colvarExpiredTime.ColumnName = "expired_time";
				colvarExpiredTime.DataType = DbType.DateTime;
				colvarExpiredTime.MaxLength = 0;
				colvarExpiredTime.AutoIncrement = false;
				colvarExpiredTime.IsNullable = false;
				colvarExpiredTime.IsPrimaryKey = false;
				colvarExpiredTime.IsForeignKey = false;
				colvarExpiredTime.IsReadOnly = false;
				colvarExpiredTime.DefaultSetting = @"";
				colvarExpiredTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExpiredTime);
				
				TableSchema.TableColumn colvarAction = new TableSchema.TableColumn(schema);
				colvarAction.ColumnName = "action";
				colvarAction.DataType = DbType.Int32;
				colvarAction.MaxLength = 0;
				colvarAction.AutoIncrement = false;
				colvarAction.IsNullable = false;
				colvarAction.IsPrimaryKey = false;
				colvarAction.IsForeignKey = false;
				colvarAction.IsReadOnly = false;
				colvarAction.DefaultSetting = @"";
				colvarAction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAction);
				
				TableSchema.TableColumn colvarCreatedTime = new TableSchema.TableColumn(schema);
				colvarCreatedTime.ColumnName = "created_time";
				colvarCreatedTime.DataType = DbType.DateTime;
				colvarCreatedTime.MaxLength = 0;
				colvarCreatedTime.AutoIncrement = false;
				colvarCreatedTime.IsNullable = false;
				colvarCreatedTime.IsPrimaryKey = false;
				colvarCreatedTime.IsForeignKey = false;
				colvarCreatedTime.IsReadOnly = false;
				colvarCreatedTime.DefaultSetting = @"";
				colvarCreatedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedTime);
				
				TableSchema.TableColumn colvarModifiedTime = new TableSchema.TableColumn(schema);
				colvarModifiedTime.ColumnName = "modified_time";
				colvarModifiedTime.DataType = DbType.DateTime;
				colvarModifiedTime.MaxLength = 0;
				colvarModifiedTime.AutoIncrement = false;
				colvarModifiedTime.IsNullable = true;
				colvarModifiedTime.IsPrimaryKey = false;
				colvarModifiedTime.IsForeignKey = false;
				colvarModifiedTime.IsReadOnly = false;
				colvarModifiedTime.DefaultSetting = @"";
				colvarModifiedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedTime);
				
				TableSchema.TableColumn colvarAlive = new TableSchema.TableColumn(schema);
				colvarAlive.ColumnName = "alive";
				colvarAlive.DataType = DbType.Int32;
				colvarAlive.MaxLength = 0;
				colvarAlive.AutoIncrement = false;
				colvarAlive.IsNullable = false;
				colvarAlive.IsPrimaryKey = false;
				colvarAlive.IsForeignKey = false;
				colvarAlive.IsReadOnly = false;
				colvarAlive.DefaultSetting = @"";
				colvarAlive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAlive);
				
				TableSchema.TableColumn colvarUserMembershipCardId = new TableSchema.TableColumn(schema);
				colvarUserMembershipCardId.ColumnName = "user_membership_card_Id";
				colvarUserMembershipCardId.DataType = DbType.Int32;
				colvarUserMembershipCardId.MaxLength = 0;
				colvarUserMembershipCardId.AutoIncrement = false;
				colvarUserMembershipCardId.IsNullable = true;
				colvarUserMembershipCardId.IsPrimaryKey = false;
				colvarUserMembershipCardId.IsForeignKey = false;
				colvarUserMembershipCardId.IsReadOnly = false;
				colvarUserMembershipCardId.DefaultSetting = @"";
				colvarUserMembershipCardId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserMembershipCardId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_polling_user_status",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("UserToken")]
		[Bindable(true)]
		public string UserToken 
		{
			get { return GetColumnValue<string>(Columns.UserToken); }
			set { SetColumnValue(Columns.UserToken, value); }
		}
		  
		[XmlAttribute("ExpiredTime")]
		[Bindable(true)]
		public DateTime ExpiredTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ExpiredTime); }
			set { SetColumnValue(Columns.ExpiredTime, value); }
		}
		  
		[XmlAttribute("Action")]
		[Bindable(true)]
		public int Action 
		{
			get { return GetColumnValue<int>(Columns.Action); }
			set { SetColumnValue(Columns.Action, value); }
		}
		  
		[XmlAttribute("CreatedTime")]
		[Bindable(true)]
		public DateTime CreatedTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedTime); }
			set { SetColumnValue(Columns.CreatedTime, value); }
		}
		  
		[XmlAttribute("ModifiedTime")]
		[Bindable(true)]
		public DateTime? ModifiedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifiedTime); }
			set { SetColumnValue(Columns.ModifiedTime, value); }
		}
		  
		[XmlAttribute("Alive")]
		[Bindable(true)]
		public int Alive 
		{
			get { return GetColumnValue<int>(Columns.Alive); }
			set { SetColumnValue(Columns.Alive, value); }
		}
		  
		[XmlAttribute("UserMembershipCardId")]
		[Bindable(true)]
		public int? UserMembershipCardId 
		{
			get { return GetColumnValue<int?>(Columns.UserMembershipCardId); }
			set { SetColumnValue(Columns.UserMembershipCardId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UserTokenColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ExpiredTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn AliveColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn UserMembershipCardIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string UserId = @"user_id";
			 public static string UserToken = @"user_token";
			 public static string ExpiredTime = @"expired_time";
			 public static string Action = @"action";
			 public static string CreatedTime = @"created_time";
			 public static string ModifiedTime = @"modified_time";
			 public static string Alive = @"alive";
			 public static string UserMembershipCardId = @"user_membership_card_Id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

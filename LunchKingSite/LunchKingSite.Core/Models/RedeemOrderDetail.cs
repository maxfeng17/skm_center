using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the RedeemOrderDetail class.
	/// </summary>
    [Serializable]
	public partial class RedeemOrderDetailCollection : RepositoryList<RedeemOrderDetail, RedeemOrderDetailCollection>
	{	   
		public RedeemOrderDetailCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>RedeemOrderDetailCollection</returns>
		public RedeemOrderDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                RedeemOrderDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the redeem_order_detail table.
	/// </summary>
	[Serializable]
	public partial class RedeemOrderDetail : RepositoryRecord<RedeemOrderDetail>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public RedeemOrderDetail()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public RedeemOrderDetail(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("redeem_order_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_GUID";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = true;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				
					colvarOrderGuid.ForeignKeyTableName = "redeem_order";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 300;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = false;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);
				
				TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
				colvarItemGuid.ColumnName = "item_GUID";
				colvarItemGuid.DataType = DbType.Guid;
				colvarItemGuid.MaxLength = 0;
				colvarItemGuid.AutoIncrement = false;
				colvarItemGuid.IsNullable = true;
				colvarItemGuid.IsPrimaryKey = false;
				colvarItemGuid.IsForeignKey = false;
				colvarItemGuid.IsReadOnly = false;
				colvarItemGuid.DefaultSetting = @"";
				colvarItemGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemGuid);
				
				TableSchema.TableColumn colvarItemUnitPrice = new TableSchema.TableColumn(schema);
				colvarItemUnitPrice.ColumnName = "item_unit_price";
				colvarItemUnitPrice.DataType = DbType.Currency;
				colvarItemUnitPrice.MaxLength = 0;
				colvarItemUnitPrice.AutoIncrement = false;
				colvarItemUnitPrice.IsNullable = false;
				colvarItemUnitPrice.IsPrimaryKey = false;
				colvarItemUnitPrice.IsForeignKey = false;
				colvarItemUnitPrice.IsReadOnly = false;
				colvarItemUnitPrice.DefaultSetting = @"";
				colvarItemUnitPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemUnitPrice);
				
				TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
				colvarItemQuantity.ColumnName = "item_quantity";
				colvarItemQuantity.DataType = DbType.Int32;
				colvarItemQuantity.MaxLength = 0;
				colvarItemQuantity.AutoIncrement = false;
				colvarItemQuantity.IsNullable = false;
				colvarItemQuantity.IsPrimaryKey = false;
				colvarItemQuantity.IsForeignKey = false;
				colvarItemQuantity.IsReadOnly = false;
				colvarItemQuantity.DefaultSetting = @"";
				colvarItemQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemQuantity);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 30;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("redeem_order_detail",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName 
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}
		  
		[XmlAttribute("ItemGuid")]
		[Bindable(true)]
		public Guid? ItemGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.ItemGuid); }
			set { SetColumnValue(Columns.ItemGuid, value); }
		}
		  
		[XmlAttribute("ItemUnitPrice")]
		[Bindable(true)]
		public decimal ItemUnitPrice 
		{
			get { return GetColumnValue<decimal>(Columns.ItemUnitPrice); }
			set { SetColumnValue(Columns.ItemUnitPrice, value); }
		}
		  
		[XmlAttribute("ItemQuantity")]
		[Bindable(true)]
		public int ItemQuantity 
		{
			get { return GetColumnValue<int>(Columns.ItemQuantity); }
			set { SetColumnValue(Columns.ItemQuantity, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemUnitPriceColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemQuantityColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string OrderGuid = @"order_GUID";
			 public static string ItemName = @"item_name";
			 public static string ItemGuid = @"item_GUID";
			 public static string ItemUnitPrice = @"item_unit_price";
			 public static string ItemQuantity = @"item_quantity";
			 public static string Status = @"status";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

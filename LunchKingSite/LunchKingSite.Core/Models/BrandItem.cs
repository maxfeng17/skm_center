using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the BrandItem class.
    /// </summary>
    [Serializable]
    public partial class BrandItemCollection : RepositoryList<BrandItem, BrandItemCollection>
    {
        public BrandItemCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BrandItemCollection</returns>
        public BrandItemCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BrandItem o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the brand_item table.
    /// </summary>
    [Serializable]
    public partial class BrandItem : RepositoryRecord<BrandItem>, IRecordBase
    {
        #region .ctors and Default Settings

        public BrandItem()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public BrandItem(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("brand_item", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarMainId = new TableSchema.TableColumn(schema);
                colvarMainId.ColumnName = "main_id";
                colvarMainId.DataType = DbType.Int32;
                colvarMainId.MaxLength = 0;
                colvarMainId.AutoIncrement = false;
                colvarMainId.IsNullable = false;
                colvarMainId.IsPrimaryKey = false;
                colvarMainId.IsForeignKey = false;
                colvarMainId.IsReadOnly = false;
                colvarMainId.DefaultSetting = @"";
                colvarMainId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMainId);

                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = false;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;

                colvarSeq.DefaultSetting = @"((0))";
                colvarSeq.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSeq);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Boolean;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                colvarStatus.DefaultSetting = @"((1))";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
                colvarCreator.ColumnName = "creator";
                colvarCreator.DataType = DbType.String;
                colvarCreator.MaxLength = 100;
                colvarCreator.AutoIncrement = false;
                colvarCreator.IsNullable = false;
                colvarCreator.IsPrimaryKey = false;
                colvarCreator.IsForeignKey = false;
                colvarCreator.IsReadOnly = false;
                colvarCreator.DefaultSetting = @"";
                colvarCreator.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreator);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 1073741823;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                colvarMessage.DefaultSetting = @"";
                colvarMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = false;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;

                colvarItemId.DefaultSetting = @"((0))";
                colvarItemId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemId);

                TableSchema.TableColumn colvarSeqStatus = new TableSchema.TableColumn(schema);
                colvarSeqStatus.ColumnName = "seq_status";
                colvarSeqStatus.DataType = DbType.Boolean;
                colvarSeqStatus.MaxLength = 0;
                colvarSeqStatus.AutoIncrement = false;
                colvarSeqStatus.IsNullable = false;
                colvarSeqStatus.IsPrimaryKey = false;
                colvarSeqStatus.IsForeignKey = false;
                colvarSeqStatus.IsReadOnly = false;

                colvarSeqStatus.DefaultSetting = @"((0))";
                colvarSeqStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSeqStatus);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("brand_item", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("MainId")]
        [Bindable(true)]
        public int MainId
        {
            get { return GetColumnValue<int>(Columns.MainId); }
            set { SetColumnValue(Columns.MainId, value); }
        }

        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int Seq
        {
            get { return GetColumnValue<int>(Columns.Seq); }
            set { SetColumnValue(Columns.Seq, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public bool Status
        {
            get { return GetColumnValue<bool>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("Creator")]
        [Bindable(true)]
        public string Creator
        {
            get { return GetColumnValue<string>(Columns.Creator); }
            set { SetColumnValue(Columns.Creator, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get { return GetColumnValue<string>(Columns.Message); }
            set { SetColumnValue(Columns.Message, value); }
        }

        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int ItemId
        {
            get { return GetColumnValue<int>(Columns.ItemId); }
            set { SetColumnValue(Columns.ItemId, value); }
        }

        [XmlAttribute("SeqStatus")]
        [Bindable(true)]
        public bool SeqStatus
        {
            get { return GetColumnValue<bool>(Columns.SeqStatus); }
            set { SetColumnValue(Columns.SeqStatus, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn MainIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ItemIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn SeqStatusColumn
        {
            get { return Schema.Columns[8]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string MainId = @"main_id";
            public static string Seq = @"seq";
            public static string Status = @"status";
            public static string Creator = @"creator";
            public static string CreateTime = @"create_time";
            public static string Message = @"message";
            public static string ItemId = @"item_id";
            public static string SeqStatus = @"seq_status";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpPointTransactionOrder class.
	/// </summary>
    [Serializable]
	public partial class PcpPointTransactionOrderCollection : RepositoryList<PcpPointTransactionOrder, PcpPointTransactionOrderCollection>
	{	   
		public PcpPointTransactionOrderCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpPointTransactionOrderCollection</returns>
		public PcpPointTransactionOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpPointTransactionOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_point_transaction_order table.
	/// </summary>
	[Serializable]
	public partial class PcpPointTransactionOrder : RepositoryRecord<PcpPointTransactionOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpPointTransactionOrder()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpPointTransactionOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_point_transaction_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
				colvarSellerUserId.ColumnName = "seller_user_id";
				colvarSellerUserId.DataType = DbType.Int32;
				colvarSellerUserId.MaxLength = 0;
				colvarSellerUserId.AutoIncrement = false;
				colvarSellerUserId.IsNullable = false;
				colvarSellerUserId.IsPrimaryKey = false;
				colvarSellerUserId.IsForeignKey = false;
				colvarSellerUserId.IsReadOnly = false;
				colvarSellerUserId.DefaultSetting = @"";
				colvarSellerUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerUserId);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarOderStatus = new TableSchema.TableColumn(schema);
				colvarOderStatus.ColumnName = "oder_status";
				colvarOderStatus.DataType = DbType.Byte;
				colvarOderStatus.MaxLength = 0;
				colvarOderStatus.AutoIncrement = false;
				colvarOderStatus.IsNullable = false;
				colvarOderStatus.IsPrimaryKey = false;
				colvarOderStatus.IsForeignKey = false;
				colvarOderStatus.IsReadOnly = false;
				colvarOderStatus.DefaultSetting = @"";
				colvarOderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOderStatus);
				
				TableSchema.TableColumn colvarEinvoiceMode = new TableSchema.TableColumn(schema);
				colvarEinvoiceMode.ColumnName = "einvoice_mode";
				colvarEinvoiceMode.DataType = DbType.Byte;
				colvarEinvoiceMode.MaxLength = 0;
				colvarEinvoiceMode.AutoIncrement = false;
				colvarEinvoiceMode.IsNullable = false;
				colvarEinvoiceMode.IsPrimaryKey = false;
				colvarEinvoiceMode.IsForeignKey = false;
				colvarEinvoiceMode.IsReadOnly = false;
				colvarEinvoiceMode.DefaultSetting = @"";
				colvarEinvoiceMode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEinvoiceMode);
				
				TableSchema.TableColumn colvarTransactionAmount = new TableSchema.TableColumn(schema);
				colvarTransactionAmount.ColumnName = "transaction_amount";
				colvarTransactionAmount.DataType = DbType.Currency;
				colvarTransactionAmount.MaxLength = 0;
				colvarTransactionAmount.AutoIncrement = false;
				colvarTransactionAmount.IsNullable = false;
				colvarTransactionAmount.IsPrimaryKey = false;
				colvarTransactionAmount.IsForeignKey = false;
				colvarTransactionAmount.IsReadOnly = false;
				colvarTransactionAmount.DefaultSetting = @"";
				colvarTransactionAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransactionAmount);
				
				TableSchema.TableColumn colvarRegularsPoint = new TableSchema.TableColumn(schema);
				colvarRegularsPoint.ColumnName = "regulars_point";
				colvarRegularsPoint.DataType = DbType.Int32;
				colvarRegularsPoint.MaxLength = 0;
				colvarRegularsPoint.AutoIncrement = false;
				colvarRegularsPoint.IsNullable = false;
				colvarRegularsPoint.IsPrimaryKey = false;
				colvarRegularsPoint.IsForeignKey = false;
				colvarRegularsPoint.IsReadOnly = false;
				
						colvarRegularsPoint.DefaultSetting = @"((0))";
				colvarRegularsPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRegularsPoint);
				
				TableSchema.TableColumn colvarFavorPoint = new TableSchema.TableColumn(schema);
				colvarFavorPoint.ColumnName = "favor_point";
				colvarFavorPoint.DataType = DbType.Int32;
				colvarFavorPoint.MaxLength = 0;
				colvarFavorPoint.AutoIncrement = false;
				colvarFavorPoint.IsNullable = false;
				colvarFavorPoint.IsPrimaryKey = false;
				colvarFavorPoint.IsForeignKey = false;
				colvarFavorPoint.IsReadOnly = false;
				
						colvarFavorPoint.DefaultSetting = @"((0))";
				colvarFavorPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFavorPoint);
				
				TableSchema.TableColumn colvarFavorPointStartTime = new TableSchema.TableColumn(schema);
				colvarFavorPointStartTime.ColumnName = "favor_point_start_time";
				colvarFavorPointStartTime.DataType = DbType.DateTime;
				colvarFavorPointStartTime.MaxLength = 0;
				colvarFavorPointStartTime.AutoIncrement = false;
				colvarFavorPointStartTime.IsNullable = true;
				colvarFavorPointStartTime.IsPrimaryKey = false;
				colvarFavorPointStartTime.IsForeignKey = false;
				colvarFavorPointStartTime.IsReadOnly = false;
				colvarFavorPointStartTime.DefaultSetting = @"";
				colvarFavorPointStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFavorPointStartTime);
				
				TableSchema.TableColumn colvarFavorPointExpireTime = new TableSchema.TableColumn(schema);
				colvarFavorPointExpireTime.ColumnName = "favor_point_expire_time";
				colvarFavorPointExpireTime.DataType = DbType.DateTime;
				colvarFavorPointExpireTime.MaxLength = 0;
				colvarFavorPointExpireTime.AutoIncrement = false;
				colvarFavorPointExpireTime.IsNullable = true;
				colvarFavorPointExpireTime.IsPrimaryKey = false;
				colvarFavorPointExpireTime.IsForeignKey = false;
				colvarFavorPointExpireTime.IsReadOnly = false;
				colvarFavorPointExpireTime.DefaultSetting = @"";
				colvarFavorPointExpireTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFavorPointExpireTime);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = -1;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_point_transaction_order",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SellerUserId")]
		[Bindable(true)]
		public int SellerUserId 
		{
			get { return GetColumnValue<int>(Columns.SellerUserId); }
			set { SetColumnValue(Columns.SellerUserId, value); }
		}
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("OderStatus")]
		[Bindable(true)]
		public byte OderStatus 
		{
			get { return GetColumnValue<byte>(Columns.OderStatus); }
			set { SetColumnValue(Columns.OderStatus, value); }
		}
		  
		[XmlAttribute("EinvoiceMode")]
		[Bindable(true)]
		public byte EinvoiceMode 
		{
			get { return GetColumnValue<byte>(Columns.EinvoiceMode); }
			set { SetColumnValue(Columns.EinvoiceMode, value); }
		}
		  
		[XmlAttribute("TransactionAmount")]
		[Bindable(true)]
		public decimal TransactionAmount 
		{
			get { return GetColumnValue<decimal>(Columns.TransactionAmount); }
			set { SetColumnValue(Columns.TransactionAmount, value); }
		}
		  
		[XmlAttribute("RegularsPoint")]
		[Bindable(true)]
		public int RegularsPoint 
		{
			get { return GetColumnValue<int>(Columns.RegularsPoint); }
			set { SetColumnValue(Columns.RegularsPoint, value); }
		}
		  
		[XmlAttribute("FavorPoint")]
		[Bindable(true)]
		public int FavorPoint 
		{
			get { return GetColumnValue<int>(Columns.FavorPoint); }
			set { SetColumnValue(Columns.FavorPoint, value); }
		}
		  
		[XmlAttribute("FavorPointStartTime")]
		[Bindable(true)]
		public DateTime? FavorPointStartTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.FavorPointStartTime); }
			set { SetColumnValue(Columns.FavorPointStartTime, value); }
		}
		  
		[XmlAttribute("FavorPointExpireTime")]
		[Bindable(true)]
		public DateTime? FavorPointExpireTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.FavorPointExpireTime); }
			set { SetColumnValue(Columns.FavorPointExpireTime, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerUserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OderStatusColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EinvoiceModeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn TransactionAmountColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn RegularsPointColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn FavorPointColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn FavorPointStartTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn FavorPointExpireTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SellerUserId = @"seller_user_id";
			 public static string Guid = @"guid";
			 public static string OderStatus = @"oder_status";
			 public static string EinvoiceMode = @"einvoice_mode";
			 public static string TransactionAmount = @"transaction_amount";
			 public static string RegularsPoint = @"regulars_point";
			 public static string FavorPoint = @"favor_point";
			 public static string FavorPointStartTime = @"favor_point_start_time";
			 public static string FavorPointExpireTime = @"favor_point_expire_time";
			 public static string Description = @"description";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

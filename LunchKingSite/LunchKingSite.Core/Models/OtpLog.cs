using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OtpLog class.
	/// </summary>
    [Serializable]
	public partial class OtpLogCollection : RepositoryList<OtpLog, OtpLogCollection>
	{	   
		public OtpLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OtpLogCollection</returns>
		public OtpLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OtpLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the otp_log table.
	/// </summary>
	[Serializable]
	public partial class OtpLog : RepositoryRecord<OtpLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OtpLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OtpLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("otp_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTransId = new TableSchema.TableColumn(schema);
				colvarTransId.ColumnName = "trans_id";
				colvarTransId.DataType = DbType.AnsiString;
				colvarTransId.MaxLength = 40;
				colvarTransId.AutoIncrement = false;
				colvarTransId.IsNullable = false;
				colvarTransId.IsPrimaryKey = false;
				colvarTransId.IsForeignKey = false;
				colvarTransId.IsReadOnly = false;
				colvarTransId.DefaultSetting = @"";
				colvarTransId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransId);
				
				TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
				colvarContent.ColumnName = "content";
				colvarContent.DataType = DbType.String;
				colvarContent.MaxLength = -1;
				colvarContent.AutoIncrement = false;
				colvarContent.IsNullable = true;
				colvarContent.IsPrimaryKey = false;
				colvarContent.IsForeignKey = false;
				colvarContent.IsReadOnly = false;
				colvarContent.DefaultSetting = @"";
				colvarContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContent);
				
				TableSchema.TableColumn colvarOrderFromType = new TableSchema.TableColumn(schema);
				colvarOrderFromType.ColumnName = "order_from_type";
				colvarOrderFromType.DataType = DbType.Int32;
				colvarOrderFromType.MaxLength = 0;
				colvarOrderFromType.AutoIncrement = false;
				colvarOrderFromType.IsNullable = false;
				colvarOrderFromType.IsPrimaryKey = false;
				colvarOrderFromType.IsForeignKey = false;
				colvarOrderFromType.IsReadOnly = false;
				colvarOrderFromType.DefaultSetting = @"";
				colvarOrderFromType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderFromType);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarServerName = new TableSchema.TableColumn(schema);
				colvarServerName.ColumnName = "server_name";
				colvarServerName.DataType = DbType.String;
				colvarServerName.MaxLength = 100;
				colvarServerName.AutoIncrement = false;
				colvarServerName.IsNullable = true;
				colvarServerName.IsPrimaryKey = false;
				colvarServerName.IsForeignKey = false;
				colvarServerName.IsReadOnly = false;
				colvarServerName.DefaultSetting = @"";
				colvarServerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServerName);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("otp_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TransId")]
		[Bindable(true)]
		public string TransId 
		{
			get { return GetColumnValue<string>(Columns.TransId); }
			set { SetColumnValue(Columns.TransId, value); }
		}
		  
		[XmlAttribute("Content")]
		[Bindable(true)]
		public string Content 
		{
			get { return GetColumnValue<string>(Columns.Content); }
			set { SetColumnValue(Columns.Content, value); }
		}
		  
		[XmlAttribute("OrderFromType")]
		[Bindable(true)]
		public int OrderFromType 
		{
			get { return GetColumnValue<int>(Columns.OrderFromType); }
			set { SetColumnValue(Columns.OrderFromType, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ServerName")]
		[Bindable(true)]
		public string ServerName 
		{
			get { return GetColumnValue<string>(Columns.ServerName); }
			set { SetColumnValue(Columns.ServerName, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TransIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ContentColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderFromTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ServerNameColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TransId = @"trans_id";
			 public static string Content = @"content";
			 public static string OrderFromType = @"order_from_type";
			 public static string CreateTime = @"create_time";
			 public static string ServerName = @"server_name";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

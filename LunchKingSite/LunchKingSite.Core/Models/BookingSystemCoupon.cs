using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BookingSystemCoupon class.
	/// </summary>
    [Serializable]
	public partial class BookingSystemCouponCollection : RepositoryList<BookingSystemCoupon, BookingSystemCouponCollection>
	{	   
		public BookingSystemCouponCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BookingSystemCouponCollection</returns>
		public BookingSystemCouponCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BookingSystemCoupon o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the booking_system_coupon table.
	/// </summary>
	[Serializable]
	public partial class BookingSystemCoupon : RepositoryRecord<BookingSystemCoupon>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BookingSystemCoupon()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BookingSystemCoupon(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("booking_system_coupon", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarReservationId = new TableSchema.TableColumn(schema);
				colvarReservationId.ColumnName = "reservation_id";
				colvarReservationId.DataType = DbType.Int32;
				colvarReservationId.MaxLength = 0;
				colvarReservationId.AutoIncrement = false;
				colvarReservationId.IsNullable = false;
				colvarReservationId.IsPrimaryKey = false;
				colvarReservationId.IsForeignKey = false;
				colvarReservationId.IsReadOnly = false;
				colvarReservationId.DefaultSetting = @"";
				colvarReservationId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReservationId);
				
				TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
				colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
				colvarCouponSequenceNumber.DataType = DbType.AnsiString;
				colvarCouponSequenceNumber.MaxLength = 50;
				colvarCouponSequenceNumber.AutoIncrement = false;
				colvarCouponSequenceNumber.IsNullable = false;
				colvarCouponSequenceNumber.IsPrimaryKey = false;
				colvarCouponSequenceNumber.IsForeignKey = false;
				colvarCouponSequenceNumber.IsReadOnly = false;
				colvarCouponSequenceNumber.DefaultSetting = @"";
				colvarCouponSequenceNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponSequenceNumber);
				
				TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
				colvarOrderDetailGuid.ColumnName = "order_detail_guid";
				colvarOrderDetailGuid.DataType = DbType.Guid;
				colvarOrderDetailGuid.MaxLength = 0;
				colvarOrderDetailGuid.AutoIncrement = false;
				colvarOrderDetailGuid.IsNullable = false;
				colvarOrderDetailGuid.IsPrimaryKey = false;
				colvarOrderDetailGuid.IsForeignKey = false;
				colvarOrderDetailGuid.IsReadOnly = false;
				colvarOrderDetailGuid.DefaultSetting = @"";
				colvarOrderDetailGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderDetailGuid);
				
				TableSchema.TableColumn colvarPrefix = new TableSchema.TableColumn(schema);
				colvarPrefix.ColumnName = "prefix";
				colvarPrefix.DataType = DbType.AnsiString;
				colvarPrefix.MaxLength = 10;
				colvarPrefix.AutoIncrement = false;
				colvarPrefix.IsNullable = true;
				colvarPrefix.IsPrimaryKey = false;
				colvarPrefix.IsForeignKey = false;
				colvarPrefix.IsReadOnly = false;
				colvarPrefix.DefaultSetting = @"";
				colvarPrefix.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrefix);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("booking_system_coupon",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ReservationId")]
		[Bindable(true)]
		public int ReservationId 
		{
			get { return GetColumnValue<int>(Columns.ReservationId); }
			set { SetColumnValue(Columns.ReservationId, value); }
		}
		  
		[XmlAttribute("CouponSequenceNumber")]
		[Bindable(true)]
		public string CouponSequenceNumber 
		{
			get { return GetColumnValue<string>(Columns.CouponSequenceNumber); }
			set { SetColumnValue(Columns.CouponSequenceNumber, value); }
		}
		  
		[XmlAttribute("OrderDetailGuid")]
		[Bindable(true)]
		public Guid OrderDetailGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderDetailGuid); }
			set { SetColumnValue(Columns.OrderDetailGuid, value); }
		}
		  
		[XmlAttribute("Prefix")]
		[Bindable(true)]
		public string Prefix 
		{
			get { return GetColumnValue<string>(Columns.Prefix); }
			set { SetColumnValue(Columns.Prefix, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ReservationIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponSequenceNumberColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderDetailGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PrefixColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ReservationId = @"reservation_id";
			 public static string CouponSequenceNumber = @"coupon_sequence_number";
			 public static string OrderDetailGuid = @"order_detail_guid";
			 public static string Prefix = @"prefix";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

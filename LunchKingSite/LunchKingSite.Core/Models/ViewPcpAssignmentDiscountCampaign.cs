using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpAssignmentDiscountCampaign class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpAssignmentDiscountCampaignCollection : ReadOnlyList<ViewPcpAssignmentDiscountCampaign, ViewPcpAssignmentDiscountCampaignCollection>
    {        
        public ViewPcpAssignmentDiscountCampaignCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_assignment_discount_campaign view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpAssignmentDiscountCampaign : ReadOnlyRecord<ViewPcpAssignmentDiscountCampaign>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_assignment_discount_campaign", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarCampaignNo = new TableSchema.TableColumn(schema);
                colvarCampaignNo.ColumnName = "campaign_no";
                colvarCampaignNo.DataType = DbType.AnsiString;
                colvarCampaignNo.MaxLength = 15;
                colvarCampaignNo.AutoIncrement = false;
                colvarCampaignNo.IsNullable = true;
                colvarCampaignNo.IsPrimaryKey = false;
                colvarCampaignNo.IsForeignKey = false;
                colvarCampaignNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCampaignNo);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = true;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarSendCount = new TableSchema.TableColumn(schema);
                colvarSendCount.ColumnName = "send_count";
                colvarSendCount.DataType = DbType.Int32;
                colvarSendCount.MaxLength = 0;
                colvarSendCount.AutoIncrement = false;
                colvarSendCount.IsNullable = false;
                colvarSendCount.IsPrimaryKey = false;
                colvarSendCount.IsForeignKey = false;
                colvarSendCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarSendCount);
                
                TableSchema.TableColumn colvarTemplateId = new TableSchema.TableColumn(schema);
                colvarTemplateId.ColumnName = "template_id";
                colvarTemplateId.DataType = DbType.Int32;
                colvarTemplateId.MaxLength = 0;
                colvarTemplateId.AutoIncrement = false;
                colvarTemplateId.IsNullable = true;
                colvarTemplateId.IsPrimaryKey = false;
                colvarTemplateId.IsForeignKey = false;
                colvarTemplateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTemplateId);
                
                TableSchema.TableColumn colvarUseCount = new TableSchema.TableColumn(schema);
                colvarUseCount.ColumnName = "use_count";
                colvarUseCount.DataType = DbType.Int32;
                colvarUseCount.MaxLength = 0;
                colvarUseCount.AutoIncrement = false;
                colvarUseCount.IsNullable = true;
                colvarUseCount.IsPrimaryKey = false;
                colvarUseCount.IsForeignKey = false;
                colvarUseCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseCount);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_assignment_discount_campaign",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpAssignmentDiscountCampaign()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpAssignmentDiscountCampaign(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpAssignmentDiscountCampaign(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpAssignmentDiscountCampaign(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("CampaignNo")]
        [Bindable(true)]
        public string CampaignNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("campaign_no");
		    }
            set 
		    {
			    SetColumnValue("campaign_no", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime? StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("SendCount")]
        [Bindable(true)]
        public int SendCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("send_count");
		    }
            set 
		    {
			    SetColumnValue("send_count", value);
            }
        }
	      
        [XmlAttribute("TemplateId")]
        [Bindable(true)]
        public int? TemplateId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("template_id");
		    }
            set 
		    {
			    SetColumnValue("template_id", value);
            }
        }
	      
        [XmlAttribute("UseCount")]
        [Bindable(true)]
        public int? UseCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("use_count");
		    }
            set 
		    {
			    SetColumnValue("use_count", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string CampaignNo = @"campaign_no";
            
            public static string StartTime = @"start_time";
            
            public static string Type = @"type";
            
            public static string SendCount = @"send_count";
            
            public static string TemplateId = @"template_id";
            
            public static string UseCount = @"use_count";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

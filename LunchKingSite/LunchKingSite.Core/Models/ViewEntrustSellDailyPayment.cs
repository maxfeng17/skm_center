using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEntrustSellDailyPayment class.
    /// </summary>
    [Serializable]
    public partial class ViewEntrustSellDailyPaymentCollection : ReadOnlyList<ViewEntrustSellDailyPayment, ViewEntrustSellDailyPaymentCollection>
    {        
        public ViewEntrustSellDailyPaymentCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_entrust_sell_daily_payment view.
    /// </summary>
    [Serializable]
    public partial class ViewEntrustSellDailyPayment : ReadOnlyRecord<ViewEntrustSellDailyPayment>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_entrust_sell_daily_payment", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarTransTime = new TableSchema.TableColumn(schema);
                colvarTransTime.ColumnName = "trans_time";
                colvarTransTime.DataType = DbType.DateTime;
                colvarTransTime.MaxLength = 0;
                colvarTransTime.AutoIncrement = false;
                colvarTransTime.IsNullable = true;
                colvarTransTime.IsPrimaryKey = false;
                colvarTransTime.IsForeignKey = false;
                colvarTransTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarTransTime);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarTransType = new TableSchema.TableColumn(schema);
                colvarTransType.ColumnName = "trans_type";
                colvarTransType.DataType = DbType.Int32;
                colvarTransType.MaxLength = 0;
                colvarTransType.AutoIncrement = false;
                colvarTransType.IsNullable = true;
                colvarTransType.IsPrimaryKey = false;
                colvarTransType.IsForeignKey = false;
                colvarTransType.IsReadOnly = false;
                
                schema.Columns.Add(colvarTransType);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 1073741823;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_entrust_sell_daily_payment",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewEntrustSellDailyPayment()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEntrustSellDailyPayment(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewEntrustSellDailyPayment(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewEntrustSellDailyPayment(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("TransTime")]
        [Bindable(true)]
        public DateTime? TransTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("trans_time");
		    }
            set 
		    {
			    SetColumnValue("trans_time", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal Amount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("amount");
		    }
            set 
		    {
			    SetColumnValue("amount", value);
            }
        }
	      
        [XmlAttribute("TransType")]
        [Bindable(true)]
        public int? TransType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("trans_type");
		    }
            set 
		    {
			    SetColumnValue("trans_type", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string TransTime = @"trans_time";
            
            public static string OrderId = @"order_id";
            
            public static string Amount = @"amount";
            
            public static string TransType = @"trans_type";
            
            public static string MemberName = @"member_name";
            
            public static string CouponUsage = @"coupon_usage";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

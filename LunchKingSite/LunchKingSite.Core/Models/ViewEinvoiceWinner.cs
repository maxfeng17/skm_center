using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEinvoiceWinner class.
    /// </summary>
    [Serializable]
    public partial class ViewEinvoiceWinnerCollection : ReadOnlyList<ViewEinvoiceWinner, ViewEinvoiceWinnerCollection>
    {        
        public ViewEinvoiceWinnerCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_einvoice_winners view.
    /// </summary>
    [Serializable]
    public partial class ViewEinvoiceWinner : ReadOnlyRecord<ViewEinvoiceWinner>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_einvoice_winners", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = false;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarInvoiceNumber = new TableSchema.TableColumn(schema);
                colvarInvoiceNumber.ColumnName = "invoice_number";
                colvarInvoiceNumber.DataType = DbType.String;
                colvarInvoiceNumber.MaxLength = 10;
                colvarInvoiceNumber.AutoIncrement = false;
                colvarInvoiceNumber.IsNullable = true;
                colvarInvoiceNumber.IsPrimaryKey = false;
                colvarInvoiceNumber.IsForeignKey = false;
                colvarInvoiceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceNumber);
                
                TableSchema.TableColumn colvarInvoiceNumberTime = new TableSchema.TableColumn(schema);
                colvarInvoiceNumberTime.ColumnName = "invoice_number_time";
                colvarInvoiceNumberTime.DataType = DbType.DateTime;
                colvarInvoiceNumberTime.MaxLength = 0;
                colvarInvoiceNumberTime.AutoIncrement = false;
                colvarInvoiceNumberTime.IsNullable = true;
                colvarInvoiceNumberTime.IsPrimaryKey = false;
                colvarInvoiceNumberTime.IsForeignKey = false;
                colvarInvoiceNumberTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceNumberTime);
                
                TableSchema.TableColumn colvarOrderTime = new TableSchema.TableColumn(schema);
                colvarOrderTime.ColumnName = "order_time";
                colvarOrderTime.DataType = DbType.DateTime;
                colvarOrderTime.MaxLength = 0;
                colvarOrderTime.AutoIncrement = false;
                colvarOrderTime.IsNullable = false;
                colvarOrderTime.IsPrimaryKey = false;
                colvarOrderTime.IsForeignKey = false;
                colvarOrderTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTime);
                
                TableSchema.TableColumn colvarInvoiceMode2 = new TableSchema.TableColumn(schema);
                colvarInvoiceMode2.ColumnName = "invoice_mode2";
                colvarInvoiceMode2.DataType = DbType.Int32;
                colvarInvoiceMode2.MaxLength = 0;
                colvarInvoiceMode2.AutoIncrement = false;
                colvarInvoiceMode2.IsNullable = false;
                colvarInvoiceMode2.IsPrimaryKey = false;
                colvarInvoiceMode2.IsForeignKey = false;
                colvarInvoiceMode2.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceMode2);
                
                TableSchema.TableColumn colvarInvoiceType = new TableSchema.TableColumn(schema);
                colvarInvoiceType.ColumnName = "invoice_type";
                colvarInvoiceType.DataType = DbType.String;
                colvarInvoiceType.MaxLength = 8;
                colvarInvoiceType.AutoIncrement = false;
                colvarInvoiceType.IsNullable = true;
                colvarInvoiceType.IsPrimaryKey = false;
                colvarInvoiceType.IsForeignKey = false;
                colvarInvoiceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceType);
                
                TableSchema.TableColumn colvarInvoicePaperedTime = new TableSchema.TableColumn(schema);
                colvarInvoicePaperedTime.ColumnName = "invoice_papered_time";
                colvarInvoicePaperedTime.DataType = DbType.DateTime;
                colvarInvoicePaperedTime.MaxLength = 0;
                colvarInvoicePaperedTime.AutoIncrement = false;
                colvarInvoicePaperedTime.IsNullable = true;
                colvarInvoicePaperedTime.IsPrimaryKey = false;
                colvarInvoicePaperedTime.IsForeignKey = false;
                colvarInvoicePaperedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoicePaperedTime);
                
                TableSchema.TableColumn colvarRefund = new TableSchema.TableColumn(schema);
                colvarRefund.ColumnName = "refund";
                colvarRefund.DataType = DbType.String;
                colvarRefund.MaxLength = 4;
                colvarRefund.AutoIncrement = false;
                colvarRefund.IsNullable = false;
                colvarRefund.IsPrimaryKey = false;
                colvarRefund.IsForeignKey = false;
                colvarRefund.IsReadOnly = false;
                
                schema.Columns.Add(colvarRefund);
                
                TableSchema.TableColumn colvarMembername = new TableSchema.TableColumn(schema);
                colvarMembername.ColumnName = "membername";
                colvarMembername.DataType = DbType.String;
                colvarMembername.MaxLength = 100;
                colvarMembername.AutoIncrement = false;
                colvarMembername.IsNullable = true;
                colvarMembername.IsPrimaryKey = false;
                colvarMembername.IsForeignKey = false;
                colvarMembername.IsReadOnly = false;
                
                schema.Columns.Add(colvarMembername);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarUserEmail = new TableSchema.TableColumn(schema);
                colvarUserEmail.ColumnName = "user_email";
                colvarUserEmail.DataType = DbType.String;
                colvarUserEmail.MaxLength = 256;
                colvarUserEmail.AutoIncrement = false;
                colvarUserEmail.IsNullable = false;
                colvarUserEmail.IsPrimaryKey = false;
                colvarUserEmail.IsForeignKey = false;
                colvarUserEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserEmail);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                TableSchema.TableColumn colvarInvoiceBuyerName = new TableSchema.TableColumn(schema);
                colvarInvoiceBuyerName.ColumnName = "invoice_buyer_name";
                colvarInvoiceBuyerName.DataType = DbType.String;
                colvarInvoiceBuyerName.MaxLength = 50;
                colvarInvoiceBuyerName.AutoIncrement = false;
                colvarInvoiceBuyerName.IsNullable = true;
                colvarInvoiceBuyerName.IsPrimaryKey = false;
                colvarInvoiceBuyerName.IsForeignKey = false;
                colvarInvoiceBuyerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceBuyerName);
                
                TableSchema.TableColumn colvarInvoiceBuyerAddress = new TableSchema.TableColumn(schema);
                colvarInvoiceBuyerAddress.ColumnName = "invoice_buyer_address";
                colvarInvoiceBuyerAddress.DataType = DbType.String;
                colvarInvoiceBuyerAddress.MaxLength = 250;
                colvarInvoiceBuyerAddress.AutoIncrement = false;
                colvarInvoiceBuyerAddress.IsNullable = true;
                colvarInvoiceBuyerAddress.IsPrimaryKey = false;
                colvarInvoiceBuyerAddress.IsForeignKey = false;
                colvarInvoiceBuyerAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceBuyerAddress);
                
                TableSchema.TableColumn colvarInvoiceWinnerresponseTime = new TableSchema.TableColumn(schema);
                colvarInvoiceWinnerresponseTime.ColumnName = "invoice_winnerresponse_time";
                colvarInvoiceWinnerresponseTime.DataType = DbType.DateTime;
                colvarInvoiceWinnerresponseTime.MaxLength = 0;
                colvarInvoiceWinnerresponseTime.AutoIncrement = false;
                colvarInvoiceWinnerresponseTime.IsNullable = true;
                colvarInvoiceWinnerresponseTime.IsPrimaryKey = false;
                colvarInvoiceWinnerresponseTime.IsForeignKey = false;
                colvarInvoiceWinnerresponseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceWinnerresponseTime);
                
                TableSchema.TableColumn colvarInvoiceWinnerresponsePhone = new TableSchema.TableColumn(schema);
                colvarInvoiceWinnerresponsePhone.ColumnName = "invoice_winnerresponse_phone";
                colvarInvoiceWinnerresponsePhone.DataType = DbType.String;
                colvarInvoiceWinnerresponsePhone.MaxLength = 100;
                colvarInvoiceWinnerresponsePhone.AutoIncrement = false;
                colvarInvoiceWinnerresponsePhone.IsNullable = true;
                colvarInvoiceWinnerresponsePhone.IsPrimaryKey = false;
                colvarInvoiceWinnerresponsePhone.IsForeignKey = false;
                colvarInvoiceWinnerresponsePhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceWinnerresponsePhone);
                
                TableSchema.TableColumn colvarInvoiceSumAmount = new TableSchema.TableColumn(schema);
                colvarInvoiceSumAmount.ColumnName = "invoice_sum_amount";
                colvarInvoiceSumAmount.DataType = DbType.Currency;
                colvarInvoiceSumAmount.MaxLength = 0;
                colvarInvoiceSumAmount.AutoIncrement = false;
                colvarInvoiceSumAmount.IsNullable = false;
                colvarInvoiceSumAmount.IsPrimaryKey = false;
                colvarInvoiceSumAmount.IsForeignKey = false;
                colvarInvoiceSumAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceSumAmount);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarLoveCode = new TableSchema.TableColumn(schema);
                colvarLoveCode.ColumnName = "love_code";
                colvarLoveCode.DataType = DbType.String;
                colvarLoveCode.MaxLength = 8;
                colvarLoveCode.AutoIncrement = false;
                colvarLoveCode.IsNullable = true;
                colvarLoveCode.IsPrimaryKey = false;
                colvarLoveCode.IsForeignKey = false;
                colvarLoveCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarLoveCode);
                
                TableSchema.TableColumn colvarCarrierType = new TableSchema.TableColumn(schema);
                colvarCarrierType.ColumnName = "carrier_type";
                colvarCarrierType.DataType = DbType.Int32;
                colvarCarrierType.MaxLength = 0;
                colvarCarrierType.AutoIncrement = false;
                colvarCarrierType.IsNullable = false;
                colvarCarrierType.IsPrimaryKey = false;
                colvarCarrierType.IsForeignKey = false;
                colvarCarrierType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCarrierType);
                
                TableSchema.TableColumn colvarCarrierTypeDescription = new TableSchema.TableColumn(schema);
                colvarCarrierTypeDescription.ColumnName = "carrier_type_description";
                colvarCarrierTypeDescription.DataType = DbType.String;
                colvarCarrierTypeDescription.MaxLength = 5;
                colvarCarrierTypeDescription.AutoIncrement = false;
                colvarCarrierTypeDescription.IsNullable = false;
                colvarCarrierTypeDescription.IsPrimaryKey = false;
                colvarCarrierTypeDescription.IsForeignKey = false;
                colvarCarrierTypeDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarCarrierTypeDescription);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_einvoice_winners",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewEinvoiceWinner()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEinvoiceWinner(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewEinvoiceWinner(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewEinvoiceWinner(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("InvoiceNumber")]
        [Bindable(true)]
        public string InvoiceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_number");
		    }
            set 
		    {
			    SetColumnValue("invoice_number", value);
            }
        }
	      
        [XmlAttribute("InvoiceNumberTime")]
        [Bindable(true)]
        public DateTime? InvoiceNumberTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("invoice_number_time");
		    }
            set 
		    {
			    SetColumnValue("invoice_number_time", value);
            }
        }
	      
        [XmlAttribute("OrderTime")]
        [Bindable(true)]
        public DateTime OrderTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("order_time");
		    }
            set 
		    {
			    SetColumnValue("order_time", value);
            }
        }
	      
        [XmlAttribute("InvoiceMode2")]
        [Bindable(true)]
        public int InvoiceMode2 
	    {
		    get
		    {
			    return GetColumnValue<int>("invoice_mode2");
		    }
            set 
		    {
			    SetColumnValue("invoice_mode2", value);
            }
        }
	      
        [XmlAttribute("InvoiceType")]
        [Bindable(true)]
        public string InvoiceType 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_type");
		    }
            set 
		    {
			    SetColumnValue("invoice_type", value);
            }
        }
	      
        [XmlAttribute("InvoicePaperedTime")]
        [Bindable(true)]
        public DateTime? InvoicePaperedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("invoice_papered_time");
		    }
            set 
		    {
			    SetColumnValue("invoice_papered_time", value);
            }
        }
	      
        [XmlAttribute("Refund")]
        [Bindable(true)]
        public string Refund 
	    {
		    get
		    {
			    return GetColumnValue<string>("refund");
		    }
            set 
		    {
			    SetColumnValue("refund", value);
            }
        }
	      
        [XmlAttribute("Membername")]
        [Bindable(true)]
        public string Membername 
	    {
		    get
		    {
			    return GetColumnValue<string>("membername");
		    }
            set 
		    {
			    SetColumnValue("membername", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("UserEmail")]
        [Bindable(true)]
        public string UserEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_email");
		    }
            set 
		    {
			    SetColumnValue("user_email", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	      
        [XmlAttribute("InvoiceBuyerName")]
        [Bindable(true)]
        public string InvoiceBuyerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_buyer_name");
		    }
            set 
		    {
			    SetColumnValue("invoice_buyer_name", value);
            }
        }
	      
        [XmlAttribute("InvoiceBuyerAddress")]
        [Bindable(true)]
        public string InvoiceBuyerAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_buyer_address");
		    }
            set 
		    {
			    SetColumnValue("invoice_buyer_address", value);
            }
        }
	      
        [XmlAttribute("InvoiceWinnerresponseTime")]
        [Bindable(true)]
        public DateTime? InvoiceWinnerresponseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("invoice_winnerresponse_time");
		    }
            set 
		    {
			    SetColumnValue("invoice_winnerresponse_time", value);
            }
        }
	      
        [XmlAttribute("InvoiceWinnerresponsePhone")]
        [Bindable(true)]
        public string InvoiceWinnerresponsePhone 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_winnerresponse_phone");
		    }
            set 
		    {
			    SetColumnValue("invoice_winnerresponse_phone", value);
            }
        }
	      
        [XmlAttribute("InvoiceSumAmount")]
        [Bindable(true)]
        public decimal InvoiceSumAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("invoice_sum_amount");
		    }
            set 
		    {
			    SetColumnValue("invoice_sum_amount", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("LoveCode")]
        [Bindable(true)]
        public string LoveCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("love_code");
		    }
            set 
		    {
			    SetColumnValue("love_code", value);
            }
        }
	      
        [XmlAttribute("CarrierType")]
        [Bindable(true)]
        public int CarrierType 
	    {
		    get
		    {
			    return GetColumnValue<int>("carrier_type");
		    }
            set 
		    {
			    SetColumnValue("carrier_type", value);
            }
        }
	      
        [XmlAttribute("CarrierTypeDescription")]
        [Bindable(true)]
        public string CarrierTypeDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("carrier_type_description");
		    }
            set 
		    {
			    SetColumnValue("carrier_type_description", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string OrderId = @"order_id";
            
            public static string InvoiceNumber = @"invoice_number";
            
            public static string InvoiceNumberTime = @"invoice_number_time";
            
            public static string OrderTime = @"order_time";
            
            public static string InvoiceMode2 = @"invoice_mode2";
            
            public static string InvoiceType = @"invoice_type";
            
            public static string InvoicePaperedTime = @"invoice_papered_time";
            
            public static string Refund = @"refund";
            
            public static string Membername = @"membername";
            
            public static string UserName = @"user_name";
            
            public static string UserEmail = @"user_email";
            
            public static string Mobile = @"mobile";
            
            public static string InvoiceBuyerName = @"invoice_buyer_name";
            
            public static string InvoiceBuyerAddress = @"invoice_buyer_address";
            
            public static string InvoiceWinnerresponseTime = @"invoice_winnerresponse_time";
            
            public static string InvoiceWinnerresponsePhone = @"invoice_winnerresponse_phone";
            
            public static string InvoiceSumAmount = @"invoice_sum_amount";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string LoveCode = @"love_code";
            
            public static string CarrierType = @"carrier_type";
            
            public static string CarrierTypeDescription = @"carrier_type_description";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MobileMember class.
	/// </summary>
    [Serializable]
	public partial class MobileMemberCollection : RepositoryList<MobileMember, MobileMemberCollection>
	{	   
		public MobileMemberCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MobileMemberCollection</returns>
		public MobileMemberCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MobileMember o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the mobile_member table.
	/// </summary>
	[Serializable]
	
	public partial class MobileMember : RepositoryRecord<MobileMember>, IRecordBase
	
	{
		#region .ctors and Default Settings
		
		public MobileMember()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MobileMember(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("mobile_member", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
				colvarMobileNumber.ColumnName = "mobile_number";
				colvarMobileNumber.DataType = DbType.String;
				colvarMobileNumber.MaxLength = 10;
				colvarMobileNumber.AutoIncrement = false;
				colvarMobileNumber.IsNullable = false;
				colvarMobileNumber.IsPrimaryKey = false;
				colvarMobileNumber.IsForeignKey = false;
				colvarMobileNumber.IsReadOnly = false;
				colvarMobileNumber.DefaultSetting = @"";
				colvarMobileNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileNumber);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = true;
				colvarUserId.IsForeignKey = true;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				
					colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarIsLockedOut = new TableSchema.TableColumn(schema);
				colvarIsLockedOut.ColumnName = "is_locked_out";
				colvarIsLockedOut.DataType = DbType.Boolean;
				colvarIsLockedOut.MaxLength = 0;
				colvarIsLockedOut.AutoIncrement = false;
				colvarIsLockedOut.IsNullable = false;
				colvarIsLockedOut.IsPrimaryKey = false;
				colvarIsLockedOut.IsForeignKey = false;
				colvarIsLockedOut.IsReadOnly = false;
				colvarIsLockedOut.DefaultSetting = @"";
				colvarIsLockedOut.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsLockedOut);
				
				TableSchema.TableColumn colvarPassword = new TableSchema.TableColumn(schema);
				colvarPassword.ColumnName = "password";
				colvarPassword.DataType = DbType.String;
				colvarPassword.MaxLength = 128;
				colvarPassword.AutoIncrement = false;
				colvarPassword.IsNullable = true;
				colvarPassword.IsPrimaryKey = false;
				colvarPassword.IsForeignKey = false;
				colvarPassword.IsReadOnly = false;
				colvarPassword.DefaultSetting = @"";
				colvarPassword.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPassword);
				
				TableSchema.TableColumn colvarPasswordFormat = new TableSchema.TableColumn(schema);
				colvarPasswordFormat.ColumnName = "password_format";
				colvarPasswordFormat.DataType = DbType.Int32;
				colvarPasswordFormat.MaxLength = 0;
				colvarPasswordFormat.AutoIncrement = false;
				colvarPasswordFormat.IsNullable = false;
				colvarPasswordFormat.IsPrimaryKey = false;
				colvarPasswordFormat.IsForeignKey = false;
				colvarPasswordFormat.IsReadOnly = false;
				colvarPasswordFormat.DefaultSetting = @"";
				colvarPasswordFormat.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPasswordFormat);
				
				TableSchema.TableColumn colvarPasswordSalt = new TableSchema.TableColumn(schema);
				colvarPasswordSalt.ColumnName = "password_salt";
				colvarPasswordSalt.DataType = DbType.String;
				colvarPasswordSalt.MaxLength = 128;
				colvarPasswordSalt.AutoIncrement = false;
				colvarPasswordSalt.IsNullable = true;
				colvarPasswordSalt.IsPrimaryKey = false;
				colvarPasswordSalt.IsForeignKey = false;
				colvarPasswordSalt.IsReadOnly = false;
				colvarPasswordSalt.DefaultSetting = @"";
				colvarPasswordSalt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPasswordSalt);
				
				TableSchema.TableColumn colvarFailedPasswordAttemptCount = new TableSchema.TableColumn(schema);
				colvarFailedPasswordAttemptCount.ColumnName = "failed_password_attempt_count";
				colvarFailedPasswordAttemptCount.DataType = DbType.Int32;
				colvarFailedPasswordAttemptCount.MaxLength = 0;
				colvarFailedPasswordAttemptCount.AutoIncrement = false;
				colvarFailedPasswordAttemptCount.IsNullable = false;
				colvarFailedPasswordAttemptCount.IsPrimaryKey = false;
				colvarFailedPasswordAttemptCount.IsForeignKey = false;
				colvarFailedPasswordAttemptCount.IsReadOnly = false;
				colvarFailedPasswordAttemptCount.DefaultSetting = @"";
				colvarFailedPasswordAttemptCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailedPasswordAttemptCount);
				
				TableSchema.TableColumn colvarFailedPasswordAttemptWindowStart = new TableSchema.TableColumn(schema);
				colvarFailedPasswordAttemptWindowStart.ColumnName = "failed_password_attempt_window_start";
				colvarFailedPasswordAttemptWindowStart.DataType = DbType.DateTime;
				colvarFailedPasswordAttemptWindowStart.MaxLength = 0;
				colvarFailedPasswordAttemptWindowStart.AutoIncrement = false;
				colvarFailedPasswordAttemptWindowStart.IsNullable = false;
				colvarFailedPasswordAttemptWindowStart.IsPrimaryKey = false;
				colvarFailedPasswordAttemptWindowStart.IsForeignKey = false;
				colvarFailedPasswordAttemptWindowStart.IsReadOnly = false;
				colvarFailedPasswordAttemptWindowStart.DefaultSetting = @"";
				colvarFailedPasswordAttemptWindowStart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailedPasswordAttemptWindowStart);
				
				TableSchema.TableColumn colvarLastLoginDate = new TableSchema.TableColumn(schema);
				colvarLastLoginDate.ColumnName = "last_login_date";
				colvarLastLoginDate.DataType = DbType.DateTime;
				colvarLastLoginDate.MaxLength = 0;
				colvarLastLoginDate.AutoIncrement = false;
				colvarLastLoginDate.IsNullable = false;
				colvarLastLoginDate.IsPrimaryKey = false;
				colvarLastLoginDate.IsForeignKey = false;
				colvarLastLoginDate.IsReadOnly = false;
				colvarLastLoginDate.DefaultSetting = @"";
				colvarLastLoginDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastLoginDate);
				
				TableSchema.TableColumn colvarLastPasswordChangedDate = new TableSchema.TableColumn(schema);
				colvarLastPasswordChangedDate.ColumnName = "last_password_changed_date";
				colvarLastPasswordChangedDate.DataType = DbType.DateTime;
				colvarLastPasswordChangedDate.MaxLength = 0;
				colvarLastPasswordChangedDate.AutoIncrement = false;
				colvarLastPasswordChangedDate.IsNullable = false;
				colvarLastPasswordChangedDate.IsPrimaryKey = false;
				colvarLastPasswordChangedDate.IsForeignKey = false;
				colvarLastPasswordChangedDate.IsReadOnly = false;
				colvarLastPasswordChangedDate.DefaultSetting = @"";
				colvarLastPasswordChangedDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastPasswordChangedDate);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarActivedDate = new TableSchema.TableColumn(schema);
				colvarActivedDate.ColumnName = "actived_date";
				colvarActivedDate.DataType = DbType.DateTime;
				colvarActivedDate.MaxLength = 0;
				colvarActivedDate.AutoIncrement = false;
				colvarActivedDate.IsNullable = false;
				colvarActivedDate.IsPrimaryKey = false;
				colvarActivedDate.IsForeignKey = false;
				colvarActivedDate.IsReadOnly = false;
				colvarActivedDate.DefaultSetting = @"";
				colvarActivedDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActivedDate);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarResetPasswordKey = new TableSchema.TableColumn(schema);
				colvarResetPasswordKey.ColumnName = "reset_password_key";
				colvarResetPasswordKey.DataType = DbType.String;
				colvarResetPasswordKey.MaxLength = 8;
				colvarResetPasswordKey.AutoIncrement = false;
				colvarResetPasswordKey.IsNullable = true;
				colvarResetPasswordKey.IsPrimaryKey = false;
				colvarResetPasswordKey.IsForeignKey = false;
				colvarResetPasswordKey.IsReadOnly = false;
				colvarResetPasswordKey.DefaultSetting = @"";
				colvarResetPasswordKey.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResetPasswordKey);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("mobile_member",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("MobileNumber")]
		[Bindable(true)]
		public string MobileNumber 
		{
			get { return GetColumnValue<string>(Columns.MobileNumber); }
			set { SetColumnValue(Columns.MobileNumber, value); }
		}
		
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		[XmlAttribute("IsLockedOut")]
		[Bindable(true)]
		public bool IsLockedOut 
		{
			get { return GetColumnValue<bool>(Columns.IsLockedOut); }
			set { SetColumnValue(Columns.IsLockedOut, value); }
		}
		
		[XmlAttribute("Password")]
		[Bindable(true)]
		public string Password 
		{
			get { return GetColumnValue<string>(Columns.Password); }
			set { SetColumnValue(Columns.Password, value); }
		}
		
		[XmlAttribute("PasswordFormat")]
		[Bindable(true)]
		public int PasswordFormat 
		{
			get { return GetColumnValue<int>(Columns.PasswordFormat); }
			set { SetColumnValue(Columns.PasswordFormat, value); }
		}
		
		[XmlAttribute("PasswordSalt")]
		[Bindable(true)]
		public string PasswordSalt 
		{
			get { return GetColumnValue<string>(Columns.PasswordSalt); }
			set { SetColumnValue(Columns.PasswordSalt, value); }
		}
		
		[XmlAttribute("FailedPasswordAttemptCount")]
		[Bindable(true)]
		public int FailedPasswordAttemptCount 
		{
			get { return GetColumnValue<int>(Columns.FailedPasswordAttemptCount); }
			set { SetColumnValue(Columns.FailedPasswordAttemptCount, value); }
		}
		
		[XmlAttribute("FailedPasswordAttemptWindowStart")]
		[Bindable(true)]
		public DateTime FailedPasswordAttemptWindowStart 
		{
			get { return GetColumnValue<DateTime>(Columns.FailedPasswordAttemptWindowStart); }
			set { SetColumnValue(Columns.FailedPasswordAttemptWindowStart, value); }
		}
		
		[XmlAttribute("LastLoginDate")]
		[Bindable(true)]
		public DateTime LastLoginDate 
		{
			get { return GetColumnValue<DateTime>(Columns.LastLoginDate); }
			set { SetColumnValue(Columns.LastLoginDate, value); }
		}
		
		[XmlAttribute("LastPasswordChangedDate")]
		[Bindable(true)]
		public DateTime LastPasswordChangedDate 
		{
			get { return GetColumnValue<DateTime>(Columns.LastPasswordChangedDate); }
			set { SetColumnValue(Columns.LastPasswordChangedDate, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("ActivedDate")]
		[Bindable(true)]
		public DateTime ActivedDate 
		{
			get { return GetColumnValue<DateTime>(Columns.ActivedDate); }
			set { SetColumnValue(Columns.ActivedDate, value); }
		}
		
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		
		[XmlAttribute("ResetPasswordKey")]
		[Bindable(true)]
		public string ResetPasswordKey 
		{
			get { return GetColumnValue<string>(Columns.ResetPasswordKey); }
			set { SetColumnValue(Columns.ResetPasswordKey, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn MobileNumberColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IsLockedOutColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordFormatColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordSaltColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn FailedPasswordAttemptCountColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn FailedPasswordAttemptWindowStartColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn LastLoginDateColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn LastPasswordChangedDateColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ActivedDateColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ResetPasswordKeyColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string MobileNumber = @"mobile_number";
			 public static string UserId = @"user_id";
			 public static string IsLockedOut = @"is_locked_out";
			 public static string Password = @"password";
			 public static string PasswordFormat = @"password_format";
			 public static string PasswordSalt = @"password_salt";
			 public static string FailedPasswordAttemptCount = @"failed_password_attempt_count";
			 public static string FailedPasswordAttemptWindowStart = @"failed_password_attempt_window_start";
			 public static string LastLoginDate = @"last_login_date";
			 public static string LastPasswordChangedDate = @"last_password_changed_date";
			 public static string CreateTime = @"create_time";
			 public static string ActivedDate = @"actived_date";
			 public static string Status = @"status";
			 public static string ResetPasswordKey = @"reset_password_key";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewOrderNotYetShipped class.
    /// </summary>
    [Serializable]
    public partial class ViewOrderNotYetShippedCollection : ReadOnlyList<ViewOrderNotYetShipped, ViewOrderNotYetShippedCollection>
    {        
        public ViewOrderNotYetShippedCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_order_not_yet_shipped view.
    /// </summary>
    [Serializable]
    public partial class ViewOrderNotYetShipped : ReadOnlyRecord<ViewOrderNotYetShipped>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_order_not_yet_shipped", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarOrderCreateTime = new TableSchema.TableColumn(schema);
                colvarOrderCreateTime.ColumnName = "order_create_time";
                colvarOrderCreateTime.DataType = DbType.DateTime;
                colvarOrderCreateTime.MaxLength = 0;
                colvarOrderCreateTime.AutoIncrement = false;
                colvarOrderCreateTime.IsNullable = false;
                colvarOrderCreateTime.IsPrimaryKey = false;
                colvarOrderCreateTime.IsForeignKey = false;
                colvarOrderCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCreateTime);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
                colvarShipType.ColumnName = "ship_type";
                colvarShipType.DataType = DbType.Int32;
                colvarShipType.MaxLength = 0;
                colvarShipType.AutoIncrement = false;
                colvarShipType.IsNullable = true;
                colvarShipType.IsPrimaryKey = false;
                colvarShipType.IsForeignKey = false;
                colvarShipType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipType);
                
                TableSchema.TableColumn colvarShippingdateType = new TableSchema.TableColumn(schema);
                colvarShippingdateType.ColumnName = "shippingdate_type";
                colvarShippingdateType.DataType = DbType.Int32;
                colvarShippingdateType.MaxLength = 0;
                colvarShippingdateType.AutoIncrement = false;
                colvarShippingdateType.IsNullable = true;
                colvarShippingdateType.IsPrimaryKey = false;
                colvarShippingdateType.IsForeignKey = false;
                colvarShippingdateType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippingdateType);
                
                TableSchema.TableColumn colvarShippingdate = new TableSchema.TableColumn(schema);
                colvarShippingdate.ColumnName = "shippingdate";
                colvarShippingdate.DataType = DbType.Int32;
                colvarShippingdate.MaxLength = 0;
                colvarShippingdate.AutoIncrement = false;
                colvarShippingdate.IsNullable = true;
                colvarShippingdate.IsPrimaryKey = false;
                colvarShippingdate.IsForeignKey = false;
                colvarShippingdate.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippingdate);
                
                TableSchema.TableColumn colvarProductUseDateEndSet = new TableSchema.TableColumn(schema);
                colvarProductUseDateEndSet.ColumnName = "product_use_date_end_set";
                colvarProductUseDateEndSet.DataType = DbType.Int32;
                colvarProductUseDateEndSet.MaxLength = 0;
                colvarProductUseDateEndSet.AutoIncrement = false;
                colvarProductUseDateEndSet.IsNullable = true;
                colvarProductUseDateEndSet.IsPrimaryKey = false;
                colvarProductUseDateEndSet.IsForeignKey = false;
                colvarProductUseDateEndSet.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductUseDateEndSet);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                TableSchema.TableColumn colvarOpdId = new TableSchema.TableColumn(schema);
                colvarOpdId.ColumnName = "opd_id";
                colvarOpdId.DataType = DbType.Int32;
                colvarOpdId.MaxLength = 0;
                colvarOpdId.AutoIncrement = false;
                colvarOpdId.IsNullable = true;
                colvarOpdId.IsPrimaryKey = false;
                colvarOpdId.IsForeignKey = false;
                colvarOpdId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpdId);
                
                TableSchema.TableColumn colvarLastShipDate = new TableSchema.TableColumn(schema);
                colvarLastShipDate.ColumnName = "last_ship_date";
                colvarLastShipDate.DataType = DbType.DateTime;
                colvarLastShipDate.MaxLength = 0;
                colvarLastShipDate.AutoIncrement = false;
                colvarLastShipDate.IsNullable = true;
                colvarLastShipDate.IsPrimaryKey = false;
                colvarLastShipDate.IsForeignKey = false;
                colvarLastShipDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastShipDate);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_order_not_yet_shipped",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewOrderNotYetShipped()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewOrderNotYetShipped(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewOrderNotYetShipped(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewOrderNotYetShipped(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("OrderCreateTime")]
        [Bindable(true)]
        public DateTime OrderCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("order_create_time");
		    }
            set 
		    {
			    SetColumnValue("order_create_time", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("ShipType")]
        [Bindable(true)]
        public int? ShipType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ship_type");
		    }
            set 
		    {
			    SetColumnValue("ship_type", value);
            }
        }
	      
        [XmlAttribute("ShippingdateType")]
        [Bindable(true)]
        public int? ShippingdateType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("shippingdate_type");
		    }
            set 
		    {
			    SetColumnValue("shippingdate_type", value);
            }
        }
	      
        [XmlAttribute("Shippingdate")]
        [Bindable(true)]
        public int? Shippingdate 
	    {
		    get
		    {
			    return GetColumnValue<int?>("shippingdate");
		    }
            set 
		    {
			    SetColumnValue("shippingdate", value);
            }
        }
	      
        [XmlAttribute("ProductUseDateEndSet")]
        [Bindable(true)]
        public int? ProductUseDateEndSet 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_use_date_end_set");
		    }
            set 
		    {
			    SetColumnValue("product_use_date_end_set", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("OpdId")]
        [Bindable(true)]
        public int? OpdId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("opd_id");
		    }
            set 
		    {
			    SetColumnValue("opd_id", value);
            }
        }
	      
        [XmlAttribute("LastShipDate")]
        [Bindable(true)]
        public DateTime? LastShipDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("last_ship_date");
		    }
            set 
		    {
			    SetColumnValue("last_ship_date", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderGuid = @"order_guid";
            
            public static string OrderId = @"order_id";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string OrderCreateTime = @"order_create_time";
            
            public static string OrderStatus = @"order_status";
            
            public static string ShipType = @"ship_type";
            
            public static string ShippingdateType = @"shippingdate_type";
            
            public static string Shippingdate = @"shippingdate";
            
            public static string ProductUseDateEndSet = @"product_use_date_end_set";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
            public static string OpdId = @"opd_id";
            
            public static string LastShipDate = @"last_ship_date";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

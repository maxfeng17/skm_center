using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_family_net_verify_retry")]
	public partial class ViewFamilyNetVerifyRetry
    {
        
		[Key]
		[Column("peztemp_id")]
		public int PeztempId { get; set; }
        
		[Column("pincode_order_no")]
		public int? PincodeOrderNo { get; set; }
        
		[Column("verify_log_id")]
		public int VerifyLogId { get; set; }
             
    }
}
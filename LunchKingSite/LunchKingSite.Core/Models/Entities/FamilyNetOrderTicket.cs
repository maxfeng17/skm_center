using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("family_net_order_ticket")]
	public partial class FamilyNetOrderTicket
	{
        public FamilyNetOrderTicket()
        {
		this.ApiName = string.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("request_type")]
		public byte RequestType { get; set; }
        
		[Column("api_name")]
		public string ApiName { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
            
	}
}
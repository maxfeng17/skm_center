using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("deal_google_metadata")]
	public class DealGoogleMetadata
	{
		public DealGoogleMetadata()
		{
			Brand = string.Empty;
			Gtin = string.Empty;
			Mpn = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("bid")]
		public Guid Bid { get; set; }

		[Column("brand")]
		public string Brand { get; set; }

		[Column("gtin")]
		public string Gtin { get; set; }

		[Column("mpn")]
		public string Mpn { get; set; }
	}
}

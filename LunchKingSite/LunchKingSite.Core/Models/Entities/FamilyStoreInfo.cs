using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("family_store_info")]
	public partial class FamilyStoreInfo
	{
        public FamilyStoreInfo()
        {
		this.StoreCode = string.Empty;
		this.StoreName = string.Empty;
		this.Address = string.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("store_code")]
		public string StoreCode { get; set; }
        
		[Column("store_name")]
		public string StoreName { get; set; }
        
		[Column("address")]
		public string Address { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
            
	}
}
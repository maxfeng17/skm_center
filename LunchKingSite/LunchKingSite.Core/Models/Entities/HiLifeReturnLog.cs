using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("hi_life_return_log")]
	public partial class HiLifeReturnLog
	{
        public HiLifeReturnLog()
        {
		this.ReturnDate = String.Empty;
		this.Active = String.Empty;
		this.Reason = String.Empty;
		this.Pincode = String.Empty;
		this.IpAddress = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("batch_id")]
		public Guid? BatchId { get; set; }
        
		[Column("return_date")]
		public string ReturnDate { get; set; }
        
		[Column("active")]
		public string Active { get; set; }
        
		[Column("reason")]
		public string Reason { get; set; }
        
		[Column("pincode")]
		public string Pincode { get; set; }
        
		[Column("order_guid")]
		public Guid? OrderGuid { get; set; }
        
		[Column("ip_address")]
		public string IpAddress { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("hilife_return")]
		public string HilifeReturn { get; set; }
        
		[Column("return_type")]
		public byte ReturnType { get; set; }
            
	}
}
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("view_skm_pay_order_query_umall_gift")]
	public class ViewSkmPayOrderQueryUmallGift
	{
		public ViewSkmPayOrderQueryUmallGift()
		{
			OrderNo = string.Empty;
			ShopCode = string.Empty;
			BrandCounterCode = string.Empty;
			SellPosid = string.Empty;
			SellNo = string.Empty;
			SellInvoice = string.Empty;
			CompId = string.Empty;
		}

		[Column("id")]
		public int Id { get; set; }

		[Column("trust_id")]
		public Guid TrustId { get; set; }

		[Column("order_no")]
		public string OrderNo { get; set; }

		[Column("sell_day")]
		public DateTime? SellDay { get; set; }

		[Column("shop_code", TypeName = "varchar")]
		public string ShopCode { get; set; }

		[Column("brand_counter_code", TypeName = "varchar")]
		public string BrandCounterCode { get; set; }

		[Column("sell_posid", TypeName = "varchar")]
		public string SellPosid { get; set; }

		[Column("sell_no", TypeName = "varchar")]
		public string SellNo { get; set; }

		[Column("sell_invoice", TypeName = "varchar")]
		public string SellInvoice { get; set; }

		[Column("comp_id", TypeName = "varchar")]
		public string CompId { get; set; }
	}
}

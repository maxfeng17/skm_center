﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("skm_store_display_name")]
    public partial class SkmStoreDisplayName
    {
        public SkmStoreDisplayName()
        {
            this.DisplayName = String.Empty;
        }

        [Key]
        [Column("guid")]
        public Guid Guid { get; set; }

        [Column("exchange_type")]
        public int ExchangeType { get; set; }

        [Column("display_name")]
        public string DisplayName { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }

        [Column("create_user")]
        public int CreateUser { get; set; }

        [Column("modify_time")]
        public DateTime? ModifyTime { get; set; }

        [Column("modify_user")]
        public int? ModifyUser { get; set; }

        [Column("shop_code")]
        public string ShopCode { get; set; }

    }
}
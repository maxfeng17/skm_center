using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("gmc_avg_price_deal")]
	public partial class GmcAvgPriceDeal
	{
        public GmcAvgPriceDeal()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("bid")]
		public Guid Bid { get; set; }
            
	}
}
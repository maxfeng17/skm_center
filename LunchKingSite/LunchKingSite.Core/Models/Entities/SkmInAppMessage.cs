using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_in_app_message")]
	public partial class SkmInAppMessage
	{
        public SkmInAppMessage()
        {
		this.Subject = String.Empty;
		this.MsgContent = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("event_type")]
		public byte EventType { get; set; }
        
		[Column("url")]
		public string Url { get; set; }
        
		[Column("guid_val")]
		public Guid? GuidVal { get; set; }
        
		[Column("subject")]
		public string Subject { get; set; }
        
		[Column("msg_content")]
		public string MsgContent { get; set; }
        
		[Column("is_read")]
		public bool IsRead { get; set; }
        
		[Column("send_time")]
		public DateTime SendTime { get; set; }
        
		[Column("read_time")]
		public DateTime? ReadTime { get; set; }
        
		[Column("modify_time")]
		public DateTime ModifyTime { get; set; }
        
		[Column("msg_type")]
		public byte MsgType { get; set; }
        
		[Column("is_del")]
		public bool IsDel { get; set; }
            
	}
}
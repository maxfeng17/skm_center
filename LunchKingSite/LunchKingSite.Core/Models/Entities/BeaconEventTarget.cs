using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("beacon_event_target")]
	public partial class BeaconEventTarget
	{
        public BeaconEventTarget()
        {
		this.TargetUserToken = string.Empty;
		this.Remark = string.Empty;
        }
        
		[Key]
		[Column("target_id")]
		public int TargetId { get; set; }
        
		[Column("event_id")]
		public int EventId { get; set; }
        
		[Column("target_user_token")]
		public string TargetUserToken { get; set; }
        
		[Column("remark")]
		public string Remark { get; set; }
            
	}
}
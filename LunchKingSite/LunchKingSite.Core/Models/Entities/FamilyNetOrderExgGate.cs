using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("family_net_order_exg_gate")]
	public partial class FamilyNetOrderExgGate
	{
        public FamilyNetOrderExgGate()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("peztemp_id")]
		public int PeztempId { get; set; }
        
		[Column("processing")]
		public bool Processing { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("modify_time")]
		public DateTime ModifyTime { get; set; }
            
	}
}
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    [Table("skm_token_tracking")]
    public class SkmTokenTracking
    {
        public SkmTokenTracking()
        {
            ShaMemberId = string.Empty;
            MemberCardNo = string.Empty;
            MemberLevel = string.Empty;
            OriSkmToken = string.Empty;
            NewSkmToken = string.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("sha_member_id", TypeName = "varchar")]
        public string ShaMemberId { get; set; }

        [Column("member_card_no", TypeName = "varchar")]
        public string MemberCardNo { get; set; }

        [Column("member_level", TypeName = "varchar")]
        public string MemberLevel { get; set; }

        [Column("ori_skm_token", TypeName = "varchar")]
        public string OriSkmToken { get; set; }

        [Column("new_skm_token", TypeName = "varchar")]
        public string NewSkmToken { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }

        [Column("device_type")]
        public int? DeviceType { get; set; }
    }
}
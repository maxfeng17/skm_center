using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_pay_parking_order_trans_log")]
	public partial class SkmPayParkingOrderTransLog
	{
        public SkmPayParkingOrderTransLog()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("order_id")]
		public int OrderId { get; set; }
        
		[Column("order_status")]
		public int OrderStatus { get; set; }
        
		[Column("action")]
		public string Action { get; set; }
        
		[Column("is_success")]
		public bool IsSuccess { get; set; }
        
		[Column("request_object")]
		public string RequestObject { get; set; }
        
		[Column("response_object")]
		public string ResponseObject { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("memo")]
		public string Memo { get; set; }
            
	}
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("beacon_subevent")]
	public partial class BeaconSubevent
	{
        public BeaconSubevent()
        {
		this.Subject = string.Empty;
		this.Content = string.Empty;
		this.ActionUrl = string.Empty;
        }
        
		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("subevent_id")]
		public int SubeventId { get; set; }
        
		[Column("event_id")]
		public int EventId { get; set; }
        
		[Column("subject")]
		public string Subject { get; set; }
        
		[Column("content")]
		public string Content { get; set; }
        
		[Column("event_type")]
		public byte EventType { get; set; }
        
		[Column("action_bid")]
		public Guid ActionBid { get; set; }
        
		[Column("action_url")]
		public string ActionUrl { get; set; }
            
	}
}
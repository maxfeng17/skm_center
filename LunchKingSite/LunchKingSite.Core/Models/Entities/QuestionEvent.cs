using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("question_event")]
	public partial class QuestionEvent
	{
        public QuestionEvent()
        {
		this.Subject = String.Empty;
		this.QuestionContent = String.Empty;
		this.CreateId = String.Empty;
		this.QuestionToken = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("subject")]
		public string Subject { get; set; }
        
		[Column("question_content")]
		public string QuestionContent { get; set; }
        
		[Column("start_date")]
		public DateTime StartDate { get; set; }
        
		[Column("end_date")]
		public DateTime EndDate { get; set; }
        
		[Column("create_id")]
		public string CreateId { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("question_token")]
		public string QuestionToken { get; set; }
        
		[Column("login_mode")]
		public byte LoginMode { get; set; }
        
		[Column("disabled")]
		public bool Disabled { get; set; }
            
	}
}
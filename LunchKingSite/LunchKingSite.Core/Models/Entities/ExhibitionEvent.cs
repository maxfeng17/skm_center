using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("exhibition_event")]
	public partial class ExhibitionEvent
    {
        public ExhibitionEvent()
        {
            this.BannerPic = String.Empty;
            this.Subject = String.Empty;
            this.EventContent = String.Empty;
            this.EventRule = String.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("status")]
        public byte Status { get; set; }

        [Column("banner_pic")]
        public string BannerPic { get; set; }

        [Column("subject")]
        public string Subject { get; set; }

        [Column("event_content")]
        public string EventContent { get; set; }

        [Column("event_rule")]
        public string EventRule { get; set; }

        [Column("start_date")]
        public DateTime StartDate { get; set; }

        [Column("end_date")]
        public DateTime EndDate { get; set; }

        [Column("is_hq_event")]
        public bool IsHqEvent { get; set; }

        [Column("creater")]
        public int Creater { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }

        [Column("seq")]
        public int Seq { get; set; }

        [Column("exhibition_code_id")]
        public long ExhibitionCodeId { get; set; }

        [Column("event_type")]
        public byte EventType { get; set; }

        [Column("banner_link")]
        public string BannerLink { get; set; }
    }
}
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("skm_pay_order")]
	public class SkmPayOrder
	{
		public SkmPayOrder()
		{
			OrderNo = string.Empty;
			PreTransNo = string.Empty;
			PaymentToken = string.Empty;
			Memo = string.Empty;
			SkmTradeNo = string.Empty;
			ShopCode = string.Empty;
			CardHash = string.Empty;
			SkmRefundTradeNo = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("order_no")]
		public string OrderNo { get; set; }

		[Column("order_guid")]
		public Guid OrderGuid { get; set; }

		[Column("order_amount")]
		public int OrderAmount { get; set; }

		[Column("payment_amount")]
		public int PaymentAmount { get; set; }

		[Column("redeem_amount")]
		public int RedeemAmount { get; set; }

		[Column("redeem_point")]
		public int RedeemPoint { get; set; }

		[Column("pre_trans_no", TypeName = "varchar")]
		public string PreTransNo { get; set; }

		[Column("payment_token", TypeName = "varchar")]
		public string PaymentToken { get; set; }

		[Column("status")]
		public int Status { get; set; }

		[Column("memo")]
		public string Memo { get; set; }

		[Column("refund_date")]
		public DateTime? RefundDate { get; set; }

		[Column("create_date")]
		public DateTime CreateDate { get; set; }

		[Column("last_modify_date")]
		public DateTime? LastModifyDate { get; set; }

		[Column("skm_trade_no", TypeName = "varchar")]
		public string SkmTradeNo { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("external_deal_guid")]
		public Guid ExternalDealGuid { get; set; }

		[Column("main_bid")]
		public Guid MainBid { get; set; }

		[Column("bid")]
		public Guid Bid { get; set; }

		[Column("burning_skm_point")]
		public int BurningSkmPoint { get; set; }

		[Column("shop_code", TypeName = "varchar")]
		public string ShopCode { get; set; }

		[Column("card_hash", TypeName = "varchar")]
		public string CardHash { get; set; }

		[Column("skm_refund_trade_no", TypeName = "varchar")]
		public string SkmRefundTradeNo { get; set; }

		[Column("install_period")]
		public int InstallPeriod { get; set; }

		[Column("install_down_pay")]
		public int InstallDownPay { get; set; }

		[Column("install_each_pay")]
		public int InstallEachPay { get; set; }

		[Column("install_fee")]
		public int InstallFee { get; set; }
	}
}

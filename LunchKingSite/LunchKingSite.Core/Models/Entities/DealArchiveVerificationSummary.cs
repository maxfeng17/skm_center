﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    [Table("deal_archive_verification_summary")]
    public class DealArchiveVerificationSummary
    {
        public DealArchiveVerificationSummary()
        {
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("bid")]
        public Guid Bid { get; set; }

        [Column("unverified")]
        public int Unverified { get; set; }

        [Column("verified")]
        public int Verified { get; set; }

        [Column("returned")]
        public int Returned { get; set; }

        [Column("before_deal_end_returned")]
        public int BeforeDealEndReturned { get; set; }

        [Column("force_verified")]
        public int ForceVerified { get; set; }

        [Column("force_returned")]
        public int ForceReturned { get; set; }
    }
}

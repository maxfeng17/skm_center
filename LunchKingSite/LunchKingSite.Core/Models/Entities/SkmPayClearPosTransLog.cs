using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("skm_pay_clear_pos_trans_log")]
	public class SkmPayClearPosTransLog
	{
		public SkmPayClearPosTransLog()
		{
			RequestObject = string.Empty;
			ResponseObject = string.Empty;
			Memo = string.Empty;
			PlatFormId = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("sell_day")]
		public DateTime SellDay { get; set; }

		[Column("request_object")]
		public string RequestObject { get; set; }

		[Column("response_object")]
		public string ResponseObject { get; set; }

		[Column("is_success")]
		public bool IsSuccess { get; set; }

		[Column("create_date")]
		public DateTime CreateDate { get; set; }

		[Column("modify_date")]
		public DateTime ModifyDate { get; set; }

		[Column("is_retry")]
		public bool IsRetry { get; set; }

		[Column("memo")]
		public string Memo { get; set; }

		[Column("plat_form_id")]
		public string PlatFormId { get; set; }
	}
}

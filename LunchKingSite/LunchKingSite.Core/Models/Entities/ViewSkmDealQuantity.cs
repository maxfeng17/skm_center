﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("view_skm_Deal_quantity")]
    public partial class ViewSkmDealQuantity
    {

        [Key]
        [Column("external_deal_guid")]
        public Guid ExternalDealGuid { get; set; }

        [Column("shop_code")]
        public string ShopCode { get; set; }

        [Column("exchange_item_id")]
        public string ExchangeItemId { get; set; }

        [Column("skm_event_id")]
        public string SkmEventId { get; set; }

        [Column("bid")]
        public Guid? Bid { get; set; }

        [Column("ordered_quantity")]
        public int OrderedQuantity { get; set; }

        [Column("order_total_limit")]
        public int OrderTotalLimit { get; set; }
    }
}

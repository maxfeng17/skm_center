using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("question_answer")]
	public partial class QuestionAnswer
	{
        public QuestionAnswer()
        {
		this.IpAddress = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("event_id")]
		public int EventId { get; set; }
        
		[Column("answer_time")]
		public DateTime AnswerTime { get; set; }
        
		[Column("ip_address")]
		public string IpAddress { get; set; }
        
		[Column("order_from_type")]
		public byte OrderFromType { get; set; }
        
		[Column("sent_discount_code")]
		public bool SentDiscountCode { get; set; }
        
		[Column("sent_discount_time")]
		public DateTime? SentDiscountTime { get; set; }
            
	}
}
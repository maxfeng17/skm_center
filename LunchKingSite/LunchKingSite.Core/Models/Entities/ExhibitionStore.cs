using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("exhibition_store")]
	public partial class ExhibitionStore
	{
        public ExhibitionStore()
        {
        }
        
		[Key]
		[Column("store_guid")]
		public Guid StoreGuid { get; set; }
        
		[Key]
		[Column("event_id")]
		public int EventId { get; set; }
            
	}
}
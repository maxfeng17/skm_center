using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("external_deal_exhibition_code")]
	public partial class ExternalDealExhibitionCode
	{
        public ExternalDealExhibitionCode()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("external_deal_guid")]
		public Guid ExternalDealGuid { get; set; }
        
		[Column("exhibition_code_id")]
		public long ExhibitionCodeId { get; set; }
            
	}
}
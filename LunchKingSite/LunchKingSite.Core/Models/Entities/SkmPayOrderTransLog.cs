using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("skm_pay_order_trans_log")]
	public class SkmPayOrderTransLog
	{
		public SkmPayOrderTransLog()
		{
			ApiName = string.Empty;
			RequestObject = string.Empty;
			ResponseObject = string.Empty;
			Memo = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("order_id")]
		public int OrderId { get; set; }

		[Column("order_status")]
		public int OrderStatus { get; set; }

		[Column("api_name", TypeName = "varchar")]
		public string ApiName { get; set; }

		[Column("is_success")]
		public bool IsSuccess { get; set; }

		[Column("request_object")]
		public string RequestObject { get; set; }

		[Column("response_object")]
		public string ResponseObject { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("memo")]
		public string Memo { get; set; }
	}
}

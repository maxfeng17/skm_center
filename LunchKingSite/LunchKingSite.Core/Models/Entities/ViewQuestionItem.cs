using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_question_item")]
	public partial class ViewQuestionItem
    {
        
		[Key]
		[Column("item_id")]
		public int ItemId { get; set; }
        
		[Column("event_id")]
		public int EventId { get; set; }
        
		[Column("category_name")]
		public string CategoryName { get; set; }
        
		[Column("category_id")]
		public int CategoryId { get; set; }
        
		[Column("category_seq")]
		public int CategorySeq { get; set; }
        
		[Column("question_content")]
		public string QuestionContent { get; set; }
        
		[Column("question_type")]
		public byte QuestionType { get; set; }
        
		[Column("is_required")]
		public bool IsRequired { get; set; }
        
		[Column("seq")]
		public int Seq { get; set; }
        
		[Column("supplement")]
		public string Supplement { get; set; }
             
    }
}
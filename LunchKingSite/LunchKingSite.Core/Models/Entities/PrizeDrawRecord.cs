using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("prize_draw_record")]
	public partial class PrizeDrawRecord
	{
        public PrizeDrawRecord()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("draw_id")]
		public Guid DrawId { get; set; }
        
		[Column("bid")]
		public Guid Bid { get; set; }
        
		[Column("status")]
		public byte Status { get; set; }
        
		[Column("event_id")]
		public int EventId { get; set; }
        
		[Column("item_id")]
		public int ItemId { get; set; }
        
		[Column("qty")]
		public int Qty { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("order_guid")]
		public Guid? OrderGuid { get; set; }
        
		[Column("coupon_id")]
		public int? CouponId { get; set; }
        
		[Column("random_order_val")]
		public int RandomOrderVal { get; set; }
        
		[Column("random_index_val")]
		public int RandomIndexVal { get; set; }
        
		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }
        
		[Column("app_input_seller_id")]
		public Guid AppInputSellerId { get; set; }
        
		[Column("exchange_shop_code")]
		public string ExchangeShopCode { get; set; }
        
		[Column("is_sent")]
		public bool IsSent { get; set; }
            
	}
}
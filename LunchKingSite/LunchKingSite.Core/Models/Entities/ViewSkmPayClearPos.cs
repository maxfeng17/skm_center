﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("view_skm_pay_clear_pos")]
    public partial class ViewSkmPayClearPos
    {

        [Column("store_id")]
        public string StoreId { get; set; }

        [Column("plat_form_id")]
        public string PlatFormId { get; set; }

        [Key]
        [Column("skm_pay_order_id")]
        public int SkmPayOrderId { get; set; }

        [Column("sell_shopid")]
        public string SellShopid { get; set; }

        [Column("sell_posid")]
        public string SellPosid { get; set; }

        [Column("sell_day")]
        public DateTime SellDay { get; set; }

        [Column("sell_transtype")]
        public int SellTranstype { get; set; }

        [Column("skm_pay_order_detail_id")]
        public int SkmPayOrderDetailId { get; set; }

        [Column("pre_sales_status")]
        public int PreSalesStatus { get; set; }

        [Column("return_code")]
        public string ReturnCode { get; set; }

        [Column("nd_type")]
        public string NdType { get; set; }

        [Column("sell_rate")]
        public int SellRate { get; set; }

        [Column("good_price")]
        public int GoodPrice { get; set; }

        [Column("sell_tender")]
        public string SellTender { get; set; }

        [Column("tender_price")]
        public int TenderPrice { get; set; }

        [Column("point_deduct_total")]
        public int PointDeductTotal { get; set; }

    }
}
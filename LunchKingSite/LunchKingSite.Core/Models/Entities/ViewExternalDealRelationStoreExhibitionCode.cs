﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("view_external_deal_relation_store_exhibition_code")]
    public partial class ViewExternalDealRelationStoreExhibitionCode
    {
        [Column("event_name")]
        public string EventName { get; set; }

        [Key]
        [Column("event_guid")]
        public Guid EventGuid { get; set; }

        [Column("start_date")]
        public DateTime? StartDate { get; set; }

        [Column("end_date")]
        public DateTime? EndDate { get; set; }

        [Column("exhibition_code_id")]
        public long ExhibitionCodeId { get; set; }

        [Column("store_guid")]
        public Guid StoreGuid { get; set; }

        [Column("is_crm_deal")]
        public bool IsCrmDeal { get; set; }

    }
}
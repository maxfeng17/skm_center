using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
	[Table("skm_pay_invoice_tender_detail")]
	public partial class SkmPayInvoiceTenderDetail
	{
        public SkmPayInvoiceTenderDetail()
        {
		this.NdType = String.Empty;
		this.SellTender = String.Empty;
		this.SecNo = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("skm_pay_invoice_id")]
		public int SkmPayInvoiceId { get; set; }
        
		[Column("nd_type")]
		public string NdType { get; set; }
        
		[Column("sell_tender")]
		public string SellTender { get; set; }
        
		[Column("tender_price")]
		public int TenderPrice { get; set; }
        
		[Column("sec_no")]
		public string SecNo { get; set; }
        
		[Column("tx_amount")]
		public int? TxAmount { get; set; }
        
		[Column("card_no")]
		public string CardNo { get; set; }
        
		[Column("card_type")]
		public string CardType { get; set; }
        
		[Column("mid")]
		public string Mid { get; set; }
        
		[Column("terminal_id")]
		public string TerminalId { get; set; }
        
		[Column("approval_code")]
		public string ApprovalCode { get; set; }
        
		[Column("periods")]
		public int? Periods { get; set; }
        
		[Column("hand_amt")]
		public int? HandAmt { get; set; }
        
		[Column("each_amt")]
		public int? EachAmt { get; set; }
        
		[Column("use_bonus")]
		public bool? UseBonus { get; set; }

        [Column("card_no2")]
        public string CardNo2 { get; set; }

        [Column("bonus_use_point")]
		public int? BonusUsePoint { get; set; }
        
		[Column("bonus_point")]
		public int? BonusPoint { get; set; }
        
		[Column("bonus_dis_amt")]
		public int? BonusDisAmt { get; set; }

        [Column("pay_order_no")]
        public string PayOrderNo { get; set; }

        [Column("point_add_total")]
        public int PointAddTotal { get; set; }

        [Column("point_deduct_total")]
        public int PointDeductTotal { get; set; }

    }
}
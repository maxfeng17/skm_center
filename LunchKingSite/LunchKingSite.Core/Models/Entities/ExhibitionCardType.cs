using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("exhibition_card_type")]
    public partial class ExhibitionCardType
    {
        public ExhibitionCardType()
        {
        }

        [Key]
        [Column("exhibition_event_id")]
        public int ExhibitionEventId { get; set; }

        [Key]
        [Column("card_type")]
        public byte CardType { get; set; }

    }
}
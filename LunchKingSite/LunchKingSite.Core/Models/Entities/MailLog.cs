using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("[mail_log]")]
	public class MailLog
	{
		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("sender")]
		public string Sender { get; set; }

		[Column("receiver")]
		public string Receiver { get; set; }

		[Column("subject")]
		public string Subject { get; set; }

		[Column("category")]
		public int Category { get; set; }

		[Column("mail_content_log_id")]
		public int MailContentLogId { get; set; }

		[Column("result")]
		public bool Result { get; set; }

		[Column("last_error")]
		public string LastError { get; set; }

		[Column("resend_time")]
		public DateTime? ResendTime { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}

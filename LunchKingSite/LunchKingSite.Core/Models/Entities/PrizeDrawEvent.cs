using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("prize_draw_event")]
    public partial class PrizeDrawEvent
    {
        public PrizeDrawEvent()
        {
            this.Token = String.Empty;
            this.Subject = String.Empty;
            this.PrizeImg = String.Empty;
            this.FullImg = String.Empty;
            this.SmallImg = String.Empty;
            this.WinningImg = String.Empty;
            this.WinningBackgroundImg = String.Empty;
            this.ModifyId = String.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("token")]
        public string Token { get; set; }

        [Column("channel_odm_id")]
        public int ChannelOdmId { get; set; }

        [Column("start_date")]
        public DateTime StartDate { get; set; }

        [Column("end_date")]
        public DateTime EndDate { get; set; }

        [Column("subject")]
        public string Subject { get; set; }

        [Column("event_content")]
        public string EventContent { get; set; }

        [Column("join_fee")]
        public int JoinFee { get; set; }

        [Column("daily_limit")]
        public int DailyLimit { get; set; }

        [Column("total_limit")]
        public int TotalLimit { get; set; }

        [Column("temple")]
        public int Temple { get; set; }

        [Column("status")]
        public byte Status { get; set; }

        [Column("prize_img")]
        public string PrizeImg { get; set; }

        [Column("full_img")]
        public string FullImg { get; set; }

        [Column("small_img")]
        public string SmallImg { get; set; }

        [Column("winning_img")]
        public string WinningImg { get; set; }

        [Column("winning_background_img")]
        public string WinningBackgroundImg { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }

        [Column("modify_time")]
        public DateTime ModifyTime { get; set; }

        [Column("modify_id")]
        public string ModifyId { get; set; }

        [Column("seller_guid")]
        public Guid? SellerGuid { get; set; }

        [Column("notice_date")]
        public DateTime? NoticeDate { get; set; }

        [Column("period_hour")]
        public int PeriodHour { get; set; }

        [Column("notice")]
        public string Notice { get; set; }

    }
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("exhibition_user_group_member_token")]
	public partial class ExhibitionUserGroupMemberToken
	{
        public ExhibitionUserGroupMemberToken()
        {
		this.SkmMemberToken = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("exhibition_user_group_id")]
		public int ExhibitionUserGroupId { get; set; }
        
		[Column("skm_member_token")]
		public string SkmMemberToken { get; set; }
            
	}
}
﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("prize_draw_item_period")]
    public partial class PrizeDrawItemPeriod
    {
        public PrizeDrawItemPeriod()
        {
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("prize_draw_item_id")]
        public int PrizeDrawItemId { get; set; }

        [Column("start_date")]
        public DateTime StartDate { get; set; }

        [Column("end_date")]
        public DateTime EndDate { get; set; }

        [Column("quantity")]
        public int Quantity { get; set; }

        [Column("is_active")]
        public bool IsActive { get; set; }

        [Column("create_date")]
        public DateTime CreateDate { get; set; }

        [Column("modify_date")]
        public DateTime ModifyDate { get; set; }

    }
}
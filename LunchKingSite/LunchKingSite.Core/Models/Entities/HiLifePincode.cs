using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("hi_life_pincode")]
	public partial class HiLifePincode
	{
        public HiLifePincode()
        {
		this.Pincode = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("batch_id")]
		public Guid? BatchId { get; set; }
        
		[Column("peztemp_id")]
		public int PeztempId { get; set; }
        
		[Column("order_guid")]
		public Guid? OrderGuid { get; set; }
        
		[Column("pincode")]
		public string Pincode { get; set; }
        
		[Column("coupon_id")]
		public int CouponId { get; set; }
        
		[Column("view_count")]
		public int ViewCount { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }

        [Column("modify_time")]
        public DateTime ModifyTime { get; set; }

        [Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("return_status")]
		public byte ReturnStatus { get; set; }
        
		[Column("is_verified")]
		public bool IsVerified { get; set; }
            
	}
}
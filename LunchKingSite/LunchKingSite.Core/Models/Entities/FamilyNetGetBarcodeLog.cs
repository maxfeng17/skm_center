using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("family_net_get_barcode_log")]
	public partial class FamilyNetGetBarcodeLog
	{
        public FamilyNetGetBarcodeLog()
        {
		this.Pezcode = string.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("batch_id")]
		public Guid BatchId { get; set; }
        
		[Column("order_guid")]
		public Guid OrderGuid { get; set; }
        
		[Column("request_time")]
		public DateTime RequestTime { get; set; }
        
		[Column("request_qty")]
		public int RequestQty { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("order_no")]
		public int OrderNo { get; set; }
        
		[Column("coupon_id")]
		public int CouponId { get; set; }
        
		[Column("pezcode")]
		public string Pezcode { get; set; }
        
		[Column("barcode1")]
		public string Barcode1 { get; set; }
        
		[Column("barcode2")]
		public string Barcode2 { get; set; }
        
		[Column("barcode3")]
		public string Barcode3 { get; set; }
        
		[Column("barcode4")]
		public string Barcode4 { get; set; }
            
	}
}
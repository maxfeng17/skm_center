using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("hi_life_verify_log")]
	public partial class HiLifeVerifyLog
	{
        public HiLifeVerifyLog()
        {
		this.ExchangeTranTime = String.Empty;
		this.StoreCode = String.Empty;
		this.MachineCode = String.Empty;
		this.ExchangeDate = String.Empty;
		this.InvoiceNumber = String.Empty;
		this.IsSuccessFlag = String.Empty;
		this.Pincode = String.Empty;
		this.IpAddress = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("batch_id")]
		public Guid? BatchId { get; set; }
        
		[Column("exchange_tran_time")]
		public string ExchangeTranTime { get; set; }
        
		[Column("store_code")]
		public string StoreCode { get; set; }
        
		[Column("machine_code")]
		public string MachineCode { get; set; }
        
		[Column("exchange_date")]
		public string ExchangeDate { get; set; }
        
		[Column("invoice_number")]
		public string InvoiceNumber { get; set; }
        
		[Column("is_success_flag")]
		public string IsSuccessFlag { get; set; }
        
		[Column("is_success")]
		public bool IsSuccess { get; set; }
        
		[Column("pincode")]
		public string Pincode { get; set; }
        
		[Column("order_guid")]
		public Guid? OrderGuid { get; set; }
        
		[Column("batch_file_name")]
		public string BatchFileName { get; set; }
        
		[Column("ip_address")]
		public string IpAddress { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("remark")]
		public string Remark { get; set; }
        
		[Column("hilife_input")]
		public string HilifeInput { get; set; }
        
		[Column("verify_type")]
        public int VerifyType { get; set; }

    }
}
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("[mail_content_log]")]
	public class MailContentLog
	{
		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("content")]
		public string Content { get; set; }
	}
}

using System; 
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_prize_external_deal")]
	public partial class ViewPrizeExternalDeal
    {
        
		[Key]
		[Column("Guid")]
		public Guid Guid { get; set; }
        
		[Column("active_type")]
		public int ActiveType { get; set; }
        
		[Column("title")]
		public string Title { get; set; }
        
		[Column("product_code")]
		public string ProductCode { get; set; }
        
		[Column("item_orig_price")]
		public decimal? ItemOrigPrice { get; set; }
        
		[Column("discount_type")]
		public int DiscountType { get; set; }
        
		[Column("discount")]
		public int Discount { get; set; }
        
		[Column("description")]
		public string Description { get; set; }
        
		[Column("introduction")]
		public string Introduction { get; set; }
        
		[Column("category_list")]
		public string CategoryList { get; set; }
        
		[Column("tag_list")]
		public string TagList { get; set; }
        
		[Column("business_hour_order_time_s")]
		public DateTime BusinessHourOrderTimeS { get; set; }
        
		[Column("business_hour_order_time_e")]
		public DateTime BusinessHourOrderTimeE { get; set; }
        
		[Column("business_hour_deliver_time_s")]
		public DateTime? BusinessHourDeliverTimeS { get; set; }
        
		[Column("business_hour_deliver_time_e")]
		public DateTime? BusinessHourDeliverTimeE { get; set; }
        
		[Column("settlement_time")]
		public DateTime? SettlementTime { get; set; }
        
		[Column("beacon_type")]
		public int BeaconType { get; set; }
        
		[Column("beacon_message")]
		public string BeaconMessage { get; set; }
        
		[Column("status")]
		public int Status { get; set; }
        
		[Column("create_id")]
		public string CreateId { get; set; }
        
		[Column("create_time")]
		public DateTime? CreateTime { get; set; }
        
		[Column("modify_id")]
		public string ModifyId { get; set; }
        
		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }
        
		[Column("come_from_type")]
		public int ComeFromType { get; set; }
        
		[Column("item_no")]
		public string ItemNo { get; set; }
        
		[Column("remark")]
		public string Remark { get; set; }
        
		[Column("special_item_no")]
		public string SpecialItemNo { get; set; }
        
		[Column("image_path")]
		public string ImagePath { get; set; }
        
		[Column("app_deal_pic")]
		public string AppDealPic { get; set; }
        
		[Column("buy")]
		public int? Buy { get; set; }
        
		[Column("free")]
		public int? Free { get; set; }
        
		[Column("is_hq_deal")]
		public bool IsHqDeal { get; set; }
        
		[Column("is_prize_deal")]
		public bool IsPrizeDeal { get; set; }
        
		[Column("order_total_limit")]
		public int OrderTotalLimit { get; set; }
        
		[Column("bid")]
		public Guid? Bid { get; set; }
        
		[Column("burning_point")]
		public int BurningPoint { get; set; }
        
		[Column("ordered_quantity")]
		public int OrderedQuantity { get; set; }
        
		[Column("remaining_quantity")]
		public int? RemainingQuantity { get; set; }
             
    }
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("family_net_return_log")]
	public partial class FamilyNetReturnLog
	{
        public FamilyNetReturnLog()
        {
		this.OrderDate = string.Empty;
		this.ActCode = string.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("order_date")]
		public string OrderDate { get; set; }
        
		[Column("act_code")]
		public string ActCode { get; set; }
        
		[Column("status")]
		public string Status { get; set; }
        
		[Column("create_time")]
		public DateTime? CreateTime { get; set; }
        
		[Column("reply_time")]
		public DateTime? ReplyTime { get; set; }
        
		[Column("reply_code")]
		public string ReplyCode { get; set; }
        
		[Column("reply_desc")]
		public string ReplyDesc { get; set; }
        
		[Column("order_guid")]
		public Guid? OrderGuid { get; set; }
        
		[Column("coupon_id")]
		public int? CouponId { get; set; }
        
		[Column("return_type")]
		public byte ReturnType { get; set; }
            
	}
}
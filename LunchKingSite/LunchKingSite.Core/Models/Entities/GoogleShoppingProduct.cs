﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    [Table("google_shopping_product")]
    public class GoogleShoppingProduct
    {
        public GoogleShoppingProduct()
        {
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("bid")]
        public Guid Bid { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("link")]
        public string Link { get; set; }

        [Column("image_link")]
        public string ImageLink { get; set; }

        [Column("brand")]
        public string Brand { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("gtin")]
        public string Gtin { get; set; }

        [Column("mpn")]
        public string Mpn { get; set; }

        [Column("quality_issue")]
        public string QualityIssue { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }

        [Column("approved")]
        public bool? Approved { get; set; }
    }
}

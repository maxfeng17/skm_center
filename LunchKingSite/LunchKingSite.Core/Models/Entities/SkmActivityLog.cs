using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_activity_log")]
	public partial class SkmActivityLog
	{
        public SkmActivityLog()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("activity_id")]
		public int ActivityId { get; set; }
        
		[Column("item_id")]
		public int ItemId { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("order_guid")]
		public Guid OrderGuid { get; set; }
        
		[Column("coupon_id")]
		public int CouponId { get; set; }
            
	}
}
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    [Serializable]
	[Table("family_net_event")]
	public class FamilyNetEvent
	{
		public FamilyNetEvent()
		{
			ActCode = string.Empty;
			ItemCode = string.Empty;
			ItemName = string.Empty;
			Remark = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("main_business_hour_guid")]
		public Guid MainBusinessHourGuid { get; set; }

		[Column("business_hour_guid")]
		public Guid BusinessHourGuid { get; set; }

		[Column("act_code")]
		public string ActCode { get; set; }

		[Column("item_code", TypeName = "varchar")]
		public string ItemCode { get; set; }

		[Column("item_name")]
		public string ItemName { get; set; }

		[Column("item_orig_price")]
		public decimal ItemOrigPrice { get; set; }

		[Column("item_price")]
		public decimal ItemPrice { get; set; }

		[Column("purchase_price")]
		public decimal PurchasePrice { get; set; }

		[Column("max_item_count")]
		public int MaxItemCount { get; set; }

		[Column("buy")]
		public int Buy { get; set; }

		[Column("free")]
		public int Free { get; set; }

		[Column("business_hour_order_time_s")]
		public DateTime BusinessHourOrderTimeS { get; set; }

		[Column("business_hour_order_time_e")]
		public DateTime BusinessHourOrderTimeE { get; set; }

		[Column("business_hour_deliver_time_s")]
		public DateTime BusinessHourDeliverTimeS { get; set; }

		[Column("business_hour_deliver_time_e")]
		public DateTime BusinessHourDeliverTimeE { get; set; }

		[Column("remark")]
		public string Remark { get; set; }

		[Column("create_id")]
		public int CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_time")]
		public DateTime ModifyTime { get; set; }

		[Column("status")]
		public int Status { get; set; }

		[Column("is_combo_deal")]
		public bool IsComboDeal { get; set; }

		[Column("combo_deal_main_id")]
		public int ComboDealMainId { get; set; }

		[Column("version")]
		public short Version { get; set; }
	}
}

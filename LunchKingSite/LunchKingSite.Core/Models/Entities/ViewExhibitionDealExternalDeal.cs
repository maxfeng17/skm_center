using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_exhibition_deal_external_deal")]
	public partial class ViewExhibitionDealExternalDeal
    {
        
		[Key]
		[Column("exhibition_event_id")]
		public int ExhibitionEventId { get; set; }
        
		[Column("category_id")]
		public int CategoryId { get; set; }
        
		[Column("external_deal_guid")]
		public Guid ExternalDealGuid { get; set; }
        
		[Column("start_date")]
		public DateTime? StartDate { get; set; }
        
		[Column("end_date")]
		public DateTime? EndDate { get; set; }
             
    }
}
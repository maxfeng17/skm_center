using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("question_answer_discount_log")]
	public partial class QuestionAnswerDiscountLog
	{
        public QuestionAnswerDiscountLog()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("answer_id")]
		public int AnswerId { get; set; }
        
		[Column("event_discount_id")]
		public int EventDiscountId { get; set; }
        
		[Column("discount_campaign")]
		public int DiscountCampaign { get; set; }
        
		[Column("discount_code_id")]
		public int DiscountCodeId { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
            
	}
}
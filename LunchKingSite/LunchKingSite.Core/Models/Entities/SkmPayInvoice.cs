using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_pay_invoice")]
	public partial class SkmPayInvoice
	{
        public SkmPayInvoice()
        {
		this.SellShopid = String.Empty;
		this.SellOrderNo = String.Empty;
		this.SellPosid = String.Empty;
		this.SellNo = String.Empty;
		this.VipNo = String.Empty;
		this.EuiVehicleTypeNo = String.Empty;
		this.SellCasher = String.Empty;
		this.PlatFormId = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("skm_pay_order_id")]
		public int SkmPayOrderId { get; set; }
        
		[Column("sell_shopid")]
		public string SellShopid { get; set; }
        
		[Column("sell_order_no")]
		public string SellOrderNo { get; set; }
        
		[Column("sell_posid")]
		public string SellPosid { get; set; }
        
		[Column("sell_no")]
		public string SellNo { get; set; }
        
		[Column("sell_day")]
		public DateTime SellDay { get; set; }
        
		[Column("tday")]
		public DateTime Tday { get; set; }
        
		[Column("total_amt")]
		public int TotalAmt { get; set; }
        
		[Column("total_sale_amt")]
		public int TotalSaleAmt { get; set; }
        
		[Column("total_qty")]
		public int TotalQty { get; set; }
        
		[Column("sell_invoice")]
		public string SellInvoice { get; set; }
        
		[Column("sell_transtype")]
		public int SellTranstype { get; set; }
        
		[Column("vip_no")]
		public string VipNo { get; set; }
        
		[Column("comp_id")]
		public string CompId { get; set; }
        
		[Column("sell_org_day")]
		public DateTime? SellOrgDay { get; set; }
        
		[Column("sell_org_no")]
		public string SellOrgNo { get; set; }
        
		[Column("sell_org_posid")]
		public string SellOrgPosid { get; set; }
        
		[Column("eui_vehicle_no")]
		public string EuiVehicleNo { get; set; }
        
		[Column("eui_vehicle_no_hidden")]
		public string EuiVehicleNoHidden { get; set; }
        
		[Column("eui_donate_no")]
		public string EuiDonateNo { get; set; }
        
		[Column("eui_donate_status")]
		public bool EuiDonateStatus { get; set; }
        
		[Column("eui_universal_status")]
		public bool EuiUniversalStatus { get; set; }
        
		[Column("eui_vehicle_type_no")]
		public string EuiVehicleTypeNo { get; set; }
        
		[Column("sell_casher")]
		public string SellCasher { get; set; }
        
		[Column("eui_print_cnt")]
		public int EuiPrintCnt { get; set; }
        
		[Column("parking_token")]
		public string ParkingToken { get; set; }
        
		[Column("pre_sales_status")]
		public int PreSalesStatus { get; set; }
        
		[Column("create_date")]
		public DateTime CreateDate { get; set; }
        
		[Column("last_modify_date")]
		public DateTime? LastModifyDate { get; set; }
        
		[Column("memo")]
		public string Memo { get; set; }
        
		[Column("disc_amt")]
		public int DiscAmt { get; set; }
        
		[Column("rate_amt")]
		public int RateAmt { get; set; }
        
		[Column("extra_amt")]
		public int ExtraAmt { get; set; }
        
		[Column("return_code")]
		public string ReturnCode { get; set; }
        
		[Column("return_message")]
		public string ReturnMessage { get; set; }
        
		[Column("store_id")]
		public string StoreId { get; set; }
        
		[Column("plat_form_id")]
		public string PlatFormId { get; set; }
            
	}
}
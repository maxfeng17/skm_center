using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("skm_pay_invoice_sell_detail")]
    public partial class SkmPayInvoiceSellDetail
    {
        public SkmPayInvoiceSellDetail()
        {
            this.NdType = String.Empty;
            this.SellOrderNoS = String.Empty;
            this.ProductId = String.Empty;
            this.GoodsName = String.Empty;
            this.PmtType = String.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("skm_pay_invoice_id")]
        public int SkmPayInvoiceId { get; set; }

        [Column("sell_id")]
        public string SellId { get; set; }

        [Column("nd_type")]
        public string NdType { get; set; }

        [Column("sell_order_no_s")]
        public string SellOrderNoS { get; set; }

        [Column("sell_qty")]
        public int SellQty { get; set; }

        [Column("sell_price")]
        public int SellPrice { get; set; }

        [Column("sell_discount")]
        public int SellDiscount { get; set; }

        [Column("sell_rate")]
        public int SellRate { get; set; }

        [Column("sell_rate_amt")]
        public int SellRateAmt { get; set; }

        [Column("sell_amt")]
        public int SellAmt { get; set; }

        [Column("product_id")]
        public string ProductId { get; set; }

        [Column("sell_ori_price")]
        public int SellOriPrice { get; set; }

        [Column("goods_name")]
        public string GoodsName { get; set; }

        [Column("product_discount_type")]
        public int ProductDiscountType { get; set; }

        [Column("pmt_type")]
        public string PmtType { get; set; }

        [Column("point_add")]
        public int PointAdd { get; set; }

        [Column("point_deduct")]
        public int PointDeduct { get; set; }

        [Column("store_id")]
        public string StoreId { get; set; }

    }
}
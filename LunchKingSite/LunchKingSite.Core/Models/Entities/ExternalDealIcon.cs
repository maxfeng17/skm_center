using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("external_deal_icon")]
    public partial class ExternalDealIcon
    {
        public ExternalDealIcon()
        {
            this.IconName = String.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("icon_name")]
        public string IconName { get; set; }

        [Column("style_id")]
        public int StyleId { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }

    }
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("exhibition_activity")]
	public partial class ExhibitionActivity
	{
        public ExhibitionActivity()
        {
		this.BannerPic = String.Empty;
		this.EventLink = String.Empty;
        }
        
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("exhibition_event_id")]
        public int ExhibitionEventId { get; set; }

        [Column("banner_pic")]
        public string BannerPic { get; set; }

        [Column("event_link")]
        public string EventLink { get; set; }

        [Column("Seq")]
        public int Seq { get; set; }
        
    }
}
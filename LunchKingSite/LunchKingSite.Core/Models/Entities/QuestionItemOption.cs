using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("question_item_option")]
	public partial class QuestionItemOption
	{
        public QuestionItemOption()
        {
		this.OptionCode = String.Empty;
		this.OptionContent = String.Empty;
		this.Supplement = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("item_id")]
		public int ItemId { get; set; }
        
		[Column("option_value")]
		public int OptionValue { get; set; }
        
		[Column("option_code")]
		public string OptionCode { get; set; }
        
		[Column("option_content")]
		public string OptionContent { get; set; }
        
		[Column("seq")]
		public int Seq { get; set; }
        
		[Column("jump_item_id")]
		public int JumpItemId { get; set; }
        
		[Column("supplement")]
		public string Supplement { get; set; }
        
		[Column("other_input")]
		public bool OtherInput { get; set; }
            
	}
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("beacon_field_device_link")]
	public partial class BeaconFieldDeviceLink
	{
        public BeaconFieldDeviceLink()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("field_id")]
		public int FieldId { get; set; }
        
		[Column("device_id")]
		public int DeviceId { get; set; }
            
	}
}
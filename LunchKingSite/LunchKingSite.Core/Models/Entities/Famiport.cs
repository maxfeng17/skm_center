using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("famiport")]
	public partial class Famiport
	{
        public Famiport()
        {
		this.EventId = string.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("business_hour_guid")]
		public Guid BusinessHourGuid { get; set; }
        
		[Column("event_id")]
		public string EventId { get; set; }
        
		[Column("password")]
		public string Password { get; set; }
        
		[Column("type")]
		public int Type { get; set; }
        
		[Column("status")]
		public int Status { get; set; }
        
		[Column("remark")]
		public string Remark { get; set; }
        
		[Column("create_id")]
		public string CreateId { get; set; }
        
		[Column("create_time")]
		public DateTime? CreateTime { get; set; }
        
		[Column("latest_update_time")]
		public DateTime? LatestUpdateTime { get; set; }
        
		[Column("is_import_sequencenumber")]
		public bool IsImportSequencenumber { get; set; }
        
		[Column("vendor_id")]
		public string VendorId { get; set; }
        
		[Column("event_attention_1")]
		public string EventAttention1 { get; set; }
        
		[Column("event_attention_2")]
		public string EventAttention2 { get; set; }
        
		[Column("event_attention_3")]
		public string EventAttention3 { get; set; }
        
		[Column("event_attention_4")]
		public string EventAttention4 { get; set; }
        
		[Column("item_name")]
		public string ItemName { get; set; }
        
		[Column("item_code")]
		public string ItemCode { get; set; }
        
		[Column("pincode_qty")]
		public int PincodeQty { get; set; }
        
		[Column("business_hour_order_time_s")]
		public DateTime? BusinessHourOrderTimeS { get; set; }
        
		[Column("business_hour_order_time_e")]
		public DateTime? BusinessHourOrderTimeE { get; set; }
        
		[Column("business_hour_deliver_time_s")]
		public DateTime? BusinessHourDeliverTimeS { get; set; }
        
		[Column("business_hour_deliver_time_e")]
		public DateTime? BusinessHourDeliverTimeE { get; set; }
        
		[Column("item_orig_price")]
		public decimal ItemOrigPrice { get; set; }
        
		[Column("item_price")]
		public decimal ItemPrice { get; set; }
        
		[Column("item_attention_1")]
		public string ItemAttention1 { get; set; }
        
		[Column("item_attention_2")]
		public string ItemAttention2 { get; set; }
        
		[Column("item_attention_3")]
		public string ItemAttention3 { get; set; }
        
		[Column("item_attention_4")]
		public string ItemAttention4 { get; set; }
        
		[Column("item_remark")]
		public string ItemRemark { get; set; }
        
		[Column("is_out_of_date")]
		public bool IsOutOfDate { get; set; }
        
		[Column("is_produced_code")]
		public bool IsProducedCode { get; set; }
        
		[Column("test_code_qty")]
		public int TestCodeQty { get; set; }
        
		[Column("display_type")]
		public int DisplayType { get; set; }
        
		[Column("is_lock")]
		public bool IsLock { get; set; }
            
	}
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("beacon_group")]
	public partial class BeaconGroup
	{
        public BeaconGroup()
        {
		this.GroupCode = string.Empty;
		this.GroupRemark = string.Empty;
		this.AuxiliaryCode = string.Empty;
        }
        
		[Key]
		[Column("group_id")]
		public int GroupId { get; set; }
        
		[Column("trigger_app_id")]
		public int TriggerAppId { get; set; }
        
		[Column("group_code")]
		public string GroupCode { get; set; }
        
		[Column("group_remark")]
		public string GroupRemark { get; set; }
        
		[Column("auxiliary_code")]
		public string AuxiliaryCode { get; set; }
            
	}
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("family_net_lock_log")]
	public partial class FamilyNetLockLog
	{
        public FamilyNetLockLog()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("batch_id")]
		public Guid BatchId { get; set; }
        
		[Column("order_guid")]
		public Guid OrderGuid { get; set; }
        
		[Column("coupon_id")]
		public int CouponId { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("lock_time")]
		public DateTime LockTime { get; set; }
            
	}
}
using System; 
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_ad_board")]
	public partial class SkmAdBoard
	{
        public SkmAdBoard()
        {
		this.OpenUrl = String.Empty;
		this.ImageUrl = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }
        [Column("link_type")]
        public int LinkType { get; set; }
        [Column("open_url")]
		public string OpenUrl { get; set; }
        
		[Column("image_url")]
		public string ImageUrl { get; set; }
        
		[Column("start_date")]
		public DateTime StartDate { get; set; }
        
		[Column("end_date")]
		public DateTime EndDate { get; set; }
        
		[Column("is_enable")]
		public bool IsEnable { get; set; }
        
		[Column("sort")]
		public int Sort { get; set; }

        [Column("link_store_guid")]
        public Guid? LinkStoreGuid { get; set; }

        [Column("need_login")]
        public bool NeedLogin { get; set; }

        [Column("create_date")]
		public DateTime CreateDate { get; set; }
        
		[Column("create_user")]
		public int CreateUser { get; set; }

        [Column("last_update_date")]
        public DateTime? LastUpdateDate { get; set; }

        [Column("last_update_user")]
        public int? LastUpdateUser { get; set; }
    }
}
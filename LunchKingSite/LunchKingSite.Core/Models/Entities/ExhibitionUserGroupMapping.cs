using System; 
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("exhibition_user_group_mapping")]
	public partial class ExhibitionUserGroupMapping
	{
        public ExhibitionUserGroupMapping()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("exhibition_user_group_id")]
		public int ExhibitionUserGroupId { get; set; }
        
		[Column("exhibition_event_id")]
		public int ExhibitionEventId { get; set; }
            
	}
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("prize_draw_record_log")]
	public partial class PrizeDrawRecordLog
	{
        public PrizeDrawRecordLog()
        {
		this.ApiName = String.Empty;
		this.RequestLog = String.Empty;
		this.ShopCode = String.Empty;
		this.SendData = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("record_id")]
		public int RecordId { get; set; }
        
		[Column("api_name")]
		public string ApiName { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("request_log")]
		public string RequestLog { get; set; }
        
		[Column("shop_code")]
		public string ShopCode { get; set; }
        
		[Column("send_data")]
		public string SendData { get; set; }
        
		[Column("send_time")]
		public DateTime SendTime { get; set; }
        
		[Column("received_data")]
		public string ReceivedData { get; set; }
        
		[Column("received_time")]
		public DateTime? ReceivedTime { get; set; }
            
	}
}
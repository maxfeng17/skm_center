using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_beacon_device_group")]
	public partial class ViewBeaconDeviceGroup
    {
        [Key]
        [Column("nid")]
		public Guid Nid { get; set; }
        
		[Column("group_id")]
		public int GroupId { get; set; }
        
		[Column("trigger_app_id")]
		public int TriggerAppId { get; set; }
        
		[Column("group_code")]
		public string GroupCode { get; set; }
        
		[Column("group_remark")]
		public string GroupRemark { get; set; }
        
		[Column("auxiliary_code")]
		public string AuxiliaryCode { get; set; }
        
		[Column("device_id")]
		public int DeviceId { get; set; }
        
		[Column("device_name")]
		public string DeviceName { get; set; }
        
		[Column("major")]
		public int Major { get; set; }
        
		[Column("minor")]
		public int Minor { get; set; }
        
		[Column("electric_mac_address")]
		public string ElectricMacAddress { get; set; }
        
		[Column("electric_power")]
		public double? ElectricPower { get; set; }
        
		[Column("last_update_time")]
		public DateTime LastUpdateTime { get; set; }
        
		[Column("floor")]
		public string Floor { get; set; }
        
		[Column("xtop")]
		public int Xtop { get; set; }
        
		[Column("xleft")]
		public int Xleft { get; set; }
             
    }
}
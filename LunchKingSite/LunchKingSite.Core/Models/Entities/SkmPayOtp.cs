using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_pay_otp")]
	public partial class SkmPayOtp
	{
        public SkmPayOtp()
        {
		this.OtpUrl = String.Empty;
		this.FreeDealPayReply = String.Empty;
		this.PlatFormId = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public long Id { get; set; }
        
		[Column("skm_pay_order_id")]
		public int SkmPayOrderId { get; set; }
        
		[Column("app_access_count")]
		public int AppAccessCount { get; set; }
        
		[Column("job_access_count")]
		public int JobAccessCount { get; set; }
        
		[Column("otp_url")]
		public string OtpUrl { get; set; }
        
		[Column("free_deal_pay_reply")]
		public string FreeDealPayReply { get; set; }
        
		[Column("result")]
		public string Result { get; set; }
        
		[Column("error_message")]
		public string ErrorMessage { get; set; }
        
		[Column("is_processing")]
		public bool IsProcessing { get; set; }
        
		[Column("is_finished")]
		public bool IsFinished { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("last_app_access_time")]
		public DateTime LastAppAccessTime { get; set; }
        
		[Column("vip_app_pass")]
		public string VipAppPass { get; set; }
        
		[Column("plat_form_id")]
		public string PlatFormId { get; set; }
            
	}
}
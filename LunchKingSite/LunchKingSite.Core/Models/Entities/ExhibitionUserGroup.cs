using System; 
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("exhibition_user_group")]
	public partial class ExhibitionUserGroup
	{
        public ExhibitionUserGroup()
        {
		this.FileName = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("name")]
		public string Name { get; set; }
        
		[Column("start_date")]
		public DateTime StartDate { get; set; }
        
		[Column("end_date")]
		public DateTime EndDate { get; set; }
        
		[Column("file_name")]
		public string FileName { get; set; }
        
		[Column("file_user_count")]
		public int FileUserCount { get; set; }
        
		[Column("seller_guid")]
		public Guid SellerGuid { get; set; }
        
		[Column("is_hq")]
		public bool IsHq { get; set; }
        
		[Column("create_date")]
		public DateTime CreateDate { get; set; }
        
		[Column("create_user")]
		public int CreateUser { get; set; }
        
		[Column("modify_date")]
		public DateTime? ModifyDate { get; set; }
        
		[Column("modify_user")]
		public int? ModifyUser { get; set; }
        
		[Column("is_hq_created")]
		public bool IsHqCreated { get; set; }
            
	}
}
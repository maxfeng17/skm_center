using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("channel_pos_api_log")]
	public partial class ChannelPosApiLog
	{
        public ChannelPosApiLog()
        {
		this.ApiName = string.Empty;
		this.KeyVal = string.Empty;
		this.Memo = string.Empty;
		this.OauthToken = string.Empty;
		this.RequestIp = string.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("api_name")]
		public string ApiName { get; set; }
        
		[Column("key_val")]
		public string KeyVal { get; set; }
        
		[Column("memo")]
		public string Memo { get; set; }
        
		[Column("oauth_token")]
		public string OauthToken { get; set; }
        
		[Column("request_type")]
		public byte RequestType { get; set; }
        
		[Column("request_ip")]
		public string RequestIp { get; set; }
        
		[Column("request_time")]
		public DateTime RequestTime { get; set; }
        
		[Column("return_code")]
		public int? ReturnCode { get; set; }
        
		[Column("return_message")]
		public string ReturnMessage { get; set; }
            
	}
}
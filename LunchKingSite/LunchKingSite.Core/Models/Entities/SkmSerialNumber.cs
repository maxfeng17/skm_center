using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_serial_number")]
	public partial class SkmSerialNumber
	{
        public SkmSerialNumber()
        {
		this.PrefixCode = String.Empty;
		this.SkmTradeNo = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("prefix_code")]
		public string PrefixCode { get; set; }
        
		[Column("serial_number")]
		public int SerialNumber { get; set; }
        
		[Column("skm_trade_no")]
		public string SkmTradeNo { get; set; }
        
		[Column("processing")]
		public bool Processing { get; set; }
        
		[Column("serial_time")]
		public int SerialTime { get; set; }
        
		[Column("is_trade_complete")]
		public bool IsTradeComplete { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("modify_time")]
		public DateTime ModifyTime { get; set; }
            
	}
}
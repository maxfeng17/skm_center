using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("family_net_verify_log")]
	public partial class FamilyNetVerifyLog
	{
        public FamilyNetVerifyLog()
        {
		this.TranNo = string.Empty;
		this.SerialNo = string.Empty;
		this.CdActCode = string.Empty;
		this.CdPinCode = string.Empty;
		this.ItemCode = string.Empty;
		this.PaymentTime = string.Empty;
		this.StoreCode = string.Empty;
		this.Remark = string.Empty;
		this.IpAddress = string.Empty;
		this.PincodeTranNo = string.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("tran_no")]
		public string TranNo { get; set; }
        
		[Column("serial_no")]
		public string SerialNo { get; set; }
        
		[Column("cd_act_code")]
		public string CdActCode { get; set; }
        
		[Column("cd_pin_code")]
		public string CdPinCode { get; set; }
        
		[Column("item_code")]
		public string ItemCode { get; set; }
        
		[Column("payment_time")]
		public string PaymentTime { get; set; }
        
		[Column("store_code")]
		public string StoreCode { get; set; }
        
		[Column("tm_trans_no")]
		public string TmTransNo { get; set; }
        
		[Column("price")]
		public int Price { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("reply_time")]
		public DateTime? ReplyTime { get; set; }
        
		[Column("reply_code")]
		public byte? ReplyCode { get; set; }
        
		[Column("reply_desc")]
		public string ReplyDesc { get; set; }
        
		[Column("status")]
		public byte Status { get; set; }
        
		[Column("remark")]
		public string Remark { get; set; }
        
		[Column("ip_address")]
		public string IpAddress { get; set; }
        
		[Column("modify_time")]
		public DateTime ModifyTime { get; set; }
        
		[Column("pincode_order_no")]
		public int? PincodeOrderNo { get; set; }
        
		[Column("retry_count")]
		public int RetryCount { get; set; }
        
		[Column("flag")]
		public byte Flag { get; set; }
        
		[Column("pincode_tran_no")]
		public string PincodeTranNo { get; set; }
            
	}
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("hi_life_get_pincode_log")]
	public partial class HiLifeGetPincodeLog
	{
        public HiLifeGetPincodeLog()
        {
		this.Pincode = String.Empty;
		this.IpAddress = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("batch_id")]
		public Guid BatchId { get; set; }
        
		[Column("order_guid")]
		public Guid OrderGuid { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("coupon_id")]
		public int CouponId { get; set; }
        
		[Column("peztemp_id")]
		public int PeztempId { get; set; }
        
		[Column("pincode")]
		public string Pincode { get; set; }
        
		[Column("ip_address")]
		public string IpAddress { get; set; }
        
		[Column("request_qty")]
		public int RequestQty { get; set; }
        
		[Column("request_time")]
		public DateTime RequestTime { get; set; }
            
	}
}
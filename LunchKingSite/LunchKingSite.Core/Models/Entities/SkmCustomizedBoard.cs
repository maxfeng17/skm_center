﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("skm_customized_board")]
    public partial class SkmCustomizedBoard
    {
        public SkmCustomizedBoard()
        {
            this.LinkUrl = String.Empty;
            this.ImageUrl = String.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("content_type")]
        public int ContentType { get; set; }

        [Column("layout_type")]
        public int LayoutType { get; set; }

        [Column("link_url")]
        public string LinkUrl { get; set; }

        [Column("image_url")]
        public string ImageUrl { get; set; }

        [Column("seller_guid")]
        public Guid SellerGuid { get; set; }

        [Column("create_user")]
        public int CreateUser { get; set; }

        [Column("image_limite_width")]
        public int ImageLimiteWidth { get; set; }

        [Column("image_limite_height")]
        public int ImageLimiteHeight { get; set; }

        [Column("create_date")]
        public DateTime CreateDate { get; set; }

        [Column("last_modify_user")]
        public int LastModifyUser { get; set; }

        [Column("last_modify_date")]
        public DateTime LastModifyDate { get; set; }

    }
}
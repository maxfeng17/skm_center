using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_serial_number_log")]
	public partial class SkmSerialNumberLog
	{
        public SkmSerialNumberLog()
        {
		this.TradeNo = String.Empty;
		this.ResultMsg = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("serial_number_id")]
		public int SerialNumberId { get; set; }
        
		[Column("trade_type")]
		public int TradeType { get; set; }
        
		[Column("skm_pay_order_id")]
		public int SkmPayOrderId { get; set; }
        
		[Column("trade_no")]
		public string TradeNo { get; set; }
        
		[Column("request_time")]
		public DateTime RequestTime { get; set; }
        
		[Column("response_time")]
		public DateTime? ResponseTime { get; set; }
        
		[Column("result_success")]
		public bool ResultSuccess { get; set; }
        
		[Column("result_msg")]
		public string ResultMsg { get; set; }
            
	}
}
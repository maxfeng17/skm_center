using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("beacon_log")]
	public partial class BeaconLog
	{
        public BeaconLog()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("event_id")]
		public int EventId { get; set; }
        
		[Column("trigger_app_id")]
		public int TriggerAppId { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("app_device_id")]
		public int AppDeviceId { get; set; }
        
		[Column("group_id")]
		public int GroupId { get; set; }
        
		[Column("beacon_device_id")]
		public int BeaconDeviceId { get; set; }
        
		[Column("create_date")]
		public DateTime CreateDate { get; set; }
            
	}
}
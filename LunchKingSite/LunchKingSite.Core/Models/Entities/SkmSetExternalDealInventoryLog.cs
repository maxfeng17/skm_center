﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("skm_set_external_deal_inventory_log")]
    public partial class SkmSetExternalDealInventoryLog
    {
        public SkmSetExternalDealInventoryLog()
        {
            this.ShopCode = String.Empty;
            this.ExchangeItemId = String.Empty;
            this.SkmEventId = String.Empty;
            this.CreateUser = String.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("external_deal_guid")]
        public Guid ExternalDealGuid { get; set; }

        [Column("shop_code")]
        public string ShopCode { get; set; }

        [Column("exchange_item_id")]
        public string ExchangeItemId { get; set; }

        [Column("skm_event_id")]
        public string SkmEventId { get; set; }

        [Column("origion_business_hour_data")]
        public string OrigionBusinessHourData { get; set; }

        [Column("update_business_hour_data")]
        public string UpdateBusinessHourData { get; set; }

        [Column("create_user")]
        public string CreateUser { get; set; }

        [Column("create_date")]
        public DateTime CreateDate { get; set; }

    }
}

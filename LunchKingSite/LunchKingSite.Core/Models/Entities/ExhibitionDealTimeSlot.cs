using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("exhibition_deal_time_slot")]
	public partial class ExhibitionDealTimeSlot
	{
        public ExhibitionDealTimeSlot()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("seller_guid")]
		public Guid SellerGuid { get; set; }
        
		[Column("exhibition_id")]
		public int ExhibitionId { get; set; }
        
		[Column("exhibition_category_id")]
		public int ExhibitionCategoryId { get; set; }
        
		[Column("external_guid")]
		public Guid ExternalGuid { get; set; }
        
		[Column("effective_date")]
		public DateTime EffectiveDate { get; set; }
        
		[Column("sequence")]
		public int Sequence { get; set; }
        
		[Column("is_hidden")]
		public bool IsHidden { get; set; }
            
	}
}
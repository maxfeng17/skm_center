using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("external_deal_combo")]
	public partial class ExternalDealCombo
	{
        public ExternalDealCombo()
        {
		this.ShopCode = String.Empty;
		this.ShopName = String.Empty;
        }
        
		[Key]
		[Column("external_deal_guid")]
		public Guid ExternalDealGuid { get; set; }
        
		[Key]
		[Column("shop_code")]
		public string ShopCode { get; set; }
        
		[Column("shop_guid")]
		public Guid ShopGuid { get; set; }
        
		[Column("shop_name")]
		public string ShopName { get; set; }
        
		[Column("seller_guid")]
		public Guid SellerGuid { get; set; }
        
		[Column("main_bid")]
		public Guid? MainBid { get; set; }
        
		[Column("bid")]
		public Guid? Bid { get; set; }
        
		[Column("qty")]
		public int Qty { get; set; }
        
		[Column("is_del")]
		public bool IsDel { get; set; }
            
	}
}
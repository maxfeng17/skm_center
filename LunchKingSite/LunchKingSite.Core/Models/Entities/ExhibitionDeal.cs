using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("exhibition_deal")]
	public partial class ExhibitionDeal
    {
        public ExhibitionDeal()
        {
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("event_id")]
        public int EventId { get; set; }

        [Column("category_id")]
        public int CategoryId { get; set; }

        [Column("external_deal_guid")]
        public Guid ExternalDealGuid { get; set; }

        [Column("is_main_deal")]
        public bool IsMainDeal { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }

    }
}
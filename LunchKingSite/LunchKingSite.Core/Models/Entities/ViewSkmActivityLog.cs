using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_skm_activity_log")]
	public partial class ViewSkmActivityLog
    {
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("activity_id")]
		public int ActivityId { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("charges_val")]
		public int ChargesVal { get; set; }
        
		[Column("charges_cost")]
		public int ChargesCost { get; set; }
             
    }
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("family_net_pincode_trans")]
	public partial class FamilyNetPincodeTran
	{
        public FamilyNetPincodeTran()
        {
		this.Pincode = string.Empty;
		this.ReplyCode = string.Empty;
		this.ReplyDesc = string.Empty;
		this.PinExpiredate = string.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("family_net_event_id")]
		public int FamilyNetEventId { get; set; }
        
		[Column("order_guid")]
		public Guid OrderGuid { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("qty")]
		public int Qty { get; set; }
        
		[Column("pincode")]
		public string Pincode { get; set; }
        
		[Column("reply_code")]
		public string ReplyCode { get; set; }
        
		[Column("reply_desc")]
		public string ReplyDesc { get; set; }
        
		[Column("pin_expiredate")]
		public string PinExpiredate { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("status")]
		public byte Status { get; set; }
        
		[Column("is_refund")]
		public bool IsRefund { get; set; }
        
		[Column("order_ticket")]
		public int OrderTicket { get; set; }
            
	}
}
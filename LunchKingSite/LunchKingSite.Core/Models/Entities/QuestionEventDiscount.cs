using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("question_event_discount")]
	public partial class QuestionEventDiscount
	{
        public QuestionEventDiscount()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("event_id")]
		public int EventId { get; set; }
        
		[Column("discount_campaign")]
		public int DiscountCampaign { get; set; }
        
		[Column("disabled")]
		public bool Disabled { get; set; }
        
		[Column("qty")]
		public int Qty { get; set; }
            
	}
}
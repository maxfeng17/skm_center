using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("skm_install_event")]
	public class SkmInstallEvent
	{
		public SkmInstallEvent()
		{
			Title = string.Empty;
			CreateId = string.Empty;
			ModifyId = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("title")]
		public string Title { get; set; }

		[Column("start_date")]
		public DateTime StartDate { get; set; }

		[Column("end_date")]
		public DateTime EndDate { get; set; }

		[Column("is_available")]
		public bool IsAvailable { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }

		[Column("modify_id")]
		public string ModifyId { get; set; }
	}
}

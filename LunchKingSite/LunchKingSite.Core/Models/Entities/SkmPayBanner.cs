using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_pay_banner")]
	public partial class SkmPayBanner
	{
        public SkmPayBanner()
        {
		this.Name = String.Empty;
		this.BannerPath = String.Empty;
		this.ContentHtml = String.Empty;
		this.ModifyUser = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("name")]
		public string Name { get; set; }
        
		[Column("start_time")]
		public DateTime StartTime { get; set; }
        
		[Column("end_time")]
		public DateTime EndTime { get; set; }
        
		[Column("banner_path")]
		public string BannerPath { get; set; }
        
		[Column("content_html")]
		public string ContentHtml { get; set; }
        
		[Column("seq")]
		public int Seq { get; set; }
        
		[Column("modify_time")]
		public DateTime ModifyTime { get; set; }
        
		[Column("modify_user")]
		public string ModifyUser { get; set; }
        
		[Column("is_hidden")]
		public bool IsHidden { get; set; }
            
	}
}
﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("view_external_deal_store_display_name")]
    public partial class ViewExternalDealStoreDisplayName
    {

        [Key]
        [Column("skm_store_display_name_Guid")]
        public Guid SkmStoreDisplayNameGuid { get; set; }

        [Column("display_name")]
        public string DisplayName { get; set; }

        [Column("shop_code")]
        public string ShopCode { get; set; }

        [Column("exchange_type")]
        public int ExchangeType { get; set; }

        [Column("external_deal_guid")]
        public Guid ExternalDealGuid { get; set; }

        [Column("shop_name")]
        public string ShopName { get; set; }

        [Column("seller_guid")]
        public Guid SellerGuid { get; set; }

    }
}
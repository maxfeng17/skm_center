using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("beacon_event")]
	public partial class BeaconEvent
	{
        public BeaconEvent()
        {
		this.Subject = string.Empty;
		this.Content = string.Empty;
		this.ActionUrl = string.Empty;
		this.EffectiveTimeStart = string.Empty;
		this.EffectiveTimeEnd = string.Empty;
        }
        
		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("event_id")]
		public int EventId { get; set; }
        
		[Column("trigger_app_id")]
		public int TriggerAppId { get; set; }
        
		[Column("enabled")]
		public bool Enabled { get; set; }
        
		[Column("subject")]
		public string Subject { get; set; }
        
		[Column("content")]
		public string Content { get; set; }
        
		[Column("event_type")]
		public byte EventType { get; set; }
        
		[Column("action_bid")]
		public Guid ActionBid { get; set; }
        
		[Column("action_url")]
		public string ActionUrl { get; set; }
        
		[Column("effective_date_start")]
		public DateTime EffectiveDateStart { get; set; }
        
		[Column("effective_date_end")]
		public DateTime EffectiveDateEnd { get; set; }
        
		[Column("effective_time_start")]
		public string EffectiveTimeStart { get; set; }
        
		[Column("effective_time_end")]
		public string EffectiveTimeEnd { get; set; }
        
		[Column("is_all_beacon_trigger")]
		public bool IsAllBeaconTrigger { get; set; }
        
		[Column("create_date")]
		public DateTime CreateDate { get; set; }
        
		[Column("create_id")]
		public int CreateId { get; set; }
            
	}
}
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("skm_install_event_bank")]
	public class SkmInstallEventBank
	{
		public SkmInstallEventBank()
		{
			BankNo = string.Empty;
			CreateId = string.Empty;
			ModifyId = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("event_id")]
		public int EventId { get; set; }

		[Column("bank_no", TypeName = "varchar")]
		public string BankNo { get; set; }

		[Column("is_co_branded")]
		public bool IsCoBranded { get; set; }

		[Column("start_date")]
		public DateTime StartDate { get; set; }

		[Column("end_date")]
		public DateTime EndDate { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("create_id", TypeName = "varchar")]
		public string CreateId { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }

		[Column("modify_id", TypeName = "varchar")]
		public string ModifyId { get; set; }

		[Column("is_del")]
		public bool IsDel { get; set; }

		[Column("seq")]
		public int Seq { get; set; }
	}
}

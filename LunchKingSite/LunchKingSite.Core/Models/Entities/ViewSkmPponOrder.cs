using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_skm_ppon_order")]
	public partial class ViewSkmPponOrder
    {
        
		[Key]
		[Column("GUID")]
		public Guid Guid { get; set; }
        
		[Column("seller_GUID")]
		public Guid SellerGuid { get; set; }
        
		[Column("business_hour_guid")]
		public Guid BusinessHourGuid { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("order_status")]
		public int OrderStatus { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("business_hour_order_time_s")]
		public DateTime BusinessHourOrderTimeS { get; set; }
        
		[Column("business_hour_order_time_e")]
		public DateTime BusinessHourOrderTimeE { get; set; }
        
		[Column("business_hour_deliver_time_s")]
		public DateTime? BusinessHourDeliverTimeS { get; set; }
        
		[Column("business_hour_deliver_time_e")]
		public DateTime? BusinessHourDeliverTimeE { get; set; }
             
    }
}
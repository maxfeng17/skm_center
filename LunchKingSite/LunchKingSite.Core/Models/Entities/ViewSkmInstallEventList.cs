using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("view_skm_install_event_list")]
	public class ViewSkmInstallEventList
	{
		public ViewSkmInstallEventList()
		{
			Title = string.Empty;
		}

		[Column("id")]
		public int Id { get; set; }

		[Column("title")]
		public string Title { get; set; }

		[Column("start_date")]
		public DateTime StartDate { get; set; }

		[Column("end_date")]
		public DateTime EndDate { get; set; }

		[Column("bank_amount")]
		public int? BankAmount { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }

		[Column("is_available")]
		public bool IsAvailable { get; set; }
	}
}

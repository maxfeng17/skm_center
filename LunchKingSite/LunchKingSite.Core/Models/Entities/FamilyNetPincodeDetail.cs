using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("family_net_pincode_detail")]
	public partial class FamilyNetPincodeDetail
	{
        public FamilyNetPincodeDetail()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("pincode_order_no")]
		public int PincodeOrderNo { get; set; }
        
		[Column("serial_no")]
		public int SerialNo { get; set; }
        
		[Column("item_code")]
		public string ItemCode { get; set; }
        
		[Column("prod_name")]
		public string ProdName { get; set; }
        
		[Column("barcode_1")]
		public string Barcode1 { get; set; }
        
		[Column("barcode_2")]
		public string Barcode2 { get; set; }
        
		[Column("barcode_3")]
		public string Barcode3 { get; set; }
        
		[Column("barcode_4")]
		public string Barcode4 { get; set; }
        
		[Column("barcode_5")]
		public string Barcode5 { get; set; }
        
		[Column("expire_date")]
		public string ExpireDate { get; set; }
        
		[Column("exg_kind")]
		public string ExgKind { get; set; }
        
		[Column("exg_bonus")]
		public int ExgBonus { get; set; }
        
		[Column("dis_amt")]
		public int DisAmt { get; set; }
        
		[Column("prod_memo_1")]
		public string ProdMemo1 { get; set; }
        
		[Column("prod_memo_2")]
		public string ProdMemo2 { get; set; }
        
		[Column("prod_memo_3")]
		public string ProdMemo3 { get; set; }
        
		[Column("prod_memo_4")]
		public string ProdMemo4 { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("is_exchange")]
		public bool IsExchange { get; set; }
        
		[Column("tran_no")]
		public string TranNo { get; set; }
        
		[Column("payment_time")]
		public string PaymentTime { get; set; }
        
		[Column("store_code")]
		public string StoreCode { get; set; }
        
		[Column("tm_trans_no")]
		public string TmTransNo { get; set; }
        
		[Column("price")]
		public int? Price { get; set; }
        
		[Column("payment_datetime")]
		public DateTime? PaymentDatetime { get; set; }
            
	}
}
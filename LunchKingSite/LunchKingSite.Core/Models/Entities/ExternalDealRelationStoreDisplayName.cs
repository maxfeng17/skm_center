﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("external_deal_relation_store_display_name")]
    public partial class ExternalDealRelationStoreDisplayName
    {
        public ExternalDealRelationStoreDisplayName()
        {
        }

        [Key]
        [Column("skm_store_display_name_guid")]
        public Guid SkmStoreDisplayNameGuid { get; set; }

        [Key]
        [Column("external_deal_guid")]
        public Guid ExternalDealGuid { get; set; }

        [Column("create_date")]
        public DateTime CreateDate { get; set; }

    }
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("prize_draw_item")]
	public partial class PrizeDrawItem
	{
        public PrizeDrawItem()
        {
		this.PrizeName = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("prize_draw_event_id")]
		public int PrizeDrawEventId { get; set; }
        
		[Column("prize_name")]
		public string PrizeName { get; set; }
        
		[Column("prize_type")]
		public byte PrizeType { get; set; }
        
		[Column("external_deal_id")]
		public Guid ExternalDealId { get; set; }
        
		[Column("qty")]
		public int Qty { get; set; }
        
		[Column("is_thanks")]
		public bool IsThanks { get; set; }
        
		[Column("prize_point")]
		public int PrizePoint { get; set; }
        
		[Column("winning_rate")]
		public decimal WinningRate { get; set; }
        
		[Column("effective_start_date")]
		public DateTime? EffectiveStartDate { get; set; }
        
		[Column("effective_end_date")]
		public DateTime? EffectiveEndDate { get; set; }
        
		[Column("is_remove")]
		public bool IsRemove { get; set; }

        [Column("is_use")]
        public bool IsUse { get; set; }

    }
}
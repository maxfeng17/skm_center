using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("peztemp")]
	public partial class Peztemp
	{
        public Peztemp()
        {
        }
        
		[Key]
		[Column("Id")]
		public int Id { get; set; }
        
		[Column("OrderDetailGuid")]
		public Guid? OrderDetailGuid { get; set; }
        
		[Column("PezCode")]
		public string PezCode { get; set; }
        
		[Column("Bid")]
		public Guid? Bid { get; set; }
        
		[Column("Option_1")]
		public string Option1 { get; set; }
        
		[Column("user_id")]
		public string UserId { get; set; }
        
		[Column("is_used")]
		public bool IsUsed { get; set; }
        
		[Column("used_time")]
		public DateTime? UsedTime { get; set; }
        
		[Column("order_guid")]
		public Guid? OrderGuid { get; set; }
        
		[Column("cvs_id")]
		public int? CvsId { get; set; }
        
		[Column("batch_id")]
		public int BatchId { get; set; }
        
		[Column("service_code")]
		public int ServiceCode { get; set; }
        
		[Column("status")]
		public int Status { get; set; }
        
		[Column("is_test")]
		public bool IsTest { get; set; }
        
		[Column("is_backup_code")]
		public bool IsBackupCode { get; set; }
        
		[Column("remark")]
		public string Remark { get; set; }
            
	}
}
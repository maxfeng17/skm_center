using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("beacon_trigger_app")]
	public partial class BeaconTriggerApp
	{
        public BeaconTriggerApp()
        {
		this.Name = string.Empty;
        }
        
		[Key]
		[Column("trigger_app_id")]
		public int TriggerAppId { get; set; }
        
		[Column("name")]
		public string Name { get; set; }
        
		[Column("seller_uuid")]
		public Guid SellerUuid { get; set; }
        
		[Column("electric_uuid")]
		public Guid ElectricUuid { get; set; }
        
		[Column("cd_sec")]
		public int CdSec { get; set; }
        
		[Column("daily_max_received_count")]
		public int DailyMaxReceivedCount { get; set; }
        
		[Column("is_testing")]
		public bool IsTesting { get; set; }
        
		[Column("function_name")]
		public string FunctionName { get; set; }
        
		[Column("subevent_min_count")]
		public int SubeventMinCount { get; set; }
        
		[Column("subevent_max_count")]
		public int SubeventMaxCount { get; set; }
            
	}
}
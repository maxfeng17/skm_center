using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_exhibition_event_user_group")]
	public partial class ViewExhibitionEventUserGroup
    {
        
		[Key]
		[Column("exhibition_event_id")]
		public int ExhibitionEventId { get; set; }
        
		[Column("status")]
		public byte Status { get; set; }
        
		[Column("banner_pic")]
		public string BannerPic { get; set; }
        
		[Column("subject")]
		public string Subject { get; set; }
        
		[Column("event_content")]
		public string EventContent { get; set; }
        
		[Column("event_rule")]
		public string EventRule { get; set; }
        
		[Column("start_date")]
		public DateTime StartDate { get; set; }
        
		[Column("end_date")]
		public DateTime EndDate { get; set; }
        
		[Column("is_hq_event")]
		public bool IsHqEvent { get; set; }
        
		[Column("seq")]
		public int Seq { get; set; }
        
		[Column("event_type")]
		public byte EventType { get; set; }
        
		[Column("store_guid")]
		public Guid StoreGuid { get; set; }
        
		[Column("card_type")]
		public byte CardType { get; set; }
        
		[Column("banner_link")]
		public string BannerLink { get; set; }
        
		[Column("skm_member_token")]
		public string SkmMemberToken { get; set; }
        
		[Column("exhibition_user_group_id")]
		public int? ExhibitionUserGroupId { get; set; }
             
    }
}
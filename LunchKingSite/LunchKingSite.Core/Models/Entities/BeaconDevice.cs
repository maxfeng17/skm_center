using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("beacon_device")]
	public partial class BeaconDevice
	{
        public BeaconDevice()
        {
		this.DeviceName = string.Empty;
		this.ElectricMacAddress = string.Empty;
        }
        
		[Key]
		[Column("device_id")]
		public int DeviceId { get; set; }
        
		[Column("device_name")]
		public string DeviceName { get; set; }
        
		[Column("major")]
		public int Major { get; set; }
        
		[Column("minor")]
		public int Minor { get; set; }
        
		[Column("electric_mac_address")]
		public string ElectricMacAddress { get; set; }
        
		[Column("electric_power")]
		public double? ElectricPower { get; set; }
        
		[Column("last_update_time")]
		public DateTime LastUpdateTime { get; set; }
            
	}
}
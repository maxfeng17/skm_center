using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_activity_item")]
	public partial class SkmActivityItem
	{
        public SkmActivityItem()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("activity_id")]
		public int ActivityId { get; set; }
        
		[Column("external_deal_guid")]
		public Guid ExternalDealGuid { get; set; }
        
		[Column("charges_val")]
		public int ChargesVal { get; set; }
        
		[Column("charges_cost")]
		public int ChargesCost { get; set; }
        
		[Column("bid")]
		public Guid Bid { get; set; }
            
	}
}
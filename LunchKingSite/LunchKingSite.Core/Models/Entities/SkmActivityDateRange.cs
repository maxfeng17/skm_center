using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_activity_date_range")]
	public partial class SkmActivityDateRange
	{
        public SkmActivityDateRange()
        {
		this.RangeName = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("activity_id")]
		public int ActivityId { get; set; }
        
		[Column("date_range_start")]
		public DateTime DateRangeStart { get; set; }
        
		[Column("date_range_end")]
		public DateTime DateRangeEnd { get; set; }
        
		[Column("range_name")]
		public string RangeName { get; set; }
            
	}
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("family_net_pincode")]
	public partial class FamilyNetPincode
	{
        public FamilyNetPincode()
        {
		this.ActCode = String.Empty;
        }
        
		[Key]
		[Column("order_no")]
		public int OrderNo { get; set; }
        
		[Column("order_date")]
		public DateTime OrderDate { get; set; }
        
		[Column("peztemp_id")]
		public int PeztempId { get; set; }
        
		[Column("act_code")]
		public string ActCode { get; set; }
        
		[Column("status")]
		public byte Status { get; set; }
        
		[Column("tran_no")]
		public string TranNo { get; set; }
        
		[Column("reply_code")]
		public string ReplyCode { get; set; }
        
		[Column("reply_desc")]
		public string ReplyDesc { get; set; }
        
		[Column("pin_expiredate")]
		public string PinExpiredate { get; set; }
        
		[Column("act_memo1")]
		public string ActMemo1 { get; set; }
        
		[Column("act_memo2")]
		public string ActMemo2 { get; set; }
        
		[Column("act_memo3")]
		public string ActMemo3 { get; set; }
        
		[Column("act_memo4")]
		public string ActMemo4 { get; set; }
        
		[Column("fail_retry")]
		public byte FailRetry { get; set; }
        
		[Column("modify_date")]
		public DateTime ModifyDate { get; set; }
        
		[Column("view_count")]
		public int ViewCount { get; set; }
        
		[Column("is_verified")]
		public bool IsVerified { get; set; }
        
		[Column("is_lock")]
		public bool IsLock { get; set; }
        
		[Column("lock_time")]
		public DateTime? LockTime { get; set; }
        
		[Column("store_code")]
		public string StoreCode { get; set; }
        
		[Column("order_guid")]
		public Guid OrderGuid { get; set; }
        
		[Column("coupon_id")]
		public int CouponId { get; set; }
        
		[Column("pincode_trans_id")]
		public int PincodeTransId { get; set; }
        
		[Column("return_status")]
		public byte ReturnStatus { get; set; }
        
		[Column("is_verifying")]
		public bool IsVerifying { get; set; }
        
		[Column("verifying_time")]
		public DateTime? VerifyingTime { get; set; }
        
		[Column("order_ticket")]
		public int OrderTicket { get; set; }
        
		[Column("reissue_pin")]
		public bool ReissuePin { get; set; }
        
		[Column("barcode_reget")]
		public bool BarcodeReget { get; set; }
        
		[Column("barcode_reget_time")]
		public DateTime? BarcodeRegetTime { get; set; }
            
        [Column("fn_tran_no")]
		public string FnTranNo { get; set; }
	}
}
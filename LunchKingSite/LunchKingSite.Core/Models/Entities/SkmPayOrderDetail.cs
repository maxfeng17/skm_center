using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
	[Table("skm_pay_order_detail")]
	public partial class SkmPayOrderDetail
	{
        public SkmPayOrderDetail()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("skm_pay_order_id")]
		public int SkmPayOrderId { get; set; }
        
		[Column("trust_id")]
		public Guid TrustId { get; set; }
            
	}
}
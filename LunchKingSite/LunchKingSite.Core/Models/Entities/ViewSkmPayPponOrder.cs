using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_skm_pay_ppon_order")]
	public partial class ViewSkmPayPponOrder
    {
        
		[Key]
		[Column("GUID")]
		public Guid Guid { get; set; }
        
		[Column("seller_GUID")]
		public Guid SellerGuid { get; set; }
        
		[Column("business_hour_guid")]
		public Guid BusinessHourGuid { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("order_status")]
		public int OrderStatus { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("status")]
		public int Status { get; set; }
        
		[Column("skm_pay_order_id")]
		public int SkmPayOrderId { get; set; }
        
		[Column("order_no")]
		public string OrderNo { get; set; }
        
		[Column("refund_date")]
		public DateTime? RefundDate { get; set; }
        
		[Column("skm_trade_no")]
		public string SkmTradeNo { get; set; }
             
    }
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("exhibition_log")]
	public partial class ExhibitionLog
	{
        public ExhibitionLog()
        {
		this.ModifyItem = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("log_type")]
		public byte LogType { get; set; }
        
		[Column("crud")]
		public byte Crud { get; set; }
        
		[Column("target_id")]
		public int TargetId { get; set; }
        
		[Column("modify_id")]
		public int ModifyId { get; set; }
        
		[Column("modify_time")]
		public DateTime ModifyTime { get; set; }
        
		[Column("modify_item")]
		public string ModifyItem { get; set; }
            
	}
}
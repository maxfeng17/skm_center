using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("exhibition_code")]
	public partial class ExhibitionCode
	{
        public ExhibitionCode()
        {
            this.SellerName = String.Empty;
            this.ExhibitionName = String.Empty;
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("seller_guid")]
        public Guid SellerGuid { get; set; }

        [Column("seller_name")]
        public string SellerName { get; set; }

        [Column("exhibition_name")]
        public string ExhibitionName { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }
    }
}
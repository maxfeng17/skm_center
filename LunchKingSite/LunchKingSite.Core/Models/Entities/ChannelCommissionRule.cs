using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    [Table("channel_commission_rule")]
    public partial class ChannelCommissionRule
    {
        public ChannelCommissionRule()
        {
            this.KeyWord = string.Empty;
            this.ReturnCode = string.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("channel_source")]
        public byte ChannelSource { get; set; }

        [Column("deal_type")]
        public int DealType { get; set; }

        [Column("key_word")]
        public string KeyWord { get; set; }

        [Column("gross_margin_limit")]
        public decimal GrossMarginLimit { get; set; }

        [Column("return_code")]
        public string ReturnCode { get; set; }

        [Column("create_time")]
        public DateTime CreateTime { get; set; }

        [Column("modify_time")]
        public DateTime ModifyTime { get; set; }

        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        [Column("rule_type")]
        public byte RuleType { get; set; }

    }
}
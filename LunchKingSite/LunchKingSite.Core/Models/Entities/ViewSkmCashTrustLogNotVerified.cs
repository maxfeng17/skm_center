using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    [Table("view_skm_cash_trust_log_not_verified")]
    public class ViewSkmCashTrustLogNotVerified
    {
        public ViewSkmCashTrustLogNotVerified()
        {
            SkmToken = string.Empty;
            Title = string.Empty;
            OrderNo = string.Empty;
            ShopCode = string.Empty;
            ShopName = string.Empty;
            ItemName = string.Empty;
        }

        [Column("skm_token")]
        public string SkmToken { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("is_skm_pay")]
        public bool IsSkmPay { get; set; }

        [Column("order_no")]
        public string OrderNo { get; set; }

        [Column("shop_code", TypeName = "varchar")]
        public string ShopCode { get; set; }

        [Column("shop_guid")]
        public Guid ShopGuid { get; set; }

        [Column("shop_name")]
        public string ShopName { get; set; }

        [Column("external_deal_guid")]
        public Guid ExternalDealGuid { get; set; }

        [Column("main_bid")]
        public Guid MainBid { get; set; }

        [Column("bid")]
        public Guid Bid { get; set; }

        [Column("order_status")]
        public int OrderStatus { get; set; }

        [Column("business_hour_order_time_s")]
        public DateTime BusinessHourOrderTimeS { get; set; }

        [Column("business_hour_order_time_e")]
        public DateTime BusinessHourOrderTimeE { get; set; }

        [Column("business_hour_deliver_time_s")]
        public DateTime? BusinessHourDeliverTimeS { get; set; }

        [Column("business_hour_deliver_time_e")]
        public DateTime? BusinessHourDeliverTimeE { get; set; }

        [Column("usage_verified_time")]
        public DateTime? UsageVerifiedTime { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("order_guid")]
        public Guid OrderGuid { get; set; }

        [Column("trust_id")]
        public Guid TrustId { get; set; }

        [Column("item_name")]
        public string ItemName { get; set; }
    }
}
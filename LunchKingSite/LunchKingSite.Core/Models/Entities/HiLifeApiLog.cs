using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("hi_life_api_log")]
	public partial class HiLifeApiLog
	{
		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("api_name")]
		public string ApiName { get; set; }

		[Column("content")]
		public string Content { get; set; }

		[Column("result")]
		public string Result { get; set; }

		[Column("start_time")]
		public DateTime StartTime { get; set; }

		[Column("end_time")]
		public DateTime? EndTime { get; set; }

		[Column("server_name")]
		public string ServerName { get; set; }
	}
}

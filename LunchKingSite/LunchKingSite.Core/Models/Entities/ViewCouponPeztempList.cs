using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_coupon_peztemp_list")]
	public partial class ViewCouponPeztempList
    {
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("order_detail_id")]
		public Guid OrderDetailId { get; set; }
        
		[Column("sequence_number")]
		public string SequenceNumber { get; set; }
        
		[Column("code")]
		public string Code { get; set; }
        
		[Column("description")]
		public string Description { get; set; }
        
		[Column("status")]
		public int Status { get; set; }
        
		[Column("available")]
		public bool? Available { get; set; }
        
		[Column("store_sequence")]
		public int? StoreSequence { get; set; }
        
		[Column("is_reservation_lock")]
		public bool IsReservationLock { get; set; }
        
		[Column("order_guid")]
		public Guid OrderGuid { get; set; }
        
		[Column("peztemp_id")]
		public int PeztempId { get; set; }
        
		[Column("peztemp_code")]
		public string PeztempCode { get; set; }
        
		[Column("Bid")]
		public Guid? Bid { get; set; }
             
    }
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("beacon_event_time_slot")]
	public partial class BeaconEventTimeSlot
	{
        public BeaconEventTimeSlot()
        {
		this.Floor = string.Empty;
        }
        
		[Key]
		[Column("time_slot_id")]
		public int TimeSlotId { get; set; }
        
		[Column("event_id")]
		public int EventId { get; set; }
        
		[Column("effective_date")]
		public DateTime EffectiveDate { get; set; }
        
		[Column("sequence")]
		public int Sequence { get; set; }
        
		[Column("status")]
		public byte Status { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("group_id")]
		public int GroupId { get; set; }
        
		[Column("floor")]
		public string Floor { get; set; }
            
	}
}
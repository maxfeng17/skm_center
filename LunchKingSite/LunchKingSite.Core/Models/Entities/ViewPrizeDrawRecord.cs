using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_prize_draw_record")]
	public partial class ViewPrizeDrawRecord
    {
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("bid")]
		public Guid Bid { get; set; }
        
		[Column("active_type")]
		public int? ActiveType { get; set; }
        
		[Column("event_id")]
		public int EventId { get; set; }
        
		[Column("item_id")]
		public int ItemId { get; set; }
        
		[Column("item_name")]
		public string ItemName { get; set; }
        
		[Column("is_thanks")]
		public bool IsThanks { get; set; }
        
		[Column("qty")]
		public int Qty { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("exchange_shop_code")]
		public string ExchangeShopCode { get; set; }
        
		[Column("shop_name")]
		public string ShopName { get; set; }
             
    }
}
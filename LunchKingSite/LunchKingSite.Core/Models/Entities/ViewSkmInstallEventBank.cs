using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("view_skm_install_event_bank")]
	public class ViewSkmInstallEventBank
	{
		public ViewSkmInstallEventBank()
		{
			EventTitle = string.Empty;
			EventCreateId = string.Empty;
			EventModifyId = string.Empty;
			BankNo = string.Empty;
			BankCreateId = string.Empty;
			BankModifyId = string.Empty;
		}

		[Column("event_id")]
		public int EventId { get; set; }

		[Column("event_title")]
		public string EventTitle { get; set; }

		[Column("event_start_date")]
		public DateTime EventStartDate { get; set; }

		[Column("event_end_date")]
		public DateTime EventEndDate { get; set; }

		[Column("event_is_available")]
		public bool EventIsAvailable { get; set; }

		[Column("event_create_time")]
		public DateTime EventCreateTime { get; set; }

		[Column("event_create_id")]
		public string EventCreateId { get; set; }

		[Column("event_modify_time")]
		public DateTime? EventModifyTime { get; set; }

		[Column("event_modify_id")]
		public string EventModifyId { get; set; }

		[Column("event_bank_id")]
		public int? EventBankId { get; set; }

		[Column("bank_no", TypeName = "varchar")]
		public string BankNo { get; set; }

		[Column("is_co_branded")]
		public bool? IsCoBranded { get; set; }

		[Column("bank_start_date")]
		public DateTime? BankStartDate { get; set; }

		[Column("bank_end_date")]
		public DateTime? BankEndDate { get; set; }

		[Column("bank_create_time")]
		public DateTime? BankCreateTime { get; set; }

		[Column("bank_create_id", TypeName = "varchar")]
		public string BankCreateId { get; set; }

		[Column("bank_modify_time")]
		public DateTime? BankModifyTime { get; set; }

		[Column("bank_modify_id", TypeName = "varchar")]
		public string BankModifyId { get; set; }

		[Column("bank_is_del")]
		public bool? BankIsDel { get; set; }

		[Column("bank_seq")]
		public int? BankSeq { get; set; }

		[Column("period_id")]
		public int? PeriodId { get; set; }

		[Column("install_period")]
		public int? InstallPeriod { get; set; }

		[Column("amount")]
		public int? Amount { get; set; }

		[Column("pay_fee")]
		public int? PayFee { get; set; }

		[Column("interest_rate")]
		public decimal? InterestRate { get; set; }

		[Column("period_is_available")]
		public bool? PeriodIsAvailable { get; set; }
	}
}

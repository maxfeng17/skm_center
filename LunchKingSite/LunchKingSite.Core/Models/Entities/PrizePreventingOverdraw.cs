using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("prize_preventing_overdraw")]
	public partial class PrizePreventingOverdraw
	{
        public PrizePreventingOverdraw()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("item_id")]
		public int ItemId { get; set; }
        
		[Column("qty")]
		public int Qty { get; set; }
        
		[Column("user_id")]
		public int UserId { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("modify_time")]
		public DateTime ModifyTime { get; set; }
        
		[Column("processing")]
		public bool Processing { get; set; }
            
	}
}
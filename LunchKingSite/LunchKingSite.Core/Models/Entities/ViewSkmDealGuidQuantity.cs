﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Models.Entities
{
    /// <summary>
	///
	/// </summary>
	[Serializable]
    [Table("view_skm_deal_guid_quantity")]
    public partial class ViewSkmDealGuidQuantity
    {

        [Key]
        [Column("shop_name")]
        public string ShopName { get; set; }

        [Column("shop_code")]
        public string ShopCode { get; set; }

        [Column("seller_guid")]
        public Guid SellerGuid { get; set; }

        [Column("external_deal_guid")]
        public Guid ExternalDealGuid { get; set; }

        [Column("bid")]
        public Guid? Bid { get; set; }

        [Column("order_total_limit")]
        public decimal OrderTotalLimit { get; set; }

        [Column("ordered_quantity")]
        public int OrderedQuantity { get; set; }

        [Column("verified_quantity")]
        public int VerifiedQuantity { get; set; }

        [Column("canceled_quantity")]
        public int CanceledQuantity { get; set; }

        [Column("returned_quantity")]
        public int ReturnedQuantity { get; set; }

    }
}

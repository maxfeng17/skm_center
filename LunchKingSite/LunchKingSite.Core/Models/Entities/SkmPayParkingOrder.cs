using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("skm_pay_parking_order")]
	public class SkmPayParkingOrder
	{
		public SkmPayParkingOrder()
		{
			OrderNo = string.Empty;
			ShopCode = string.Empty;
			MemberCardNo = string.Empty;
			SkmTradeNo = string.Empty;
			SkmRefundTradeNo = string.Empty;
			WalletPreTransNo = string.Empty;
			WalletPaymentToken = string.Empty;
			WalletCardHash = string.Empty;
			ParkingToken = string.Empty;
			ParkingTansNo = string.Empty;
			ParkingTicketNo = string.Empty;
			ParkingLicensePictureUuid = string.Empty;
			ParkingAreaNo = string.Empty;
			ParkingRrn = string.Empty;
			PosId = string.Empty;
			Memo = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("order_no", TypeName = "varchar")]
		public string OrderNo { get; set; }

		[Column("shop_code", TypeName = "varchar")]
		public string ShopCode { get; set; }

		[Column("device_type")]
		public int DeviceType { get; set; }

		[Column("member_card_no", TypeName = "varchar")]
		public string MemberCardNo { get; set; }

		[Column("order_amount")]
		public int OrderAmount { get; set; }

		[Column("skm_trade_no", TypeName = "varchar")]
		public string SkmTradeNo { get; set; }

		[Column("skm_refund_trade_no", TypeName = "varchar")]
		public string SkmRefundTradeNo { get; set; }

		[Column("wallet_pre_trans_no", TypeName = "varchar")]
		public string WalletPreTransNo { get; set; }

		[Column("wallet_payment_token", TypeName = "varchar")]
		public string WalletPaymentToken { get; set; }

		[Column("wallet_payment_amount")]
		public int WalletPaymentAmount { get; set; }

		[Column("wallet_card_hash", TypeName = "varchar")]
		public string WalletCardHash { get; set; }

		[Column("parking_token", TypeName = "varchar")]
		public string ParkingToken { get; set; }

		[Column("parking_tans_no")]
		public string ParkingTansNo { get; set; }

		[Column("parking_ticket_no", TypeName = "varchar")]
		public string ParkingTicketNo { get; set; }

		[Column("parking_license_picture_uuid", TypeName = "varchar")]
		public string ParkingLicensePictureUuid { get; set; }

		[Column("discount_minutes")]
		public decimal DiscountMinutes { get; set; }

		[Column("discount_amount")]
		public int DiscountAmount { get; set; }

        [Column("is_discount_point")]
        public bool IsDiscountPoint { get; set; }

        [Column("parking_area_no", TypeName = "varchar")]
		public string ParkingAreaNo { get; set; }

		[Column("parking_rrn", TypeName = "varchar")]
		public string ParkingRrn { get; set; }

		[Column("pos_id", TypeName = "varchar")]
		public string PosId { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("create_date")]
		public DateTime CreateDate { get; set; }

		[Column("last_modify_date")]
		public DateTime? LastModifyDate { get; set; }

		[Column("refund_date")]
		public DateTime? RefundDate { get; set; }

		[Column("status")]
		public int Status { get; set; }

		[Column("memo")]
		public string Memo { get; set; }
	}
}

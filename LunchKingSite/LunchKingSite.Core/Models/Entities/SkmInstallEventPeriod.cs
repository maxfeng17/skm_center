using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("skm_install_event_period")]
	public class SkmInstallEventPeriod
	{
		public SkmInstallEventPeriod()
		{
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("event_bank_id")]
		public int EventBankId { get; set; }

		[Column("install_period")]
		public int InstallPeriod { get; set; }

		[Column("amount")]
		public int Amount { get; set; }

		[Column("pay_fee")]
		public int PayFee { get; set; }

		[Column("interest_rate")]
		public decimal InterestRate { get; set; }

		[Column("is_available")]
		public bool IsAvailable { get; set; }
	}
}

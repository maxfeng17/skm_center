using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("device_push_record")]
	public partial class DevicePushRecord
	{
        public DevicePushRecord()
        {
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("device_id")]
		public int DeviceId { get; set; }
        
		[Column("member_id")]
		public int? MemberId { get; set; }
        
		[Column("identifier_type")]
		public int IdentifierType { get; set; }
        
		[Column("action_id")]
		public int ActionId { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("is_read")]
		public bool IsRead { get; set; }
        
		[Column("read_time")]
		public DateTime? ReadTime { get; set; }
        
		[Column("is_remove")]
		public bool IsRemove { get; set; }
        
		[Column("remove_time")]
		public DateTime? RemoveTime { get; set; }
        
		[Column("trigger_type")]
		public int TriggerType { get; set; }
        
		[Column("is_deal")]
		public bool IsDeal { get; set; }
            
	}
}
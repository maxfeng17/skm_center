using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("beacon_group_device_link")]
	public partial class BeaconGroupDeviceLink
	{
        public BeaconGroupDeviceLink()
        {
		this.Floor = string.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("group_id")]
		public int GroupId { get; set; }
        
		[Column("device_id")]
		public int DeviceId { get; set; }
        
		[Column("floor")]
		public string Floor { get; set; }
        
		[Column("xtop")]
		public int Xtop { get; set; }
        
		[Column("xleft")]
		public int Xleft { get; set; }
            
	}
}
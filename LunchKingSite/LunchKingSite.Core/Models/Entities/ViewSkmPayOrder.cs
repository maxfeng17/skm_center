using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
	[Table("view_skm_pay_order")]
	public class ViewSkmPayOrder
	{
		public ViewSkmPayOrder()
		{
			OrderNo = string.Empty;
			SkmTradeNo = string.Empty;
			SkmRefundTradeNo = string.Empty;
			PreTransNo = string.Empty;
			OrderMemo = string.Empty;
			DealName = string.Empty;
			ItemNo = string.Empty;
			SpecialItemNo = string.Empty;
			ProductCode = string.Empty;
			ShopCode = string.Empty;
			ShopName = string.Empty;
			InvoiceNo = string.Empty;
			InvoiceDonateNo = string.Empty;
			CompId = string.Empty;
			InvoiceCarrierNo = string.Empty;
			VipNo = string.Empty;
		}

        [Key]
        [Column("order_id")]
		public int OrderId { get; set; }

		[Column("order_guid")]
		public Guid OrderGuid { get; set; }

		[Column("order_no")]
		public string OrderNo { get; set; }

		[Column("order_amount")]
		public int OrderAmount { get; set; }

		[Column("payment_amount")]
		public int PaymentAmount { get; set; }

		[Column("redeem_amount")]
		public int RedeemAmount { get; set; }

		[Column("redeem_point")]
		public int RedeemPoint { get; set; }

		[Column("burning_skm_point")]
		public int BurningSkmPoint { get; set; }

		[Column("order_status")]
		public int OrderStatus { get; set; }

		[Column("skm_trade_no", TypeName = "varchar")]
		public string SkmTradeNo { get; set; }

		[Column("skm_refund_trade_no", TypeName = "varchar")]
		public string SkmRefundTradeNo { get; set; }

		[Column("pre_trans_no", TypeName = "varchar")]
		public string PreTransNo { get; set; }

		[Column("main_bid")]
		public Guid MainBid { get; set; }

		[Column("bid")]
		public Guid Bid { get; set; }

		[Column("external_deal_guid")]
		public Guid ExternalDealGuid { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("create_date")]
		public DateTime CreateDate { get; set; }

		[Column("last_modify_date")]
		public DateTime? LastModifyDate { get; set; }

		[Column("refund_date")]
		public DateTime? RefundDate { get; set; }

		[Column("order_memo")]
		public string OrderMemo { get; set; }

		[Column("deal_name")]
		public string DealName { get; set; }

		[Column("item_no")]
		public string ItemNo { get; set; }

		[Column("special_item_no")]
		public string SpecialItemNo { get; set; }

		[Column("product_code")]
		public string ProductCode { get; set; }

		[Column("shop_code", TypeName = "varchar")]
		public string ShopCode { get; set; }

		[Column("shop_name")]
		public string ShopName { get; set; }

		[Column("invoice_id")]
		public int? InvoiceId { get; set; }

		[Column("invoice_date")]
		public DateTime? InvoiceDate { get; set; }

		[Column("invoice_no", TypeName = "varchar")]
		public string InvoiceNo { get; set; }

		[Column("invoice_amount")]
		public int? InvoiceAmount { get; set; }

		[Column("invoice_isdonate")]
		public bool? InvoiceIsdonate { get; set; }

		[Column("invoice_donate_no", TypeName = "varchar")]
		public string InvoiceDonateNo { get; set; }

		[Column("comp_id", TypeName = "varchar")]
		public string CompId { get; set; }

		[Column("invoice_carrier_type")]
		public int InvoiceCarrierType { get; set; }

		[Column("invoice_type")]
		public int? InvoiceType { get; set; }

		[Column("invoice_carrier_no", TypeName = "varchar")]
		public string InvoiceCarrierNo { get; set; }

		[Column("invoice_status")]
		public int? InvoiceStatus { get; set; }

		[Column("vip_no", TypeName = "varchar")]
		public string VipNo { get; set; }

		[Column("install_period")]
		public int InstallPeriod { get; set; }

		[Column("install_down_pay")]
		public int InstallDownPay { get; set; }

		[Column("install_each_pay")]
		public int InstallEachPay { get; set; }

		[Column("install_fee")]
		public int InstallFee { get; set; }
	}
}

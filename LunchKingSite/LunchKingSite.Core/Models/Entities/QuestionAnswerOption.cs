using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("question_answer_option")]
	public partial class QuestionAnswerOption
	{
        public QuestionAnswerOption()
        {
		this.AnswerContent = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("answer_id")]
		public int AnswerId { get; set; }
        
		[Column("option_id")]
		public int OptionId { get; set; }
        
		[Column("answer_content")]
		public string AnswerContent { get; set; }
            
	}
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("skm_activity")]
	public partial class SkmActivity
	{
        public SkmActivity()
        {
		this.Token = String.Empty;
		this.Subject = String.Empty;
        }
        
		[Key]
		[Column("id")]
		public int Id { get; set; }
        
		[Column("token")]
		public string Token { get; set; }
        
		[Column("subject")]
		public string Subject { get; set; }
        
		[Column("act_content")]
		public string ActContent { get; set; }
        
		[Column("create_time")]
		public DateTime CreateTime { get; set; }
        
		[Column("start_date")]
		public DateTime StartDate { get; set; }
        
		[Column("end_date")]
		public DateTime EndDate { get; set; }
        
		[Column("join_start_date")]
		public DateTime JoinStartDate { get; set; }
        
		[Column("join_end_date")]
		public DateTime JoinEndDate { get; set; }
        
		[Column("seller_guid")]
		public Guid SellerGuid { get; set; }
            
	}
}
using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace LunchKingSite.Core.Models.Entities
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	[Table("view_beacon_event_time_slot")]
	public partial class ViewBeaconEventTimeSlot
    {
        [Key]
        [Column("nid")]
        public Guid Nid { get; set; }
        
		[Column("time_slot_id")]
		public int TimeSlotId { get; set; }
        
		[Column("effective_date")]
		public DateTime EffectiveDate { get; set; }
        
		[Column("sequence")]
		public int Sequence { get; set; }
        
		[Column("status")]
		public byte Status { get; set; }
        
		[Column("group_id")]
		public int GroupId { get; set; }
        
		[Column("trigger_app_id")]
		public int TriggerAppId { get; set; }
        
		[Column("event_id")]
		public int EventId { get; set; }
        
		[Column("subject")]
		public string Subject { get; set; }
        
		[Column("content")]
		public string Content { get; set; }
        
		[Column("event_type")]
		public byte EventType { get; set; }
        
		[Column("action_bid")]
		public Guid ActionBid { get; set; }
        
		[Column("action_url")]
		public string ActionUrl { get; set; }
        
		[Column("enabled")]
		public bool Enabled { get; set; }
        
		[Column("floor")]
		public string Floor { get; set; }
             
    }
}
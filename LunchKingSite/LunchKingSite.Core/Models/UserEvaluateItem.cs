﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the UserEvaluateItem class.
    /// </summary>
    [Serializable]
    public partial class UserEvaluateItemCollection : RepositoryList<UserEvaluateItem, UserEvaluateItemCollection>
    {
        public UserEvaluateItemCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>UserEvaluateItemCollection</returns>
        public UserEvaluateItemCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                UserEvaluateItem o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the user_evaluate_item table.
    /// </summary>
    [Serializable]
    public partial class UserEvaluateItem : RepositoryRecord<UserEvaluateItem>, IRecordBase
    {
        #region .ctors and Default Settings

        public UserEvaluateItem()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public UserEvaluateItem(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        public UserEvaluateItem(object keyID)
        {
            SetSQLProps();
            InitSetDefaults();
        }

        public UserEvaluateItem(string columnName, object columnValue)
        {
            SetSQLProps();
            InitSetDefaults();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("user_evaluate_item", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 20;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                colvarItemName.DefaultSetting = @"";
                colvarItemName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarItemDesc = new TableSchema.TableColumn(schema);
                colvarItemDesc.ColumnName = "item_desc";
                colvarItemDesc.DataType = DbType.String;
                colvarItemDesc.MaxLength = 50;
                colvarItemDesc.AutoIncrement = false;
                colvarItemDesc.IsNullable = true;
                colvarItemDesc.IsPrimaryKey = false;
                colvarItemDesc.IsForeignKey = false;
                colvarItemDesc.IsReadOnly = false;
                colvarItemDesc.DefaultSetting = @"";
                colvarItemDesc.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemDesc);

                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = true;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                colvarSequence.DefaultSetting = @"";
                colvarSequence.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSequence);

                TableSchema.TableColumn colvarIsRequired = new TableSchema.TableColumn(schema);
                colvarIsRequired.ColumnName = "is_required";
                colvarIsRequired.DataType = DbType.Boolean;
                colvarIsRequired.MaxLength = 0;
                colvarIsRequired.AutoIncrement = false;
                colvarIsRequired.IsNullable = false;
                colvarIsRequired.IsPrimaryKey = false;
                colvarIsRequired.IsForeignKey = false;
                colvarIsRequired.IsReadOnly = false;

                colvarIsRequired.DefaultSetting = @"((0))";
                colvarIsRequired.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsRequired);

                TableSchema.TableColumn colvarIsEnable = new TableSchema.TableColumn(schema);
                colvarIsEnable.ColumnName = "is_enable";
                colvarIsEnable.DataType = DbType.Boolean;
                colvarIsEnable.MaxLength = 0;
                colvarIsEnable.AutoIncrement = false;
                colvarIsEnable.IsNullable = false;
                colvarIsEnable.IsPrimaryKey = false;
                colvarIsEnable.IsForeignKey = false;
                colvarIsEnable.IsReadOnly = false;

                colvarIsEnable.DefaultSetting = @"((0))";
                colvarIsEnable.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsEnable);

                TableSchema.TableColumn colvarIsCount = new TableSchema.TableColumn(schema);
                colvarIsCount.ColumnName = "is_count";
                colvarIsCount.DataType = DbType.Boolean;
                colvarIsCount.MaxLength = 0;
                colvarIsCount.AutoIncrement = false;
                colvarIsCount.IsNullable = false;
                colvarIsCount.IsPrimaryKey = false;
                colvarIsCount.IsForeignKey = false;
                colvarIsCount.IsReadOnly = false;

                colvarIsCount.DefaultSetting = @"((0))";
                colvarIsCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsCount);

                TableSchema.TableColumn colvarEvaluateType = new TableSchema.TableColumn(schema);
                colvarEvaluateType.ColumnName = "evaluate_type";
                colvarEvaluateType.DataType = DbType.Int32;
                colvarEvaluateType.MaxLength = 0;
                colvarEvaluateType.AutoIncrement = false;
                colvarEvaluateType.IsNullable = false;
                colvarEvaluateType.IsPrimaryKey = false;
                colvarEvaluateType.IsForeignKey = false;
                colvarEvaluateType.IsReadOnly = false;
                colvarEvaluateType.DefaultSetting = @"";
                colvarEvaluateType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEvaluateType);

                TableSchema.TableColumn colvarCombineItem = new TableSchema.TableColumn(schema);
                colvarCombineItem.ColumnName = "combine_item";
                colvarCombineItem.DataType = DbType.String;
                colvarCombineItem.MaxLength = 20;
                colvarCombineItem.AutoIncrement = false;
                colvarCombineItem.IsNullable = true;
                colvarCombineItem.IsPrimaryKey = false;
                colvarCombineItem.IsForeignKey = false;
                colvarCombineItem.IsReadOnly = false;
                colvarCombineItem.DefaultSetting = @"";
                colvarCombineItem.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCombineItem);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 50;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("user_evaluate_item", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get { return GetColumnValue<string>(Columns.ItemName); }
            set { SetColumnValue(Columns.ItemName, value); }
        }

        [XmlAttribute("ItemDesc")]
        [Bindable(true)]
        public string ItemDesc
        {
            get { return GetColumnValue<string>(Columns.ItemDesc); }
            set { SetColumnValue(Columns.ItemDesc, value); }
        }

        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public int? Sequence
        {
            get { return GetColumnValue<int?>(Columns.Sequence); }
            set { SetColumnValue(Columns.Sequence, value); }
        }

        [XmlAttribute("IsRequired")]
        [Bindable(true)]
        public bool IsRequired
        {
            get { return GetColumnValue<bool>(Columns.IsRequired); }
            set { SetColumnValue(Columns.IsRequired, value); }
        }

        [XmlAttribute("IsEnable")]
        [Bindable(true)]
        public bool IsEnable
        {
            get { return GetColumnValue<bool>(Columns.IsEnable); }
            set { SetColumnValue(Columns.IsEnable, value); }
        }

        [XmlAttribute("IsCount")]
        [Bindable(true)]
        public bool IsCount
        {
            get { return GetColumnValue<bool>(Columns.IsCount); }
            set { SetColumnValue(Columns.IsCount, value); }
        }

        [XmlAttribute("EvaluateType")]
        [Bindable(true)]
        public int EvaluateType
        {
            get { return GetColumnValue<int>(Columns.EvaluateType); }
            set { SetColumnValue(Columns.EvaluateType, value); }
        }

        [XmlAttribute("CombineItem")]
        [Bindable(true)]
        public string CombineItem
        {
            get { return GetColumnValue<string>(Columns.CombineItem); }
            set { SetColumnValue(Columns.CombineItem, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)



        #region ObjectDataSource support


        /// <summary>
        /// Inserts a record, can be used with the Object Data Source
        /// </summary>
        public static void Insert(string varItemName, string varItemDesc, int? varSequence, bool varIsRequired, bool varIsEnable, bool varIsCount, int varEvaluateType, string varCombineItem, DateTime? varCreateTime, string varCreateId, DateTime? varModifyTime, string varModifyId)
        {
            UserEvaluateItem item = new UserEvaluateItem();

            item.ItemName = varItemName;

            item.ItemDesc = varItemDesc;

            item.Sequence = varSequence;

            item.IsRequired = varIsRequired;

            item.IsEnable = varIsEnable;

            item.IsCount = varIsCount;

            item.EvaluateType = varEvaluateType;

            item.CombineItem = varCombineItem;

            item.CreateTime = varCreateTime;

            item.CreateId = varCreateId;

            item.ModifyTime = varModifyTime;

            item.ModifyId = varModifyId;
        }

        /// <summary>
        /// Updates a record, can be used with the Object Data Source
        /// </summary>
        public static void Update(int varId, string varItemName, string varItemDesc, int? varSequence, bool varIsRequired, bool varIsEnable, bool varIsCount, int varEvaluateType, string varCombineItem, DateTime? varCreateTime, string varCreateId, DateTime? varModifyTime, string varModifyId)
        {
            UserEvaluateItem item = new UserEvaluateItem();

            item.Id = varId;

            item.ItemName = varItemName;

            item.ItemDesc = varItemDesc;

            item.Sequence = varSequence;

            item.IsRequired = varIsRequired;

            item.IsEnable = varIsEnable;

            item.IsCount = varIsCount;

            item.EvaluateType = varEvaluateType;

            item.CombineItem = varCombineItem;

            item.CreateTime = varCreateTime;

            item.CreateId = varCreateId;

            item.ModifyTime = varModifyTime;

            item.ModifyId = varModifyId;

            item.IsNew = false;
        }
        #endregion



        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ItemDescColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn IsRequiredColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn IsEnableColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn IsCountColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn EvaluateTypeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CombineItemColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[12]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string ItemName = @"item_name";
            public static string ItemDesc = @"item_desc";
            public static string Sequence = @"sequence";
            public static string IsRequired = @"is_required";
            public static string IsEnable = @"is_enable";
            public static string IsCount = @"is_count";
            public static string EvaluateType = @"evaluate_type";
            public static string CombineItem = @"combine_item";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string ModifyTime = @"modify_time";
            public static string ModifyId = @"modify_id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealSellerRelationship class.
	/// </summary>
    [Serializable]
	public partial class DealSellerRelationshipCollection : RepositoryList<DealSellerRelationship, DealSellerRelationshipCollection>
	{	   
		public DealSellerRelationshipCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealSellerRelationshipCollection</returns>
		public DealSellerRelationshipCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealSellerRelationship o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_seller_relationship table.
	/// </summary>
	[Serializable]
	public partial class DealSellerRelationship : RepositoryRecord<DealSellerRelationship>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealSellerRelationship()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DealSellerRelationship(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("deal_seller_relationship", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
				colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeS.MaxLength = 0;
				colvarBusinessHourOrderTimeS.AutoIncrement = false;
				colvarBusinessHourOrderTimeS.IsNullable = false;
				colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeS.IsForeignKey = false;
				colvarBusinessHourOrderTimeS.IsReadOnly = false;
				colvarBusinessHourOrderTimeS.DefaultSetting = @"";
				colvarBusinessHourOrderTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeS);
				
				TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
				colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeE.MaxLength = 0;
				colvarBusinessHourOrderTimeE.AutoIncrement = false;
				colvarBusinessHourOrderTimeE.IsNullable = false;
				colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeE.IsForeignKey = false;
				colvarBusinessHourOrderTimeE.IsReadOnly = false;
				colvarBusinessHourOrderTimeE.DefaultSetting = @"";
				colvarBusinessHourOrderTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeE);
				
				TableSchema.TableColumn colvarLastEndTime = new TableSchema.TableColumn(schema);
				colvarLastEndTime.ColumnName = "last_end_time";
				colvarLastEndTime.DataType = DbType.DateTime;
				colvarLastEndTime.MaxLength = 0;
				colvarLastEndTime.AutoIncrement = false;
				colvarLastEndTime.IsNullable = true;
				colvarLastEndTime.IsPrimaryKey = false;
				colvarLastEndTime.IsForeignKey = false;
				colvarLastEndTime.IsReadOnly = false;
				colvarLastEndTime.DefaultSetting = @"";
				colvarLastEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastEndTime);
				
				TableSchema.TableColumn colvarIsNewDeal = new TableSchema.TableColumn(schema);
				colvarIsNewDeal.ColumnName = "is_new_deal";
				colvarIsNewDeal.DataType = DbType.Boolean;
				colvarIsNewDeal.MaxLength = 0;
				colvarIsNewDeal.AutoIncrement = false;
				colvarIsNewDeal.IsNullable = false;
				colvarIsNewDeal.IsPrimaryKey = false;
				colvarIsNewDeal.IsForeignKey = false;
				colvarIsNewDeal.IsReadOnly = false;
				colvarIsNewDeal.DefaultSetting = @"";
				colvarIsNewDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsNewDeal);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 50;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = true;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("deal_seller_relationship",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("BusinessHourOrderTimeS")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeS 
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeS); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeS, value); }
		}
		  
		[XmlAttribute("BusinessHourOrderTimeE")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeE 
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeE); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeE, value); }
		}
		  
		[XmlAttribute("LastEndTime")]
		[Bindable(true)]
		public DateTime? LastEndTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastEndTime); }
			set { SetColumnValue(Columns.LastEndTime, value); }
		}
		  
		[XmlAttribute("IsNewDeal")]
		[Bindable(true)]
		public bool IsNewDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsNewDeal); }
			set { SetColumnValue(Columns.IsNewDeal, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourOrderTimeSColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourOrderTimeEColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn LastEndTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IsNewDealColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SellerGuid = @"seller_guid";
			 public static string Bid = @"bid";
			 public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
			 public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
			 public static string LastEndTime = @"last_end_time";
			 public static string IsNewDeal = @"is_new_deal";
			 public static string Memo = @"memo";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

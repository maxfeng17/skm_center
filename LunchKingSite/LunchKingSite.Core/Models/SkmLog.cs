using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmLog class.
	/// </summary>
    [Serializable]
	public partial class SkmLogCollection : RepositoryList<SkmLog, SkmLogCollection>
	{	   
		public SkmLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmLogCollection</returns>
		public SkmLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_log table.
	/// </summary>
	[Serializable]
	public partial class SkmLog : RepositoryRecord<SkmLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSkmToken = new TableSchema.TableColumn(schema);
				colvarSkmToken.ColumnName = "skm_token";
				colvarSkmToken.DataType = DbType.String;
				colvarSkmToken.MaxLength = 50;
				colvarSkmToken.AutoIncrement = false;
				colvarSkmToken.IsNullable = false;
				colvarSkmToken.IsPrimaryKey = false;
				colvarSkmToken.IsForeignKey = false;
				colvarSkmToken.IsReadOnly = false;
				colvarSkmToken.DefaultSetting = @"";
				colvarSkmToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmToken);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				
						colvarUserId.DefaultSetting = @"((0))";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarInputValue = new TableSchema.TableColumn(schema);
				colvarInputValue.ColumnName = "input_value";
				colvarInputValue.DataType = DbType.String;
				colvarInputValue.MaxLength = -1;
				colvarInputValue.AutoIncrement = false;
				colvarInputValue.IsNullable = true;
				colvarInputValue.IsPrimaryKey = false;
				colvarInputValue.IsForeignKey = false;
				colvarInputValue.IsReadOnly = false;
				colvarInputValue.DefaultSetting = @"";
				colvarInputValue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInputValue);
				
				TableSchema.TableColumn colvarOutputValue = new TableSchema.TableColumn(schema);
				colvarOutputValue.ColumnName = "output_value";
				colvarOutputValue.DataType = DbType.String;
				colvarOutputValue.MaxLength = -1;
				colvarOutputValue.AutoIncrement = false;
				colvarOutputValue.IsNullable = true;
				colvarOutputValue.IsPrimaryKey = false;
				colvarOutputValue.IsForeignKey = false;
				colvarOutputValue.IsReadOnly = false;
				colvarOutputValue.DefaultSetting = @"";
				colvarOutputValue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOutputValue);
				
				TableSchema.TableColumn colvarLogType = new TableSchema.TableColumn(schema);
				colvarLogType.ColumnName = "log_type";
				colvarLogType.DataType = DbType.Int32;
				colvarLogType.MaxLength = 0;
				colvarLogType.AutoIncrement = false;
				colvarLogType.IsNullable = false;
				colvarLogType.IsPrimaryKey = false;
				colvarLogType.IsForeignKey = false;
				colvarLogType.IsReadOnly = false;
				
						colvarLogType.DefaultSetting = @"((0))";
				colvarLogType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogType);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SkmToken")]
		[Bindable(true)]
		public string SkmToken 
		{
			get { return GetColumnValue<string>(Columns.SkmToken); }
			set { SetColumnValue(Columns.SkmToken, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("InputValue")]
		[Bindable(true)]
		public string InputValue 
		{
			get { return GetColumnValue<string>(Columns.InputValue); }
			set { SetColumnValue(Columns.InputValue, value); }
		}
		  
		[XmlAttribute("OutputValue")]
		[Bindable(true)]
		public string OutputValue 
		{
			get { return GetColumnValue<string>(Columns.OutputValue); }
			set { SetColumnValue(Columns.OutputValue, value); }
		}
		  
		[XmlAttribute("LogType")]
		[Bindable(true)]
		public int LogType 
		{
			get { return GetColumnValue<int>(Columns.LogType); }
			set { SetColumnValue(Columns.LogType, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmTokenColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn InputValueColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OutputValueColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn LogTypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SkmToken = @"skm_token";
			 public static string UserId = @"user_id";
			 public static string InputValue = @"input_value";
			 public static string OutputValue = @"output_value";
			 public static string LogType = @"log_type";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

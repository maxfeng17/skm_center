﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Models
{
    public class CARDECPayOtherRtnModel
    {
        /// <summary>
        /// 交易結果回應碼
        /// </summary>
        public string retCode { get; set; }
        /// <summary>
        /// 訂單狀態碼
        /// </summary>
        public string orderStatus { get; set; }
        /// <summary>
        /// 授權方式
        /// </summary>
        public string authType { get; set; }
        /// <summary>
        /// 幣別
        /// </summary>
        public string currency { get; set; }
        /// <summary>
        /// 採購日期
        /// </summary>
        public string purchaseDate { get; set; }
        /// <summary>
        /// 交易金額 包含兩位小數，如100 代表 1.00 元
        /// </summary>
        public string transAmt { get; set; }
        /// <summary>
        /// 授權碼
        /// </summary>
        public string authIdResp { get; set; }
        /// <summary>
        /// 調單編號
        /// </summary>
        public string rrn { get; set; }
        /// <summary>
        /// 請款金額包 含兩位小數，如100 代表1.00 元。
        /// </summary>
        public string settleAmt { get; set; }
        /// <summary>
        /// 請款批號
        /// </summary>
        public string settleSeq { get; set; }
        /// <summary>
        /// 請款日期
        /// </summary>
        public string settleDate { get; set; }
        /// <summary>
        /// 退貨金額 包含兩位小數，如100 代表1.00 元。
        /// </summary>
        public string refundTransAmt { get; set; }
        /// <summary>
        /// 退貨調單編號
        /// </summary>
        public string refundRRN { get; set; }
        /// <summary>
        /// 退貨授權碼
        /// </summary>
        public string refundAuthIdResp { get; set; }
        /// <summary>
        /// 退貨日期
        /// </summary>
        public string refundDate { get; set; }
        /// <summary>
        /// 紅利訂單號碼
        /// </summary>
        public string redeemOrderNo { get; set; }
        /// <summary>
        /// 折抵點數
        /// </summary>
        public string redeemPoint { get; set; }
        /// <summary>
        /// 折抵金額 包含兩位小數，如100 代表1.00 元。
        /// </summary>
        public string redeemAmt { get; set; }
        /// <summary>
        /// 實付金額  包含兩位小數，如100 代表1.00 元。
        /// </summary>
        public string postRedeemAmt { get; set; }
        /// <summary>
        /// 剩餘點數
        /// </summary>
        public string postRedeemPoint { get; set; }
        /// <summary>
        /// 分期訂單號碼
        /// </summary>
        public string installOrderNo { get; set; }
        /// <summary>
        /// 分期期數
        /// </summary>
        public string installPeriod { get; set; }
        /// <summary>
        /// 首期金額 包含兩位小數，如100 代表1.00 元。
        /// </summary>
        public string installDownPay { get; set; }
        /// <summary>
        /// 每期金額 包含兩位小數，如100 代表1.00 元。
        /// </summary>
        public string installPay { get; set; }
        /// <summary>
        /// 首期手續費  包含兩位小數，如100 代表1.00 元。
        /// </summary>
        public string installDownPayFee { get; set; }
    }
}

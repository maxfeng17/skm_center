using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBookingSystemCouponReservation class.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemCouponReservationCollection : ReadOnlyList<ViewBookingSystemCouponReservation, ViewBookingSystemCouponReservationCollection>
    {        
        public ViewBookingSystemCouponReservationCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_booking_system_coupon_reservation view.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemCouponReservation : ReadOnlyRecord<ViewBookingSystemCouponReservation>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_booking_system_coupon_reservation", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBookingType = new TableSchema.TableColumn(schema);
                colvarBookingType.ColumnName = "booking_type";
                colvarBookingType.DataType = DbType.Int32;
                colvarBookingType.MaxLength = 0;
                colvarBookingType.AutoIncrement = false;
                colvarBookingType.IsNullable = false;
                colvarBookingType.IsPrimaryKey = false;
                colvarBookingType.IsForeignKey = false;
                colvarBookingType.IsReadOnly = false;
                
                schema.Columns.Add(colvarBookingType);
                
                TableSchema.TableColumn colvarApiId = new TableSchema.TableColumn(schema);
                colvarApiId.ColumnName = "api_id";
                colvarApiId.DataType = DbType.Int32;
                colvarApiId.MaxLength = 0;
                colvarApiId.AutoIncrement = false;
                colvarApiId.IsNullable = true;
                colvarApiId.IsPrimaryKey = false;
                colvarApiId.IsForeignKey = false;
                colvarApiId.IsReadOnly = false;
                
                schema.Columns.Add(colvarApiId);
                
                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.AnsiString;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = false;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponSequenceNumber);
                
                TableSchema.TableColumn colvarOrderKey = new TableSchema.TableColumn(schema);
                colvarOrderKey.ColumnName = "order_key";
                colvarOrderKey.DataType = DbType.AnsiString;
                colvarOrderKey.MaxLength = 50;
                colvarOrderKey.AutoIncrement = false;
                colvarOrderKey.IsNullable = true;
                colvarOrderKey.IsPrimaryKey = false;
                colvarOrderKey.IsForeignKey = false;
                colvarOrderKey.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderKey);
                
                TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
                colvarOrderDetailGuid.ColumnName = "order_detail_guid";
                colvarOrderDetailGuid.DataType = DbType.Guid;
                colvarOrderDetailGuid.MaxLength = 0;
                colvarOrderDetailGuid.AutoIncrement = false;
                colvarOrderDetailGuid.IsNullable = false;
                colvarOrderDetailGuid.IsPrimaryKey = false;
                colvarOrderDetailGuid.IsForeignKey = false;
                colvarOrderDetailGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailGuid);
                
                TableSchema.TableColumn colvarReservationDate = new TableSchema.TableColumn(schema);
                colvarReservationDate.ColumnName = "reservation_date";
                colvarReservationDate.DataType = DbType.DateTime;
                colvarReservationDate.MaxLength = 0;
                colvarReservationDate.AutoIncrement = false;
                colvarReservationDate.IsNullable = false;
                colvarReservationDate.IsPrimaryKey = false;
                colvarReservationDate.IsForeignKey = false;
                colvarReservationDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarReservationDate);
                
                TableSchema.TableColumn colvarTimeSlot = new TableSchema.TableColumn(schema);
                colvarTimeSlot.ColumnName = "time_slot";
                colvarTimeSlot.DataType = DbType.AnsiString;
                colvarTimeSlot.MaxLength = 5;
                colvarTimeSlot.AutoIncrement = false;
                colvarTimeSlot.IsNullable = false;
                colvarTimeSlot.IsPrimaryKey = false;
                colvarTimeSlot.IsForeignKey = false;
                colvarTimeSlot.IsReadOnly = false;
                
                schema.Columns.Add(colvarTimeSlot);
                
                TableSchema.TableColumn colvarReservationDateTime = new TableSchema.TableColumn(schema);
                colvarReservationDateTime.ColumnName = "reservation_date_time";
                colvarReservationDateTime.DataType = DbType.DateTime;
                colvarReservationDateTime.MaxLength = 0;
                colvarReservationDateTime.AutoIncrement = false;
                colvarReservationDateTime.IsNullable = true;
                colvarReservationDateTime.IsPrimaryKey = false;
                colvarReservationDateTime.IsForeignKey = false;
                colvarReservationDateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReservationDateTime);
                
                TableSchema.TableColumn colvarCreateDatetime = new TableSchema.TableColumn(schema);
                colvarCreateDatetime.ColumnName = "create_datetime";
                colvarCreateDatetime.DataType = DbType.DateTime;
                colvarCreateDatetime.MaxLength = 0;
                colvarCreateDatetime.AutoIncrement = false;
                colvarCreateDatetime.IsNullable = false;
                colvarCreateDatetime.IsPrimaryKey = false;
                colvarCreateDatetime.IsForeignKey = false;
                colvarCreateDatetime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateDatetime);
                
                TableSchema.TableColumn colvarIsCancel = new TableSchema.TableColumn(schema);
                colvarIsCancel.ColumnName = "is_cancel";
                colvarIsCancel.DataType = DbType.Boolean;
                colvarIsCancel.MaxLength = 0;
                colvarIsCancel.AutoIncrement = false;
                colvarIsCancel.IsNullable = false;
                colvarIsCancel.IsPrimaryKey = false;
                colvarIsCancel.IsForeignKey = false;
                colvarIsCancel.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCancel);
                
                TableSchema.TableColumn colvarCancelDate = new TableSchema.TableColumn(schema);
                colvarCancelDate.ColumnName = "cancel_date";
                colvarCancelDate.DataType = DbType.DateTime;
                colvarCancelDate.MaxLength = 0;
                colvarCancelDate.AutoIncrement = false;
                colvarCancelDate.IsNullable = true;
                colvarCancelDate.IsPrimaryKey = false;
                colvarCancelDate.IsForeignKey = false;
                colvarCancelDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCancelDate);
                
                TableSchema.TableColumn colvarIsLock = new TableSchema.TableColumn(schema);
                colvarIsLock.ColumnName = "is_lock";
                colvarIsLock.DataType = DbType.Boolean;
                colvarIsLock.MaxLength = 0;
                colvarIsLock.AutoIncrement = false;
                colvarIsLock.IsNullable = false;
                colvarIsLock.IsPrimaryKey = false;
                colvarIsLock.IsForeignKey = false;
                colvarIsLock.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsLock);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_booking_system_coupon_reservation",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBookingSystemCouponReservation()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBookingSystemCouponReservation(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBookingSystemCouponReservation(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBookingSystemCouponReservation(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BookingType")]
        [Bindable(true)]
        public int BookingType 
	    {
		    get
		    {
			    return GetColumnValue<int>("booking_type");
		    }
            set 
		    {
			    SetColumnValue("booking_type", value);
            }
        }
	      
        [XmlAttribute("ApiId")]
        [Bindable(true)]
        public int? ApiId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("api_id");
		    }
            set 
		    {
			    SetColumnValue("api_id", value);
            }
        }
	      
        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_sequence_number");
		    }
            set 
		    {
			    SetColumnValue("coupon_sequence_number", value);
            }
        }
	      
        [XmlAttribute("OrderKey")]
        [Bindable(true)]
        public string OrderKey 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_key");
		    }
            set 
		    {
			    SetColumnValue("order_key", value);
            }
        }
	      
        [XmlAttribute("OrderDetailGuid")]
        [Bindable(true)]
        public Guid OrderDetailGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_detail_guid");
		    }
            set 
		    {
			    SetColumnValue("order_detail_guid", value);
            }
        }
	      
        [XmlAttribute("ReservationDate")]
        [Bindable(true)]
        public DateTime ReservationDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("reservation_date");
		    }
            set 
		    {
			    SetColumnValue("reservation_date", value);
            }
        }
	      
        [XmlAttribute("TimeSlot")]
        [Bindable(true)]
        public string TimeSlot 
	    {
		    get
		    {
			    return GetColumnValue<string>("time_slot");
		    }
            set 
		    {
			    SetColumnValue("time_slot", value);
            }
        }
	      
        [XmlAttribute("ReservationDateTime")]
        [Bindable(true)]
        public DateTime? ReservationDateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("reservation_date_time");
		    }
            set 
		    {
			    SetColumnValue("reservation_date_time", value);
            }
        }
	      
        [XmlAttribute("CreateDatetime")]
        [Bindable(true)]
        public DateTime CreateDatetime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_datetime");
		    }
            set 
		    {
			    SetColumnValue("create_datetime", value);
            }
        }
	      
        [XmlAttribute("IsCancel")]
        [Bindable(true)]
        public bool IsCancel 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_cancel");
		    }
            set 
		    {
			    SetColumnValue("is_cancel", value);
            }
        }
	      
        [XmlAttribute("CancelDate")]
        [Bindable(true)]
        public DateTime? CancelDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("cancel_date");
		    }
            set 
		    {
			    SetColumnValue("cancel_date", value);
            }
        }
	      
        [XmlAttribute("IsLock")]
        [Bindable(true)]
        public bool IsLock 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_lock");
		    }
            set 
		    {
			    SetColumnValue("is_lock", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BookingType = @"booking_type";
            
            public static string ApiId = @"api_id";
            
            public static string CouponSequenceNumber = @"coupon_sequence_number";
            
            public static string OrderKey = @"order_key";
            
            public static string OrderDetailGuid = @"order_detail_guid";
            
            public static string ReservationDate = @"reservation_date";
            
            public static string TimeSlot = @"time_slot";
            
            public static string ReservationDateTime = @"reservation_date_time";
            
            public static string CreateDatetime = @"create_datetime";
            
            public static string IsCancel = @"is_cancel";
            
            public static string CancelDate = @"cancel_date";
            
            public static string IsLock = @"is_lock";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

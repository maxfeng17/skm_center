using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealOrderMemberOrderShow class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealOrderMemberOrderShowCollection : ReadOnlyList<ViewHiDealOrderMemberOrderShow, ViewHiDealOrderMemberOrderShowCollection>
    {        
        public ViewHiDealOrderMemberOrderShowCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_order_member_order_show view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealOrderMemberOrderShow : ReadOnlyRecord<ViewHiDealOrderMemberOrderShow>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_order_member_order_show", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarPk = new TableSchema.TableColumn(schema);
                colvarPk.ColumnName = "pk";
                colvarPk.DataType = DbType.Int32;
                colvarPk.MaxLength = 0;
                colvarPk.AutoIncrement = false;
                colvarPk.IsNullable = false;
                colvarPk.IsPrimaryKey = false;
                colvarPk.IsForeignKey = false;
                colvarPk.IsReadOnly = false;
                
                schema.Columns.Add(colvarPk);
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = false;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarHiDealId = new TableSchema.TableColumn(schema);
                colvarHiDealId.ColumnName = "hi_deal_id";
                colvarHiDealId.DataType = DbType.Int32;
                colvarHiDealId.MaxLength = 0;
                colvarHiDealId.AutoIncrement = false;
                colvarHiDealId.IsNullable = false;
                colvarHiDealId.IsPrimaryKey = false;
                colvarHiDealId.IsForeignKey = false;
                colvarHiDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealId);
                
                TableSchema.TableColumn colvarTotalAmount = new TableSchema.TableColumn(schema);
                colvarTotalAmount.ColumnName = "total_amount";
                colvarTotalAmount.DataType = DbType.Currency;
                colvarTotalAmount.MaxLength = 0;
                colvarTotalAmount.AutoIncrement = false;
                colvarTotalAmount.IsNullable = false;
                colvarTotalAmount.IsPrimaryKey = false;
                colvarTotalAmount.IsForeignKey = false;
                colvarTotalAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalAmount);
                
                TableSchema.TableColumn colvarPcash = new TableSchema.TableColumn(schema);
                colvarPcash.ColumnName = "pcash";
                colvarPcash.DataType = DbType.Currency;
                colvarPcash.MaxLength = 0;
                colvarPcash.AutoIncrement = false;
                colvarPcash.IsNullable = false;
                colvarPcash.IsPrimaryKey = false;
                colvarPcash.IsForeignKey = false;
                colvarPcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcash);
                
                TableSchema.TableColumn colvarScash = new TableSchema.TableColumn(schema);
                colvarScash.ColumnName = "scash";
                colvarScash.DataType = DbType.Currency;
                colvarScash.MaxLength = 0;
                colvarScash.AutoIncrement = false;
                colvarScash.IsNullable = false;
                colvarScash.IsPrimaryKey = false;
                colvarScash.IsForeignKey = false;
                colvarScash.IsReadOnly = false;
                
                schema.Columns.Add(colvarScash);
                
                TableSchema.TableColumn colvarBcash = new TableSchema.TableColumn(schema);
                colvarBcash.ColumnName = "bcash";
                colvarBcash.DataType = DbType.Currency;
                colvarBcash.MaxLength = 0;
                colvarBcash.AutoIncrement = false;
                colvarBcash.IsNullable = false;
                colvarBcash.IsPrimaryKey = false;
                colvarBcash.IsForeignKey = false;
                colvarBcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarBcash);
                
                TableSchema.TableColumn colvarCreditCardAmt = new TableSchema.TableColumn(schema);
                colvarCreditCardAmt.ColumnName = "credit_card_amt";
                colvarCreditCardAmt.DataType = DbType.Currency;
                colvarCreditCardAmt.MaxLength = 0;
                colvarCreditCardAmt.AutoIncrement = false;
                colvarCreditCardAmt.IsNullable = false;
                colvarCreditCardAmt.IsPrimaryKey = false;
                colvarCreditCardAmt.IsForeignKey = false;
                colvarCreditCardAmt.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreditCardAmt);
                
                TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
                colvarDiscountAmount.ColumnName = "discount_amount";
                colvarDiscountAmount.DataType = DbType.Currency;
                colvarDiscountAmount.MaxLength = 0;
                colvarDiscountAmount.AutoIncrement = false;
                colvarDiscountAmount.IsNullable = false;
                colvarDiscountAmount.IsPrimaryKey = false;
                colvarDiscountAmount.IsForeignKey = false;
                colvarDiscountAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountAmount);
                
                TableSchema.TableColumn colvarAtmAmount = new TableSchema.TableColumn(schema);
                colvarAtmAmount.ColumnName = "atm_amount";
                colvarAtmAmount.DataType = DbType.Currency;
                colvarAtmAmount.MaxLength = 0;
                colvarAtmAmount.AutoIncrement = false;
                colvarAtmAmount.IsNullable = false;
                colvarAtmAmount.IsPrimaryKey = false;
                colvarAtmAmount.IsForeignKey = false;
                colvarAtmAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAtmAmount);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "product_name";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 250;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = true;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductName);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = true;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarPaymentType = new TableSchema.TableColumn(schema);
                colvarPaymentType.ColumnName = "payment_type";
                colvarPaymentType.DataType = DbType.Int32;
                colvarPaymentType.MaxLength = 0;
                colvarPaymentType.AutoIncrement = false;
                colvarPaymentType.IsNullable = true;
                colvarPaymentType.IsPrimaryKey = false;
                colvarPaymentType.IsForeignKey = false;
                colvarPaymentType.IsReadOnly = false;
                
                schema.Columns.Add(colvarPaymentType);
                
                TableSchema.TableColumn colvarOrderShowStatus = new TableSchema.TableColumn(schema);
                colvarOrderShowStatus.ColumnName = "order_show_status";
                colvarOrderShowStatus.DataType = DbType.Int32;
                colvarOrderShowStatus.MaxLength = 0;
                colvarOrderShowStatus.AutoIncrement = false;
                colvarOrderShowStatus.IsNullable = true;
                colvarOrderShowStatus.IsPrimaryKey = false;
                colvarOrderShowStatus.IsForeignKey = false;
                colvarOrderShowStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderShowStatus);
                
                TableSchema.TableColumn colvarRemainCount = new TableSchema.TableColumn(schema);
                colvarRemainCount.ColumnName = "remain_count";
                colvarRemainCount.DataType = DbType.Int32;
                colvarRemainCount.MaxLength = 0;
                colvarRemainCount.AutoIncrement = false;
                colvarRemainCount.IsNullable = true;
                colvarRemainCount.IsPrimaryKey = false;
                colvarRemainCount.IsForeignKey = false;
                colvarRemainCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemainCount);
                
                TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
                colvarUseStartTime.ColumnName = "use_start_time";
                colvarUseStartTime.DataType = DbType.DateTime;
                colvarUseStartTime.MaxLength = 0;
                colvarUseStartTime.AutoIncrement = false;
                colvarUseStartTime.IsNullable = true;
                colvarUseStartTime.IsPrimaryKey = false;
                colvarUseStartTime.IsForeignKey = false;
                colvarUseStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseStartTime);
                
                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.DateTime;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseEndTime);
                
                TableSchema.TableColumn colvarOrderEndTime = new TableSchema.TableColumn(schema);
                colvarOrderEndTime.ColumnName = "order_end_time";
                colvarOrderEndTime.DataType = DbType.DateTime;
                colvarOrderEndTime.MaxLength = 0;
                colvarOrderEndTime.AutoIncrement = false;
                colvarOrderEndTime.IsNullable = true;
                colvarOrderEndTime.IsPrimaryKey = false;
                colvarOrderEndTime.IsForeignKey = false;
                colvarOrderEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderEndTime);
                
                TableSchema.TableColumn colvarIsOrderComplete = new TableSchema.TableColumn(schema);
                colvarIsOrderComplete.ColumnName = "is_order_complete";
                colvarIsOrderComplete.DataType = DbType.Boolean;
                colvarIsOrderComplete.MaxLength = 0;
                colvarIsOrderComplete.AutoIncrement = false;
                colvarIsOrderComplete.IsNullable = true;
                colvarIsOrderComplete.IsPrimaryKey = false;
                colvarIsOrderComplete.IsForeignKey = false;
                colvarIsOrderComplete.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsOrderComplete);
                
                TableSchema.TableColumn colvarIsOrderNorefund = new TableSchema.TableColumn(schema);
                colvarIsOrderNorefund.ColumnName = "is_order_norefund";
                colvarIsOrderNorefund.DataType = DbType.Boolean;
                colvarIsOrderNorefund.MaxLength = 0;
                colvarIsOrderNorefund.AutoIncrement = false;
                colvarIsOrderNorefund.IsNullable = true;
                colvarIsOrderNorefund.IsPrimaryKey = false;
                colvarIsOrderNorefund.IsForeignKey = false;
                colvarIsOrderNorefund.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsOrderNorefund);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = true;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
                colvarLastName.ColumnName = "last_name";
                colvarLastName.DataType = DbType.String;
                colvarLastName.MaxLength = 50;
                colvarLastName.AutoIncrement = false;
                colvarLastName.IsNullable = true;
                colvarLastName.IsPrimaryKey = false;
                colvarLastName.IsForeignKey = false;
                colvarLastName.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastName);
                
                TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
                colvarFirstName.ColumnName = "first_name";
                colvarFirstName.DataType = DbType.String;
                colvarFirstName.MaxLength = 50;
                colvarFirstName.AutoIncrement = false;
                colvarFirstName.IsNullable = true;
                colvarFirstName.IsPrimaryKey = false;
                colvarFirstName.IsForeignKey = false;
                colvarFirstName.IsReadOnly = false;
                
                schema.Columns.Add(colvarFirstName);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                TableSchema.TableColumn colvarCompanyAddress = new TableSchema.TableColumn(schema);
                colvarCompanyAddress.ColumnName = "company_address";
                colvarCompanyAddress.DataType = DbType.String;
                colvarCompanyAddress.MaxLength = 100;
                colvarCompanyAddress.AutoIncrement = false;
                colvarCompanyAddress.IsNullable = true;
                colvarCompanyAddress.IsPrimaryKey = false;
                colvarCompanyAddress.IsForeignKey = false;
                colvarCompanyAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAddress);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = true;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
                colvarOrderCount.ColumnName = "order_count";
                colvarOrderCount.DataType = DbType.Int32;
                colvarOrderCount.MaxLength = 0;
                colvarOrderCount.AutoIncrement = false;
                colvarOrderCount.IsNullable = true;
                colvarOrderCount.IsPrimaryKey = false;
                colvarOrderCount.IsForeignKey = false;
                colvarOrderCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCount);
                
                TableSchema.TableColumn colvarOrderTotal = new TableSchema.TableColumn(schema);
                colvarOrderTotal.ColumnName = "order_total";
                colvarOrderTotal.DataType = DbType.Int32;
                colvarOrderTotal.MaxLength = 0;
                colvarOrderTotal.AutoIncrement = false;
                colvarOrderTotal.IsNullable = true;
                colvarOrderTotal.IsPrimaryKey = false;
                colvarOrderTotal.IsForeignKey = false;
                colvarOrderTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTotal);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = true;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarBuildingGuid = new TableSchema.TableColumn(schema);
                colvarBuildingGuid.ColumnName = "building_GUID";
                colvarBuildingGuid.DataType = DbType.Guid;
                colvarBuildingGuid.MaxLength = 0;
                colvarBuildingGuid.AutoIncrement = false;
                colvarBuildingGuid.IsNullable = true;
                colvarBuildingGuid.IsPrimaryKey = false;
                colvarBuildingGuid.IsForeignKey = false;
                colvarBuildingGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingGuid);
                
                TableSchema.TableColumn colvarGender = new TableSchema.TableColumn(schema);
                colvarGender.ColumnName = "gender";
                colvarGender.DataType = DbType.Int32;
                colvarGender.MaxLength = 0;
                colvarGender.AutoIncrement = false;
                colvarGender.IsNullable = true;
                colvarGender.IsPrimaryKey = false;
                colvarGender.IsForeignKey = false;
                colvarGender.IsReadOnly = false;
                
                schema.Columns.Add(colvarGender);
                
                TableSchema.TableColumn colvarMemberCreateTime = new TableSchema.TableColumn(schema);
                colvarMemberCreateTime.ColumnName = "member_create_time";
                colvarMemberCreateTime.DataType = DbType.DateTime;
                colvarMemberCreateTime.MaxLength = 0;
                colvarMemberCreateTime.AutoIncrement = false;
                colvarMemberCreateTime.IsNullable = true;
                colvarMemberCreateTime.IsPrimaryKey = false;
                colvarMemberCreateTime.IsForeignKey = false;
                colvarMemberCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberCreateTime);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 100;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_order_member_order_show",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealOrderMemberOrderShow()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealOrderMemberOrderShow(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealOrderMemberOrderShow(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealOrderMemberOrderShow(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Pk")]
        [Bindable(true)]
        public int Pk 
	    {
		    get
		    {
			    return GetColumnValue<int>("pk");
		    }
            set 
		    {
			    SetColumnValue("pk", value);
            }
        }
	      
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("guid");
		    }
            set 
		    {
			    SetColumnValue("guid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("HiDealId")]
        [Bindable(true)]
        public int HiDealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("hi_deal_id");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_id", value);
            }
        }
	      
        [XmlAttribute("TotalAmount")]
        [Bindable(true)]
        public decimal TotalAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total_amount");
		    }
            set 
		    {
			    SetColumnValue("total_amount", value);
            }
        }
	      
        [XmlAttribute("Pcash")]
        [Bindable(true)]
        public decimal Pcash 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("pcash");
		    }
            set 
		    {
			    SetColumnValue("pcash", value);
            }
        }
	      
        [XmlAttribute("Scash")]
        [Bindable(true)]
        public decimal Scash 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("scash");
		    }
            set 
		    {
			    SetColumnValue("scash", value);
            }
        }
	      
        [XmlAttribute("Bcash")]
        [Bindable(true)]
        public decimal Bcash 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("bcash");
		    }
            set 
		    {
			    SetColumnValue("bcash", value);
            }
        }
	      
        [XmlAttribute("CreditCardAmt")]
        [Bindable(true)]
        public decimal CreditCardAmt 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("credit_card_amt");
		    }
            set 
		    {
			    SetColumnValue("credit_card_amt", value);
            }
        }
	      
        [XmlAttribute("DiscountAmount")]
        [Bindable(true)]
        public decimal DiscountAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("discount_amount");
		    }
            set 
		    {
			    SetColumnValue("discount_amount", value);
            }
        }
	      
        [XmlAttribute("AtmAmount")]
        [Bindable(true)]
        public decimal AtmAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("atm_amount");
		    }
            set 
		    {
			    SetColumnValue("atm_amount", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_name");
		    }
            set 
		    {
			    SetColumnValue("product_name", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int? DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("PaymentType")]
        [Bindable(true)]
        public int? PaymentType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("payment_type");
		    }
            set 
		    {
			    SetColumnValue("payment_type", value);
            }
        }
	      
        [XmlAttribute("OrderShowStatus")]
        [Bindable(true)]
        public int? OrderShowStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_show_status");
		    }
            set 
		    {
			    SetColumnValue("order_show_status", value);
            }
        }
	      
        [XmlAttribute("RemainCount")]
        [Bindable(true)]
        public int? RemainCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("remain_count");
		    }
            set 
		    {
			    SetColumnValue("remain_count", value);
            }
        }
	      
        [XmlAttribute("UseStartTime")]
        [Bindable(true)]
        public DateTime? UseStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_start_time");
		    }
            set 
		    {
			    SetColumnValue("use_start_time", value);
            }
        }
	      
        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public DateTime? UseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_end_time");
		    }
            set 
		    {
			    SetColumnValue("use_end_time", value);
            }
        }
	      
        [XmlAttribute("OrderEndTime")]
        [Bindable(true)]
        public DateTime? OrderEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("order_end_time");
		    }
            set 
		    {
			    SetColumnValue("order_end_time", value);
            }
        }
	      
        [XmlAttribute("IsOrderComplete")]
        [Bindable(true)]
        public bool? IsOrderComplete 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_order_complete");
		    }
            set 
		    {
			    SetColumnValue("is_order_complete", value);
            }
        }
	      
        [XmlAttribute("IsOrderNorefund")]
        [Bindable(true)]
        public bool? IsOrderNorefund 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_order_norefund");
		    }
            set 
		    {
			    SetColumnValue("is_order_norefund", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("LastName")]
        [Bindable(true)]
        public string LastName 
	    {
		    get
		    {
			    return GetColumnValue<string>("last_name");
		    }
            set 
		    {
			    SetColumnValue("last_name", value);
            }
        }
	      
        [XmlAttribute("FirstName")]
        [Bindable(true)]
        public string FirstName 
	    {
		    get
		    {
			    return GetColumnValue<string>("first_name");
		    }
            set 
		    {
			    SetColumnValue("first_name", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	      
        [XmlAttribute("CompanyAddress")]
        [Bindable(true)]
        public string CompanyAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_address");
		    }
            set 
		    {
			    SetColumnValue("company_address", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int? UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("OrderCount")]
        [Bindable(true)]
        public int? OrderCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_count");
		    }
            set 
		    {
			    SetColumnValue("order_count", value);
            }
        }
	      
        [XmlAttribute("OrderTotal")]
        [Bindable(true)]
        public int? OrderTotal 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_total");
		    }
            set 
		    {
			    SetColumnValue("order_total", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int? ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("BuildingGuid")]
        [Bindable(true)]
        public Guid? BuildingGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("building_GUID");
		    }
            set 
		    {
			    SetColumnValue("building_GUID", value);
            }
        }
	      
        [XmlAttribute("Gender")]
        [Bindable(true)]
        public int? Gender 
	    {
		    get
		    {
			    return GetColumnValue<int?>("gender");
		    }
            set 
		    {
			    SetColumnValue("gender", value);
            }
        }
	      
        [XmlAttribute("MemberCreateTime")]
        [Bindable(true)]
        public DateTime? MemberCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("member_create_time");
		    }
            set 
		    {
			    SetColumnValue("member_create_time", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int? Status 
	    {
		    get
		    {
			    return GetColumnValue<int?>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Pk = @"pk";
            
            public static string Guid = @"guid";
            
            public static string OrderId = @"order_id";
            
            public static string HiDealId = @"hi_deal_id";
            
            public static string TotalAmount = @"total_amount";
            
            public static string Pcash = @"pcash";
            
            public static string Scash = @"scash";
            
            public static string Bcash = @"bcash";
            
            public static string CreditCardAmt = @"credit_card_amt";
            
            public static string DiscountAmount = @"discount_amount";
            
            public static string AtmAmount = @"atm_amount";
            
            public static string OrderStatus = @"order_status";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ProductName = @"product_name";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string PaymentType = @"payment_type";
            
            public static string OrderShowStatus = @"order_show_status";
            
            public static string RemainCount = @"remain_count";
            
            public static string UseStartTime = @"use_start_time";
            
            public static string UseEndTime = @"use_end_time";
            
            public static string OrderEndTime = @"order_end_time";
            
            public static string IsOrderComplete = @"is_order_complete";
            
            public static string IsOrderNorefund = @"is_order_norefund";
            
            public static string UserName = @"user_name";
            
            public static string LastName = @"last_name";
            
            public static string FirstName = @"first_name";
            
            public static string Mobile = @"mobile";
            
            public static string CompanyAddress = @"company_address";
            
            public static string UniqueId = @"unique_id";
            
            public static string OrderCount = @"order_count";
            
            public static string OrderTotal = @"order_total";
            
            public static string ProductId = @"product_id";
            
            public static string BuildingGuid = @"building_GUID";
            
            public static string Gender = @"gender";
            
            public static string MemberCreateTime = @"member_create_time";
            
            public static string Status = @"status";
            
            public static string MemberName = @"member_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

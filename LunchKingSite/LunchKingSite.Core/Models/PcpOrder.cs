using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpOrder class.
	/// </summary>
    [Serializable]
	public partial class PcpOrderCollection : RepositoryList<PcpOrder, PcpOrderCollection>
	{	   
		public PcpOrderCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpOrderCollection</returns>
		public PcpOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_order table.
	/// </summary>
	[Serializable]
	public partial class PcpOrder : RepositoryRecord<PcpOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpOrder()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarPk = new TableSchema.TableColumn(schema);
				colvarPk.ColumnName = "pk";
				colvarPk.DataType = DbType.Int32;
				colvarPk.MaxLength = 0;
				colvarPk.AutoIncrement = true;
				colvarPk.IsNullable = false;
				colvarPk.IsPrimaryKey = true;
				colvarPk.IsForeignKey = false;
				colvarPk.IsReadOnly = false;
				colvarPk.DefaultSetting = @"";
				colvarPk.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPk);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				
						colvarGuid.DefaultSetting = @"(newid())";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarCardId = new TableSchema.TableColumn(schema);
				colvarCardId.ColumnName = "card_id";
				colvarCardId.DataType = DbType.Int32;
				colvarCardId.MaxLength = 0;
				colvarCardId.AutoIncrement = false;
				colvarCardId.IsNullable = true;
				colvarCardId.IsPrimaryKey = false;
				colvarCardId.IsForeignKey = false;
				colvarCardId.IsReadOnly = false;
				colvarCardId.DefaultSetting = @"";
				colvarCardId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardId);
				
				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Currency;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = true;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);
				
				TableSchema.TableColumn colvarDiscountCodeId = new TableSchema.TableColumn(schema);
				colvarDiscountCodeId.ColumnName = "discount_code_id";
				colvarDiscountCodeId.DataType = DbType.Int32;
				colvarDiscountCodeId.MaxLength = 0;
				colvarDiscountCodeId.AutoIncrement = false;
				colvarDiscountCodeId.IsNullable = true;
				colvarDiscountCodeId.IsPrimaryKey = false;
				colvarDiscountCodeId.IsForeignKey = false;
				colvarDiscountCodeId.IsReadOnly = false;
				colvarDiscountCodeId.DefaultSetting = @"";
				colvarDiscountCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountCodeId);
				
				TableSchema.TableColumn colvarBonusPoint = new TableSchema.TableColumn(schema);
				colvarBonusPoint.ColumnName = "bonus_point";
				colvarBonusPoint.DataType = DbType.Currency;
				colvarBonusPoint.MaxLength = 0;
				colvarBonusPoint.AutoIncrement = false;
				colvarBonusPoint.IsNullable = true;
				colvarBonusPoint.IsPrimaryKey = false;
				colvarBonusPoint.IsForeignKey = false;
				colvarBonusPoint.IsReadOnly = false;
				colvarBonusPoint.DefaultSetting = @"";
				colvarBonusPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBonusPoint);
				
				TableSchema.TableColumn colvarOrderTime = new TableSchema.TableColumn(schema);
				colvarOrderTime.ColumnName = "order_time";
				colvarOrderTime.DataType = DbType.DateTime;
				colvarOrderTime.MaxLength = 0;
				colvarOrderTime.AutoIncrement = false;
				colvarOrderTime.IsNullable = false;
				colvarOrderTime.IsPrimaryKey = false;
				colvarOrderTime.IsForeignKey = false;
				colvarOrderTime.IsReadOnly = false;
				colvarOrderTime.DefaultSetting = @"";
				colvarOrderTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderTime);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarStatusDescription = new TableSchema.TableColumn(schema);
				colvarStatusDescription.ColumnName = "status_description";
				colvarStatusDescription.DataType = DbType.String;
				colvarStatusDescription.MaxLength = -1;
				colvarStatusDescription.AutoIncrement = false;
				colvarStatusDescription.IsNullable = true;
				colvarStatusDescription.IsPrimaryKey = false;
				colvarStatusDescription.IsForeignKey = false;
				colvarStatusDescription.IsReadOnly = false;
				colvarStatusDescription.DefaultSetting = @"";
				colvarStatusDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatusDescription);
				
				TableSchema.TableColumn colvarIdentityCodeId = new TableSchema.TableColumn(schema);
				colvarIdentityCodeId.ColumnName = "identity_code_id";
				colvarIdentityCodeId.DataType = DbType.Int64;
				colvarIdentityCodeId.MaxLength = 0;
				colvarIdentityCodeId.AutoIncrement = false;
				colvarIdentityCodeId.IsNullable = true;
				colvarIdentityCodeId.IsPrimaryKey = false;
				colvarIdentityCodeId.IsForeignKey = false;
				colvarIdentityCodeId.IsReadOnly = false;
				colvarIdentityCodeId.DefaultSetting = @"";
				colvarIdentityCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdentityCodeId);
				
				TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
				colvarDiscountAmount.ColumnName = "discount_amount";
				colvarDiscountAmount.DataType = DbType.Currency;
				colvarDiscountAmount.MaxLength = 0;
				colvarDiscountAmount.AutoIncrement = false;
				colvarDiscountAmount.IsNullable = true;
				colvarDiscountAmount.IsPrimaryKey = false;
				colvarDiscountAmount.IsForeignKey = false;
				colvarDiscountAmount.IsReadOnly = false;
				colvarDiscountAmount.DefaultSetting = @"";
				colvarDiscountAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountAmount);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = false;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				
						colvarStoreGuid.DefaultSetting = @"(CONVERT([uniqueidentifier],CONVERT([binary],(0),0),0))";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarOrderType = new TableSchema.TableColumn(schema);
				colvarOrderType.ColumnName = "order_type";
				colvarOrderType.DataType = DbType.Int32;
				colvarOrderType.MaxLength = 0;
				colvarOrderType.AutoIncrement = false;
				colvarOrderType.IsNullable = true;
				colvarOrderType.IsPrimaryKey = false;
				colvarOrderType.IsForeignKey = false;
				colvarOrderType.IsReadOnly = false;
				colvarOrderType.DefaultSetting = @"";
				colvarOrderType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_order",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Pk")]
		[Bindable(true)]
		public int Pk 
		{
			get { return GetColumnValue<int>(Columns.Pk); }
			set { SetColumnValue(Columns.Pk, value); }
		}
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("CardId")]
		[Bindable(true)]
		public int? CardId 
		{
			get { return GetColumnValue<int?>(Columns.CardId); }
			set { SetColumnValue(Columns.CardId, value); }
		}
		  
		[XmlAttribute("Amount")]
		[Bindable(true)]
		public decimal? Amount 
		{
			get { return GetColumnValue<decimal?>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}
		  
		[XmlAttribute("DiscountCodeId")]
		[Bindable(true)]
		public int? DiscountCodeId 
		{
			get { return GetColumnValue<int?>(Columns.DiscountCodeId); }
			set { SetColumnValue(Columns.DiscountCodeId, value); }
		}
		  
		[XmlAttribute("BonusPoint")]
		[Bindable(true)]
		public decimal? BonusPoint 
		{
			get { return GetColumnValue<decimal?>(Columns.BonusPoint); }
			set { SetColumnValue(Columns.BonusPoint, value); }
		}
		  
		[XmlAttribute("OrderTime")]
		[Bindable(true)]
		public DateTime OrderTime 
		{
			get { return GetColumnValue<DateTime>(Columns.OrderTime); }
			set { SetColumnValue(Columns.OrderTime, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("StatusDescription")]
		[Bindable(true)]
		public string StatusDescription 
		{
			get { return GetColumnValue<string>(Columns.StatusDescription); }
			set { SetColumnValue(Columns.StatusDescription, value); }
		}
		  
		[XmlAttribute("IdentityCodeId")]
		[Bindable(true)]
		public long? IdentityCodeId 
		{
			get { return GetColumnValue<long?>(Columns.IdentityCodeId); }
			set { SetColumnValue(Columns.IdentityCodeId, value); }
		}
		  
		[XmlAttribute("DiscountAmount")]
		[Bindable(true)]
		public decimal? DiscountAmount 
		{
			get { return GetColumnValue<decimal?>(Columns.DiscountAmount); }
			set { SetColumnValue(Columns.DiscountAmount, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid StoreGuid 
		{
			get { return GetColumnValue<Guid>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int? CreateId 
		{
			get { return GetColumnValue<int?>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("OrderType")]
		[Bindable(true)]
		public int? OrderType 
		{
			get { return GetColumnValue<int?>(Columns.OrderType); }
			set { SetColumnValue(Columns.OrderType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn PkColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CardIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountCodeIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BonusPointColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusDescriptionColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IdentityCodeIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountAmountColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderTypeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Pk = @"pk";
			 public static string Guid = @"guid";
			 public static string UserId = @"user_id";
			 public static string CardId = @"card_id";
			 public static string Amount = @"amount";
			 public static string DiscountCodeId = @"discount_code_id";
			 public static string BonusPoint = @"bonus_point";
			 public static string OrderTime = @"order_time";
			 public static string Status = @"status";
			 public static string StatusDescription = @"status_description";
			 public static string IdentityCodeId = @"identity_code_id";
			 public static string DiscountAmount = @"discount_amount";
			 public static string StoreGuid = @"store_guid";
			 public static string CreateId = @"create_id";
			 public static string OrderType = @"order_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

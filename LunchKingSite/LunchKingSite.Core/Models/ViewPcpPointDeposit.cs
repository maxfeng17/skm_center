using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpPointDeposit class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpPointDepositCollection : ReadOnlyList<ViewPcpPointDeposit, ViewPcpPointDepositCollection>
    {        
        public ViewPcpPointDepositCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_point_deposit view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpPointDeposit : ReadOnlyRecord<ViewPcpPointDeposit>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_point_deposit", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarPoint = new TableSchema.TableColumn(schema);
                colvarPoint.ColumnName = "point";
                colvarPoint.DataType = DbType.Int32;
                colvarPoint.MaxLength = 0;
                colvarPoint.AutoIncrement = false;
                colvarPoint.IsNullable = false;
                colvarPoint.IsPrimaryKey = false;
                colvarPoint.IsForeignKey = false;
                colvarPoint.IsReadOnly = false;
                
                schema.Columns.Add(colvarPoint);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Byte;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = -1;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarAssignmentId = new TableSchema.TableColumn(schema);
                colvarAssignmentId.ColumnName = "assignment_id";
                colvarAssignmentId.DataType = DbType.Int32;
                colvarAssignmentId.MaxLength = 0;
                colvarAssignmentId.AutoIncrement = false;
                colvarAssignmentId.IsNullable = true;
                colvarAssignmentId.IsPrimaryKey = false;
                colvarAssignmentId.IsForeignKey = false;
                colvarAssignmentId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAssignmentId);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = false;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarExpireTime = new TableSchema.TableColumn(schema);
                colvarExpireTime.ColumnName = "expire_time";
                colvarExpireTime.DataType = DbType.DateTime;
                colvarExpireTime.MaxLength = 0;
                colvarExpireTime.AutoIncrement = false;
                colvarExpireTime.IsNullable = false;
                colvarExpireTime.IsPrimaryKey = false;
                colvarExpireTime.IsForeignKey = false;
                colvarExpireTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarExpireTime);
                
                TableSchema.TableColumn colvarOriginalExpireTime = new TableSchema.TableColumn(schema);
                colvarOriginalExpireTime.ColumnName = "original_expire_time";
                colvarOriginalExpireTime.DataType = DbType.DateTime;
                colvarOriginalExpireTime.MaxLength = 0;
                colvarOriginalExpireTime.AutoIncrement = false;
                colvarOriginalExpireTime.IsNullable = false;
                colvarOriginalExpireTime.IsPrimaryKey = false;
                colvarOriginalExpireTime.IsForeignKey = false;
                colvarOriginalExpireTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOriginalExpireTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.Int32;
                colvarModifyId.MaxLength = 0;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarWithdrawalPoint = new TableSchema.TableColumn(schema);
                colvarWithdrawalPoint.ColumnName = "withdrawal_point";
                colvarWithdrawalPoint.DataType = DbType.Int32;
                colvarWithdrawalPoint.MaxLength = 0;
                colvarWithdrawalPoint.AutoIncrement = false;
                colvarWithdrawalPoint.IsNullable = true;
                colvarWithdrawalPoint.IsPrimaryKey = false;
                colvarWithdrawalPoint.IsForeignKey = false;
                colvarWithdrawalPoint.IsReadOnly = false;
                
                schema.Columns.Add(colvarWithdrawalPoint);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_point_deposit",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpPointDeposit()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpPointDeposit(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpPointDeposit(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpPointDeposit(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("Point")]
        [Bindable(true)]
        public int Point 
	    {
		    get
		    {
			    return GetColumnValue<int>("point");
		    }
            set 
		    {
			    SetColumnValue("point", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public byte Type 
	    {
		    get
		    {
			    return GetColumnValue<byte>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message 
	    {
		    get
		    {
			    return GetColumnValue<string>("message");
		    }
            set 
		    {
			    SetColumnValue("message", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("AssignmentId")]
        [Bindable(true)]
        public int? AssignmentId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("assignment_id");
		    }
            set 
		    {
			    SetColumnValue("assignment_id", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId 
	    {
		    get
		    {
			    return GetColumnValue<int>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("ExpireTime")]
        [Bindable(true)]
        public DateTime ExpireTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("expire_time");
		    }
            set 
		    {
			    SetColumnValue("expire_time", value);
            }
        }
	      
        [XmlAttribute("OriginalExpireTime")]
        [Bindable(true)]
        public DateTime OriginalExpireTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("original_expire_time");
		    }
            set 
		    {
			    SetColumnValue("original_expire_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public int? ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("WithdrawalPoint")]
        [Bindable(true)]
        public int? WithdrawalPoint 
	    {
		    get
		    {
			    return GetColumnValue<int?>("withdrawal_point");
		    }
            set 
		    {
			    SetColumnValue("withdrawal_point", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string Point = @"point";
            
            public static string Type = @"type";
            
            public static string Message = @"message";
            
            public static string UserId = @"user_id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string AssignmentId = @"assignment_id";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string StartTime = @"start_time";
            
            public static string ExpireTime = @"expire_time";
            
            public static string OriginalExpireTime = @"original_expire_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string ModifyTime = @"modify_time";
            
            public static string WithdrawalPoint = @"withdrawal_point";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

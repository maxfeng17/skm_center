using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the PponOption class.
    /// </summary>
    [Serializable]
    public partial class PponOptionCollection : RepositoryList<PponOption, PponOptionCollection>
    {
        public PponOptionCollection() {}

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PponOptionCollection</returns>
        public PponOptionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PponOption o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the ppon_option table.
    /// </summary>
    [Serializable]
    public partial class PponOption : RepositoryRecord<PponOption>, IRecordBase
    {
        #region .ctors and Default Settings

        public PponOption()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public PponOption(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("ppon_option", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                colvarDealId.DefaultSetting = @"";
                colvarDealId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDealId);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarCatgName = new TableSchema.TableColumn(schema);
                colvarCatgName.ColumnName = "catg_name";
                colvarCatgName.DataType = DbType.String;
                colvarCatgName.MaxLength = 50;
                colvarCatgName.AutoIncrement = false;
                colvarCatgName.IsNullable = false;
                colvarCatgName.IsPrimaryKey = false;
                colvarCatgName.IsForeignKey = false;
                colvarCatgName.IsReadOnly = false;
                colvarCatgName.DefaultSetting = @"";
                colvarCatgName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCatgName);

                TableSchema.TableColumn colvarCatgSeq = new TableSchema.TableColumn(schema);
                colvarCatgSeq.ColumnName = "catg_seq";
                colvarCatgSeq.DataType = DbType.Int32;
                colvarCatgSeq.MaxLength = 0;
                colvarCatgSeq.AutoIncrement = false;
                colvarCatgSeq.IsNullable = false;
                colvarCatgSeq.IsPrimaryKey = false;
                colvarCatgSeq.IsForeignKey = false;
                colvarCatgSeq.IsReadOnly = false;
                colvarCatgSeq.DefaultSetting = @"";
                colvarCatgSeq.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCatgSeq);

                TableSchema.TableColumn colvarOptionName = new TableSchema.TableColumn(schema);
                colvarOptionName.ColumnName = "option_name";
                colvarOptionName.DataType = DbType.String;
                colvarOptionName.MaxLength = 50;
                colvarOptionName.AutoIncrement = false;
                colvarOptionName.IsNullable = false;
                colvarOptionName.IsPrimaryKey = false;
                colvarOptionName.IsForeignKey = false;
                colvarOptionName.IsReadOnly = false;
                colvarOptionName.DefaultSetting = @"";
                colvarOptionName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOptionName);

                TableSchema.TableColumn colvarOptionSeq = new TableSchema.TableColumn(schema);
                colvarOptionSeq.ColumnName = "option_seq";
                colvarOptionSeq.DataType = DbType.Int32;
                colvarOptionSeq.MaxLength = 0;
                colvarOptionSeq.AutoIncrement = false;
                colvarOptionSeq.IsNullable = false;
                colvarOptionSeq.IsPrimaryKey = false;
                colvarOptionSeq.IsForeignKey = false;
                colvarOptionSeq.IsReadOnly = false;
                colvarOptionSeq.DefaultSetting = @"";
                colvarOptionSeq.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOptionSeq);

                TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
                colvarQuantity.ColumnName = "quantity";
                colvarQuantity.DataType = DbType.Int32;
                colvarQuantity.MaxLength = 0;
                colvarQuantity.AutoIncrement = false;
                colvarQuantity.IsNullable = true;
                colvarQuantity.IsPrimaryKey = false;
                colvarQuantity.IsForeignKey = false;
                colvarQuantity.IsReadOnly = false;
                colvarQuantity.DefaultSetting = @"";
                colvarQuantity.ForeignKeyTableName = "";
                schema.Columns.Add(colvarQuantity);

                TableSchema.TableColumn colvarIsDisabled = new TableSchema.TableColumn(schema);
                colvarIsDisabled.ColumnName = "is_disabled";
                colvarIsDisabled.DataType = DbType.Boolean;
                colvarIsDisabled.MaxLength = 0;
                colvarIsDisabled.AutoIncrement = false;
                colvarIsDisabled.IsNullable = false;
                colvarIsDisabled.IsPrimaryKey = false;
                colvarIsDisabled.IsForeignKey = false;
                colvarIsDisabled.IsReadOnly = false;

                colvarIsDisabled.DefaultSetting = @"((0))";
                colvarIsDisabled.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsDisabled);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 256;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarItemNo = new TableSchema.TableColumn(schema);
                colvarItemNo.ColumnName = "item_no";
                colvarItemNo.DataType = DbType.String;
                colvarItemNo.MaxLength = 100;
                colvarItemNo.AutoIncrement = false;
                colvarItemNo.IsNullable = true;
                colvarItemNo.IsPrimaryKey = false;
                colvarItemNo.IsForeignKey = false;
                colvarItemNo.IsReadOnly = false;
                colvarItemNo.DefaultSetting = @"";
                colvarItemNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemNo);

                TableSchema.TableColumn colvarInfoGuid = new TableSchema.TableColumn(schema);
                colvarInfoGuid.ColumnName = "info_guid";
                colvarInfoGuid.DataType = DbType.Guid;
                colvarInfoGuid.MaxLength = 0;
                colvarInfoGuid.AutoIncrement = false;
                colvarInfoGuid.IsNullable = true;
                colvarInfoGuid.IsPrimaryKey = false;
                colvarInfoGuid.IsForeignKey = false;
                colvarInfoGuid.IsReadOnly = false;
                colvarInfoGuid.DefaultSetting = @"";
                colvarInfoGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInfoGuid);

                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_guid";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = true;
                colvarItemGuid.IsPrimaryKey = false;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;
                colvarItemGuid.DefaultSetting = @"";
                colvarItemGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemGuid);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("ppon_option",schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId
        {
            get { return GetColumnValue<int>(Columns.DealId); }
            set { SetColumnValue(Columns.DealId, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid
        {
            get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        [XmlAttribute("CatgName")]
        [Bindable(true)]
        public string CatgName
        {
            get { return GetColumnValue<string>(Columns.CatgName); }
            set { SetColumnValue(Columns.CatgName, value); }
        }

        [XmlAttribute("CatgSeq")]
        [Bindable(true)]
        public int CatgSeq
        {
            get { return GetColumnValue<int>(Columns.CatgSeq); }
            set { SetColumnValue(Columns.CatgSeq, value); }
        }

        [XmlAttribute("OptionName")]
        [Bindable(true)]
        public string OptionName
        {
            get { return GetColumnValue<string>(Columns.OptionName); }
            set { SetColumnValue(Columns.OptionName, value); }
        }

        [XmlAttribute("OptionSeq")]
        [Bindable(true)]
        public int OptionSeq
        {
            get { return GetColumnValue<int>(Columns.OptionSeq); }
            set { SetColumnValue(Columns.OptionSeq, value); }
        }

        [XmlAttribute("Quantity")]
        [Bindable(true)]
        public int? Quantity
        {
            get { return GetColumnValue<int?>(Columns.Quantity); }
            set { SetColumnValue(Columns.Quantity, value); }
        }

        [XmlAttribute("IsDisabled")]
        [Bindable(true)]
        public bool IsDisabled
        {
            get { return GetColumnValue<bool>(Columns.IsDisabled); }
            set { SetColumnValue(Columns.IsDisabled, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ItemNo")]
        [Bindable(true)]
        public string ItemNo
        {
            get { return GetColumnValue<string>(Columns.ItemNo); }
            set { SetColumnValue(Columns.ItemNo, value); }
        }

        [XmlAttribute("InfoGuid")]
        [Bindable(true)]
        public Guid? InfoGuid
        {
            get { return GetColumnValue<Guid?>(Columns.InfoGuid); }
            set { SetColumnValue(Columns.InfoGuid, value); }
        }

        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid? ItemGuid
        {
            get { return GetColumnValue<Guid?>(Columns.ItemGuid); }
            set { SetColumnValue(Columns.ItemGuid, value); }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn DealIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CatgNameColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CatgSeqColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn OptionNameColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn OptionSeqColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn QuantityColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn IsDisabledColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn ItemNoColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn InfoGuidColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn ItemGuidColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[16]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string DealId = @"deal_id";
            public static string BusinessHourGuid = @"business_hour_guid";
            public static string CatgName = @"catg_name";
            public static string CatgSeq = @"catg_seq";
            public static string OptionName = @"option_name";
            public static string OptionSeq = @"option_seq";
            public static string Quantity = @"quantity";
            public static string IsDisabled = @"is_disabled";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string ItemNo = @"item_no";
            public static string InfoGuid = @"info_guid";
            public static string ItemGuid = @"item_guid";
            public static string Guid = @"guid";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

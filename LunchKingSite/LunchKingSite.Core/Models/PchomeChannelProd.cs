using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PchomeChannelProd class.
	/// </summary>
    [Serializable]
	public partial class PchomeChannelProdCollection : RepositoryList<PchomeChannelProd, PchomeChannelProdCollection>
	{	   
		public PchomeChannelProdCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PchomeChannelProdCollection</returns>
		public PchomeChannelProdCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PchomeChannelProd o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pchome_channel_prod table.
	/// </summary>
	[Serializable]
	public partial class PchomeChannelProd : RepositoryRecord<PchomeChannelProd>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PchomeChannelProd()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PchomeChannelProd(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pchome_channel_prod", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = true;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarProdId = new TableSchema.TableColumn(schema);
				colvarProdId.ColumnName = "prod_id";
				colvarProdId.DataType = DbType.AnsiString;
				colvarProdId.MaxLength = 20;
				colvarProdId.AutoIncrement = false;
				colvarProdId.IsNullable = false;
				colvarProdId.IsPrimaryKey = false;
				colvarProdId.IsForeignKey = false;
				colvarProdId.IsReadOnly = false;
				colvarProdId.DefaultSetting = @"";
				colvarProdId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProdId);
				
				TableSchema.TableColumn colvarProdGroupId = new TableSchema.TableColumn(schema);
				colvarProdGroupId.ColumnName = "prod_group_id";
				colvarProdGroupId.DataType = DbType.AnsiString;
				colvarProdGroupId.MaxLength = 16;
				colvarProdGroupId.AutoIncrement = false;
				colvarProdGroupId.IsNullable = false;
				colvarProdGroupId.IsPrimaryKey = false;
				colvarProdGroupId.IsForeignKey = false;
				colvarProdGroupId.IsReadOnly = false;
				colvarProdGroupId.DefaultSetting = @"";
				colvarProdGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProdGroupId);
				
				TableSchema.TableColumn colvarIsEticket = new TableSchema.TableColumn(schema);
				colvarIsEticket.ColumnName = "is_eticket";
				colvarIsEticket.DataType = DbType.Boolean;
				colvarIsEticket.MaxLength = 0;
				colvarIsEticket.AutoIncrement = false;
				colvarIsEticket.IsNullable = false;
				colvarIsEticket.IsPrimaryKey = false;
				colvarIsEticket.IsForeignKey = false;
				colvarIsEticket.IsReadOnly = false;
				colvarIsEticket.DefaultSetting = @"";
				colvarIsEticket.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsEticket);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarIsShelf = new TableSchema.TableColumn(schema);
				colvarIsShelf.ColumnName = "is_shelf";
				colvarIsShelf.DataType = DbType.Int32;
				colvarIsShelf.MaxLength = 0;
				colvarIsShelf.AutoIncrement = false;
				colvarIsShelf.IsNullable = false;
				colvarIsShelf.IsPrimaryKey = false;
				colvarIsShelf.IsForeignKey = false;
				colvarIsShelf.IsReadOnly = false;
				colvarIsShelf.DefaultSetting = @"";
				colvarIsShelf.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsShelf);
				
				TableSchema.TableColumn colvarIsVerify = new TableSchema.TableColumn(schema);
				colvarIsVerify.ColumnName = "is_verify";
				colvarIsVerify.DataType = DbType.Boolean;
				colvarIsVerify.MaxLength = 0;
				colvarIsVerify.AutoIncrement = false;
				colvarIsVerify.IsNullable = false;
				colvarIsVerify.IsPrimaryKey = false;
				colvarIsVerify.IsForeignKey = false;
				colvarIsVerify.IsReadOnly = false;
				
						colvarIsVerify.DefaultSetting = @"((0))";
				colvarIsVerify.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsVerify);
				
				TableSchema.TableColumn colvarIsArchive = new TableSchema.TableColumn(schema);
				colvarIsArchive.ColumnName = "is_archive";
				colvarIsArchive.DataType = DbType.Int32;
				colvarIsArchive.MaxLength = 0;
				colvarIsArchive.AutoIncrement = false;
				colvarIsArchive.IsNullable = false;
				colvarIsArchive.IsPrimaryKey = false;
				colvarIsArchive.IsForeignKey = false;
				colvarIsArchive.IsReadOnly = false;
				
						colvarIsArchive.DefaultSetting = @"((0))";
				colvarIsArchive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsArchive);
				
				TableSchema.TableColumn colvarVerifyTime = new TableSchema.TableColumn(schema);
				colvarVerifyTime.ColumnName = "verify_time";
				colvarVerifyTime.DataType = DbType.DateTime;
				colvarVerifyTime.MaxLength = 0;
				colvarVerifyTime.AutoIncrement = false;
				colvarVerifyTime.IsNullable = true;
				colvarVerifyTime.IsPrimaryKey = false;
				colvarVerifyTime.IsForeignKey = false;
				colvarVerifyTime.IsReadOnly = false;
				colvarVerifyTime.DefaultSetting = @"";
				colvarVerifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pchome_channel_prod",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("ProdId")]
		[Bindable(true)]
		public string ProdId 
		{
			get { return GetColumnValue<string>(Columns.ProdId); }
			set { SetColumnValue(Columns.ProdId, value); }
		}
		  
		[XmlAttribute("ProdGroupId")]
		[Bindable(true)]
		public string ProdGroupId 
		{
			get { return GetColumnValue<string>(Columns.ProdGroupId); }
			set { SetColumnValue(Columns.ProdGroupId, value); }
		}
		  
		[XmlAttribute("IsEticket")]
		[Bindable(true)]
		public bool IsEticket 
		{
			get { return GetColumnValue<bool>(Columns.IsEticket); }
			set { SetColumnValue(Columns.IsEticket, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("IsShelf")]
		[Bindable(true)]
		public int IsShelf 
		{
			get { return GetColumnValue<int>(Columns.IsShelf); }
			set { SetColumnValue(Columns.IsShelf, value); }
		}
		  
		[XmlAttribute("IsVerify")]
		[Bindable(true)]
		public bool IsVerify 
		{
			get { return GetColumnValue<bool>(Columns.IsVerify); }
			set { SetColumnValue(Columns.IsVerify, value); }
		}
		  
		[XmlAttribute("IsArchive")]
		[Bindable(true)]
		public int IsArchive 
		{
			get { return GetColumnValue<int>(Columns.IsArchive); }
			set { SetColumnValue(Columns.IsArchive, value); }
		}
		  
		[XmlAttribute("VerifyTime")]
		[Bindable(true)]
		public DateTime? VerifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.VerifyTime); }
			set { SetColumnValue(Columns.VerifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ProdIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ProdGroupIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IsEticketColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IsShelfColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn IsVerifyColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IsArchiveColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn VerifyTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Bid = @"bid";
			 public static string ProdId = @"prod_id";
			 public static string ProdGroupId = @"prod_group_id";
			 public static string IsEticket = @"is_eticket";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string IsShelf = @"is_shelf";
			 public static string IsVerify = @"is_verify";
			 public static string IsArchive = @"is_archive";
			 public static string VerifyTime = @"verify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

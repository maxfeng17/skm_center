using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewCtAtmRefund class.
    /// </summary>
    [Serializable]
    public partial class ViewCtAtmRefundCollection : ReadOnlyList<ViewCtAtmRefund, ViewCtAtmRefundCollection>
    {
        public ViewCtAtmRefundCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ct_atm_refund view.
    /// </summary>
    [Serializable]
    public partial class ViewCtAtmRefund : ReadOnlyRecord<ViewCtAtmRefund>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ct_atm_refund", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarSi = new TableSchema.TableColumn(schema);
                colvarSi.ColumnName = "si";
                colvarSi.DataType = DbType.Int32;
                colvarSi.MaxLength = 0;
                colvarSi.AutoIncrement = false;
                colvarSi.IsNullable = false;
                colvarSi.IsPrimaryKey = false;
                colvarSi.IsForeignKey = false;
                colvarSi.IsReadOnly = false;

                schema.Columns.Add(colvarSi);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;

                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarPaymentId = new TableSchema.TableColumn(schema);
                colvarPaymentId.ColumnName = "payment_id";
                colvarPaymentId.DataType = DbType.AnsiString;
                colvarPaymentId.MaxLength = 50;
                colvarPaymentId.AutoIncrement = false;
                colvarPaymentId.IsNullable = false;
                colvarPaymentId.IsPrimaryKey = false;
                colvarPaymentId.IsForeignKey = false;
                colvarPaymentId.IsReadOnly = false;

                schema.Columns.Add(colvarPaymentId);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Int32;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;

                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
                colvarAccountName.ColumnName = "account_name";
                colvarAccountName.DataType = DbType.String;
                colvarAccountName.MaxLength = 50;
                colvarAccountName.AutoIncrement = false;
                colvarAccountName.IsNullable = false;
                colvarAccountName.IsPrimaryKey = false;
                colvarAccountName.IsForeignKey = false;
                colvarAccountName.IsReadOnly = false;

                schema.Columns.Add(colvarAccountName);

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "ID";
                colvarId.DataType = DbType.AnsiString;
                colvarId.MaxLength = 30;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = true;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarBankName = new TableSchema.TableColumn(schema);
                colvarBankName.ColumnName = "bank_name";
                colvarBankName.DataType = DbType.String;
                colvarBankName.MaxLength = 50;
                colvarBankName.AutoIncrement = false;
                colvarBankName.IsNullable = false;
                colvarBankName.IsPrimaryKey = false;
                colvarBankName.IsForeignKey = false;
                colvarBankName.IsReadOnly = false;

                schema.Columns.Add(colvarBankName);

                TableSchema.TableColumn colvarBranchName = new TableSchema.TableColumn(schema);
                colvarBranchName.ColumnName = "branch_name";
                colvarBranchName.DataType = DbType.String;
                colvarBranchName.MaxLength = 50;
                colvarBranchName.AutoIncrement = false;
                colvarBranchName.IsNullable = true;
                colvarBranchName.IsPrimaryKey = false;
                colvarBranchName.IsForeignKey = false;
                colvarBranchName.IsReadOnly = false;

                schema.Columns.Add(colvarBranchName);

                TableSchema.TableColumn colvarBankCode = new TableSchema.TableColumn(schema);
                colvarBankCode.ColumnName = "bank_code";
                colvarBankCode.DataType = DbType.AnsiString;
                colvarBankCode.MaxLength = 10;
                colvarBankCode.AutoIncrement = false;
                colvarBankCode.IsNullable = true;
                colvarBankCode.IsPrimaryKey = false;
                colvarBankCode.IsForeignKey = false;
                colvarBankCode.IsReadOnly = false;

                schema.Columns.Add(colvarBankCode);

                TableSchema.TableColumn colvarAccountNumber = new TableSchema.TableColumn(schema);
                colvarAccountNumber.ColumnName = "account_number";
                colvarAccountNumber.DataType = DbType.AnsiString;
                colvarAccountNumber.MaxLength = 50;
                colvarAccountNumber.AutoIncrement = false;
                colvarAccountNumber.IsNullable = true;
                colvarAccountNumber.IsPrimaryKey = false;
                colvarAccountNumber.IsForeignKey = false;
                colvarAccountNumber.IsReadOnly = false;

                schema.Columns.Add(colvarAccountNumber);

                TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
                colvarPhone.ColumnName = "phone";
                colvarPhone.DataType = DbType.AnsiString;
                colvarPhone.MaxLength = 20;
                colvarPhone.AutoIncrement = false;
                colvarPhone.IsNullable = true;
                colvarPhone.IsPrimaryKey = false;
                colvarPhone.IsForeignKey = false;
                colvarPhone.IsReadOnly = false;

                schema.Columns.Add(colvarPhone);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarRefundProcessTime = new TableSchema.TableColumn(schema);
                colvarRefundProcessTime.ColumnName = "refund_process_time";
                colvarRefundProcessTime.DataType = DbType.DateTime;
                colvarRefundProcessTime.MaxLength = 0;
                colvarRefundProcessTime.AutoIncrement = false;
                colvarRefundProcessTime.IsNullable = true;
                colvarRefundProcessTime.IsPrimaryKey = false;
                colvarRefundProcessTime.IsForeignKey = false;
                colvarRefundProcessTime.IsReadOnly = false;

                schema.Columns.Add(colvarRefundProcessTime);

                TableSchema.TableColumn colvarRefundSendTime = new TableSchema.TableColumn(schema);
                colvarRefundSendTime.ColumnName = "refund_send_time";
                colvarRefundSendTime.DataType = DbType.DateTime;
                colvarRefundSendTime.MaxLength = 0;
                colvarRefundSendTime.AutoIncrement = false;
                colvarRefundSendTime.IsNullable = true;
                colvarRefundSendTime.IsPrimaryKey = false;
                colvarRefundSendTime.IsForeignKey = false;
                colvarRefundSendTime.IsReadOnly = false;

                schema.Columns.Add(colvarRefundSendTime);

                TableSchema.TableColumn colvarRefundResponseTime = new TableSchema.TableColumn(schema);
                colvarRefundResponseTime.ColumnName = "refund_response_time";
                colvarRefundResponseTime.DataType = DbType.DateTime;
                colvarRefundResponseTime.MaxLength = 0;
                colvarRefundResponseTime.AutoIncrement = false;
                colvarRefundResponseTime.IsNullable = true;
                colvarRefundResponseTime.IsPrimaryKey = false;
                colvarRefundResponseTime.IsForeignKey = false;
                colvarRefundResponseTime.IsReadOnly = false;

                schema.Columns.Add(colvarRefundResponseTime);

                TableSchema.TableColumn colvarTargetDate = new TableSchema.TableColumn(schema);
                colvarTargetDate.ColumnName = "target_date";
                colvarTargetDate.DataType = DbType.DateTime;
                colvarTargetDate.MaxLength = 0;
                colvarTargetDate.AutoIncrement = false;
                colvarTargetDate.IsNullable = true;
                colvarTargetDate.IsPrimaryKey = false;
                colvarTargetDate.IsForeignKey = false;
                colvarTargetDate.IsReadOnly = false;

                schema.Columns.Add(colvarTargetDate);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarFailReason = new TableSchema.TableColumn(schema);
                colvarFailReason.ColumnName = "fail_reason";
                colvarFailReason.DataType = DbType.String;
                colvarFailReason.MaxLength = 50;
                colvarFailReason.AutoIncrement = false;
                colvarFailReason.IsNullable = true;
                colvarFailReason.IsPrimaryKey = false;
                colvarFailReason.IsForeignKey = false;
                colvarFailReason.IsReadOnly = false;

                schema.Columns.Add(colvarFailReason);

                TableSchema.TableColumn colvarCouponList = new TableSchema.TableColumn(schema);
                colvarCouponList.ColumnName = "coupon_list";
                colvarCouponList.DataType = DbType.AnsiString;
                colvarCouponList.MaxLength = -1;
                colvarCouponList.AutoIncrement = false;
                colvarCouponList.IsNullable = true;
                colvarCouponList.IsPrimaryKey = false;
                colvarCouponList.IsForeignKey = false;
                colvarCouponList.IsReadOnly = false;

                schema.Columns.Add(colvarCouponList);

                TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
                colvarFlag.ColumnName = "flag";
                colvarFlag.DataType = DbType.Int32;
                colvarFlag.MaxLength = 0;
                colvarFlag.AutoIncrement = false;
                colvarFlag.IsNullable = false;
                colvarFlag.IsPrimaryKey = false;
                colvarFlag.IsForeignKey = false;
                colvarFlag.IsReadOnly = false;

                schema.Columns.Add(colvarFlag);

                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;

                schema.Columns.Add(colvarOrderId);

                TableSchema.TableColumn colvarProcessDate = new TableSchema.TableColumn(schema);
                colvarProcessDate.ColumnName = "process_date";
                colvarProcessDate.DataType = DbType.DateTime;
                colvarProcessDate.MaxLength = 0;
                colvarProcessDate.AutoIncrement = false;
                colvarProcessDate.IsNullable = true;
                colvarProcessDate.IsPrimaryKey = false;
                colvarProcessDate.IsForeignKey = false;
                colvarProcessDate.IsReadOnly = false;

                schema.Columns.Add(colvarProcessDate);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;

                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarPaymentType = new TableSchema.TableColumn(schema);
                colvarPaymentType.ColumnName = "payment_type";
                colvarPaymentType.DataType = DbType.String;
                colvarPaymentType.MaxLength = 7;
                colvarPaymentType.AutoIncrement = false;
                colvarPaymentType.IsNullable = false;
                colvarPaymentType.IsPrimaryKey = false;
                colvarPaymentType.IsForeignKey = false;
                colvarPaymentType.IsReadOnly = false;

                schema.Columns.Add(colvarPaymentType);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ct_atm_refund",schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewCtAtmRefund()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewCtAtmRefund(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewCtAtmRefund(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewCtAtmRefund(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Si")]
        [Bindable(true)]
        public int Si
        {
            get
            {
                return GetColumnValue<int>("si");
            }
            set
            {
                SetColumnValue("si", value);
            }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid
        {
            get
            {
                return GetColumnValue<Guid>("order_guid");
            }
            set
            {
                SetColumnValue("order_guid", value);
            }
        }

        [XmlAttribute("PaymentId")]
        [Bindable(true)]
        public string PaymentId
        {
            get
            {
                return GetColumnValue<string>("payment_id");
            }
            set
            {
                SetColumnValue("payment_id", value);
            }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public int Amount
        {
            get
            {
                return GetColumnValue<int>("amount");
            }
            set
            {
                SetColumnValue("amount", value);
            }
        }

        [XmlAttribute("AccountName")]
        [Bindable(true)]
        public string AccountName
        {
            get
            {
                return GetColumnValue<string>("account_name");
            }
            set
            {
                SetColumnValue("account_name", value);
            }
        }

        [XmlAttribute("Id")]
        [Bindable(true)]
        public string Id
        {
            get
            {
                return GetColumnValue<string>("ID");
            }
            set
            {
                SetColumnValue("ID", value);
            }
        }

        [XmlAttribute("BankName")]
        [Bindable(true)]
        public string BankName
        {
            get
            {
                return GetColumnValue<string>("bank_name");
            }
            set
            {
                SetColumnValue("bank_name", value);
            }
        }

        [XmlAttribute("BranchName")]
        [Bindable(true)]
        public string BranchName
        {
            get
            {
                return GetColumnValue<string>("branch_name");
            }
            set
            {
                SetColumnValue("branch_name", value);
            }
        }

        [XmlAttribute("BankCode")]
        [Bindable(true)]
        public string BankCode
        {
            get
            {
                return GetColumnValue<string>("bank_code");
            }
            set
            {
                SetColumnValue("bank_code", value);
            }
        }

        [XmlAttribute("AccountNumber")]
        [Bindable(true)]
        public string AccountNumber
        {
            get
            {
                return GetColumnValue<string>("account_number");
            }
            set
            {
                SetColumnValue("account_number", value);
            }
        }

        [XmlAttribute("Phone")]
        [Bindable(true)]
        public string Phone
        {
            get
            {
                return GetColumnValue<string>("phone");
            }
            set
            {
                SetColumnValue("phone", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("RefundProcessTime")]
        [Bindable(true)]
        public DateTime? RefundProcessTime
        {
            get
            {
                return GetColumnValue<DateTime?>("refund_process_time");
            }
            set
            {
                SetColumnValue("refund_process_time", value);
            }
        }

        [XmlAttribute("RefundSendTime")]
        [Bindable(true)]
        public DateTime? RefundSendTime
        {
            get
            {
                return GetColumnValue<DateTime?>("refund_send_time");
            }
            set
            {
                SetColumnValue("refund_send_time", value);
            }
        }

        [XmlAttribute("RefundResponseTime")]
        [Bindable(true)]
        public DateTime? RefundResponseTime
        {
            get
            {
                return GetColumnValue<DateTime?>("refund_response_time");
            }
            set
            {
                SetColumnValue("refund_response_time", value);
            }
        }

        [XmlAttribute("TargetDate")]
        [Bindable(true)]
        public DateTime? TargetDate
        {
            get
            {
                return GetColumnValue<DateTime?>("target_date");
            }
            set
            {
                SetColumnValue("target_date", value);
            }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get
            {
                return GetColumnValue<int>("status");
            }
            set
            {
                SetColumnValue("status", value);
            }
        }

        [XmlAttribute("FailReason")]
        [Bindable(true)]
        public string FailReason
        {
            get
            {
                return GetColumnValue<string>("fail_reason");
            }
            set
            {
                SetColumnValue("fail_reason", value);
            }
        }

        [XmlAttribute("CouponList")]
        [Bindable(true)]
        public string CouponList
        {
            get
            {
                return GetColumnValue<string>("coupon_list");
            }
            set
            {
                SetColumnValue("coupon_list", value);
            }
        }

        [XmlAttribute("Flag")]
        [Bindable(true)]
        public int Flag
        {
            get
            {
                return GetColumnValue<int>("flag");
            }
            set
            {
                SetColumnValue("flag", value);
            }
        }

        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId
        {
            get
            {
                return GetColumnValue<string>("order_id");
            }
            set
            {
                SetColumnValue("order_id", value);
            }
        }

        [XmlAttribute("ProcessDate")]
        [Bindable(true)]
        public DateTime? ProcessDate
        {
            get
            {
                return GetColumnValue<DateTime?>("process_date");
            }
            set
            {
                SetColumnValue("process_date", value);
            }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get
            {
                return GetColumnValue<int>("user_id");
            }
            set
            {
                SetColumnValue("user_id", value);
            }
        }

        [XmlAttribute("PaymentType")]
        [Bindable(true)]
        public string PaymentType
        {
            get
            {
                return GetColumnValue<string>("payment_type");
            }
            set
            {
                SetColumnValue("payment_type", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Si = @"si";

            public static string OrderGuid = @"order_guid";

            public static string PaymentId = @"payment_id";

            public static string Amount = @"amount";

            public static string AccountName = @"account_name";

            public static string Id = @"ID";

            public static string BankName = @"bank_name";

            public static string BranchName = @"branch_name";

            public static string BankCode = @"bank_code";

            public static string AccountNumber = @"account_number";

            public static string Phone = @"phone";

            public static string CreateTime = @"create_time";

            public static string RefundProcessTime = @"refund_process_time";

            public static string RefundSendTime = @"refund_send_time";

            public static string RefundResponseTime = @"refund_response_time";

            public static string TargetDate = @"target_date";

            public static string Status = @"status";

            public static string FailReason = @"fail_reason";

            public static string CouponList = @"coupon_list";

            public static string Flag = @"flag";

            public static string OrderId = @"order_id";

            public static string ProcessDate = @"process_date";

            public static string UserId = @"user_id";

            public static string PaymentType = @"payment_type";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealCouponStoreListCount class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealCouponStoreListCountCollection : ReadOnlyList<ViewHiDealCouponStoreListCount, ViewHiDealCouponStoreListCountCollection>
    {
        public ViewHiDealCouponStoreListCountCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_coupon_store_list_count view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealCouponStoreListCount : ReadOnlyRecord<ViewHiDealCouponStoreListCount>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_coupon_store_list_count", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;

                schema.Columns.Add(colvarDealId);

                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 50;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;

                schema.Columns.Add(colvarDealName);

                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = false;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;

                schema.Columns.Add(colvarProductId);

                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "product_name";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 250;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = true;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;

                schema.Columns.Add(colvarProductName);

                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;

                schema.Columns.Add(colvarStoreGuid);

                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;

                schema.Columns.Add(colvarStoreName);

                TableSchema.TableColumn colvarCouponAmount = new TableSchema.TableColumn(schema);
                colvarCouponAmount.ColumnName = "CouponAmount";
                colvarCouponAmount.DataType = DbType.Int32;
                colvarCouponAmount.MaxLength = 0;
                colvarCouponAmount.AutoIncrement = false;
                colvarCouponAmount.IsNullable = true;
                colvarCouponAmount.IsPrimaryKey = false;
                colvarCouponAmount.IsForeignKey = false;
                colvarCouponAmount.IsReadOnly = false;

                schema.Columns.Add(colvarCouponAmount);

                TableSchema.TableColumn colvarCouponAmountSetting = new TableSchema.TableColumn(schema);
                colvarCouponAmountSetting.ColumnName = "CouponAmountSetting";
                colvarCouponAmountSetting.DataType = DbType.Int32;
                colvarCouponAmountSetting.MaxLength = 0;
                colvarCouponAmountSetting.AutoIncrement = false;
                colvarCouponAmountSetting.IsNullable = true;
                colvarCouponAmountSetting.IsPrimaryKey = false;
                colvarCouponAmountSetting.IsForeignKey = false;
                colvarCouponAmountSetting.IsReadOnly = false;

                schema.Columns.Add(colvarCouponAmountSetting);

                TableSchema.TableColumn colvarRefundProcessingCouponAmount = new TableSchema.TableColumn(schema);
                colvarRefundProcessingCouponAmount.ColumnName = "RefundProcessingCouponAmount";
                colvarRefundProcessingCouponAmount.DataType = DbType.Int32;
                colvarRefundProcessingCouponAmount.MaxLength = 0;
                colvarRefundProcessingCouponAmount.AutoIncrement = false;
                colvarRefundProcessingCouponAmount.IsNullable = true;
                colvarRefundProcessingCouponAmount.IsPrimaryKey = false;
                colvarRefundProcessingCouponAmount.IsForeignKey = false;
                colvarRefundProcessingCouponAmount.IsReadOnly = false;

                schema.Columns.Add(colvarRefundProcessingCouponAmount);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_coupon_store_list_count", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewHiDealCouponStoreListCount()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealCouponStoreListCount(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewHiDealCouponStoreListCount(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewHiDealCouponStoreListCount(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId
        {
            get
            {
                return GetColumnValue<int>("deal_id");
            }
            set
            {
                SetColumnValue("deal_id", value);
            }
        }

        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName
        {
            get
            {
                return GetColumnValue<string>("deal_name");
            }
            set
            {
                SetColumnValue("deal_name", value);
            }
        }

        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int ProductId
        {
            get
            {
                return GetColumnValue<int>("product_id");
            }
            set
            {
                SetColumnValue("product_id", value);
            }
        }

        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName
        {
            get
            {
                return GetColumnValue<string>("product_name");
            }
            set
            {
                SetColumnValue("product_name", value);
            }
        }

        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid
        {
            get
            {
                return GetColumnValue<Guid?>("store_guid");
            }
            set
            {
                SetColumnValue("store_guid", value);
            }
        }

        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName
        {
            get
            {
                return GetColumnValue<string>("store_name");
            }
            set
            {
                SetColumnValue("store_name", value);
            }
        }

        [XmlAttribute("CouponAmount")]
        [Bindable(true)]
        public int? CouponAmount
        {
            get
            {
                return GetColumnValue<int?>("CouponAmount");
            }
            set
            {
                SetColumnValue("CouponAmount", value);
            }
        }

        [XmlAttribute("CouponAmountSetting")]
        [Bindable(true)]
        public int? CouponAmountSetting
        {
            get
            {
                return GetColumnValue<int?>("CouponAmountSetting");
            }
            set
            {
                SetColumnValue("CouponAmountSetting", value);
            }
        }

        [XmlAttribute("RefundProcessingCouponAmount")]
        [Bindable(true)]
        public int? RefundProcessingCouponAmount
        {
            get
            {
                return GetColumnValue<int?>("RefundProcessingCouponAmount");
            }
            set
            {
                SetColumnValue("RefundProcessingCouponAmount", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string DealId = @"deal_id";

            public static string DealName = @"deal_name";

            public static string ProductId = @"product_id";

            public static string ProductName = @"product_name";

            public static string StoreGuid = @"store_guid";

            public static string StoreName = @"store_name";

            public static string CouponAmount = @"CouponAmount";

            public static string CouponAmountSetting = @"CouponAmountSetting";

            public static string RefundProcessingCouponAmount = @"RefundProcessingCouponAmount";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

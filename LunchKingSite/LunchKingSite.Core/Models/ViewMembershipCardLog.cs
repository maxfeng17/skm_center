using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMembershipCardLog class.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardLogCollection : ReadOnlyList<ViewMembershipCardLog, ViewMembershipCardLogCollection>
    {        
        public ViewMembershipCardLogCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_membership_card_log view.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardLog : ReadOnlyRecord<ViewMembershipCardLog>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_membership_card_log", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarMembershipCardIdBefore = new TableSchema.TableColumn(schema);
                colvarMembershipCardIdBefore.ColumnName = "membership_card_id_before";
                colvarMembershipCardIdBefore.DataType = DbType.Int32;
                colvarMembershipCardIdBefore.MaxLength = 0;
                colvarMembershipCardIdBefore.AutoIncrement = false;
                colvarMembershipCardIdBefore.IsNullable = false;
                colvarMembershipCardIdBefore.IsPrimaryKey = false;
                colvarMembershipCardIdBefore.IsForeignKey = false;
                colvarMembershipCardIdBefore.IsReadOnly = false;
                
                schema.Columns.Add(colvarMembershipCardIdBefore);
                
                TableSchema.TableColumn colvarMembershipCardIdAfter = new TableSchema.TableColumn(schema);
                colvarMembershipCardIdAfter.ColumnName = "membership_card_id_after";
                colvarMembershipCardIdAfter.DataType = DbType.Int32;
                colvarMembershipCardIdAfter.MaxLength = 0;
                colvarMembershipCardIdAfter.AutoIncrement = false;
                colvarMembershipCardIdAfter.IsNullable = false;
                colvarMembershipCardIdAfter.IsPrimaryKey = false;
                colvarMembershipCardIdAfter.IsForeignKey = false;
                colvarMembershipCardIdAfter.IsReadOnly = false;
                
                schema.Columns.Add(colvarMembershipCardIdAfter);
                
                TableSchema.TableColumn colvarUserMembershipCardId = new TableSchema.TableColumn(schema);
                colvarUserMembershipCardId.ColumnName = "user_membership_card_id";
                colvarUserMembershipCardId.DataType = DbType.Int32;
                colvarUserMembershipCardId.MaxLength = 0;
                colvarUserMembershipCardId.AutoIncrement = false;
                colvarUserMembershipCardId.IsNullable = true;
                colvarUserMembershipCardId.IsPrimaryKey = false;
                colvarUserMembershipCardId.IsForeignKey = false;
                colvarUserMembershipCardId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserMembershipCardId);
                
                TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
                colvarMemo.ColumnName = "memo";
                colvarMemo.DataType = DbType.String;
                colvarMemo.MaxLength = -1;
                colvarMemo.AutoIncrement = false;
                colvarMemo.IsNullable = false;
                colvarMemo.IsPrimaryKey = false;
                colvarMemo.IsForeignKey = false;
                colvarMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemo);
                
                TableSchema.TableColumn colvarLogType = new TableSchema.TableColumn(schema);
                colvarLogType.ColumnName = "log_type";
                colvarLogType.DataType = DbType.Byte;
                colvarLogType.MaxLength = 0;
                colvarLogType.AutoIncrement = false;
                colvarLogType.IsNullable = false;
                colvarLogType.IsPrimaryKey = false;
                colvarLogType.IsForeignKey = false;
                colvarLogType.IsReadOnly = false;
                
                schema.Columns.Add(colvarLogType);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarLevel = new TableSchema.TableColumn(schema);
                colvarLevel.ColumnName = "level";
                colvarLevel.DataType = DbType.Int32;
                colvarLevel.MaxLength = 0;
                colvarLevel.AutoIncrement = false;
                colvarLevel.IsNullable = true;
                colvarLevel.IsPrimaryKey = false;
                colvarLevel.IsForeignKey = false;
                colvarLevel.IsReadOnly = false;
                
                schema.Columns.Add(colvarLevel);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = true;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarPcpOrderGuid = new TableSchema.TableColumn(schema);
                colvarPcpOrderGuid.ColumnName = "pcp_order_guid";
                colvarPcpOrderGuid.DataType = DbType.Guid;
                colvarPcpOrderGuid.MaxLength = 0;
                colvarPcpOrderGuid.AutoIncrement = false;
                colvarPcpOrderGuid.IsNullable = true;
                colvarPcpOrderGuid.IsPrimaryKey = false;
                colvarPcpOrderGuid.IsForeignKey = false;
                colvarPcpOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcpOrderGuid);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 20;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarDiscountType = new TableSchema.TableColumn(schema);
                colvarDiscountType.ColumnName = "discount_type";
                colvarDiscountType.DataType = DbType.Int32;
                colvarDiscountType.MaxLength = 0;
                colvarDiscountType.AutoIncrement = false;
                colvarDiscountType.IsNullable = true;
                colvarDiscountType.IsPrimaryKey = false;
                colvarDiscountType.IsForeignKey = false;
                colvarDiscountType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_membership_card_log",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMembershipCardLog()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMembershipCardLog(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMembershipCardLog(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMembershipCardLog(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("MembershipCardIdBefore")]
        [Bindable(true)]
        public int MembershipCardIdBefore 
	    {
		    get
		    {
			    return GetColumnValue<int>("membership_card_id_before");
		    }
            set 
		    {
			    SetColumnValue("membership_card_id_before", value);
            }
        }
	      
        [XmlAttribute("MembershipCardIdAfter")]
        [Bindable(true)]
        public int MembershipCardIdAfter 
	    {
		    get
		    {
			    return GetColumnValue<int>("membership_card_id_after");
		    }
            set 
		    {
			    SetColumnValue("membership_card_id_after", value);
            }
        }
	      
        [XmlAttribute("UserMembershipCardId")]
        [Bindable(true)]
        public int? UserMembershipCardId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("user_membership_card_id");
		    }
            set 
		    {
			    SetColumnValue("user_membership_card_id", value);
            }
        }
	      
        [XmlAttribute("Memo")]
        [Bindable(true)]
        public string Memo 
	    {
		    get
		    {
			    return GetColumnValue<string>("memo");
		    }
            set 
		    {
			    SetColumnValue("memo", value);
            }
        }
	      
        [XmlAttribute("LogType")]
        [Bindable(true)]
        public byte LogType 
	    {
		    get
		    {
			    return GetColumnValue<byte>("log_type");
		    }
            set 
		    {
			    SetColumnValue("log_type", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId 
	    {
		    get
		    {
			    return GetColumnValue<int>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("Level")]
        [Bindable(true)]
        public int? Level 
	    {
		    get
		    {
			    return GetColumnValue<int?>("level");
		    }
            set 
		    {
			    SetColumnValue("level", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int? UserId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("PcpOrderGuid")]
        [Bindable(true)]
        public Guid? PcpOrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("pcp_order_guid");
		    }
            set 
		    {
			    SetColumnValue("pcp_order_guid", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("DiscountType")]
        [Bindable(true)]
        public int? DiscountType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("discount_type");
		    }
            set 
		    {
			    SetColumnValue("discount_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string MembershipCardIdBefore = @"membership_card_id_before";
            
            public static string MembershipCardIdAfter = @"membership_card_id_after";
            
            public static string UserMembershipCardId = @"user_membership_card_id";
            
            public static string Memo = @"memo";
            
            public static string LogType = @"log_type";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string Level = @"level";
            
            public static string UserId = @"user_id";
            
            public static string EndTime = @"end_time";
            
            public static string SellerName = @"seller_name";
            
            public static string PcpOrderGuid = @"pcp_order_guid";
            
            public static string Code = @"code";
            
            public static string DiscountType = @"discount_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

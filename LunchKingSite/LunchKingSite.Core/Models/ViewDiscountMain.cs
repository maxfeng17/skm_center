using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewDiscountMain class.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountMainCollection : ReadOnlyList<ViewDiscountMain, ViewDiscountMainCollection>
    {
        public ViewDiscountMainCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_discount_main view.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountMain : ReadOnlyRecord<ViewDiscountMain>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_discount_main", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarApply = new TableSchema.TableColumn(schema);
                colvarApply.ColumnName = "apply";
                colvarApply.DataType = DbType.AnsiString;
                colvarApply.MaxLength = 10;
                colvarApply.AutoIncrement = false;
                colvarApply.IsNullable = true;
                colvarApply.IsPrimaryKey = false;
                colvarApply.IsForeignKey = false;
                colvarApply.IsReadOnly = false;

                schema.Columns.Add(colvarApply);

                TableSchema.TableColumn colvarApplyTime = new TableSchema.TableColumn(schema);
                colvarApplyTime.ColumnName = "apply_time";
                colvarApplyTime.DataType = DbType.DateTime;
                colvarApplyTime.MaxLength = 0;
                colvarApplyTime.AutoIncrement = false;
                colvarApplyTime.IsNullable = true;
                colvarApplyTime.IsPrimaryKey = false;
                colvarApplyTime.IsForeignKey = false;
                colvarApplyTime.IsReadOnly = false;

                schema.Columns.Add(colvarApplyTime);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 100;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;

                schema.Columns.Add(colvarName);

                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = true;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;

                schema.Columns.Add(colvarStartTime);

                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;

                schema.Columns.Add(colvarEndTime);

                TableSchema.TableColumn colvarBetween = new TableSchema.TableColumn(schema);
                colvarBetween.ColumnName = "between";
                colvarBetween.DataType = DbType.AnsiString;
                colvarBetween.MaxLength = 41;
                colvarBetween.AutoIncrement = false;
                colvarBetween.IsNullable = true;
                colvarBetween.IsPrimaryKey = false;
                colvarBetween.IsForeignKey = false;
                colvarBetween.IsReadOnly = false;

                schema.Columns.Add(colvarBetween);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = true;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;

                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
                colvarQty.ColumnName = "qty";
                colvarQty.DataType = DbType.Int32;
                colvarQty.MaxLength = 0;
                colvarQty.AutoIncrement = false;
                colvarQty.IsNullable = true;
                colvarQty.IsPrimaryKey = false;
                colvarQty.IsForeignKey = false;
                colvarQty.IsReadOnly = false;

                schema.Columns.Add(colvarQty);

                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;

                schema.Columns.Add(colvarTotal);

                TableSchema.TableColumn colvarAlreadyUse = new TableSchema.TableColumn(schema);
                colvarAlreadyUse.ColumnName = "already_use";
                colvarAlreadyUse.DataType = DbType.Currency;
                colvarAlreadyUse.MaxLength = 0;
                colvarAlreadyUse.AutoIncrement = false;
                colvarAlreadyUse.IsNullable = true;
                colvarAlreadyUse.IsPrimaryKey = false;
                colvarAlreadyUse.IsForeignKey = false;
                colvarAlreadyUse.IsReadOnly = false;

                schema.Columns.Add(colvarAlreadyUse);

                TableSchema.TableColumn colvarRealUse = new TableSchema.TableColumn(schema);
                colvarRealUse.ColumnName = "real_use";
                colvarRealUse.DataType = DbType.Currency;
                colvarRealUse.MaxLength = 0;
                colvarRealUse.AutoIncrement = false;
                colvarRealUse.IsNullable = true;
                colvarRealUse.IsPrimaryKey = false;
                colvarRealUse.IsForeignKey = false;
                colvarRealUse.IsReadOnly = false;

                schema.Columns.Add(colvarRealUse);

                TableSchema.TableColumn colvarUser = new TableSchema.TableColumn(schema);
                colvarUser.ColumnName = "user";
                colvarUser.DataType = DbType.AnsiString;
                colvarUser.MaxLength = 50;
                colvarUser.AutoIncrement = false;
                colvarUser.IsNullable = true;
                colvarUser.IsPrimaryKey = false;
                colvarUser.IsForeignKey = false;
                colvarUser.IsReadOnly = false;

                schema.Columns.Add(colvarUser);

                TableSchema.TableColumn colvarCurrentCount = new TableSchema.TableColumn(schema);
                colvarCurrentCount.ColumnName = "current_count";
                colvarCurrentCount.DataType = DbType.Int32;
                colvarCurrentCount.MaxLength = 0;
                colvarCurrentCount.AutoIncrement = false;
                colvarCurrentCount.IsNullable = true;
                colvarCurrentCount.IsPrimaryKey = false;
                colvarCurrentCount.IsForeignKey = false;
                colvarCurrentCount.IsReadOnly = false;

                schema.Columns.Add(colvarCurrentCount);

                TableSchema.TableColumn colvarCancelTime = new TableSchema.TableColumn(schema);
                colvarCancelTime.ColumnName = "cancel_time";
                colvarCancelTime.DataType = DbType.DateTime;
                colvarCancelTime.MaxLength = 0;
                colvarCancelTime.AutoIncrement = false;
                colvarCancelTime.IsNullable = true;
                colvarCancelTime.IsPrimaryKey = false;
                colvarCancelTime.IsForeignKey = false;
                colvarCancelTime.IsReadOnly = false;

                schema.Columns.Add(colvarCancelTime);

                TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
                colvarFlag.ColumnName = "flag";
                colvarFlag.DataType = DbType.Int32;
                colvarFlag.MaxLength = 0;
                colvarFlag.AutoIncrement = false;
                colvarFlag.IsNullable = false;
                colvarFlag.IsPrimaryKey = false;
                colvarFlag.IsForeignKey = false;
                colvarFlag.IsReadOnly = false;

                schema.Columns.Add(colvarFlag);

                TableSchema.TableColumn colvarMinimumAmount = new TableSchema.TableColumn(schema);
                colvarMinimumAmount.ColumnName = "minimum_amount";
                colvarMinimumAmount.DataType = DbType.Int32;
                colvarMinimumAmount.MaxLength = 0;
                colvarMinimumAmount.AutoIncrement = false;
                colvarMinimumAmount.IsNullable = true;
                colvarMinimumAmount.IsPrimaryKey = false;
                colvarMinimumAmount.IsForeignKey = false;
                colvarMinimumAmount.IsReadOnly = false;

                schema.Columns.Add(colvarMinimumAmount);

                TableSchema.TableColumn colvarEventDateS = new TableSchema.TableColumn(schema);
                colvarEventDateS.ColumnName = "event_date_s";
                colvarEventDateS.DataType = DbType.DateTime;
                colvarEventDateS.MaxLength = 0;
                colvarEventDateS.AutoIncrement = false;
                colvarEventDateS.IsNullable = true;
                colvarEventDateS.IsPrimaryKey = false;
                colvarEventDateS.IsForeignKey = false;
                colvarEventDateS.IsReadOnly = false;

                schema.Columns.Add(colvarEventDateS);

                TableSchema.TableColumn colvarEventDateE = new TableSchema.TableColumn(schema);
                colvarEventDateE.ColumnName = "event_date_e";
                colvarEventDateE.DataType = DbType.DateTime;
                colvarEventDateE.MaxLength = 0;
                colvarEventDateE.AutoIncrement = false;
                colvarEventDateE.IsNullable = true;
                colvarEventDateE.IsPrimaryKey = false;
                colvarEventDateE.IsForeignKey = false;
                colvarEventDateE.IsReadOnly = false;

                schema.Columns.Add(colvarEventDateE);

                TableSchema.TableColumn colvarFreeGiftDiscount = new TableSchema.TableColumn(schema);
                colvarFreeGiftDiscount.ColumnName = "free_gift_discount";
                colvarFreeGiftDiscount.DataType = DbType.Currency;
                colvarFreeGiftDiscount.MaxLength = 0;
                colvarFreeGiftDiscount.AutoIncrement = false;
                colvarFreeGiftDiscount.IsNullable = true;
                colvarFreeGiftDiscount.IsPrimaryKey = false;
                colvarFreeGiftDiscount.IsForeignKey = false;
                colvarFreeGiftDiscount.IsReadOnly = false;

                schema.Columns.Add(colvarFreeGiftDiscount);

                TableSchema.TableColumn colvarMinGrossMargin = new TableSchema.TableColumn(schema);
                colvarMinGrossMargin.ColumnName = "min_gross_margin";
                colvarMinGrossMargin.DataType = DbType.Int32;
                colvarMinGrossMargin.MaxLength = 0;
                colvarMinGrossMargin.AutoIncrement = false;
                colvarMinGrossMargin.IsNullable = true;
                colvarMinGrossMargin.IsPrimaryKey = false;
                colvarMinGrossMargin.IsForeignKey = false;
                colvarMinGrossMargin.IsReadOnly = false;

                schema.Columns.Add(colvarMinGrossMargin);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_discount_main", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewDiscountMain()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewDiscountMain(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewDiscountMain(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewDiscountMain(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("Apply")]
        [Bindable(true)]
        public string Apply
        {
            get
            {
                return GetColumnValue<string>("apply");
            }
            set
            {
                SetColumnValue("apply", value);
            }
        }

        [XmlAttribute("ApplyTime")]
        [Bindable(true)]
        public DateTime? ApplyTime
        {
            get
            {
                return GetColumnValue<DateTime?>("apply_time");
            }
            set
            {
                SetColumnValue("apply_time", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get
            {
                return GetColumnValue<DateTime?>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name
        {
            get
            {
                return GetColumnValue<string>("name");
            }
            set
            {
                SetColumnValue("name", value);
            }
        }

        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime? StartTime
        {
            get
            {
                return GetColumnValue<DateTime?>("start_time");
            }
            set
            {
                SetColumnValue("start_time", value);
            }
        }

        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime
        {
            get
            {
                return GetColumnValue<DateTime?>("end_time");
            }
            set
            {
                SetColumnValue("end_time", value);
            }
        }

        [XmlAttribute("Between")]
        [Bindable(true)]
        public string Between
        {
            get
            {
                return GetColumnValue<string>("between");
            }
            set
            {
                SetColumnValue("between", value);
            }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal? Amount
        {
            get
            {
                return GetColumnValue<decimal?>("amount");
            }
            set
            {
                SetColumnValue("amount", value);
            }
        }

        [XmlAttribute("Qty")]
        [Bindable(true)]
        public int? Qty
        {
            get
            {
                return GetColumnValue<int?>("qty");
            }
            set
            {
                SetColumnValue("qty", value);
            }
        }

        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal? Total
        {
            get
            {
                return GetColumnValue<decimal?>("total");
            }
            set
            {
                SetColumnValue("total", value);
            }
        }

        [XmlAttribute("AlreadyUse")]
        [Bindable(true)]
        public decimal? AlreadyUse
        {
            get
            {
                return GetColumnValue<decimal?>("already_use");
            }
            set
            {
                SetColumnValue("already_use", value);
            }
        }

        [XmlAttribute("RealUse")]
        [Bindable(true)]
        public decimal? RealUse
        {
            get
            {
                return GetColumnValue<decimal?>("real_use");
            }
            set
            {
                SetColumnValue("real_use", value);
            }
        }

        [XmlAttribute("User")]
        [Bindable(true)]
        public string User
        {
            get
            {
                return GetColumnValue<string>("user");
            }
            set
            {
                SetColumnValue("user", value);
            }
        }

        [XmlAttribute("CurrentCount")]
        [Bindable(true)]
        public int? CurrentCount
        {
            get
            {
                return GetColumnValue<int?>("current_count");
            }
            set
            {
                SetColumnValue("current_count", value);
            }
        }

        [XmlAttribute("CancelTime")]
        [Bindable(true)]
        public DateTime? CancelTime
        {
            get
            {
                return GetColumnValue<DateTime?>("cancel_time");
            }
            set
            {
                SetColumnValue("cancel_time", value);
            }
        }

        [XmlAttribute("Flag")]
        [Bindable(true)]
        public int Flag
        {
            get
            {
                return GetColumnValue<int>("flag");
            }
            set
            {
                SetColumnValue("flag", value);
            }
        }

        [XmlAttribute("MinimumAmount")]
        [Bindable(true)]
        public int? MinimumAmount
        {
            get
            {
                return GetColumnValue<int?>("minimum_amount");
            }
            set
            {
                SetColumnValue("minimum_amount", value);
            }
        }

        [XmlAttribute("EventDateS")]
        [Bindable(true)]
        public DateTime? EventDateS
        {
            get
            {
                return GetColumnValue<DateTime?>("event_date_s");
            }
            set
            {
                SetColumnValue("event_date_s", value);
            }
        }

        [XmlAttribute("EventDateE")]
        [Bindable(true)]
        public DateTime? EventDateE
        {
            get
            {
                return GetColumnValue<DateTime?>("event_date_e");
            }
            set
            {
                SetColumnValue("event_date_e", value);
            }
        }

        [XmlAttribute("FreeGiftDiscount")]
        [Bindable(true)]
        public decimal? FreeGiftDiscount
        {
            get
            {
                return GetColumnValue<decimal?>("free_gift_discount");
            }
            set
            {
                SetColumnValue("free_gift_discount", value);
            }
        }

        [XmlAttribute("MinGrossMargin")]
        [Bindable(true)]
        public int? MinGrossMargin
        {
            get
            {
                return GetColumnValue<int?>("min_gross_margin");
            }
            set
            {
                SetColumnValue("min_gross_margin", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Id = @"id";

            public static string Apply = @"apply";

            public static string ApplyTime = @"apply_time";

            public static string CreateTime = @"create_time";

            public static string Name = @"name";

            public static string StartTime = @"start_time";

            public static string EndTime = @"end_time";

            public static string Between = @"between";

            public static string Amount = @"amount";

            public static string Qty = @"qty";

            public static string Total = @"total";

            public static string AlreadyUse = @"already_use";

            public static string RealUse = @"real_use";

            public static string User = @"user";

            public static string CurrentCount = @"current_count";

            public static string CancelTime = @"cancel_time";

            public static string Flag = @"flag";

            public static string MinimumAmount = @"minimum_amount";

            public static string EventDateS = @"event_date_s";

            public static string EventDateE = @"event_date_e";

            public static string FreeGiftDiscount = @"free_gift_discount";

            public static string MinGrossMargin = @"min_gross_margin";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

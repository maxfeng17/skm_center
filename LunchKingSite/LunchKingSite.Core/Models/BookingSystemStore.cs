using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BookingSystemStore class.
	/// </summary>
    [Serializable]
	public partial class BookingSystemStoreCollection : RepositoryList<BookingSystemStore, BookingSystemStoreCollection>
	{	   
		public BookingSystemStoreCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BookingSystemStoreCollection</returns>
		public BookingSystemStoreCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BookingSystemStore o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the booking_system_store table.
	/// </summary>
	[Serializable]
	public partial class BookingSystemStore : RepositoryRecord<BookingSystemStore>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BookingSystemStore()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BookingSystemStore(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("booking_system_store", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarBookingId = new TableSchema.TableColumn(schema);
				colvarBookingId.ColumnName = "booking_id";
				colvarBookingId.DataType = DbType.Int32;
				colvarBookingId.MaxLength = 0;
				colvarBookingId.AutoIncrement = true;
				colvarBookingId.IsNullable = false;
				colvarBookingId.IsPrimaryKey = true;
				colvarBookingId.IsForeignKey = false;
				colvarBookingId.IsReadOnly = false;
				colvarBookingId.DefaultSetting = @"";
				colvarBookingId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBookingId);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = false;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarBookingType = new TableSchema.TableColumn(schema);
				colvarBookingType.ColumnName = "booking_type";
				colvarBookingType.DataType = DbType.Int32;
				colvarBookingType.MaxLength = 0;
				colvarBookingType.AutoIncrement = false;
				colvarBookingType.IsNullable = false;
				colvarBookingType.IsPrimaryKey = false;
				colvarBookingType.IsForeignKey = false;
				colvarBookingType.IsReadOnly = false;
				colvarBookingType.DefaultSetting = @"";
				colvarBookingType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBookingType);
				
				TableSchema.TableColumn colvarSun = new TableSchema.TableColumn(schema);
				colvarSun.ColumnName = "SUN";
				colvarSun.DataType = DbType.Boolean;
				colvarSun.MaxLength = 0;
				colvarSun.AutoIncrement = false;
				colvarSun.IsNullable = false;
				colvarSun.IsPrimaryKey = false;
				colvarSun.IsForeignKey = false;
				colvarSun.IsReadOnly = false;
				
						colvarSun.DefaultSetting = @"((0))";
				colvarSun.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSun);
				
				TableSchema.TableColumn colvarMon = new TableSchema.TableColumn(schema);
				colvarMon.ColumnName = "MON";
				colvarMon.DataType = DbType.Boolean;
				colvarMon.MaxLength = 0;
				colvarMon.AutoIncrement = false;
				colvarMon.IsNullable = false;
				colvarMon.IsPrimaryKey = false;
				colvarMon.IsForeignKey = false;
				colvarMon.IsReadOnly = false;
				
						colvarMon.DefaultSetting = @"((0))";
				colvarMon.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMon);
				
				TableSchema.TableColumn colvarTue = new TableSchema.TableColumn(schema);
				colvarTue.ColumnName = "TUE";
				colvarTue.DataType = DbType.Boolean;
				colvarTue.MaxLength = 0;
				colvarTue.AutoIncrement = false;
				colvarTue.IsNullable = false;
				colvarTue.IsPrimaryKey = false;
				colvarTue.IsForeignKey = false;
				colvarTue.IsReadOnly = false;
				
						colvarTue.DefaultSetting = @"((0))";
				colvarTue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTue);
				
				TableSchema.TableColumn colvarWed = new TableSchema.TableColumn(schema);
				colvarWed.ColumnName = "WED";
				colvarWed.DataType = DbType.Boolean;
				colvarWed.MaxLength = 0;
				colvarWed.AutoIncrement = false;
				colvarWed.IsNullable = false;
				colvarWed.IsPrimaryKey = false;
				colvarWed.IsForeignKey = false;
				colvarWed.IsReadOnly = false;
				
						colvarWed.DefaultSetting = @"((0))";
				colvarWed.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWed);
				
				TableSchema.TableColumn colvarThu = new TableSchema.TableColumn(schema);
				colvarThu.ColumnName = "THU";
				colvarThu.DataType = DbType.Boolean;
				colvarThu.MaxLength = 0;
				colvarThu.AutoIncrement = false;
				colvarThu.IsNullable = false;
				colvarThu.IsPrimaryKey = false;
				colvarThu.IsForeignKey = false;
				colvarThu.IsReadOnly = false;
				
						colvarThu.DefaultSetting = @"((0))";
				colvarThu.ForeignKeyTableName = "";
				schema.Columns.Add(colvarThu);
				
				TableSchema.TableColumn colvarFri = new TableSchema.TableColumn(schema);
				colvarFri.ColumnName = "FRI";
				colvarFri.DataType = DbType.Boolean;
				colvarFri.MaxLength = 0;
				colvarFri.AutoIncrement = false;
				colvarFri.IsNullable = false;
				colvarFri.IsPrimaryKey = false;
				colvarFri.IsForeignKey = false;
				colvarFri.IsReadOnly = false;
				
						colvarFri.DefaultSetting = @"((0))";
				colvarFri.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFri);
				
				TableSchema.TableColumn colvarSat = new TableSchema.TableColumn(schema);
				colvarSat.ColumnName = "SAT";
				colvarSat.DataType = DbType.Boolean;
				colvarSat.MaxLength = 0;
				colvarSat.AutoIncrement = false;
				colvarSat.IsNullable = false;
				colvarSat.IsPrimaryKey = false;
				colvarSat.IsForeignKey = false;
				colvarSat.IsReadOnly = false;
				
						colvarSat.DefaultSetting = @"((0))";
				colvarSat.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSat);
				
				TableSchema.TableColumn colvarBookingOpeningTimeS = new TableSchema.TableColumn(schema);
				colvarBookingOpeningTimeS.ColumnName = "booking_opening_time_s";
				colvarBookingOpeningTimeS.DataType = DbType.DateTime;
				colvarBookingOpeningTimeS.MaxLength = 0;
				colvarBookingOpeningTimeS.AutoIncrement = false;
				colvarBookingOpeningTimeS.IsNullable = false;
				colvarBookingOpeningTimeS.IsPrimaryKey = false;
				colvarBookingOpeningTimeS.IsForeignKey = false;
				colvarBookingOpeningTimeS.IsReadOnly = false;
				colvarBookingOpeningTimeS.DefaultSetting = @"";
				colvarBookingOpeningTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBookingOpeningTimeS);
				
				TableSchema.TableColumn colvarBookingOpeningTimeE = new TableSchema.TableColumn(schema);
				colvarBookingOpeningTimeE.ColumnName = "booking_opening_time_e";
				colvarBookingOpeningTimeE.DataType = DbType.DateTime;
				colvarBookingOpeningTimeE.MaxLength = 0;
				colvarBookingOpeningTimeE.AutoIncrement = false;
				colvarBookingOpeningTimeE.IsNullable = false;
				colvarBookingOpeningTimeE.IsPrimaryKey = false;
				colvarBookingOpeningTimeE.IsForeignKey = false;
				colvarBookingOpeningTimeE.IsReadOnly = false;
				colvarBookingOpeningTimeE.DefaultSetting = @"";
				colvarBookingOpeningTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBookingOpeningTimeE);
				
				TableSchema.TableColumn colvarTimeSpan = new TableSchema.TableColumn(schema);
				colvarTimeSpan.ColumnName = "time_span";
				colvarTimeSpan.DataType = DbType.Int32;
				colvarTimeSpan.MaxLength = 0;
				colvarTimeSpan.AutoIncrement = false;
				colvarTimeSpan.IsNullable = false;
				colvarTimeSpan.IsPrimaryKey = false;
				colvarTimeSpan.IsForeignKey = false;
				colvarTimeSpan.IsReadOnly = false;
				
						colvarTimeSpan.DefaultSetting = @"((30))";
				colvarTimeSpan.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTimeSpan);
				
				TableSchema.TableColumn colvarDefaultMaxNumberOfPeople = new TableSchema.TableColumn(schema);
				colvarDefaultMaxNumberOfPeople.ColumnName = "default_max_number_of_people";
				colvarDefaultMaxNumberOfPeople.DataType = DbType.Int32;
				colvarDefaultMaxNumberOfPeople.MaxLength = 0;
				colvarDefaultMaxNumberOfPeople.AutoIncrement = false;
				colvarDefaultMaxNumberOfPeople.IsNullable = false;
				colvarDefaultMaxNumberOfPeople.IsPrimaryKey = false;
				colvarDefaultMaxNumberOfPeople.IsForeignKey = false;
				colvarDefaultMaxNumberOfPeople.IsReadOnly = false;
				
						colvarDefaultMaxNumberOfPeople.DefaultSetting = @"((0))";
				colvarDefaultMaxNumberOfPeople.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDefaultMaxNumberOfPeople);
				
				TableSchema.TableColumn colvarIsDetailedSetting = new TableSchema.TableColumn(schema);
				colvarIsDetailedSetting.ColumnName = "is_detailed_setting";
				colvarIsDetailedSetting.DataType = DbType.Boolean;
				colvarIsDetailedSetting.MaxLength = 0;
				colvarIsDetailedSetting.AutoIncrement = false;
				colvarIsDetailedSetting.IsNullable = false;
				colvarIsDetailedSetting.IsPrimaryKey = false;
				colvarIsDetailedSetting.IsForeignKey = false;
				colvarIsDetailedSetting.IsReadOnly = false;
				
						colvarIsDetailedSetting.DefaultSetting = @"((0))";
				colvarIsDetailedSetting.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDetailedSetting);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("booking_system_store",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("BookingId")]
		[Bindable(true)]
		public int BookingId 
		{
			get { return GetColumnValue<int>(Columns.BookingId); }
			set { SetColumnValue(Columns.BookingId, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid StoreGuid 
		{
			get { return GetColumnValue<Guid>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("BookingType")]
		[Bindable(true)]
		public int BookingType 
		{
			get { return GetColumnValue<int>(Columns.BookingType); }
			set { SetColumnValue(Columns.BookingType, value); }
		}
		  
		[XmlAttribute("Sun")]
		[Bindable(true)]
		public bool Sun 
		{
			get { return GetColumnValue<bool>(Columns.Sun); }
			set { SetColumnValue(Columns.Sun, value); }
		}
		  
		[XmlAttribute("Mon")]
		[Bindable(true)]
		public bool Mon 
		{
			get { return GetColumnValue<bool>(Columns.Mon); }
			set { SetColumnValue(Columns.Mon, value); }
		}
		  
		[XmlAttribute("Tue")]
		[Bindable(true)]
		public bool Tue 
		{
			get { return GetColumnValue<bool>(Columns.Tue); }
			set { SetColumnValue(Columns.Tue, value); }
		}
		  
		[XmlAttribute("Wed")]
		[Bindable(true)]
		public bool Wed 
		{
			get { return GetColumnValue<bool>(Columns.Wed); }
			set { SetColumnValue(Columns.Wed, value); }
		}
		  
		[XmlAttribute("Thu")]
		[Bindable(true)]
		public bool Thu 
		{
			get { return GetColumnValue<bool>(Columns.Thu); }
			set { SetColumnValue(Columns.Thu, value); }
		}
		  
		[XmlAttribute("Fri")]
		[Bindable(true)]
		public bool Fri 
		{
			get { return GetColumnValue<bool>(Columns.Fri); }
			set { SetColumnValue(Columns.Fri, value); }
		}
		  
		[XmlAttribute("Sat")]
		[Bindable(true)]
		public bool Sat 
		{
			get { return GetColumnValue<bool>(Columns.Sat); }
			set { SetColumnValue(Columns.Sat, value); }
		}
		  
		[XmlAttribute("BookingOpeningTimeS")]
		[Bindable(true)]
		public DateTime BookingOpeningTimeS 
		{
			get { return GetColumnValue<DateTime>(Columns.BookingOpeningTimeS); }
			set { SetColumnValue(Columns.BookingOpeningTimeS, value); }
		}
		  
		[XmlAttribute("BookingOpeningTimeE")]
		[Bindable(true)]
		public DateTime BookingOpeningTimeE 
		{
			get { return GetColumnValue<DateTime>(Columns.BookingOpeningTimeE); }
			set { SetColumnValue(Columns.BookingOpeningTimeE, value); }
		}
		  
		[XmlAttribute("TimeSpan")]
		[Bindable(true)]
		public int TimeSpan 
		{
			get { return GetColumnValue<int>(Columns.TimeSpan); }
			set { SetColumnValue(Columns.TimeSpan, value); }
		}
		  
		[XmlAttribute("DefaultMaxNumberOfPeople")]
		[Bindable(true)]
		public int DefaultMaxNumberOfPeople 
		{
			get { return GetColumnValue<int>(Columns.DefaultMaxNumberOfPeople); }
			set { SetColumnValue(Columns.DefaultMaxNumberOfPeople, value); }
		}
		  
		[XmlAttribute("IsDetailedSetting")]
		[Bindable(true)]
		public bool IsDetailedSetting 
		{
			get { return GetColumnValue<bool>(Columns.IsDetailedSetting); }
			set { SetColumnValue(Columns.IsDetailedSetting, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn BookingIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BookingTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SunColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn MonColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn TueColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn WedColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ThuColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn FriColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SatColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn BookingOpeningTimeSColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn BookingOpeningTimeEColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn TimeSpanColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn DefaultMaxNumberOfPeopleColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDetailedSettingColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string BookingId = @"booking_id";
			 public static string StoreGuid = @"store_guid";
			 public static string BookingType = @"booking_type";
			 public static string Sun = @"SUN";
			 public static string Mon = @"MON";
			 public static string Tue = @"TUE";
			 public static string Wed = @"WED";
			 public static string Thu = @"THU";
			 public static string Fri = @"FRI";
			 public static string Sat = @"SAT";
			 public static string BookingOpeningTimeS = @"booking_opening_time_s";
			 public static string BookingOpeningTimeE = @"booking_opening_time_e";
			 public static string TimeSpan = @"time_span";
			 public static string DefaultMaxNumberOfPeople = @"default_max_number_of_people";
			 public static string IsDetailedSetting = @"is_detailed_setting";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class CustomerServiceMessageCollection : RepositoryList<CustomerServiceMessage, CustomerServiceMessageCollection>
	{
			public CustomerServiceMessageCollection() {}

			public CustomerServiceMessageCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							CustomerServiceMessage o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class CustomerServiceMessage : RepositoryRecord<CustomerServiceMessage>, IRecordBase
	{
		#region .ctors and Default Settings
		public CustomerServiceMessage()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public CustomerServiceMessage(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("customer_service_message", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarServiceNo = new TableSchema.TableColumn(schema);
				colvarServiceNo.ColumnName = "service_no";
				colvarServiceNo.DataType = DbType.AnsiString;
				colvarServiceNo.MaxLength = 50;
				colvarServiceNo.AutoIncrement = false;
				colvarServiceNo.IsNullable = false;
				colvarServiceNo.IsPrimaryKey = true;
				colvarServiceNo.IsForeignKey = false;
				colvarServiceNo.IsReadOnly = false;
				colvarServiceNo.DefaultSetting = @"";
				colvarServiceNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceNo);

				TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
				colvarCategory.ColumnName = "category";
				colvarCategory.DataType = DbType.Int32;
				colvarCategory.MaxLength = 0;
				colvarCategory.AutoIncrement = false;
				colvarCategory.IsNullable = false;
				colvarCategory.IsPrimaryKey = false;
				colvarCategory.IsForeignKey = false;
				colvarCategory.IsReadOnly = false;
				colvarCategory.DefaultSetting = @"";
				colvarCategory.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategory);

				TableSchema.TableColumn colvarSubCategory = new TableSchema.TableColumn(schema);
				colvarSubCategory.ColumnName = "sub_category";
				colvarSubCategory.DataType = DbType.Int32;
				colvarSubCategory.MaxLength = 0;
				colvarSubCategory.AutoIncrement = false;
				colvarSubCategory.IsNullable = false;
				colvarSubCategory.IsPrimaryKey = false;
				colvarSubCategory.IsForeignKey = false;
				colvarSubCategory.IsReadOnly = false;
				colvarSubCategory.DefaultSetting = @"";
				colvarSubCategory.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategory);

				TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
				colvarPhone.ColumnName = "phone";
				colvarPhone.DataType = DbType.AnsiString;
				colvarPhone.MaxLength = 50;
				colvarPhone.AutoIncrement = false;
				colvarPhone.IsNullable = true;
				colvarPhone.IsPrimaryKey = false;
				colvarPhone.IsForeignKey = false;
				colvarPhone.IsReadOnly = false;
				colvarPhone.DefaultSetting = @"";
				colvarPhone.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhone);

				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 50;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = true;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);

				TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
				colvarEmail.ColumnName = "email";
				colvarEmail.DataType = DbType.AnsiString;
				colvarEmail.MaxLength = 100;
				colvarEmail.AutoIncrement = false;
				colvarEmail.IsNullable = true;
				colvarEmail.IsPrimaryKey = false;
				colvarEmail.IsForeignKey = false;
				colvarEmail.IsReadOnly = false;
				colvarEmail.DefaultSetting = @"";
				colvarEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmail);

				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 100;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.Int32;
				colvarModifyId.MaxLength = 0;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarMessageType = new TableSchema.TableColumn(schema);
				colvarMessageType.ColumnName = "message_type";
				colvarMessageType.DataType = DbType.Int32;
				colvarMessageType.MaxLength = 0;
				colvarMessageType.AutoIncrement = false;
				colvarMessageType.IsNullable = false;
				colvarMessageType.IsPrimaryKey = false;
				colvarMessageType.IsForeignKey = false;
				colvarMessageType.IsReadOnly = false;
				colvarMessageType.DefaultSetting = @"";
				colvarMessageType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessageType);

				TableSchema.TableColumn colvarWorkUser = new TableSchema.TableColumn(schema);
				colvarWorkUser.ColumnName = "work_user";
				colvarWorkUser.DataType = DbType.Int32;
				colvarWorkUser.MaxLength = 0;
				colvarWorkUser.AutoIncrement = false;
				colvarWorkUser.IsNullable = true;
				colvarWorkUser.IsPrimaryKey = false;
				colvarWorkUser.IsForeignKey = false;
				colvarWorkUser.IsReadOnly = false;
				colvarWorkUser.DefaultSetting = @"";
				colvarWorkUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWorkUser);

				TableSchema.TableColumn colvarPriority = new TableSchema.TableColumn(schema);
				colvarPriority.ColumnName = "priority";
				colvarPriority.DataType = DbType.Int32;
				colvarPriority.MaxLength = 0;
				colvarPriority.AutoIncrement = false;
				colvarPriority.IsNullable = false;
				colvarPriority.IsPrimaryKey = false;
				colvarPriority.IsForeignKey = false;
				colvarPriority.IsReadOnly = false;
				colvarPriority.DefaultSetting = @"";
				colvarPriority.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPriority);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = true;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarSource = new TableSchema.TableColumn(schema);
				colvarSource.ColumnName = "source";
				colvarSource.DataType = DbType.Int32;
				colvarSource.MaxLength = 0;
				colvarSource.AutoIncrement = false;
				colvarSource.IsNullable = false;
				colvarSource.IsPrimaryKey = false;
				colvarSource.IsForeignKey = false;
				colvarSource.IsReadOnly = false;
				colvarSource.DefaultSetting = @"((0))";
				colvarSource.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSource);

				TableSchema.TableColumn colvarCoWorkUser = new TableSchema.TableColumn(schema);
				colvarCoWorkUser.ColumnName = "co_work_user";
				colvarCoWorkUser.DataType = DbType.Int32;
				colvarCoWorkUser.MaxLength = 0;
				colvarCoWorkUser.AutoIncrement = false;
				colvarCoWorkUser.IsNullable = true;
				colvarCoWorkUser.IsPrimaryKey = false;
				colvarCoWorkUser.IsForeignKey = false;
				colvarCoWorkUser.IsReadOnly = false;
				colvarCoWorkUser.DefaultSetting = @"";
				colvarCoWorkUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCoWorkUser);

				TableSchema.TableColumn colvarParentServiceNo = new TableSchema.TableColumn(schema);
				colvarParentServiceNo.ColumnName = "parent_service_no";
				colvarParentServiceNo.DataType = DbType.AnsiString;
				colvarParentServiceNo.MaxLength = 50;
				colvarParentServiceNo.AutoIncrement = false;
				colvarParentServiceNo.IsNullable = true;
				colvarParentServiceNo.IsPrimaryKey = false;
				colvarParentServiceNo.IsForeignKey = false;
				colvarParentServiceNo.IsReadOnly = false;
				colvarParentServiceNo.DefaultSetting = @"";
				colvarParentServiceNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentServiceNo);

				TableSchema.TableColumn colvarTsMemberNo = new TableSchema.TableColumn(schema);
				colvarTsMemberNo.ColumnName = "ts_member_no";
				colvarTsMemberNo.DataType = DbType.AnsiString;
				colvarTsMemberNo.MaxLength = 80;
				colvarTsMemberNo.AutoIncrement = false;
				colvarTsMemberNo.IsNullable = true;
				colvarTsMemberNo.IsPrimaryKey = false;
				colvarTsMemberNo.IsForeignKey = false;
				colvarTsMemberNo.IsReadOnly = false;
				colvarTsMemberNo.DefaultSetting = @"";
				colvarTsMemberNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTsMemberNo);

				TableSchema.TableColumn colvarIssueFromType = new TableSchema.TableColumn(schema);
				colvarIssueFromType.ColumnName = "issue_from_type";
				colvarIssueFromType.DataType = DbType.Int32;
				colvarIssueFromType.MaxLength = 0;
				colvarIssueFromType.AutoIncrement = false;
				colvarIssueFromType.IsNullable = false;
				colvarIssueFromType.IsPrimaryKey = false;
				colvarIssueFromType.IsForeignKey = false;
				colvarIssueFromType.IsReadOnly = false;
				colvarIssueFromType.DefaultSetting = @"((0))";
				colvarIssueFromType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIssueFromType);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("customer_service_message",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("ServiceNo")]
		[Bindable(true)]
		public string ServiceNo
		{
			get { return GetColumnValue<string>(Columns.ServiceNo); }
			set { SetColumnValue(Columns.ServiceNo, value); }
		}

		[XmlAttribute("Category")]
		[Bindable(true)]
		public int Category
		{
			get { return GetColumnValue<int>(Columns.Category); }
			set { SetColumnValue(Columns.Category, value); }
		}

		[XmlAttribute("SubCategory")]
		[Bindable(true)]
		public int SubCategory
		{
			get { return GetColumnValue<int>(Columns.SubCategory); }
			set { SetColumnValue(Columns.SubCategory, value); }
		}

		[XmlAttribute("Phone")]
		[Bindable(true)]
		public string Phone
		{
			get { return GetColumnValue<string>(Columns.Phone); }
			set { SetColumnValue(Columns.Phone, value); }
		}

		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}

		[XmlAttribute("Email")]
		[Bindable(true)]
		public string Email
		{
			get { return GetColumnValue<string>(Columns.Email); }
			set { SetColumnValue(Columns.Email, value); }
		}

		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public int? ModifyId
		{
			get { return GetColumnValue<int?>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("MessageType")]
		[Bindable(true)]
		public int MessageType
		{
			get { return GetColumnValue<int>(Columns.MessageType); }
			set { SetColumnValue(Columns.MessageType, value); }
		}

		[XmlAttribute("WorkUser")]
		[Bindable(true)]
		public int? WorkUser
		{
			get { return GetColumnValue<int?>(Columns.WorkUser); }
			set { SetColumnValue(Columns.WorkUser, value); }
		}

		[XmlAttribute("Priority")]
		[Bindable(true)]
		public int Priority
		{
			get { return GetColumnValue<int>(Columns.Priority); }
			set { SetColumnValue(Columns.Priority, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int? UserId
		{
			get { return GetColumnValue<int?>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("Source")]
		[Bindable(true)]
		public int Source
		{
			get { return GetColumnValue<int>(Columns.Source); }
			set { SetColumnValue(Columns.Source, value); }
		}

		[XmlAttribute("CoWorkUser")]
		[Bindable(true)]
		public int? CoWorkUser
		{
			get { return GetColumnValue<int?>(Columns.CoWorkUser); }
			set { SetColumnValue(Columns.CoWorkUser, value); }
		}

		[XmlAttribute("ParentServiceNo")]
		[Bindable(true)]
		public string ParentServiceNo
		{
			get { return GetColumnValue<string>(Columns.ParentServiceNo); }
			set { SetColumnValue(Columns.ParentServiceNo, value); }
		}

		[XmlAttribute("TsMemberNo")]
		[Bindable(true)]
		public string TsMemberNo
		{
			get { return GetColumnValue<string>(Columns.TsMemberNo); }
			set { SetColumnValue(Columns.TsMemberNo, value); }
		}

		[XmlAttribute("IssueFromType")]
		[Bindable(true)]
		public int IssueFromType
		{
			get { return GetColumnValue<int>(Columns.IssueFromType); }
			set { SetColumnValue(Columns.IssueFromType, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn ServiceNoColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn CategoryColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn SubCategoryColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn PhoneColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn NameColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn EmailColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn StatusColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn ModifyIdColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn MessageTypeColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn WorkUserColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn PriorityColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn SourceColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn CoWorkUserColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn ParentServiceNoColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn TsMemberNoColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn IssueFromTypeColumn
		{
			get { return Schema.Columns[20]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string ServiceNo = @"service_no";
			public static string Category = @"category";
			public static string SubCategory = @"sub_category";
			public static string Phone = @"phone";
			public static string Name = @"name";
			public static string Email = @"email";
			public static string Status = @"status";
			public static string CreateId = @"create_id";
			public static string CreateTime = @"create_time";
			public static string ModifyId = @"modify_id";
			public static string ModifyTime = @"modify_time";
			public static string MessageType = @"message_type";
			public static string WorkUser = @"work_user";
			public static string Priority = @"priority";
			public static string UserId = @"user_id";
			public static string OrderGuid = @"order_guid";
			public static string Source = @"source";
			public static string CoWorkUser = @"co_work_user";
			public static string ParentServiceNo = @"parent_service_no";
			public static string TsMemberNo = @"ts_member_no";
			public static string IssueFromType = @"issue_from_type";
		}

		#endregion

	}
}

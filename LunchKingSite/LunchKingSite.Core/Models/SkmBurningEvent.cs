using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the SkmBurningEvent class.
    /// </summary>
    [Serializable]
    public partial class SkmBurningEventCollection : RepositoryList<SkmBurningEvent, SkmBurningEventCollection>
    {
        public SkmBurningEventCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmBurningEventCollection</returns>
        public SkmBurningEventCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmBurningEvent o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }


    /// <summary>
    /// This is an ActiveRecord class which wraps the skm_burning_event table.
    /// </summary>

    [Serializable]
    public partial class SkmBurningEvent : RepositoryRecord<SkmBurningEvent>, IRecordBase
    {
        #region .ctors and Default Settings

        public SkmBurningEvent()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SkmBurningEvent(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("skm_burning_event", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarExternalGuid = new TableSchema.TableColumn(schema);
                colvarExternalGuid.ColumnName = "external_guid";
                colvarExternalGuid.DataType = DbType.Guid;
                colvarExternalGuid.MaxLength = 0;
                colvarExternalGuid.AutoIncrement = false;
                colvarExternalGuid.IsNullable = false;
                colvarExternalGuid.IsPrimaryKey = false;
                colvarExternalGuid.IsForeignKey = false;
                colvarExternalGuid.IsReadOnly = false;
                colvarExternalGuid.DefaultSetting = @"";
                colvarExternalGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarExternalGuid);

                TableSchema.TableColumn colvarSkmEventId = new TableSchema.TableColumn(schema);
                colvarSkmEventId.ColumnName = "skm_event_id";
                colvarSkmEventId.DataType = DbType.AnsiString;
                colvarSkmEventId.MaxLength = 20;
                colvarSkmEventId.AutoIncrement = false;
                colvarSkmEventId.IsNullable = false;
                colvarSkmEventId.IsPrimaryKey = false;
                colvarSkmEventId.IsForeignKey = false;
                colvarSkmEventId.IsReadOnly = false;
                colvarSkmEventId.DefaultSetting = @"";
                colvarSkmEventId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSkmEventId);

                TableSchema.TableColumn colvarExchangeItemId = new TableSchema.TableColumn(schema);
                colvarExchangeItemId.ColumnName = "exchange_item_id";
                colvarExchangeItemId.DataType = DbType.AnsiString;
                colvarExchangeItemId.MaxLength = 20;
                colvarExchangeItemId.AutoIncrement = false;
                colvarExchangeItemId.IsNullable = false;
                colvarExchangeItemId.IsPrimaryKey = false;
                colvarExchangeItemId.IsForeignKey = false;
                colvarExchangeItemId.IsReadOnly = false;
                colvarExchangeItemId.DefaultSetting = @"";
                colvarExchangeItemId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarExchangeItemId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                colvarCreateTime.DefaultSetting = @"(getdate())";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Byte;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                colvarStatus.DefaultSetting = @"((0))";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarRequestLog = new TableSchema.TableColumn(schema);
                colvarRequestLog.ColumnName = "request_log";
                colvarRequestLog.DataType = DbType.AnsiString;
                colvarRequestLog.MaxLength = -1;
                colvarRequestLog.AutoIncrement = false;
                colvarRequestLog.IsNullable = false;
                colvarRequestLog.IsPrimaryKey = false;
                colvarRequestLog.IsForeignKey = false;
                colvarRequestLog.IsReadOnly = false;

                colvarRequestLog.DefaultSetting = @"('')";
                colvarRequestLog.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRequestLog);

                TableSchema.TableColumn colvarBurningPoint = new TableSchema.TableColumn(schema);
                colvarBurningPoint.ColumnName = "burning_point";
                colvarBurningPoint.DataType = DbType.Int32;
                colvarBurningPoint.MaxLength = 0;
                colvarBurningPoint.AutoIncrement = false;
                colvarBurningPoint.IsNullable = false;
                colvarBurningPoint.IsPrimaryKey = false;
                colvarBurningPoint.IsForeignKey = false;
                colvarBurningPoint.IsReadOnly = false;

                colvarBurningPoint.DefaultSetting = @"((0))";
                colvarBurningPoint.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBurningPoint);

                TableSchema.TableColumn colvarCloseProjectStatus = new TableSchema.TableColumn(schema);
                colvarCloseProjectStatus.ColumnName = "close_project_status";
                colvarCloseProjectStatus.DataType = DbType.Boolean;
                colvarCloseProjectStatus.MaxLength = 0;
                colvarCloseProjectStatus.AutoIncrement = false;
                colvarCloseProjectStatus.IsNullable = false;
                colvarCloseProjectStatus.IsPrimaryKey = false;
                colvarCloseProjectStatus.IsForeignKey = false;
                colvarCloseProjectStatus.IsReadOnly = false;

                colvarCloseProjectStatus.DefaultSetting = @"((0))";
                colvarCloseProjectStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCloseProjectStatus);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;

                colvarModifyTime.DefaultSetting = @"(getdate())";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarSendData = new TableSchema.TableColumn(schema);
                colvarSendData.ColumnName = "send_data";
                colvarSendData.DataType = DbType.String;
                colvarSendData.MaxLength = -1;
                colvarSendData.AutoIncrement = false;
                colvarSendData.IsNullable = true;
                colvarSendData.IsPrimaryKey = false;
                colvarSendData.IsForeignKey = false;
                colvarSendData.IsReadOnly = false;
                colvarSendData.DefaultSetting = @"";
                colvarSendData.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSendData);

                TableSchema.TableColumn colvarReceivedData = new TableSchema.TableColumn(schema);
                colvarReceivedData.ColumnName = "received_data";
                colvarReceivedData.DataType = DbType.String;
                colvarReceivedData.MaxLength = -1;
                colvarReceivedData.AutoIncrement = false;
                colvarReceivedData.IsNullable = true;
                colvarReceivedData.IsPrimaryKey = false;
                colvarReceivedData.IsForeignKey = false;
                colvarReceivedData.IsReadOnly = false;
                colvarReceivedData.DefaultSetting = @"";
                colvarReceivedData.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReceivedData);

                TableSchema.TableColumn colvarReceivedTime = new TableSchema.TableColumn(schema);
                colvarReceivedTime.ColumnName = "received_time";
                colvarReceivedTime.DataType = DbType.DateTime;
                colvarReceivedTime.MaxLength = 0;
                colvarReceivedTime.AutoIncrement = false;
                colvarReceivedTime.IsNullable = true;
                colvarReceivedTime.IsPrimaryKey = false;
                colvarReceivedTime.IsForeignKey = false;
                colvarReceivedTime.IsReadOnly = false;
                colvarReceivedTime.DefaultSetting = @"";
                colvarReceivedTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReceivedTime);

                TableSchema.TableColumn colvarBurningEventSid = new TableSchema.TableColumn(schema);
                colvarBurningEventSid.ColumnName = "burning_event_sid";
                colvarBurningEventSid.DataType = DbType.AnsiString;
                colvarBurningEventSid.MaxLength = 20;
                colvarBurningEventSid.AutoIncrement = false;
                colvarBurningEventSid.IsNullable = false;
                colvarBurningEventSid.IsPrimaryKey = false;
                colvarBurningEventSid.IsForeignKey = false;
                colvarBurningEventSid.IsReadOnly = false;

                colvarBurningEventSid.DefaultSetting = @"('')";
                colvarBurningEventSid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBurningEventSid);

                TableSchema.TableColumn colvarGiftWalletId = new TableSchema.TableColumn(schema);
                colvarGiftWalletId.ColumnName = "gift_wallet_id";
                colvarGiftWalletId.DataType = DbType.AnsiString;
                colvarGiftWalletId.MaxLength = 20;
                colvarGiftWalletId.AutoIncrement = false;
                colvarGiftWalletId.IsNullable = false;
                colvarGiftWalletId.IsPrimaryKey = false;
                colvarGiftWalletId.IsForeignKey = false;
                colvarGiftWalletId.IsReadOnly = false;

                colvarGiftWalletId.DefaultSetting = @"('')";
                colvarGiftWalletId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGiftWalletId);

                TableSchema.TableColumn colvarGiftPoint = new TableSchema.TableColumn(schema);
                colvarGiftPoint.ColumnName = "gift_point";
                colvarGiftPoint.DataType = DbType.Int32;
                colvarGiftPoint.MaxLength = 0;
                colvarGiftPoint.AutoIncrement = false;
                colvarGiftPoint.IsNullable = false;
                colvarGiftPoint.IsPrimaryKey = false;
                colvarGiftPoint.IsForeignKey = false;
                colvarGiftPoint.IsReadOnly = false;

                colvarGiftPoint.DefaultSetting = @"((0))";
                colvarGiftPoint.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGiftPoint);

                TableSchema.TableColumn colvarStoreCode = new TableSchema.TableColumn(schema);
                colvarStoreCode.ColumnName = "store_code";
                colvarStoreCode.DataType = DbType.AnsiString;
                colvarStoreCode.MaxLength = 4;
                colvarStoreCode.AutoIncrement = false;
                colvarStoreCode.IsNullable = false;
                colvarStoreCode.IsPrimaryKey = false;
                colvarStoreCode.IsForeignKey = false;
                colvarStoreCode.IsReadOnly = false;

                colvarStoreCode.DefaultSetting = @"('')";
                colvarStoreCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreCode);

                TableSchema.TableColumn colvarIsCostCenterHq = new TableSchema.TableColumn(schema);
                colvarIsCostCenterHq.ColumnName = "is_cost_center_hq";
                colvarIsCostCenterHq.DataType = DbType.Boolean;
                colvarIsCostCenterHq.MaxLength = 0;
                colvarIsCostCenterHq.AutoIncrement = false;
                colvarIsCostCenterHq.IsNullable = false;
                colvarIsCostCenterHq.IsPrimaryKey = false;
                colvarIsCostCenterHq.IsForeignKey = false;
                colvarIsCostCenterHq.IsReadOnly = false;

                colvarIsCostCenterHq.DefaultSetting = @"((0))";
                colvarIsCostCenterHq.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsCostCenterHq);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("skm_burning_event", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("ExternalGuid")]
        [Bindable(true)]
        public Guid ExternalGuid
        {
            get { return GetColumnValue<Guid>(Columns.ExternalGuid); }
            set { SetColumnValue(Columns.ExternalGuid, value); }
        }

        [XmlAttribute("SkmEventId")]
        [Bindable(true)]
        public string SkmEventId
        {
            get { return GetColumnValue<string>(Columns.SkmEventId); }
            set { SetColumnValue(Columns.SkmEventId, value); }
        }

        [XmlAttribute("ExchangeItemId")]
        [Bindable(true)]
        public string ExchangeItemId
        {
            get { return GetColumnValue<string>(Columns.ExchangeItemId); }
            set { SetColumnValue(Columns.ExchangeItemId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public byte Status
        {
            get { return GetColumnValue<byte>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("RequestLog")]
        [Bindable(true)]
        public string RequestLog
        {
            get { return GetColumnValue<string>(Columns.RequestLog); }
            set { SetColumnValue(Columns.RequestLog, value); }
        }

        [XmlAttribute("BurningPoint")]
        [Bindable(true)]
        public int BurningPoint
        {
            get { return GetColumnValue<int>(Columns.BurningPoint); }
            set { SetColumnValue(Columns.BurningPoint, value); }
        }

        [XmlAttribute("CloseProjectStatus")]
        [Bindable(true)]
        public bool CloseProjectStatus
        {
            get { return GetColumnValue<bool>(Columns.CloseProjectStatus); }
            set { SetColumnValue(Columns.CloseProjectStatus, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime
        {
            get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("SendData")]
        [Bindable(true)]
        public string SendData
        {
            get { return GetColumnValue<string>(Columns.SendData); }
            set { SetColumnValue(Columns.SendData, value); }
        }

        [XmlAttribute("ReceivedData")]
        [Bindable(true)]
        public string ReceivedData
        {
            get { return GetColumnValue<string>(Columns.ReceivedData); }
            set { SetColumnValue(Columns.ReceivedData, value); }
        }

        [XmlAttribute("ReceivedTime")]
        [Bindable(true)]
        public DateTime? ReceivedTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ReceivedTime); }
            set { SetColumnValue(Columns.ReceivedTime, value); }
        }

        [XmlAttribute("BurningEventSid")]
        [Bindable(true)]
        public string BurningEventSid
        {
            get { return GetColumnValue<string>(Columns.BurningEventSid); }
            set { SetColumnValue(Columns.BurningEventSid, value); }
        }

        [XmlAttribute("GiftWalletId")]
        [Bindable(true)]
        public string GiftWalletId
        {
            get { return GetColumnValue<string>(Columns.GiftWalletId); }
            set { SetColumnValue(Columns.GiftWalletId, value); }
        }

        [XmlAttribute("GiftPoint")]
        [Bindable(true)]
        public int GiftPoint
        {
            get { return GetColumnValue<int>(Columns.GiftPoint); }
            set { SetColumnValue(Columns.GiftPoint, value); }
        }

        [XmlAttribute("StoreCode")]
        [Bindable(true)]
        public string StoreCode
        {
            get { return GetColumnValue<string>(Columns.StoreCode); }
            set { SetColumnValue(Columns.StoreCode, value); }
        }

        [XmlAttribute("IsCostCenterHq")]
        [Bindable(true)]
        public bool IsCostCenterHq
        {
            get { return GetColumnValue<bool>(Columns.IsCostCenterHq); }
            set { SetColumnValue(Columns.IsCostCenterHq, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ExternalGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn SkmEventIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn ExchangeItemIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn RequestLogColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn BurningPointColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CloseProjectStatusColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn SendDataColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn ReceivedDataColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn ReceivedTimeColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn BurningEventSidColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn GiftWalletIdColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn GiftPointColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn StoreCodeColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn IsCostCenterHqColumn
        {
            get { return Schema.Columns[17]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string ExternalGuid = @"external_guid";
            public static string SkmEventId = @"skm_event_id";
            public static string ExchangeItemId = @"exchange_item_id";
            public static string CreateTime = @"create_time";
            public static string Status = @"status";
            public static string RequestLog = @"request_log";
            public static string BurningPoint = @"burning_point";
            public static string CloseProjectStatus = @"close_project_status";
            public static string ModifyTime = @"modify_time";
            public static string SendData = @"send_data";
            public static string ReceivedData = @"received_data";
            public static string ReceivedTime = @"received_time";
            public static string BurningEventSid = @"burning_event_sid";
            public static string GiftWalletId = @"gift_wallet_id";
            public static string GiftPoint = @"gift_point";
            public static string StoreCode = @"store_code";
            public static string IsCostCenterHq = @"is_cost_center_hq";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

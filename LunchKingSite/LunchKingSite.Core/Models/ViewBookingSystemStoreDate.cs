using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBookingSystemStoreDate class.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemStoreDateCollection : ReadOnlyList<ViewBookingSystemStoreDate, ViewBookingSystemStoreDateCollection>
    {        
        public ViewBookingSystemStoreDateCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_booking_system_store_date view.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemStoreDate : ReadOnlyRecord<ViewBookingSystemStoreDate>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_booking_system_store_date", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBookingSystemDateId = new TableSchema.TableColumn(schema);
                colvarBookingSystemDateId.ColumnName = "booking_system_date_id";
                colvarBookingSystemDateId.DataType = DbType.Int32;
                colvarBookingSystemDateId.MaxLength = 0;
                colvarBookingSystemDateId.AutoIncrement = false;
                colvarBookingSystemDateId.IsNullable = false;
                colvarBookingSystemDateId.IsPrimaryKey = false;
                colvarBookingSystemDateId.IsForeignKey = false;
                colvarBookingSystemDateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBookingSystemDateId);
                
                TableSchema.TableColumn colvarBookingSystemStoreBookingId = new TableSchema.TableColumn(schema);
                colvarBookingSystemStoreBookingId.ColumnName = "booking_system_store_booking_id";
                colvarBookingSystemStoreBookingId.DataType = DbType.Int32;
                colvarBookingSystemStoreBookingId.MaxLength = 0;
                colvarBookingSystemStoreBookingId.AutoIncrement = false;
                colvarBookingSystemStoreBookingId.IsNullable = false;
                colvarBookingSystemStoreBookingId.IsPrimaryKey = false;
                colvarBookingSystemStoreBookingId.IsForeignKey = false;
                colvarBookingSystemStoreBookingId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBookingSystemStoreBookingId);
                
                TableSchema.TableColumn colvarBookingType = new TableSchema.TableColumn(schema);
                colvarBookingType.ColumnName = "booking_type";
                colvarBookingType.DataType = DbType.Int32;
                colvarBookingType.MaxLength = 0;
                colvarBookingType.AutoIncrement = false;
                colvarBookingType.IsNullable = false;
                colvarBookingType.IsPrimaryKey = false;
                colvarBookingType.IsForeignKey = false;
                colvarBookingType.IsReadOnly = false;
                
                schema.Columns.Add(colvarBookingType);
                
                TableSchema.TableColumn colvarSun = new TableSchema.TableColumn(schema);
                colvarSun.ColumnName = "SUN";
                colvarSun.DataType = DbType.Boolean;
                colvarSun.MaxLength = 0;
                colvarSun.AutoIncrement = false;
                colvarSun.IsNullable = false;
                colvarSun.IsPrimaryKey = false;
                colvarSun.IsForeignKey = false;
                colvarSun.IsReadOnly = false;
                
                schema.Columns.Add(colvarSun);
                
                TableSchema.TableColumn colvarMon = new TableSchema.TableColumn(schema);
                colvarMon.ColumnName = "MON";
                colvarMon.DataType = DbType.Boolean;
                colvarMon.MaxLength = 0;
                colvarMon.AutoIncrement = false;
                colvarMon.IsNullable = false;
                colvarMon.IsPrimaryKey = false;
                colvarMon.IsForeignKey = false;
                colvarMon.IsReadOnly = false;
                
                schema.Columns.Add(colvarMon);
                
                TableSchema.TableColumn colvarTue = new TableSchema.TableColumn(schema);
                colvarTue.ColumnName = "TUE";
                colvarTue.DataType = DbType.Boolean;
                colvarTue.MaxLength = 0;
                colvarTue.AutoIncrement = false;
                colvarTue.IsNullable = false;
                colvarTue.IsPrimaryKey = false;
                colvarTue.IsForeignKey = false;
                colvarTue.IsReadOnly = false;
                
                schema.Columns.Add(colvarTue);
                
                TableSchema.TableColumn colvarWed = new TableSchema.TableColumn(schema);
                colvarWed.ColumnName = "WED";
                colvarWed.DataType = DbType.Boolean;
                colvarWed.MaxLength = 0;
                colvarWed.AutoIncrement = false;
                colvarWed.IsNullable = false;
                colvarWed.IsPrimaryKey = false;
                colvarWed.IsForeignKey = false;
                colvarWed.IsReadOnly = false;
                
                schema.Columns.Add(colvarWed);
                
                TableSchema.TableColumn colvarThu = new TableSchema.TableColumn(schema);
                colvarThu.ColumnName = "THU";
                colvarThu.DataType = DbType.Boolean;
                colvarThu.MaxLength = 0;
                colvarThu.AutoIncrement = false;
                colvarThu.IsNullable = false;
                colvarThu.IsPrimaryKey = false;
                colvarThu.IsForeignKey = false;
                colvarThu.IsReadOnly = false;
                
                schema.Columns.Add(colvarThu);
                
                TableSchema.TableColumn colvarFri = new TableSchema.TableColumn(schema);
                colvarFri.ColumnName = "FRI";
                colvarFri.DataType = DbType.Boolean;
                colvarFri.MaxLength = 0;
                colvarFri.AutoIncrement = false;
                colvarFri.IsNullable = false;
                colvarFri.IsPrimaryKey = false;
                colvarFri.IsForeignKey = false;
                colvarFri.IsReadOnly = false;
                
                schema.Columns.Add(colvarFri);
                
                TableSchema.TableColumn colvarSat = new TableSchema.TableColumn(schema);
                colvarSat.ColumnName = "SAT";
                colvarSat.DataType = DbType.Boolean;
                colvarSat.MaxLength = 0;
                colvarSat.AutoIncrement = false;
                colvarSat.IsNullable = false;
                colvarSat.IsPrimaryKey = false;
                colvarSat.IsForeignKey = false;
                colvarSat.IsReadOnly = false;
                
                schema.Columns.Add(colvarSat);
                
                TableSchema.TableColumn colvarDayType = new TableSchema.TableColumn(schema);
                colvarDayType.ColumnName = "day_type";
                colvarDayType.DataType = DbType.Int32;
                colvarDayType.MaxLength = 0;
                colvarDayType.AutoIncrement = false;
                colvarDayType.IsNullable = false;
                colvarDayType.IsPrimaryKey = false;
                colvarDayType.IsForeignKey = false;
                colvarDayType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDayType);
                
                TableSchema.TableColumn colvarEffectiveDate = new TableSchema.TableColumn(schema);
                colvarEffectiveDate.ColumnName = "effective_date";
                colvarEffectiveDate.DataType = DbType.DateTime;
                colvarEffectiveDate.MaxLength = 0;
                colvarEffectiveDate.AutoIncrement = false;
                colvarEffectiveDate.IsNullable = true;
                colvarEffectiveDate.IsPrimaryKey = false;
                colvarEffectiveDate.IsForeignKey = false;
                colvarEffectiveDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarEffectiveDate);
                
                TableSchema.TableColumn colvarIsOpening = new TableSchema.TableColumn(schema);
                colvarIsOpening.ColumnName = "is_opening";
                colvarIsOpening.DataType = DbType.Boolean;
                colvarIsOpening.MaxLength = 0;
                colvarIsOpening.AutoIncrement = false;
                colvarIsOpening.IsNullable = false;
                colvarIsOpening.IsPrimaryKey = false;
                colvarIsOpening.IsForeignKey = false;
                colvarIsOpening.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsOpening);
                
                TableSchema.TableColumn colvarIsDetailedSetting = new TableSchema.TableColumn(schema);
                colvarIsDetailedSetting.ColumnName = "is_detailed_setting";
                colvarIsDetailedSetting.DataType = DbType.Boolean;
                colvarIsDetailedSetting.MaxLength = 0;
                colvarIsDetailedSetting.AutoIncrement = false;
                colvarIsDetailedSetting.IsNullable = false;
                colvarIsDetailedSetting.IsPrimaryKey = false;
                colvarIsDetailedSetting.IsForeignKey = false;
                colvarIsDetailedSetting.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsDetailedSetting);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_booking_system_store_date",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBookingSystemStoreDate()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBookingSystemStoreDate(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBookingSystemStoreDate(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBookingSystemStoreDate(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BookingSystemDateId")]
        [Bindable(true)]
        public int BookingSystemDateId 
	    {
		    get
		    {
			    return GetColumnValue<int>("booking_system_date_id");
		    }
            set 
		    {
			    SetColumnValue("booking_system_date_id", value);
            }
        }
	      
        [XmlAttribute("BookingSystemStoreBookingId")]
        [Bindable(true)]
        public int BookingSystemStoreBookingId 
	    {
		    get
		    {
			    return GetColumnValue<int>("booking_system_store_booking_id");
		    }
            set 
		    {
			    SetColumnValue("booking_system_store_booking_id", value);
            }
        }
	      
        [XmlAttribute("BookingType")]
        [Bindable(true)]
        public int BookingType 
	    {
		    get
		    {
			    return GetColumnValue<int>("booking_type");
		    }
            set 
		    {
			    SetColumnValue("booking_type", value);
            }
        }
	      
        [XmlAttribute("Sun")]
        [Bindable(true)]
        public bool Sun 
	    {
		    get
		    {
			    return GetColumnValue<bool>("SUN");
		    }
            set 
		    {
			    SetColumnValue("SUN", value);
            }
        }
	      
        [XmlAttribute("Mon")]
        [Bindable(true)]
        public bool Mon 
	    {
		    get
		    {
			    return GetColumnValue<bool>("MON");
		    }
            set 
		    {
			    SetColumnValue("MON", value);
            }
        }
	      
        [XmlAttribute("Tue")]
        [Bindable(true)]
        public bool Tue 
	    {
		    get
		    {
			    return GetColumnValue<bool>("TUE");
		    }
            set 
		    {
			    SetColumnValue("TUE", value);
            }
        }
	      
        [XmlAttribute("Wed")]
        [Bindable(true)]
        public bool Wed 
	    {
		    get
		    {
			    return GetColumnValue<bool>("WED");
		    }
            set 
		    {
			    SetColumnValue("WED", value);
            }
        }
	      
        [XmlAttribute("Thu")]
        [Bindable(true)]
        public bool Thu 
	    {
		    get
		    {
			    return GetColumnValue<bool>("THU");
		    }
            set 
		    {
			    SetColumnValue("THU", value);
            }
        }
	      
        [XmlAttribute("Fri")]
        [Bindable(true)]
        public bool Fri 
	    {
		    get
		    {
			    return GetColumnValue<bool>("FRI");
		    }
            set 
		    {
			    SetColumnValue("FRI", value);
            }
        }
	      
        [XmlAttribute("Sat")]
        [Bindable(true)]
        public bool Sat 
	    {
		    get
		    {
			    return GetColumnValue<bool>("SAT");
		    }
            set 
		    {
			    SetColumnValue("SAT", value);
            }
        }
	      
        [XmlAttribute("DayType")]
        [Bindable(true)]
        public int DayType 
	    {
		    get
		    {
			    return GetColumnValue<int>("day_type");
		    }
            set 
		    {
			    SetColumnValue("day_type", value);
            }
        }
	      
        [XmlAttribute("EffectiveDate")]
        [Bindable(true)]
        public DateTime? EffectiveDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("effective_date");
		    }
            set 
		    {
			    SetColumnValue("effective_date", value);
            }
        }
	      
        [XmlAttribute("IsOpening")]
        [Bindable(true)]
        public bool IsOpening 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_opening");
		    }
            set 
		    {
			    SetColumnValue("is_opening", value);
            }
        }
	      
        [XmlAttribute("IsDetailedSetting")]
        [Bindable(true)]
        public bool IsDetailedSetting 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_detailed_setting");
		    }
            set 
		    {
			    SetColumnValue("is_detailed_setting", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BookingSystemDateId = @"booking_system_date_id";
            
            public static string BookingSystemStoreBookingId = @"booking_system_store_booking_id";
            
            public static string BookingType = @"booking_type";
            
            public static string Sun = @"SUN";
            
            public static string Mon = @"MON";
            
            public static string Tue = @"TUE";
            
            public static string Wed = @"WED";
            
            public static string Thu = @"THU";
            
            public static string Fri = @"FRI";
            
            public static string Sat = @"SAT";
            
            public static string DayType = @"day_type";
            
            public static string EffectiveDate = @"effective_date";
            
            public static string IsOpening = @"is_opening";
            
            public static string IsDetailedSetting = @"is_detailed_setting";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpPointWithdrawalOverall class.
	/// </summary>
    [Serializable]
	public partial class PcpPointWithdrawalOverallCollection : RepositoryList<PcpPointWithdrawalOverall, PcpPointWithdrawalOverallCollection>
	{	   
		public PcpPointWithdrawalOverallCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpPointWithdrawalOverallCollection</returns>
		public PcpPointWithdrawalOverallCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpPointWithdrawalOverall o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_point_withdrawal_overall table.
	/// </summary>
	[Serializable]
	public partial class PcpPointWithdrawalOverall : RepositoryRecord<PcpPointWithdrawalOverall>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpPointWithdrawalOverall()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpPointWithdrawalOverall(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_point_withdrawal_overall", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Byte;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				
						colvarOrderGuid.DefaultSetting = @"(CONVERT([uniqueidentifier],CONVERT([binary],(0),(0)),(0)))";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarAssignmentId = new TableSchema.TableColumn(schema);
				colvarAssignmentId.ColumnName = "assignment_id";
				colvarAssignmentId.DataType = DbType.Int32;
				colvarAssignmentId.MaxLength = 0;
				colvarAssignmentId.AutoIncrement = false;
				colvarAssignmentId.IsNullable = true;
				colvarAssignmentId.IsPrimaryKey = false;
				colvarAssignmentId.IsForeignKey = false;
				colvarAssignmentId.IsReadOnly = false;
				
						colvarAssignmentId.DefaultSetting = @"((0))";
				colvarAssignmentId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAssignmentId);
				
				TableSchema.TableColumn colvarPoint = new TableSchema.TableColumn(schema);
				colvarPoint.ColumnName = "point";
				colvarPoint.DataType = DbType.Int32;
				colvarPoint.MaxLength = 0;
				colvarPoint.AutoIncrement = false;
				colvarPoint.IsNullable = false;
				colvarPoint.IsPrimaryKey = false;
				colvarPoint.IsForeignKey = false;
				colvarPoint.IsReadOnly = false;
				
						colvarPoint.DefaultSetting = @"((0))";
				colvarPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPoint);
				
				TableSchema.TableColumn colvarPointType = new TableSchema.TableColumn(schema);
				colvarPointType.ColumnName = "point_type";
				colvarPointType.DataType = DbType.Byte;
				colvarPointType.MaxLength = 0;
				colvarPointType.AutoIncrement = false;
				colvarPointType.IsNullable = false;
				colvarPointType.IsPrimaryKey = false;
				colvarPointType.IsForeignKey = false;
				colvarPointType.IsReadOnly = false;
				
						colvarPointType.DefaultSetting = @"((0))";
				colvarPointType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPointType);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				
						colvarUserId.DefaultSetting = @"((0))";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				
						colvarCreateId.DefaultSetting = @"((0))";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
				colvarOrderClassification.ColumnName = "order_classification";
				colvarOrderClassification.DataType = DbType.Int32;
				colvarOrderClassification.MaxLength = 0;
				colvarOrderClassification.AutoIncrement = false;
				colvarOrderClassification.IsNullable = true;
				colvarOrderClassification.IsPrimaryKey = false;
				colvarOrderClassification.IsForeignKey = false;
				colvarOrderClassification.IsReadOnly = false;
				colvarOrderClassification.DefaultSetting = @"";
				colvarOrderClassification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderClassification);
				
				TableSchema.TableColumn colvarDiscountCampaignId = new TableSchema.TableColumn(schema);
				colvarDiscountCampaignId.ColumnName = "discount_campaign_id";
				colvarDiscountCampaignId.DataType = DbType.Int32;
				colvarDiscountCampaignId.MaxLength = 0;
				colvarDiscountCampaignId.AutoIncrement = false;
				colvarDiscountCampaignId.IsNullable = true;
				colvarDiscountCampaignId.IsPrimaryKey = false;
				colvarDiscountCampaignId.IsForeignKey = false;
				colvarDiscountCampaignId.IsReadOnly = false;
				colvarDiscountCampaignId.DefaultSetting = @"";
				colvarDiscountCampaignId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountCampaignId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_point_withdrawal_overall",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public byte Type 
		{
			get { return GetColumnValue<byte>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("AssignmentId")]
		[Bindable(true)]
		public int? AssignmentId 
		{
			get { return GetColumnValue<int?>(Columns.AssignmentId); }
			set { SetColumnValue(Columns.AssignmentId, value); }
		}
		  
		[XmlAttribute("Point")]
		[Bindable(true)]
		public int Point 
		{
			get { return GetColumnValue<int>(Columns.Point); }
			set { SetColumnValue(Columns.Point, value); }
		}
		  
		[XmlAttribute("PointType")]
		[Bindable(true)]
		public byte PointType 
		{
			get { return GetColumnValue<byte>(Columns.PointType); }
			set { SetColumnValue(Columns.PointType, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("OrderClassification")]
		[Bindable(true)]
		public int? OrderClassification 
		{
			get { return GetColumnValue<int?>(Columns.OrderClassification); }
			set { SetColumnValue(Columns.OrderClassification, value); }
		}
		  
		[XmlAttribute("DiscountCampaignId")]
		[Bindable(true)]
		public int? DiscountCampaignId 
		{
			get { return GetColumnValue<int?>(Columns.DiscountCampaignId); }
			set { SetColumnValue(Columns.DiscountCampaignId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AssignmentIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PointColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PointTypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderClassificationColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountCampaignIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Type = @"type";
			 public static string OrderGuid = @"order_guid";
			 public static string AssignmentId = @"assignment_id";
			 public static string Point = @"point";
			 public static string PointType = @"point_type";
			 public static string UserId = @"user_id";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string OrderClassification = @"order_classification";
			 public static string DiscountCampaignId = @"discount_campaign_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

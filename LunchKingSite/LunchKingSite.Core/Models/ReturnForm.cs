using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ReturnForm class.
	/// </summary>
    [Serializable]
	public partial class ReturnFormCollection : RepositoryList<ReturnForm, ReturnFormCollection>
	{	   
		public ReturnFormCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ReturnFormCollection</returns>
		public ReturnFormCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ReturnForm o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the return_form table.
	/// </summary>
	[Serializable]
	public partial class ReturnForm : RepositoryRecord<ReturnForm>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ReturnForm()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ReturnForm(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("return_form", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = false;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarBusinessModel = new TableSchema.TableColumn(schema);
				colvarBusinessModel.ColumnName = "business_model";
				colvarBusinessModel.DataType = DbType.Int32;
				colvarBusinessModel.MaxLength = 0;
				colvarBusinessModel.AutoIncrement = false;
				colvarBusinessModel.IsNullable = false;
				colvarBusinessModel.IsPrimaryKey = false;
				colvarBusinessModel.IsForeignKey = false;
				colvarBusinessModel.IsReadOnly = false;
				colvarBusinessModel.DefaultSetting = @"";
				colvarBusinessModel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessModel);
				
				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = false;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);
				
				TableSchema.TableColumn colvarProgressStatus = new TableSchema.TableColumn(schema);
				colvarProgressStatus.ColumnName = "progress_status";
				colvarProgressStatus.DataType = DbType.Int32;
				colvarProgressStatus.MaxLength = 0;
				colvarProgressStatus.AutoIncrement = false;
				colvarProgressStatus.IsNullable = false;
				colvarProgressStatus.IsPrimaryKey = false;
				colvarProgressStatus.IsForeignKey = false;
				colvarProgressStatus.IsReadOnly = false;
				colvarProgressStatus.DefaultSetting = @"";
				colvarProgressStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProgressStatus);
				
				TableSchema.TableColumn colvarVendorProgressStatus = new TableSchema.TableColumn(schema);
				colvarVendorProgressStatus.ColumnName = "vendor_progress_status";
				colvarVendorProgressStatus.DataType = DbType.Int32;
				colvarVendorProgressStatus.MaxLength = 0;
				colvarVendorProgressStatus.AutoIncrement = false;
				colvarVendorProgressStatus.IsNullable = true;
				colvarVendorProgressStatus.IsPrimaryKey = false;
				colvarVendorProgressStatus.IsForeignKey = false;
				colvarVendorProgressStatus.IsReadOnly = false;
				colvarVendorProgressStatus.DefaultSetting = @"";
				colvarVendorProgressStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorProgressStatus);
				
				TableSchema.TableColumn colvarVendorMemo = new TableSchema.TableColumn(schema);
				colvarVendorMemo.ColumnName = "vendor_memo";
				colvarVendorMemo.DataType = DbType.String;
				colvarVendorMemo.MaxLength = 30;
				colvarVendorMemo.AutoIncrement = false;
				colvarVendorMemo.IsNullable = true;
				colvarVendorMemo.IsPrimaryKey = false;
				colvarVendorMemo.IsForeignKey = false;
				colvarVendorMemo.IsReadOnly = false;
				colvarVendorMemo.DefaultSetting = @"";
				colvarVendorMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorMemo);
				
				TableSchema.TableColumn colvarRefundType = new TableSchema.TableColumn(schema);
				colvarRefundType.ColumnName = "refund_type";
				colvarRefundType.DataType = DbType.Int32;
				colvarRefundType.MaxLength = 0;
				colvarRefundType.AutoIncrement = false;
				colvarRefundType.IsNullable = false;
				colvarRefundType.IsPrimaryKey = false;
				colvarRefundType.IsForeignKey = false;
				colvarRefundType.IsReadOnly = false;
				colvarRefundType.DefaultSetting = @"";
				colvarRefundType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundType);
				
				TableSchema.TableColumn colvarReturnReason = new TableSchema.TableColumn(schema);
				colvarReturnReason.ColumnName = "return_reason";
				colvarReturnReason.DataType = DbType.String;
				colvarReturnReason.MaxLength = 4000;
				colvarReturnReason.AutoIncrement = false;
				colvarReturnReason.IsNullable = true;
				colvarReturnReason.IsPrimaryKey = false;
				colvarReturnReason.IsForeignKey = false;
				colvarReturnReason.IsReadOnly = false;
				colvarReturnReason.DefaultSetting = @"";
				colvarReturnReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnReason);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarIsCreditNoteReceived = new TableSchema.TableColumn(schema);
				colvarIsCreditNoteReceived.ColumnName = "is_credit_note_received";
				colvarIsCreditNoteReceived.DataType = DbType.Boolean;
				colvarIsCreditNoteReceived.MaxLength = 0;
				colvarIsCreditNoteReceived.AutoIncrement = false;
				colvarIsCreditNoteReceived.IsNullable = false;
				colvarIsCreditNoteReceived.IsPrimaryKey = false;
				colvarIsCreditNoteReceived.IsForeignKey = false;
				colvarIsCreditNoteReceived.IsReadOnly = false;
				colvarIsCreditNoteReceived.DefaultSetting = @"";
				colvarIsCreditNoteReceived.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCreditNoteReceived);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 500;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = true;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarRefundProcessMemo = new TableSchema.TableColumn(schema);
				colvarRefundProcessMemo.ColumnName = "refund_process_memo";
				colvarRefundProcessMemo.DataType = DbType.String;
				colvarRefundProcessMemo.MaxLength = 100;
				colvarRefundProcessMemo.AutoIncrement = false;
				colvarRefundProcessMemo.IsNullable = true;
				colvarRefundProcessMemo.IsPrimaryKey = false;
				colvarRefundProcessMemo.IsForeignKey = false;
				colvarRefundProcessMemo.IsReadOnly = false;
				colvarRefundProcessMemo.DefaultSetting = @"";
				colvarRefundProcessMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundProcessMemo);
				
				TableSchema.TableColumn colvarFinishTime = new TableSchema.TableColumn(schema);
				colvarFinishTime.ColumnName = "finish_time";
				colvarFinishTime.DataType = DbType.DateTime;
				colvarFinishTime.MaxLength = 0;
				colvarFinishTime.AutoIncrement = false;
				colvarFinishTime.IsNullable = true;
				colvarFinishTime.IsPrimaryKey = false;
				colvarFinishTime.IsForeignKey = false;
				colvarFinishTime.IsReadOnly = false;
				colvarFinishTime.DefaultSetting = @"";
				colvarFinishTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFinishTime);
				
				TableSchema.TableColumn colvarVendorProcessTime = new TableSchema.TableColumn(schema);
				colvarVendorProcessTime.ColumnName = "vendor_process_time";
				colvarVendorProcessTime.DataType = DbType.DateTime;
				colvarVendorProcessTime.MaxLength = 0;
				colvarVendorProcessTime.AutoIncrement = false;
				colvarVendorProcessTime.IsNullable = true;
				colvarVendorProcessTime.IsPrimaryKey = false;
				colvarVendorProcessTime.IsForeignKey = false;
				colvarVendorProcessTime.IsReadOnly = false;
				colvarVendorProcessTime.DefaultSetting = @"";
				colvarVendorProcessTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorProcessTime);
				
				TableSchema.TableColumn colvarIsSystemRefund = new TableSchema.TableColumn(schema);
				colvarIsSystemRefund.ColumnName = "is_system_refund";
				colvarIsSystemRefund.DataType = DbType.Boolean;
				colvarIsSystemRefund.MaxLength = 0;
				colvarIsSystemRefund.AutoIncrement = false;
				colvarIsSystemRefund.IsNullable = true;
				colvarIsSystemRefund.IsPrimaryKey = false;
				colvarIsSystemRefund.IsForeignKey = false;
				colvarIsSystemRefund.IsReadOnly = false;
				colvarIsSystemRefund.DefaultSetting = @"";
				colvarIsSystemRefund.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSystemRefund);
				
				TableSchema.TableColumn colvarThirdPartyPaymentSystem = new TableSchema.TableColumn(schema);
				colvarThirdPartyPaymentSystem.ColumnName = "third_party_payment_system";
				colvarThirdPartyPaymentSystem.DataType = DbType.Byte;
				colvarThirdPartyPaymentSystem.MaxLength = 0;
				colvarThirdPartyPaymentSystem.AutoIncrement = false;
				colvarThirdPartyPaymentSystem.IsNullable = false;
				colvarThirdPartyPaymentSystem.IsPrimaryKey = false;
				colvarThirdPartyPaymentSystem.IsForeignKey = false;
				colvarThirdPartyPaymentSystem.IsReadOnly = false;
				
						colvarThirdPartyPaymentSystem.DefaultSetting = @"((0))";
				colvarThirdPartyPaymentSystem.ForeignKeyTableName = "";
				schema.Columns.Add(colvarThirdPartyPaymentSystem);
				
				TableSchema.TableColumn colvarOrderFromType = new TableSchema.TableColumn(schema);
				colvarOrderFromType.ColumnName = "order_from_type";
				colvarOrderFromType.DataType = DbType.Int32;
				colvarOrderFromType.MaxLength = 0;
				colvarOrderFromType.AutoIncrement = false;
				colvarOrderFromType.IsNullable = true;
				colvarOrderFromType.IsPrimaryKey = false;
				colvarOrderFromType.IsForeignKey = false;
				colvarOrderFromType.IsReadOnly = false;
				colvarOrderFromType.DefaultSetting = @"";
				colvarOrderFromType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderFromType);
				
				TableSchema.TableColumn colvarCreditNoteType = new TableSchema.TableColumn(schema);
				colvarCreditNoteType.ColumnName = "credit_note_type";
				colvarCreditNoteType.DataType = DbType.Int32;
				colvarCreditNoteType.MaxLength = 0;
				colvarCreditNoteType.AutoIncrement = false;
				colvarCreditNoteType.IsNullable = true;
				colvarCreditNoteType.IsPrimaryKey = false;
				colvarCreditNoteType.IsForeignKey = false;
				colvarCreditNoteType.IsReadOnly = false;
				colvarCreditNoteType.DefaultSetting = @"";
				colvarCreditNoteType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditNoteType);
				
				TableSchema.TableColumn colvarReceiverName = new TableSchema.TableColumn(schema);
				colvarReceiverName.ColumnName = "receiver_name";
				colvarReceiverName.DataType = DbType.String;
				colvarReceiverName.MaxLength = 50;
				colvarReceiverName.AutoIncrement = false;
				colvarReceiverName.IsNullable = true;
				colvarReceiverName.IsPrimaryKey = false;
				colvarReceiverName.IsForeignKey = false;
				colvarReceiverName.IsReadOnly = false;
				colvarReceiverName.DefaultSetting = @"";
				colvarReceiverName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverName);
				
				TableSchema.TableColumn colvarReceiverAddress = new TableSchema.TableColumn(schema);
				colvarReceiverAddress.ColumnName = "receiver_address";
				colvarReceiverAddress.DataType = DbType.String;
				colvarReceiverAddress.MaxLength = 200;
				colvarReceiverAddress.AutoIncrement = false;
				colvarReceiverAddress.IsNullable = true;
				colvarReceiverAddress.IsPrimaryKey = false;
				colvarReceiverAddress.IsForeignKey = false;
				colvarReceiverAddress.IsReadOnly = false;
				colvarReceiverAddress.DefaultSetting = @"";
				colvarReceiverAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverAddress);
				
				TableSchema.TableColumn colvarIsReceive = new TableSchema.TableColumn(schema);
				colvarIsReceive.ColumnName = "isReceive";
				colvarIsReceive.DataType = DbType.Boolean;
				colvarIsReceive.MaxLength = 0;
				colvarIsReceive.AutoIncrement = false;
				colvarIsReceive.IsNullable = true;
				colvarIsReceive.IsPrimaryKey = false;
				colvarIsReceive.IsForeignKey = false;
				colvarIsReceive.IsReadOnly = false;
				colvarIsReceive.DefaultSetting = @"";
				colvarIsReceive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReceive);
				
				TableSchema.TableColumn colvarNotifyChannel = new TableSchema.TableColumn(schema);
				colvarNotifyChannel.ColumnName = "notify_channel";
				colvarNotifyChannel.DataType = DbType.Int32;
				colvarNotifyChannel.MaxLength = 0;
				colvarNotifyChannel.AutoIncrement = false;
				colvarNotifyChannel.IsNullable = true;
				colvarNotifyChannel.IsPrimaryKey = false;
				colvarNotifyChannel.IsForeignKey = false;
				colvarNotifyChannel.IsReadOnly = false;
				colvarNotifyChannel.DefaultSetting = @"";
				colvarNotifyChannel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNotifyChannel);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("return_form",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId 
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("BusinessModel")]
		[Bindable(true)]
		public int BusinessModel 
		{
			get { return GetColumnValue<int>(Columns.BusinessModel); }
			set { SetColumnValue(Columns.BusinessModel, value); }
		}
		  
		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int DeliveryType 
		{
			get { return GetColumnValue<int>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}
		  
		[XmlAttribute("ProgressStatus")]
		[Bindable(true)]
		public int ProgressStatus 
		{
			get { return GetColumnValue<int>(Columns.ProgressStatus); }
			set { SetColumnValue(Columns.ProgressStatus, value); }
		}
		  
		[XmlAttribute("VendorProgressStatus")]
		[Bindable(true)]
		public int? VendorProgressStatus 
		{
			get { return GetColumnValue<int?>(Columns.VendorProgressStatus); }
			set { SetColumnValue(Columns.VendorProgressStatus, value); }
		}
		  
		[XmlAttribute("VendorMemo")]
		[Bindable(true)]
		public string VendorMemo 
		{
			get { return GetColumnValue<string>(Columns.VendorMemo); }
			set { SetColumnValue(Columns.VendorMemo, value); }
		}
		  
		[XmlAttribute("RefundType")]
		[Bindable(true)]
		public int RefundType 
		{
			get { return GetColumnValue<int>(Columns.RefundType); }
			set { SetColumnValue(Columns.RefundType, value); }
		}
		  
		[XmlAttribute("ReturnReason")]
		[Bindable(true)]
		public string ReturnReason 
		{
			get { return GetColumnValue<string>(Columns.ReturnReason); }
			set { SetColumnValue(Columns.ReturnReason, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("IsCreditNoteReceived")]
		[Bindable(true)]
		public bool IsCreditNoteReceived 
		{
			get { return GetColumnValue<bool>(Columns.IsCreditNoteReceived); }
			set { SetColumnValue(Columns.IsCreditNoteReceived, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("RefundProcessMemo")]
		[Bindable(true)]
		public string RefundProcessMemo 
		{
			get { return GetColumnValue<string>(Columns.RefundProcessMemo); }
			set { SetColumnValue(Columns.RefundProcessMemo, value); }
		}
		  
		[XmlAttribute("FinishTime")]
		[Bindable(true)]
		public DateTime? FinishTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.FinishTime); }
			set { SetColumnValue(Columns.FinishTime, value); }
		}
		  
		[XmlAttribute("VendorProcessTime")]
		[Bindable(true)]
		public DateTime? VendorProcessTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.VendorProcessTime); }
			set { SetColumnValue(Columns.VendorProcessTime, value); }
		}
		  
		[XmlAttribute("IsSystemRefund")]
		[Bindable(true)]
		public bool? IsSystemRefund 
		{
			get { return GetColumnValue<bool?>(Columns.IsSystemRefund); }
			set { SetColumnValue(Columns.IsSystemRefund, value); }
		}
		  
		[XmlAttribute("ThirdPartyPaymentSystem")]
		[Bindable(true)]
		public byte ThirdPartyPaymentSystem 
		{
			get { return GetColumnValue<byte>(Columns.ThirdPartyPaymentSystem); }
			set { SetColumnValue(Columns.ThirdPartyPaymentSystem, value); }
		}
		  
		[XmlAttribute("OrderFromType")]
		[Bindable(true)]
		public int? OrderFromType 
		{
			get { return GetColumnValue<int?>(Columns.OrderFromType); }
			set { SetColumnValue(Columns.OrderFromType, value); }
		}
		  
		[XmlAttribute("CreditNoteType")]
		[Bindable(true)]
		public int? CreditNoteType 
		{
			get { return GetColumnValue<int?>(Columns.CreditNoteType); }
			set { SetColumnValue(Columns.CreditNoteType, value); }
		}
		  
		[XmlAttribute("ReceiverName")]
		[Bindable(true)]
		public string ReceiverName 
		{
			get { return GetColumnValue<string>(Columns.ReceiverName); }
			set { SetColumnValue(Columns.ReceiverName, value); }
		}
		  
		[XmlAttribute("ReceiverAddress")]
		[Bindable(true)]
		public string ReceiverAddress 
		{
			get { return GetColumnValue<string>(Columns.ReceiverAddress); }
			set { SetColumnValue(Columns.ReceiverAddress, value); }
		}
		  
		[XmlAttribute("IsReceive")]
		[Bindable(true)]
		public bool? IsReceive 
		{
			get { return GetColumnValue<bool?>(Columns.IsReceive); }
			set { SetColumnValue(Columns.IsReceive, value); }
		}
		  
		[XmlAttribute("NotifyChannel")]
		[Bindable(true)]
		public int? NotifyChannel 
		{
			get { return GetColumnValue<int?>(Columns.NotifyChannel); }
			set { SetColumnValue(Columns.NotifyChannel, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (1)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessModelColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryTypeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ProgressStatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorProgressStatusColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorMemoColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundTypeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnReasonColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCreditNoteReceivedColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundProcessMemoColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn FinishTimeColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorProcessTimeColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn IsSystemRefundColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn ThirdPartyPaymentSystemColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderFromTypeColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn CreditNoteTypeColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverNameColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverAddressColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReceiveColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn NotifyChannelColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderId = @"order_id";
			 public static string OrderGuid = @"order_guid";
			 public static string BusinessModel = @"business_model";
			 public static string DeliveryType = @"delivery_type";
			 public static string ProgressStatus = @"progress_status";
			 public static string VendorProgressStatus = @"vendor_progress_status";
			 public static string VendorMemo = @"vendor_memo";
			 public static string RefundType = @"refund_type";
			 public static string ReturnReason = @"return_reason";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string IsCreditNoteReceived = @"is_credit_note_received";
			 public static string Memo = @"memo";
			 public static string RefundProcessMemo = @"refund_process_memo";
			 public static string FinishTime = @"finish_time";
			 public static string VendorProcessTime = @"vendor_process_time";
			 public static string IsSystemRefund = @"is_system_refund";
			 public static string ThirdPartyPaymentSystem = @"third_party_payment_system";
			 public static string OrderFromType = @"order_from_type";
			 public static string CreditNoteType = @"credit_note_type";
			 public static string ReceiverName = @"receiver_name";
			 public static string ReceiverAddress = @"receiver_address";
			 public static string IsReceive = @"isReceive";
			 public static string NotifyChannel = @"notify_channel";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

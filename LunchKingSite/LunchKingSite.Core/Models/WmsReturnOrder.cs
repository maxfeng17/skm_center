using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the WmsReturnOrder class.
    /// </summary>
    [Serializable]
    public partial class WmsReturnOrderCollection : RepositoryList<WmsReturnOrder, WmsReturnOrderCollection>
    {
        public WmsReturnOrderCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>WmsReturnOrderCollection</returns>
        public WmsReturnOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                WmsReturnOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }


    /// <summary>
    /// This is an ActiveRecord class which wraps the wms_return_order table.
    /// </summary>

    [Serializable]
    public partial class WmsReturnOrder : RepositoryRecord<WmsReturnOrder>, IRecordBase
    {
        #region .ctors and Default Settings

        public WmsReturnOrder()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public WmsReturnOrder(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("wms_return_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_guid";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = false;
                colvarItemGuid.IsPrimaryKey = false;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;
                colvarItemGuid.DefaultSetting = @"";
                colvarItemGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemGuid);

                TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
                colvarQty.ColumnName = "qty";
                colvarQty.DataType = DbType.Int32;
                colvarQty.MaxLength = 0;
                colvarQty.AutoIncrement = false;
                colvarQty.IsNullable = false;
                colvarQty.IsPrimaryKey = false;
                colvarQty.IsForeignKey = false;
                colvarQty.IsReadOnly = false;
                colvarQty.DefaultSetting = @"";
                colvarQty.ForeignKeyTableName = "";
                schema.Columns.Add(colvarQty);

                TableSchema.TableColumn colvarSalesId = new TableSchema.TableColumn(schema);
                colvarSalesId.ColumnName = "sales_id";
                colvarSalesId.DataType = DbType.Int32;
                colvarSalesId.MaxLength = 0;
                colvarSalesId.AutoIncrement = false;
                colvarSalesId.IsNullable = false;
                colvarSalesId.IsPrimaryKey = false;
                colvarSalesId.IsForeignKey = false;
                colvarSalesId.IsReadOnly = false;
                colvarSalesId.DefaultSetting = @"";
                colvarSalesId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSalesId);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarPchomeReturnOrderId = new TableSchema.TableColumn(schema);
                colvarPchomeReturnOrderId.ColumnName = "pchome_return_order_id";
                colvarPchomeReturnOrderId.DataType = DbType.AnsiString;
                colvarPchomeReturnOrderId.MaxLength = 50;
                colvarPchomeReturnOrderId.AutoIncrement = false;
                colvarPchomeReturnOrderId.IsNullable = true;
                colvarPchomeReturnOrderId.IsPrimaryKey = false;
                colvarPchomeReturnOrderId.IsForeignKey = false;
                colvarPchomeReturnOrderId.IsReadOnly = false;
                colvarPchomeReturnOrderId.DefaultSetting = @"";
                colvarPchomeReturnOrderId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPchomeReturnOrderId);

                TableSchema.TableColumn colvarInvalidation = new TableSchema.TableColumn(schema);
                colvarInvalidation.ColumnName = "invalidation";
                colvarInvalidation.DataType = DbType.Int32;
                colvarInvalidation.MaxLength = 0;
                colvarInvalidation.AutoIncrement = false;
                colvarInvalidation.IsNullable = false;
                colvarInvalidation.IsPrimaryKey = false;
                colvarInvalidation.IsForeignKey = false;
                colvarInvalidation.IsReadOnly = false;

                colvarInvalidation.DefaultSetting = @"((0))";
                colvarInvalidation.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvalidation);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.AnsiString;
                colvarModifyId.MaxLength = 256;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarApplyDate = new TableSchema.TableColumn(schema);
                colvarApplyDate.ColumnName = "apply_date";
                colvarApplyDate.DataType = DbType.DateTime;
                colvarApplyDate.MaxLength = 0;
                colvarApplyDate.AutoIncrement = false;
                colvarApplyDate.IsNullable = true;
                colvarApplyDate.IsPrimaryKey = false;
                colvarApplyDate.IsForeignKey = false;
                colvarApplyDate.IsReadOnly = false;
                colvarApplyDate.DefaultSetting = @"";
                colvarApplyDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarApplyDate);

                TableSchema.TableColumn colvarReadyDate = new TableSchema.TableColumn(schema);
                colvarReadyDate.ColumnName = "ready_date";
                colvarReadyDate.DataType = DbType.DateTime;
                colvarReadyDate.MaxLength = 0;
                colvarReadyDate.AutoIncrement = false;
                colvarReadyDate.IsNullable = true;
                colvarReadyDate.IsPrimaryKey = false;
                colvarReadyDate.IsForeignKey = false;
                colvarReadyDate.IsReadOnly = false;
                colvarReadyDate.DefaultSetting = @"";
                colvarReadyDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReadyDate);

                TableSchema.TableColumn colvarShipDate = new TableSchema.TableColumn(schema);
                colvarShipDate.ColumnName = "ship_date";
                colvarShipDate.DataType = DbType.DateTime;
                colvarShipDate.MaxLength = 0;
                colvarShipDate.AutoIncrement = false;
                colvarShipDate.IsNullable = true;
                colvarShipDate.IsPrimaryKey = false;
                colvarShipDate.IsForeignKey = false;
                colvarShipDate.IsReadOnly = false;
                colvarShipDate.DefaultSetting = @"";
                colvarShipDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShipDate);

                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 1000;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = true;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;
                colvarRemark.DefaultSetting = @"";
                colvarRemark.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRemark);

                TableSchema.TableColumn colvarSource = new TableSchema.TableColumn(schema);
                colvarSource.ColumnName = "source";
                colvarSource.DataType = DbType.Int32;
                colvarSource.MaxLength = 0;
                colvarSource.AutoIncrement = false;
                colvarSource.IsNullable = false;
                colvarSource.IsPrimaryKey = false;
                colvarSource.IsForeignKey = false;
                colvarSource.IsReadOnly = false;

                colvarSource.DefaultSetting = @"((0))";
                colvarSource.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSource);

                TableSchema.TableColumn colvarReason = new TableSchema.TableColumn(schema);
                colvarReason.ColumnName = "reason";
                colvarReason.DataType = DbType.String;
                colvarReason.MaxLength = 50;
                colvarReason.AutoIncrement = false;
                colvarReason.IsNullable = true;
                colvarReason.IsPrimaryKey = false;
                colvarReason.IsForeignKey = false;
                colvarReason.IsReadOnly = false;
                colvarReason.DefaultSetting = @"";
                colvarReason.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReason);

                TableSchema.TableColumn colvarErrorX = new TableSchema.TableColumn(schema);
                colvarErrorX.ColumnName = "error";
                colvarErrorX.DataType = DbType.String;
                colvarErrorX.MaxLength = 200;
                colvarErrorX.AutoIncrement = false;
                colvarErrorX.IsNullable = true;
                colvarErrorX.IsPrimaryKey = false;
                colvarErrorX.IsForeignKey = false;
                colvarErrorX.IsReadOnly = false;
                colvarErrorX.DefaultSetting = @"";
                colvarErrorX.ForeignKeyTableName = "";
                schema.Columns.Add(colvarErrorX);

                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = true;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                colvarAccountId.DefaultSetting = @"";
                colvarAccountId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAccountId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("wms_return_order", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid ItemGuid
        {
            get { return GetColumnValue<Guid>(Columns.ItemGuid); }
            set { SetColumnValue(Columns.ItemGuid, value); }
        }

        [XmlAttribute("Qty")]
        [Bindable(true)]
        public int Qty
        {
            get { return GetColumnValue<int>(Columns.Qty); }
            set { SetColumnValue(Columns.Qty, value); }
        }

        [XmlAttribute("SalesId")]
        [Bindable(true)]
        public int SalesId
        {
            get { return GetColumnValue<int>(Columns.SalesId); }
            set { SetColumnValue(Columns.SalesId, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("PchomeReturnOrderId")]
        [Bindable(true)]
        public string PchomeReturnOrderId
        {
            get { return GetColumnValue<string>(Columns.PchomeReturnOrderId); }
            set { SetColumnValue(Columns.PchomeReturnOrderId, value); }
        }

        [XmlAttribute("Invalidation")]
        [Bindable(true)]
        public int Invalidation
        {
            get { return GetColumnValue<int>(Columns.Invalidation); }
            set { SetColumnValue(Columns.Invalidation, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ApplyDate")]
        [Bindable(true)]
        public DateTime? ApplyDate
        {
            get { return GetColumnValue<DateTime?>(Columns.ApplyDate); }
            set { SetColumnValue(Columns.ApplyDate, value); }
        }

        [XmlAttribute("ReadyDate")]
        [Bindable(true)]
        public DateTime? ReadyDate
        {
            get { return GetColumnValue<DateTime?>(Columns.ReadyDate); }
            set { SetColumnValue(Columns.ReadyDate, value); }
        }

        [XmlAttribute("ShipDate")]
        [Bindable(true)]
        public DateTime? ShipDate
        {
            get { return GetColumnValue<DateTime?>(Columns.ShipDate); }
            set { SetColumnValue(Columns.ShipDate, value); }
        }

        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark
        {
            get { return GetColumnValue<string>(Columns.Remark); }
            set { SetColumnValue(Columns.Remark, value); }
        }

        [XmlAttribute("Source")]
        [Bindable(true)]
        public int Source
        {
            get { return GetColumnValue<int>(Columns.Source); }
            set { SetColumnValue(Columns.Source, value); }
        }

        [XmlAttribute("Reason")]
        [Bindable(true)]
        public string Reason
        {
            get { return GetColumnValue<string>(Columns.Reason); }
            set { SetColumnValue(Columns.Reason, value); }
        }

        [XmlAttribute("ErrorX")]
        [Bindable(true)]
        public string ErrorX
        {
            get { return GetColumnValue<string>(Columns.ErrorX); }
            set { SetColumnValue(Columns.ErrorX, value); }
        }

        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId
        {
            get { return GetColumnValue<string>(Columns.AccountId); }
            set { SetColumnValue(Columns.AccountId, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ItemGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn QtyColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn SalesIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn PchomeReturnOrderIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn InvalidationColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn ApplyDateColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn ReadyDateColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn ShipDateColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn SourceColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn ReasonColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn ErrorXColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn AccountIdColumn
        {
            get { return Schema.Columns[18]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"guid";
            public static string ItemGuid = @"item_guid";
            public static string Qty = @"qty";
            public static string SalesId = @"sales_id";
            public static string Status = @"status";
            public static string PchomeReturnOrderId = @"pchome_return_order_id";
            public static string Invalidation = @"invalidation";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string ApplyDate = @"apply_date";
            public static string ReadyDate = @"ready_date";
            public static string ShipDate = @"ship_date";
            public static string Remark = @"remark";
            public static string Source = @"source";
            public static string Reason = @"reason";
            public static string ErrorX = @"error";
            public static string AccountId = @"account_id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

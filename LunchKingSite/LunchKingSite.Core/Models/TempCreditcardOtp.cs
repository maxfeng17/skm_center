using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the TempCreditcardOtp class.
	/// </summary>
	[Serializable]
	public partial class TempCreditcardOtpCollection : RepositoryList<TempCreditcardOtp, TempCreditcardOtpCollection>
	{
		public TempCreditcardOtpCollection() {}

		/// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
		/// </summary>
		/// <returns>TempCreditcardOtpCollection</returns>
		public TempCreditcardOtpCollection Filter()
		{
			for (int i = this.Count - 1; i > -1; i--)
			{
				TempCreditcardOtp o = this[i];
				foreach (SubSonic.Where w in this.wheres)
				{
					bool remove = false;
					System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
					if (pi.CanRead)
					{
						object val = pi.GetValue(o, null);
						switch (w.Comparison)
						{
							case SubSonic.Comparison.Equals:
								if (!val.Equals(w.ParameterValue))
								{
									remove = true;
								}
								break;
						}
					}
					if (remove)
					{
						this.Remove(o);
						break;
					}
				}
			}
			return this;
		}


	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the temp_creditcard_otp table.
	/// </summary>
	[Serializable]
	public partial class TempCreditcardOtp : RepositoryRecord<TempCreditcardOtp>, IRecordBase
	{
		#region .ctors and Default Settings

		public TempCreditcardOtp()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public TempCreditcardOtp(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("temp_creditcard_otp", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns

				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarTransId = new TableSchema.TableColumn(schema);
				colvarTransId.ColumnName = "trans_id";
				colvarTransId.DataType = DbType.AnsiString;
				colvarTransId.MaxLength = 40;
				colvarTransId.AutoIncrement = false;
				colvarTransId.IsNullable = false;
				colvarTransId.IsPrimaryKey = false;
				colvarTransId.IsForeignKey = false;
				colvarTransId.IsReadOnly = false;
				colvarTransId.DefaultSetting = @"";
				colvarTransId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransId);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				TableSchema.TableColumn colvarOtpInfo = new TableSchema.TableColumn(schema);
				colvarOtpInfo.ColumnName = "otp_info";
				colvarOtpInfo.DataType = DbType.String;
				colvarOtpInfo.MaxLength = -1;
				colvarOtpInfo.AutoIncrement = false;
				colvarOtpInfo.IsNullable = false;
				colvarOtpInfo.IsPrimaryKey = false;
				colvarOtpInfo.IsForeignKey = false;
				colvarOtpInfo.IsReadOnly = false;
				colvarOtpInfo.DefaultSetting = @"";
				colvarOtpInfo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOtpInfo);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarEncryptSalt = new TableSchema.TableColumn(schema);
				colvarEncryptSalt.ColumnName = "encrypt_salt";
				colvarEncryptSalt.DataType = DbType.String;
				colvarEncryptSalt.MaxLength = 128;
				colvarEncryptSalt.AutoIncrement = false;
				colvarEncryptSalt.IsNullable = false;
				colvarEncryptSalt.IsPrimaryKey = false;
				colvarEncryptSalt.IsForeignKey = false;
				colvarEncryptSalt.IsReadOnly = false;
				colvarEncryptSalt.DefaultSetting = @"";
				colvarEncryptSalt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEncryptSalt);

				TableSchema.TableColumn colvarTicketId = new TableSchema.TableColumn(schema);
				colvarTicketId.ColumnName = "ticket_id";
				colvarTicketId.DataType = DbType.String;
				colvarTicketId.MaxLength = 100;
				colvarTicketId.AutoIncrement = false;
				colvarTicketId.IsNullable = true;
				colvarTicketId.IsPrimaryKey = false;
				colvarTicketId.IsForeignKey = false;
				colvarTicketId.IsReadOnly = false;
				colvarTicketId.DefaultSetting = @"";
				colvarTicketId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTicketId);

				TableSchema.TableColumn colvarAppUserId = new TableSchema.TableColumn(schema);
				colvarAppUserId.ColumnName = "app_user_id";
				colvarAppUserId.DataType = DbType.String;
				colvarAppUserId.MaxLength = 50;
				colvarAppUserId.AutoIncrement = false;
				colvarAppUserId.IsNullable = true;
				colvarAppUserId.IsPrimaryKey = false;
				colvarAppUserId.IsForeignKey = false;
				colvarAppUserId.IsReadOnly = false;
				colvarAppUserId.DefaultSetting = @"";
				colvarAppUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppUserId);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("temp_creditcard_otp",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("TransId")]
		[Bindable(true)]
		public string TransId
		{
			get { return GetColumnValue<string>(Columns.TransId); }
			set { SetColumnValue(Columns.TransId, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		[XmlAttribute("OtpInfo")]
		[Bindable(true)]
		public string OtpInfo
		{
			get { return GetColumnValue<string>(Columns.OtpInfo); }
			set { SetColumnValue(Columns.OtpInfo, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("EncryptSalt")]
		[Bindable(true)]
		public string EncryptSalt
		{
			get { return GetColumnValue<string>(Columns.EncryptSalt); }
			set { SetColumnValue(Columns.EncryptSalt, value); }
		}

		[XmlAttribute("TicketId")]
		[Bindable(true)]
		public string TicketId
		{
			get { return GetColumnValue<string>(Columns.TicketId); }
			set { SetColumnValue(Columns.TicketId, value); }
		}

		[XmlAttribute("AppUserId")]
		[Bindable(true)]
		public string AppUserId
		{
			get { return GetColumnValue<string>(Columns.AppUserId); }
			set { SetColumnValue(Columns.AppUserId, value); }
		}

		#endregion




		//no foreign key tables defined (0)



		//no ManyToMany tables defined (0)





		#region Typed Columns


		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}



		public static TableSchema.TableColumn TransIdColumn
		{
			get { return Schema.Columns[1]; }
		}



		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[2]; }
		}



		public static TableSchema.TableColumn OtpInfoColumn
		{
			get { return Schema.Columns[3]; }
		}



		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[4]; }
		}



		public static TableSchema.TableColumn EncryptSaltColumn
		{
			get { return Schema.Columns[5]; }
		}



		public static TableSchema.TableColumn TicketIdColumn
		{
			get { return Schema.Columns[6]; }
		}



		public static TableSchema.TableColumn AppUserIdColumn
		{
			get { return Schema.Columns[7]; }
		}



		#endregion
		#region Columns Struct
		public struct Columns
		{
			public static string Id = @"Id";
			public static string TransId = @"trans_id";
			public static string UserId = @"user_id";
			public static string OtpInfo = @"otp_info";
			public static string CreateTime = @"create_time";
			public static string EncryptSalt = @"encrypt_salt";
			public static string TicketId = @"ticket_id";
			public static string AppUserId = @"app_user_id";

		}
		#endregion

		#region Update PK Collections

		#endregion

		#region Deep Save

		#endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBalanceSheetUndeducted class.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetUndeductedCollection : ReadOnlyList<ViewBalanceSheetUndeducted, ViewBalanceSheetUndeductedCollection>
    {        
        public ViewBalanceSheetUndeductedCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_balance_sheet_undeducted view.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetUndeducted : ReadOnlyRecord<ViewBalanceSheetUndeducted>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_balance_sheet_undeducted", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarMerchandiseGuid = new TableSchema.TableColumn(schema);
                colvarMerchandiseGuid.ColumnName = "merchandise_guid";
                colvarMerchandiseGuid.DataType = DbType.Guid;
                colvarMerchandiseGuid.MaxLength = 0;
                colvarMerchandiseGuid.AutoIncrement = false;
                colvarMerchandiseGuid.IsNullable = false;
                colvarMerchandiseGuid.IsPrimaryKey = false;
                colvarMerchandiseGuid.IsForeignKey = false;
                colvarMerchandiseGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMerchandiseGuid);
                
                TableSchema.TableColumn colvarBizModel = new TableSchema.TableColumn(schema);
                colvarBizModel.ColumnName = "biz_model";
                colvarBizModel.DataType = DbType.Int32;
                colvarBizModel.MaxLength = 0;
                colvarBizModel.AutoIncrement = false;
                colvarBizModel.IsNullable = false;
                colvarBizModel.IsPrimaryKey = false;
                colvarBizModel.IsForeignKey = false;
                colvarBizModel.IsReadOnly = false;
                
                schema.Columns.Add(colvarBizModel);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarBalanceSheetId = new TableSchema.TableColumn(schema);
                colvarBalanceSheetId.ColumnName = "balance_sheet_id";
                colvarBalanceSheetId.DataType = DbType.Int32;
                colvarBalanceSheetId.MaxLength = 0;
                colvarBalanceSheetId.AutoIncrement = false;
                colvarBalanceSheetId.IsNullable = false;
                colvarBalanceSheetId.IsPrimaryKey = false;
                colvarBalanceSheetId.IsForeignKey = false;
                colvarBalanceSheetId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBalanceSheetId);
                
                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustId);
                
                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponId);
                
                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.String;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = true;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponSequenceNumber);
                
                TableSchema.TableColumn colvarDetailStatus = new TableSchema.TableColumn(schema);
                colvarDetailStatus.ColumnName = "detail_status";
                colvarDetailStatus.DataType = DbType.Int32;
                colvarDetailStatus.MaxLength = 0;
                colvarDetailStatus.AutoIncrement = false;
                colvarDetailStatus.IsNullable = false;
                colvarDetailStatus.IsPrimaryKey = false;
                colvarDetailStatus.IsForeignKey = false;
                colvarDetailStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarDetailStatus);
                
                TableSchema.TableColumn colvarUndoUser = new TableSchema.TableColumn(schema);
                colvarUndoUser.ColumnName = "undo_user";
                colvarUndoUser.DataType = DbType.String;
                colvarUndoUser.MaxLength = 256;
                colvarUndoUser.AutoIncrement = false;
                colvarUndoUser.IsNullable = true;
                colvarUndoUser.IsPrimaryKey = false;
                colvarUndoUser.IsForeignKey = false;
                colvarUndoUser.IsReadOnly = false;
                
                schema.Columns.Add(colvarUndoUser);
                
                TableSchema.TableColumn colvarUndoTime = new TableSchema.TableColumn(schema);
                colvarUndoTime.ColumnName = "undo_time";
                colvarUndoTime.DataType = DbType.DateTime;
                colvarUndoTime.MaxLength = 0;
                colvarUndoTime.AutoIncrement = false;
                colvarUndoTime.IsNullable = true;
                colvarUndoTime.IsPrimaryKey = false;
                colvarUndoTime.IsForeignKey = false;
                colvarUndoTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUndoTime);
                
                TableSchema.TableColumn colvarVerifyUser = new TableSchema.TableColumn(schema);
                colvarVerifyUser.ColumnName = "verify_user";
                colvarVerifyUser.DataType = DbType.String;
                colvarVerifyUser.MaxLength = 256;
                colvarVerifyUser.AutoIncrement = false;
                colvarVerifyUser.IsNullable = true;
                colvarVerifyUser.IsPrimaryKey = false;
                colvarVerifyUser.IsForeignKey = false;
                colvarVerifyUser.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifyUser);
                
                TableSchema.TableColumn colvarVerifyTime = new TableSchema.TableColumn(schema);
                colvarVerifyTime.ColumnName = "verify_time";
                colvarVerifyTime.DataType = DbType.DateTime;
                colvarVerifyTime.MaxLength = 0;
                colvarVerifyTime.AutoIncrement = false;
                colvarVerifyTime.IsNullable = true;
                colvarVerifyTime.IsPrimaryKey = false;
                colvarVerifyTime.IsForeignKey = false;
                colvarVerifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifyTime);
                
                TableSchema.TableColumn colvarBsModifyMessage = new TableSchema.TableColumn(schema);
                colvarBsModifyMessage.ColumnName = "bs_modify_message";
                colvarBsModifyMessage.DataType = DbType.String;
                colvarBsModifyMessage.MaxLength = 100;
                colvarBsModifyMessage.AutoIncrement = false;
                colvarBsModifyMessage.IsNullable = true;
                colvarBsModifyMessage.IsPrimaryKey = false;
                colvarBsModifyMessage.IsForeignKey = false;
                colvarBsModifyMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarBsModifyMessage);
                
                TableSchema.TableColumn colvarBsModifyUser = new TableSchema.TableColumn(schema);
                colvarBsModifyUser.ColumnName = "bs_modify_user";
                colvarBsModifyUser.DataType = DbType.String;
                colvarBsModifyUser.MaxLength = 256;
                colvarBsModifyUser.AutoIncrement = false;
                colvarBsModifyUser.IsNullable = true;
                colvarBsModifyUser.IsPrimaryKey = false;
                colvarBsModifyUser.IsForeignKey = false;
                colvarBsModifyUser.IsReadOnly = false;
                
                schema.Columns.Add(colvarBsModifyUser);
                
                TableSchema.TableColumn colvarBsModifyTime = new TableSchema.TableColumn(schema);
                colvarBsModifyTime.ColumnName = "bs_modify_time";
                colvarBsModifyTime.DataType = DbType.DateTime;
                colvarBsModifyTime.MaxLength = 0;
                colvarBsModifyTime.AutoIncrement = false;
                colvarBsModifyTime.IsNullable = true;
                colvarBsModifyTime.IsPrimaryKey = false;
                colvarBsModifyTime.IsForeignKey = false;
                colvarBsModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarBsModifyTime);
                
                TableSchema.TableColumn colvarPponCost = new TableSchema.TableColumn(schema);
                colvarPponCost.ColumnName = "ppon_cost";
                colvarPponCost.DataType = DbType.Currency;
                colvarPponCost.MaxLength = 0;
                colvarPponCost.AutoIncrement = false;
                colvarPponCost.IsNullable = true;
                colvarPponCost.IsPrimaryKey = false;
                colvarPponCost.IsForeignKey = false;
                colvarPponCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarPponCost);
                
                TableSchema.TableColumn colvarPiinCost = new TableSchema.TableColumn(schema);
                colvarPiinCost.ColumnName = "piin_cost";
                colvarPiinCost.DataType = DbType.Currency;
                colvarPiinCost.MaxLength = 0;
                colvarPiinCost.AutoIncrement = false;
                colvarPiinCost.IsNullable = true;
                colvarPiinCost.IsPrimaryKey = false;
                colvarPiinCost.IsForeignKey = false;
                colvarPiinCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarPiinCost);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_balance_sheet_undeducted",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBalanceSheetUndeducted()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBalanceSheetUndeducted(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBalanceSheetUndeducted(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBalanceSheetUndeducted(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("MerchandiseGuid")]
        [Bindable(true)]
        public Guid MerchandiseGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("merchandise_guid");
		    }
            set 
		    {
			    SetColumnValue("merchandise_guid", value);
            }
        }
	      
        [XmlAttribute("BizModel")]
        [Bindable(true)]
        public int BizModel 
	    {
		    get
		    {
			    return GetColumnValue<int>("biz_model");
		    }
            set 
		    {
			    SetColumnValue("biz_model", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("BalanceSheetId")]
        [Bindable(true)]
        public int BalanceSheetId 
	    {
		    get
		    {
			    return GetColumnValue<int>("balance_sheet_id");
		    }
            set 
		    {
			    SetColumnValue("balance_sheet_id", value);
            }
        }
	      
        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("trust_id");
		    }
            set 
		    {
			    SetColumnValue("trust_id", value);
            }
        }
	      
        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("coupon_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_id", value);
            }
        }
	      
        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_sequence_number");
		    }
            set 
		    {
			    SetColumnValue("coupon_sequence_number", value);
            }
        }
	      
        [XmlAttribute("DetailStatus")]
        [Bindable(true)]
        public int DetailStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("detail_status");
		    }
            set 
		    {
			    SetColumnValue("detail_status", value);
            }
        }
	      
        [XmlAttribute("UndoUser")]
        [Bindable(true)]
        public string UndoUser 
	    {
		    get
		    {
			    return GetColumnValue<string>("undo_user");
		    }
            set 
		    {
			    SetColumnValue("undo_user", value);
            }
        }
	      
        [XmlAttribute("UndoTime")]
        [Bindable(true)]
        public DateTime? UndoTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("undo_time");
		    }
            set 
		    {
			    SetColumnValue("undo_time", value);
            }
        }
	      
        [XmlAttribute("VerifyUser")]
        [Bindable(true)]
        public string VerifyUser 
	    {
		    get
		    {
			    return GetColumnValue<string>("verify_user");
		    }
            set 
		    {
			    SetColumnValue("verify_user", value);
            }
        }
	      
        [XmlAttribute("VerifyTime")]
        [Bindable(true)]
        public DateTime? VerifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("verify_time");
		    }
            set 
		    {
			    SetColumnValue("verify_time", value);
            }
        }
	      
        [XmlAttribute("BsModifyMessage")]
        [Bindable(true)]
        public string BsModifyMessage 
	    {
		    get
		    {
			    return GetColumnValue<string>("bs_modify_message");
		    }
            set 
		    {
			    SetColumnValue("bs_modify_message", value);
            }
        }
	      
        [XmlAttribute("BsModifyUser")]
        [Bindable(true)]
        public string BsModifyUser 
	    {
		    get
		    {
			    return GetColumnValue<string>("bs_modify_user");
		    }
            set 
		    {
			    SetColumnValue("bs_modify_user", value);
            }
        }
	      
        [XmlAttribute("BsModifyTime")]
        [Bindable(true)]
        public DateTime? BsModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bs_modify_time");
		    }
            set 
		    {
			    SetColumnValue("bs_modify_time", value);
            }
        }
	      
        [XmlAttribute("PponCost")]
        [Bindable(true)]
        public decimal? PponCost 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("ppon_cost");
		    }
            set 
		    {
			    SetColumnValue("ppon_cost", value);
            }
        }
	      
        [XmlAttribute("PiinCost")]
        [Bindable(true)]
        public decimal? PiinCost 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("piin_cost");
		    }
            set 
		    {
			    SetColumnValue("piin_cost", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string MerchandiseGuid = @"merchandise_guid";
            
            public static string BizModel = @"biz_model";
            
            public static string StoreGuid = @"store_guid";
            
            public static string StoreName = @"store_name";
            
            public static string BalanceSheetId = @"balance_sheet_id";
            
            public static string TrustId = @"trust_id";
            
            public static string CouponId = @"coupon_id";
            
            public static string CouponSequenceNumber = @"coupon_sequence_number";
            
            public static string DetailStatus = @"detail_status";
            
            public static string UndoUser = @"undo_user";
            
            public static string UndoTime = @"undo_time";
            
            public static string VerifyUser = @"verify_user";
            
            public static string VerifyTime = @"verify_time";
            
            public static string BsModifyMessage = @"bs_modify_message";
            
            public static string BsModifyUser = @"bs_modify_user";
            
            public static string BsModifyTime = @"bs_modify_time";
            
            public static string PponCost = @"ppon_cost";
            
            public static string PiinCost = @"piin_cost";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

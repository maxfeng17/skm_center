using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the DealCost class.
    /// </summary>
    [Serializable]
    public partial class DealCostCollection : RepositoryList<DealCost, DealCostCollection>
    {
        public DealCostCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealCostCollection</returns>
        public DealCostCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealCost o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the deal_cost table.
    /// </summary>
    [Serializable]
    public partial class DealCost : RepositoryRecord<DealCost>, IRecordBase
    {
        #region .ctors and Default Settings

        public DealCost()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public DealCost(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("deal_cost", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Currency;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = true;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;
                colvarCost.DefaultSetting = @"";
                colvarCost.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCost);

                TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
                colvarQuantity.ColumnName = "quantity";
                colvarQuantity.DataType = DbType.Int32;
                colvarQuantity.MaxLength = 0;
                colvarQuantity.AutoIncrement = false;
                colvarQuantity.IsNullable = true;
                colvarQuantity.IsPrimaryKey = false;
                colvarQuantity.IsForeignKey = false;
                colvarQuantity.IsReadOnly = false;
                colvarQuantity.DefaultSetting = @"";
                colvarQuantity.ForeignKeyTableName = "";
                schema.Columns.Add(colvarQuantity);

                TableSchema.TableColumn colvarCumulativeQuantity = new TableSchema.TableColumn(schema);
                colvarCumulativeQuantity.ColumnName = "cumulative_quantity";
                colvarCumulativeQuantity.DataType = DbType.Int32;
                colvarCumulativeQuantity.MaxLength = 0;
                colvarCumulativeQuantity.AutoIncrement = false;
                colvarCumulativeQuantity.IsNullable = true;
                colvarCumulativeQuantity.IsPrimaryKey = false;
                colvarCumulativeQuantity.IsForeignKey = false;
                colvarCumulativeQuantity.IsReadOnly = false;
                colvarCumulativeQuantity.DefaultSetting = @"";
                colvarCumulativeQuantity.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCumulativeQuantity);

                TableSchema.TableColumn colvarLowerCumulativeQuantity = new TableSchema.TableColumn(schema);
                colvarLowerCumulativeQuantity.ColumnName = "lower_cumulative_quantity";
                colvarLowerCumulativeQuantity.DataType = DbType.Int32;
                colvarLowerCumulativeQuantity.MaxLength = 0;
                colvarLowerCumulativeQuantity.AutoIncrement = false;
                colvarLowerCumulativeQuantity.IsNullable = true;
                colvarLowerCumulativeQuantity.IsPrimaryKey = false;
                colvarLowerCumulativeQuantity.IsForeignKey = false;
                colvarLowerCumulativeQuantity.IsReadOnly = false;
                colvarLowerCumulativeQuantity.DefaultSetting = @"";
                colvarLowerCumulativeQuantity.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLowerCumulativeQuantity);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("deal_cost", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid
        {
            get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        [XmlAttribute("Cost")]
        [Bindable(true)]
        public decimal? Cost
        {
            get { return GetColumnValue<decimal?>(Columns.Cost); }
            set { SetColumnValue(Columns.Cost, value); }
        }

        [XmlAttribute("Quantity")]
        [Bindable(true)]
        public int? Quantity
        {
            get { return GetColumnValue<int?>(Columns.Quantity); }
            set { SetColumnValue(Columns.Quantity, value); }
        }

        [XmlAttribute("CumulativeQuantity")]
        [Bindable(true)]
        public int? CumulativeQuantity
        {
            get { return GetColumnValue<int?>(Columns.CumulativeQuantity); }
            set { SetColumnValue(Columns.CumulativeQuantity, value); }
        }

        [XmlAttribute("LowerCumulativeQuantity")]
        [Bindable(true)]
        public int? LowerCumulativeQuantity
        {
            get { return GetColumnValue<int?>(Columns.LowerCumulativeQuantity); }
            set { SetColumnValue(Columns.LowerCumulativeQuantity, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn CostColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn QuantityColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CumulativeQuantityColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn LowerCumulativeQuantityColumn
        {
            get { return Schema.Columns[5]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string BusinessHourGuid = @"business_hour_guid";
            public static string Cost = @"cost";
            public static string Quantity = @"quantity";
            public static string CumulativeQuantity = @"cumulative_quantity";
            public static string LowerCumulativeQuantity = @"lower_cumulative_quantity";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

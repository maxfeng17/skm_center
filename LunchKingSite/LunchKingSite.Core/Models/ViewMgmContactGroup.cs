using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMgmContactGroup class.
    /// </summary>
    [Serializable]
    public partial class ViewMgmContactGroupCollection : ReadOnlyList<ViewMgmContactGroup, ViewMgmContactGroupCollection>
    {        
        public ViewMgmContactGroupCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_mgm_contact_group view.
    /// </summary>
    [Serializable]
    public partial class ViewMgmContactGroup : ReadOnlyRecord<ViewMgmContactGroup>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_mgm_contact_group", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarContactUserId = new TableSchema.TableColumn(schema);
                colvarContactUserId.ColumnName = "contact_user_id";
                colvarContactUserId.DataType = DbType.Int32;
                colvarContactUserId.MaxLength = 0;
                colvarContactUserId.AutoIncrement = false;
                colvarContactUserId.IsNullable = true;
                colvarContactUserId.IsPrimaryKey = false;
                colvarContactUserId.IsForeignKey = false;
                colvarContactUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarContactUserId);
                
                TableSchema.TableColumn colvarContactName = new TableSchema.TableColumn(schema);
                colvarContactName.ColumnName = "contact_name";
                colvarContactName.DataType = DbType.String;
                colvarContactName.MaxLength = 256;
                colvarContactName.AutoIncrement = false;
                colvarContactName.IsNullable = false;
                colvarContactName.IsPrimaryKey = false;
                colvarContactName.IsForeignKey = false;
                colvarContactName.IsReadOnly = false;
                
                schema.Columns.Add(colvarContactName);
                
                TableSchema.TableColumn colvarContactNickName = new TableSchema.TableColumn(schema);
                colvarContactNickName.ColumnName = "contact_nick_name";
                colvarContactNickName.DataType = DbType.String;
                colvarContactNickName.MaxLength = 256;
                colvarContactNickName.AutoIncrement = false;
                colvarContactNickName.IsNullable = true;
                colvarContactNickName.IsPrimaryKey = false;
                colvarContactNickName.IsForeignKey = false;
                colvarContactNickName.IsReadOnly = false;
                
                schema.Columns.Add(colvarContactNickName);
                
                TableSchema.TableColumn colvarContactValue = new TableSchema.TableColumn(schema);
                colvarContactValue.ColumnName = "contact_value";
                colvarContactValue.DataType = DbType.String;
                colvarContactValue.MaxLength = 256;
                colvarContactValue.AutoIncrement = false;
                colvarContactValue.IsNullable = true;
                colvarContactValue.IsPrimaryKey = false;
                colvarContactValue.IsForeignKey = false;
                colvarContactValue.IsReadOnly = false;
                
                schema.Columns.Add(colvarContactValue);
                
                TableSchema.TableColumn colvarContactType = new TableSchema.TableColumn(schema);
                colvarContactType.ColumnName = "contact_type";
                colvarContactType.DataType = DbType.Int32;
                colvarContactType.MaxLength = 0;
                colvarContactType.AutoIncrement = false;
                colvarContactType.IsNullable = true;
                colvarContactType.IsPrimaryKey = false;
                colvarContactType.IsForeignKey = false;
                colvarContactType.IsReadOnly = false;
                
                schema.Columns.Add(colvarContactType);
                
                TableSchema.TableColumn colvarContactBirthday = new TableSchema.TableColumn(schema);
                colvarContactBirthday.ColumnName = "contact_birthday";
                colvarContactBirthday.DataType = DbType.DateTime;
                colvarContactBirthday.MaxLength = 0;
                colvarContactBirthday.AutoIncrement = false;
                colvarContactBirthday.IsNullable = true;
                colvarContactBirthday.IsPrimaryKey = false;
                colvarContactBirthday.IsForeignKey = false;
                colvarContactBirthday.IsReadOnly = false;
                
                schema.Columns.Add(colvarContactBirthday);
                
                TableSchema.TableColumn colvarContactId = new TableSchema.TableColumn(schema);
                colvarContactId.ColumnName = "contact_id";
                colvarContactId.DataType = DbType.Int32;
                colvarContactId.MaxLength = 0;
                colvarContactId.AutoIncrement = false;
                colvarContactId.IsNullable = true;
                colvarContactId.IsPrimaryKey = false;
                colvarContactId.IsForeignKey = false;
                colvarContactId.IsReadOnly = false;
                
                schema.Columns.Add(colvarContactId);
                
                TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
                colvarGroupId.ColumnName = "group_id";
                colvarGroupId.DataType = DbType.Int32;
                colvarGroupId.MaxLength = 0;
                colvarGroupId.AutoIncrement = false;
                colvarGroupId.IsNullable = true;
                colvarGroupId.IsPrimaryKey = false;
                colvarGroupId.IsForeignKey = false;
                colvarGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupId);
                
                TableSchema.TableColumn colvarGroupName = new TableSchema.TableColumn(schema);
                colvarGroupName.ColumnName = "group_name";
                colvarGroupName.DataType = DbType.String;
                colvarGroupName.MaxLength = 500;
                colvarGroupName.AutoIncrement = false;
                colvarGroupName.IsNullable = true;
                colvarGroupName.IsPrimaryKey = false;
                colvarGroupName.IsForeignKey = false;
                colvarGroupName.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupName);
                
                TableSchema.TableColumn colvarGroupImagePath = new TableSchema.TableColumn(schema);
                colvarGroupImagePath.ColumnName = "group_image_path";
                colvarGroupImagePath.DataType = DbType.String;
                colvarGroupImagePath.MaxLength = 500;
                colvarGroupImagePath.AutoIncrement = false;
                colvarGroupImagePath.IsNullable = true;
                colvarGroupImagePath.IsPrimaryKey = false;
                colvarGroupImagePath.IsForeignKey = false;
                colvarGroupImagePath.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupImagePath);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_mgm_contact_group",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMgmContactGroup()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMgmContactGroup(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMgmContactGroup(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMgmContactGroup(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("ContactUserId")]
        [Bindable(true)]
        public int? ContactUserId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("contact_user_id");
		    }
            set 
		    {
			    SetColumnValue("contact_user_id", value);
            }
        }
	      
        [XmlAttribute("ContactName")]
        [Bindable(true)]
        public string ContactName 
	    {
		    get
		    {
			    return GetColumnValue<string>("contact_name");
		    }
            set 
		    {
			    SetColumnValue("contact_name", value);
            }
        }
	      
        [XmlAttribute("ContactNickName")]
        [Bindable(true)]
        public string ContactNickName 
	    {
		    get
		    {
			    return GetColumnValue<string>("contact_nick_name");
		    }
            set 
		    {
			    SetColumnValue("contact_nick_name", value);
            }
        }
	      
        [XmlAttribute("ContactValue")]
        [Bindable(true)]
        public string ContactValue 
	    {
		    get
		    {
			    return GetColumnValue<string>("contact_value");
		    }
            set 
		    {
			    SetColumnValue("contact_value", value);
            }
        }
	      
        [XmlAttribute("ContactType")]
        [Bindable(true)]
        public int? ContactType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("contact_type");
		    }
            set 
		    {
			    SetColumnValue("contact_type", value);
            }
        }
	      
        [XmlAttribute("ContactBirthday")]
        [Bindable(true)]
        public DateTime? ContactBirthday 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("contact_birthday");
		    }
            set 
		    {
			    SetColumnValue("contact_birthday", value);
            }
        }
	      
        [XmlAttribute("ContactId")]
        [Bindable(true)]
        public int? ContactId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("contact_id");
		    }
            set 
		    {
			    SetColumnValue("contact_id", value);
            }
        }
	      
        [XmlAttribute("GroupId")]
        [Bindable(true)]
        public int? GroupId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_id");
		    }
            set 
		    {
			    SetColumnValue("group_id", value);
            }
        }
	      
        [XmlAttribute("GroupName")]
        [Bindable(true)]
        public string GroupName 
	    {
		    get
		    {
			    return GetColumnValue<string>("group_name");
		    }
            set 
		    {
			    SetColumnValue("group_name", value);
            }
        }
	      
        [XmlAttribute("GroupImagePath")]
        [Bindable(true)]
        public string GroupImagePath 
	    {
		    get
		    {
			    return GetColumnValue<string>("group_image_path");
		    }
            set 
		    {
			    SetColumnValue("group_image_path", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string UserId = @"user_id";
            
            public static string ContactUserId = @"contact_user_id";
            
            public static string ContactName = @"contact_name";
            
            public static string ContactNickName = @"contact_nick_name";
            
            public static string ContactValue = @"contact_value";
            
            public static string ContactType = @"contact_type";
            
            public static string ContactBirthday = @"contact_birthday";
            
            public static string ContactId = @"contact_id";
            
            public static string GroupId = @"group_id";
            
            public static string GroupName = @"group_name";
            
            public static string GroupImagePath = @"group_image_path";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

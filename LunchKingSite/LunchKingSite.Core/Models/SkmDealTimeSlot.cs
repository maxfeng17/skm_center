﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the SkmDealTimeSlot class.
    /// </summary>
    [Serializable]
    public partial class SkmDealTimeSlotCollection : RepositoryList<SkmDealTimeSlot, SkmDealTimeSlotCollection>
    {
        public SkmDealTimeSlotCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmDealTimeSlotCollection</returns>
        public SkmDealTimeSlotCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmDealTimeSlot o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the skm_deal_time_slot table.
    /// </summary>
    [Serializable]
    public partial class SkmDealTimeSlot : RepositoryRecord<SkmDealTimeSlot>, IRecordBase
    {
        #region .ctors and Default Settings

        public SkmDealTimeSlot()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SkmDealTimeSlot(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("skm_deal_time_slot", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                colvarCityId.DefaultSetting = @"";
                colvarCityId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCityId);

                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = false;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                colvarSequence.DefaultSetting = @"";
                colvarSequence.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSequence);

                TableSchema.TableColumn colvarEffectiveStart = new TableSchema.TableColumn(schema);
                colvarEffectiveStart.ColumnName = "effective_start";
                colvarEffectiveStart.DataType = DbType.DateTime;
                colvarEffectiveStart.MaxLength = 0;
                colvarEffectiveStart.AutoIncrement = false;
                colvarEffectiveStart.IsNullable = false;
                colvarEffectiveStart.IsPrimaryKey = false;
                colvarEffectiveStart.IsForeignKey = false;
                colvarEffectiveStart.IsReadOnly = false;
                colvarEffectiveStart.DefaultSetting = @"";
                colvarEffectiveStart.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEffectiveStart);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarCategoryType = new TableSchema.TableColumn(schema);
                colvarCategoryType.ColumnName = "category_type";
                colvarCategoryType.DataType = DbType.Int32;
                colvarCategoryType.MaxLength = 0;
                colvarCategoryType.AutoIncrement = false;
                colvarCategoryType.IsNullable = false;
                colvarCategoryType.IsPrimaryKey = false;
                colvarCategoryType.IsForeignKey = false;
                colvarCategoryType.IsReadOnly = false;
                colvarCategoryType.DefaultSetting = @"";
                colvarCategoryType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCategoryType);

                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = false;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                colvarDealType.DefaultSetting = @"";
                colvarDealType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDealType);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("skm_deal_time_slot", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid
        {
            get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId
        {
            get { return GetColumnValue<int>(Columns.CityId); }
            set { SetColumnValue(Columns.CityId, value); }
        }

        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public int Sequence
        {
            get { return GetColumnValue<int>(Columns.Sequence); }
            set { SetColumnValue(Columns.Sequence, value); }
        }

        [XmlAttribute("EffectiveStart")]
        [Bindable(true)]
        public DateTime EffectiveStart
        {
            get { return GetColumnValue<DateTime>(Columns.EffectiveStart); }
            set { SetColumnValue(Columns.EffectiveStart, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("CategoryType")]
        [Bindable(true)]
        public int CategoryType
        {
            get { return GetColumnValue<int>(Columns.CategoryType); }
            set { SetColumnValue(Columns.CategoryType, value); }
        }

        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int DealType
        {
            get { return GetColumnValue<int>(Columns.DealType); }
            set { SetColumnValue(Columns.DealType, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn EffectiveStartColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CategoryTypeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn DealTypeColumn
        {
            get { return Schema.Columns[7]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string BusinessHourGuid = @"business_hour_guid";
            public static string CityId = @"city_id";
            public static string Sequence = @"sequence";
            public static string EffectiveStart = @"effective_start";
            public static string Status = @"status";
            public static string CategoryType = @"category_type";
            public static string DealType = @"deal_type";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpUserDeposit class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpUserDepositCollection : ReadOnlyList<ViewPcpUserDeposit, ViewPcpUserDepositCollection>
    {        
        public ViewPcpUserDepositCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_user_deposit view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpUserDeposit : ReadOnlyRecord<ViewPcpUserDeposit>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_user_deposit", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarUserDepositId = new TableSchema.TableColumn(schema);
                colvarUserDepositId.ColumnName = "user_deposit_id";
                colvarUserDepositId.DataType = DbType.Int32;
                colvarUserDepositId.MaxLength = 0;
                colvarUserDepositId.AutoIncrement = false;
                colvarUserDepositId.IsNullable = false;
                colvarUserDepositId.IsPrimaryKey = false;
                colvarUserDepositId.IsForeignKey = false;
                colvarUserDepositId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserDepositId);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
                colvarGroupId.ColumnName = "group_id";
                colvarGroupId.DataType = DbType.Int32;
                colvarGroupId.MaxLength = 0;
                colvarGroupId.AutoIncrement = false;
                colvarGroupId.IsNullable = true;
                colvarGroupId.IsPrimaryKey = false;
                colvarGroupId.IsForeignKey = false;
                colvarGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupId);
                
                TableSchema.TableColumn colvarMenuId = new TableSchema.TableColumn(schema);
                colvarMenuId.ColumnName = "menu_id";
                colvarMenuId.DataType = DbType.Int32;
                colvarMenuId.MaxLength = 0;
                colvarMenuId.AutoIncrement = false;
                colvarMenuId.IsNullable = true;
                colvarMenuId.IsPrimaryKey = false;
                colvarMenuId.IsForeignKey = false;
                colvarMenuId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMenuId);
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 100;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = true;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = false;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 100;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarTotalAmount = new TableSchema.TableColumn(schema);
                colvarTotalAmount.ColumnName = "total_amount";
                colvarTotalAmount.DataType = DbType.Int32;
                colvarTotalAmount.MaxLength = 0;
                colvarTotalAmount.AutoIncrement = false;
                colvarTotalAmount.IsNullable = false;
                colvarTotalAmount.IsPrimaryKey = false;
                colvarTotalAmount.IsForeignKey = false;
                colvarTotalAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalAmount);
                
                TableSchema.TableColumn colvarRemainAmount = new TableSchema.TableColumn(schema);
                colvarRemainAmount.ColumnName = "remain_amount";
                colvarRemainAmount.DataType = DbType.Int32;
                colvarRemainAmount.MaxLength = 0;
                colvarRemainAmount.AutoIncrement = false;
                colvarRemainAmount.IsNullable = false;
                colvarRemainAmount.IsPrimaryKey = false;
                colvarRemainAmount.IsForeignKey = false;
                colvarRemainAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemainAmount);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_user_deposit",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpUserDeposit()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpUserDeposit(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpUserDeposit(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpUserDeposit(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("UserDepositId")]
        [Bindable(true)]
        public int UserDepositId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_deposit_id");
		    }
            set 
		    {
			    SetColumnValue("user_deposit_id", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("GroupId")]
        [Bindable(true)]
        public int? GroupId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_id");
		    }
            set 
		    {
			    SetColumnValue("group_id", value);
            }
        }
	      
        [XmlAttribute("MenuId")]
        [Bindable(true)]
        public int? MenuId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("menu_id");
		    }
            set 
		    {
			    SetColumnValue("menu_id", value);
            }
        }
	      
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("title");
		    }
            set 
		    {
			    SetColumnValue("title", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int ItemId 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("TotalAmount")]
        [Bindable(true)]
        public int TotalAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("total_amount");
		    }
            set 
		    {
			    SetColumnValue("total_amount", value);
            }
        }
	      
        [XmlAttribute("RemainAmount")]
        [Bindable(true)]
        public int RemainAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("remain_amount");
		    }
            set 
		    {
			    SetColumnValue("remain_amount", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId 
	    {
		    get
		    {
			    return GetColumnValue<int>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string UserDepositId = @"user_deposit_id";
            
            public static string UserId = @"user_id";
            
            public static string GroupId = @"group_id";
            
            public static string MenuId = @"menu_id";
            
            public static string Title = @"title";
            
            public static string ItemId = @"item_id";
            
            public static string ItemName = @"item_name";
            
            public static string TotalAmount = @"total_amount";
            
            public static string RemainAmount = @"remain_amount";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

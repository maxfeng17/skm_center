using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MembershipCardGroupPermission class.
	/// </summary>
    [Serializable]
	public partial class MembershipCardGroupPermissionCollection : RepositoryList<MembershipCardGroupPermission, MembershipCardGroupPermissionCollection>
	{	   
		public MembershipCardGroupPermissionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MembershipCardGroupPermissionCollection</returns>
		public MembershipCardGroupPermissionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MembershipCardGroupPermission o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the membership_card_group_permission table.
	/// </summary>
	
	[Serializable]
	public partial class MembershipCardGroupPermission : RepositoryRecord<MembershipCardGroupPermission>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MembershipCardGroupPermission()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MembershipCardGroupPermission(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("membership_card_group_permission", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCardGroudId = new TableSchema.TableColumn(schema);
				colvarCardGroudId.ColumnName = "card_groud_id";
				colvarCardGroudId.DataType = DbType.Int32;
				colvarCardGroudId.MaxLength = 0;
				colvarCardGroudId.AutoIncrement = false;
				colvarCardGroudId.IsNullable = false;
				colvarCardGroudId.IsPrimaryKey = false;
				colvarCardGroudId.IsForeignKey = false;
				colvarCardGroudId.IsReadOnly = false;
				colvarCardGroudId.DefaultSetting = @"";
				colvarCardGroudId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardGroudId);
				
				TableSchema.TableColumn colvarScope = new TableSchema.TableColumn(schema);
				colvarScope.ColumnName = "scope";
				colvarScope.DataType = DbType.Int32;
				colvarScope.MaxLength = 0;
				colvarScope.AutoIncrement = false;
				colvarScope.IsNullable = false;
				colvarScope.IsPrimaryKey = false;
				colvarScope.IsForeignKey = false;
				colvarScope.IsReadOnly = false;
				colvarScope.DefaultSetting = @"";
				colvarScope.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScope);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("membership_card_group_permission",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("CardGroudId")]
		[Bindable(true)]
		public int CardGroudId 
		{
			get { return GetColumnValue<int>(Columns.CardGroudId); }
			set { SetColumnValue(Columns.CardGroudId, value); }
		}
		
		[XmlAttribute("Scope")]
		[Bindable(true)]
		public int Scope 
		{
			get { return GetColumnValue<int>(Columns.Scope); }
			set { SetColumnValue(Columns.Scope, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CardGroudIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ScopeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CardGroudId = @"card_groud_id";
			 public static string Scope = @"scope";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

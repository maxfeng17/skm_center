using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmBeaconMessageDeviceInfoLink class.
	/// </summary>
    [Serializable]
	public partial class SkmBeaconMessageDeviceInfoLinkCollection : RepositoryList<SkmBeaconMessageDeviceInfoLink, SkmBeaconMessageDeviceInfoLinkCollection>
	{	   
		public SkmBeaconMessageDeviceInfoLinkCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmBeaconMessageDeviceInfoLinkCollection</returns>
		public SkmBeaconMessageDeviceInfoLinkCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmBeaconMessageDeviceInfoLink o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_beacon_message_device_info_link table.
	/// </summary>
	[Serializable]
	public partial class SkmBeaconMessageDeviceInfoLink : RepositoryRecord<SkmBeaconMessageDeviceInfoLink>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmBeaconMessageDeviceInfoLink()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmBeaconMessageDeviceInfoLink(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_beacon_message_device_info_link", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = false;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSkmBeaconMessageId = new TableSchema.TableColumn(schema);
				colvarSkmBeaconMessageId.ColumnName = "skm_beacon_message_id";
				colvarSkmBeaconMessageId.DataType = DbType.Int32;
				colvarSkmBeaconMessageId.MaxLength = 0;
				colvarSkmBeaconMessageId.AutoIncrement = false;
				colvarSkmBeaconMessageId.IsNullable = false;
				colvarSkmBeaconMessageId.IsPrimaryKey = true;
				colvarSkmBeaconMessageId.IsForeignKey = false;
				colvarSkmBeaconMessageId.IsReadOnly = false;
				colvarSkmBeaconMessageId.DefaultSetting = @"";
				colvarSkmBeaconMessageId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmBeaconMessageId);
				
				TableSchema.TableColumn colvarActionEventDeviceInfoId = new TableSchema.TableColumn(schema);
				colvarActionEventDeviceInfoId.ColumnName = "action_event_device_info_id";
				colvarActionEventDeviceInfoId.DataType = DbType.Int32;
				colvarActionEventDeviceInfoId.MaxLength = 0;
				colvarActionEventDeviceInfoId.AutoIncrement = false;
				colvarActionEventDeviceInfoId.IsNullable = false;
				colvarActionEventDeviceInfoId.IsPrimaryKey = true;
				colvarActionEventDeviceInfoId.IsForeignKey = false;
				colvarActionEventDeviceInfoId.IsReadOnly = false;
				colvarActionEventDeviceInfoId.DefaultSetting = @"";
				colvarActionEventDeviceInfoId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActionEventDeviceInfoId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_beacon_message_device_info_link",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("SkmBeaconMessageId")]
		[Bindable(true)]
		public int SkmBeaconMessageId 
		{
			get { return GetColumnValue<int>(Columns.SkmBeaconMessageId); }
			set { SetColumnValue(Columns.SkmBeaconMessageId, value); }
		}
		
		[XmlAttribute("ActionEventDeviceInfoId")]
		[Bindable(true)]
		public int ActionEventDeviceInfoId 
		{
			get { return GetColumnValue<int>(Columns.ActionEventDeviceInfoId); }
			set { SetColumnValue(Columns.ActionEventDeviceInfoId, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmBeaconMessageIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionEventDeviceInfoIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SkmBeaconMessageId = @"skm_beacon_message_id";
			 public static string ActionEventDeviceInfoId = @"action_event_device_info_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

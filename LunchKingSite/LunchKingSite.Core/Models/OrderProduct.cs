using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OrderProduct class.
	/// </summary>
    [Serializable]
	public partial class OrderProductCollection : RepositoryList<OrderProduct, OrderProductCollection>
	{	   
		public OrderProductCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OrderProductCollection</returns>
		public OrderProductCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OrderProduct o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the order_product table.
	/// </summary>
	[Serializable]
	public partial class OrderProduct : RepositoryRecord<OrderProduct>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OrderProduct()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OrderProduct(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order_product", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 4000;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = false;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);
				
				TableSchema.TableColumn colvarIsOriginal = new TableSchema.TableColumn(schema);
				colvarIsOriginal.ColumnName = "is_original";
				colvarIsOriginal.DataType = DbType.Boolean;
				colvarIsOriginal.MaxLength = 0;
				colvarIsOriginal.AutoIncrement = false;
				colvarIsOriginal.IsNullable = false;
				colvarIsOriginal.IsPrimaryKey = false;
				colvarIsOriginal.IsForeignKey = false;
				colvarIsOriginal.IsReadOnly = false;
				colvarIsOriginal.DefaultSetting = @"";
				colvarIsOriginal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOriginal);
				
				TableSchema.TableColumn colvarIsCurrent = new TableSchema.TableColumn(schema);
				colvarIsCurrent.ColumnName = "is_current";
				colvarIsCurrent.DataType = DbType.Boolean;
				colvarIsCurrent.MaxLength = 0;
				colvarIsCurrent.AutoIncrement = false;
				colvarIsCurrent.IsNullable = false;
				colvarIsCurrent.IsPrimaryKey = false;
				colvarIsCurrent.IsForeignKey = false;
				colvarIsCurrent.IsReadOnly = false;
				colvarIsCurrent.DefaultSetting = @"";
				colvarIsCurrent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCurrent);
				
				TableSchema.TableColumn colvarIsReturning = new TableSchema.TableColumn(schema);
				colvarIsReturning.ColumnName = "is_returning";
				colvarIsReturning.DataType = DbType.Boolean;
				colvarIsReturning.MaxLength = 0;
				colvarIsReturning.AutoIncrement = false;
				colvarIsReturning.IsNullable = false;
				colvarIsReturning.IsPrimaryKey = false;
				colvarIsReturning.IsForeignKey = false;
				colvarIsReturning.IsReadOnly = false;
				colvarIsReturning.DefaultSetting = @"";
				colvarIsReturning.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReturning);
				
				TableSchema.TableColumn colvarIsExchanging = new TableSchema.TableColumn(schema);
				colvarIsExchanging.ColumnName = "is_exchanging";
				colvarIsExchanging.DataType = DbType.Boolean;
				colvarIsExchanging.MaxLength = 0;
				colvarIsExchanging.AutoIncrement = false;
				colvarIsExchanging.IsNullable = false;
				colvarIsExchanging.IsPrimaryKey = false;
				colvarIsExchanging.IsForeignKey = false;
				colvarIsExchanging.IsReadOnly = false;
				colvarIsExchanging.DefaultSetting = @"";
				colvarIsExchanging.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsExchanging);
				
				TableSchema.TableColumn colvarIsReturned = new TableSchema.TableColumn(schema);
				colvarIsReturned.ColumnName = "is_returned";
				colvarIsReturned.DataType = DbType.Boolean;
				colvarIsReturned.MaxLength = 0;
				colvarIsReturned.AutoIncrement = false;
				colvarIsReturned.IsNullable = false;
				colvarIsReturned.IsPrimaryKey = false;
				colvarIsReturned.IsForeignKey = false;
				colvarIsReturned.IsReadOnly = false;
				colvarIsReturned.DefaultSetting = @"";
				colvarIsReturned.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReturned);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order_product",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name 
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}
		  
		[XmlAttribute("IsOriginal")]
		[Bindable(true)]
		public bool IsOriginal 
		{
			get { return GetColumnValue<bool>(Columns.IsOriginal); }
			set { SetColumnValue(Columns.IsOriginal, value); }
		}
		  
		[XmlAttribute("IsCurrent")]
		[Bindable(true)]
		public bool IsCurrent 
		{
			get { return GetColumnValue<bool>(Columns.IsCurrent); }
			set { SetColumnValue(Columns.IsCurrent, value); }
		}
		  
		[XmlAttribute("IsReturning")]
		[Bindable(true)]
		public bool IsReturning 
		{
			get { return GetColumnValue<bool>(Columns.IsReturning); }
			set { SetColumnValue(Columns.IsReturning, value); }
		}
		  
		[XmlAttribute("IsExchanging")]
		[Bindable(true)]
		public bool IsExchanging 
		{
			get { return GetColumnValue<bool>(Columns.IsExchanging); }
			set { SetColumnValue(Columns.IsExchanging, value); }
		}
		  
		[XmlAttribute("IsReturned")]
		[Bindable(true)]
		public bool IsReturned 
		{
			get { return GetColumnValue<bool>(Columns.IsReturned); }
			set { SetColumnValue(Columns.IsReturned, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (1)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOriginalColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCurrentColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReturningColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IsExchangingColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReturnedColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderGuid = @"order_guid";
			 public static string Name = @"name";
			 public static string IsOriginal = @"is_original";
			 public static string IsCurrent = @"is_current";
			 public static string IsReturning = @"is_returning";
			 public static string IsExchanging = @"is_exchanging";
			 public static string IsReturned = @"is_returned";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

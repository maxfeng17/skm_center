using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpSellerMenuItem class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpSellerMenuItemCollection : ReadOnlyList<ViewPcpSellerMenuItem, ViewPcpSellerMenuItemCollection>
    {        
        public ViewPcpSellerMenuItemCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_seller_menu_item view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpSellerMenuItem : ReadOnlyRecord<ViewPcpSellerMenuItem>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_seller_menu_item", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarMenuId = new TableSchema.TableColumn(schema);
                colvarMenuId.ColumnName = "menu_id";
                colvarMenuId.DataType = DbType.Int32;
                colvarMenuId.MaxLength = 0;
                colvarMenuId.AutoIncrement = false;
                colvarMenuId.IsNullable = false;
                colvarMenuId.IsPrimaryKey = false;
                colvarMenuId.IsForeignKey = false;
                colvarMenuId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMenuId);
                
                TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
                colvarGroupId.ColumnName = "group_id";
                colvarGroupId.DataType = DbType.Int32;
                colvarGroupId.MaxLength = 0;
                colvarGroupId.AutoIncrement = false;
                colvarGroupId.IsNullable = false;
                colvarGroupId.IsPrimaryKey = false;
                colvarGroupId.IsForeignKey = false;
                colvarGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupId);
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 100;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = false;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = true;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 100;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarDefaultAmount = new TableSchema.TableColumn(schema);
                colvarDefaultAmount.ColumnName = "default_amount";
                colvarDefaultAmount.DataType = DbType.Int32;
                colvarDefaultAmount.MaxLength = 0;
                colvarDefaultAmount.AutoIncrement = false;
                colvarDefaultAmount.IsNullable = false;
                colvarDefaultAmount.IsPrimaryKey = false;
                colvarDefaultAmount.IsForeignKey = false;
                colvarDefaultAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarDefaultAmount);
                
                TableSchema.TableColumn colvarSTime = new TableSchema.TableColumn(schema);
                colvarSTime.ColumnName = "s_time";
                colvarSTime.DataType = DbType.DateTime;
                colvarSTime.MaxLength = 0;
                colvarSTime.AutoIncrement = false;
                colvarSTime.IsNullable = false;
                colvarSTime.IsPrimaryKey = false;
                colvarSTime.IsForeignKey = false;
                colvarSTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarSTime);
                
                TableSchema.TableColumn colvarETime = new TableSchema.TableColumn(schema);
                colvarETime.ColumnName = "e_time";
                colvarETime.DataType = DbType.DateTime;
                colvarETime.MaxLength = 0;
                colvarETime.AutoIncrement = false;
                colvarETime.IsNullable = false;
                colvarETime.IsPrimaryKey = false;
                colvarETime.IsForeignKey = false;
                colvarETime.IsReadOnly = false;
                
                schema.Columns.Add(colvarETime);
                
                TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
                colvarEnabled.ColumnName = "enabled";
                colvarEnabled.DataType = DbType.Int32;
                colvarEnabled.MaxLength = 0;
                colvarEnabled.AutoIncrement = false;
                colvarEnabled.IsNullable = false;
                colvarEnabled.IsPrimaryKey = false;
                colvarEnabled.IsForeignKey = false;
                colvarEnabled.IsReadOnly = false;
                
                schema.Columns.Add(colvarEnabled);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_seller_menu_item",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpSellerMenuItem()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpSellerMenuItem(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpSellerMenuItem(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpSellerMenuItem(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("MenuId")]
        [Bindable(true)]
        public int MenuId 
	    {
		    get
		    {
			    return GetColumnValue<int>("menu_id");
		    }
            set 
		    {
			    SetColumnValue("menu_id", value);
            }
        }
	      
        [XmlAttribute("GroupId")]
        [Bindable(true)]
        public int GroupId 
	    {
		    get
		    {
			    return GetColumnValue<int>("group_id");
		    }
            set 
		    {
			    SetColumnValue("group_id", value);
            }
        }
	      
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("title");
		    }
            set 
		    {
			    SetColumnValue("title", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int? ItemId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("DefaultAmount")]
        [Bindable(true)]
        public int DefaultAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("default_amount");
		    }
            set 
		    {
			    SetColumnValue("default_amount", value);
            }
        }
	      
        [XmlAttribute("STime")]
        [Bindable(true)]
        public DateTime STime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("s_time");
		    }
            set 
		    {
			    SetColumnValue("s_time", value);
            }
        }
	      
        [XmlAttribute("ETime")]
        [Bindable(true)]
        public DateTime ETime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("e_time");
		    }
            set 
		    {
			    SetColumnValue("e_time", value);
            }
        }
	      
        [XmlAttribute("Enabled")]
        [Bindable(true)]
        public int Enabled 
	    {
		    get
		    {
			    return GetColumnValue<int>("enabled");
		    }
            set 
		    {
			    SetColumnValue("enabled", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string MenuId = @"menu_id";
            
            public static string GroupId = @"group_id";
            
            public static string Title = @"title";
            
            public static string ItemId = @"item_id";
            
            public static string ItemName = @"item_name";
            
            public static string DefaultAmount = @"default_amount";
            
            public static string STime = @"s_time";
            
            public static string ETime = @"e_time";
            
            public static string Enabled = @"enabled";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

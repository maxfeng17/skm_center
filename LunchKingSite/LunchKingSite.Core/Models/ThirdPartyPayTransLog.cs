using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ThirdPartyPayTransLog class.
	/// </summary>
    [Serializable]
	public partial class ThirdPartyPayTransLogCollection : RepositoryList<ThirdPartyPayTransLog, ThirdPartyPayTransLogCollection>
	{	   
		public ThirdPartyPayTransLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ThirdPartyPayTransLogCollection</returns>
		public ThirdPartyPayTransLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ThirdPartyPayTransLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the third_party_pay_trans_log table.
	/// </summary>
	[Serializable]
	public partial class ThirdPartyPayTransLog : RepositoryRecord<ThirdPartyPayTransLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ThirdPartyPayTransLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ThirdPartyPayTransLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("third_party_pay_trans_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarPaymentOrg = new TableSchema.TableColumn(schema);
				colvarPaymentOrg.ColumnName = "payment_org";
				colvarPaymentOrg.DataType = DbType.Int32;
				colvarPaymentOrg.MaxLength = 0;
				colvarPaymentOrg.AutoIncrement = false;
				colvarPaymentOrg.IsNullable = false;
				colvarPaymentOrg.IsPrimaryKey = false;
				colvarPaymentOrg.IsForeignKey = false;
				colvarPaymentOrg.IsReadOnly = false;
				colvarPaymentOrg.DefaultSetting = @"";
				colvarPaymentOrg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentOrg);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarOneTimeCode = new TableSchema.TableColumn(schema);
				colvarOneTimeCode.ColumnName = "one_time_code";
				colvarOneTimeCode.DataType = DbType.AnsiString;
				colvarOneTimeCode.MaxLength = 50;
				colvarOneTimeCode.AutoIncrement = false;
				colvarOneTimeCode.IsNullable = true;
				colvarOneTimeCode.IsPrimaryKey = false;
				colvarOneTimeCode.IsForeignKey = false;
				colvarOneTimeCode.IsReadOnly = false;
				colvarOneTimeCode.DefaultSetting = @"";
				colvarOneTimeCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOneTimeCode);
				
				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 50;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = true;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);
				
				TableSchema.TableColumn colvarOrderName = new TableSchema.TableColumn(schema);
				colvarOrderName.ColumnName = "order_name";
				colvarOrderName.DataType = DbType.String;
				colvarOrderName.MaxLength = 255;
				colvarOrderName.AutoIncrement = false;
				colvarOrderName.IsNullable = true;
				colvarOrderName.IsPrimaryKey = false;
				colvarOrderName.IsForeignKey = false;
				colvarOrderName.IsReadOnly = false;
				colvarOrderName.DefaultSetting = @"";
				colvarOrderName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderName);
				
				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Int32;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = false;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);
				
				TableSchema.TableColumn colvarTransId = new TableSchema.TableColumn(schema);
				colvarTransId.ColumnName = "trans_id";
				colvarTransId.DataType = DbType.AnsiString;
				colvarTransId.MaxLength = 40;
				colvarTransId.AutoIncrement = false;
				colvarTransId.IsNullable = true;
				colvarTransId.IsPrimaryKey = false;
				colvarTransId.IsForeignKey = false;
				colvarTransId.IsReadOnly = false;
				colvarTransId.DefaultSetting = @"";
				colvarTransId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransId);
				
				TableSchema.TableColumn colvarTicketId = new TableSchema.TableColumn(schema);
				colvarTicketId.ColumnName = "ticket_id";
				colvarTicketId.DataType = DbType.String;
				colvarTicketId.MaxLength = 100;
				colvarTicketId.AutoIncrement = false;
				colvarTicketId.IsNullable = true;
				colvarTicketId.IsPrimaryKey = false;
				colvarTicketId.IsForeignKey = false;
				colvarTicketId.IsReadOnly = false;
				colvarTicketId.DefaultSetting = @"";
				colvarTicketId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTicketId);
				
				TableSchema.TableColumn colvarPaymentStatus = new TableSchema.TableColumn(schema);
				colvarPaymentStatus.ColumnName = "payment_status";
				colvarPaymentStatus.DataType = DbType.Int32;
				colvarPaymentStatus.MaxLength = 0;
				colvarPaymentStatus.AutoIncrement = false;
				colvarPaymentStatus.IsNullable = true;
				colvarPaymentStatus.IsPrimaryKey = false;
				colvarPaymentStatus.IsForeignKey = false;
				colvarPaymentStatus.IsReadOnly = false;
				colvarPaymentStatus.DefaultSetting = @"";
				colvarPaymentStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentStatus);
				
				TableSchema.TableColumn colvarReturnCode = new TableSchema.TableColumn(schema);
				colvarReturnCode.ColumnName = "return_code";
				colvarReturnCode.DataType = DbType.Int32;
				colvarReturnCode.MaxLength = 0;
				colvarReturnCode.AutoIncrement = false;
				colvarReturnCode.IsNullable = true;
				colvarReturnCode.IsPrimaryKey = false;
				colvarReturnCode.IsForeignKey = false;
				colvarReturnCode.IsReadOnly = false;
				colvarReturnCode.DefaultSetting = @"";
				colvarReturnCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnCode);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("third_party_pay_trans_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("PaymentOrg")]
		[Bindable(true)]
		public int PaymentOrg 
		{
			get { return GetColumnValue<int>(Columns.PaymentOrg); }
			set { SetColumnValue(Columns.PaymentOrg, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("OneTimeCode")]
		[Bindable(true)]
		public string OneTimeCode 
		{
			get { return GetColumnValue<string>(Columns.OneTimeCode); }
			set { SetColumnValue(Columns.OneTimeCode, value); }
		}
		  
		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId 
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}
		  
		[XmlAttribute("OrderName")]
		[Bindable(true)]
		public string OrderName 
		{
			get { return GetColumnValue<string>(Columns.OrderName); }
			set { SetColumnValue(Columns.OrderName, value); }
		}
		  
		[XmlAttribute("Amount")]
		[Bindable(true)]
		public int Amount 
		{
			get { return GetColumnValue<int>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}
		  
		[XmlAttribute("TransId")]
		[Bindable(true)]
		public string TransId 
		{
			get { return GetColumnValue<string>(Columns.TransId); }
			set { SetColumnValue(Columns.TransId, value); }
		}
		  
		[XmlAttribute("TicketId")]
		[Bindable(true)]
		public string TicketId 
		{
			get { return GetColumnValue<string>(Columns.TicketId); }
			set { SetColumnValue(Columns.TicketId, value); }
		}
		  
		[XmlAttribute("PaymentStatus")]
		[Bindable(true)]
		public int? PaymentStatus 
		{
			get { return GetColumnValue<int?>(Columns.PaymentStatus); }
			set { SetColumnValue(Columns.PaymentStatus, value); }
		}
		  
		[XmlAttribute("ReturnCode")]
		[Bindable(true)]
		public int? ReturnCode 
		{
			get { return GetColumnValue<int?>(Columns.ReturnCode); }
			set { SetColumnValue(Columns.ReturnCode, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PaymentOrgColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OneTimeCodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderNameColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn TransIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn TicketIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn PaymentStatusColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnCodeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string PaymentOrg = @"payment_org";
			 public static string UserId = @"user_id";
			 public static string OneTimeCode = @"one_time_code";
			 public static string OrderId = @"order_id";
			 public static string OrderName = @"order_name";
			 public static string Amount = @"amount";
			 public static string TransId = @"trans_id";
			 public static string TicketId = @"ticket_id";
			 public static string PaymentStatus = @"payment_status";
			 public static string ReturnCode = @"return_code";
			 public static string CreateTime = @"create_time";
			 public static string ModifyTime = @"modify_time";
			 public static string OrderGuid = @"order_guid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

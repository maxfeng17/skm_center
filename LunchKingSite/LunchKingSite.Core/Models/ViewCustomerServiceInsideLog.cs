using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewCustomerServiceInsideLog class.
    /// </summary>
    [Serializable]
    public partial class ViewCustomerServiceInsideLogCollection : ReadOnlyList<ViewCustomerServiceInsideLog, ViewCustomerServiceInsideLogCollection>
    {        
        public ViewCustomerServiceInsideLogCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_customer_service_inside_log view.
    /// </summary>
    [Serializable]
    public partial class ViewCustomerServiceInsideLog : ReadOnlyRecord<ViewCustomerServiceInsideLog>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_customer_service_inside_log", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarServiceNo = new TableSchema.TableColumn(schema);
                colvarServiceNo.ColumnName = "service_no";
                colvarServiceNo.DataType = DbType.AnsiString;
                colvarServiceNo.MaxLength = 50;
                colvarServiceNo.AutoIncrement = false;
                colvarServiceNo.IsNullable = false;
                colvarServiceNo.IsPrimaryKey = false;
                colvarServiceNo.IsForeignKey = false;
                colvarServiceNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarServiceNo);
                
                TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
                colvarContent.ColumnName = "content";
                colvarContent.DataType = DbType.String;
                colvarContent.MaxLength = -1;
                colvarContent.AutoIncrement = false;
                colvarContent.IsNullable = true;
                colvarContent.IsPrimaryKey = false;
                colvarContent.IsForeignKey = false;
                colvarContent.IsReadOnly = false;
                
                schema.Columns.Add(colvarContent);
                
                TableSchema.TableColumn colvarFile = new TableSchema.TableColumn(schema);
                colvarFile.ColumnName = "file";
                colvarFile.DataType = DbType.String;
                colvarFile.MaxLength = 100;
                colvarFile.AutoIncrement = false;
                colvarFile.IsNullable = true;
                colvarFile.IsPrimaryKey = false;
                colvarFile.IsForeignKey = false;
                colvarFile.IsReadOnly = false;
                
                schema.Columns.Add(colvarFile);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 100;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = false;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
                colvarMemo.ColumnName = "memo";
                colvarMemo.DataType = DbType.String;
                colvarMemo.MaxLength = 250;
                colvarMemo.AutoIncrement = false;
                colvarMemo.IsNullable = true;
                colvarMemo.IsPrimaryKey = false;
                colvarMemo.IsForeignKey = false;
                colvarMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemo);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_customer_service_inside_log", schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewCustomerServiceInsideLog()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewCustomerServiceInsideLog(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if (useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewCustomerServiceInsideLog(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewCustomerServiceInsideLog(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ServiceNo")]
        [Bindable(true)]
        public string ServiceNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("service_no");
		    }
            set 
		    {
			    SetColumnValue("service_no", value);
            }
        }
	      
        [XmlAttribute("Content")]
        [Bindable(true)]
        public string Content 
	    {
		    get
		    {
			    return GetColumnValue<string>("content");
		    }
            set 
		    {
			    SetColumnValue("content", value);
            }
        }
	      
        [XmlAttribute("File")]
        [Bindable(true)]
        public string File 
	    {
		    get
		    {
			    return GetColumnValue<string>("file");
		    }
            set 
		    {
			    SetColumnValue("file", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("Memo")]
        [Bindable(true)]
        public string Memo 
	    {
		    get
		    {
			    return GetColumnValue<string>("memo");
		    }
            set 
		    {
			    SetColumnValue("memo", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ServiceNo = @"service_no";
            
            public static string Content = @"content";
            
            public static string File = @"file";
            
            public static string Status = @"status";
            
            public static string CreateTime = @"create_time";
            
            public static string Name = @"name";
            
            public static string Memo = @"memo";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

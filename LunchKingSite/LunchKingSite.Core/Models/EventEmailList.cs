using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the EventEmailList class.
	/// </summary>
    [Serializable]
	public partial class EventEmailListCollection : RepositoryList<EventEmailList, EventEmailListCollection>
	{	   
		public EventEmailListCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EventEmailListCollection</returns>
		public EventEmailListCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EventEmailList o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the event_email_list table.
	/// </summary>
	[Serializable]
	public partial class EventEmailList : RepositoryRecord<EventEmailList>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public EventEmailList()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public EventEmailList(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("event_email_list", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarSi = new TableSchema.TableColumn(schema);
				colvarSi.ColumnName = "si";
				colvarSi.DataType = DbType.Int64;
				colvarSi.MaxLength = 0;
				colvarSi.AutoIncrement = true;
				colvarSi.IsNullable = false;
				colvarSi.IsPrimaryKey = true;
				colvarSi.IsForeignKey = false;
				colvarSi.IsReadOnly = false;
				colvarSi.DefaultSetting = @"";
				colvarSi.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSi);
				
				TableSchema.TableColumn colvarEid = new TableSchema.TableColumn(schema);
				colvarEid.ColumnName = "eid";
				colvarEid.DataType = DbType.AnsiString;
				colvarEid.MaxLength = 50;
				colvarEid.AutoIncrement = false;
				colvarEid.IsNullable = true;
				colvarEid.IsPrimaryKey = false;
				colvarEid.IsForeignKey = false;
				colvarEid.IsReadOnly = false;
				colvarEid.DefaultSetting = @"";
				colvarEid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEid);
				
				TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
				colvarEmail.ColumnName = "email";
				colvarEmail.DataType = DbType.AnsiString;
				colvarEmail.MaxLength = 100;
				colvarEmail.AutoIncrement = false;
				colvarEmail.IsNullable = true;
				colvarEmail.IsPrimaryKey = false;
				colvarEmail.IsForeignKey = false;
				colvarEmail.IsReadOnly = false;
				colvarEmail.DefaultSetting = @"";
				colvarEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmail);
				
				TableSchema.TableColumn colvarCreattime = new TableSchema.TableColumn(schema);
				colvarCreattime.ColumnName = "creattime";
				colvarCreattime.DataType = DbType.DateTime;
				colvarCreattime.MaxLength = 0;
				colvarCreattime.AutoIncrement = false;
				colvarCreattime.IsNullable = true;
				colvarCreattime.IsPrimaryKey = false;
				colvarCreattime.IsForeignKey = false;
				colvarCreattime.IsReadOnly = false;
				
						colvarCreattime.DefaultSetting = @"(getdate())";
				colvarCreattime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreattime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("event_email_list",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Si")]
		[Bindable(true)]
		public long Si 
		{
			get { return GetColumnValue<long>(Columns.Si); }
			set { SetColumnValue(Columns.Si, value); }
		}
		  
		[XmlAttribute("Eid")]
		[Bindable(true)]
		public string Eid 
		{
			get { return GetColumnValue<string>(Columns.Eid); }
			set { SetColumnValue(Columns.Eid, value); }
		}
		  
		[XmlAttribute("Email")]
		[Bindable(true)]
		public string Email 
		{
			get { return GetColumnValue<string>(Columns.Email); }
			set { SetColumnValue(Columns.Email, value); }
		}
		  
		[XmlAttribute("Creattime")]
		[Bindable(true)]
		public DateTime? Creattime 
		{
			get { return GetColumnValue<DateTime?>(Columns.Creattime); }
			set { SetColumnValue(Columns.Creattime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn SiColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn EmailColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreattimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Si = @"si";
			 public static string Eid = @"eid";
			 public static string Email = @"email";
			 public static string Creattime = @"creattime";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

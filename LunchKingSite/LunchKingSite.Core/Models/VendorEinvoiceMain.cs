using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VendorEinvoiceMain class.
	/// </summary>
    [Serializable]
	public partial class VendorEinvoiceMainCollection : RepositoryList<VendorEinvoiceMain, VendorEinvoiceMainCollection>
	{	   
		public VendorEinvoiceMainCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VendorEinvoiceMainCollection</returns>
		public VendorEinvoiceMainCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VendorEinvoiceMain o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the vendor_einvoice_main table.
	/// </summary>
	[Serializable]
	public partial class VendorEinvoiceMain : RepositoryRecord<VendorEinvoiceMain>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VendorEinvoiceMain()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VendorEinvoiceMain(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("vendor_einvoice_main", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarInvoiceNumber = new TableSchema.TableColumn(schema);
				colvarInvoiceNumber.ColumnName = "invoice_number";
				colvarInvoiceNumber.DataType = DbType.String;
				colvarInvoiceNumber.MaxLength = 10;
				colvarInvoiceNumber.AutoIncrement = false;
				colvarInvoiceNumber.IsNullable = true;
				colvarInvoiceNumber.IsPrimaryKey = false;
				colvarInvoiceNumber.IsForeignKey = false;
				colvarInvoiceNumber.IsReadOnly = false;
				colvarInvoiceNumber.DefaultSetting = @"";
				colvarInvoiceNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceNumber);
				
				TableSchema.TableColumn colvarInvoiceNumberTime = new TableSchema.TableColumn(schema);
				colvarInvoiceNumberTime.ColumnName = "invoice_number_time";
				colvarInvoiceNumberTime.DataType = DbType.DateTime;
				colvarInvoiceNumberTime.MaxLength = 0;
				colvarInvoiceNumberTime.AutoIncrement = false;
				colvarInvoiceNumberTime.IsNullable = true;
				colvarInvoiceNumberTime.IsPrimaryKey = false;
				colvarInvoiceNumberTime.IsForeignKey = false;
				colvarInvoiceNumberTime.IsReadOnly = false;
				colvarInvoiceNumberTime.DefaultSetting = @"";
				colvarInvoiceNumberTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceNumberTime);
				
				TableSchema.TableColumn colvarInvoiceComId = new TableSchema.TableColumn(schema);
				colvarInvoiceComId.ColumnName = "invoice_com_id";
				colvarInvoiceComId.DataType = DbType.String;
				colvarInvoiceComId.MaxLength = 10;
				colvarInvoiceComId.AutoIncrement = false;
				colvarInvoiceComId.IsNullable = true;
				colvarInvoiceComId.IsPrimaryKey = false;
				colvarInvoiceComId.IsForeignKey = false;
				colvarInvoiceComId.IsReadOnly = false;
				colvarInvoiceComId.DefaultSetting = @"";
				colvarInvoiceComId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceComId);
				
				TableSchema.TableColumn colvarInvoiceComName = new TableSchema.TableColumn(schema);
				colvarInvoiceComName.ColumnName = "invoice_com_name";
				colvarInvoiceComName.DataType = DbType.String;
				colvarInvoiceComName.MaxLength = 250;
				colvarInvoiceComName.AutoIncrement = false;
				colvarInvoiceComName.IsNullable = true;
				colvarInvoiceComName.IsPrimaryKey = false;
				colvarInvoiceComName.IsForeignKey = false;
				colvarInvoiceComName.IsReadOnly = false;
				colvarInvoiceComName.DefaultSetting = @"";
				colvarInvoiceComName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceComName);
				
				TableSchema.TableColumn colvarInvoiceStatus = new TableSchema.TableColumn(schema);
				colvarInvoiceStatus.ColumnName = "invoice_status";
				colvarInvoiceStatus.DataType = DbType.Int32;
				colvarInvoiceStatus.MaxLength = 0;
				colvarInvoiceStatus.AutoIncrement = false;
				colvarInvoiceStatus.IsNullable = false;
				colvarInvoiceStatus.IsPrimaryKey = false;
				colvarInvoiceStatus.IsForeignKey = false;
				colvarInvoiceStatus.IsReadOnly = false;
				colvarInvoiceStatus.DefaultSetting = @"";
				colvarInvoiceStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceStatus);
				
				TableSchema.TableColumn colvarInvoiceMode2 = new TableSchema.TableColumn(schema);
				colvarInvoiceMode2.ColumnName = "invoice_mode2";
				colvarInvoiceMode2.DataType = DbType.Int32;
				colvarInvoiceMode2.MaxLength = 0;
				colvarInvoiceMode2.AutoIncrement = false;
				colvarInvoiceMode2.IsNullable = false;
				colvarInvoiceMode2.IsPrimaryKey = false;
				colvarInvoiceMode2.IsForeignKey = false;
				colvarInvoiceMode2.IsReadOnly = false;
				colvarInvoiceMode2.DefaultSetting = @"";
				colvarInvoiceMode2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceMode2);
				
				TableSchema.TableColumn colvarInvoicePass = new TableSchema.TableColumn(schema);
				colvarInvoicePass.ColumnName = "invoice_pass";
				colvarInvoicePass.DataType = DbType.AnsiStringFixedLength;
				colvarInvoicePass.MaxLength = 4;
				colvarInvoicePass.AutoIncrement = false;
				colvarInvoicePass.IsNullable = false;
				colvarInvoicePass.IsPrimaryKey = false;
				colvarInvoicePass.IsForeignKey = false;
				colvarInvoicePass.IsReadOnly = false;
				colvarInvoicePass.DefaultSetting = @"";
				colvarInvoicePass.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoicePass);
				
				TableSchema.TableColumn colvarInvoicePapered = new TableSchema.TableColumn(schema);
				colvarInvoicePapered.ColumnName = "invoice_papered";
				colvarInvoicePapered.DataType = DbType.Boolean;
				colvarInvoicePapered.MaxLength = 0;
				colvarInvoicePapered.AutoIncrement = false;
				colvarInvoicePapered.IsNullable = false;
				colvarInvoicePapered.IsPrimaryKey = false;
				colvarInvoicePapered.IsForeignKey = false;
				colvarInvoicePapered.IsReadOnly = false;
				colvarInvoicePapered.DefaultSetting = @"";
				colvarInvoicePapered.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoicePapered);
				
				TableSchema.TableColumn colvarInvoicePaperedTime = new TableSchema.TableColumn(schema);
				colvarInvoicePaperedTime.ColumnName = "invoice_papered_time";
				colvarInvoicePaperedTime.DataType = DbType.DateTime;
				colvarInvoicePaperedTime.MaxLength = 0;
				colvarInvoicePaperedTime.AutoIncrement = false;
				colvarInvoicePaperedTime.IsNullable = true;
				colvarInvoicePaperedTime.IsPrimaryKey = false;
				colvarInvoicePaperedTime.IsForeignKey = false;
				colvarInvoicePaperedTime.IsReadOnly = false;
				colvarInvoicePaperedTime.DefaultSetting = @"";
				colvarInvoicePaperedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoicePaperedTime);
				
				TableSchema.TableColumn colvarOrderAmount = new TableSchema.TableColumn(schema);
				colvarOrderAmount.ColumnName = "order_amount";
				colvarOrderAmount.DataType = DbType.Currency;
				colvarOrderAmount.MaxLength = 0;
				colvarOrderAmount.AutoIncrement = false;
				colvarOrderAmount.IsNullable = false;
				colvarOrderAmount.IsPrimaryKey = false;
				colvarOrderAmount.IsForeignKey = false;
				colvarOrderAmount.IsReadOnly = false;
				colvarOrderAmount.DefaultSetting = @"";
				colvarOrderAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderAmount);
				
				TableSchema.TableColumn colvarInvoiceTax = new TableSchema.TableColumn(schema);
				colvarInvoiceTax.ColumnName = "invoice_tax";
				colvarInvoiceTax.DataType = DbType.Decimal;
				colvarInvoiceTax.MaxLength = 0;
				colvarInvoiceTax.AutoIncrement = false;
				colvarInvoiceTax.IsNullable = false;
				colvarInvoiceTax.IsPrimaryKey = false;
				colvarInvoiceTax.IsForeignKey = false;
				colvarInvoiceTax.IsReadOnly = false;
				colvarInvoiceTax.DefaultSetting = @"";
				colvarInvoiceTax.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceTax);
				
				TableSchema.TableColumn colvarCarrierType = new TableSchema.TableColumn(schema);
				colvarCarrierType.ColumnName = "carrier_type";
				colvarCarrierType.DataType = DbType.Int32;
				colvarCarrierType.MaxLength = 0;
				colvarCarrierType.AutoIncrement = false;
				colvarCarrierType.IsNullable = false;
				colvarCarrierType.IsPrimaryKey = false;
				colvarCarrierType.IsForeignKey = false;
				colvarCarrierType.IsReadOnly = false;
				colvarCarrierType.DefaultSetting = @"";
				colvarCarrierType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCarrierType);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarIsUpload = new TableSchema.TableColumn(schema);
				colvarIsUpload.ColumnName = "is_upload";
				colvarIsUpload.DataType = DbType.Boolean;
				colvarIsUpload.MaxLength = 0;
				colvarIsUpload.AutoIncrement = false;
				colvarIsUpload.IsNullable = false;
				colvarIsUpload.IsPrimaryKey = false;
				colvarIsUpload.IsForeignKey = false;
				colvarIsUpload.IsReadOnly = false;
				colvarIsUpload.DefaultSetting = @"";
				colvarIsUpload.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsUpload);
				
				TableSchema.TableColumn colvarAccountantName = new TableSchema.TableColumn(schema);
				colvarAccountantName.ColumnName = "accountant_name";
				colvarAccountantName.DataType = DbType.String;
				colvarAccountantName.MaxLength = 20;
				colvarAccountantName.AutoIncrement = false;
				colvarAccountantName.IsNullable = false;
				colvarAccountantName.IsPrimaryKey = false;
				colvarAccountantName.IsForeignKey = false;
				colvarAccountantName.IsReadOnly = false;
				colvarAccountantName.DefaultSetting = @"";
				colvarAccountantName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountantName);
				
				TableSchema.TableColumn colvarSellerAddress = new TableSchema.TableColumn(schema);
				colvarSellerAddress.ColumnName = "seller_address";
				colvarSellerAddress.DataType = DbType.String;
				colvarSellerAddress.MaxLength = 250;
				colvarSellerAddress.AutoIncrement = false;
				colvarSellerAddress.IsNullable = false;
				colvarSellerAddress.IsPrimaryKey = false;
				colvarSellerAddress.IsForeignKey = false;
				colvarSellerAddress.IsReadOnly = false;
				colvarSellerAddress.DefaultSetting = @"";
				colvarSellerAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerAddress);
				
				TableSchema.TableColumn colvarShippingFee = new TableSchema.TableColumn(schema);
				colvarShippingFee.ColumnName = "shipping_fee";
				colvarShippingFee.DataType = DbType.Int32;
				colvarShippingFee.MaxLength = 0;
				colvarShippingFee.AutoIncrement = false;
				colvarShippingFee.IsNullable = false;
				colvarShippingFee.IsPrimaryKey = false;
				colvarShippingFee.IsForeignKey = false;
				colvarShippingFee.IsReadOnly = false;
				
						colvarShippingFee.DefaultSetting = @"((0))";
				colvarShippingFee.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShippingFee);
				
				TableSchema.TableColumn colvarOverdueAmount = new TableSchema.TableColumn(schema);
				colvarOverdueAmount.ColumnName = "overdue_amount";
				colvarOverdueAmount.DataType = DbType.Int32;
				colvarOverdueAmount.MaxLength = 0;
				colvarOverdueAmount.AutoIncrement = false;
				colvarOverdueAmount.IsNullable = false;
				colvarOverdueAmount.IsPrimaryKey = false;
				colvarOverdueAmount.IsForeignKey = false;
				colvarOverdueAmount.IsReadOnly = false;
				
						colvarOverdueAmount.DefaultSetting = @"((0))";
				colvarOverdueAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOverdueAmount);
				
				TableSchema.TableColumn colvarCancelDate = new TableSchema.TableColumn(schema);
				colvarCancelDate.ColumnName = "cancel_date";
				colvarCancelDate.DataType = DbType.DateTime;
				colvarCancelDate.MaxLength = 0;
				colvarCancelDate.AutoIncrement = false;
				colvarCancelDate.IsNullable = true;
				colvarCancelDate.IsPrimaryKey = false;
				colvarCancelDate.IsForeignKey = false;
				colvarCancelDate.IsReadOnly = false;
				colvarCancelDate.DefaultSetting = @"";
				colvarCancelDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelDate);
				
				TableSchema.TableColumn colvarWmsAmount = new TableSchema.TableColumn(schema);
				colvarWmsAmount.ColumnName = "wms_amount";
				colvarWmsAmount.DataType = DbType.Int32;
				colvarWmsAmount.MaxLength = 0;
				colvarWmsAmount.AutoIncrement = false;
				colvarWmsAmount.IsNullable = false;
				colvarWmsAmount.IsPrimaryKey = false;
				colvarWmsAmount.IsForeignKey = false;
				colvarWmsAmount.IsReadOnly = false;
				
						colvarWmsAmount.DefaultSetting = @"((0))";
				colvarWmsAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWmsAmount);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("vendor_einvoice_main",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("InvoiceNumber")]
		[Bindable(true)]
		public string InvoiceNumber 
		{
			get { return GetColumnValue<string>(Columns.InvoiceNumber); }
			set { SetColumnValue(Columns.InvoiceNumber, value); }
		}
		  
		[XmlAttribute("InvoiceNumberTime")]
		[Bindable(true)]
		public DateTime? InvoiceNumberTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.InvoiceNumberTime); }
			set { SetColumnValue(Columns.InvoiceNumberTime, value); }
		}
		  
		[XmlAttribute("InvoiceComId")]
		[Bindable(true)]
		public string InvoiceComId 
		{
			get { return GetColumnValue<string>(Columns.InvoiceComId); }
			set { SetColumnValue(Columns.InvoiceComId, value); }
		}
		  
		[XmlAttribute("InvoiceComName")]
		[Bindable(true)]
		public string InvoiceComName 
		{
			get { return GetColumnValue<string>(Columns.InvoiceComName); }
			set { SetColumnValue(Columns.InvoiceComName, value); }
		}
		  
		[XmlAttribute("InvoiceStatus")]
		[Bindable(true)]
		public int InvoiceStatus 
		{
			get { return GetColumnValue<int>(Columns.InvoiceStatus); }
			set { SetColumnValue(Columns.InvoiceStatus, value); }
		}
		  
		[XmlAttribute("InvoiceMode2")]
		[Bindable(true)]
		public int InvoiceMode2 
		{
			get { return GetColumnValue<int>(Columns.InvoiceMode2); }
			set { SetColumnValue(Columns.InvoiceMode2, value); }
		}
		  
		[XmlAttribute("InvoicePass")]
		[Bindable(true)]
		public string InvoicePass 
		{
			get { return GetColumnValue<string>(Columns.InvoicePass); }
			set { SetColumnValue(Columns.InvoicePass, value); }
		}
		  
		[XmlAttribute("InvoicePapered")]
		[Bindable(true)]
		public bool InvoicePapered 
		{
			get { return GetColumnValue<bool>(Columns.InvoicePapered); }
			set { SetColumnValue(Columns.InvoicePapered, value); }
		}
		  
		[XmlAttribute("InvoicePaperedTime")]
		[Bindable(true)]
		public DateTime? InvoicePaperedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.InvoicePaperedTime); }
			set { SetColumnValue(Columns.InvoicePaperedTime, value); }
		}
		  
		[XmlAttribute("OrderAmount")]
		[Bindable(true)]
		public decimal OrderAmount 
		{
			get { return GetColumnValue<decimal>(Columns.OrderAmount); }
			set { SetColumnValue(Columns.OrderAmount, value); }
		}
		  
		[XmlAttribute("InvoiceTax")]
		[Bindable(true)]
		public decimal InvoiceTax 
		{
			get { return GetColumnValue<decimal>(Columns.InvoiceTax); }
			set { SetColumnValue(Columns.InvoiceTax, value); }
		}
		  
		[XmlAttribute("CarrierType")]
		[Bindable(true)]
		public int CarrierType 
		{
			get { return GetColumnValue<int>(Columns.CarrierType); }
			set { SetColumnValue(Columns.CarrierType, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("IsUpload")]
		[Bindable(true)]
		public bool IsUpload 
		{
			get { return GetColumnValue<bool>(Columns.IsUpload); }
			set { SetColumnValue(Columns.IsUpload, value); }
		}
		  
		[XmlAttribute("AccountantName")]
		[Bindable(true)]
		public string AccountantName 
		{
			get { return GetColumnValue<string>(Columns.AccountantName); }
			set { SetColumnValue(Columns.AccountantName, value); }
		}
		  
		[XmlAttribute("SellerAddress")]
		[Bindable(true)]
		public string SellerAddress 
		{
			get { return GetColumnValue<string>(Columns.SellerAddress); }
			set { SetColumnValue(Columns.SellerAddress, value); }
		}
		  
		[XmlAttribute("ShippingFee")]
		[Bindable(true)]
		public int ShippingFee 
		{
			get { return GetColumnValue<int>(Columns.ShippingFee); }
			set { SetColumnValue(Columns.ShippingFee, value); }
		}
		  
		[XmlAttribute("OverdueAmount")]
		[Bindable(true)]
		public int OverdueAmount 
		{
			get { return GetColumnValue<int>(Columns.OverdueAmount); }
			set { SetColumnValue(Columns.OverdueAmount, value); }
		}
		  
		[XmlAttribute("CancelDate")]
		[Bindable(true)]
		public DateTime? CancelDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.CancelDate); }
			set { SetColumnValue(Columns.CancelDate, value); }
		}
		  
		[XmlAttribute("WmsAmount")]
		[Bindable(true)]
		public int WmsAmount 
		{
			get { return GetColumnValue<int>(Columns.WmsAmount); }
			set { SetColumnValue(Columns.WmsAmount, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceNumberColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceNumberTimeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceComIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceComNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceStatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceMode2Column
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoicePassColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoicePaperedColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoicePaperedTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderAmountColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceTaxColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CarrierTypeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn IsUploadColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountantNameColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerAddressColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ShippingFeeColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn OverdueAmountColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelDateColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn WmsAmountColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string InvoiceNumber = @"invoice_number";
			 public static string InvoiceNumberTime = @"invoice_number_time";
			 public static string InvoiceComId = @"invoice_com_id";
			 public static string InvoiceComName = @"invoice_com_name";
			 public static string InvoiceStatus = @"invoice_status";
			 public static string InvoiceMode2 = @"invoice_mode2";
			 public static string InvoicePass = @"invoice_pass";
			 public static string InvoicePapered = @"invoice_papered";
			 public static string InvoicePaperedTime = @"invoice_papered_time";
			 public static string OrderAmount = @"order_amount";
			 public static string InvoiceTax = @"invoice_tax";
			 public static string CarrierType = @"carrier_type";
			 public static string SellerGuid = @"seller_guid";
			 public static string IsUpload = @"is_upload";
			 public static string AccountantName = @"accountant_name";
			 public static string SellerAddress = @"seller_address";
			 public static string ShippingFee = @"shipping_fee";
			 public static string OverdueAmount = @"overdue_amount";
			 public static string CancelDate = @"cancel_date";
			 public static string WmsAmount = @"wms_amount";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBookingSystemReservationList class.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemReservationListCollection : ReadOnlyList<ViewBookingSystemReservationList, ViewBookingSystemReservationListCollection>
    {
        public ViewBookingSystemReservationListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_booking_system_reservation_list view.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemReservationList : ReadOnlyRecord<ViewBookingSystemReservationList>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_booking_system_reservation_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarBookingSystemReservationId = new TableSchema.TableColumn(schema);
                colvarBookingSystemReservationId.ColumnName = "booking_system_reservation_id";
                colvarBookingSystemReservationId.DataType = DbType.Int32;
                colvarBookingSystemReservationId.MaxLength = 0;
                colvarBookingSystemReservationId.AutoIncrement = false;
                colvarBookingSystemReservationId.IsNullable = false;
                colvarBookingSystemReservationId.IsPrimaryKey = false;
                colvarBookingSystemReservationId.IsForeignKey = false;
                colvarBookingSystemReservationId.IsReadOnly = false;

                schema.Columns.Add(colvarBookingSystemReservationId);

                TableSchema.TableColumn colvarBookingSystemStoreBookingId = new TableSchema.TableColumn(schema);
                colvarBookingSystemStoreBookingId.ColumnName = "booking_system_store_booking_id";
                colvarBookingSystemStoreBookingId.DataType = DbType.Int32;
                colvarBookingSystemStoreBookingId.MaxLength = 0;
                colvarBookingSystemStoreBookingId.AutoIncrement = false;
                colvarBookingSystemStoreBookingId.IsNullable = false;
                colvarBookingSystemStoreBookingId.IsPrimaryKey = false;
                colvarBookingSystemStoreBookingId.IsForeignKey = false;
                colvarBookingSystemStoreBookingId.IsReadOnly = false;

                schema.Columns.Add(colvarBookingSystemStoreBookingId);

                TableSchema.TableColumn colvarReservationDate = new TableSchema.TableColumn(schema);
                colvarReservationDate.ColumnName = "reservation_date";
                colvarReservationDate.DataType = DbType.DateTime;
                colvarReservationDate.MaxLength = 0;
                colvarReservationDate.AutoIncrement = false;
                colvarReservationDate.IsNullable = false;
                colvarReservationDate.IsPrimaryKey = false;
                colvarReservationDate.IsForeignKey = false;
                colvarReservationDate.IsReadOnly = false;

                schema.Columns.Add(colvarReservationDate);

                TableSchema.TableColumn colvarNumberOfPeople = new TableSchema.TableColumn(schema);
                colvarNumberOfPeople.ColumnName = "number_of_people";
                colvarNumberOfPeople.DataType = DbType.Int32;
                colvarNumberOfPeople.MaxLength = 0;
                colvarNumberOfPeople.AutoIncrement = false;
                colvarNumberOfPeople.IsNullable = false;
                colvarNumberOfPeople.IsPrimaryKey = false;
                colvarNumberOfPeople.IsForeignKey = false;
                colvarNumberOfPeople.IsReadOnly = false;

                schema.Columns.Add(colvarNumberOfPeople);

                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 256;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = false;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;

                schema.Columns.Add(colvarRemark);

                TableSchema.TableColumn colvarContactName = new TableSchema.TableColumn(schema);
                colvarContactName.ColumnName = "contact_name";
                colvarContactName.DataType = DbType.String;
                colvarContactName.MaxLength = 30;
                colvarContactName.AutoIncrement = false;
                colvarContactName.IsNullable = false;
                colvarContactName.IsPrimaryKey = false;
                colvarContactName.IsForeignKey = false;
                colvarContactName.IsReadOnly = false;

                schema.Columns.Add(colvarContactName);

                TableSchema.TableColumn colvarContactGender = new TableSchema.TableColumn(schema);
                colvarContactGender.ColumnName = "contact_Gender";
                colvarContactGender.DataType = DbType.Boolean;
                colvarContactGender.MaxLength = 0;
                colvarContactGender.AutoIncrement = false;
                colvarContactGender.IsNullable = false;
                colvarContactGender.IsPrimaryKey = false;
                colvarContactGender.IsForeignKey = false;
                colvarContactGender.IsReadOnly = false;

                schema.Columns.Add(colvarContactGender);

                TableSchema.TableColumn colvarContactNumber = new TableSchema.TableColumn(schema);
                colvarContactNumber.ColumnName = "contact_number";
                colvarContactNumber.DataType = DbType.AnsiString;
                colvarContactNumber.MaxLength = 12;
                colvarContactNumber.AutoIncrement = false;
                colvarContactNumber.IsNullable = false;
                colvarContactNumber.IsPrimaryKey = false;
                colvarContactNumber.IsForeignKey = false;
                colvarContactNumber.IsReadOnly = false;

                schema.Columns.Add(colvarContactNumber);

                TableSchema.TableColumn colvarIsCheckin = new TableSchema.TableColumn(schema);
                colvarIsCheckin.ColumnName = "is_checkin";
                colvarIsCheckin.DataType = DbType.Boolean;
                colvarIsCheckin.MaxLength = 0;
                colvarIsCheckin.AutoIncrement = false;
                colvarIsCheckin.IsNullable = false;
                colvarIsCheckin.IsPrimaryKey = false;
                colvarIsCheckin.IsForeignKey = false;
                colvarIsCheckin.IsReadOnly = false;

                schema.Columns.Add(colvarIsCheckin);

                TableSchema.TableColumn colvarIsCancel = new TableSchema.TableColumn(schema);
                colvarIsCancel.ColumnName = "is_cancel";
                colvarIsCancel.DataType = DbType.Boolean;
                colvarIsCancel.MaxLength = 0;
                colvarIsCancel.AutoIncrement = false;
                colvarIsCancel.IsNullable = false;
                colvarIsCancel.IsPrimaryKey = false;
                colvarIsCancel.IsForeignKey = false;
                colvarIsCancel.IsReadOnly = false;

                schema.Columns.Add(colvarIsCancel);

                TableSchema.TableColumn colvarCreateDatetime = new TableSchema.TableColumn(schema);
                colvarCreateDatetime.ColumnName = "create_datetime";
                colvarCreateDatetime.DataType = DbType.DateTime;
                colvarCreateDatetime.MaxLength = 0;
                colvarCreateDatetime.AutoIncrement = false;
                colvarCreateDatetime.IsNullable = false;
                colvarCreateDatetime.IsPrimaryKey = false;
                colvarCreateDatetime.IsForeignKey = false;
                colvarCreateDatetime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateDatetime);

                TableSchema.TableColumn colvarTimeSlot = new TableSchema.TableColumn(schema);
                colvarTimeSlot.ColumnName = "time_slot";
                colvarTimeSlot.DataType = DbType.AnsiString;
                colvarTimeSlot.MaxLength = 5;
                colvarTimeSlot.AutoIncrement = false;
                colvarTimeSlot.IsNullable = false;
                colvarTimeSlot.IsPrimaryKey = false;
                colvarTimeSlot.IsForeignKey = false;
                colvarTimeSlot.IsReadOnly = false;

                schema.Columns.Add(colvarTimeSlot);

                TableSchema.TableColumn colvarCouponInfo = new TableSchema.TableColumn(schema);
                colvarCouponInfo.ColumnName = "coupon_info";
                colvarCouponInfo.DataType = DbType.String;
                colvarCouponInfo.MaxLength = 256;
                colvarCouponInfo.AutoIncrement = false;
                colvarCouponInfo.IsNullable = true;
                colvarCouponInfo.IsPrimaryKey = false;
                colvarCouponInfo.IsForeignKey = false;
                colvarCouponInfo.IsReadOnly = false;

                schema.Columns.Add(colvarCouponInfo);

                TableSchema.TableColumn colvarCouponLink = new TableSchema.TableColumn(schema);
                colvarCouponLink.ColumnName = "coupon_link";
                colvarCouponLink.DataType = DbType.AnsiString;
                colvarCouponLink.MaxLength = 128;
                colvarCouponLink.AutoIncrement = false;
                colvarCouponLink.IsNullable = true;
                colvarCouponLink.IsPrimaryKey = false;
                colvarCouponLink.IsForeignKey = false;
                colvarCouponLink.IsReadOnly = false;

                schema.Columns.Add(colvarCouponLink);

                TableSchema.TableColumn colvarIsLock = new TableSchema.TableColumn(schema);
                colvarIsLock.ColumnName = "is_lock";
                colvarIsLock.DataType = DbType.Boolean;
                colvarIsLock.MaxLength = 0;
                colvarIsLock.AutoIncrement = false;
                colvarIsLock.IsNullable = false;
                colvarIsLock.IsPrimaryKey = false;
                colvarIsLock.IsForeignKey = false;
                colvarIsLock.IsReadOnly = false;

                schema.Columns.Add(colvarIsLock);

                TableSchema.TableColumn colvarContactEmail = new TableSchema.TableColumn(schema);
                colvarContactEmail.ColumnName = "contact_email";
                colvarContactEmail.DataType = DbType.String;
                colvarContactEmail.MaxLength = 256;
                colvarContactEmail.AutoIncrement = false;
                colvarContactEmail.IsNullable = false;
                colvarContactEmail.IsPrimaryKey = false;
                colvarContactEmail.IsForeignKey = false;
                colvarContactEmail.IsReadOnly = false;

                schema.Columns.Add(colvarContactEmail);

                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;

                schema.Columns.Add(colvarSellerName);

                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;

                schema.Columns.Add(colvarStoreName);

                TableSchema.TableColumn colvarAddressString = new TableSchema.TableColumn(schema);
                colvarAddressString.ColumnName = "address_string";
                colvarAddressString.DataType = DbType.String;
                colvarAddressString.MaxLength = 140;
                colvarAddressString.AutoIncrement = false;
                colvarAddressString.IsNullable = true;
                colvarAddressString.IsPrimaryKey = false;
                colvarAddressString.IsForeignKey = false;
                colvarAddressString.IsReadOnly = false;

                schema.Columns.Add(colvarAddressString);

                TableSchema.TableColumn colvarBookingType = new TableSchema.TableColumn(schema);
                colvarBookingType.ColumnName = "booking_type";
                colvarBookingType.DataType = DbType.Int32;
                colvarBookingType.MaxLength = 0;
                colvarBookingType.AutoIncrement = false;
                colvarBookingType.IsNullable = false;
                colvarBookingType.IsPrimaryKey = false;
                colvarBookingType.IsForeignKey = false;
                colvarBookingType.IsReadOnly = false;

                schema.Columns.Add(colvarBookingType);

                TableSchema.TableColumn colvarBookingSystemDateId = new TableSchema.TableColumn(schema);
                colvarBookingSystemDateId.ColumnName = "booking_system_date_id";
                colvarBookingSystemDateId.DataType = DbType.Int32;
                colvarBookingSystemDateId.MaxLength = 0;
                colvarBookingSystemDateId.AutoIncrement = false;
                colvarBookingSystemDateId.IsNullable = false;
                colvarBookingSystemDateId.IsPrimaryKey = false;
                colvarBookingSystemDateId.IsForeignKey = false;
                colvarBookingSystemDateId.IsReadOnly = false;

                schema.Columns.Add(colvarBookingSystemDateId);

                TableSchema.TableColumn colvarIsReturn = new TableSchema.TableColumn(schema);
                colvarIsReturn.ColumnName = "Is_Return";
                colvarIsReturn.DataType = DbType.Boolean;
                colvarIsReturn.MaxLength = 0;
                colvarIsReturn.AutoIncrement = false;
                colvarIsReturn.IsNullable = false;
                colvarIsReturn.IsPrimaryKey = false;
                colvarIsReturn.IsForeignKey = false;
                colvarIsReturn.IsReadOnly = false;

                schema.Columns.Add(colvarIsReturn);

                TableSchema.TableColumn colvarIsPartialCancel = new TableSchema.TableColumn(schema);
                colvarIsPartialCancel.ColumnName = "Is_PartialCancel";
                colvarIsPartialCancel.DataType = DbType.Boolean;
                colvarIsPartialCancel.MaxLength = 0;
                colvarIsPartialCancel.AutoIncrement = false;
                colvarIsPartialCancel.IsNullable = false;
                colvarIsPartialCancel.IsPrimaryKey = false;
                colvarIsPartialCancel.IsForeignKey = false;
                colvarIsPartialCancel.IsReadOnly = false;

                schema.Columns.Add(colvarIsPartialCancel);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_booking_system_reservation_list", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewBookingSystemReservationList()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBookingSystemReservationList(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewBookingSystemReservationList(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewBookingSystemReservationList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("BookingSystemReservationId")]
        [Bindable(true)]
        public int BookingSystemReservationId
        {
            get
            {
                return GetColumnValue<int>("booking_system_reservation_id");
            }
            set
            {
                SetColumnValue("booking_system_reservation_id", value);
            }
        }

        [XmlAttribute("BookingSystemStoreBookingId")]
        [Bindable(true)]
        public int BookingSystemStoreBookingId
        {
            get
            {
                return GetColumnValue<int>("booking_system_store_booking_id");
            }
            set
            {
                SetColumnValue("booking_system_store_booking_id", value);
            }
        }

        [XmlAttribute("ReservationDate")]
        [Bindable(true)]
        public DateTime ReservationDate
        {
            get
            {
                return GetColumnValue<DateTime>("reservation_date");
            }
            set
            {
                SetColumnValue("reservation_date", value);
            }
        }

        [XmlAttribute("NumberOfPeople")]
        [Bindable(true)]
        public int NumberOfPeople
        {
            get
            {
                return GetColumnValue<int>("number_of_people");
            }
            set
            {
                SetColumnValue("number_of_people", value);
            }
        }

        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark
        {
            get
            {
                return GetColumnValue<string>("remark");
            }
            set
            {
                SetColumnValue("remark", value);
            }
        }

        [XmlAttribute("ContactName")]
        [Bindable(true)]
        public string ContactName
        {
            get
            {
                return GetColumnValue<string>("contact_name");
            }
            set
            {
                SetColumnValue("contact_name", value);
            }
        }

        [XmlAttribute("ContactGender")]
        [Bindable(true)]
        public bool ContactGender
        {
            get
            {
                return GetColumnValue<bool>("contact_Gender");
            }
            set
            {
                SetColumnValue("contact_Gender", value);
            }
        }

        [XmlAttribute("ContactNumber")]
        [Bindable(true)]
        public string ContactNumber
        {
            get
            {
                return GetColumnValue<string>("contact_number");
            }
            set
            {
                SetColumnValue("contact_number", value);
            }
        }

        [XmlAttribute("IsCheckin")]
        [Bindable(true)]
        public bool IsCheckin
        {
            get
            {
                return GetColumnValue<bool>("is_checkin");
            }
            set
            {
                SetColumnValue("is_checkin", value);
            }
        }

        [XmlAttribute("IsCancel")]
        [Bindable(true)]
        public bool IsCancel
        {
            get
            {
                return GetColumnValue<bool>("is_cancel");
            }
            set
            {
                SetColumnValue("is_cancel", value);
            }
        }

        [XmlAttribute("CreateDatetime")]
        [Bindable(true)]
        public DateTime CreateDatetime
        {
            get
            {
                return GetColumnValue<DateTime>("create_datetime");
            }
            set
            {
                SetColumnValue("create_datetime", value);
            }
        }

        [XmlAttribute("TimeSlot")]
        [Bindable(true)]
        public string TimeSlot
        {
            get
            {
                return GetColumnValue<string>("time_slot");
            }
            set
            {
                SetColumnValue("time_slot", value);
            }
        }

        [XmlAttribute("CouponInfo")]
        [Bindable(true)]
        public string CouponInfo
        {
            get
            {
                return GetColumnValue<string>("coupon_info");
            }
            set
            {
                SetColumnValue("coupon_info", value);
            }
        }

        [XmlAttribute("CouponLink")]
        [Bindable(true)]
        public string CouponLink
        {
            get
            {
                return GetColumnValue<string>("coupon_link");
            }
            set
            {
                SetColumnValue("coupon_link", value);
            }
        }

        [XmlAttribute("IsLock")]
        [Bindable(true)]
        public bool IsLock
        {
            get
            {
                return GetColumnValue<bool>("is_lock");
            }
            set
            {
                SetColumnValue("is_lock", value);
            }
        }

        [XmlAttribute("ContactEmail")]
        [Bindable(true)]
        public string ContactEmail
        {
            get
            {
                return GetColumnValue<string>("contact_email");
            }
            set
            {
                SetColumnValue("contact_email", value);
            }
        }

        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName
        {
            get
            {
                return GetColumnValue<string>("seller_name");
            }
            set
            {
                SetColumnValue("seller_name", value);
            }
        }

        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName
        {
            get
            {
                return GetColumnValue<string>("store_name");
            }
            set
            {
                SetColumnValue("store_name", value);
            }
        }

        [XmlAttribute("AddressString")]
        [Bindable(true)]
        public string AddressString
        {
            get
            {
                return GetColumnValue<string>("address_string");
            }
            set
            {
                SetColumnValue("address_string", value);
            }
        }

        [XmlAttribute("BookingType")]
        [Bindable(true)]
        public int BookingType
        {
            get
            {
                return GetColumnValue<int>("booking_type");
            }
            set
            {
                SetColumnValue("booking_type", value);
            }
        }

        [XmlAttribute("BookingSystemDateId")]
        [Bindable(true)]
        public int BookingSystemDateId
        {
            get
            {
                return GetColumnValue<int>("booking_system_date_id");
            }
            set
            {
                SetColumnValue("booking_system_date_id", value);
            }
        }

        [XmlAttribute("IsReturn")]
        [Bindable(true)]
        public bool IsReturn
        {
            get
            {
                return GetColumnValue<bool>("Is_Return");
            }
            set
            {
                SetColumnValue("Is_Return", value);
            }
        }

        [XmlAttribute("IsPartialCancel")]
        [Bindable(true)]
        public bool IsPartialCancel
        {
            get
            {
                return GetColumnValue<bool>("Is_PartialCancel");
            }
            set
            {
                SetColumnValue("Is_PartialCancel", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string BookingSystemReservationId = @"booking_system_reservation_id";

            public static string BookingSystemStoreBookingId = @"booking_system_store_booking_id";

            public static string ReservationDate = @"reservation_date";

            public static string NumberOfPeople = @"number_of_people";

            public static string Remark = @"remark";

            public static string ContactName = @"contact_name";

            public static string ContactGender = @"contact_Gender";

            public static string ContactNumber = @"contact_number";

            public static string IsCheckin = @"is_checkin";

            public static string IsCancel = @"is_cancel";

            public static string CreateDatetime = @"create_datetime";

            public static string TimeSlot = @"time_slot";

            public static string CouponInfo = @"coupon_info";

            public static string CouponLink = @"coupon_link";

            public static string IsLock = @"is_lock";

            public static string ContactEmail = @"contact_email";

            public static string SellerName = @"seller_name";

            public static string StoreName = @"store_name";

            public static string AddressString = @"address_string";

            public static string BookingType = @"booking_type";

            public static string BookingSystemDateId = @"booking_system_date_id";

            public static string IsReturn = @"Is_Return";

            public static string IsPartialCancel = @"Is_PartialCancel";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

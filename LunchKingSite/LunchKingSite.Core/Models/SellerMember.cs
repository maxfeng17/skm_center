using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the SellerMember class.
    /// </summary>
    [Serializable]
    public partial class SellerMemberCollection : RepositoryList<SellerMember, SellerMemberCollection>
    {
        public SellerMemberCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SellerMemberCollection</returns>
        public SellerMemberCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SellerMember o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the seller_member table.
    /// </summary>
    [Serializable]
    public partial class SellerMember : RepositoryRecord<SellerMember>, IRecordBase
    {
        #region .ctors and Default Settings

        public SellerMember()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SellerMember(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("seller_member", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = false;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;
                colvarSellerUserId.DefaultSetting = @"";
                colvarSellerUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerUserId);

                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.String;
                colvarMobile.MaxLength = 10;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = false;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                colvarMobile.DefaultSetting = @"";
                colvarMobile.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMobile);

                TableSchema.TableColumn colvarGender = new TableSchema.TableColumn(schema);
                colvarGender.ColumnName = "gender";
                colvarGender.DataType = DbType.Int32;
                colvarGender.MaxLength = 0;
                colvarGender.AutoIncrement = false;
                colvarGender.IsNullable = true;
                colvarGender.IsPrimaryKey = false;
                colvarGender.IsForeignKey = false;
                colvarGender.IsReadOnly = false;
                colvarGender.DefaultSetting = @"";
                colvarGender.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGender);

                TableSchema.TableColumn colvarBirthday = new TableSchema.TableColumn(schema);
                colvarBirthday.ColumnName = "birthday";
                colvarBirthday.DataType = DbType.DateTime;
                colvarBirthday.MaxLength = 0;
                colvarBirthday.AutoIncrement = false;
                colvarBirthday.IsNullable = true;
                colvarBirthday.IsPrimaryKey = false;
                colvarBirthday.IsForeignKey = false;
                colvarBirthday.IsReadOnly = false;
                colvarBirthday.DefaultSetting = @"";
                colvarBirthday.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBirthday);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.Int32;
                colvarModifyId.MaxLength = 0;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = true;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarRemarks = new TableSchema.TableColumn(schema);
                colvarRemarks.ColumnName = "remarks";
                colvarRemarks.DataType = DbType.String;
                colvarRemarks.MaxLength = -1;
                colvarRemarks.AutoIncrement = false;
                colvarRemarks.IsNullable = true;
                colvarRemarks.IsPrimaryKey = false;
                colvarRemarks.IsForeignKey = false;
                colvarRemarks.IsReadOnly = false;
                colvarRemarks.DefaultSetting = @"";
                colvarRemarks.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRemarks);

                TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
                colvarLastName.ColumnName = "last_name";
                colvarLastName.DataType = DbType.String;
                colvarLastName.MaxLength = 50;
                colvarLastName.AutoIncrement = false;
                colvarLastName.IsNullable = true;
                colvarLastName.IsPrimaryKey = false;
                colvarLastName.IsForeignKey = false;
                colvarLastName.IsReadOnly = false;
                colvarLastName.DefaultSetting = @"";
                colvarLastName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastName);

                TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
                colvarFirstName.ColumnName = "first_name";
                colvarFirstName.DataType = DbType.String;
                colvarFirstName.MaxLength = 50;
                colvarFirstName.AutoIncrement = false;
                colvarFirstName.IsNullable = true;
                colvarFirstName.IsPrimaryKey = false;
                colvarFirstName.IsForeignKey = false;
                colvarFirstName.IsReadOnly = false;
                colvarFirstName.DefaultSetting = @"";
                colvarFirstName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFirstName);

                TableSchema.TableColumn colvarContactEmail = new TableSchema.TableColumn(schema);
                colvarContactEmail.ColumnName = "contact_email";
                colvarContactEmail.DataType = DbType.String;
                colvarContactEmail.MaxLength = 100;
                colvarContactEmail.AutoIncrement = false;
                colvarContactEmail.IsNullable = true;
                colvarContactEmail.IsPrimaryKey = false;
                colvarContactEmail.IsForeignKey = false;
                colvarContactEmail.IsReadOnly = false;
                colvarContactEmail.DefaultSetting = @"";
                colvarContactEmail.ForeignKeyTableName = "";
                schema.Columns.Add(colvarContactEmail);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("seller_member", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int SellerUserId
        {
            get { return GetColumnValue<int>(Columns.SellerUserId); }
            set { SetColumnValue(Columns.SellerUserId, value); }
        }

        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile
        {
            get { return GetColumnValue<string>(Columns.Mobile); }
            set { SetColumnValue(Columns.Mobile, value); }
        }

        [XmlAttribute("Gender")]
        [Bindable(true)]
        public int? Gender
        {
            get { return GetColumnValue<int?>(Columns.Gender); }
            set { SetColumnValue(Columns.Gender, value); }
        }

        [XmlAttribute("Birthday")]
        [Bindable(true)]
        public DateTime? Birthday
        {
            get { return GetColumnValue<DateTime?>(Columns.Birthday); }
            set { SetColumnValue(Columns.Birthday, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId
        {
            get { return GetColumnValue<int>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public int? ModifyId
        {
            get { return GetColumnValue<int?>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int? UserId
        {
            get { return GetColumnValue<int?>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("Remarks")]
        [Bindable(true)]
        public string Remarks
        {
            get { return GetColumnValue<string>(Columns.Remarks); }
            set { SetColumnValue(Columns.Remarks, value); }
        }

        [XmlAttribute("LastName")]
        [Bindable(true)]
        public string LastName
        {
            get { return GetColumnValue<string>(Columns.LastName); }
            set { SetColumnValue(Columns.LastName, value); }
        }

        [XmlAttribute("FirstName")]
        [Bindable(true)]
        public string FirstName
        {
            get { return GetColumnValue<string>(Columns.FirstName); }
            set { SetColumnValue(Columns.FirstName, value); }
        }

        [XmlAttribute("ContactEmail")]
        [Bindable(true)]
        public string ContactEmail
        {
            get { return GetColumnValue<string>(Columns.ContactEmail); }
            set { SetColumnValue(Columns.ContactEmail, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn SellerUserIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn MobileColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn GenderColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn BirthdayColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn RemarksColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn LastNameColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn FirstNameColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn ContactEmailColumn
        {
            get { return Schema.Columns[13]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string SellerUserId = @"seller_user_id";
            public static string Mobile = @"mobile";
            public static string Gender = @"gender";
            public static string Birthday = @"birthday";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string ModifyTime = @"modify_time";
            public static string ModifyId = @"modify_id";
            public static string UserId = @"user_id";
            public static string Remarks = @"remarks";
            public static string LastName = @"last_name";
            public static string FirstName = @"first_name";
            public static string ContactEmail = @"contact_email";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

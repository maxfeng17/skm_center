using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MgmGiftMatch class.
	/// </summary>
    [Serializable]
	public partial class MgmGiftMatchCollection : RepositoryList<MgmGiftMatch, MgmGiftMatchCollection>
	{	   
		public MgmGiftMatchCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MgmGiftMatchCollection</returns>
		public MgmGiftMatchCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MgmGiftMatch o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the mgm_gift_match table.
	/// </summary>
	[Serializable]
	public partial class MgmGiftMatch : RepositoryRecord<MgmGiftMatch>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MgmGiftMatch()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MgmGiftMatch(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("mgm_gift_match", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarMgmGiftVersionId = new TableSchema.TableColumn(schema);
				colvarMgmGiftVersionId.ColumnName = "mgm_gift_version_id";
				colvarMgmGiftVersionId.DataType = DbType.Int32;
				colvarMgmGiftVersionId.MaxLength = 0;
				colvarMgmGiftVersionId.AutoIncrement = false;
				colvarMgmGiftVersionId.IsNullable = false;
				colvarMgmGiftVersionId.IsPrimaryKey = false;
				colvarMgmGiftVersionId.IsForeignKey = false;
				colvarMgmGiftVersionId.IsReadOnly = false;
				colvarMgmGiftVersionId.DefaultSetting = @"";
				colvarMgmGiftVersionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMgmGiftVersionId);
				
				TableSchema.TableColumn colvarMatchType = new TableSchema.TableColumn(schema);
				colvarMatchType.ColumnName = "match_type";
				colvarMatchType.DataType = DbType.Int32;
				colvarMatchType.MaxLength = 0;
				colvarMatchType.AutoIncrement = false;
				colvarMatchType.IsNullable = false;
				colvarMatchType.IsPrimaryKey = false;
				colvarMatchType.IsForeignKey = false;
				colvarMatchType.IsReadOnly = false;
				colvarMatchType.DefaultSetting = @"";
				colvarMatchType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMatchType);
				
				TableSchema.TableColumn colvarMatchValue = new TableSchema.TableColumn(schema);
				colvarMatchValue.ColumnName = "match_value";
				colvarMatchValue.DataType = DbType.String;
				colvarMatchValue.MaxLength = 200;
				colvarMatchValue.AutoIncrement = false;
				colvarMatchValue.IsNullable = true;
				colvarMatchValue.IsPrimaryKey = false;
				colvarMatchValue.IsForeignKey = false;
				colvarMatchValue.IsReadOnly = false;
				colvarMatchValue.DefaultSetting = @"";
				colvarMatchValue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMatchValue);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("mgm_gift_match",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("MgmGiftVersionId")]
		[Bindable(true)]
		public int MgmGiftVersionId 
		{
			get { return GetColumnValue<int>(Columns.MgmGiftVersionId); }
			set { SetColumnValue(Columns.MgmGiftVersionId, value); }
		}
		  
		[XmlAttribute("MatchType")]
		[Bindable(true)]
		public int MatchType 
		{
			get { return GetColumnValue<int>(Columns.MatchType); }
			set { SetColumnValue(Columns.MatchType, value); }
		}
		  
		[XmlAttribute("MatchValue")]
		[Bindable(true)]
		public string MatchValue 
		{
			get { return GetColumnValue<string>(Columns.MatchValue); }
			set { SetColumnValue(Columns.MatchValue, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn MgmGiftVersionIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn MatchTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MatchValueColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string MgmGiftVersionId = @"mgm_gift_version_id";
			 public static string MatchType = @"match_type";
			 public static string MatchValue = @"match_value";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ShoppingGuideQuery class.
	/// </summary>
    [Serializable]
	public partial class ShoppingGuideQueryCollection : RepositoryList<ShoppingGuideQuery, ShoppingGuideQueryCollection>
	{	   
		public ShoppingGuideQueryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ShoppingGuideQueryCollection</returns>
		public ShoppingGuideQueryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ShoppingGuideQuery o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the shopping_guide_query table.
	/// </summary>
	
	[Serializable]
	public partial class ShoppingGuideQuery : RepositoryRecord<ShoppingGuideQuery>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ShoppingGuideQuery()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ShoppingGuideQuery(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("shopping_guide_query", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarQueryToken = new TableSchema.TableColumn(schema);
				colvarQueryToken.ColumnName = "query_token";
				colvarQueryToken.DataType = DbType.Guid;
				colvarQueryToken.MaxLength = 0;
				colvarQueryToken.AutoIncrement = false;
				colvarQueryToken.IsNullable = false;
				colvarQueryToken.IsPrimaryKey = false;
				colvarQueryToken.IsForeignKey = false;
				colvarQueryToken.IsReadOnly = false;
				colvarQueryToken.DefaultSetting = @"";
				colvarQueryToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQueryToken);
				
				TableSchema.TableColumn colvarAppId = new TableSchema.TableColumn(schema);
				colvarAppId.ColumnName = "app_id";
				colvarAppId.DataType = DbType.AnsiString;
				colvarAppId.MaxLength = 50;
				colvarAppId.AutoIncrement = false;
				colvarAppId.IsNullable = false;
				colvarAppId.IsPrimaryKey = false;
				colvarAppId.IsForeignKey = false;
				colvarAppId.IsReadOnly = false;
				colvarAppId.DefaultSetting = @"";
				colvarAppId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppId);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarExpiredTime = new TableSchema.TableColumn(schema);
				colvarExpiredTime.ColumnName = "expired_time";
				colvarExpiredTime.DataType = DbType.DateTime;
				colvarExpiredTime.MaxLength = 0;
				colvarExpiredTime.AutoIncrement = false;
				colvarExpiredTime.IsNullable = false;
				colvarExpiredTime.IsPrimaryKey = false;
				colvarExpiredTime.IsForeignKey = false;
				colvarExpiredTime.IsReadOnly = false;
				colvarExpiredTime.DefaultSetting = @"";
				colvarExpiredTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExpiredTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("shopping_guide_query",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("QueryToken")]
		[Bindable(true)]
		public Guid QueryToken 
		{
			get { return GetColumnValue<Guid>(Columns.QueryToken); }
			set { SetColumnValue(Columns.QueryToken, value); }
		}
		
		[XmlAttribute("AppId")]
		[Bindable(true)]
		public string AppId 
		{
			get { return GetColumnValue<string>(Columns.AppId); }
			set { SetColumnValue(Columns.AppId, value); }
		}
		
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("ExpiredTime")]
		[Bindable(true)]
		public DateTime ExpiredTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ExpiredTime); }
			set { SetColumnValue(Columns.ExpiredTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn QueryTokenColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AppIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ExpiredTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string QueryToken = @"query_token";
			 public static string AppId = @"app_id";
			 public static string Bid = @"bid";
			 public static string CreateTime = @"create_time";
			 public static string ExpiredTime = @"expired_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

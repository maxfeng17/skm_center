using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the WmsRefundShipFee class.
	/// </summary>
    [Serializable]
	public partial class WmsRefundShipFeeCollection : RepositoryList<WmsRefundShipFee, WmsRefundShipFeeCollection>
	{	   
		public WmsRefundShipFeeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>WmsRefundShipFeeCollection</returns>
		public WmsRefundShipFeeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                WmsRefundShipFee o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the wms_refund_ship_fee table.
	/// </summary>
	[Serializable]
	public partial class WmsRefundShipFee : RepositoryRecord<WmsRefundShipFee>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public WmsRefundShipFee()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public WmsRefundShipFee(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("wms_refund_ship_fee", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarVendorOrderId = new TableSchema.TableColumn(schema);
				colvarVendorOrderId.ColumnName = "vendor_order_id";
				colvarVendorOrderId.DataType = DbType.String;
				colvarVendorOrderId.MaxLength = 66;
				colvarVendorOrderId.AutoIncrement = false;
				colvarVendorOrderId.IsNullable = false;
				colvarVendorOrderId.IsPrimaryKey = false;
				colvarVendorOrderId.IsForeignKey = false;
				colvarVendorOrderId.IsReadOnly = false;
				colvarVendorOrderId.DefaultSetting = @"";
				colvarVendorOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorOrderId);
				
				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.String;
				colvarOrderId.MaxLength = 24;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = false;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);
				
				TableSchema.TableColumn colvarFee = new TableSchema.TableColumn(schema);
				colvarFee.ColumnName = "fee";
				colvarFee.DataType = DbType.Int32;
				colvarFee.MaxLength = 0;
				colvarFee.AutoIncrement = false;
				colvarFee.IsNullable = true;
				colvarFee.IsPrimaryKey = false;
				colvarFee.IsForeignKey = false;
				colvarFee.IsReadOnly = false;
				colvarFee.DefaultSetting = @"";
				colvarFee.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFee);
				
				TableSchema.TableColumn colvarAccountingDate = new TableSchema.TableColumn(schema);
				colvarAccountingDate.ColumnName = "accounting_date";
				colvarAccountingDate.DataType = DbType.DateTime;
				colvarAccountingDate.MaxLength = 0;
				colvarAccountingDate.AutoIncrement = false;
				colvarAccountingDate.IsNullable = true;
				colvarAccountingDate.IsPrimaryKey = false;
				colvarAccountingDate.IsForeignKey = false;
				colvarAccountingDate.IsReadOnly = false;
				colvarAccountingDate.DefaultSetting = @"";
				colvarAccountingDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountingDate);
				
				TableSchema.TableColumn colvarRefundShipId = new TableSchema.TableColumn(schema);
				colvarRefundShipId.ColumnName = "refund_ship_id";
				colvarRefundShipId.DataType = DbType.String;
				colvarRefundShipId.MaxLength = 30;
				colvarRefundShipId.AutoIncrement = false;
				colvarRefundShipId.IsNullable = false;
				colvarRefundShipId.IsPrimaryKey = false;
				colvarRefundShipId.IsForeignKey = false;
				colvarRefundShipId.IsReadOnly = false;
				colvarRefundShipId.DefaultSetting = @"";
				colvarRefundShipId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundShipId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarBalanceSheetId = new TableSchema.TableColumn(schema);
				colvarBalanceSheetId.ColumnName = "balance_sheet_id";
				colvarBalanceSheetId.DataType = DbType.Int32;
				colvarBalanceSheetId.MaxLength = 0;
				colvarBalanceSheetId.AutoIncrement = false;
				colvarBalanceSheetId.IsNullable = true;
				colvarBalanceSheetId.IsPrimaryKey = false;
				colvarBalanceSheetId.IsForeignKey = false;
				colvarBalanceSheetId.IsReadOnly = false;
				colvarBalanceSheetId.DefaultSetting = @"";
				colvarBalanceSheetId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBalanceSheetId);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("wms_refund_ship_fee",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("VendorOrderId")]
		[Bindable(true)]
		public string VendorOrderId 
		{
			get { return GetColumnValue<string>(Columns.VendorOrderId); }
			set { SetColumnValue(Columns.VendorOrderId, value); }
		}
		  
		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId 
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}
		  
		[XmlAttribute("Fee")]
		[Bindable(true)]
		public int? Fee 
		{
			get { return GetColumnValue<int?>(Columns.Fee); }
			set { SetColumnValue(Columns.Fee, value); }
		}
		  
		[XmlAttribute("AccountingDate")]
		[Bindable(true)]
		public DateTime? AccountingDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.AccountingDate); }
			set { SetColumnValue(Columns.AccountingDate, value); }
		}
		  
		[XmlAttribute("RefundShipId")]
		[Bindable(true)]
		public string RefundShipId 
		{
			get { return GetColumnValue<string>(Columns.RefundShipId); }
			set { SetColumnValue(Columns.RefundShipId, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("BalanceSheetId")]
		[Bindable(true)]
		public int? BalanceSheetId 
		{
			get { return GetColumnValue<int?>(Columns.BalanceSheetId); }
			set { SetColumnValue(Columns.BalanceSheetId, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorOrderIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn FeeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountingDateColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundShipIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn BalanceSheetIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string VendorOrderId = @"vendor_order_id";
			 public static string OrderId = @"order_id";
			 public static string Fee = @"fee";
			 public static string AccountingDate = @"accounting_date";
			 public static string RefundShipId = @"refund_ship_id";
			 public static string OrderGuid = @"order_guid";
			 public static string BalanceSheetId = @"balance_sheet_id";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

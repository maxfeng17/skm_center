using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewExpiringOrder class.
    /// </summary>
    [Serializable]
    public partial class ViewExpiringOrderCollection : ReadOnlyList<ViewExpiringOrder, ViewExpiringOrderCollection>
    {        
        public ViewExpiringOrderCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_expiring_order view.
    /// </summary>
    [Serializable]
    public partial class ViewExpiringOrder : ReadOnlyRecord<ViewExpiringOrder>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_expiring_order", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarLastShipDate = new TableSchema.TableColumn(schema);
                colvarLastShipDate.ColumnName = "last_ship_date";
                colvarLastShipDate.DataType = DbType.DateTime;
                colvarLastShipDate.MaxLength = 0;
                colvarLastShipDate.AutoIncrement = false;
                colvarLastShipDate.IsNullable = true;
                colvarLastShipDate.IsPrimaryKey = false;
                colvarLastShipDate.IsForeignKey = false;
                colvarLastShipDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastShipDate);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
                colvarShipType.ColumnName = "ship_type";
                colvarShipType.DataType = DbType.Int32;
                colvarShipType.MaxLength = 0;
                colvarShipType.AutoIncrement = false;
                colvarShipType.IsNullable = true;
                colvarShipType.IsPrimaryKey = false;
                colvarShipType.IsForeignKey = false;
                colvarShipType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipType);
                
                TableSchema.TableColumn colvarShippingdateType = new TableSchema.TableColumn(schema);
                colvarShippingdateType.ColumnName = "shippingdate_type";
                colvarShippingdateType.DataType = DbType.Int32;
                colvarShippingdateType.MaxLength = 0;
                colvarShippingdateType.AutoIncrement = false;
                colvarShippingdateType.IsNullable = true;
                colvarShippingdateType.IsPrimaryKey = false;
                colvarShippingdateType.IsForeignKey = false;
                colvarShippingdateType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippingdateType);
                
                TableSchema.TableColumn colvarShippingdate = new TableSchema.TableColumn(schema);
                colvarShippingdate.ColumnName = "shippingdate";
                colvarShippingdate.DataType = DbType.Int32;
                colvarShippingdate.MaxLength = 0;
                colvarShippingdate.AutoIncrement = false;
                colvarShippingdate.IsNullable = true;
                colvarShippingdate.IsPrimaryKey = false;
                colvarShippingdate.IsForeignKey = false;
                colvarShippingdate.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippingdate);
                
                TableSchema.TableColumn colvarProductUseDateEndSet = new TableSchema.TableColumn(schema);
                colvarProductUseDateEndSet.ColumnName = "product_use_date_end_set";
                colvarProductUseDateEndSet.DataType = DbType.Int32;
                colvarProductUseDateEndSet.MaxLength = 0;
                colvarProductUseDateEndSet.AutoIncrement = false;
                colvarProductUseDateEndSet.IsNullable = true;
                colvarProductUseDateEndSet.IsPrimaryKey = false;
                colvarProductUseDateEndSet.IsForeignKey = false;
                colvarProductUseDateEndSet.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductUseDateEndSet);
                
                TableSchema.TableColumn colvarOrderList = new TableSchema.TableColumn(schema);
                colvarOrderList.ColumnName = "order_list";
                colvarOrderList.DataType = DbType.AnsiString;
                colvarOrderList.MaxLength = 1;
                colvarOrderList.AutoIncrement = false;
                colvarOrderList.IsNullable = false;
                colvarOrderList.IsPrimaryKey = false;
                colvarOrderList.IsForeignKey = false;
                colvarOrderList.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderList);
                
                TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
                colvarOrderCount.ColumnName = "order_count";
                colvarOrderCount.DataType = DbType.Int32;
                colvarOrderCount.MaxLength = 0;
                colvarOrderCount.AutoIncrement = false;
                colvarOrderCount.IsNullable = true;
                colvarOrderCount.IsPrimaryKey = false;
                colvarOrderCount.IsForeignKey = false;
                colvarOrderCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCount);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_expiring_order",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewExpiringOrder()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewExpiringOrder(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewExpiringOrder(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewExpiringOrder(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("LastShipDate")]
        [Bindable(true)]
        public DateTime? LastShipDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("last_ship_date");
		    }
            set 
		    {
			    SetColumnValue("last_ship_date", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("ShipType")]
        [Bindable(true)]
        public int? ShipType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ship_type");
		    }
            set 
		    {
			    SetColumnValue("ship_type", value);
            }
        }
	      
        [XmlAttribute("ShippingdateType")]
        [Bindable(true)]
        public int? ShippingdateType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("shippingdate_type");
		    }
            set 
		    {
			    SetColumnValue("shippingdate_type", value);
            }
        }
	      
        [XmlAttribute("Shippingdate")]
        [Bindable(true)]
        public int? Shippingdate 
	    {
		    get
		    {
			    return GetColumnValue<int?>("shippingdate");
		    }
            set 
		    {
			    SetColumnValue("shippingdate", value);
            }
        }
	      
        [XmlAttribute("ProductUseDateEndSet")]
        [Bindable(true)]
        public int? ProductUseDateEndSet 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_use_date_end_set");
		    }
            set 
		    {
			    SetColumnValue("product_use_date_end_set", value);
            }
        }
	      
        [XmlAttribute("OrderList")]
        [Bindable(true)]
        public string OrderList 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_list");
		    }
            set 
		    {
			    SetColumnValue("order_list", value);
            }
        }
	      
        [XmlAttribute("OrderCount")]
        [Bindable(true)]
        public int? OrderCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_count");
		    }
            set 
		    {
			    SetColumnValue("order_count", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string SellerGuid = @"seller_guid";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string LastShipDate = @"last_ship_date";
            
            public static string UniqueId = @"unique_id";
            
            public static string ItemName = @"item_name";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string ShipType = @"ship_type";
            
            public static string ShippingdateType = @"shippingdate_type";
            
            public static string Shippingdate = @"shippingdate";
            
            public static string ProductUseDateEndSet = @"product_use_date_end_set";
            
            public static string OrderList = @"order_list";
            
            public static string OrderCount = @"order_count";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

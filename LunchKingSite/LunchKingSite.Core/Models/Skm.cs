﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LunchKingSite.DataOrm
{
    public class SkmAppBannerSort
    {
        public int Id { get; set; }
        public int Sort { get; set; }
    }
    public class SkmDealSort
    {
        public DateTime DealDate { get; set; }
        public List<SeqDeals> Deals { get; set; }
        
    }
    public class SeqDeals
    {
        public int Id { get; set; }
        public int timeSlotId { get; set; }
        public Guid BusinessHourGuid { get; set; }
        private string _itemName;
        public string ItemName {
            get
            {
                var subjectByte = Encoding.UTF32.GetBytes(_itemName);
                int subjectLen = 20 * 4; //預設最長20個字
                if (subjectByte.Length < subjectLen)
                {
                    subjectLen = subjectByte.Length;
                }
                return string.Format("{0}{1}", Encoding.UTF32.GetString(subjectByte, 0, subjectLen),
                    subjectByte.Length > (20*4) ? "..." : string.Empty);
            }
            set { _itemName = value; }
        }
        public int CityId { get; set; }
        public int Sequence { get; set; }
        public string DealDate { get; set; }
        public int Status { get; set; }
        public bool IsLcock { get; set; }
        public string Url { get; set; }
        public string BeaconId { get; set; }
        public int Type { get; set; }
        public int BeaconCount { get; set; }
        public string Floor { get; set; }
    }

    public class SkmAppStyleImages
    {
        public string BusinessHourGuid { get; set; }
        public string FileName { get; set; }
        public string FileUrl { get; set; }
    }

    public class SkmAppStyleModel
    {
        public int Id { get; set; }
        public string CategoryId { get; set; }
        public Guid? Sid { get; set; }
        public string Name { get; set; }
        public List<SkmAppStyleSetupModel> Styles { get; set; }
    }

    public class SkmAppStyleSetupModel
    {
        public Guid Gid { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public int Status { get; set; }
        public string DealStatus { get; set; }
    }

    public class SkmBeaconDeviceInfo
    {
        public int Major { get; set; }
        public int Minor { get; set; }
        public string ElectricPower { get; set; }
        public string DeviceName { get; set; }
        public string LastUpdateTime { get; set; }
        public string Floor { get; set; }
    }
}

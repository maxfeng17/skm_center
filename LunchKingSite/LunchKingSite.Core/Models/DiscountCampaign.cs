using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    [Serializable]
    public partial class DiscountCampaignCollection : RepositoryList<DiscountCampaign, DiscountCampaignCollection>
    {
        public DiscountCampaignCollection() { }

        public DiscountCampaignCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DiscountCampaign o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }

    [Serializable]
    public partial class DiscountCampaign : RepositoryRecord<DiscountCampaign>, IRecordBase
    {
        #region .ctors and Default Settings
        public DiscountCampaign()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public DiscountCampaign(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("discount_campaign", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = true;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 100;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                colvarName.DefaultSetting = @"";
                colvarName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarName);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = true;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                colvarAmount.DefaultSetting = @"";
                colvarAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
                colvarQty.ColumnName = "qty";
                colvarQty.DataType = DbType.Int32;
                colvarQty.MaxLength = 0;
                colvarQty.AutoIncrement = false;
                colvarQty.IsNullable = true;
                colvarQty.IsPrimaryKey = false;
                colvarQty.IsForeignKey = false;
                colvarQty.IsReadOnly = false;
                colvarQty.DefaultSetting = @"";
                colvarQty.ForeignKeyTableName = "";
                schema.Columns.Add(colvarQty);

                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = true;
                colvarTotal.DefaultSetting = @"";
                colvarTotal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTotal);

                TableSchema.TableColumn colvarApplyTime = new TableSchema.TableColumn(schema);
                colvarApplyTime.ColumnName = "apply_time";
                colvarApplyTime.DataType = DbType.DateTime;
                colvarApplyTime.MaxLength = 0;
                colvarApplyTime.AutoIncrement = false;
                colvarApplyTime.IsNullable = true;
                colvarApplyTime.IsPrimaryKey = false;
                colvarApplyTime.IsForeignKey = false;
                colvarApplyTime.IsReadOnly = false;
                colvarApplyTime.DefaultSetting = @"";
                colvarApplyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarApplyTime);

                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = true;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                colvarStartTime.DefaultSetting = @"";
                colvarStartTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStartTime);

                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                colvarEndTime.DefaultSetting = @"";
                colvarEndTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEndTime);

                TableSchema.TableColumn colvarCancelTime = new TableSchema.TableColumn(schema);
                colvarCancelTime.ColumnName = "cancel_time";
                colvarCancelTime.DataType = DbType.DateTime;
                colvarCancelTime.MaxLength = 0;
                colvarCancelTime.AutoIncrement = false;
                colvarCancelTime.IsNullable = true;
                colvarCancelTime.IsPrimaryKey = false;
                colvarCancelTime.IsForeignKey = false;
                colvarCancelTime.IsReadOnly = false;
                colvarCancelTime.DefaultSetting = @"";
                colvarCancelTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCancelTime);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarApplyId = new TableSchema.TableColumn(schema);
                colvarApplyId.ColumnName = "apply_id";
                colvarApplyId.DataType = DbType.AnsiString;
                colvarApplyId.MaxLength = 50;
                colvarApplyId.AutoIncrement = false;
                colvarApplyId.IsNullable = true;
                colvarApplyId.IsPrimaryKey = false;
                colvarApplyId.IsForeignKey = false;
                colvarApplyId.IsReadOnly = false;
                colvarApplyId.DefaultSetting = @"";
                colvarApplyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarApplyId);

                TableSchema.TableColumn colvarCancelId = new TableSchema.TableColumn(schema);
                colvarCancelId.ColumnName = "cancel_id";
                colvarCancelId.DataType = DbType.AnsiString;
                colvarCancelId.MaxLength = 50;
                colvarCancelId.AutoIncrement = false;
                colvarCancelId.IsNullable = true;
                colvarCancelId.IsPrimaryKey = false;
                colvarCancelId.IsForeignKey = false;
                colvarCancelId.IsReadOnly = false;
                colvarCancelId.DefaultSetting = @"";
                colvarCancelId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCancelId);

                TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
                colvarFlag.ColumnName = "flag";
                colvarFlag.DataType = DbType.Int32;
                colvarFlag.MaxLength = 0;
                colvarFlag.AutoIncrement = false;
                colvarFlag.IsNullable = false;
                colvarFlag.IsPrimaryKey = false;
                colvarFlag.IsForeignKey = false;
                colvarFlag.IsReadOnly = false;
                colvarFlag.DefaultSetting = @"((15))";
                colvarFlag.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFlag);

                TableSchema.TableColumn colvarMinimumAmount = new TableSchema.TableColumn(schema);
                colvarMinimumAmount.ColumnName = "minimum_amount";
                colvarMinimumAmount.DataType = DbType.Int32;
                colvarMinimumAmount.MaxLength = 0;
                colvarMinimumAmount.AutoIncrement = false;
                colvarMinimumAmount.IsNullable = true;
                colvarMinimumAmount.IsPrimaryKey = false;
                colvarMinimumAmount.IsForeignKey = false;
                colvarMinimumAmount.IsReadOnly = false;
                colvarMinimumAmount.DefaultSetting = @"";
                colvarMinimumAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMinimumAmount);

                TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
                colvarCardGroupId.ColumnName = "card_group_id";
                colvarCardGroupId.DataType = DbType.Int32;
                colvarCardGroupId.MaxLength = 0;
                colvarCardGroupId.AutoIncrement = false;
                colvarCardGroupId.IsNullable = true;
                colvarCardGroupId.IsPrimaryKey = false;
                colvarCardGroupId.IsForeignKey = false;
                colvarCardGroupId.IsReadOnly = false;
                colvarCardGroupId.DefaultSetting = @"";
                colvarCardGroupId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardGroupId);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                colvarType.DefaultSetting = @"((0))";
                colvarType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.Int32;
                colvarModifyId.MaxLength = 0;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarEventDateS = new TableSchema.TableColumn(schema);
                colvarEventDateS.ColumnName = "event_date_s";
                colvarEventDateS.DataType = DbType.DateTime;
                colvarEventDateS.MaxLength = 0;
                colvarEventDateS.AutoIncrement = false;
                colvarEventDateS.IsNullable = true;
                colvarEventDateS.IsPrimaryKey = false;
                colvarEventDateS.IsForeignKey = false;
                colvarEventDateS.IsReadOnly = false;
                colvarEventDateS.DefaultSetting = @"";
                colvarEventDateS.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventDateS);

                TableSchema.TableColumn colvarEventDateE = new TableSchema.TableColumn(schema);
                colvarEventDateE.ColumnName = "event_date_e";
                colvarEventDateE.DataType = DbType.DateTime;
                colvarEventDateE.MaxLength = 0;
                colvarEventDateE.AutoIncrement = false;
                colvarEventDateE.IsNullable = true;
                colvarEventDateE.IsPrimaryKey = false;
                colvarEventDateE.IsForeignKey = false;
                colvarEventDateE.IsReadOnly = false;
                colvarEventDateE.DefaultSetting = @"";
                colvarEventDateE.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventDateE);

                TableSchema.TableColumn colvarFreeGiftDiscount = new TableSchema.TableColumn(schema);
                colvarFreeGiftDiscount.ColumnName = "free_gift_discount";
                colvarFreeGiftDiscount.DataType = DbType.Currency;
                colvarFreeGiftDiscount.MaxLength = 0;
                colvarFreeGiftDiscount.AutoIncrement = false;
                colvarFreeGiftDiscount.IsNullable = true;
                colvarFreeGiftDiscount.IsPrimaryKey = false;
                colvarFreeGiftDiscount.IsForeignKey = false;
                colvarFreeGiftDiscount.IsReadOnly = false;
                colvarFreeGiftDiscount.DefaultSetting = @"";
                colvarFreeGiftDiscount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFreeGiftDiscount);

                TableSchema.TableColumn colvarTemplateId = new TableSchema.TableColumn(schema);
                colvarTemplateId.ColumnName = "template_id";
                colvarTemplateId.DataType = DbType.Int32;
                colvarTemplateId.MaxLength = 0;
                colvarTemplateId.AutoIncrement = false;
                colvarTemplateId.IsNullable = true;
                colvarTemplateId.IsPrimaryKey = false;
                colvarTemplateId.IsForeignKey = false;
                colvarTemplateId.IsReadOnly = false;
                colvarTemplateId.DefaultSetting = @"";
                colvarTemplateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTemplateId);

                TableSchema.TableColumn colvarAvailableDateType = new TableSchema.TableColumn(schema);
                colvarAvailableDateType.ColumnName = "available_date_type";
                colvarAvailableDateType.DataType = DbType.Int32;
                colvarAvailableDateType.MaxLength = 0;
                colvarAvailableDateType.AutoIncrement = false;
                colvarAvailableDateType.IsNullable = false;
                colvarAvailableDateType.IsPrimaryKey = false;
                colvarAvailableDateType.IsForeignKey = false;
                colvarAvailableDateType.IsReadOnly = false;
                colvarAvailableDateType.DefaultSetting = @"";
                colvarAvailableDateType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAvailableDateType);

                TableSchema.TableColumn colvarCardCombineUse = new TableSchema.TableColumn(schema);
                colvarCardCombineUse.ColumnName = "card_combine_use";
                colvarCardCombineUse.DataType = DbType.Boolean;
                colvarCardCombineUse.MaxLength = 0;
                colvarCardCombineUse.AutoIncrement = false;
                colvarCardCombineUse.IsNullable = false;
                colvarCardCombineUse.IsPrimaryKey = false;
                colvarCardCombineUse.IsForeignKey = false;
                colvarCardCombineUse.IsReadOnly = false;
                colvarCardCombineUse.DefaultSetting = @"";
                colvarCardCombineUse.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardCombineUse);

                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = true;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;
                colvarSellerUserId.DefaultSetting = @"";
                colvarSellerUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerUserId);

                TableSchema.TableColumn colvarCampaignNo = new TableSchema.TableColumn(schema);
                colvarCampaignNo.ColumnName = "campaign_no";
                colvarCampaignNo.DataType = DbType.AnsiString;
                colvarCampaignNo.MaxLength = 15;
                colvarCampaignNo.AutoIncrement = false;
                colvarCampaignNo.IsNullable = true;
                colvarCampaignNo.IsPrimaryKey = false;
                colvarCampaignNo.IsForeignKey = false;
                colvarCampaignNo.IsReadOnly = false;
                colvarCampaignNo.DefaultSetting = @"";
                colvarCampaignNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCampaignNo);

                TableSchema.TableColumn colvarMinGrossMargin = new TableSchema.TableColumn(schema);
                colvarMinGrossMargin.ColumnName = "min_gross_margin";
                colvarMinGrossMargin.DataType = DbType.Int32;
                colvarMinGrossMargin.MaxLength = 0;
                colvarMinGrossMargin.AutoIncrement = false;
                colvarMinGrossMargin.IsNullable = true;
                colvarMinGrossMargin.IsPrimaryKey = false;
                colvarMinGrossMargin.IsForeignKey = false;
                colvarMinGrossMargin.IsReadOnly = false;
                colvarMinGrossMargin.DefaultSetting = @"";
                colvarMinGrossMargin.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMinGrossMargin);

                TableSchema.TableColumn colvarIsDiscountPriceForDeal = new TableSchema.TableColumn(schema);
                colvarIsDiscountPriceForDeal.ColumnName = "is_discount_price_for_deal";
                colvarIsDiscountPriceForDeal.DataType = DbType.Boolean;
                colvarIsDiscountPriceForDeal.MaxLength = 0;
                colvarIsDiscountPriceForDeal.AutoIncrement = false;
                colvarIsDiscountPriceForDeal.IsNullable = false;
                colvarIsDiscountPriceForDeal.IsPrimaryKey = false;
                colvarIsDiscountPriceForDeal.IsForeignKey = false;
                colvarIsDiscountPriceForDeal.IsReadOnly = false;
                colvarIsDiscountPriceForDeal.DefaultSetting = @"((1))";
                colvarIsDiscountPriceForDeal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsDiscountPriceForDeal);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("discount_campaign", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name
        {
            get { return GetColumnValue<string>(Columns.Name); }
            set { SetColumnValue(Columns.Name, value); }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal? Amount
        {
            get { return GetColumnValue<decimal?>(Columns.Amount); }
            set { SetColumnValue(Columns.Amount, value); }
        }

        [XmlAttribute("Qty")]
        [Bindable(true)]
        public int? Qty
        {
            get { return GetColumnValue<int?>(Columns.Qty); }
            set { SetColumnValue(Columns.Qty, value); }
        }

        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal? Total
        {
            get { return GetColumnValue<decimal?>(Columns.Total); }
            set { SetColumnValue(Columns.Total, value); }
        }

        [XmlAttribute("ApplyTime")]
        [Bindable(true)]
        public DateTime? ApplyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ApplyTime); }
            set { SetColumnValue(Columns.ApplyTime, value); }
        }

        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime? StartTime
        {
            get { return GetColumnValue<DateTime?>(Columns.StartTime); }
            set { SetColumnValue(Columns.StartTime, value); }
        }

        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime
        {
            get { return GetColumnValue<DateTime?>(Columns.EndTime); }
            set { SetColumnValue(Columns.EndTime, value); }
        }

        [XmlAttribute("CancelTime")]
        [Bindable(true)]
        public DateTime? CancelTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CancelTime); }
            set { SetColumnValue(Columns.CancelTime, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("ApplyId")]
        [Bindable(true)]
        public string ApplyId
        {
            get { return GetColumnValue<string>(Columns.ApplyId); }
            set { SetColumnValue(Columns.ApplyId, value); }
        }

        [XmlAttribute("CancelId")]
        [Bindable(true)]
        public string CancelId
        {
            get { return GetColumnValue<string>(Columns.CancelId); }
            set { SetColumnValue(Columns.CancelId, value); }
        }

        [XmlAttribute("Flag")]
        [Bindable(true)]
        public int Flag
        {
            get { return GetColumnValue<int>(Columns.Flag); }
            set { SetColumnValue(Columns.Flag, value); }
        }

        [XmlAttribute("MinimumAmount")]
        [Bindable(true)]
        public int? MinimumAmount
        {
            get { return GetColumnValue<int?>(Columns.MinimumAmount); }
            set { SetColumnValue(Columns.MinimumAmount, value); }
        }

        [XmlAttribute("CardGroupId")]
        [Bindable(true)]
        public int? CardGroupId
        {
            get { return GetColumnValue<int?>(Columns.CardGroupId); }
            set { SetColumnValue(Columns.CardGroupId, value); }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get { return GetColumnValue<int>(Columns.Type); }
            set { SetColumnValue(Columns.Type, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public int? ModifyId
        {
            get { return GetColumnValue<int?>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("EventDateS")]
        [Bindable(true)]
        public DateTime? EventDateS
        {
            get { return GetColumnValue<DateTime?>(Columns.EventDateS); }
            set { SetColumnValue(Columns.EventDateS, value); }
        }

        [XmlAttribute("EventDateE")]
        [Bindable(true)]
        public DateTime? EventDateE
        {
            get { return GetColumnValue<DateTime?>(Columns.EventDateE); }
            set { SetColumnValue(Columns.EventDateE, value); }
        }

        [XmlAttribute("FreeGiftDiscount")]
        [Bindable(true)]
        public decimal? FreeGiftDiscount
        {
            get { return GetColumnValue<decimal?>(Columns.FreeGiftDiscount); }
            set { SetColumnValue(Columns.FreeGiftDiscount, value); }
        }

        [XmlAttribute("TemplateId")]
        [Bindable(true)]
        public int? TemplateId
        {
            get { return GetColumnValue<int?>(Columns.TemplateId); }
            set { SetColumnValue(Columns.TemplateId, value); }
        }

        [XmlAttribute("AvailableDateType")]
        [Bindable(true)]
        public int AvailableDateType
        {
            get { return GetColumnValue<int>(Columns.AvailableDateType); }
            set { SetColumnValue(Columns.AvailableDateType, value); }
        }

        [XmlAttribute("CardCombineUse")]
        [Bindable(true)]
        public bool CardCombineUse
        {
            get { return GetColumnValue<bool>(Columns.CardCombineUse); }
            set { SetColumnValue(Columns.CardCombineUse, value); }
        }

        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int? SellerUserId
        {
            get { return GetColumnValue<int?>(Columns.SellerUserId); }
            set { SetColumnValue(Columns.SellerUserId, value); }
        }

        [XmlAttribute("CampaignNo")]
        [Bindable(true)]
        public string CampaignNo
        {
            get { return GetColumnValue<string>(Columns.CampaignNo); }
            set { SetColumnValue(Columns.CampaignNo, value); }
        }

        [XmlAttribute("MinGrossMargin")]
        [Bindable(true)]
        public int? MinGrossMargin
        {
            get { return GetColumnValue<int?>(Columns.MinGrossMargin); }
            set { SetColumnValue(Columns.MinGrossMargin, value); }
        }

        [XmlAttribute("IsDiscountPriceForDeal")]
        [Bindable(true)]
        public bool IsDiscountPriceForDeal
        {
            get { return GetColumnValue<bool>(Columns.IsDiscountPriceForDeal); }
            set { SetColumnValue(Columns.IsDiscountPriceForDeal, value); }
        }

        #endregion

        #region Typed Columns

        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }

        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[1]; }
        }

        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[2]; }
        }

        public static TableSchema.TableColumn QtyColumn
        {
            get { return Schema.Columns[3]; }
        }

        public static TableSchema.TableColumn TotalColumn
        {
            get { return Schema.Columns[4]; }
        }

        public static TableSchema.TableColumn ApplyTimeColumn
        {
            get { return Schema.Columns[5]; }
        }

        public static TableSchema.TableColumn StartTimeColumn
        {
            get { return Schema.Columns[6]; }
        }

        public static TableSchema.TableColumn EndTimeColumn
        {
            get { return Schema.Columns[7]; }
        }

        public static TableSchema.TableColumn CancelTimeColumn
        {
            get { return Schema.Columns[8]; }
        }

        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }

        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[10]; }
        }

        public static TableSchema.TableColumn ApplyIdColumn
        {
            get { return Schema.Columns[11]; }
        }

        public static TableSchema.TableColumn CancelIdColumn
        {
            get { return Schema.Columns[12]; }
        }

        public static TableSchema.TableColumn FlagColumn
        {
            get { return Schema.Columns[13]; }
        }

        public static TableSchema.TableColumn MinimumAmountColumn
        {
            get { return Schema.Columns[14]; }
        }

        public static TableSchema.TableColumn CardGroupIdColumn
        {
            get { return Schema.Columns[15]; }
        }

        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[16]; }
        }

        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[17]; }
        }

        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[18]; }
        }

        public static TableSchema.TableColumn EventDateSColumn
        {
            get { return Schema.Columns[19]; }
        }

        public static TableSchema.TableColumn EventDateEColumn
        {
            get { return Schema.Columns[20]; }
        }

        public static TableSchema.TableColumn FreeGiftDiscountColumn
        {
            get { return Schema.Columns[21]; }
        }

        public static TableSchema.TableColumn TemplateIdColumn
        {
            get { return Schema.Columns[22]; }
        }

        public static TableSchema.TableColumn AvailableDateTypeColumn
        {
            get { return Schema.Columns[23]; }
        }

        public static TableSchema.TableColumn CardCombineUseColumn
        {
            get { return Schema.Columns[24]; }
        }

        public static TableSchema.TableColumn SellerUserIdColumn
        {
            get { return Schema.Columns[25]; }
        }

        public static TableSchema.TableColumn CampaignNoColumn
        {
            get { return Schema.Columns[26]; }
        }

        public static TableSchema.TableColumn MinGrossMarginColumn
        {
            get { return Schema.Columns[27]; }
        }

        public static TableSchema.TableColumn IsDiscountPriceForDealColumn
        {
            get { return Schema.Columns[28]; }
        }

        #endregion

        #region Columns Struct

        public struct Columns
        {
            public static string Id = @"id";
            public static string Name = @"name";
            public static string Amount = @"amount";
            public static string Qty = @"qty";
            public static string Total = @"total";
            public static string ApplyTime = @"apply_time";
            public static string StartTime = @"start_time";
            public static string EndTime = @"end_time";
            public static string CancelTime = @"cancel_time";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string ApplyId = @"apply_id";
            public static string CancelId = @"cancel_id";
            public static string Flag = @"flag";
            public static string MinimumAmount = @"minimum_amount";
            public static string CardGroupId = @"card_group_id";
            public static string Type = @"type";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string EventDateS = @"event_date_s";
            public static string EventDateE = @"event_date_e";
            public static string FreeGiftDiscount = @"free_gift_discount";
            public static string TemplateId = @"template_id";
            public static string AvailableDateType = @"available_date_type";
            public static string CardCombineUse = @"card_combine_use";
            public static string SellerUserId = @"seller_user_id";
            public static string CampaignNo = @"campaign_no";
            public static string MinGrossMargin = @"min_gross_margin";
            public static string IsDiscountPriceForDeal = @"is_discount_price_for_deal";
        }

        #endregion

    }
}

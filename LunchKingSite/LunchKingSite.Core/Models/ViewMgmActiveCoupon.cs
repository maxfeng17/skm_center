using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMgmActiveCoupon class.
    /// </summary>
    [Serializable]
    public partial class ViewMgmActiveCouponCollection : ReadOnlyList<ViewMgmActiveCoupon, ViewMgmActiveCouponCollection>
    {
        public ViewMgmActiveCouponCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_mgm_active_coupon view.
    /// </summary>
    [Serializable]
    public partial class ViewMgmActiveCoupon : ReadOnlyRecord<ViewMgmActiveCoupon>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_mgm_active_coupon", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;

                schema.Columns.Add(colvarCouponId);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;

                schema.Columns.Add(colvarOrderGuid);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_mgm_active_coupon",schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewMgmActiveCoupon()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMgmActiveCoupon(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewMgmActiveCoupon(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewMgmActiveCoupon(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId
        {
            get
            {
                return GetColumnValue<int?>("coupon_id");
            }
            set
            {
                SetColumnValue("coupon_id", value);
            }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid
        {
            get
            {
                return GetColumnValue<Guid?>("business_hour_guid");
            }
            set
            {
                SetColumnValue("business_hour_guid", value);
            }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid
        {
            get
            {
                return GetColumnValue<Guid>("order_guid");
            }
            set
            {
                SetColumnValue("order_guid", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string CouponId = @"coupon_id";

            public static string BusinessHourGuid = @"business_hour_guid";

            public static string OrderGuid = @"order_guid";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

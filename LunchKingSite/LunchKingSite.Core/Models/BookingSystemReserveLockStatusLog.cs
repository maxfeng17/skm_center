using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BookingSystemReserveLockStatusLog class.
	/// </summary>
    [Serializable]
	public partial class BookingSystemReserveLockStatusLogCollection : RepositoryList<BookingSystemReserveLockStatusLog, BookingSystemReserveLockStatusLogCollection>
	{	   
		public BookingSystemReserveLockStatusLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BookingSystemReserveLockStatusLogCollection</returns>
		public BookingSystemReserveLockStatusLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BookingSystemReserveLockStatusLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the booking_system_reserve_lock_status_log table.
	/// </summary>
	[Serializable]
	public partial class BookingSystemReserveLockStatusLog : RepositoryRecord<BookingSystemReserveLockStatusLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BookingSystemReserveLockStatusLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BookingSystemReserveLockStatusLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("booking_system_reserve_lock_status_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
				colvarDealType.ColumnName = "deal_type";
				colvarDealType.DataType = DbType.Int32;
				colvarDealType.MaxLength = 0;
				colvarDealType.AutoIncrement = false;
				colvarDealType.IsNullable = false;
				colvarDealType.IsPrimaryKey = false;
				colvarDealType.IsForeignKey = false;
				colvarDealType.IsReadOnly = false;
				colvarDealType.DefaultSetting = @"";
				colvarDealType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealType);
				
				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "coupon_id";
				colvarCouponId.DataType = DbType.Int32;
				colvarCouponId.MaxLength = 0;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = false;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = false;
				colvarCouponId.IsReadOnly = false;
				colvarCouponId.DefaultSetting = @"";
				colvarCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponId);
				
				TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
				colvarAccountId.ColumnName = "account_id";
				colvarAccountId.DataType = DbType.String;
				colvarAccountId.MaxLength = 256;
				colvarAccountId.AutoIncrement = false;
				colvarAccountId.IsNullable = false;
				colvarAccountId.IsPrimaryKey = false;
				colvarAccountId.IsForeignKey = false;
				colvarAccountId.IsReadOnly = false;
				colvarAccountId.DefaultSetting = @"";
				colvarAccountId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarReserveLockStatus = new TableSchema.TableColumn(schema);
				colvarReserveLockStatus.ColumnName = "reserve_lock_status";
				colvarReserveLockStatus.DataType = DbType.Int32;
				colvarReserveLockStatus.MaxLength = 0;
				colvarReserveLockStatus.AutoIncrement = false;
				colvarReserveLockStatus.IsNullable = false;
				colvarReserveLockStatus.IsPrimaryKey = false;
				colvarReserveLockStatus.IsForeignKey = false;
				colvarReserveLockStatus.IsReadOnly = false;
				colvarReserveLockStatus.DefaultSetting = @"";
				colvarReserveLockStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReserveLockStatus);
				
				TableSchema.TableColumn colvarLogInfo = new TableSchema.TableColumn(schema);
				colvarLogInfo.ColumnName = "log_info";
				colvarLogInfo.DataType = DbType.String;
				colvarLogInfo.MaxLength = 50;
				colvarLogInfo.AutoIncrement = false;
				colvarLogInfo.IsNullable = false;
				colvarLogInfo.IsPrimaryKey = false;
				colvarLogInfo.IsForeignKey = false;
				colvarLogInfo.IsReadOnly = false;
				colvarLogInfo.DefaultSetting = @"";
				colvarLogInfo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogInfo);
				
				TableSchema.TableColumn colvarCouponReservationDate = new TableSchema.TableColumn(schema);
				colvarCouponReservationDate.ColumnName = "coupon_reservation_date";
				colvarCouponReservationDate.DataType = DbType.DateTime;
				colvarCouponReservationDate.MaxLength = 0;
				colvarCouponReservationDate.AutoIncrement = false;
				colvarCouponReservationDate.IsNullable = true;
				colvarCouponReservationDate.IsPrimaryKey = false;
				colvarCouponReservationDate.IsForeignKey = false;
				colvarCouponReservationDate.IsReadOnly = false;
				colvarCouponReservationDate.DefaultSetting = @"";
				colvarCouponReservationDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponReservationDate);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("booking_system_reserve_lock_status_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("DealType")]
		[Bindable(true)]
		public int DealType 
		{
			get { return GetColumnValue<int>(Columns.DealType); }
			set { SetColumnValue(Columns.DealType, value); }
		}
		  
		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public int CouponId 
		{
			get { return GetColumnValue<int>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}
		  
		[XmlAttribute("AccountId")]
		[Bindable(true)]
		public string AccountId 
		{
			get { return GetColumnValue<string>(Columns.AccountId); }
			set { SetColumnValue(Columns.AccountId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ReserveLockStatus")]
		[Bindable(true)]
		public int ReserveLockStatus 
		{
			get { return GetColumnValue<int>(Columns.ReserveLockStatus); }
			set { SetColumnValue(Columns.ReserveLockStatus, value); }
		}
		  
		[XmlAttribute("LogInfo")]
		[Bindable(true)]
		public string LogInfo 
		{
			get { return GetColumnValue<string>(Columns.LogInfo); }
			set { SetColumnValue(Columns.LogInfo, value); }
		}
		  
		[XmlAttribute("CouponReservationDate")]
		[Bindable(true)]
		public DateTime? CouponReservationDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.CouponReservationDate); }
			set { SetColumnValue(Columns.CouponReservationDate, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DealTypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ReserveLockStatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn LogInfoColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponReservationDateColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string DealType = @"deal_type";
			 public static string CouponId = @"coupon_id";
			 public static string AccountId = @"account_id";
			 public static string ModifyTime = @"modify_time";
			 public static string ReserveLockStatus = @"reserve_lock_status";
			 public static string LogInfo = @"log_info";
			 public static string CouponReservationDate = @"coupon_reservation_date";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

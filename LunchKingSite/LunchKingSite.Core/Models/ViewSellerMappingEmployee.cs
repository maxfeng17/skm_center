using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewSellerMappingEmployee class.
    /// </summary>
    [Serializable]
    public partial class ViewSellerMappingEmployeeCollection : ReadOnlyList<ViewSellerMappingEmployee, ViewSellerMappingEmployeeCollection>
    {        
        public ViewSellerMappingEmployeeCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_seller_mapping_employee view.
    /// </summary>
    [Serializable]
    public partial class ViewSellerMappingEmployee : ReadOnlyRecord<ViewSellerMappingEmployee>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_seller_mapping_employee", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_Guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarDeptName = new TableSchema.TableColumn(schema);
                colvarDeptName.ColumnName = "dept_name";
                colvarDeptName.DataType = DbType.String;
                colvarDeptName.MaxLength = 100;
                colvarDeptName.AutoIncrement = false;
                colvarDeptName.IsNullable = true;
                colvarDeptName.IsPrimaryKey = false;
                colvarDeptName.IsForeignKey = false;
                colvarDeptName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeptName);
                
                TableSchema.TableColumn colvarEmpId = new TableSchema.TableColumn(schema);
                colvarEmpId.ColumnName = "emp_id";
                colvarEmpId.DataType = DbType.AnsiString;
                colvarEmpId.MaxLength = 50;
                colvarEmpId.AutoIncrement = false;
                colvarEmpId.IsNullable = false;
                colvarEmpId.IsPrimaryKey = false;
                colvarEmpId.IsForeignKey = false;
                colvarEmpId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEmpId);
                
                TableSchema.TableColumn colvarEmpName = new TableSchema.TableColumn(schema);
                colvarEmpName.ColumnName = "emp_name";
                colvarEmpName.DataType = DbType.String;
                colvarEmpName.MaxLength = 50;
                colvarEmpName.AutoIncrement = false;
                colvarEmpName.IsNullable = true;
                colvarEmpName.IsPrimaryKey = false;
                colvarEmpName.IsForeignKey = false;
                colvarEmpName.IsReadOnly = false;
                
                schema.Columns.Add(colvarEmpName);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_seller_mapping_employee",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewSellerMappingEmployee()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewSellerMappingEmployee(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewSellerMappingEmployee(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewSellerMappingEmployee(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_Guid");
		    }
            set 
		    {
			    SetColumnValue("seller_Guid", value);
            }
        }
	      
        [XmlAttribute("DeptName")]
        [Bindable(true)]
        public string DeptName 
	    {
		    get
		    {
			    return GetColumnValue<string>("dept_name");
		    }
            set 
		    {
			    SetColumnValue("dept_name", value);
            }
        }
	      
        [XmlAttribute("EmpId")]
        [Bindable(true)]
        public string EmpId 
	    {
		    get
		    {
			    return GetColumnValue<string>("emp_id");
		    }
            set 
		    {
			    SetColumnValue("emp_id", value);
            }
        }
	      
        [XmlAttribute("EmpName")]
        [Bindable(true)]
        public string EmpName 
	    {
		    get
		    {
			    return GetColumnValue<string>("emp_name");
		    }
            set 
		    {
			    SetColumnValue("emp_name", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string SellerGuid = @"seller_Guid";
            
            public static string DeptName = @"dept_name";
            
            public static string EmpId = @"emp_id";
            
            public static string EmpName = @"emp_name";
            
            public static string UniqueId = @"unique_id";
            
            public static string UserName = @"user_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

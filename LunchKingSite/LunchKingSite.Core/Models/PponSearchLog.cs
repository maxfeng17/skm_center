using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PponSearchLog class.
	/// </summary>
    [Serializable]
	public partial class PponSearchLogCollection : RepositoryList<PponSearchLog, PponSearchLogCollection>
	{	   
		public PponSearchLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PponSearchLogCollection</returns>
		public PponSearchLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PponSearchLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the ppon_search_log table.
	/// </summary>
	[Serializable]
	
	public partial class PponSearchLog : RepositoryRecord<PponSearchLog>, IRecordBase
	
	{
		#region .ctors and Default Settings
		
		public PponSearchLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PponSearchLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("ppon_search_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarWord = new TableSchema.TableColumn(schema);
				colvarWord.ColumnName = "word";
				colvarWord.DataType = DbType.String;
				colvarWord.MaxLength = 150;
				colvarWord.AutoIncrement = false;
				colvarWord.IsNullable = false;
				colvarWord.IsPrimaryKey = false;
				colvarWord.IsForeignKey = false;
				colvarWord.IsReadOnly = false;
				colvarWord.DefaultSetting = @"";
				colvarWord.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWord);
				
				TableSchema.TableColumn colvarResultCount = new TableSchema.TableColumn(schema);
				colvarResultCount.ColumnName = "result_count";
				colvarResultCount.DataType = DbType.Int32;
				colvarResultCount.MaxLength = 0;
				colvarResultCount.AutoIncrement = false;
				colvarResultCount.IsNullable = false;
				colvarResultCount.IsPrimaryKey = false;
				colvarResultCount.IsForeignKey = false;
				colvarResultCount.IsReadOnly = false;
				colvarResultCount.DefaultSetting = @"";
				colvarResultCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResultCount);
				
				TableSchema.TableColumn colvarSessionId = new TableSchema.TableColumn(schema);
				colvarSessionId.ColumnName = "session_id";
				colvarSessionId.DataType = DbType.String;
				colvarSessionId.MaxLength = 255;
				colvarSessionId.AutoIncrement = false;
				colvarSessionId.IsNullable = false;
				colvarSessionId.IsPrimaryKey = false;
				colvarSessionId.IsForeignKey = false;
				colvarSessionId.IsReadOnly = false;
				colvarSessionId.DefaultSetting = @"";
				colvarSessionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSessionId);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 255;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarDeviceType = new TableSchema.TableColumn(schema);
				colvarDeviceType.ColumnName = "device_type";
				colvarDeviceType.DataType = DbType.Int32;
				colvarDeviceType.MaxLength = 0;
				colvarDeviceType.AutoIncrement = false;
				colvarDeviceType.IsNullable = true;
				colvarDeviceType.IsPrimaryKey = false;
				colvarDeviceType.IsForeignKey = false;
				colvarDeviceType.IsReadOnly = false;
				colvarDeviceType.DefaultSetting = @"";
				colvarDeviceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeviceType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("ppon_search_log",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("Word")]
		[Bindable(true)]
		public string Word 
		{
			get { return GetColumnValue<string>(Columns.Word); }
			set { SetColumnValue(Columns.Word, value); }
		}
		
		[XmlAttribute("ResultCount")]
		[Bindable(true)]
		public int ResultCount 
		{
			get { return GetColumnValue<int>(Columns.ResultCount); }
			set { SetColumnValue(Columns.ResultCount, value); }
		}
		
		[XmlAttribute("SessionId")]
		[Bindable(true)]
		public string SessionId 
		{
			get { return GetColumnValue<string>(Columns.SessionId); }
			set { SetColumnValue(Columns.SessionId, value); }
		}
		
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("DeviceType")]
		[Bindable(true)]
		public int? DeviceType 
		{
			get { return GetColumnValue<int?>(Columns.DeviceType); }
			set { SetColumnValue(Columns.DeviceType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn WordColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ResultCountColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SessionIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn DeviceTypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Word = @"word";
			 public static string ResultCount = @"result_count";
			 public static string SessionId = @"session_id";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string DeviceType = @"device_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpPointTransactionOrderRecord class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpPointTransactionOrderRecordCollection : ReadOnlyList<ViewPcpPointTransactionOrderRecord, ViewPcpPointTransactionOrderRecordCollection>
    {        
        public ViewPcpPointTransactionOrderRecordCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_point_transaction_order_record view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpPointTransactionOrderRecord : ReadOnlyRecord<ViewPcpPointTransactionOrderRecord>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_point_transaction_order_record", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = true;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = true;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarExpireTime = new TableSchema.TableColumn(schema);
                colvarExpireTime.ColumnName = "expire_time";
                colvarExpireTime.DataType = DbType.DateTime;
                colvarExpireTime.MaxLength = 0;
                colvarExpireTime.AutoIncrement = false;
                colvarExpireTime.IsNullable = true;
                colvarExpireTime.IsPrimaryKey = false;
                colvarExpireTime.IsForeignKey = false;
                colvarExpireTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarExpireTime);
                
                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = -1;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage);
                
                TableSchema.TableColumn colvarPointin = new TableSchema.TableColumn(schema);
                colvarPointin.ColumnName = "pointin";
                colvarPointin.DataType = DbType.Int32;
                colvarPointin.MaxLength = 0;
                colvarPointin.AutoIncrement = false;
                colvarPointin.IsNullable = false;
                colvarPointin.IsPrimaryKey = false;
                colvarPointin.IsForeignKey = false;
                colvarPointin.IsReadOnly = false;
                
                schema.Columns.Add(colvarPointin);
                
                TableSchema.TableColumn colvarPointout = new TableSchema.TableColumn(schema);
                colvarPointout.ColumnName = "pointout";
                colvarPointout.DataType = DbType.Int32;
                colvarPointout.MaxLength = 0;
                colvarPointout.AutoIncrement = false;
                colvarPointout.IsNullable = false;
                colvarPointout.IsPrimaryKey = false;
                colvarPointout.IsForeignKey = false;
                colvarPointout.IsReadOnly = false;
                
                schema.Columns.Add(colvarPointout);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Int32;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarPointType = new TableSchema.TableColumn(schema);
                colvarPointType.ColumnName = "point_type";
                colvarPointType.DataType = DbType.Byte;
                colvarPointType.MaxLength = 0;
                colvarPointType.AutoIncrement = false;
                colvarPointType.IsNullable = false;
                colvarPointType.IsPrimaryKey = false;
                colvarPointType.IsForeignKey = false;
                colvarPointType.IsReadOnly = false;
                
                schema.Columns.Add(colvarPointType);
                
                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = true;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountId);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarInvoiceNumber = new TableSchema.TableColumn(schema);
                colvarInvoiceNumber.ColumnName = "invoice_number";
                colvarInvoiceNumber.DataType = DbType.String;
                colvarInvoiceNumber.MaxLength = 10;
                colvarInvoiceNumber.AutoIncrement = false;
                colvarInvoiceNumber.IsNullable = true;
                colvarInvoiceNumber.IsPrimaryKey = false;
                colvarInvoiceNumber.IsForeignKey = false;
                colvarInvoiceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceNumber);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_point_transaction_order_record",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpPointTransactionOrderRecord()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpPointTransactionOrderRecord(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpPointTransactionOrderRecord(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpPointTransactionOrderRecord(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int? Id 
	    {
		    get
		    {
			    return GetColumnValue<int?>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid? Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("guid");
		    }
            set 
		    {
			    SetColumnValue("guid", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ExpireTime")]
        [Bindable(true)]
        public DateTime? ExpireTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("expire_time");
		    }
            set 
		    {
			    SetColumnValue("expire_time", value);
            }
        }
	      
        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message 
	    {
		    get
		    {
			    return GetColumnValue<string>("message");
		    }
            set 
		    {
			    SetColumnValue("message", value);
            }
        }
	      
        [XmlAttribute("Pointin")]
        [Bindable(true)]
        public int Pointin 
	    {
		    get
		    {
			    return GetColumnValue<int>("pointin");
		    }
            set 
		    {
			    SetColumnValue("pointin", value);
            }
        }
	      
        [XmlAttribute("Pointout")]
        [Bindable(true)]
        public int Pointout 
	    {
		    get
		    {
			    return GetColumnValue<int>("pointout");
		    }
            set 
		    {
			    SetColumnValue("pointout", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public int? Total 
	    {
		    get
		    {
			    return GetColumnValue<int?>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("PointType")]
        [Bindable(true)]
        public byte PointType 
	    {
		    get
		    {
			    return GetColumnValue<byte>("point_type");
		    }
            set 
		    {
			    SetColumnValue("point_type", value);
            }
        }
	      
        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_id");
		    }
            set 
		    {
			    SetColumnValue("account_id", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("InvoiceNumber")]
        [Bindable(true)]
        public string InvoiceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_number");
		    }
            set 
		    {
			    SetColumnValue("invoice_number", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string Guid = @"guid";
            
            public static string CreateTime = @"create_time";
            
            public static string ExpireTime = @"expire_time";
            
            public static string Message = @"message";
            
            public static string Pointin = @"pointin";
            
            public static string Pointout = @"pointout";
            
            public static string Total = @"total";
            
            public static string PointType = @"point_type";
            
            public static string AccountId = @"account_id";
            
            public static string UserId = @"user_id";
            
            public static string InvoiceNumber = @"invoice_number";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

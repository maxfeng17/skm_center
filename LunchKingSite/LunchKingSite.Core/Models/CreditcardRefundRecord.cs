using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the CreditcardRefundRecord class.
    /// </summary>
    [Serializable]
    public partial class CreditcardRefundRecordCollection : RepositoryList<CreditcardRefundRecord, CreditcardRefundRecordCollection>
    {
        public CreditcardRefundRecordCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CreditcardRefundRecordCollection</returns>
        public CreditcardRefundRecordCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CreditcardRefundRecord o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the creditcard_refund_record table.
    /// </summary>
    [Serializable]
    public partial class CreditcardRefundRecord : RepositoryRecord<CreditcardRefundRecord>, IRecordBase
    {
        #region .ctors and Default Settings

        public CreditcardRefundRecord()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public CreditcardRefundRecord(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("creditcard_refund_record", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarTransId = new TableSchema.TableColumn(schema);
                colvarTransId.ColumnName = "trans_id";
                colvarTransId.DataType = DbType.AnsiString;
                colvarTransId.MaxLength = 40;
                colvarTransId.AutoIncrement = false;
                colvarTransId.IsNullable = false;
                colvarTransId.IsPrimaryKey = true;
                colvarTransId.IsForeignKey = false;
                colvarTransId.IsReadOnly = false;
                colvarTransId.DefaultSetting = @"";
                colvarTransId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTransId);

                TableSchema.TableColumn colvarApiProvider = new TableSchema.TableColumn(schema);
                colvarApiProvider.ColumnName = "api_provider";
                colvarApiProvider.DataType = DbType.Int32;
                colvarApiProvider.MaxLength = 0;
                colvarApiProvider.AutoIncrement = false;
                colvarApiProvider.IsNullable = false;
                colvarApiProvider.IsPrimaryKey = false;
                colvarApiProvider.IsForeignKey = false;
                colvarApiProvider.IsReadOnly = false;
                colvarApiProvider.DefaultSetting = @"";
                colvarApiProvider.ForeignKeyTableName = "";
                schema.Columns.Add(colvarApiProvider);

                TableSchema.TableColumn colvarRefundReturnNumber = new TableSchema.TableColumn(schema);
                colvarRefundReturnNumber.ColumnName = "refund_return_number";
                colvarRefundReturnNumber.DataType = DbType.AnsiString;
                colvarRefundReturnNumber.MaxLength = 50;
                colvarRefundReturnNumber.AutoIncrement = false;
                colvarRefundReturnNumber.IsNullable = false;
                colvarRefundReturnNumber.IsPrimaryKey = false;
                colvarRefundReturnNumber.IsForeignKey = false;
                colvarRefundReturnNumber.IsReadOnly = false;
                colvarRefundReturnNumber.DefaultSetting = @"";
                colvarRefundReturnNumber.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRefundReturnNumber);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = -1;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                colvarMessage.DefaultSetting = @"";
                colvarMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMessage);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("creditcard_refund_record", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("TransId")]
        [Bindable(true)]
        public string TransId
        {
            get { return GetColumnValue<string>(Columns.TransId); }
            set { SetColumnValue(Columns.TransId, value); }
        }

        [XmlAttribute("ApiProvider")]
        [Bindable(true)]
        public int ApiProvider
        {
            get { return GetColumnValue<int>(Columns.ApiProvider); }
            set { SetColumnValue(Columns.ApiProvider, value); }
        }

        [XmlAttribute("RefundReturnNumber")]
        [Bindable(true)]
        public string RefundReturnNumber
        {
            get { return GetColumnValue<string>(Columns.RefundReturnNumber); }
            set { SetColumnValue(Columns.RefundReturnNumber, value); }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get { return GetColumnValue<string>(Columns.Message); }
            set { SetColumnValue(Columns.Message, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn TransIdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ApiProviderColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn RefundReturnNumberColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[3]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string TransId = @"trans_id";
            public static string ApiProvider = @"api_provider";
            public static string RefundReturnNumber = @"refund_return_number";
            public static string Message = @"message";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the TaskFlow class.
    /// </summary>
    [Serializable]
    public partial class TaskFlowCollection : RepositoryList<TaskFlow, TaskFlowCollection>
    {
        public TaskFlowCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TaskFlowCollection</returns>
        public TaskFlowCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TaskFlow o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the task_flow table.
    /// </summary>
    [Serializable]
    public partial class TaskFlow : RepositoryRecord<TaskFlow>, IRecordBase
    {
        #region .ctors and Default Settings

        public TaskFlow()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public TaskFlow(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("task_flow", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarProcessId = new TableSchema.TableColumn(schema);
                colvarProcessId.ColumnName = "process_id";
                colvarProcessId.DataType = DbType.Guid;
                colvarProcessId.MaxLength = 0;
                colvarProcessId.AutoIncrement = false;
                colvarProcessId.IsNullable = false;
                colvarProcessId.IsPrimaryKey = true;
                colvarProcessId.IsForeignKey = false;
                colvarProcessId.IsReadOnly = false;
                colvarProcessId.DefaultSetting = @"";
                colvarProcessId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProcessId);

                TableSchema.TableColumn colvarApplyUser = new TableSchema.TableColumn(schema);
                colvarApplyUser.ColumnName = "apply_user";
                colvarApplyUser.DataType = DbType.Guid;
                colvarApplyUser.MaxLength = 0;
                colvarApplyUser.AutoIncrement = false;
                colvarApplyUser.IsNullable = false;
                colvarApplyUser.IsPrimaryKey = false;
                colvarApplyUser.IsForeignKey = false;
                colvarApplyUser.IsReadOnly = false;
                colvarApplyUser.DefaultSetting = @"";
                colvarApplyUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarApplyUser);

                TableSchema.TableColumn colvarExecUser = new TableSchema.TableColumn(schema);
                colvarExecUser.ColumnName = "exec_user";
                colvarExecUser.DataType = DbType.Guid;
                colvarExecUser.MaxLength = 0;
                colvarExecUser.AutoIncrement = false;
                colvarExecUser.IsNullable = false;
                colvarExecUser.IsPrimaryKey = false;
                colvarExecUser.IsForeignKey = false;
                colvarExecUser.IsReadOnly = false;
                colvarExecUser.DefaultSetting = @"";
                colvarExecUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarExecUser);

                TableSchema.TableColumn colvarAgentUser = new TableSchema.TableColumn(schema);
                colvarAgentUser.ColumnName = "agent_user";
                colvarAgentUser.DataType = DbType.Guid;
                colvarAgentUser.MaxLength = 0;
                colvarAgentUser.AutoIncrement = false;
                colvarAgentUser.IsNullable = false;
                colvarAgentUser.IsPrimaryKey = false;
                colvarAgentUser.IsForeignKey = false;
                colvarAgentUser.IsReadOnly = false;
                colvarAgentUser.DefaultSetting = @"";
                colvarAgentUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAgentUser);

                TableSchema.TableColumn colvarRealExecUser = new TableSchema.TableColumn(schema);
                colvarRealExecUser.ColumnName = "real_exec_user";
                colvarRealExecUser.DataType = DbType.Guid;
                colvarRealExecUser.MaxLength = 0;
                colvarRealExecUser.AutoIncrement = false;
                colvarRealExecUser.IsNullable = false;
                colvarRealExecUser.IsPrimaryKey = false;
                colvarRealExecUser.IsForeignKey = false;
                colvarRealExecUser.IsReadOnly = false;
                colvarRealExecUser.DefaultSetting = @"";
                colvarRealExecUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRealExecUser);

                TableSchema.TableColumn colvarCategoryName = new TableSchema.TableColumn(schema);
                colvarCategoryName.ColumnName = "category_name";
                colvarCategoryName.DataType = DbType.Int32;
                colvarCategoryName.MaxLength = 0;
                colvarCategoryName.AutoIncrement = false;
                colvarCategoryName.IsNullable = false;
                colvarCategoryName.IsPrimaryKey = false;
                colvarCategoryName.IsForeignKey = false;
                colvarCategoryName.IsReadOnly = false;
                colvarCategoryName.DefaultSetting = @"";
                colvarCategoryName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCategoryName);

                TableSchema.TableColumn colvarSubject = new TableSchema.TableColumn(schema);
                colvarSubject.ColumnName = "subject";
                colvarSubject.DataType = DbType.String;
                colvarSubject.MaxLength = 100;
                colvarSubject.AutoIncrement = false;
                colvarSubject.IsNullable = true;
                colvarSubject.IsPrimaryKey = false;
                colvarSubject.IsForeignKey = false;
                colvarSubject.IsReadOnly = false;
                colvarSubject.DefaultSetting = @"";
                colvarSubject.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSubject);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarEipTaskStatus = new TableSchema.TableColumn(schema);
                colvarEipTaskStatus.ColumnName = "eip_task_status";
                colvarEipTaskStatus.DataType = DbType.Int32;
                colvarEipTaskStatus.MaxLength = 0;
                colvarEipTaskStatus.AutoIncrement = false;
                colvarEipTaskStatus.IsNullable = false;
                colvarEipTaskStatus.IsPrimaryKey = false;
                colvarEipTaskStatus.IsForeignKey = false;
                colvarEipTaskStatus.IsReadOnly = false;
                colvarEipTaskStatus.DefaultSetting = @"";
                colvarEipTaskStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEipTaskStatus);

                TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
                colvarUrl.ColumnName = "url";
                colvarUrl.DataType = DbType.AnsiString;
                colvarUrl.MaxLength = 2000;
                colvarUrl.AutoIncrement = false;
                colvarUrl.IsNullable = true;
                colvarUrl.IsPrimaryKey = false;
                colvarUrl.IsForeignKey = false;
                colvarUrl.IsReadOnly = false;
                colvarUrl.DefaultSetting = @"";
                colvarUrl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUrl);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("task_flow", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("ProcessId")]
        [Bindable(true)]
        public Guid ProcessId
        {
            get { return GetColumnValue<Guid>(Columns.ProcessId); }
            set { SetColumnValue(Columns.ProcessId, value); }
        }

        [XmlAttribute("ApplyUser")]
        [Bindable(true)]
        public Guid ApplyUser
        {
            get { return GetColumnValue<Guid>(Columns.ApplyUser); }
            set { SetColumnValue(Columns.ApplyUser, value); }
        }

        [XmlAttribute("ExecUser")]
        [Bindable(true)]
        public Guid ExecUser
        {
            get { return GetColumnValue<Guid>(Columns.ExecUser); }
            set { SetColumnValue(Columns.ExecUser, value); }
        }

        [XmlAttribute("AgentUser")]
        [Bindable(true)]
        public Guid AgentUser
        {
            get { return GetColumnValue<Guid>(Columns.AgentUser); }
            set { SetColumnValue(Columns.AgentUser, value); }
        }

        [XmlAttribute("RealExecUser")]
        [Bindable(true)]
        public Guid RealExecUser
        {
            get { return GetColumnValue<Guid>(Columns.RealExecUser); }
            set { SetColumnValue(Columns.RealExecUser, value); }
        }

        [XmlAttribute("CategoryName")]
        [Bindable(true)]
        public int CategoryName
        {
            get { return GetColumnValue<int>(Columns.CategoryName); }
            set { SetColumnValue(Columns.CategoryName, value); }
        }

        [XmlAttribute("Subject")]
        [Bindable(true)]
        public string Subject
        {
            get { return GetColumnValue<string>(Columns.Subject); }
            set { SetColumnValue(Columns.Subject, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime
        {
            get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("EipTaskStatus")]
        [Bindable(true)]
        public int EipTaskStatus
        {
            get { return GetColumnValue<int>(Columns.EipTaskStatus); }
            set { SetColumnValue(Columns.EipTaskStatus, value); }
        }

        [XmlAttribute("Url")]
        [Bindable(true)]
        public string Url
        {
            get { return GetColumnValue<string>(Columns.Url); }
            set { SetColumnValue(Columns.Url, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn ProcessIdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ApplyUserColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ExecUserColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn AgentUserColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn RealExecUserColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CategoryNameColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn SubjectColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn EipTaskStatusColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn UrlColumn
        {
            get { return Schema.Columns[10]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string ProcessId = @"process_id";
            public static string ApplyUser = @"apply_user";
            public static string ExecUser = @"exec_user";
            public static string AgentUser = @"agent_user";
            public static string RealExecUser = @"real_exec_user";
            public static string CategoryName = @"category_name";
            public static string Subject = @"subject";
            public static string CreateTime = @"create_time";
            public static string ModifyTime = @"modify_time";
            public static string EipTaskStatus = @"eip_task_status";
            public static string Url = @"url";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

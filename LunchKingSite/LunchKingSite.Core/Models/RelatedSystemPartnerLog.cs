using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the RelatedSystemPartnerLog class.
	/// </summary>
    [Serializable]
	public partial class RelatedSystemPartnerLogCollection : RepositoryList<RelatedSystemPartnerLog, RelatedSystemPartnerLogCollection>
	{	   
		public RelatedSystemPartnerLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>RelatedSystemPartnerLogCollection</returns>
		public RelatedSystemPartnerLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                RelatedSystemPartnerLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the related_system_partner_log table.
	/// </summary>
	[Serializable]
	public partial class RelatedSystemPartnerLog : RepositoryRecord<RelatedSystemPartnerLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public RelatedSystemPartnerLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public RelatedSystemPartnerLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("related_system_partner_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarPartnerId = new TableSchema.TableColumn(schema);
				colvarPartnerId.ColumnName = "partner_id";
				colvarPartnerId.DataType = DbType.Int32;
				colvarPartnerId.MaxLength = 0;
				colvarPartnerId.AutoIncrement = false;
				colvarPartnerId.IsNullable = false;
				colvarPartnerId.IsPrimaryKey = false;
				colvarPartnerId.IsForeignKey = false;
				colvarPartnerId.IsReadOnly = false;
				colvarPartnerId.DefaultSetting = @"";
				colvarPartnerId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPartnerId);
				
				TableSchema.TableColumn colvarMessageType = new TableSchema.TableColumn(schema);
				colvarMessageType.ColumnName = "message_type";
				colvarMessageType.DataType = DbType.Int32;
				colvarMessageType.MaxLength = 0;
				colvarMessageType.AutoIncrement = false;
				colvarMessageType.IsNullable = true;
				colvarMessageType.IsPrimaryKey = false;
				colvarMessageType.IsForeignKey = false;
				colvarMessageType.IsReadOnly = false;
				colvarMessageType.DefaultSetting = @"";
				colvarMessageType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessageType);
				
				TableSchema.TableColumn colvarMessageContent = new TableSchema.TableColumn(schema);
				colvarMessageContent.ColumnName = "message_content";
				colvarMessageContent.DataType = DbType.String;
				colvarMessageContent.MaxLength = 1073741823;
				colvarMessageContent.AutoIncrement = false;
				colvarMessageContent.IsNullable = true;
				colvarMessageContent.IsPrimaryKey = false;
				colvarMessageContent.IsForeignKey = false;
				colvarMessageContent.IsReadOnly = false;
				colvarMessageContent.DefaultSetting = @"";
				colvarMessageContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessageContent);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 200;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("related_system_partner_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("PartnerId")]
		[Bindable(true)]
		public int PartnerId 
		{
			get { return GetColumnValue<int>(Columns.PartnerId); }
			set { SetColumnValue(Columns.PartnerId, value); }
		}
		  
		[XmlAttribute("MessageType")]
		[Bindable(true)]
		public int? MessageType 
		{
			get { return GetColumnValue<int?>(Columns.MessageType); }
			set { SetColumnValue(Columns.MessageType, value); }
		}
		  
		[XmlAttribute("MessageContent")]
		[Bindable(true)]
		public string MessageContent 
		{
			get { return GetColumnValue<string>(Columns.MessageContent); }
			set { SetColumnValue(Columns.MessageContent, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status 
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PartnerIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageContentColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string PartnerId = @"partner_id";
			 public static string MessageType = @"message_type";
			 public static string MessageContent = @"message_content";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string Status = @"status";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

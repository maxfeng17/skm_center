using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewFamiportPinRollbackLog class.
    /// </summary>
    [Serializable]
    public partial class ViewFamiportPinRollbackLogCollection : ReadOnlyList<ViewFamiportPinRollbackLog, ViewFamiportPinRollbackLogCollection>
    {        
        public ViewFamiportPinRollbackLogCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_famiport_pin_rollback_log view.
    /// </summary>
    [Serializable]
    public partial class ViewFamiportPinRollbackLog : ReadOnlyRecord<ViewFamiportPinRollbackLog>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_famiport_pin_rollback_log", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarPezCode = new TableSchema.TableColumn(schema);
                colvarPezCode.ColumnName = "PezCode";
                colvarPezCode.DataType = DbType.String;
                colvarPezCode.MaxLength = 50;
                colvarPezCode.AutoIncrement = false;
                colvarPezCode.IsNullable = true;
                colvarPezCode.IsPrimaryKey = false;
                colvarPezCode.IsForeignKey = false;
                colvarPezCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarPezCode);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 15;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarTenCode = new TableSchema.TableColumn(schema);
                colvarTenCode.ColumnName = "ten_code";
                colvarTenCode.DataType = DbType.AnsiString;
                colvarTenCode.MaxLength = 6;
                colvarTenCode.AutoIncrement = false;
                colvarTenCode.IsNullable = false;
                colvarTenCode.IsPrimaryKey = false;
                colvarTenCode.IsForeignKey = false;
                colvarTenCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarTenCode);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_famiport_pin_rollback_log",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewFamiportPinRollbackLog()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewFamiportPinRollbackLog(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewFamiportPinRollbackLog(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewFamiportPinRollbackLog(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("PezCode")]
        [Bindable(true)]
        public string PezCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("PezCode");
		    }
            set 
		    {
			    SetColumnValue("PezCode", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("TenCode")]
        [Bindable(true)]
        public string TenCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("ten_code");
		    }
            set 
		    {
			    SetColumnValue("ten_code", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string PezCode = @"PezCode";
            
            public static string ItemName = @"item_name";
            
            public static string CreateTime = @"create_time";
            
            public static string TenCode = @"ten_code";
            
            public static string UserName = @"user_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

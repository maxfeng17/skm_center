using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEventPromoCategory class.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromoCategoryCollection : ReadOnlyList<ViewEventPromoCategory, ViewEventPromoCategoryCollection>
    {        
        public ViewEventPromoCategoryCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_event_promo_category view.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromoCategory : ReadOnlyRecord<ViewEventPromoCategory>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_event_promo_category", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "Seq";
                colvarSeq.DataType = DbType.Int64;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = true;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarSeq);
                
                TableSchema.TableColumn colvarCid = new TableSchema.TableColumn(schema);
                colvarCid.ColumnName = "cid";
                colvarCid.DataType = DbType.Int32;
                colvarCid.MaxLength = 0;
                colvarCid.AutoIncrement = false;
                colvarCid.IsNullable = false;
                colvarCid.IsPrimaryKey = false;
                colvarCid.IsForeignKey = false;
                colvarCid.IsReadOnly = false;
                
                schema.Columns.Add(colvarCid);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 250;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = true;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "Description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = 100;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = true;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescription);
                
                TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
                colvarCategory.ColumnName = "category";
                colvarCategory.DataType = DbType.String;
                colvarCategory.MaxLength = 50;
                colvarCategory.AutoIncrement = false;
                colvarCategory.IsNullable = true;
                colvarCategory.IsPrimaryKey = false;
                colvarCategory.IsForeignKey = false;
                colvarCategory.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = false;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
                colvarItemOrigPrice.ColumnName = "item_orig_price";
                colvarItemOrigPrice.DataType = DbType.Currency;
                colvarItemOrigPrice.MaxLength = 0;
                colvarItemOrigPrice.AutoIncrement = false;
                colvarItemOrigPrice.IsNullable = false;
                colvarItemOrigPrice.IsPrimaryKey = false;
                colvarItemOrigPrice.IsForeignKey = false;
                colvarItemOrigPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemOrigPrice);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarDiscount = new TableSchema.TableColumn(schema);
                colvarDiscount.ColumnName = "Discount";
                colvarDiscount.DataType = DbType.Currency;
                colvarDiscount.MaxLength = 0;
                colvarDiscount.AutoIncrement = false;
                colvarDiscount.IsNullable = true;
                colvarDiscount.IsPrimaryKey = false;
                colvarDiscount.IsForeignKey = false;
                colvarDiscount.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscount);
                
                TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
                colvarImagePath.ColumnName = "image_path";
                colvarImagePath.DataType = DbType.String;
                colvarImagePath.MaxLength = 500;
                colvarImagePath.AutoIncrement = false;
                colvarImagePath.IsNullable = true;
                colvarImagePath.IsPrimaryKey = false;
                colvarImagePath.IsForeignKey = false;
                colvarImagePath.IsReadOnly = false;
                
                schema.Columns.Add(colvarImagePath);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_event_promo_category",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewEventPromoCategory()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEventPromoCategory(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewEventPromoCategory(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewEventPromoCategory(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Seq")]
        [Bindable(true)]
        public long? Seq 
	    {
		    get
		    {
			    return GetColumnValue<long?>("Seq");
		    }
            set 
		    {
			    SetColumnValue("Seq", value);
            }
        }
	      
        [XmlAttribute("Cid")]
        [Bindable(true)]
        public int Cid 
	    {
		    get
		    {
			    return GetColumnValue<int>("cid");
		    }
            set 
		    {
			    SetColumnValue("cid", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("title");
		    }
            set 
		    {
			    SetColumnValue("title", value);
            }
        }
	      
        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description 
	    {
		    get
		    {
			    return GetColumnValue<string>("Description");
		    }
            set 
		    {
			    SetColumnValue("Description", value);
            }
        }
	      
        [XmlAttribute("Category")]
        [Bindable(true)]
        public string Category 
	    {
		    get
		    {
			    return GetColumnValue<string>("category");
		    }
            set 
		    {
			    SetColumnValue("category", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int ItemId 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("ItemOrigPrice")]
        [Bindable(true)]
        public decimal ItemOrigPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_orig_price");
		    }
            set 
		    {
			    SetColumnValue("item_orig_price", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("Discount")]
        [Bindable(true)]
        public decimal? Discount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("Discount");
		    }
            set 
		    {
			    SetColumnValue("Discount", value);
            }
        }
	      
        [XmlAttribute("ImagePath")]
        [Bindable(true)]
        public string ImagePath 
	    {
		    get
		    {
			    return GetColumnValue<string>("image_path");
		    }
            set 
		    {
			    SetColumnValue("image_path", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Seq = @"Seq";
            
            public static string Cid = @"cid";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string Title = @"title";
            
            public static string Description = @"Description";
            
            public static string Category = @"category";
            
            public static string ItemId = @"item_id";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string ItemOrigPrice = @"item_orig_price";
            
            public static string ItemPrice = @"item_price";
            
            public static string Discount = @"Discount";
            
            public static string ImagePath = @"image_path";
            
            public static string CreateTime = @"create_time";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MemberCollectDealExpireLog class.
	/// </summary>
    [Serializable]
	public partial class MemberCollectDealExpireLogCollection : RepositoryList<MemberCollectDealExpireLog, MemberCollectDealExpireLogCollection>
	{	   
		public MemberCollectDealExpireLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberCollectDealExpireLogCollection</returns>
		public MemberCollectDealExpireLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberCollectDealExpireLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the member_collect_deal_expire_log table.
	/// </summary>
	[Serializable]
	public partial class MemberCollectDealExpireLog : RepositoryRecord<MemberCollectDealExpireLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MemberCollectDealExpireLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MemberCollectDealExpireLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_collect_deal_expire_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarMemberUniqueId = new TableSchema.TableColumn(schema);
				colvarMemberUniqueId.ColumnName = "member_unique_id";
				colvarMemberUniqueId.DataType = DbType.Int32;
				colvarMemberUniqueId.MaxLength = 0;
				colvarMemberUniqueId.AutoIncrement = false;
				colvarMemberUniqueId.IsNullable = false;
				colvarMemberUniqueId.IsPrimaryKey = true;
				colvarMemberUniqueId.IsForeignKey = false;
				colvarMemberUniqueId.IsReadOnly = false;
				colvarMemberUniqueId.DefaultSetting = @"";
				colvarMemberUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberUniqueId);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = true;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarSendType = new TableSchema.TableColumn(schema);
				colvarSendType.ColumnName = "send_type";
				colvarSendType.DataType = DbType.Byte;
				colvarSendType.MaxLength = 0;
				colvarSendType.AutoIncrement = false;
				colvarSendType.IsNullable = false;
				colvarSendType.IsPrimaryKey = true;
				colvarSendType.IsForeignKey = false;
				colvarSendType.IsReadOnly = false;
				colvarSendType.DefaultSetting = @"";
				colvarSendType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendType);
				
				TableSchema.TableColumn colvarSendTime = new TableSchema.TableColumn(schema);
				colvarSendTime.ColumnName = "send_time";
				colvarSendTime.DataType = DbType.DateTime;
				colvarSendTime.MaxLength = 0;
				colvarSendTime.AutoIncrement = false;
				colvarSendTime.IsNullable = false;
				colvarSendTime.IsPrimaryKey = false;
				colvarSendTime.IsForeignKey = false;
				colvarSendTime.IsReadOnly = false;
				
						colvarSendTime.DefaultSetting = @"(getdate())";
				colvarSendTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_collect_deal_expire_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("MemberUniqueId")]
		[Bindable(true)]
		public int MemberUniqueId 
		{
			get { return GetColumnValue<int>(Columns.MemberUniqueId); }
			set { SetColumnValue(Columns.MemberUniqueId, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("SendType")]
		[Bindable(true)]
		public byte SendType 
		{
			get { return GetColumnValue<byte>(Columns.SendType); }
			set { SetColumnValue(Columns.SendType, value); }
		}
		  
		[XmlAttribute("SendTime")]
		[Bindable(true)]
		public DateTime SendTime 
		{
			get { return GetColumnValue<DateTime>(Columns.SendTime); }
			set { SetColumnValue(Columns.SendTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn MemberUniqueIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SendTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SendTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string MemberUniqueId = @"member_unique_id";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string SendType = @"send_type";
			 public static string SendTime = @"send_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponOrderStatusLog class.
    /// </summary>
    [Serializable]
    public partial class ViewPponOrderStatusLogCollection : ReadOnlyList<ViewPponOrderStatusLog, ViewPponOrderStatusLogCollection>
    {        
        public ViewPponOrderStatusLogCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_order_status_log view.
    /// </summary>
    [Serializable]
    public partial class ViewPponOrderStatusLog : ReadOnlyRecord<ViewPponOrderStatusLog>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_order_status_log", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarMemberEmail = new TableSchema.TableColumn(schema);
                colvarMemberEmail.ColumnName = "member_email";
                colvarMemberEmail.DataType = DbType.String;
                colvarMemberEmail.MaxLength = 256;
                colvarMemberEmail.AutoIncrement = false;
                colvarMemberEmail.IsNullable = false;
                colvarMemberEmail.IsPrimaryKey = false;
                colvarMemberEmail.IsForeignKey = false;
                colvarMemberEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberEmail);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = false;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarParentOrderId = new TableSchema.TableColumn(schema);
                colvarParentOrderId.ColumnName = "parent_order_id";
                colvarParentOrderId.DataType = DbType.Guid;
                colvarParentOrderId.MaxLength = 0;
                colvarParentOrderId.AutoIncrement = false;
                colvarParentOrderId.IsNullable = true;
                colvarParentOrderId.IsPrimaryKey = false;
                colvarParentOrderId.IsForeignKey = false;
                colvarParentOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentOrderId);
                
                TableSchema.TableColumn colvarGroupOrderStatus = new TableSchema.TableColumn(schema);
                colvarGroupOrderStatus.ColumnName = "group_order_status";
                colvarGroupOrderStatus.DataType = DbType.Int32;
                colvarGroupOrderStatus.MaxLength = 0;
                colvarGroupOrderStatus.AutoIncrement = false;
                colvarGroupOrderStatus.IsNullable = true;
                colvarGroupOrderStatus.IsPrimaryKey = false;
                colvarGroupOrderStatus.IsForeignKey = false;
                colvarGroupOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupOrderStatus);
                
                TableSchema.TableColumn colvarOrderLogId = new TableSchema.TableColumn(schema);
                colvarOrderLogId.ColumnName = "order_log_id";
                colvarOrderLogId.DataType = DbType.Int32;
                colvarOrderLogId.MaxLength = 0;
                colvarOrderLogId.AutoIncrement = false;
                colvarOrderLogId.IsNullable = false;
                colvarOrderLogId.IsPrimaryKey = false;
                colvarOrderLogId.IsForeignKey = false;
                colvarOrderLogId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderLogId);
                
                TableSchema.TableColumn colvarOrderLogStatus = new TableSchema.TableColumn(schema);
                colvarOrderLogStatus.ColumnName = "order_log_status";
                colvarOrderLogStatus.DataType = DbType.Int32;
                colvarOrderLogStatus.MaxLength = 0;
                colvarOrderLogStatus.AutoIncrement = false;
                colvarOrderLogStatus.IsNullable = false;
                colvarOrderLogStatus.IsPrimaryKey = false;
                colvarOrderLogStatus.IsForeignKey = false;
                colvarOrderLogStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderLogStatus);
                
                TableSchema.TableColumn colvarOrderLogCreateId = new TableSchema.TableColumn(schema);
                colvarOrderLogCreateId.ColumnName = "order_log_create_id";
                colvarOrderLogCreateId.DataType = DbType.String;
                colvarOrderLogCreateId.MaxLength = 50;
                colvarOrderLogCreateId.AutoIncrement = false;
                colvarOrderLogCreateId.IsNullable = false;
                colvarOrderLogCreateId.IsPrimaryKey = false;
                colvarOrderLogCreateId.IsForeignKey = false;
                colvarOrderLogCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderLogCreateId);
                
                TableSchema.TableColumn colvarOrderLogCreateTime = new TableSchema.TableColumn(schema);
                colvarOrderLogCreateTime.ColumnName = "order_log_create_time";
                colvarOrderLogCreateTime.DataType = DbType.DateTime;
                colvarOrderLogCreateTime.MaxLength = 0;
                colvarOrderLogCreateTime.AutoIncrement = false;
                colvarOrderLogCreateTime.IsNullable = false;
                colvarOrderLogCreateTime.IsPrimaryKey = false;
                colvarOrderLogCreateTime.IsForeignKey = false;
                colvarOrderLogCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderLogCreateTime);
                
                TableSchema.TableColumn colvarOrderLogMessage = new TableSchema.TableColumn(schema);
                colvarOrderLogMessage.ColumnName = "order_log_message";
                colvarOrderLogMessage.DataType = DbType.String;
                colvarOrderLogMessage.MaxLength = 1073741823;
                colvarOrderLogMessage.AutoIncrement = false;
                colvarOrderLogMessage.IsNullable = true;
                colvarOrderLogMessage.IsPrimaryKey = false;
                colvarOrderLogMessage.IsForeignKey = false;
                colvarOrderLogMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderLogMessage);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarBusinessHourId = new TableSchema.TableColumn(schema);
                colvarBusinessHourId.ColumnName = "business_hour_id";
                colvarBusinessHourId.DataType = DbType.AnsiString;
                colvarBusinessHourId.MaxLength = 20;
                colvarBusinessHourId.AutoIncrement = false;
                colvarBusinessHourId.IsNullable = true;
                colvarBusinessHourId.IsPrimaryKey = false;
                colvarBusinessHourId.IsForeignKey = false;
                colvarBusinessHourId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourId);
                
                TableSchema.TableColumn colvarBusinessHourTypeId = new TableSchema.TableColumn(schema);
                colvarBusinessHourTypeId.ColumnName = "business_hour_type_id";
                colvarBusinessHourTypeId.DataType = DbType.Int32;
                colvarBusinessHourTypeId.MaxLength = 0;
                colvarBusinessHourTypeId.AutoIncrement = false;
                colvarBusinessHourTypeId.IsNullable = false;
                colvarBusinessHourTypeId.IsPrimaryKey = false;
                colvarBusinessHourTypeId.IsForeignKey = false;
                colvarBusinessHourTypeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourTypeId);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
                colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeS.MaxLength = 0;
                colvarBusinessHourDeliverTimeS.AutoIncrement = false;
                colvarBusinessHourDeliverTimeS.IsNullable = true;
                colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeS.IsForeignKey = false;
                colvarBusinessHourDeliverTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeS);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliveryCharge = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliveryCharge.ColumnName = "business_hour_delivery_charge";
                colvarBusinessHourDeliveryCharge.DataType = DbType.Currency;
                colvarBusinessHourDeliveryCharge.MaxLength = 0;
                colvarBusinessHourDeliveryCharge.AutoIncrement = false;
                colvarBusinessHourDeliveryCharge.IsNullable = false;
                colvarBusinessHourDeliveryCharge.IsPrimaryKey = false;
                colvarBusinessHourDeliveryCharge.IsForeignKey = false;
                colvarBusinessHourDeliveryCharge.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliveryCharge);
                
                TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
                colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
                colvarBusinessHourOrderMinimum.MaxLength = 0;
                colvarBusinessHourOrderMinimum.AutoIncrement = false;
                colvarBusinessHourOrderMinimum.IsNullable = false;
                colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
                colvarBusinessHourOrderMinimum.IsForeignKey = false;
                colvarBusinessHourOrderMinimum.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderMinimum);
                
                TableSchema.TableColumn colvarBusinessHourPreparationTime = new TableSchema.TableColumn(schema);
                colvarBusinessHourPreparationTime.ColumnName = "business_hour_preparation_time";
                colvarBusinessHourPreparationTime.DataType = DbType.Int32;
                colvarBusinessHourPreparationTime.MaxLength = 0;
                colvarBusinessHourPreparationTime.AutoIncrement = false;
                colvarBusinessHourPreparationTime.IsNullable = false;
                colvarBusinessHourPreparationTime.IsPrimaryKey = false;
                colvarBusinessHourPreparationTime.IsForeignKey = false;
                colvarBusinessHourPreparationTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourPreparationTime);
                
                TableSchema.TableColumn colvarBusinessHourOnline = new TableSchema.TableColumn(schema);
                colvarBusinessHourOnline.ColumnName = "business_hour_online";
                colvarBusinessHourOnline.DataType = DbType.Boolean;
                colvarBusinessHourOnline.MaxLength = 0;
                colvarBusinessHourOnline.AutoIncrement = false;
                colvarBusinessHourOnline.IsNullable = false;
                colvarBusinessHourOnline.IsPrimaryKey = false;
                colvarBusinessHourOnline.IsForeignKey = false;
                colvarBusinessHourOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOnline);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
                colvarOrderTotalLimit.ColumnName = "order_total_limit";
                colvarOrderTotalLimit.DataType = DbType.Currency;
                colvarOrderTotalLimit.MaxLength = 0;
                colvarOrderTotalLimit.AutoIncrement = false;
                colvarOrderTotalLimit.IsNullable = true;
                colvarOrderTotalLimit.IsPrimaryKey = false;
                colvarOrderTotalLimit.IsForeignKey = false;
                colvarOrderTotalLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTotalLimit);
                
                TableSchema.TableColumn colvarDeliveryLimit = new TableSchema.TableColumn(schema);
                colvarDeliveryLimit.ColumnName = "delivery_limit";
                colvarDeliveryLimit.DataType = DbType.Int32;
                colvarDeliveryLimit.MaxLength = 0;
                colvarDeliveryLimit.AutoIncrement = false;
                colvarDeliveryLimit.IsNullable = true;
                colvarDeliveryLimit.IsPrimaryKey = false;
                colvarDeliveryLimit.IsForeignKey = false;
                colvarDeliveryLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryLimit);
                
                TableSchema.TableColumn colvarHoliday = new TableSchema.TableColumn(schema);
                colvarHoliday.ColumnName = "holiday";
                colvarHoliday.DataType = DbType.Int32;
                colvarHoliday.MaxLength = 0;
                colvarHoliday.AutoIncrement = false;
                colvarHoliday.IsNullable = false;
                colvarHoliday.IsPrimaryKey = false;
                colvarHoliday.IsForeignKey = false;
                colvarHoliday.IsReadOnly = false;
                
                schema.Columns.Add(colvarHoliday);
                
                TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
                colvarEventName.ColumnName = "event_name";
                colvarEventName.DataType = DbType.String;
                colvarEventName.MaxLength = 400;
                colvarEventName.AutoIncrement = false;
                colvarEventName.IsNullable = true;
                colvarEventName.IsPrimaryKey = false;
                colvarEventName.IsForeignKey = false;
                colvarEventName.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventName);
                
                TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
                colvarDepartment.ColumnName = "department";
                colvarDepartment.DataType = DbType.Int32;
                colvarDepartment.MaxLength = 0;
                colvarDepartment.AutoIncrement = false;
                colvarDepartment.IsNullable = false;
                colvarDepartment.IsPrimaryKey = false;
                colvarDepartment.IsForeignKey = false;
                colvarDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepartment);
                
                TableSchema.TableColumn colvarCouponIds = new TableSchema.TableColumn(schema);
                colvarCouponIds.ColumnName = "coupon_ids";
                colvarCouponIds.DataType = DbType.AnsiString;
                colvarCouponIds.MaxLength = -1;
                colvarCouponIds.AutoIncrement = false;
                colvarCouponIds.IsNullable = true;
                colvarCouponIds.IsPrimaryKey = false;
                colvarCouponIds.IsForeignKey = false;
                colvarCouponIds.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponIds);
                
                TableSchema.TableColumn colvarOrderReturnPaper = new TableSchema.TableColumn(schema);
                colvarOrderReturnPaper.ColumnName = "order_return_paper";
                colvarOrderReturnPaper.DataType = DbType.Boolean;
                colvarOrderReturnPaper.MaxLength = 0;
                colvarOrderReturnPaper.AutoIncrement = false;
                colvarOrderReturnPaper.IsNullable = false;
                colvarOrderReturnPaper.IsPrimaryKey = false;
                colvarOrderReturnPaper.IsForeignKey = false;
                colvarOrderReturnPaper.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderReturnPaper);
                
                TableSchema.TableColumn colvarOrderLogReason = new TableSchema.TableColumn(schema);
                colvarOrderLogReason.ColumnName = "order_log_reason";
                colvarOrderLogReason.DataType = DbType.String;
                colvarOrderLogReason.MaxLength = 1073741823;
                colvarOrderLogReason.AutoIncrement = false;
                colvarOrderLogReason.IsNullable = true;
                colvarOrderLogReason.IsPrimaryKey = false;
                colvarOrderLogReason.IsForeignKey = false;
                colvarOrderLogReason.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderLogReason);
                
                TableSchema.TableColumn colvarMessageUpdate = new TableSchema.TableColumn(schema);
                colvarMessageUpdate.ColumnName = "message_update";
                colvarMessageUpdate.DataType = DbType.Boolean;
                colvarMessageUpdate.MaxLength = 0;
                colvarMessageUpdate.AutoIncrement = false;
                colvarMessageUpdate.IsNullable = false;
                colvarMessageUpdate.IsPrimaryKey = false;
                colvarMessageUpdate.IsForeignKey = false;
                colvarMessageUpdate.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessageUpdate);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_order_status_log",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponOrderStatusLog()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponOrderStatusLog(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponOrderStatusLog(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponOrderStatusLog(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("MemberEmail")]
        [Bindable(true)]
        public string MemberEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_email");
		    }
            set 
		    {
			    SetColumnValue("member_email", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal Total 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("ParentOrderId")]
        [Bindable(true)]
        public Guid? ParentOrderId 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("parent_order_id");
		    }
            set 
		    {
			    SetColumnValue("parent_order_id", value);
            }
        }
	      
        [XmlAttribute("GroupOrderStatus")]
        [Bindable(true)]
        public int? GroupOrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_order_status");
		    }
            set 
		    {
			    SetColumnValue("group_order_status", value);
            }
        }
	      
        [XmlAttribute("OrderLogId")]
        [Bindable(true)]
        public int OrderLogId 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_log_id");
		    }
            set 
		    {
			    SetColumnValue("order_log_id", value);
            }
        }
	      
        [XmlAttribute("OrderLogStatus")]
        [Bindable(true)]
        public int OrderLogStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_log_status");
		    }
            set 
		    {
			    SetColumnValue("order_log_status", value);
            }
        }
	      
        [XmlAttribute("OrderLogCreateId")]
        [Bindable(true)]
        public string OrderLogCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_log_create_id");
		    }
            set 
		    {
			    SetColumnValue("order_log_create_id", value);
            }
        }
	      
        [XmlAttribute("OrderLogCreateTime")]
        [Bindable(true)]
        public DateTime OrderLogCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("order_log_create_time");
		    }
            set 
		    {
			    SetColumnValue("order_log_create_time", value);
            }
        }
	      
        [XmlAttribute("OrderLogMessage")]
        [Bindable(true)]
        public string OrderLogMessage 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_log_message");
		    }
            set 
		    {
			    SetColumnValue("order_log_message", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("BusinessHourId")]
        [Bindable(true)]
        public string BusinessHourId 
	    {
		    get
		    {
			    return GetColumnValue<string>("business_hour_id");
		    }
            set 
		    {
			    SetColumnValue("business_hour_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourTypeId")]
        [Bindable(true)]
        public int BusinessHourTypeId 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_type_id");
		    }
            set 
		    {
			    SetColumnValue("business_hour_type_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliveryCharge")]
        [Bindable(true)]
        public decimal BusinessHourDeliveryCharge 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_delivery_charge");
		    }
            set 
		    {
			    SetColumnValue("business_hour_delivery_charge", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderMinimum")]
        [Bindable(true)]
        public decimal BusinessHourOrderMinimum 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_order_minimum");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_minimum", value);
            }
        }
	      
        [XmlAttribute("BusinessHourPreparationTime")]
        [Bindable(true)]
        public int BusinessHourPreparationTime 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_preparation_time");
		    }
            set 
		    {
			    SetColumnValue("business_hour_preparation_time", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOnline")]
        [Bindable(true)]
        public bool BusinessHourOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool>("business_hour_online");
		    }
            set 
		    {
			    SetColumnValue("business_hour_online", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("OrderTotalLimit")]
        [Bindable(true)]
        public decimal? OrderTotalLimit 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("order_total_limit");
		    }
            set 
		    {
			    SetColumnValue("order_total_limit", value);
            }
        }
	      
        [XmlAttribute("DeliveryLimit")]
        [Bindable(true)]
        public int? DeliveryLimit 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_limit");
		    }
            set 
		    {
			    SetColumnValue("delivery_limit", value);
            }
        }
	      
        [XmlAttribute("Holiday")]
        [Bindable(true)]
        public int Holiday 
	    {
		    get
		    {
			    return GetColumnValue<int>("holiday");
		    }
            set 
		    {
			    SetColumnValue("holiday", value);
            }
        }
	      
        [XmlAttribute("EventName")]
        [Bindable(true)]
        public string EventName 
	    {
		    get
		    {
			    return GetColumnValue<string>("event_name");
		    }
            set 
		    {
			    SetColumnValue("event_name", value);
            }
        }
	      
        [XmlAttribute("Department")]
        [Bindable(true)]
        public int Department 
	    {
		    get
		    {
			    return GetColumnValue<int>("department");
		    }
            set 
		    {
			    SetColumnValue("department", value);
            }
        }
	      
        [XmlAttribute("CouponIds")]
        [Bindable(true)]
        public string CouponIds 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_ids");
		    }
            set 
		    {
			    SetColumnValue("coupon_ids", value);
            }
        }
	      
        [XmlAttribute("OrderReturnPaper")]
        [Bindable(true)]
        public bool OrderReturnPaper 
	    {
		    get
		    {
			    return GetColumnValue<bool>("order_return_paper");
		    }
            set 
		    {
			    SetColumnValue("order_return_paper", value);
            }
        }
	      
        [XmlAttribute("OrderLogReason")]
        [Bindable(true)]
        public string OrderLogReason 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_log_reason");
		    }
            set 
		    {
			    SetColumnValue("order_log_reason", value);
            }
        }
	      
        [XmlAttribute("MessageUpdate")]
        [Bindable(true)]
        public bool MessageUpdate 
	    {
		    get
		    {
			    return GetColumnValue<bool>("message_update");
		    }
            set 
		    {
			    SetColumnValue("message_update", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderGuid = @"order_guid";
            
            public static string OrderId = @"order_id";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string SellerName = @"seller_name";
            
            public static string MemberEmail = @"member_email";
            
            public static string UniqueId = @"unique_id";
            
            public static string MemberName = @"member_name";
            
            public static string Total = @"total";
            
            public static string OrderStatus = @"order_status";
            
            public static string ParentOrderId = @"parent_order_id";
            
            public static string GroupOrderStatus = @"group_order_status";
            
            public static string OrderLogId = @"order_log_id";
            
            public static string OrderLogStatus = @"order_log_status";
            
            public static string OrderLogCreateId = @"order_log_create_id";
            
            public static string OrderLogCreateTime = @"order_log_create_time";
            
            public static string OrderLogMessage = @"order_log_message";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string BusinessHourId = @"business_hour_id";
            
            public static string BusinessHourTypeId = @"business_hour_type_id";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
            public static string BusinessHourDeliveryCharge = @"business_hour_delivery_charge";
            
            public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
            
            public static string BusinessHourPreparationTime = @"business_hour_preparation_time";
            
            public static string BusinessHourOnline = @"business_hour_online";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string OrderTotalLimit = @"order_total_limit";
            
            public static string DeliveryLimit = @"delivery_limit";
            
            public static string Holiday = @"holiday";
            
            public static string EventName = @"event_name";
            
            public static string Department = @"department";
            
            public static string CouponIds = @"coupon_ids";
            
            public static string OrderReturnPaper = @"order_return_paper";
            
            public static string OrderLogReason = @"order_log_reason";
            
            public static string MessageUpdate = @"message_update";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ChannelOdmCustomer class.
	/// </summary>
    [Serializable]
	public partial class ChannelOdmCustomerCollection : RepositoryList<ChannelOdmCustomer, ChannelOdmCustomerCollection>
	{	   
		public ChannelOdmCustomerCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ChannelOdmCustomerCollection</returns>
		public ChannelOdmCustomerCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ChannelOdmCustomer o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the channel_odm_customer table.
	/// </summary>
	[Serializable]
	public partial class ChannelOdmCustomer : RepositoryRecord<ChannelOdmCustomer>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ChannelOdmCustomer()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ChannelOdmCustomer(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("channel_odm_customer", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCustomerName = new TableSchema.TableColumn(schema);
				colvarCustomerName.ColumnName = "customer_name";
				colvarCustomerName.DataType = DbType.String;
				colvarCustomerName.MaxLength = 30;
				colvarCustomerName.AutoIncrement = false;
				colvarCustomerName.IsNullable = false;
				colvarCustomerName.IsPrimaryKey = false;
				colvarCustomerName.IsForeignKey = false;
				colvarCustomerName.IsReadOnly = false;
				colvarCustomerName.DefaultSetting = @"";
				colvarCustomerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCustomerName);
				
				TableSchema.TableColumn colvarCustomerCode = new TableSchema.TableColumn(schema);
				colvarCustomerCode.ColumnName = "customer_code";
				colvarCustomerCode.DataType = DbType.AnsiString;
				colvarCustomerCode.MaxLength = 20;
				colvarCustomerCode.AutoIncrement = false;
				colvarCustomerCode.IsNullable = false;
				colvarCustomerCode.IsPrimaryKey = false;
				colvarCustomerCode.IsForeignKey = false;
				colvarCustomerCode.IsReadOnly = false;
				colvarCustomerCode.DefaultSetting = @"";
				colvarCustomerCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCustomerCode);
				
				TableSchema.TableColumn colvarSellerRootGuid = new TableSchema.TableColumn(schema);
				colvarSellerRootGuid.ColumnName = "seller_root_guid";
				colvarSellerRootGuid.DataType = DbType.Guid;
				colvarSellerRootGuid.MaxLength = 0;
				colvarSellerRootGuid.AutoIncrement = false;
				colvarSellerRootGuid.IsNullable = false;
				colvarSellerRootGuid.IsPrimaryKey = false;
				colvarSellerRootGuid.IsForeignKey = false;
				colvarSellerRootGuid.IsReadOnly = false;
				colvarSellerRootGuid.DefaultSetting = @"";
				colvarSellerRootGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerRootGuid);
				
				TableSchema.TableColumn colvarCreatedUserId = new TableSchema.TableColumn(schema);
				colvarCreatedUserId.ColumnName = "created_user_id";
				colvarCreatedUserId.DataType = DbType.Int32;
				colvarCreatedUserId.MaxLength = 0;
				colvarCreatedUserId.AutoIncrement = false;
				colvarCreatedUserId.IsNullable = false;
				colvarCreatedUserId.IsPrimaryKey = false;
				colvarCreatedUserId.IsForeignKey = false;
				colvarCreatedUserId.IsReadOnly = false;
				colvarCreatedUserId.DefaultSetting = @"";
				colvarCreatedUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedUserId);
				
				TableSchema.TableColumn colvarCreatedTime = new TableSchema.TableColumn(schema);
				colvarCreatedTime.ColumnName = "created_time";
				colvarCreatedTime.DataType = DbType.DateTime;
				colvarCreatedTime.MaxLength = 0;
				colvarCreatedTime.AutoIncrement = false;
				colvarCreatedTime.IsNullable = false;
				colvarCreatedTime.IsPrimaryKey = false;
				colvarCreatedTime.IsForeignKey = false;
				colvarCreatedTime.IsReadOnly = false;
				
						colvarCreatedTime.DefaultSetting = @"(getdate())";
				colvarCreatedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedTime);
				
				TableSchema.TableColumn colvarIsDisabled = new TableSchema.TableColumn(schema);
				colvarIsDisabled.ColumnName = "is_disabled";
				colvarIsDisabled.DataType = DbType.Boolean;
				colvarIsDisabled.MaxLength = 0;
				colvarIsDisabled.AutoIncrement = false;
				colvarIsDisabled.IsNullable = false;
				colvarIsDisabled.IsPrimaryKey = false;
				colvarIsDisabled.IsForeignKey = false;
				colvarIsDisabled.IsReadOnly = false;
				
						colvarIsDisabled.DefaultSetting = @"((0))";
				colvarIsDisabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDisabled);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("channel_odm_customer",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CustomerName")]
		[Bindable(true)]
		public string CustomerName 
		{
			get { return GetColumnValue<string>(Columns.CustomerName); }
			set { SetColumnValue(Columns.CustomerName, value); }
		}
		  
		[XmlAttribute("CustomerCode")]
		[Bindable(true)]
		public string CustomerCode 
		{
			get { return GetColumnValue<string>(Columns.CustomerCode); }
			set { SetColumnValue(Columns.CustomerCode, value); }
		}
		  
		[XmlAttribute("SellerRootGuid")]
		[Bindable(true)]
		public Guid SellerRootGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerRootGuid); }
			set { SetColumnValue(Columns.SellerRootGuid, value); }
		}
		  
		[XmlAttribute("CreatedUserId")]
		[Bindable(true)]
		public int CreatedUserId 
		{
			get { return GetColumnValue<int>(Columns.CreatedUserId); }
			set { SetColumnValue(Columns.CreatedUserId, value); }
		}
		  
		[XmlAttribute("CreatedTime")]
		[Bindable(true)]
		public DateTime CreatedTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedTime); }
			set { SetColumnValue(Columns.CreatedTime, value); }
		}
		  
		[XmlAttribute("IsDisabled")]
		[Bindable(true)]
		public bool IsDisabled 
		{
			get { return GetColumnValue<bool>(Columns.IsDisabled); }
			set { SetColumnValue(Columns.IsDisabled, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CustomerNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CustomerCodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerRootGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedUserIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDisabledColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CustomerName = @"customer_name";
			 public static string CustomerCode = @"customer_code";
			 public static string SellerRootGuid = @"seller_root_guid";
			 public static string CreatedUserId = @"created_user_id";
			 public static string CreatedTime = @"created_time";
			 public static string IsDisabled = @"is_disabled";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OrderStatusLog class.
	/// </summary>
    [Serializable]
	public partial class OrderStatusLogCollection : RepositoryList<OrderStatusLog, OrderStatusLogCollection>
	{	   
		public OrderStatusLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OrderStatusLogCollection</returns>
		public OrderStatusLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OrderStatusLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the order_status_log table.
	/// </summary>
	[Serializable]
	public partial class OrderStatusLog : RepositoryRecord<OrderStatusLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OrderStatusLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OrderStatusLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order_status_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = true;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				
					colvarOrderGuid.ForeignKeyTableName = "order";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 1073741823;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarCouponIds = new TableSchema.TableColumn(schema);
				colvarCouponIds.ColumnName = "coupon_ids";
				colvarCouponIds.DataType = DbType.AnsiString;
				colvarCouponIds.MaxLength = -1;
				colvarCouponIds.AutoIncrement = false;
				colvarCouponIds.IsNullable = true;
				colvarCouponIds.IsPrimaryKey = false;
				colvarCouponIds.IsForeignKey = false;
				colvarCouponIds.IsReadOnly = false;
				colvarCouponIds.DefaultSetting = @"";
				colvarCouponIds.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponIds);
				
				TableSchema.TableColumn colvarReason = new TableSchema.TableColumn(schema);
				colvarReason.ColumnName = "reason";
				colvarReason.DataType = DbType.String;
				colvarReason.MaxLength = 1073741823;
				colvarReason.AutoIncrement = false;
				colvarReason.IsNullable = true;
				colvarReason.IsPrimaryKey = false;
				colvarReason.IsForeignKey = false;
				colvarReason.IsReadOnly = false;
				colvarReason.DefaultSetting = @"";
				colvarReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReason);
				
				TableSchema.TableColumn colvarOrderReturnId = new TableSchema.TableColumn(schema);
				colvarOrderReturnId.ColumnName = "order_return_id";
				colvarOrderReturnId.DataType = DbType.Int32;
				colvarOrderReturnId.MaxLength = 0;
				colvarOrderReturnId.AutoIncrement = false;
				colvarOrderReturnId.IsNullable = true;
				colvarOrderReturnId.IsPrimaryKey = false;
				colvarOrderReturnId.IsForeignKey = false;
				colvarOrderReturnId.IsReadOnly = false;
				colvarOrderReturnId.DefaultSetting = @"";
				colvarOrderReturnId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderReturnId);
				
				TableSchema.TableColumn colvarVendorProgressStatus = new TableSchema.TableColumn(schema);
				colvarVendorProgressStatus.ColumnName = "vendor_progress_status";
				colvarVendorProgressStatus.DataType = DbType.Int32;
				colvarVendorProgressStatus.MaxLength = 0;
				colvarVendorProgressStatus.AutoIncrement = false;
				colvarVendorProgressStatus.IsNullable = true;
				colvarVendorProgressStatus.IsPrimaryKey = false;
				colvarVendorProgressStatus.IsForeignKey = false;
				colvarVendorProgressStatus.IsReadOnly = false;
				colvarVendorProgressStatus.DefaultSetting = @"";
				colvarVendorProgressStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorProgressStatus);
				
				TableSchema.TableColumn colvarVendorProcessTime = new TableSchema.TableColumn(schema);
				colvarVendorProcessTime.ColumnName = "vendor_process_time";
				colvarVendorProcessTime.DataType = DbType.DateTime;
				colvarVendorProcessTime.MaxLength = 0;
				colvarVendorProcessTime.AutoIncrement = false;
				colvarVendorProcessTime.IsNullable = true;
				colvarVendorProcessTime.IsPrimaryKey = false;
				colvarVendorProcessTime.IsForeignKey = false;
				colvarVendorProcessTime.IsReadOnly = false;
				colvarVendorProcessTime.DefaultSetting = @"";
				colvarVendorProcessTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorProcessTime);
				
				TableSchema.TableColumn colvarVendorMemo = new TableSchema.TableColumn(schema);
				colvarVendorMemo.ColumnName = "vendor_memo";
				colvarVendorMemo.DataType = DbType.String;
				colvarVendorMemo.MaxLength = 500;
				colvarVendorMemo.AutoIncrement = false;
				colvarVendorMemo.IsNullable = true;
				colvarVendorMemo.IsPrimaryKey = false;
				colvarVendorMemo.IsForeignKey = false;
				colvarVendorMemo.IsReadOnly = false;
				colvarVendorMemo.DefaultSetting = @"";
				colvarVendorMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorMemo);
				
				TableSchema.TableColumn colvarOrderShipId = new TableSchema.TableColumn(schema);
				colvarOrderShipId.ColumnName = "order_ship_id";
				colvarOrderShipId.DataType = DbType.Int32;
				colvarOrderShipId.MaxLength = 0;
				colvarOrderShipId.AutoIncrement = false;
				colvarOrderShipId.IsNullable = true;
				colvarOrderShipId.IsPrimaryKey = false;
				colvarOrderShipId.IsForeignKey = false;
				colvarOrderShipId.IsReadOnly = false;
				colvarOrderShipId.DefaultSetting = @"";
				colvarOrderShipId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderShipId);
				
				TableSchema.TableColumn colvarMessageUpdate = new TableSchema.TableColumn(schema);
				colvarMessageUpdate.ColumnName = "message_update";
				colvarMessageUpdate.DataType = DbType.Boolean;
				colvarMessageUpdate.MaxLength = 0;
				colvarMessageUpdate.AutoIncrement = false;
				colvarMessageUpdate.IsNullable = false;
				colvarMessageUpdate.IsPrimaryKey = false;
				colvarMessageUpdate.IsForeignKey = false;
				colvarMessageUpdate.IsReadOnly = false;
				
						colvarMessageUpdate.DefaultSetting = @"((0))";
				colvarMessageUpdate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessageUpdate);
				
				TableSchema.TableColumn colvarReceiverName = new TableSchema.TableColumn(schema);
				colvarReceiverName.ColumnName = "receiver_name";
				colvarReceiverName.DataType = DbType.String;
				colvarReceiverName.MaxLength = 50;
				colvarReceiverName.AutoIncrement = false;
				colvarReceiverName.IsNullable = true;
				colvarReceiverName.IsPrimaryKey = false;
				colvarReceiverName.IsForeignKey = false;
				colvarReceiverName.IsReadOnly = false;
				colvarReceiverName.DefaultSetting = @"";
				colvarReceiverName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverName);
				
				TableSchema.TableColumn colvarReceiverAddress = new TableSchema.TableColumn(schema);
				colvarReceiverAddress.ColumnName = "receiver_address";
				colvarReceiverAddress.DataType = DbType.String;
				colvarReceiverAddress.MaxLength = 200;
				colvarReceiverAddress.AutoIncrement = false;
				colvarReceiverAddress.IsNullable = true;
				colvarReceiverAddress.IsPrimaryKey = false;
				colvarReceiverAddress.IsForeignKey = false;
				colvarReceiverAddress.IsReadOnly = false;
				colvarReceiverAddress.DefaultSetting = @"";
				colvarReceiverAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverAddress);
				
				TableSchema.TableColumn colvarIsReceive = new TableSchema.TableColumn(schema);
				colvarIsReceive.ColumnName = "isReceive";
				colvarIsReceive.DataType = DbType.Boolean;
				colvarIsReceive.MaxLength = 0;
				colvarIsReceive.AutoIncrement = false;
				colvarIsReceive.IsNullable = true;
				colvarIsReceive.IsPrimaryKey = false;
				colvarIsReceive.IsForeignKey = false;
				colvarIsReceive.IsReadOnly = false;
				colvarIsReceive.DefaultSetting = @"";
				colvarIsReceive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReceive);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order_status_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("CouponIds")]
		[Bindable(true)]
		public string CouponIds 
		{
			get { return GetColumnValue<string>(Columns.CouponIds); }
			set { SetColumnValue(Columns.CouponIds, value); }
		}
		  
		[XmlAttribute("Reason")]
		[Bindable(true)]
		public string Reason 
		{
			get { return GetColumnValue<string>(Columns.Reason); }
			set { SetColumnValue(Columns.Reason, value); }
		}
		  
		[XmlAttribute("OrderReturnId")]
		[Bindable(true)]
		public int? OrderReturnId 
		{
			get { return GetColumnValue<int?>(Columns.OrderReturnId); }
			set { SetColumnValue(Columns.OrderReturnId, value); }
		}
		  
		[XmlAttribute("VendorProgressStatus")]
		[Bindable(true)]
		public int? VendorProgressStatus 
		{
			get { return GetColumnValue<int?>(Columns.VendorProgressStatus); }
			set { SetColumnValue(Columns.VendorProgressStatus, value); }
		}
		  
		[XmlAttribute("VendorProcessTime")]
		[Bindable(true)]
		public DateTime? VendorProcessTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.VendorProcessTime); }
			set { SetColumnValue(Columns.VendorProcessTime, value); }
		}
		  
		[XmlAttribute("VendorMemo")]
		[Bindable(true)]
		public string VendorMemo 
		{
			get { return GetColumnValue<string>(Columns.VendorMemo); }
			set { SetColumnValue(Columns.VendorMemo, value); }
		}
		  
		[XmlAttribute("OrderShipId")]
		[Bindable(true)]
		public int? OrderShipId 
		{
			get { return GetColumnValue<int?>(Columns.OrderShipId); }
			set { SetColumnValue(Columns.OrderShipId, value); }
		}
		  
		[XmlAttribute("MessageUpdate")]
		[Bindable(true)]
		public bool MessageUpdate 
		{
			get { return GetColumnValue<bool>(Columns.MessageUpdate); }
			set { SetColumnValue(Columns.MessageUpdate, value); }
		}
		  
		[XmlAttribute("ReceiverName")]
		[Bindable(true)]
		public string ReceiverName 
		{
			get { return GetColumnValue<string>(Columns.ReceiverName); }
			set { SetColumnValue(Columns.ReceiverName, value); }
		}
		  
		[XmlAttribute("ReceiverAddress")]
		[Bindable(true)]
		public string ReceiverAddress 
		{
			get { return GetColumnValue<string>(Columns.ReceiverAddress); }
			set { SetColumnValue(Columns.ReceiverAddress, value); }
		}
		  
		[XmlAttribute("IsReceive")]
		[Bindable(true)]
		public bool? IsReceive 
		{
			get { return GetColumnValue<bool?>(Columns.IsReceive); }
			set { SetColumnValue(Columns.IsReceive, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponIdsColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ReasonColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderReturnIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorProgressStatusColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorProcessTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorMemoColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderShipIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageUpdateColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverNameColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverAddressColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReceiveColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderGuid = @"order_guid";
			 public static string Status = @"status";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string Message = @"message";
			 public static string CouponIds = @"coupon_ids";
			 public static string Reason = @"reason";
			 public static string OrderReturnId = @"order_return_id";
			 public static string VendorProgressStatus = @"vendor_progress_status";
			 public static string VendorProcessTime = @"vendor_process_time";
			 public static string VendorMemo = @"vendor_memo";
			 public static string OrderShipId = @"order_ship_id";
			 public static string MessageUpdate = @"message_update";
			 public static string ReceiverName = @"receiver_name";
			 public static string ReceiverAddress = @"receiver_address";
			 public static string IsReceive = @"isReceive";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ShoppingCartGroupD class.
    /// </summary>
    [Serializable]
    public partial class ShoppingCartGroupDCollection : RepositoryList<ShoppingCartGroupD, ShoppingCartGroupDCollection>
    {
        public ShoppingCartGroupDCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ShoppingCartGroupDCollection</returns>
        public ShoppingCartGroupDCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ShoppingCartGroupD o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the shopping_cart_group_d table.
    /// </summary>
    [Serializable]
    public partial class ShoppingCartGroupD : RepositoryRecord<ShoppingCartGroupD>, IRecordBase
    {
        #region .ctors and Default Settings

        public ShoppingCartGroupD()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ShoppingCartGroupD(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("shopping_cart_group_d", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarCartGuid = new TableSchema.TableColumn(schema);
                colvarCartGuid.ColumnName = "cart_guid";
                colvarCartGuid.DataType = DbType.Guid;
                colvarCartGuid.MaxLength = 0;
                colvarCartGuid.AutoIncrement = false;
                colvarCartGuid.IsNullable = false;
                colvarCartGuid.IsPrimaryKey = false;
                colvarCartGuid.IsForeignKey = false;
                colvarCartGuid.IsReadOnly = false;
                colvarCartGuid.DefaultSetting = @"";
                colvarCartGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCartGuid);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarSubCartId = new TableSchema.TableColumn(schema);
                colvarSubCartId.ColumnName = "sub_cart_id";
                colvarSubCartId.DataType = DbType.AnsiString;
                colvarSubCartId.MaxLength = 50;
                colvarSubCartId.AutoIncrement = false;
                colvarSubCartId.IsNullable = false;
                colvarSubCartId.IsPrimaryKey = false;
                colvarSubCartId.IsForeignKey = false;
                colvarSubCartId.IsReadOnly = false;

                colvarSubCartId.DefaultSetting = @"('')";
                colvarSubCartId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSubCartId);

                TableSchema.TableColumn colvarFreightsId = new TableSchema.TableColumn(schema);
                colvarFreightsId.ColumnName = "freights_id";
                colvarFreightsId.DataType = DbType.Int32;
                colvarFreightsId.MaxLength = 0;
                colvarFreightsId.AutoIncrement = false;
                colvarFreightsId.IsNullable = false;
                colvarFreightsId.IsPrimaryKey = false;
                colvarFreightsId.IsForeignKey = false;
                colvarFreightsId.IsReadOnly = false;

                colvarFreightsId.DefaultSetting = @"((0))";
                colvarFreightsId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFreightsId);

                TableSchema.TableColumn colvarFreights = new TableSchema.TableColumn(schema);
                colvarFreights.ColumnName = "freights";
                colvarFreights.DataType = DbType.Int32;
                colvarFreights.MaxLength = 0;
                colvarFreights.AutoIncrement = false;
                colvarFreights.IsNullable = true;
                colvarFreights.IsPrimaryKey = false;
                colvarFreights.IsForeignKey = false;
                colvarFreights.IsReadOnly = false;
                colvarFreights.DefaultSetting = @"";
                colvarFreights.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFreights);

                TableSchema.TableColumn colvarNonFreightsLimit = new TableSchema.TableColumn(schema);
                colvarNonFreightsLimit.ColumnName = "non_freights_limit";
                colvarNonFreightsLimit.DataType = DbType.Int32;
                colvarNonFreightsLimit.MaxLength = 0;
                colvarNonFreightsLimit.AutoIncrement = false;
                colvarNonFreightsLimit.IsNullable = true;
                colvarNonFreightsLimit.IsPrimaryKey = false;
                colvarNonFreightsLimit.IsForeignKey = false;
                colvarNonFreightsLimit.IsReadOnly = false;
                colvarNonFreightsLimit.DefaultSetting = @"";
                colvarNonFreightsLimit.ForeignKeyTableName = "";
                schema.Columns.Add(colvarNonFreightsLimit);

                TableSchema.TableColumn colvarRealFreights = new TableSchema.TableColumn(schema);
                colvarRealFreights.ColumnName = "real_freights";
                colvarRealFreights.DataType = DbType.Int32;
                colvarRealFreights.MaxLength = 0;
                colvarRealFreights.AutoIncrement = false;
                colvarRealFreights.IsNullable = true;
                colvarRealFreights.IsPrimaryKey = false;
                colvarRealFreights.IsForeignKey = false;
                colvarRealFreights.IsReadOnly = false;
                colvarRealFreights.DefaultSetting = @"";
                colvarRealFreights.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRealFreights);

                TableSchema.TableColumn colvarNonFreightsReason = new TableSchema.TableColumn(schema);
                colvarNonFreightsReason.ColumnName = "non_freights_reason";
                colvarNonFreightsReason.DataType = DbType.String;
                colvarNonFreightsReason.MaxLength = 100;
                colvarNonFreightsReason.AutoIncrement = false;
                colvarNonFreightsReason.IsNullable = true;
                colvarNonFreightsReason.IsPrimaryKey = false;
                colvarNonFreightsReason.IsForeignKey = false;
                colvarNonFreightsReason.IsReadOnly = false;
                colvarNonFreightsReason.DefaultSetting = @"";
                colvarNonFreightsReason.ForeignKeyTableName = "";
                schema.Columns.Add(colvarNonFreightsReason);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("shopping_cart_group_d", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("CartGuid")]
        [Bindable(true)]
        public Guid CartGuid
        {
            get { return GetColumnValue<Guid>(Columns.CartGuid); }
            set { SetColumnValue(Columns.CartGuid, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId
        {
            get { return GetColumnValue<int>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("SubCartId")]
        [Bindable(true)]
        public string SubCartId
        {
            get { return GetColumnValue<string>(Columns.SubCartId); }
            set { SetColumnValue(Columns.SubCartId, value); }
        }

        [XmlAttribute("FreightsId")]
        [Bindable(true)]
        public int FreightsId
        {
            get { return GetColumnValue<int>(Columns.FreightsId); }
            set { SetColumnValue(Columns.FreightsId, value); }
        }

        [XmlAttribute("Freights")]
        [Bindable(true)]
        public int? Freights
        {
            get { return GetColumnValue<int?>(Columns.Freights); }
            set { SetColumnValue(Columns.Freights, value); }
        }

        [XmlAttribute("NonFreightsLimit")]
        [Bindable(true)]
        public int? NonFreightsLimit
        {
            get { return GetColumnValue<int?>(Columns.NonFreightsLimit); }
            set { SetColumnValue(Columns.NonFreightsLimit, value); }
        }

        [XmlAttribute("RealFreights")]
        [Bindable(true)]
        public int? RealFreights
        {
            get { return GetColumnValue<int?>(Columns.RealFreights); }
            set { SetColumnValue(Columns.RealFreights, value); }
        }

        [XmlAttribute("NonFreightsReason")]
        [Bindable(true)]
        public string NonFreightsReason
        {
            get { return GetColumnValue<string>(Columns.NonFreightsReason); }
            set { SetColumnValue(Columns.NonFreightsReason, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn CartGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn SubCartIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn FreightsIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn FreightsColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn NonFreightsLimitColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn RealFreightsColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn NonFreightsReasonColumn
        {
            get { return Schema.Columns[9]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"guid";
            public static string CartGuid = @"cart_guid";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string SubCartId = @"sub_cart_id";
            public static string FreightsId = @"freights_id";
            public static string Freights = @"freights";
            public static string NonFreightsLimit = @"non_freights_limit";
            public static string RealFreights = @"real_freights";
            public static string NonFreightsReason = @"non_freights_reason";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

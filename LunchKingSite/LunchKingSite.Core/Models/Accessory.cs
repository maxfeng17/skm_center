using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Accessory class.
	/// </summary>
    [Serializable]
	public partial class AccessoryCollection : RepositoryList<Accessory, AccessoryCollection>
	{	   
		public AccessoryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>AccessoryCollection</returns>
		public AccessoryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Accessory o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the accessory table.
	/// </summary>
	[Serializable]
	public partial class Accessory : RepositoryRecord<Accessory>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Accessory()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Accessory(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("accessory", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarAccessoryCategoryGuid = new TableSchema.TableColumn(schema);
				colvarAccessoryCategoryGuid.ColumnName = "accessory_category_GUID";
				colvarAccessoryCategoryGuid.DataType = DbType.Guid;
				colvarAccessoryCategoryGuid.MaxLength = 0;
				colvarAccessoryCategoryGuid.AutoIncrement = false;
				colvarAccessoryCategoryGuid.IsNullable = false;
				colvarAccessoryCategoryGuid.IsPrimaryKey = false;
				colvarAccessoryCategoryGuid.IsForeignKey = true;
				colvarAccessoryCategoryGuid.IsReadOnly = false;
				colvarAccessoryCategoryGuid.DefaultSetting = @"";
				
					colvarAccessoryCategoryGuid.ForeignKeyTableName = "accessory_category";
				schema.Columns.Add(colvarAccessoryCategoryGuid);
				
				TableSchema.TableColumn colvarAccessoryId = new TableSchema.TableColumn(schema);
				colvarAccessoryId.ColumnName = "accessory_id";
				colvarAccessoryId.DataType = DbType.AnsiString;
				colvarAccessoryId.MaxLength = 20;
				colvarAccessoryId.AutoIncrement = false;
				colvarAccessoryId.IsNullable = false;
				colvarAccessoryId.IsPrimaryKey = false;
				colvarAccessoryId.IsForeignKey = false;
				colvarAccessoryId.IsReadOnly = false;
				colvarAccessoryId.DefaultSetting = @"";
				colvarAccessoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessoryId);
				
				TableSchema.TableColumn colvarAccessoryName = new TableSchema.TableColumn(schema);
				colvarAccessoryName.ColumnName = "accessory_name";
				colvarAccessoryName.DataType = DbType.String;
				colvarAccessoryName.MaxLength = 50;
				colvarAccessoryName.AutoIncrement = false;
				colvarAccessoryName.IsNullable = false;
				colvarAccessoryName.IsPrimaryKey = false;
				colvarAccessoryName.IsForeignKey = false;
				colvarAccessoryName.IsReadOnly = false;
				colvarAccessoryName.DefaultSetting = @"";
				colvarAccessoryName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessoryName);
				
				TableSchema.TableColumn colvarRank = new TableSchema.TableColumn(schema);
				colvarRank.ColumnName = "rank";
				colvarRank.DataType = DbType.Int32;
				colvarRank.MaxLength = 0;
				colvarRank.AutoIncrement = false;
				colvarRank.IsNullable = true;
				colvarRank.IsPrimaryKey = false;
				colvarRank.IsForeignKey = false;
				colvarRank.IsReadOnly = false;
				colvarRank.DefaultSetting = @"";
				colvarRank.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRank);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 30;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("accessory",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("AccessoryCategoryGuid")]
		[Bindable(true)]
		public Guid AccessoryCategoryGuid 
		{
			get { return GetColumnValue<Guid>(Columns.AccessoryCategoryGuid); }
			set { SetColumnValue(Columns.AccessoryCategoryGuid, value); }
		}
		  
		[XmlAttribute("AccessoryId")]
		[Bindable(true)]
		public string AccessoryId 
		{
			get { return GetColumnValue<string>(Columns.AccessoryId); }
			set { SetColumnValue(Columns.AccessoryId, value); }
		}
		  
		[XmlAttribute("AccessoryName")]
		[Bindable(true)]
		public string AccessoryName 
		{
			get { return GetColumnValue<string>(Columns.AccessoryName); }
			set { SetColumnValue(Columns.AccessoryName, value); }
		}
		  
		[XmlAttribute("Rank")]
		[Bindable(true)]
		public int? Rank 
		{
			get { return GetColumnValue<int?>(Columns.Rank); }
			set { SetColumnValue(Columns.Rank, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessoryCategoryGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessoryIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessoryNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn RankColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string AccessoryCategoryGuid = @"accessory_category_GUID";
			 public static string AccessoryId = @"accessory_id";
			 public static string AccessoryName = @"accessory_name";
			 public static string Rank = @"rank";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SortDealTimeSlot class.
	/// </summary>
    [Serializable]
	public partial class SortDealTimeSlotCollection : RepositoryList<SortDealTimeSlot, SortDealTimeSlotCollection>
	{	   
		public SortDealTimeSlotCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SortDealTimeSlotCollection</returns>
		public SortDealTimeSlotCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SortDealTimeSlot o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the sort_deal_time_slot table.
	/// </summary>
	[Serializable]
	public partial class SortDealTimeSlot : RepositoryRecord<SortDealTimeSlot>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SortDealTimeSlot()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SortDealTimeSlot(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("sort_deal_time_slot", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = true;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarCityid = new TableSchema.TableColumn(schema);
				colvarCityid.ColumnName = "cityid";
				colvarCityid.DataType = DbType.Int32;
				colvarCityid.MaxLength = 0;
				colvarCityid.AutoIncrement = false;
				colvarCityid.IsNullable = false;
				colvarCityid.IsPrimaryKey = true;
				colvarCityid.IsForeignKey = false;
				colvarCityid.IsReadOnly = false;
				colvarCityid.DefaultSetting = @"";
				colvarCityid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityid);
				
				TableSchema.TableColumn colvarEffstart = new TableSchema.TableColumn(schema);
				colvarEffstart.ColumnName = "effstart";
				colvarEffstart.DataType = DbType.DateTime;
				colvarEffstart.MaxLength = 0;
				colvarEffstart.AutoIncrement = false;
				colvarEffstart.IsNullable = false;
				colvarEffstart.IsPrimaryKey = true;
				colvarEffstart.IsForeignKey = false;
				colvarEffstart.IsReadOnly = false;
				colvarEffstart.DefaultSetting = @"";
				colvarEffstart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEffstart);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = false;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				colvarSeq.DefaultSetting = @"";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("sort_deal_time_slot",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("Cityid")]
		[Bindable(true)]
		public int Cityid 
		{
			get { return GetColumnValue<int>(Columns.Cityid); }
			set { SetColumnValue(Columns.Cityid, value); }
		}
		  
		[XmlAttribute("Effstart")]
		[Bindable(true)]
		public DateTime Effstart 
		{
			get { return GetColumnValue<DateTime>(Columns.Effstart); }
			set { SetColumnValue(Columns.Effstart, value); }
		}
		  
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int Seq 
		{
			get { return GetColumnValue<int>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CityidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn EffstartColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Bid = @"bid";
			 public static string Cityid = @"cityid";
			 public static string Effstart = @"effstart";
			 public static string Seq = @"seq";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMergeOldNewServiceMessage class.
    /// </summary>
    [Serializable]
    public partial class ViewMergeOldNewServiceMessageCollection : ReadOnlyList<ViewMergeOldNewServiceMessage, ViewMergeOldNewServiceMessageCollection>
    {        
        public ViewMergeOldNewServiceMessageCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_merge_old_new_service_message view.
    /// </summary>
    [Serializable]
    public partial class ViewMergeOldNewServiceMessage : ReadOnlyRecord<ViewMergeOldNewServiceMessage>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_merge_old_new_service_message", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCsVersion = new TableSchema.TableColumn(schema);
                colvarCsVersion.ColumnName = "cs_version";
                colvarCsVersion.DataType = DbType.Int32;
                colvarCsVersion.MaxLength = 0;
                colvarCsVersion.AutoIncrement = false;
                colvarCsVersion.IsNullable = false;
                colvarCsVersion.IsPrimaryKey = false;
                colvarCsVersion.IsForeignKey = false;
                colvarCsVersion.IsReadOnly = false;
                
                schema.Columns.Add(colvarCsVersion);
                
                TableSchema.TableColumn colvarSmId = new TableSchema.TableColumn(schema);
                colvarSmId.ColumnName = "sm_id";
                colvarSmId.DataType = DbType.Int32;
                colvarSmId.MaxLength = 0;
                colvarSmId.AutoIncrement = false;
                colvarSmId.IsNullable = true;
                colvarSmId.IsPrimaryKey = false;
                colvarSmId.IsForeignKey = false;
                colvarSmId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSmId);
                
                TableSchema.TableColumn colvarServiceNo = new TableSchema.TableColumn(schema);
                colvarServiceNo.ColumnName = "service_no";
                colvarServiceNo.DataType = DbType.AnsiString;
                colvarServiceNo.MaxLength = 50;
                colvarServiceNo.AutoIncrement = false;
                colvarServiceNo.IsNullable = false;
                colvarServiceNo.IsPrimaryKey = false;
                colvarServiceNo.IsForeignKey = false;
                colvarServiceNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarServiceNo);
                
                TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
                colvarCategory.ColumnName = "category";
                colvarCategory.DataType = DbType.Int32;
                colvarCategory.MaxLength = 0;
                colvarCategory.AutoIncrement = false;
                colvarCategory.IsNullable = true;
                colvarCategory.IsPrimaryKey = false;
                colvarCategory.IsForeignKey = false;
                colvarCategory.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory);
                
                TableSchema.TableColumn colvarSubCategory = new TableSchema.TableColumn(schema);
                colvarSubCategory.ColumnName = "sub_category";
                colvarSubCategory.DataType = DbType.Int32;
                colvarSubCategory.MaxLength = 0;
                colvarSubCategory.AutoIncrement = false;
                colvarSubCategory.IsNullable = true;
                colvarSubCategory.IsPrimaryKey = false;
                colvarSubCategory.IsForeignKey = false;
                colvarSubCategory.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategory);
                
                TableSchema.TableColumn colvarSmMessageType = new TableSchema.TableColumn(schema);
                colvarSmMessageType.ColumnName = "sm_message_type";
                colvarSmMessageType.DataType = DbType.String;
                colvarSmMessageType.MaxLength = 50;
                colvarSmMessageType.AutoIncrement = false;
                colvarSmMessageType.IsNullable = true;
                colvarSmMessageType.IsPrimaryKey = false;
                colvarSmMessageType.IsForeignKey = false;
                colvarSmMessageType.IsReadOnly = false;
                
                schema.Columns.Add(colvarSmMessageType);
                
                TableSchema.TableColumn colvarCsmMessageType = new TableSchema.TableColumn(schema);
                colvarCsmMessageType.ColumnName = "csm_message_type";
                colvarCsmMessageType.DataType = DbType.Int32;
                colvarCsmMessageType.MaxLength = 0;
                colvarCsmMessageType.AutoIncrement = false;
                colvarCsmMessageType.IsNullable = false;
                colvarCsmMessageType.IsPrimaryKey = false;
                colvarCsmMessageType.IsForeignKey = false;
                colvarCsmMessageType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCsmMessageType);
                
                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = -1;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage);
                
                TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
                colvarPhone.ColumnName = "phone";
                colvarPhone.DataType = DbType.AnsiString;
                colvarPhone.MaxLength = 50;
                colvarPhone.AutoIncrement = false;
                colvarPhone.IsNullable = true;
                colvarPhone.IsPrimaryKey = false;
                colvarPhone.IsForeignKey = false;
                colvarPhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhone);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 50;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
                colvarEmail.ColumnName = "email";
                colvarEmail.DataType = DbType.String;
                colvarEmail.MaxLength = 100;
                colvarEmail.AutoIncrement = false;
                colvarEmail.IsNullable = true;
                colvarEmail.IsPrimaryKey = false;
                colvarEmail.IsForeignKey = false;
                colvarEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarEmail);
                
                TableSchema.TableColumn colvarSmStatus = new TableSchema.TableColumn(schema);
                colvarSmStatus.ColumnName = "sm_status";
                colvarSmStatus.DataType = DbType.String;
                colvarSmStatus.MaxLength = 50;
                colvarSmStatus.AutoIncrement = false;
                colvarSmStatus.IsNullable = true;
                colvarSmStatus.IsPrimaryKey = false;
                colvarSmStatus.IsForeignKey = false;
                colvarSmStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarSmStatus);
                
                TableSchema.TableColumn colvarCsmStatus = new TableSchema.TableColumn(schema);
                colvarCsmStatus.ColumnName = "csm_status";
                colvarCsmStatus.DataType = DbType.Int32;
                colvarCsmStatus.MaxLength = 0;
                colvarCsmStatus.AutoIncrement = false;
                colvarCsmStatus.IsNullable = true;
                colvarCsmStatus.IsPrimaryKey = false;
                colvarCsmStatus.IsForeignKey = false;
                colvarCsmStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCsmStatus);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 100;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyName = new TableSchema.TableColumn(schema);
                colvarModifyName.ColumnName = "modify_name";
                colvarModifyName.DataType = DbType.String;
                colvarModifyName.MaxLength = 100;
                colvarModifyName.AutoIncrement = false;
                colvarModifyName.IsNullable = true;
                colvarModifyName.IsPrimaryKey = false;
                colvarModifyName.IsForeignKey = false;
                colvarModifyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyName);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarSmType = new TableSchema.TableColumn(schema);
                colvarSmType.ColumnName = "sm_type";
                colvarSmType.DataType = DbType.Int32;
                colvarSmType.MaxLength = 0;
                colvarSmType.AutoIncrement = false;
                colvarSmType.IsNullable = false;
                colvarSmType.IsPrimaryKey = false;
                colvarSmType.IsForeignKey = false;
                colvarSmType.IsReadOnly = false;
                
                schema.Columns.Add(colvarSmType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_merge_old_new_service_message",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMergeOldNewServiceMessage()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMergeOldNewServiceMessage(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMergeOldNewServiceMessage(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMergeOldNewServiceMessage(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CsVersion")]
        [Bindable(true)]
        public int CsVersion 
	    {
		    get
		    {
			    return GetColumnValue<int>("cs_version");
		    }
            set 
		    {
			    SetColumnValue("cs_version", value);
            }
        }
	      
        [XmlAttribute("SmId")]
        [Bindable(true)]
        public int? SmId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("sm_id");
		    }
            set 
		    {
			    SetColumnValue("sm_id", value);
            }
        }
	      
        [XmlAttribute("ServiceNo")]
        [Bindable(true)]
        public string ServiceNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("service_no");
		    }
            set 
		    {
			    SetColumnValue("service_no", value);
            }
        }
	      
        [XmlAttribute("Category")]
        [Bindable(true)]
        public int? Category 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category");
		    }
            set 
		    {
			    SetColumnValue("category", value);
            }
        }
	      
        [XmlAttribute("SubCategory")]
        [Bindable(true)]
        public int? SubCategory 
	    {
		    get
		    {
			    return GetColumnValue<int?>("sub_category");
		    }
            set 
		    {
			    SetColumnValue("sub_category", value);
            }
        }
	      
        [XmlAttribute("SmMessageType")]
        [Bindable(true)]
        public string SmMessageType 
	    {
		    get
		    {
			    return GetColumnValue<string>("sm_message_type");
		    }
            set 
		    {
			    SetColumnValue("sm_message_type", value);
            }
        }
	      
        [XmlAttribute("CsmMessageType")]
        [Bindable(true)]
        public int CsmMessageType 
	    {
		    get
		    {
			    return GetColumnValue<int>("csm_message_type");
		    }
            set 
		    {
			    SetColumnValue("csm_message_type", value);
            }
        }
	      
        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message 
	    {
		    get
		    {
			    return GetColumnValue<string>("message");
		    }
            set 
		    {
			    SetColumnValue("message", value);
            }
        }
	      
        [XmlAttribute("Phone")]
        [Bindable(true)]
        public string Phone 
	    {
		    get
		    {
			    return GetColumnValue<string>("phone");
		    }
            set 
		    {
			    SetColumnValue("phone", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("Email")]
        [Bindable(true)]
        public string Email 
	    {
		    get
		    {
			    return GetColumnValue<string>("email");
		    }
            set 
		    {
			    SetColumnValue("email", value);
            }
        }
	      
        [XmlAttribute("SmStatus")]
        [Bindable(true)]
        public string SmStatus 
	    {
		    get
		    {
			    return GetColumnValue<string>("sm_status");
		    }
            set 
		    {
			    SetColumnValue("sm_status", value);
            }
        }
	      
        [XmlAttribute("CsmStatus")]
        [Bindable(true)]
        public int? CsmStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("csm_status");
		    }
            set 
		    {
			    SetColumnValue("csm_status", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyName")]
        [Bindable(true)]
        public string ModifyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_name");
		    }
            set 
		    {
			    SetColumnValue("modify_name", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("SmType")]
        [Bindable(true)]
        public int SmType 
	    {
		    get
		    {
			    return GetColumnValue<int>("sm_type");
		    }
            set 
		    {
			    SetColumnValue("sm_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CsVersion = @"cs_version";
            
            public static string SmId = @"sm_id";
            
            public static string ServiceNo = @"service_no";
            
            public static string Category = @"category";
            
            public static string SubCategory = @"sub_category";
            
            public static string SmMessageType = @"sm_message_type";
            
            public static string CsmMessageType = @"csm_message_type";
            
            public static string Message = @"message";
            
            public static string Phone = @"phone";
            
            public static string Name = @"name";
            
            public static string Email = @"email";
            
            public static string SmStatus = @"sm_status";
            
            public static string CsmStatus = @"csm_status";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyName = @"modify_name";
            
            public static string ModifyTime = @"modify_time";
            
            public static string OrderId = @"order_id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string SmType = @"sm_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDeal class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealCollection : ReadOnlyList<ViewHiDeal, ViewHiDealCollection>
    {        
        public ViewHiDealCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDeal : ReadOnlyRecord<ViewHiDeal>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
                colvarCategory.ColumnName = "category";
                colvarCategory.DataType = DbType.String;
                colvarCategory.MaxLength = 50;
                colvarCategory.AutoIncrement = false;
                colvarCategory.IsNullable = false;
                colvarCategory.IsPrimaryKey = false;
                colvarCategory.IsForeignKey = false;
                colvarCategory.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory);
                
                TableSchema.TableColumn colvarCategoryCodeId = new TableSchema.TableColumn(schema);
                colvarCategoryCodeId.ColumnName = "category_code_id";
                colvarCategoryCodeId.DataType = DbType.Int32;
                colvarCategoryCodeId.MaxLength = 0;
                colvarCategoryCodeId.AutoIncrement = false;
                colvarCategoryCodeId.IsNullable = false;
                colvarCategoryCodeId.IsPrimaryKey = false;
                colvarCategoryCodeId.IsForeignKey = false;
                colvarCategoryCodeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryCodeId);
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 50;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = false;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                TableSchema.TableColumn colvarSpecialCodeId = new TableSchema.TableColumn(schema);
                colvarSpecialCodeId.ColumnName = "special_code_id";
                colvarSpecialCodeId.DataType = DbType.Int32;
                colvarSpecialCodeId.MaxLength = 0;
                colvarSpecialCodeId.AutoIncrement = false;
                colvarSpecialCodeId.IsNullable = true;
                colvarSpecialCodeId.IsPrimaryKey = false;
                colvarSpecialCodeId.IsForeignKey = false;
                colvarSpecialCodeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSpecialCodeId);
                
                TableSchema.TableColumn colvarSpecialName = new TableSchema.TableColumn(schema);
                colvarSpecialName.ColumnName = "special_name";
                colvarSpecialName.DataType = DbType.String;
                colvarSpecialName.MaxLength = 50;
                colvarSpecialName.AutoIncrement = false;
                colvarSpecialName.IsNullable = true;
                colvarSpecialName.IsPrimaryKey = false;
                colvarSpecialName.IsForeignKey = false;
                colvarSpecialName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSpecialName);
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarDealGuid = new TableSchema.TableColumn(schema);
                colvarDealGuid.ColumnName = "deal_guid";
                colvarDealGuid.DataType = DbType.Guid;
                colvarDealGuid.MaxLength = 0;
                colvarDealGuid.AutoIncrement = false;
                colvarDealGuid.IsNullable = false;
                colvarDealGuid.IsPrimaryKey = false;
                colvarDealGuid.IsForeignKey = false;
                colvarDealGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealGuid);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = true;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 50;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarDealPromoLongDesc = new TableSchema.TableColumn(schema);
                colvarDealPromoLongDesc.ColumnName = "deal_promo_long_desc";
                colvarDealPromoLongDesc.DataType = DbType.String;
                colvarDealPromoLongDesc.MaxLength = 255;
                colvarDealPromoLongDesc.AutoIncrement = false;
                colvarDealPromoLongDesc.IsNullable = true;
                colvarDealPromoLongDesc.IsPrimaryKey = false;
                colvarDealPromoLongDesc.IsForeignKey = false;
                colvarDealPromoLongDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealPromoLongDesc);
                
                TableSchema.TableColumn colvarDealPromoShortDesc = new TableSchema.TableColumn(schema);
                colvarDealPromoShortDesc.ColumnName = "deal_promo_short_desc";
                colvarDealPromoShortDesc.DataType = DbType.String;
                colvarDealPromoShortDesc.MaxLength = 100;
                colvarDealPromoShortDesc.AutoIncrement = false;
                colvarDealPromoShortDesc.IsNullable = true;
                colvarDealPromoShortDesc.IsPrimaryKey = false;
                colvarDealPromoShortDesc.IsForeignKey = false;
                colvarDealPromoShortDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealPromoShortDesc);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarDealIsOpen = new TableSchema.TableColumn(schema);
                colvarDealIsOpen.ColumnName = "deal_IsOpen";
                colvarDealIsOpen.DataType = DbType.Boolean;
                colvarDealIsOpen.MaxLength = 0;
                colvarDealIsOpen.AutoIncrement = false;
                colvarDealIsOpen.IsNullable = false;
                colvarDealIsOpen.IsPrimaryKey = false;
                colvarDealIsOpen.IsForeignKey = false;
                colvarDealIsOpen.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealIsOpen);
                
                TableSchema.TableColumn colvarDealIsShow = new TableSchema.TableColumn(schema);
                colvarDealIsShow.ColumnName = "deal_IsShow";
                colvarDealIsShow.DataType = DbType.Boolean;
                colvarDealIsShow.MaxLength = 0;
                colvarDealIsShow.AutoIncrement = false;
                colvarDealIsShow.IsNullable = false;
                colvarDealIsShow.IsPrimaryKey = false;
                colvarDealIsShow.IsForeignKey = false;
                colvarDealIsShow.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealIsShow);
                
                TableSchema.TableColumn colvarDealIsSoldout = new TableSchema.TableColumn(schema);
                colvarDealIsSoldout.ColumnName = "deal_is_soldout";
                colvarDealIsSoldout.DataType = DbType.Boolean;
                colvarDealIsSoldout.MaxLength = 0;
                colvarDealIsSoldout.AutoIncrement = false;
                colvarDealIsSoldout.IsNullable = true;
                colvarDealIsSoldout.IsPrimaryKey = false;
                colvarDealIsSoldout.IsForeignKey = false;
                colvarDealIsSoldout.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealIsSoldout);
                
                TableSchema.TableColumn colvarDealIsAlwaysMain = new TableSchema.TableColumn(schema);
                colvarDealIsAlwaysMain.ColumnName = "deal_is_always_main";
                colvarDealIsAlwaysMain.DataType = DbType.Boolean;
                colvarDealIsAlwaysMain.MaxLength = 0;
                colvarDealIsAlwaysMain.AutoIncrement = false;
                colvarDealIsAlwaysMain.IsNullable = false;
                colvarDealIsAlwaysMain.IsPrimaryKey = false;
                colvarDealIsAlwaysMain.IsForeignKey = false;
                colvarDealIsAlwaysMain.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealIsAlwaysMain);
                
                TableSchema.TableColumn colvarDealIsVerifyByList = new TableSchema.TableColumn(schema);
                colvarDealIsVerifyByList.ColumnName = "deal_is_verify_by_list";
                colvarDealIsVerifyByList.DataType = DbType.Boolean;
                colvarDealIsVerifyByList.MaxLength = 0;
                colvarDealIsVerifyByList.AutoIncrement = false;
                colvarDealIsVerifyByList.IsNullable = false;
                colvarDealIsVerifyByList.IsPrimaryKey = false;
                colvarDealIsVerifyByList.IsForeignKey = false;
                colvarDealIsVerifyByList.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealIsVerifyByList);
                
                TableSchema.TableColumn colvarDealIsVerifyByPad = new TableSchema.TableColumn(schema);
                colvarDealIsVerifyByPad.ColumnName = "deal_is_verify_by_pad";
                colvarDealIsVerifyByPad.DataType = DbType.Boolean;
                colvarDealIsVerifyByPad.MaxLength = 0;
                colvarDealIsVerifyByPad.AutoIncrement = false;
                colvarDealIsVerifyByPad.IsNullable = false;
                colvarDealIsVerifyByPad.IsPrimaryKey = false;
                colvarDealIsVerifyByPad.IsForeignKey = false;
                colvarDealIsVerifyByPad.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealIsVerifyByPad);
                
                TableSchema.TableColumn colvarPicture = new TableSchema.TableColumn(schema);
                colvarPicture.ColumnName = "picture";
                colvarPicture.DataType = DbType.String;
                colvarPicture.MaxLength = -1;
                colvarPicture.AutoIncrement = false;
                colvarPicture.IsNullable = true;
                colvarPicture.IsPrimaryKey = false;
                colvarPicture.IsForeignKey = false;
                colvarPicture.IsReadOnly = false;
                
                schema.Columns.Add(colvarPicture);
                
                TableSchema.TableColumn colvarDealCreateId = new TableSchema.TableColumn(schema);
                colvarDealCreateId.ColumnName = "deal_create_id";
                colvarDealCreateId.DataType = DbType.String;
                colvarDealCreateId.MaxLength = 100;
                colvarDealCreateId.AutoIncrement = false;
                colvarDealCreateId.IsNullable = true;
                colvarDealCreateId.IsPrimaryKey = false;
                colvarDealCreateId.IsForeignKey = false;
                colvarDealCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealCreateId);
                
                TableSchema.TableColumn colvarDealCreateTime = new TableSchema.TableColumn(schema);
                colvarDealCreateTime.ColumnName = "deal_create_time";
                colvarDealCreateTime.DataType = DbType.DateTime;
                colvarDealCreateTime.MaxLength = 0;
                colvarDealCreateTime.AutoIncrement = false;
                colvarDealCreateTime.IsNullable = true;
                colvarDealCreateTime.IsPrimaryKey = false;
                colvarDealCreateTime.IsForeignKey = false;
                colvarDealCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealCreateTime);
                
                TableSchema.TableColumn colvarDealModifyId = new TableSchema.TableColumn(schema);
                colvarDealModifyId.ColumnName = "deal_modify_id";
                colvarDealModifyId.DataType = DbType.String;
                colvarDealModifyId.MaxLength = 100;
                colvarDealModifyId.AutoIncrement = false;
                colvarDealModifyId.IsNullable = true;
                colvarDealModifyId.IsPrimaryKey = false;
                colvarDealModifyId.IsForeignKey = false;
                colvarDealModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealModifyId);
                
                TableSchema.TableColumn colvarDealModifyTime = new TableSchema.TableColumn(schema);
                colvarDealModifyTime.ColumnName = "deal_modify_time";
                colvarDealModifyTime.DataType = DbType.DateTime;
                colvarDealModifyTime.MaxLength = 0;
                colvarDealModifyTime.AutoIncrement = false;
                colvarDealModifyTime.IsNullable = true;
                colvarDealModifyTime.IsPrimaryKey = false;
                colvarDealModifyTime.IsForeignKey = false;
                colvarDealModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealModifyTime);
                
                TableSchema.TableColumn colvarDealCities = new TableSchema.TableColumn(schema);
                colvarDealCities.ColumnName = "deal_cities";
                colvarDealCities.DataType = DbType.String;
                colvarDealCities.MaxLength = 100;
                colvarDealCities.AutoIncrement = false;
                colvarDealCities.IsNullable = true;
                colvarDealCities.IsPrimaryKey = false;
                colvarDealCities.IsForeignKey = false;
                colvarDealCities.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealCities);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarCitySeq = new TableSchema.TableColumn(schema);
                colvarCitySeq.ColumnName = "city_seq";
                colvarCitySeq.DataType = DbType.Int32;
                colvarCitySeq.MaxLength = 0;
                colvarCitySeq.AutoIncrement = false;
                colvarCitySeq.IsNullable = true;
                colvarCitySeq.IsPrimaryKey = false;
                colvarCitySeq.IsForeignKey = false;
                colvarCitySeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarCitySeq);
                
                TableSchema.TableColumn colvarCityCodeId = new TableSchema.TableColumn(schema);
                colvarCityCodeId.ColumnName = "city_code_id";
                colvarCityCodeId.DataType = DbType.Int32;
                colvarCityCodeId.MaxLength = 0;
                colvarCityCodeId.AutoIncrement = false;
                colvarCityCodeId.IsNullable = false;
                colvarCityCodeId.IsPrimaryKey = false;
                colvarCityCodeId.IsForeignKey = false;
                colvarCityCodeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityCodeId);
                
                TableSchema.TableColumn colvarCityCodeGroup = new TableSchema.TableColumn(schema);
                colvarCityCodeGroup.ColumnName = "city_code_group";
                colvarCityCodeGroup.DataType = DbType.AnsiString;
                colvarCityCodeGroup.MaxLength = 50;
                colvarCityCodeGroup.AutoIncrement = false;
                colvarCityCodeGroup.IsNullable = false;
                colvarCityCodeGroup.IsPrimaryKey = false;
                colvarCityCodeGroup.IsForeignKey = false;
                colvarCityCodeGroup.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityCodeGroup);
                
                TableSchema.TableColumn colvarCityCity = new TableSchema.TableColumn(schema);
                colvarCityCity.ColumnName = "city_city";
                colvarCityCity.DataType = DbType.String;
                colvarCityCity.MaxLength = 50;
                colvarCityCity.AutoIncrement = false;
                colvarCityCity.IsNullable = false;
                colvarCityCity.IsPrimaryKey = false;
                colvarCityCity.IsForeignKey = false;
                colvarCityCity.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityCity);
                
                TableSchema.TableColumn colvarCityStartTime = new TableSchema.TableColumn(schema);
                colvarCityStartTime.ColumnName = "city_start_time";
                colvarCityStartTime.DataType = DbType.DateTime;
                colvarCityStartTime.MaxLength = 0;
                colvarCityStartTime.AutoIncrement = false;
                colvarCityStartTime.IsNullable = false;
                colvarCityStartTime.IsPrimaryKey = false;
                colvarCityStartTime.IsForeignKey = false;
                colvarCityStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityStartTime);
                
                TableSchema.TableColumn colvarCityEndTime = new TableSchema.TableColumn(schema);
                colvarCityEndTime.ColumnName = "city_end_time";
                colvarCityEndTime.DataType = DbType.DateTime;
                colvarCityEndTime.MaxLength = 0;
                colvarCityEndTime.AutoIncrement = false;
                colvarCityEndTime.IsNullable = true;
                colvarCityEndTime.IsPrimaryKey = false;
                colvarCityEndTime.IsForeignKey = false;
                colvarCityEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityEndTime);
                
                TableSchema.TableColumn colvarCategoryCodeName = new TableSchema.TableColumn(schema);
                colvarCategoryCodeName.ColumnName = "category_code_name";
                colvarCategoryCodeName.DataType = DbType.String;
                colvarCategoryCodeName.MaxLength = 50;
                colvarCategoryCodeName.AutoIncrement = false;
                colvarCategoryCodeName.IsNullable = false;
                colvarCategoryCodeName.IsPrimaryKey = false;
                colvarCategoryCodeName.IsForeignKey = false;
                colvarCategoryCodeName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryCodeName);
                
                TableSchema.TableColumn colvarIsMain = new TableSchema.TableColumn(schema);
                colvarIsMain.ColumnName = "is_main";
                colvarIsMain.DataType = DbType.Boolean;
                colvarIsMain.MaxLength = 0;
                colvarIsMain.AutoIncrement = false;
                colvarIsMain.IsNullable = false;
                colvarIsMain.IsPrimaryKey = false;
                colvarIsMain.IsForeignKey = false;
                colvarIsMain.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsMain);
                
                TableSchema.TableColumn colvarIsVisa = new TableSchema.TableColumn(schema);
                colvarIsVisa.ColumnName = "is_visa";
                colvarIsVisa.DataType = DbType.Boolean;
                colvarIsVisa.MaxLength = 0;
                colvarIsVisa.AutoIncrement = false;
                colvarIsVisa.IsNullable = true;
                colvarIsVisa.IsPrimaryKey = false;
                colvarIsVisa.IsForeignKey = false;
                colvarIsVisa.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsVisa);
                
                TableSchema.TableColumn colvarPrimaryBigPicture = new TableSchema.TableColumn(schema);
                colvarPrimaryBigPicture.ColumnName = "primary_big_picture";
                colvarPrimaryBigPicture.DataType = DbType.String;
                colvarPrimaryBigPicture.MaxLength = 200;
                colvarPrimaryBigPicture.AutoIncrement = false;
                colvarPrimaryBigPicture.IsNullable = true;
                colvarPrimaryBigPicture.IsPrimaryKey = false;
                colvarPrimaryBigPicture.IsForeignKey = false;
                colvarPrimaryBigPicture.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrimaryBigPicture);
                
                TableSchema.TableColumn colvarPrimarySmallPicture = new TableSchema.TableColumn(schema);
                colvarPrimarySmallPicture.ColumnName = "primary_small_picture";
                colvarPrimarySmallPicture.DataType = DbType.String;
                colvarPrimarySmallPicture.MaxLength = 200;
                colvarPrimarySmallPicture.AutoIncrement = false;
                colvarPrimarySmallPicture.IsNullable = true;
                colvarPrimarySmallPicture.IsPrimaryKey = false;
                colvarPrimarySmallPicture.IsForeignKey = false;
                colvarPrimarySmallPicture.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrimarySmallPicture);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDeal()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDeal(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDeal(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDeal(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Category")]
        [Bindable(true)]
        public string Category 
	    {
		    get
		    {
			    return GetColumnValue<string>("category");
		    }
            set 
		    {
			    SetColumnValue("category", value);
            }
        }
	      
        [XmlAttribute("CategoryCodeId")]
        [Bindable(true)]
        public int CategoryCodeId 
	    {
		    get
		    {
			    return GetColumnValue<int>("category_code_id");
		    }
            set 
		    {
			    SetColumnValue("category_code_id", value);
            }
        }
	      
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	      
        [XmlAttribute("SpecialCodeId")]
        [Bindable(true)]
        public int? SpecialCodeId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("special_code_id");
		    }
            set 
		    {
			    SetColumnValue("special_code_id", value);
            }
        }
	      
        [XmlAttribute("SpecialName")]
        [Bindable(true)]
        public string SpecialName 
	    {
		    get
		    {
			    return GetColumnValue<string>("special_name");
		    }
            set 
		    {
			    SetColumnValue("special_name", value);
            }
        }
	      
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("DealGuid")]
        [Bindable(true)]
        public Guid DealGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("deal_guid");
		    }
            set 
		    {
			    SetColumnValue("deal_guid", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int? DealType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_type");
		    }
            set 
		    {
			    SetColumnValue("deal_type", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_name");
		    }
            set 
		    {
			    SetColumnValue("deal_name", value);
            }
        }
	      
        [XmlAttribute("DealPromoLongDesc")]
        [Bindable(true)]
        public string DealPromoLongDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_promo_long_desc");
		    }
            set 
		    {
			    SetColumnValue("deal_promo_long_desc", value);
            }
        }
	      
        [XmlAttribute("DealPromoShortDesc")]
        [Bindable(true)]
        public string DealPromoShortDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_promo_short_desc");
		    }
            set 
		    {
			    SetColumnValue("deal_promo_short_desc", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_start_time", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("DealIsOpen")]
        [Bindable(true)]
        public bool DealIsOpen 
	    {
		    get
		    {
			    return GetColumnValue<bool>("deal_IsOpen");
		    }
            set 
		    {
			    SetColumnValue("deal_IsOpen", value);
            }
        }
	      
        [XmlAttribute("DealIsShow")]
        [Bindable(true)]
        public bool DealIsShow 
	    {
		    get
		    {
			    return GetColumnValue<bool>("deal_IsShow");
		    }
            set 
		    {
			    SetColumnValue("deal_IsShow", value);
            }
        }
	      
        [XmlAttribute("DealIsSoldout")]
        [Bindable(true)]
        public bool? DealIsSoldout 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("deal_is_soldout");
		    }
            set 
		    {
			    SetColumnValue("deal_is_soldout", value);
            }
        }
	      
        [XmlAttribute("DealIsAlwaysMain")]
        [Bindable(true)]
        public bool DealIsAlwaysMain 
	    {
		    get
		    {
			    return GetColumnValue<bool>("deal_is_always_main");
		    }
            set 
		    {
			    SetColumnValue("deal_is_always_main", value);
            }
        }
	      
        [XmlAttribute("DealIsVerifyByList")]
        [Bindable(true)]
        public bool DealIsVerifyByList 
	    {
		    get
		    {
			    return GetColumnValue<bool>("deal_is_verify_by_list");
		    }
            set 
		    {
			    SetColumnValue("deal_is_verify_by_list", value);
            }
        }
	      
        [XmlAttribute("DealIsVerifyByPad")]
        [Bindable(true)]
        public bool DealIsVerifyByPad 
	    {
		    get
		    {
			    return GetColumnValue<bool>("deal_is_verify_by_pad");
		    }
            set 
		    {
			    SetColumnValue("deal_is_verify_by_pad", value);
            }
        }
	      
        [XmlAttribute("Picture")]
        [Bindable(true)]
        public string Picture 
	    {
		    get
		    {
			    return GetColumnValue<string>("picture");
		    }
            set 
		    {
			    SetColumnValue("picture", value);
            }
        }
	      
        [XmlAttribute("DealCreateId")]
        [Bindable(true)]
        public string DealCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_create_id");
		    }
            set 
		    {
			    SetColumnValue("deal_create_id", value);
            }
        }
	      
        [XmlAttribute("DealCreateTime")]
        [Bindable(true)]
        public DateTime? DealCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_create_time");
		    }
            set 
		    {
			    SetColumnValue("deal_create_time", value);
            }
        }
	      
        [XmlAttribute("DealModifyId")]
        [Bindable(true)]
        public string DealModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_modify_id");
		    }
            set 
		    {
			    SetColumnValue("deal_modify_id", value);
            }
        }
	      
        [XmlAttribute("DealModifyTime")]
        [Bindable(true)]
        public DateTime? DealModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_modify_time");
		    }
            set 
		    {
			    SetColumnValue("deal_modify_time", value);
            }
        }
	      
        [XmlAttribute("DealCities")]
        [Bindable(true)]
        public string DealCities 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_cities");
		    }
            set 
		    {
			    SetColumnValue("deal_cities", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("CitySeq")]
        [Bindable(true)]
        public int? CitySeq 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_seq");
		    }
            set 
		    {
			    SetColumnValue("city_seq", value);
            }
        }
	      
        [XmlAttribute("CityCodeId")]
        [Bindable(true)]
        public int CityCodeId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_code_id");
		    }
            set 
		    {
			    SetColumnValue("city_code_id", value);
            }
        }
	      
        [XmlAttribute("CityCodeGroup")]
        [Bindable(true)]
        public string CityCodeGroup 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_code_group");
		    }
            set 
		    {
			    SetColumnValue("city_code_group", value);
            }
        }
	      
        [XmlAttribute("CityCity")]
        [Bindable(true)]
        public string CityCity 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_city");
		    }
            set 
		    {
			    SetColumnValue("city_city", value);
            }
        }
	      
        [XmlAttribute("CityStartTime")]
        [Bindable(true)]
        public DateTime CityStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("city_start_time");
		    }
            set 
		    {
			    SetColumnValue("city_start_time", value);
            }
        }
	      
        [XmlAttribute("CityEndTime")]
        [Bindable(true)]
        public DateTime? CityEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("city_end_time");
		    }
            set 
		    {
			    SetColumnValue("city_end_time", value);
            }
        }
	      
        [XmlAttribute("CategoryCodeName")]
        [Bindable(true)]
        public string CategoryCodeName 
	    {
		    get
		    {
			    return GetColumnValue<string>("category_code_name");
		    }
            set 
		    {
			    SetColumnValue("category_code_name", value);
            }
        }
	      
        [XmlAttribute("IsMain")]
        [Bindable(true)]
        public bool IsMain 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_main");
		    }
            set 
		    {
			    SetColumnValue("is_main", value);
            }
        }
	      
        [XmlAttribute("IsVisa")]
        [Bindable(true)]
        public bool? IsVisa 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_visa");
		    }
            set 
		    {
			    SetColumnValue("is_visa", value);
            }
        }
	      
        [XmlAttribute("PrimaryBigPicture")]
        [Bindable(true)]
        public string PrimaryBigPicture 
	    {
		    get
		    {
			    return GetColumnValue<string>("primary_big_picture");
		    }
            set 
		    {
			    SetColumnValue("primary_big_picture", value);
            }
        }
	      
        [XmlAttribute("PrimarySmallPicture")]
        [Bindable(true)]
        public string PrimarySmallPicture 
	    {
		    get
		    {
			    return GetColumnValue<string>("primary_small_picture");
		    }
            set 
		    {
			    SetColumnValue("primary_small_picture", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Category = @"category";
            
            public static string CategoryCodeId = @"category_code_id";
            
            public static string CityName = @"city_name";
            
            public static string SpecialCodeId = @"special_code_id";
            
            public static string SpecialName = @"special_name";
            
            public static string DealId = @"deal_id";
            
            public static string DealGuid = @"deal_guid";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string DealType = @"deal_type";
            
            public static string DealName = @"deal_name";
            
            public static string DealPromoLongDesc = @"deal_promo_long_desc";
            
            public static string DealPromoShortDesc = @"deal_promo_short_desc";
            
            public static string DealStartTime = @"deal_start_time";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string DealIsOpen = @"deal_IsOpen";
            
            public static string DealIsShow = @"deal_IsShow";
            
            public static string DealIsSoldout = @"deal_is_soldout";
            
            public static string DealIsAlwaysMain = @"deal_is_always_main";
            
            public static string DealIsVerifyByList = @"deal_is_verify_by_list";
            
            public static string DealIsVerifyByPad = @"deal_is_verify_by_pad";
            
            public static string Picture = @"picture";
            
            public static string DealCreateId = @"deal_create_id";
            
            public static string DealCreateTime = @"deal_create_time";
            
            public static string DealModifyId = @"deal_modify_id";
            
            public static string DealModifyTime = @"deal_modify_time";
            
            public static string DealCities = @"deal_cities";
            
            public static string CityId = @"city_id";
            
            public static string CitySeq = @"city_seq";
            
            public static string CityCodeId = @"city_code_id";
            
            public static string CityCodeGroup = @"city_code_group";
            
            public static string CityCity = @"city_city";
            
            public static string CityStartTime = @"city_start_time";
            
            public static string CityEndTime = @"city_end_time";
            
            public static string CategoryCodeName = @"category_code_name";
            
            public static string IsMain = @"is_main";
            
            public static string IsVisa = @"is_visa";
            
            public static string PrimaryBigPicture = @"primary_big_picture";
            
            public static string PrimarySmallPicture = @"primary_small_picture";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

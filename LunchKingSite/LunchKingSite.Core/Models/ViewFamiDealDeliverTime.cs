using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewFamiDealDeliverTime class.
    /// </summary>
    [Serializable]
    public partial class ViewFamiDealDeliverTimeCollection : ReadOnlyList<ViewFamiDealDeliverTime, ViewFamiDealDeliverTimeCollection>
    {        
        public ViewFamiDealDeliverTimeCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_fami_deal_deliver_time view.
    /// </summary>
    [Serializable]
    public partial class ViewFamiDealDeliverTime : ReadOnlyRecord<ViewFamiDealDeliverTime>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_fami_deal_deliver_time", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = false;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponId);
                
                TableSchema.TableColumn colvarPezCode = new TableSchema.TableColumn(schema);
                colvarPezCode.ColumnName = "PezCode";
                colvarPezCode.DataType = DbType.String;
                colvarPezCode.MaxLength = 50;
                colvarPezCode.AutoIncrement = false;
                colvarPezCode.IsNullable = true;
                colvarPezCode.IsPrimaryKey = false;
                colvarPezCode.IsForeignKey = false;
                colvarPezCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarPezCode);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarCouponCodeType = new TableSchema.TableColumn(schema);
                colvarCouponCodeType.ColumnName = "coupon_code_type";
                colvarCouponCodeType.DataType = DbType.Int32;
                colvarCouponCodeType.MaxLength = 0;
                colvarCouponCodeType.AutoIncrement = false;
                colvarCouponCodeType.IsNullable = false;
                colvarCouponCodeType.IsPrimaryKey = false;
                colvarCouponCodeType.IsForeignKey = false;
                colvarCouponCodeType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponCodeType);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_fami_deal_deliver_time",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewFamiDealDeliverTime()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewFamiDealDeliverTime(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewFamiDealDeliverTime(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewFamiDealDeliverTime(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int CouponId 
	    {
		    get
		    {
			    return GetColumnValue<int>("coupon_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_id", value);
            }
        }
	      
        [XmlAttribute("PezCode")]
        [Bindable(true)]
        public string PezCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("PezCode");
		    }
            set 
		    {
			    SetColumnValue("PezCode", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("CouponCodeType")]
        [Bindable(true)]
        public int CouponCodeType 
	    {
		    get
		    {
			    return GetColumnValue<int>("coupon_code_type");
		    }
            set 
		    {
			    SetColumnValue("coupon_code_type", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CouponId = @"coupon_id";
            
            public static string PezCode = @"PezCode";
            
            public static string CouponUsage = @"coupon_usage";
            
            public static string UserId = @"user_id";
            
            public static string CouponCodeType = @"coupon_code_type";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

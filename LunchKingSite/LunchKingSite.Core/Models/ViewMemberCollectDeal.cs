using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMemberCollectDeal class.
    /// </summary>
    [Serializable]
    public partial class ViewMemberCollectDealCollection : ReadOnlyList<ViewMemberCollectDeal, ViewMemberCollectDealCollection>
    {        
        public ViewMemberCollectDealCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_member_collect_deal view.
    /// </summary>
    [Serializable]
    public partial class ViewMemberCollectDeal : ReadOnlyRecord<ViewMemberCollectDeal>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_member_collect_deal", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarMemberUniqueId = new TableSchema.TableColumn(schema);
                colvarMemberUniqueId.ColumnName = "member_unique_id";
                colvarMemberUniqueId.DataType = DbType.Int32;
                colvarMemberUniqueId.MaxLength = 0;
                colvarMemberUniqueId.AutoIncrement = false;
                colvarMemberUniqueId.IsNullable = false;
                colvarMemberUniqueId.IsPrimaryKey = false;
                colvarMemberUniqueId.IsForeignKey = false;
                colvarMemberUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberUniqueId);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarCollectTime = new TableSchema.TableColumn(schema);
                colvarCollectTime.ColumnName = "collect_time";
                colvarCollectTime.DataType = DbType.DateTime;
                colvarCollectTime.MaxLength = 0;
                colvarCollectTime.AutoIncrement = false;
                colvarCollectTime.IsNullable = false;
                colvarCollectTime.IsPrimaryKey = false;
                colvarCollectTime.IsForeignKey = false;
                colvarCollectTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCollectTime);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarChangedExpireDate.ColumnName = "changed_expire_date";
                colvarChangedExpireDate.DataType = DbType.DateTime;
                colvarChangedExpireDate.MaxLength = 0;
                colvarChangedExpireDate.AutoIncrement = false;
                colvarChangedExpireDate.IsNullable = true;
                colvarChangedExpireDate.IsPrimaryKey = false;
                colvarChangedExpireDate.IsForeignKey = false;
                colvarChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarChangedExpireDate);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_member_collect_deal",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMemberCollectDeal()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMemberCollectDeal(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMemberCollectDeal(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMemberCollectDeal(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("MemberUniqueId")]
        [Bindable(true)]
        public int MemberUniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("member_unique_id");
		    }
            set 
		    {
			    SetColumnValue("member_unique_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("CollectTime")]
        [Bindable(true)]
        public DateTime CollectTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("collect_time");
		    }
            set 
		    {
			    SetColumnValue("collect_time", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("ChangedExpireDate")]
        [Bindable(true)]
        public DateTime? ChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("changed_expire_date", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string MemberUniqueId = @"member_unique_id";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string CityId = @"city_id";
            
            public static string CollectTime = @"collect_time";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string ChangedExpireDate = @"changed_expire_date";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpDepositItem class.
	/// </summary>
    [Serializable]
	public partial class PcpDepositItemCollection : RepositoryList<PcpDepositItem, PcpDepositItemCollection>
	{	   
		public PcpDepositItemCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpDepositItemCollection</returns>
		public PcpDepositItemCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpDepositItem o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_deposit_item table.
	/// </summary>
	[Serializable]
	public partial class PcpDepositItem : RepositoryRecord<PcpDepositItem>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpDepositItem()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpDepositItem(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_deposit_item", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarDepositMenuId = new TableSchema.TableColumn(schema);
				colvarDepositMenuId.ColumnName = "deposit_menu_id";
				colvarDepositMenuId.DataType = DbType.Int32;
				colvarDepositMenuId.MaxLength = 0;
				colvarDepositMenuId.AutoIncrement = false;
				colvarDepositMenuId.IsNullable = false;
				colvarDepositMenuId.IsPrimaryKey = false;
				colvarDepositMenuId.IsForeignKey = false;
				colvarDepositMenuId.IsReadOnly = false;
				colvarDepositMenuId.DefaultSetting = @"";
				colvarDepositMenuId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDepositMenuId);
				
				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 100;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = false;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);
				
				TableSchema.TableColumn colvarCreatedId = new TableSchema.TableColumn(schema);
				colvarCreatedId.ColumnName = "created_id";
				colvarCreatedId.DataType = DbType.String;
				colvarCreatedId.MaxLength = 100;
				colvarCreatedId.AutoIncrement = false;
				colvarCreatedId.IsNullable = false;
				colvarCreatedId.IsPrimaryKey = false;
				colvarCreatedId.IsForeignKey = false;
				colvarCreatedId.IsReadOnly = false;
				colvarCreatedId.DefaultSetting = @"";
				colvarCreatedId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedId);
				
				TableSchema.TableColumn colvarModifiedTime = new TableSchema.TableColumn(schema);
				colvarModifiedTime.ColumnName = "modified_time";
				colvarModifiedTime.DataType = DbType.DateTime;
				colvarModifiedTime.MaxLength = 0;
				colvarModifiedTime.AutoIncrement = false;
				colvarModifiedTime.IsNullable = true;
				colvarModifiedTime.IsPrimaryKey = false;
				colvarModifiedTime.IsForeignKey = false;
				colvarModifiedTime.IsReadOnly = false;
				colvarModifiedTime.DefaultSetting = @"";
				colvarModifiedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedTime);
				
				TableSchema.TableColumn colvarCreatedTime = new TableSchema.TableColumn(schema);
				colvarCreatedTime.ColumnName = "created_time";
				colvarCreatedTime.DataType = DbType.DateTime;
				colvarCreatedTime.MaxLength = 0;
				colvarCreatedTime.AutoIncrement = false;
				colvarCreatedTime.IsNullable = false;
				colvarCreatedTime.IsPrimaryKey = false;
				colvarCreatedTime.IsForeignKey = false;
				colvarCreatedTime.IsReadOnly = false;
				colvarCreatedTime.DefaultSetting = @"";
				colvarCreatedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedTime);
				
				TableSchema.TableColumn colvarSize = new TableSchema.TableColumn(schema);
				colvarSize.ColumnName = "size";
				colvarSize.DataType = DbType.String;
				colvarSize.MaxLength = 5;
				colvarSize.AutoIncrement = false;
				colvarSize.IsNullable = true;
				colvarSize.IsPrimaryKey = false;
				colvarSize.IsForeignKey = false;
				colvarSize.IsReadOnly = false;
				colvarSize.DefaultSetting = @"";
				colvarSize.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSize);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_deposit_item",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("DepositMenuId")]
		[Bindable(true)]
		public int DepositMenuId 
		{
			get { return GetColumnValue<int>(Columns.DepositMenuId); }
			set { SetColumnValue(Columns.DepositMenuId, value); }
		}
		  
		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName 
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}
		  
		[XmlAttribute("CreatedId")]
		[Bindable(true)]
		public string CreatedId 
		{
			get { return GetColumnValue<string>(Columns.CreatedId); }
			set { SetColumnValue(Columns.CreatedId, value); }
		}
		  
		[XmlAttribute("ModifiedTime")]
		[Bindable(true)]
		public DateTime? ModifiedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifiedTime); }
			set { SetColumnValue(Columns.ModifiedTime, value); }
		}
		  
		[XmlAttribute("CreatedTime")]
		[Bindable(true)]
		public DateTime CreatedTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedTime); }
			set { SetColumnValue(Columns.CreatedTime, value); }
		}
		  
		[XmlAttribute("Size")]
		[Bindable(true)]
		public string Size 
		{
			get { return GetColumnValue<string>(Columns.Size); }
			set { SetColumnValue(Columns.Size, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DepositMenuIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn SizeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string DepositMenuId = @"deposit_menu_id";
			 public static string ItemName = @"item_name";
			 public static string CreatedId = @"created_id";
			 public static string ModifiedTime = @"modified_time";
			 public static string CreatedTime = @"created_time";
			 public static string Size = @"size";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

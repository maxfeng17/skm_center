using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpDiscountCampaignLockPoint class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpDiscountCampaignLockPointCollection : ReadOnlyList<ViewPcpDiscountCampaignLockPoint, ViewPcpDiscountCampaignLockPointCollection>
    {        
        public ViewPcpDiscountCampaignLockPointCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_discount_campaign_lock_point view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpDiscountCampaignLockPoint : ReadOnlyRecord<ViewPcpDiscountCampaignLockPoint>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_discount_campaign_lock_point", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarCampaignNo = new TableSchema.TableColumn(schema);
                colvarCampaignNo.ColumnName = "campaign_no";
                colvarCampaignNo.DataType = DbType.AnsiString;
                colvarCampaignNo.MaxLength = 15;
                colvarCampaignNo.AutoIncrement = false;
                colvarCampaignNo.IsNullable = true;
                colvarCampaignNo.IsPrimaryKey = false;
                colvarCampaignNo.IsForeignKey = false;
                colvarCampaignNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCampaignNo);
                
                TableSchema.TableColumn colvarSubject = new TableSchema.TableColumn(schema);
                colvarSubject.ColumnName = "subject";
                colvarSubject.DataType = DbType.String;
                colvarSubject.MaxLength = -1;
                colvarSubject.AutoIncrement = false;
                colvarSubject.IsNullable = false;
                colvarSubject.IsPrimaryKey = false;
                colvarSubject.IsForeignKey = false;
                colvarSubject.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubject);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = true;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                TableSchema.TableColumn colvarLockPoint = new TableSchema.TableColumn(schema);
                colvarLockPoint.ColumnName = "lock_point";
                colvarLockPoint.DataType = DbType.Int32;
                colvarLockPoint.MaxLength = 0;
                colvarLockPoint.AutoIncrement = false;
                colvarLockPoint.IsNullable = false;
                colvarLockPoint.IsPrimaryKey = false;
                colvarLockPoint.IsForeignKey = false;
                colvarLockPoint.IsReadOnly = false;
                
                schema.Columns.Add(colvarLockPoint);
                
                TableSchema.TableColumn colvarUsedPoint = new TableSchema.TableColumn(schema);
                colvarUsedPoint.ColumnName = "used_point";
                colvarUsedPoint.DataType = DbType.Int32;
                colvarUsedPoint.MaxLength = 0;
                colvarUsedPoint.AutoIncrement = false;
                colvarUsedPoint.IsNullable = true;
                colvarUsedPoint.IsPrimaryKey = false;
                colvarUsedPoint.IsForeignKey = false;
                colvarUsedPoint.IsReadOnly = false;
                
                schema.Columns.Add(colvarUsedPoint);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_discount_campaign_lock_point",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpDiscountCampaignLockPoint()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpDiscountCampaignLockPoint(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpDiscountCampaignLockPoint(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpDiscountCampaignLockPoint(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("CampaignNo")]
        [Bindable(true)]
        public string CampaignNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("campaign_no");
		    }
            set 
		    {
			    SetColumnValue("campaign_no", value);
            }
        }
	      
        [XmlAttribute("Subject")]
        [Bindable(true)]
        public string Subject 
	    {
		    get
		    {
			    return GetColumnValue<string>("subject");
		    }
            set 
		    {
			    SetColumnValue("subject", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime? StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	      
        [XmlAttribute("LockPoint")]
        [Bindable(true)]
        public int LockPoint 
	    {
		    get
		    {
			    return GetColumnValue<int>("lock_point");
		    }
            set 
		    {
			    SetColumnValue("lock_point", value);
            }
        }
	      
        [XmlAttribute("UsedPoint")]
        [Bindable(true)]
        public int? UsedPoint 
	    {
		    get
		    {
			    return GetColumnValue<int?>("used_point");
		    }
            set 
		    {
			    SetColumnValue("used_point", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string CampaignNo = @"campaign_no";
            
            public static string Subject = @"subject";
            
            public static string StartTime = @"start_time";
            
            public static string EndTime = @"end_time";
            
            public static string LockPoint = @"lock_point";
            
            public static string UsedPoint = @"used_point";
            
            public static string UserId = @"user_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

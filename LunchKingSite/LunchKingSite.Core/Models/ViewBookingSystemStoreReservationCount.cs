using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBookingSystemStoreReservationCount class.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemStoreReservationCountCollection : ReadOnlyList<ViewBookingSystemStoreReservationCount, ViewBookingSystemStoreReservationCountCollection>
    {        
        public ViewBookingSystemStoreReservationCountCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_booking_system_store_reservation_count view.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemStoreReservationCount : ReadOnlyRecord<ViewBookingSystemStoreReservationCount>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_booking_system_store_reservation_count", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBookingSystemStoreBookingId = new TableSchema.TableColumn(schema);
                colvarBookingSystemStoreBookingId.ColumnName = "booking_system_store_booking_id";
                colvarBookingSystemStoreBookingId.DataType = DbType.Int32;
                colvarBookingSystemStoreBookingId.MaxLength = 0;
                colvarBookingSystemStoreBookingId.AutoIncrement = false;
                colvarBookingSystemStoreBookingId.IsNullable = false;
                colvarBookingSystemStoreBookingId.IsPrimaryKey = false;
                colvarBookingSystemStoreBookingId.IsForeignKey = false;
                colvarBookingSystemStoreBookingId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBookingSystemStoreBookingId);
                
                TableSchema.TableColumn colvarNumberOfPeople = new TableSchema.TableColumn(schema);
                colvarNumberOfPeople.ColumnName = "number_of_people";
                colvarNumberOfPeople.DataType = DbType.Int32;
                colvarNumberOfPeople.MaxLength = 0;
                colvarNumberOfPeople.AutoIncrement = false;
                colvarNumberOfPeople.IsNullable = true;
                colvarNumberOfPeople.IsPrimaryKey = false;
                colvarNumberOfPeople.IsForeignKey = false;
                colvarNumberOfPeople.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumberOfPeople);
                
                TableSchema.TableColumn colvarReservationDate = new TableSchema.TableColumn(schema);
                colvarReservationDate.ColumnName = "reservation_date";
                colvarReservationDate.DataType = DbType.AnsiString;
                colvarReservationDate.MaxLength = 30;
                colvarReservationDate.AutoIncrement = false;
                colvarReservationDate.IsNullable = true;
                colvarReservationDate.IsPrimaryKey = false;
                colvarReservationDate.IsForeignKey = false;
                colvarReservationDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarReservationDate);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_booking_system_store_reservation_count",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBookingSystemStoreReservationCount()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBookingSystemStoreReservationCount(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBookingSystemStoreReservationCount(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBookingSystemStoreReservationCount(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BookingSystemStoreBookingId")]
        [Bindable(true)]
        public int BookingSystemStoreBookingId 
	    {
		    get
		    {
			    return GetColumnValue<int>("booking_system_store_booking_id");
		    }
            set 
		    {
			    SetColumnValue("booking_system_store_booking_id", value);
            }
        }
	      
        [XmlAttribute("NumberOfPeople")]
        [Bindable(true)]
        public int? NumberOfPeople 
	    {
		    get
		    {
			    return GetColumnValue<int?>("number_of_people");
		    }
            set 
		    {
			    SetColumnValue("number_of_people", value);
            }
        }
	      
        [XmlAttribute("ReservationDate")]
        [Bindable(true)]
        public string ReservationDate 
	    {
		    get
		    {
			    return GetColumnValue<string>("reservation_date");
		    }
            set 
		    {
			    SetColumnValue("reservation_date", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BookingSystemStoreBookingId = @"booking_system_store_booking_id";
            
            public static string NumberOfPeople = @"number_of_people";
            
            public static string ReservationDate = @"reservation_date";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

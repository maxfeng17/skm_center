using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class SoloEdmMainCollection : RepositoryList<SoloEdmMain, SoloEdmMainCollection>
	{
			public SoloEdmMainCollection() {}

			public SoloEdmMainCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							SoloEdmMain o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class SoloEdmMain : RepositoryRecord<SoloEdmMain>, IRecordBase
	{
		#region .ctors and Default Settings
		public SoloEdmMain()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public SoloEdmMain(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("solo_edm_main", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarSubject = new TableSchema.TableColumn(schema);
				colvarSubject.ColumnName = "subject";
				colvarSubject.DataType = DbType.String;
				colvarSubject.MaxLength = 200;
				colvarSubject.AutoIncrement = false;
				colvarSubject.IsNullable = false;
				colvarSubject.IsPrimaryKey = false;
				colvarSubject.IsForeignKey = false;
				colvarSubject.IsReadOnly = false;
				colvarSubject.DefaultSetting = @"";
				colvarSubject.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubject);

				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 100;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);

				TableSchema.TableColumn colvarTitleContent = new TableSchema.TableColumn(schema);
				colvarTitleContent.ColumnName = "title_content";
				colvarTitleContent.DataType = DbType.String;
				colvarTitleContent.MaxLength = 200;
				colvarTitleContent.AutoIncrement = false;
				colvarTitleContent.IsNullable = false;
				colvarTitleContent.IsPrimaryKey = false;
				colvarTitleContent.IsForeignKey = false;
				colvarTitleContent.IsReadOnly = false;
				colvarTitleContent.DefaultSetting = @"";
				colvarTitleContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitleContent);

				TableSchema.TableColumn colvarSendDate = new TableSchema.TableColumn(schema);
				colvarSendDate.ColumnName = "send_date";
				colvarSendDate.DataType = DbType.DateTime;
				colvarSendDate.MaxLength = 0;
				colvarSendDate.AutoIncrement = false;
				colvarSendDate.IsNullable = false;
				colvarSendDate.IsPrimaryKey = false;
				colvarSendDate.IsForeignKey = false;
				colvarSendDate.IsReadOnly = false;
				colvarSendDate.DefaultSetting = @"";
				colvarSendDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendDate);

				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = true;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);

				TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
				colvarCreator.ColumnName = "creator";
				colvarCreator.DataType = DbType.String;
				colvarCreator.MaxLength = 150;
				colvarCreator.AutoIncrement = false;
				colvarCreator.IsNullable = false;
				colvarCreator.IsPrimaryKey = false;
				colvarCreator.IsForeignKey = false;
				colvarCreator.IsReadOnly = false;
				colvarCreator.DefaultSetting = @"";
				colvarCreator.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreator);

				TableSchema.TableColumn colvarCreateDate = new TableSchema.TableColumn(schema);
				colvarCreateDate.ColumnName = "create_date";
				colvarCreateDate.DataType = DbType.DateTime;
				colvarCreateDate.MaxLength = 0;
				colvarCreateDate.AutoIncrement = false;
				colvarCreateDate.IsNullable = false;
				colvarCreateDate.IsPrimaryKey = false;
				colvarCreateDate.IsForeignKey = false;
				colvarCreateDate.IsReadOnly = false;
				colvarCreateDate.DefaultSetting = @"";
				colvarCreateDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateDate);

				TableSchema.TableColumn colvarUploadDate = new TableSchema.TableColumn(schema);
				colvarUploadDate.ColumnName = "upload_date";
				colvarUploadDate.DataType = DbType.DateTime;
				colvarUploadDate.MaxLength = 0;
				colvarUploadDate.AutoIncrement = false;
				colvarUploadDate.IsNullable = true;
				colvarUploadDate.IsPrimaryKey = false;
				colvarUploadDate.IsForeignKey = false;
				colvarUploadDate.IsReadOnly = false;
				colvarUploadDate.DefaultSetting = @"";
				colvarUploadDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUploadDate);

				TableSchema.TableColumn colvarPromoImg = new TableSchema.TableColumn(schema);
				colvarPromoImg.ColumnName = "promo_img";
				colvarPromoImg.DataType = DbType.String;
				colvarPromoImg.MaxLength = 2147483647;
				colvarPromoImg.AutoIncrement = false;
				colvarPromoImg.IsNullable = true;
				colvarPromoImg.IsPrimaryKey = false;
				colvarPromoImg.IsForeignKey = false;
				colvarPromoImg.IsReadOnly = false;
				colvarPromoImg.DefaultSetting = @"";
				colvarPromoImg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPromoImg);

				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 2147483647;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);

				TableSchema.TableColumn colvarCpa = new TableSchema.TableColumn(schema);
				colvarCpa.ColumnName = "cpa";
				colvarCpa.DataType = DbType.String;
				colvarCpa.MaxLength = 20;
				colvarCpa.AutoIncrement = false;
				colvarCpa.IsNullable = true;
				colvarCpa.IsPrimaryKey = false;
				colvarCpa.IsForeignKey = false;
				colvarCpa.IsReadOnly = false;
				colvarCpa.DefaultSetting = @"";
				colvarCpa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCpa);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("solo_edm_main",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("Subject")]
		[Bindable(true)]
		public string Subject
		{
			get { return GetColumnValue<string>(Columns.Subject); }
			set { SetColumnValue(Columns.Subject, value); }
		}

		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}

		[XmlAttribute("TitleContent")]
		[Bindable(true)]
		public string TitleContent
		{
			get { return GetColumnValue<string>(Columns.TitleContent); }
			set { SetColumnValue(Columns.TitleContent, value); }
		}

		[XmlAttribute("SendDate")]
		[Bindable(true)]
		public DateTime SendDate
		{
			get { return GetColumnValue<DateTime>(Columns.SendDate); }
			set { SetColumnValue(Columns.SendDate, value); }
		}

		[XmlAttribute("Type")]
		[Bindable(true)]
		public int? Type
		{
			get { return GetColumnValue<int?>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}

		[XmlAttribute("Creator")]
		[Bindable(true)]
		public string Creator
		{
			get { return GetColumnValue<string>(Columns.Creator); }
			set { SetColumnValue(Columns.Creator, value); }
		}

		[XmlAttribute("CreateDate")]
		[Bindable(true)]
		public DateTime CreateDate
		{
			get { return GetColumnValue<DateTime>(Columns.CreateDate); }
			set { SetColumnValue(Columns.CreateDate, value); }
		}

		[XmlAttribute("UploadDate")]
		[Bindable(true)]
		public DateTime? UploadDate
		{
			get { return GetColumnValue<DateTime?>(Columns.UploadDate); }
			set { SetColumnValue(Columns.UploadDate, value); }
		}

		[XmlAttribute("PromoImg")]
		[Bindable(true)]
		public string PromoImg
		{
			get { return GetColumnValue<string>(Columns.PromoImg); }
			set { SetColumnValue(Columns.PromoImg, value); }
		}

		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}

		[XmlAttribute("Cpa")]
		[Bindable(true)]
		public string Cpa
		{
			get { return GetColumnValue<string>(Columns.Cpa); }
			set { SetColumnValue(Columns.Cpa, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn SubjectColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn TitleColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn TitleContentColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn SendDateColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn TypeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn CreatorColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn CreateDateColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn UploadDateColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn PromoImgColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn MessageColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn CpaColumn
		{
			get { return Schema.Columns[11]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string Subject = @"subject";
			public static string Title = @"title";
			public static string TitleContent = @"title_content";
			public static string SendDate = @"send_date";
			public static string Type = @"type";
			public static string Creator = @"creator";
			public static string CreateDate = @"create_date";
			public static string UploadDate = @"upload_date";
			public static string PromoImg = @"promo_img";
			public static string Message = @"message";
			public static string Cpa = @"cpa";
		}

		#endregion

	}
}

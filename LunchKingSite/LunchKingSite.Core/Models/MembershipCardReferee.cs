﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the MembershipCardReferee class.
    /// </summary>
    [Serializable]
    public partial class MembershipCardRefereeCollection : RepositoryList<MembershipCardReferee, MembershipCardRefereeCollection>
    {
        public MembershipCardRefereeCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MembershipCardRefereeCollection</returns>
        public MembershipCardRefereeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MembershipCardReferee o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the membership_card_referee table.
    /// </summary>
    [Serializable]
    public partial class MembershipCardReferee : RepositoryRecord<MembershipCardReferee>, IRecordBase
    {
        #region .ctors and Default Settings

        public MembershipCardReferee()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public MembershipCardReferee(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("membership_card_referee", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarRefereeUserId = new TableSchema.TableColumn(schema);
                colvarRefereeUserId.ColumnName = "referee_user_id";
                colvarRefereeUserId.DataType = DbType.Int32;
                colvarRefereeUserId.MaxLength = 0;
                colvarRefereeUserId.AutoIncrement = false;
                colvarRefereeUserId.IsNullable = false;
                colvarRefereeUserId.IsPrimaryKey = false;
                colvarRefereeUserId.IsForeignKey = false;
                colvarRefereeUserId.IsReadOnly = false;
                colvarRefereeUserId.DefaultSetting = @"";
                colvarRefereeUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRefereeUserId);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
                colvarGroupId.ColumnName = "group_id";
                colvarGroupId.DataType = DbType.Int32;
                colvarGroupId.MaxLength = 0;
                colvarGroupId.AutoIncrement = false;
                colvarGroupId.IsNullable = false;
                colvarGroupId.IsPrimaryKey = false;
                colvarGroupId.IsForeignKey = false;
                colvarGroupId.IsReadOnly = false;
                colvarGroupId.DefaultSetting = @"";
                colvarGroupId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGroupId);

                TableSchema.TableColumn colvarMCardId = new TableSchema.TableColumn(schema);
                colvarMCardId.ColumnName = "m_card_id";
                colvarMCardId.DataType = DbType.Int32;
                colvarMCardId.MaxLength = 0;
                colvarMCardId.AutoIncrement = false;
                colvarMCardId.IsNullable = false;
                colvarMCardId.IsPrimaryKey = false;
                colvarMCardId.IsForeignKey = false;
                colvarMCardId.IsReadOnly = false;
                colvarMCardId.DefaultSetting = @"";
                colvarMCardId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMCardId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarIsDelete = new TableSchema.TableColumn(schema);
                colvarIsDelete.ColumnName = "is_delete";
                colvarIsDelete.DataType = DbType.Boolean;
                colvarIsDelete.MaxLength = 0;
                colvarIsDelete.AutoIncrement = false;
                colvarIsDelete.IsNullable = false;
                colvarIsDelete.IsPrimaryKey = false;
                colvarIsDelete.IsForeignKey = false;
                colvarIsDelete.IsReadOnly = false;

                colvarIsDelete.DefaultSetting = @"((0))";
                colvarIsDelete.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsDelete);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("membership_card_referee", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("RefereeUserId")]
        [Bindable(true)]
        public int RefereeUserId
        {
            get { return GetColumnValue<int>(Columns.RefereeUserId); }
            set { SetColumnValue(Columns.RefereeUserId, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get { return GetColumnValue<int>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("GroupId")]
        [Bindable(true)]
        public int GroupId
        {
            get { return GetColumnValue<int>(Columns.GroupId); }
            set { SetColumnValue(Columns.GroupId, value); }
        }

        [XmlAttribute("MCardId")]
        [Bindable(true)]
        public int MCardId
        {
            get { return GetColumnValue<int>(Columns.MCardId); }
            set { SetColumnValue(Columns.MCardId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("IsDelete")]
        [Bindable(true)]
        public bool IsDelete
        {
            get { return GetColumnValue<bool>(Columns.IsDelete); }
            set { SetColumnValue(Columns.IsDelete, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn RefereeUserIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn GroupIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn MCardIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn IsDeleteColumn
        {
            get { return Schema.Columns[6]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string RefereeUserId = @"referee_user_id";
            public static string UserId = @"user_id";
            public static string GroupId = @"group_id";
            public static string MCardId = @"m_card_id";
            public static string CreateTime = @"create_time";
            public static string IsDelete = @"is_delete";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

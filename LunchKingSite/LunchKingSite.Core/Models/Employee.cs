using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the Employee class.
    /// </summary>
    [Serializable]
    public partial class EmployeeCollection : RepositoryList<Employee, EmployeeCollection>
    {
        public EmployeeCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EmployeeCollection</returns>
        public EmployeeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Employee o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }


    /// <summary>
    /// This is an ActiveRecord class which wraps the employee table.
    /// </summary>

    [Serializable]
    public partial class Employee : RepositoryRecord<Employee>, IRecordBase
    {
        #region .ctors and Default Settings

        public Employee()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public Employee(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("employee", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarEmpId = new TableSchema.TableColumn(schema);
                colvarEmpId.ColumnName = "emp_id";
                colvarEmpId.DataType = DbType.AnsiString;
                colvarEmpId.MaxLength = 50;
                colvarEmpId.AutoIncrement = false;
                colvarEmpId.IsNullable = false;
                colvarEmpId.IsPrimaryKey = true;
                colvarEmpId.IsForeignKey = false;
                colvarEmpId.IsReadOnly = false;
                colvarEmpId.DefaultSetting = @"";
                colvarEmpId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEmpId);

                TableSchema.TableColumn colvarEmpName = new TableSchema.TableColumn(schema);
                colvarEmpName.ColumnName = "emp_name";
                colvarEmpName.DataType = DbType.String;
                colvarEmpName.MaxLength = 50;
                colvarEmpName.AutoIncrement = false;
                colvarEmpName.IsNullable = true;
                colvarEmpName.IsPrimaryKey = false;
                colvarEmpName.IsForeignKey = false;
                colvarEmpName.IsReadOnly = false;
                colvarEmpName.DefaultSetting = @"";
                colvarEmpName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEmpName);

                TableSchema.TableColumn colvarDeptId = new TableSchema.TableColumn(schema);
                colvarDeptId.ColumnName = "dept_id";
                colvarDeptId.DataType = DbType.AnsiString;
                colvarDeptId.MaxLength = 50;
                colvarDeptId.AutoIncrement = false;
                colvarDeptId.IsNullable = true;
                colvarDeptId.IsPrimaryKey = false;
                colvarDeptId.IsForeignKey = false;
                colvarDeptId.IsReadOnly = false;
                colvarDeptId.DefaultSetting = @"";
                colvarDeptId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDeptId);

                TableSchema.TableColumn colvarAvailableDate = new TableSchema.TableColumn(schema);
                colvarAvailableDate.ColumnName = "available_date";
                colvarAvailableDate.DataType = DbType.DateTime;
                colvarAvailableDate.MaxLength = 0;
                colvarAvailableDate.AutoIncrement = false;
                colvarAvailableDate.IsNullable = true;
                colvarAvailableDate.IsPrimaryKey = false;
                colvarAvailableDate.IsForeignKey = false;
                colvarAvailableDate.IsReadOnly = false;
                colvarAvailableDate.DefaultSetting = @"";
                colvarAvailableDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAvailableDate);

                TableSchema.TableColumn colvarDepartureDate = new TableSchema.TableColumn(schema);
                colvarDepartureDate.ColumnName = "departure_date";
                colvarDepartureDate.DataType = DbType.DateTime;
                colvarDepartureDate.MaxLength = 0;
                colvarDepartureDate.AutoIncrement = false;
                colvarDepartureDate.IsNullable = true;
                colvarDepartureDate.IsPrimaryKey = false;
                colvarDepartureDate.IsForeignKey = false;
                colvarDepartureDate.IsReadOnly = false;
                colvarDepartureDate.DefaultSetting = @"";
                colvarDepartureDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDepartureDate);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 256;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.String;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                colvarMobile.DefaultSetting = @"";
                colvarMobile.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMobile);

                TableSchema.TableColumn colvarIsInvisible = new TableSchema.TableColumn(schema);
                colvarIsInvisible.ColumnName = "is_invisible";
                colvarIsInvisible.DataType = DbType.Boolean;
                colvarIsInvisible.MaxLength = 0;
                colvarIsInvisible.AutoIncrement = false;
                colvarIsInvisible.IsNullable = false;
                colvarIsInvisible.IsPrimaryKey = false;
                colvarIsInvisible.IsForeignKey = false;
                colvarIsInvisible.IsReadOnly = false;

                colvarIsInvisible.DefaultSetting = @"((0))";
                colvarIsInvisible.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsInvisible);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = true;
                colvarUserId.IsReadOnly = false;

                colvarUserId.DefaultSetting = @"((0))";

                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarEmpLevel = new TableSchema.TableColumn(schema);
                colvarEmpLevel.ColumnName = "emp_level";
                colvarEmpLevel.DataType = DbType.Int32;
                colvarEmpLevel.MaxLength = 0;
                colvarEmpLevel.AutoIncrement = false;
                colvarEmpLevel.IsNullable = true;
                colvarEmpLevel.IsPrimaryKey = false;
                colvarEmpLevel.IsForeignKey = false;
                colvarEmpLevel.IsReadOnly = false;
                colvarEmpLevel.DefaultSetting = @"";
                colvarEmpLevel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEmpLevel);

                TableSchema.TableColumn colvarEmpNo = new TableSchema.TableColumn(schema);
                colvarEmpNo.ColumnName = "emp_no";
                colvarEmpNo.DataType = DbType.AnsiString;
                colvarEmpNo.MaxLength = 50;
                colvarEmpNo.AutoIncrement = false;
                colvarEmpNo.IsNullable = true;
                colvarEmpNo.IsPrimaryKey = false;
                colvarEmpNo.IsForeignKey = false;
                colvarEmpNo.IsReadOnly = false;
                colvarEmpNo.DefaultSetting = @"";
                colvarEmpNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEmpNo);

                TableSchema.TableColumn colvarDeptManager = new TableSchema.TableColumn(schema);
                colvarDeptManager.ColumnName = "dept_manager";
                colvarDeptManager.DataType = DbType.AnsiString;
                colvarDeptManager.MaxLength = 100;
                colvarDeptManager.AutoIncrement = false;
                colvarDeptManager.IsNullable = true;
                colvarDeptManager.IsPrimaryKey = false;
                colvarDeptManager.IsForeignKey = false;
                colvarDeptManager.IsReadOnly = false;
                colvarDeptManager.DefaultSetting = @"";
                colvarDeptManager.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDeptManager);

                TableSchema.TableColumn colvarTeamNo = new TableSchema.TableColumn(schema);
                colvarTeamNo.ColumnName = "team_no";
                colvarTeamNo.DataType = DbType.Int32;
                colvarTeamNo.MaxLength = 0;
                colvarTeamNo.AutoIncrement = false;
                colvarTeamNo.IsNullable = true;
                colvarTeamNo.IsPrimaryKey = false;
                colvarTeamNo.IsForeignKey = false;
                colvarTeamNo.IsReadOnly = false;
                colvarTeamNo.DefaultSetting = @"";
                colvarTeamNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTeamNo);

                TableSchema.TableColumn colvarTeamManager = new TableSchema.TableColumn(schema);
                colvarTeamManager.ColumnName = "team_manager";
                colvarTeamManager.DataType = DbType.Boolean;
                colvarTeamManager.MaxLength = 0;
                colvarTeamManager.AutoIncrement = false;
                colvarTeamManager.IsNullable = false;
                colvarTeamManager.IsPrimaryKey = false;
                colvarTeamManager.IsForeignKey = false;
                colvarTeamManager.IsReadOnly = false;

                colvarTeamManager.DefaultSetting = @"((0))";
                colvarTeamManager.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTeamManager);

                TableSchema.TableColumn colvarCrossDeptTeam = new TableSchema.TableColumn(schema);
                colvarCrossDeptTeam.ColumnName = "cross_dept_team";
                colvarCrossDeptTeam.DataType = DbType.AnsiString;
                colvarCrossDeptTeam.MaxLength = 100;
                colvarCrossDeptTeam.AutoIncrement = false;
                colvarCrossDeptTeam.IsNullable = true;
                colvarCrossDeptTeam.IsPrimaryKey = false;
                colvarCrossDeptTeam.IsForeignKey = false;
                colvarCrossDeptTeam.IsReadOnly = false;
                colvarCrossDeptTeam.DefaultSetting = @"";
                colvarCrossDeptTeam.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCrossDeptTeam);

                TableSchema.TableColumn colvarIsGrpPerformance = new TableSchema.TableColumn(schema);
                colvarIsGrpPerformance.ColumnName = "is_grp_performance";
                colvarIsGrpPerformance.DataType = DbType.Int32;
                colvarIsGrpPerformance.MaxLength = 0;
                colvarIsGrpPerformance.AutoIncrement = false;
                colvarIsGrpPerformance.IsNullable = false;
                colvarIsGrpPerformance.IsPrimaryKey = false;
                colvarIsGrpPerformance.IsForeignKey = false;
                colvarIsGrpPerformance.IsReadOnly = false;
                colvarIsGrpPerformance.DefaultSetting = @"";
                colvarIsGrpPerformance.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsGrpPerformance);

                TableSchema.TableColumn colvarIsOfficial = new TableSchema.TableColumn(schema);
                colvarIsOfficial.ColumnName = "is_official";
                colvarIsOfficial.DataType = DbType.Int32;
                colvarIsOfficial.MaxLength = 0;
                colvarIsOfficial.AutoIncrement = false;
                colvarIsOfficial.IsNullable = false;
                colvarIsOfficial.IsPrimaryKey = false;
                colvarIsOfficial.IsForeignKey = false;
                colvarIsOfficial.IsReadOnly = false;
                colvarIsOfficial.DefaultSetting = @"";
                colvarIsOfficial.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsOfficial);

                TableSchema.TableColumn colvarExtension = new TableSchema.TableColumn(schema);
                colvarExtension.ColumnName = "extension";
                colvarExtension.DataType = DbType.String;
                colvarExtension.MaxLength = 20;
                colvarExtension.AutoIncrement = false;
                colvarExtension.IsNullable = true;
                colvarExtension.IsPrimaryKey = false;
                colvarExtension.IsForeignKey = false;
                colvarExtension.IsReadOnly = false;
                colvarExtension.DefaultSetting = @"";
                colvarExtension.ForeignKeyTableName = "";
                schema.Columns.Add(colvarExtension);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("employee", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("EmpId")]
        [Bindable(true)]
        public string EmpId
        {
            get { return GetColumnValue<string>(Columns.EmpId); }
            set { SetColumnValue(Columns.EmpId, value); }
        }

        [XmlAttribute("EmpName")]
        [Bindable(true)]
        public string EmpName
        {
            get { return GetColumnValue<string>(Columns.EmpName); }
            set { SetColumnValue(Columns.EmpName, value); }
        }

        [XmlAttribute("DeptId")]
        [Bindable(true)]
        public string DeptId
        {
            get { return GetColumnValue<string>(Columns.DeptId); }
            set { SetColumnValue(Columns.DeptId, value); }
        }

        [XmlAttribute("AvailableDate")]
        [Bindable(true)]
        public DateTime? AvailableDate
        {
            get { return GetColumnValue<DateTime?>(Columns.AvailableDate); }
            set { SetColumnValue(Columns.AvailableDate, value); }
        }

        [XmlAttribute("DepartureDate")]
        [Bindable(true)]
        public DateTime? DepartureDate
        {
            get { return GetColumnValue<DateTime?>(Columns.DepartureDate); }
            set { SetColumnValue(Columns.DepartureDate, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile
        {
            get { return GetColumnValue<string>(Columns.Mobile); }
            set { SetColumnValue(Columns.Mobile, value); }
        }

        [XmlAttribute("IsInvisible")]
        [Bindable(true)]
        public bool IsInvisible
        {
            get { return GetColumnValue<bool>(Columns.IsInvisible); }
            set { SetColumnValue(Columns.IsInvisible, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get { return GetColumnValue<int>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("EmpLevel")]
        [Bindable(true)]
        public int? EmpLevel
        {
            get { return GetColumnValue<int?>(Columns.EmpLevel); }
            set { SetColumnValue(Columns.EmpLevel, value); }
        }

        [XmlAttribute("EmpNo")]
        [Bindable(true)]
        public string EmpNo
        {
            get { return GetColumnValue<string>(Columns.EmpNo); }
            set { SetColumnValue(Columns.EmpNo, value); }
        }

        [XmlAttribute("DeptManager")]
        [Bindable(true)]
        public string DeptManager
        {
            get { return GetColumnValue<string>(Columns.DeptManager); }
            set { SetColumnValue(Columns.DeptManager, value); }
        }

        [XmlAttribute("TeamNo")]
        [Bindable(true)]
        public int? TeamNo
        {
            get { return GetColumnValue<int?>(Columns.TeamNo); }
            set { SetColumnValue(Columns.TeamNo, value); }
        }

        [XmlAttribute("TeamManager")]
        [Bindable(true)]
        public bool TeamManager
        {
            get { return GetColumnValue<bool>(Columns.TeamManager); }
            set { SetColumnValue(Columns.TeamManager, value); }
        }

        [XmlAttribute("CrossDeptTeam")]
        [Bindable(true)]
        public string CrossDeptTeam
        {
            get { return GetColumnValue<string>(Columns.CrossDeptTeam); }
            set { SetColumnValue(Columns.CrossDeptTeam, value); }
        }

        [XmlAttribute("IsGrpPerformance")]
        [Bindable(true)]
        public int IsGrpPerformance
        {
            get { return GetColumnValue<int>(Columns.IsGrpPerformance); }
            set { SetColumnValue(Columns.IsGrpPerformance, value); }
        }

        [XmlAttribute("IsOfficial")]
        [Bindable(true)]
        public int IsOfficial
        {
            get { return GetColumnValue<int>(Columns.IsOfficial); }
            set { SetColumnValue(Columns.IsOfficial, value); }
        }

        [XmlAttribute("Extension")]
        [Bindable(true)]
        public string Extension
        {
            get { return GetColumnValue<string>(Columns.Extension); }
            set { SetColumnValue(Columns.Extension, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn EmpIdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn EmpNameColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn DeptIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn AvailableDateColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn DepartureDateColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn MobileColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn IsInvisibleColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn EmpLevelColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn EmpNoColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn DeptManagerColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn TeamNoColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn TeamManagerColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn CrossDeptTeamColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn IsGrpPerformanceColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn IsOfficialColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn ExtensionColumn
        {
            get { return Schema.Columns[20]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string EmpId = @"emp_id";
            public static string EmpName = @"emp_name";
            public static string DeptId = @"dept_id";
            public static string AvailableDate = @"available_date";
            public static string DepartureDate = @"departure_date";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string Mobile = @"mobile";
            public static string IsInvisible = @"is_invisible";
            public static string UserId = @"user_id";
            public static string EmpLevel = @"emp_level";
            public static string EmpNo = @"emp_no";
            public static string DeptManager = @"dept_manager";
            public static string TeamNo = @"team_no";
            public static string TeamManager = @"team_manager";
            public static string CrossDeptTeam = @"cross_dept_team";
            public static string IsGrpPerformance = @"is_grp_performance";
            public static string IsOfficial = @"is_official";
            public static string Extension = @"extension";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

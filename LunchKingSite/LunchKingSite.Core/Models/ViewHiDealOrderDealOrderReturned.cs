using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealOrderDealOrderReturned class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealOrderDealOrderReturnedCollection : ReadOnlyList<ViewHiDealOrderDealOrderReturned, ViewHiDealOrderDealOrderReturnedCollection>
    {        
        public ViewHiDealOrderDealOrderReturnedCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_order_deal_order_returned view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealOrderDealOrderReturned : ReadOnlyRecord<ViewHiDealOrderDealOrderReturned>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_order_deal_order_returned", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderPk = new TableSchema.TableColumn(schema);
                colvarOrderPk.ColumnName = "OrderPk";
                colvarOrderPk.DataType = DbType.Int32;
                colvarOrderPk.MaxLength = 0;
                colvarOrderPk.AutoIncrement = false;
                colvarOrderPk.IsNullable = false;
                colvarOrderPk.IsPrimaryKey = false;
                colvarOrderPk.IsForeignKey = false;
                colvarOrderPk.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderPk);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "OrderGuid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "OrderId";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = false;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "Amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "OrderStatus";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "CreateTime";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "DealId";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = true;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarOrderName = new TableSchema.TableColumn(schema);
                colvarOrderName.ColumnName = "OrderName";
                colvarOrderName.DataType = DbType.String;
                colvarOrderName.MaxLength = 100;
                colvarOrderName.AutoIncrement = false;
                colvarOrderName.IsNullable = false;
                colvarOrderName.IsPrimaryKey = false;
                colvarOrderName.IsForeignKey = false;
                colvarOrderName.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderName);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "UserName";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = true;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "SellerName";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "DealName";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 50;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "ProductName";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 250;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = true;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductName);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "DealStartTime";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "DealEndTime";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarReturnId = new TableSchema.TableColumn(schema);
                colvarReturnId.ColumnName = "ReturnId";
                colvarReturnId.DataType = DbType.Int32;
                colvarReturnId.MaxLength = 0;
                colvarReturnId.AutoIncrement = false;
                colvarReturnId.IsNullable = true;
                colvarReturnId.IsPrimaryKey = false;
                colvarReturnId.IsForeignKey = false;
                colvarReturnId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnId);
                
                TableSchema.TableColumn colvarReturnStatus = new TableSchema.TableColumn(schema);
                colvarReturnStatus.ColumnName = "ReturnStatus";
                colvarReturnStatus.DataType = DbType.Int32;
                colvarReturnStatus.MaxLength = 0;
                colvarReturnStatus.AutoIncrement = false;
                colvarReturnStatus.IsNullable = true;
                colvarReturnStatus.IsPrimaryKey = false;
                colvarReturnStatus.IsForeignKey = false;
                colvarReturnStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnStatus);
                
                TableSchema.TableColumn colvarReturnCreateTime = new TableSchema.TableColumn(schema);
                colvarReturnCreateTime.ColumnName = "ReturnCreateTime";
                colvarReturnCreateTime.DataType = DbType.DateTime;
                colvarReturnCreateTime.MaxLength = 0;
                colvarReturnCreateTime.AutoIncrement = false;
                colvarReturnCreateTime.IsNullable = true;
                colvarReturnCreateTime.IsPrimaryKey = false;
                colvarReturnCreateTime.IsForeignKey = false;
                colvarReturnCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnCreateTime);
                
                TableSchema.TableColumn colvarReturnCompleteTime = new TableSchema.TableColumn(schema);
                colvarReturnCompleteTime.ColumnName = "ReturnCompleteTime";
                colvarReturnCompleteTime.DataType = DbType.DateTime;
                colvarReturnCompleteTime.MaxLength = 0;
                colvarReturnCompleteTime.AutoIncrement = false;
                colvarReturnCompleteTime.IsNullable = true;
                colvarReturnCompleteTime.IsPrimaryKey = false;
                colvarReturnCompleteTime.IsForeignKey = false;
                colvarReturnCompleteTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnCompleteTime);
                
                TableSchema.TableColumn colvarReturnCancelTime = new TableSchema.TableColumn(schema);
                colvarReturnCancelTime.ColumnName = "ReturnCancelTime";
                colvarReturnCancelTime.DataType = DbType.DateTime;
                colvarReturnCancelTime.MaxLength = 0;
                colvarReturnCancelTime.AutoIncrement = false;
                colvarReturnCancelTime.IsNullable = true;
                colvarReturnCancelTime.IsPrimaryKey = false;
                colvarReturnCancelTime.IsForeignKey = false;
                colvarReturnCancelTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnCancelTime);
                
                TableSchema.TableColumn colvarCancelReason = new TableSchema.TableColumn(schema);
                colvarCancelReason.ColumnName = "cancel_reason";
                colvarCancelReason.DataType = DbType.String;
                colvarCancelReason.MaxLength = -1;
                colvarCancelReason.AutoIncrement = false;
                colvarCancelReason.IsNullable = true;
                colvarCancelReason.IsPrimaryKey = false;
                colvarCancelReason.IsForeignKey = false;
                colvarCancelReason.IsReadOnly = false;
                
                schema.Columns.Add(colvarCancelReason);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = true;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_order_deal_order_returned",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealOrderDealOrderReturned()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealOrderDealOrderReturned(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealOrderDealOrderReturned(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealOrderDealOrderReturned(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderPk")]
        [Bindable(true)]
        public int OrderPk 
	    {
		    get
		    {
			    return GetColumnValue<int>("OrderPk");
		    }
            set 
		    {
			    SetColumnValue("OrderPk", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("OrderGuid");
		    }
            set 
		    {
			    SetColumnValue("OrderGuid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("OrderId");
		    }
            set 
		    {
			    SetColumnValue("OrderId", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal Amount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("Amount");
		    }
            set 
		    {
			    SetColumnValue("Amount", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("OrderStatus");
		    }
            set 
		    {
			    SetColumnValue("OrderStatus", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("CreateTime");
		    }
            set 
		    {
			    SetColumnValue("CreateTime", value);
            }
        }
	      
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int? DealId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("DealId");
		    }
            set 
		    {
			    SetColumnValue("DealId", value);
            }
        }
	      
        [XmlAttribute("OrderName")]
        [Bindable(true)]
        public string OrderName 
	    {
		    get
		    {
			    return GetColumnValue<string>("OrderName");
		    }
            set 
		    {
			    SetColumnValue("OrderName", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("UserName");
		    }
            set 
		    {
			    SetColumnValue("UserName", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid? SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("SellerName");
		    }
            set 
		    {
			    SetColumnValue("SellerName", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("DealName");
		    }
            set 
		    {
			    SetColumnValue("DealName", value);
            }
        }
	      
        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName 
	    {
		    get
		    {
			    return GetColumnValue<string>("ProductName");
		    }
            set 
		    {
			    SetColumnValue("ProductName", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("DealStartTime");
		    }
            set 
		    {
			    SetColumnValue("DealStartTime", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("DealEndTime");
		    }
            set 
		    {
			    SetColumnValue("DealEndTime", value);
            }
        }
	      
        [XmlAttribute("ReturnId")]
        [Bindable(true)]
        public int? ReturnId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ReturnId");
		    }
            set 
		    {
			    SetColumnValue("ReturnId", value);
            }
        }
	      
        [XmlAttribute("ReturnStatus")]
        [Bindable(true)]
        public int? ReturnStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ReturnStatus");
		    }
            set 
		    {
			    SetColumnValue("ReturnStatus", value);
            }
        }
	      
        [XmlAttribute("ReturnCreateTime")]
        [Bindable(true)]
        public DateTime? ReturnCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ReturnCreateTime");
		    }
            set 
		    {
			    SetColumnValue("ReturnCreateTime", value);
            }
        }
	      
        [XmlAttribute("ReturnCompleteTime")]
        [Bindable(true)]
        public DateTime? ReturnCompleteTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ReturnCompleteTime");
		    }
            set 
		    {
			    SetColumnValue("ReturnCompleteTime", value);
            }
        }
	      
        [XmlAttribute("ReturnCancelTime")]
        [Bindable(true)]
        public DateTime? ReturnCancelTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ReturnCancelTime");
		    }
            set 
		    {
			    SetColumnValue("ReturnCancelTime", value);
            }
        }
	      
        [XmlAttribute("CancelReason")]
        [Bindable(true)]
        public string CancelReason 
	    {
		    get
		    {
			    return GetColumnValue<string>("cancel_reason");
		    }
            set 
		    {
			    SetColumnValue("cancel_reason", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int? ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderPk = @"OrderPk";
            
            public static string OrderGuid = @"OrderGuid";
            
            public static string OrderId = @"OrderId";
            
            public static string Amount = @"Amount";
            
            public static string OrderStatus = @"OrderStatus";
            
            public static string CreateTime = @"CreateTime";
            
            public static string DealId = @"DealId";
            
            public static string OrderName = @"OrderName";
            
            public static string UserName = @"UserName";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string SellerName = @"SellerName";
            
            public static string DealName = @"DealName";
            
            public static string ProductName = @"ProductName";
            
            public static string DealStartTime = @"DealStartTime";
            
            public static string DealEndTime = @"DealEndTime";
            
            public static string ReturnId = @"ReturnId";
            
            public static string ReturnStatus = @"ReturnStatus";
            
            public static string ReturnCreateTime = @"ReturnCreateTime";
            
            public static string ReturnCompleteTime = @"ReturnCompleteTime";
            
            public static string ReturnCancelTime = @"ReturnCancelTime";
            
            public static string CancelReason = @"cancel_reason";
            
            public static string ProductId = @"product_id";
            
            public static string StoreGuid = @"store_guid";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

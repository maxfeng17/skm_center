using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEventDealPromo class.
    /// </summary>
    [Serializable]
    public partial class ViewEventDealPromoCollection : ReadOnlyList<ViewEventDealPromo, ViewEventDealPromoCollection>
    {        
        public ViewEventDealPromoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_event_deal_promo view.
    /// </summary>
    [Serializable]
    public partial class ViewEventDealPromo : ReadOnlyRecord<ViewEventDealPromo>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_event_deal_promo", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = false;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarDealPromoTitle = new TableSchema.TableColumn(schema);
                colvarDealPromoTitle.ColumnName = "deal_promo_title";
                colvarDealPromoTitle.DataType = DbType.String;
                colvarDealPromoTitle.MaxLength = 50;
                colvarDealPromoTitle.AutoIncrement = false;
                colvarDealPromoTitle.IsNullable = true;
                colvarDealPromoTitle.IsPrimaryKey = false;
                colvarDealPromoTitle.IsForeignKey = false;
                colvarDealPromoTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealPromoTitle);
                
                TableSchema.TableColumn colvarDealPromoDescription = new TableSchema.TableColumn(schema);
                colvarDealPromoDescription.ColumnName = "deal_promo_description";
                colvarDealPromoDescription.DataType = DbType.String;
                colvarDealPromoDescription.MaxLength = -1;
                colvarDealPromoDescription.AutoIncrement = false;
                colvarDealPromoDescription.IsNullable = true;
                colvarDealPromoDescription.IsPrimaryKey = false;
                colvarDealPromoDescription.IsForeignKey = false;
                colvarDealPromoDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealPromoDescription);
                
                TableSchema.TableColumn colvarDealPromoImage = new TableSchema.TableColumn(schema);
                colvarDealPromoImage.ColumnName = "deal_promo_image";
                colvarDealPromoImage.DataType = DbType.String;
                colvarDealPromoImage.MaxLength = 120;
                colvarDealPromoImage.AutoIncrement = false;
                colvarDealPromoImage.IsNullable = true;
                colvarDealPromoImage.IsPrimaryKey = false;
                colvarDealPromoImage.IsForeignKey = false;
                colvarDealPromoImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealPromoImage);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_event_deal_promo",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewEventDealPromo()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEventDealPromo(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewEventDealPromo(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewEventDealPromo(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int ItemId 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("DealPromoTitle")]
        [Bindable(true)]
        public string DealPromoTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_promo_title");
		    }
            set 
		    {
			    SetColumnValue("deal_promo_title", value);
            }
        }
	      
        [XmlAttribute("DealPromoDescription")]
        [Bindable(true)]
        public string DealPromoDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_promo_description");
		    }
            set 
		    {
			    SetColumnValue("deal_promo_description", value);
            }
        }
	      
        [XmlAttribute("DealPromoImage")]
        [Bindable(true)]
        public string DealPromoImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_promo_image");
		    }
            set 
		    {
			    SetColumnValue("deal_promo_image", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ItemId = @"item_id";
            
            public static string DealPromoTitle = @"deal_promo_title";
            
            public static string DealPromoDescription = @"deal_promo_description";
            
            public static string DealPromoImage = @"deal_promo_image";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

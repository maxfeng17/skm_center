using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    [Serializable]
    public partial class HitrustRefundRecordCollection : RepositoryList<HitrustRefundRecord, HitrustRefundRecordCollection>
    {
        public HitrustRefundRecordCollection() {}

        public HitrustRefundRecordCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HitrustRefundRecord o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if(pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch(w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if(!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if(remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }

    [Serializable]
    public partial class HitrustRefundRecord : RepositoryRecord<HitrustRefundRecord>, IRecordBase
    {
        #region .ctors and Default Settings
        public HitrustRefundRecord()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public HitrustRefundRecord(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("hitrust_refund_record", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarTransId = new TableSchema.TableColumn(schema);
                colvarTransId.ColumnName = "trans_id";
                colvarTransId.DataType = DbType.AnsiString;
                colvarTransId.MaxLength = 40;
                colvarTransId.AutoIncrement = false;
                colvarTransId.IsNullable = false;
                colvarTransId.IsPrimaryKey = false;
                colvarTransId.IsForeignKey = false;
                colvarTransId.IsReadOnly = false;
                colvarTransId.DefaultSetting = @"";
                colvarTransId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTransId);

                TableSchema.TableColumn colvarTheDate = new TableSchema.TableColumn(schema);
                colvarTheDate.ColumnName = "the_date";
                colvarTheDate.DataType = DbType.DateTime;
                colvarTheDate.MaxLength = 0;
                colvarTheDate.AutoIncrement = false;
                colvarTheDate.IsNullable = false;
                colvarTheDate.IsPrimaryKey = false;
                colvarTheDate.IsForeignKey = false;
                colvarTheDate.IsReadOnly = false;
                colvarTheDate.DefaultSetting = @"";
                colvarTheDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTheDate);

                TableSchema.TableColumn colvarMerchantId = new TableSchema.TableColumn(schema);
                colvarMerchantId.ColumnName = "merchant_id";
                colvarMerchantId.DataType = DbType.String;
                colvarMerchantId.MaxLength = 50;
                colvarMerchantId.AutoIncrement = false;
                colvarMerchantId.IsNullable = false;
                colvarMerchantId.IsPrimaryKey = false;
                colvarMerchantId.IsForeignKey = false;
                colvarMerchantId.IsReadOnly = false;
                colvarMerchantId.DefaultSetting = @"";
                colvarMerchantId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMerchantId);

                TableSchema.TableColumn colvarCardNumber = new TableSchema.TableColumn(schema);
                colvarCardNumber.ColumnName = "card_number";
                colvarCardNumber.DataType = DbType.String;
                colvarCardNumber.MaxLength = 20;
                colvarCardNumber.AutoIncrement = false;
                colvarCardNumber.IsNullable = false;
                colvarCardNumber.IsPrimaryKey = false;
                colvarCardNumber.IsForeignKey = false;
                colvarCardNumber.IsReadOnly = false;
                colvarCardNumber.DefaultSetting = @"";
                colvarCardNumber.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardNumber);

                TableSchema.TableColumn colvarTransTime = new TableSchema.TableColumn(schema);
                colvarTransTime.ColumnName = "trans_time";
                colvarTransTime.DataType = DbType.DateTime;
                colvarTransTime.MaxLength = 0;
                colvarTransTime.AutoIncrement = false;
                colvarTransTime.IsNullable = false;
                colvarTransTime.IsPrimaryKey = false;
                colvarTransTime.IsForeignKey = false;
                colvarTransTime.IsReadOnly = false;
                colvarTransTime.DefaultSetting = @"";
                colvarTransTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTransTime);

                TableSchema.TableColumn colvarTransAmount = new TableSchema.TableColumn(schema);
                colvarTransAmount.ColumnName = "trans_amount";
                colvarTransAmount.DataType = DbType.Decimal;
                colvarTransAmount.MaxLength = 0;
                colvarTransAmount.AutoIncrement = false;
                colvarTransAmount.IsNullable = false;
                colvarTransAmount.IsPrimaryKey = false;
                colvarTransAmount.IsForeignKey = false;
                colvarTransAmount.IsReadOnly = false;
                colvarTransAmount.DefaultSetting = @"";
                colvarTransAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTransAmount);

                TableSchema.TableColumn colvarRefundAmount = new TableSchema.TableColumn(schema);
                colvarRefundAmount.ColumnName = "refund_amount";
                colvarRefundAmount.DataType = DbType.Decimal;
                colvarRefundAmount.MaxLength = 0;
                colvarRefundAmount.AutoIncrement = false;
                colvarRefundAmount.IsNullable = false;
                colvarRefundAmount.IsPrimaryKey = false;
                colvarRefundAmount.IsForeignKey = false;
                colvarRefundAmount.IsReadOnly = false;
                colvarRefundAmount.DefaultSetting = @"";
                colvarRefundAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRefundAmount);

                TableSchema.TableColumn colvarRemainAmount = new TableSchema.TableColumn(schema);
                colvarRemainAmount.ColumnName = "remain_amount";
                colvarRemainAmount.DataType = DbType.Decimal;
                colvarRemainAmount.MaxLength = 0;
                colvarRemainAmount.AutoIncrement = false;
                colvarRemainAmount.IsNullable = false;
                colvarRemainAmount.IsPrimaryKey = false;
                colvarRemainAmount.IsForeignKey = false;
                colvarRemainAmount.IsReadOnly = false;
                colvarRemainAmount.DefaultSetting = @"";
                colvarRemainAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRemainAmount);

                TableSchema.TableColumn colvarAuthCode = new TableSchema.TableColumn(schema);
                colvarAuthCode.ColumnName = "auth_code";
                colvarAuthCode.DataType = DbType.AnsiString;
                colvarAuthCode.MaxLength = 20;
                colvarAuthCode.AutoIncrement = false;
                colvarAuthCode.IsNullable = false;
                colvarAuthCode.IsPrimaryKey = false;
                colvarAuthCode.IsForeignKey = false;
                colvarAuthCode.IsReadOnly = false;
                colvarAuthCode.DefaultSetting = @"";
                colvarAuthCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAuthCode);

                TableSchema.TableColumn colvarInstallment = new TableSchema.TableColumn(schema);
                colvarInstallment.ColumnName = "installment";
                colvarInstallment.DataType = DbType.Int32;
                colvarInstallment.MaxLength = 0;
                colvarInstallment.AutoIncrement = false;
                colvarInstallment.IsNullable = false;
                colvarInstallment.IsPrimaryKey = false;
                colvarInstallment.IsForeignKey = false;
                colvarInstallment.IsReadOnly = false;
                colvarInstallment.DefaultSetting = @"";
                colvarInstallment.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInstallment);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarApiProvider = new TableSchema.TableColumn(schema);
                colvarApiProvider.ColumnName = "api_provider";
                colvarApiProvider.DataType = DbType.Int32;
                colvarApiProvider.MaxLength = 0;
                colvarApiProvider.AutoIncrement = false;
                colvarApiProvider.IsNullable = false;
                colvarApiProvider.IsPrimaryKey = false;
                colvarApiProvider.IsForeignKey = false;
                colvarApiProvider.IsReadOnly = false;
                colvarApiProvider.DefaultSetting = @"";
                colvarApiProvider.ForeignKeyTableName = "";
                schema.Columns.Add(colvarApiProvider);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("hitrust_refund_record",schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("TransId")]
        [Bindable(true)]
        public string TransId
        {
            get { return GetColumnValue<string>(Columns.TransId); }
            set { SetColumnValue(Columns.TransId, value); }
        }

        [XmlAttribute("TheDate")]
        [Bindable(true)]
        public DateTime TheDate
        {
            get { return GetColumnValue<DateTime>(Columns.TheDate); }
            set { SetColumnValue(Columns.TheDate, value); }
        }

        [XmlAttribute("MerchantId")]
        [Bindable(true)]
        public string MerchantId
        {
            get { return GetColumnValue<string>(Columns.MerchantId); }
            set { SetColumnValue(Columns.MerchantId, value); }
        }

        [XmlAttribute("CardNumber")]
        [Bindable(true)]
        public string CardNumber
        {
            get { return GetColumnValue<string>(Columns.CardNumber); }
            set { SetColumnValue(Columns.CardNumber, value); }
        }

        [XmlAttribute("TransTime")]
        [Bindable(true)]
        public DateTime TransTime
        {
            get { return GetColumnValue<DateTime>(Columns.TransTime); }
            set { SetColumnValue(Columns.TransTime, value); }
        }

        [XmlAttribute("TransAmount")]
        [Bindable(true)]
        public decimal TransAmount
        {
            get { return GetColumnValue<decimal>(Columns.TransAmount); }
            set { SetColumnValue(Columns.TransAmount, value); }
        }

        [XmlAttribute("RefundAmount")]
        [Bindable(true)]
        public decimal RefundAmount
        {
            get { return GetColumnValue<decimal>(Columns.RefundAmount); }
            set { SetColumnValue(Columns.RefundAmount, value); }
        }

        [XmlAttribute("RemainAmount")]
        [Bindable(true)]
        public decimal RemainAmount
        {
            get { return GetColumnValue<decimal>(Columns.RemainAmount); }
            set { SetColumnValue(Columns.RemainAmount, value); }
        }

        [XmlAttribute("AuthCode")]
        [Bindable(true)]
        public string AuthCode
        {
            get { return GetColumnValue<string>(Columns.AuthCode); }
            set { SetColumnValue(Columns.AuthCode, value); }
        }

        [XmlAttribute("Installment")]
        [Bindable(true)]
        public int Installment
        {
            get { return GetColumnValue<int>(Columns.Installment); }
            set { SetColumnValue(Columns.Installment, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ApiProvider")]
        [Bindable(true)]
        public int ApiProvider
        {
            get { return GetColumnValue<int>(Columns.ApiProvider); }
            set { SetColumnValue(Columns.ApiProvider, value); }
        }

        #endregion

        #region Typed Columns

        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }

        public static TableSchema.TableColumn TransIdColumn
        {
            get { return Schema.Columns[1]; }
        }

        public static TableSchema.TableColumn TheDateColumn
        {
            get { return Schema.Columns[2]; }
        }

        public static TableSchema.TableColumn MerchantIdColumn
        {
            get { return Schema.Columns[3]; }
        }

        public static TableSchema.TableColumn CardNumberColumn
        {
            get { return Schema.Columns[4]; }
        }

        public static TableSchema.TableColumn TransTimeColumn
        {
            get { return Schema.Columns[5]; }
        }

        public static TableSchema.TableColumn TransAmountColumn
        {
            get { return Schema.Columns[6]; }
        }

        public static TableSchema.TableColumn RefundAmountColumn
        {
            get { return Schema.Columns[7]; }
        }

        public static TableSchema.TableColumn RemainAmountColumn
        {
            get { return Schema.Columns[8]; }
        }

        public static TableSchema.TableColumn AuthCodeColumn
        {
            get { return Schema.Columns[9]; }
        }

        public static TableSchema.TableColumn InstallmentColumn
        {
            get { return Schema.Columns[10]; }
        }

        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }

        public static TableSchema.TableColumn ApiProviderColumn
        {
            get { return Schema.Columns[12]; }
        }

        #endregion

        #region Columns Struct

        public struct Columns
        {
            public static string Id = @"id";
            public static string TransId = @"trans_id";
            public static string TheDate = @"the_date";
            public static string MerchantId = @"merchant_id";
            public static string CardNumber = @"card_number";
            public static string TransTime = @"trans_time";
            public static string TransAmount = @"trans_amount";
            public static string RefundAmount = @"refund_amount";
            public static string RemainAmount = @"remain_amount";
            public static string AuthCode = @"auth_code";
            public static string Installment = @"installment";
            public static string CreateTime = @"create_time";
            public static string ApiProvider = @"api_provider";

        }

        #endregion

    }
}

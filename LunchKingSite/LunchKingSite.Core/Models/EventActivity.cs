using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class EventActivityCollection : RepositoryList<EventActivity, EventActivityCollection>
	{
			public EventActivityCollection() {}

			public EventActivityCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							EventActivity o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class EventActivity : RepositoryRecord<EventActivity>, IRecordBase
	{
		#region .ctors and Default Settings
		public EventActivity()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public EventActivity(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("event_activity", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarEventname = new TableSchema.TableColumn(schema);
				colvarEventname.ColumnName = "eventname";
				colvarEventname.DataType = DbType.String;
				colvarEventname.MaxLength = 100;
				colvarEventname.AutoIncrement = false;
				colvarEventname.IsNullable = false;
				colvarEventname.IsPrimaryKey = false;
				colvarEventname.IsForeignKey = false;
				colvarEventname.IsReadOnly = false;
				colvarEventname.DefaultSetting = @"";
				colvarEventname.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventname);

				TableSchema.TableColumn colvarDescription1 = new TableSchema.TableColumn(schema);
				colvarDescription1.ColumnName = "description_1";
				colvarDescription1.DataType = DbType.String;
				colvarDescription1.MaxLength = 2147483647;
				colvarDescription1.AutoIncrement = false;
				colvarDescription1.IsNullable = true;
				colvarDescription1.IsPrimaryKey = false;
				colvarDescription1.IsForeignKey = false;
				colvarDescription1.IsReadOnly = false;
				colvarDescription1.DefaultSetting = @"";
				colvarDescription1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription1);

				TableSchema.TableColumn colvarDescription2 = new TableSchema.TableColumn(schema);
				colvarDescription2.ColumnName = "description_2";
				colvarDescription2.DataType = DbType.String;
				colvarDescription2.MaxLength = 2147483647;
				colvarDescription2.AutoIncrement = false;
				colvarDescription2.IsNullable = true;
				colvarDescription2.IsPrimaryKey = false;
				colvarDescription2.IsForeignKey = false;
				colvarDescription2.IsReadOnly = false;
				colvarDescription2.DefaultSetting = @"";
				colvarDescription2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription2);

				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);

				TableSchema.TableColumn colvarMode = new TableSchema.TableColumn(schema);
				colvarMode.ColumnName = "mode";
				colvarMode.DataType = DbType.Int32;
				colvarMode.MaxLength = 0;
				colvarMode.AutoIncrement = false;
				colvarMode.IsNullable = false;
				colvarMode.IsPrimaryKey = false;
				colvarMode.IsForeignKey = false;
				colvarMode.IsReadOnly = false;
				colvarMode.DefaultSetting = @"";
				colvarMode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMode);

				TableSchema.TableColumn colvarTotalMax = new TableSchema.TableColumn(schema);
				colvarTotalMax.ColumnName = "total_max";
				colvarTotalMax.DataType = DbType.Int32;
				colvarTotalMax.MaxLength = 0;
				colvarTotalMax.AutoIncrement = false;
				colvarTotalMax.IsNullable = true;
				colvarTotalMax.IsPrimaryKey = false;
				colvarTotalMax.IsForeignKey = false;
				colvarTotalMax.IsReadOnly = false;
				colvarTotalMax.DefaultSetting = @"((0))";
				colvarTotalMax.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalMax);

				TableSchema.TableColumn colvarTotalCurrent = new TableSchema.TableColumn(schema);
				colvarTotalCurrent.ColumnName = "total_current";
				colvarTotalCurrent.DataType = DbType.Int32;
				colvarTotalCurrent.MaxLength = 0;
				colvarTotalCurrent.AutoIncrement = false;
				colvarTotalCurrent.IsNullable = true;
				colvarTotalCurrent.IsPrimaryKey = false;
				colvarTotalCurrent.IsForeignKey = false;
				colvarTotalCurrent.IsReadOnly = false;
				colvarTotalCurrent.DefaultSetting = @"((0))";
				colvarTotalCurrent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalCurrent);

				TableSchema.TableColumn colvarWinningMax = new TableSchema.TableColumn(schema);
				colvarWinningMax.ColumnName = "winning_max";
				colvarWinningMax.DataType = DbType.Int32;
				colvarWinningMax.MaxLength = 0;
				colvarWinningMax.AutoIncrement = false;
				colvarWinningMax.IsNullable = true;
				colvarWinningMax.IsPrimaryKey = false;
				colvarWinningMax.IsForeignKey = false;
				colvarWinningMax.IsReadOnly = false;
				colvarWinningMax.DefaultSetting = @"((0))";
				colvarWinningMax.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWinningMax);

				TableSchema.TableColumn colvarWinningCurrent = new TableSchema.TableColumn(schema);
				colvarWinningCurrent.ColumnName = "winning_current";
				colvarWinningCurrent.DataType = DbType.Int32;
				colvarWinningCurrent.MaxLength = 0;
				colvarWinningCurrent.AutoIncrement = false;
				colvarWinningCurrent.IsNullable = true;
				colvarWinningCurrent.IsPrimaryKey = false;
				colvarWinningCurrent.IsForeignKey = false;
				colvarWinningCurrent.IsReadOnly = false;
				colvarWinningCurrent.DefaultSetting = @"((0))";
				colvarWinningCurrent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWinningCurrent);

				TableSchema.TableColumn colvarIsImmediate = new TableSchema.TableColumn(schema);
				colvarIsImmediate.ColumnName = "is_immediate";
				colvarIsImmediate.DataType = DbType.Boolean;
				colvarIsImmediate.MaxLength = 0;
				colvarIsImmediate.AutoIncrement = false;
				colvarIsImmediate.IsNullable = true;
				colvarIsImmediate.IsPrimaryKey = false;
				colvarIsImmediate.IsForeignKey = false;
				colvarIsImmediate.IsReadOnly = false;
				colvarIsImmediate.DefaultSetting = @"((0))";
				colvarIsImmediate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsImmediate);

				TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
				colvarStartDate.ColumnName = "startDate";
				colvarStartDate.DataType = DbType.DateTime;
				colvarStartDate.MaxLength = 0;
				colvarStartDate.AutoIncrement = false;
				colvarStartDate.IsNullable = false;
				colvarStartDate.IsPrimaryKey = false;
				colvarStartDate.IsForeignKey = false;
				colvarStartDate.IsReadOnly = false;
				colvarStartDate.DefaultSetting = @"";
				colvarStartDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartDate);

				TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
				colvarEndDate.ColumnName = "endDate";
				colvarEndDate.DataType = DbType.DateTime;
				colvarEndDate.MaxLength = 0;
				colvarEndDate.AutoIncrement = false;
				colvarEndDate.IsNullable = false;
				colvarEndDate.IsPrimaryKey = false;
				colvarEndDate.IsForeignKey = false;
				colvarEndDate.IsReadOnly = false;
				colvarEndDate.DefaultSetting = @"";
				colvarEndDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndDate);

				TableSchema.TableColumn colvarFbPicture = new TableSchema.TableColumn(schema);
				colvarFbPicture.ColumnName = "fb_picture";
				colvarFbPicture.DataType = DbType.AnsiString;
				colvarFbPicture.MaxLength = 200;
				colvarFbPicture.AutoIncrement = false;
				colvarFbPicture.IsNullable = true;
				colvarFbPicture.IsPrimaryKey = false;
				colvarFbPicture.IsForeignKey = false;
				colvarFbPicture.IsReadOnly = false;
				colvarFbPicture.DefaultSetting = @"";
				colvarFbPicture.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFbPicture);

				TableSchema.TableColumn colvarFbDesc = new TableSchema.TableColumn(schema);
				colvarFbDesc.ColumnName = "fb_desc";
				colvarFbDesc.DataType = DbType.String;
				colvarFbDesc.MaxLength = 150;
				colvarFbDesc.AutoIncrement = false;
				colvarFbDesc.IsNullable = true;
				colvarFbDesc.IsPrimaryKey = false;
				colvarFbDesc.IsForeignKey = false;
				colvarFbDesc.IsReadOnly = false;
				colvarFbDesc.DefaultSetting = @"";
				colvarFbDesc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFbDesc);

				TableSchema.TableColumn colvarCpa = new TableSchema.TableColumn(schema);
				colvarCpa.ColumnName = "cpa";
				colvarCpa.DataType = DbType.AnsiString;
				colvarCpa.MaxLength = 50;
				colvarCpa.AutoIncrement = false;
				colvarCpa.IsNullable = true;
				colvarCpa.IsPrimaryKey = false;
				colvarCpa.IsForeignKey = false;
				colvarCpa.IsReadOnly = false;
				colvarCpa.DefaultSetting = @"";
				colvarCpa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCpa);

				TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
				colvarUrl.ColumnName = "url";
				colvarUrl.DataType = DbType.AnsiString;
				colvarUrl.MaxLength = 250;
				colvarUrl.AutoIncrement = false;
				colvarUrl.IsNullable = true;
				colvarUrl.IsPrimaryKey = false;
				colvarUrl.IsForeignKey = false;
				colvarUrl.IsReadOnly = false;
				colvarUrl.DefaultSetting = @"((0))";
				colvarUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUrl);

				TableSchema.TableColumn colvarEdmDefaultCity = new TableSchema.TableColumn(schema);
				colvarEdmDefaultCity.ColumnName = "edm_default_city";
				colvarEdmDefaultCity.DataType = DbType.Int32;
				colvarEdmDefaultCity.MaxLength = 0;
				colvarEdmDefaultCity.AutoIncrement = false;
				colvarEdmDefaultCity.IsNullable = true;
				colvarEdmDefaultCity.IsPrimaryKey = false;
				colvarEdmDefaultCity.IsForeignKey = false;
				colvarEdmDefaultCity.IsReadOnly = false;
				colvarEdmDefaultCity.DefaultSetting = @"((477))";
				colvarEdmDefaultCity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEdmDefaultCity);

				TableSchema.TableColumn colvarExcludeCpaList = new TableSchema.TableColumn(schema);
				colvarExcludeCpaList.ColumnName = "exclude_cpa_list";
				colvarExcludeCpaList.DataType = DbType.AnsiString;
				colvarExcludeCpaList.MaxLength = 250;
				colvarExcludeCpaList.AutoIncrement = false;
				colvarExcludeCpaList.IsNullable = true;
				colvarExcludeCpaList.IsPrimaryKey = false;
				colvarExcludeCpaList.IsForeignKey = false;
				colvarExcludeCpaList.IsReadOnly = false;
				colvarExcludeCpaList.DefaultSetting = @"";
				colvarExcludeCpaList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExcludeCpaList);

				TableSchema.TableColumn colvarEdmBg1 = new TableSchema.TableColumn(schema);
				colvarEdmBg1.ColumnName = "edm_bg_1";
				colvarEdmBg1.DataType = DbType.AnsiString;
				colvarEdmBg1.MaxLength = 100;
				colvarEdmBg1.AutoIncrement = false;
				colvarEdmBg1.IsNullable = true;
				colvarEdmBg1.IsPrimaryKey = false;
				colvarEdmBg1.IsForeignKey = false;
				colvarEdmBg1.IsReadOnly = false;
				colvarEdmBg1.DefaultSetting = @"";
				colvarEdmBg1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEdmBg1);

				TableSchema.TableColumn colvarEdmBg2 = new TableSchema.TableColumn(schema);
				colvarEdmBg2.ColumnName = "edm_bg_2";
				colvarEdmBg2.DataType = DbType.AnsiString;
				colvarEdmBg2.MaxLength = 100;
				colvarEdmBg2.AutoIncrement = false;
				colvarEdmBg2.IsNullable = true;
				colvarEdmBg2.IsPrimaryKey = false;
				colvarEdmBg2.IsForeignKey = false;
				colvarEdmBg2.IsReadOnly = false;
				colvarEdmBg2.DefaultSetting = @"";
				colvarEdmBg2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEdmBg2);

				TableSchema.TableColumn colvarEdmCityColor = new TableSchema.TableColumn(schema);
				colvarEdmCityColor.ColumnName = "edm_city_color";
				colvarEdmCityColor.DataType = DbType.String;
				colvarEdmCityColor.MaxLength = 10;
				colvarEdmCityColor.AutoIncrement = false;
				colvarEdmCityColor.IsNullable = true;
				colvarEdmCityColor.IsPrimaryKey = false;
				colvarEdmCityColor.IsForeignKey = false;
				colvarEdmCityColor.IsReadOnly = false;
				colvarEdmCityColor.DefaultSetting = @"";
				colvarEdmCityColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEdmCityColor);

				TableSchema.TableColumn colvarCookieexpire1 = new TableSchema.TableColumn(schema);
				colvarCookieexpire1.ColumnName = "cookieexpire_1";
				colvarCookieexpire1.DataType = DbType.Int32;
				colvarCookieexpire1.MaxLength = 0;
				colvarCookieexpire1.AutoIncrement = false;
				colvarCookieexpire1.IsNullable = true;
				colvarCookieexpire1.IsPrimaryKey = false;
				colvarCookieexpire1.IsForeignKey = false;
				colvarCookieexpire1.IsReadOnly = false;
				colvarCookieexpire1.DefaultSetting = @"((0))";
				colvarCookieexpire1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCookieexpire1);

				TableSchema.TableColumn colvarCookieexpire2 = new TableSchema.TableColumn(schema);
				colvarCookieexpire2.ColumnName = "cookieexpire_2";
				colvarCookieexpire2.DataType = DbType.Int32;
				colvarCookieexpire2.MaxLength = 0;
				colvarCookieexpire2.AutoIncrement = false;
				colvarCookieexpire2.IsNullable = true;
				colvarCookieexpire2.IsPrimaryKey = false;
				colvarCookieexpire2.IsForeignKey = false;
				colvarCookieexpire2.IsReadOnly = false;
				colvarCookieexpire2.DefaultSetting = @"((0))";
				colvarCookieexpire2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCookieexpire2);

				TableSchema.TableColumn colvarCookieexpire3 = new TableSchema.TableColumn(schema);
				colvarCookieexpire3.ColumnName = "cookieexpire_3";
				colvarCookieexpire3.DataType = DbType.Int32;
				colvarCookieexpire3.MaxLength = 0;
				colvarCookieexpire3.AutoIncrement = false;
				colvarCookieexpire3.IsNullable = true;
				colvarCookieexpire3.IsPrimaryKey = false;
				colvarCookieexpire3.IsForeignKey = false;
				colvarCookieexpire3.IsReadOnly = false;
				colvarCookieexpire3.DefaultSetting = @"((0))";
				colvarCookieexpire3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCookieexpire3);

				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Boolean;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"((1))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);

				TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
				colvarCreator.ColumnName = "creator";
				colvarCreator.DataType = DbType.String;
				colvarCreator.MaxLength = 100;
				colvarCreator.AutoIncrement = false;
				colvarCreator.IsNullable = false;
				colvarCreator.IsPrimaryKey = false;
				colvarCreator.IsForeignKey = false;
				colvarCreator.IsReadOnly = false;
				colvarCreator.DefaultSetting = @"";
				colvarCreator.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreator);

				TableSchema.TableColumn colvarCdt = new TableSchema.TableColumn(schema);
				colvarCdt.ColumnName = "cdt";
				colvarCdt.DataType = DbType.DateTime;
				colvarCdt.MaxLength = 0;
				colvarCdt.AutoIncrement = false;
				colvarCdt.IsNullable = false;
				colvarCdt.IsPrimaryKey = false;
				colvarCdt.IsForeignKey = false;
				colvarCdt.IsReadOnly = false;
				colvarCdt.DefaultSetting = @"";
				colvarCdt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCdt);

				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 1073741823;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);

				TableSchema.TableColumn colvarDiscountCampaignId = new TableSchema.TableColumn(schema);
				colvarDiscountCampaignId.ColumnName = "discount_campaign_id";
				colvarDiscountCampaignId.DataType = DbType.Int32;
				colvarDiscountCampaignId.MaxLength = 0;
				colvarDiscountCampaignId.AutoIncrement = false;
				colvarDiscountCampaignId.IsNullable = true;
				colvarDiscountCampaignId.IsPrimaryKey = false;
				colvarDiscountCampaignId.IsForeignKey = false;
				colvarDiscountCampaignId.IsReadOnly = false;
				colvarDiscountCampaignId.DefaultSetting = @"((0))";
				colvarDiscountCampaignId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountCampaignId);

				TableSchema.TableColumn colvarPageName = new TableSchema.TableColumn(schema);
				colvarPageName.ColumnName = "page_name";
				colvarPageName.DataType = DbType.String;
				colvarPageName.MaxLength = 50;
				colvarPageName.AutoIncrement = false;
				colvarPageName.IsNullable = true;
				colvarPageName.IsPrimaryKey = false;
				colvarPageName.IsForeignKey = false;
				colvarPageName.IsReadOnly = false;
				colvarPageName.DefaultSetting = @"";
				colvarPageName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPageName);

				TableSchema.TableColumn colvarDiscountList = new TableSchema.TableColumn(schema);
				colvarDiscountList.ColumnName = "discount_list";
				colvarDiscountList.DataType = DbType.AnsiString;
				colvarDiscountList.MaxLength = 200;
				colvarDiscountList.AutoIncrement = false;
				colvarDiscountList.IsNullable = true;
				colvarDiscountList.IsPrimaryKey = false;
				colvarDiscountList.IsForeignKey = false;
				colvarDiscountList.IsReadOnly = false;
				colvarDiscountList.DefaultSetting = @"";
				colvarDiscountList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountList);

				TableSchema.TableColumn colvarAutoCloseSeconds = new TableSchema.TableColumn(schema);
				colvarAutoCloseSeconds.ColumnName = "auto_close_seconds";
				colvarAutoCloseSeconds.DataType = DbType.Int32;
				colvarAutoCloseSeconds.MaxLength = 0;
				colvarAutoCloseSeconds.AutoIncrement = false;
				colvarAutoCloseSeconds.IsNullable = true;
				colvarAutoCloseSeconds.IsPrimaryKey = false;
				colvarAutoCloseSeconds.IsForeignKey = false;
				colvarAutoCloseSeconds.IsReadOnly = false;
				colvarAutoCloseSeconds.DefaultSetting = @"";
				colvarAutoCloseSeconds.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAutoCloseSeconds);

				TableSchema.TableColumn colvarVersion = new TableSchema.TableColumn(schema);
				colvarVersion.ColumnName = "version";
				colvarVersion.DataType = DbType.Int32;
				colvarVersion.MaxLength = 0;
				colvarVersion.AutoIncrement = false;
				colvarVersion.IsNullable = true;
				colvarVersion.IsPrimaryKey = false;
				colvarVersion.IsForeignKey = false;
				colvarVersion.IsReadOnly = false;
				colvarVersion.DefaultSetting = @"";
				colvarVersion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVersion);

				TableSchema.TableColumn colvarWebLinkUrl = new TableSchema.TableColumn(schema);
				colvarWebLinkUrl.ColumnName = "web_link_url";
				colvarWebLinkUrl.DataType = DbType.String;
				colvarWebLinkUrl.MaxLength = 200;
				colvarWebLinkUrl.AutoIncrement = false;
				colvarWebLinkUrl.IsNullable = true;
				colvarWebLinkUrl.IsPrimaryKey = false;
				colvarWebLinkUrl.IsForeignKey = false;
				colvarWebLinkUrl.IsReadOnly = false;
				colvarWebLinkUrl.DefaultSetting = @"";
				colvarWebLinkUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWebLinkUrl);

				TableSchema.TableColumn colvarWebImage = new TableSchema.TableColumn(schema);
				colvarWebImage.ColumnName = "web_image";
				colvarWebImage.DataType = DbType.String;
				colvarWebImage.MaxLength = 200;
				colvarWebImage.AutoIncrement = false;
				colvarWebImage.IsNullable = true;
				colvarWebImage.IsPrimaryKey = false;
				colvarWebImage.IsForeignKey = false;
				colvarWebImage.IsReadOnly = false;
				colvarWebImage.DefaultSetting = @"";
				colvarWebImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWebImage);

				TableSchema.TableColumn colvarWebOpenTarget = new TableSchema.TableColumn(schema);
				colvarWebOpenTarget.ColumnName = "web_open_target";
				colvarWebOpenTarget.DataType = DbType.Int32;
				colvarWebOpenTarget.MaxLength = 0;
				colvarWebOpenTarget.AutoIncrement = false;
				colvarWebOpenTarget.IsNullable = true;
				colvarWebOpenTarget.IsPrimaryKey = false;
				colvarWebOpenTarget.IsForeignKey = false;
				colvarWebOpenTarget.IsReadOnly = false;
				colvarWebOpenTarget.DefaultSetting = @"";
				colvarWebOpenTarget.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWebOpenTarget);

				TableSchema.TableColumn colvarMobileLinkUrl = new TableSchema.TableColumn(schema);
				colvarMobileLinkUrl.ColumnName = "mobile_link_url";
				colvarMobileLinkUrl.DataType = DbType.String;
				colvarMobileLinkUrl.MaxLength = 200;
				colvarMobileLinkUrl.AutoIncrement = false;
				colvarMobileLinkUrl.IsNullable = true;
				colvarMobileLinkUrl.IsPrimaryKey = false;
				colvarMobileLinkUrl.IsForeignKey = false;
				colvarMobileLinkUrl.IsReadOnly = false;
				colvarMobileLinkUrl.DefaultSetting = @"";
				colvarMobileLinkUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileLinkUrl);

				TableSchema.TableColumn colvarMobileImage = new TableSchema.TableColumn(schema);
				colvarMobileImage.ColumnName = "mobile_image";
				colvarMobileImage.DataType = DbType.String;
				colvarMobileImage.MaxLength = 200;
				colvarMobileImage.AutoIncrement = false;
				colvarMobileImage.IsNullable = true;
				colvarMobileImage.IsPrimaryKey = false;
				colvarMobileImage.IsForeignKey = false;
				colvarMobileImage.IsReadOnly = false;
				colvarMobileImage.DefaultSetting = @"";
				colvarMobileImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileImage);

				TableSchema.TableColumn colvarMobileOpenTarget = new TableSchema.TableColumn(schema);
				colvarMobileOpenTarget.ColumnName = "mobile_open_target";
				colvarMobileOpenTarget.DataType = DbType.Int32;
				colvarMobileOpenTarget.MaxLength = 0;
				colvarMobileOpenTarget.AutoIncrement = false;
				colvarMobileOpenTarget.IsNullable = true;
				colvarMobileOpenTarget.IsPrimaryKey = false;
				colvarMobileOpenTarget.IsForeignKey = false;
				colvarMobileOpenTarget.IsReadOnly = false;
				colvarMobileOpenTarget.DefaultSetting = @"";
				colvarMobileOpenTarget.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileOpenTarget);

				TableSchema.TableColumn colvarAppLinkUrl = new TableSchema.TableColumn(schema);
				colvarAppLinkUrl.ColumnName = "app_link_url";
				colvarAppLinkUrl.DataType = DbType.String;
				colvarAppLinkUrl.MaxLength = 200;
				colvarAppLinkUrl.AutoIncrement = false;
				colvarAppLinkUrl.IsNullable = true;
				colvarAppLinkUrl.IsPrimaryKey = false;
				colvarAppLinkUrl.IsForeignKey = false;
				colvarAppLinkUrl.IsReadOnly = false;
				colvarAppLinkUrl.DefaultSetting = @"";
				colvarAppLinkUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppLinkUrl);

				TableSchema.TableColumn colvarAppImage = new TableSchema.TableColumn(schema);
				colvarAppImage.ColumnName = "app_image";
				colvarAppImage.DataType = DbType.String;
				colvarAppImage.MaxLength = 200;
				colvarAppImage.AutoIncrement = false;
				colvarAppImage.IsNullable = true;
				colvarAppImage.IsPrimaryKey = false;
				colvarAppImage.IsForeignKey = false;
				colvarAppImage.IsReadOnly = false;
				colvarAppImage.DefaultSetting = @"";
				colvarAppImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppImage);

				TableSchema.TableColumn colvarAppText = new TableSchema.TableColumn(schema);
				colvarAppText.ColumnName = "app_text";
				colvarAppText.DataType = DbType.String;
				colvarAppText.MaxLength = 200;
				colvarAppText.AutoIncrement = false;
				colvarAppText.IsNullable = true;
				colvarAppText.IsPrimaryKey = false;
				colvarAppText.IsForeignKey = false;
				colvarAppText.IsReadOnly = false;
				colvarAppText.DefaultSetting = @"";
				colvarAppText.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppText);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("event_activity",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("Eventname")]
		[Bindable(true)]
		public string Eventname
		{
			get { return GetColumnValue<string>(Columns.Eventname); }
			set { SetColumnValue(Columns.Eventname, value); }
		}

		[XmlAttribute("Description1")]
		[Bindable(true)]
		public string Description1
		{
			get { return GetColumnValue<string>(Columns.Description1); }
			set { SetColumnValue(Columns.Description1, value); }
		}

		[XmlAttribute("Description2")]
		[Bindable(true)]
		public string Description2
		{
			get { return GetColumnValue<string>(Columns.Description2); }
			set { SetColumnValue(Columns.Description2, value); }
		}

		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}

		[XmlAttribute("Mode")]
		[Bindable(true)]
		public int Mode
		{
			get { return GetColumnValue<int>(Columns.Mode); }
			set { SetColumnValue(Columns.Mode, value); }
		}

		[XmlAttribute("TotalMax")]
		[Bindable(true)]
		public int? TotalMax
		{
			get { return GetColumnValue<int?>(Columns.TotalMax); }
			set { SetColumnValue(Columns.TotalMax, value); }
		}

		[XmlAttribute("TotalCurrent")]
		[Bindable(true)]
		public int? TotalCurrent
		{
			get { return GetColumnValue<int?>(Columns.TotalCurrent); }
			set { SetColumnValue(Columns.TotalCurrent, value); }
		}

		[XmlAttribute("WinningMax")]
		[Bindable(true)]
		public int? WinningMax
		{
			get { return GetColumnValue<int?>(Columns.WinningMax); }
			set { SetColumnValue(Columns.WinningMax, value); }
		}

		[XmlAttribute("WinningCurrent")]
		[Bindable(true)]
		public int? WinningCurrent
		{
			get { return GetColumnValue<int?>(Columns.WinningCurrent); }
			set { SetColumnValue(Columns.WinningCurrent, value); }
		}

		[XmlAttribute("IsImmediate")]
		[Bindable(true)]
		public bool? IsImmediate
		{
			get { return GetColumnValue<bool?>(Columns.IsImmediate); }
			set { SetColumnValue(Columns.IsImmediate, value); }
		}

		[XmlAttribute("StartDate")]
		[Bindable(true)]
		public DateTime StartDate
		{
			get { return GetColumnValue<DateTime>(Columns.StartDate); }
			set { SetColumnValue(Columns.StartDate, value); }
		}

		[XmlAttribute("EndDate")]
		[Bindable(true)]
		public DateTime EndDate
		{
			get { return GetColumnValue<DateTime>(Columns.EndDate); }
			set { SetColumnValue(Columns.EndDate, value); }
		}

		[XmlAttribute("FbPicture")]
		[Bindable(true)]
		public string FbPicture
		{
			get { return GetColumnValue<string>(Columns.FbPicture); }
			set { SetColumnValue(Columns.FbPicture, value); }
		}

		[XmlAttribute("FbDesc")]
		[Bindable(true)]
		public string FbDesc
		{
			get { return GetColumnValue<string>(Columns.FbDesc); }
			set { SetColumnValue(Columns.FbDesc, value); }
		}

		[XmlAttribute("Cpa")]
		[Bindable(true)]
		public string Cpa
		{
			get { return GetColumnValue<string>(Columns.Cpa); }
			set { SetColumnValue(Columns.Cpa, value); }
		}

		[XmlAttribute("Url")]
		[Bindable(true)]
		public string Url
		{
			get { return GetColumnValue<string>(Columns.Url); }
			set { SetColumnValue(Columns.Url, value); }
		}

		[XmlAttribute("EdmDefaultCity")]
		[Bindable(true)]
		public int? EdmDefaultCity
		{
			get { return GetColumnValue<int?>(Columns.EdmDefaultCity); }
			set { SetColumnValue(Columns.EdmDefaultCity, value); }
		}

		[XmlAttribute("ExcludeCpaList")]
		[Bindable(true)]
		public string ExcludeCpaList
		{
			get { return GetColumnValue<string>(Columns.ExcludeCpaList); }
			set { SetColumnValue(Columns.ExcludeCpaList, value); }
		}

		[XmlAttribute("EdmBg1")]
		[Bindable(true)]
		public string EdmBg1
		{
			get { return GetColumnValue<string>(Columns.EdmBg1); }
			set { SetColumnValue(Columns.EdmBg1, value); }
		}

		[XmlAttribute("EdmBg2")]
		[Bindable(true)]
		public string EdmBg2
		{
			get { return GetColumnValue<string>(Columns.EdmBg2); }
			set { SetColumnValue(Columns.EdmBg2, value); }
		}

		[XmlAttribute("EdmCityColor")]
		[Bindable(true)]
		public string EdmCityColor
		{
			get { return GetColumnValue<string>(Columns.EdmCityColor); }
			set { SetColumnValue(Columns.EdmCityColor, value); }
		}

		[XmlAttribute("Cookieexpire1")]
		[Bindable(true)]
		public int? Cookieexpire1
		{
			get { return GetColumnValue<int?>(Columns.Cookieexpire1); }
			set { SetColumnValue(Columns.Cookieexpire1, value); }
		}

		[XmlAttribute("Cookieexpire2")]
		[Bindable(true)]
		public int? Cookieexpire2
		{
			get { return GetColumnValue<int?>(Columns.Cookieexpire2); }
			set { SetColumnValue(Columns.Cookieexpire2, value); }
		}

		[XmlAttribute("Cookieexpire3")]
		[Bindable(true)]
		public int? Cookieexpire3
		{
			get { return GetColumnValue<int?>(Columns.Cookieexpire3); }
			set { SetColumnValue(Columns.Cookieexpire3, value); }
		}

		[XmlAttribute("Status")]
		[Bindable(true)]
		public bool Status
		{
			get { return GetColumnValue<bool>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}

		[XmlAttribute("Creator")]
		[Bindable(true)]
		public string Creator
		{
			get { return GetColumnValue<string>(Columns.Creator); }
			set { SetColumnValue(Columns.Creator, value); }
		}

		[XmlAttribute("Cdt")]
		[Bindable(true)]
		public DateTime Cdt
		{
			get { return GetColumnValue<DateTime>(Columns.Cdt); }
			set { SetColumnValue(Columns.Cdt, value); }
		}

		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}

		[XmlAttribute("DiscountCampaignId")]
		[Bindable(true)]
		public int? DiscountCampaignId
		{
			get { return GetColumnValue<int?>(Columns.DiscountCampaignId); }
			set { SetColumnValue(Columns.DiscountCampaignId, value); }
		}

		[XmlAttribute("PageName")]
		[Bindable(true)]
		public string PageName
		{
			get { return GetColumnValue<string>(Columns.PageName); }
			set { SetColumnValue(Columns.PageName, value); }
		}

		[XmlAttribute("DiscountList")]
		[Bindable(true)]
		public string DiscountList
		{
			get { return GetColumnValue<string>(Columns.DiscountList); }
			set { SetColumnValue(Columns.DiscountList, value); }
		}

		[XmlAttribute("AutoCloseSeconds")]
		[Bindable(true)]
		public int? AutoCloseSeconds
		{
			get { return GetColumnValue<int?>(Columns.AutoCloseSeconds); }
			set { SetColumnValue(Columns.AutoCloseSeconds, value); }
		}

		[XmlAttribute("Version")]
		[Bindable(true)]
		public int? Version
		{
			get { return GetColumnValue<int?>(Columns.Version); }
			set { SetColumnValue(Columns.Version, value); }
		}

		[XmlAttribute("WebLinkUrl")]
		[Bindable(true)]
		public string WebLinkUrl
		{
			get { return GetColumnValue<string>(Columns.WebLinkUrl); }
			set { SetColumnValue(Columns.WebLinkUrl, value); }
		}

		[XmlAttribute("WebImage")]
		[Bindable(true)]
		public string WebImage
		{
			get { return GetColumnValue<string>(Columns.WebImage); }
			set { SetColumnValue(Columns.WebImage, value); }
		}

		[XmlAttribute("WebOpenTarget")]
		[Bindable(true)]
		public int? WebOpenTarget
		{
			get { return GetColumnValue<int?>(Columns.WebOpenTarget); }
			set { SetColumnValue(Columns.WebOpenTarget, value); }
		}

		[XmlAttribute("MobileLinkUrl")]
		[Bindable(true)]
		public string MobileLinkUrl
		{
			get { return GetColumnValue<string>(Columns.MobileLinkUrl); }
			set { SetColumnValue(Columns.MobileLinkUrl, value); }
		}

		[XmlAttribute("MobileImage")]
		[Bindable(true)]
		public string MobileImage
		{
			get { return GetColumnValue<string>(Columns.MobileImage); }
			set { SetColumnValue(Columns.MobileImage, value); }
		}

		[XmlAttribute("MobileOpenTarget")]
		[Bindable(true)]
		public int? MobileOpenTarget
		{
			get { return GetColumnValue<int?>(Columns.MobileOpenTarget); }
			set { SetColumnValue(Columns.MobileOpenTarget, value); }
		}

		[XmlAttribute("AppLinkUrl")]
		[Bindable(true)]
		public string AppLinkUrl
		{
			get { return GetColumnValue<string>(Columns.AppLinkUrl); }
			set { SetColumnValue(Columns.AppLinkUrl, value); }
		}

		[XmlAttribute("AppImage")]
		[Bindable(true)]
		public string AppImage
		{
			get { return GetColumnValue<string>(Columns.AppImage); }
			set { SetColumnValue(Columns.AppImage, value); }
		}

		[XmlAttribute("AppText")]
		[Bindable(true)]
		public string AppText
		{
			get { return GetColumnValue<string>(Columns.AppText); }
			set { SetColumnValue(Columns.AppText, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn EventnameColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn Description1Column
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn Description2Column
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn TypeColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn ModeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn TotalMaxColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn TotalCurrentColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn WinningMaxColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn WinningCurrentColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn IsImmediateColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn StartDateColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn EndDateColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn FbPictureColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn FbDescColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn CpaColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn UrlColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn EdmDefaultCityColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn ExcludeCpaListColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn EdmBg1Column
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn EdmBg2Column
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn EdmCityColorColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn Cookieexpire1Column
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn Cookieexpire2Column
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn Cookieexpire3Column
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn StatusColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn CreatorColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn CdtColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn MessageColumn
		{
			get { return Schema.Columns[28]; }
		}

		public static TableSchema.TableColumn DiscountCampaignIdColumn
		{
			get { return Schema.Columns[29]; }
		}

		public static TableSchema.TableColumn PageNameColumn
		{
			get { return Schema.Columns[30]; }
		}

		public static TableSchema.TableColumn DiscountListColumn
		{
			get { return Schema.Columns[31]; }
		}

		public static TableSchema.TableColumn AutoCloseSecondsColumn
		{
			get { return Schema.Columns[32]; }
		}

		public static TableSchema.TableColumn VersionColumn
		{
			get { return Schema.Columns[33]; }
		}

		public static TableSchema.TableColumn WebLinkUrlColumn
		{
			get { return Schema.Columns[34]; }
		}

		public static TableSchema.TableColumn WebImageColumn
		{
			get { return Schema.Columns[35]; }
		}

		public static TableSchema.TableColumn WebOpenTargetColumn
		{
			get { return Schema.Columns[36]; }
		}

		public static TableSchema.TableColumn MobileLinkUrlColumn
		{
			get { return Schema.Columns[37]; }
		}

		public static TableSchema.TableColumn MobileImageColumn
		{
			get { return Schema.Columns[38]; }
		}

		public static TableSchema.TableColumn MobileOpenTargetColumn
		{
			get { return Schema.Columns[39]; }
		}

		public static TableSchema.TableColumn AppLinkUrlColumn
		{
			get { return Schema.Columns[40]; }
		}

		public static TableSchema.TableColumn AppImageColumn
		{
			get { return Schema.Columns[41]; }
		}

		public static TableSchema.TableColumn AppTextColumn
		{
			get { return Schema.Columns[42]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string Eventname = @"eventname";
			public static string Description1 = @"description_1";
			public static string Description2 = @"description_2";
			public static string Type = @"type";
			public static string Mode = @"mode";
			public static string TotalMax = @"total_max";
			public static string TotalCurrent = @"total_current";
			public static string WinningMax = @"winning_max";
			public static string WinningCurrent = @"winning_current";
			public static string IsImmediate = @"is_immediate";
			public static string StartDate = @"startDate";
			public static string EndDate = @"endDate";
			public static string FbPicture = @"fb_picture";
			public static string FbDesc = @"fb_desc";
			public static string Cpa = @"cpa";
			public static string Url = @"url";
			public static string EdmDefaultCity = @"edm_default_city";
			public static string ExcludeCpaList = @"exclude_cpa_list";
			public static string EdmBg1 = @"edm_bg_1";
			public static string EdmBg2 = @"edm_bg_2";
			public static string EdmCityColor = @"edm_city_color";
			public static string Cookieexpire1 = @"cookieexpire_1";
			public static string Cookieexpire2 = @"cookieexpire_2";
			public static string Cookieexpire3 = @"cookieexpire_3";
			public static string Status = @"status";
			public static string Creator = @"creator";
			public static string Cdt = @"cdt";
			public static string Message = @"message";
			public static string DiscountCampaignId = @"discount_campaign_id";
			public static string PageName = @"page_name";
			public static string DiscountList = @"discount_list";
			public static string AutoCloseSeconds = @"auto_close_seconds";
			public static string Version = @"version";
			public static string WebLinkUrl = @"web_link_url";
			public static string WebImage = @"web_image";
			public static string WebOpenTarget = @"web_open_target";
			public static string MobileLinkUrl = @"mobile_link_url";
			public static string MobileImage = @"mobile_image";
			public static string MobileOpenTarget = @"mobile_open_target";
			public static string AppLinkUrl = @"app_link_url";
			public static string AppImage = @"app_image";
			public static string AppText = @"app_text";
		}

		#endregion

	}
}

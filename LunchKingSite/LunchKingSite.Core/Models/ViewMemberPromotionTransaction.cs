using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMemberPromotionTransaction class.
    /// </summary>
    [Serializable]
    public partial class ViewMemberPromotionTransactionCollection : ReadOnlyList<ViewMemberPromotionTransaction, ViewMemberPromotionTransactionCollection>
    {        
        public ViewMemberPromotionTransactionCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_member_promotion_transaction view.
    /// </summary>
    [Serializable]
    public partial class ViewMemberPromotionTransaction : ReadOnlyRecord<ViewMemberPromotionTransaction>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_member_promotion_transaction", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarDepositOrWithdrawal = new TableSchema.TableColumn(schema);
                colvarDepositOrWithdrawal.ColumnName = "deposit_or_withdrawal";
                colvarDepositOrWithdrawal.DataType = DbType.AnsiString;
                colvarDepositOrWithdrawal.MaxLength = 1;
                colvarDepositOrWithdrawal.AutoIncrement = false;
                colvarDepositOrWithdrawal.IsNullable = false;
                colvarDepositOrWithdrawal.IsPrimaryKey = false;
                colvarDepositOrWithdrawal.IsForeignKey = false;
                colvarDepositOrWithdrawal.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepositOrWithdrawal);
                
                TableSchema.TableColumn colvarWithdrawalId = new TableSchema.TableColumn(schema);
                colvarWithdrawalId.ColumnName = "withdrawal_id";
                colvarWithdrawalId.DataType = DbType.Int32;
                colvarWithdrawalId.MaxLength = 0;
                colvarWithdrawalId.AutoIncrement = false;
                colvarWithdrawalId.IsNullable = false;
                colvarWithdrawalId.IsPrimaryKey = false;
                colvarWithdrawalId.IsForeignKey = false;
                colvarWithdrawalId.IsReadOnly = false;
                
                schema.Columns.Add(colvarWithdrawalId);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = false;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarExpireTime = new TableSchema.TableColumn(schema);
                colvarExpireTime.ColumnName = "expire_time";
                colvarExpireTime.DataType = DbType.DateTime;
                colvarExpireTime.MaxLength = 0;
                colvarExpireTime.AutoIncrement = false;
                colvarExpireTime.IsNullable = false;
                colvarExpireTime.IsPrimaryKey = false;
                colvarExpireTime.IsForeignKey = false;
                colvarExpireTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarExpireTime);
                
                TableSchema.TableColumn colvarPromotionValue = new TableSchema.TableColumn(schema);
                colvarPromotionValue.ColumnName = "promotion_value";
                colvarPromotionValue.DataType = DbType.Double;
                colvarPromotionValue.MaxLength = 0;
                colvarPromotionValue.AutoIncrement = false;
                colvarPromotionValue.IsNullable = true;
                colvarPromotionValue.IsPrimaryKey = false;
                colvarPromotionValue.IsForeignKey = false;
                colvarPromotionValue.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromotionValue);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarMemberUserName = new TableSchema.TableColumn(schema);
                colvarMemberUserName.ColumnName = "member_user_name";
                colvarMemberUserName.DataType = DbType.String;
                colvarMemberUserName.MaxLength = 256;
                colvarMemberUserName.AutoIncrement = false;
                colvarMemberUserName.IsNullable = false;
                colvarMemberUserName.IsPrimaryKey = false;
                colvarMemberUserName.IsForeignKey = false;
                colvarMemberUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberUserName);
                
                TableSchema.TableColumn colvarMemberUserId = new TableSchema.TableColumn(schema);
                colvarMemberUserId.ColumnName = "member_user_id";
                colvarMemberUserId.DataType = DbType.Int32;
                colvarMemberUserId.MaxLength = 0;
                colvarMemberUserId.AutoIncrement = false;
                colvarMemberUserId.IsNullable = false;
                colvarMemberUserId.IsPrimaryKey = false;
                colvarMemberUserId.IsForeignKey = false;
                colvarMemberUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberUserId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_GUID";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarAction = new TableSchema.TableColumn(schema);
                colvarAction.ColumnName = "action";
                colvarAction.DataType = DbType.String;
                colvarAction.MaxLength = 200;
                colvarAction.AutoIncrement = false;
                colvarAction.IsNullable = true;
                colvarAction.IsPrimaryKey = false;
                colvarAction.IsForeignKey = false;
                colvarAction.IsReadOnly = false;
                
                schema.Columns.Add(colvarAction);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = true;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarWithdrawalOrderGuid = new TableSchema.TableColumn(schema);
                colvarWithdrawalOrderGuid.ColumnName = "withdrawal_order_GUID";
                colvarWithdrawalOrderGuid.DataType = DbType.Guid;
                colvarWithdrawalOrderGuid.MaxLength = 0;
                colvarWithdrawalOrderGuid.AutoIncrement = false;
                colvarWithdrawalOrderGuid.IsNullable = true;
                colvarWithdrawalOrderGuid.IsPrimaryKey = false;
                colvarWithdrawalOrderGuid.IsForeignKey = false;
                colvarWithdrawalOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarWithdrawalOrderGuid);
                
                TableSchema.TableColumn colvarRedeemOrderGuid = new TableSchema.TableColumn(schema);
                colvarRedeemOrderGuid.ColumnName = "redeem_order_GUID";
                colvarRedeemOrderGuid.DataType = DbType.Guid;
                colvarRedeemOrderGuid.MaxLength = 0;
                colvarRedeemOrderGuid.AutoIncrement = false;
                colvarRedeemOrderGuid.IsNullable = true;
                colvarRedeemOrderGuid.IsPrimaryKey = false;
                colvarRedeemOrderGuid.IsForeignKey = false;
                colvarRedeemOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarRedeemOrderGuid);
                
                TableSchema.TableColumn colvarBalance = new TableSchema.TableColumn(schema);
                colvarBalance.ColumnName = "balance";
                colvarBalance.DataType = DbType.Double;
                colvarBalance.MaxLength = 0;
                colvarBalance.AutoIncrement = false;
                colvarBalance.IsNullable = true;
                colvarBalance.IsPrimaryKey = false;
                colvarBalance.IsForeignKey = false;
                colvarBalance.IsReadOnly = false;
                
                schema.Columns.Add(colvarBalance);
                
                TableSchema.TableColumn colvarRunSum = new TableSchema.TableColumn(schema);
                colvarRunSum.ColumnName = "run_sum";
                colvarRunSum.DataType = DbType.Double;
                colvarRunSum.MaxLength = 0;
                colvarRunSum.AutoIncrement = false;
                colvarRunSum.IsNullable = true;
                colvarRunSum.IsPrimaryKey = false;
                colvarRunSum.IsForeignKey = false;
                colvarRunSum.IsReadOnly = false;
                
                schema.Columns.Add(colvarRunSum);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 30;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                TableSchema.TableColumn colvarCouponEventContentName = new TableSchema.TableColumn(schema);
                colvarCouponEventContentName.ColumnName = "coupon_event_content_name";
                colvarCouponEventContentName.DataType = DbType.String;
                colvarCouponEventContentName.MaxLength = 400;
                colvarCouponEventContentName.AutoIncrement = false;
                colvarCouponEventContentName.IsNullable = true;
                colvarCouponEventContentName.IsPrimaryKey = false;
                colvarCouponEventContentName.IsForeignKey = false;
                colvarCouponEventContentName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponEventContentName);
                
                TableSchema.TableColumn colvarPromotionType = new TableSchema.TableColumn(schema);
                colvarPromotionType.ColumnName = "promotion_type";
                colvarPromotionType.DataType = DbType.Int32;
                colvarPromotionType.MaxLength = 0;
                colvarPromotionType.AutoIncrement = false;
                colvarPromotionType.IsNullable = true;
                colvarPromotionType.IsPrimaryKey = false;
                colvarPromotionType.IsForeignKey = false;
                colvarPromotionType.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromotionType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_member_promotion_transaction",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMemberPromotionTransaction()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMemberPromotionTransaction(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMemberPromotionTransaction(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMemberPromotionTransaction(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("DepositOrWithdrawal")]
        [Bindable(true)]
        public string DepositOrWithdrawal 
	    {
		    get
		    {
			    return GetColumnValue<string>("deposit_or_withdrawal");
		    }
            set 
		    {
			    SetColumnValue("deposit_or_withdrawal", value);
            }
        }
	      
        [XmlAttribute("WithdrawalId")]
        [Bindable(true)]
        public int WithdrawalId 
	    {
		    get
		    {
			    return GetColumnValue<int>("withdrawal_id");
		    }
            set 
		    {
			    SetColumnValue("withdrawal_id", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("ExpireTime")]
        [Bindable(true)]
        public DateTime ExpireTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("expire_time");
		    }
            set 
		    {
			    SetColumnValue("expire_time", value);
            }
        }
	      
        [XmlAttribute("PromotionValue")]
        [Bindable(true)]
        public double? PromotionValue 
	    {
		    get
		    {
			    return GetColumnValue<double?>("promotion_value");
		    }
            set 
		    {
			    SetColumnValue("promotion_value", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("MemberUserName")]
        [Bindable(true)]
        public string MemberUserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_user_name");
		    }
            set 
		    {
			    SetColumnValue("member_user_name", value);
            }
        }
	      
        [XmlAttribute("MemberUserId")]
        [Bindable(true)]
        public int MemberUserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("member_user_id");
		    }
            set 
		    {
			    SetColumnValue("member_user_id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("order_GUID");
		    }
            set 
		    {
			    SetColumnValue("order_GUID", value);
            }
        }
	      
        [XmlAttribute("Action")]
        [Bindable(true)]
        public string Action 
	    {
		    get
		    {
			    return GetColumnValue<string>("action");
		    }
            set 
		    {
			    SetColumnValue("action", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int? Type 
	    {
		    get
		    {
			    return GetColumnValue<int?>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("WithdrawalOrderGuid")]
        [Bindable(true)]
        public Guid? WithdrawalOrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("withdrawal_order_GUID");
		    }
            set 
		    {
			    SetColumnValue("withdrawal_order_GUID", value);
            }
        }
	      
        [XmlAttribute("RedeemOrderGuid")]
        [Bindable(true)]
        public Guid? RedeemOrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("redeem_order_GUID");
		    }
            set 
		    {
			    SetColumnValue("redeem_order_GUID", value);
            }
        }
	      
        [XmlAttribute("Balance")]
        [Bindable(true)]
        public double? Balance 
	    {
		    get
		    {
			    return GetColumnValue<double?>("balance");
		    }
            set 
		    {
			    SetColumnValue("balance", value);
            }
        }
	      
        [XmlAttribute("RunSum")]
        [Bindable(true)]
        public double? RunSum 
	    {
		    get
		    {
			    return GetColumnValue<double?>("run_sum");
		    }
            set 
		    {
			    SetColumnValue("run_sum", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	      
        [XmlAttribute("CouponEventContentName")]
        [Bindable(true)]
        public string CouponEventContentName 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_event_content_name");
		    }
            set 
		    {
			    SetColumnValue("coupon_event_content_name", value);
            }
        }
	      
        [XmlAttribute("PromotionType")]
        [Bindable(true)]
        public int? PromotionType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("promotion_type");
		    }
            set 
		    {
			    SetColumnValue("promotion_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string DepositOrWithdrawal = @"deposit_or_withdrawal";
            
            public static string WithdrawalId = @"withdrawal_id";
            
            public static string StartTime = @"start_time";
            
            public static string ExpireTime = @"expire_time";
            
            public static string PromotionValue = @"promotion_value";
            
            public static string CreateTime = @"create_time";
            
            public static string MemberUserName = @"member_user_name";
            
            public static string MemberUserId = @"member_user_id";
            
            public static string OrderGuid = @"order_GUID";
            
            public static string Action = @"action";
            
            public static string CreateId = @"create_id";
            
            public static string Type = @"type";
            
            public static string WithdrawalOrderGuid = @"withdrawal_order_GUID";
            
            public static string RedeemOrderGuid = @"redeem_order_GUID";
            
            public static string Balance = @"balance";
            
            public static string RunSum = @"run_sum";
            
            public static string CouponUsage = @"coupon_usage";
            
            public static string CouponEventContentName = @"coupon_event_content_name";
            
            public static string PromotionType = @"promotion_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

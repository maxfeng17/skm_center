using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Audit class.
	/// </summary>
    [Serializable]
	public partial class AuditCollection : RepositoryList<Audit, AuditCollection>
	{	   
		public AuditCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>AuditCollection</returns>
		public AuditCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Audit o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the audit table.
	/// </summary>
	[Serializable]
	public partial class Audit : RepositoryRecord<Audit>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Audit()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Audit(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("audit", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarReferenceId = new TableSchema.TableColumn(schema);
				colvarReferenceId.ColumnName = "reference_id";
				colvarReferenceId.DataType = DbType.AnsiString;
				colvarReferenceId.MaxLength = 250;
				colvarReferenceId.AutoIncrement = false;
				colvarReferenceId.IsNullable = true;
				colvarReferenceId.IsPrimaryKey = false;
				colvarReferenceId.IsForeignKey = false;
				colvarReferenceId.IsReadOnly = false;
				colvarReferenceId.DefaultSetting = @"";
				colvarReferenceId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferenceId);
				
				TableSchema.TableColumn colvarReferenceType = new TableSchema.TableColumn(schema);
				colvarReferenceType.ColumnName = "reference_type";
				colvarReferenceType.DataType = DbType.Int32;
				colvarReferenceType.MaxLength = 0;
				colvarReferenceType.AutoIncrement = false;
				colvarReferenceType.IsNullable = false;
				colvarReferenceType.IsPrimaryKey = false;
				colvarReferenceType.IsForeignKey = false;
				colvarReferenceType.IsReadOnly = false;
				colvarReferenceType.DefaultSetting = @"";
				colvarReferenceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferenceType);
				
				TableSchema.TableColumn colvarCommonVisible = new TableSchema.TableColumn(schema);
				colvarCommonVisible.ColumnName = "common_visible";
				colvarCommonVisible.DataType = DbType.Boolean;
				colvarCommonVisible.MaxLength = 0;
				colvarCommonVisible.AutoIncrement = false;
				colvarCommonVisible.IsNullable = true;
				colvarCommonVisible.IsPrimaryKey = false;
				colvarCommonVisible.IsForeignKey = false;
				colvarCommonVisible.IsReadOnly = false;
				colvarCommonVisible.DefaultSetting = @"";
				colvarCommonVisible.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCommonVisible);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 1073741823;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = false;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("audit",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ReferenceId")]
		[Bindable(true)]
		public string ReferenceId 
		{
			get { return GetColumnValue<string>(Columns.ReferenceId); }
			set { SetColumnValue(Columns.ReferenceId, value); }
		}
		  
		[XmlAttribute("ReferenceType")]
		[Bindable(true)]
		public int ReferenceType 
		{
			get { return GetColumnValue<int>(Columns.ReferenceType); }
			set { SetColumnValue(Columns.ReferenceType, value); }
		}
		  
		[XmlAttribute("CommonVisible")]
		[Bindable(true)]
		public bool? CommonVisible 
		{
			get { return GetColumnValue<bool?>(Columns.CommonVisible); }
			set { SetColumnValue(Columns.CommonVisible, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferenceIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferenceTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CommonVisibleColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ReferenceId = @"reference_id";
			 public static string ReferenceType = @"reference_type";
			 public static string CommonVisible = @"common_visible";
			 public static string Message = @"message";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

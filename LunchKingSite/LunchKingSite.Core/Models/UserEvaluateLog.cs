using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the UserEvaluateLog class.
	/// </summary>
    [Serializable]
	public partial class UserEvaluateLogCollection : RepositoryList<UserEvaluateLog, UserEvaluateLogCollection>
	{	   
		public UserEvaluateLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>UserEvaluateLogCollection</returns>
		public UserEvaluateLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                UserEvaluateLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the user_evaluate_log table.
	/// </summary>
	[Serializable]
	public partial class UserEvaluateLog : RepositoryRecord<UserEvaluateLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public UserEvaluateLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public UserEvaluateLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("user_evaluate_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarSendtime = new TableSchema.TableColumn(schema);
				colvarSendtime.ColumnName = "sendtime";
				colvarSendtime.DataType = DbType.DateTime;
				colvarSendtime.MaxLength = 0;
				colvarSendtime.AutoIncrement = false;
				colvarSendtime.IsNullable = false;
				colvarSendtime.IsPrimaryKey = false;
				colvarSendtime.IsForeignKey = false;
				colvarSendtime.IsReadOnly = false;
				colvarSendtime.DefaultSetting = @"";
				colvarSendtime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendtime);
				
				TableSchema.TableColumn colvarIsOpen = new TableSchema.TableColumn(schema);
				colvarIsOpen.ColumnName = "is_open";
				colvarIsOpen.DataType = DbType.Boolean;
				colvarIsOpen.MaxLength = 0;
				colvarIsOpen.AutoIncrement = false;
				colvarIsOpen.IsNullable = false;
				colvarIsOpen.IsPrimaryKey = false;
				colvarIsOpen.IsForeignKey = false;
				colvarIsOpen.IsReadOnly = false;
				colvarIsOpen.DefaultSetting = @"";
				colvarIsOpen.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOpen);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("user_evaluate_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("Sendtime")]
		[Bindable(true)]
		public DateTime Sendtime 
		{
			get { return GetColumnValue<DateTime>(Columns.Sendtime); }
			set { SetColumnValue(Columns.Sendtime, value); }
		}
		  
		[XmlAttribute("IsOpen")]
		[Bindable(true)]
		public bool IsOpen 
		{
			get { return GetColumnValue<bool>(Columns.IsOpen); }
			set { SetColumnValue(Columns.IsOpen, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SendtimeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOpenColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderGuid = @"order_guid";
			 public static string Sendtime = @"sendtime";
			 public static string IsOpen = @"is_open";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

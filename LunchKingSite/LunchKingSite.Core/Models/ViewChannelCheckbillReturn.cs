using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewChannelCheckBillReturn class.
    /// </summary>
    [Serializable]
    public partial class ViewChannelCheckBillReturnCollection : ReadOnlyList<ViewChannelCheckBillReturn, ViewChannelCheckBillReturnCollection>
    {        
        public ViewChannelCheckBillReturnCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_channel_check_bill_return view.
    /// </summary>
    [Serializable]
    public partial class ViewChannelCheckBillReturn : ReadOnlyRecord<ViewChannelCheckBillReturn>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_channel_check_bill_return", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCId = new TableSchema.TableColumn(schema);
                colvarCId.ColumnName = "c_id";
                colvarCId.DataType = DbType.String;
                colvarCId.MaxLength = 50;
                colvarCId.AutoIncrement = false;
                colvarCId.IsNullable = true;
                colvarCId.IsPrimaryKey = false;
                colvarCId.IsForeignKey = false;
                colvarCId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCId);
                
                TableSchema.TableColumn colvarPrize = new TableSchema.TableColumn(schema);
                colvarPrize.ColumnName = "prize";
                colvarPrize.DataType = DbType.Currency;
                colvarPrize.MaxLength = 0;
                colvarPrize.AutoIncrement = false;
                colvarPrize.IsNullable = false;
                colvarPrize.IsPrimaryKey = false;
                colvarPrize.IsForeignKey = false;
                colvarPrize.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrize);
                
                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Currency;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = true;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarCost);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarBuyDate = new TableSchema.TableColumn(schema);
                colvarBuyDate.ColumnName = "buy_date";
                colvarBuyDate.DataType = DbType.DateTime;
                colvarBuyDate.MaxLength = 0;
                colvarBuyDate.AutoIncrement = false;
                colvarBuyDate.IsNullable = false;
                colvarBuyDate.IsPrimaryKey = false;
                colvarBuyDate.IsForeignKey = false;
                colvarBuyDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuyDate);
                
                TableSchema.TableColumn colvarReturnDate = new TableSchema.TableColumn(schema);
                colvarReturnDate.ColumnName = "return_date";
                colvarReturnDate.DataType = DbType.DateTime;
                colvarReturnDate.MaxLength = 0;
                colvarReturnDate.AutoIncrement = false;
                colvarReturnDate.IsNullable = true;
                colvarReturnDate.IsPrimaryKey = false;
                colvarReturnDate.IsForeignKey = false;
                colvarReturnDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnDate);
                
                TableSchema.TableColumn colvarUId = new TableSchema.TableColumn(schema);
                colvarUId.ColumnName = "u_id";
                colvarUId.DataType = DbType.Int32;
                colvarUId.MaxLength = 0;
                colvarUId.AutoIncrement = false;
                colvarUId.IsNullable = false;
                colvarUId.IsPrimaryKey = false;
                colvarUId.IsForeignKey = false;
                colvarUId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUId);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "BID";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBid);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_GUID";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarRId = new TableSchema.TableColumn(schema);
                colvarRId.ColumnName = "r_id";
                colvarRId.DataType = DbType.AnsiString;
                colvarRId.MaxLength = 50;
                colvarRId.AutoIncrement = false;
                colvarRId.IsNullable = false;
                colvarRId.IsPrimaryKey = false;
                colvarRId.IsForeignKey = false;
                colvarRId.IsReadOnly = false;
                
                schema.Columns.Add(colvarRId);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_channel_check_bill_return",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewChannelCheckBillReturn()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewChannelCheckBillReturn(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewChannelCheckBillReturn(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewChannelCheckBillReturn(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CId")]
        [Bindable(true)]
        public string CId 
	    {
		    get
		    {
			    return GetColumnValue<string>("c_id");
		    }
            set 
		    {
			    SetColumnValue("c_id", value);
            }
        }
	      
        [XmlAttribute("Prize")]
        [Bindable(true)]
        public decimal Prize 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("prize");
		    }
            set 
		    {
			    SetColumnValue("prize", value);
            }
        }
	      
        [XmlAttribute("Cost")]
        [Bindable(true)]
        public decimal? Cost 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("cost");
		    }
            set 
		    {
			    SetColumnValue("cost", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("BuyDate")]
        [Bindable(true)]
        public DateTime BuyDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("buy_date");
		    }
            set 
		    {
			    SetColumnValue("buy_date", value);
            }
        }
	      
        [XmlAttribute("ReturnDate")]
        [Bindable(true)]
        public DateTime? ReturnDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("return_date");
		    }
            set 
		    {
			    SetColumnValue("return_date", value);
            }
        }
	      
        [XmlAttribute("UId")]
        [Bindable(true)]
        public int UId 
	    {
		    get
		    {
			    return GetColumnValue<int>("u_id");
		    }
            set 
		    {
			    SetColumnValue("u_id", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	      
        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("BID");
		    }
            set 
		    {
			    SetColumnValue("BID", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_GUID");
		    }
            set 
		    {
			    SetColumnValue("order_GUID", value);
            }
        }
	      
        [XmlAttribute("RId")]
        [Bindable(true)]
        public string RId 
	    {
		    get
		    {
			    return GetColumnValue<string>("r_id");
		    }
            set 
		    {
			    SetColumnValue("r_id", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CId = @"c_id";
            
            public static string Prize = @"prize";
            
            public static string Cost = @"cost";
            
            public static string OrderId = @"order_id";
            
            public static string BuyDate = @"buy_date";
            
            public static string ReturnDate = @"return_date";
            
            public static string UId = @"u_id";
            
            public static string CouponUsage = @"coupon_usage";
            
            public static string Bid = @"BID";
            
            public static string OrderGuid = @"order_GUID";
            
            public static string RId = @"r_id";
            
            public static string Type = @"type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

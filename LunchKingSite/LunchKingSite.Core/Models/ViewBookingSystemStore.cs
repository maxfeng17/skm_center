using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBookingSystemStore class.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemStoreCollection : ReadOnlyList<ViewBookingSystemStore, ViewBookingSystemStoreCollection>
    {        
        public ViewBookingSystemStoreCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_booking_system_store view.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemStore : ReadOnlyRecord<ViewBookingSystemStore>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_booking_system_store", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = false;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarBookingType = new TableSchema.TableColumn(schema);
                colvarBookingType.ColumnName = "booking_type";
                colvarBookingType.DataType = DbType.Int32;
                colvarBookingType.MaxLength = 0;
                colvarBookingType.AutoIncrement = false;
                colvarBookingType.IsNullable = false;
                colvarBookingType.IsPrimaryKey = false;
                colvarBookingType.IsForeignKey = false;
                colvarBookingType.IsReadOnly = false;
                
                schema.Columns.Add(colvarBookingType);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
                colvarPhone.ColumnName = "phone";
                colvarPhone.DataType = DbType.AnsiString;
                colvarPhone.MaxLength = 100;
                colvarPhone.AutoIncrement = false;
                colvarPhone.IsNullable = true;
                colvarPhone.IsPrimaryKey = false;
                colvarPhone.IsForeignKey = false;
                colvarPhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhone);
                
                TableSchema.TableColumn colvarTownshipId = new TableSchema.TableColumn(schema);
                colvarTownshipId.ColumnName = "township_id";
                colvarTownshipId.DataType = DbType.Int32;
                colvarTownshipId.MaxLength = 0;
                colvarTownshipId.AutoIncrement = false;
                colvarTownshipId.IsNullable = true;
                colvarTownshipId.IsPrimaryKey = false;
                colvarTownshipId.IsForeignKey = false;
                colvarTownshipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTownshipId);
                
                TableSchema.TableColumn colvarAddressString = new TableSchema.TableColumn(schema);
                colvarAddressString.ColumnName = "address_string";
                colvarAddressString.DataType = DbType.String;
                colvarAddressString.MaxLength = 140;
                colvarAddressString.AutoIncrement = false;
                colvarAddressString.IsNullable = true;
                colvarAddressString.IsPrimaryKey = false;
                colvarAddressString.IsForeignKey = false;
                colvarAddressString.IsReadOnly = false;
                
                schema.Columns.Add(colvarAddressString);
                
                TableSchema.TableColumn colvarWebUrl = new TableSchema.TableColumn(schema);
                colvarWebUrl.ColumnName = "web_url";
                colvarWebUrl.DataType = DbType.String;
                colvarWebUrl.MaxLength = 256;
                colvarWebUrl.AutoIncrement = false;
                colvarWebUrl.IsNullable = true;
                colvarWebUrl.IsPrimaryKey = false;
                colvarWebUrl.IsForeignKey = false;
                colvarWebUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarWebUrl);
                
                TableSchema.TableColumn colvarBookingId = new TableSchema.TableColumn(schema);
                colvarBookingId.ColumnName = "booking_id";
                colvarBookingId.DataType = DbType.Int32;
                colvarBookingId.MaxLength = 0;
                colvarBookingId.AutoIncrement = false;
                colvarBookingId.IsNullable = false;
                colvarBookingId.IsPrimaryKey = false;
                colvarBookingId.IsForeignKey = false;
                colvarBookingId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBookingId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_booking_system_store",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBookingSystemStore()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBookingSystemStore(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBookingSystemStore(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBookingSystemStore(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("BookingType")]
        [Bindable(true)]
        public int BookingType 
	    {
		    get
		    {
			    return GetColumnValue<int>("booking_type");
		    }
            set 
		    {
			    SetColumnValue("booking_type", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("Phone")]
        [Bindable(true)]
        public string Phone 
	    {
		    get
		    {
			    return GetColumnValue<string>("phone");
		    }
            set 
		    {
			    SetColumnValue("phone", value);
            }
        }
	      
        [XmlAttribute("TownshipId")]
        [Bindable(true)]
        public int? TownshipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("township_id");
		    }
            set 
		    {
			    SetColumnValue("township_id", value);
            }
        }
	      
        [XmlAttribute("AddressString")]
        [Bindable(true)]
        public string AddressString 
	    {
		    get
		    {
			    return GetColumnValue<string>("address_string");
		    }
            set 
		    {
			    SetColumnValue("address_string", value);
            }
        }
	      
        [XmlAttribute("WebUrl")]
        [Bindable(true)]
        public string WebUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("web_url");
		    }
            set 
		    {
			    SetColumnValue("web_url", value);
            }
        }
	      
        [XmlAttribute("BookingId")]
        [Bindable(true)]
        public int BookingId 
	    {
		    get
		    {
			    return GetColumnValue<int>("booking_id");
		    }
            set 
		    {
			    SetColumnValue("booking_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string SellerGuid = @"seller_guid";
            
            public static string StoreGuid = @"store_guid";
            
            public static string BookingType = @"booking_type";
            
            public static string SellerName = @"seller_name";
            
            public static string StoreName = @"store_name";
            
            public static string Phone = @"phone";
            
            public static string TownshipId = @"township_id";
            
            public static string AddressString = @"address_string";
            
            public static string WebUrl = @"web_url";
            
            public static string BookingId = @"booking_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

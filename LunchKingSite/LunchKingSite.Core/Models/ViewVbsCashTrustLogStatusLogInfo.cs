using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVbsCashTrustLogStatusLogInfo class.
    /// </summary>
    [Serializable]
    public partial class ViewVbsCashTrustLogStatusLogInfoCollection : ReadOnlyList<ViewVbsCashTrustLogStatusLogInfo, ViewVbsCashTrustLogStatusLogInfoCollection>
    {
        public ViewVbsCashTrustLogStatusLogInfoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vbs_cash_trust_log_status_log_info view.
    /// </summary>
    [Serializable]
    public partial class ViewVbsCashTrustLogStatusLogInfo : ReadOnlyRecord<ViewVbsCashTrustLogStatusLogInfo>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vbs_cash_trust_log_status_log_info", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = true;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;

                schema.Columns.Add(colvarTrustId);

                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.String;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = true;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;

                schema.Columns.Add(colvarCouponSequenceNumber);

                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;

                schema.Columns.Add(colvarStoreGuid);

                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;

                schema.Columns.Add(colvarStoreName);

                TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
                colvarProductGuid.ColumnName = "product_guid";
                colvarProductGuid.DataType = DbType.Guid;
                colvarProductGuid.MaxLength = 0;
                colvarProductGuid.AutoIncrement = false;
                colvarProductGuid.IsNullable = true;
                colvarProductGuid.IsPrimaryKey = false;
                colvarProductGuid.IsForeignKey = false;
                colvarProductGuid.IsReadOnly = false;

                schema.Columns.Add(colvarProductGuid);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarSpecialStatus = new TableSchema.TableColumn(schema);
                colvarSpecialStatus.ColumnName = "special_status";
                colvarSpecialStatus.DataType = DbType.Int32;
                colvarSpecialStatus.MaxLength = 0;
                colvarSpecialStatus.AutoIncrement = false;
                colvarSpecialStatus.IsNullable = true;
                colvarSpecialStatus.IsPrimaryKey = false;
                colvarSpecialStatus.IsForeignKey = false;
                colvarSpecialStatus.IsReadOnly = false;

                schema.Columns.Add(colvarSpecialStatus);

                TableSchema.TableColumn colvarVerifyTime = new TableSchema.TableColumn(schema);
                colvarVerifyTime.ColumnName = "verify_time";
                colvarVerifyTime.DataType = DbType.DateTime;
                colvarVerifyTime.MaxLength = 0;
                colvarVerifyTime.AutoIncrement = false;
                colvarVerifyTime.IsNullable = true;
                colvarVerifyTime.IsPrimaryKey = false;
                colvarVerifyTime.IsForeignKey = false;
                colvarVerifyTime.IsReadOnly = false;

                schema.Columns.Add(colvarVerifyTime);

                TableSchema.TableColumn colvarVerifyCreateId = new TableSchema.TableColumn(schema);
                colvarVerifyCreateId.ColumnName = "verify_create_id";
                colvarVerifyCreateId.DataType = DbType.String;
                colvarVerifyCreateId.MaxLength = 256;
                colvarVerifyCreateId.AutoIncrement = false;
                colvarVerifyCreateId.IsNullable = true;
                colvarVerifyCreateId.IsPrimaryKey = false;
                colvarVerifyCreateId.IsForeignKey = false;
                colvarVerifyCreateId.IsReadOnly = false;

                schema.Columns.Add(colvarVerifyCreateId);

                TableSchema.TableColumn colvarReturnTime = new TableSchema.TableColumn(schema);
                colvarReturnTime.ColumnName = "return_time";
                colvarReturnTime.DataType = DbType.DateTime;
                colvarReturnTime.MaxLength = 0;
                colvarReturnTime.AutoIncrement = false;
                colvarReturnTime.IsNullable = true;
                colvarReturnTime.IsPrimaryKey = false;
                colvarReturnTime.IsForeignKey = false;
                colvarReturnTime.IsReadOnly = false;

                schema.Columns.Add(colvarReturnTime);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;

                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
                colvarOrderClassification.ColumnName = "order_classification";
                colvarOrderClassification.DataType = DbType.Int32;
                colvarOrderClassification.MaxLength = 0;
                colvarOrderClassification.AutoIncrement = false;
                colvarOrderClassification.IsNullable = false;
                colvarOrderClassification.IsPrimaryKey = false;
                colvarOrderClassification.IsForeignKey = false;
                colvarOrderClassification.IsReadOnly = false;

                schema.Columns.Add(colvarOrderClassification);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 4000;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;

                schema.Columns.Add(colvarItemName);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vbs_cash_trust_log_status_log_info",schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewVbsCashTrustLogStatusLogInfo()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVbsCashTrustLogStatusLogInfo(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewVbsCashTrustLogStatusLogInfo(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewVbsCashTrustLogStatusLogInfo(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid? TrustId
        {
            get
            {
                return GetColumnValue<Guid?>("trust_id");
            }
            set
            {
                SetColumnValue("trust_id", value);
            }
        }

        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber
        {
            get
            {
                return GetColumnValue<string>("coupon_sequence_number");
            }
            set
            {
                SetColumnValue("coupon_sequence_number", value);
            }
        }

        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid
        {
            get
            {
                return GetColumnValue<Guid?>("store_guid");
            }
            set
            {
                SetColumnValue("store_guid", value);
            }
        }

        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName
        {
            get
            {
                return GetColumnValue<string>("store_name");
            }
            set
            {
                SetColumnValue("store_name", value);
            }
        }

        [XmlAttribute("ProductGuid")]
        [Bindable(true)]
        public Guid? ProductGuid
        {
            get
            {
                return GetColumnValue<Guid?>("product_guid");
            }
            set
            {
                SetColumnValue("product_guid", value);
            }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int? Status
        {
            get
            {
                return GetColumnValue<int?>("status");
            }
            set
            {
                SetColumnValue("status", value);
            }
        }

        [XmlAttribute("SpecialStatus")]
        [Bindable(true)]
        public int? SpecialStatus
        {
            get
            {
                return GetColumnValue<int?>("special_status");
            }
            set
            {
                SetColumnValue("special_status", value);
            }
        }

        [XmlAttribute("VerifyTime")]
        [Bindable(true)]
        public DateTime? VerifyTime
        {
            get
            {
                return GetColumnValue<DateTime?>("verify_time");
            }
            set
            {
                SetColumnValue("verify_time", value);
            }
        }

        [XmlAttribute("VerifyCreateId")]
        [Bindable(true)]
        public string VerifyCreateId
        {
            get
            {
                return GetColumnValue<string>("verify_create_id");
            }
            set
            {
                SetColumnValue("verify_create_id", value);
            }
        }

        [XmlAttribute("ReturnTime")]
        [Bindable(true)]
        public DateTime? ReturnTime
        {
            get
            {
                return GetColumnValue<DateTime?>("return_time");
            }
            set
            {
                SetColumnValue("return_time", value);
            }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid
        {
            get
            {
                return GetColumnValue<Guid?>("order_guid");
            }
            set
            {
                SetColumnValue("order_guid", value);
            }
        }

        [XmlAttribute("OrderClassification")]
        [Bindable(true)]
        public int OrderClassification
        {
            get
            {
                return GetColumnValue<int>("order_classification");
            }
            set
            {
                SetColumnValue("order_classification", value);
            }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get
            {
                return GetColumnValue<string>("item_name");
            }
            set
            {
                SetColumnValue("item_name", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string TrustId = @"trust_id";

            public static string CouponSequenceNumber = @"coupon_sequence_number";

            public static string StoreGuid = @"store_guid";

            public static string StoreName = @"store_name";

            public static string ProductGuid = @"product_guid";

            public static string Status = @"status";

            public static string SpecialStatus = @"special_status";

            public static string VerifyTime = @"verify_time";

            public static string VerifyCreateId = @"verify_create_id";

            public static string ReturnTime = @"return_time";

            public static string OrderGuid = @"order_guid";

            public static string OrderClassification = @"order_classification";

            public static string ItemName = @"item_name";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMgmTrustLogCoupon class.
    /// </summary>
    [Serializable]
    public partial class ViewMgmTrustLogCouponCollection : ReadOnlyList<ViewMgmTrustLogCoupon, ViewMgmTrustLogCouponCollection>
    {        
        public ViewMgmTrustLogCouponCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the View_mgm_trust_log_coupon view.
    /// </summary>
    [Serializable]
    public partial class ViewMgmTrustLogCoupon : ReadOnlyRecord<ViewMgmTrustLogCoupon>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("View_mgm_trust_log_coupon", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustId);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Int32;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarTrustProvider = new TableSchema.TableColumn(schema);
                colvarTrustProvider.ColumnName = "trust_provider";
                colvarTrustProvider.DataType = DbType.Int32;
                colvarTrustProvider.MaxLength = 0;
                colvarTrustProvider.AutoIncrement = false;
                colvarTrustProvider.IsNullable = false;
                colvarTrustProvider.IsPrimaryKey = false;
                colvarTrustProvider.IsForeignKey = false;
                colvarTrustProvider.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustProvider);
                
                TableSchema.TableColumn colvarBankStatus = new TableSchema.TableColumn(schema);
                colvarBankStatus.ColumnName = "bank_status";
                colvarBankStatus.DataType = DbType.Int32;
                colvarBankStatus.MaxLength = 0;
                colvarBankStatus.AutoIncrement = false;
                colvarBankStatus.IsNullable = false;
                colvarBankStatus.IsPrimaryKey = false;
                colvarBankStatus.IsForeignKey = false;
                colvarBankStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBankStatus);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarTrustedTime = new TableSchema.TableColumn(schema);
                colvarTrustedTime.ColumnName = "trusted_time";
                colvarTrustedTime.DataType = DbType.DateTime;
                colvarTrustedTime.MaxLength = 0;
                colvarTrustedTime.AutoIncrement = false;
                colvarTrustedTime.IsNullable = true;
                colvarTrustedTime.IsPrimaryKey = false;
                colvarTrustedTime.IsForeignKey = false;
                colvarTrustedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustedTime);
                
                TableSchema.TableColumn colvarTrustedBankTime = new TableSchema.TableColumn(schema);
                colvarTrustedBankTime.ColumnName = "trusted_bank_time";
                colvarTrustedBankTime.DataType = DbType.DateTime;
                colvarTrustedBankTime.MaxLength = 0;
                colvarTrustedBankTime.AutoIncrement = false;
                colvarTrustedBankTime.IsNullable = true;
                colvarTrustedBankTime.IsPrimaryKey = false;
                colvarTrustedBankTime.IsForeignKey = false;
                colvarTrustedBankTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustedBankTime);
                
                TableSchema.TableColumn colvarVerifiedTime = new TableSchema.TableColumn(schema);
                colvarVerifiedTime.ColumnName = "verified_time";
                colvarVerifiedTime.DataType = DbType.DateTime;
                colvarVerifiedTime.MaxLength = 0;
                colvarVerifiedTime.AutoIncrement = false;
                colvarVerifiedTime.IsNullable = true;
                colvarVerifiedTime.IsPrimaryKey = false;
                colvarVerifiedTime.IsForeignKey = false;
                colvarVerifiedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifiedTime);
                
                TableSchema.TableColumn colvarVerifiedBankTime = new TableSchema.TableColumn(schema);
                colvarVerifiedBankTime.ColumnName = "verified_bank_time";
                colvarVerifiedBankTime.DataType = DbType.DateTime;
                colvarVerifiedBankTime.MaxLength = 0;
                colvarVerifiedBankTime.AutoIncrement = false;
                colvarVerifiedBankTime.IsNullable = true;
                colvarVerifiedBankTime.IsPrimaryKey = false;
                colvarVerifiedBankTime.IsForeignKey = false;
                colvarVerifiedBankTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifiedBankTime);
                
                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.String;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = true;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponSequenceNumber);
                
                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponId);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
                colvarOrderDetailGuid.ColumnName = "order_detail_guid";
                colvarOrderDetailGuid.DataType = DbType.Guid;
                colvarOrderDetailGuid.MaxLength = 0;
                colvarOrderDetailGuid.AutoIncrement = false;
                colvarOrderDetailGuid.IsNullable = false;
                colvarOrderDetailGuid.IsPrimaryKey = false;
                colvarOrderDetailGuid.IsForeignKey = false;
                colvarOrderDetailGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailGuid);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = -1;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarPcash = new TableSchema.TableColumn(schema);
                colvarPcash.ColumnName = "pcash";
                colvarPcash.DataType = DbType.Int32;
                colvarPcash.MaxLength = 0;
                colvarPcash.AutoIncrement = false;
                colvarPcash.IsNullable = false;
                colvarPcash.IsPrimaryKey = false;
                colvarPcash.IsForeignKey = false;
                colvarPcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcash);
                
                TableSchema.TableColumn colvarScash = new TableSchema.TableColumn(schema);
                colvarScash.ColumnName = "scash";
                colvarScash.DataType = DbType.Int32;
                colvarScash.MaxLength = 0;
                colvarScash.AutoIncrement = false;
                colvarScash.IsNullable = false;
                colvarScash.IsPrimaryKey = false;
                colvarScash.IsForeignKey = false;
                colvarScash.IsReadOnly = false;
                
                schema.Columns.Add(colvarScash);
                
                TableSchema.TableColumn colvarBcash = new TableSchema.TableColumn(schema);
                colvarBcash.ColumnName = "bcash";
                colvarBcash.DataType = DbType.Int32;
                colvarBcash.MaxLength = 0;
                colvarBcash.AutoIncrement = false;
                colvarBcash.IsNullable = false;
                colvarBcash.IsPrimaryKey = false;
                colvarBcash.IsForeignKey = false;
                colvarBcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarBcash);
                
                TableSchema.TableColumn colvarCreditCard = new TableSchema.TableColumn(schema);
                colvarCreditCard.ColumnName = "credit_card";
                colvarCreditCard.DataType = DbType.Int32;
                colvarCreditCard.MaxLength = 0;
                colvarCreditCard.AutoIncrement = false;
                colvarCreditCard.IsNullable = false;
                colvarCreditCard.IsPrimaryKey = false;
                colvarCreditCard.IsForeignKey = false;
                colvarCreditCard.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreditCard);
                
                TableSchema.TableColumn colvarReportTrustedGuid = new TableSchema.TableColumn(schema);
                colvarReportTrustedGuid.ColumnName = "report_trusted_guid";
                colvarReportTrustedGuid.DataType = DbType.Guid;
                colvarReportTrustedGuid.MaxLength = 0;
                colvarReportTrustedGuid.AutoIncrement = false;
                colvarReportTrustedGuid.IsNullable = true;
                colvarReportTrustedGuid.IsPrimaryKey = false;
                colvarReportTrustedGuid.IsForeignKey = false;
                colvarReportTrustedGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarReportTrustedGuid);
                
                TableSchema.TableColumn colvarReportVerifiedGuid = new TableSchema.TableColumn(schema);
                colvarReportVerifiedGuid.ColumnName = "report_verified_guid";
                colvarReportVerifiedGuid.DataType = DbType.Guid;
                colvarReportVerifiedGuid.MaxLength = 0;
                colvarReportVerifiedGuid.AutoIncrement = false;
                colvarReportVerifiedGuid.IsNullable = true;
                colvarReportVerifiedGuid.IsPrimaryKey = false;
                colvarReportVerifiedGuid.IsForeignKey = false;
                colvarReportVerifiedGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarReportVerifiedGuid);
                
                TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
                colvarDiscountAmount.ColumnName = "discount_amount";
                colvarDiscountAmount.DataType = DbType.Int32;
                colvarDiscountAmount.MaxLength = 0;
                colvarDiscountAmount.AutoIncrement = false;
                colvarDiscountAmount.IsNullable = false;
                colvarDiscountAmount.IsPrimaryKey = false;
                colvarDiscountAmount.IsForeignKey = false;
                colvarDiscountAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountAmount);
                
                TableSchema.TableColumn colvarAtm = new TableSchema.TableColumn(schema);
                colvarAtm.ColumnName = "atm";
                colvarAtm.DataType = DbType.Int32;
                colvarAtm.MaxLength = 0;
                colvarAtm.AutoIncrement = false;
                colvarAtm.IsNullable = false;
                colvarAtm.IsPrimaryKey = false;
                colvarAtm.IsForeignKey = false;
                colvarAtm.IsReadOnly = false;
                
                schema.Columns.Add(colvarAtm);
                
                TableSchema.TableColumn colvarSpecialStatus = new TableSchema.TableColumn(schema);
                colvarSpecialStatus.ColumnName = "special_status";
                colvarSpecialStatus.DataType = DbType.Int32;
                colvarSpecialStatus.MaxLength = 0;
                colvarSpecialStatus.AutoIncrement = false;
                colvarSpecialStatus.IsNullable = false;
                colvarSpecialStatus.IsPrimaryKey = false;
                colvarSpecialStatus.IsForeignKey = false;
                colvarSpecialStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarSpecialStatus);
                
                TableSchema.TableColumn colvarStoreVerifiedGuid = new TableSchema.TableColumn(schema);
                colvarStoreVerifiedGuid.ColumnName = "store_verified_guid";
                colvarStoreVerifiedGuid.DataType = DbType.Guid;
                colvarStoreVerifiedGuid.MaxLength = 0;
                colvarStoreVerifiedGuid.AutoIncrement = false;
                colvarStoreVerifiedGuid.IsNullable = true;
                colvarStoreVerifiedGuid.IsPrimaryKey = false;
                colvarStoreVerifiedGuid.IsForeignKey = false;
                colvarStoreVerifiedGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreVerifiedGuid);
                
                TableSchema.TableColumn colvarSpecialOperatedTime = new TableSchema.TableColumn(schema);
                colvarSpecialOperatedTime.ColumnName = "special_operated_time";
                colvarSpecialOperatedTime.DataType = DbType.DateTime;
                colvarSpecialOperatedTime.MaxLength = 0;
                colvarSpecialOperatedTime.AutoIncrement = false;
                colvarSpecialOperatedTime.IsNullable = true;
                colvarSpecialOperatedTime.IsPrimaryKey = false;
                colvarSpecialOperatedTime.IsForeignKey = false;
                colvarSpecialOperatedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarSpecialOperatedTime);
                
                TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
                colvarOrderClassification.ColumnName = "order_classification";
                colvarOrderClassification.DataType = DbType.Int32;
                colvarOrderClassification.MaxLength = 0;
                colvarOrderClassification.AutoIncrement = false;
                colvarOrderClassification.IsNullable = false;
                colvarOrderClassification.IsPrimaryKey = false;
                colvarOrderClassification.IsForeignKey = false;
                colvarOrderClassification.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderClassification);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarCheckoutType = new TableSchema.TableColumn(schema);
                colvarCheckoutType.ColumnName = "checkout_type";
                colvarCheckoutType.DataType = DbType.Int32;
                colvarCheckoutType.MaxLength = 0;
                colvarCheckoutType.AutoIncrement = false;
                colvarCheckoutType.IsNullable = false;
                colvarCheckoutType.IsPrimaryKey = false;
                colvarCheckoutType.IsForeignKey = false;
                colvarCheckoutType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCheckoutType);
                
                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Int32;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = false;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarCost);
                
                TableSchema.TableColumn colvarReceivableId = new TableSchema.TableColumn(schema);
                colvarReceivableId.ColumnName = "receivable_id";
                colvarReceivableId.DataType = DbType.Int32;
                colvarReceivableId.MaxLength = 0;
                colvarReceivableId.AutoIncrement = false;
                colvarReceivableId.IsNullable = true;
                colvarReceivableId.IsPrimaryKey = false;
                colvarReceivableId.IsForeignKey = false;
                colvarReceivableId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceivableId);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = false;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarPrefix = new TableSchema.TableColumn(schema);
                colvarPrefix.ColumnName = "prefix";
                colvarPrefix.DataType = DbType.AnsiString;
                colvarPrefix.MaxLength = 10;
                colvarPrefix.AutoIncrement = false;
                colvarPrefix.IsNullable = true;
                colvarPrefix.IsPrimaryKey = false;
                colvarPrefix.IsForeignKey = false;
                colvarPrefix.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrefix);
                
                TableSchema.TableColumn colvarUninvoicedAmount = new TableSchema.TableColumn(schema);
                colvarUninvoicedAmount.ColumnName = "uninvoiced_amount";
                colvarUninvoicedAmount.DataType = DbType.Int32;
                colvarUninvoicedAmount.MaxLength = 0;
                colvarUninvoicedAmount.AutoIncrement = false;
                colvarUninvoicedAmount.IsNullable = false;
                colvarUninvoicedAmount.IsPrimaryKey = false;
                colvarUninvoicedAmount.IsForeignKey = false;
                colvarUninvoicedAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarUninvoicedAmount);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarUsageVerifiedTime = new TableSchema.TableColumn(schema);
                colvarUsageVerifiedTime.ColumnName = "usage_verified_time";
                colvarUsageVerifiedTime.DataType = DbType.DateTime;
                colvarUsageVerifiedTime.MaxLength = 0;
                colvarUsageVerifiedTime.AutoIncrement = false;
                colvarUsageVerifiedTime.IsNullable = true;
                colvarUsageVerifiedTime.IsPrimaryKey = false;
                colvarUsageVerifiedTime.IsForeignKey = false;
                colvarUsageVerifiedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUsageVerifiedTime);
                
                TableSchema.TableColumn colvarLcash = new TableSchema.TableColumn(schema);
                colvarLcash.ColumnName = "lcash";
                colvarLcash.DataType = DbType.Int32;
                colvarLcash.MaxLength = 0;
                colvarLcash.AutoIncrement = false;
                colvarLcash.IsNullable = false;
                colvarLcash.IsPrimaryKey = false;
                colvarLcash.IsForeignKey = false;
                colvarLcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarLcash);
                
                TableSchema.TableColumn colvarReturnedTime = new TableSchema.TableColumn(schema);
                colvarReturnedTime.ColumnName = "returned_time";
                colvarReturnedTime.DataType = DbType.DateTime;
                colvarReturnedTime.MaxLength = 0;
                colvarReturnedTime.AutoIncrement = false;
                colvarReturnedTime.IsNullable = true;
                colvarReturnedTime.IsPrimaryKey = false;
                colvarReturnedTime.IsForeignKey = false;
                colvarReturnedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedTime);
                
                TableSchema.TableColumn colvarTcash = new TableSchema.TableColumn(schema);
                colvarTcash.ColumnName = "tcash";
                colvarTcash.DataType = DbType.Int32;
                colvarTcash.MaxLength = 0;
                colvarTcash.AutoIncrement = false;
                colvarTcash.IsNullable = false;
                colvarTcash.IsPrimaryKey = false;
                colvarTcash.IsForeignKey = false;
                colvarTcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarTcash);
                
                TableSchema.TableColumn colvarThirdPartyPayment = new TableSchema.TableColumn(schema);
                colvarThirdPartyPayment.ColumnName = "third_party_payment";
                colvarThirdPartyPayment.DataType = DbType.Byte;
                colvarThirdPartyPayment.MaxLength = 0;
                colvarThirdPartyPayment.AutoIncrement = false;
                colvarThirdPartyPayment.IsNullable = false;
                colvarThirdPartyPayment.IsPrimaryKey = false;
                colvarThirdPartyPayment.IsForeignKey = false;
                colvarThirdPartyPayment.IsReadOnly = false;
                
                schema.Columns.Add(colvarThirdPartyPayment);
                
                TableSchema.TableColumn colvarVerifiedStoreGuid = new TableSchema.TableColumn(schema);
                colvarVerifiedStoreGuid.ColumnName = "verified_store_guid";
                colvarVerifiedStoreGuid.DataType = DbType.Guid;
                colvarVerifiedStoreGuid.MaxLength = 0;
                colvarVerifiedStoreGuid.AutoIncrement = false;
                colvarVerifiedStoreGuid.IsNullable = true;
                colvarVerifiedStoreGuid.IsPrimaryKey = false;
                colvarVerifiedStoreGuid.IsForeignKey = false;
                colvarVerifiedStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifiedStoreGuid);
                
                TableSchema.TableColumn colvarOrderDetailId = new TableSchema.TableColumn(schema);
                colvarOrderDetailId.ColumnName = "order_detail_id";
                colvarOrderDetailId.DataType = DbType.Guid;
                colvarOrderDetailId.MaxLength = 0;
                colvarOrderDetailId.AutoIncrement = false;
                colvarOrderDetailId.IsNullable = false;
                colvarOrderDetailId.IsPrimaryKey = false;
                colvarOrderDetailId.IsForeignKey = false;
                colvarOrderDetailId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailId);
                
                TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
                colvarSequenceNumber.ColumnName = "sequence_number";
                colvarSequenceNumber.DataType = DbType.AnsiString;
                colvarSequenceNumber.MaxLength = 50;
                colvarSequenceNumber.AutoIncrement = false;
                colvarSequenceNumber.IsNullable = false;
                colvarSequenceNumber.IsPrimaryKey = false;
                colvarSequenceNumber.IsForeignKey = false;
                colvarSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequenceNumber);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 50;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = false;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = 50;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = true;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescription);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarAvailable = new TableSchema.TableColumn(schema);
                colvarAvailable.ColumnName = "available";
                colvarAvailable.DataType = DbType.Boolean;
                colvarAvailable.MaxLength = 0;
                colvarAvailable.AutoIncrement = false;
                colvarAvailable.IsNullable = true;
                colvarAvailable.IsPrimaryKey = false;
                colvarAvailable.IsForeignKey = false;
                colvarAvailable.IsReadOnly = false;
                
                schema.Columns.Add(colvarAvailable);
                
                TableSchema.TableColumn colvarStoreSequence = new TableSchema.TableColumn(schema);
                colvarStoreSequence.ColumnName = "store_sequence";
                colvarStoreSequence.DataType = DbType.Int32;
                colvarStoreSequence.MaxLength = 0;
                colvarStoreSequence.AutoIncrement = false;
                colvarStoreSequence.IsNullable = true;
                colvarStoreSequence.IsPrimaryKey = false;
                colvarStoreSequence.IsForeignKey = false;
                colvarStoreSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreSequence);
                
                TableSchema.TableColumn colvarIsReservationLock = new TableSchema.TableColumn(schema);
                colvarIsReservationLock.ColumnName = "is_reservation_lock";
                colvarIsReservationLock.DataType = DbType.Boolean;
                colvarIsReservationLock.MaxLength = 0;
                colvarIsReservationLock.AutoIncrement = false;
                colvarIsReservationLock.IsNullable = false;
                colvarIsReservationLock.IsPrimaryKey = false;
                colvarIsReservationLock.IsForeignKey = false;
                colvarIsReservationLock.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsReservationLock);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("View_mgm_trust_log_coupon",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMgmTrustLogCoupon()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMgmTrustLogCoupon(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMgmTrustLogCoupon(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMgmTrustLogCoupon(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("trust_id");
		    }
            set 
		    {
			    SetColumnValue("trust_id", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public int Amount 
	    {
		    get
		    {
			    return GetColumnValue<int>("amount");
		    }
            set 
		    {
			    SetColumnValue("amount", value);
            }
        }
	      
        [XmlAttribute("TrustProvider")]
        [Bindable(true)]
        public int TrustProvider 
	    {
		    get
		    {
			    return GetColumnValue<int>("trust_provider");
		    }
            set 
		    {
			    SetColumnValue("trust_provider", value);
            }
        }
	      
        [XmlAttribute("BankStatus")]
        [Bindable(true)]
        public int BankStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("bank_status");
		    }
            set 
		    {
			    SetColumnValue("bank_status", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("TrustedTime")]
        [Bindable(true)]
        public DateTime? TrustedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("trusted_time");
		    }
            set 
		    {
			    SetColumnValue("trusted_time", value);
            }
        }
	      
        [XmlAttribute("TrustedBankTime")]
        [Bindable(true)]
        public DateTime? TrustedBankTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("trusted_bank_time");
		    }
            set 
		    {
			    SetColumnValue("trusted_bank_time", value);
            }
        }
	      
        [XmlAttribute("VerifiedTime")]
        [Bindable(true)]
        public DateTime? VerifiedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("verified_time");
		    }
            set 
		    {
			    SetColumnValue("verified_time", value);
            }
        }
	      
        [XmlAttribute("VerifiedBankTime")]
        [Bindable(true)]
        public DateTime? VerifiedBankTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("verified_bank_time");
		    }
            set 
		    {
			    SetColumnValue("verified_bank_time", value);
            }
        }
	      
        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_sequence_number");
		    }
            set 
		    {
			    SetColumnValue("coupon_sequence_number", value);
            }
        }
	      
        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("coupon_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_id", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderDetailGuid")]
        [Bindable(true)]
        public Guid OrderDetailGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_detail_guid");
		    }
            set 
		    {
			    SetColumnValue("order_detail_guid", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("Pcash")]
        [Bindable(true)]
        public int Pcash 
	    {
		    get
		    {
			    return GetColumnValue<int>("pcash");
		    }
            set 
		    {
			    SetColumnValue("pcash", value);
            }
        }
	      
        [XmlAttribute("Scash")]
        [Bindable(true)]
        public int Scash 
	    {
		    get
		    {
			    return GetColumnValue<int>("scash");
		    }
            set 
		    {
			    SetColumnValue("scash", value);
            }
        }
	      
        [XmlAttribute("Bcash")]
        [Bindable(true)]
        public int Bcash 
	    {
		    get
		    {
			    return GetColumnValue<int>("bcash");
		    }
            set 
		    {
			    SetColumnValue("bcash", value);
            }
        }
	      
        [XmlAttribute("CreditCard")]
        [Bindable(true)]
        public int CreditCard 
	    {
		    get
		    {
			    return GetColumnValue<int>("credit_card");
		    }
            set 
		    {
			    SetColumnValue("credit_card", value);
            }
        }
	      
        [XmlAttribute("ReportTrustedGuid")]
        [Bindable(true)]
        public Guid? ReportTrustedGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("report_trusted_guid");
		    }
            set 
		    {
			    SetColumnValue("report_trusted_guid", value);
            }
        }
	      
        [XmlAttribute("ReportVerifiedGuid")]
        [Bindable(true)]
        public Guid? ReportVerifiedGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("report_verified_guid");
		    }
            set 
		    {
			    SetColumnValue("report_verified_guid", value);
            }
        }
	      
        [XmlAttribute("DiscountAmount")]
        [Bindable(true)]
        public int DiscountAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("discount_amount");
		    }
            set 
		    {
			    SetColumnValue("discount_amount", value);
            }
        }
	      
        [XmlAttribute("Atm")]
        [Bindable(true)]
        public int Atm 
	    {
		    get
		    {
			    return GetColumnValue<int>("atm");
		    }
            set 
		    {
			    SetColumnValue("atm", value);
            }
        }
	      
        [XmlAttribute("SpecialStatus")]
        [Bindable(true)]
        public int SpecialStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("special_status");
		    }
            set 
		    {
			    SetColumnValue("special_status", value);
            }
        }
	      
        [XmlAttribute("StoreVerifiedGuid")]
        [Bindable(true)]
        public Guid? StoreVerifiedGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_verified_guid");
		    }
            set 
		    {
			    SetColumnValue("store_verified_guid", value);
            }
        }
	      
        [XmlAttribute("SpecialOperatedTime")]
        [Bindable(true)]
        public DateTime? SpecialOperatedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("special_operated_time");
		    }
            set 
		    {
			    SetColumnValue("special_operated_time", value);
            }
        }
	      
        [XmlAttribute("OrderClassification")]
        [Bindable(true)]
        public int OrderClassification 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_classification");
		    }
            set 
		    {
			    SetColumnValue("order_classification", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("CheckoutType")]
        [Bindable(true)]
        public int CheckoutType 
	    {
		    get
		    {
			    return GetColumnValue<int>("checkout_type");
		    }
            set 
		    {
			    SetColumnValue("checkout_type", value);
            }
        }
	      
        [XmlAttribute("Cost")]
        [Bindable(true)]
        public int Cost 
	    {
		    get
		    {
			    return GetColumnValue<int>("cost");
		    }
            set 
		    {
			    SetColumnValue("cost", value);
            }
        }
	      
        [XmlAttribute("ReceivableId")]
        [Bindable(true)]
        public int? ReceivableId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("receivable_id");
		    }
            set 
		    {
			    SetColumnValue("receivable_id", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("Prefix")]
        [Bindable(true)]
        public string Prefix 
	    {
		    get
		    {
			    return GetColumnValue<string>("prefix");
		    }
            set 
		    {
			    SetColumnValue("prefix", value);
            }
        }
	      
        [XmlAttribute("UninvoicedAmount")]
        [Bindable(true)]
        public int UninvoicedAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("uninvoiced_amount");
		    }
            set 
		    {
			    SetColumnValue("uninvoiced_amount", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("UsageVerifiedTime")]
        [Bindable(true)]
        public DateTime? UsageVerifiedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("usage_verified_time");
		    }
            set 
		    {
			    SetColumnValue("usage_verified_time", value);
            }
        }
	      
        [XmlAttribute("Lcash")]
        [Bindable(true)]
        public int Lcash 
	    {
		    get
		    {
			    return GetColumnValue<int>("lcash");
		    }
            set 
		    {
			    SetColumnValue("lcash", value);
            }
        }
	      
        [XmlAttribute("ReturnedTime")]
        [Bindable(true)]
        public DateTime? ReturnedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("returned_time");
		    }
            set 
		    {
			    SetColumnValue("returned_time", value);
            }
        }
	      
        [XmlAttribute("Tcash")]
        [Bindable(true)]
        public int Tcash 
	    {
		    get
		    {
			    return GetColumnValue<int>("tcash");
		    }
            set 
		    {
			    SetColumnValue("tcash", value);
            }
        }
	      
        [XmlAttribute("ThirdPartyPayment")]
        [Bindable(true)]
        public byte ThirdPartyPayment 
	    {
		    get
		    {
			    return GetColumnValue<byte>("third_party_payment");
		    }
            set 
		    {
			    SetColumnValue("third_party_payment", value);
            }
        }
	      
        [XmlAttribute("VerifiedStoreGuid")]
        [Bindable(true)]
        public Guid? VerifiedStoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("verified_store_guid");
		    }
            set 
		    {
			    SetColumnValue("verified_store_guid", value);
            }
        }
	      
        [XmlAttribute("OrderDetailId")]
        [Bindable(true)]
        public Guid OrderDetailId 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_detail_id");
		    }
            set 
		    {
			    SetColumnValue("order_detail_id", value);
            }
        }
	      
        [XmlAttribute("SequenceNumber")]
        [Bindable(true)]
        public string SequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("sequence_number");
		    }
            set 
		    {
			    SetColumnValue("sequence_number", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description 
	    {
		    get
		    {
			    return GetColumnValue<string>("description");
		    }
            set 
		    {
			    SetColumnValue("description", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("Available")]
        [Bindable(true)]
        public bool? Available 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("available");
		    }
            set 
		    {
			    SetColumnValue("available", value);
            }
        }
	      
        [XmlAttribute("StoreSequence")]
        [Bindable(true)]
        public int? StoreSequence 
	    {
		    get
		    {
			    return GetColumnValue<int?>("store_sequence");
		    }
            set 
		    {
			    SetColumnValue("store_sequence", value);
            }
        }
	      
        [XmlAttribute("IsReservationLock")]
        [Bindable(true)]
        public bool IsReservationLock 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_reservation_lock");
		    }
            set 
		    {
			    SetColumnValue("is_reservation_lock", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string TrustId = @"trust_id";
            
            public static string Amount = @"amount";
            
            public static string TrustProvider = @"trust_provider";
            
            public static string BankStatus = @"bank_status";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyTime = @"modify_time";
            
            public static string TrustedTime = @"trusted_time";
            
            public static string TrustedBankTime = @"trusted_bank_time";
            
            public static string VerifiedTime = @"verified_time";
            
            public static string VerifiedBankTime = @"verified_bank_time";
            
            public static string CouponSequenceNumber = @"coupon_sequence_number";
            
            public static string CouponId = @"coupon_id";
            
            public static string UserId = @"user_id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string OrderDetailGuid = @"order_detail_guid";
            
            public static string ItemName = @"item_name";
            
            public static string Pcash = @"pcash";
            
            public static string Scash = @"scash";
            
            public static string Bcash = @"bcash";
            
            public static string CreditCard = @"credit_card";
            
            public static string ReportTrustedGuid = @"report_trusted_guid";
            
            public static string ReportVerifiedGuid = @"report_verified_guid";
            
            public static string DiscountAmount = @"discount_amount";
            
            public static string Atm = @"atm";
            
            public static string SpecialStatus = @"special_status";
            
            public static string StoreVerifiedGuid = @"store_verified_guid";
            
            public static string SpecialOperatedTime = @"special_operated_time";
            
            public static string OrderClassification = @"order_classification";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string CheckoutType = @"checkout_type";
            
            public static string Cost = @"cost";
            
            public static string ReceivableId = @"receivable_id";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string Prefix = @"prefix";
            
            public static string UninvoicedAmount = @"uninvoiced_amount";
            
            public static string StoreGuid = @"store_guid";
            
            public static string UsageVerifiedTime = @"usage_verified_time";
            
            public static string Lcash = @"lcash";
            
            public static string ReturnedTime = @"returned_time";
            
            public static string Tcash = @"tcash";
            
            public static string ThirdPartyPayment = @"third_party_payment";
            
            public static string VerifiedStoreGuid = @"verified_store_guid";
            
            public static string OrderDetailId = @"order_detail_id";
            
            public static string SequenceNumber = @"sequence_number";
            
            public static string Code = @"code";
            
            public static string Description = @"description";
            
            public static string Status = @"status";
            
            public static string Available = @"available";
            
            public static string StoreSequence = @"store_sequence";
            
            public static string IsReservationLock = @"is_reservation_lock";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

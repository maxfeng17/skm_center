using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.OrderEntities
{
	[Table("view_lineshop_orderinfo")]
	public class ViewLineshopOrderinfo
	{
		public ViewLineshopOrderinfo()
		{
			Ecid = string.Empty;
			OrderId = string.Empty;
			FirstRsrc = string.Empty;
			LastRsrc = string.Empty;
			LastExternalRsrc = string.Empty;
		}

		[Column("order_guid")]
		public Guid OrderGuid { get; set; }

		[Column("ecid", TypeName = "varchar")]
		public string Ecid { get; set; }

		[Column("send_type")]
		public int SendType { get; set; }

		[Column("GUID")]
		public Guid Guid { get; set; }

		[Column("order_id", TypeName = "varchar")]
		public string OrderId { get; set; }

		[Column("order_status")]
		public int OrderStatus { get; set; }

		[Column("order_time")]
		public DateTime OrderTime { get; set; }

		[Column("order_from_type")]
		public int OrderFromType { get; set; }

		[Column("ordertotal")]
		public decimal Ordertotal { get; set; }

		[Column("first_rsrc")]
		public string FirstRsrc { get; set; }

		[Column("last_rsrc")]
		public string LastRsrc { get; set; }

		[Column("last_external_rsrc")]
		public string LastExternalRsrc { get; set; }
	}
}

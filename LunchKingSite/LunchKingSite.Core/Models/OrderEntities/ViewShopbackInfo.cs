using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.OrderEntities
{
	[Table("view_shopback_info")]
	public class ViewShopbackInfo
	{
		public ViewShopbackInfo()
		{
			Gid = string.Empty;
			OrderId = string.Empty;
			CouponSequenceNumber = string.Empty;
			ItemName = string.Empty;
			FirstRsrc = string.Empty;
			LastRsrc = string.Empty;
			LastExternalRsrc = string.Empty;
		}

		[Column("id")]
		public int Id { get; set; }

		[Column("order_guid")]
		public Guid OrderGuid { get; set; }

		[Column("bid")]
		public Guid Bid { get; set; }

		[Column("gid", TypeName = "varchar")]
		public string Gid { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("GUID")]
		public Guid Guid { get; set; }

		[Column("order_id", TypeName = "varchar")]
		public string OrderId { get; set; }

		[Column("order_status")]
		public int OrderStatus { get; set; }

		[Column("trust_id")]
		public Guid TrustId { get; set; }

		[Column("coupon_id")]
		public int? CouponId { get; set; }

		[Column("coupon_sequence_number")]
		public string CouponSequenceNumber { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("delivery_type")]
		public int DeliveryType { get; set; }

		[Column("amount")]
		public decimal Amount { get; set; }

		[Column("cash_trust_log_status")]
		public int CashTrustLogStatus { get; set; }

		[Column("order_time")]
		public DateTime OrderTime { get; set; }

		[Column("item_name")]
		public string ItemName { get; set; }

		[Column("first_rsrc")]
		public string FirstRsrc { get; set; }

		[Column("last_rsrc")]
		public string LastRsrc { get; set; }

		[Column("last_external_rsrc")]
		public string LastExternalRsrc { get; set; }

	    [Column("send_type")]
	    public int? SendType { get; set; }
    }
}

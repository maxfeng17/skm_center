using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.OrderEntities
{
	[Table("affiliates_info")]
	public class AffiliatesInfo
	{
		public AffiliatesInfo()
		{
			Gid = string.Empty;
			ReturnCode = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("trust_id")]
		public Guid TrustId { get; set; }

		[Column("order_guid")]
		public Guid OrderGuid { get; set; }

		[Column("bid")]
		public Guid Bid { get; set; }

		[Column("delivery_type")]
		public int DeliveryType { get; set; }

		[Column("gid", TypeName = "varchar")]
		public string Gid { get; set; }

		[Column("amount")]
		public decimal Amount { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("send_count")]
		public int SendCount { get; set; }

		[Column("send_type")]
		public int SendType { get; set; }

		[Column("return_code", TypeName = "varchar")]
		public string ReturnCode { get; set; }
	}
}

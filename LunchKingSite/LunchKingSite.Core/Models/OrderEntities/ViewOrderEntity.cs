﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.OrderEntities
{
	[Table("view_order_entity")]
	public class ViewOrderEntity
	{
		public ViewOrderEntity()
		{
		}

		[Column("id")]
		public Guid Id { get; set; }

		[Column("deal_id")]
		public Guid DealId { get; set; }

		[Column("main_deal_id")]
		public Guid MainDealId { get; set; }

		[Column("order_id", TypeName = "varchar")]
		public string OrderId { get; set; }

		[Column("total")]
		public decimal Total { get; set; }

		[Column("turnover")]
		public int Turnover { get; set; }

		[Column("order_status")]
		public int OrderStatus { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("order_from_type")]
		public int OrderFromType { get; set; }	

        [Column("user_id")]
		public int UserId { get; set; }	
	}
}

using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the RedeemOrder class.
	/// </summary>
    [Serializable]
	public partial class RedeemOrderCollection : RepositoryList<RedeemOrder, RedeemOrderCollection>
	{	   
		public RedeemOrderCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>RedeemOrderCollection</returns>
		public RedeemOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                RedeemOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the redeem_order table.
	/// </summary>
	[Serializable]
	public partial class RedeemOrder : RepositoryRecord<RedeemOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public RedeemOrder()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public RedeemOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("redeem_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = true;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_GUID";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 50;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);
				
				TableSchema.TableColumn colvarMemberEmail = new TableSchema.TableColumn(schema);
				colvarMemberEmail.ColumnName = "member_email";
				colvarMemberEmail.DataType = DbType.AnsiString;
				colvarMemberEmail.MaxLength = 50;
				colvarMemberEmail.AutoIncrement = false;
				colvarMemberEmail.IsNullable = false;
				colvarMemberEmail.IsPrimaryKey = false;
				colvarMemberEmail.IsForeignKey = false;
				colvarMemberEmail.IsReadOnly = false;
				colvarMemberEmail.DefaultSetting = @"";
				colvarMemberEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberEmail);
				
				TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
				colvarMemberName.ColumnName = "member_name";
				colvarMemberName.DataType = DbType.String;
				colvarMemberName.MaxLength = 50;
				colvarMemberName.AutoIncrement = false;
				colvarMemberName.IsNullable = false;
				colvarMemberName.IsPrimaryKey = false;
				colvarMemberName.IsForeignKey = false;
				colvarMemberName.IsReadOnly = false;
				colvarMemberName.DefaultSetting = @"";
				colvarMemberName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberName);
				
				TableSchema.TableColumn colvarPhoneNumber = new TableSchema.TableColumn(schema);
				colvarPhoneNumber.ColumnName = "phone_number";
				colvarPhoneNumber.DataType = DbType.AnsiString;
				colvarPhoneNumber.MaxLength = 50;
				colvarPhoneNumber.AutoIncrement = false;
				colvarPhoneNumber.IsNullable = true;
				colvarPhoneNumber.IsPrimaryKey = false;
				colvarPhoneNumber.IsForeignKey = false;
				colvarPhoneNumber.IsReadOnly = false;
				colvarPhoneNumber.DefaultSetting = @"";
				colvarPhoneNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhoneNumber);
				
				TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
				colvarMobileNumber.ColumnName = "mobile_number";
				colvarMobileNumber.DataType = DbType.AnsiString;
				colvarMobileNumber.MaxLength = 50;
				colvarMobileNumber.AutoIncrement = false;
				colvarMobileNumber.IsNullable = true;
				colvarMobileNumber.IsPrimaryKey = false;
				colvarMobileNumber.IsForeignKey = false;
				colvarMobileNumber.IsReadOnly = false;
				colvarMobileNumber.DefaultSetting = @"";
				colvarMobileNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileNumber);
				
				TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
				colvarDeliveryAddress.ColumnName = "delivery_address";
				colvarDeliveryAddress.DataType = DbType.String;
				colvarDeliveryAddress.MaxLength = 200;
				colvarDeliveryAddress.AutoIncrement = false;
				colvarDeliveryAddress.IsNullable = true;
				colvarDeliveryAddress.IsPrimaryKey = false;
				colvarDeliveryAddress.IsForeignKey = false;
				colvarDeliveryAddress.IsReadOnly = false;
				colvarDeliveryAddress.DefaultSetting = @"";
				colvarDeliveryAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryAddress);
				
				TableSchema.TableColumn colvarDeliveryTime = new TableSchema.TableColumn(schema);
				colvarDeliveryTime.ColumnName = "delivery_time";
				colvarDeliveryTime.DataType = DbType.DateTime;
				colvarDeliveryTime.MaxLength = 0;
				colvarDeliveryTime.AutoIncrement = false;
				colvarDeliveryTime.IsNullable = true;
				colvarDeliveryTime.IsPrimaryKey = false;
				colvarDeliveryTime.IsForeignKey = false;
				colvarDeliveryTime.IsReadOnly = false;
				colvarDeliveryTime.DefaultSetting = @"";
				colvarDeliveryTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryTime);
				
				TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
				colvarTotal.ColumnName = "total";
				colvarTotal.DataType = DbType.Currency;
				colvarTotal.MaxLength = 0;
				colvarTotal.AutoIncrement = false;
				colvarTotal.IsNullable = false;
				colvarTotal.IsPrimaryKey = false;
				colvarTotal.IsForeignKey = false;
				colvarTotal.IsReadOnly = false;
				colvarTotal.DefaultSetting = @"";
				colvarTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotal);
				
				TableSchema.TableColumn colvarUserMemo = new TableSchema.TableColumn(schema);
				colvarUserMemo.ColumnName = "user_memo";
				colvarUserMemo.DataType = DbType.String;
				colvarUserMemo.MaxLength = 200;
				colvarUserMemo.AutoIncrement = false;
				colvarUserMemo.IsNullable = true;
				colvarUserMemo.IsPrimaryKey = false;
				colvarUserMemo.IsForeignKey = false;
				colvarUserMemo.IsReadOnly = false;
				colvarUserMemo.DefaultSetting = @"";
				colvarUserMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserMemo);
				
				TableSchema.TableColumn colvarOrderMemo = new TableSchema.TableColumn(schema);
				colvarOrderMemo.ColumnName = "order_memo";
				colvarOrderMemo.DataType = DbType.String;
				colvarOrderMemo.MaxLength = 200;
				colvarOrderMemo.AutoIncrement = false;
				colvarOrderMemo.IsNullable = true;
				colvarOrderMemo.IsPrimaryKey = false;
				colvarOrderMemo.IsForeignKey = false;
				colvarOrderMemo.IsReadOnly = false;
				colvarOrderMemo.DefaultSetting = @"";
				colvarOrderMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderMemo);
				
				TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
				colvarOrderStatus.ColumnName = "order_status";
				colvarOrderStatus.DataType = DbType.Int32;
				colvarOrderStatus.MaxLength = 0;
				colvarOrderStatus.AutoIncrement = false;
				colvarOrderStatus.IsNullable = false;
				colvarOrderStatus.IsPrimaryKey = false;
				colvarOrderStatus.IsForeignKey = false;
				colvarOrderStatus.IsReadOnly = false;
				colvarOrderStatus.DefaultSetting = @"";
				colvarOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStatus);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 30;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = false;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = true;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				
					colvarCityId.ForeignKeyTableName = "city";
				schema.Columns.Add(colvarCityId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("redeem_order",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId 
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName 
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}
		  
		[XmlAttribute("MemberEmail")]
		[Bindable(true)]
		public string MemberEmail 
		{
			get { return GetColumnValue<string>(Columns.MemberEmail); }
			set { SetColumnValue(Columns.MemberEmail, value); }
		}
		  
		[XmlAttribute("MemberName")]
		[Bindable(true)]
		public string MemberName 
		{
			get { return GetColumnValue<string>(Columns.MemberName); }
			set { SetColumnValue(Columns.MemberName, value); }
		}
		  
		[XmlAttribute("PhoneNumber")]
		[Bindable(true)]
		public string PhoneNumber 
		{
			get { return GetColumnValue<string>(Columns.PhoneNumber); }
			set { SetColumnValue(Columns.PhoneNumber, value); }
		}
		  
		[XmlAttribute("MobileNumber")]
		[Bindable(true)]
		public string MobileNumber 
		{
			get { return GetColumnValue<string>(Columns.MobileNumber); }
			set { SetColumnValue(Columns.MobileNumber, value); }
		}
		  
		[XmlAttribute("DeliveryAddress")]
		[Bindable(true)]
		public string DeliveryAddress 
		{
			get { return GetColumnValue<string>(Columns.DeliveryAddress); }
			set { SetColumnValue(Columns.DeliveryAddress, value); }
		}
		  
		[XmlAttribute("DeliveryTime")]
		[Bindable(true)]
		public DateTime? DeliveryTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.DeliveryTime); }
			set { SetColumnValue(Columns.DeliveryTime, value); }
		}
		  
		[XmlAttribute("Total")]
		[Bindable(true)]
		public decimal Total 
		{
			get { return GetColumnValue<decimal>(Columns.Total); }
			set { SetColumnValue(Columns.Total, value); }
		}
		  
		[XmlAttribute("UserMemo")]
		[Bindable(true)]
		public string UserMemo 
		{
			get { return GetColumnValue<string>(Columns.UserMemo); }
			set { SetColumnValue(Columns.UserMemo, value); }
		}
		  
		[XmlAttribute("OrderMemo")]
		[Bindable(true)]
		public string OrderMemo 
		{
			get { return GetColumnValue<string>(Columns.OrderMemo); }
			set { SetColumnValue(Columns.OrderMemo, value); }
		}
		  
		[XmlAttribute("OrderStatus")]
		[Bindable(true)]
		public int OrderStatus 
		{
			get { return GetColumnValue<int>(Columns.OrderStatus); }
			set { SetColumnValue(Columns.OrderStatus, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int CityId 
		{
			get { return GetColumnValue<int>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberEmailColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberNameColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn PhoneNumberColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn MobileNumberColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryAddressColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn UserMemoColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderMemoColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderStatusColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string OrderId = @"order_id";
			 public static string SellerGuid = @"seller_GUID";
			 public static string SellerName = @"seller_name";
			 public static string MemberEmail = @"member_email";
			 public static string MemberName = @"member_name";
			 public static string PhoneNumber = @"phone_number";
			 public static string MobileNumber = @"mobile_number";
			 public static string DeliveryAddress = @"delivery_address";
			 public static string DeliveryTime = @"delivery_time";
			 public static string Total = @"total";
			 public static string UserMemo = @"user_memo";
			 public static string OrderMemo = @"order_memo";
			 public static string OrderStatus = @"order_status";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string CityId = @"city_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

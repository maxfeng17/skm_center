using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProductMappingOther class.
    /// </summary>
    [Serializable]
    public partial class ProductMappingOtherCollection : RepositoryList<ProductMappingOther, ProductMappingOtherCollection>
    {
        public ProductMappingOtherCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProductMappingOtherCollection</returns>
        public ProductMappingOtherCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProductMappingOther o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }


    /// <summary>
    /// This is an ActiveRecord class which wraps the product_mapping_others table.
    /// </summary>

    [Serializable]
    public partial class ProductMappingOther : RepositoryRecord<ProductMappingOther>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProductMappingOther()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProductMappingOther(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("product_mapping_others", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarMainBusinessHourId = new TableSchema.TableColumn(schema);
                colvarMainBusinessHourId.ColumnName = "main_business_hour_id";
                colvarMainBusinessHourId.DataType = DbType.Guid;
                colvarMainBusinessHourId.MaxLength = 0;
                colvarMainBusinessHourId.AutoIncrement = false;
                colvarMainBusinessHourId.IsNullable = false;
                colvarMainBusinessHourId.IsPrimaryKey = true;
                colvarMainBusinessHourId.IsForeignKey = false;
                colvarMainBusinessHourId.IsReadOnly = false;
                colvarMainBusinessHourId.DefaultSetting = @"";
                colvarMainBusinessHourId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMainBusinessHourId);

                TableSchema.TableColumn colvarAssociationBusinessHourId = new TableSchema.TableColumn(schema);
                colvarAssociationBusinessHourId.ColumnName = "association_business_hour_id";
                colvarAssociationBusinessHourId.DataType = DbType.Guid;
                colvarAssociationBusinessHourId.MaxLength = 0;
                colvarAssociationBusinessHourId.AutoIncrement = false;
                colvarAssociationBusinessHourId.IsNullable = false;
                colvarAssociationBusinessHourId.IsPrimaryKey = true;
                colvarAssociationBusinessHourId.IsForeignKey = false;
                colvarAssociationBusinessHourId.IsReadOnly = false;
                colvarAssociationBusinessHourId.DefaultSetting = @"";
                colvarAssociationBusinessHourId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAssociationBusinessHourId);

                TableSchema.TableColumn colvarWeight = new TableSchema.TableColumn(schema);
                colvarWeight.ColumnName = "weight";
                colvarWeight.DataType = DbType.Int32;
                colvarWeight.MaxLength = 0;
                colvarWeight.AutoIncrement = false;
                colvarWeight.IsNullable = false;
                colvarWeight.IsPrimaryKey = false;
                colvarWeight.IsForeignKey = false;
                colvarWeight.IsReadOnly = false;
                colvarWeight.DefaultSetting = @"";
                colvarWeight.ForeignKeyTableName = "";
                schema.Columns.Add(colvarWeight);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("product_mapping_others", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("MainBusinessHourId")]
        [Bindable(true)]
        public Guid MainBusinessHourId
        {
            get { return GetColumnValue<Guid>(Columns.MainBusinessHourId); }
            set { SetColumnValue(Columns.MainBusinessHourId, value); }
        }

        [XmlAttribute("AssociationBusinessHourId")]
        [Bindable(true)]
        public Guid AssociationBusinessHourId
        {
            get { return GetColumnValue<Guid>(Columns.AssociationBusinessHourId); }
            set { SetColumnValue(Columns.AssociationBusinessHourId, value); }
        }

        [XmlAttribute("Weight")]
        [Bindable(true)]
        public int Weight
        {
            get { return GetColumnValue<int>(Columns.Weight); }
            set { SetColumnValue(Columns.Weight, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn MainBusinessHourIdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn AssociationBusinessHourIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn WeightColumn
        {
            get { return Schema.Columns[2]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string MainBusinessHourId = @"main_business_hour_id";
            public static string AssociationBusinessHourId = @"association_business_hour_id";
            public static string Weight = @"weight";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewCustomerServiceOutsideLogCollection : ReadOnlyList<ViewCustomerServiceOutsideLog, ViewCustomerServiceOutsideLogCollection>
	{
			public ViewCustomerServiceOutsideLogCollection() {}

	}

	[Serializable]
	public partial class ViewCustomerServiceOutsideLog : ReadOnlyRecord<ViewCustomerServiceOutsideLog>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewCustomerServiceOutsideLog()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewCustomerServiceOutsideLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewCustomerServiceOutsideLog(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewCustomerServiceOutsideLog(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_customer_service_outside_log", TableType.View, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = false;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = false;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarServiceNo = new TableSchema.TableColumn(schema);
				colvarServiceNo.ColumnName = "service_no";
				colvarServiceNo.DataType = DbType.AnsiString;
				colvarServiceNo.MaxLength = 50;
				colvarServiceNo.AutoIncrement = false;
				colvarServiceNo.IsNullable = false;
				colvarServiceNo.IsPrimaryKey = false;
				colvarServiceNo.IsForeignKey = false;
				colvarServiceNo.IsReadOnly = false;
				colvarServiceNo.DefaultSetting = @"";
				colvarServiceNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceNo);

				TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
				colvarContent.ColumnName = "content";
				colvarContent.DataType = DbType.String;
				colvarContent.MaxLength = 2147483647;
				colvarContent.AutoIncrement = false;
				colvarContent.IsNullable = true;
				colvarContent.IsPrimaryKey = false;
				colvarContent.IsForeignKey = false;
				colvarContent.IsReadOnly = false;
				colvarContent.DefaultSetting = @"";
				colvarContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContent);

				TableSchema.TableColumn colvarFile = new TableSchema.TableColumn(schema);
				colvarFile.ColumnName = "file";
				colvarFile.DataType = DbType.String;
				colvarFile.MaxLength = 100;
				colvarFile.AutoIncrement = false;
				colvarFile.IsNullable = true;
				colvarFile.IsPrimaryKey = false;
				colvarFile.IsForeignKey = false;
				colvarFile.IsReadOnly = false;
				colvarFile.DefaultSetting = @"";
				colvarFile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFile);

				TableSchema.TableColumn colvarCreateName = new TableSchema.TableColumn(schema);
				colvarCreateName.ColumnName = "create_name";
				colvarCreateName.DataType = DbType.String;
				colvarCreateName.MaxLength = 100;
				colvarCreateName.AutoIncrement = false;
				colvarCreateName.IsNullable = true;
				colvarCreateName.IsPrimaryKey = false;
				colvarCreateName.IsForeignKey = false;
				colvarCreateName.IsReadOnly = false;
				colvarCreateName.DefaultSetting = @"";
				colvarCreateName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateName);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 100;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = false;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);

				TableSchema.TableColumn colvarIsRead = new TableSchema.TableColumn(schema);
				colvarIsRead.ColumnName = "is_read";
				colvarIsRead.DataType = DbType.Int32;
				colvarIsRead.MaxLength = 0;
				colvarIsRead.AutoIncrement = false;
				colvarIsRead.IsNullable = true;
				colvarIsRead.IsPrimaryKey = false;
				colvarIsRead.IsForeignKey = false;
				colvarIsRead.IsReadOnly = false;
				colvarIsRead.DefaultSetting = @"";
				colvarIsRead.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsRead);

				TableSchema.TableColumn colvarMethod = new TableSchema.TableColumn(schema);
				colvarMethod.ColumnName = "method";
				colvarMethod.DataType = DbType.Int32;
				colvarMethod.MaxLength = 0;
				colvarMethod.AutoIncrement = false;
				colvarMethod.IsNullable = true;
				colvarMethod.IsPrimaryKey = false;
				colvarMethod.IsForeignKey = false;
				colvarMethod.IsReadOnly = false;
				colvarMethod.DefaultSetting = @"";
				colvarMethod.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMethod);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = true;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarParentServiceNo = new TableSchema.TableColumn(schema);
				colvarParentServiceNo.ColumnName = "parent_service_no";
				colvarParentServiceNo.DataType = DbType.AnsiString;
				colvarParentServiceNo.MaxLength = 50;
				colvarParentServiceNo.AutoIncrement = false;
				colvarParentServiceNo.IsNullable = true;
				colvarParentServiceNo.IsPrimaryKey = false;
				colvarParentServiceNo.IsForeignKey = false;
				colvarParentServiceNo.IsReadOnly = false;
				colvarParentServiceNo.DefaultSetting = @"";
				colvarParentServiceNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentServiceNo);

				TableSchema.TableColumn colvarTsMemberNo = new TableSchema.TableColumn(schema);
				colvarTsMemberNo.ColumnName = "ts_member_no";
				colvarTsMemberNo.DataType = DbType.AnsiString;
				colvarTsMemberNo.MaxLength = 80;
				colvarTsMemberNo.AutoIncrement = false;
				colvarTsMemberNo.IsNullable = true;
				colvarTsMemberNo.IsPrimaryKey = false;
				colvarTsMemberNo.IsForeignKey = false;
				colvarTsMemberNo.IsReadOnly = false;
				colvarTsMemberNo.DefaultSetting = @"";
				colvarTsMemberNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTsMemberNo);

				TableSchema.TableColumn colvarIssueFromType = new TableSchema.TableColumn(schema);
				colvarIssueFromType.ColumnName = "issue_from_type";
				colvarIssueFromType.DataType = DbType.Int32;
				colvarIssueFromType.MaxLength = 0;
				colvarIssueFromType.AutoIncrement = false;
				colvarIssueFromType.IsNullable = true;
				colvarIssueFromType.IsPrimaryKey = false;
				colvarIssueFromType.IsForeignKey = false;
				colvarIssueFromType.IsReadOnly = false;
				colvarIssueFromType.DefaultSetting = @"";
				colvarIssueFromType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIssueFromType);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_customer_service_outside_log",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("ServiceNo")]
		[Bindable(true)]
		public string ServiceNo
		{
			get { return GetColumnValue<string>(Columns.ServiceNo); }
			set { SetColumnValue(Columns.ServiceNo, value); }
		}

		[XmlAttribute("Content")]
		[Bindable(true)]
		public string Content
		{
			get { return GetColumnValue<string>(Columns.Content); }
			set { SetColumnValue(Columns.Content, value); }
		}

		[XmlAttribute("File")]
		[Bindable(true)]
		public string File
		{
			get { return GetColumnValue<string>(Columns.File); }
			set { SetColumnValue(Columns.File, value); }
		}

		[XmlAttribute("CreateName")]
		[Bindable(true)]
		public string CreateName
		{
			get { return GetColumnValue<string>(Columns.CreateName); }
			set { SetColumnValue(Columns.CreateName, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}

		[XmlAttribute("IsRead")]
		[Bindable(true)]
		public int? IsRead
		{
			get { return GetColumnValue<int?>(Columns.IsRead); }
			set { SetColumnValue(Columns.IsRead, value); }
		}

		[XmlAttribute("Method")]
		[Bindable(true)]
		public int? Method
		{
			get { return GetColumnValue<int?>(Columns.Method); }
			set { SetColumnValue(Columns.Method, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int? UserId
		{
			get { return GetColumnValue<int?>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("ParentServiceNo")]
		[Bindable(true)]
		public string ParentServiceNo
		{
			get { return GetColumnValue<string>(Columns.ParentServiceNo); }
			set { SetColumnValue(Columns.ParentServiceNo, value); }
		}

		[XmlAttribute("TsMemberNo")]
		[Bindable(true)]
		public string TsMemberNo
		{
			get { return GetColumnValue<string>(Columns.TsMemberNo); }
			set { SetColumnValue(Columns.TsMemberNo, value); }
		}

		[XmlAttribute("IssueFromType")]
		[Bindable(true)]
		public int? IssueFromType
		{
			get { return GetColumnValue<int?>(Columns.IssueFromType); }
			set { SetColumnValue(Columns.IssueFromType, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn ServiceNoColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn ContentColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn FileColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn CreateNameColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn NameColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn IsReadColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn MethodColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn ParentServiceNoColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn TsMemberNoColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn IssueFromTypeColumn
		{
			get { return Schema.Columns[13]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string ServiceNo = @"service_no";
			public static string Content = @"content";
			public static string File = @"file";
			public static string CreateName = @"create_name";
			public static string CreateTime = @"create_time";
			public static string Name = @"name";
			public static string IsRead = @"is_read";
			public static string Method = @"method";
			public static string UserId = @"user_id";
			public static string OrderGuid = @"order_guid";
			public static string ParentServiceNo = @"parent_service_no";
			public static string TsMemberNo = @"ts_member_no";
			public static string IssueFromType = @"issue_from_type";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewLiontravelOrderReturnFormList class.
    /// </summary>
    [Serializable]
    public partial class ViewLiontravelOrderReturnFormListCollection : ReadOnlyList<ViewLiontravelOrderReturnFormList, ViewLiontravelOrderReturnFormListCollection>
    {        
        public ViewLiontravelOrderReturnFormListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_liontravel_order_return_form_list view.
    /// </summary>
    [Serializable]
    public partial class ViewLiontravelOrderReturnFormList : ReadOnlyRecord<ViewLiontravelOrderReturnFormList>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_liontravel_order_return_form_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
                colvarProductGuid.ColumnName = "product_guid";
                colvarProductGuid.DataType = DbType.Guid;
                colvarProductGuid.MaxLength = 0;
                colvarProductGuid.AutoIncrement = false;
                colvarProductGuid.IsNullable = false;
                colvarProductGuid.IsPrimaryKey = false;
                colvarProductGuid.IsForeignKey = false;
                colvarProductGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductGuid);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = false;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarComboPackCount = new TableSchema.TableColumn(schema);
                colvarComboPackCount.ColumnName = "combo_pack_count";
                colvarComboPackCount.DataType = DbType.Int32;
                colvarComboPackCount.MaxLength = 0;
                colvarComboPackCount.AutoIncrement = false;
                colvarComboPackCount.IsNullable = false;
                colvarComboPackCount.IsPrimaryKey = false;
                colvarComboPackCount.IsForeignKey = false;
                colvarComboPackCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarComboPackCount);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 500;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = false;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.DateTime;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseEndTime);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarPhoneNumber = new TableSchema.TableColumn(schema);
                colvarPhoneNumber.ColumnName = "phone_number";
                colvarPhoneNumber.DataType = DbType.AnsiString;
                colvarPhoneNumber.MaxLength = 50;
                colvarPhoneNumber.AutoIncrement = false;
                colvarPhoneNumber.IsNullable = true;
                colvarPhoneNumber.IsPrimaryKey = false;
                colvarPhoneNumber.IsForeignKey = false;
                colvarPhoneNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhoneNumber);
                
                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryAddress);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = false;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarOrderShipId = new TableSchema.TableColumn(schema);
                colvarOrderShipId.ColumnName = "order_ship_id";
                colvarOrderShipId.DataType = DbType.Int32;
                colvarOrderShipId.MaxLength = 0;
                colvarOrderShipId.AutoIncrement = false;
                colvarOrderShipId.IsNullable = true;
                colvarOrderShipId.IsPrimaryKey = false;
                colvarOrderShipId.IsForeignKey = false;
                colvarOrderShipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderShipId);
                
                TableSchema.TableColumn colvarShipTime = new TableSchema.TableColumn(schema);
                colvarShipTime.ColumnName = "ship_time";
                colvarShipTime.DataType = DbType.DateTime;
                colvarShipTime.MaxLength = 0;
                colvarShipTime.AutoIncrement = false;
                colvarShipTime.IsNullable = true;
                colvarShipTime.IsPrimaryKey = false;
                colvarShipTime.IsForeignKey = false;
                colvarShipTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipTime);
                
                TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
                colvarReturnFormId.ColumnName = "return_form_id";
                colvarReturnFormId.DataType = DbType.Int32;
                colvarReturnFormId.MaxLength = 0;
                colvarReturnFormId.AutoIncrement = false;
                colvarReturnFormId.IsNullable = false;
                colvarReturnFormId.IsPrimaryKey = false;
                colvarReturnFormId.IsForeignKey = false;
                colvarReturnFormId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnFormId);
                
                TableSchema.TableColumn colvarReturnApplicationTime = new TableSchema.TableColumn(schema);
                colvarReturnApplicationTime.ColumnName = "return_application_time";
                colvarReturnApplicationTime.DataType = DbType.DateTime;
                colvarReturnApplicationTime.MaxLength = 0;
                colvarReturnApplicationTime.AutoIncrement = false;
                colvarReturnApplicationTime.IsNullable = false;
                colvarReturnApplicationTime.IsPrimaryKey = false;
                colvarReturnApplicationTime.IsForeignKey = false;
                colvarReturnApplicationTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnApplicationTime);
                
                TableSchema.TableColumn colvarReturnReason = new TableSchema.TableColumn(schema);
                colvarReturnReason.ColumnName = "return_reason";
                colvarReturnReason.DataType = DbType.String;
                colvarReturnReason.MaxLength = 4000;
                colvarReturnReason.AutoIncrement = false;
                colvarReturnReason.IsNullable = true;
                colvarReturnReason.IsPrimaryKey = false;
                colvarReturnReason.IsForeignKey = false;
                colvarReturnReason.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnReason);
                
                TableSchema.TableColumn colvarProgressStatus = new TableSchema.TableColumn(schema);
                colvarProgressStatus.ColumnName = "progress_status";
                colvarProgressStatus.DataType = DbType.Int32;
                colvarProgressStatus.MaxLength = 0;
                colvarProgressStatus.AutoIncrement = false;
                colvarProgressStatus.IsNullable = false;
                colvarProgressStatus.IsPrimaryKey = false;
                colvarProgressStatus.IsForeignKey = false;
                colvarProgressStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarProgressStatus);
                
                TableSchema.TableColumn colvarVendorProgressStatus = new TableSchema.TableColumn(schema);
                colvarVendorProgressStatus.ColumnName = "vendor_progress_status";
                colvarVendorProgressStatus.DataType = DbType.Int32;
                colvarVendorProgressStatus.MaxLength = 0;
                colvarVendorProgressStatus.AutoIncrement = false;
                colvarVendorProgressStatus.IsNullable = true;
                colvarVendorProgressStatus.IsPrimaryKey = false;
                colvarVendorProgressStatus.IsForeignKey = false;
                colvarVendorProgressStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorProgressStatus);
                
                TableSchema.TableColumn colvarVendorMemo = new TableSchema.TableColumn(schema);
                colvarVendorMemo.ColumnName = "vendor_memo";
                colvarVendorMemo.DataType = DbType.String;
                colvarVendorMemo.MaxLength = 30;
                colvarVendorMemo.AutoIncrement = false;
                colvarVendorMemo.IsNullable = true;
                colvarVendorMemo.IsPrimaryKey = false;
                colvarVendorMemo.IsForeignKey = false;
                colvarVendorMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorMemo);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarRefundType = new TableSchema.TableColumn(schema);
                colvarRefundType.ColumnName = "refund_type";
                colvarRefundType.DataType = DbType.Int32;
                colvarRefundType.MaxLength = 0;
                colvarRefundType.AutoIncrement = false;
                colvarRefundType.IsNullable = false;
                colvarRefundType.IsPrimaryKey = false;
                colvarRefundType.IsForeignKey = false;
                colvarRefundType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRefundType);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = false;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
                colvarSellerEmail.ColumnName = "seller_email";
                colvarSellerEmail.DataType = DbType.AnsiString;
                colvarSellerEmail.MaxLength = 200;
                colvarSellerEmail.AutoIncrement = false;
                colvarSellerEmail.IsNullable = true;
                colvarSellerEmail.IsPrimaryKey = false;
                colvarSellerEmail.IsForeignKey = false;
                colvarSellerEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerEmail);
                
                TableSchema.TableColumn colvarReturnedPersonName = new TableSchema.TableColumn(schema);
                colvarReturnedPersonName.ColumnName = "returned_person_name";
                colvarReturnedPersonName.DataType = DbType.String;
                colvarReturnedPersonName.MaxLength = 100;
                colvarReturnedPersonName.AutoIncrement = false;
                colvarReturnedPersonName.IsNullable = true;
                colvarReturnedPersonName.IsPrimaryKey = false;
                colvarReturnedPersonName.IsForeignKey = false;
                colvarReturnedPersonName.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonName);
                
                TableSchema.TableColumn colvarReturnedPersonTel = new TableSchema.TableColumn(schema);
                colvarReturnedPersonTel.ColumnName = "returned_person_tel";
                colvarReturnedPersonTel.DataType = DbType.AnsiString;
                colvarReturnedPersonTel.MaxLength = 100;
                colvarReturnedPersonTel.AutoIncrement = false;
                colvarReturnedPersonTel.IsNullable = true;
                colvarReturnedPersonTel.IsPrimaryKey = false;
                colvarReturnedPersonTel.IsForeignKey = false;
                colvarReturnedPersonTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonTel);
                
                TableSchema.TableColumn colvarReturnedPersonEmail = new TableSchema.TableColumn(schema);
                colvarReturnedPersonEmail.ColumnName = "returned_person_email";
                colvarReturnedPersonEmail.DataType = DbType.AnsiString;
                colvarReturnedPersonEmail.MaxLength = 200;
                colvarReturnedPersonEmail.AutoIncrement = false;
                colvarReturnedPersonEmail.IsNullable = true;
                colvarReturnedPersonEmail.IsPrimaryKey = false;
                colvarReturnedPersonEmail.IsForeignKey = false;
                colvarReturnedPersonEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonEmail);
                
                TableSchema.TableColumn colvarEmpName = new TableSchema.TableColumn(schema);
                colvarEmpName.ColumnName = "emp_name";
                colvarEmpName.DataType = DbType.String;
                colvarEmpName.MaxLength = 50;
                colvarEmpName.AutoIncrement = false;
                colvarEmpName.IsNullable = true;
                colvarEmpName.IsPrimaryKey = false;
                colvarEmpName.IsForeignKey = false;
                colvarEmpName.IsReadOnly = false;
                
                schema.Columns.Add(colvarEmpName);
                
                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = false;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType);
                
                TableSchema.TableColumn colvarLabelIconList = new TableSchema.TableColumn(schema);
                colvarLabelIconList.ColumnName = "label_icon_list";
                colvarLabelIconList.DataType = DbType.AnsiString;
                colvarLabelIconList.MaxLength = 255;
                colvarLabelIconList.AutoIncrement = false;
                colvarLabelIconList.IsNullable = true;
                colvarLabelIconList.IsPrimaryKey = false;
                colvarLabelIconList.IsForeignKey = false;
                colvarLabelIconList.IsReadOnly = false;
                
                schema.Columns.Add(colvarLabelIconList);
                
                TableSchema.TableColumn colvarRelatedOrderId = new TableSchema.TableColumn(schema);
                colvarRelatedOrderId.ColumnName = "related_order_id";
                colvarRelatedOrderId.DataType = DbType.AnsiString;
                colvarRelatedOrderId.MaxLength = 50;
                colvarRelatedOrderId.AutoIncrement = false;
                colvarRelatedOrderId.IsNullable = false;
                colvarRelatedOrderId.IsPrimaryKey = false;
                colvarRelatedOrderId.IsForeignKey = false;
                colvarRelatedOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarRelatedOrderId);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = false;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_liontravel_order_return_form_list",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewLiontravelOrderReturnFormList()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewLiontravelOrderReturnFormList(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewLiontravelOrderReturnFormList(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewLiontravelOrderReturnFormList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ProductGuid")]
        [Bindable(true)]
        public Guid ProductGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("product_guid");
		    }
            set 
		    {
			    SetColumnValue("product_guid", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("ComboPackCount")]
        [Bindable(true)]
        public int ComboPackCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("combo_pack_count");
		    }
            set 
		    {
			    SetColumnValue("combo_pack_count", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_name");
		    }
            set 
		    {
			    SetColumnValue("deal_name", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public DateTime? UseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_end_time");
		    }
            set 
		    {
			    SetColumnValue("use_end_time", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("PhoneNumber")]
        [Bindable(true)]
        public string PhoneNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("phone_number");
		    }
            set 
		    {
			    SetColumnValue("phone_number", value);
            }
        }
	      
        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_address");
		    }
            set 
		    {
			    SetColumnValue("delivery_address", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal Total 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("OrderShipId")]
        [Bindable(true)]
        public int? OrderShipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_ship_id");
		    }
            set 
		    {
			    SetColumnValue("order_ship_id", value);
            }
        }
	      
        [XmlAttribute("ShipTime")]
        [Bindable(true)]
        public DateTime? ShipTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ship_time");
		    }
            set 
		    {
			    SetColumnValue("ship_time", value);
            }
        }
	      
        [XmlAttribute("ReturnFormId")]
        [Bindable(true)]
        public int ReturnFormId 
	    {
		    get
		    {
			    return GetColumnValue<int>("return_form_id");
		    }
            set 
		    {
			    SetColumnValue("return_form_id", value);
            }
        }
	      
        [XmlAttribute("ReturnApplicationTime")]
        [Bindable(true)]
        public DateTime ReturnApplicationTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("return_application_time");
		    }
            set 
		    {
			    SetColumnValue("return_application_time", value);
            }
        }
	      
        [XmlAttribute("ReturnReason")]
        [Bindable(true)]
        public string ReturnReason 
	    {
		    get
		    {
			    return GetColumnValue<string>("return_reason");
		    }
            set 
		    {
			    SetColumnValue("return_reason", value);
            }
        }
	      
        [XmlAttribute("ProgressStatus")]
        [Bindable(true)]
        public int ProgressStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("progress_status");
		    }
            set 
		    {
			    SetColumnValue("progress_status", value);
            }
        }
	      
        [XmlAttribute("VendorProgressStatus")]
        [Bindable(true)]
        public int? VendorProgressStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vendor_progress_status");
		    }
            set 
		    {
			    SetColumnValue("vendor_progress_status", value);
            }
        }
	      
        [XmlAttribute("VendorMemo")]
        [Bindable(true)]
        public string VendorMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("vendor_memo");
		    }
            set 
		    {
			    SetColumnValue("vendor_memo", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("RefundType")]
        [Bindable(true)]
        public int RefundType 
	    {
		    get
		    {
			    return GetColumnValue<int>("refund_type");
		    }
            set 
		    {
			    SetColumnValue("refund_type", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerEmail")]
        [Bindable(true)]
        public string SellerEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_email");
		    }
            set 
		    {
			    SetColumnValue("seller_email", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonName")]
        [Bindable(true)]
        public string ReturnedPersonName 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_name");
		    }
            set 
		    {
			    SetColumnValue("returned_person_name", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonTel")]
        [Bindable(true)]
        public string ReturnedPersonTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_tel");
		    }
            set 
		    {
			    SetColumnValue("returned_person_tel", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonEmail")]
        [Bindable(true)]
        public string ReturnedPersonEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_email");
		    }
            set 
		    {
			    SetColumnValue("returned_person_email", value);
            }
        }
	      
        [XmlAttribute("EmpName")]
        [Bindable(true)]
        public string EmpName 
	    {
		    get
		    {
			    return GetColumnValue<string>("emp_name");
		    }
            set 
		    {
			    SetColumnValue("emp_name", value);
            }
        }
	      
        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int DealType 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_type");
		    }
            set 
		    {
			    SetColumnValue("deal_type", value);
            }
        }
	      
        [XmlAttribute("LabelIconList")]
        [Bindable(true)]
        public string LabelIconList 
	    {
		    get
		    {
			    return GetColumnValue<string>("label_icon_list");
		    }
            set 
		    {
			    SetColumnValue("label_icon_list", value);
            }
        }
	      
        [XmlAttribute("RelatedOrderId")]
        [Bindable(true)]
        public string RelatedOrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("related_order_id");
		    }
            set 
		    {
			    SetColumnValue("related_order_id", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ProductGuid = @"product_guid";
            
            public static string ProductId = @"product_id";
            
            public static string ComboPackCount = @"combo_pack_count";
            
            public static string DealName = @"deal_name";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string UseEndTime = @"use_end_time";
            
            public static string OrderGuid = @"order_guid";
            
            public static string OrderId = @"order_id";
            
            public static string MemberName = @"member_name";
            
            public static string PhoneNumber = @"phone_number";
            
            public static string DeliveryAddress = @"delivery_address";
            
            public static string OrderStatus = @"order_status";
            
            public static string Total = @"total";
            
            public static string OrderShipId = @"order_ship_id";
            
            public static string ShipTime = @"ship_time";
            
            public static string ReturnFormId = @"return_form_id";
            
            public static string ReturnApplicationTime = @"return_application_time";
            
            public static string ReturnReason = @"return_reason";
            
            public static string ProgressStatus = @"progress_status";
            
            public static string VendorProgressStatus = @"vendor_progress_status";
            
            public static string VendorMemo = @"vendor_memo";
            
            public static string ModifyTime = @"modify_time";
            
            public static string RefundType = @"refund_type";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerEmail = @"seller_email";
            
            public static string ReturnedPersonName = @"returned_person_name";
            
            public static string ReturnedPersonTel = @"returned_person_tel";
            
            public static string ReturnedPersonEmail = @"returned_person_email";
            
            public static string EmpName = @"emp_name";
            
            public static string DealType = @"deal_type";
            
            public static string LabelIconList = @"label_icon_list";
            
            public static string RelatedOrderId = @"related_order_id";
            
            public static string Type = @"type";
            
            public static string Mobile = @"mobile";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

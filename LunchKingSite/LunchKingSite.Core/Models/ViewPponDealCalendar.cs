using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponDealCalendar class.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealCalendarCollection : ReadOnlyList<ViewPponDealCalendar, ViewPponDealCalendarCollection>
    {        
        public ViewPponDealCalendarCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_deal_calendar view.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealCalendar : ReadOnlyRecord<ViewPponDealCalendar>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_deal_calendar", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
                colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeS.MaxLength = 0;
                colvarBusinessHourDeliverTimeS.AutoIncrement = false;
                colvarBusinessHourDeliverTimeS.IsNullable = true;
                colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeS.IsForeignKey = false;
                colvarBusinessHourDeliverTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeS);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarDevelopeSalesId = new TableSchema.TableColumn(schema);
                colvarDevelopeSalesId.ColumnName = "develope_sales_id";
                colvarDevelopeSalesId.DataType = DbType.Int32;
                colvarDevelopeSalesId.MaxLength = 0;
                colvarDevelopeSalesId.AutoIncrement = false;
                colvarDevelopeSalesId.IsNullable = false;
                colvarDevelopeSalesId.IsPrimaryKey = false;
                colvarDevelopeSalesId.IsForeignKey = false;
                colvarDevelopeSalesId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDevelopeSalesId);
                
                TableSchema.TableColumn colvarDevelopeDeptId = new TableSchema.TableColumn(schema);
                colvarDevelopeDeptId.ColumnName = "develope_dept_id";
                colvarDevelopeDeptId.DataType = DbType.AnsiString;
                colvarDevelopeDeptId.MaxLength = 50;
                colvarDevelopeDeptId.AutoIncrement = false;
                colvarDevelopeDeptId.IsNullable = true;
                colvarDevelopeDeptId.IsPrimaryKey = false;
                colvarDevelopeDeptId.IsForeignKey = false;
                colvarDevelopeDeptId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDevelopeDeptId);
                
                TableSchema.TableColumn colvarDevelopeTeamNo = new TableSchema.TableColumn(schema);
                colvarDevelopeTeamNo.ColumnName = "develope_team_no";
                colvarDevelopeTeamNo.DataType = DbType.Int32;
                colvarDevelopeTeamNo.MaxLength = 0;
                colvarDevelopeTeamNo.AutoIncrement = false;
                colvarDevelopeTeamNo.IsNullable = true;
                colvarDevelopeTeamNo.IsPrimaryKey = false;
                colvarDevelopeTeamNo.IsForeignKey = false;
                colvarDevelopeTeamNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarDevelopeTeamNo);
                
                TableSchema.TableColumn colvarOperationSalesId = new TableSchema.TableColumn(schema);
                colvarOperationSalesId.ColumnName = "operation_sales_id";
                colvarOperationSalesId.DataType = DbType.Int32;
                colvarOperationSalesId.MaxLength = 0;
                colvarOperationSalesId.AutoIncrement = false;
                colvarOperationSalesId.IsNullable = true;
                colvarOperationSalesId.IsPrimaryKey = false;
                colvarOperationSalesId.IsForeignKey = false;
                colvarOperationSalesId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOperationSalesId);
                
                TableSchema.TableColumn colvarOperationDeptId = new TableSchema.TableColumn(schema);
                colvarOperationDeptId.ColumnName = "operation_dept_id";
                colvarOperationDeptId.DataType = DbType.AnsiString;
                colvarOperationDeptId.MaxLength = 50;
                colvarOperationDeptId.AutoIncrement = false;
                colvarOperationDeptId.IsNullable = true;
                colvarOperationDeptId.IsPrimaryKey = false;
                colvarOperationDeptId.IsForeignKey = false;
                colvarOperationDeptId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOperationDeptId);
                
                TableSchema.TableColumn colvarOperationTeamNo = new TableSchema.TableColumn(schema);
                colvarOperationTeamNo.ColumnName = "operation_team_no";
                colvarOperationTeamNo.DataType = DbType.Int32;
                colvarOperationTeamNo.MaxLength = 0;
                colvarOperationTeamNo.AutoIncrement = false;
                colvarOperationTeamNo.IsNullable = true;
                colvarOperationTeamNo.IsPrimaryKey = false;
                colvarOperationTeamNo.IsForeignKey = false;
                colvarOperationTeamNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarOperationTeamNo);
                
                TableSchema.TableColumn colvarNewDealType = new TableSchema.TableColumn(schema);
                colvarNewDealType.ColumnName = "new_deal_type";
                colvarNewDealType.DataType = DbType.Int32;
                colvarNewDealType.MaxLength = 0;
                colvarNewDealType.AutoIncrement = false;
                colvarNewDealType.IsNullable = true;
                colvarNewDealType.IsPrimaryKey = false;
                colvarNewDealType.IsForeignKey = false;
                colvarNewDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarNewDealType);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarGroupOrderStatus = new TableSchema.TableColumn(schema);
                colvarGroupOrderStatus.ColumnName = "group_order_status";
                colvarGroupOrderStatus.DataType = DbType.Int32;
                colvarGroupOrderStatus.MaxLength = 0;
                colvarGroupOrderStatus.AutoIncrement = false;
                colvarGroupOrderStatus.IsNullable = true;
                colvarGroupOrderStatus.IsPrimaryKey = false;
                colvarGroupOrderStatus.IsForeignKey = false;
                colvarGroupOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupOrderStatus);
                
                TableSchema.TableColumn colvarIsHideContent = new TableSchema.TableColumn(schema);
                colvarIsHideContent.ColumnName = "is_hide_content";
                colvarIsHideContent.DataType = DbType.Boolean;
                colvarIsHideContent.MaxLength = 0;
                colvarIsHideContent.AutoIncrement = false;
                colvarIsHideContent.IsNullable = true;
                colvarIsHideContent.IsPrimaryKey = false;
                colvarIsHideContent.IsForeignKey = false;
                colvarIsHideContent.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsHideContent);
                
                TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
                colvarEventName.ColumnName = "event_name";
                colvarEventName.DataType = DbType.String;
                colvarEventName.MaxLength = 400;
                colvarEventName.AutoIncrement = false;
                colvarEventName.IsNullable = true;
                colvarEventName.IsPrimaryKey = false;
                colvarEventName.IsForeignKey = false;
                colvarEventName.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventName);
                
                TableSchema.TableColumn colvarContentName = new TableSchema.TableColumn(schema);
                colvarContentName.ColumnName = "content_name";
                colvarContentName.DataType = DbType.String;
                colvarContentName.MaxLength = 500;
                colvarContentName.AutoIncrement = false;
                colvarContentName.IsNullable = true;
                colvarContentName.IsPrimaryKey = false;
                colvarContentName.IsForeignKey = false;
                colvarContentName.IsReadOnly = false;
                
                schema.Columns.Add(colvarContentName);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = true;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType);
                
                TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarChangedExpireDate.ColumnName = "changed_expire_date";
                colvarChangedExpireDate.DataType = DbType.DateTime;
                colvarChangedExpireDate.MaxLength = 0;
                colvarChangedExpireDate.AutoIncrement = false;
                colvarChangedExpireDate.IsNullable = true;
                colvarChangedExpireDate.IsPrimaryKey = false;
                colvarChangedExpireDate.IsForeignKey = false;
                colvarChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarChangedExpireDate);
                
                TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
                colvarSlug.ColumnName = "slug";
                colvarSlug.DataType = DbType.String;
                colvarSlug.MaxLength = 100;
                colvarSlug.AutoIncrement = false;
                colvarSlug.IsNullable = true;
                colvarSlug.IsPrimaryKey = false;
                colvarSlug.IsForeignKey = false;
                colvarSlug.IsReadOnly = false;
                
                schema.Columns.Add(colvarSlug);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarOrderedQuantity = new TableSchema.TableColumn(schema);
                colvarOrderedQuantity.ColumnName = "ordered_quantity";
                colvarOrderedQuantity.DataType = DbType.Int32;
                colvarOrderedQuantity.MaxLength = 0;
                colvarOrderedQuantity.AutoIncrement = false;
                colvarOrderedQuantity.IsNullable = true;
                colvarOrderedQuantity.IsPrimaryKey = false;
                colvarOrderedQuantity.IsForeignKey = false;
                colvarOrderedQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderedQuantity);
                
                TableSchema.TableColumn colvarIsQuantityMultiplier = new TableSchema.TableColumn(schema);
                colvarIsQuantityMultiplier.ColumnName = "is_quantity_multiplier";
                colvarIsQuantityMultiplier.DataType = DbType.Boolean;
                colvarIsQuantityMultiplier.MaxLength = 0;
                colvarIsQuantityMultiplier.AutoIncrement = false;
                colvarIsQuantityMultiplier.IsNullable = false;
                colvarIsQuantityMultiplier.IsPrimaryKey = false;
                colvarIsQuantityMultiplier.IsForeignKey = false;
                colvarIsQuantityMultiplier.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsQuantityMultiplier);
                
                TableSchema.TableColumn colvarQuantityMultiplier = new TableSchema.TableColumn(schema);
                colvarQuantityMultiplier.ColumnName = "quantity_multiplier";
                colvarQuantityMultiplier.DataType = DbType.Int32;
                colvarQuantityMultiplier.MaxLength = 0;
                colvarQuantityMultiplier.AutoIncrement = false;
                colvarQuantityMultiplier.IsNullable = true;
                colvarQuantityMultiplier.IsPrimaryKey = false;
                colvarQuantityMultiplier.IsForeignKey = false;
                colvarQuantityMultiplier.IsReadOnly = false;
                
                schema.Columns.Add(colvarQuantityMultiplier);
                
                TableSchema.TableColumn colvarContinuedQuantity = new TableSchema.TableColumn(schema);
                colvarContinuedQuantity.ColumnName = "continued_quantity";
                colvarContinuedQuantity.DataType = DbType.Int32;
                colvarContinuedQuantity.MaxLength = 0;
                colvarContinuedQuantity.AutoIncrement = false;
                colvarContinuedQuantity.IsNullable = false;
                colvarContinuedQuantity.IsPrimaryKey = false;
                colvarContinuedQuantity.IsForeignKey = false;
                colvarContinuedQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarContinuedQuantity);
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = true;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarProposalSourceType = new TableSchema.TableColumn(schema);
                colvarProposalSourceType.ColumnName = "proposal_source_type";
                colvarProposalSourceType.DataType = DbType.Int32;
                colvarProposalSourceType.MaxLength = 0;
                colvarProposalSourceType.AutoIncrement = false;
                colvarProposalSourceType.IsNullable = true;
                colvarProposalSourceType.IsPrimaryKey = false;
                colvarProposalSourceType.IsForeignKey = false;
                colvarProposalSourceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProposalSourceType);
                
                TableSchema.TableColumn colvarIsWms = new TableSchema.TableColumn(schema);
                colvarIsWms.ColumnName = "is_wms";
                colvarIsWms.DataType = DbType.Boolean;
                colvarIsWms.MaxLength = 0;
                colvarIsWms.AutoIncrement = false;
                colvarIsWms.IsNullable = false;
                colvarIsWms.IsPrimaryKey = false;
                colvarIsWms.IsForeignKey = false;
                colvarIsWms.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsWms);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = true;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_deal_calendar",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponDealCalendar()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponDealCalendar(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponDealCalendar(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponDealCalendar(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("DevelopeSalesId")]
        [Bindable(true)]
        public int DevelopeSalesId 
	    {
		    get
		    {
			    return GetColumnValue<int>("develope_sales_id");
		    }
            set 
		    {
			    SetColumnValue("develope_sales_id", value);
            }
        }
	      
        [XmlAttribute("DevelopeDeptId")]
        [Bindable(true)]
        public string DevelopeDeptId 
	    {
		    get
		    {
			    return GetColumnValue<string>("develope_dept_id");
		    }
            set 
		    {
			    SetColumnValue("develope_dept_id", value);
            }
        }
	      
        [XmlAttribute("DevelopeTeamNo")]
        [Bindable(true)]
        public int? DevelopeTeamNo 
	    {
		    get
		    {
			    return GetColumnValue<int?>("develope_team_no");
		    }
            set 
		    {
			    SetColumnValue("develope_team_no", value);
            }
        }
	      
        [XmlAttribute("OperationSalesId")]
        [Bindable(true)]
        public int? OperationSalesId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("operation_sales_id");
		    }
            set 
		    {
			    SetColumnValue("operation_sales_id", value);
            }
        }
	      
        [XmlAttribute("OperationDeptId")]
        [Bindable(true)]
        public string OperationDeptId 
	    {
		    get
		    {
			    return GetColumnValue<string>("operation_dept_id");
		    }
            set 
		    {
			    SetColumnValue("operation_dept_id", value);
            }
        }
	      
        [XmlAttribute("OperationTeamNo")]
        [Bindable(true)]
        public int? OperationTeamNo 
	    {
		    get
		    {
			    return GetColumnValue<int?>("operation_team_no");
		    }
            set 
		    {
			    SetColumnValue("operation_team_no", value);
            }
        }
	      
        [XmlAttribute("NewDealType")]
        [Bindable(true)]
        public int? NewDealType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("new_deal_type");
		    }
            set 
		    {
			    SetColumnValue("new_deal_type", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("GroupOrderStatus")]
        [Bindable(true)]
        public int? GroupOrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_order_status");
		    }
            set 
		    {
			    SetColumnValue("group_order_status", value);
            }
        }
	      
        [XmlAttribute("IsHideContent")]
        [Bindable(true)]
        public bool? IsHideContent 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_hide_content");
		    }
            set 
		    {
			    SetColumnValue("is_hide_content", value);
            }
        }
	      
        [XmlAttribute("EventName")]
        [Bindable(true)]
        public string EventName 
	    {
		    get
		    {
			    return GetColumnValue<string>("event_name");
		    }
            set 
		    {
			    SetColumnValue("event_name", value);
            }
        }
	      
        [XmlAttribute("ContentName")]
        [Bindable(true)]
        public string ContentName 
	    {
		    get
		    {
			    return GetColumnValue<string>("content_name");
		    }
            set 
		    {
			    SetColumnValue("content_name", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int? DealType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_type");
		    }
            set 
		    {
			    SetColumnValue("deal_type", value);
            }
        }
	      
        [XmlAttribute("ChangedExpireDate")]
        [Bindable(true)]
        public DateTime? ChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("Slug")]
        [Bindable(true)]
        public string Slug 
	    {
		    get
		    {
			    return GetColumnValue<string>("slug");
		    }
            set 
		    {
			    SetColumnValue("slug", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("OrderedQuantity")]
        [Bindable(true)]
        public int? OrderedQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ordered_quantity");
		    }
            set 
		    {
			    SetColumnValue("ordered_quantity", value);
            }
        }
	      
        [XmlAttribute("IsQuantityMultiplier")]
        [Bindable(true)]
        public bool IsQuantityMultiplier 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_quantity_multiplier");
		    }
            set 
		    {
			    SetColumnValue("is_quantity_multiplier", value);
            }
        }
	      
        [XmlAttribute("QuantityMultiplier")]
        [Bindable(true)]
        public int? QuantityMultiplier 
	    {
		    get
		    {
			    return GetColumnValue<int?>("quantity_multiplier");
		    }
            set 
		    {
			    SetColumnValue("quantity_multiplier", value);
            }
        }
	      
        [XmlAttribute("ContinuedQuantity")]
        [Bindable(true)]
        public int ContinuedQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("continued_quantity");
		    }
            set 
		    {
			    SetColumnValue("continued_quantity", value);
            }
        }
	      
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int? Id 
	    {
		    get
		    {
			    return GetColumnValue<int?>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("ProposalSourceType")]
        [Bindable(true)]
        public int? ProposalSourceType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("proposal_source_type");
		    }
            set 
		    {
			    SetColumnValue("proposal_source_type", value);
            }
        }
	      
        [XmlAttribute("IsWms")]
        [Bindable(true)]
        public bool IsWms 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_wms");
		    }
            set 
		    {
			    SetColumnValue("is_wms", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int? DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerId = @"seller_id";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string DevelopeSalesId = @"develope_sales_id";
            
            public static string DevelopeDeptId = @"develope_dept_id";
            
            public static string DevelopeTeamNo = @"develope_team_no";
            
            public static string OperationSalesId = @"operation_sales_id";
            
            public static string OperationDeptId = @"operation_dept_id";
            
            public static string OperationTeamNo = @"operation_team_no";
            
            public static string NewDealType = @"new_deal_type";
            
            public static string ItemName = @"item_name";
            
            public static string GroupOrderStatus = @"group_order_status";
            
            public static string IsHideContent = @"is_hide_content";
            
            public static string EventName = @"event_name";
            
            public static string ContentName = @"content_name";
            
            public static string UniqueId = @"unique_id";
            
            public static string DealType = @"deal_type";
            
            public static string ChangedExpireDate = @"changed_expire_date";
            
            public static string Slug = @"slug";
            
            public static string ItemPrice = @"item_price";
            
            public static string OrderedQuantity = @"ordered_quantity";
            
            public static string IsQuantityMultiplier = @"is_quantity_multiplier";
            
            public static string QuantityMultiplier = @"quantity_multiplier";
            
            public static string ContinuedQuantity = @"continued_quantity";
            
            public static string Id = @"id";
            
            public static string ProposalSourceType = @"proposal_source_type";
            
            public static string IsWms = @"is_wms";
            
            public static string DeliveryType = @"delivery_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

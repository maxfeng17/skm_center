﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the GaOverview class.
    /// </summary>
    [Serializable]
    public partial class GaOverviewCollection : RepositoryList<GaOverview, GaOverviewCollection>
    {
        public GaOverviewCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>GaOverviewCollection</returns>
        public GaOverviewCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                GaOverview o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the ga_overview table.
    /// </summary>
    [Serializable]
    public partial class GaOverview : RepositoryRecord<GaOverview>, IRecordBase
    {
        #region .ctors and Default Settings

        public GaOverview()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public GaOverview(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("ga_overview", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarQueryDate = new TableSchema.TableColumn(schema);
                colvarQueryDate.ColumnName = "query_date";
                colvarQueryDate.DataType = DbType.DateTime;
                colvarQueryDate.MaxLength = 0;
                colvarQueryDate.AutoIncrement = false;
                colvarQueryDate.IsNullable = false;
                colvarQueryDate.IsPrimaryKey = true;
                colvarQueryDate.IsForeignKey = false;
                colvarQueryDate.IsReadOnly = false;
                colvarQueryDate.DefaultSetting = @"";
                colvarQueryDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarQueryDate);

                TableSchema.TableColumn colvarViewId = new TableSchema.TableColumn(schema);
                colvarViewId.ColumnName = "view_id";
                colvarViewId.DataType = DbType.Int32;
                colvarViewId.MaxLength = 0;
                colvarViewId.AutoIncrement = false;
                colvarViewId.IsNullable = false;
                colvarViewId.IsPrimaryKey = true;
                colvarViewId.IsForeignKey = false;
                colvarViewId.IsReadOnly = false;
                colvarViewId.DefaultSetting = @"";
                colvarViewId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarViewId);

                TableSchema.TableColumn colvarViewName = new TableSchema.TableColumn(schema);
                colvarViewName.ColumnName = "view_name";
                colvarViewName.DataType = DbType.String;
                colvarViewName.MaxLength = 50;
                colvarViewName.AutoIncrement = false;
                colvarViewName.IsNullable = false;
                colvarViewName.IsPrimaryKey = false;
                colvarViewName.IsForeignKey = false;
                colvarViewName.IsReadOnly = false;
                colvarViewName.DefaultSetting = @"";
                colvarViewName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarViewName);

                TableSchema.TableColumn colvarSessions = new TableSchema.TableColumn(schema);
                colvarSessions.ColumnName = "sessions";
                colvarSessions.DataType = DbType.Int32;
                colvarSessions.MaxLength = 0;
                colvarSessions.AutoIncrement = false;
                colvarSessions.IsNullable = false;
                colvarSessions.IsPrimaryKey = false;
                colvarSessions.IsForeignKey = false;
                colvarSessions.IsReadOnly = false;

                colvarSessions.DefaultSetting = @"((0))";
                colvarSessions.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSessions);

                TableSchema.TableColumn colvarUsers = new TableSchema.TableColumn(schema);
                colvarUsers.ColumnName = "users";
                colvarUsers.DataType = DbType.Int32;
                colvarUsers.MaxLength = 0;
                colvarUsers.AutoIncrement = false;
                colvarUsers.IsNullable = false;
                colvarUsers.IsPrimaryKey = false;
                colvarUsers.IsForeignKey = false;
                colvarUsers.IsReadOnly = false;

                colvarUsers.DefaultSetting = @"((0))";
                colvarUsers.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUsers);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("ga_overview", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("QueryDate")]
        [Bindable(true)]
        public DateTime QueryDate
        {
            get { return GetColumnValue<DateTime>(Columns.QueryDate); }
            set { SetColumnValue(Columns.QueryDate, value); }
        }

        [XmlAttribute("ViewId")]
        [Bindable(true)]
        public int ViewId
        {
            get { return GetColumnValue<int>(Columns.ViewId); }
            set { SetColumnValue(Columns.ViewId, value); }
        }

        [XmlAttribute("ViewName")]
        [Bindable(true)]
        public string ViewName
        {
            get { return GetColumnValue<string>(Columns.ViewName); }
            set { SetColumnValue(Columns.ViewName, value); }
        }

        [XmlAttribute("Sessions")]
        [Bindable(true)]
        public int Sessions
        {
            get { return GetColumnValue<int>(Columns.Sessions); }
            set { SetColumnValue(Columns.Sessions, value); }
        }

        [XmlAttribute("Users")]
        [Bindable(true)]
        public int Users
        {
            get { return GetColumnValue<int>(Columns.Users); }
            set { SetColumnValue(Columns.Users, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn QueryDateColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ViewIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ViewNameColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn SessionsColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn UsersColumn
        {
            get { return Schema.Columns[4]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string QueryDate = @"query_date";
            public static string ViewId = @"view_id";
            public static string ViewName = @"view_name";
            public static string Sessions = @"sessions";
            public static string Users = @"users";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmMember class.
	/// </summary>
    [Serializable]
	public partial class SkmMemberCollection : RepositoryList<SkmMember, SkmMemberCollection>
	{	   
		public SkmMemberCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmMemberCollection</returns>
		public SkmMemberCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmMember o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_member table.
	/// </summary>
	[Serializable]
	public partial class SkmMember : RepositoryRecord<SkmMember>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmMember()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmMember(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_member", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSkmToken = new TableSchema.TableColumn(schema);
				colvarSkmToken.ColumnName = "skm_token";
				colvarSkmToken.DataType = DbType.String;
				colvarSkmToken.MaxLength = 50;
				colvarSkmToken.AutoIncrement = false;
				colvarSkmToken.IsNullable = false;
				colvarSkmToken.IsPrimaryKey = false;
				colvarSkmToken.IsForeignKey = false;
				colvarSkmToken.IsReadOnly = false;
				colvarSkmToken.DefaultSetting = @"";
				colvarSkmToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmToken);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarIsEnable = new TableSchema.TableColumn(schema);
				colvarIsEnable.ColumnName = "is_enable";
				colvarIsEnable.DataType = DbType.Boolean;
				colvarIsEnable.MaxLength = 0;
				colvarIsEnable.AutoIncrement = false;
				colvarIsEnable.IsNullable = false;
				colvarIsEnable.IsPrimaryKey = false;
				colvarIsEnable.IsForeignKey = false;
				colvarIsEnable.IsReadOnly = false;
				
						colvarIsEnable.DefaultSetting = @"((1))";
				colvarIsEnable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsEnable);
				
				TableSchema.TableColumn colvarIsShared = new TableSchema.TableColumn(schema);
				colvarIsShared.ColumnName = "is_shared";
				colvarIsShared.DataType = DbType.Boolean;
				colvarIsShared.MaxLength = 0;
				colvarIsShared.AutoIncrement = false;
				colvarIsShared.IsNullable = false;
				colvarIsShared.IsPrimaryKey = false;
				colvarIsShared.IsForeignKey = false;
				colvarIsShared.IsReadOnly = false;
				
						colvarIsShared.DefaultSetting = @"((0))";
				colvarIsShared.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsShared);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_member",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SkmToken")]
		[Bindable(true)]
		public string SkmToken 
		{
			get { return GetColumnValue<string>(Columns.SkmToken); }
			set { SetColumnValue(Columns.SkmToken, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("IsEnable")]
		[Bindable(true)]
		public bool IsEnable 
		{
			get { return GetColumnValue<bool>(Columns.IsEnable); }
			set { SetColumnValue(Columns.IsEnable, value); }
		}
		  
		[XmlAttribute("IsShared")]
		[Bindable(true)]
		public bool IsShared 
		{
			get { return GetColumnValue<bool>(Columns.IsShared); }
			set { SetColumnValue(Columns.IsShared, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmTokenColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IsEnableColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IsSharedColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SkmToken = @"skm_token";
			 public static string UserId = @"user_id";
			 public static string CreateTime = @"create_time";
			 public static string IsEnable = @"is_enable";
			 public static string IsShared = @"is_shared";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

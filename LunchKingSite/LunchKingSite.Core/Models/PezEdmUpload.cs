using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PezEdmUpload class.
	/// </summary>
    [Serializable]
	public partial class PezEdmUploadCollection : RepositoryList<PezEdmUpload, PezEdmUploadCollection>
	{	   
		public PezEdmUploadCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PezEdmUploadCollection</returns>
		public PezEdmUploadCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PezEdmUpload o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pez_edm_upload table.
	/// </summary>
	[Serializable]
	public partial class PezEdmUpload : RepositoryRecord<PezEdmUpload>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PezEdmUpload()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PezEdmUpload(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pez_edm_upload", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarEdmContent = new TableSchema.TableColumn(schema);
				colvarEdmContent.ColumnName = "edm_content";
				colvarEdmContent.DataType = DbType.String;
				colvarEdmContent.MaxLength = -1;
				colvarEdmContent.AutoIncrement = false;
				colvarEdmContent.IsNullable = true;
				colvarEdmContent.IsPrimaryKey = false;
				colvarEdmContent.IsForeignKey = false;
				colvarEdmContent.IsReadOnly = false;
				colvarEdmContent.DefaultSetting = @"";
				colvarEdmContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEdmContent);
				
				TableSchema.TableColumn colvarPm = new TableSchema.TableColumn(schema);
				colvarPm.ColumnName = "PM";
				colvarPm.DataType = DbType.String;
				colvarPm.MaxLength = 150;
				colvarPm.AutoIncrement = false;
				colvarPm.IsNullable = true;
				colvarPm.IsPrimaryKey = false;
				colvarPm.IsForeignKey = false;
				colvarPm.IsReadOnly = false;
				colvarPm.DefaultSetting = @"";
				colvarPm.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPm);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "code";
				colvarCode.DataType = DbType.String;
				colvarCode.MaxLength = 50;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = true;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarDept = new TableSchema.TableColumn(schema);
				colvarDept.ColumnName = "dept";
				colvarDept.DataType = DbType.String;
				colvarDept.MaxLength = 50;
				colvarDept.AutoIncrement = false;
				colvarDept.IsNullable = true;
				colvarDept.IsPrimaryKey = false;
				colvarDept.IsForeignKey = false;
				colvarDept.IsReadOnly = false;
				colvarDept.DefaultSetting = @"";
				colvarDept.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDept);
				
				TableSchema.TableColumn colvarPart = new TableSchema.TableColumn(schema);
				colvarPart.ColumnName = "part";
				colvarPart.DataType = DbType.String;
				colvarPart.MaxLength = 50;
				colvarPart.AutoIncrement = false;
				colvarPart.IsNullable = true;
				colvarPart.IsPrimaryKey = false;
				colvarPart.IsForeignKey = false;
				colvarPart.IsReadOnly = false;
				colvarPart.DefaultSetting = @"";
				colvarPart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPart);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pez_edm_upload",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("EdmContent")]
		[Bindable(true)]
		public string EdmContent 
		{
			get { return GetColumnValue<string>(Columns.EdmContent); }
			set { SetColumnValue(Columns.EdmContent, value); }
		}
		  
		[XmlAttribute("Pm")]
		[Bindable(true)]
		public string Pm 
		{
			get { return GetColumnValue<string>(Columns.Pm); }
			set { SetColumnValue(Columns.Pm, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public string Code 
		{
			get { return GetColumnValue<string>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("Dept")]
		[Bindable(true)]
		public string Dept 
		{
			get { return GetColumnValue<string>(Columns.Dept); }
			set { SetColumnValue(Columns.Dept, value); }
		}
		  
		[XmlAttribute("Part")]
		[Bindable(true)]
		public string Part 
		{
			get { return GetColumnValue<string>(Columns.Part); }
			set { SetColumnValue(Columns.Part, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EdmContentColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PmColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DeptColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn PartColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string EdmContent = @"edm_content";
			 public static string Pm = @"PM";
			 public static string CreateTime = @"create_time";
			 public static string Code = @"code";
			 public static string Dept = @"dept";
			 public static string Part = @"part";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealReturnedDetail class.
	/// </summary>
    [Serializable]
	public partial class HiDealReturnedDetailCollection : RepositoryList<HiDealReturnedDetail, HiDealReturnedDetailCollection>
	{	   
		public HiDealReturnedDetailCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealReturnedDetailCollection</returns>
		public HiDealReturnedDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealReturnedDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_returned_detail table.
	/// </summary>
	[Serializable]
	public partial class HiDealReturnedDetail : RepositoryRecord<HiDealReturnedDetail>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealReturnedDetail()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealReturnedDetail(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_returned_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarReturnedId = new TableSchema.TableColumn(schema);
				colvarReturnedId.ColumnName = "returned_id";
				colvarReturnedId.DataType = DbType.Int32;
				colvarReturnedId.MaxLength = 0;
				colvarReturnedId.AutoIncrement = false;
				colvarReturnedId.IsNullable = false;
				colvarReturnedId.IsPrimaryKey = false;
				colvarReturnedId.IsForeignKey = true;
				colvarReturnedId.IsReadOnly = false;
				colvarReturnedId.DefaultSetting = @"";
				
					colvarReturnedId.ForeignKeyTableName = "hi_deal_returned";
				schema.Columns.Add(colvarReturnedId);
				
				TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
				colvarOrderDetailGuid.ColumnName = "order_detail_guid";
				colvarOrderDetailGuid.DataType = DbType.Guid;
				colvarOrderDetailGuid.MaxLength = 0;
				colvarOrderDetailGuid.AutoIncrement = false;
				colvarOrderDetailGuid.IsNullable = false;
				colvarOrderDetailGuid.IsPrimaryKey = false;
				colvarOrderDetailGuid.IsForeignKey = true;
				colvarOrderDetailGuid.IsReadOnly = false;
				colvarOrderDetailGuid.DefaultSetting = @"";
				
					colvarOrderDetailGuid.ForeignKeyTableName = "hi_deal_order_detail";
				schema.Columns.Add(colvarOrderDetailGuid);
				
				TableSchema.TableColumn colvarHiDealId = new TableSchema.TableColumn(schema);
				colvarHiDealId.ColumnName = "hi_deal_id";
				colvarHiDealId.DataType = DbType.Int32;
				colvarHiDealId.MaxLength = 0;
				colvarHiDealId.AutoIncrement = false;
				colvarHiDealId.IsNullable = false;
				colvarHiDealId.IsPrimaryKey = false;
				colvarHiDealId.IsForeignKey = true;
				colvarHiDealId.IsReadOnly = false;
				colvarHiDealId.DefaultSetting = @"";
				
					colvarHiDealId.ForeignKeyTableName = "hi_deal_deal";
				schema.Columns.Add(colvarHiDealId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = true;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				
					colvarSellerGuid.ForeignKeyTableName = "seller";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 50;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);
				
				TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
				colvarProductId.ColumnName = "product_id";
				colvarProductId.DataType = DbType.Int32;
				colvarProductId.MaxLength = 0;
				colvarProductId.AutoIncrement = false;
				colvarProductId.IsNullable = false;
				colvarProductId.IsPrimaryKey = false;
				colvarProductId.IsForeignKey = true;
				colvarProductId.IsReadOnly = false;
				colvarProductId.DefaultSetting = @"";
				
					colvarProductId.ForeignKeyTableName = "hi_deal_product";
				schema.Columns.Add(colvarProductId);
				
				TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
				colvarProductName.ColumnName = "product_name";
				colvarProductName.DataType = DbType.String;
				colvarProductName.MaxLength = 250;
				colvarProductName.AutoIncrement = false;
				colvarProductName.IsNullable = false;
				colvarProductName.IsPrimaryKey = false;
				colvarProductName.IsForeignKey = false;
				colvarProductName.IsReadOnly = false;
				colvarProductName.DefaultSetting = @"";
				colvarProductName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductName);
				
				TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
				colvarItemQuantity.ColumnName = "item_quantity";
				colvarItemQuantity.DataType = DbType.Int32;
				colvarItemQuantity.MaxLength = 0;
				colvarItemQuantity.AutoIncrement = false;
				colvarItemQuantity.IsNullable = false;
				colvarItemQuantity.IsPrimaryKey = false;
				colvarItemQuantity.IsForeignKey = false;
				colvarItemQuantity.IsReadOnly = false;
				
						colvarItemQuantity.DefaultSetting = @"((0))";
				colvarItemQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemQuantity);
				
				TableSchema.TableColumn colvarUnitPrice = new TableSchema.TableColumn(schema);
				colvarUnitPrice.ColumnName = "unit_price";
				colvarUnitPrice.DataType = DbType.Currency;
				colvarUnitPrice.MaxLength = 0;
				colvarUnitPrice.AutoIncrement = false;
				colvarUnitPrice.IsNullable = false;
				colvarUnitPrice.IsPrimaryKey = false;
				colvarUnitPrice.IsForeignKey = false;
				colvarUnitPrice.IsReadOnly = false;
				
						colvarUnitPrice.DefaultSetting = @"((0))";
				colvarUnitPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUnitPrice);
				
				TableSchema.TableColumn colvarDetailTotalAmt = new TableSchema.TableColumn(schema);
				colvarDetailTotalAmt.ColumnName = "detail_total_amt";
				colvarDetailTotalAmt.DataType = DbType.Currency;
				colvarDetailTotalAmt.MaxLength = 0;
				colvarDetailTotalAmt.AutoIncrement = false;
				colvarDetailTotalAmt.IsNullable = false;
				colvarDetailTotalAmt.IsPrimaryKey = false;
				colvarDetailTotalAmt.IsForeignKey = false;
				colvarDetailTotalAmt.IsReadOnly = false;
				
						colvarDetailTotalAmt.DefaultSetting = @"((0))";
				colvarDetailTotalAmt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDetailTotalAmt);
				
				TableSchema.TableColumn colvarCategory1 = new TableSchema.TableColumn(schema);
				colvarCategory1.ColumnName = "category1";
				colvarCategory1.DataType = DbType.Int32;
				colvarCategory1.MaxLength = 0;
				colvarCategory1.AutoIncrement = false;
				colvarCategory1.IsNullable = true;
				colvarCategory1.IsPrimaryKey = false;
				colvarCategory1.IsForeignKey = false;
				colvarCategory1.IsReadOnly = false;
				colvarCategory1.DefaultSetting = @"";
				colvarCategory1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategory1);
				
				TableSchema.TableColumn colvarOption1 = new TableSchema.TableColumn(schema);
				colvarOption1.ColumnName = "option1";
				colvarOption1.DataType = DbType.Int32;
				colvarOption1.MaxLength = 0;
				colvarOption1.AutoIncrement = false;
				colvarOption1.IsNullable = true;
				colvarOption1.IsPrimaryKey = false;
				colvarOption1.IsForeignKey = false;
				colvarOption1.IsReadOnly = false;
				colvarOption1.DefaultSetting = @"";
				colvarOption1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOption1);
				
				TableSchema.TableColumn colvarOptionName1 = new TableSchema.TableColumn(schema);
				colvarOptionName1.ColumnName = "option_name1";
				colvarOptionName1.DataType = DbType.String;
				colvarOptionName1.MaxLength = 50;
				colvarOptionName1.AutoIncrement = false;
				colvarOptionName1.IsNullable = true;
				colvarOptionName1.IsPrimaryKey = false;
				colvarOptionName1.IsForeignKey = false;
				colvarOptionName1.IsReadOnly = false;
				colvarOptionName1.DefaultSetting = @"";
				colvarOptionName1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOptionName1);
				
				TableSchema.TableColumn colvarCategory2 = new TableSchema.TableColumn(schema);
				colvarCategory2.ColumnName = "category2";
				colvarCategory2.DataType = DbType.Int32;
				colvarCategory2.MaxLength = 0;
				colvarCategory2.AutoIncrement = false;
				colvarCategory2.IsNullable = true;
				colvarCategory2.IsPrimaryKey = false;
				colvarCategory2.IsForeignKey = false;
				colvarCategory2.IsReadOnly = false;
				colvarCategory2.DefaultSetting = @"";
				colvarCategory2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategory2);
				
				TableSchema.TableColumn colvarOption2 = new TableSchema.TableColumn(schema);
				colvarOption2.ColumnName = "option2";
				colvarOption2.DataType = DbType.Int32;
				colvarOption2.MaxLength = 0;
				colvarOption2.AutoIncrement = false;
				colvarOption2.IsNullable = true;
				colvarOption2.IsPrimaryKey = false;
				colvarOption2.IsForeignKey = false;
				colvarOption2.IsReadOnly = false;
				colvarOption2.DefaultSetting = @"";
				colvarOption2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOption2);
				
				TableSchema.TableColumn colvarOptionName2 = new TableSchema.TableColumn(schema);
				colvarOptionName2.ColumnName = "option_name2";
				colvarOptionName2.DataType = DbType.String;
				colvarOptionName2.MaxLength = 50;
				colvarOptionName2.AutoIncrement = false;
				colvarOptionName2.IsNullable = true;
				colvarOptionName2.IsPrimaryKey = false;
				colvarOptionName2.IsForeignKey = false;
				colvarOptionName2.IsReadOnly = false;
				colvarOptionName2.DefaultSetting = @"";
				colvarOptionName2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOptionName2);
				
				TableSchema.TableColumn colvarCategory3 = new TableSchema.TableColumn(schema);
				colvarCategory3.ColumnName = "category3";
				colvarCategory3.DataType = DbType.Int32;
				colvarCategory3.MaxLength = 0;
				colvarCategory3.AutoIncrement = false;
				colvarCategory3.IsNullable = true;
				colvarCategory3.IsPrimaryKey = false;
				colvarCategory3.IsForeignKey = false;
				colvarCategory3.IsReadOnly = false;
				colvarCategory3.DefaultSetting = @"";
				colvarCategory3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategory3);
				
				TableSchema.TableColumn colvarOption3 = new TableSchema.TableColumn(schema);
				colvarOption3.ColumnName = "option3";
				colvarOption3.DataType = DbType.Int32;
				colvarOption3.MaxLength = 0;
				colvarOption3.AutoIncrement = false;
				colvarOption3.IsNullable = true;
				colvarOption3.IsPrimaryKey = false;
				colvarOption3.IsForeignKey = false;
				colvarOption3.IsReadOnly = false;
				colvarOption3.DefaultSetting = @"";
				colvarOption3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOption3);
				
				TableSchema.TableColumn colvarOptionName3 = new TableSchema.TableColumn(schema);
				colvarOptionName3.ColumnName = "option_name3";
				colvarOptionName3.DataType = DbType.String;
				colvarOptionName3.MaxLength = 50;
				colvarOptionName3.AutoIncrement = false;
				colvarOptionName3.IsNullable = true;
				colvarOptionName3.IsPrimaryKey = false;
				colvarOptionName3.IsForeignKey = false;
				colvarOptionName3.IsReadOnly = false;
				colvarOptionName3.DefaultSetting = @"";
				colvarOptionName3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOptionName3);
				
				TableSchema.TableColumn colvarCategory4 = new TableSchema.TableColumn(schema);
				colvarCategory4.ColumnName = "category4";
				colvarCategory4.DataType = DbType.Int32;
				colvarCategory4.MaxLength = 0;
				colvarCategory4.AutoIncrement = false;
				colvarCategory4.IsNullable = true;
				colvarCategory4.IsPrimaryKey = false;
				colvarCategory4.IsForeignKey = false;
				colvarCategory4.IsReadOnly = false;
				colvarCategory4.DefaultSetting = @"";
				colvarCategory4.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategory4);
				
				TableSchema.TableColumn colvarOption4 = new TableSchema.TableColumn(schema);
				colvarOption4.ColumnName = "option4";
				colvarOption4.DataType = DbType.Int32;
				colvarOption4.MaxLength = 0;
				colvarOption4.AutoIncrement = false;
				colvarOption4.IsNullable = true;
				colvarOption4.IsPrimaryKey = false;
				colvarOption4.IsForeignKey = false;
				colvarOption4.IsReadOnly = false;
				colvarOption4.DefaultSetting = @"";
				colvarOption4.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOption4);
				
				TableSchema.TableColumn colvarOptionName4 = new TableSchema.TableColumn(schema);
				colvarOptionName4.ColumnName = "option_name4";
				colvarOptionName4.DataType = DbType.String;
				colvarOptionName4.MaxLength = 50;
				colvarOptionName4.AutoIncrement = false;
				colvarOptionName4.IsNullable = true;
				colvarOptionName4.IsPrimaryKey = false;
				colvarOptionName4.IsForeignKey = false;
				colvarOptionName4.IsReadOnly = false;
				colvarOptionName4.DefaultSetting = @"";
				colvarOptionName4.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOptionName4);
				
				TableSchema.TableColumn colvarCategory5 = new TableSchema.TableColumn(schema);
				colvarCategory5.ColumnName = "category5";
				colvarCategory5.DataType = DbType.Int32;
				colvarCategory5.MaxLength = 0;
				colvarCategory5.AutoIncrement = false;
				colvarCategory5.IsNullable = true;
				colvarCategory5.IsPrimaryKey = false;
				colvarCategory5.IsForeignKey = false;
				colvarCategory5.IsReadOnly = false;
				colvarCategory5.DefaultSetting = @"";
				colvarCategory5.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategory5);
				
				TableSchema.TableColumn colvarOption5 = new TableSchema.TableColumn(schema);
				colvarOption5.ColumnName = "option5";
				colvarOption5.DataType = DbType.Int32;
				colvarOption5.MaxLength = 0;
				colvarOption5.AutoIncrement = false;
				colvarOption5.IsNullable = true;
				colvarOption5.IsPrimaryKey = false;
				colvarOption5.IsForeignKey = false;
				colvarOption5.IsReadOnly = false;
				colvarOption5.DefaultSetting = @"";
				colvarOption5.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOption5);
				
				TableSchema.TableColumn colvarOptionName5 = new TableSchema.TableColumn(schema);
				colvarOptionName5.ColumnName = "option_name5";
				colvarOptionName5.DataType = DbType.String;
				colvarOptionName5.MaxLength = 50;
				colvarOptionName5.AutoIncrement = false;
				colvarOptionName5.IsNullable = true;
				colvarOptionName5.IsPrimaryKey = false;
				colvarOptionName5.IsForeignKey = false;
				colvarOptionName5.IsReadOnly = false;
				colvarOptionName5.DefaultSetting = @"";
				colvarOptionName5.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOptionName5);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = true;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = false;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarProductType = new TableSchema.TableColumn(schema);
				colvarProductType.ColumnName = "product_type";
				colvarProductType.DataType = DbType.Int32;
				colvarProductType.MaxLength = 0;
				colvarProductType.AutoIncrement = false;
				colvarProductType.IsNullable = false;
				colvarProductType.IsPrimaryKey = false;
				colvarProductType.IsForeignKey = false;
				colvarProductType.IsReadOnly = false;
				colvarProductType.DefaultSetting = @"";
				colvarProductType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductType);
				
				TableSchema.TableColumn colvarReturnItemQuantity = new TableSchema.TableColumn(schema);
				colvarReturnItemQuantity.ColumnName = "return_item_quantity";
				colvarReturnItemQuantity.DataType = DbType.Int32;
				colvarReturnItemQuantity.MaxLength = 0;
				colvarReturnItemQuantity.AutoIncrement = false;
				colvarReturnItemQuantity.IsNullable = false;
				colvarReturnItemQuantity.IsPrimaryKey = false;
				colvarReturnItemQuantity.IsForeignKey = false;
				colvarReturnItemQuantity.IsReadOnly = false;
				
						colvarReturnItemQuantity.DefaultSetting = @"((0))";
				colvarReturnItemQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnItemQuantity);
				
				TableSchema.TableColumn colvarReturnDetailTotalAmt = new TableSchema.TableColumn(schema);
				colvarReturnDetailTotalAmt.ColumnName = "return_detail_total_amt";
				colvarReturnDetailTotalAmt.DataType = DbType.Int32;
				colvarReturnDetailTotalAmt.MaxLength = 0;
				colvarReturnDetailTotalAmt.AutoIncrement = false;
				colvarReturnDetailTotalAmt.IsNullable = false;
				colvarReturnDetailTotalAmt.IsPrimaryKey = false;
				colvarReturnDetailTotalAmt.IsForeignKey = false;
				colvarReturnDetailTotalAmt.IsReadOnly = false;
				
						colvarReturnDetailTotalAmt.DefaultSetting = @"((0))";
				colvarReturnDetailTotalAmt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnDetailTotalAmt);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_returned_detail",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ReturnedId")]
		[Bindable(true)]
		public int ReturnedId 
		{
			get { return GetColumnValue<int>(Columns.ReturnedId); }
			set { SetColumnValue(Columns.ReturnedId, value); }
		}
		  
		[XmlAttribute("OrderDetailGuid")]
		[Bindable(true)]
		public Guid OrderDetailGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderDetailGuid); }
			set { SetColumnValue(Columns.OrderDetailGuid, value); }
		}
		  
		[XmlAttribute("HiDealId")]
		[Bindable(true)]
		public int HiDealId 
		{
			get { return GetColumnValue<int>(Columns.HiDealId); }
			set { SetColumnValue(Columns.HiDealId, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName 
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}
		  
		[XmlAttribute("ProductId")]
		[Bindable(true)]
		public int ProductId 
		{
			get { return GetColumnValue<int>(Columns.ProductId); }
			set { SetColumnValue(Columns.ProductId, value); }
		}
		  
		[XmlAttribute("ProductName")]
		[Bindable(true)]
		public string ProductName 
		{
			get { return GetColumnValue<string>(Columns.ProductName); }
			set { SetColumnValue(Columns.ProductName, value); }
		}
		  
		[XmlAttribute("ItemQuantity")]
		[Bindable(true)]
		public int ItemQuantity 
		{
			get { return GetColumnValue<int>(Columns.ItemQuantity); }
			set { SetColumnValue(Columns.ItemQuantity, value); }
		}
		  
		[XmlAttribute("UnitPrice")]
		[Bindable(true)]
		public decimal UnitPrice 
		{
			get { return GetColumnValue<decimal>(Columns.UnitPrice); }
			set { SetColumnValue(Columns.UnitPrice, value); }
		}
		  
		[XmlAttribute("DetailTotalAmt")]
		[Bindable(true)]
		public decimal DetailTotalAmt 
		{
			get { return GetColumnValue<decimal>(Columns.DetailTotalAmt); }
			set { SetColumnValue(Columns.DetailTotalAmt, value); }
		}
		  
		[XmlAttribute("Category1")]
		[Bindable(true)]
		public int? Category1 
		{
			get { return GetColumnValue<int?>(Columns.Category1); }
			set { SetColumnValue(Columns.Category1, value); }
		}
		  
		[XmlAttribute("Option1")]
		[Bindable(true)]
		public int? Option1 
		{
			get { return GetColumnValue<int?>(Columns.Option1); }
			set { SetColumnValue(Columns.Option1, value); }
		}
		  
		[XmlAttribute("OptionName1")]
		[Bindable(true)]
		public string OptionName1 
		{
			get { return GetColumnValue<string>(Columns.OptionName1); }
			set { SetColumnValue(Columns.OptionName1, value); }
		}
		  
		[XmlAttribute("Category2")]
		[Bindable(true)]
		public int? Category2 
		{
			get { return GetColumnValue<int?>(Columns.Category2); }
			set { SetColumnValue(Columns.Category2, value); }
		}
		  
		[XmlAttribute("Option2")]
		[Bindable(true)]
		public int? Option2 
		{
			get { return GetColumnValue<int?>(Columns.Option2); }
			set { SetColumnValue(Columns.Option2, value); }
		}
		  
		[XmlAttribute("OptionName2")]
		[Bindable(true)]
		public string OptionName2 
		{
			get { return GetColumnValue<string>(Columns.OptionName2); }
			set { SetColumnValue(Columns.OptionName2, value); }
		}
		  
		[XmlAttribute("Category3")]
		[Bindable(true)]
		public int? Category3 
		{
			get { return GetColumnValue<int?>(Columns.Category3); }
			set { SetColumnValue(Columns.Category3, value); }
		}
		  
		[XmlAttribute("Option3")]
		[Bindable(true)]
		public int? Option3 
		{
			get { return GetColumnValue<int?>(Columns.Option3); }
			set { SetColumnValue(Columns.Option3, value); }
		}
		  
		[XmlAttribute("OptionName3")]
		[Bindable(true)]
		public string OptionName3 
		{
			get { return GetColumnValue<string>(Columns.OptionName3); }
			set { SetColumnValue(Columns.OptionName3, value); }
		}
		  
		[XmlAttribute("Category4")]
		[Bindable(true)]
		public int? Category4 
		{
			get { return GetColumnValue<int?>(Columns.Category4); }
			set { SetColumnValue(Columns.Category4, value); }
		}
		  
		[XmlAttribute("Option4")]
		[Bindable(true)]
		public int? Option4 
		{
			get { return GetColumnValue<int?>(Columns.Option4); }
			set { SetColumnValue(Columns.Option4, value); }
		}
		  
		[XmlAttribute("OptionName4")]
		[Bindable(true)]
		public string OptionName4 
		{
			get { return GetColumnValue<string>(Columns.OptionName4); }
			set { SetColumnValue(Columns.OptionName4, value); }
		}
		  
		[XmlAttribute("Category5")]
		[Bindable(true)]
		public int? Category5 
		{
			get { return GetColumnValue<int?>(Columns.Category5); }
			set { SetColumnValue(Columns.Category5, value); }
		}
		  
		[XmlAttribute("Option5")]
		[Bindable(true)]
		public int? Option5 
		{
			get { return GetColumnValue<int?>(Columns.Option5); }
			set { SetColumnValue(Columns.Option5, value); }
		}
		  
		[XmlAttribute("OptionName5")]
		[Bindable(true)]
		public string OptionName5 
		{
			get { return GetColumnValue<string>(Columns.OptionName5); }
			set { SetColumnValue(Columns.OptionName5, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid? StoreGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int DeliveryType 
		{
			get { return GetColumnValue<int>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ProductType")]
		[Bindable(true)]
		public int ProductType 
		{
			get { return GetColumnValue<int>(Columns.ProductType); }
			set { SetColumnValue(Columns.ProductType, value); }
		}
		  
		[XmlAttribute("ReturnItemQuantity")]
		[Bindable(true)]
		public int ReturnItemQuantity 
		{
			get { return GetColumnValue<int>(Columns.ReturnItemQuantity); }
			set { SetColumnValue(Columns.ReturnItemQuantity, value); }
		}
		  
		[XmlAttribute("ReturnDetailTotalAmt")]
		[Bindable(true)]
		public int ReturnDetailTotalAmt 
		{
			get { return GetColumnValue<int>(Columns.ReturnDetailTotalAmt); }
			set { SetColumnValue(Columns.ReturnDetailTotalAmt, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (5)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnedIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderDetailGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn HiDealIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerNameColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductNameColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemQuantityColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn UnitPriceColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn DetailTotalAmtColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn Category1Column
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn Option1Column
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn OptionName1Column
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn Category2Column
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn Option2Column
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn OptionName2Column
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn Category3Column
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn Option3Column
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn OptionName3Column
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn Category4Column
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn Option4Column
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn OptionName4Column
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn Category5Column
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn Option5Column
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn OptionName5Column
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryTypeColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductTypeColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnItemQuantityColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnDetailTotalAmtColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ReturnedId = @"returned_id";
			 public static string OrderDetailGuid = @"order_detail_guid";
			 public static string HiDealId = @"hi_deal_id";
			 public static string SellerGuid = @"seller_guid";
			 public static string SellerName = @"seller_name";
			 public static string ProductId = @"product_id";
			 public static string ProductName = @"product_name";
			 public static string ItemQuantity = @"item_quantity";
			 public static string UnitPrice = @"unit_price";
			 public static string DetailTotalAmt = @"detail_total_amt";
			 public static string Category1 = @"category1";
			 public static string Option1 = @"option1";
			 public static string OptionName1 = @"option_name1";
			 public static string Category2 = @"category2";
			 public static string Option2 = @"option2";
			 public static string OptionName2 = @"option_name2";
			 public static string Category3 = @"category3";
			 public static string Option3 = @"option3";
			 public static string OptionName3 = @"option_name3";
			 public static string Category4 = @"category4";
			 public static string Option4 = @"option4";
			 public static string OptionName4 = @"option_name4";
			 public static string Category5 = @"category5";
			 public static string Option5 = @"option5";
			 public static string OptionName5 = @"option_name5";
			 public static string StoreGuid = @"store_guid";
			 public static string DeliveryType = @"delivery_type";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string ProductType = @"product_type";
			 public static string ReturnItemQuantity = @"return_item_quantity";
			 public static string ReturnDetailTotalAmt = @"return_detail_total_amt";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    public partial class SPs{
        
        /// <summary>
        /// Creates an object wrapper for the AssociateKeys Procedure
        /// </summary>
        public static StoredProcedure AssociateKeys(long? surrogateInstanceId, string keysToAssociate, byte[] concatenatedKeyProperties, byte? encodingOption, Guid? singleKeyId)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("AssociateKeys", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@surrogateInstanceId", surrogateInstanceId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@keysToAssociate", keysToAssociate, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@concatenatedKeyProperties", concatenatedKeyProperties, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@encodingOption", encodingOption, DbType.Byte, 0, 3);
        	
            sp.Command.AddParameter("@singleKeyId", singleKeyId, DbType.Guid, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the CompleteKeys Procedure
        /// </summary>
        public static StoredProcedure CompleteKeys(long? surrogateInstanceId, string keysToComplete)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("CompleteKeys", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@surrogateInstanceId", surrogateInstanceId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@keysToComplete", keysToComplete, DbType.AnsiString, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the CreateInstance Procedure
        /// </summary>
        public static StoredProcedure CreateInstance(Guid? instanceId, long? surrogateLockOwnerId, Guid? workflowHostType, long? serviceDeploymentId, long? surrogateInstanceId, int? result)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("CreateInstance", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@instanceId", instanceId, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@surrogateLockOwnerId", surrogateLockOwnerId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@workflowHostType", workflowHostType, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@serviceDeploymentId", serviceDeploymentId, DbType.Int64, 0, 19);
        	
            sp.Command.AddOutputParameter("@surrogateInstanceId", DbType.Int64, 0, 19);
            
            sp.Command.AddOutputParameter("@result", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the CreateLockOwner Procedure
        /// </summary>
        public static StoredProcedure CreateLockOwner(Guid? lockOwnerId, int? lockTimeout, Guid? workflowHostType, bool? enqueueCommand, bool? deleteInstanceOnCompletion, byte[] primitiveLockOwnerData, byte[] complexLockOwnerData, byte[] writeOnlyPrimitiveLockOwnerData, byte[] writeOnlyComplexLockOwnerData, byte? encodingOption, string machineName)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("CreateLockOwner", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@lockOwnerId", lockOwnerId, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@lockTimeout", lockTimeout, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@workflowHostType", workflowHostType, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@enqueueCommand", enqueueCommand, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@deleteInstanceOnCompletion", deleteInstanceOnCompletion, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@primitiveLockOwnerData", primitiveLockOwnerData, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@complexLockOwnerData", complexLockOwnerData, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@writeOnlyPrimitiveLockOwnerData", writeOnlyPrimitiveLockOwnerData, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@writeOnlyComplexLockOwnerData", writeOnlyComplexLockOwnerData, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@encodingOption", encodingOption, DbType.Byte, 0, 3);
        	
            sp.Command.AddParameter("@machineName", machineName, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the CreateServiceDeployment Procedure
        /// </summary>
        public static StoredProcedure CreateServiceDeployment(Guid? serviceDeploymentHash, string siteName, string relativeServicePath, string relativeApplicationPath, string serviceName, string serviceNamespace, long? serviceDeploymentId)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("CreateServiceDeployment", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@serviceDeploymentHash", serviceDeploymentHash, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@siteName", siteName, DbType.String, null, null);
        	
            sp.Command.AddParameter("@relativeServicePath", relativeServicePath, DbType.String, null, null);
        	
            sp.Command.AddParameter("@relativeApplicationPath", relativeApplicationPath, DbType.String, null, null);
        	
            sp.Command.AddParameter("@serviceName", serviceName, DbType.String, null, null);
        	
            sp.Command.AddParameter("@serviceNamespace", serviceNamespace, DbType.String, null, null);
        	
            sp.Command.AddOutputParameter("@serviceDeploymentId", DbType.Int64, 0, 19);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the DeleteInstance Procedure
        /// </summary>
        public static StoredProcedure DeleteInstance(long? surrogateInstanceId)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("DeleteInstance", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@surrogateInstanceId", surrogateInstanceId, DbType.Int64, 0, 19);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the DeleteLockOwner Procedure
        /// </summary>
        public static StoredProcedure DeleteLockOwner(long? surrogateLockOwnerId)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("DeleteLockOwner", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@surrogateLockOwnerId", surrogateLockOwnerId, DbType.Int64, 0, 19);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the DetectRunnableInstances Procedure
        /// </summary>
        public static StoredProcedure DetectRunnableInstances(Guid? workflowHostType)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("DetectRunnableInstances", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@workflowHostType", workflowHostType, DbType.Guid, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the ExtendLock Procedure
        /// </summary>
        public static StoredProcedure ExtendLock(long? surrogateLockOwnerId, int? lockTimeout)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("ExtendLock", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@surrogateLockOwnerId", surrogateLockOwnerId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@lockTimeout", lockTimeout, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the FreeKeys Procedure
        /// </summary>
        public static StoredProcedure FreeKeys(long? surrogateInstanceId, string keysToFree)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("FreeKeys", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@surrogateInstanceId", surrogateInstanceId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@keysToFree", keysToFree, DbType.AnsiString, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the GetActivatableWorkflowsActivationParameters Procedure
        /// </summary>
        public static StoredProcedure GetActivatableWorkflowsActivationParameters(string machineName)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("GetActivatableWorkflowsActivationParameters", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@machineName", machineName, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the InsertPromotedProperties Procedure
        /// </summary>
        public static StoredProcedure InsertPromotedProperties(Guid? instanceId, string promotionName, string value1, string value2, string value3, string value4, string value5, string value6, string value7, string value8, string value9, string value10, string value11, string value12, string value13, string value14, string value15, string value16, string value17, string value18, string value19, string value20, string value21, string value22, string value23, string value24, string value25, string value26, string value27, string value28, string value29, string value30, string value31, string value32, byte[] value33, byte[] value34, byte[] value35, byte[] value36, byte[] value37, byte[] value38, byte[] value39, byte[] value40, byte[] value41, byte[] value42, byte[] value43, byte[] value44, byte[] value45, byte[] value46, byte[] value47, byte[] value48, byte[] value49, byte[] value50, byte[] value51, byte[] value52, byte[] value53, byte[] value54, byte[] value55, byte[] value56, byte[] value57, byte[] value58, byte[] value59, byte[] value60, byte[] value61, byte[] value62, byte[] value63, byte[] value64)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("InsertPromotedProperties", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@instanceId", instanceId, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@promotionName", promotionName, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value1", value1, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value2", value2, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value3", value3, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value4", value4, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value5", value5, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value6", value6, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value7", value7, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value8", value8, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value9", value9, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value10", value10, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value11", value11, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value12", value12, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value13", value13, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value14", value14, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value15", value15, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value16", value16, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value17", value17, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value18", value18, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value19", value19, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value20", value20, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value21", value21, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value22", value22, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value23", value23, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value24", value24, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value25", value25, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value26", value26, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value27", value27, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value28", value28, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value29", value29, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value30", value30, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value31", value31, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value32", value32, DbType.String, null, null);
        	
            sp.Command.AddParameter("@value33", value33, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value34", value34, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value35", value35, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value36", value36, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value37", value37, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value38", value38, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value39", value39, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value40", value40, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value41", value41, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value42", value42, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value43", value43, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value44", value44, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value45", value45, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value46", value46, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value47", value47, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value48", value48, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value49", value49, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value50", value50, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value51", value51, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value52", value52, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value53", value53, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value54", value54, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value55", value55, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value56", value56, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value57", value57, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value58", value58, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value59", value59, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value60", value60, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value61", value61, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value62", value62, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value63", value63, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@value64", value64, DbType.Binary, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the InsertRunnableInstanceEntry Procedure
        /// </summary>
        public static StoredProcedure InsertRunnableInstanceEntry(long? surrogateInstanceId, Guid? workflowHostType, long? serviceDeploymentId, bool? isSuspended, bool? isReadyToRun, DateTime? pendingTimer)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("InsertRunnableInstanceEntry", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@surrogateInstanceId", surrogateInstanceId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@workflowHostType", workflowHostType, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@serviceDeploymentId", serviceDeploymentId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@isSuspended", isSuspended, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@isReadyToRun", isReadyToRun, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@pendingTimer", pendingTimer, DbType.DateTime, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the LoadInstance Procedure
        /// </summary>
        public static StoredProcedure LoadInstance(long? surrogateLockOwnerId, byte? operationType, long? handleInstanceVersion, bool? handleIsBoundToLock, Guid? keyToLoadBy, Guid? instanceId, string keysToAssociate, byte? encodingOption, byte[] concatenatedKeyProperties, Guid? singleKeyId, int? operationTimeout)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("LoadInstance", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@surrogateLockOwnerId", surrogateLockOwnerId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@operationType", operationType, DbType.Byte, 0, 3);
        	
            sp.Command.AddParameter("@handleInstanceVersion", handleInstanceVersion, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@handleIsBoundToLock", handleIsBoundToLock, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@keyToLoadBy", keyToLoadBy, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@instanceId", instanceId, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@keysToAssociate", keysToAssociate, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@encodingOption", encodingOption, DbType.Byte, 0, 3);
        	
            sp.Command.AddParameter("@concatenatedKeyProperties", concatenatedKeyProperties, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@singleKeyId", singleKeyId, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@operationTimeout", operationTimeout, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the LockInstance Procedure
        /// </summary>
        public static StoredProcedure LockInstance(Guid? instanceId, long? surrogateLockOwnerId, long? handleInstanceVersion, bool? handleIsBoundToLock, long? surrogateInstanceId, long? lockVersion, int? result)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("LockInstance", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@instanceId", instanceId, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@surrogateLockOwnerId", surrogateLockOwnerId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@handleInstanceVersion", handleInstanceVersion, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@handleIsBoundToLock", handleIsBoundToLock, DbType.Boolean, null, null);
        	
            sp.Command.AddOutputParameter("@surrogateInstanceId", DbType.Int64, 0, 19);
            
            sp.Command.AddOutputParameter("@lockVersion", DbType.Int64, 0, 19);
            
            sp.Command.AddOutputParameter("@result", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the RecoverInstanceLocks Procedure
        /// </summary>
        public static StoredProcedure RecoverInstanceLocks()
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("RecoverInstanceLocks", DataService.GetInstance("LKSiteDB"), "");
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the SaveInstance Procedure
        /// </summary>
        public static StoredProcedure SaveInstance(Guid? instanceId, long? surrogateLockOwnerId, long? handleInstanceVersion, bool? handleIsBoundToLock, byte[] primitiveDataProperties, byte[] complexDataProperties, byte[] writeOnlyPrimitiveDataProperties, byte[] writeOnlyComplexDataProperties, byte[] metadataProperties, bool? metadataIsConsistent, byte? encodingOption, long? timerDurationMilliseconds, byte? suspensionStateChange, string suspensionReason, string suspensionExceptionName, string keysToAssociate, string keysToComplete, string keysToFree, byte[] concatenatedKeyProperties, bool? unlockInstance, bool? isReadyToRun, bool? isCompleted, Guid? singleKeyId, string lastMachineRunOn, string executionStatus, string blockingBookmarks, Guid? workflowHostType, long? serviceDeploymentId, int? operationTimeout)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("SaveInstance", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@instanceId", instanceId, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@surrogateLockOwnerId", surrogateLockOwnerId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@handleInstanceVersion", handleInstanceVersion, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@handleIsBoundToLock", handleIsBoundToLock, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@primitiveDataProperties", primitiveDataProperties, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@complexDataProperties", complexDataProperties, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@writeOnlyPrimitiveDataProperties", writeOnlyPrimitiveDataProperties, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@writeOnlyComplexDataProperties", writeOnlyComplexDataProperties, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@metadataProperties", metadataProperties, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@metadataIsConsistent", metadataIsConsistent, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@encodingOption", encodingOption, DbType.Byte, 0, 3);
        	
            sp.Command.AddParameter("@timerDurationMilliseconds", timerDurationMilliseconds, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@suspensionStateChange", suspensionStateChange, DbType.Byte, 0, 3);
        	
            sp.Command.AddParameter("@suspensionReason", suspensionReason, DbType.String, null, null);
        	
            sp.Command.AddParameter("@suspensionExceptionName", suspensionExceptionName, DbType.String, null, null);
        	
            sp.Command.AddParameter("@keysToAssociate", keysToAssociate, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@keysToComplete", keysToComplete, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@keysToFree", keysToFree, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@concatenatedKeyProperties", concatenatedKeyProperties, DbType.Binary, null, null);
        	
            sp.Command.AddParameter("@unlockInstance", unlockInstance, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@isReadyToRun", isReadyToRun, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@isCompleted", isCompleted, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@singleKeyId", singleKeyId, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@lastMachineRunOn", lastMachineRunOn, DbType.String, null, null);
        	
            sp.Command.AddParameter("@executionStatus", executionStatus, DbType.String, null, null);
        	
            sp.Command.AddParameter("@blockingBookmarks", blockingBookmarks, DbType.String, null, null);
        	
            sp.Command.AddParameter("@workflowHostType", workflowHostType, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@serviceDeploymentId", serviceDeploymentId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@operationTimeout", operationTimeout, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the sp_UpdateTrustCouponInfo Procedure
        /// </summary>
        public static StoredProcedure SpUpdateTrustCouponInfo(string sequencenumber, int? couponid, Guid? trustid)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("sp_UpdateTrustCouponInfo", DataService.GetInstance("LKSiteDB"), "dbo");
        	
            sp.Command.AddParameter("@sequence_number", sequencenumber, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@coupon_id", couponid, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@trust_id", trustid, DbType.Guid, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the TryLoadRunnableInstance Procedure
        /// </summary>
        public static StoredProcedure TryLoadRunnableInstance(long? surrogateLockOwnerId, Guid? workflowHostType, byte? operationType, long? handleInstanceVersion, bool? handleIsBoundToLock, byte? encodingOption, int? operationTimeout)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("TryLoadRunnableInstance", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@surrogateLockOwnerId", surrogateLockOwnerId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@workflowHostType", workflowHostType, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@operationType", operationType, DbType.Byte, 0, 3);
        	
            sp.Command.AddParameter("@handleInstanceVersion", handleInstanceVersion, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@handleIsBoundToLock", handleIsBoundToLock, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@encodingOption", encodingOption, DbType.Byte, 0, 3);
        	
            sp.Command.AddParameter("@operationTimeout", operationTimeout, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the UnlockInstance Procedure
        /// </summary>
        public static StoredProcedure UnlockInstance(long? surrogateLockOwnerId, Guid? instanceId, long? handleInstanceVersion)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("UnlockInstance", DataService.GetInstance("LKSiteDB"), "System.Activities.DurableInstancing");
        	
            sp.Command.AddParameter("@surrogateLockOwnerId", surrogateLockOwnerId, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@instanceId", instanceId, DbType.Guid, null, null);
        	
            sp.Command.AddParameter("@handleInstanceVersion", handleInstanceVersion, DbType.Int64, 0, 19);
        	
            return sp;
        }

        public static StoredProcedure PreventingOversell(Guid bid, int qty, int userId, int saleMultipleBase, int timeoutSec, int orderLimit) 
        {
            StoredProcedure sp = new SubSonic.StoredProcedure("sp_preventingOversell", DataService.GetInstance("LKSiteDB"), "dbo");
            
            sp.Command.AddParameter("@bid", bid, DbType.Guid);
            sp.Command.AddParameter("@qty", qty, DbType.Int32);
            sp.Command.AddParameter("@userId", userId, DbType.Int32);
            sp.Command.AddParameter("@saleMultipleBase", saleMultipleBase, DbType.Int32);
            sp.Command.AddParameter("@timeoutSec", timeoutSec, DbType.Int32);
            sp.Command.AddParameter("@orderTotalLimit", orderLimit, DbType.Int32);
            sp.Command.AddOutputParameter("@pid", DbType.Int32);
            sp.Command.AddOutputParameter("@checkResult", DbType.Byte);
            sp.Command.AddOutputParameter("@msg", 100, DbType.String);

            return sp;
        }

        public static StoredProcedure PrizeOverdraw(int itemId, int qty, int userId, int timeoutSec, int prizeLimit)
        {
            StoredProcedure sp = new SubSonic.StoredProcedure("sp_prizeOverdraw", DataService.GetInstance("LKSiteDB"), "dbo");

            sp.Command.AddParameter("@itemId", itemId, DbType.Int32);
            sp.Command.AddParameter("@qty", qty, DbType.Int32);
            sp.Command.AddParameter("@userId", userId, DbType.Int32);
            sp.Command.AddParameter("@timeoutSec", timeoutSec, DbType.Int32);
            sp.Command.AddParameter("@prizeTotalLimit", prizeLimit, DbType.Int32);
            sp.Command.AddOutputParameter("@pid", DbType.Int32);
            sp.Command.AddOutputParameter("@checkResult", DbType.Byte);
            sp.Command.AddOutputParameter("@msg", 100, DbType.String);

            return sp;
        }
    }
    
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BrandDiscountStatusChangeLog class.
	/// </summary>
    [Serializable]
	public partial class BrandDiscountStatusChangeLogCollection : RepositoryList<BrandDiscountStatusChangeLog, BrandDiscountStatusChangeLogCollection>
	{	   
		public BrandDiscountStatusChangeLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BrandDiscountStatusChangeLogCollection</returns>
		public BrandDiscountStatusChangeLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BrandDiscountStatusChangeLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the brand_discount_status_change_log table.
	/// </summary>
	[Serializable]
	public partial class BrandDiscountStatusChangeLog : RepositoryRecord<BrandDiscountStatusChangeLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BrandDiscountStatusChangeLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BrandDiscountStatusChangeLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("brand_discount_status_change_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBrandId = new TableSchema.TableColumn(schema);
				colvarBrandId.ColumnName = "brand_id";
				colvarBrandId.DataType = DbType.Int32;
				colvarBrandId.MaxLength = 0;
				colvarBrandId.AutoIncrement = false;
				colvarBrandId.IsNullable = false;
				colvarBrandId.IsPrimaryKey = false;
				colvarBrandId.IsForeignKey = false;
				colvarBrandId.IsReadOnly = false;
				colvarBrandId.DefaultSetting = @"";
				colvarBrandId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandId);
				
				TableSchema.TableColumn colvarBrandItemId = new TableSchema.TableColumn(schema);
				colvarBrandItemId.ColumnName = "brand_item_id";
				colvarBrandItemId.DataType = DbType.Int32;
				colvarBrandItemId.MaxLength = 0;
				colvarBrandItemId.AutoIncrement = false;
				colvarBrandItemId.IsNullable = false;
				colvarBrandItemId.IsPrimaryKey = false;
				colvarBrandItemId.IsForeignKey = false;
				colvarBrandItemId.IsReadOnly = false;
				colvarBrandItemId.DefaultSetting = @"";
				colvarBrandItemId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandItemId);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarOldBusinessHourStatus = new TableSchema.TableColumn(schema);
				colvarOldBusinessHourStatus.ColumnName = "old_business_hour_status";
				colvarOldBusinessHourStatus.DataType = DbType.Int32;
				colvarOldBusinessHourStatus.MaxLength = 0;
				colvarOldBusinessHourStatus.AutoIncrement = false;
				colvarOldBusinessHourStatus.IsNullable = false;
				colvarOldBusinessHourStatus.IsPrimaryKey = false;
				colvarOldBusinessHourStatus.IsForeignKey = false;
				colvarOldBusinessHourStatus.IsReadOnly = false;
				colvarOldBusinessHourStatus.DefaultSetting = @"";
				colvarOldBusinessHourStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOldBusinessHourStatus);
				
				TableSchema.TableColumn colvarNewBusinessHourStatus = new TableSchema.TableColumn(schema);
				colvarNewBusinessHourStatus.ColumnName = "new_business_hour_status";
				colvarNewBusinessHourStatus.DataType = DbType.Int32;
				colvarNewBusinessHourStatus.MaxLength = 0;
				colvarNewBusinessHourStatus.AutoIncrement = false;
				colvarNewBusinessHourStatus.IsNullable = false;
				colvarNewBusinessHourStatus.IsPrimaryKey = false;
				colvarNewBusinessHourStatus.IsForeignKey = false;
				colvarNewBusinessHourStatus.IsReadOnly = false;
				colvarNewBusinessHourStatus.DefaultSetting = @"";
				colvarNewBusinessHourStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNewBusinessHourStatus);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 250;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 250;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("brand_discount_status_change_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("BrandId")]
		[Bindable(true)]
		public int BrandId 
		{
			get { return GetColumnValue<int>(Columns.BrandId); }
			set { SetColumnValue(Columns.BrandId, value); }
		}
		  
		[XmlAttribute("BrandItemId")]
		[Bindable(true)]
		public int BrandItemId 
		{
			get { return GetColumnValue<int>(Columns.BrandItemId); }
			set { SetColumnValue(Columns.BrandItemId, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("OldBusinessHourStatus")]
		[Bindable(true)]
		public int OldBusinessHourStatus 
		{
			get { return GetColumnValue<int>(Columns.OldBusinessHourStatus); }
			set { SetColumnValue(Columns.OldBusinessHourStatus, value); }
		}
		  
		[XmlAttribute("NewBusinessHourStatus")]
		[Bindable(true)]
		public int NewBusinessHourStatus 
		{
			get { return GetColumnValue<int>(Columns.NewBusinessHourStatus); }
			set { SetColumnValue(Columns.NewBusinessHourStatus, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BrandIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BrandItemIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OldBusinessHourStatusColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn NewBusinessHourStatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BrandId = @"brand_id";
			 public static string BrandItemId = @"brand_item_id";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string OldBusinessHourStatus = @"old_business_hour_status";
			 public static string NewBusinessHourStatus = @"new_business_hour_status";
			 public static string Type = @"type";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

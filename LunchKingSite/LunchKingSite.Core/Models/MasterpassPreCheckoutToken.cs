using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MasterpassPreCheckoutToken class.
	/// </summary>
    [Serializable]
	public partial class MasterpassPreCheckoutTokenCollection : RepositoryList<MasterpassPreCheckoutToken, MasterpassPreCheckoutTokenCollection>
	{	   
		public MasterpassPreCheckoutTokenCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MasterpassPreCheckoutTokenCollection</returns>
		public MasterpassPreCheckoutTokenCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MasterpassPreCheckoutToken o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the masterpass_pre_checkout_token table.
	/// </summary>
	[Serializable]
	public partial class MasterpassPreCheckoutToken : RepositoryRecord<MasterpassPreCheckoutToken>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MasterpassPreCheckoutToken()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MasterpassPreCheckoutToken(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("masterpass_pre_checkout_token", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = true;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarAccessToken = new TableSchema.TableColumn(schema);
				colvarAccessToken.ColumnName = "access_token";
				colvarAccessToken.DataType = DbType.AnsiString;
				colvarAccessToken.MaxLength = 50;
				colvarAccessToken.AutoIncrement = false;
				colvarAccessToken.IsNullable = false;
				colvarAccessToken.IsPrimaryKey = false;
				colvarAccessToken.IsForeignKey = false;
				colvarAccessToken.IsReadOnly = false;
				colvarAccessToken.DefaultSetting = @"";
				colvarAccessToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessToken);
				
				TableSchema.TableColumn colvarOauthSecret = new TableSchema.TableColumn(schema);
				colvarOauthSecret.ColumnName = "oauth_secret";
				colvarOauthSecret.DataType = DbType.AnsiString;
				colvarOauthSecret.MaxLength = 50;
				colvarOauthSecret.AutoIncrement = false;
				colvarOauthSecret.IsNullable = false;
				colvarOauthSecret.IsPrimaryKey = false;
				colvarOauthSecret.IsForeignKey = false;
				colvarOauthSecret.IsReadOnly = false;
				colvarOauthSecret.DefaultSetting = @"";
				colvarOauthSecret.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOauthSecret);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				
						colvarModifyTime.DefaultSetting = @"(getdate())";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarIsUsed = new TableSchema.TableColumn(schema);
				colvarIsUsed.ColumnName = "is_used";
				colvarIsUsed.DataType = DbType.Boolean;
				colvarIsUsed.MaxLength = 0;
				colvarIsUsed.AutoIncrement = false;
				colvarIsUsed.IsNullable = false;
				colvarIsUsed.IsPrimaryKey = false;
				colvarIsUsed.IsForeignKey = false;
				colvarIsUsed.IsReadOnly = false;
				
						colvarIsUsed.DefaultSetting = @"((0))";
				colvarIsUsed.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsUsed);
				
				TableSchema.TableColumn colvarFailReason = new TableSchema.TableColumn(schema);
				colvarFailReason.ColumnName = "fail_reason";
				colvarFailReason.DataType = DbType.String;
				colvarFailReason.MaxLength = -1;
				colvarFailReason.AutoIncrement = false;
				colvarFailReason.IsNullable = true;
				colvarFailReason.IsPrimaryKey = false;
				colvarFailReason.IsForeignKey = false;
				colvarFailReason.IsReadOnly = false;
				colvarFailReason.DefaultSetting = @"";
				colvarFailReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailReason);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("masterpass_pre_checkout_token",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("AccessToken")]
		[Bindable(true)]
		public string AccessToken 
		{
			get { return GetColumnValue<string>(Columns.AccessToken); }
			set { SetColumnValue(Columns.AccessToken, value); }
		}
		  
		[XmlAttribute("OauthSecret")]
		[Bindable(true)]
		public string OauthSecret 
		{
			get { return GetColumnValue<string>(Columns.OauthSecret); }
			set { SetColumnValue(Columns.OauthSecret, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("IsUsed")]
		[Bindable(true)]
		public bool IsUsed 
		{
			get { return GetColumnValue<bool>(Columns.IsUsed); }
			set { SetColumnValue(Columns.IsUsed, value); }
		}
		  
		[XmlAttribute("FailReason")]
		[Bindable(true)]
		public string FailReason 
		{
			get { return GetColumnValue<string>(Columns.FailReason); }
			set { SetColumnValue(Columns.FailReason, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessTokenColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OauthSecretColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IsUsedColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn FailReasonColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string UserId = @"user_id";
			 public static string AccessToken = @"access_token";
			 public static string OauthSecret = @"oauth_secret";
			 public static string ModifyTime = @"modify_time";
			 public static string IsUsed = @"is_used";
			 public static string FailReason = @"fail_reason";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

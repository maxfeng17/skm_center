using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealCity class.
	/// </summary>
    [Serializable]
	public partial class HiDealCityCollection : RepositoryList<HiDealCity, HiDealCityCollection>
	{	   
		public HiDealCityCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealCityCollection</returns>
		public HiDealCityCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealCity o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_city table.
	/// </summary>
	[Serializable]
	public partial class HiDealCity : RepositoryRecord<HiDealCity>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealCity()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealCity(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_city", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarHiDealId = new TableSchema.TableColumn(schema);
				colvarHiDealId.ColumnName = "hi_deal_id";
				colvarHiDealId.DataType = DbType.Int32;
				colvarHiDealId.MaxLength = 0;
				colvarHiDealId.AutoIncrement = false;
				colvarHiDealId.IsNullable = false;
				colvarHiDealId.IsPrimaryKey = false;
				colvarHiDealId.IsForeignKey = true;
				colvarHiDealId.IsReadOnly = false;
				colvarHiDealId.DefaultSetting = @"";
				
					colvarHiDealId.ForeignKeyTableName = "hi_deal_deal";
				schema.Columns.Add(colvarHiDealId);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = true;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				colvarSeq.DefaultSetting = @"";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				TableSchema.TableColumn colvarCityCodeId = new TableSchema.TableColumn(schema);
				colvarCityCodeId.ColumnName = "city_code_id";
				colvarCityCodeId.DataType = DbType.Int32;
				colvarCityCodeId.MaxLength = 0;
				colvarCityCodeId.AutoIncrement = false;
				colvarCityCodeId.IsNullable = false;
				colvarCityCodeId.IsPrimaryKey = false;
				colvarCityCodeId.IsForeignKey = true;
				colvarCityCodeId.IsReadOnly = false;
				colvarCityCodeId.DefaultSetting = @"";
				
					colvarCityCodeId.ForeignKeyTableName = "system_code";
				schema.Columns.Add(colvarCityCodeId);
				
				TableSchema.TableColumn colvarCityCodeGroup = new TableSchema.TableColumn(schema);
				colvarCityCodeGroup.ColumnName = "city_code_group";
				colvarCityCodeGroup.DataType = DbType.AnsiString;
				colvarCityCodeGroup.MaxLength = 50;
				colvarCityCodeGroup.AutoIncrement = false;
				colvarCityCodeGroup.IsNullable = false;
				colvarCityCodeGroup.IsPrimaryKey = false;
				colvarCityCodeGroup.IsForeignKey = true;
				colvarCityCodeGroup.IsReadOnly = false;
				colvarCityCodeGroup.DefaultSetting = @"";
				
					colvarCityCodeGroup.ForeignKeyTableName = "system_code";
				schema.Columns.Add(colvarCityCodeGroup);
				
				TableSchema.TableColumn colvarCity = new TableSchema.TableColumn(schema);
				colvarCity.ColumnName = "city";
				colvarCity.DataType = DbType.String;
				colvarCity.MaxLength = 50;
				colvarCity.AutoIncrement = false;
				colvarCity.IsNullable = false;
				colvarCity.IsPrimaryKey = false;
				colvarCity.IsForeignKey = false;
				colvarCity.IsReadOnly = false;
				colvarCity.DefaultSetting = @"";
				colvarCity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCity);
				
				TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
				colvarStartTime.ColumnName = "start_time";
				colvarStartTime.DataType = DbType.DateTime;
				colvarStartTime.MaxLength = 0;
				colvarStartTime.AutoIncrement = false;
				colvarStartTime.IsNullable = false;
				colvarStartTime.IsPrimaryKey = false;
				colvarStartTime.IsForeignKey = false;
				colvarStartTime.IsReadOnly = false;
				colvarStartTime.DefaultSetting = @"";
				colvarStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartTime);
				
				TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
				colvarEndTime.ColumnName = "end_time";
				colvarEndTime.DataType = DbType.DateTime;
				colvarEndTime.MaxLength = 0;
				colvarEndTime.AutoIncrement = false;
				colvarEndTime.IsNullable = true;
				colvarEndTime.IsPrimaryKey = false;
				colvarEndTime.IsForeignKey = false;
				colvarEndTime.IsReadOnly = false;
				colvarEndTime.DefaultSetting = @"";
				colvarEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndTime);
				
				TableSchema.TableColumn colvarIsMain = new TableSchema.TableColumn(schema);
				colvarIsMain.ColumnName = "is_main";
				colvarIsMain.DataType = DbType.Boolean;
				colvarIsMain.MaxLength = 0;
				colvarIsMain.AutoIncrement = false;
				colvarIsMain.IsNullable = false;
				colvarIsMain.IsPrimaryKey = false;
				colvarIsMain.IsForeignKey = false;
				colvarIsMain.IsReadOnly = false;
				
						colvarIsMain.DefaultSetting = @"((1))";
				colvarIsMain.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsMain);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_city",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("HiDealId")]
		[Bindable(true)]
		public int HiDealId 
		{
			get { return GetColumnValue<int>(Columns.HiDealId); }
			set { SetColumnValue(Columns.HiDealId, value); }
		}
		  
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int? Seq 
		{
			get { return GetColumnValue<int?>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		  
		[XmlAttribute("CityCodeId")]
		[Bindable(true)]
		public int CityCodeId 
		{
			get { return GetColumnValue<int>(Columns.CityCodeId); }
			set { SetColumnValue(Columns.CityCodeId, value); }
		}
		  
		[XmlAttribute("CityCodeGroup")]
		[Bindable(true)]
		public string CityCodeGroup 
		{
			get { return GetColumnValue<string>(Columns.CityCodeGroup); }
			set { SetColumnValue(Columns.CityCodeGroup, value); }
		}
		  
		[XmlAttribute("City")]
		[Bindable(true)]
		public string City 
		{
			get { return GetColumnValue<string>(Columns.City); }
			set { SetColumnValue(Columns.City, value); }
		}
		  
		[XmlAttribute("StartTime")]
		[Bindable(true)]
		public DateTime StartTime 
		{
			get { return GetColumnValue<DateTime>(Columns.StartTime); }
			set { SetColumnValue(Columns.StartTime, value); }
		}
		  
		[XmlAttribute("EndTime")]
		[Bindable(true)]
		public DateTime? EndTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.EndTime); }
			set { SetColumnValue(Columns.EndTime, value); }
		}
		  
		[XmlAttribute("IsMain")]
		[Bindable(true)]
		public bool IsMain 
		{
			get { return GetColumnValue<bool>(Columns.IsMain); }
			set { SetColumnValue(Columns.IsMain, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn HiDealIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CityCodeIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CityCodeGroupColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CityColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StartTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn EndTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IsMainColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string HiDealId = @"hi_deal_id";
			 public static string Seq = @"seq";
			 public static string CityCodeId = @"city_code_id";
			 public static string CityCodeGroup = @"city_code_group";
			 public static string City = @"city";
			 public static string StartTime = @"start_time";
			 public static string EndTime = @"end_time";
			 public static string IsMain = @"is_main";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

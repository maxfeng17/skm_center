using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealReturnedLight class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealReturnedLightCollection : ReadOnlyList<ViewHiDealReturnedLight, ViewHiDealReturnedLightCollection>
    {        
        public ViewHiDealReturnedLightCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_returned_light view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealReturnedLight : ReadOnlyRecord<ViewHiDealReturnedLight>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_returned_light", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = false;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 50;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarApplicationTime = new TableSchema.TableColumn(schema);
                colvarApplicationTime.ColumnName = "application_time";
                colvarApplicationTime.DataType = DbType.DateTime;
                colvarApplicationTime.MaxLength = 0;
                colvarApplicationTime.AutoIncrement = false;
                colvarApplicationTime.IsNullable = true;
                colvarApplicationTime.IsPrimaryKey = false;
                colvarApplicationTime.IsForeignKey = false;
                colvarApplicationTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarApplicationTime);
                
                TableSchema.TableColumn colvarReturnReason = new TableSchema.TableColumn(schema);
                colvarReturnReason.ColumnName = "return_reason";
                colvarReturnReason.DataType = DbType.String;
                colvarReturnReason.MaxLength = -1;
                colvarReturnReason.AutoIncrement = false;
                colvarReturnReason.IsNullable = true;
                colvarReturnReason.IsPrimaryKey = false;
                colvarReturnReason.IsForeignKey = false;
                colvarReturnReason.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnReason);
                
                TableSchema.TableColumn colvarCashBack = new TableSchema.TableColumn(schema);
                colvarCashBack.ColumnName = "cash_back";
                colvarCashBack.DataType = DbType.Boolean;
                colvarCashBack.MaxLength = 0;
                colvarCashBack.AutoIncrement = false;
                colvarCashBack.IsNullable = false;
                colvarCashBack.IsPrimaryKey = false;
                colvarCashBack.IsForeignKey = false;
                colvarCashBack.IsReadOnly = false;
                
                schema.Columns.Add(colvarCashBack);
                
                TableSchema.TableColumn colvarReturnedStatus = new TableSchema.TableColumn(schema);
                colvarReturnedStatus.ColumnName = "returned_status";
                colvarReturnedStatus.DataType = DbType.Int32;
                colvarReturnedStatus.MaxLength = 0;
                colvarReturnedStatus.AutoIncrement = false;
                colvarReturnedStatus.IsNullable = false;
                colvarReturnedStatus.IsPrimaryKey = false;
                colvarReturnedStatus.IsForeignKey = false;
                colvarReturnedStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedStatus);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
                colvarLastName.ColumnName = "last_name";
                colvarLastName.DataType = DbType.String;
                colvarLastName.MaxLength = 50;
                colvarLastName.AutoIncrement = false;
                colvarLastName.IsNullable = true;
                colvarLastName.IsPrimaryKey = false;
                colvarLastName.IsForeignKey = false;
                colvarLastName.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastName);
                
                TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
                colvarFirstName.ColumnName = "first_name";
                colvarFirstName.DataType = DbType.String;
                colvarFirstName.MaxLength = 50;
                colvarFirstName.AutoIncrement = false;
                colvarFirstName.IsNullable = true;
                colvarFirstName.IsPrimaryKey = false;
                colvarFirstName.IsForeignKey = false;
                colvarFirstName.IsReadOnly = false;
                
                schema.Columns.Add(colvarFirstName);
                
                TableSchema.TableColumn colvarReturnCancelReason = new TableSchema.TableColumn(schema);
                colvarReturnCancelReason.ColumnName = "return_cancel_reason";
                colvarReturnCancelReason.DataType = DbType.String;
                colvarReturnCancelReason.MaxLength = -1;
                colvarReturnCancelReason.AutoIncrement = false;
                colvarReturnCancelReason.IsNullable = true;
                colvarReturnCancelReason.IsPrimaryKey = false;
                colvarReturnCancelReason.IsForeignKey = false;
                colvarReturnCancelReason.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnCancelReason);
                
                TableSchema.TableColumn colvarReturnCancelTime = new TableSchema.TableColumn(schema);
                colvarReturnCancelTime.ColumnName = "return_cancel_time";
                colvarReturnCancelTime.DataType = DbType.DateTime;
                colvarReturnCancelTime.MaxLength = 0;
                colvarReturnCancelTime.AutoIncrement = false;
                colvarReturnCancelTime.IsNullable = true;
                colvarReturnCancelTime.IsPrimaryKey = false;
                colvarReturnCancelTime.IsForeignKey = false;
                colvarReturnCancelTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnCancelTime);
                
                TableSchema.TableColumn colvarReturnCancelUserName = new TableSchema.TableColumn(schema);
                colvarReturnCancelUserName.ColumnName = "return_cancel_user_name";
                colvarReturnCancelUserName.DataType = DbType.String;
                colvarReturnCancelUserName.MaxLength = 256;
                colvarReturnCancelUserName.AutoIncrement = false;
                colvarReturnCancelUserName.IsNullable = true;
                colvarReturnCancelUserName.IsPrimaryKey = false;
                colvarReturnCancelUserName.IsForeignKey = false;
                colvarReturnCancelUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnCancelUserName);
                
                TableSchema.TableColumn colvarReturnCompletedTime = new TableSchema.TableColumn(schema);
                colvarReturnCompletedTime.ColumnName = "return_completed_time";
                colvarReturnCompletedTime.DataType = DbType.DateTime;
                colvarReturnCompletedTime.MaxLength = 0;
                colvarReturnCompletedTime.AutoIncrement = false;
                colvarReturnCompletedTime.IsNullable = true;
                colvarReturnCompletedTime.IsPrimaryKey = false;
                colvarReturnCompletedTime.IsForeignKey = false;
                colvarReturnCompletedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnCompletedTime);
                
                TableSchema.TableColumn colvarReviewUserName = new TableSchema.TableColumn(schema);
                colvarReviewUserName.ColumnName = "review_user_name";
                colvarReviewUserName.DataType = DbType.String;
                colvarReviewUserName.MaxLength = 256;
                colvarReviewUserName.AutoIncrement = false;
                colvarReviewUserName.IsNullable = true;
                colvarReviewUserName.IsPrimaryKey = false;
                colvarReviewUserName.IsForeignKey = false;
                colvarReviewUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarReviewUserName);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "ProductId";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = true;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "ProductName";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 250;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = true;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductName);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
                colvarItemQuantity.ColumnName = "item_quantity";
                colvarItemQuantity.DataType = DbType.Int32;
                colvarItemQuantity.MaxLength = 0;
                colvarItemQuantity.AutoIncrement = false;
                colvarItemQuantity.IsNullable = false;
                colvarItemQuantity.IsPrimaryKey = false;
                colvarItemQuantity.IsForeignKey = false;
                colvarItemQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemQuantity);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "MemberName";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 100;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = true;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_returned_light",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealReturnedLight()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealReturnedLight(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealReturnedLight(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealReturnedLight(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_name");
		    }
            set 
		    {
			    SetColumnValue("deal_name", value);
            }
        }
	      
        [XmlAttribute("ApplicationTime")]
        [Bindable(true)]
        public DateTime? ApplicationTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("application_time");
		    }
            set 
		    {
			    SetColumnValue("application_time", value);
            }
        }
	      
        [XmlAttribute("ReturnReason")]
        [Bindable(true)]
        public string ReturnReason 
	    {
		    get
		    {
			    return GetColumnValue<string>("return_reason");
		    }
            set 
		    {
			    SetColumnValue("return_reason", value);
            }
        }
	      
        [XmlAttribute("CashBack")]
        [Bindable(true)]
        public bool CashBack 
	    {
		    get
		    {
			    return GetColumnValue<bool>("cash_back");
		    }
            set 
		    {
			    SetColumnValue("cash_back", value);
            }
        }
	      
        [XmlAttribute("ReturnedStatus")]
        [Bindable(true)]
        public int ReturnedStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("returned_status");
		    }
            set 
		    {
			    SetColumnValue("returned_status", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("LastName")]
        [Bindable(true)]
        public string LastName 
	    {
		    get
		    {
			    return GetColumnValue<string>("last_name");
		    }
            set 
		    {
			    SetColumnValue("last_name", value);
            }
        }
	      
        [XmlAttribute("FirstName")]
        [Bindable(true)]
        public string FirstName 
	    {
		    get
		    {
			    return GetColumnValue<string>("first_name");
		    }
            set 
		    {
			    SetColumnValue("first_name", value);
            }
        }
	      
        [XmlAttribute("ReturnCancelReason")]
        [Bindable(true)]
        public string ReturnCancelReason 
	    {
		    get
		    {
			    return GetColumnValue<string>("return_cancel_reason");
		    }
            set 
		    {
			    SetColumnValue("return_cancel_reason", value);
            }
        }
	      
        [XmlAttribute("ReturnCancelTime")]
        [Bindable(true)]
        public DateTime? ReturnCancelTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("return_cancel_time");
		    }
            set 
		    {
			    SetColumnValue("return_cancel_time", value);
            }
        }
	      
        [XmlAttribute("ReturnCancelUserName")]
        [Bindable(true)]
        public string ReturnCancelUserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("return_cancel_user_name");
		    }
            set 
		    {
			    SetColumnValue("return_cancel_user_name", value);
            }
        }
	      
        [XmlAttribute("ReturnCompletedTime")]
        [Bindable(true)]
        public DateTime? ReturnCompletedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("return_completed_time");
		    }
            set 
		    {
			    SetColumnValue("return_completed_time", value);
            }
        }
	      
        [XmlAttribute("ReviewUserName")]
        [Bindable(true)]
        public string ReviewUserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("review_user_name");
		    }
            set 
		    {
			    SetColumnValue("review_user_name", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int? ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ProductId");
		    }
            set 
		    {
			    SetColumnValue("ProductId", value);
            }
        }
	      
        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName 
	    {
		    get
		    {
			    return GetColumnValue<string>("ProductName");
		    }
            set 
		    {
			    SetColumnValue("ProductName", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("ItemQuantity")]
        [Bindable(true)]
        public int ItemQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_quantity");
		    }
            set 
		    {
			    SetColumnValue("item_quantity", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("MemberName");
		    }
            set 
		    {
			    SetColumnValue("MemberName", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderId = @"order_id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string DealName = @"deal_name";
            
            public static string ApplicationTime = @"application_time";
            
            public static string ReturnReason = @"return_reason";
            
            public static string CashBack = @"cash_back";
            
            public static string ReturnedStatus = @"returned_status";
            
            public static string UserName = @"user_name";
            
            public static string LastName = @"last_name";
            
            public static string FirstName = @"first_name";
            
            public static string ReturnCancelReason = @"return_cancel_reason";
            
            public static string ReturnCancelTime = @"return_cancel_time";
            
            public static string ReturnCancelUserName = @"return_cancel_user_name";
            
            public static string ReturnCompletedTime = @"return_completed_time";
            
            public static string ReviewUserName = @"review_user_name";
            
            public static string UniqueId = @"unique_id";
            
            public static string Id = @"id";
            
            public static string ProductId = @"ProductId";
            
            public static string ProductName = @"ProductName";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string ItemQuantity = @"item_quantity";
            
            public static string MemberName = @"MemberName";
            
            public static string Mobile = @"mobile";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

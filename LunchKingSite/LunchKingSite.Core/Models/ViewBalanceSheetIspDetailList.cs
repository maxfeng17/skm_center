using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBalanceSheetIspDetailList class.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetIspDetailListCollection : ReadOnlyList<ViewBalanceSheetIspDetailList, ViewBalanceSheetIspDetailListCollection>
    {        
        public ViewBalanceSheetIspDetailListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_balance_sheet_isp_detail_list view.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetIspDetailList : ReadOnlyRecord<ViewBalanceSheetIspDetailList>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_balance_sheet_isp_detail_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBalanceSheetId = new TableSchema.TableColumn(schema);
                colvarBalanceSheetId.ColumnName = "balance_sheet_id";
                colvarBalanceSheetId.DataType = DbType.Int32;
                colvarBalanceSheetId.MaxLength = 0;
                colvarBalanceSheetId.AutoIncrement = false;
                colvarBalanceSheetId.IsNullable = false;
                colvarBalanceSheetId.IsPrimaryKey = false;
                colvarBalanceSheetId.IsForeignKey = false;
                colvarBalanceSheetId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBalanceSheetId);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarProductDeliveryType = new TableSchema.TableColumn(schema);
                colvarProductDeliveryType.ColumnName = "product_delivery_type";
                colvarProductDeliveryType.DataType = DbType.Int32;
                colvarProductDeliveryType.MaxLength = 0;
                colvarProductDeliveryType.AutoIncrement = false;
                colvarProductDeliveryType.IsNullable = false;
                colvarProductDeliveryType.IsPrimaryKey = false;
                colvarProductDeliveryType.IsForeignKey = false;
                colvarProductDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductDeliveryType);
                
                TableSchema.TableColumn colvarShipToStockTime = new TableSchema.TableColumn(schema);
                colvarShipToStockTime.ColumnName = "ship_to_stock_time";
                colvarShipToStockTime.DataType = DbType.DateTime;
                colvarShipToStockTime.MaxLength = 0;
                colvarShipToStockTime.AutoIncrement = false;
                colvarShipToStockTime.IsNullable = true;
                colvarShipToStockTime.IsPrimaryKey = false;
                colvarShipToStockTime.IsForeignKey = false;
                colvarShipToStockTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipToStockTime);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_balance_sheet_isp_detail_list",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBalanceSheetIspDetailList()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBalanceSheetIspDetailList(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBalanceSheetIspDetailList(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBalanceSheetIspDetailList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BalanceSheetId")]
        [Bindable(true)]
        public int BalanceSheetId 
	    {
		    get
		    {
			    return GetColumnValue<int>("balance_sheet_id");
		    }
            set 
		    {
			    SetColumnValue("balance_sheet_id", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("ProductDeliveryType")]
        [Bindable(true)]
        public int ProductDeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_delivery_type");
		    }
            set 
		    {
			    SetColumnValue("product_delivery_type", value);
            }
        }
	      
        [XmlAttribute("ShipToStockTime")]
        [Bindable(true)]
        public DateTime? ShipToStockTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ship_to_stock_time");
		    }
            set 
		    {
			    SetColumnValue("ship_to_stock_time", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BalanceSheetId = @"balance_sheet_id";
            
            public static string OrderId = @"order_id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string ProductDeliveryType = @"product_delivery_type";
            
            public static string ShipToStockTime = @"ship_to_stock_time";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

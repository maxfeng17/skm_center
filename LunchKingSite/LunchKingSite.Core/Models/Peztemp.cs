using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Peztemp class.
	/// </summary>
    [Serializable]
	public partial class PeztempCollection : RepositoryList<Peztemp, PeztempCollection>
	{	   
		public PeztempCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PeztempCollection</returns>
		public PeztempCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Peztemp o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the peztemp table.
	/// </summary>
	[Serializable]
	public partial class Peztemp : RepositoryRecord<Peztemp>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Peztemp()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Peztemp(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("peztemp", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
				colvarOrderDetailGuid.ColumnName = "OrderDetailGuid";
				colvarOrderDetailGuid.DataType = DbType.Guid;
				colvarOrderDetailGuid.MaxLength = 0;
				colvarOrderDetailGuid.AutoIncrement = false;
				colvarOrderDetailGuid.IsNullable = true;
				colvarOrderDetailGuid.IsPrimaryKey = false;
				colvarOrderDetailGuid.IsForeignKey = false;
				colvarOrderDetailGuid.IsReadOnly = false;
				colvarOrderDetailGuid.DefaultSetting = @"";
				colvarOrderDetailGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderDetailGuid);
				
				TableSchema.TableColumn colvarPezCode = new TableSchema.TableColumn(schema);
				colvarPezCode.ColumnName = "PezCode";
				colvarPezCode.DataType = DbType.String;
				colvarPezCode.MaxLength = 50;
				colvarPezCode.AutoIncrement = false;
				colvarPezCode.IsNullable = true;
				colvarPezCode.IsPrimaryKey = false;
				colvarPezCode.IsForeignKey = false;
				colvarPezCode.IsReadOnly = false;
				colvarPezCode.DefaultSetting = @"";
				colvarPezCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPezCode);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "Bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = true;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarOption1 = new TableSchema.TableColumn(schema);
				colvarOption1.ColumnName = "Option_1";
				colvarOption1.DataType = DbType.String;
				colvarOption1.MaxLength = 200;
				colvarOption1.AutoIncrement = false;
				colvarOption1.IsNullable = true;
				colvarOption1.IsPrimaryKey = false;
				colvarOption1.IsForeignKey = false;
				colvarOption1.IsReadOnly = false;
				colvarOption1.DefaultSetting = @"";
				colvarOption1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOption1);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.String;
				colvarUserId.MaxLength = 150;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = true;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarIsUsed = new TableSchema.TableColumn(schema);
				colvarIsUsed.ColumnName = "is_used";
				colvarIsUsed.DataType = DbType.Boolean;
				colvarIsUsed.MaxLength = 0;
				colvarIsUsed.AutoIncrement = false;
				colvarIsUsed.IsNullable = false;
				colvarIsUsed.IsPrimaryKey = false;
				colvarIsUsed.IsForeignKey = false;
				colvarIsUsed.IsReadOnly = false;
				
						colvarIsUsed.DefaultSetting = @"((0))";
				colvarIsUsed.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsUsed);
				
				TableSchema.TableColumn colvarUsedTime = new TableSchema.TableColumn(schema);
				colvarUsedTime.ColumnName = "used_time";
				colvarUsedTime.DataType = DbType.DateTime;
				colvarUsedTime.MaxLength = 0;
				colvarUsedTime.AutoIncrement = false;
				colvarUsedTime.IsNullable = true;
				colvarUsedTime.IsPrimaryKey = false;
				colvarUsedTime.IsForeignKey = false;
				colvarUsedTime.IsReadOnly = false;
				colvarUsedTime.DefaultSetting = @"";
				colvarUsedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUsedTime);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarCvsId = new TableSchema.TableColumn(schema);
				colvarCvsId.ColumnName = "cvs_id";
				colvarCvsId.DataType = DbType.Int32;
				colvarCvsId.MaxLength = 0;
				colvarCvsId.AutoIncrement = false;
				colvarCvsId.IsNullable = true;
				colvarCvsId.IsPrimaryKey = false;
				colvarCvsId.IsForeignKey = false;
				colvarCvsId.IsReadOnly = false;
				colvarCvsId.DefaultSetting = @"";
				colvarCvsId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCvsId);
				
				TableSchema.TableColumn colvarBatchId = new TableSchema.TableColumn(schema);
				colvarBatchId.ColumnName = "batch_id";
				colvarBatchId.DataType = DbType.Int32;
				colvarBatchId.MaxLength = 0;
				colvarBatchId.AutoIncrement = false;
				colvarBatchId.IsNullable = false;
				colvarBatchId.IsPrimaryKey = false;
				colvarBatchId.IsForeignKey = false;
				colvarBatchId.IsReadOnly = false;
				
						colvarBatchId.DefaultSetting = @"((0))";
				colvarBatchId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBatchId);
				
				TableSchema.TableColumn colvarServiceCode = new TableSchema.TableColumn(schema);
				colvarServiceCode.ColumnName = "service_code";
				colvarServiceCode.DataType = DbType.Int32;
				colvarServiceCode.MaxLength = 0;
				colvarServiceCode.AutoIncrement = false;
				colvarServiceCode.IsNullable = false;
				colvarServiceCode.IsPrimaryKey = false;
				colvarServiceCode.IsForeignKey = false;
				colvarServiceCode.IsReadOnly = false;
				
						colvarServiceCode.DefaultSetting = @"((0))";
				colvarServiceCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceCode);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarIsTest = new TableSchema.TableColumn(schema);
				colvarIsTest.ColumnName = "is_test";
				colvarIsTest.DataType = DbType.Boolean;
				colvarIsTest.MaxLength = 0;
				colvarIsTest.AutoIncrement = false;
				colvarIsTest.IsNullable = false;
				colvarIsTest.IsPrimaryKey = false;
				colvarIsTest.IsForeignKey = false;
				colvarIsTest.IsReadOnly = false;
				
						colvarIsTest.DefaultSetting = @"((0))";
				colvarIsTest.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTest);
				
				TableSchema.TableColumn colvarIsBackupCode = new TableSchema.TableColumn(schema);
				colvarIsBackupCode.ColumnName = "is_backup_code";
				colvarIsBackupCode.DataType = DbType.Boolean;
				colvarIsBackupCode.MaxLength = 0;
				colvarIsBackupCode.AutoIncrement = false;
				colvarIsBackupCode.IsNullable = false;
				colvarIsBackupCode.IsPrimaryKey = false;
				colvarIsBackupCode.IsForeignKey = false;
				colvarIsBackupCode.IsReadOnly = false;
				
						colvarIsBackupCode.DefaultSetting = @"((0))";
				colvarIsBackupCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsBackupCode);
				
				TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
				colvarRemark.ColumnName = "remark";
				colvarRemark.DataType = DbType.AnsiString;
				colvarRemark.MaxLength = 20;
				colvarRemark.AutoIncrement = false;
				colvarRemark.IsNullable = true;
				colvarRemark.IsPrimaryKey = false;
				colvarRemark.IsForeignKey = false;
				colvarRemark.IsReadOnly = false;
				colvarRemark.DefaultSetting = @"";
				colvarRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemark);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("peztemp",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderDetailGuid")]
		[Bindable(true)]
		public Guid? OrderDetailGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.OrderDetailGuid); }
			set { SetColumnValue(Columns.OrderDetailGuid, value); }
		}
		  
		[XmlAttribute("PezCode")]
		[Bindable(true)]
		public string PezCode 
		{
			get { return GetColumnValue<string>(Columns.PezCode); }
			set { SetColumnValue(Columns.PezCode, value); }
		}
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid? Bid 
		{
			get { return GetColumnValue<Guid?>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("Option1")]
		[Bindable(true)]
		public string Option1 
		{
			get { return GetColumnValue<string>(Columns.Option1); }
			set { SetColumnValue(Columns.Option1, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public string UserId 
		{
			get { return GetColumnValue<string>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("IsUsed")]
		[Bindable(true)]
		public bool IsUsed 
		{
			get { return GetColumnValue<bool>(Columns.IsUsed); }
			set { SetColumnValue(Columns.IsUsed, value); }
		}
		  
		[XmlAttribute("UsedTime")]
		[Bindable(true)]
		public DateTime? UsedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UsedTime); }
			set { SetColumnValue(Columns.UsedTime, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("CvsId")]
		[Bindable(true)]
		public int? CvsId 
		{
			get { return GetColumnValue<int?>(Columns.CvsId); }
			set { SetColumnValue(Columns.CvsId, value); }
		}
		  
		[XmlAttribute("BatchId")]
		[Bindable(true)]
		public int BatchId 
		{
			get { return GetColumnValue<int>(Columns.BatchId); }
			set { SetColumnValue(Columns.BatchId, value); }
		}
		  
		[XmlAttribute("ServiceCode")]
		[Bindable(true)]
		public int ServiceCode 
		{
			get { return GetColumnValue<int>(Columns.ServiceCode); }
			set { SetColumnValue(Columns.ServiceCode, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("IsTest")]
		[Bindable(true)]
		public bool IsTest 
		{
			get { return GetColumnValue<bool>(Columns.IsTest); }
			set { SetColumnValue(Columns.IsTest, value); }
		}
		  
		[XmlAttribute("IsBackupCode")]
		[Bindable(true)]
		public bool IsBackupCode 
		{
			get { return GetColumnValue<bool>(Columns.IsBackupCode); }
			set { SetColumnValue(Columns.IsBackupCode, value); }
		}
		  
		[XmlAttribute("Remark")]
		[Bindable(true)]
		public string Remark 
		{
			get { return GetColumnValue<string>(Columns.Remark); }
			set { SetColumnValue(Columns.Remark, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderDetailGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PezCodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn Option1Column
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IsUsedColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn UsedTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CvsIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn BatchIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ServiceCodeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn IsTestColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn IsBackupCodeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string OrderDetailGuid = @"OrderDetailGuid";
			 public static string PezCode = @"PezCode";
			 public static string Bid = @"Bid";
			 public static string Option1 = @"Option_1";
			 public static string UserId = @"user_id";
			 public static string IsUsed = @"is_used";
			 public static string UsedTime = @"used_time";
			 public static string OrderGuid = @"order_guid";
			 public static string CvsId = @"cvs_id";
			 public static string BatchId = @"batch_id";
			 public static string ServiceCode = @"service_code";
			 public static string Status = @"status";
			 public static string IsTest = @"is_test";
			 public static string IsBackupCode = @"is_backup_code";
			 public static string Remark = @"remark";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewWeeklyPayReport class.
    /// </summary>
    [Serializable]
    public partial class ViewWeeklyPayReportCollection : ReadOnlyList<ViewWeeklyPayReport, ViewWeeklyPayReportCollection>
    {        
        public ViewWeeklyPayReportCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_weekly_pay_reports view.
    /// </summary>
    [Serializable]
    public partial class ViewWeeklyPayReport : ReadOnlyRecord<ViewWeeklyPayReport>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_weekly_pay_reports", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "Id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
                colvarEventName.ColumnName = "event_name";
                colvarEventName.DataType = DbType.String;
                colvarEventName.MaxLength = 500;
                colvarEventName.AutoIncrement = false;
                colvarEventName.IsNullable = true;
                colvarEventName.IsPrimaryKey = false;
                colvarEventName.IsForeignKey = false;
                colvarEventName.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventName);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "company_name";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 200;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyName);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
                colvarAccountName.ColumnName = "account_name";
                colvarAccountName.DataType = DbType.String;
                colvarAccountName.MaxLength = 100;
                colvarAccountName.AutoIncrement = false;
                colvarAccountName.IsNullable = true;
                colvarAccountName.IsPrimaryKey = false;
                colvarAccountName.IsForeignKey = false;
                colvarAccountName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountName);
                
                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.AnsiString;
                colvarAccountId.MaxLength = 10;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = true;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountId);
                
                TableSchema.TableColumn colvarBankNo = new TableSchema.TableColumn(schema);
                colvarBankNo.ColumnName = "bank_no";
                colvarBankNo.DataType = DbType.AnsiString;
                colvarBankNo.MaxLength = 20;
                colvarBankNo.AutoIncrement = false;
                colvarBankNo.IsNullable = true;
                colvarBankNo.IsPrimaryKey = false;
                colvarBankNo.IsForeignKey = false;
                colvarBankNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarBankNo);
                
                TableSchema.TableColumn colvarBranchNo = new TableSchema.TableColumn(schema);
                colvarBranchNo.ColumnName = "branch_no";
                colvarBranchNo.DataType = DbType.AnsiString;
                colvarBranchNo.MaxLength = 20;
                colvarBranchNo.AutoIncrement = false;
                colvarBranchNo.IsNullable = true;
                colvarBranchNo.IsPrimaryKey = false;
                colvarBranchNo.IsForeignKey = false;
                colvarBranchNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarBranchNo);
                
                TableSchema.TableColumn colvarAccountNo = new TableSchema.TableColumn(schema);
                colvarAccountNo.ColumnName = "account_no";
                colvarAccountNo.DataType = DbType.AnsiString;
                colvarAccountNo.MaxLength = 50;
                colvarAccountNo.AutoIncrement = false;
                colvarAccountNo.IsNullable = true;
                colvarAccountNo.IsPrimaryKey = false;
                colvarAccountNo.IsForeignKey = false;
                colvarAccountNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountNo);
                
                TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
                colvarEmail.ColumnName = "email";
                colvarEmail.DataType = DbType.String;
                colvarEmail.MaxLength = 200;
                colvarEmail.AutoIncrement = false;
                colvarEmail.IsNullable = true;
                colvarEmail.IsPrimaryKey = false;
                colvarEmail.IsForeignKey = false;
                colvarEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarEmail);
                
                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 1000;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage);
                
                TableSchema.TableColumn colvarSignCompanyID = new TableSchema.TableColumn(schema);
                colvarSignCompanyID.ColumnName = "SignCompanyID";
                colvarSignCompanyID.DataType = DbType.AnsiString;
                colvarSignCompanyID.MaxLength = 20;
                colvarSignCompanyID.AutoIncrement = false;
                colvarSignCompanyID.IsNullable = true;
                colvarSignCompanyID.IsPrimaryKey = false;
                colvarSignCompanyID.IsForeignKey = false;
                colvarSignCompanyID.IsReadOnly = false;
                
                schema.Columns.Add(colvarSignCompanyID);
                
                TableSchema.TableColumn colvarIntervalStart = new TableSchema.TableColumn(schema);
                colvarIntervalStart.ColumnName = "interval_start";
                colvarIntervalStart.DataType = DbType.DateTime;
                colvarIntervalStart.MaxLength = 0;
                colvarIntervalStart.AutoIncrement = false;
                colvarIntervalStart.IsNullable = false;
                colvarIntervalStart.IsPrimaryKey = false;
                colvarIntervalStart.IsForeignKey = false;
                colvarIntervalStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarIntervalStart);
                
                TableSchema.TableColumn colvarIntervalEnd = new TableSchema.TableColumn(schema);
                colvarIntervalEnd.ColumnName = "interval_end";
                colvarIntervalEnd.DataType = DbType.DateTime;
                colvarIntervalEnd.MaxLength = 0;
                colvarIntervalEnd.AutoIncrement = false;
                colvarIntervalEnd.IsNullable = false;
                colvarIntervalEnd.IsPrimaryKey = false;
                colvarIntervalEnd.IsForeignKey = false;
                colvarIntervalEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarIntervalEnd);
                
                TableSchema.TableColumn colvarCreditCard = new TableSchema.TableColumn(schema);
                colvarCreditCard.ColumnName = "credit_card";
                colvarCreditCard.DataType = DbType.Int32;
                colvarCreditCard.MaxLength = 0;
                colvarCreditCard.AutoIncrement = false;
                colvarCreditCard.IsNullable = false;
                colvarCreditCard.IsPrimaryKey = false;
                colvarCreditCard.IsForeignKey = false;
                colvarCreditCard.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreditCard);
                
                TableSchema.TableColumn colvarAtm = new TableSchema.TableColumn(schema);
                colvarAtm.ColumnName = "atm";
                colvarAtm.DataType = DbType.Int32;
                colvarAtm.MaxLength = 0;
                colvarAtm.AutoIncrement = false;
                colvarAtm.IsNullable = false;
                colvarAtm.IsPrimaryKey = false;
                colvarAtm.IsForeignKey = false;
                colvarAtm.IsReadOnly = false;
                
                schema.Columns.Add(colvarAtm);
                
                TableSchema.TableColumn colvarReportGuid = new TableSchema.TableColumn(schema);
                colvarReportGuid.ColumnName = "report_guid";
                colvarReportGuid.DataType = DbType.Guid;
                colvarReportGuid.MaxLength = 0;
                colvarReportGuid.AutoIncrement = false;
                colvarReportGuid.IsNullable = false;
                colvarReportGuid.IsPrimaryKey = false;
                colvarReportGuid.IsForeignKey = false;
                colvarReportGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarReportGuid);
                
                TableSchema.TableColumn colvarIsLastWeek = new TableSchema.TableColumn(schema);
                colvarIsLastWeek.ColumnName = "is_last_week";
                colvarIsLastWeek.DataType = DbType.Boolean;
                colvarIsLastWeek.MaxLength = 0;
                colvarIsLastWeek.AutoIncrement = false;
                colvarIsLastWeek.IsNullable = false;
                colvarIsLastWeek.IsPrimaryKey = false;
                colvarIsLastWeek.IsForeignKey = false;
                colvarIsLastWeek.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsLastWeek);
                
                TableSchema.TableColumn colvarErrorIn = new TableSchema.TableColumn(schema);
                colvarErrorIn.ColumnName = "error_in";
                colvarErrorIn.DataType = DbType.Decimal;
                colvarErrorIn.MaxLength = 0;
                colvarErrorIn.AutoIncrement = false;
                colvarErrorIn.IsNullable = false;
                colvarErrorIn.IsPrimaryKey = false;
                colvarErrorIn.IsForeignKey = false;
                colvarErrorIn.IsReadOnly = false;
                
                schema.Columns.Add(colvarErrorIn);
                
                TableSchema.TableColumn colvarErrorOut = new TableSchema.TableColumn(schema);
                colvarErrorOut.ColumnName = "error_out";
                colvarErrorOut.DataType = DbType.Decimal;
                colvarErrorOut.MaxLength = 0;
                colvarErrorOut.AutoIncrement = false;
                colvarErrorOut.IsNullable = false;
                colvarErrorOut.IsPrimaryKey = false;
                colvarErrorOut.IsForeignKey = false;
                colvarErrorOut.IsReadOnly = false;
                
                schema.Columns.Add(colvarErrorOut);
                
                TableSchema.TableColumn colvarTotalCount = new TableSchema.TableColumn(schema);
                colvarTotalCount.ColumnName = "total_count";
                colvarTotalCount.DataType = DbType.Int32;
                colvarTotalCount.MaxLength = 0;
                colvarTotalCount.AutoIncrement = false;
                colvarTotalCount.IsNullable = false;
                colvarTotalCount.IsPrimaryKey = false;
                colvarTotalCount.IsForeignKey = false;
                colvarTotalCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalCount);
                
                TableSchema.TableColumn colvarTotalSum = new TableSchema.TableColumn(schema);
                colvarTotalSum.ColumnName = "total_sum";
                colvarTotalSum.DataType = DbType.Decimal;
                colvarTotalSum.MaxLength = 0;
                colvarTotalSum.AutoIncrement = false;
                colvarTotalSum.IsNullable = false;
                colvarTotalSum.IsPrimaryKey = false;
                colvarTotalSum.IsForeignKey = false;
                colvarTotalSum.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalSum);
                
                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Decimal;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = false;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarCost);
                
                TableSchema.TableColumn colvarStoreCompanyName = new TableSchema.TableColumn(schema);
                colvarStoreCompanyName.ColumnName = "StoreCompanyName";
                colvarStoreCompanyName.DataType = DbType.String;
                colvarStoreCompanyName.MaxLength = 200;
                colvarStoreCompanyName.AutoIncrement = false;
                colvarStoreCompanyName.IsNullable = true;
                colvarStoreCompanyName.IsPrimaryKey = false;
                colvarStoreCompanyName.IsForeignKey = false;
                colvarStoreCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreCompanyName);
                
                TableSchema.TableColumn colvarCompanyBossName = new TableSchema.TableColumn(schema);
                colvarCompanyBossName.ColumnName = "CompanyBossName";
                colvarCompanyBossName.DataType = DbType.String;
                colvarCompanyBossName.MaxLength = 100;
                colvarCompanyBossName.AutoIncrement = false;
                colvarCompanyBossName.IsNullable = true;
                colvarCompanyBossName.IsPrimaryKey = false;
                colvarCompanyBossName.IsForeignKey = false;
                colvarCompanyBossName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBossName);
                
                TableSchema.TableColumn colvarCompanyAccountName = new TableSchema.TableColumn(schema);
                colvarCompanyAccountName.ColumnName = "CompanyAccountName";
                colvarCompanyAccountName.DataType = DbType.String;
                colvarCompanyAccountName.MaxLength = 100;
                colvarCompanyAccountName.AutoIncrement = false;
                colvarCompanyAccountName.IsNullable = true;
                colvarCompanyAccountName.IsPrimaryKey = false;
                colvarCompanyAccountName.IsForeignKey = false;
                colvarCompanyAccountName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAccountName);
                
                TableSchema.TableColumn colvarCompanyID = new TableSchema.TableColumn(schema);
                colvarCompanyID.ColumnName = "CompanyID";
                colvarCompanyID.DataType = DbType.AnsiString;
                colvarCompanyID.MaxLength = 20;
                colvarCompanyID.AutoIncrement = false;
                colvarCompanyID.IsNullable = true;
                colvarCompanyID.IsPrimaryKey = false;
                colvarCompanyID.IsForeignKey = false;
                colvarCompanyID.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyID);
                
                TableSchema.TableColumn colvarCompanyBankCode = new TableSchema.TableColumn(schema);
                colvarCompanyBankCode.ColumnName = "CompanyBankCode";
                colvarCompanyBankCode.DataType = DbType.AnsiString;
                colvarCompanyBankCode.MaxLength = 20;
                colvarCompanyBankCode.AutoIncrement = false;
                colvarCompanyBankCode.IsNullable = true;
                colvarCompanyBankCode.IsPrimaryKey = false;
                colvarCompanyBankCode.IsForeignKey = false;
                colvarCompanyBankCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBankCode);
                
                TableSchema.TableColumn colvarCompanyBranchCode = new TableSchema.TableColumn(schema);
                colvarCompanyBranchCode.ColumnName = "CompanyBranchCode";
                colvarCompanyBranchCode.DataType = DbType.AnsiString;
                colvarCompanyBranchCode.MaxLength = 20;
                colvarCompanyBranchCode.AutoIncrement = false;
                colvarCompanyBranchCode.IsNullable = true;
                colvarCompanyBranchCode.IsPrimaryKey = false;
                colvarCompanyBranchCode.IsForeignKey = false;
                colvarCompanyBranchCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBranchCode);
                
                TableSchema.TableColumn colvarCompanyAccount = new TableSchema.TableColumn(schema);
                colvarCompanyAccount.ColumnName = "CompanyAccount";
                colvarCompanyAccount.DataType = DbType.AnsiString;
                colvarCompanyAccount.MaxLength = 50;
                colvarCompanyAccount.AutoIncrement = false;
                colvarCompanyAccount.IsNullable = true;
                colvarCompanyAccount.IsPrimaryKey = false;
                colvarCompanyAccount.IsForeignKey = false;
                colvarCompanyAccount.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAccount);
                
                TableSchema.TableColumn colvarStoreSignCompanyID = new TableSchema.TableColumn(schema);
                colvarStoreSignCompanyID.ColumnName = "StoreSignCompanyID";
                colvarStoreSignCompanyID.DataType = DbType.AnsiString;
                colvarStoreSignCompanyID.MaxLength = 20;
                colvarStoreSignCompanyID.AutoIncrement = false;
                colvarStoreSignCompanyID.IsNullable = true;
                colvarStoreSignCompanyID.IsPrimaryKey = false;
                colvarStoreSignCompanyID.IsForeignKey = false;
                colvarStoreSignCompanyID.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreSignCompanyID);
                
                TableSchema.TableColumn colvarErrorInSum = new TableSchema.TableColumn(schema);
                colvarErrorInSum.ColumnName = "error_in_sum";
                colvarErrorInSum.DataType = DbType.Int32;
                colvarErrorInSum.MaxLength = 0;
                colvarErrorInSum.AutoIncrement = false;
                colvarErrorInSum.IsNullable = false;
                colvarErrorInSum.IsPrimaryKey = false;
                colvarErrorInSum.IsForeignKey = false;
                colvarErrorInSum.IsReadOnly = false;
                
                schema.Columns.Add(colvarErrorInSum);
                
                TableSchema.TableColumn colvarErrorOutSum = new TableSchema.TableColumn(schema);
                colvarErrorOutSum.ColumnName = "error_out_sum";
                colvarErrorOutSum.DataType = DbType.Int32;
                colvarErrorOutSum.MaxLength = 0;
                colvarErrorOutSum.AutoIncrement = false;
                colvarErrorOutSum.IsNullable = false;
                colvarErrorOutSum.IsPrimaryKey = false;
                colvarErrorOutSum.IsForeignKey = false;
                colvarErrorOutSum.IsReadOnly = false;
                
                schema.Columns.Add(colvarErrorOutSum);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarPaytocompany = new TableSchema.TableColumn(schema);
                colvarPaytocompany.ColumnName = "paytocompany";
                colvarPaytocompany.DataType = DbType.Int32;
                colvarPaytocompany.MaxLength = 0;
                colvarPaytocompany.AutoIncrement = false;
                colvarPaytocompany.IsNullable = true;
                colvarPaytocompany.IsPrimaryKey = false;
                colvarPaytocompany.IsForeignKey = false;
                colvarPaytocompany.IsReadOnly = false;
                
                schema.Columns.Add(colvarPaytocompany);
                
                TableSchema.TableColumn colvarVendorBillingModel = new TableSchema.TableColumn(schema);
                colvarVendorBillingModel.ColumnName = "vendor_billing_model";
                colvarVendorBillingModel.DataType = DbType.Int32;
                colvarVendorBillingModel.MaxLength = 0;
                colvarVendorBillingModel.AutoIncrement = false;
                colvarVendorBillingModel.IsNullable = true;
                colvarVendorBillingModel.IsPrimaryKey = false;
                colvarVendorBillingModel.IsForeignKey = false;
                colvarVendorBillingModel.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorBillingModel);
                
                TableSchema.TableColumn colvarVendorReceiptType = new TableSchema.TableColumn(schema);
                colvarVendorReceiptType.ColumnName = "vendor_receipt_type";
                colvarVendorReceiptType.DataType = DbType.Int32;
                colvarVendorReceiptType.MaxLength = 0;
                colvarVendorReceiptType.AutoIncrement = false;
                colvarVendorReceiptType.IsNullable = true;
                colvarVendorReceiptType.IsPrimaryKey = false;
                colvarVendorReceiptType.IsForeignKey = false;
                colvarVendorReceiptType.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorReceiptType);
                
                TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
                colvarRemittanceType.ColumnName = "remittance_type";
                colvarRemittanceType.DataType = DbType.Int32;
                colvarRemittanceType.MaxLength = 0;
                colvarRemittanceType.AutoIncrement = false;
                colvarRemittanceType.IsNullable = true;
                colvarRemittanceType.IsPrimaryKey = false;
                colvarRemittanceType.IsForeignKey = false;
                colvarRemittanceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemittanceType);
                
                TableSchema.TableColumn colvarCompanyEmail = new TableSchema.TableColumn(schema);
                colvarCompanyEmail.ColumnName = "CompanyEmail";
                colvarCompanyEmail.DataType = DbType.String;
                colvarCompanyEmail.MaxLength = 250;
                colvarCompanyEmail.AutoIncrement = false;
                colvarCompanyEmail.IsNullable = true;
                colvarCompanyEmail.IsPrimaryKey = false;
                colvarCompanyEmail.IsForeignKey = false;
                colvarCompanyEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyEmail);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = true;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.String;
                colvarUserId.MaxLength = 100;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarResponseTime = new TableSchema.TableColumn(schema);
                colvarResponseTime.ColumnName = "response_time";
                colvarResponseTime.DataType = DbType.DateTime;
                colvarResponseTime.MaxLength = 0;
                colvarResponseTime.AutoIncrement = false;
                colvarResponseTime.IsNullable = true;
                colvarResponseTime.IsPrimaryKey = false;
                colvarResponseTime.IsForeignKey = false;
                colvarResponseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarResponseTime);
                
                TableSchema.TableColumn colvarResult = new TableSchema.TableColumn(schema);
                colvarResult.ColumnName = "result";
                colvarResult.DataType = DbType.String;
                colvarResult.MaxLength = 10;
                colvarResult.AutoIncrement = false;
                colvarResult.IsNullable = true;
                colvarResult.IsPrimaryKey = false;
                colvarResult.IsForeignKey = false;
                colvarResult.IsReadOnly = false;
                
                schema.Columns.Add(colvarResult);
                
                TableSchema.TableColumn colvarTransferAmount = new TableSchema.TableColumn(schema);
                colvarTransferAmount.ColumnName = "transfer_amount";
                colvarTransferAmount.DataType = DbType.Decimal;
                colvarTransferAmount.MaxLength = 0;
                colvarTransferAmount.AutoIncrement = false;
                colvarTransferAmount.IsNullable = true;
                colvarTransferAmount.IsPrimaryKey = false;
                colvarTransferAmount.IsForeignKey = false;
                colvarTransferAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarTransferAmount);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_weekly_pay_reports",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewWeeklyPayReport()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewWeeklyPayReport(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewWeeklyPayReport(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewWeeklyPayReport(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("Id");
		    }
            set 
		    {
			    SetColumnValue("Id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("EventName")]
        [Bindable(true)]
        public string EventName 
	    {
		    get
		    {
			    return GetColumnValue<string>("event_name");
		    }
            set 
		    {
			    SetColumnValue("event_name", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_name");
		    }
            set 
		    {
			    SetColumnValue("company_name", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("AccountName")]
        [Bindable(true)]
        public string AccountName 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_name");
		    }
            set 
		    {
			    SetColumnValue("account_name", value);
            }
        }
	      
        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_id");
		    }
            set 
		    {
			    SetColumnValue("account_id", value);
            }
        }
	      
        [XmlAttribute("BankNo")]
        [Bindable(true)]
        public string BankNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("bank_no");
		    }
            set 
		    {
			    SetColumnValue("bank_no", value);
            }
        }
	      
        [XmlAttribute("BranchNo")]
        [Bindable(true)]
        public string BranchNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("branch_no");
		    }
            set 
		    {
			    SetColumnValue("branch_no", value);
            }
        }
	      
        [XmlAttribute("AccountNo")]
        [Bindable(true)]
        public string AccountNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_no");
		    }
            set 
		    {
			    SetColumnValue("account_no", value);
            }
        }
	      
        [XmlAttribute("Email")]
        [Bindable(true)]
        public string Email 
	    {
		    get
		    {
			    return GetColumnValue<string>("email");
		    }
            set 
		    {
			    SetColumnValue("email", value);
            }
        }
	      
        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message 
	    {
		    get
		    {
			    return GetColumnValue<string>("message");
		    }
            set 
		    {
			    SetColumnValue("message", value);
            }
        }
	      
        [XmlAttribute("SignCompanyID")]
        [Bindable(true)]
        public string SignCompanyID 
	    {
		    get
		    {
			    return GetColumnValue<string>("SignCompanyID");
		    }
            set 
		    {
			    SetColumnValue("SignCompanyID", value);
            }
        }
	      
        [XmlAttribute("IntervalStart")]
        [Bindable(true)]
        public DateTime IntervalStart 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("interval_start");
		    }
            set 
		    {
			    SetColumnValue("interval_start", value);
            }
        }
	      
        [XmlAttribute("IntervalEnd")]
        [Bindable(true)]
        public DateTime IntervalEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("interval_end");
		    }
            set 
		    {
			    SetColumnValue("interval_end", value);
            }
        }
	      
        [XmlAttribute("CreditCard")]
        [Bindable(true)]
        public int CreditCard 
	    {
		    get
		    {
			    return GetColumnValue<int>("credit_card");
		    }
            set 
		    {
			    SetColumnValue("credit_card", value);
            }
        }
	      
        [XmlAttribute("Atm")]
        [Bindable(true)]
        public int Atm 
	    {
		    get
		    {
			    return GetColumnValue<int>("atm");
		    }
            set 
		    {
			    SetColumnValue("atm", value);
            }
        }
	      
        [XmlAttribute("ReportGuid")]
        [Bindable(true)]
        public Guid ReportGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("report_guid");
		    }
            set 
		    {
			    SetColumnValue("report_guid", value);
            }
        }
	      
        [XmlAttribute("IsLastWeek")]
        [Bindable(true)]
        public bool IsLastWeek 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_last_week");
		    }
            set 
		    {
			    SetColumnValue("is_last_week", value);
            }
        }
	      
        [XmlAttribute("ErrorIn")]
        [Bindable(true)]
        public decimal ErrorIn 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("error_in");
		    }
            set 
		    {
			    SetColumnValue("error_in", value);
            }
        }
	      
        [XmlAttribute("ErrorOut")]
        [Bindable(true)]
        public decimal ErrorOut 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("error_out");
		    }
            set 
		    {
			    SetColumnValue("error_out", value);
            }
        }
	      
        [XmlAttribute("TotalCount")]
        [Bindable(true)]
        public int TotalCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("total_count");
		    }
            set 
		    {
			    SetColumnValue("total_count", value);
            }
        }
	      
        [XmlAttribute("TotalSum")]
        [Bindable(true)]
        public decimal TotalSum 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total_sum");
		    }
            set 
		    {
			    SetColumnValue("total_sum", value);
            }
        }
	      
        [XmlAttribute("Cost")]
        [Bindable(true)]
        public decimal Cost 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("cost");
		    }
            set 
		    {
			    SetColumnValue("cost", value);
            }
        }
	      
        [XmlAttribute("StoreCompanyName")]
        [Bindable(true)]
        public string StoreCompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("StoreCompanyName");
		    }
            set 
		    {
			    SetColumnValue("StoreCompanyName", value);
            }
        }
	      
        [XmlAttribute("CompanyBossName")]
        [Bindable(true)]
        public string CompanyBossName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBossName");
		    }
            set 
		    {
			    SetColumnValue("CompanyBossName", value);
            }
        }
	      
        [XmlAttribute("CompanyAccountName")]
        [Bindable(true)]
        public string CompanyAccountName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyAccountName");
		    }
            set 
		    {
			    SetColumnValue("CompanyAccountName", value);
            }
        }
	      
        [XmlAttribute("CompanyID")]
        [Bindable(true)]
        public string CompanyID 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyID");
		    }
            set 
		    {
			    SetColumnValue("CompanyID", value);
            }
        }
	      
        [XmlAttribute("CompanyBankCode")]
        [Bindable(true)]
        public string CompanyBankCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBankCode");
		    }
            set 
		    {
			    SetColumnValue("CompanyBankCode", value);
            }
        }
	      
        [XmlAttribute("CompanyBranchCode")]
        [Bindable(true)]
        public string CompanyBranchCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBranchCode");
		    }
            set 
		    {
			    SetColumnValue("CompanyBranchCode", value);
            }
        }
	      
        [XmlAttribute("CompanyAccount")]
        [Bindable(true)]
        public string CompanyAccount 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyAccount");
		    }
            set 
		    {
			    SetColumnValue("CompanyAccount", value);
            }
        }
	      
        [XmlAttribute("StoreSignCompanyID")]
        [Bindable(true)]
        public string StoreSignCompanyID 
	    {
		    get
		    {
			    return GetColumnValue<string>("StoreSignCompanyID");
		    }
            set 
		    {
			    SetColumnValue("StoreSignCompanyID", value);
            }
        }
	      
        [XmlAttribute("ErrorInSum")]
        [Bindable(true)]
        public int ErrorInSum 
	    {
		    get
		    {
			    return GetColumnValue<int>("error_in_sum");
		    }
            set 
		    {
			    SetColumnValue("error_in_sum", value);
            }
        }
	      
        [XmlAttribute("ErrorOutSum")]
        [Bindable(true)]
        public int ErrorOutSum 
	    {
		    get
		    {
			    return GetColumnValue<int>("error_out_sum");
		    }
            set 
		    {
			    SetColumnValue("error_out_sum", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("Paytocompany")]
        [Bindable(true)]
        public int? Paytocompany 
	    {
		    get
		    {
			    return GetColumnValue<int?>("paytocompany");
		    }
            set 
		    {
			    SetColumnValue("paytocompany", value);
            }
        }
	      
        [XmlAttribute("VendorBillingModel")]
        [Bindable(true)]
        public int? VendorBillingModel 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vendor_billing_model");
		    }
            set 
		    {
			    SetColumnValue("vendor_billing_model", value);
            }
        }
	      
        [XmlAttribute("VendorReceiptType")]
        [Bindable(true)]
        public int? VendorReceiptType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vendor_receipt_type");
		    }
            set 
		    {
			    SetColumnValue("vendor_receipt_type", value);
            }
        }
	      
        [XmlAttribute("RemittanceType")]
        [Bindable(true)]
        public int? RemittanceType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("remittance_type");
		    }
            set 
		    {
			    SetColumnValue("remittance_type", value);
            }
        }
	      
        [XmlAttribute("CompanyEmail")]
        [Bindable(true)]
        public string CompanyEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyEmail");
		    }
            set 
		    {
			    SetColumnValue("CompanyEmail", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int? UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public string UserId 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("ResponseTime")]
        [Bindable(true)]
        public DateTime? ResponseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("response_time");
		    }
            set 
		    {
			    SetColumnValue("response_time", value);
            }
        }
	      
        [XmlAttribute("Result")]
        [Bindable(true)]
        public string Result 
	    {
		    get
		    {
			    return GetColumnValue<string>("result");
		    }
            set 
		    {
			    SetColumnValue("result", value);
            }
        }
	      
        [XmlAttribute("TransferAmount")]
        [Bindable(true)]
        public decimal? TransferAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("transfer_amount");
		    }
            set 
		    {
			    SetColumnValue("transfer_amount", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"Id";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string EventName = @"event_name";
            
            public static string StoreName = @"store_name";
            
            public static string CompanyName = @"company_name";
            
            public static string SellerName = @"seller_name";
            
            public static string AccountName = @"account_name";
            
            public static string AccountId = @"account_id";
            
            public static string BankNo = @"bank_no";
            
            public static string BranchNo = @"branch_no";
            
            public static string AccountNo = @"account_no";
            
            public static string Email = @"email";
            
            public static string Message = @"message";
            
            public static string SignCompanyID = @"SignCompanyID";
            
            public static string IntervalStart = @"interval_start";
            
            public static string IntervalEnd = @"interval_end";
            
            public static string CreditCard = @"credit_card";
            
            public static string Atm = @"atm";
            
            public static string ReportGuid = @"report_guid";
            
            public static string IsLastWeek = @"is_last_week";
            
            public static string ErrorIn = @"error_in";
            
            public static string ErrorOut = @"error_out";
            
            public static string TotalCount = @"total_count";
            
            public static string TotalSum = @"total_sum";
            
            public static string Cost = @"cost";
            
            public static string StoreCompanyName = @"StoreCompanyName";
            
            public static string CompanyBossName = @"CompanyBossName";
            
            public static string CompanyAccountName = @"CompanyAccountName";
            
            public static string CompanyID = @"CompanyID";
            
            public static string CompanyBankCode = @"CompanyBankCode";
            
            public static string CompanyBranchCode = @"CompanyBranchCode";
            
            public static string CompanyAccount = @"CompanyAccount";
            
            public static string StoreSignCompanyID = @"StoreSignCompanyID";
            
            public static string ErrorInSum = @"error_in_sum";
            
            public static string ErrorOutSum = @"error_out_sum";
            
            public static string CreateTime = @"create_time";
            
            public static string StoreGuid = @"store_guid";
            
            public static string Paytocompany = @"paytocompany";
            
            public static string VendorBillingModel = @"vendor_billing_model";
            
            public static string VendorReceiptType = @"vendor_receipt_type";
            
            public static string RemittanceType = @"remittance_type";
            
            public static string CompanyEmail = @"CompanyEmail";
            
            public static string UniqueId = @"unique_id";
            
            public static string UserId = @"user_id";
            
            public static string ResponseTime = @"response_time";
            
            public static string Result = @"result";
            
            public static string TransferAmount = @"transfer_amount";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

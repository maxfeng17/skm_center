using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ShipCompany class.
    /// </summary>
    [Serializable]
    public partial class ShipCompanyCollection : RepositoryList<ShipCompany, ShipCompanyCollection>
    {
        public ShipCompanyCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ShipCompanyCollection</returns>
        public ShipCompanyCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ShipCompany o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }


    /// <summary>
    /// This is an ActiveRecord class which wraps the ship_company table.
    /// </summary>

    [Serializable]
    public partial class ShipCompany : RepositoryRecord<ShipCompany>, IRecordBase
    {
        #region .ctors and Default Settings

        public ShipCompany()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ShipCompany(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("ship_company", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Decimal;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = false;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                colvarSequence.DefaultSetting = @"";
                colvarSequence.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSequence);

                TableSchema.TableColumn colvarShipCompanyName = new TableSchema.TableColumn(schema);
                colvarShipCompanyName.ColumnName = "ship_company_name";
                colvarShipCompanyName.DataType = DbType.String;
                colvarShipCompanyName.MaxLength = 100;
                colvarShipCompanyName.AutoIncrement = false;
                colvarShipCompanyName.IsNullable = false;
                colvarShipCompanyName.IsPrimaryKey = false;
                colvarShipCompanyName.IsForeignKey = false;
                colvarShipCompanyName.IsReadOnly = false;
                colvarShipCompanyName.DefaultSetting = @"";
                colvarShipCompanyName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShipCompanyName);

                TableSchema.TableColumn colvarShipWebsite = new TableSchema.TableColumn(schema);
                colvarShipWebsite.ColumnName = "ship_website";
                colvarShipWebsite.DataType = DbType.String;
                colvarShipWebsite.MaxLength = 300;
                colvarShipWebsite.AutoIncrement = false;
                colvarShipWebsite.IsNullable = true;
                colvarShipWebsite.IsPrimaryKey = false;
                colvarShipWebsite.IsForeignKey = false;
                colvarShipWebsite.IsReadOnly = false;
                colvarShipWebsite.DefaultSetting = @"";
                colvarShipWebsite.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShipWebsite);

                TableSchema.TableColumn colvarServiceTel = new TableSchema.TableColumn(schema);
                colvarServiceTel.ColumnName = "service_tel";
                colvarServiceTel.DataType = DbType.String;
                colvarServiceTel.MaxLength = 100;
                colvarServiceTel.AutoIncrement = false;
                colvarServiceTel.IsNullable = true;
                colvarServiceTel.IsPrimaryKey = false;
                colvarServiceTel.IsForeignKey = false;
                colvarServiceTel.IsReadOnly = false;
                colvarServiceTel.DefaultSetting = @"";
                colvarServiceTel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarServiceTel);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Boolean;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
                colvarCreator.ColumnName = "creator";
                colvarCreator.DataType = DbType.String;
                colvarCreator.MaxLength = 50;
                colvarCreator.AutoIncrement = false;
                colvarCreator.IsNullable = true;
                colvarCreator.IsPrimaryKey = false;
                colvarCreator.IsForeignKey = false;
                colvarCreator.IsReadOnly = false;
                colvarCreator.DefaultSetting = @"";
                colvarCreator.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreator);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarMin = new TableSchema.TableColumn(schema);
                colvarMin.ColumnName = "min";
                colvarMin.DataType = DbType.Int32;
                colvarMin.MaxLength = 0;
                colvarMin.AutoIncrement = false;
                colvarMin.IsNullable = false;
                colvarMin.IsPrimaryKey = false;
                colvarMin.IsForeignKey = false;
                colvarMin.IsReadOnly = false;

                colvarMin.DefaultSetting = @"((0))";
                colvarMin.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMin);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("ship_company", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public decimal Sequence
        {
            get { return GetColumnValue<decimal>(Columns.Sequence); }
            set { SetColumnValue(Columns.Sequence, value); }
        }

        [XmlAttribute("ShipCompanyName")]
        [Bindable(true)]
        public string ShipCompanyName
        {
            get { return GetColumnValue<string>(Columns.ShipCompanyName); }
            set { SetColumnValue(Columns.ShipCompanyName, value); }
        }

        [XmlAttribute("ShipWebsite")]
        [Bindable(true)]
        public string ShipWebsite
        {
            get { return GetColumnValue<string>(Columns.ShipWebsite); }
            set { SetColumnValue(Columns.ShipWebsite, value); }
        }

        [XmlAttribute("ServiceTel")]
        [Bindable(true)]
        public string ServiceTel
        {
            get { return GetColumnValue<string>(Columns.ServiceTel); }
            set { SetColumnValue(Columns.ServiceTel, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public bool? Status
        {
            get { return GetColumnValue<bool?>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("Creator")]
        [Bindable(true)]
        public string Creator
        {
            get { return GetColumnValue<string>(Columns.Creator); }
            set { SetColumnValue(Columns.Creator, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Min")]
        [Bindable(true)]
        public int Min
        {
            get { return GetColumnValue<int>(Columns.Min); }
            set { SetColumnValue(Columns.Min, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ShipCompanyNameColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn ShipWebsiteColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ServiceTelColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn MinColumn
        {
            get { return Schema.Columns[8]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Sequence = @"sequence";
            public static string ShipCompanyName = @"ship_company_name";
            public static string ShipWebsite = @"ship_website";
            public static string ServiceTel = @"service_tel";
            public static string Status = @"status";
            public static string Creator = @"creator";
            public static string CreateTime = @"create_time";
            public static string Min = @"min";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

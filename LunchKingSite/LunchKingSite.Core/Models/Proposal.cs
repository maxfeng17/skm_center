using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Proposal class.
	/// </summary>
    [Serializable]
	public partial class ProposalCollection : RepositoryList<Proposal, ProposalCollection>
	{	   
		public ProposalCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalCollection</returns>
		public ProposalCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Proposal o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the proposal table.
	/// </summary>
	[Serializable]
	public partial class Proposal : RepositoryRecord<Proposal>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Proposal()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Proposal(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("proposal", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = true;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarReferrerBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarReferrerBusinessHourGuid.ColumnName = "referrer_business_hour_guid";
				colvarReferrerBusinessHourGuid.DataType = DbType.Guid;
				colvarReferrerBusinessHourGuid.MaxLength = 0;
				colvarReferrerBusinessHourGuid.AutoIncrement = false;
				colvarReferrerBusinessHourGuid.IsNullable = true;
				colvarReferrerBusinessHourGuid.IsPrimaryKey = false;
				colvarReferrerBusinessHourGuid.IsForeignKey = false;
				colvarReferrerBusinessHourGuid.IsReadOnly = false;
				colvarReferrerBusinessHourGuid.DefaultSetting = @"";
				colvarReferrerBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferrerBusinessHourGuid);
				
				TableSchema.TableColumn colvarCopyType = new TableSchema.TableColumn(schema);
				colvarCopyType.ColumnName = "copy_type";
				colvarCopyType.DataType = DbType.Int32;
				colvarCopyType.MaxLength = 0;
				colvarCopyType.AutoIncrement = false;
				colvarCopyType.IsNullable = false;
				colvarCopyType.IsPrimaryKey = false;
				colvarCopyType.IsForeignKey = false;
				colvarCopyType.IsReadOnly = false;
				
						colvarCopyType.DefaultSetting = @"((0))";
				colvarCopyType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCopyType);
				
				TableSchema.TableColumn colvarDevelopeSalesId = new TableSchema.TableColumn(schema);
				colvarDevelopeSalesId.ColumnName = "develope_sales_id";
				colvarDevelopeSalesId.DataType = DbType.Int32;
				colvarDevelopeSalesId.MaxLength = 0;
				colvarDevelopeSalesId.AutoIncrement = false;
				colvarDevelopeSalesId.IsNullable = false;
				colvarDevelopeSalesId.IsPrimaryKey = false;
				colvarDevelopeSalesId.IsForeignKey = false;
				colvarDevelopeSalesId.IsReadOnly = false;
				colvarDevelopeSalesId.DefaultSetting = @"";
				colvarDevelopeSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDevelopeSalesId);
				
				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = false;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);
				
				TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
				colvarDealType.ColumnName = "deal_type";
				colvarDealType.DataType = DbType.Int32;
				colvarDealType.MaxLength = 0;
				colvarDealType.AutoIncrement = false;
				colvarDealType.IsNullable = false;
				colvarDealType.IsPrimaryKey = false;
				colvarDealType.IsForeignKey = false;
				colvarDealType.IsReadOnly = false;
				
						colvarDealType.DefaultSetting = @"((0))";
				colvarDealType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealType);
				
				TableSchema.TableColumn colvarDealSubType = new TableSchema.TableColumn(schema);
				colvarDealSubType.ColumnName = "deal_sub_type";
				colvarDealSubType.DataType = DbType.Int32;
				colvarDealSubType.MaxLength = 0;
				colvarDealSubType.AutoIncrement = false;
				colvarDealSubType.IsNullable = false;
				colvarDealSubType.IsPrimaryKey = false;
				colvarDealSubType.IsForeignKey = false;
				colvarDealSubType.IsReadOnly = false;
				
						colvarDealSubType.DefaultSetting = @"((0))";
				colvarDealSubType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealSubType);
				
				TableSchema.TableColumn colvarApplyFlag = new TableSchema.TableColumn(schema);
				colvarApplyFlag.ColumnName = "apply_flag";
				colvarApplyFlag.DataType = DbType.Int32;
				colvarApplyFlag.MaxLength = 0;
				colvarApplyFlag.AutoIncrement = false;
				colvarApplyFlag.IsNullable = false;
				colvarApplyFlag.IsPrimaryKey = false;
				colvarApplyFlag.IsForeignKey = false;
				colvarApplyFlag.IsReadOnly = false;
				
						colvarApplyFlag.DefaultSetting = @"((0))";
				colvarApplyFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApplyFlag);
				
				TableSchema.TableColumn colvarApproveFlag = new TableSchema.TableColumn(schema);
				colvarApproveFlag.ColumnName = "approve_flag";
				colvarApproveFlag.DataType = DbType.Int32;
				colvarApproveFlag.MaxLength = 0;
				colvarApproveFlag.AutoIncrement = false;
				colvarApproveFlag.IsNullable = false;
				colvarApproveFlag.IsPrimaryKey = false;
				colvarApproveFlag.IsForeignKey = false;
				colvarApproveFlag.IsReadOnly = false;
				
						colvarApproveFlag.DefaultSetting = @"((0))";
				colvarApproveFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApproveFlag);
				
				TableSchema.TableColumn colvarBusinessCreateFlag = new TableSchema.TableColumn(schema);
				colvarBusinessCreateFlag.ColumnName = "business_create_flag";
				colvarBusinessCreateFlag.DataType = DbType.Int32;
				colvarBusinessCreateFlag.MaxLength = 0;
				colvarBusinessCreateFlag.AutoIncrement = false;
				colvarBusinessCreateFlag.IsNullable = false;
				colvarBusinessCreateFlag.IsPrimaryKey = false;
				colvarBusinessCreateFlag.IsForeignKey = false;
				colvarBusinessCreateFlag.IsReadOnly = false;
				
						colvarBusinessCreateFlag.DefaultSetting = @"((0))";
				colvarBusinessCreateFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessCreateFlag);
				
				TableSchema.TableColumn colvarBusinessFlag = new TableSchema.TableColumn(schema);
				colvarBusinessFlag.ColumnName = "business_flag";
				colvarBusinessFlag.DataType = DbType.Int32;
				colvarBusinessFlag.MaxLength = 0;
				colvarBusinessFlag.AutoIncrement = false;
				colvarBusinessFlag.IsNullable = false;
				colvarBusinessFlag.IsPrimaryKey = false;
				colvarBusinessFlag.IsForeignKey = false;
				colvarBusinessFlag.IsReadOnly = false;
				
						colvarBusinessFlag.DefaultSetting = @"((0))";
				colvarBusinessFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessFlag);
				
				TableSchema.TableColumn colvarEditFlag = new TableSchema.TableColumn(schema);
				colvarEditFlag.ColumnName = "edit_flag";
				colvarEditFlag.DataType = DbType.Int32;
				colvarEditFlag.MaxLength = 0;
				colvarEditFlag.AutoIncrement = false;
				colvarEditFlag.IsNullable = false;
				colvarEditFlag.IsPrimaryKey = false;
				colvarEditFlag.IsForeignKey = false;
				colvarEditFlag.IsReadOnly = false;
				
						colvarEditFlag.DefaultSetting = @"((0))";
				colvarEditFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEditFlag);
				
				TableSchema.TableColumn colvarListingFlag = new TableSchema.TableColumn(schema);
				colvarListingFlag.ColumnName = "listing_flag";
				colvarListingFlag.DataType = DbType.Int32;
				colvarListingFlag.MaxLength = 0;
				colvarListingFlag.AutoIncrement = false;
				colvarListingFlag.IsNullable = false;
				colvarListingFlag.IsPrimaryKey = false;
				colvarListingFlag.IsForeignKey = false;
				colvarListingFlag.IsReadOnly = false;
				
						colvarListingFlag.DefaultSetting = @"((0))";
				colvarListingFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarListingFlag);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarPayType = new TableSchema.TableColumn(schema);
				colvarPayType.ColumnName = "pay_type";
				colvarPayType.DataType = DbType.Int32;
				colvarPayType.MaxLength = 0;
				colvarPayType.AutoIncrement = false;
				colvarPayType.IsNullable = false;
				colvarPayType.IsPrimaryKey = false;
				colvarPayType.IsForeignKey = false;
				colvarPayType.IsReadOnly = false;
				
						colvarPayType.DefaultSetting = @"((1))";
				colvarPayType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayType);
				
				TableSchema.TableColumn colvarProductSpec = new TableSchema.TableColumn(schema);
				colvarProductSpec.ColumnName = "product_spec";
				colvarProductSpec.DataType = DbType.String;
				colvarProductSpec.MaxLength = 4000;
				colvarProductSpec.AutoIncrement = false;
				colvarProductSpec.IsNullable = false;
				colvarProductSpec.IsPrimaryKey = false;
				colvarProductSpec.IsForeignKey = false;
				colvarProductSpec.IsReadOnly = false;
				colvarProductSpec.DefaultSetting = @"";
				colvarProductSpec.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductSpec);
				
				TableSchema.TableColumn colvarSpecialFlag = new TableSchema.TableColumn(schema);
				colvarSpecialFlag.ColumnName = "special_flag";
				colvarSpecialFlag.DataType = DbType.Int32;
				colvarSpecialFlag.MaxLength = 0;
				colvarSpecialFlag.AutoIncrement = false;
				colvarSpecialFlag.IsNullable = false;
				colvarSpecialFlag.IsPrimaryKey = false;
				colvarSpecialFlag.IsForeignKey = false;
				colvarSpecialFlag.IsReadOnly = false;
				
						colvarSpecialFlag.DefaultSetting = @"((0))";
				colvarSpecialFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialFlag);
				
				TableSchema.TableColumn colvarSpecialFlagText = new TableSchema.TableColumn(schema);
				colvarSpecialFlagText.ColumnName = "special_flag_text";
				colvarSpecialFlagText.DataType = DbType.String;
				colvarSpecialFlagText.MaxLength = 1073741823;
				colvarSpecialFlagText.AutoIncrement = false;
				colvarSpecialFlagText.IsNullable = false;
				colvarSpecialFlagText.IsPrimaryKey = false;
				colvarSpecialFlagText.IsForeignKey = false;
				colvarSpecialFlagText.IsReadOnly = false;
				colvarSpecialFlagText.DefaultSetting = @"";
				colvarSpecialFlagText.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialFlagText);
				
				TableSchema.TableColumn colvarMarketAnalysis = new TableSchema.TableColumn(schema);
				colvarMarketAnalysis.ColumnName = "market_analysis";
				colvarMarketAnalysis.DataType = DbType.String;
				colvarMarketAnalysis.MaxLength = 1000;
				colvarMarketAnalysis.AutoIncrement = false;
				colvarMarketAnalysis.IsNullable = false;
				colvarMarketAnalysis.IsPrimaryKey = false;
				colvarMarketAnalysis.IsForeignKey = false;
				colvarMarketAnalysis.IsReadOnly = false;
				colvarMarketAnalysis.DefaultSetting = @"";
				colvarMarketAnalysis.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMarketAnalysis);
				
				TableSchema.TableColumn colvarMultiDealsDelete = new TableSchema.TableColumn(schema);
				colvarMultiDealsDelete.ColumnName = "multi_deals_delete";
				colvarMultiDealsDelete.DataType = DbType.String;
				colvarMultiDealsDelete.MaxLength = 1073741823;
				colvarMultiDealsDelete.AutoIncrement = false;
				colvarMultiDealsDelete.IsNullable = false;
				colvarMultiDealsDelete.IsPrimaryKey = false;
				colvarMultiDealsDelete.IsForeignKey = false;
				colvarMultiDealsDelete.IsReadOnly = false;
				colvarMultiDealsDelete.DefaultSetting = @"";
				colvarMultiDealsDelete.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMultiDealsDelete);
				
				TableSchema.TableColumn colvarOrderTimeS = new TableSchema.TableColumn(schema);
				colvarOrderTimeS.ColumnName = "order_time_s";
				colvarOrderTimeS.DataType = DbType.DateTime;
				colvarOrderTimeS.MaxLength = 0;
				colvarOrderTimeS.AutoIncrement = false;
				colvarOrderTimeS.IsNullable = true;
				colvarOrderTimeS.IsPrimaryKey = false;
				colvarOrderTimeS.IsForeignKey = false;
				colvarOrderTimeS.IsReadOnly = false;
				colvarOrderTimeS.DefaultSetting = @"";
				colvarOrderTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderTimeS);
				
				TableSchema.TableColumn colvarScheduleExpect = new TableSchema.TableColumn(schema);
				colvarScheduleExpect.ColumnName = "schedule_expect";
				colvarScheduleExpect.DataType = DbType.String;
				colvarScheduleExpect.MaxLength = 150;
				colvarScheduleExpect.AutoIncrement = false;
				colvarScheduleExpect.IsNullable = false;
				colvarScheduleExpect.IsPrimaryKey = false;
				colvarScheduleExpect.IsForeignKey = false;
				colvarScheduleExpect.IsReadOnly = false;
				colvarScheduleExpect.DefaultSetting = @"";
				colvarScheduleExpect.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScheduleExpect);
				
				TableSchema.TableColumn colvarStartUseUnit = new TableSchema.TableColumn(schema);
				colvarStartUseUnit.ColumnName = "start_use_unit";
				colvarStartUseUnit.DataType = DbType.Int32;
				colvarStartUseUnit.MaxLength = 0;
				colvarStartUseUnit.AutoIncrement = false;
				colvarStartUseUnit.IsNullable = false;
				colvarStartUseUnit.IsPrimaryKey = false;
				colvarStartUseUnit.IsForeignKey = false;
				colvarStartUseUnit.IsReadOnly = false;
				
						colvarStartUseUnit.DefaultSetting = @"((0))";
				colvarStartUseUnit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartUseUnit);
				
				TableSchema.TableColumn colvarStartUseText = new TableSchema.TableColumn(schema);
				colvarStartUseText.ColumnName = "start_use_text";
				colvarStartUseText.DataType = DbType.String;
				colvarStartUseText.MaxLength = 500;
				colvarStartUseText.AutoIncrement = false;
				colvarStartUseText.IsNullable = false;
				colvarStartUseText.IsPrimaryKey = false;
				colvarStartUseText.IsForeignKey = false;
				colvarStartUseText.IsReadOnly = false;
				colvarStartUseText.DefaultSetting = @"";
				colvarStartUseText.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartUseText);
				
				TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
				colvarShipType.ColumnName = "ship_type";
				colvarShipType.DataType = DbType.Int32;
				colvarShipType.MaxLength = 0;
				colvarShipType.AutoIncrement = false;
				colvarShipType.IsNullable = false;
				colvarShipType.IsPrimaryKey = false;
				colvarShipType.IsForeignKey = false;
				colvarShipType.IsReadOnly = false;
				
						colvarShipType.DefaultSetting = @"((0))";
				colvarShipType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipType);
				
				TableSchema.TableColumn colvarShipText1 = new TableSchema.TableColumn(schema);
				colvarShipText1.ColumnName = "ship_text1";
				colvarShipText1.DataType = DbType.String;
				colvarShipText1.MaxLength = 50;
				colvarShipText1.AutoIncrement = false;
				colvarShipText1.IsNullable = false;
				colvarShipText1.IsPrimaryKey = false;
				colvarShipText1.IsForeignKey = false;
				colvarShipText1.IsReadOnly = false;
				colvarShipText1.DefaultSetting = @"";
				colvarShipText1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipText1);
				
				TableSchema.TableColumn colvarShipText2 = new TableSchema.TableColumn(schema);
				colvarShipText2.ColumnName = "ship_text2";
				colvarShipText2.DataType = DbType.String;
				colvarShipText2.MaxLength = 50;
				colvarShipText2.AutoIncrement = false;
				colvarShipText2.IsNullable = false;
				colvarShipText2.IsPrimaryKey = false;
				colvarShipText2.IsForeignKey = false;
				colvarShipText2.IsReadOnly = false;
				colvarShipText2.DefaultSetting = @"";
				colvarShipText2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipText2);
				
				TableSchema.TableColumn colvarContractMemo = new TableSchema.TableColumn(schema);
				colvarContractMemo.ColumnName = "contract_memo";
				colvarContractMemo.DataType = DbType.String;
				colvarContractMemo.MaxLength = 1073741823;
				colvarContractMemo.AutoIncrement = false;
				colvarContractMemo.IsNullable = false;
				colvarContractMemo.IsPrimaryKey = false;
				colvarContractMemo.IsForeignKey = false;
				colvarContractMemo.IsReadOnly = false;
				colvarContractMemo.DefaultSetting = @"";
				colvarContractMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContractMemo);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 1000;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = false;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 50;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarBrandName = new TableSchema.TableColumn(schema);
				colvarBrandName.ColumnName = "brand_name";
				colvarBrandName.DataType = DbType.String;
				colvarBrandName.MaxLength = 100;
				colvarBrandName.AutoIncrement = false;
				colvarBrandName.IsNullable = true;
				colvarBrandName.IsPrimaryKey = false;
				colvarBrandName.IsForeignKey = false;
				colvarBrandName.IsReadOnly = false;
				colvarBrandName.DefaultSetting = @"";
				colvarBrandName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandName);
				
				TableSchema.TableColumn colvarMarketingResource = new TableSchema.TableColumn(schema);
				colvarMarketingResource.ColumnName = "marketing_resource";
				colvarMarketingResource.DataType = DbType.String;
				colvarMarketingResource.MaxLength = 500;
				colvarMarketingResource.AutoIncrement = false;
				colvarMarketingResource.IsNullable = true;
				colvarMarketingResource.IsPrimaryKey = false;
				colvarMarketingResource.IsForeignKey = false;
				colvarMarketingResource.IsReadOnly = false;
				colvarMarketingResource.DefaultSetting = @"";
				colvarMarketingResource.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMarketingResource);
				
				TableSchema.TableColumn colvarShipText3 = new TableSchema.TableColumn(schema);
				colvarShipText3.ColumnName = "ship_text3";
				colvarShipText3.DataType = DbType.String;
				colvarShipText3.MaxLength = 50;
				colvarShipText3.AutoIncrement = false;
				colvarShipText3.IsNullable = true;
				colvarShipText3.IsPrimaryKey = false;
				colvarShipText3.IsForeignKey = false;
				colvarShipText3.IsReadOnly = false;
				colvarShipText3.DefaultSetting = @"";
				colvarShipText3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipText3);
				
				TableSchema.TableColumn colvarShippingdateType = new TableSchema.TableColumn(schema);
				colvarShippingdateType.ColumnName = "shippingdate_type";
				colvarShippingdateType.DataType = DbType.Int32;
				colvarShippingdateType.MaxLength = 0;
				colvarShippingdateType.AutoIncrement = false;
				colvarShippingdateType.IsNullable = true;
				colvarShippingdateType.IsPrimaryKey = false;
				colvarShippingdateType.IsForeignKey = false;
				colvarShippingdateType.IsReadOnly = false;
				colvarShippingdateType.DefaultSetting = @"";
				colvarShippingdateType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShippingdateType);
				
				TableSchema.TableColumn colvarBusinessPrice = new TableSchema.TableColumn(schema);
				colvarBusinessPrice.ColumnName = "business_price";
				colvarBusinessPrice.DataType = DbType.Int32;
				colvarBusinessPrice.MaxLength = 0;
				colvarBusinessPrice.AutoIncrement = false;
				colvarBusinessPrice.IsNullable = true;
				colvarBusinessPrice.IsPrimaryKey = false;
				colvarBusinessPrice.IsForeignKey = false;
				colvarBusinessPrice.IsReadOnly = false;
				colvarBusinessPrice.DefaultSetting = @"";
				colvarBusinessPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessPrice);
				
				TableSchema.TableColumn colvarConsumerTime = new TableSchema.TableColumn(schema);
				colvarConsumerTime.ColumnName = "consumer_time";
				colvarConsumerTime.DataType = DbType.Int32;
				colvarConsumerTime.MaxLength = 0;
				colvarConsumerTime.AutoIncrement = false;
				colvarConsumerTime.IsNullable = false;
				colvarConsumerTime.IsPrimaryKey = false;
				colvarConsumerTime.IsForeignKey = false;
				colvarConsumerTime.IsReadOnly = false;
				
						colvarConsumerTime.DefaultSetting = @"((0))";
				colvarConsumerTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsumerTime);
				
				TableSchema.TableColumn colvarPhotographerAppointFlag = new TableSchema.TableColumn(schema);
				colvarPhotographerAppointFlag.ColumnName = "photographer_appoint_flag";
				colvarPhotographerAppointFlag.DataType = DbType.Int32;
				colvarPhotographerAppointFlag.MaxLength = 0;
				colvarPhotographerAppointFlag.AutoIncrement = false;
				colvarPhotographerAppointFlag.IsNullable = false;
				colvarPhotographerAppointFlag.IsPrimaryKey = false;
				colvarPhotographerAppointFlag.IsForeignKey = false;
				colvarPhotographerAppointFlag.IsReadOnly = false;
				
						colvarPhotographerAppointFlag.DefaultSetting = @"((0))";
				colvarPhotographerAppointFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhotographerAppointFlag);
				
				TableSchema.TableColumn colvarSpecialAppointFlagText = new TableSchema.TableColumn(schema);
				colvarSpecialAppointFlagText.ColumnName = "special_appoint_flag_text";
				colvarSpecialAppointFlagText.DataType = DbType.String;
				colvarSpecialAppointFlagText.MaxLength = 1073741823;
				colvarSpecialAppointFlagText.AutoIncrement = false;
				colvarSpecialAppointFlagText.IsNullable = true;
				colvarSpecialAppointFlagText.IsPrimaryKey = false;
				colvarSpecialAppointFlagText.IsForeignKey = false;
				colvarSpecialAppointFlagText.IsReadOnly = false;
				colvarSpecialAppointFlagText.DefaultSetting = @"";
				colvarSpecialAppointFlagText.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialAppointFlagText);
				
				TableSchema.TableColumn colvarEditPassFlag = new TableSchema.TableColumn(schema);
				colvarEditPassFlag.ColumnName = "edit_pass_flag";
				colvarEditPassFlag.DataType = DbType.Int32;
				colvarEditPassFlag.MaxLength = 0;
				colvarEditPassFlag.AutoIncrement = false;
				colvarEditPassFlag.IsNullable = false;
				colvarEditPassFlag.IsPrimaryKey = false;
				colvarEditPassFlag.IsForeignKey = false;
				colvarEditPassFlag.IsReadOnly = false;
				
						colvarEditPassFlag.DefaultSetting = @"((0))";
				colvarEditPassFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEditPassFlag);
				
				TableSchema.TableColumn colvarAncestorBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarAncestorBusinessHourGuid.ColumnName = "ancestor_business_hour_guid";
				colvarAncestorBusinessHourGuid.DataType = DbType.Guid;
				colvarAncestorBusinessHourGuid.MaxLength = 0;
				colvarAncestorBusinessHourGuid.AutoIncrement = false;
				colvarAncestorBusinessHourGuid.IsNullable = true;
				colvarAncestorBusinessHourGuid.IsPrimaryKey = false;
				colvarAncestorBusinessHourGuid.IsForeignKey = false;
				colvarAncestorBusinessHourGuid.IsReadOnly = false;
				colvarAncestorBusinessHourGuid.DefaultSetting = @"";
				colvarAncestorBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAncestorBusinessHourGuid);
				
				TableSchema.TableColumn colvarAncestorSequenceBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarAncestorSequenceBusinessHourGuid.ColumnName = "ancestor_sequence_business_hour_guid";
				colvarAncestorSequenceBusinessHourGuid.DataType = DbType.Guid;
				colvarAncestorSequenceBusinessHourGuid.MaxLength = 0;
				colvarAncestorSequenceBusinessHourGuid.AutoIncrement = false;
				colvarAncestorSequenceBusinessHourGuid.IsNullable = true;
				colvarAncestorSequenceBusinessHourGuid.IsPrimaryKey = false;
				colvarAncestorSequenceBusinessHourGuid.IsForeignKey = false;
				colvarAncestorSequenceBusinessHourGuid.IsReadOnly = false;
				colvarAncestorSequenceBusinessHourGuid.DefaultSetting = @"";
				colvarAncestorSequenceBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAncestorSequenceBusinessHourGuid);
				
				TableSchema.TableColumn colvarStartUseText2 = new TableSchema.TableColumn(schema);
				colvarStartUseText2.ColumnName = "start_use_text2";
				colvarStartUseText2.DataType = DbType.String;
				colvarStartUseText2.MaxLength = 500;
				colvarStartUseText2.AutoIncrement = false;
				colvarStartUseText2.IsNullable = false;
				colvarStartUseText2.IsPrimaryKey = false;
				colvarStartUseText2.IsForeignKey = false;
				colvarStartUseText2.IsReadOnly = false;
				
						colvarStartUseText2.DefaultSetting = @"('')";
				colvarStartUseText2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartUseText2);
				
				TableSchema.TableColumn colvarSaleMarketAnalysis = new TableSchema.TableColumn(schema);
				colvarSaleMarketAnalysis.ColumnName = "sale_market_analysis";
				colvarSaleMarketAnalysis.DataType = DbType.String;
				colvarSaleMarketAnalysis.MaxLength = -1;
				colvarSaleMarketAnalysis.AutoIncrement = false;
				colvarSaleMarketAnalysis.IsNullable = true;
				colvarSaleMarketAnalysis.IsPrimaryKey = false;
				colvarSaleMarketAnalysis.IsForeignKey = false;
				colvarSaleMarketAnalysis.IsReadOnly = false;
				colvarSaleMarketAnalysis.DefaultSetting = @"";
				colvarSaleMarketAnalysis.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSaleMarketAnalysis);
				
				TableSchema.TableColumn colvarMediaReportFlag = new TableSchema.TableColumn(schema);
				colvarMediaReportFlag.ColumnName = "media_report_flag";
				colvarMediaReportFlag.DataType = DbType.Int32;
				colvarMediaReportFlag.MaxLength = 0;
				colvarMediaReportFlag.AutoIncrement = false;
				colvarMediaReportFlag.IsNullable = false;
				colvarMediaReportFlag.IsPrimaryKey = false;
				colvarMediaReportFlag.IsForeignKey = false;
				colvarMediaReportFlag.IsReadOnly = false;
				
						colvarMediaReportFlag.DefaultSetting = @"((0))";
				colvarMediaReportFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMediaReportFlag);
				
				TableSchema.TableColumn colvarMediaReportFlagText = new TableSchema.TableColumn(schema);
				colvarMediaReportFlagText.ColumnName = "media_report_flag_text";
				colvarMediaReportFlagText.DataType = DbType.String;
				colvarMediaReportFlagText.MaxLength = 1073741823;
				colvarMediaReportFlagText.AutoIncrement = false;
				colvarMediaReportFlagText.IsNullable = true;
				colvarMediaReportFlagText.IsPrimaryKey = false;
				colvarMediaReportFlagText.IsForeignKey = false;
				colvarMediaReportFlagText.IsReadOnly = false;
				colvarMediaReportFlagText.DefaultSetting = @"";
				colvarMediaReportFlagText.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMediaReportFlagText);
				
				TableSchema.TableColumn colvarDealCharacterFlag = new TableSchema.TableColumn(schema);
				colvarDealCharacterFlag.ColumnName = "deal_character_flag";
				colvarDealCharacterFlag.DataType = DbType.Int32;
				colvarDealCharacterFlag.MaxLength = 0;
				colvarDealCharacterFlag.AutoIncrement = false;
				colvarDealCharacterFlag.IsNullable = false;
				colvarDealCharacterFlag.IsPrimaryKey = false;
				colvarDealCharacterFlag.IsForeignKey = false;
				colvarDealCharacterFlag.IsReadOnly = false;
				
						colvarDealCharacterFlag.DefaultSetting = @"((0))";
				colvarDealCharacterFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealCharacterFlag);
				
				TableSchema.TableColumn colvarDealCharacterFlagText = new TableSchema.TableColumn(schema);
				colvarDealCharacterFlagText.ColumnName = "deal_character_flag_text";
				colvarDealCharacterFlagText.DataType = DbType.String;
				colvarDealCharacterFlagText.MaxLength = 1073741823;
				colvarDealCharacterFlagText.AutoIncrement = false;
				colvarDealCharacterFlagText.IsNullable = true;
				colvarDealCharacterFlagText.IsPrimaryKey = false;
				colvarDealCharacterFlagText.IsForeignKey = false;
				colvarDealCharacterFlagText.IsReadOnly = false;
				colvarDealCharacterFlagText.DefaultSetting = @"";
				colvarDealCharacterFlagText.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealCharacterFlagText);
				
				TableSchema.TableColumn colvarPicAlt = new TableSchema.TableColumn(schema);
				colvarPicAlt.ColumnName = "pic_alt";
				colvarPicAlt.DataType = DbType.String;
				colvarPicAlt.MaxLength = 120;
				colvarPicAlt.AutoIncrement = false;
				colvarPicAlt.IsNullable = true;
				colvarPicAlt.IsPrimaryKey = false;
				colvarPicAlt.IsForeignKey = false;
				colvarPicAlt.IsReadOnly = false;
				colvarPicAlt.DefaultSetting = @"";
				colvarPicAlt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPicAlt);
				
				TableSchema.TableColumn colvarVendorReceiptType = new TableSchema.TableColumn(schema);
				colvarVendorReceiptType.ColumnName = "vendor_receipt_type";
				colvarVendorReceiptType.DataType = DbType.Int32;
				colvarVendorReceiptType.MaxLength = 0;
				colvarVendorReceiptType.AutoIncrement = false;
				colvarVendorReceiptType.IsNullable = true;
				colvarVendorReceiptType.IsPrimaryKey = false;
				colvarVendorReceiptType.IsForeignKey = false;
				colvarVendorReceiptType.IsReadOnly = false;
				colvarVendorReceiptType.DefaultSetting = @"";
				colvarVendorReceiptType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorReceiptType);
				
				TableSchema.TableColumn colvarOthermessage = new TableSchema.TableColumn(schema);
				colvarOthermessage.ColumnName = "othermessage";
				colvarOthermessage.DataType = DbType.String;
				colvarOthermessage.MaxLength = 50;
				colvarOthermessage.AutoIncrement = false;
				colvarOthermessage.IsNullable = true;
				colvarOthermessage.IsPrimaryKey = false;
				colvarOthermessage.IsForeignKey = false;
				colvarOthermessage.IsReadOnly = false;
				colvarOthermessage.DefaultSetting = @"";
				colvarOthermessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOthermessage);
				
				TableSchema.TableColumn colvarDeliveryMethod = new TableSchema.TableColumn(schema);
				colvarDeliveryMethod.ColumnName = "delivery_method";
				colvarDeliveryMethod.DataType = DbType.Int32;
				colvarDeliveryMethod.MaxLength = 0;
				colvarDeliveryMethod.AutoIncrement = false;
				colvarDeliveryMethod.IsNullable = true;
				colvarDeliveryMethod.IsPrimaryKey = false;
				colvarDeliveryMethod.IsForeignKey = false;
				colvarDeliveryMethod.IsReadOnly = false;
				colvarDeliveryMethod.DefaultSetting = @"";
				colvarDeliveryMethod.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryMethod);
				
				TableSchema.TableColumn colvarSellerProposalFlag = new TableSchema.TableColumn(schema);
				colvarSellerProposalFlag.ColumnName = "seller_proposal_flag";
				colvarSellerProposalFlag.DataType = DbType.Int32;
				colvarSellerProposalFlag.MaxLength = 0;
				colvarSellerProposalFlag.AutoIncrement = false;
				colvarSellerProposalFlag.IsNullable = false;
				colvarSellerProposalFlag.IsPrimaryKey = false;
				colvarSellerProposalFlag.IsForeignKey = false;
				colvarSellerProposalFlag.IsReadOnly = false;
				
						colvarSellerProposalFlag.DefaultSetting = @"((0))";
				colvarSellerProposalFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerProposalFlag);
				
				TableSchema.TableColumn colvarProposalCreatedType = new TableSchema.TableColumn(schema);
				colvarProposalCreatedType.ColumnName = "proposal_created_type";
				colvarProposalCreatedType.DataType = DbType.Int16;
				colvarProposalCreatedType.MaxLength = 0;
				colvarProposalCreatedType.AutoIncrement = false;
				colvarProposalCreatedType.IsNullable = false;
				colvarProposalCreatedType.IsPrimaryKey = false;
				colvarProposalCreatedType.IsForeignKey = false;
				colvarProposalCreatedType.IsReadOnly = false;
				
						colvarProposalCreatedType.DefaultSetting = @"((1))";
				colvarProposalCreatedType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProposalCreatedType);
				
				TableSchema.TableColumn colvarTrialPeriod = new TableSchema.TableColumn(schema);
				colvarTrialPeriod.ColumnName = "trial_period";
				colvarTrialPeriod.DataType = DbType.Boolean;
				colvarTrialPeriod.MaxLength = 0;
				colvarTrialPeriod.AutoIncrement = false;
				colvarTrialPeriod.IsNullable = true;
				colvarTrialPeriod.IsPrimaryKey = false;
				colvarTrialPeriod.IsForeignKey = false;
				colvarTrialPeriod.IsReadOnly = false;
				colvarTrialPeriod.DefaultSetting = @"";
				colvarTrialPeriod.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrialPeriod);
				
				TableSchema.TableColumn colvarMohist = new TableSchema.TableColumn(schema);
				colvarMohist.ColumnName = "mohist";
				colvarMohist.DataType = DbType.Boolean;
				colvarMohist.MaxLength = 0;
				colvarMohist.AutoIncrement = false;
				colvarMohist.IsNullable = true;
				colvarMohist.IsPrimaryKey = false;
				colvarMohist.IsForeignKey = false;
				colvarMohist.IsReadOnly = false;
				colvarMohist.DefaultSetting = @"";
				colvarMohist.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMohist);
				
				TableSchema.TableColumn colvarShipOther = new TableSchema.TableColumn(schema);
				colvarShipOther.ColumnName = "ship_other";
				colvarShipOther.DataType = DbType.String;
				colvarShipOther.MaxLength = 250;
				colvarShipOther.AutoIncrement = false;
				colvarShipOther.IsNullable = true;
				colvarShipOther.IsPrimaryKey = false;
				colvarShipOther.IsForeignKey = false;
				colvarShipOther.IsReadOnly = false;
				colvarShipOther.DefaultSetting = @"";
				colvarShipOther.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipOther);
				
				TableSchema.TableColumn colvarPassSeller = new TableSchema.TableColumn(schema);
				colvarPassSeller.ColumnName = "pass_seller";
				colvarPassSeller.DataType = DbType.Int32;
				colvarPassSeller.MaxLength = 0;
				colvarPassSeller.AutoIncrement = false;
				colvarPassSeller.IsNullable = false;
				colvarPassSeller.IsPrimaryKey = false;
				colvarPassSeller.IsForeignKey = false;
				colvarPassSeller.IsReadOnly = false;
				
						colvarPassSeller.DefaultSetting = @"((0))";
				colvarPassSeller.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPassSeller);
				
				TableSchema.TableColumn colvarSendSeller = new TableSchema.TableColumn(schema);
				colvarSendSeller.ColumnName = "send_seller";
				colvarSendSeller.DataType = DbType.Int32;
				colvarSendSeller.MaxLength = 0;
				colvarSendSeller.AutoIncrement = false;
				colvarSendSeller.IsNullable = false;
				colvarSendSeller.IsPrimaryKey = false;
				colvarSendSeller.IsForeignKey = false;
				colvarSendSeller.IsReadOnly = false;
				
						colvarSendSeller.DefaultSetting = @"((0))";
				colvarSendSeller.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendSeller);
				
				TableSchema.TableColumn colvarFilesDone = new TableSchema.TableColumn(schema);
				colvarFilesDone.ColumnName = "files_done";
				colvarFilesDone.DataType = DbType.Boolean;
				colvarFilesDone.MaxLength = 0;
				colvarFilesDone.AutoIncrement = false;
				colvarFilesDone.IsNullable = false;
				colvarFilesDone.IsPrimaryKey = false;
				colvarFilesDone.IsForeignKey = false;
				colvarFilesDone.IsReadOnly = false;
				
						colvarFilesDone.DefaultSetting = @"((0))";
				colvarFilesDone.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFilesDone);
				
				TableSchema.TableColumn colvarGroupCouponDealType = new TableSchema.TableColumn(schema);
				colvarGroupCouponDealType.ColumnName = "group_coupon_deal_type";
				colvarGroupCouponDealType.DataType = DbType.Int32;
				colvarGroupCouponDealType.MaxLength = 0;
				colvarGroupCouponDealType.AutoIncrement = false;
				colvarGroupCouponDealType.IsNullable = true;
				colvarGroupCouponDealType.IsPrimaryKey = false;
				colvarGroupCouponDealType.IsForeignKey = false;
				colvarGroupCouponDealType.IsReadOnly = false;
				colvarGroupCouponDealType.DefaultSetting = @"";
				colvarGroupCouponDealType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupCouponDealType);
				
				TableSchema.TableColumn colvarIsEarlierPageCheck = new TableSchema.TableColumn(schema);
				colvarIsEarlierPageCheck.ColumnName = "is_earlier_page_check";
				colvarIsEarlierPageCheck.DataType = DbType.Boolean;
				colvarIsEarlierPageCheck.MaxLength = 0;
				colvarIsEarlierPageCheck.AutoIncrement = false;
				colvarIsEarlierPageCheck.IsNullable = false;
				colvarIsEarlierPageCheck.IsPrimaryKey = false;
				colvarIsEarlierPageCheck.IsForeignKey = false;
				colvarIsEarlierPageCheck.IsReadOnly = false;
				
						colvarIsEarlierPageCheck.DefaultSetting = @"((0))";
				colvarIsEarlierPageCheck.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsEarlierPageCheck);
				
				TableSchema.TableColumn colvarEarlierPageCheckDay = new TableSchema.TableColumn(schema);
				colvarEarlierPageCheckDay.ColumnName = "earlier_page_check_day";
				colvarEarlierPageCheckDay.DataType = DbType.Int32;
				colvarEarlierPageCheckDay.MaxLength = 0;
				colvarEarlierPageCheckDay.AutoIncrement = false;
				colvarEarlierPageCheckDay.IsNullable = true;
				colvarEarlierPageCheckDay.IsPrimaryKey = false;
				colvarEarlierPageCheckDay.IsForeignKey = false;
				colvarEarlierPageCheckDay.IsReadOnly = false;
				colvarEarlierPageCheckDay.DefaultSetting = @"";
				colvarEarlierPageCheckDay.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEarlierPageCheckDay);
				
				TableSchema.TableColumn colvarConsignment = new TableSchema.TableColumn(schema);
				colvarConsignment.ColumnName = "consignment";
				colvarConsignment.DataType = DbType.Boolean;
				colvarConsignment.MaxLength = 0;
				colvarConsignment.AutoIncrement = false;
				colvarConsignment.IsNullable = false;
				colvarConsignment.IsPrimaryKey = false;
				colvarConsignment.IsForeignKey = false;
				colvarConsignment.IsReadOnly = false;
				
						colvarConsignment.DefaultSetting = @"((0))";
				colvarConsignment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsignment);
				
				TableSchema.TableColumn colvarSystemPic = new TableSchema.TableColumn(schema);
				colvarSystemPic.ColumnName = "system_pic";
				colvarSystemPic.DataType = DbType.Boolean;
				colvarSystemPic.MaxLength = 0;
				colvarSystemPic.AutoIncrement = false;
				colvarSystemPic.IsNullable = false;
				colvarSystemPic.IsPrimaryKey = false;
				colvarSystemPic.IsForeignKey = false;
				colvarSystemPic.IsReadOnly = false;
				
						colvarSystemPic.DefaultSetting = @"((0))";
				colvarSystemPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSystemPic);
				
				TableSchema.TableColumn colvarOperationSalesId = new TableSchema.TableColumn(schema);
				colvarOperationSalesId.ColumnName = "operation_sales_id";
				colvarOperationSalesId.DataType = DbType.Int32;
				colvarOperationSalesId.MaxLength = 0;
				colvarOperationSalesId.AutoIncrement = false;
				colvarOperationSalesId.IsNullable = true;
				colvarOperationSalesId.IsPrimaryKey = false;
				colvarOperationSalesId.IsForeignKey = false;
				colvarOperationSalesId.IsReadOnly = false;
				colvarOperationSalesId.DefaultSetting = @"";
				colvarOperationSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOperationSalesId);
				
				TableSchema.TableColumn colvarSpecialPrice = new TableSchema.TableColumn(schema);
				colvarSpecialPrice.ColumnName = "special_price";
				colvarSpecialPrice.DataType = DbType.Boolean;
				colvarSpecialPrice.MaxLength = 0;
				colvarSpecialPrice.AutoIncrement = false;
				colvarSpecialPrice.IsNullable = false;
				colvarSpecialPrice.IsPrimaryKey = false;
				colvarSpecialPrice.IsForeignKey = false;
				colvarSpecialPrice.IsReadOnly = false;
				
						colvarSpecialPrice.DefaultSetting = @"((0))";
				colvarSpecialPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialPrice);
				
				TableSchema.TableColumn colvarIsBankDeal = new TableSchema.TableColumn(schema);
				colvarIsBankDeal.ColumnName = "is_bank_deal";
				colvarIsBankDeal.DataType = DbType.Boolean;
				colvarIsBankDeal.MaxLength = 0;
				colvarIsBankDeal.AutoIncrement = false;
				colvarIsBankDeal.IsNullable = false;
				colvarIsBankDeal.IsPrimaryKey = false;
				colvarIsBankDeal.IsForeignKey = false;
				colvarIsBankDeal.IsReadOnly = false;
				
						colvarIsBankDeal.DefaultSetting = @"((0))";
				colvarIsBankDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsBankDeal);
				
				TableSchema.TableColumn colvarIsPromotionDeal = new TableSchema.TableColumn(schema);
				colvarIsPromotionDeal.ColumnName = "is_promotion_deal";
				colvarIsPromotionDeal.DataType = DbType.Boolean;
				colvarIsPromotionDeal.MaxLength = 0;
				colvarIsPromotionDeal.AutoIncrement = false;
				colvarIsPromotionDeal.IsNullable = false;
				colvarIsPromotionDeal.IsPrimaryKey = false;
				colvarIsPromotionDeal.IsForeignKey = false;
				colvarIsPromotionDeal.IsReadOnly = false;
				
						colvarIsPromotionDeal.DefaultSetting = @"((0))";
				colvarIsPromotionDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsPromotionDeal);
				
				TableSchema.TableColumn colvarIsExhibitionDeal = new TableSchema.TableColumn(schema);
				colvarIsExhibitionDeal.ColumnName = "is_exhibition_deal";
				colvarIsExhibitionDeal.DataType = DbType.Boolean;
				colvarIsExhibitionDeal.MaxLength = 0;
				colvarIsExhibitionDeal.AutoIncrement = false;
				colvarIsExhibitionDeal.IsNullable = false;
				colvarIsExhibitionDeal.IsPrimaryKey = false;
				colvarIsExhibitionDeal.IsForeignKey = false;
				colvarIsExhibitionDeal.IsReadOnly = false;
				
						colvarIsExhibitionDeal.DefaultSetting = @"((0))";
				colvarIsExhibitionDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsExhibitionDeal);
				
				TableSchema.TableColumn colvarNoDiscountShown = new TableSchema.TableColumn(schema);
				colvarNoDiscountShown.ColumnName = "no_discount_shown";
				colvarNoDiscountShown.DataType = DbType.Boolean;
				colvarNoDiscountShown.MaxLength = 0;
				colvarNoDiscountShown.AutoIncrement = false;
				colvarNoDiscountShown.IsNullable = false;
				colvarNoDiscountShown.IsPrimaryKey = false;
				colvarNoDiscountShown.IsForeignKey = false;
				colvarNoDiscountShown.IsReadOnly = false;
				
						colvarNoDiscountShown.DefaultSetting = @"((0))";
				colvarNoDiscountShown.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNoDiscountShown);
				
				TableSchema.TableColumn colvarDeliveryIslands = new TableSchema.TableColumn(schema);
				colvarDeliveryIslands.ColumnName = "delivery_islands";
				colvarDeliveryIslands.DataType = DbType.Boolean;
				colvarDeliveryIslands.MaxLength = 0;
				colvarDeliveryIslands.AutoIncrement = false;
				colvarDeliveryIslands.IsNullable = false;
				colvarDeliveryIslands.IsPrimaryKey = false;
				colvarDeliveryIslands.IsForeignKey = false;
				colvarDeliveryIslands.IsReadOnly = false;
				
						colvarDeliveryIslands.DefaultSetting = @"((0))";
				colvarDeliveryIslands.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryIslands);
				
				TableSchema.TableColumn colvarIsProduction = new TableSchema.TableColumn(schema);
				colvarIsProduction.ColumnName = "is_production";
				colvarIsProduction.DataType = DbType.Boolean;
				colvarIsProduction.MaxLength = 0;
				colvarIsProduction.AutoIncrement = false;
				colvarIsProduction.IsNullable = false;
				colvarIsProduction.IsPrimaryKey = false;
				colvarIsProduction.IsForeignKey = false;
				colvarIsProduction.IsReadOnly = false;
				
						colvarIsProduction.DefaultSetting = @"((0))";
				colvarIsProduction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsProduction);
				
				TableSchema.TableColumn colvarProductionDescription = new TableSchema.TableColumn(schema);
				colvarProductionDescription.ColumnName = "production_description";
				colvarProductionDescription.DataType = DbType.String;
				colvarProductionDescription.MaxLength = 1000;
				colvarProductionDescription.AutoIncrement = false;
				colvarProductionDescription.IsNullable = true;
				colvarProductionDescription.IsPrimaryKey = false;
				colvarProductionDescription.IsForeignKey = false;
				colvarProductionDescription.IsReadOnly = false;
				colvarProductionDescription.DefaultSetting = @"";
				colvarProductionDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductionDescription);
				
				TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
				colvarRemittanceType.ColumnName = "remittance_type";
				colvarRemittanceType.DataType = DbType.Int32;
				colvarRemittanceType.MaxLength = 0;
				colvarRemittanceType.AutoIncrement = false;
				colvarRemittanceType.IsNullable = false;
				colvarRemittanceType.IsPrimaryKey = false;
				colvarRemittanceType.IsForeignKey = false;
				colvarRemittanceType.IsReadOnly = false;
				
						colvarRemittanceType.DefaultSetting = @"((0))";
				colvarRemittanceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemittanceType);
				
				TableSchema.TableColumn colvarNoTax = new TableSchema.TableColumn(schema);
				colvarNoTax.ColumnName = "no_tax";
				colvarNoTax.DataType = DbType.Boolean;
				colvarNoTax.MaxLength = 0;
				colvarNoTax.AutoIncrement = false;
				colvarNoTax.IsNullable = false;
				colvarNoTax.IsPrimaryKey = false;
				colvarNoTax.IsForeignKey = false;
				colvarNoTax.IsReadOnly = false;
				
						colvarNoTax.DefaultSetting = @"((0))";
				colvarNoTax.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNoTax);
				
				TableSchema.TableColumn colvarDealType1 = new TableSchema.TableColumn(schema);
				colvarDealType1.ColumnName = "deal_type1";
				colvarDealType1.DataType = DbType.Int32;
				colvarDealType1.MaxLength = 0;
				colvarDealType1.AutoIncrement = false;
				colvarDealType1.IsNullable = true;
				colvarDealType1.IsPrimaryKey = false;
				colvarDealType1.IsForeignKey = false;
				colvarDealType1.IsReadOnly = false;
				colvarDealType1.DefaultSetting = @"";
				colvarDealType1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealType1);
				
				TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
				colvarDealName.ColumnName = "deal_name";
				colvarDealName.DataType = DbType.String;
				colvarDealName.MaxLength = 100;
				colvarDealName.AutoIncrement = false;
				colvarDealName.IsNullable = true;
				colvarDealName.IsPrimaryKey = false;
				colvarDealName.IsForeignKey = false;
				colvarDealName.IsReadOnly = false;
				colvarDealName.DefaultSetting = @"";
				colvarDealName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealName);
				
				TableSchema.TableColumn colvarDealSource = new TableSchema.TableColumn(schema);
				colvarDealSource.ColumnName = "deal_source";
				colvarDealSource.DataType = DbType.Int32;
				colvarDealSource.MaxLength = 0;
				colvarDealSource.AutoIncrement = false;
				colvarDealSource.IsNullable = true;
				colvarDealSource.IsPrimaryKey = false;
				colvarDealSource.IsForeignKey = false;
				colvarDealSource.IsReadOnly = false;
				colvarDealSource.DefaultSetting = @"";
				colvarDealSource.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealSource);
				
				TableSchema.TableColumn colvarProposalSourceType = new TableSchema.TableColumn(schema);
				colvarProposalSourceType.ColumnName = "proposal_source_type";
				colvarProposalSourceType.DataType = DbType.Int32;
				colvarProposalSourceType.MaxLength = 0;
				colvarProposalSourceType.AutoIncrement = false;
				colvarProposalSourceType.IsNullable = false;
				colvarProposalSourceType.IsPrimaryKey = false;
				colvarProposalSourceType.IsForeignKey = false;
				colvarProposalSourceType.IsReadOnly = false;
				
						colvarProposalSourceType.DefaultSetting = @"((0))";
				colvarProposalSourceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProposalSourceType);
				
				TableSchema.TableColumn colvarDealType2 = new TableSchema.TableColumn(schema);
				colvarDealType2.ColumnName = "deal_type_2";
				colvarDealType2.DataType = DbType.Int32;
				colvarDealType2.MaxLength = 0;
				colvarDealType2.AutoIncrement = false;
				colvarDealType2.IsNullable = true;
				colvarDealType2.IsPrimaryKey = false;
				colvarDealType2.IsForeignKey = false;
				colvarDealType2.IsReadOnly = false;
				colvarDealType2.DefaultSetting = @"";
				colvarDealType2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealType2);
				
				TableSchema.TableColumn colvarOrderTimeE = new TableSchema.TableColumn(schema);
				colvarOrderTimeE.ColumnName = "order_time_e";
				colvarOrderTimeE.DataType = DbType.DateTime;
				colvarOrderTimeE.MaxLength = 0;
				colvarOrderTimeE.AutoIncrement = false;
				colvarOrderTimeE.IsNullable = true;
				colvarOrderTimeE.IsPrimaryKey = false;
				colvarOrderTimeE.IsForeignKey = false;
				colvarOrderTimeE.IsReadOnly = false;
				colvarOrderTimeE.DefaultSetting = @"";
				colvarOrderTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderTimeE);
				
				TableSchema.TableColumn colvarDeliveryTimeS = new TableSchema.TableColumn(schema);
				colvarDeliveryTimeS.ColumnName = "delivery_time_s";
				colvarDeliveryTimeS.DataType = DbType.DateTime;
				colvarDeliveryTimeS.MaxLength = 0;
				colvarDeliveryTimeS.AutoIncrement = false;
				colvarDeliveryTimeS.IsNullable = true;
				colvarDeliveryTimeS.IsPrimaryKey = false;
				colvarDeliveryTimeS.IsForeignKey = false;
				colvarDeliveryTimeS.IsReadOnly = false;
				colvarDeliveryTimeS.DefaultSetting = @"";
				colvarDeliveryTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryTimeS);
				
				TableSchema.TableColumn colvarDeliveryTimeE = new TableSchema.TableColumn(schema);
				colvarDeliveryTimeE.ColumnName = "delivery_time_e";
				colvarDeliveryTimeE.DataType = DbType.DateTime;
				colvarDeliveryTimeE.MaxLength = 0;
				colvarDeliveryTimeE.AutoIncrement = false;
				colvarDeliveryTimeE.IsNullable = true;
				colvarDeliveryTimeE.IsPrimaryKey = false;
				colvarDeliveryTimeE.IsForeignKey = false;
				colvarDeliveryTimeE.IsReadOnly = false;
				colvarDeliveryTimeE.DefaultSetting = @"";
				colvarDeliveryTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryTimeE);
				
				TableSchema.TableColumn colvarFlowCompleteTime = new TableSchema.TableColumn(schema);
				colvarFlowCompleteTime.ColumnName = "flow_complete_time";
				colvarFlowCompleteTime.DataType = DbType.DateTime;
				colvarFlowCompleteTime.MaxLength = 0;
				colvarFlowCompleteTime.AutoIncrement = false;
				colvarFlowCompleteTime.IsNullable = true;
				colvarFlowCompleteTime.IsPrimaryKey = false;
				colvarFlowCompleteTime.IsForeignKey = false;
				colvarFlowCompleteTime.IsReadOnly = false;
				colvarFlowCompleteTime.DefaultSetting = @"";
				colvarFlowCompleteTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFlowCompleteTime);
				
				TableSchema.TableColumn colvarCchannel = new TableSchema.TableColumn(schema);
				colvarCchannel.ColumnName = "cchannel";
				colvarCchannel.DataType = DbType.Boolean;
				colvarCchannel.MaxLength = 0;
				colvarCchannel.AutoIncrement = false;
				colvarCchannel.IsNullable = false;
				colvarCchannel.IsPrimaryKey = false;
				colvarCchannel.IsForeignKey = false;
				colvarCchannel.IsReadOnly = false;
				
						colvarCchannel.DefaultSetting = @"((0))";
				colvarCchannel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCchannel);
				
				TableSchema.TableColumn colvarCchannelLink = new TableSchema.TableColumn(schema);
				colvarCchannelLink.ColumnName = "cchannel_link";
				colvarCchannelLink.DataType = DbType.AnsiString;
				colvarCchannelLink.MaxLength = 255;
				colvarCchannelLink.AutoIncrement = false;
				colvarCchannelLink.IsNullable = true;
				colvarCchannelLink.IsPrimaryKey = false;
				colvarCchannelLink.IsForeignKey = false;
				colvarCchannelLink.IsReadOnly = false;
				colvarCchannelLink.DefaultSetting = @"";
				colvarCchannelLink.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCchannelLink);
				
				TableSchema.TableColumn colvarNoTaxNotification = new TableSchema.TableColumn(schema);
				colvarNoTaxNotification.ColumnName = "no_tax_notification";
				colvarNoTaxNotification.DataType = DbType.Boolean;
				colvarNoTaxNotification.MaxLength = 0;
				colvarNoTaxNotification.AutoIncrement = false;
				colvarNoTaxNotification.IsNullable = false;
				colvarNoTaxNotification.IsPrimaryKey = false;
				colvarNoTaxNotification.IsForeignKey = false;
				colvarNoTaxNotification.IsReadOnly = false;
				
						colvarNoTaxNotification.DefaultSetting = @"((0))";
				colvarNoTaxNotification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNoTaxNotification);
				
				TableSchema.TableColumn colvarFlowCompleteTimes = new TableSchema.TableColumn(schema);
				colvarFlowCompleteTimes.ColumnName = "flow_complete_times";
				colvarFlowCompleteTimes.DataType = DbType.Int32;
				colvarFlowCompleteTimes.MaxLength = 0;
				colvarFlowCompleteTimes.AutoIncrement = false;
				colvarFlowCompleteTimes.IsNullable = false;
				colvarFlowCompleteTimes.IsPrimaryKey = false;
				colvarFlowCompleteTimes.IsForeignKey = false;
				colvarFlowCompleteTimes.IsReadOnly = false;
				
						colvarFlowCompleteTimes.DefaultSetting = @"((0))";
				colvarFlowCompleteTimes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFlowCompleteTimes);
				
				TableSchema.TableColumn colvarDealTypeDetail = new TableSchema.TableColumn(schema);
				colvarDealTypeDetail.ColumnName = "deal_type_detail";
				colvarDealTypeDetail.DataType = DbType.Int32;
				colvarDealTypeDetail.MaxLength = 0;
				colvarDealTypeDetail.AutoIncrement = false;
				colvarDealTypeDetail.IsNullable = true;
				colvarDealTypeDetail.IsPrimaryKey = false;
				colvarDealTypeDetail.IsForeignKey = false;
				colvarDealTypeDetail.IsReadOnly = false;
				colvarDealTypeDetail.DefaultSetting = @"";
				colvarDealTypeDetail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealTypeDetail);
				
				TableSchema.TableColumn colvarNoAtm = new TableSchema.TableColumn(schema);
				colvarNoAtm.ColumnName = "no_atm";
				colvarNoAtm.DataType = DbType.Boolean;
				colvarNoAtm.MaxLength = 0;
				colvarNoAtm.AutoIncrement = false;
				colvarNoAtm.IsNullable = false;
				colvarNoAtm.IsPrimaryKey = false;
				colvarNoAtm.IsForeignKey = false;
				colvarNoAtm.IsReadOnly = false;
				
						colvarNoAtm.DefaultSetting = @"((0))";
				colvarNoAtm.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNoAtm);
				
				TableSchema.TableColumn colvarIsGame = new TableSchema.TableColumn(schema);
				colvarIsGame.ColumnName = "is_game";
				colvarIsGame.DataType = DbType.Boolean;
				colvarIsGame.MaxLength = 0;
				colvarIsGame.AutoIncrement = false;
				colvarIsGame.IsNullable = false;
				colvarIsGame.IsPrimaryKey = false;
				colvarIsGame.IsForeignKey = false;
				colvarIsGame.IsReadOnly = false;
				
						colvarIsGame.DefaultSetting = @"('0')";
				colvarIsGame.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsGame);
				
				TableSchema.TableColumn colvarIsWms = new TableSchema.TableColumn(schema);
				colvarIsWms.ColumnName = "is_wms";
				colvarIsWms.DataType = DbType.Boolean;
				colvarIsWms.MaxLength = 0;
				colvarIsWms.AutoIncrement = false;
				colvarIsWms.IsNullable = false;
				colvarIsWms.IsPrimaryKey = false;
				colvarIsWms.IsForeignKey = false;
				colvarIsWms.IsReadOnly = false;
				
						colvarIsWms.DefaultSetting = @"((0))";
				colvarIsWms.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsWms);
				
				TableSchema.TableColumn colvarAgentChannels = new TableSchema.TableColumn(schema);
				colvarAgentChannels.ColumnName = "agent_channels";
				colvarAgentChannels.DataType = DbType.AnsiString;
				colvarAgentChannels.MaxLength = 50;
				colvarAgentChannels.AutoIncrement = false;
				colvarAgentChannels.IsNullable = false;
				colvarAgentChannels.IsPrimaryKey = false;
				colvarAgentChannels.IsForeignKey = false;
				colvarAgentChannels.IsReadOnly = false;
				
						colvarAgentChannels.DefaultSetting = @"('')";
				colvarAgentChannels.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAgentChannels);
				
				TableSchema.TableColumn colvarIsChannelGift = new TableSchema.TableColumn(schema);
				colvarIsChannelGift.ColumnName = "is_channel_gift";
				colvarIsChannelGift.DataType = DbType.Boolean;
				colvarIsChannelGift.MaxLength = 0;
				colvarIsChannelGift.AutoIncrement = false;
				colvarIsChannelGift.IsNullable = false;
				colvarIsChannelGift.IsPrimaryKey = false;
				colvarIsChannelGift.IsForeignKey = false;
				colvarIsChannelGift.IsReadOnly = false;
				
						colvarIsChannelGift.DefaultSetting = @"((0))";
				colvarIsChannelGift.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsChannelGift);

                TableSchema.TableColumn colvarIsTaishinChosen = new TableSchema.TableColumn(schema);
                colvarIsTaishinChosen.ColumnName = "is_taishin_chosen";
                colvarIsTaishinChosen.DataType = DbType.Boolean;
                colvarIsTaishinChosen.MaxLength = 0;
                colvarIsTaishinChosen.AutoIncrement = false;
                colvarIsTaishinChosen.IsNullable = false;
                colvarIsTaishinChosen.IsPrimaryKey = false;
                colvarIsTaishinChosen.IsForeignKey = false;
                colvarIsTaishinChosen.IsReadOnly = false;
                colvarIsTaishinChosen.DefaultSetting = @"((0))";
                colvarIsTaishinChosen.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsTaishinChosen);

                BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("proposal",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid? BusinessHourGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("ReferrerBusinessHourGuid")]
		[Bindable(true)]
		public Guid? ReferrerBusinessHourGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.ReferrerBusinessHourGuid); }
			set { SetColumnValue(Columns.ReferrerBusinessHourGuid, value); }
		}
		  
		[XmlAttribute("CopyType")]
		[Bindable(true)]
		public int CopyType 
		{
			get { return GetColumnValue<int>(Columns.CopyType); }
			set { SetColumnValue(Columns.CopyType, value); }
		}
		  
		[XmlAttribute("DevelopeSalesId")]
		[Bindable(true)]
		public int DevelopeSalesId 
		{
			get { return GetColumnValue<int>(Columns.DevelopeSalesId); }
			set { SetColumnValue(Columns.DevelopeSalesId, value); }
		}
		  
		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int DeliveryType 
		{
			get { return GetColumnValue<int>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}
		  
		[XmlAttribute("DealType")]
		[Bindable(true)]
		public int DealType 
		{
			get { return GetColumnValue<int>(Columns.DealType); }
			set { SetColumnValue(Columns.DealType, value); }
		}
		  
		[XmlAttribute("DealSubType")]
		[Bindable(true)]
		public int DealSubType 
		{
			get { return GetColumnValue<int>(Columns.DealSubType); }
			set { SetColumnValue(Columns.DealSubType, value); }
		}
		  
		[XmlAttribute("ApplyFlag")]
		[Bindable(true)]
		public int ApplyFlag 
		{
			get { return GetColumnValue<int>(Columns.ApplyFlag); }
			set { SetColumnValue(Columns.ApplyFlag, value); }
		}
		  
		[XmlAttribute("ApproveFlag")]
		[Bindable(true)]
		public int ApproveFlag 
		{
			get { return GetColumnValue<int>(Columns.ApproveFlag); }
			set { SetColumnValue(Columns.ApproveFlag, value); }
		}
		  
		[XmlAttribute("BusinessCreateFlag")]
		[Bindable(true)]
		public int BusinessCreateFlag 
		{
			get { return GetColumnValue<int>(Columns.BusinessCreateFlag); }
			set { SetColumnValue(Columns.BusinessCreateFlag, value); }
		}
		  
		[XmlAttribute("BusinessFlag")]
		[Bindable(true)]
		public int BusinessFlag 
		{
			get { return GetColumnValue<int>(Columns.BusinessFlag); }
			set { SetColumnValue(Columns.BusinessFlag, value); }
		}
		  
		[XmlAttribute("EditFlag")]
		[Bindable(true)]
		public int EditFlag 
		{
			get { return GetColumnValue<int>(Columns.EditFlag); }
			set { SetColumnValue(Columns.EditFlag, value); }
		}
		  
		[XmlAttribute("ListingFlag")]
		[Bindable(true)]
		public int ListingFlag 
		{
			get { return GetColumnValue<int>(Columns.ListingFlag); }
			set { SetColumnValue(Columns.ListingFlag, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("PayType")]
		[Bindable(true)]
		public int PayType 
		{
			get { return GetColumnValue<int>(Columns.PayType); }
			set { SetColumnValue(Columns.PayType, value); }
		}
		  
		[XmlAttribute("ProductSpec")]
		[Bindable(true)]
		public string ProductSpec 
		{
			get { return GetColumnValue<string>(Columns.ProductSpec); }
			set { SetColumnValue(Columns.ProductSpec, value); }
		}
		  
		[XmlAttribute("SpecialFlag")]
		[Bindable(true)]
		public int SpecialFlag 
		{
			get { return GetColumnValue<int>(Columns.SpecialFlag); }
			set { SetColumnValue(Columns.SpecialFlag, value); }
		}
		  
		[XmlAttribute("SpecialFlagText")]
		[Bindable(true)]
		public string SpecialFlagText 
		{
			get { return GetColumnValue<string>(Columns.SpecialFlagText); }
			set { SetColumnValue(Columns.SpecialFlagText, value); }
		}
		  
		[XmlAttribute("MarketAnalysis")]
		[Bindable(true)]
		public string MarketAnalysis 
		{
			get { return GetColumnValue<string>(Columns.MarketAnalysis); }
			set { SetColumnValue(Columns.MarketAnalysis, value); }
		}
		  
		[XmlAttribute("MultiDealsDelete")]
		[Bindable(true)]
		public string MultiDealsDelete 
		{
			get { return GetColumnValue<string>(Columns.MultiDealsDelete); }
			set { SetColumnValue(Columns.MultiDealsDelete, value); }
		}
		  
		[XmlAttribute("OrderTimeS")]
		[Bindable(true)]
		public DateTime? OrderTimeS 
		{
			get { return GetColumnValue<DateTime?>(Columns.OrderTimeS); }
			set { SetColumnValue(Columns.OrderTimeS, value); }
		}
		  
		[XmlAttribute("ScheduleExpect")]
		[Bindable(true)]
		public string ScheduleExpect 
		{
			get { return GetColumnValue<string>(Columns.ScheduleExpect); }
			set { SetColumnValue(Columns.ScheduleExpect, value); }
		}
		  
		[XmlAttribute("StartUseUnit")]
		[Bindable(true)]
		public int StartUseUnit 
		{
			get { return GetColumnValue<int>(Columns.StartUseUnit); }
			set { SetColumnValue(Columns.StartUseUnit, value); }
		}
		  
		[XmlAttribute("StartUseText")]
		[Bindable(true)]
		public string StartUseText 
		{
			get { return GetColumnValue<string>(Columns.StartUseText); }
			set { SetColumnValue(Columns.StartUseText, value); }
		}
		  
		[XmlAttribute("ShipType")]
		[Bindable(true)]
		public int ShipType 
		{
			get { return GetColumnValue<int>(Columns.ShipType); }
			set { SetColumnValue(Columns.ShipType, value); }
		}
		  
		[XmlAttribute("ShipText1")]
		[Bindable(true)]
		public string ShipText1 
		{
			get { return GetColumnValue<string>(Columns.ShipText1); }
			set { SetColumnValue(Columns.ShipText1, value); }
		}
		  
		[XmlAttribute("ShipText2")]
		[Bindable(true)]
		public string ShipText2 
		{
			get { return GetColumnValue<string>(Columns.ShipText2); }
			set { SetColumnValue(Columns.ShipText2, value); }
		}
		  
		[XmlAttribute("ContractMemo")]
		[Bindable(true)]
		public string ContractMemo 
		{
			get { return GetColumnValue<string>(Columns.ContractMemo); }
			set { SetColumnValue(Columns.ContractMemo, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("BrandName")]
		[Bindable(true)]
		public string BrandName 
		{
			get { return GetColumnValue<string>(Columns.BrandName); }
			set { SetColumnValue(Columns.BrandName, value); }
		}
		  
		[XmlAttribute("MarketingResource")]
		[Bindable(true)]
		public string MarketingResource 
		{
			get { return GetColumnValue<string>(Columns.MarketingResource); }
			set { SetColumnValue(Columns.MarketingResource, value); }
		}
		  
		[XmlAttribute("ShipText3")]
		[Bindable(true)]
		public string ShipText3 
		{
			get { return GetColumnValue<string>(Columns.ShipText3); }
			set { SetColumnValue(Columns.ShipText3, value); }
		}
		  
		[XmlAttribute("ShippingdateType")]
		[Bindable(true)]
		public int? ShippingdateType 
		{
			get { return GetColumnValue<int?>(Columns.ShippingdateType); }
			set { SetColumnValue(Columns.ShippingdateType, value); }
		}
		  
		[XmlAttribute("BusinessPrice")]
		[Bindable(true)]
		public int? BusinessPrice 
		{
			get { return GetColumnValue<int?>(Columns.BusinessPrice); }
			set { SetColumnValue(Columns.BusinessPrice, value); }
		}
		  
		[XmlAttribute("ConsumerTime")]
		[Bindable(true)]
		public int ConsumerTime 
		{
			get { return GetColumnValue<int>(Columns.ConsumerTime); }
			set { SetColumnValue(Columns.ConsumerTime, value); }
		}
		  
		[XmlAttribute("PhotographerAppointFlag")]
		[Bindable(true)]
		public int PhotographerAppointFlag 
		{
			get { return GetColumnValue<int>(Columns.PhotographerAppointFlag); }
			set { SetColumnValue(Columns.PhotographerAppointFlag, value); }
		}
		  
		[XmlAttribute("SpecialAppointFlagText")]
		[Bindable(true)]
		public string SpecialAppointFlagText 
		{
			get { return GetColumnValue<string>(Columns.SpecialAppointFlagText); }
			set { SetColumnValue(Columns.SpecialAppointFlagText, value); }
		}
		  
		[XmlAttribute("EditPassFlag")]
		[Bindable(true)]
		public int EditPassFlag 
		{
			get { return GetColumnValue<int>(Columns.EditPassFlag); }
			set { SetColumnValue(Columns.EditPassFlag, value); }
		}
		  
		[XmlAttribute("AncestorBusinessHourGuid")]
		[Bindable(true)]
		public Guid? AncestorBusinessHourGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.AncestorBusinessHourGuid); }
			set { SetColumnValue(Columns.AncestorBusinessHourGuid, value); }
		}
		  
		[XmlAttribute("AncestorSequenceBusinessHourGuid")]
		[Bindable(true)]
		public Guid? AncestorSequenceBusinessHourGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.AncestorSequenceBusinessHourGuid); }
			set { SetColumnValue(Columns.AncestorSequenceBusinessHourGuid, value); }
		}
		  
		[XmlAttribute("StartUseText2")]
		[Bindable(true)]
		public string StartUseText2 
		{
			get { return GetColumnValue<string>(Columns.StartUseText2); }
			set { SetColumnValue(Columns.StartUseText2, value); }
		}
		  
		[XmlAttribute("SaleMarketAnalysis")]
		[Bindable(true)]
		public string SaleMarketAnalysis 
		{
			get { return GetColumnValue<string>(Columns.SaleMarketAnalysis); }
			set { SetColumnValue(Columns.SaleMarketAnalysis, value); }
		}
		  
		[XmlAttribute("MediaReportFlag")]
		[Bindable(true)]
		public int MediaReportFlag 
		{
			get { return GetColumnValue<int>(Columns.MediaReportFlag); }
			set { SetColumnValue(Columns.MediaReportFlag, value); }
		}
		  
		[XmlAttribute("MediaReportFlagText")]
		[Bindable(true)]
		public string MediaReportFlagText 
		{
			get { return GetColumnValue<string>(Columns.MediaReportFlagText); }
			set { SetColumnValue(Columns.MediaReportFlagText, value); }
		}
		  
		[XmlAttribute("DealCharacterFlag")]
		[Bindable(true)]
		public int DealCharacterFlag 
		{
			get { return GetColumnValue<int>(Columns.DealCharacterFlag); }
			set { SetColumnValue(Columns.DealCharacterFlag, value); }
		}
		  
		[XmlAttribute("DealCharacterFlagText")]
		[Bindable(true)]
		public string DealCharacterFlagText 
		{
			get { return GetColumnValue<string>(Columns.DealCharacterFlagText); }
			set { SetColumnValue(Columns.DealCharacterFlagText, value); }
		}
		  
		[XmlAttribute("PicAlt")]
		[Bindable(true)]
		public string PicAlt 
		{
			get { return GetColumnValue<string>(Columns.PicAlt); }
			set { SetColumnValue(Columns.PicAlt, value); }
		}
		  
		[XmlAttribute("VendorReceiptType")]
		[Bindable(true)]
		public int? VendorReceiptType 
		{
			get { return GetColumnValue<int?>(Columns.VendorReceiptType); }
			set { SetColumnValue(Columns.VendorReceiptType, value); }
		}
		  
		[XmlAttribute("Othermessage")]
		[Bindable(true)]
		public string Othermessage 
		{
			get { return GetColumnValue<string>(Columns.Othermessage); }
			set { SetColumnValue(Columns.Othermessage, value); }
		}
		  
		[XmlAttribute("DeliveryMethod")]
		[Bindable(true)]
		public int? DeliveryMethod 
		{
			get { return GetColumnValue<int?>(Columns.DeliveryMethod); }
			set { SetColumnValue(Columns.DeliveryMethod, value); }
		}
		  
		[XmlAttribute("SellerProposalFlag")]
		[Bindable(true)]
		public int SellerProposalFlag 
		{
			get { return GetColumnValue<int>(Columns.SellerProposalFlag); }
			set { SetColumnValue(Columns.SellerProposalFlag, value); }
		}
		  
		[XmlAttribute("ProposalCreatedType")]
		[Bindable(true)]
		public short ProposalCreatedType 
		{
			get { return GetColumnValue<short>(Columns.ProposalCreatedType); }
			set { SetColumnValue(Columns.ProposalCreatedType, value); }
		}
		  
		[XmlAttribute("TrialPeriod")]
		[Bindable(true)]
		public bool? TrialPeriod 
		{
			get { return GetColumnValue<bool?>(Columns.TrialPeriod); }
			set { SetColumnValue(Columns.TrialPeriod, value); }
		}
		  
		[XmlAttribute("Mohist")]
		[Bindable(true)]
		public bool? Mohist 
		{
			get { return GetColumnValue<bool?>(Columns.Mohist); }
			set { SetColumnValue(Columns.Mohist, value); }
		}
		  
		[XmlAttribute("ShipOther")]
		[Bindable(true)]
		public string ShipOther 
		{
			get { return GetColumnValue<string>(Columns.ShipOther); }
			set { SetColumnValue(Columns.ShipOther, value); }
		}
		  
		[XmlAttribute("PassSeller")]
		[Bindable(true)]
		public int PassSeller 
		{
			get { return GetColumnValue<int>(Columns.PassSeller); }
			set { SetColumnValue(Columns.PassSeller, value); }
		}
		  
		[XmlAttribute("SendSeller")]
		[Bindable(true)]
		public int SendSeller 
		{
			get { return GetColumnValue<int>(Columns.SendSeller); }
			set { SetColumnValue(Columns.SendSeller, value); }
		}
		  
		[XmlAttribute("FilesDone")]
		[Bindable(true)]
		public bool FilesDone 
		{
			get { return GetColumnValue<bool>(Columns.FilesDone); }
			set { SetColumnValue(Columns.FilesDone, value); }
		}
		  
		[XmlAttribute("GroupCouponDealType")]
		[Bindable(true)]
		public int? GroupCouponDealType 
		{
			get { return GetColumnValue<int?>(Columns.GroupCouponDealType); }
			set { SetColumnValue(Columns.GroupCouponDealType, value); }
		}
		  
		[XmlAttribute("IsEarlierPageCheck")]
		[Bindable(true)]
		public bool IsEarlierPageCheck 
		{
			get { return GetColumnValue<bool>(Columns.IsEarlierPageCheck); }
			set { SetColumnValue(Columns.IsEarlierPageCheck, value); }
		}
		  
		[XmlAttribute("EarlierPageCheckDay")]
		[Bindable(true)]
		public int? EarlierPageCheckDay 
		{
			get { return GetColumnValue<int?>(Columns.EarlierPageCheckDay); }
			set { SetColumnValue(Columns.EarlierPageCheckDay, value); }
		}
		  
		[XmlAttribute("Consignment")]
		[Bindable(true)]
		public bool Consignment 
		{
			get { return GetColumnValue<bool>(Columns.Consignment); }
			set { SetColumnValue(Columns.Consignment, value); }
		}
		  
		[XmlAttribute("SystemPic")]
		[Bindable(true)]
		public bool SystemPic 
		{
			get { return GetColumnValue<bool>(Columns.SystemPic); }
			set { SetColumnValue(Columns.SystemPic, value); }
		}
		  
		[XmlAttribute("OperationSalesId")]
		[Bindable(true)]
		public int? OperationSalesId 
		{
			get { return GetColumnValue<int?>(Columns.OperationSalesId); }
			set { SetColumnValue(Columns.OperationSalesId, value); }
		}
		  
		[XmlAttribute("SpecialPrice")]
		[Bindable(true)]
		public bool SpecialPrice 
		{
			get { return GetColumnValue<bool>(Columns.SpecialPrice); }
			set { SetColumnValue(Columns.SpecialPrice, value); }
		}
		  
		[XmlAttribute("IsBankDeal")]
		[Bindable(true)]
		public bool IsBankDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsBankDeal); }
			set { SetColumnValue(Columns.IsBankDeal, value); }
		}
		  
		[XmlAttribute("IsPromotionDeal")]
		[Bindable(true)]
		public bool IsPromotionDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsPromotionDeal); }
			set { SetColumnValue(Columns.IsPromotionDeal, value); }
		}
		  
		[XmlAttribute("IsExhibitionDeal")]
		[Bindable(true)]
		public bool IsExhibitionDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsExhibitionDeal); }
			set { SetColumnValue(Columns.IsExhibitionDeal, value); }
		}
		  
		[XmlAttribute("NoDiscountShown")]
		[Bindable(true)]
		public bool NoDiscountShown 
		{
			get { return GetColumnValue<bool>(Columns.NoDiscountShown); }
			set { SetColumnValue(Columns.NoDiscountShown, value); }
		}
		  
		[XmlAttribute("DeliveryIslands")]
		[Bindable(true)]
		public bool DeliveryIslands 
		{
			get { return GetColumnValue<bool>(Columns.DeliveryIslands); }
			set { SetColumnValue(Columns.DeliveryIslands, value); }
		}
		  
		[XmlAttribute("IsProduction")]
		[Bindable(true)]
		public bool IsProduction 
		{
			get { return GetColumnValue<bool>(Columns.IsProduction); }
			set { SetColumnValue(Columns.IsProduction, value); }
		}
		  
		[XmlAttribute("ProductionDescription")]
		[Bindable(true)]
		public string ProductionDescription 
		{
			get { return GetColumnValue<string>(Columns.ProductionDescription); }
			set { SetColumnValue(Columns.ProductionDescription, value); }
		}
		  
		[XmlAttribute("RemittanceType")]
		[Bindable(true)]
		public int RemittanceType 
		{
			get { return GetColumnValue<int>(Columns.RemittanceType); }
			set { SetColumnValue(Columns.RemittanceType, value); }
		}
		  
		[XmlAttribute("NoTax")]
		[Bindable(true)]
		public bool NoTax 
		{
			get { return GetColumnValue<bool>(Columns.NoTax); }
			set { SetColumnValue(Columns.NoTax, value); }
		}
		  
		[XmlAttribute("DealType1")]
		[Bindable(true)]
		public int? DealType1 
		{
			get { return GetColumnValue<int?>(Columns.DealType1); }
			set { SetColumnValue(Columns.DealType1, value); }
		}
		  
		[XmlAttribute("DealName")]
		[Bindable(true)]
		public string DealName 
		{
			get { return GetColumnValue<string>(Columns.DealName); }
			set { SetColumnValue(Columns.DealName, value); }
		}
		  
		[XmlAttribute("DealSource")]
		[Bindable(true)]
		public int? DealSource 
		{
			get { return GetColumnValue<int?>(Columns.DealSource); }
			set { SetColumnValue(Columns.DealSource, value); }
		}
		  
		[XmlAttribute("ProposalSourceType")]
		[Bindable(true)]
		public int ProposalSourceType 
		{
			get { return GetColumnValue<int>(Columns.ProposalSourceType); }
			set { SetColumnValue(Columns.ProposalSourceType, value); }
		}
		  
		[XmlAttribute("DealType2")]
		[Bindable(true)]
		public int? DealType2 
		{
			get { return GetColumnValue<int?>(Columns.DealType2); }
			set { SetColumnValue(Columns.DealType2, value); }
		}
		  
		[XmlAttribute("OrderTimeE")]
		[Bindable(true)]
		public DateTime? OrderTimeE 
		{
			get { return GetColumnValue<DateTime?>(Columns.OrderTimeE); }
			set { SetColumnValue(Columns.OrderTimeE, value); }
		}
		  
		[XmlAttribute("DeliveryTimeS")]
		[Bindable(true)]
		public DateTime? DeliveryTimeS 
		{
			get { return GetColumnValue<DateTime?>(Columns.DeliveryTimeS); }
			set { SetColumnValue(Columns.DeliveryTimeS, value); }
		}
		  
		[XmlAttribute("DeliveryTimeE")]
		[Bindable(true)]
		public DateTime? DeliveryTimeE 
		{
			get { return GetColumnValue<DateTime?>(Columns.DeliveryTimeE); }
			set { SetColumnValue(Columns.DeliveryTimeE, value); }
		}
		  
		[XmlAttribute("FlowCompleteTime")]
		[Bindable(true)]
		public DateTime? FlowCompleteTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.FlowCompleteTime); }
			set { SetColumnValue(Columns.FlowCompleteTime, value); }
		}
		  
		[XmlAttribute("Cchannel")]
		[Bindable(true)]
		public bool Cchannel 
		{
			get { return GetColumnValue<bool>(Columns.Cchannel); }
			set { SetColumnValue(Columns.Cchannel, value); }
		}
		  
		[XmlAttribute("CchannelLink")]
		[Bindable(true)]
		public string CchannelLink 
		{
			get { return GetColumnValue<string>(Columns.CchannelLink); }
			set { SetColumnValue(Columns.CchannelLink, value); }
		}
		  
		[XmlAttribute("NoTaxNotification")]
		[Bindable(true)]
		public bool NoTaxNotification 
		{
			get { return GetColumnValue<bool>(Columns.NoTaxNotification); }
			set { SetColumnValue(Columns.NoTaxNotification, value); }
		}
		  
		[XmlAttribute("FlowCompleteTimes")]
		[Bindable(true)]
		public int FlowCompleteTimes 
		{
			get { return GetColumnValue<int>(Columns.FlowCompleteTimes); }
			set { SetColumnValue(Columns.FlowCompleteTimes, value); }
		}
		  
		[XmlAttribute("DealTypeDetail")]
		[Bindable(true)]
		public int? DealTypeDetail 
		{
			get { return GetColumnValue<int?>(Columns.DealTypeDetail); }
			set { SetColumnValue(Columns.DealTypeDetail, value); }
		}
		  
		[XmlAttribute("NoAtm")]
		[Bindable(true)]
		public bool NoAtm 
		{
			get { return GetColumnValue<bool>(Columns.NoAtm); }
			set { SetColumnValue(Columns.NoAtm, value); }
		}
		  
		[XmlAttribute("IsGame")]
		[Bindable(true)]
		public bool IsGame 
		{
			get { return GetColumnValue<bool>(Columns.IsGame); }
			set { SetColumnValue(Columns.IsGame, value); }
		}
		  
		[XmlAttribute("IsWms")]
		[Bindable(true)]
		public bool IsWms 
		{
			get { return GetColumnValue<bool>(Columns.IsWms); }
			set { SetColumnValue(Columns.IsWms, value); }
		}
		  
		[XmlAttribute("AgentChannels")]
		[Bindable(true)]
		public string AgentChannels 
		{
			get { return GetColumnValue<string>(Columns.AgentChannels); }
			set { SetColumnValue(Columns.AgentChannels, value); }
		}
		  
		[XmlAttribute("IsChannelGift")]
		[Bindable(true)]
		public bool IsChannelGift 
		{
			get { return GetColumnValue<bool>(Columns.IsChannelGift); }
			set { SetColumnValue(Columns.IsChannelGift, value); }
        }

        [XmlAttribute("IsTaishinChosen")]
        [Bindable(true)]
        public bool IsTaishinChosen
        {
            get { return GetColumnValue<bool>(Columns.IsTaishinChosen); }
            set { SetColumnValue(Columns.IsTaishinChosen, value); }
        }


        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferrerBusinessHourGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CopyTypeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DevelopeSalesIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryTypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn DealTypeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn DealSubTypeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ApplyFlagColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ApproveFlagColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessCreateFlagColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessFlagColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn EditFlagColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ListingFlagColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn PayTypeColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductSpecColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialFlagColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialFlagTextColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn MarketAnalysisColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn MultiDealsDeleteColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderTimeSColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn ScheduleExpectColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn StartUseUnitColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn StartUseTextColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn ShipTypeColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn ShipText1Column
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn ShipText2Column
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn ContractMemoColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn BrandNameColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn MarketingResourceColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn ShipText3Column
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn ShippingdateTypeColumn
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessPriceColumn
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        public static TableSchema.TableColumn ConsumerTimeColumn
        {
            get { return Schema.Columns[40]; }
        }
        
        
        
        public static TableSchema.TableColumn PhotographerAppointFlagColumn
        {
            get { return Schema.Columns[41]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialAppointFlagTextColumn
        {
            get { return Schema.Columns[42]; }
        }
        
        
        
        public static TableSchema.TableColumn EditPassFlagColumn
        {
            get { return Schema.Columns[43]; }
        }
        
        
        
        public static TableSchema.TableColumn AncestorBusinessHourGuidColumn
        {
            get { return Schema.Columns[44]; }
        }
        
        
        
        public static TableSchema.TableColumn AncestorSequenceBusinessHourGuidColumn
        {
            get { return Schema.Columns[45]; }
        }
        
        
        
        public static TableSchema.TableColumn StartUseText2Column
        {
            get { return Schema.Columns[46]; }
        }
        
        
        
        public static TableSchema.TableColumn SaleMarketAnalysisColumn
        {
            get { return Schema.Columns[47]; }
        }
        
        
        
        public static TableSchema.TableColumn MediaReportFlagColumn
        {
            get { return Schema.Columns[48]; }
        }
        
        
        
        public static TableSchema.TableColumn MediaReportFlagTextColumn
        {
            get { return Schema.Columns[49]; }
        }
        
        
        
        public static TableSchema.TableColumn DealCharacterFlagColumn
        {
            get { return Schema.Columns[50]; }
        }
        
        
        
        public static TableSchema.TableColumn DealCharacterFlagTextColumn
        {
            get { return Schema.Columns[51]; }
        }
        
        
        
        public static TableSchema.TableColumn PicAltColumn
        {
            get { return Schema.Columns[52]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorReceiptTypeColumn
        {
            get { return Schema.Columns[53]; }
        }
        
        
        
        public static TableSchema.TableColumn OthermessageColumn
        {
            get { return Schema.Columns[54]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryMethodColumn
        {
            get { return Schema.Columns[55]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerProposalFlagColumn
        {
            get { return Schema.Columns[56]; }
        }
        
        
        
        public static TableSchema.TableColumn ProposalCreatedTypeColumn
        {
            get { return Schema.Columns[57]; }
        }
        
        
        
        public static TableSchema.TableColumn TrialPeriodColumn
        {
            get { return Schema.Columns[58]; }
        }
        
        
        
        public static TableSchema.TableColumn MohistColumn
        {
            get { return Schema.Columns[59]; }
        }
        
        
        
        public static TableSchema.TableColumn ShipOtherColumn
        {
            get { return Schema.Columns[60]; }
        }
        
        
        
        public static TableSchema.TableColumn PassSellerColumn
        {
            get { return Schema.Columns[61]; }
        }
        
        
        
        public static TableSchema.TableColumn SendSellerColumn
        {
            get { return Schema.Columns[62]; }
        }
        
        
        
        public static TableSchema.TableColumn FilesDoneColumn
        {
            get { return Schema.Columns[63]; }
        }
        
        
        
        public static TableSchema.TableColumn GroupCouponDealTypeColumn
        {
            get { return Schema.Columns[64]; }
        }
        
        
        
        public static TableSchema.TableColumn IsEarlierPageCheckColumn
        {
            get { return Schema.Columns[65]; }
        }
        
        
        
        public static TableSchema.TableColumn EarlierPageCheckDayColumn
        {
            get { return Schema.Columns[66]; }
        }
        
        
        
        public static TableSchema.TableColumn ConsignmentColumn
        {
            get { return Schema.Columns[67]; }
        }
        
        
        
        public static TableSchema.TableColumn SystemPicColumn
        {
            get { return Schema.Columns[68]; }
        }
        
        
        
        public static TableSchema.TableColumn OperationSalesIdColumn
        {
            get { return Schema.Columns[69]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialPriceColumn
        {
            get { return Schema.Columns[70]; }
        }
        
        
        
        public static TableSchema.TableColumn IsBankDealColumn
        {
            get { return Schema.Columns[71]; }
        }
        
        
        
        public static TableSchema.TableColumn IsPromotionDealColumn
        {
            get { return Schema.Columns[72]; }
        }
        
        
        
        public static TableSchema.TableColumn IsExhibitionDealColumn
        {
            get { return Schema.Columns[73]; }
        }
        
        
        
        public static TableSchema.TableColumn NoDiscountShownColumn
        {
            get { return Schema.Columns[74]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryIslandsColumn
        {
            get { return Schema.Columns[75]; }
        }
        
        
        
        public static TableSchema.TableColumn IsProductionColumn
        {
            get { return Schema.Columns[76]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductionDescriptionColumn
        {
            get { return Schema.Columns[77]; }
        }
        
        
        
        public static TableSchema.TableColumn RemittanceTypeColumn
        {
            get { return Schema.Columns[78]; }
        }
        
        
        
        public static TableSchema.TableColumn NoTaxColumn
        {
            get { return Schema.Columns[79]; }
        }
        
        
        
        public static TableSchema.TableColumn DealType1Column
        {
            get { return Schema.Columns[80]; }
        }
        
        
        
        public static TableSchema.TableColumn DealNameColumn
        {
            get { return Schema.Columns[81]; }
        }
        
        
        
        public static TableSchema.TableColumn DealSourceColumn
        {
            get { return Schema.Columns[82]; }
        }
        
        
        
        public static TableSchema.TableColumn ProposalSourceTypeColumn
        {
            get { return Schema.Columns[83]; }
        }
        
        
        
        public static TableSchema.TableColumn DealType2Column
        {
            get { return Schema.Columns[84]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderTimeEColumn
        {
            get { return Schema.Columns[85]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryTimeSColumn
        {
            get { return Schema.Columns[86]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryTimeEColumn
        {
            get { return Schema.Columns[87]; }
        }
        
        
        
        public static TableSchema.TableColumn FlowCompleteTimeColumn
        {
            get { return Schema.Columns[88]; }
        }
        
        
        
        public static TableSchema.TableColumn CchannelColumn
        {
            get { return Schema.Columns[89]; }
        }
        
        
        
        public static TableSchema.TableColumn CchannelLinkColumn
        {
            get { return Schema.Columns[90]; }
        }
        
        
        
        public static TableSchema.TableColumn NoTaxNotificationColumn
        {
            get { return Schema.Columns[91]; }
        }
        
        
        
        public static TableSchema.TableColumn FlowCompleteTimesColumn
        {
            get { return Schema.Columns[92]; }
        }
        
        
        
        public static TableSchema.TableColumn DealTypeDetailColumn
        {
            get { return Schema.Columns[93]; }
        }
        
        
        
        public static TableSchema.TableColumn NoAtmColumn
        {
            get { return Schema.Columns[94]; }
        }
        
        
        
        public static TableSchema.TableColumn IsGameColumn
        {
            get { return Schema.Columns[95]; }
        }
        
        
        
        public static TableSchema.TableColumn IsWmsColumn
        {
            get { return Schema.Columns[96]; }
        }
        
        
        
        public static TableSchema.TableColumn AgentChannelsColumn
        {
            get { return Schema.Columns[97]; }
        }
        
        
        
        public static TableSchema.TableColumn IsChannelGiftColumn
        {
            get { return Schema.Columns[98]; }
        }

        public static TableSchema.TableColumn IsTaishinChosenColumn
        {
            get { return Schema.Columns[99]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
		{
			 public static string Id = @"id";
			 public static string SellerGuid = @"seller_guid";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string ReferrerBusinessHourGuid = @"referrer_business_hour_guid";
			 public static string CopyType = @"copy_type";
			 public static string DevelopeSalesId = @"develope_sales_id";
			 public static string DeliveryType = @"delivery_type";
			 public static string DealType = @"deal_type";
			 public static string DealSubType = @"deal_sub_type";
			 public static string ApplyFlag = @"apply_flag";
			 public static string ApproveFlag = @"approve_flag";
			 public static string BusinessCreateFlag = @"business_create_flag";
			 public static string BusinessFlag = @"business_flag";
			 public static string EditFlag = @"edit_flag";
			 public static string ListingFlag = @"listing_flag";
			 public static string Status = @"status";
			 public static string PayType = @"pay_type";
			 public static string ProductSpec = @"product_spec";
			 public static string SpecialFlag = @"special_flag";
			 public static string SpecialFlagText = @"special_flag_text";
			 public static string MarketAnalysis = @"market_analysis";
			 public static string MultiDealsDelete = @"multi_deals_delete";
			 public static string OrderTimeS = @"order_time_s";
			 public static string ScheduleExpect = @"schedule_expect";
			 public static string StartUseUnit = @"start_use_unit";
			 public static string StartUseText = @"start_use_text";
			 public static string ShipType = @"ship_type";
			 public static string ShipText1 = @"ship_text1";
			 public static string ShipText2 = @"ship_text2";
			 public static string ContractMemo = @"contract_memo";
			 public static string Memo = @"memo";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string BrandName = @"brand_name";
			 public static string MarketingResource = @"marketing_resource";
			 public static string ShipText3 = @"ship_text3";
			 public static string ShippingdateType = @"shippingdate_type";
			 public static string BusinessPrice = @"business_price";
			 public static string ConsumerTime = @"consumer_time";
			 public static string PhotographerAppointFlag = @"photographer_appoint_flag";
			 public static string SpecialAppointFlagText = @"special_appoint_flag_text";
			 public static string EditPassFlag = @"edit_pass_flag";
			 public static string AncestorBusinessHourGuid = @"ancestor_business_hour_guid";
			 public static string AncestorSequenceBusinessHourGuid = @"ancestor_sequence_business_hour_guid";
			 public static string StartUseText2 = @"start_use_text2";
			 public static string SaleMarketAnalysis = @"sale_market_analysis";
			 public static string MediaReportFlag = @"media_report_flag";
			 public static string MediaReportFlagText = @"media_report_flag_text";
			 public static string DealCharacterFlag = @"deal_character_flag";
			 public static string DealCharacterFlagText = @"deal_character_flag_text";
			 public static string PicAlt = @"pic_alt";
			 public static string VendorReceiptType = @"vendor_receipt_type";
			 public static string Othermessage = @"othermessage";
			 public static string DeliveryMethod = @"delivery_method";
			 public static string SellerProposalFlag = @"seller_proposal_flag";
			 public static string ProposalCreatedType = @"proposal_created_type";
			 public static string TrialPeriod = @"trial_period";
			 public static string Mohist = @"mohist";
			 public static string ShipOther = @"ship_other";
			 public static string PassSeller = @"pass_seller";
			 public static string SendSeller = @"send_seller";
			 public static string FilesDone = @"files_done";
			 public static string GroupCouponDealType = @"group_coupon_deal_type";
			 public static string IsEarlierPageCheck = @"is_earlier_page_check";
			 public static string EarlierPageCheckDay = @"earlier_page_check_day";
			 public static string Consignment = @"consignment";
			 public static string SystemPic = @"system_pic";
			 public static string OperationSalesId = @"operation_sales_id";
			 public static string SpecialPrice = @"special_price";
			 public static string IsBankDeal = @"is_bank_deal";
			 public static string IsPromotionDeal = @"is_promotion_deal";
			 public static string IsExhibitionDeal = @"is_exhibition_deal";
			 public static string NoDiscountShown = @"no_discount_shown";
			 public static string DeliveryIslands = @"delivery_islands";
			 public static string IsProduction = @"is_production";
			 public static string ProductionDescription = @"production_description";
			 public static string RemittanceType = @"remittance_type";
			 public static string NoTax = @"no_tax";
			 public static string DealType1 = @"deal_type1";
			 public static string DealName = @"deal_name";
			 public static string DealSource = @"deal_source";
			 public static string ProposalSourceType = @"proposal_source_type";
			 public static string DealType2 = @"deal_type_2";
			 public static string OrderTimeE = @"order_time_e";
			 public static string DeliveryTimeS = @"delivery_time_s";
			 public static string DeliveryTimeE = @"delivery_time_e";
			 public static string FlowCompleteTime = @"flow_complete_time";
			 public static string Cchannel = @"cchannel";
			 public static string CchannelLink = @"cchannel_link";
			 public static string NoTaxNotification = @"no_tax_notification";
			 public static string FlowCompleteTimes = @"flow_complete_times";
			 public static string DealTypeDetail = @"deal_type_detail";
			 public static string NoAtm = @"no_atm";
			 public static string IsGame = @"is_game";
			 public static string IsWms = @"is_wms";
			 public static string AgentChannels = @"agent_channels";
			 public static string IsChannelGift = @"is_channel_gift";
			 public static string IsTaishinChosen = @"is_taishin_chosen";			

		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

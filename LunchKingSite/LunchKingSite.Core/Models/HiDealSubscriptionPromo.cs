using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealSubscriptionPromo class.
	/// </summary>
    [Serializable]
	public partial class HiDealSubscriptionPromoCollection : RepositoryList<HiDealSubscriptionPromo, HiDealSubscriptionPromoCollection>
	{	   
		public HiDealSubscriptionPromoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealSubscriptionPromoCollection</returns>
		public HiDealSubscriptionPromoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealSubscriptionPromo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_subscription_promo table.
	/// </summary>
	[Serializable]
	public partial class HiDealSubscriptionPromo : RepositoryRecord<HiDealSubscriptionPromo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealSubscriptionPromo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealSubscriptionPromo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_subscription_promo", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarPromoTitle = new TableSchema.TableColumn(schema);
				colvarPromoTitle.ColumnName = "promo_title";
				colvarPromoTitle.DataType = DbType.String;
				colvarPromoTitle.MaxLength = 50;
				colvarPromoTitle.AutoIncrement = false;
				colvarPromoTitle.IsNullable = false;
				colvarPromoTitle.IsPrimaryKey = false;
				colvarPromoTitle.IsForeignKey = false;
				colvarPromoTitle.IsReadOnly = false;
				colvarPromoTitle.DefaultSetting = @"";
				colvarPromoTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPromoTitle);
				
				TableSchema.TableColumn colvarPromoContext = new TableSchema.TableColumn(schema);
				colvarPromoContext.ColumnName = "promo_context";
				colvarPromoContext.DataType = DbType.String;
				colvarPromoContext.MaxLength = -1;
				colvarPromoContext.AutoIncrement = false;
				colvarPromoContext.IsNullable = true;
				colvarPromoContext.IsPrimaryKey = false;
				colvarPromoContext.IsForeignKey = false;
				colvarPromoContext.IsReadOnly = false;
				colvarPromoContext.DefaultSetting = @"";
				colvarPromoContext.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPromoContext);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				
						colvarModifyTime.DefaultSetting = @"(getdate())";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarModifyBy = new TableSchema.TableColumn(schema);
				colvarModifyBy.ColumnName = "modify_by";
				colvarModifyBy.DataType = DbType.String;
				colvarModifyBy.MaxLength = 50;
				colvarModifyBy.AutoIncrement = false;
				colvarModifyBy.IsNullable = true;
				colvarModifyBy.IsPrimaryKey = false;
				colvarModifyBy.IsForeignKey = false;
				colvarModifyBy.IsReadOnly = false;
				colvarModifyBy.DefaultSetting = @"";
				colvarModifyBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyBy);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_subscription_promo",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("PromoTitle")]
		[Bindable(true)]
		public string PromoTitle 
		{
			get { return GetColumnValue<string>(Columns.PromoTitle); }
			set { SetColumnValue(Columns.PromoTitle, value); }
		}
		  
		[XmlAttribute("PromoContext")]
		[Bindable(true)]
		public string PromoContext 
		{
			get { return GetColumnValue<string>(Columns.PromoContext); }
			set { SetColumnValue(Columns.PromoContext, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ModifyBy")]
		[Bindable(true)]
		public string ModifyBy 
		{
			get { return GetColumnValue<string>(Columns.ModifyBy); }
			set { SetColumnValue(Columns.ModifyBy, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PromoTitleColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PromoContextColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyByColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string PromoTitle = @"promo_title";
			 public static string PromoContext = @"promo_context";
			 public static string Status = @"status";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
			 public static string ModifyTime = @"modify_time";
			 public static string ModifyBy = @"modify_by";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

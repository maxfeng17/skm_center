﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProposalSellerFile class.
    /// </summary>
    [Serializable]
    public partial class ProposalSellerFileCollection : RepositoryList<ProposalSellerFile, ProposalSellerFileCollection>
    {
        public ProposalSellerFileCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalSellerFileCollection</returns>
        public ProposalSellerFileCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProposalSellerFile o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the proposal_seller_files table.
    /// </summary>
    [Serializable]
    public partial class ProposalSellerFile : RepositoryRecord<ProposalSellerFile>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProposalSellerFile()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProposalSellerFile(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("proposal_seller_files", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarProposalId = new TableSchema.TableColumn(schema);
                colvarProposalId.ColumnName = "proposal_id";
                colvarProposalId.DataType = DbType.Int32;
                colvarProposalId.MaxLength = 0;
                colvarProposalId.AutoIncrement = false;
                colvarProposalId.IsNullable = false;
                colvarProposalId.IsPrimaryKey = false;
                colvarProposalId.IsForeignKey = false;
                colvarProposalId.IsReadOnly = false;
                colvarProposalId.DefaultSetting = @"";
                colvarProposalId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProposalId);

                TableSchema.TableColumn colvarOriFileName = new TableSchema.TableColumn(schema);
                colvarOriFileName.ColumnName = "ori_file_name";
                colvarOriFileName.DataType = DbType.String;
                colvarOriFileName.MaxLength = 100;
                colvarOriFileName.AutoIncrement = false;
                colvarOriFileName.IsNullable = false;
                colvarOriFileName.IsPrimaryKey = false;
                colvarOriFileName.IsForeignKey = false;
                colvarOriFileName.IsReadOnly = false;
                colvarOriFileName.DefaultSetting = @"";
                colvarOriFileName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOriFileName);

                TableSchema.TableColumn colvarRealFileName = new TableSchema.TableColumn(schema);
                colvarRealFileName.ColumnName = "real_file_name";
                colvarRealFileName.DataType = DbType.String;
                colvarRealFileName.MaxLength = 100;
                colvarRealFileName.AutoIncrement = false;
                colvarRealFileName.IsNullable = false;
                colvarRealFileName.IsPrimaryKey = false;
                colvarRealFileName.IsForeignKey = false;
                colvarRealFileName.IsReadOnly = false;
                colvarRealFileName.DefaultSetting = @"";
                colvarRealFileName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRealFileName);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarFileStatus = new TableSchema.TableColumn(schema);
                colvarFileStatus.ColumnName = "file_status";
                colvarFileStatus.DataType = DbType.Int32;
                colvarFileStatus.MaxLength = 0;
                colvarFileStatus.AutoIncrement = false;
                colvarFileStatus.IsNullable = false;
                colvarFileStatus.IsPrimaryKey = false;
                colvarFileStatus.IsForeignKey = false;
                colvarFileStatus.IsReadOnly = false;
                colvarFileStatus.DefaultSetting = @"";
                colvarFileStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFileStatus);

                TableSchema.TableColumn colvarFileSize = new TableSchema.TableColumn(schema);
                colvarFileSize.ColumnName = "file_size";
                colvarFileSize.DataType = DbType.Int32;
                colvarFileSize.MaxLength = 0;
                colvarFileSize.AutoIncrement = false;
                colvarFileSize.IsNullable = false;
                colvarFileSize.IsPrimaryKey = false;
                colvarFileSize.IsForeignKey = false;
                colvarFileSize.IsReadOnly = false;

                colvarFileSize.DefaultSetting = @"((0))";
                colvarFileSize.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFileSize);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("proposal_seller_files", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("ProposalId")]
        [Bindable(true)]
        public int ProposalId
        {
            get { return GetColumnValue<int>(Columns.ProposalId); }
            set { SetColumnValue(Columns.ProposalId, value); }
        }

        [XmlAttribute("OriFileName")]
        [Bindable(true)]
        public string OriFileName
        {
            get { return GetColumnValue<string>(Columns.OriFileName); }
            set { SetColumnValue(Columns.OriFileName, value); }
        }

        [XmlAttribute("RealFileName")]
        [Bindable(true)]
        public string RealFileName
        {
            get { return GetColumnValue<string>(Columns.RealFileName); }
            set { SetColumnValue(Columns.RealFileName, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("FileStatus")]
        [Bindable(true)]
        public int FileStatus
        {
            get { return GetColumnValue<int>(Columns.FileStatus); }
            set { SetColumnValue(Columns.FileStatus, value); }
        }

        [XmlAttribute("FileSize")]
        [Bindable(true)]
        public int FileSize
        {
            get { return GetColumnValue<int>(Columns.FileSize); }
            set { SetColumnValue(Columns.FileSize, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ProposalIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn OriFileNameColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn RealFileNameColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn FileStatusColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn FileSizeColumn
        {
            get { return Schema.Columns[7]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string ProposalId = @"proposal_id";
            public static string OriFileName = @"ori_file_name";
            public static string RealFileName = @"real_file_name";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string FileStatus = @"file_status";
            public static string FileSize = @"file_size";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMembershipCard class.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardCollection : ReadOnlyList<ViewMembershipCard, ViewMembershipCardCollection>
    {        
        public ViewMembershipCardCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_membership_card view.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCard : ReadOnlyRecord<ViewMembershipCard>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_membership_card", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCardId = new TableSchema.TableColumn(schema);
                colvarCardId.ColumnName = "card_id";
                colvarCardId.DataType = DbType.Int32;
                colvarCardId.MaxLength = 0;
                colvarCardId.AutoIncrement = false;
                colvarCardId.IsNullable = false;
                colvarCardId.IsPrimaryKey = false;
                colvarCardId.IsForeignKey = false;
                colvarCardId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardId);
                
                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = false;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerUserId);
                
                TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
                colvarCardGroupId.ColumnName = "card_group_id";
                colvarCardGroupId.DataType = DbType.Int32;
                colvarCardGroupId.MaxLength = 0;
                colvarCardGroupId.AutoIncrement = false;
                colvarCardGroupId.IsNullable = false;
                colvarCardGroupId.IsPrimaryKey = false;
                colvarCardGroupId.IsForeignKey = false;
                colvarCardGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardGroupId);
                
                TableSchema.TableColumn colvarVersionId = new TableSchema.TableColumn(schema);
                colvarVersionId.ColumnName = "version_id";
                colvarVersionId.DataType = DbType.Int32;
                colvarVersionId.MaxLength = 0;
                colvarVersionId.AutoIncrement = false;
                colvarVersionId.IsNullable = false;
                colvarVersionId.IsPrimaryKey = false;
                colvarVersionId.IsForeignKey = false;
                colvarVersionId.IsReadOnly = false;
                
                schema.Columns.Add(colvarVersionId);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Byte;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
                colvarOpenTime.ColumnName = "open_time";
                colvarOpenTime.DataType = DbType.DateTime;
                colvarOpenTime.MaxLength = 0;
                colvarOpenTime.AutoIncrement = false;
                colvarOpenTime.IsNullable = false;
                colvarOpenTime.IsPrimaryKey = false;
                colvarOpenTime.IsForeignKey = false;
                colvarOpenTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpenTime);
                
                TableSchema.TableColumn colvarCloseTime = new TableSchema.TableColumn(schema);
                colvarCloseTime.ColumnName = "close_time";
                colvarCloseTime.DataType = DbType.DateTime;
                colvarCloseTime.MaxLength = 0;
                colvarCloseTime.AutoIncrement = false;
                colvarCloseTime.IsNullable = false;
                colvarCloseTime.IsPrimaryKey = false;
                colvarCloseTime.IsForeignKey = false;
                colvarCloseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCloseTime);
                
                TableSchema.TableColumn colvarLevel = new TableSchema.TableColumn(schema);
                colvarLevel.ColumnName = "level";
                colvarLevel.DataType = DbType.Int32;
                colvarLevel.MaxLength = 0;
                colvarLevel.AutoIncrement = false;
                colvarLevel.IsNullable = false;
                colvarLevel.IsPrimaryKey = false;
                colvarLevel.IsForeignKey = false;
                colvarLevel.IsReadOnly = false;
                
                schema.Columns.Add(colvarLevel);
                
                TableSchema.TableColumn colvarPaymentPercent = new TableSchema.TableColumn(schema);
                colvarPaymentPercent.ColumnName = "payment_percent";
                colvarPaymentPercent.DataType = DbType.Double;
                colvarPaymentPercent.MaxLength = 0;
                colvarPaymentPercent.AutoIncrement = false;
                colvarPaymentPercent.IsNullable = false;
                colvarPaymentPercent.IsPrimaryKey = false;
                colvarPaymentPercent.IsForeignKey = false;
                colvarPaymentPercent.IsReadOnly = false;
                
                schema.Columns.Add(colvarPaymentPercent);
                
                TableSchema.TableColumn colvarInstruction = new TableSchema.TableColumn(schema);
                colvarInstruction.ColumnName = "instruction";
                colvarInstruction.DataType = DbType.String;
                colvarInstruction.MaxLength = -1;
                colvarInstruction.AutoIncrement = false;
                colvarInstruction.IsNullable = false;
                colvarInstruction.IsPrimaryKey = false;
                colvarInstruction.IsForeignKey = false;
                colvarInstruction.IsReadOnly = false;
                
                schema.Columns.Add(colvarInstruction);
                
                TableSchema.TableColumn colvarOthers = new TableSchema.TableColumn(schema);
                colvarOthers.ColumnName = "others";
                colvarOthers.DataType = DbType.String;
                colvarOthers.MaxLength = -1;
                colvarOthers.AutoIncrement = false;
                colvarOthers.IsNullable = true;
                colvarOthers.IsPrimaryKey = false;
                colvarOthers.IsForeignKey = false;
                colvarOthers.IsReadOnly = false;
                
                schema.Columns.Add(colvarOthers);
                
                TableSchema.TableColumn colvarAvailableDateType = new TableSchema.TableColumn(schema);
                colvarAvailableDateType.ColumnName = "available_date_type";
                colvarAvailableDateType.DataType = DbType.Int32;
                colvarAvailableDateType.MaxLength = 0;
                colvarAvailableDateType.AutoIncrement = false;
                colvarAvailableDateType.IsNullable = false;
                colvarAvailableDateType.IsPrimaryKey = false;
                colvarAvailableDateType.IsForeignKey = false;
                colvarAvailableDateType.IsReadOnly = false;
                
                schema.Columns.Add(colvarAvailableDateType);
                
                TableSchema.TableColumn colvarOrderNeeded = new TableSchema.TableColumn(schema);
                colvarOrderNeeded.ColumnName = "order_needed";
                colvarOrderNeeded.DataType = DbType.Int32;
                colvarOrderNeeded.MaxLength = 0;
                colvarOrderNeeded.AutoIncrement = false;
                colvarOrderNeeded.IsNullable = true;
                colvarOrderNeeded.IsPrimaryKey = false;
                colvarOrderNeeded.IsForeignKey = false;
                colvarOrderNeeded.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderNeeded);
                
                TableSchema.TableColumn colvarAmountNeeded = new TableSchema.TableColumn(schema);
                colvarAmountNeeded.ColumnName = "amount_needed";
                colvarAmountNeeded.DataType = DbType.Currency;
                colvarAmountNeeded.MaxLength = 0;
                colvarAmountNeeded.AutoIncrement = false;
                colvarAmountNeeded.IsNullable = true;
                colvarAmountNeeded.IsPrimaryKey = false;
                colvarAmountNeeded.IsForeignKey = false;
                colvarAmountNeeded.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmountNeeded);
                
                TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
                colvarEnabled.ColumnName = "enabled";
                colvarEnabled.DataType = DbType.Boolean;
                colvarEnabled.MaxLength = 0;
                colvarEnabled.AutoIncrement = false;
                colvarEnabled.IsNullable = false;
                colvarEnabled.IsPrimaryKey = false;
                colvarEnabled.IsForeignKey = false;
                colvarEnabled.IsReadOnly = false;
                
                schema.Columns.Add(colvarEnabled);
                
                TableSchema.TableColumn colvarConditionalLogic = new TableSchema.TableColumn(schema);
                colvarConditionalLogic.ColumnName = "conditional_logic";
                colvarConditionalLogic.DataType = DbType.Int32;
                colvarConditionalLogic.MaxLength = 0;
                colvarConditionalLogic.AutoIncrement = false;
                colvarConditionalLogic.IsNullable = false;
                colvarConditionalLogic.IsPrimaryKey = false;
                colvarConditionalLogic.IsForeignKey = false;
                colvarConditionalLogic.IsReadOnly = false;
                
                schema.Columns.Add(colvarConditionalLogic);
                
                TableSchema.TableColumn colvarCombineUse = new TableSchema.TableColumn(schema);
                colvarCombineUse.ColumnName = "combine_use";
                colvarCombineUse.DataType = DbType.Boolean;
                colvarCombineUse.MaxLength = 0;
                colvarCombineUse.AutoIncrement = false;
                colvarCombineUse.IsNullable = false;
                colvarCombineUse.IsPrimaryKey = false;
                colvarCombineUse.IsForeignKey = false;
                colvarCombineUse.IsReadOnly = false;
                
                schema.Columns.Add(colvarCombineUse);
                
                TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
                colvarImagePath.ColumnName = "image_path";
                colvarImagePath.DataType = DbType.String;
                colvarImagePath.MaxLength = 250;
                colvarImagePath.AutoIncrement = false;
                colvarImagePath.IsNullable = true;
                colvarImagePath.IsPrimaryKey = false;
                colvarImagePath.IsForeignKey = false;
                colvarImagePath.IsReadOnly = false;
                
                schema.Columns.Add(colvarImagePath);
                
                TableSchema.TableColumn colvarBackgroundColor = new TableSchema.TableColumn(schema);
                colvarBackgroundColor.ColumnName = "background_color";
                colvarBackgroundColor.DataType = DbType.String;
                colvarBackgroundColor.MaxLength = 9;
                colvarBackgroundColor.AutoIncrement = false;
                colvarBackgroundColor.IsNullable = true;
                colvarBackgroundColor.IsPrimaryKey = false;
                colvarBackgroundColor.IsForeignKey = false;
                colvarBackgroundColor.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundColor);
                
                TableSchema.TableColumn colvarBackgroundImage = new TableSchema.TableColumn(schema);
                colvarBackgroundImage.ColumnName = "background_image";
                colvarBackgroundImage.DataType = DbType.String;
                colvarBackgroundImage.MaxLength = 250;
                colvarBackgroundImage.AutoIncrement = false;
                colvarBackgroundImage.IsNullable = true;
                colvarBackgroundImage.IsPrimaryKey = false;
                colvarBackgroundImage.IsForeignKey = false;
                colvarBackgroundImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundImage);
                
                TableSchema.TableColumn colvarIconImage = new TableSchema.TableColumn(schema);
                colvarIconImage.ColumnName = "icon_image";
                colvarIconImage.DataType = DbType.String;
                colvarIconImage.MaxLength = 250;
                colvarIconImage.AutoIncrement = false;
                colvarIconImage.IsNullable = true;
                colvarIconImage.IsPrimaryKey = false;
                colvarIconImage.IsForeignKey = false;
                colvarIconImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarIconImage);
                
                TableSchema.TableColumn colvarBackgroundColorId = new TableSchema.TableColumn(schema);
                colvarBackgroundColorId.ColumnName = "background_color_id";
                colvarBackgroundColorId.DataType = DbType.Int32;
                colvarBackgroundColorId.MaxLength = 0;
                colvarBackgroundColorId.AutoIncrement = false;
                colvarBackgroundColorId.IsNullable = false;
                colvarBackgroundColorId.IsPrimaryKey = false;
                colvarBackgroundColorId.IsForeignKey = false;
                colvarBackgroundColorId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundColorId);
                
                TableSchema.TableColumn colvarBackgroundImageId = new TableSchema.TableColumn(schema);
                colvarBackgroundImageId.ColumnName = "background_image_id";
                colvarBackgroundImageId.DataType = DbType.Int32;
                colvarBackgroundImageId.MaxLength = 0;
                colvarBackgroundImageId.AutoIncrement = false;
                colvarBackgroundImageId.IsNullable = false;
                colvarBackgroundImageId.IsPrimaryKey = false;
                colvarBackgroundImageId.IsForeignKey = false;
                colvarBackgroundImageId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundImageId);
                
                TableSchema.TableColumn colvarIconImageId = new TableSchema.TableColumn(schema);
                colvarIconImageId.ColumnName = "icon_image_id";
                colvarIconImageId.DataType = DbType.Int32;
                colvarIconImageId.MaxLength = 0;
                colvarIconImageId.AutoIncrement = false;
                colvarIconImageId.IsNullable = false;
                colvarIconImageId.IsPrimaryKey = false;
                colvarIconImageId.IsForeignKey = false;
                colvarIconImageId.IsReadOnly = false;
                
                schema.Columns.Add(colvarIconImageId);
                
                TableSchema.TableColumn colvarContractValidDate = new TableSchema.TableColumn(schema);
                colvarContractValidDate.ColumnName = "contract_valid_date";
                colvarContractValidDate.DataType = DbType.DateTime;
                colvarContractValidDate.MaxLength = 0;
                colvarContractValidDate.AutoIncrement = false;
                colvarContractValidDate.IsNullable = true;
                colvarContractValidDate.IsPrimaryKey = false;
                colvarContractValidDate.IsForeignKey = false;
                colvarContractValidDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarContractValidDate);
                
                TableSchema.TableColumn colvarReleaseCardAmount = new TableSchema.TableColumn(schema);
                colvarReleaseCardAmount.ColumnName = "release_card_amount";
                colvarReleaseCardAmount.DataType = DbType.Int32;
                colvarReleaseCardAmount.MaxLength = 0;
                colvarReleaseCardAmount.AutoIncrement = false;
                colvarReleaseCardAmount.IsNullable = true;
                colvarReleaseCardAmount.IsPrimaryKey = false;
                colvarReleaseCardAmount.IsForeignKey = false;
                colvarReleaseCardAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarReleaseCardAmount);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_membership_card",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMembershipCard()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMembershipCard(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMembershipCard(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMembershipCard(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CardId")]
        [Bindable(true)]
        public int CardId 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_id");
		    }
            set 
		    {
			    SetColumnValue("card_id", value);
            }
        }
	      
        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int SellerUserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_user_id");
		    }
            set 
		    {
			    SetColumnValue("seller_user_id", value);
            }
        }
	      
        [XmlAttribute("CardGroupId")]
        [Bindable(true)]
        public int CardGroupId 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_group_id");
		    }
            set 
		    {
			    SetColumnValue("card_group_id", value);
            }
        }
	      
        [XmlAttribute("VersionId")]
        [Bindable(true)]
        public int VersionId 
	    {
		    get
		    {
			    return GetColumnValue<int>("version_id");
		    }
            set 
		    {
			    SetColumnValue("version_id", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public byte Status 
	    {
		    get
		    {
			    return GetColumnValue<byte>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("OpenTime")]
        [Bindable(true)]
        public DateTime OpenTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("open_time");
		    }
            set 
		    {
			    SetColumnValue("open_time", value);
            }
        }
	      
        [XmlAttribute("CloseTime")]
        [Bindable(true)]
        public DateTime CloseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("close_time");
		    }
            set 
		    {
			    SetColumnValue("close_time", value);
            }
        }
	      
        [XmlAttribute("Level")]
        [Bindable(true)]
        public int Level 
	    {
		    get
		    {
			    return GetColumnValue<int>("level");
		    }
            set 
		    {
			    SetColumnValue("level", value);
            }
        }
	      
        [XmlAttribute("PaymentPercent")]
        [Bindable(true)]
        public double PaymentPercent 
	    {
		    get
		    {
			    return GetColumnValue<double>("payment_percent");
		    }
            set 
		    {
			    SetColumnValue("payment_percent", value);
            }
        }
	      
        [XmlAttribute("Instruction")]
        [Bindable(true)]
        public string Instruction 
	    {
		    get
		    {
			    return GetColumnValue<string>("instruction");
		    }
            set 
		    {
			    SetColumnValue("instruction", value);
            }
        }
	      
        [XmlAttribute("Others")]
        [Bindable(true)]
        public string Others 
	    {
		    get
		    {
			    return GetColumnValue<string>("others");
		    }
            set 
		    {
			    SetColumnValue("others", value);
            }
        }
	      
        [XmlAttribute("AvailableDateType")]
        [Bindable(true)]
        public int AvailableDateType 
	    {
		    get
		    {
			    return GetColumnValue<int>("available_date_type");
		    }
            set 
		    {
			    SetColumnValue("available_date_type", value);
            }
        }
	      
        [XmlAttribute("OrderNeeded")]
        [Bindable(true)]
        public int? OrderNeeded 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_needed");
		    }
            set 
		    {
			    SetColumnValue("order_needed", value);
            }
        }
	      
        [XmlAttribute("AmountNeeded")]
        [Bindable(true)]
        public decimal? AmountNeeded 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("amount_needed");
		    }
            set 
		    {
			    SetColumnValue("amount_needed", value);
            }
        }
	      
        [XmlAttribute("Enabled")]
        [Bindable(true)]
        public bool Enabled 
	    {
		    get
		    {
			    return GetColumnValue<bool>("enabled");
		    }
            set 
		    {
			    SetColumnValue("enabled", value);
            }
        }
	      
        [XmlAttribute("ConditionalLogic")]
        [Bindable(true)]
        public int ConditionalLogic 
	    {
		    get
		    {
			    return GetColumnValue<int>("conditional_logic");
		    }
            set 
		    {
			    SetColumnValue("conditional_logic", value);
            }
        }
	      
        [XmlAttribute("CombineUse")]
        [Bindable(true)]
        public bool CombineUse 
	    {
		    get
		    {
			    return GetColumnValue<bool>("combine_use");
		    }
            set 
		    {
			    SetColumnValue("combine_use", value);
            }
        }
	      
        [XmlAttribute("ImagePath")]
        [Bindable(true)]
        public string ImagePath 
	    {
		    get
		    {
			    return GetColumnValue<string>("image_path");
		    }
            set 
		    {
			    SetColumnValue("image_path", value);
            }
        }
	      
        [XmlAttribute("BackgroundColor")]
        [Bindable(true)]
        public string BackgroundColor 
	    {
		    get
		    {
			    return GetColumnValue<string>("background_color");
		    }
            set 
		    {
			    SetColumnValue("background_color", value);
            }
        }
	      
        [XmlAttribute("BackgroundImage")]
        [Bindable(true)]
        public string BackgroundImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("background_image");
		    }
            set 
		    {
			    SetColumnValue("background_image", value);
            }
        }
	      
        [XmlAttribute("IconImage")]
        [Bindable(true)]
        public string IconImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("icon_image");
		    }
            set 
		    {
			    SetColumnValue("icon_image", value);
            }
        }
	      
        [XmlAttribute("BackgroundColorId")]
        [Bindable(true)]
        public int BackgroundColorId 
	    {
		    get
		    {
			    return GetColumnValue<int>("background_color_id");
		    }
            set 
		    {
			    SetColumnValue("background_color_id", value);
            }
        }
	      
        [XmlAttribute("BackgroundImageId")]
        [Bindable(true)]
        public int BackgroundImageId 
	    {
		    get
		    {
			    return GetColumnValue<int>("background_image_id");
		    }
            set 
		    {
			    SetColumnValue("background_image_id", value);
            }
        }
	      
        [XmlAttribute("IconImageId")]
        [Bindable(true)]
        public int IconImageId 
	    {
		    get
		    {
			    return GetColumnValue<int>("icon_image_id");
		    }
            set 
		    {
			    SetColumnValue("icon_image_id", value);
            }
        }
	      
        [XmlAttribute("ContractValidDate")]
        [Bindable(true)]
        public DateTime? ContractValidDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("contract_valid_date");
		    }
            set 
		    {
			    SetColumnValue("contract_valid_date", value);
            }
        }
	      
        [XmlAttribute("ReleaseCardAmount")]
        [Bindable(true)]
        public int? ReleaseCardAmount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("release_card_amount");
		    }
            set 
		    {
			    SetColumnValue("release_card_amount", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CardId = @"card_id";
            
            public static string SellerUserId = @"seller_user_id";
            
            public static string CardGroupId = @"card_group_id";
            
            public static string VersionId = @"version_id";
            
            public static string Status = @"status";
            
            public static string OpenTime = @"open_time";
            
            public static string CloseTime = @"close_time";
            
            public static string Level = @"level";
            
            public static string PaymentPercent = @"payment_percent";
            
            public static string Instruction = @"instruction";
            
            public static string Others = @"others";
            
            public static string AvailableDateType = @"available_date_type";
            
            public static string OrderNeeded = @"order_needed";
            
            public static string AmountNeeded = @"amount_needed";
            
            public static string Enabled = @"enabled";
            
            public static string ConditionalLogic = @"conditional_logic";
            
            public static string CombineUse = @"combine_use";
            
            public static string ImagePath = @"image_path";
            
            public static string BackgroundColor = @"background_color";
            
            public static string BackgroundImage = @"background_image";
            
            public static string IconImage = @"icon_image";
            
            public static string BackgroundColorId = @"background_color_id";
            
            public static string BackgroundImageId = @"background_image_id";
            
            public static string IconImageId = @"icon_image_id";
            
            public static string ContractValidDate = @"contract_valid_date";
            
            public static string ReleaseCardAmount = @"release_card_amount";
            
            public static string SellerGuid = @"seller_guid";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealSellerProduct class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealSellerProductCollection : ReadOnlyList<ViewHiDealSellerProduct, ViewHiDealSellerProductCollection>
    {        
        public ViewHiDealSellerProductCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_seller_product view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealSellerProduct : ReadOnlyRecord<ViewHiDealSellerProduct>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_seller_product", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 50;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarPromoLongDesc = new TableSchema.TableColumn(schema);
                colvarPromoLongDesc.ColumnName = "promo_long_desc";
                colvarPromoLongDesc.DataType = DbType.String;
                colvarPromoLongDesc.MaxLength = 255;
                colvarPromoLongDesc.AutoIncrement = false;
                colvarPromoLongDesc.IsNullable = true;
                colvarPromoLongDesc.IsPrimaryKey = false;
                colvarPromoLongDesc.IsForeignKey = false;
                colvarPromoLongDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoLongDesc);
                
                TableSchema.TableColumn colvarPromoShortDesc = new TableSchema.TableColumn(schema);
                colvarPromoShortDesc.ColumnName = "promo_short_desc";
                colvarPromoShortDesc.DataType = DbType.String;
                colvarPromoShortDesc.MaxLength = 100;
                colvarPromoShortDesc.AutoIncrement = false;
                colvarPromoShortDesc.IsNullable = true;
                colvarPromoShortDesc.IsPrimaryKey = false;
                colvarPromoShortDesc.IsForeignKey = false;
                colvarPromoShortDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoShortDesc);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = true;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarProductname = new TableSchema.TableColumn(schema);
                colvarProductname.ColumnName = "productname";
                colvarProductname.DataType = DbType.String;
                colvarProductname.MaxLength = 250;
                colvarProductname.AutoIncrement = false;
                colvarProductname.IsNullable = true;
                colvarProductname.IsPrimaryKey = false;
                colvarProductname.IsForeignKey = false;
                colvarProductname.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductname);
                
                TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
                colvarOrderCount.ColumnName = "order_count";
                colvarOrderCount.DataType = DbType.Int32;
                colvarOrderCount.MaxLength = 0;
                colvarOrderCount.AutoIncrement = false;
                colvarOrderCount.IsNullable = false;
                colvarOrderCount.IsPrimaryKey = false;
                colvarOrderCount.IsForeignKey = false;
                colvarOrderCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCount);
                
                TableSchema.TableColumn colvarReturnedCount = new TableSchema.TableColumn(schema);
                colvarReturnedCount.ColumnName = "returned_count";
                colvarReturnedCount.DataType = DbType.Int32;
                colvarReturnedCount.MaxLength = 0;
                colvarReturnedCount.AutoIncrement = false;
                colvarReturnedCount.IsNullable = false;
                colvarReturnedCount.IsPrimaryKey = false;
                colvarReturnedCount.IsForeignKey = false;
                colvarReturnedCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedCount);

                TableSchema.TableColumn colvarSales = new TableSchema.TableColumn(schema);
                colvarSales.ColumnName = "sales";
                colvarSales.DataType = DbType.String;
                colvarSales.MaxLength = 100;
                colvarSales.AutoIncrement = false;
                colvarSales.IsNullable = true;
                colvarSales.IsPrimaryKey = false;
                colvarSales.IsForeignKey = false;
                colvarSales.IsReadOnly = false;

                schema.Columns.Add(colvarSales);

                TableSchema.TableColumn colvarEmpUserId = new TableSchema.TableColumn(schema);
                colvarEmpUserId.ColumnName = "emp_user_id";
                colvarEmpUserId.DataType = DbType.Int32;
                colvarEmpUserId.MaxLength = 0;
                colvarEmpUserId.AutoIncrement = false;
                colvarEmpUserId.IsNullable = true;
                colvarEmpUserId.IsPrimaryKey = false;
                colvarEmpUserId.IsForeignKey = false;
                colvarEmpUserId.IsReadOnly = false;

                schema.Columns.Add(colvarEmpUserId);

                TableSchema.TableColumn colvarTeamNo = new TableSchema.TableColumn(schema);
                colvarTeamNo.ColumnName = "team_no";
                colvarTeamNo.DataType = DbType.Int32;
                colvarTeamNo.MaxLength = 0;
                colvarTeamNo.AutoIncrement = false;
                colvarTeamNo.IsNullable = true;
                colvarTeamNo.IsPrimaryKey = false;
                colvarTeamNo.IsForeignKey = false;
                colvarTeamNo.IsReadOnly = false;

                schema.Columns.Add(colvarTeamNo);

                TableSchema.TableColumn colvarDeptId = new TableSchema.TableColumn(schema);
                colvarDeptId.ColumnName = "dept_id";
                colvarDeptId.DataType = DbType.AnsiString;
                colvarDeptId.MaxLength = 50;
                colvarDeptId.AutoIncrement = false;
                colvarDeptId.IsNullable = true;
                colvarDeptId.IsPrimaryKey = false;
                colvarDeptId.IsForeignKey = false;
                colvarDeptId.IsReadOnly = false;

                schema.Columns.Add(colvarDeptId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_seller_product",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealSellerProduct()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealSellerProduct(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealSellerProduct(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealSellerProduct(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_name");
		    }
            set 
		    {
			    SetColumnValue("deal_name", value);
            }
        }
	      
        [XmlAttribute("PromoLongDesc")]
        [Bindable(true)]
        public string PromoLongDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("promo_long_desc");
		    }
            set 
		    {
			    SetColumnValue("promo_long_desc", value);
            }
        }
	      
        [XmlAttribute("PromoShortDesc")]
        [Bindable(true)]
        public string PromoShortDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("promo_short_desc");
		    }
            set 
		    {
			    SetColumnValue("promo_short_desc", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_start_time", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int? ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("Productname")]
        [Bindable(true)]
        public string Productname 
	    {
		    get
		    {
			    return GetColumnValue<string>("productname");
		    }
            set 
		    {
			    SetColumnValue("productname", value);
            }
        }
	      
        [XmlAttribute("OrderCount")]
        [Bindable(true)]
        public int OrderCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_count");
		    }
            set 
		    {
			    SetColumnValue("order_count", value);
            }
        }
	      
        [XmlAttribute("ReturnedCount")]
        [Bindable(true)]
        public int ReturnedCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("returned_count");
		    }
            set 
		    {
			    SetColumnValue("returned_count", value);
            }
        }

        [XmlAttribute("Sales")]
        [Bindable(true)]
        public string Sales
        {
            get
            {
                return GetColumnValue<string>("sales");
            }
            set
            {
                SetColumnValue("sales", value);
            }
        }

        [XmlAttribute("EmpUserId")]
        [Bindable(true)]
        public int? EmpUserId
        {
            get
            {
                return GetColumnValue<int?>("emp_user_id");
            }
            set
            {
                SetColumnValue("emp_user_id", value);
            }
        }

        [XmlAttribute("TeamNo")]
        [Bindable(true)]
        public int? TeamNo
        {
            get
            {
                return GetColumnValue<int?>("team_no");
            }
            set
            {
                SetColumnValue("team_no", value);
            }
        }

        [XmlAttribute("DeptId")]
        [Bindable(true)]
        public string DeptId
        {
            get
            {
                return GetColumnValue<string>("dept_id");
            }
            set
            {
                SetColumnValue("dept_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string SellerGuid = @"seller_guid";
            
            public static string SellerName = @"seller_name";
            
            public static string DealId = @"deal_id";
            
            public static string DealName = @"deal_name";
            
            public static string PromoLongDesc = @"promo_long_desc";
            
            public static string PromoShortDesc = @"promo_short_desc";
            
            public static string DealStartTime = @"deal_start_time";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string ProductId = @"product_id";
            
            public static string Productname = @"productname";
            
            public static string OrderCount = @"order_count";
            
            public static string ReturnedCount = @"returned_count";

            public static string Sales = @"sales";

            public static string EmpUserId = @"emp_user_id";

            public static string TeamNo = @"team_no";

            public static string DeptId = @"dept_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

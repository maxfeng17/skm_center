using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PponDealEvaluateStar class.
	/// </summary>
    [Serializable]
	public partial class PponDealEvaluateStarCollection : RepositoryList<PponDealEvaluateStar, PponDealEvaluateStarCollection>
	{	   
		public PponDealEvaluateStarCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PponDealEvaluateStarCollection</returns>
		public PponDealEvaluateStarCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PponDealEvaluateStar o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the ppon_deal_evaluate_star table.
	/// </summary>
	[Serializable]
	public partial class PponDealEvaluateStar : RepositoryRecord<PponDealEvaluateStar>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PponDealEvaluateStar()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PponDealEvaluateStar(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("ppon_deal_evaluate_star", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarCnt = new TableSchema.TableColumn(schema);
				colvarCnt.ColumnName = "cnt";
				colvarCnt.DataType = DbType.Int32;
				colvarCnt.MaxLength = 0;
				colvarCnt.AutoIncrement = false;
				colvarCnt.IsNullable = false;
				colvarCnt.IsPrimaryKey = false;
				colvarCnt.IsForeignKey = false;
				colvarCnt.IsReadOnly = false;
				colvarCnt.DefaultSetting = @"";
				colvarCnt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCnt);
				
				TableSchema.TableColumn colvarStar = new TableSchema.TableColumn(schema);
				colvarStar.ColumnName = "star";
				colvarStar.DataType = DbType.Decimal;
				colvarStar.MaxLength = 0;
				colvarStar.AutoIncrement = false;
				colvarStar.IsNullable = false;
				colvarStar.IsPrimaryKey = false;
				colvarStar.IsForeignKey = false;
				colvarStar.IsReadOnly = false;
				colvarStar.DefaultSetting = @"";
				colvarStar.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStar);
				
				TableSchema.TableColumn colvarBatchNum = new TableSchema.TableColumn(schema);
				colvarBatchNum.ColumnName = "batch_num";
				colvarBatchNum.DataType = DbType.Int32;
				colvarBatchNum.MaxLength = 0;
				colvarBatchNum.AutoIncrement = false;
				colvarBatchNum.IsNullable = false;
				colvarBatchNum.IsPrimaryKey = false;
				colvarBatchNum.IsForeignKey = false;
				colvarBatchNum.IsReadOnly = false;
				colvarBatchNum.DefaultSetting = @"";
				colvarBatchNum.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBatchNum);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("ppon_deal_evaluate_star",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("Cnt")]
		[Bindable(true)]
		public int Cnt 
		{
			get { return GetColumnValue<int>(Columns.Cnt); }
			set { SetColumnValue(Columns.Cnt, value); }
		}
		  
		[XmlAttribute("Star")]
		[Bindable(true)]
		public decimal Star 
		{
			get { return GetColumnValue<decimal>(Columns.Star); }
			set { SetColumnValue(Columns.Star, value); }
		}
		  
		[XmlAttribute("BatchNum")]
		[Bindable(true)]
		public int BatchNum 
		{
			get { return GetColumnValue<int>(Columns.BatchNum); }
			set { SetColumnValue(Columns.BatchNum, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CntColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn StarColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn BatchNumColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Bid = @"bid";
			 public static string Cnt = @"cnt";
			 public static string Star = @"star";
			 public static string BatchNum = @"batch_num";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

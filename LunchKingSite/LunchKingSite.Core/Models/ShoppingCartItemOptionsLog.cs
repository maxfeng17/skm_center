﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ShoppingCartItemOptionsLog class.
    /// </summary>
    [Serializable]
    public partial class ShoppingCartItemOptionsLogCollection : RepositoryList<ShoppingCartItemOptionsLog, ShoppingCartItemOptionsLogCollection>
    {
        public ShoppingCartItemOptionsLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ShoppingCartItemOptionsLogCollection</returns>
        public ShoppingCartItemOptionsLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ShoppingCartItemOptionsLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the shopping_cart_item_options_log table.
    /// </summary>
    [Serializable]
    public partial class ShoppingCartItemOptionsLog : RepositoryRecord<ShoppingCartItemOptionsLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public ShoppingCartItemOptionsLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ShoppingCartItemOptionsLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("shopping_cart_item_options_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarTicketId = new TableSchema.TableColumn(schema);
                colvarTicketId.ColumnName = "ticket_id";
                colvarTicketId.DataType = DbType.AnsiString;
                colvarTicketId.MaxLength = 100;
                colvarTicketId.AutoIncrement = false;
                colvarTicketId.IsNullable = false;
                colvarTicketId.IsPrimaryKey = false;
                colvarTicketId.IsForeignKey = false;
                colvarTicketId.IsReadOnly = false;
                colvarTicketId.DefaultSetting = @"";
                colvarTicketId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTicketId);

                TableSchema.TableColumn colvarItemOptions = new TableSchema.TableColumn(schema);
                colvarItemOptions.ColumnName = "item_options";
                colvarItemOptions.DataType = DbType.String;
                colvarItemOptions.MaxLength = -1;
                colvarItemOptions.AutoIncrement = false;
                colvarItemOptions.IsNullable = false;
                colvarItemOptions.IsPrimaryKey = false;
                colvarItemOptions.IsForeignKey = false;
                colvarItemOptions.IsReadOnly = false;
                colvarItemOptions.DefaultSetting = @"";
                colvarItemOptions.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemOptions);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarIsRemove = new TableSchema.TableColumn(schema);
                colvarIsRemove.ColumnName = "is_remove";
                colvarIsRemove.DataType = DbType.Boolean;
                colvarIsRemove.MaxLength = 0;
                colvarIsRemove.AutoIncrement = false;
                colvarIsRemove.IsNullable = false;
                colvarIsRemove.IsPrimaryKey = false;
                colvarIsRemove.IsForeignKey = false;
                colvarIsRemove.IsReadOnly = false;
                colvarIsRemove.DefaultSetting = @"";
                colvarIsRemove.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsRemove);

                TableSchema.TableColumn colvarLogTime = new TableSchema.TableColumn(schema);
                colvarLogTime.ColumnName = "log_time";
                colvarLogTime.DataType = DbType.DateTime;
                colvarLogTime.MaxLength = 0;
                colvarLogTime.AutoIncrement = false;
                colvarLogTime.IsNullable = false;
                colvarLogTime.IsPrimaryKey = false;
                colvarLogTime.IsForeignKey = false;
                colvarLogTime.IsReadOnly = false;
                colvarLogTime.DefaultSetting = @"";
                colvarLogTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLogTime);

                TableSchema.TableColumn colvarShoppingCartGroupGuid = new TableSchema.TableColumn(schema);
                colvarShoppingCartGroupGuid.ColumnName = "shopping_cart_group_guid";
                colvarShoppingCartGroupGuid.DataType = DbType.Guid;
                colvarShoppingCartGroupGuid.MaxLength = 0;
                colvarShoppingCartGroupGuid.AutoIncrement = false;
                colvarShoppingCartGroupGuid.IsNullable = false;
                colvarShoppingCartGroupGuid.IsPrimaryKey = false;
                colvarShoppingCartGroupGuid.IsForeignKey = false;
                colvarShoppingCartGroupGuid.IsReadOnly = false;
                colvarShoppingCartGroupGuid.DefaultSetting = @"";
                colvarShoppingCartGroupGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShoppingCartGroupGuid);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("shopping_cart_item_options_log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("TicketId")]
        [Bindable(true)]
        public string TicketId
        {
            get { return GetColumnValue<string>(Columns.TicketId); }
            set { SetColumnValue(Columns.TicketId, value); }
        }

        [XmlAttribute("ItemOptions")]
        [Bindable(true)]
        public string ItemOptions
        {
            get { return GetColumnValue<string>(Columns.ItemOptions); }
            set { SetColumnValue(Columns.ItemOptions, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get { return GetColumnValue<int>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("IsRemove")]
        [Bindable(true)]
        public bool IsRemove
        {
            get { return GetColumnValue<bool>(Columns.IsRemove); }
            set { SetColumnValue(Columns.IsRemove, value); }
        }

        [XmlAttribute("LogTime")]
        [Bindable(true)]
        public DateTime LogTime
        {
            get { return GetColumnValue<DateTime>(Columns.LogTime); }
            set { SetColumnValue(Columns.LogTime, value); }
        }

        [XmlAttribute("ShoppingCartGroupGuid")]
        [Bindable(true)]
        public Guid ShoppingCartGroupGuid
        {
            get { return GetColumnValue<Guid>(Columns.ShoppingCartGroupGuid); }
            set { SetColumnValue(Columns.ShoppingCartGroupGuid, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn TicketIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ItemOptionsColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn IsRemoveColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn LogTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ShoppingCartGroupGuidColumn
        {
            get { return Schema.Columns[7]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"guid";
            public static string TicketId = @"ticket_id";
            public static string ItemOptions = @"item_options";
            public static string UserId = @"user_id";
            public static string CreateTime = @"create_time";
            public static string IsRemove = @"is_remove";
            public static string LogTime = @"log_time";
            public static string ShoppingCartGroupGuid = @"shopping_cart_group_guid";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

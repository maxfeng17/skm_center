using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BalanceSheetWmsBillRelationship class.
	/// </summary>
    [Serializable]
	public partial class BalanceSheetWmsBillRelationshipCollection : RepositoryList<BalanceSheetWmsBillRelationship, BalanceSheetWmsBillRelationshipCollection>
	{	   
		public BalanceSheetWmsBillRelationshipCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BalanceSheetWmsBillRelationshipCollection</returns>
		public BalanceSheetWmsBillRelationshipCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BalanceSheetWmsBillRelationship o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the balance_sheet_wms_bill_relationship table.
	/// </summary>
	[Serializable]
	public partial class BalanceSheetWmsBillRelationship : RepositoryRecord<BalanceSheetWmsBillRelationship>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BalanceSheetWmsBillRelationship()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BalanceSheetWmsBillRelationship(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("balance_sheet_wms_bill_relationship", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarBalanceSheetId = new TableSchema.TableColumn(schema);
				colvarBalanceSheetId.ColumnName = "balance_sheet_id";
				colvarBalanceSheetId.DataType = DbType.Int32;
				colvarBalanceSheetId.MaxLength = 0;
				colvarBalanceSheetId.AutoIncrement = false;
				colvarBalanceSheetId.IsNullable = false;
				colvarBalanceSheetId.IsPrimaryKey = true;
				colvarBalanceSheetId.IsForeignKey = false;
				colvarBalanceSheetId.IsReadOnly = false;
				colvarBalanceSheetId.DefaultSetting = @"";
				colvarBalanceSheetId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBalanceSheetId);
				
				TableSchema.TableColumn colvarBillId = new TableSchema.TableColumn(schema);
				colvarBillId.ColumnName = "bill_id";
				colvarBillId.DataType = DbType.Int32;
				colvarBillId.MaxLength = 0;
				colvarBillId.AutoIncrement = false;
				colvarBillId.IsNullable = false;
				colvarBillId.IsPrimaryKey = true;
				colvarBillId.IsForeignKey = false;
				colvarBillId.IsReadOnly = false;
				colvarBillId.DefaultSetting = @"";
				colvarBillId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBillId);
				
				TableSchema.TableColumn colvarBillMoney = new TableSchema.TableColumn(schema);
				colvarBillMoney.ColumnName = "bill_money";
				colvarBillMoney.DataType = DbType.Int32;
				colvarBillMoney.MaxLength = 0;
				colvarBillMoney.AutoIncrement = false;
				colvarBillMoney.IsNullable = false;
				colvarBillMoney.IsPrimaryKey = false;
				colvarBillMoney.IsForeignKey = false;
				colvarBillMoney.IsReadOnly = false;
				colvarBillMoney.DefaultSetting = @"";
				colvarBillMoney.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBillMoney);
				
				TableSchema.TableColumn colvarBillMoneyNotaxed = new TableSchema.TableColumn(schema);
				colvarBillMoneyNotaxed.ColumnName = "bill_money_notaxed";
				colvarBillMoneyNotaxed.DataType = DbType.Int32;
				colvarBillMoneyNotaxed.MaxLength = 0;
				colvarBillMoneyNotaxed.AutoIncrement = false;
				colvarBillMoneyNotaxed.IsNullable = false;
				colvarBillMoneyNotaxed.IsPrimaryKey = false;
				colvarBillMoneyNotaxed.IsForeignKey = false;
				colvarBillMoneyNotaxed.IsReadOnly = false;
				colvarBillMoneyNotaxed.DefaultSetting = @"";
				colvarBillMoneyNotaxed.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBillMoneyNotaxed);
				
				TableSchema.TableColumn colvarBillTax = new TableSchema.TableColumn(schema);
				colvarBillTax.ColumnName = "bill_tax";
				colvarBillTax.DataType = DbType.Int32;
				colvarBillTax.MaxLength = 0;
				colvarBillTax.AutoIncrement = false;
				colvarBillTax.IsNullable = false;
				colvarBillTax.IsPrimaryKey = false;
				colvarBillTax.IsForeignKey = false;
				colvarBillTax.IsReadOnly = false;
				colvarBillTax.DefaultSetting = @"";
				colvarBillTax.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBillTax);
				
				TableSchema.TableColumn colvarPaymentTotal = new TableSchema.TableColumn(schema);
				colvarPaymentTotal.ColumnName = "payment_total";
				colvarPaymentTotal.DataType = DbType.Int32;
				colvarPaymentTotal.MaxLength = 0;
				colvarPaymentTotal.AutoIncrement = false;
				colvarPaymentTotal.IsNullable = true;
				colvarPaymentTotal.IsPrimaryKey = false;
				colvarPaymentTotal.IsForeignKey = false;
				colvarPaymentTotal.IsReadOnly = false;
				colvarPaymentTotal.DefaultSetting = @"";
				colvarPaymentTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentTotal);
				
				TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
				colvarRemark.ColumnName = "remark";
				colvarRemark.DataType = DbType.String;
				colvarRemark.MaxLength = 250;
				colvarRemark.AutoIncrement = false;
				colvarRemark.IsNullable = true;
				colvarRemark.IsPrimaryKey = false;
				colvarRemark.IsForeignKey = false;
				colvarRemark.IsReadOnly = false;
				colvarRemark.DefaultSetting = @"";
				colvarRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemark);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 100;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 100;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = false;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("balance_sheet_wms_bill_relationship",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("BalanceSheetId")]
		[Bindable(true)]
		public int BalanceSheetId 
		{
			get { return GetColumnValue<int>(Columns.BalanceSheetId); }
			set { SetColumnValue(Columns.BalanceSheetId, value); }
		}
		  
		[XmlAttribute("BillId")]
		[Bindable(true)]
		public int BillId 
		{
			get { return GetColumnValue<int>(Columns.BillId); }
			set { SetColumnValue(Columns.BillId, value); }
		}
		  
		[XmlAttribute("BillMoney")]
		[Bindable(true)]
		public int BillMoney 
		{
			get { return GetColumnValue<int>(Columns.BillMoney); }
			set { SetColumnValue(Columns.BillMoney, value); }
		}
		  
		[XmlAttribute("BillMoneyNotaxed")]
		[Bindable(true)]
		public int BillMoneyNotaxed 
		{
			get { return GetColumnValue<int>(Columns.BillMoneyNotaxed); }
			set { SetColumnValue(Columns.BillMoneyNotaxed, value); }
		}
		  
		[XmlAttribute("BillTax")]
		[Bindable(true)]
		public int BillTax 
		{
			get { return GetColumnValue<int>(Columns.BillTax); }
			set { SetColumnValue(Columns.BillTax, value); }
		}
		  
		[XmlAttribute("PaymentTotal")]
		[Bindable(true)]
		public int? PaymentTotal 
		{
			get { return GetColumnValue<int?>(Columns.PaymentTotal); }
			set { SetColumnValue(Columns.PaymentTotal, value); }
		}
		  
		[XmlAttribute("Remark")]
		[Bindable(true)]
		public string Remark 
		{
			get { return GetColumnValue<string>(Columns.Remark); }
			set { SetColumnValue(Columns.Remark, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn BalanceSheetIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BillIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BillMoneyColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BillMoneyNotaxedColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn BillTaxColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PaymentTotalColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string BalanceSheetId = @"balance_sheet_id";
			 public static string BillId = @"bill_id";
			 public static string BillMoney = @"bill_money";
			 public static string BillMoneyNotaxed = @"bill_money_notaxed";
			 public static string BillTax = @"bill_tax";
			 public static string PaymentTotal = @"payment_total";
			 public static string Remark = @"remark";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

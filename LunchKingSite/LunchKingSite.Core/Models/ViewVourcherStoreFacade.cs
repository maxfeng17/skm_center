using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using Microsoft.SqlServer.Types;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVourcherStoreFacade class.
    /// </summary>
    [Serializable]
    public partial class ViewVourcherStoreFacadeCollection : ReadOnlyList<ViewVourcherStoreFacade, ViewVourcherStoreFacadeCollection>
    {        
        public ViewVourcherStoreFacadeCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vourcher_store_facade view.
    /// </summary>
    [Serializable]
    public partial class ViewVourcherStoreFacade : ReadOnlyRecord<ViewVourcherStoreFacade>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vourcher_store_facade", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
                colvarPhone.ColumnName = "phone";
                colvarPhone.DataType = DbType.AnsiString;
                colvarPhone.MaxLength = 100;
                colvarPhone.AutoIncrement = false;
                colvarPhone.IsNullable = true;
                colvarPhone.IsPrimaryKey = false;
                colvarPhone.IsForeignKey = false;
                colvarPhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhone);
                
                TableSchema.TableColumn colvarRegionId = new TableSchema.TableColumn(schema);
                colvarRegionId.ColumnName = "region_id";
                colvarRegionId.DataType = DbType.Int32;
                colvarRegionId.MaxLength = 0;
                colvarRegionId.AutoIncrement = false;
                colvarRegionId.IsNullable = true;
                colvarRegionId.IsPrimaryKey = false;
                colvarRegionId.IsForeignKey = false;
                colvarRegionId.IsReadOnly = false;
                
                schema.Columns.Add(colvarRegionId);
                
                TableSchema.TableColumn colvarRegionName = new TableSchema.TableColumn(schema);
                colvarRegionName.ColumnName = "region_name";
                colvarRegionName.DataType = DbType.String;
                colvarRegionName.MaxLength = 20;
                colvarRegionName.AutoIncrement = false;
                colvarRegionName.IsNullable = true;
                colvarRegionName.IsPrimaryKey = false;
                colvarRegionName.IsForeignKey = false;
                colvarRegionName.IsReadOnly = false;
                
                schema.Columns.Add(colvarRegionName);
                
                TableSchema.TableColumn colvarCity = new TableSchema.TableColumn(schema);
                colvarCity.ColumnName = "city";
                colvarCity.DataType = DbType.String;
                colvarCity.MaxLength = 20;
                colvarCity.AutoIncrement = false;
                colvarCity.IsNullable = false;
                colvarCity.IsPrimaryKey = false;
                colvarCity.IsForeignKey = false;
                colvarCity.IsReadOnly = false;
                
                schema.Columns.Add(colvarCity);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = true;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarTown = new TableSchema.TableColumn(schema);
                colvarTown.ColumnName = "town";
                colvarTown.DataType = DbType.String;
                colvarTown.MaxLength = 20;
                colvarTown.AutoIncrement = false;
                colvarTown.IsNullable = false;
                colvarTown.IsPrimaryKey = false;
                colvarTown.IsForeignKey = false;
                colvarTown.IsReadOnly = false;
                
                schema.Columns.Add(colvarTown);
                
                TableSchema.TableColumn colvarTownshipId = new TableSchema.TableColumn(schema);
                colvarTownshipId.ColumnName = "township_id";
                colvarTownshipId.DataType = DbType.Int32;
                colvarTownshipId.MaxLength = 0;
                colvarTownshipId.AutoIncrement = false;
                colvarTownshipId.IsNullable = true;
                colvarTownshipId.IsPrimaryKey = false;
                colvarTownshipId.IsForeignKey = false;
                colvarTownshipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTownshipId);
                
                TableSchema.TableColumn colvarAddressString = new TableSchema.TableColumn(schema);
                colvarAddressString.ColumnName = "address_string";
                colvarAddressString.DataType = DbType.String;
                colvarAddressString.MaxLength = 100;
                colvarAddressString.AutoIncrement = false;
                colvarAddressString.IsNullable = true;
                colvarAddressString.IsPrimaryKey = false;
                colvarAddressString.IsForeignKey = false;
                colvarAddressString.IsReadOnly = false;
                
                schema.Columns.Add(colvarAddressString);
                
                TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
                colvarCoordinate.ColumnName = "coordinate";
                colvarCoordinate.DataType = DbType.AnsiString;
                colvarCoordinate.MaxLength = -1;
                colvarCoordinate.AutoIncrement = false;
                colvarCoordinate.IsNullable = true;
                colvarCoordinate.IsPrimaryKey = false;
                colvarCoordinate.IsForeignKey = false;
                colvarCoordinate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCoordinate);
                
                TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
                colvarOpenTime.ColumnName = "open_time";
                colvarOpenTime.DataType = DbType.String;
                colvarOpenTime.MaxLength = 256;
                colvarOpenTime.AutoIncrement = false;
                colvarOpenTime.IsNullable = true;
                colvarOpenTime.IsPrimaryKey = false;
                colvarOpenTime.IsForeignKey = false;
                colvarOpenTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpenTime);
                
                TableSchema.TableColumn colvarCloseDate = new TableSchema.TableColumn(schema);
                colvarCloseDate.ColumnName = "close_date";
                colvarCloseDate.DataType = DbType.String;
                colvarCloseDate.MaxLength = 256;
                colvarCloseDate.AutoIncrement = false;
                colvarCloseDate.IsNullable = true;
                colvarCloseDate.IsPrimaryKey = false;
                colvarCloseDate.IsForeignKey = false;
                colvarCloseDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCloseDate);
                
                TableSchema.TableColumn colvarRemarks = new TableSchema.TableColumn(schema);
                colvarRemarks.ColumnName = "remarks";
                colvarRemarks.DataType = DbType.String;
                colvarRemarks.MaxLength = 256;
                colvarRemarks.AutoIncrement = false;
                colvarRemarks.IsNullable = true;
                colvarRemarks.IsPrimaryKey = false;
                colvarRemarks.IsForeignKey = false;
                colvarRemarks.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemarks);
                
                TableSchema.TableColumn colvarMrt = new TableSchema.TableColumn(schema);
                colvarMrt.ColumnName = "mrt";
                colvarMrt.DataType = DbType.String;
                colvarMrt.MaxLength = 256;
                colvarMrt.AutoIncrement = false;
                colvarMrt.IsNullable = true;
                colvarMrt.IsPrimaryKey = false;
                colvarMrt.IsForeignKey = false;
                colvarMrt.IsReadOnly = false;
                
                schema.Columns.Add(colvarMrt);
                
                TableSchema.TableColumn colvarCar = new TableSchema.TableColumn(schema);
                colvarCar.ColumnName = "car";
                colvarCar.DataType = DbType.String;
                colvarCar.MaxLength = 256;
                colvarCar.AutoIncrement = false;
                colvarCar.IsNullable = true;
                colvarCar.IsPrimaryKey = false;
                colvarCar.IsForeignKey = false;
                colvarCar.IsReadOnly = false;
                
                schema.Columns.Add(colvarCar);
                
                TableSchema.TableColumn colvarBus = new TableSchema.TableColumn(schema);
                colvarBus.ColumnName = "bus";
                colvarBus.DataType = DbType.String;
                colvarBus.MaxLength = 256;
                colvarBus.AutoIncrement = false;
                colvarBus.IsNullable = true;
                colvarBus.IsPrimaryKey = false;
                colvarBus.IsForeignKey = false;
                colvarBus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBus);
                
                TableSchema.TableColumn colvarOtherVehicles = new TableSchema.TableColumn(schema);
                colvarOtherVehicles.ColumnName = "other_vehicles";
                colvarOtherVehicles.DataType = DbType.String;
                colvarOtherVehicles.MaxLength = 256;
                colvarOtherVehicles.AutoIncrement = false;
                colvarOtherVehicles.IsNullable = true;
                colvarOtherVehicles.IsPrimaryKey = false;
                colvarOtherVehicles.IsForeignKey = false;
                colvarOtherVehicles.IsReadOnly = false;
                
                schema.Columns.Add(colvarOtherVehicles);
                
                TableSchema.TableColumn colvarWebUrl = new TableSchema.TableColumn(schema);
                colvarWebUrl.ColumnName = "web_url";
                colvarWebUrl.DataType = DbType.String;
                colvarWebUrl.MaxLength = 256;
                colvarWebUrl.AutoIncrement = false;
                colvarWebUrl.IsNullable = true;
                colvarWebUrl.IsPrimaryKey = false;
                colvarWebUrl.IsForeignKey = false;
                colvarWebUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarWebUrl);
                
                TableSchema.TableColumn colvarFacebookUrl = new TableSchema.TableColumn(schema);
                colvarFacebookUrl.ColumnName = "facebook_url";
                colvarFacebookUrl.DataType = DbType.String;
                colvarFacebookUrl.MaxLength = 256;
                colvarFacebookUrl.AutoIncrement = false;
                colvarFacebookUrl.IsNullable = true;
                colvarFacebookUrl.IsPrimaryKey = false;
                colvarFacebookUrl.IsForeignKey = false;
                colvarFacebookUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarFacebookUrl);
                
                TableSchema.TableColumn colvarPlurkUrl = new TableSchema.TableColumn(schema);
                colvarPlurkUrl.ColumnName = "plurk_url";
                colvarPlurkUrl.DataType = DbType.String;
                colvarPlurkUrl.MaxLength = 256;
                colvarPlurkUrl.AutoIncrement = false;
                colvarPlurkUrl.IsNullable = true;
                colvarPlurkUrl.IsPrimaryKey = false;
                colvarPlurkUrl.IsForeignKey = false;
                colvarPlurkUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarPlurkUrl);
                
                TableSchema.TableColumn colvarBlogUrl = new TableSchema.TableColumn(schema);
                colvarBlogUrl.ColumnName = "blog_url";
                colvarBlogUrl.DataType = DbType.String;
                colvarBlogUrl.MaxLength = 256;
                colvarBlogUrl.AutoIncrement = false;
                colvarBlogUrl.IsNullable = true;
                colvarBlogUrl.IsPrimaryKey = false;
                colvarBlogUrl.IsForeignKey = false;
                colvarBlogUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarBlogUrl);
                
                TableSchema.TableColumn colvarOtherUrl = new TableSchema.TableColumn(schema);
                colvarOtherUrl.ColumnName = "other_url";
                colvarOtherUrl.DataType = DbType.String;
                colvarOtherUrl.MaxLength = 256;
                colvarOtherUrl.AutoIncrement = false;
                colvarOtherUrl.IsNullable = true;
                colvarOtherUrl.IsPrimaryKey = false;
                colvarOtherUrl.IsForeignKey = false;
                colvarOtherUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarOtherUrl);
                
                TableSchema.TableColumn colvarCreditcardAvailable = new TableSchema.TableColumn(schema);
                colvarCreditcardAvailable.ColumnName = "creditcard_available";
                colvarCreditcardAvailable.DataType = DbType.Boolean;
                colvarCreditcardAvailable.MaxLength = 0;
                colvarCreditcardAvailable.AutoIncrement = false;
                colvarCreditcardAvailable.IsNullable = false;
                colvarCreditcardAvailable.IsPrimaryKey = false;
                colvarCreditcardAvailable.IsForeignKey = false;
                colvarCreditcardAvailable.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreditcardAvailable);
                
                TableSchema.TableColumn colvarSellerConsumptionAvg = new TableSchema.TableColumn(schema);
                colvarSellerConsumptionAvg.ColumnName = "seller_consumption_avg";
                colvarSellerConsumptionAvg.DataType = DbType.Int32;
                colvarSellerConsumptionAvg.MaxLength = 0;
                colvarSellerConsumptionAvg.AutoIncrement = false;
                colvarSellerConsumptionAvg.IsNullable = true;
                colvarSellerConsumptionAvg.IsPrimaryKey = false;
                colvarSellerConsumptionAvg.IsForeignKey = false;
                colvarSellerConsumptionAvg.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerConsumptionAvg);
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarVourcherEventId = new TableSchema.TableColumn(schema);
                colvarVourcherEventId.ColumnName = "vourcher_event_id";
                colvarVourcherEventId.DataType = DbType.Int32;
                colvarVourcherEventId.MaxLength = 0;
                colvarVourcherEventId.AutoIncrement = false;
                colvarVourcherEventId.IsNullable = false;
                colvarVourcherEventId.IsPrimaryKey = false;
                colvarVourcherEventId.IsForeignKey = false;
                colvarVourcherEventId.IsReadOnly = false;
                
                schema.Columns.Add(colvarVourcherEventId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = false;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarPageCount = new TableSchema.TableColumn(schema);
                colvarPageCount.ColumnName = "page_count";
                colvarPageCount.DataType = DbType.Int32;
                colvarPageCount.MaxLength = 0;
                colvarPageCount.AutoIncrement = false;
                colvarPageCount.IsNullable = false;
                colvarPageCount.IsPrimaryKey = false;
                colvarPageCount.IsForeignKey = false;
                colvarPageCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarPageCount);
                
                TableSchema.TableColumn colvarMaxQuantity = new TableSchema.TableColumn(schema);
                colvarMaxQuantity.ColumnName = "max_quantity";
                colvarMaxQuantity.DataType = DbType.Int32;
                colvarMaxQuantity.MaxLength = 0;
                colvarMaxQuantity.AutoIncrement = false;
                colvarMaxQuantity.IsNullable = false;
                colvarMaxQuantity.IsPrimaryKey = false;
                colvarMaxQuantity.IsForeignKey = false;
                colvarMaxQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarMaxQuantity);
                
                TableSchema.TableColumn colvarCurrentQuantity = new TableSchema.TableColumn(schema);
                colvarCurrentQuantity.ColumnName = "current_quantity";
                colvarCurrentQuantity.DataType = DbType.Int32;
                colvarCurrentQuantity.MaxLength = 0;
                colvarCurrentQuantity.AutoIncrement = false;
                colvarCurrentQuantity.IsNullable = false;
                colvarCurrentQuantity.IsPrimaryKey = false;
                colvarCurrentQuantity.IsForeignKey = false;
                colvarCurrentQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarCurrentQuantity);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerLogoimgPath = new TableSchema.TableColumn(schema);
                colvarSellerLogoimgPath.ColumnName = "seller_logoimg_path";
                colvarSellerLogoimgPath.DataType = DbType.AnsiString;
                colvarSellerLogoimgPath.MaxLength = 500;
                colvarSellerLogoimgPath.AutoIncrement = false;
                colvarSellerLogoimgPath.IsNullable = true;
                colvarSellerLogoimgPath.IsPrimaryKey = false;
                colvarSellerLogoimgPath.IsForeignKey = false;
                colvarSellerLogoimgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerLogoimgPath);
                
                TableSchema.TableColumn colvarSellerCategory = new TableSchema.TableColumn(schema);
                colvarSellerCategory.ColumnName = "seller_category";
                colvarSellerCategory.DataType = DbType.Int32;
                colvarSellerCategory.MaxLength = 0;
                colvarSellerCategory.AutoIncrement = false;
                colvarSellerCategory.IsNullable = true;
                colvarSellerCategory.IsPrimaryKey = false;
                colvarSellerCategory.IsForeignKey = false;
                colvarSellerCategory.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCategory);
                
                TableSchema.TableColumn colvarContents = new TableSchema.TableColumn(schema);
                colvarContents.ColumnName = "contents";
                colvarContents.DataType = DbType.String;
                colvarContents.MaxLength = 1000;
                colvarContents.AutoIncrement = false;
                colvarContents.IsNullable = true;
                colvarContents.IsPrimaryKey = false;
                colvarContents.IsForeignKey = false;
                colvarContents.IsReadOnly = false;
                
                schema.Columns.Add(colvarContents);
                
                TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
                colvarStartDate.ColumnName = "start_date";
                colvarStartDate.DataType = DbType.DateTime;
                colvarStartDate.MaxLength = 0;
                colvarStartDate.AutoIncrement = false;
                colvarStartDate.IsNullable = true;
                colvarStartDate.IsPrimaryKey = false;
                colvarStartDate.IsForeignKey = false;
                colvarStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartDate);
                
                TableSchema.TableColumn colvarVourcherPageCount = new TableSchema.TableColumn(schema);
                colvarVourcherPageCount.ColumnName = "vourcher_page_count";
                colvarVourcherPageCount.DataType = DbType.Int32;
                colvarVourcherPageCount.MaxLength = 0;
                colvarVourcherPageCount.AutoIncrement = false;
                colvarVourcherPageCount.IsNullable = true;
                colvarVourcherPageCount.IsPrimaryKey = false;
                colvarVourcherPageCount.IsForeignKey = false;
                colvarVourcherPageCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarVourcherPageCount);
                
                TableSchema.TableColumn colvarEnable = new TableSchema.TableColumn(schema);
                colvarEnable.ColumnName = "enable";
                colvarEnable.DataType = DbType.Boolean;
                colvarEnable.MaxLength = 0;
                colvarEnable.AutoIncrement = false;
                colvarEnable.IsNullable = true;
                colvarEnable.IsPrimaryKey = false;
                colvarEnable.IsForeignKey = false;
                colvarEnable.IsReadOnly = false;
                
                schema.Columns.Add(colvarEnable);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCityBit = new TableSchema.TableColumn(schema);
                colvarCityBit.ColumnName = "city_bit";
                colvarCityBit.DataType = DbType.Int32;
                colvarCityBit.MaxLength = 0;
                colvarCityBit.AutoIncrement = false;
                colvarCityBit.IsNullable = true;
                colvarCityBit.IsPrimaryKey = false;
                colvarCityBit.IsForeignKey = false;
                colvarCityBit.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityBit);
                
                TableSchema.TableColumn colvarEventPicUrl = new TableSchema.TableColumn(schema);
                colvarEventPicUrl.ColumnName = "event_pic_url";
                colvarEventPicUrl.DataType = DbType.AnsiString;
                colvarEventPicUrl.MaxLength = 500;
                colvarEventPicUrl.AutoIncrement = false;
                colvarEventPicUrl.IsNullable = true;
                colvarEventPicUrl.IsPrimaryKey = false;
                colvarEventPicUrl.IsForeignKey = false;
                colvarEventPicUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventPicUrl);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vourcher_store_facade",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVourcherStoreFacade()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVourcherStoreFacade(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVourcherStoreFacade(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVourcherStoreFacade(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("Phone")]
        [Bindable(true)]
        public string Phone 
	    {
		    get
		    {
			    return GetColumnValue<string>("phone");
		    }
            set 
		    {
			    SetColumnValue("phone", value);
            }
        }
	      
        [XmlAttribute("RegionId")]
        [Bindable(true)]
        public int? RegionId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("region_id");
		    }
            set 
		    {
			    SetColumnValue("region_id", value);
            }
        }
	      
        [XmlAttribute("RegionName")]
        [Bindable(true)]
        public string RegionName 
	    {
		    get
		    {
			    return GetColumnValue<string>("region_name");
		    }
            set 
		    {
			    SetColumnValue("region_name", value);
            }
        }
	      
        [XmlAttribute("City")]
        [Bindable(true)]
        public string City 
	    {
		    get
		    {
			    return GetColumnValue<string>("city");
		    }
            set 
		    {
			    SetColumnValue("city", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int? CityId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("Town")]
        [Bindable(true)]
        public string Town 
	    {
		    get
		    {
			    return GetColumnValue<string>("town");
		    }
            set 
		    {
			    SetColumnValue("town", value);
            }
        }
	      
        [XmlAttribute("TownshipId")]
        [Bindable(true)]
        public int? TownshipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("township_id");
		    }
            set 
		    {
			    SetColumnValue("township_id", value);
            }
        }
	      
        [XmlAttribute("AddressString")]
        [Bindable(true)]
        public string AddressString 
	    {
		    get
		    {
			    return GetColumnValue<string>("address_string");
		    }
            set 
		    {
			    SetColumnValue("address_string", value);
            }
        }
	      
        [XmlAttribute("Coordinate")]
        [Bindable(true)]
        public string Coordinate
        {
            get 
            {
                try
                {
                    return GetColumnValue<object>(Columns.Coordinate) != null ? GetColumnValue<object>(Columns.Coordinate).ToString() : string.Empty;
                }
                catch
                {
                    return GetColumnValue<string>(Columns.Coordinate) != null ? GetColumnValue<string>(Columns.Coordinate).ToString() : string.Empty;
                }
            }
            set { SetColumnValue(Columns.Coordinate, value); }
        }
	      
        [XmlAttribute("OpenTime")]
        [Bindable(true)]
        public string OpenTime 
	    {
		    get
		    {
			    return GetColumnValue<string>("open_time");
		    }
            set 
		    {
			    SetColumnValue("open_time", value);
            }
        }
	      
        [XmlAttribute("CloseDate")]
        [Bindable(true)]
        public string CloseDate 
	    {
		    get
		    {
			    return GetColumnValue<string>("close_date");
		    }
            set 
		    {
			    SetColumnValue("close_date", value);
            }
        }
	      
        [XmlAttribute("Remarks")]
        [Bindable(true)]
        public string Remarks 
	    {
		    get
		    {
			    return GetColumnValue<string>("remarks");
		    }
            set 
		    {
			    SetColumnValue("remarks", value);
            }
        }
	      
        [XmlAttribute("Mrt")]
        [Bindable(true)]
        public string Mrt 
	    {
		    get
		    {
			    return GetColumnValue<string>("mrt");
		    }
            set 
		    {
			    SetColumnValue("mrt", value);
            }
        }
	      
        [XmlAttribute("Car")]
        [Bindable(true)]
        public string Car 
	    {
		    get
		    {
			    return GetColumnValue<string>("car");
		    }
            set 
		    {
			    SetColumnValue("car", value);
            }
        }
	      
        [XmlAttribute("Bus")]
        [Bindable(true)]
        public string Bus 
	    {
		    get
		    {
			    return GetColumnValue<string>("bus");
		    }
            set 
		    {
			    SetColumnValue("bus", value);
            }
        }
	      
        [XmlAttribute("OtherVehicles")]
        [Bindable(true)]
        public string OtherVehicles 
	    {
		    get
		    {
			    return GetColumnValue<string>("other_vehicles");
		    }
            set 
		    {
			    SetColumnValue("other_vehicles", value);
            }
        }
	      
        [XmlAttribute("WebUrl")]
        [Bindable(true)]
        public string WebUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("web_url");
		    }
            set 
		    {
			    SetColumnValue("web_url", value);
            }
        }
	      
        [XmlAttribute("FacebookUrl")]
        [Bindable(true)]
        public string FacebookUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("facebook_url");
		    }
            set 
		    {
			    SetColumnValue("facebook_url", value);
            }
        }
	      
        [XmlAttribute("PlurkUrl")]
        [Bindable(true)]
        public string PlurkUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("plurk_url");
		    }
            set 
		    {
			    SetColumnValue("plurk_url", value);
            }
        }
	      
        [XmlAttribute("BlogUrl")]
        [Bindable(true)]
        public string BlogUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("blog_url");
		    }
            set 
		    {
			    SetColumnValue("blog_url", value);
            }
        }
	      
        [XmlAttribute("OtherUrl")]
        [Bindable(true)]
        public string OtherUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("other_url");
		    }
            set 
		    {
			    SetColumnValue("other_url", value);
            }
        }
	      
        [XmlAttribute("CreditcardAvailable")]
        [Bindable(true)]
        public bool CreditcardAvailable 
	    {
		    get
		    {
			    return GetColumnValue<bool>("creditcard_available");
		    }
            set 
		    {
			    SetColumnValue("creditcard_available", value);
            }
        }
	      
        [XmlAttribute("SellerConsumptionAvg")]
        [Bindable(true)]
        public int? SellerConsumptionAvg 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seller_consumption_avg");
		    }
            set 
		    {
			    SetColumnValue("seller_consumption_avg", value);
            }
        }
	      
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("VourcherEventId")]
        [Bindable(true)]
        public int VourcherEventId 
	    {
		    get
		    {
			    return GetColumnValue<int>("vourcher_event_id");
		    }
            set 
		    {
			    SetColumnValue("vourcher_event_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("PageCount")]
        [Bindable(true)]
        public int PageCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("page_count");
		    }
            set 
		    {
			    SetColumnValue("page_count", value);
            }
        }
	      
        [XmlAttribute("MaxQuantity")]
        [Bindable(true)]
        public int MaxQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("max_quantity");
		    }
            set 
		    {
			    SetColumnValue("max_quantity", value);
            }
        }
	      
        [XmlAttribute("CurrentQuantity")]
        [Bindable(true)]
        public int CurrentQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("current_quantity");
		    }
            set 
		    {
			    SetColumnValue("current_quantity", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("GUID");
		    }
            set 
		    {
			    SetColumnValue("GUID", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerLogoimgPath")]
        [Bindable(true)]
        public string SellerLogoimgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_logoimg_path");
		    }
            set 
		    {
			    SetColumnValue("seller_logoimg_path", value);
            }
        }
	      
        [XmlAttribute("SellerCategory")]
        [Bindable(true)]
        public int? SellerCategory 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seller_category");
		    }
            set 
		    {
			    SetColumnValue("seller_category", value);
            }
        }
	      
        [XmlAttribute("Contents")]
        [Bindable(true)]
        public string Contents 
	    {
		    get
		    {
			    return GetColumnValue<string>("contents");
		    }
            set 
		    {
			    SetColumnValue("contents", value);
            }
        }
	      
        [XmlAttribute("StartDate")]
        [Bindable(true)]
        public DateTime? StartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_date");
		    }
            set 
		    {
			    SetColumnValue("start_date", value);
            }
        }
	      
        [XmlAttribute("VourcherPageCount")]
        [Bindable(true)]
        public int? VourcherPageCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vourcher_page_count");
		    }
            set 
		    {
			    SetColumnValue("vourcher_page_count", value);
            }
        }
	      
        [XmlAttribute("Enable")]
        [Bindable(true)]
        public bool? Enable 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("enable");
		    }
            set 
		    {
			    SetColumnValue("enable", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int? Status 
	    {
		    get
		    {
			    return GetColumnValue<int?>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("CityBit")]
        [Bindable(true)]
        public int? CityBit 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_bit");
		    }
            set 
		    {
			    SetColumnValue("city_bit", value);
            }
        }
	      
        [XmlAttribute("EventPicUrl")]
        [Bindable(true)]
        public string EventPicUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("event_pic_url");
		    }
            set 
		    {
			    SetColumnValue("event_pic_url", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string StoreName = @"store_name";
            
            public static string Phone = @"phone";
            
            public static string RegionId = @"region_id";
            
            public static string RegionName = @"region_name";
            
            public static string City = @"city";
            
            public static string CityId = @"city_id";
            
            public static string Town = @"town";
            
            public static string TownshipId = @"township_id";
            
            public static string AddressString = @"address_string";
            
            public static string Coordinate = @"coordinate";
            
            public static string OpenTime = @"open_time";
            
            public static string CloseDate = @"close_date";
            
            public static string Remarks = @"remarks";
            
            public static string Mrt = @"mrt";
            
            public static string Car = @"car";
            
            public static string Bus = @"bus";
            
            public static string OtherVehicles = @"other_vehicles";
            
            public static string WebUrl = @"web_url";
            
            public static string FacebookUrl = @"facebook_url";
            
            public static string PlurkUrl = @"plurk_url";
            
            public static string BlogUrl = @"blog_url";
            
            public static string OtherUrl = @"other_url";
            
            public static string CreditcardAvailable = @"creditcard_available";
            
            public static string SellerConsumptionAvg = @"seller_consumption_avg";
            
            public static string Id = @"id";
            
            public static string VourcherEventId = @"vourcher_event_id";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string StoreGuid = @"store_guid";
            
            public static string PageCount = @"page_count";
            
            public static string MaxQuantity = @"max_quantity";
            
            public static string CurrentQuantity = @"current_quantity";
            
            public static string SellerId = @"seller_id";
            
            public static string Guid = @"GUID";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerLogoimgPath = @"seller_logoimg_path";
            
            public static string SellerCategory = @"seller_category";
            
            public static string Contents = @"contents";
            
            public static string StartDate = @"start_date";
            
            public static string VourcherPageCount = @"vourcher_page_count";
            
            public static string Enable = @"enable";
            
            public static string Status = @"status";
            
            public static string CityBit = @"city_bit";
            
            public static string EventPicUrl = @"event_pic_url";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpAssignment class.
	/// </summary>
    [Serializable]
	public partial class PcpAssignmentCollection : RepositoryList<PcpAssignment, PcpAssignmentCollection>
	{	   
		public PcpAssignmentCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpAssignmentCollection</returns>
		public PcpAssignmentCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpAssignment o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_assignment table.
	/// </summary>
	[Serializable]
	public partial class PcpAssignment : RepositoryRecord<PcpAssignment>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpAssignment()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpAssignment(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_assignment", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarAssignmentType = new TableSchema.TableColumn(schema);
				colvarAssignmentType.ColumnName = "assignment_type";
				colvarAssignmentType.DataType = DbType.Int32;
				colvarAssignmentType.MaxLength = 0;
				colvarAssignmentType.AutoIncrement = false;
				colvarAssignmentType.IsNullable = false;
				colvarAssignmentType.IsPrimaryKey = false;
				colvarAssignmentType.IsForeignKey = false;
				colvarAssignmentType.IsReadOnly = false;
				colvarAssignmentType.DefaultSetting = @"";
				colvarAssignmentType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAssignmentType);
				
				TableSchema.TableColumn colvarSendType = new TableSchema.TableColumn(schema);
				colvarSendType.ColumnName = "send_type";
				colvarSendType.DataType = DbType.Int32;
				colvarSendType.MaxLength = 0;
				colvarSendType.AutoIncrement = false;
				colvarSendType.IsNullable = false;
				colvarSendType.IsPrimaryKey = false;
				colvarSendType.IsForeignKey = false;
				colvarSendType.IsReadOnly = false;
				colvarSendType.DefaultSetting = @"";
				colvarSendType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendType);
				
				TableSchema.TableColumn colvarMessageType = new TableSchema.TableColumn(schema);
				colvarMessageType.ColumnName = "message_type";
				colvarMessageType.DataType = DbType.Int32;
				colvarMessageType.MaxLength = 0;
				colvarMessageType.AutoIncrement = false;
				colvarMessageType.IsNullable = false;
				colvarMessageType.IsPrimaryKey = false;
				colvarMessageType.IsForeignKey = false;
				colvarMessageType.IsReadOnly = false;
				colvarMessageType.DefaultSetting = @"";
				colvarMessageType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessageType);
				
				TableSchema.TableColumn colvarDiscountCampaignId = new TableSchema.TableColumn(schema);
				colvarDiscountCampaignId.ColumnName = "discount_campaign_id";
				colvarDiscountCampaignId.DataType = DbType.Int32;
				colvarDiscountCampaignId.MaxLength = 0;
				colvarDiscountCampaignId.AutoIncrement = false;
				colvarDiscountCampaignId.IsNullable = true;
				colvarDiscountCampaignId.IsPrimaryKey = false;
				colvarDiscountCampaignId.IsForeignKey = false;
				colvarDiscountCampaignId.IsReadOnly = false;
				colvarDiscountCampaignId.DefaultSetting = @"";
				colvarDiscountCampaignId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountCampaignId);
				
				TableSchema.TableColumn colvarMembershipCardGroupId = new TableSchema.TableColumn(schema);
				colvarMembershipCardGroupId.ColumnName = "membership_card_group_id";
				colvarMembershipCardGroupId.DataType = DbType.Int32;
				colvarMembershipCardGroupId.MaxLength = 0;
				colvarMembershipCardGroupId.AutoIncrement = false;
				colvarMembershipCardGroupId.IsNullable = true;
				colvarMembershipCardGroupId.IsPrimaryKey = false;
				colvarMembershipCardGroupId.IsForeignKey = false;
				colvarMembershipCardGroupId.IsReadOnly = false;
				colvarMembershipCardGroupId.DefaultSetting = @"";
				colvarMembershipCardGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMembershipCardGroupId);
				
				TableSchema.TableColumn colvarExecutionType = new TableSchema.TableColumn(schema);
				colvarExecutionType.ColumnName = "execution_type";
				colvarExecutionType.DataType = DbType.Int32;
				colvarExecutionType.MaxLength = 0;
				colvarExecutionType.AutoIncrement = false;
				colvarExecutionType.IsNullable = false;
				colvarExecutionType.IsPrimaryKey = false;
				colvarExecutionType.IsForeignKey = false;
				colvarExecutionType.IsReadOnly = false;
				colvarExecutionType.DefaultSetting = @"";
				colvarExecutionType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExecutionType);
				
				TableSchema.TableColumn colvarExecutionTime = new TableSchema.TableColumn(schema);
				colvarExecutionTime.ColumnName = "execution_time";
				colvarExecutionTime.DataType = DbType.DateTime;
				colvarExecutionTime.MaxLength = 0;
				colvarExecutionTime.AutoIncrement = false;
				colvarExecutionTime.IsNullable = false;
				colvarExecutionTime.IsPrimaryKey = false;
				colvarExecutionTime.IsForeignKey = false;
				colvarExecutionTime.IsReadOnly = false;
				colvarExecutionTime.DefaultSetting = @"";
				colvarExecutionTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExecutionTime);
				
				TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
				colvarStartTime.ColumnName = "start_time";
				colvarStartTime.DataType = DbType.DateTime;
				colvarStartTime.MaxLength = 0;
				colvarStartTime.AutoIncrement = false;
				colvarStartTime.IsNullable = true;
				colvarStartTime.IsPrimaryKey = false;
				colvarStartTime.IsForeignKey = false;
				colvarStartTime.IsReadOnly = false;
				colvarStartTime.DefaultSetting = @"";
				colvarStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartTime);
				
				TableSchema.TableColumn colvarCompleteTime = new TableSchema.TableColumn(schema);
				colvarCompleteTime.ColumnName = "complete_time";
				colvarCompleteTime.DataType = DbType.DateTime;
				colvarCompleteTime.MaxLength = 0;
				colvarCompleteTime.AutoIncrement = false;
				colvarCompleteTime.IsNullable = true;
				colvarCompleteTime.IsPrimaryKey = false;
				colvarCompleteTime.IsForeignKey = false;
				colvarCompleteTime.IsReadOnly = false;
				colvarCompleteTime.DefaultSetting = @"";
				colvarCompleteTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompleteTime);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCancelTime = new TableSchema.TableColumn(schema);
				colvarCancelTime.ColumnName = "cancel_time";
				colvarCancelTime.DataType = DbType.DateTime;
				colvarCancelTime.MaxLength = 0;
				colvarCancelTime.AutoIncrement = false;
				colvarCancelTime.IsNullable = true;
				colvarCancelTime.IsPrimaryKey = false;
				colvarCancelTime.IsForeignKey = false;
				colvarCancelTime.IsReadOnly = false;
				colvarCancelTime.DefaultSetting = @"";
				colvarCancelTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelTime);
				
				TableSchema.TableColumn colvarCancelId = new TableSchema.TableColumn(schema);
				colvarCancelId.ColumnName = "cancel_id";
				colvarCancelId.DataType = DbType.Int32;
				colvarCancelId.MaxLength = 0;
				colvarCancelId.AutoIncrement = false;
				colvarCancelId.IsNullable = true;
				colvarCancelId.IsPrimaryKey = false;
				colvarCancelId.IsForeignKey = false;
				colvarCancelId.IsReadOnly = false;
				colvarCancelId.DefaultSetting = @"";
				colvarCancelId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelId);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = -1;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = false;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarSendCount = new TableSchema.TableColumn(schema);
				colvarSendCount.ColumnName = "send_count";
				colvarSendCount.DataType = DbType.Int32;
				colvarSendCount.MaxLength = 0;
				colvarSendCount.AutoIncrement = false;
				colvarSendCount.IsNullable = false;
				colvarSendCount.IsPrimaryKey = false;
				colvarSendCount.IsForeignKey = false;
				colvarSendCount.IsReadOnly = false;
				
						colvarSendCount.DefaultSetting = @"((0))";
				colvarSendCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendCount);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = true;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarMemberType = new TableSchema.TableColumn(schema);
				colvarMemberType.ColumnName = "member_type";
				colvarMemberType.DataType = DbType.Byte;
				colvarMemberType.MaxLength = 0;
				colvarMemberType.AutoIncrement = false;
				colvarMemberType.IsNullable = false;
				colvarMemberType.IsPrimaryKey = false;
				colvarMemberType.IsForeignKey = false;
				colvarMemberType.IsReadOnly = false;
				
						colvarMemberType.DefaultSetting = @"((0))";
				colvarMemberType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberType);
				
				TableSchema.TableColumn colvarSubject = new TableSchema.TableColumn(schema);
				colvarSubject.ColumnName = "subject";
				colvarSubject.DataType = DbType.String;
				colvarSubject.MaxLength = -1;
				colvarSubject.AutoIncrement = false;
				colvarSubject.IsNullable = false;
				colvarSubject.IsPrimaryKey = false;
				colvarSubject.IsForeignKey = false;
				colvarSubject.IsReadOnly = false;
				
						colvarSubject.DefaultSetting = @"('')";
				colvarSubject.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubject);
				
				TableSchema.TableColumn colvarDiscountTemplateId = new TableSchema.TableColumn(schema);
				colvarDiscountTemplateId.ColumnName = "discount_template_id";
				colvarDiscountTemplateId.DataType = DbType.Int32;
				colvarDiscountTemplateId.MaxLength = 0;
				colvarDiscountTemplateId.AutoIncrement = false;
				colvarDiscountTemplateId.IsNullable = true;
				colvarDiscountTemplateId.IsPrimaryKey = false;
				colvarDiscountTemplateId.IsForeignKey = false;
				colvarDiscountTemplateId.IsReadOnly = false;
				colvarDiscountTemplateId.DefaultSetting = @"";
				colvarDiscountTemplateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountTemplateId);
				
				TableSchema.TableColumn colvarSuccessCount = new TableSchema.TableColumn(schema);
				colvarSuccessCount.ColumnName = "success_count";
				colvarSuccessCount.DataType = DbType.Int32;
				colvarSuccessCount.MaxLength = 0;
				colvarSuccessCount.AutoIncrement = false;
				colvarSuccessCount.IsNullable = true;
				colvarSuccessCount.IsPrimaryKey = false;
				colvarSuccessCount.IsForeignKey = false;
				colvarSuccessCount.IsReadOnly = false;
				colvarSuccessCount.DefaultSetting = @"";
				colvarSuccessCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSuccessCount);
				
				TableSchema.TableColumn colvarPcpPointType = new TableSchema.TableColumn(schema);
				colvarPcpPointType.ColumnName = "pcp_point_type";
				colvarPcpPointType.DataType = DbType.Byte;
				colvarPcpPointType.MaxLength = 0;
				colvarPcpPointType.AutoIncrement = false;
				colvarPcpPointType.IsNullable = false;
				colvarPcpPointType.IsPrimaryKey = false;
				colvarPcpPointType.IsForeignKey = false;
				colvarPcpPointType.IsReadOnly = false;
				
						colvarPcpPointType.DefaultSetting = @"((0))";
				colvarPcpPointType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPcpPointType);
				
				TableSchema.TableColumn colvarFilterCount = new TableSchema.TableColumn(schema);
				colvarFilterCount.ColumnName = "filter_count";
				colvarFilterCount.DataType = DbType.Int32;
				colvarFilterCount.MaxLength = 0;
				colvarFilterCount.AutoIncrement = false;
				colvarFilterCount.IsNullable = true;
				colvarFilterCount.IsPrimaryKey = false;
				colvarFilterCount.IsForeignKey = false;
				colvarFilterCount.IsReadOnly = false;
				colvarFilterCount.DefaultSetting = @"";
				colvarFilterCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFilterCount);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_assignment",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("AssignmentType")]
		[Bindable(true)]
		public int AssignmentType 
		{
			get { return GetColumnValue<int>(Columns.AssignmentType); }
			set { SetColumnValue(Columns.AssignmentType, value); }
		}
		  
		[XmlAttribute("SendType")]
		[Bindable(true)]
		public int SendType 
		{
			get { return GetColumnValue<int>(Columns.SendType); }
			set { SetColumnValue(Columns.SendType, value); }
		}
		  
		[XmlAttribute("MessageType")]
		[Bindable(true)]
		public int MessageType 
		{
			get { return GetColumnValue<int>(Columns.MessageType); }
			set { SetColumnValue(Columns.MessageType, value); }
		}
		  
		[XmlAttribute("DiscountCampaignId")]
		[Bindable(true)]
		public int? DiscountCampaignId 
		{
			get { return GetColumnValue<int?>(Columns.DiscountCampaignId); }
			set { SetColumnValue(Columns.DiscountCampaignId, value); }
		}
		  
		[XmlAttribute("MembershipCardGroupId")]
		[Bindable(true)]
		public int? MembershipCardGroupId 
		{
			get { return GetColumnValue<int?>(Columns.MembershipCardGroupId); }
			set { SetColumnValue(Columns.MembershipCardGroupId, value); }
		}
		  
		[XmlAttribute("ExecutionType")]
		[Bindable(true)]
		public int ExecutionType 
		{
			get { return GetColumnValue<int>(Columns.ExecutionType); }
			set { SetColumnValue(Columns.ExecutionType, value); }
		}
		  
		[XmlAttribute("ExecutionTime")]
		[Bindable(true)]
		public DateTime ExecutionTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ExecutionTime); }
			set { SetColumnValue(Columns.ExecutionTime, value); }
		}
		  
		[XmlAttribute("StartTime")]
		[Bindable(true)]
		public DateTime? StartTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.StartTime); }
			set { SetColumnValue(Columns.StartTime, value); }
		}
		  
		[XmlAttribute("CompleteTime")]
		[Bindable(true)]
		public DateTime? CompleteTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CompleteTime); }
			set { SetColumnValue(Columns.CompleteTime, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CancelTime")]
		[Bindable(true)]
		public DateTime? CancelTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CancelTime); }
			set { SetColumnValue(Columns.CancelTime, value); }
		}
		  
		[XmlAttribute("CancelId")]
		[Bindable(true)]
		public int? CancelId 
		{
			get { return GetColumnValue<int?>(Columns.CancelId); }
			set { SetColumnValue(Columns.CancelId, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("SendCount")]
		[Bindable(true)]
		public int SendCount 
		{
			get { return GetColumnValue<int>(Columns.SendCount); }
			set { SetColumnValue(Columns.SendCount, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid? StoreGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("MemberType")]
		[Bindable(true)]
		public byte MemberType 
		{
			get { return GetColumnValue<byte>(Columns.MemberType); }
			set { SetColumnValue(Columns.MemberType, value); }
		}
		  
		[XmlAttribute("Subject")]
		[Bindable(true)]
		public string Subject 
		{
			get { return GetColumnValue<string>(Columns.Subject); }
			set { SetColumnValue(Columns.Subject, value); }
		}
		  
		[XmlAttribute("DiscountTemplateId")]
		[Bindable(true)]
		public int? DiscountTemplateId 
		{
			get { return GetColumnValue<int?>(Columns.DiscountTemplateId); }
			set { SetColumnValue(Columns.DiscountTemplateId, value); }
		}
		  
		[XmlAttribute("SuccessCount")]
		[Bindable(true)]
		public int? SuccessCount 
		{
			get { return GetColumnValue<int?>(Columns.SuccessCount); }
			set { SetColumnValue(Columns.SuccessCount, value); }
		}
		  
		[XmlAttribute("PcpPointType")]
		[Bindable(true)]
		public byte PcpPointType 
		{
			get { return GetColumnValue<byte>(Columns.PcpPointType); }
			set { SetColumnValue(Columns.PcpPointType, value); }
		}
		  
		[XmlAttribute("FilterCount")]
		[Bindable(true)]
		public int? FilterCount 
		{
			get { return GetColumnValue<int?>(Columns.FilterCount); }
			set { SetColumnValue(Columns.FilterCount, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AssignmentTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SendTypeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageTypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountCampaignIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn MembershipCardGroupIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ExecutionTypeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ExecutionTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn StartTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CompleteTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelIdColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn SendCountColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberTypeColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn SubjectColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountTemplateIdColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn SuccessCountColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn PcpPointTypeColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn FilterCountColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string UserId = @"user_id";
			 public static string Status = @"status";
			 public static string AssignmentType = @"assignment_type";
			 public static string SendType = @"send_type";
			 public static string MessageType = @"message_type";
			 public static string DiscountCampaignId = @"discount_campaign_id";
			 public static string MembershipCardGroupId = @"membership_card_group_id";
			 public static string ExecutionType = @"execution_type";
			 public static string ExecutionTime = @"execution_time";
			 public static string StartTime = @"start_time";
			 public static string CompleteTime = @"complete_time";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
			 public static string CancelTime = @"cancel_time";
			 public static string CancelId = @"cancel_id";
			 public static string Message = @"message";
			 public static string SendCount = @"send_count";
			 public static string StoreGuid = @"store_guid";
			 public static string MemberType = @"member_type";
			 public static string Subject = @"subject";
			 public static string DiscountTemplateId = @"discount_template_id";
			 public static string SuccessCount = @"success_count";
			 public static string PcpPointType = @"pcp_point_type";
			 public static string FilterCount = @"filter_count";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

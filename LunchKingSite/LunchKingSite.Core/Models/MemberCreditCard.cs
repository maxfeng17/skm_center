using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the MemberCreditCard class.
    /// </summary>
    [Serializable]
    public partial class MemberCreditCardCollection : RepositoryList<MemberCreditCard, MemberCreditCardCollection>
    {
        public MemberCreditCardCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberCreditCardCollection</returns>
        public MemberCreditCardCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberCreditCard o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }


    /// <summary>
    /// This is an ActiveRecord class which wraps the member_credit_card table.
    /// </summary>

    [Serializable]
    public partial class MemberCreditCard : RepositoryRecord<MemberCreditCard>, IRecordBase
    {
        #region .ctors and Default Settings

        public MemberCreditCard()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public MemberCreditCard(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("member_credit_card", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarCreditCardInfo = new TableSchema.TableColumn(schema);
                colvarCreditCardInfo.ColumnName = "credit_card_info";
                colvarCreditCardInfo.DataType = DbType.AnsiString;
                colvarCreditCardInfo.MaxLength = 256;
                colvarCreditCardInfo.AutoIncrement = false;
                colvarCreditCardInfo.IsNullable = false;
                colvarCreditCardInfo.IsPrimaryKey = false;
                colvarCreditCardInfo.IsForeignKey = false;
                colvarCreditCardInfo.IsReadOnly = false;
                colvarCreditCardInfo.DefaultSetting = @"";
                colvarCreditCardInfo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreditCardInfo);

                TableSchema.TableColumn colvarCreditCardTail = new TableSchema.TableColumn(schema);
                colvarCreditCardTail.ColumnName = "credit_card_tail";
                colvarCreditCardTail.DataType = DbType.AnsiStringFixedLength;
                colvarCreditCardTail.MaxLength = 4;
                colvarCreditCardTail.AutoIncrement = false;
                colvarCreditCardTail.IsNullable = false;
                colvarCreditCardTail.IsPrimaryKey = false;
                colvarCreditCardTail.IsForeignKey = false;
                colvarCreditCardTail.IsReadOnly = false;
                colvarCreditCardTail.DefaultSetting = @"";
                colvarCreditCardTail.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreditCardTail);

                TableSchema.TableColumn colvarCreditCardName = new TableSchema.TableColumn(schema);
                colvarCreditCardName.ColumnName = "credit_card_name";
                colvarCreditCardName.DataType = DbType.String;
                colvarCreditCardName.MaxLength = 100;
                colvarCreditCardName.AutoIncrement = false;
                colvarCreditCardName.IsNullable = true;
                colvarCreditCardName.IsPrimaryKey = false;
                colvarCreditCardName.IsForeignKey = false;
                colvarCreditCardName.IsReadOnly = false;
                colvarCreditCardName.DefaultSetting = @"";
                colvarCreditCardName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreditCardName);

                TableSchema.TableColumn colvarLastUseTime = new TableSchema.TableColumn(schema);
                colvarLastUseTime.ColumnName = "last_use_time";
                colvarLastUseTime.DataType = DbType.DateTime;
                colvarLastUseTime.MaxLength = 0;
                colvarLastUseTime.AutoIncrement = false;
                colvarLastUseTime.IsNullable = true;
                colvarLastUseTime.IsPrimaryKey = false;
                colvarLastUseTime.IsForeignKey = false;
                colvarLastUseTime.IsReadOnly = false;
                colvarLastUseTime.DefaultSetting = @"";
                colvarLastUseTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastUseTime);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarEncryptSalt = new TableSchema.TableColumn(schema);
                colvarEncryptSalt.ColumnName = "encrypt_salt";
                colvarEncryptSalt.DataType = DbType.String;
                colvarEncryptSalt.MaxLength = 128;
                colvarEncryptSalt.AutoIncrement = false;
                colvarEncryptSalt.IsNullable = false;
                colvarEncryptSalt.IsPrimaryKey = false;
                colvarEncryptSalt.IsForeignKey = false;
                colvarEncryptSalt.IsReadOnly = false;
                colvarEncryptSalt.DefaultSetting = @"";
                colvarEncryptSalt.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEncryptSalt);

                TableSchema.TableColumn colvarCreditCardYear = new TableSchema.TableColumn(schema);
                colvarCreditCardYear.ColumnName = "credit_card_year";
                colvarCreditCardYear.DataType = DbType.AnsiStringFixedLength;
                colvarCreditCardYear.MaxLength = 2;
                colvarCreditCardYear.AutoIncrement = false;
                colvarCreditCardYear.IsNullable = false;
                colvarCreditCardYear.IsPrimaryKey = false;
                colvarCreditCardYear.IsForeignKey = false;
                colvarCreditCardYear.IsReadOnly = false;

                colvarCreditCardYear.DefaultSetting = @"('00')";
                colvarCreditCardYear.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreditCardYear);

                TableSchema.TableColumn colvarCreditCardMonth = new TableSchema.TableColumn(schema);
                colvarCreditCardMonth.ColumnName = "credit_card_month";
                colvarCreditCardMonth.DataType = DbType.AnsiStringFixedLength;
                colvarCreditCardMonth.MaxLength = 2;
                colvarCreditCardMonth.AutoIncrement = false;
                colvarCreditCardMonth.IsNullable = false;
                colvarCreditCardMonth.IsPrimaryKey = false;
                colvarCreditCardMonth.IsForeignKey = false;
                colvarCreditCardMonth.IsReadOnly = false;

                colvarCreditCardMonth.DefaultSetting = @"('01')";
                colvarCreditCardMonth.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreditCardMonth);

                TableSchema.TableColumn colvarCardType = new TableSchema.TableColumn(schema);
                colvarCardType.ColumnName = "card_type";
                colvarCardType.DataType = DbType.Int32;
                colvarCardType.MaxLength = 0;
                colvarCardType.AutoIncrement = false;
                colvarCardType.IsNullable = false;
                colvarCardType.IsPrimaryKey = false;
                colvarCardType.IsForeignKey = false;
                colvarCardType.IsReadOnly = false;

                colvarCardType.DefaultSetting = @"((0))";
                colvarCardType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardType);

                TableSchema.TableColumn colvarBankId = new TableSchema.TableColumn(schema);
                colvarBankId.ColumnName = "bank_id";
                colvarBankId.DataType = DbType.Int32;
                colvarBankId.MaxLength = 0;
                colvarBankId.AutoIncrement = false;
                colvarBankId.IsNullable = false;
                colvarBankId.IsPrimaryKey = false;
                colvarBankId.IsForeignKey = false;
                colvarBankId.IsReadOnly = false;

                colvarBankId.DefaultSetting = @"((0))";
                colvarBankId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBankId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarBin = new TableSchema.TableColumn(schema);
                colvarBin.ColumnName = "bin";
                colvarBin.DataType = DbType.String;
                colvarBin.MaxLength = 20;
                colvarBin.AutoIncrement = false;
                colvarBin.IsNullable = true;
                colvarBin.IsPrimaryKey = false;
                colvarBin.IsForeignKey = false;
                colvarBin.IsReadOnly = false;
                colvarBin.DefaultSetting = @"";
                colvarBin.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBin);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("member_credit_card", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get { return GetColumnValue<int>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("CreditCardInfo")]
        [Bindable(true)]
        public string CreditCardInfo
        {
            get { return GetColumnValue<string>(Columns.CreditCardInfo); }
            set { SetColumnValue(Columns.CreditCardInfo, value); }
        }

        [XmlAttribute("CreditCardTail")]
        [Bindable(true)]
        public string CreditCardTail
        {
            get { return GetColumnValue<string>(Columns.CreditCardTail); }
            set { SetColumnValue(Columns.CreditCardTail, value); }
        }

        [XmlAttribute("CreditCardName")]
        [Bindable(true)]
        public string CreditCardName
        {
            get { return GetColumnValue<string>(Columns.CreditCardName); }
            set { SetColumnValue(Columns.CreditCardName, value); }
        }

        [XmlAttribute("LastUseTime")]
        [Bindable(true)]
        public DateTime? LastUseTime
        {
            get { return GetColumnValue<DateTime?>(Columns.LastUseTime); }
            set { SetColumnValue(Columns.LastUseTime, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("EncryptSalt")]
        [Bindable(true)]
        public string EncryptSalt
        {
            get { return GetColumnValue<string>(Columns.EncryptSalt); }
            set { SetColumnValue(Columns.EncryptSalt, value); }
        }

        [XmlAttribute("CreditCardYear")]
        [Bindable(true)]
        public string CreditCardYear
        {
            get { return GetColumnValue<string>(Columns.CreditCardYear); }
            set { SetColumnValue(Columns.CreditCardYear, value); }
        }

        [XmlAttribute("CreditCardMonth")]
        [Bindable(true)]
        public string CreditCardMonth
        {
            get { return GetColumnValue<string>(Columns.CreditCardMonth); }
            set { SetColumnValue(Columns.CreditCardMonth, value); }
        }

        [XmlAttribute("CardType")]
        [Bindable(true)]
        public int CardType
        {
            get { return GetColumnValue<int>(Columns.CardType); }
            set { SetColumnValue(Columns.CardType, value); }
        }

        [XmlAttribute("BankId")]
        [Bindable(true)]
        public int BankId
        {
            get { return GetColumnValue<int>(Columns.BankId); }
            set { SetColumnValue(Columns.BankId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("Bin")]
        [Bindable(true)]
        public string Bin
        {
            get { return GetColumnValue<string>(Columns.Bin); }
            set { SetColumnValue(Columns.Bin, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn CreditCardInfoColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreditCardTailColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreditCardNameColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn LastUseTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn EncryptSaltColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CreditCardYearColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn CreditCardMonthColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn CardTypeColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn BankIdColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn BinColumn
        {
            get { return Schema.Columns[14]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string UserId = @"user_id";
            public static string CreditCardInfo = @"credit_card_info";
            public static string CreditCardTail = @"credit_card_tail";
            public static string CreditCardName = @"credit_card_name";
            public static string LastUseTime = @"last_use_time";
            public static string CreateTime = @"create_time";
            public static string Guid = @"guid";
            public static string EncryptSalt = @"encrypt_salt";
            public static string CreditCardYear = @"credit_card_year";
            public static string CreditCardMonth = @"credit_card_month";
            public static string CardType = @"card_type";
            public static string BankId = @"bank_id";
            public static string ModifyTime = @"modify_time";
            public static string Bin = @"bin";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

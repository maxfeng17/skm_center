using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealReturnedCoupon class.
	/// </summary>
    [Serializable]
	public partial class HiDealReturnedCouponCollection : RepositoryList<HiDealReturnedCoupon, HiDealReturnedCouponCollection>
	{	   
		public HiDealReturnedCouponCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealReturnedCouponCollection</returns>
		public HiDealReturnedCouponCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealReturnedCoupon o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_returned_coupon table.
	/// </summary>
	[Serializable]
	public partial class HiDealReturnedCoupon : RepositoryRecord<HiDealReturnedCoupon>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealReturnedCoupon()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealReturnedCoupon(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_returned_coupon", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarReturnedId = new TableSchema.TableColumn(schema);
				colvarReturnedId.ColumnName = "returned_id";
				colvarReturnedId.DataType = DbType.Int32;
				colvarReturnedId.MaxLength = 0;
				colvarReturnedId.AutoIncrement = false;
				colvarReturnedId.IsNullable = false;
				colvarReturnedId.IsPrimaryKey = false;
				colvarReturnedId.IsForeignKey = true;
				colvarReturnedId.IsReadOnly = false;
				colvarReturnedId.DefaultSetting = @"";
				
					colvarReturnedId.ForeignKeyTableName = "hi_deal_returned";
				schema.Columns.Add(colvarReturnedId);
				
				TableSchema.TableColumn colvarReturnedDetailId = new TableSchema.TableColumn(schema);
				colvarReturnedDetailId.ColumnName = "returned_detail_id";
				colvarReturnedDetailId.DataType = DbType.Int32;
				colvarReturnedDetailId.MaxLength = 0;
				colvarReturnedDetailId.AutoIncrement = false;
				colvarReturnedDetailId.IsNullable = false;
				colvarReturnedDetailId.IsPrimaryKey = false;
				colvarReturnedDetailId.IsForeignKey = true;
				colvarReturnedDetailId.IsReadOnly = false;
				colvarReturnedDetailId.DefaultSetting = @"";
				
					colvarReturnedDetailId.ForeignKeyTableName = "hi_deal_returned_detail";
				schema.Columns.Add(colvarReturnedDetailId);
				
				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "coupon_id";
				colvarCouponId.DataType = DbType.Int64;
				colvarCouponId.MaxLength = 0;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = false;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = true;
				colvarCouponId.IsReadOnly = false;
				colvarCouponId.DefaultSetting = @"";
				
					colvarCouponId.ForeignKeyTableName = "hi_deal_coupon";
				schema.Columns.Add(colvarCouponId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_returned_coupon",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ReturnedId")]
		[Bindable(true)]
		public int ReturnedId 
		{
			get { return GetColumnValue<int>(Columns.ReturnedId); }
			set { SetColumnValue(Columns.ReturnedId, value); }
		}
		  
		[XmlAttribute("ReturnedDetailId")]
		[Bindable(true)]
		public int ReturnedDetailId 
		{
			get { return GetColumnValue<int>(Columns.ReturnedDetailId); }
			set { SetColumnValue(Columns.ReturnedDetailId, value); }
		}
		  
		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public long CouponId 
		{
			get { return GetColumnValue<long>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (3)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnedIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnedDetailIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ReturnedId = @"returned_id";
			 public static string ReturnedDetailId = @"returned_detail_id";
			 public static string CouponId = @"coupon_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

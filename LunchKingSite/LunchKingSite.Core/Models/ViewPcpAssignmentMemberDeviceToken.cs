using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpAssignmentMemberDeviceToken class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpAssignmentMemberDeviceTokenCollection : ReadOnlyList<ViewPcpAssignmentMemberDeviceToken, ViewPcpAssignmentMemberDeviceTokenCollection>
    {        
        public ViewPcpAssignmentMemberDeviceTokenCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_assignment_member_device_token view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpAssignmentMemberDeviceToken : ReadOnlyRecord<ViewPcpAssignmentMemberDeviceToken>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_assignment_member_device_token", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int64;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarAssignmentId = new TableSchema.TableColumn(schema);
                colvarAssignmentId.ColumnName = "assignment_id";
                colvarAssignmentId.DataType = DbType.Int32;
                colvarAssignmentId.MaxLength = 0;
                colvarAssignmentId.AutoIncrement = false;
                colvarAssignmentId.IsNullable = false;
                colvarAssignmentId.IsPrimaryKey = false;
                colvarAssignmentId.IsForeignKey = false;
                colvarAssignmentId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAssignmentId);
                
                TableSchema.TableColumn colvarMemberId = new TableSchema.TableColumn(schema);
                colvarMemberId.ColumnName = "member_id";
                colvarMemberId.DataType = DbType.Int32;
                colvarMemberId.MaxLength = 0;
                colvarMemberId.AutoIncrement = false;
                colvarMemberId.IsNullable = false;
                colvarMemberId.IsPrimaryKey = false;
                colvarMemberId.IsForeignKey = false;
                colvarMemberId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberId);
                
                TableSchema.TableColumn colvarMemberType = new TableSchema.TableColumn(schema);
                colvarMemberType.ColumnName = "member_type";
                colvarMemberType.DataType = DbType.Int32;
                colvarMemberType.MaxLength = 0;
                colvarMemberType.AutoIncrement = false;
                colvarMemberType.IsNullable = false;
                colvarMemberType.IsPrimaryKey = false;
                colvarMemberType.IsForeignKey = false;
                colvarMemberType.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberType);
                
                TableSchema.TableColumn colvarSendTime = new TableSchema.TableColumn(schema);
                colvarSendTime.ColumnName = "send_time";
                colvarSendTime.DataType = DbType.DateTime;
                colvarSendTime.MaxLength = 0;
                colvarSendTime.AutoIncrement = false;
                colvarSendTime.IsNullable = true;
                colvarSendTime.IsPrimaryKey = false;
                colvarSendTime.IsForeignKey = false;
                colvarSendTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarSendTime);
                
                TableSchema.TableColumn colvarDiscountCodeId = new TableSchema.TableColumn(schema);
                colvarDiscountCodeId.ColumnName = "discount_code_id";
                colvarDiscountCodeId.DataType = DbType.Int32;
                colvarDiscountCodeId.MaxLength = 0;
                colvarDiscountCodeId.AutoIncrement = false;
                colvarDiscountCodeId.IsNullable = true;
                colvarDiscountCodeId.IsPrimaryKey = false;
                colvarDiscountCodeId.IsForeignKey = false;
                colvarDiscountCodeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountCodeId);
                
                TableSchema.TableColumn colvarDeviceId = new TableSchema.TableColumn(schema);
                colvarDeviceId.ColumnName = "device_id";
                colvarDeviceId.DataType = DbType.Int32;
                colvarDeviceId.MaxLength = 0;
                colvarDeviceId.AutoIncrement = false;
                colvarDeviceId.IsNullable = false;
                colvarDeviceId.IsPrimaryKey = false;
                colvarDeviceId.IsForeignKey = false;
                colvarDeviceId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeviceId);
                
                TableSchema.TableColumn colvarDevice = new TableSchema.TableColumn(schema);
                colvarDevice.ColumnName = "device";
                colvarDevice.DataType = DbType.AnsiString;
                colvarDevice.MaxLength = 50;
                colvarDevice.AutoIncrement = false;
                colvarDevice.IsNullable = false;
                colvarDevice.IsPrimaryKey = false;
                colvarDevice.IsForeignKey = false;
                colvarDevice.IsReadOnly = false;
                
                schema.Columns.Add(colvarDevice);
                
                TableSchema.TableColumn colvarToken = new TableSchema.TableColumn(schema);
                colvarToken.ColumnName = "token";
                colvarToken.DataType = DbType.String;
                colvarToken.MaxLength = 256;
                colvarToken.AutoIncrement = false;
                colvarToken.IsNullable = false;
                colvarToken.IsPrimaryKey = false;
                colvarToken.IsForeignKey = false;
                colvarToken.IsReadOnly = false;
                
                schema.Columns.Add(colvarToken);
                
                TableSchema.TableColumn colvarTokenStatus = new TableSchema.TableColumn(schema);
                colvarTokenStatus.ColumnName = "token_status";
                colvarTokenStatus.DataType = DbType.Int32;
                colvarTokenStatus.MaxLength = 0;
                colvarTokenStatus.AutoIncrement = false;
                colvarTokenStatus.IsNullable = false;
                colvarTokenStatus.IsPrimaryKey = false;
                colvarTokenStatus.IsForeignKey = false;
                colvarTokenStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarTokenStatus);
                
                TableSchema.TableColumn colvarMobileOsType = new TableSchema.TableColumn(schema);
                colvarMobileOsType.ColumnName = "mobile_os_type";
                colvarMobileOsType.DataType = DbType.Int32;
                colvarMobileOsType.MaxLength = 0;
                colvarMobileOsType.AutoIncrement = false;
                colvarMobileOsType.IsNullable = false;
                colvarMobileOsType.IsPrimaryKey = false;
                colvarMobileOsType.IsForeignKey = false;
                colvarMobileOsType.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobileOsType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_assignment_member_device_token",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpAssignmentMemberDeviceToken()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpAssignmentMemberDeviceToken(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpAssignmentMemberDeviceToken(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpAssignmentMemberDeviceToken(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public long Id 
	    {
		    get
		    {
			    return GetColumnValue<long>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("AssignmentId")]
        [Bindable(true)]
        public int AssignmentId 
	    {
		    get
		    {
			    return GetColumnValue<int>("assignment_id");
		    }
            set 
		    {
			    SetColumnValue("assignment_id", value);
            }
        }
	      
        [XmlAttribute("MemberId")]
        [Bindable(true)]
        public int MemberId 
	    {
		    get
		    {
			    return GetColumnValue<int>("member_id");
		    }
            set 
		    {
			    SetColumnValue("member_id", value);
            }
        }
	      
        [XmlAttribute("MemberType")]
        [Bindable(true)]
        public int MemberType 
	    {
		    get
		    {
			    return GetColumnValue<int>("member_type");
		    }
            set 
		    {
			    SetColumnValue("member_type", value);
            }
        }
	      
        [XmlAttribute("SendTime")]
        [Bindable(true)]
        public DateTime? SendTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("send_time");
		    }
            set 
		    {
			    SetColumnValue("send_time", value);
            }
        }
	      
        [XmlAttribute("DiscountCodeId")]
        [Bindable(true)]
        public int? DiscountCodeId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("discount_code_id");
		    }
            set 
		    {
			    SetColumnValue("discount_code_id", value);
            }
        }
	      
        [XmlAttribute("DeviceId")]
        [Bindable(true)]
        public int DeviceId 
	    {
		    get
		    {
			    return GetColumnValue<int>("device_id");
		    }
            set 
		    {
			    SetColumnValue("device_id", value);
            }
        }
	      
        [XmlAttribute("Device")]
        [Bindable(true)]
        public string Device 
	    {
		    get
		    {
			    return GetColumnValue<string>("device");
		    }
            set 
		    {
			    SetColumnValue("device", value);
            }
        }
	      
        [XmlAttribute("Token")]
        [Bindable(true)]
        public string Token 
	    {
		    get
		    {
			    return GetColumnValue<string>("token");
		    }
            set 
		    {
			    SetColumnValue("token", value);
            }
        }
	      
        [XmlAttribute("TokenStatus")]
        [Bindable(true)]
        public int TokenStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("token_status");
		    }
            set 
		    {
			    SetColumnValue("token_status", value);
            }
        }
	      
        [XmlAttribute("MobileOsType")]
        [Bindable(true)]
        public int MobileOsType 
	    {
		    get
		    {
			    return GetColumnValue<int>("mobile_os_type");
		    }
            set 
		    {
			    SetColumnValue("mobile_os_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string AssignmentId = @"assignment_id";
            
            public static string MemberId = @"member_id";
            
            public static string MemberType = @"member_type";
            
            public static string SendTime = @"send_time";
            
            public static string DiscountCodeId = @"discount_code_id";
            
            public static string DeviceId = @"device_id";
            
            public static string Device = @"device";
            
            public static string Token = @"token";
            
            public static string TokenStatus = @"token_status";
            
            public static string MobileOsType = @"mobile_os_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealPayment class.
	/// </summary>
    [Serializable]
	public partial class DealPaymentCollection : RepositoryList<DealPayment, DealPaymentCollection>
	{	   
		public DealPaymentCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealPaymentCollection</returns>
		public DealPaymentCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealPayment o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_payment table.
	/// </summary>
	
	[Serializable]
	public partial class DealPayment : RepositoryRecord<DealPayment>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealPayment()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DealPayment(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("deal_payment", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarBankId = new TableSchema.TableColumn(schema);
				colvarBankId.ColumnName = "bank_id";
				colvarBankId.DataType = DbType.Int32;
				colvarBankId.MaxLength = 0;
				colvarBankId.AutoIncrement = false;
				colvarBankId.IsNullable = true;
				colvarBankId.IsPrimaryKey = false;
				colvarBankId.IsForeignKey = false;
				colvarBankId.IsReadOnly = false;
				colvarBankId.DefaultSetting = @"";
				colvarBankId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBankId);
				
				TableSchema.TableColumn colvarIsBankDeal = new TableSchema.TableColumn(schema);
				colvarIsBankDeal.ColumnName = "is_bank_deal";
				colvarIsBankDeal.DataType = DbType.Boolean;
				colvarIsBankDeal.MaxLength = 0;
				colvarIsBankDeal.AutoIncrement = false;
				colvarIsBankDeal.IsNullable = false;
				colvarIsBankDeal.IsPrimaryKey = false;
				colvarIsBankDeal.IsForeignKey = false;
				colvarIsBankDeal.IsReadOnly = false;
				
						colvarIsBankDeal.DefaultSetting = @"((0))";
				colvarIsBankDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsBankDeal);
				
				TableSchema.TableColumn colvarAllowGuestBuy = new TableSchema.TableColumn(schema);
				colvarAllowGuestBuy.ColumnName = "allow_guest_buy";
				colvarAllowGuestBuy.DataType = DbType.Boolean;
				colvarAllowGuestBuy.MaxLength = 0;
				colvarAllowGuestBuy.AutoIncrement = false;
				colvarAllowGuestBuy.IsNullable = false;
				colvarAllowGuestBuy.IsPrimaryKey = false;
				colvarAllowGuestBuy.IsForeignKey = false;
				colvarAllowGuestBuy.IsReadOnly = false;
				
						colvarAllowGuestBuy.DefaultSetting = @"((0))";
				colvarAllowGuestBuy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAllowGuestBuy);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("deal_payment",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		
		[XmlAttribute("BankId")]
		[Bindable(true)]
		public int? BankId 
		{
			get { return GetColumnValue<int?>(Columns.BankId); }
			set { SetColumnValue(Columns.BankId, value); }
		}
		
		[XmlAttribute("IsBankDeal")]
		[Bindable(true)]
		public bool IsBankDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsBankDeal); }
			set { SetColumnValue(Columns.IsBankDeal, value); }
		}
		
		[XmlAttribute("AllowGuestBuy")]
		[Bindable(true)]
		public bool AllowGuestBuy 
		{
			get { return GetColumnValue<bool>(Columns.AllowGuestBuy); }
			set { SetColumnValue(Columns.AllowGuestBuy, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BankIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IsBankDealColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AllowGuestBuyColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string BankId = @"bank_id";
			 public static string IsBankDeal = @"is_bank_deal";
			 public static string AllowGuestBuy = @"allow_guest_buy";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

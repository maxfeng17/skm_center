﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the SellerContractFile class.
    /// </summary>
    [Serializable]
    public partial class SellerContractFileCollection : RepositoryList<SellerContractFile, SellerContractFileCollection>
    {
        public SellerContractFileCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SellerContractFileCollection</returns>
        public SellerContractFileCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SellerContractFile o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the seller_contract_files table.
    /// </summary>
    [Serializable]
    public partial class SellerContractFile : RepositoryRecord<SellerContractFile>, IRecordBase
    {
        #region .ctors and Default Settings

        public SellerContractFile()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SellerContractFile(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("seller_contract_files", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                colvarSellerGuid.DefaultSetting = @"";
                colvarSellerGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarFileName = new TableSchema.TableColumn(schema);
                colvarFileName.ColumnName = "file_name";
                colvarFileName.DataType = DbType.String;
                colvarFileName.MaxLength = 100;
                colvarFileName.AutoIncrement = false;
                colvarFileName.IsNullable = false;
                colvarFileName.IsPrimaryKey = false;
                colvarFileName.IsForeignKey = false;
                colvarFileName.IsReadOnly = false;
                colvarFileName.DefaultSetting = @"";
                colvarFileName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFileName);

                TableSchema.TableColumn colvarFilePath = new TableSchema.TableColumn(schema);
                colvarFilePath.ColumnName = "file_path";
                colvarFilePath.DataType = DbType.String;
                colvarFilePath.MaxLength = 200;
                colvarFilePath.AutoIncrement = false;
                colvarFilePath.IsNullable = true;
                colvarFilePath.IsPrimaryKey = false;
                colvarFilePath.IsForeignKey = false;
                colvarFilePath.IsReadOnly = false;
                colvarFilePath.DefaultSetting = @"";
                colvarFilePath.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFilePath);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                colvarCreateTime.DefaultSetting = @"(getdate())";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 100;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                colvarStatus.DefaultSetting = @"((1))";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarContractType = new TableSchema.TableColumn(schema);
                colvarContractType.ColumnName = "contract_type";
                colvarContractType.DataType = DbType.Int32;
                colvarContractType.MaxLength = 0;
                colvarContractType.AutoIncrement = false;
                colvarContractType.IsNullable = false;
                colvarContractType.IsPrimaryKey = false;
                colvarContractType.IsForeignKey = false;
                colvarContractType.IsReadOnly = false;
                colvarContractType.DefaultSetting = @"";
                colvarContractType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarContractType);

                TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
                colvarMemo.ColumnName = "memo";
                colvarMemo.DataType = DbType.String;
                colvarMemo.MaxLength = 100;
                colvarMemo.AutoIncrement = false;
                colvarMemo.IsNullable = false;
                colvarMemo.IsPrimaryKey = false;
                colvarMemo.IsForeignKey = false;
                colvarMemo.IsReadOnly = false;
                colvarMemo.DefaultSetting = @"";
                colvarMemo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMemo);

                TableSchema.TableColumn colvarDueDate = new TableSchema.TableColumn(schema);
                colvarDueDate.ColumnName = "DueDate";
                colvarDueDate.DataType = DbType.DateTime;
                colvarDueDate.MaxLength = 0;
                colvarDueDate.AutoIncrement = false;
                colvarDueDate.IsNullable = true;
                colvarDueDate.IsPrimaryKey = false;
                colvarDueDate.IsForeignKey = false;
                colvarDueDate.IsReadOnly = false;
                colvarDueDate.DefaultSetting = @"";
                colvarDueDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDueDate);

                TableSchema.TableColumn colvarPartyBContact = new TableSchema.TableColumn(schema);
                colvarPartyBContact.ColumnName = "PartyBContact";
                colvarPartyBContact.DataType = DbType.String;
                colvarPartyBContact.MaxLength = 50;
                colvarPartyBContact.AutoIncrement = false;
                colvarPartyBContact.IsNullable = false;
                colvarPartyBContact.IsPrimaryKey = false;
                colvarPartyBContact.IsForeignKey = false;
                colvarPartyBContact.IsReadOnly = false;

                colvarPartyBContact.DefaultSetting = @"('')";
                colvarPartyBContact.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPartyBContact);

                TableSchema.TableColumn colvarVersion = new TableSchema.TableColumn(schema);
                colvarVersion.ColumnName = "version";
                colvarVersion.DataType = DbType.AnsiString;
                colvarVersion.MaxLength = 20;
                colvarVersion.AutoIncrement = false;
                colvarVersion.IsNullable = true;
                colvarVersion.IsPrimaryKey = false;
                colvarVersion.IsForeignKey = false;
                colvarVersion.IsReadOnly = false;
                colvarVersion.DefaultSetting = @"";
                colvarVersion.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVersion);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("seller_contract_files", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid
        {
            get { return GetColumnValue<Guid>(Columns.SellerGuid); }
            set { SetColumnValue(Columns.SellerGuid, value); }
        }

        [XmlAttribute("FileName")]
        [Bindable(true)]
        public string FileName
        {
            get { return GetColumnValue<string>(Columns.FileName); }
            set { SetColumnValue(Columns.FileName, value); }
        }

        [XmlAttribute("FilePath")]
        [Bindable(true)]
        public string FilePath
        {
            get { return GetColumnValue<string>(Columns.FilePath); }
            set { SetColumnValue(Columns.FilePath, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("ContractType")]
        [Bindable(true)]
        public int ContractType
        {
            get { return GetColumnValue<int>(Columns.ContractType); }
            set { SetColumnValue(Columns.ContractType, value); }
        }

        [XmlAttribute("Memo")]
        [Bindable(true)]
        public string Memo
        {
            get { return GetColumnValue<string>(Columns.Memo); }
            set { SetColumnValue(Columns.Memo, value); }
        }

        [XmlAttribute("DueDate")]
        [Bindable(true)]
        public DateTime DueDate
        {
            get { return GetColumnValue<DateTime>(Columns.DueDate); }
            set { SetColumnValue(Columns.DueDate, value); }
        }

        [XmlAttribute("PartyBContact")]
        [Bindable(true)]
        public string PartyBContact
        {
            get { return GetColumnValue<string>(Columns.PartyBContact); }
            set { SetColumnValue(Columns.PartyBContact, value); }
        }

        [XmlAttribute("Version")]
        [Bindable(true)]
        public string Version
        {
            get { return GetColumnValue<string>(Columns.Version); }
            set { SetColumnValue(Columns.Version, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn FileNameColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn FilePathColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ContractTypeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn DueDateColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn PartyBContactColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn VersionColumn
        {
            get { return Schema.Columns[11]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"guid";
            public static string SellerGuid = @"seller_guid";
            public static string FileName = @"file_name";
            public static string FilePath = @"file_path";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string Status = @"status";
            public static string ContractType = @"contract_type";
            public static string Memo = @"memo";
            public static string DueDate = @"DueDate";
            public static string PartyBContact = @"PartyBContact";
            public static string Version = @"version";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

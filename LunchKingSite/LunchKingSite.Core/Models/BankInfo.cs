using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the BankInfo class.
    /// </summary>
    [Serializable]
    public partial class BankInfoCollection : RepositoryList<BankInfo, BankInfoCollection>
    {
        public BankInfoCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BankInfoCollection</returns>
        public BankInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BankInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the bank_info table.
    /// </summary>
    [Serializable]
    public partial class BankInfo : RepositoryRecord<BankInfo>, IRecordBase
    {
        #region .ctors and Default Settings

        public BankInfo()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public BankInfo(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("bank_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarBankNo = new TableSchema.TableColumn(schema);
                colvarBankNo.ColumnName = "bank_no";
                colvarBankNo.DataType = DbType.String;
                colvarBankNo.MaxLength = 50;
                colvarBankNo.AutoIncrement = false;
                colvarBankNo.IsNullable = false;
                colvarBankNo.IsPrimaryKey = false;
                colvarBankNo.IsForeignKey = false;
                colvarBankNo.IsReadOnly = false;
                colvarBankNo.DefaultSetting = @"";
                colvarBankNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBankNo);

                TableSchema.TableColumn colvarBankName = new TableSchema.TableColumn(schema);
                colvarBankName.ColumnName = "bank_name";
                colvarBankName.DataType = DbType.String;
                colvarBankName.MaxLength = -1;
                colvarBankName.AutoIncrement = false;
                colvarBankName.IsNullable = false;
                colvarBankName.IsPrimaryKey = false;
                colvarBankName.IsForeignKey = false;
                colvarBankName.IsReadOnly = false;
                colvarBankName.DefaultSetting = @"";
                colvarBankName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBankName);

                TableSchema.TableColumn colvarBranchNo = new TableSchema.TableColumn(schema);
                colvarBranchNo.ColumnName = "branch_no";
                colvarBranchNo.DataType = DbType.String;
                colvarBranchNo.MaxLength = 50;
                colvarBranchNo.AutoIncrement = false;
                colvarBranchNo.IsNullable = true;
                colvarBranchNo.IsPrimaryKey = false;
                colvarBranchNo.IsForeignKey = false;
                colvarBranchNo.IsReadOnly = false;
                colvarBranchNo.DefaultSetting = @"";
                colvarBranchNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBranchNo);

                TableSchema.TableColumn colvarBranchName = new TableSchema.TableColumn(schema);
                colvarBranchName.ColumnName = "branch_name";
                colvarBranchName.DataType = DbType.String;
                colvarBranchName.MaxLength = -1;
                colvarBranchName.AutoIncrement = false;
                colvarBranchName.IsNullable = true;
                colvarBranchName.IsPrimaryKey = false;
                colvarBranchName.IsForeignKey = false;
                colvarBranchName.IsReadOnly = false;
                colvarBranchName.DefaultSetting = @"";
                colvarBranchName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBranchName);

                TableSchema.TableColumn colvarAddress = new TableSchema.TableColumn(schema);
                colvarAddress.ColumnName = "address";
                colvarAddress.DataType = DbType.String;
                colvarAddress.MaxLength = -1;
                colvarAddress.AutoIncrement = false;
                colvarAddress.IsNullable = true;
                colvarAddress.IsPrimaryKey = false;
                colvarAddress.IsForeignKey = false;
                colvarAddress.IsReadOnly = false;
                colvarAddress.DefaultSetting = @"";
                colvarAddress.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAddress);

                TableSchema.TableColumn colvarTelephone = new TableSchema.TableColumn(schema);
                colvarTelephone.ColumnName = "telephone";
                colvarTelephone.DataType = DbType.String;
                colvarTelephone.MaxLength = 50;
                colvarTelephone.AutoIncrement = false;
                colvarTelephone.IsNullable = true;
                colvarTelephone.IsPrimaryKey = false;
                colvarTelephone.IsForeignKey = false;
                colvarTelephone.IsReadOnly = false;
                colvarTelephone.DefaultSetting = @"";
                colvarTelephone.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTelephone);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("bank_info", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("BankNo")]
        [Bindable(true)]
        public string BankNo
        {
            get { return GetColumnValue<string>(Columns.BankNo); }
            set { SetColumnValue(Columns.BankNo, value); }
        }

        [XmlAttribute("BankName")]
        [Bindable(true)]
        public string BankName
        {
            get { return GetColumnValue<string>(Columns.BankName); }
            set { SetColumnValue(Columns.BankName, value); }
        }

        [XmlAttribute("BranchNo")]
        [Bindable(true)]
        public string BranchNo
        {
            get { return GetColumnValue<string>(Columns.BranchNo); }
            set { SetColumnValue(Columns.BranchNo, value); }
        }

        [XmlAttribute("BranchName")]
        [Bindable(true)]
        public string BranchName
        {
            get { return GetColumnValue<string>(Columns.BranchName); }
            set { SetColumnValue(Columns.BranchName, value); }
        }

        [XmlAttribute("Address")]
        [Bindable(true)]
        public string Address
        {
            get { return GetColumnValue<string>(Columns.Address); }
            set { SetColumnValue(Columns.Address, value); }
        }

        [XmlAttribute("Telephone")]
        [Bindable(true)]
        public string Telephone
        {
            get { return GetColumnValue<string>(Columns.Telephone); }
            set { SetColumnValue(Columns.Telephone, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn BankNoColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn BankNameColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn BranchNoColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn BranchNameColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn AddressColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn TelephoneColumn
        {
            get { return Schema.Columns[6]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string BankNo = @"bank_no";
            public static string BankName = @"bank_name";
            public static string BranchNo = @"branch_no";
            public static string BranchName = @"branch_name";
            public static string Address = @"address";
            public static string Telephone = @"telephone";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

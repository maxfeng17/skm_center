using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DiscountTemplate class.
	/// </summary>
    [Serializable]
	public partial class DiscountTemplateCollection : RepositoryList<DiscountTemplate, DiscountTemplateCollection>
	{	   
		public DiscountTemplateCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DiscountTemplateCollection</returns>
		public DiscountTemplateCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DiscountTemplate o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the discount_template table.
	/// </summary>
	[Serializable]
	public partial class DiscountTemplate : RepositoryRecord<DiscountTemplate>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DiscountTemplate()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DiscountTemplate(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("discount_template", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
				colvarSellerUserId.ColumnName = "seller_user_id";
				colvarSellerUserId.DataType = DbType.Int32;
				colvarSellerUserId.MaxLength = 0;
				colvarSellerUserId.AutoIncrement = false;
				colvarSellerUserId.IsNullable = false;
				colvarSellerUserId.IsPrimaryKey = false;
				colvarSellerUserId.IsForeignKey = false;
				colvarSellerUserId.IsReadOnly = false;
				colvarSellerUserId.DefaultSetting = @"";
				colvarSellerUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerUserId);
				
				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Currency;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = false;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);
				
				TableSchema.TableColumn colvarMinimumAmount = new TableSchema.TableColumn(schema);
				colvarMinimumAmount.ColumnName = "minimum_amount";
				colvarMinimumAmount.DataType = DbType.Currency;
				colvarMinimumAmount.MaxLength = 0;
				colvarMinimumAmount.AutoIncrement = false;
				colvarMinimumAmount.IsNullable = true;
				colvarMinimumAmount.IsPrimaryKey = false;
				colvarMinimumAmount.IsForeignKey = false;
				colvarMinimumAmount.IsReadOnly = false;
				colvarMinimumAmount.DefaultSetting = @"";
				colvarMinimumAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMinimumAmount);
				
				TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
				colvarStartTime.ColumnName = "start_time";
				colvarStartTime.DataType = DbType.DateTime;
				colvarStartTime.MaxLength = 0;
				colvarStartTime.AutoIncrement = false;
				colvarStartTime.IsNullable = false;
				colvarStartTime.IsPrimaryKey = false;
				colvarStartTime.IsForeignKey = false;
				colvarStartTime.IsReadOnly = false;
				colvarStartTime.DefaultSetting = @"";
				colvarStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartTime);
				
				TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
				colvarEndTime.ColumnName = "end_time";
				colvarEndTime.DataType = DbType.DateTime;
				colvarEndTime.MaxLength = 0;
				colvarEndTime.AutoIncrement = false;
				colvarEndTime.IsNullable = false;
				colvarEndTime.IsPrimaryKey = false;
				colvarEndTime.IsForeignKey = false;
				colvarEndTime.IsReadOnly = false;
				colvarEndTime.DefaultSetting = @"";
				colvarEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndTime);
				
				TableSchema.TableColumn colvarAvailableDateType = new TableSchema.TableColumn(schema);
				colvarAvailableDateType.ColumnName = "available_date_type";
				colvarAvailableDateType.DataType = DbType.Int32;
				colvarAvailableDateType.MaxLength = 0;
				colvarAvailableDateType.AutoIncrement = false;
				colvarAvailableDateType.IsNullable = false;
				colvarAvailableDateType.IsPrimaryKey = false;
				colvarAvailableDateType.IsForeignKey = false;
				colvarAvailableDateType.IsReadOnly = false;
				colvarAvailableDateType.DefaultSetting = @"";
				colvarAvailableDateType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAvailableDateType);
				
				TableSchema.TableColumn colvarCardCombineUse = new TableSchema.TableColumn(schema);
				colvarCardCombineUse.ColumnName = "card_combine_use";
				colvarCardCombineUse.DataType = DbType.Boolean;
				colvarCardCombineUse.MaxLength = 0;
				colvarCardCombineUse.AutoIncrement = false;
				colvarCardCombineUse.IsNullable = false;
				colvarCardCombineUse.IsPrimaryKey = false;
				colvarCardCombineUse.IsForeignKey = false;
				colvarCardCombineUse.IsReadOnly = false;
				colvarCardCombineUse.DefaultSetting = @"";
				colvarCardCombineUse.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardCombineUse);
				
				TableSchema.TableColumn colvarIsDraft = new TableSchema.TableColumn(schema);
				colvarIsDraft.ColumnName = "is_draft";
				colvarIsDraft.DataType = DbType.Boolean;
				colvarIsDraft.MaxLength = 0;
				colvarIsDraft.AutoIncrement = false;
				colvarIsDraft.IsNullable = false;
				colvarIsDraft.IsPrimaryKey = false;
				colvarIsDraft.IsForeignKey = false;
				colvarIsDraft.IsReadOnly = false;
				colvarIsDraft.DefaultSetting = @"";
				colvarIsDraft.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDraft);
				
				TableSchema.TableColumn colvarSituationType = new TableSchema.TableColumn(schema);
				colvarSituationType.ColumnName = "situation_type";
				colvarSituationType.DataType = DbType.Int32;
				colvarSituationType.MaxLength = 0;
				colvarSituationType.AutoIncrement = false;
				colvarSituationType.IsNullable = false;
				colvarSituationType.IsPrimaryKey = false;
				colvarSituationType.IsForeignKey = false;
				colvarSituationType.IsReadOnly = false;
				colvarSituationType.DefaultSetting = @"";
				colvarSituationType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSituationType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("discount_template",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SellerUserId")]
		[Bindable(true)]
		public int SellerUserId 
		{
			get { return GetColumnValue<int>(Columns.SellerUserId); }
			set { SetColumnValue(Columns.SellerUserId, value); }
		}
		  
		[XmlAttribute("Amount")]
		[Bindable(true)]
		public decimal Amount 
		{
			get { return GetColumnValue<decimal>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}
		  
		[XmlAttribute("MinimumAmount")]
		[Bindable(true)]
		public decimal? MinimumAmount 
		{
			get { return GetColumnValue<decimal?>(Columns.MinimumAmount); }
			set { SetColumnValue(Columns.MinimumAmount, value); }
		}
		  
		[XmlAttribute("StartTime")]
		[Bindable(true)]
		public DateTime StartTime 
		{
			get { return GetColumnValue<DateTime>(Columns.StartTime); }
			set { SetColumnValue(Columns.StartTime, value); }
		}
		  
		[XmlAttribute("EndTime")]
		[Bindable(true)]
		public DateTime EndTime 
		{
			get { return GetColumnValue<DateTime>(Columns.EndTime); }
			set { SetColumnValue(Columns.EndTime, value); }
		}
		  
		[XmlAttribute("AvailableDateType")]
		[Bindable(true)]
		public int AvailableDateType 
		{
			get { return GetColumnValue<int>(Columns.AvailableDateType); }
			set { SetColumnValue(Columns.AvailableDateType, value); }
		}
		  
		[XmlAttribute("CardCombineUse")]
		[Bindable(true)]
		public bool CardCombineUse 
		{
			get { return GetColumnValue<bool>(Columns.CardCombineUse); }
			set { SetColumnValue(Columns.CardCombineUse, value); }
		}
		  
		[XmlAttribute("IsDraft")]
		[Bindable(true)]
		public bool IsDraft 
		{
			get { return GetColumnValue<bool>(Columns.IsDraft); }
			set { SetColumnValue(Columns.IsDraft, value); }
		}
		  
		[XmlAttribute("SituationType")]
		[Bindable(true)]
		public int SituationType 
		{
			get { return GetColumnValue<int>(Columns.SituationType); }
			set { SetColumnValue(Columns.SituationType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerUserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MinimumAmountColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn StartTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn EndTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn AvailableDateTypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CardCombineUseColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDraftColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SituationTypeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SellerUserId = @"seller_user_id";
			 public static string Amount = @"amount";
			 public static string MinimumAmount = @"minimum_amount";
			 public static string StartTime = @"start_time";
			 public static string EndTime = @"end_time";
			 public static string AvailableDateType = @"available_date_type";
			 public static string CardCombineUse = @"card_combine_use";
			 public static string IsDraft = @"is_draft";
			 public static string SituationType = @"situation_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

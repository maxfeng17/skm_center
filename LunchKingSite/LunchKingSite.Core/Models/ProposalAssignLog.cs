﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProposalAssignLog class.
    /// </summary>
    [Serializable]
    public partial class ProposalAssignLogCollection : RepositoryList<ProposalAssignLog, ProposalAssignLogCollection>
    {
        public ProposalAssignLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalAssignLogCollection</returns>
        public ProposalAssignLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProposalAssignLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the proposal_assign_log table.
    /// </summary>
    [Serializable]
    public partial class ProposalAssignLog : RepositoryRecord<ProposalAssignLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProposalAssignLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProposalAssignLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("proposal_assign_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarPid = new TableSchema.TableColumn(schema);
                colvarPid.ColumnName = "pid";
                colvarPid.DataType = DbType.Int32;
                colvarPid.MaxLength = 0;
                colvarPid.AutoIncrement = false;
                colvarPid.IsNullable = false;
                colvarPid.IsPrimaryKey = false;
                colvarPid.IsForeignKey = false;
                colvarPid.IsReadOnly = false;
                colvarPid.DefaultSetting = @"";
                colvarPid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPid);

                TableSchema.TableColumn colvarAssignFlag = new TableSchema.TableColumn(schema);
                colvarAssignFlag.ColumnName = "assign_flag";
                colvarAssignFlag.DataType = DbType.Int32;
                colvarAssignFlag.MaxLength = 0;
                colvarAssignFlag.AutoIncrement = false;
                colvarAssignFlag.IsNullable = true;
                colvarAssignFlag.IsPrimaryKey = false;
                colvarAssignFlag.IsForeignKey = false;
                colvarAssignFlag.IsReadOnly = false;
                colvarAssignFlag.DefaultSetting = @"";
                colvarAssignFlag.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAssignFlag);

                TableSchema.TableColumn colvarAssignEmail = new TableSchema.TableColumn(schema);
                colvarAssignEmail.ColumnName = "assign_email";
                colvarAssignEmail.DataType = DbType.String;
                colvarAssignEmail.MaxLength = 250;
                colvarAssignEmail.AutoIncrement = false;
                colvarAssignEmail.IsNullable = true;
                colvarAssignEmail.IsPrimaryKey = false;
                colvarAssignEmail.IsForeignKey = false;
                colvarAssignEmail.IsReadOnly = false;
                colvarAssignEmail.DefaultSetting = @"";
                colvarAssignEmail.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAssignEmail);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 250;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("proposal_assign_log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Pid")]
        [Bindable(true)]
        public int Pid
        {
            get { return GetColumnValue<int>(Columns.Pid); }
            set { SetColumnValue(Columns.Pid, value); }
        }

        [XmlAttribute("AssignFlag")]
        [Bindable(true)]
        public int? AssignFlag
        {
            get { return GetColumnValue<int?>(Columns.AssignFlag); }
            set { SetColumnValue(Columns.AssignFlag, value); }
        }

        [XmlAttribute("AssignEmail")]
        [Bindable(true)]
        public string AssignEmail
        {
            get { return GetColumnValue<string>(Columns.AssignEmail); }
            set { SetColumnValue(Columns.AssignEmail, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn PidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn AssignFlagColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn AssignEmailColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Pid = @"pid";
            public static string AssignFlag = @"assign_flag";
            public static string AssignEmail = @"assign_email";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

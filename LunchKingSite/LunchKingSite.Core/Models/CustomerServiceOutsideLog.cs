using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the CustomerServiceOutsideLog class.
    /// </summary>
    [Serializable]
    public partial class CustomerServiceOutsideLogCollection : RepositoryList<CustomerServiceOutsideLog, CustomerServiceOutsideLogCollection>
    {
        public CustomerServiceOutsideLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CustomerServiceOutsideLogCollection</returns>
        public CustomerServiceOutsideLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CustomerServiceOutsideLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }


    /// <summary>
    /// This is an ActiveRecord class which wraps the customer_service_outside_log table.
    /// </summary>

    [Serializable]
    public partial class CustomerServiceOutsideLog : RepositoryRecord<CustomerServiceOutsideLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public CustomerServiceOutsideLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public CustomerServiceOutsideLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("customer_service_outside_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarServiceNo = new TableSchema.TableColumn(schema);
                colvarServiceNo.ColumnName = "service_no";
                colvarServiceNo.DataType = DbType.AnsiString;
                colvarServiceNo.MaxLength = 50;
                colvarServiceNo.AutoIncrement = false;
                colvarServiceNo.IsNullable = false;
                colvarServiceNo.IsPrimaryKey = false;
                colvarServiceNo.IsForeignKey = false;
                colvarServiceNo.IsReadOnly = false;
                colvarServiceNo.DefaultSetting = @"";
                colvarServiceNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarServiceNo);

                TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
                colvarContent.ColumnName = "content";
                colvarContent.DataType = DbType.String;
                colvarContent.MaxLength = -1;
                colvarContent.AutoIncrement = false;
                colvarContent.IsNullable = true;
                colvarContent.IsPrimaryKey = false;
                colvarContent.IsForeignKey = false;
                colvarContent.IsReadOnly = false;
                colvarContent.DefaultSetting = @"";
                colvarContent.ForeignKeyTableName = "";
                schema.Columns.Add(colvarContent);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                colvarType.DefaultSetting = @"";
                colvarType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarFile = new TableSchema.TableColumn(schema);
                colvarFile.ColumnName = "file";
                colvarFile.DataType = DbType.String;
                colvarFile.MaxLength = 100;
                colvarFile.AutoIncrement = false;
                colvarFile.IsNullable = true;
                colvarFile.IsPrimaryKey = false;
                colvarFile.IsForeignKey = false;
                colvarFile.IsReadOnly = false;
                colvarFile.DefaultSetting = @"";
                colvarFile.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFile);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateName = new TableSchema.TableColumn(schema);
                colvarCreateName.ColumnName = "create_name";
                colvarCreateName.DataType = DbType.String;
                colvarCreateName.MaxLength = 100;
                colvarCreateName.AutoIncrement = false;
                colvarCreateName.IsNullable = true;
                colvarCreateName.IsPrimaryKey = false;
                colvarCreateName.IsForeignKey = false;
                colvarCreateName.IsReadOnly = false;
                colvarCreateName.DefaultSetting = @"";
                colvarCreateName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateName);

                TableSchema.TableColumn colvarIsRead = new TableSchema.TableColumn(schema);
                colvarIsRead.ColumnName = "is_read";
                colvarIsRead.DataType = DbType.Int32;
                colvarIsRead.MaxLength = 0;
                colvarIsRead.AutoIncrement = false;
                colvarIsRead.IsNullable = true;
                colvarIsRead.IsPrimaryKey = false;
                colvarIsRead.IsForeignKey = false;
                colvarIsRead.IsReadOnly = false;

                colvarIsRead.DefaultSetting = @"((0))";
                colvarIsRead.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsRead);

                TableSchema.TableColumn colvarMethod = new TableSchema.TableColumn(schema);
                colvarMethod.ColumnName = "method";
                colvarMethod.DataType = DbType.Int32;
                colvarMethod.MaxLength = 0;
                colvarMethod.AutoIncrement = false;
                colvarMethod.IsNullable = true;
                colvarMethod.IsPrimaryKey = false;
                colvarMethod.IsForeignKey = false;
                colvarMethod.IsReadOnly = false;

                colvarMethod.DefaultSetting = @"((0))";
                colvarMethod.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMethod);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("customer_service_outside_log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("ServiceNo")]
        [Bindable(true)]
        public string ServiceNo
        {
            get { return GetColumnValue<string>(Columns.ServiceNo); }
            set { SetColumnValue(Columns.ServiceNo, value); }
        }

        [XmlAttribute("Content")]
        [Bindable(true)]
        public string Content
        {
            get { return GetColumnValue<string>(Columns.Content); }
            set { SetColumnValue(Columns.Content, value); }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get { return GetColumnValue<int>(Columns.Type); }
            set { SetColumnValue(Columns.Type, value); }
        }

        [XmlAttribute("File")]
        [Bindable(true)]
        public string File
        {
            get { return GetColumnValue<string>(Columns.File); }
            set { SetColumnValue(Columns.File, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int? CreateId
        {
            get { return GetColumnValue<int?>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateName")]
        [Bindable(true)]
        public string CreateName
        {
            get { return GetColumnValue<string>(Columns.CreateName); }
            set { SetColumnValue(Columns.CreateName, value); }
        }

        [XmlAttribute("IsRead")]
        [Bindable(true)]
        public int? IsRead
        {
            get { return GetColumnValue<int?>(Columns.IsRead); }
            set { SetColumnValue(Columns.IsRead, value); }
        }

        [XmlAttribute("Method")]
        [Bindable(true)]
        public int? Method
        {
            get { return GetColumnValue<int?>(Columns.Method); }
            set { SetColumnValue(Columns.Method, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ServiceNoColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ContentColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn FileColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CreateNameColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn IsReadColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn MethodColumn
        {
            get { return Schema.Columns[10]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string ServiceNo = @"service_no";
            public static string Content = @"content";
            public static string Type = @"type";
            public static string File = @"file";
            public static string Status = @"status";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string CreateName = @"create_name";
            public static string IsRead = @"is_read";
            public static string Method = @"method";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the WmsRefundOrderStatusLog class.
	/// </summary>
    [Serializable]
	public partial class WmsRefundOrderStatusLogCollection : RepositoryList<WmsRefundOrderStatusLog, WmsRefundOrderStatusLogCollection>
	{	   
		public WmsRefundOrderStatusLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>WmsRefundOrderStatusLogCollection</returns>
		public WmsRefundOrderStatusLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                WmsRefundOrderStatusLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the wms_refund_order_status_log table.
	/// </summary>
	[Serializable]
	public partial class WmsRefundOrderStatusLog : RepositoryRecord<WmsRefundOrderStatusLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public WmsRefundOrderStatusLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public WmsRefundOrderStatusLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("wms_refund_order_status_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarWmsRefundOrderId = new TableSchema.TableColumn(schema);
				colvarWmsRefundOrderId.ColumnName = "wms_refund_order_id";
				colvarWmsRefundOrderId.DataType = DbType.Int32;
				colvarWmsRefundOrderId.MaxLength = 0;
				colvarWmsRefundOrderId.AutoIncrement = false;
				colvarWmsRefundOrderId.IsNullable = false;
				colvarWmsRefundOrderId.IsPrimaryKey = false;
				colvarWmsRefundOrderId.IsForeignKey = false;
				colvarWmsRefundOrderId.IsReadOnly = false;
				colvarWmsRefundOrderId.DefaultSetting = @"";
				colvarWmsRefundOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWmsRefundOrderId);
				
				TableSchema.TableColumn colvarHasPickupTime = new TableSchema.TableColumn(schema);
				colvarHasPickupTime.ColumnName = "has_pickup_time";
				colvarHasPickupTime.DataType = DbType.DateTime;
				colvarHasPickupTime.MaxLength = 0;
				colvarHasPickupTime.AutoIncrement = false;
				colvarHasPickupTime.IsNullable = true;
				colvarHasPickupTime.IsPrimaryKey = false;
				colvarHasPickupTime.IsForeignKey = false;
				colvarHasPickupTime.IsReadOnly = false;
				colvarHasPickupTime.DefaultSetting = @"";
				colvarHasPickupTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHasPickupTime);
				
				TableSchema.TableColumn colvarArrivalTime = new TableSchema.TableColumn(schema);
				colvarArrivalTime.ColumnName = "arrival_time";
				colvarArrivalTime.DataType = DbType.DateTime;
				colvarArrivalTime.MaxLength = 0;
				colvarArrivalTime.AutoIncrement = false;
				colvarArrivalTime.IsNullable = true;
				colvarArrivalTime.IsPrimaryKey = false;
				colvarArrivalTime.IsForeignKey = false;
				colvarArrivalTime.IsReadOnly = false;
				colvarArrivalTime.DefaultSetting = @"";
				colvarArrivalTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarArrivalTime);
				
				TableSchema.TableColumn colvarCsConfirmedTime = new TableSchema.TableColumn(schema);
				colvarCsConfirmedTime.ColumnName = "cs_confirmed_time";
				colvarCsConfirmedTime.DataType = DbType.DateTime;
				colvarCsConfirmedTime.MaxLength = 0;
				colvarCsConfirmedTime.AutoIncrement = false;
				colvarCsConfirmedTime.IsNullable = true;
				colvarCsConfirmedTime.IsPrimaryKey = false;
				colvarCsConfirmedTime.IsForeignKey = false;
				colvarCsConfirmedTime.IsReadOnly = false;
				colvarCsConfirmedTime.DefaultSetting = @"";
				colvarCsConfirmedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCsConfirmedTime);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("wms_refund_order_status_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("WmsRefundOrderId")]
		[Bindable(true)]
		public int WmsRefundOrderId 
		{
			get { return GetColumnValue<int>(Columns.WmsRefundOrderId); }
			set { SetColumnValue(Columns.WmsRefundOrderId, value); }
		}
		  
		[XmlAttribute("HasPickupTime")]
		[Bindable(true)]
		public DateTime? HasPickupTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.HasPickupTime); }
			set { SetColumnValue(Columns.HasPickupTime, value); }
		}
		  
		[XmlAttribute("ArrivalTime")]
		[Bindable(true)]
		public DateTime? ArrivalTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ArrivalTime); }
			set { SetColumnValue(Columns.ArrivalTime, value); }
		}
		  
		[XmlAttribute("CsConfirmedTime")]
		[Bindable(true)]
		public DateTime? CsConfirmedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CsConfirmedTime); }
			set { SetColumnValue(Columns.CsConfirmedTime, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status 
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn WmsRefundOrderIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn HasPickupTimeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ArrivalTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CsConfirmedTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string WmsRefundOrderId = @"wms_refund_order_id";
			 public static string HasPickupTime = @"has_pickup_time";
			 public static string ArrivalTime = @"arrival_time";
			 public static string CsConfirmedTime = @"cs_confirmed_time";
			 public static string Status = @"status";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DeviceIdentyfierInfo class.
	/// </summary>
    [Serializable]
	public partial class DeviceIdentyfierInfoCollection : RepositoryList<DeviceIdentyfierInfo, DeviceIdentyfierInfoCollection>
	{	   
		public DeviceIdentyfierInfoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DeviceIdentyfierInfoCollection</returns>
		public DeviceIdentyfierInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DeviceIdentyfierInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the device_identyfier_info table.
	/// </summary>
	[Serializable]
	public partial class DeviceIdentyfierInfo : RepositoryRecord<DeviceIdentyfierInfo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DeviceIdentyfierInfo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DeviceIdentyfierInfo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("device_identyfier_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarIdentifierCode = new TableSchema.TableColumn(schema);
				colvarIdentifierCode.ColumnName = "identifier_code";
				colvarIdentifierCode.DataType = DbType.AnsiString;
				colvarIdentifierCode.MaxLength = 50;
				colvarIdentifierCode.AutoIncrement = false;
				colvarIdentifierCode.IsNullable = false;
				colvarIdentifierCode.IsPrimaryKey = false;
				colvarIdentifierCode.IsForeignKey = false;
				colvarIdentifierCode.IsReadOnly = false;
				colvarIdentifierCode.DefaultSetting = @"";
				colvarIdentifierCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdentifierCode);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = true;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarClientIp = new TableSchema.TableColumn(schema);
				colvarClientIp.ColumnName = "client_ip";
				colvarClientIp.DataType = DbType.AnsiString;
				colvarClientIp.MaxLength = 34;
				colvarClientIp.AutoIncrement = false;
				colvarClientIp.IsNullable = true;
				colvarClientIp.IsPrimaryKey = false;
				colvarClientIp.IsForeignKey = false;
				colvarClientIp.IsReadOnly = false;
				colvarClientIp.DefaultSetting = @"";
				colvarClientIp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarClientIp);
				
				TableSchema.TableColumn colvarActionTrigger = new TableSchema.TableColumn(schema);
				colvarActionTrigger.ColumnName = "action_trigger";
				colvarActionTrigger.DataType = DbType.AnsiString;
				colvarActionTrigger.MaxLength = 50;
				colvarActionTrigger.AutoIncrement = false;
				colvarActionTrigger.IsNullable = false;
				colvarActionTrigger.IsPrimaryKey = false;
				colvarActionTrigger.IsForeignKey = false;
				colvarActionTrigger.IsReadOnly = false;
				
						colvarActionTrigger.DefaultSetting = @"('')";
				colvarActionTrigger.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActionTrigger);
				
				TableSchema.TableColumn colvarManufacturer = new TableSchema.TableColumn(schema);
				colvarManufacturer.ColumnName = "manufacturer";
				colvarManufacturer.DataType = DbType.AnsiString;
				colvarManufacturer.MaxLength = 50;
				colvarManufacturer.AutoIncrement = false;
				colvarManufacturer.IsNullable = false;
				colvarManufacturer.IsPrimaryKey = false;
				colvarManufacturer.IsForeignKey = false;
				colvarManufacturer.IsReadOnly = false;
				
						colvarManufacturer.DefaultSetting = @"('')";
				colvarManufacturer.ForeignKeyTableName = "";
				schema.Columns.Add(colvarManufacturer);
				
				TableSchema.TableColumn colvarModel = new TableSchema.TableColumn(schema);
				colvarModel.ColumnName = "model";
				colvarModel.DataType = DbType.AnsiString;
				colvarModel.MaxLength = 50;
				colvarModel.AutoIncrement = false;
				colvarModel.IsNullable = false;
				colvarModel.IsPrimaryKey = false;
				colvarModel.IsForeignKey = false;
				colvarModel.IsReadOnly = false;
				
						colvarModel.DefaultSetting = @"('')";
				colvarModel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModel);
				
				TableSchema.TableColumn colvarOs = new TableSchema.TableColumn(schema);
				colvarOs.ColumnName = "os";
				colvarOs.DataType = DbType.AnsiString;
				colvarOs.MaxLength = 50;
				colvarOs.AutoIncrement = false;
				colvarOs.IsNullable = false;
				colvarOs.IsPrimaryKey = false;
				colvarOs.IsForeignKey = false;
				colvarOs.IsReadOnly = false;
				
						colvarOs.DefaultSetting = @"('')";
				colvarOs.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOs);
				
				TableSchema.TableColumn colvarOsVer = new TableSchema.TableColumn(schema);
				colvarOsVer.ColumnName = "os_ver";
				colvarOsVer.DataType = DbType.AnsiString;
				colvarOsVer.MaxLength = 50;
				colvarOsVer.AutoIncrement = false;
				colvarOsVer.IsNullable = false;
				colvarOsVer.IsPrimaryKey = false;
				colvarOsVer.IsForeignKey = false;
				colvarOsVer.IsReadOnly = false;
				
						colvarOsVer.DefaultSetting = @"('')";
				colvarOsVer.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOsVer);
				
				TableSchema.TableColumn colvarAppVer = new TableSchema.TableColumn(schema);
				colvarAppVer.ColumnName = "app_ver";
				colvarAppVer.DataType = DbType.AnsiString;
				colvarAppVer.MaxLength = 50;
				colvarAppVer.AutoIncrement = false;
				colvarAppVer.IsNullable = false;
				colvarAppVer.IsPrimaryKey = false;
				colvarAppVer.IsForeignKey = false;
				colvarAppVer.IsReadOnly = false;
				
						colvarAppVer.DefaultSetting = @"('')";
				colvarAppVer.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppVer);
				
				TableSchema.TableColumn colvarLongitude = new TableSchema.TableColumn(schema);
				colvarLongitude.ColumnName = "longitude";
				colvarLongitude.DataType = DbType.Double;
				colvarLongitude.MaxLength = 0;
				colvarLongitude.AutoIncrement = false;
				colvarLongitude.IsNullable = false;
				colvarLongitude.IsPrimaryKey = false;
				colvarLongitude.IsForeignKey = false;
				colvarLongitude.IsReadOnly = false;
				
						colvarLongitude.DefaultSetting = @"((0))";
				colvarLongitude.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLongitude);
				
				TableSchema.TableColumn colvarLatitude = new TableSchema.TableColumn(schema);
				colvarLatitude.ColumnName = "latitude";
				colvarLatitude.DataType = DbType.Double;
				colvarLatitude.MaxLength = 0;
				colvarLatitude.AutoIncrement = false;
				colvarLatitude.IsNullable = false;
				colvarLatitude.IsPrimaryKey = false;
				colvarLatitude.IsForeignKey = false;
				colvarLatitude.IsReadOnly = false;
				
						colvarLatitude.DefaultSetting = @"((0))";
				colvarLatitude.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLatitude);
				
				TableSchema.TableColumn colvarMemberUniqueId = new TableSchema.TableColumn(schema);
				colvarMemberUniqueId.ColumnName = "member_unique_id";
				colvarMemberUniqueId.DataType = DbType.Int32;
				colvarMemberUniqueId.MaxLength = 0;
				colvarMemberUniqueId.AutoIncrement = false;
				colvarMemberUniqueId.IsNullable = true;
				colvarMemberUniqueId.IsPrimaryKey = false;
				colvarMemberUniqueId.IsForeignKey = false;
				colvarMemberUniqueId.IsReadOnly = false;
				colvarMemberUniqueId.DefaultSetting = @"";
				colvarMemberUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberUniqueId);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = true;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("device_identyfier_info",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("IdentifierCode")]
		[Bindable(true)]
		public string IdentifierCode 
		{
			get { return GetColumnValue<string>(Columns.IdentifierCode); }
			set { SetColumnValue(Columns.IdentifierCode, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int? UserId 
		{
			get { return GetColumnValue<int?>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("ClientIp")]
		[Bindable(true)]
		public string ClientIp 
		{
			get { return GetColumnValue<string>(Columns.ClientIp); }
			set { SetColumnValue(Columns.ClientIp, value); }
		}
		  
		[XmlAttribute("ActionTrigger")]
		[Bindable(true)]
		public string ActionTrigger 
		{
			get { return GetColumnValue<string>(Columns.ActionTrigger); }
			set { SetColumnValue(Columns.ActionTrigger, value); }
		}
		  
		[XmlAttribute("Manufacturer")]
		[Bindable(true)]
		public string Manufacturer 
		{
			get { return GetColumnValue<string>(Columns.Manufacturer); }
			set { SetColumnValue(Columns.Manufacturer, value); }
		}
		  
		[XmlAttribute("Model")]
		[Bindable(true)]
		public string Model 
		{
			get { return GetColumnValue<string>(Columns.Model); }
			set { SetColumnValue(Columns.Model, value); }
		}
		  
		[XmlAttribute("Os")]
		[Bindable(true)]
		public string Os 
		{
			get { return GetColumnValue<string>(Columns.Os); }
			set { SetColumnValue(Columns.Os, value); }
		}
		  
		[XmlAttribute("OsVer")]
		[Bindable(true)]
		public string OsVer 
		{
			get { return GetColumnValue<string>(Columns.OsVer); }
			set { SetColumnValue(Columns.OsVer, value); }
		}
		  
		[XmlAttribute("AppVer")]
		[Bindable(true)]
		public string AppVer 
		{
			get { return GetColumnValue<string>(Columns.AppVer); }
			set { SetColumnValue(Columns.AppVer, value); }
		}
		  
		[XmlAttribute("Longitude")]
		[Bindable(true)]
		public double Longitude 
		{
			get { return GetColumnValue<double>(Columns.Longitude); }
			set { SetColumnValue(Columns.Longitude, value); }
		}
		  
		[XmlAttribute("Latitude")]
		[Bindable(true)]
		public double Latitude 
		{
			get { return GetColumnValue<double>(Columns.Latitude); }
			set { SetColumnValue(Columns.Latitude, value); }
		}
		  
		[XmlAttribute("MemberUniqueId")]
		[Bindable(true)]
		public int? MemberUniqueId 
		{
			get { return GetColumnValue<int?>(Columns.MemberUniqueId); }
			set { SetColumnValue(Columns.MemberUniqueId, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int? CityId 
		{
			get { return GetColumnValue<int?>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdentifierCodeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ClientIpColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionTriggerColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ManufacturerColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModelColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn OsColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn OsVerColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn AppVerColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn LongitudeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn LatitudeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberUniqueIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string IdentifierCode = @"identifier_code";
			 public static string UserId = @"user_id";
			 public static string ClientIp = @"client_ip";
			 public static string ActionTrigger = @"action_trigger";
			 public static string Manufacturer = @"manufacturer";
			 public static string Model = @"model";
			 public static string Os = @"os";
			 public static string OsVer = @"os_ver";
			 public static string AppVer = @"app_ver";
			 public static string Longitude = @"longitude";
			 public static string Latitude = @"latitude";
			 public static string MemberUniqueId = @"member_unique_id";
			 public static string CityId = @"city_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

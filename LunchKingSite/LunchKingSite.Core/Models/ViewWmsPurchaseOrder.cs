using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewWmsPurchaseOrderCollection : ReadOnlyList<ViewWmsPurchaseOrder, ViewWmsPurchaseOrderCollection>
	{
			public ViewWmsPurchaseOrderCollection() {}

	}

	[Serializable]
	public partial class ViewWmsPurchaseOrder : ReadOnlyRecord<ViewWmsPurchaseOrder>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewWmsPurchaseOrder()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewWmsPurchaseOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewWmsPurchaseOrder(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewWmsPurchaseOrder(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_wms_purchase_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 100;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarInfoGuid = new TableSchema.TableColumn(schema);
				colvarInfoGuid.ColumnName = "info_guid";
				colvarInfoGuid.DataType = DbType.Guid;
				colvarInfoGuid.MaxLength = 0;
				colvarInfoGuid.AutoIncrement = false;
				colvarInfoGuid.IsNullable = false;
				colvarInfoGuid.IsPrimaryKey = false;
				colvarInfoGuid.IsForeignKey = false;
				colvarInfoGuid.IsReadOnly = false;
				colvarInfoGuid.DefaultSetting = @"";
				colvarInfoGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInfoGuid);

				TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
				colvarItemGuid.ColumnName = "item_guid";
				colvarItemGuid.DataType = DbType.Guid;
				colvarItemGuid.MaxLength = 0;
				colvarItemGuid.AutoIncrement = false;
				colvarItemGuid.IsNullable = false;
				colvarItemGuid.IsPrimaryKey = false;
				colvarItemGuid.IsForeignKey = false;
				colvarItemGuid.IsReadOnly = false;
				colvarItemGuid.DefaultSetting = @"";
				colvarItemGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemGuid);

				TableSchema.TableColumn colvarPurchaseOrderGuid = new TableSchema.TableColumn(schema);
				colvarPurchaseOrderGuid.ColumnName = "purchase_order_guid";
				colvarPurchaseOrderGuid.DataType = DbType.Guid;
				colvarPurchaseOrderGuid.MaxLength = 0;
				colvarPurchaseOrderGuid.AutoIncrement = false;
				colvarPurchaseOrderGuid.IsNullable = false;
				colvarPurchaseOrderGuid.IsPrimaryKey = false;
				colvarPurchaseOrderGuid.IsForeignKey = false;
				colvarPurchaseOrderGuid.IsReadOnly = false;
				colvarPurchaseOrderGuid.DefaultSetting = @"";
				colvarPurchaseOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPurchaseOrderGuid);

				TableSchema.TableColumn colvarPchomeProdId = new TableSchema.TableColumn(schema);
				colvarPchomeProdId.ColumnName = "pchome_prod_id";
				colvarPchomeProdId.DataType = DbType.String;
				colvarPchomeProdId.MaxLength = 50;
				colvarPchomeProdId.AutoIncrement = false;
				colvarPchomeProdId.IsNullable = true;
				colvarPchomeProdId.IsPrimaryKey = false;
				colvarPchomeProdId.IsForeignKey = false;
				colvarPchomeProdId.IsReadOnly = false;
				colvarPchomeProdId.DefaultSetting = @"";
				colvarPchomeProdId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPchomeProdId);

				TableSchema.TableColumn colvarPchomePurchaseOrderId = new TableSchema.TableColumn(schema);
				colvarPchomePurchaseOrderId.ColumnName = "pchome_purchase_order_id";
				colvarPchomePurchaseOrderId.DataType = DbType.AnsiString;
				colvarPchomePurchaseOrderId.MaxLength = 50;
				colvarPchomePurchaseOrderId.AutoIncrement = false;
				colvarPchomePurchaseOrderId.IsNullable = true;
				colvarPchomePurchaseOrderId.IsPrimaryKey = false;
				colvarPchomePurchaseOrderId.IsForeignKey = false;
				colvarPchomePurchaseOrderId.IsReadOnly = false;
				colvarPchomePurchaseOrderId.DefaultSetting = @"";
				colvarPchomePurchaseOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPchomePurchaseOrderId);

				TableSchema.TableColumn colvarProductNo = new TableSchema.TableColumn(schema);
				colvarProductNo.ColumnName = "product_no";
				colvarProductNo.DataType = DbType.Int32;
				colvarProductNo.MaxLength = 0;
				colvarProductNo.AutoIncrement = false;
				colvarProductNo.IsNullable = false;
				colvarProductNo.IsPrimaryKey = false;
				colvarProductNo.IsForeignKey = false;
				colvarProductNo.IsReadOnly = false;
				colvarProductNo.DefaultSetting = @"";
				colvarProductNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductNo);

				TableSchema.TableColumn colvarProductBrandName = new TableSchema.TableColumn(schema);
				colvarProductBrandName.ColumnName = "product_brand_name";
				colvarProductBrandName.DataType = DbType.String;
				colvarProductBrandName.MaxLength = 70;
				colvarProductBrandName.AutoIncrement = false;
				colvarProductBrandName.IsNullable = false;
				colvarProductBrandName.IsPrimaryKey = false;
				colvarProductBrandName.IsForeignKey = false;
				colvarProductBrandName.IsReadOnly = false;
				colvarProductBrandName.DefaultSetting = @"";
				colvarProductBrandName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductBrandName);

				TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
				colvarProductName.ColumnName = "product_name";
				colvarProductName.DataType = DbType.String;
				colvarProductName.MaxLength = 200;
				colvarProductName.AutoIncrement = false;
				colvarProductName.IsNullable = false;
				colvarProductName.IsPrimaryKey = false;
				colvarProductName.IsForeignKey = false;
				colvarProductName.IsReadOnly = false;
				colvarProductName.DefaultSetting = @"";
				colvarProductName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductName);

				TableSchema.TableColumn colvarSpecName = new TableSchema.TableColumn(schema);
				colvarSpecName.ColumnName = "spec_name";
				colvarSpecName.DataType = DbType.String;
				colvarSpecName.MaxLength = 100;
				colvarSpecName.AutoIncrement = false;
				colvarSpecName.IsNullable = true;
				colvarSpecName.IsPrimaryKey = false;
				colvarSpecName.IsForeignKey = false;
				colvarSpecName.IsReadOnly = false;
				colvarSpecName.DefaultSetting = @"";
				colvarSpecName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecName);

				TableSchema.TableColumn colvarProductCode = new TableSchema.TableColumn(schema);
				colvarProductCode.ColumnName = "product_code";
				colvarProductCode.DataType = DbType.AnsiString;
				colvarProductCode.MaxLength = 50;
				colvarProductCode.AutoIncrement = false;
				colvarProductCode.IsNullable = true;
				colvarProductCode.IsPrimaryKey = false;
				colvarProductCode.IsForeignKey = false;
				colvarProductCode.IsReadOnly = false;
				colvarProductCode.DefaultSetting = @"";
				colvarProductCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductCode);

				TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
				colvarPrice.ColumnName = "price";
				colvarPrice.DataType = DbType.Int32;
				colvarPrice.MaxLength = 0;
				colvarPrice.AutoIncrement = false;
				colvarPrice.IsNullable = true;
				colvarPrice.IsPrimaryKey = false;
				colvarPrice.IsForeignKey = false;
				colvarPrice.IsReadOnly = false;
				colvarPrice.DefaultSetting = @"";
				colvarPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrice);

				TableSchema.TableColumn colvarWarehouseType = new TableSchema.TableColumn(schema);
				colvarWarehouseType.ColumnName = "warehouse_type";
				colvarWarehouseType.DataType = DbType.Int32;
				colvarWarehouseType.MaxLength = 0;
				colvarWarehouseType.AutoIncrement = false;
				colvarWarehouseType.IsNullable = false;
				colvarWarehouseType.IsPrimaryKey = false;
				colvarWarehouseType.IsForeignKey = false;
				colvarWarehouseType.IsReadOnly = false;
				colvarWarehouseType.DefaultSetting = @"";
				colvarWarehouseType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWarehouseType);

				TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
				colvarQty.ColumnName = "qty";
				colvarQty.DataType = DbType.Int32;
				colvarQty.MaxLength = 0;
				colvarQty.AutoIncrement = false;
				colvarQty.IsNullable = false;
				colvarQty.IsPrimaryKey = false;
				colvarQty.IsForeignKey = false;
				colvarQty.IsReadOnly = false;
				colvarQty.DefaultSetting = @"";
				colvarQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQty);

				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);

				TableSchema.TableColumn colvarSalesId = new TableSchema.TableColumn(schema);
				colvarSalesId.ColumnName = "sales_id";
				colvarSalesId.DataType = DbType.Int32;
				colvarSalesId.MaxLength = 0;
				colvarSalesId.AutoIncrement = false;
				colvarSalesId.IsNullable = false;
				colvarSalesId.IsPrimaryKey = false;
				colvarSalesId.IsForeignKey = false;
				colvarSalesId.IsReadOnly = false;
				colvarSalesId.DefaultSetting = @"";
				colvarSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesId);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarActualQty = new TableSchema.TableColumn(schema);
				colvarActualQty.ColumnName = "actual_qty";
				colvarActualQty.DataType = DbType.Int32;
				colvarActualQty.MaxLength = 0;
				colvarActualQty.AutoIncrement = false;
				colvarActualQty.IsNullable = true;
				colvarActualQty.IsPrimaryKey = false;
				colvarActualQty.IsForeignKey = false;
				colvarActualQty.IsReadOnly = false;
				colvarActualQty.DefaultSetting = @"";
				colvarActualQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActualQty);

				TableSchema.TableColumn colvarRefundQty = new TableSchema.TableColumn(schema);
				colvarRefundQty.ColumnName = "refund_qty";
				colvarRefundQty.DataType = DbType.Int32;
				colvarRefundQty.MaxLength = 0;
				colvarRefundQty.AutoIncrement = false;
				colvarRefundQty.IsNullable = true;
				colvarRefundQty.IsPrimaryKey = false;
				colvarRefundQty.IsForeignKey = false;
				colvarRefundQty.IsReadOnly = false;
				colvarRefundQty.DefaultSetting = @"";
				colvarRefundQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundQty);

				TableSchema.TableColumn colvarInvalidation = new TableSchema.TableColumn(schema);
				colvarInvalidation.ColumnName = "invalidation";
				colvarInvalidation.DataType = DbType.Int32;
				colvarInvalidation.MaxLength = 0;
				colvarInvalidation.AutoIncrement = false;
				colvarInvalidation.IsNullable = false;
				colvarInvalidation.IsPrimaryKey = false;
				colvarInvalidation.IsForeignKey = false;
				colvarInvalidation.IsReadOnly = false;
				colvarInvalidation.DefaultSetting = @"";
				colvarInvalidation.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvalidation);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_wms_purchase_order",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("InfoGuid")]
		[Bindable(true)]
		public Guid InfoGuid
		{
			get { return GetColumnValue<Guid>(Columns.InfoGuid); }
			set { SetColumnValue(Columns.InfoGuid, value); }
		}

		[XmlAttribute("ItemGuid")]
		[Bindable(true)]
		public Guid ItemGuid
		{
			get { return GetColumnValue<Guid>(Columns.ItemGuid); }
			set { SetColumnValue(Columns.ItemGuid, value); }
		}

		[XmlAttribute("PurchaseOrderGuid")]
		[Bindable(true)]
		public Guid PurchaseOrderGuid
		{
			get { return GetColumnValue<Guid>(Columns.PurchaseOrderGuid); }
			set { SetColumnValue(Columns.PurchaseOrderGuid, value); }
		}

		[XmlAttribute("PchomeProdId")]
		[Bindable(true)]
		public string PchomeProdId
		{
			get { return GetColumnValue<string>(Columns.PchomeProdId); }
			set { SetColumnValue(Columns.PchomeProdId, value); }
		}

		[XmlAttribute("PchomePurchaseOrderId")]
		[Bindable(true)]
		public string PchomePurchaseOrderId
		{
			get { return GetColumnValue<string>(Columns.PchomePurchaseOrderId); }
			set { SetColumnValue(Columns.PchomePurchaseOrderId, value); }
		}

		[XmlAttribute("ProductNo")]
		[Bindable(true)]
		public int ProductNo
		{
			get { return GetColumnValue<int>(Columns.ProductNo); }
			set { SetColumnValue(Columns.ProductNo, value); }
		}

		[XmlAttribute("ProductBrandName")]
		[Bindable(true)]
		public string ProductBrandName
		{
			get { return GetColumnValue<string>(Columns.ProductBrandName); }
			set { SetColumnValue(Columns.ProductBrandName, value); }
		}

		[XmlAttribute("ProductName")]
		[Bindable(true)]
		public string ProductName
		{
			get { return GetColumnValue<string>(Columns.ProductName); }
			set { SetColumnValue(Columns.ProductName, value); }
		}

		[XmlAttribute("SpecName")]
		[Bindable(true)]
		public string SpecName
		{
			get { return GetColumnValue<string>(Columns.SpecName); }
			set { SetColumnValue(Columns.SpecName, value); }
		}

		[XmlAttribute("ProductCode")]
		[Bindable(true)]
		public string ProductCode
		{
			get { return GetColumnValue<string>(Columns.ProductCode); }
			set { SetColumnValue(Columns.ProductCode, value); }
		}

		[XmlAttribute("Price")]
		[Bindable(true)]
		public int? Price
		{
			get { return GetColumnValue<int?>(Columns.Price); }
			set { SetColumnValue(Columns.Price, value); }
		}

		[XmlAttribute("WarehouseType")]
		[Bindable(true)]
		public int WarehouseType
		{
			get { return GetColumnValue<int>(Columns.WarehouseType); }
			set { SetColumnValue(Columns.WarehouseType, value); }
		}

		[XmlAttribute("Qty")]
		[Bindable(true)]
		public int Qty
		{
			get { return GetColumnValue<int>(Columns.Qty); }
			set { SetColumnValue(Columns.Qty, value); }
		}

		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}

		[XmlAttribute("SalesId")]
		[Bindable(true)]
		public int SalesId
		{
			get { return GetColumnValue<int>(Columns.SalesId); }
			set { SetColumnValue(Columns.SalesId, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("ActualQty")]
		[Bindable(true)]
		public int? ActualQty
		{
			get { return GetColumnValue<int?>(Columns.ActualQty); }
			set { SetColumnValue(Columns.ActualQty, value); }
		}

		[XmlAttribute("RefundQty")]
		[Bindable(true)]
		public int? RefundQty
		{
			get { return GetColumnValue<int?>(Columns.RefundQty); }
			set { SetColumnValue(Columns.RefundQty, value); }
		}

		[XmlAttribute("Invalidation")]
		[Bindable(true)]
		public int Invalidation
		{
			get { return GetColumnValue<int>(Columns.Invalidation); }
			set { SetColumnValue(Columns.Invalidation, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn SellerNameColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn InfoGuidColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn ItemGuidColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn PurchaseOrderGuidColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn PchomeProdIdColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn PchomePurchaseOrderIdColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn ProductNoColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn ProductBrandNameColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn ProductNameColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn SpecNameColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn ProductCodeColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn PriceColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn WarehouseTypeColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn QtyColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn StatusColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn SalesIdColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn ActualQtyColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn RefundQtyColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn InvalidationColumn
		{
			get { return Schema.Columns[20]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string SellerName = @"seller_name";
			public static string SellerGuid = @"seller_guid";
			public static string InfoGuid = @"info_guid";
			public static string ItemGuid = @"item_guid";
			public static string PurchaseOrderGuid = @"purchase_order_guid";
			public static string PchomeProdId = @"pchome_prod_id";
			public static string PchomePurchaseOrderId = @"pchome_purchase_order_id";
			public static string ProductNo = @"product_no";
			public static string ProductBrandName = @"product_brand_name";
			public static string ProductName = @"product_name";
			public static string SpecName = @"spec_name";
			public static string ProductCode = @"product_code";
			public static string Price = @"price";
			public static string WarehouseType = @"warehouse_type";
			public static string Qty = @"qty";
			public static string Status = @"status";
			public static string SalesId = @"sales_id";
			public static string CreateTime = @"create_time";
			public static string ActualQty = @"actual_qty";
			public static string RefundQty = @"refund_qty";
			public static string Invalidation = @"invalidation";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

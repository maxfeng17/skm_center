using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewDiscountEventCampaign class.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountEventCampaignCollection : ReadOnlyList<ViewDiscountEventCampaign, ViewDiscountEventCampaignCollection>
    {        
        public ViewDiscountEventCampaignCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_discount_event_campaign view.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountEventCampaign : ReadOnlyRecord<ViewDiscountEventCampaign>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_discount_event_campaign", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = true;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarCampaignId = new TableSchema.TableColumn(schema);
                colvarCampaignId.ColumnName = "campaign_id";
                colvarCampaignId.DataType = DbType.Int32;
                colvarCampaignId.MaxLength = 0;
                colvarCampaignId.AutoIncrement = false;
                colvarCampaignId.IsNullable = false;
                colvarCampaignId.IsPrimaryKey = false;
                colvarCampaignId.IsForeignKey = false;
                colvarCampaignId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCampaignId);
                
                TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
                colvarEventId.ColumnName = "event_id";
                colvarEventId.DataType = DbType.Int32;
                colvarEventId.MaxLength = 0;
                colvarEventId.AutoIncrement = false;
                colvarEventId.IsNullable = true;
                colvarEventId.IsPrimaryKey = false;
                colvarEventId.IsForeignKey = false;
                colvarEventId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventId);
                
                TableSchema.TableColumn colvarEventQty = new TableSchema.TableColumn(schema);
                colvarEventQty.ColumnName = "event_qty";
                colvarEventQty.DataType = DbType.Int32;
                colvarEventQty.MaxLength = 0;
                colvarEventQty.AutoIncrement = false;
                colvarEventQty.IsNullable = true;
                colvarEventQty.IsPrimaryKey = false;
                colvarEventQty.IsForeignKey = false;
                colvarEventQty.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventQty);
                
                TableSchema.TableColumn colvarCampaignName = new TableSchema.TableColumn(schema);
                colvarCampaignName.ColumnName = "campaign_name";
                colvarCampaignName.DataType = DbType.String;
                colvarCampaignName.MaxLength = 100;
                colvarCampaignName.AutoIncrement = false;
                colvarCampaignName.IsNullable = true;
                colvarCampaignName.IsPrimaryKey = false;
                colvarCampaignName.IsForeignKey = false;
                colvarCampaignName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCampaignName);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = true;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarCampaignQty = new TableSchema.TableColumn(schema);
                colvarCampaignQty.ColumnName = "campaign_qty";
                colvarCampaignQty.DataType = DbType.Int32;
                colvarCampaignQty.MaxLength = 0;
                colvarCampaignQty.AutoIncrement = false;
                colvarCampaignQty.IsNullable = true;
                colvarCampaignQty.IsPrimaryKey = false;
                colvarCampaignQty.IsForeignKey = false;
                colvarCampaignQty.IsReadOnly = false;
                
                schema.Columns.Add(colvarCampaignQty);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarApplyTime = new TableSchema.TableColumn(schema);
                colvarApplyTime.ColumnName = "apply_time";
                colvarApplyTime.DataType = DbType.DateTime;
                colvarApplyTime.MaxLength = 0;
                colvarApplyTime.AutoIncrement = false;
                colvarApplyTime.IsNullable = true;
                colvarApplyTime.IsPrimaryKey = false;
                colvarApplyTime.IsForeignKey = false;
                colvarApplyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarApplyTime);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = true;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
                colvarFlag.ColumnName = "flag";
                colvarFlag.DataType = DbType.Int32;
                colvarFlag.MaxLength = 0;
                colvarFlag.AutoIncrement = false;
                colvarFlag.IsNullable = false;
                colvarFlag.IsPrimaryKey = false;
                colvarFlag.IsForeignKey = false;
                colvarFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarFlag);
                
                TableSchema.TableColumn colvarMinimumAmount = new TableSchema.TableColumn(schema);
                colvarMinimumAmount.ColumnName = "minimum_amount";
                colvarMinimumAmount.DataType = DbType.Int32;
                colvarMinimumAmount.MaxLength = 0;
                colvarMinimumAmount.AutoIncrement = false;
                colvarMinimumAmount.IsNullable = true;
                colvarMinimumAmount.IsPrimaryKey = false;
                colvarMinimumAmount.IsForeignKey = false;
                colvarMinimumAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarMinimumAmount);
                
                TableSchema.TableColumn colvarCancelTime = new TableSchema.TableColumn(schema);
                colvarCancelTime.ColumnName = "cancel_time";
                colvarCancelTime.DataType = DbType.DateTime;
                colvarCancelTime.MaxLength = 0;
                colvarCancelTime.AutoIncrement = false;
                colvarCancelTime.IsNullable = true;
                colvarCancelTime.IsPrimaryKey = false;
                colvarCancelTime.IsForeignKey = false;
                colvarCancelTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCancelTime);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_discount_event_campaign",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewDiscountEventCampaign()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewDiscountEventCampaign(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewDiscountEventCampaign(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewDiscountEventCampaign(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int? Id 
	    {
		    get
		    {
			    return GetColumnValue<int?>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("CampaignId")]
        [Bindable(true)]
        public int CampaignId 
	    {
		    get
		    {
			    return GetColumnValue<int>("campaign_id");
		    }
            set 
		    {
			    SetColumnValue("campaign_id", value);
            }
        }
	      
        [XmlAttribute("EventId")]
        [Bindable(true)]
        public int? EventId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("event_id");
		    }
            set 
		    {
			    SetColumnValue("event_id", value);
            }
        }
	      
        [XmlAttribute("EventQty")]
        [Bindable(true)]
        public int? EventQty 
	    {
		    get
		    {
			    return GetColumnValue<int?>("event_qty");
		    }
            set 
		    {
			    SetColumnValue("event_qty", value);
            }
        }
	      
        [XmlAttribute("CampaignName")]
        [Bindable(true)]
        public string CampaignName 
	    {
		    get
		    {
			    return GetColumnValue<string>("campaign_name");
		    }
            set 
		    {
			    SetColumnValue("campaign_name", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal? Amount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("amount");
		    }
            set 
		    {
			    SetColumnValue("amount", value);
            }
        }
	      
        [XmlAttribute("CampaignQty")]
        [Bindable(true)]
        public int? CampaignQty 
	    {
		    get
		    {
			    return GetColumnValue<int?>("campaign_qty");
		    }
            set 
		    {
			    SetColumnValue("campaign_qty", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal? Total 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("ApplyTime")]
        [Bindable(true)]
        public DateTime? ApplyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("apply_time");
		    }
            set 
		    {
			    SetColumnValue("apply_time", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime? StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("Flag")]
        [Bindable(true)]
        public int Flag 
	    {
		    get
		    {
			    return GetColumnValue<int>("flag");
		    }
            set 
		    {
			    SetColumnValue("flag", value);
            }
        }
	      
        [XmlAttribute("MinimumAmount")]
        [Bindable(true)]
        public int? MinimumAmount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("minimum_amount");
		    }
            set 
		    {
			    SetColumnValue("minimum_amount", value);
            }
        }
	      
        [XmlAttribute("CancelTime")]
        [Bindable(true)]
        public DateTime? CancelTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("cancel_time");
		    }
            set 
		    {
			    SetColumnValue("cancel_time", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string CampaignId = @"campaign_id";
            
            public static string EventId = @"event_id";
            
            public static string EventQty = @"event_qty";
            
            public static string CampaignName = @"campaign_name";
            
            public static string Amount = @"amount";
            
            public static string CampaignQty = @"campaign_qty";
            
            public static string Total = @"total";
            
            public static string ApplyTime = @"apply_time";
            
            public static string StartTime = @"start_time";
            
            public static string EndTime = @"end_time";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string Flag = @"flag";
            
            public static string MinimumAmount = @"minimum_amount";
            
            public static string CancelTime = @"cancel_time";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

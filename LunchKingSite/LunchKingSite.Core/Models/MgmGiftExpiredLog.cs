using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MgmGiftExpiredLog class.
	/// </summary>
    [Serializable]
	public partial class MgmGiftExpiredLogCollection : RepositoryList<MgmGiftExpiredLog, MgmGiftExpiredLogCollection>
	{	   
		public MgmGiftExpiredLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MgmGiftExpiredLogCollection</returns>
		public MgmGiftExpiredLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MgmGiftExpiredLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the mgm_gift_expired_log table.
	/// </summary>
	[Serializable]
	public partial class MgmGiftExpiredLog : RepositoryRecord<MgmGiftExpiredLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MgmGiftExpiredLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MgmGiftExpiredLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("mgm_gift_expired_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarGiftAccessCode = new TableSchema.TableColumn(schema);
				colvarGiftAccessCode.ColumnName = "gift_access_code";
				colvarGiftAccessCode.DataType = DbType.Guid;
				colvarGiftAccessCode.MaxLength = 0;
				colvarGiftAccessCode.AutoIncrement = false;
				colvarGiftAccessCode.IsNullable = false;
				colvarGiftAccessCode.IsPrimaryKey = false;
				colvarGiftAccessCode.IsForeignKey = false;
				colvarGiftAccessCode.IsReadOnly = false;
				colvarGiftAccessCode.DefaultSetting = @"";
				colvarGiftAccessCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGiftAccessCode);
				
				TableSchema.TableColumn colvarSenderId = new TableSchema.TableColumn(schema);
				colvarSenderId.ColumnName = "sender_id";
				colvarSenderId.DataType = DbType.Int32;
				colvarSenderId.MaxLength = 0;
				colvarSenderId.AutoIncrement = false;
				colvarSenderId.IsNullable = false;
				colvarSenderId.IsPrimaryKey = false;
				colvarSenderId.IsForeignKey = false;
				colvarSenderId.IsReadOnly = false;
				colvarSenderId.DefaultSetting = @"";
				colvarSenderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSenderId);
				
				TableSchema.TableColumn colvarSendTime = new TableSchema.TableColumn(schema);
				colvarSendTime.ColumnName = "send_time";
				colvarSendTime.DataType = DbType.DateTime;
				colvarSendTime.MaxLength = 0;
				colvarSendTime.AutoIncrement = false;
				colvarSendTime.IsNullable = true;
				colvarSendTime.IsPrimaryKey = false;
				colvarSendTime.IsForeignKey = false;
				colvarSendTime.IsReadOnly = false;
				colvarSendTime.DefaultSetting = @"";
				colvarSendTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendTime);
				
				TableSchema.TableColumn colvarMatchValue = new TableSchema.TableColumn(schema);
				colvarMatchValue.ColumnName = "match_value";
				colvarMatchValue.DataType = DbType.String;
				colvarMatchValue.MaxLength = 200;
				colvarMatchValue.AutoIncrement = false;
				colvarMatchValue.IsNullable = true;
				colvarMatchValue.IsPrimaryKey = false;
				colvarMatchValue.IsForeignKey = false;
				colvarMatchValue.IsReadOnly = false;
				colvarMatchValue.DefaultSetting = @"";
				colvarMatchValue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMatchValue);
				
				TableSchema.TableColumn colvarCreatedTime = new TableSchema.TableColumn(schema);
				colvarCreatedTime.ColumnName = "created_time";
				colvarCreatedTime.DataType = DbType.DateTime;
				colvarCreatedTime.MaxLength = 0;
				colvarCreatedTime.AutoIncrement = false;
				colvarCreatedTime.IsNullable = true;
				colvarCreatedTime.IsPrimaryKey = false;
				colvarCreatedTime.IsForeignKey = false;
				colvarCreatedTime.IsReadOnly = false;
				colvarCreatedTime.DefaultSetting = @"";
				colvarCreatedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("mgm_gift_expired_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("GiftAccessCode")]
		[Bindable(true)]
		public Guid GiftAccessCode 
		{
			get { return GetColumnValue<Guid>(Columns.GiftAccessCode); }
			set { SetColumnValue(Columns.GiftAccessCode, value); }
		}
		  
		[XmlAttribute("SenderId")]
		[Bindable(true)]
		public int SenderId 
		{
			get { return GetColumnValue<int>(Columns.SenderId); }
			set { SetColumnValue(Columns.SenderId, value); }
		}
		  
		[XmlAttribute("SendTime")]
		[Bindable(true)]
		public DateTime? SendTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.SendTime); }
			set { SetColumnValue(Columns.SendTime, value); }
		}
		  
		[XmlAttribute("MatchValue")]
		[Bindable(true)]
		public string MatchValue 
		{
			get { return GetColumnValue<string>(Columns.MatchValue); }
			set { SetColumnValue(Columns.MatchValue, value); }
		}
		  
		[XmlAttribute("CreatedTime")]
		[Bindable(true)]
		public DateTime? CreatedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreatedTime); }
			set { SetColumnValue(Columns.CreatedTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GiftAccessCodeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SenderIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SendTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn MatchValueColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string GiftAccessCode = @"gift_access_code";
			 public static string SenderId = @"sender_id";
			 public static string SendTime = @"send_time";
			 public static string MatchValue = @"match_value";
			 public static string CreatedTime = @"created_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

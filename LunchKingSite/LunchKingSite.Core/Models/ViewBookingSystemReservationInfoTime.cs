using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBookingSystemReservationInfoTime class.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemReservationInfoTimeCollection : ReadOnlyList<ViewBookingSystemReservationInfoTime, ViewBookingSystemReservationInfoTimeCollection>
    {        
        public ViewBookingSystemReservationInfoTimeCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_booking_system_reservation_info_time view.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemReservationInfoTime : ReadOnlyRecord<ViewBookingSystemReservationInfoTime>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_booking_system_reservation_info_time", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBookingSystemStoreBookingId = new TableSchema.TableColumn(schema);
                colvarBookingSystemStoreBookingId.ColumnName = "booking_system_store_booking_id";
                colvarBookingSystemStoreBookingId.DataType = DbType.Int32;
                colvarBookingSystemStoreBookingId.MaxLength = 0;
                colvarBookingSystemStoreBookingId.AutoIncrement = false;
                colvarBookingSystemStoreBookingId.IsNullable = false;
                colvarBookingSystemStoreBookingId.IsPrimaryKey = false;
                colvarBookingSystemStoreBookingId.IsForeignKey = false;
                colvarBookingSystemStoreBookingId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBookingSystemStoreBookingId);
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarReservationDate = new TableSchema.TableColumn(schema);
                colvarReservationDate.ColumnName = "reservation_date";
                colvarReservationDate.DataType = DbType.DateTime;
                colvarReservationDate.MaxLength = 0;
                colvarReservationDate.AutoIncrement = false;
                colvarReservationDate.IsNullable = true;
                colvarReservationDate.IsPrimaryKey = false;
                colvarReservationDate.IsForeignKey = false;
                colvarReservationDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarReservationDate);
                
                TableSchema.TableColumn colvarTimeSlot = new TableSchema.TableColumn(schema);
                colvarTimeSlot.ColumnName = "time_slot";
                colvarTimeSlot.DataType = DbType.AnsiString;
                colvarTimeSlot.MaxLength = 5;
                colvarTimeSlot.AutoIncrement = false;
                colvarTimeSlot.IsNullable = false;
                colvarTimeSlot.IsPrimaryKey = false;
                colvarTimeSlot.IsForeignKey = false;
                colvarTimeSlot.IsReadOnly = false;
                
                schema.Columns.Add(colvarTimeSlot);
                
                TableSchema.TableColumn colvarMaxNumberOfPeople = new TableSchema.TableColumn(schema);
                colvarMaxNumberOfPeople.ColumnName = "max_number_of_people";
                colvarMaxNumberOfPeople.DataType = DbType.Int32;
                colvarMaxNumberOfPeople.MaxLength = 0;
                colvarMaxNumberOfPeople.AutoIncrement = false;
                colvarMaxNumberOfPeople.IsNullable = false;
                colvarMaxNumberOfPeople.IsPrimaryKey = false;
                colvarMaxNumberOfPeople.IsForeignKey = false;
                colvarMaxNumberOfPeople.IsReadOnly = false;
                
                schema.Columns.Add(colvarMaxNumberOfPeople);
                
                TableSchema.TableColumn colvarNumberOfPeople = new TableSchema.TableColumn(schema);
                colvarNumberOfPeople.ColumnName = "number_of_people";
                colvarNumberOfPeople.DataType = DbType.Int32;
                colvarNumberOfPeople.MaxLength = 0;
                colvarNumberOfPeople.AutoIncrement = false;
                colvarNumberOfPeople.IsNullable = true;
                colvarNumberOfPeople.IsPrimaryKey = false;
                colvarNumberOfPeople.IsForeignKey = false;
                colvarNumberOfPeople.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumberOfPeople);
                
                TableSchema.TableColumn colvarRemainedQuantity = new TableSchema.TableColumn(schema);
                colvarRemainedQuantity.ColumnName = "remained_quantity";
                colvarRemainedQuantity.DataType = DbType.Int32;
                colvarRemainedQuantity.MaxLength = 0;
                colvarRemainedQuantity.AutoIncrement = false;
                colvarRemainedQuantity.IsNullable = true;
                colvarRemainedQuantity.IsPrimaryKey = false;
                colvarRemainedQuantity.IsForeignKey = false;
                colvarRemainedQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemainedQuantity);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_booking_system_reservation_info_time",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBookingSystemReservationInfoTime()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBookingSystemReservationInfoTime(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBookingSystemReservationInfoTime(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBookingSystemReservationInfoTime(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BookingSystemStoreBookingId")]
        [Bindable(true)]
        public int BookingSystemStoreBookingId 
	    {
		    get
		    {
			    return GetColumnValue<int>("booking_system_store_booking_id");
		    }
            set 
		    {
			    SetColumnValue("booking_system_store_booking_id", value);
            }
        }
	      
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("ReservationDate")]
        [Bindable(true)]
        public DateTime? ReservationDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("reservation_date");
		    }
            set 
		    {
			    SetColumnValue("reservation_date", value);
            }
        }
	      
        [XmlAttribute("TimeSlot")]
        [Bindable(true)]
        public string TimeSlot 
	    {
		    get
		    {
			    return GetColumnValue<string>("time_slot");
		    }
            set 
		    {
			    SetColumnValue("time_slot", value);
            }
        }
	      
        [XmlAttribute("MaxNumberOfPeople")]
        [Bindable(true)]
        public int MaxNumberOfPeople 
	    {
		    get
		    {
			    return GetColumnValue<int>("max_number_of_people");
		    }
            set 
		    {
			    SetColumnValue("max_number_of_people", value);
            }
        }
	      
        [XmlAttribute("NumberOfPeople")]
        [Bindable(true)]
        public int? NumberOfPeople 
	    {
		    get
		    {
			    return GetColumnValue<int?>("number_of_people");
		    }
            set 
		    {
			    SetColumnValue("number_of_people", value);
            }
        }
	      
        [XmlAttribute("RemainedQuantity")]
        [Bindable(true)]
        public int? RemainedQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("remained_quantity");
		    }
            set 
		    {
			    SetColumnValue("remained_quantity", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BookingSystemStoreBookingId = @"booking_system_store_booking_id";
            
            public static string Id = @"id";
            
            public static string ReservationDate = @"reservation_date";
            
            public static string TimeSlot = @"time_slot";
            
            public static string MaxNumberOfPeople = @"max_number_of_people";
            
            public static string NumberOfPeople = @"number_of_people";
            
            public static string RemainedQuantity = @"remained_quantity";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponOrderReturnList class.
    /// </summary>
    [Serializable]
    public partial class ViewPponOrderReturnListCollection : ReadOnlyList<ViewPponOrderReturnList, ViewPponOrderReturnListCollection>
    {        
        public ViewPponOrderReturnListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_order_return_list view.
    /// </summary>
    [Serializable]
    public partial class ViewPponOrderReturnList : ReadOnlyRecord<ViewPponOrderReturnList>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_order_return_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarMemberEmail = new TableSchema.TableColumn(schema);
                colvarMemberEmail.ColumnName = "member_email";
                colvarMemberEmail.DataType = DbType.String;
                colvarMemberEmail.MaxLength = 256;
                colvarMemberEmail.AutoIncrement = false;
                colvarMemberEmail.IsNullable = false;
                colvarMemberEmail.IsPrimaryKey = false;
                colvarMemberEmail.IsForeignKey = false;
                colvarMemberEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberEmail);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = false;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarParentOrderId = new TableSchema.TableColumn(schema);
                colvarParentOrderId.ColumnName = "parent_order_id";
                colvarParentOrderId.DataType = DbType.Guid;
                colvarParentOrderId.MaxLength = 0;
                colvarParentOrderId.AutoIncrement = false;
                colvarParentOrderId.IsNullable = true;
                colvarParentOrderId.IsPrimaryKey = false;
                colvarParentOrderId.IsForeignKey = false;
                colvarParentOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentOrderId);
                
                TableSchema.TableColumn colvarGroupOrderStatus = new TableSchema.TableColumn(schema);
                colvarGroupOrderStatus.ColumnName = "group_order_status";
                colvarGroupOrderStatus.DataType = DbType.Int32;
                colvarGroupOrderStatus.MaxLength = 0;
                colvarGroupOrderStatus.AutoIncrement = false;
                colvarGroupOrderStatus.IsNullable = true;
                colvarGroupOrderStatus.IsPrimaryKey = false;
                colvarGroupOrderStatus.IsForeignKey = false;
                colvarGroupOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupOrderStatus);
                
                TableSchema.TableColumn colvarReturnId = new TableSchema.TableColumn(schema);
                colvarReturnId.ColumnName = "return_id";
                colvarReturnId.DataType = DbType.Int32;
                colvarReturnId.MaxLength = 0;
                colvarReturnId.AutoIncrement = false;
                colvarReturnId.IsNullable = false;
                colvarReturnId.IsPrimaryKey = false;
                colvarReturnId.IsForeignKey = false;
                colvarReturnId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnId);
                
                TableSchema.TableColumn colvarReturnStatus = new TableSchema.TableColumn(schema);
                colvarReturnStatus.ColumnName = "return_status";
                colvarReturnStatus.DataType = DbType.Int32;
                colvarReturnStatus.MaxLength = 0;
                colvarReturnStatus.AutoIncrement = false;
                colvarReturnStatus.IsNullable = false;
                colvarReturnStatus.IsPrimaryKey = false;
                colvarReturnStatus.IsForeignKey = false;
                colvarReturnStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnStatus);
                
                TableSchema.TableColumn colvarReturnFlag = new TableSchema.TableColumn(schema);
                colvarReturnFlag.ColumnName = "return_flag";
                colvarReturnFlag.DataType = DbType.Int32;
                colvarReturnFlag.MaxLength = 0;
                colvarReturnFlag.AutoIncrement = false;
                colvarReturnFlag.IsNullable = false;
                colvarReturnFlag.IsPrimaryKey = false;
                colvarReturnFlag.IsForeignKey = false;
                colvarReturnFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnFlag);
                
                TableSchema.TableColumn colvarReason = new TableSchema.TableColumn(schema);
                colvarReason.ColumnName = "reason";
                colvarReason.DataType = DbType.String;
                colvarReason.MaxLength = -1;
                colvarReason.AutoIncrement = false;
                colvarReason.IsNullable = true;
                colvarReason.IsPrimaryKey = false;
                colvarReason.IsForeignKey = false;
                colvarReason.IsReadOnly = false;
                
                schema.Columns.Add(colvarReason);
                
                TableSchema.TableColumn colvarReturnMessage = new TableSchema.TableColumn(schema);
                colvarReturnMessage.ColumnName = "return_message";
                colvarReturnMessage.DataType = DbType.String;
                colvarReturnMessage.MaxLength = -1;
                colvarReturnMessage.AutoIncrement = false;
                colvarReturnMessage.IsNullable = true;
                colvarReturnMessage.IsPrimaryKey = false;
                colvarReturnMessage.IsForeignKey = false;
                colvarReturnMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnMessage);
                
                TableSchema.TableColumn colvarReturnCreateId = new TableSchema.TableColumn(schema);
                colvarReturnCreateId.ColumnName = "return_create_id";
                colvarReturnCreateId.DataType = DbType.String;
                colvarReturnCreateId.MaxLength = 50;
                colvarReturnCreateId.AutoIncrement = false;
                colvarReturnCreateId.IsNullable = false;
                colvarReturnCreateId.IsPrimaryKey = false;
                colvarReturnCreateId.IsForeignKey = false;
                colvarReturnCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnCreateId);
                
                TableSchema.TableColumn colvarReturnCreateTime = new TableSchema.TableColumn(schema);
                colvarReturnCreateTime.ColumnName = "return_create_time";
                colvarReturnCreateTime.DataType = DbType.DateTime;
                colvarReturnCreateTime.MaxLength = 0;
                colvarReturnCreateTime.AutoIncrement = false;
                colvarReturnCreateTime.IsNullable = false;
                colvarReturnCreateTime.IsPrimaryKey = false;
                colvarReturnCreateTime.IsForeignKey = false;
                colvarReturnCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnCreateTime);
                
                TableSchema.TableColumn colvarReturnModifyId = new TableSchema.TableColumn(schema);
                colvarReturnModifyId.ColumnName = "return_modify_id";
                colvarReturnModifyId.DataType = DbType.String;
                colvarReturnModifyId.MaxLength = 50;
                colvarReturnModifyId.AutoIncrement = false;
                colvarReturnModifyId.IsNullable = true;
                colvarReturnModifyId.IsPrimaryKey = false;
                colvarReturnModifyId.IsForeignKey = false;
                colvarReturnModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnModifyId);
                
                TableSchema.TableColumn colvarReturnModifyTime = new TableSchema.TableColumn(schema);
                colvarReturnModifyTime.ColumnName = "return_modify_time";
                colvarReturnModifyTime.DataType = DbType.DateTime;
                colvarReturnModifyTime.MaxLength = 0;
                colvarReturnModifyTime.AutoIncrement = false;
                colvarReturnModifyTime.IsNullable = true;
                colvarReturnModifyTime.IsPrimaryKey = false;
                colvarReturnModifyTime.IsForeignKey = false;
                colvarReturnModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnModifyTime);
                
                TableSchema.TableColumn colvarReceiverName = new TableSchema.TableColumn(schema);
                colvarReceiverName.ColumnName = "receiver_name";
                colvarReceiverName.DataType = DbType.String;
                colvarReceiverName.MaxLength = 50;
                colvarReceiverName.AutoIncrement = false;
                colvarReceiverName.IsNullable = true;
                colvarReceiverName.IsPrimaryKey = false;
                colvarReceiverName.IsForeignKey = false;
                colvarReceiverName.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverName);
                
                TableSchema.TableColumn colvarReceiverAddress = new TableSchema.TableColumn(schema);
                colvarReceiverAddress.ColumnName = "receiver_address";
                colvarReceiverAddress.DataType = DbType.String;
                colvarReceiverAddress.MaxLength = 200;
                colvarReceiverAddress.AutoIncrement = false;
                colvarReceiverAddress.IsNullable = true;
                colvarReceiverAddress.IsPrimaryKey = false;
                colvarReceiverAddress.IsForeignKey = false;
                colvarReceiverAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverAddress);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarBusinessHourId = new TableSchema.TableColumn(schema);
                colvarBusinessHourId.ColumnName = "business_hour_id";
                colvarBusinessHourId.DataType = DbType.AnsiString;
                colvarBusinessHourId.MaxLength = 20;
                colvarBusinessHourId.AutoIncrement = false;
                colvarBusinessHourId.IsNullable = true;
                colvarBusinessHourId.IsPrimaryKey = false;
                colvarBusinessHourId.IsForeignKey = false;
                colvarBusinessHourId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourId);
                
                TableSchema.TableColumn colvarBusinessHourTypeId = new TableSchema.TableColumn(schema);
                colvarBusinessHourTypeId.ColumnName = "business_hour_type_id";
                colvarBusinessHourTypeId.DataType = DbType.Int32;
                colvarBusinessHourTypeId.MaxLength = 0;
                colvarBusinessHourTypeId.AutoIncrement = false;
                colvarBusinessHourTypeId.IsNullable = false;
                colvarBusinessHourTypeId.IsPrimaryKey = false;
                colvarBusinessHourTypeId.IsForeignKey = false;
                colvarBusinessHourTypeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourTypeId);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
                colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeS.MaxLength = 0;
                colvarBusinessHourDeliverTimeS.AutoIncrement = false;
                colvarBusinessHourDeliverTimeS.IsNullable = true;
                colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeS.IsForeignKey = false;
                colvarBusinessHourDeliverTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeS);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliveryCharge = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliveryCharge.ColumnName = "business_hour_delivery_charge";
                colvarBusinessHourDeliveryCharge.DataType = DbType.Currency;
                colvarBusinessHourDeliveryCharge.MaxLength = 0;
                colvarBusinessHourDeliveryCharge.AutoIncrement = false;
                colvarBusinessHourDeliveryCharge.IsNullable = false;
                colvarBusinessHourDeliveryCharge.IsPrimaryKey = false;
                colvarBusinessHourDeliveryCharge.IsForeignKey = false;
                colvarBusinessHourDeliveryCharge.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliveryCharge);
                
                TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
                colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
                colvarBusinessHourOrderMinimum.MaxLength = 0;
                colvarBusinessHourOrderMinimum.AutoIncrement = false;
                colvarBusinessHourOrderMinimum.IsNullable = false;
                colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
                colvarBusinessHourOrderMinimum.IsForeignKey = false;
                colvarBusinessHourOrderMinimum.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderMinimum);
                
                TableSchema.TableColumn colvarBusinessHourPreparationTime = new TableSchema.TableColumn(schema);
                colvarBusinessHourPreparationTime.ColumnName = "business_hour_preparation_time";
                colvarBusinessHourPreparationTime.DataType = DbType.Int32;
                colvarBusinessHourPreparationTime.MaxLength = 0;
                colvarBusinessHourPreparationTime.AutoIncrement = false;
                colvarBusinessHourPreparationTime.IsNullable = false;
                colvarBusinessHourPreparationTime.IsPrimaryKey = false;
                colvarBusinessHourPreparationTime.IsForeignKey = false;
                colvarBusinessHourPreparationTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourPreparationTime);
                
                TableSchema.TableColumn colvarBusinessHourOnline = new TableSchema.TableColumn(schema);
                colvarBusinessHourOnline.ColumnName = "business_hour_online";
                colvarBusinessHourOnline.DataType = DbType.Boolean;
                colvarBusinessHourOnline.MaxLength = 0;
                colvarBusinessHourOnline.AutoIncrement = false;
                colvarBusinessHourOnline.IsNullable = false;
                colvarBusinessHourOnline.IsPrimaryKey = false;
                colvarBusinessHourOnline.IsForeignKey = false;
                colvarBusinessHourOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOnline);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
                colvarOrderTotalLimit.ColumnName = "order_total_limit";
                colvarOrderTotalLimit.DataType = DbType.Currency;
                colvarOrderTotalLimit.MaxLength = 0;
                colvarOrderTotalLimit.AutoIncrement = false;
                colvarOrderTotalLimit.IsNullable = true;
                colvarOrderTotalLimit.IsPrimaryKey = false;
                colvarOrderTotalLimit.IsForeignKey = false;
                colvarOrderTotalLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTotalLimit);
                
                TableSchema.TableColumn colvarDeliveryLimit = new TableSchema.TableColumn(schema);
                colvarDeliveryLimit.ColumnName = "delivery_limit";
                colvarDeliveryLimit.DataType = DbType.Int32;
                colvarDeliveryLimit.MaxLength = 0;
                colvarDeliveryLimit.AutoIncrement = false;
                colvarDeliveryLimit.IsNullable = true;
                colvarDeliveryLimit.IsPrimaryKey = false;
                colvarDeliveryLimit.IsForeignKey = false;
                colvarDeliveryLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryLimit);
                
                TableSchema.TableColumn colvarHoliday = new TableSchema.TableColumn(schema);
                colvarHoliday.ColumnName = "holiday";
                colvarHoliday.DataType = DbType.Int32;
                colvarHoliday.MaxLength = 0;
                colvarHoliday.AutoIncrement = false;
                colvarHoliday.IsNullable = false;
                colvarHoliday.IsPrimaryKey = false;
                colvarHoliday.IsForeignKey = false;
                colvarHoliday.IsReadOnly = false;
                
                schema.Columns.Add(colvarHoliday);
                
                TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
                colvarEventName.ColumnName = "event_name";
                colvarEventName.DataType = DbType.String;
                colvarEventName.MaxLength = 400;
                colvarEventName.AutoIncrement = false;
                colvarEventName.IsNullable = true;
                colvarEventName.IsPrimaryKey = false;
                colvarEventName.IsForeignKey = false;
                colvarEventName.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventName);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
                colvarDepartment.ColumnName = "department";
                colvarDepartment.DataType = DbType.Int32;
                colvarDepartment.MaxLength = 0;
                colvarDepartment.AutoIncrement = false;
                colvarDepartment.IsNullable = false;
                colvarDepartment.IsPrimaryKey = false;
                colvarDepartment.IsForeignKey = false;
                colvarDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepartment);
                
                TableSchema.TableColumn colvarCouponIds = new TableSchema.TableColumn(schema);
                colvarCouponIds.ColumnName = "coupon_ids";
                colvarCouponIds.DataType = DbType.AnsiString;
                colvarCouponIds.MaxLength = -1;
                colvarCouponIds.AutoIncrement = false;
                colvarCouponIds.IsNullable = true;
                colvarCouponIds.IsPrimaryKey = false;
                colvarCouponIds.IsForeignKey = false;
                colvarCouponIds.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponIds);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = true;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
                colvarMobileNumber.ColumnName = "mobile_number";
                colvarMobileNumber.DataType = DbType.AnsiString;
                colvarMobileNumber.MaxLength = 50;
                colvarMobileNumber.AutoIncrement = false;
                colvarMobileNumber.IsNullable = true;
                colvarMobileNumber.IsPrimaryKey = false;
                colvarMobileNumber.IsForeignKey = false;
                colvarMobileNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobileNumber);
                
                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryAddress);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarReturnedPersonEmail = new TableSchema.TableColumn(schema);
                colvarReturnedPersonEmail.ColumnName = "returned_person_email";
                colvarReturnedPersonEmail.DataType = DbType.AnsiString;
                colvarReturnedPersonEmail.MaxLength = 200;
                colvarReturnedPersonEmail.AutoIncrement = false;
                colvarReturnedPersonEmail.IsNullable = true;
                colvarReturnedPersonEmail.IsPrimaryKey = false;
                colvarReturnedPersonEmail.IsForeignKey = false;
                colvarReturnedPersonEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonEmail);
                
                TableSchema.TableColumn colvarReturnedPersonTel = new TableSchema.TableColumn(schema);
                colvarReturnedPersonTel.ColumnName = "returned_person_tel";
                colvarReturnedPersonTel.DataType = DbType.AnsiString;
                colvarReturnedPersonTel.MaxLength = 100;
                colvarReturnedPersonTel.AutoIncrement = false;
                colvarReturnedPersonTel.IsNullable = true;
                colvarReturnedPersonTel.IsPrimaryKey = false;
                colvarReturnedPersonTel.IsForeignKey = false;
                colvarReturnedPersonTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonTel);
                
                TableSchema.TableColumn colvarReturnedPersonName = new TableSchema.TableColumn(schema);
                colvarReturnedPersonName.ColumnName = "returned_person_name";
                colvarReturnedPersonName.DataType = DbType.String;
                colvarReturnedPersonName.MaxLength = 100;
                colvarReturnedPersonName.AutoIncrement = false;
                colvarReturnedPersonName.IsNullable = true;
                colvarReturnedPersonName.IsPrimaryKey = false;
                colvarReturnedPersonName.IsForeignKey = false;
                colvarReturnedPersonName.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonName);
                
                TableSchema.TableColumn colvarSellerContactPerson = new TableSchema.TableColumn(schema);
                colvarSellerContactPerson.ColumnName = "seller_contact_person";
                colvarSellerContactPerson.DataType = DbType.String;
                colvarSellerContactPerson.MaxLength = 100;
                colvarSellerContactPerson.AutoIncrement = false;
                colvarSellerContactPerson.IsNullable = true;
                colvarSellerContactPerson.IsPrimaryKey = false;
                colvarSellerContactPerson.IsForeignKey = false;
                colvarSellerContactPerson.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerContactPerson);
                
                TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
                colvarQuantity.ColumnName = "quantity";
                colvarQuantity.DataType = DbType.Int32;
                colvarQuantity.MaxLength = 0;
                colvarQuantity.AutoIncrement = false;
                colvarQuantity.IsNullable = true;
                colvarQuantity.IsPrimaryKey = false;
                colvarQuantity.IsForeignKey = false;
                colvarQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarQuantity);
                
                TableSchema.TableColumn colvarInvoiceMode = new TableSchema.TableColumn(schema);
                colvarInvoiceMode.ColumnName = "invoice_mode";
                colvarInvoiceMode.DataType = DbType.Int32;
                colvarInvoiceMode.MaxLength = 0;
                colvarInvoiceMode.AutoIncrement = false;
                colvarInvoiceMode.IsNullable = true;
                colvarInvoiceMode.IsPrimaryKey = false;
                colvarInvoiceMode.IsForeignKey = false;
                colvarInvoiceMode.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceMode);
                
                TableSchema.TableColumn colvarLabelIconList = new TableSchema.TableColumn(schema);
                colvarLabelIconList.ColumnName = "label_icon_list";
                colvarLabelIconList.DataType = DbType.AnsiString;
                colvarLabelIconList.MaxLength = 255;
                colvarLabelIconList.AutoIncrement = false;
                colvarLabelIconList.IsNullable = true;
                colvarLabelIconList.IsPrimaryKey = false;
                colvarLabelIconList.IsForeignKey = false;
                colvarLabelIconList.IsReadOnly = false;
                
                schema.Columns.Add(colvarLabelIconList);
                
                TableSchema.TableColumn colvarDealEmpName = new TableSchema.TableColumn(schema);
                colvarDealEmpName.ColumnName = "deal_emp_name";
                colvarDealEmpName.DataType = DbType.String;
                colvarDealEmpName.MaxLength = 50;
                colvarDealEmpName.AutoIncrement = false;
                colvarDealEmpName.IsNullable = true;
                colvarDealEmpName.IsPrimaryKey = false;
                colvarDealEmpName.IsForeignKey = false;
                colvarDealEmpName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEmpName);
                
                TableSchema.TableColumn colvarVendorProgressStatus = new TableSchema.TableColumn(schema);
                colvarVendorProgressStatus.ColumnName = "vendor_progress_status";
                colvarVendorProgressStatus.DataType = DbType.Int32;
                colvarVendorProgressStatus.MaxLength = 0;
                colvarVendorProgressStatus.AutoIncrement = false;
                colvarVendorProgressStatus.IsNullable = true;
                colvarVendorProgressStatus.IsPrimaryKey = false;
                colvarVendorProgressStatus.IsForeignKey = false;
                colvarVendorProgressStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorProgressStatus);
                
                TableSchema.TableColumn colvarVendorProcessTime = new TableSchema.TableColumn(schema);
                colvarVendorProcessTime.ColumnName = "vendor_process_time";
                colvarVendorProcessTime.DataType = DbType.DateTime;
                colvarVendorProcessTime.MaxLength = 0;
                colvarVendorProcessTime.AutoIncrement = false;
                colvarVendorProcessTime.IsNullable = true;
                colvarVendorProcessTime.IsPrimaryKey = false;
                colvarVendorProcessTime.IsForeignKey = false;
                colvarVendorProcessTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorProcessTime);
                
                TableSchema.TableColumn colvarVendorMemo = new TableSchema.TableColumn(schema);
                colvarVendorMemo.ColumnName = "vendor_memo";
                colvarVendorMemo.DataType = DbType.String;
                colvarVendorMemo.MaxLength = 500;
                colvarVendorMemo.AutoIncrement = false;
                colvarVendorMemo.IsNullable = true;
                colvarVendorMemo.IsPrimaryKey = false;
                colvarVendorMemo.IsForeignKey = false;
                colvarVendorMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorMemo);
                
                TableSchema.TableColumn colvarOrderShipId = new TableSchema.TableColumn(schema);
                colvarOrderShipId.ColumnName = "order_ship_id";
                colvarOrderShipId.DataType = DbType.Int32;
                colvarOrderShipId.MaxLength = 0;
                colvarOrderShipId.AutoIncrement = false;
                colvarOrderShipId.IsNullable = true;
                colvarOrderShipId.IsPrimaryKey = false;
                colvarOrderShipId.IsForeignKey = false;
                colvarOrderShipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderShipId);
                
                TableSchema.TableColumn colvarPhoneNumber = new TableSchema.TableColumn(schema);
                colvarPhoneNumber.ColumnName = "phone_number";
                colvarPhoneNumber.DataType = DbType.AnsiString;
                colvarPhoneNumber.MaxLength = 50;
                colvarPhoneNumber.AutoIncrement = false;
                colvarPhoneNumber.IsNullable = true;
                colvarPhoneNumber.IsPrimaryKey = false;
                colvarPhoneNumber.IsForeignKey = false;
                colvarPhoneNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhoneNumber);
                
                TableSchema.TableColumn colvarOrignalShipTime = new TableSchema.TableColumn(schema);
                colvarOrignalShipTime.ColumnName = "orignal_ship_time";
                colvarOrignalShipTime.DataType = DbType.DateTime;
                colvarOrignalShipTime.MaxLength = 0;
                colvarOrignalShipTime.AutoIncrement = false;
                colvarOrignalShipTime.IsNullable = true;
                colvarOrignalShipTime.IsPrimaryKey = false;
                colvarOrignalShipTime.IsForeignKey = false;
                colvarOrignalShipTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrignalShipTime);
                
                TableSchema.TableColumn colvarShipCompanyId = new TableSchema.TableColumn(schema);
                colvarShipCompanyId.ColumnName = "ship_company_id";
                colvarShipCompanyId.DataType = DbType.Int32;
                colvarShipCompanyId.MaxLength = 0;
                colvarShipCompanyId.AutoIncrement = false;
                colvarShipCompanyId.IsNullable = true;
                colvarShipCompanyId.IsPrimaryKey = false;
                colvarShipCompanyId.IsForeignKey = false;
                colvarShipCompanyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipCompanyId);
                
                TableSchema.TableColumn colvarShipNo = new TableSchema.TableColumn(schema);
                colvarShipNo.ColumnName = "ship_no";
                colvarShipNo.DataType = DbType.String;
                colvarShipNo.MaxLength = -1;
                colvarShipNo.AutoIncrement = false;
                colvarShipNo.IsNullable = true;
                colvarShipNo.IsPrimaryKey = false;
                colvarShipNo.IsForeignKey = false;
                colvarShipNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipNo);
                
                TableSchema.TableColumn colvarShipTime = new TableSchema.TableColumn(schema);
                colvarShipTime.ColumnName = "ship_time";
                colvarShipTime.DataType = DbType.DateTime;
                colvarShipTime.MaxLength = 0;
                colvarShipTime.AutoIncrement = false;
                colvarShipTime.IsNullable = true;
                colvarShipTime.IsPrimaryKey = false;
                colvarShipTime.IsForeignKey = false;
                colvarShipTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipTime);
                
                TableSchema.TableColumn colvarShipMemo = new TableSchema.TableColumn(schema);
                colvarShipMemo.ColumnName = "ship_memo";
                colvarShipMemo.DataType = DbType.String;
                colvarShipMemo.MaxLength = 60;
                colvarShipMemo.AutoIncrement = false;
                colvarShipMemo.IsNullable = true;
                colvarShipMemo.IsPrimaryKey = false;
                colvarShipMemo.IsForeignKey = false;
                colvarShipMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipMemo);
                
                TableSchema.TableColumn colvarMessageUpdate = new TableSchema.TableColumn(schema);
                colvarMessageUpdate.ColumnName = "message_update";
                colvarMessageUpdate.DataType = DbType.Boolean;
                colvarMessageUpdate.MaxLength = 0;
                colvarMessageUpdate.AutoIncrement = false;
                colvarMessageUpdate.IsNullable = false;
                colvarMessageUpdate.IsPrimaryKey = false;
                colvarMessageUpdate.IsForeignKey = false;
                colvarMessageUpdate.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessageUpdate);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_order_return_list",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponOrderReturnList()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponOrderReturnList(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponOrderReturnList(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponOrderReturnList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("MemberEmail")]
        [Bindable(true)]
        public string MemberEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_email");
		    }
            set 
		    {
			    SetColumnValue("member_email", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal Total 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("ParentOrderId")]
        [Bindable(true)]
        public Guid? ParentOrderId 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("parent_order_id");
		    }
            set 
		    {
			    SetColumnValue("parent_order_id", value);
            }
        }
	      
        [XmlAttribute("GroupOrderStatus")]
        [Bindable(true)]
        public int? GroupOrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_order_status");
		    }
            set 
		    {
			    SetColumnValue("group_order_status", value);
            }
        }
	      
        [XmlAttribute("ReturnId")]
        [Bindable(true)]
        public int ReturnId 
	    {
		    get
		    {
			    return GetColumnValue<int>("return_id");
		    }
            set 
		    {
			    SetColumnValue("return_id", value);
            }
        }
	      
        [XmlAttribute("ReturnStatus")]
        [Bindable(true)]
        public int ReturnStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("return_status");
		    }
            set 
		    {
			    SetColumnValue("return_status", value);
            }
        }
	      
        [XmlAttribute("ReturnFlag")]
        [Bindable(true)]
        public int ReturnFlag 
	    {
		    get
		    {
			    return GetColumnValue<int>("return_flag");
		    }
            set 
		    {
			    SetColumnValue("return_flag", value);
            }
        }
	      
        [XmlAttribute("Reason")]
        [Bindable(true)]
        public string Reason 
	    {
		    get
		    {
			    return GetColumnValue<string>("reason");
		    }
            set 
		    {
			    SetColumnValue("reason", value);
            }
        }
	      
        [XmlAttribute("ReturnMessage")]
        [Bindable(true)]
        public string ReturnMessage 
	    {
		    get
		    {
			    return GetColumnValue<string>("return_message");
		    }
            set 
		    {
			    SetColumnValue("return_message", value);
            }
        }
	      
        [XmlAttribute("ReturnCreateId")]
        [Bindable(true)]
        public string ReturnCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("return_create_id");
		    }
            set 
		    {
			    SetColumnValue("return_create_id", value);
            }
        }
	      
        [XmlAttribute("ReturnCreateTime")]
        [Bindable(true)]
        public DateTime ReturnCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("return_create_time");
		    }
            set 
		    {
			    SetColumnValue("return_create_time", value);
            }
        }
	      
        [XmlAttribute("ReturnModifyId")]
        [Bindable(true)]
        public string ReturnModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("return_modify_id");
		    }
            set 
		    {
			    SetColumnValue("return_modify_id", value);
            }
        }
	      
        [XmlAttribute("ReturnModifyTime")]
        [Bindable(true)]
        public DateTime? ReturnModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("return_modify_time");
		    }
            set 
		    {
			    SetColumnValue("return_modify_time", value);
            }
        }
	      
        [XmlAttribute("ReceiverName")]
        [Bindable(true)]
        public string ReceiverName 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_name");
		    }
            set 
		    {
			    SetColumnValue("receiver_name", value);
            }
        }
	      
        [XmlAttribute("ReceiverAddress")]
        [Bindable(true)]
        public string ReceiverAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_address");
		    }
            set 
		    {
			    SetColumnValue("receiver_address", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("BusinessHourId")]
        [Bindable(true)]
        public string BusinessHourId 
	    {
		    get
		    {
			    return GetColumnValue<string>("business_hour_id");
		    }
            set 
		    {
			    SetColumnValue("business_hour_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourTypeId")]
        [Bindable(true)]
        public int BusinessHourTypeId 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_type_id");
		    }
            set 
		    {
			    SetColumnValue("business_hour_type_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliveryCharge")]
        [Bindable(true)]
        public decimal BusinessHourDeliveryCharge 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_delivery_charge");
		    }
            set 
		    {
			    SetColumnValue("business_hour_delivery_charge", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderMinimum")]
        [Bindable(true)]
        public decimal BusinessHourOrderMinimum 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_order_minimum");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_minimum", value);
            }
        }
	      
        [XmlAttribute("BusinessHourPreparationTime")]
        [Bindable(true)]
        public int BusinessHourPreparationTime 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_preparation_time");
		    }
            set 
		    {
			    SetColumnValue("business_hour_preparation_time", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOnline")]
        [Bindable(true)]
        public bool BusinessHourOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool>("business_hour_online");
		    }
            set 
		    {
			    SetColumnValue("business_hour_online", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("OrderTotalLimit")]
        [Bindable(true)]
        public decimal? OrderTotalLimit 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("order_total_limit");
		    }
            set 
		    {
			    SetColumnValue("order_total_limit", value);
            }
        }
	      
        [XmlAttribute("DeliveryLimit")]
        [Bindable(true)]
        public int? DeliveryLimit 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_limit");
		    }
            set 
		    {
			    SetColumnValue("delivery_limit", value);
            }
        }
	      
        [XmlAttribute("Holiday")]
        [Bindable(true)]
        public int Holiday 
	    {
		    get
		    {
			    return GetColumnValue<int>("holiday");
		    }
            set 
		    {
			    SetColumnValue("holiday", value);
            }
        }
	      
        [XmlAttribute("EventName")]
        [Bindable(true)]
        public string EventName 
	    {
		    get
		    {
			    return GetColumnValue<string>("event_name");
		    }
            set 
		    {
			    SetColumnValue("event_name", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("Department")]
        [Bindable(true)]
        public int Department 
	    {
		    get
		    {
			    return GetColumnValue<int>("department");
		    }
            set 
		    {
			    SetColumnValue("department", value);
            }
        }
	      
        [XmlAttribute("CouponIds")]
        [Bindable(true)]
        public string CouponIds 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_ids");
		    }
            set 
		    {
			    SetColumnValue("coupon_ids", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int? DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("MobileNumber")]
        [Bindable(true)]
        public string MobileNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile_number");
		    }
            set 
		    {
			    SetColumnValue("mobile_number", value);
            }
        }
	      
        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_address");
		    }
            set 
		    {
			    SetColumnValue("delivery_address", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonEmail")]
        [Bindable(true)]
        public string ReturnedPersonEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_email");
		    }
            set 
		    {
			    SetColumnValue("returned_person_email", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonTel")]
        [Bindable(true)]
        public string ReturnedPersonTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_tel");
		    }
            set 
		    {
			    SetColumnValue("returned_person_tel", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonName")]
        [Bindable(true)]
        public string ReturnedPersonName 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_name");
		    }
            set 
		    {
			    SetColumnValue("returned_person_name", value);
            }
        }
	      
        [XmlAttribute("SellerContactPerson")]
        [Bindable(true)]
        public string SellerContactPerson 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_contact_person");
		    }
            set 
		    {
			    SetColumnValue("seller_contact_person", value);
            }
        }
	      
        [XmlAttribute("Quantity")]
        [Bindable(true)]
        public int? Quantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("quantity");
		    }
            set 
		    {
			    SetColumnValue("quantity", value);
            }
        }
	      
        [XmlAttribute("InvoiceMode")]
        [Bindable(true)]
        public int? InvoiceMode 
	    {
		    get
		    {
			    return GetColumnValue<int?>("invoice_mode");
		    }
            set 
		    {
			    SetColumnValue("invoice_mode", value);
            }
        }
	      
        [XmlAttribute("LabelIconList")]
        [Bindable(true)]
        public string LabelIconList 
	    {
		    get
		    {
			    return GetColumnValue<string>("label_icon_list");
		    }
            set 
		    {
			    SetColumnValue("label_icon_list", value);
            }
        }
	      
        [XmlAttribute("DealEmpName")]
        [Bindable(true)]
        public string DealEmpName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_emp_name");
		    }
            set 
		    {
			    SetColumnValue("deal_emp_name", value);
            }
        }
	      
        [XmlAttribute("VendorProgressStatus")]
        [Bindable(true)]
        public int? VendorProgressStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vendor_progress_status");
		    }
            set 
		    {
			    SetColumnValue("vendor_progress_status", value);
            }
        }
	      
        [XmlAttribute("VendorProcessTime")]
        [Bindable(true)]
        public DateTime? VendorProcessTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("vendor_process_time");
		    }
            set 
		    {
			    SetColumnValue("vendor_process_time", value);
            }
        }
	      
        [XmlAttribute("VendorMemo")]
        [Bindable(true)]
        public string VendorMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("vendor_memo");
		    }
            set 
		    {
			    SetColumnValue("vendor_memo", value);
            }
        }
	      
        [XmlAttribute("OrderShipId")]
        [Bindable(true)]
        public int? OrderShipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_ship_id");
		    }
            set 
		    {
			    SetColumnValue("order_ship_id", value);
            }
        }
	      
        [XmlAttribute("PhoneNumber")]
        [Bindable(true)]
        public string PhoneNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("phone_number");
		    }
            set 
		    {
			    SetColumnValue("phone_number", value);
            }
        }
	      
        [XmlAttribute("OrignalShipTime")]
        [Bindable(true)]
        public DateTime? OrignalShipTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("orignal_ship_time");
		    }
            set 
		    {
			    SetColumnValue("orignal_ship_time", value);
            }
        }
	      
        [XmlAttribute("ShipCompanyId")]
        [Bindable(true)]
        public int? ShipCompanyId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ship_company_id");
		    }
            set 
		    {
			    SetColumnValue("ship_company_id", value);
            }
        }
	      
        [XmlAttribute("ShipNo")]
        [Bindable(true)]
        public string ShipNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("ship_no");
		    }
            set 
		    {
			    SetColumnValue("ship_no", value);
            }
        }
	      
        [XmlAttribute("ShipTime")]
        [Bindable(true)]
        public DateTime? ShipTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ship_time");
		    }
            set 
		    {
			    SetColumnValue("ship_time", value);
            }
        }
	      
        [XmlAttribute("ShipMemo")]
        [Bindable(true)]
        public string ShipMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("ship_memo");
		    }
            set 
		    {
			    SetColumnValue("ship_memo", value);
            }
        }
	      
        [XmlAttribute("MessageUpdate")]
        [Bindable(true)]
        public bool MessageUpdate 
	    {
		    get
		    {
			    return GetColumnValue<bool>("message_update");
		    }
            set 
		    {
			    SetColumnValue("message_update", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderGuid = @"order_guid";
            
            public static string OrderId = @"order_id";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string SellerName = @"seller_name";
            
            public static string MemberEmail = @"member_email";
            
            public static string MemberName = @"member_name";
            
            public static string Total = @"total";
            
            public static string OrderStatus = @"order_status";
            
            public static string ParentOrderId = @"parent_order_id";
            
            public static string GroupOrderStatus = @"group_order_status";
            
            public static string ReturnId = @"return_id";
            
            public static string ReturnStatus = @"return_status";
            
            public static string ReturnFlag = @"return_flag";
            
            public static string Reason = @"reason";
            
            public static string ReturnMessage = @"return_message";
            
            public static string ReturnCreateId = @"return_create_id";
            
            public static string ReturnCreateTime = @"return_create_time";
            
            public static string ReturnModifyId = @"return_modify_id";
            
            public static string ReturnModifyTime = @"return_modify_time";
            
            public static string ReceiverName = @"receiver_name";
            
            public static string ReceiverAddress = @"receiver_address";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string BusinessHourId = @"business_hour_id";
            
            public static string BusinessHourTypeId = @"business_hour_type_id";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
            public static string BusinessHourDeliveryCharge = @"business_hour_delivery_charge";
            
            public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
            
            public static string BusinessHourPreparationTime = @"business_hour_preparation_time";
            
            public static string BusinessHourOnline = @"business_hour_online";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string OrderTotalLimit = @"order_total_limit";
            
            public static string DeliveryLimit = @"delivery_limit";
            
            public static string Holiday = @"holiday";
            
            public static string EventName = @"event_name";
            
            public static string ItemName = @"item_name";
            
            public static string Department = @"department";
            
            public static string CouponIds = @"coupon_ids";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string MobileNumber = @"mobile_number";
            
            public static string DeliveryAddress = @"delivery_address";
            
            public static string CouponUsage = @"coupon_usage";
            
            public static string Type = @"type";
            
            public static string UniqueId = @"unique_id";
            
            public static string ReturnedPersonEmail = @"returned_person_email";
            
            public static string ReturnedPersonTel = @"returned_person_tel";
            
            public static string ReturnedPersonName = @"returned_person_name";
            
            public static string SellerContactPerson = @"seller_contact_person";
            
            public static string Quantity = @"quantity";
            
            public static string InvoiceMode = @"invoice_mode";
            
            public static string LabelIconList = @"label_icon_list";
            
            public static string DealEmpName = @"deal_emp_name";
            
            public static string VendorProgressStatus = @"vendor_progress_status";
            
            public static string VendorProcessTime = @"vendor_process_time";
            
            public static string VendorMemo = @"vendor_memo";
            
            public static string OrderShipId = @"order_ship_id";
            
            public static string PhoneNumber = @"phone_number";
            
            public static string OrignalShipTime = @"orignal_ship_time";
            
            public static string ShipCompanyId = @"ship_company_id";
            
            public static string ShipNo = @"ship_no";
            
            public static string ShipTime = @"ship_time";
            
            public static string ShipMemo = @"ship_memo";
            
            public static string MessageUpdate = @"message_update";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the AppDownloadSmsLog class.
	/// </summary>
    [Serializable]
	public partial class AppDownloadSmsLogCollection : RepositoryList<AppDownloadSmsLog, AppDownloadSmsLogCollection>
	{	   
		public AppDownloadSmsLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>AppDownloadSmsLogCollection</returns>
		public AppDownloadSmsLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                AppDownloadSmsLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the app_download_sms_log table.
	/// </summary>
	[Serializable]
	public partial class AppDownloadSmsLog : RepositoryRecord<AppDownloadSmsLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public AppDownloadSmsLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public AppDownloadSmsLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("app_download_sms_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarPhoneNo = new TableSchema.TableColumn(schema);
				colvarPhoneNo.ColumnName = "phone_no";
				colvarPhoneNo.DataType = DbType.AnsiString;
				colvarPhoneNo.MaxLength = 10;
				colvarPhoneNo.AutoIncrement = false;
				colvarPhoneNo.IsNullable = false;
				colvarPhoneNo.IsPrimaryKey = false;
				colvarPhoneNo.IsForeignKey = false;
				colvarPhoneNo.IsReadOnly = false;
				colvarPhoneNo.DefaultSetting = @"";
				colvarPhoneNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhoneNo);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarIp = new TableSchema.TableColumn(schema);
				colvarIp.ColumnName = "ip";
				colvarIp.DataType = DbType.AnsiString;
				colvarIp.MaxLength = 40;
				colvarIp.AutoIncrement = false;
				colvarIp.IsNullable = true;
				colvarIp.IsPrimaryKey = false;
				colvarIp.IsForeignKey = false;
				colvarIp.IsReadOnly = false;
				colvarIp.DefaultSetting = @"";
				colvarIp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIp);
				
				TableSchema.TableColumn colvarIsSendSuccess = new TableSchema.TableColumn(schema);
				colvarIsSendSuccess.ColumnName = "is_send_success";
				colvarIsSendSuccess.DataType = DbType.Boolean;
				colvarIsSendSuccess.MaxLength = 0;
				colvarIsSendSuccess.AutoIncrement = false;
				colvarIsSendSuccess.IsNullable = false;
				colvarIsSendSuccess.IsPrimaryKey = false;
				colvarIsSendSuccess.IsForeignKey = false;
				colvarIsSendSuccess.IsReadOnly = false;
				
						colvarIsSendSuccess.DefaultSetting = @"((0))";
				colvarIsSendSuccess.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSendSuccess);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("app_download_sms_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("PhoneNo")]
		[Bindable(true)]
		public string PhoneNo 
		{
			get { return GetColumnValue<string>(Columns.PhoneNo); }
			set { SetColumnValue(Columns.PhoneNo, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("Ip")]
		[Bindable(true)]
		public string Ip 
		{
			get { return GetColumnValue<string>(Columns.Ip); }
			set { SetColumnValue(Columns.Ip, value); }
		}
		  
		[XmlAttribute("IsSendSuccess")]
		[Bindable(true)]
		public bool IsSendSuccess 
		{
			get { return GetColumnValue<bool>(Columns.IsSendSuccess); }
			set { SetColumnValue(Columns.IsSendSuccess, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PhoneNoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IpColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IsSendSuccessColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string PhoneNo = @"phone_no";
			 public static string CreateTime = @"create_time";
			 public static string Ip = @"ip";
			 public static string IsSendSuccess = @"is_send_success";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

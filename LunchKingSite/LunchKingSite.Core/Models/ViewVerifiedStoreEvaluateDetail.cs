using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVerifiedStoreEvaluateDetail class.
    /// </summary>
    [Serializable]
    public partial class ViewVerifiedStoreEvaluateDetailCollection : ReadOnlyList<ViewVerifiedStoreEvaluateDetail, ViewVerifiedStoreEvaluateDetailCollection>
    {        
        public ViewVerifiedStoreEvaluateDetailCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_verified_store_evaluate_detail view.
    /// </summary>
    [Serializable]
    public partial class ViewVerifiedStoreEvaluateDetail : ReadOnlyRecord<ViewVerifiedStoreEvaluateDetail>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_verified_store_evaluate_detail", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarVerifiedStoreGuid = new TableSchema.TableColumn(schema);
                colvarVerifiedStoreGuid.ColumnName = "verified_store_guid";
                colvarVerifiedStoreGuid.DataType = DbType.Guid;
                colvarVerifiedStoreGuid.MaxLength = 0;
                colvarVerifiedStoreGuid.AutoIncrement = false;
                colvarVerifiedStoreGuid.IsNullable = true;
                colvarVerifiedStoreGuid.IsPrimaryKey = false;
                colvarVerifiedStoreGuid.IsForeignKey = false;
                colvarVerifiedStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifiedStoreGuid);
                
                TableSchema.TableColumn colvarEvaluateMainId = new TableSchema.TableColumn(schema);
                colvarEvaluateMainId.ColumnName = "evaluate_main_id";
                colvarEvaluateMainId.DataType = DbType.Int32;
                colvarEvaluateMainId.MaxLength = 0;
                colvarEvaluateMainId.AutoIncrement = false;
                colvarEvaluateMainId.IsNullable = false;
                colvarEvaluateMainId.IsPrimaryKey = false;
                colvarEvaluateMainId.IsForeignKey = false;
                colvarEvaluateMainId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEvaluateMainId);
                
                TableSchema.TableColumn colvarStar = new TableSchema.TableColumn(schema);
                colvarStar.ColumnName = "star";
                colvarStar.DataType = DbType.Decimal;
                colvarStar.MaxLength = 0;
                colvarStar.AutoIncrement = false;
                colvarStar.IsNullable = true;
                colvarStar.IsPrimaryKey = false;
                colvarStar.IsForeignKey = false;
                colvarStar.IsReadOnly = false;
                
                schema.Columns.Add(colvarStar);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_verified_store_evaluate_detail",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVerifiedStoreEvaluateDetail()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVerifiedStoreEvaluateDetail(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVerifiedStoreEvaluateDetail(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVerifiedStoreEvaluateDetail(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("VerifiedStoreGuid")]
        [Bindable(true)]
        public Guid? VerifiedStoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("verified_store_guid");
		    }
            set 
		    {
			    SetColumnValue("verified_store_guid", value);
            }
        }
	      
        [XmlAttribute("EvaluateMainId")]
        [Bindable(true)]
        public int EvaluateMainId 
	    {
		    get
		    {
			    return GetColumnValue<int>("evaluate_main_id");
		    }
            set 
		    {
			    SetColumnValue("evaluate_main_id", value);
            }
        }
	      
        [XmlAttribute("Star")]
        [Bindable(true)]
        public decimal? Star 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("star");
		    }
            set 
		    {
			    SetColumnValue("star", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string VerifiedStoreGuid = @"verified_store_guid";
            
            public static string EvaluateMainId = @"evaluate_main_id";
            
            public static string Star = @"star";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

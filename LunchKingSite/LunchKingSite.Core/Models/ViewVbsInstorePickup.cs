using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVbsInstorePickup class.
    /// </summary>
    [Serializable]
    public partial class ViewVbsInstorePickupCollection : ReadOnlyList<ViewVbsInstorePickup, ViewVbsInstorePickupCollection>
    {        
        public ViewVbsInstorePickupCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vbs_instore_pickup view.
    /// </summary>
    [Serializable]
    public partial class ViewVbsInstorePickup : ReadOnlyRecord<ViewVbsInstorePickup>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vbs_instore_pickup", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarSellerAddressOld = new TableSchema.TableColumn(schema);
                colvarSellerAddressOld.ColumnName = "seller_address_old";
                colvarSellerAddressOld.DataType = DbType.String;
                colvarSellerAddressOld.MaxLength = 140;
                colvarSellerAddressOld.AutoIncrement = false;
                colvarSellerAddressOld.IsNullable = true;
                colvarSellerAddressOld.IsPrimaryKey = false;
                colvarSellerAddressOld.IsForeignKey = false;
                colvarSellerAddressOld.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerAddressOld);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarContactsOld = new TableSchema.TableColumn(schema);
                colvarContactsOld.ColumnName = "contacts_old";
                colvarContactsOld.DataType = DbType.String;
                colvarContactsOld.MaxLength = 2000;
                colvarContactsOld.AutoIncrement = false;
                colvarContactsOld.IsNullable = true;
                colvarContactsOld.IsPrimaryKey = false;
                colvarContactsOld.IsForeignKey = false;
                colvarContactsOld.IsReadOnly = false;
                
                schema.Columns.Add(colvarContactsOld);
                
                TableSchema.TableColumn colvarCompanyID = new TableSchema.TableColumn(schema);
                colvarCompanyID.ColumnName = "CompanyID";
                colvarCompanyID.DataType = DbType.AnsiString;
                colvarCompanyID.MaxLength = 20;
                colvarCompanyID.AutoIncrement = false;
                colvarCompanyID.IsNullable = true;
                colvarCompanyID.IsPrimaryKey = false;
                colvarCompanyID.IsForeignKey = false;
                colvarCompanyID.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyID);
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarReturnCycle = new TableSchema.TableColumn(schema);
                colvarReturnCycle.ColumnName = "return_cycle";
                colvarReturnCycle.DataType = DbType.Int32;
                colvarReturnCycle.MaxLength = 0;
                colvarReturnCycle.AutoIncrement = false;
                colvarReturnCycle.IsNullable = true;
                colvarReturnCycle.IsPrimaryKey = false;
                colvarReturnCycle.IsForeignKey = false;
                colvarReturnCycle.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnCycle);
                
                TableSchema.TableColumn colvarReturnType = new TableSchema.TableColumn(schema);
                colvarReturnType.ColumnName = "return_type";
                colvarReturnType.DataType = DbType.Int32;
                colvarReturnType.MaxLength = 0;
                colvarReturnType.AutoIncrement = false;
                colvarReturnType.IsNullable = true;
                colvarReturnType.IsPrimaryKey = false;
                colvarReturnType.IsForeignKey = false;
                colvarReturnType.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnType);
                
                TableSchema.TableColumn colvarReturnOther = new TableSchema.TableColumn(schema);
                colvarReturnOther.ColumnName = "return_other";
                colvarReturnOther.DataType = DbType.String;
                colvarReturnOther.MaxLength = -1;
                colvarReturnOther.AutoIncrement = false;
                colvarReturnOther.IsNullable = true;
                colvarReturnOther.IsPrimaryKey = false;
                colvarReturnOther.IsForeignKey = false;
                colvarReturnOther.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnOther);
                
                TableSchema.TableColumn colvarServiceChannel = new TableSchema.TableColumn(schema);
                colvarServiceChannel.ColumnName = "service_channel";
                colvarServiceChannel.DataType = DbType.Int32;
                colvarServiceChannel.MaxLength = 0;
                colvarServiceChannel.AutoIncrement = false;
                colvarServiceChannel.IsNullable = false;
                colvarServiceChannel.IsPrimaryKey = false;
                colvarServiceChannel.IsForeignKey = false;
                colvarServiceChannel.IsReadOnly = false;
                
                schema.Columns.Add(colvarServiceChannel);
                
                TableSchema.TableColumn colvarVerifyTime = new TableSchema.TableColumn(schema);
                colvarVerifyTime.ColumnName = "verify_time";
                colvarVerifyTime.DataType = DbType.DateTime;
                colvarVerifyTime.MaxLength = 0;
                colvarVerifyTime.AutoIncrement = false;
                colvarVerifyTime.IsNullable = true;
                colvarVerifyTime.IsPrimaryKey = false;
                colvarVerifyTime.IsForeignKey = false;
                colvarVerifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifyTime);
                
                TableSchema.TableColumn colvarVerifyMemo = new TableSchema.TableColumn(schema);
                colvarVerifyMemo.ColumnName = "verify_memo";
                colvarVerifyMemo.DataType = DbType.String;
                colvarVerifyMemo.MaxLength = -1;
                colvarVerifyMemo.AutoIncrement = false;
                colvarVerifyMemo.IsNullable = true;
                colvarVerifyMemo.IsPrimaryKey = false;
                colvarVerifyMemo.IsForeignKey = false;
                colvarVerifyMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifyMemo);
                
                TableSchema.TableColumn colvarStoreCreateTime = new TableSchema.TableColumn(schema);
                colvarStoreCreateTime.ColumnName = "store_create_time";
                colvarStoreCreateTime.DataType = DbType.DateTime;
                colvarStoreCreateTime.MaxLength = 0;
                colvarStoreCreateTime.AutoIncrement = false;
                colvarStoreCreateTime.IsNullable = true;
                colvarStoreCreateTime.IsPrimaryKey = false;
                colvarStoreCreateTime.IsForeignKey = false;
                colvarStoreCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreCreateTime);
                
                TableSchema.TableColumn colvarStoreSubCode = new TableSchema.TableColumn(schema);
                colvarStoreSubCode.ColumnName = "store_sub_code";
                colvarStoreSubCode.DataType = DbType.String;
                colvarStoreSubCode.MaxLength = 20;
                colvarStoreSubCode.AutoIncrement = false;
                colvarStoreSubCode.IsNullable = true;
                colvarStoreSubCode.IsPrimaryKey = false;
                colvarStoreSubCode.IsForeignKey = false;
                colvarStoreSubCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreSubCode);
                
                TableSchema.TableColumn colvarCheckStatus = new TableSchema.TableColumn(schema);
                colvarCheckStatus.ColumnName = "check_status";
                colvarCheckStatus.DataType = DbType.Int32;
                colvarCheckStatus.MaxLength = 0;
                colvarCheckStatus.AutoIncrement = false;
                colvarCheckStatus.IsNullable = true;
                colvarCheckStatus.IsPrimaryKey = false;
                colvarCheckStatus.IsForeignKey = false;
                colvarCheckStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCheckStatus);
                
                TableSchema.TableColumn colvarCheckFinishTime = new TableSchema.TableColumn(schema);
                colvarCheckFinishTime.ColumnName = "check_finish_time";
                colvarCheckFinishTime.DataType = DbType.DateTime;
                colvarCheckFinishTime.MaxLength = 0;
                colvarCheckFinishTime.AutoIncrement = false;
                colvarCheckFinishTime.IsNullable = true;
                colvarCheckFinishTime.IsPrimaryKey = false;
                colvarCheckFinishTime.IsForeignKey = false;
                colvarCheckFinishTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCheckFinishTime);
                
                TableSchema.TableColumn colvarFinishTime = new TableSchema.TableColumn(schema);
                colvarFinishTime.ColumnName = "finish_time";
                colvarFinishTime.DataType = DbType.DateTime;
                colvarFinishTime.MaxLength = 0;
                colvarFinishTime.AutoIncrement = false;
                colvarFinishTime.IsNullable = true;
                colvarFinishTime.IsPrimaryKey = false;
                colvarFinishTime.IsForeignKey = false;
                colvarFinishTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarFinishTime);
                
                TableSchema.TableColumn colvarIsEnabled = new TableSchema.TableColumn(schema);
                colvarIsEnabled.ColumnName = "is_enabled";
                colvarIsEnabled.DataType = DbType.Boolean;
                colvarIsEnabled.MaxLength = 0;
                colvarIsEnabled.AutoIncrement = false;
                colvarIsEnabled.IsNullable = true;
                colvarIsEnabled.IsPrimaryKey = false;
                colvarIsEnabled.IsForeignKey = false;
                colvarIsEnabled.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsEnabled);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 50;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarContacts = new TableSchema.TableColumn(schema);
                colvarContacts.ColumnName = "contacts";
                colvarContacts.DataType = DbType.String;
                colvarContacts.MaxLength = 500;
                colvarContacts.AutoIncrement = false;
                colvarContacts.IsNullable = true;
                colvarContacts.IsPrimaryKey = false;
                colvarContacts.IsForeignKey = false;
                colvarContacts.IsReadOnly = false;
                
                schema.Columns.Add(colvarContacts);
                
                TableSchema.TableColumn colvarSellerAddress = new TableSchema.TableColumn(schema);
                colvarSellerAddress.ColumnName = "seller_address";
                colvarSellerAddress.DataType = DbType.String;
                colvarSellerAddress.MaxLength = 500;
                colvarSellerAddress.AutoIncrement = false;
                colvarSellerAddress.IsNullable = true;
                colvarSellerAddress.IsPrimaryKey = false;
                colvarSellerAddress.IsForeignKey = false;
                colvarSellerAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerAddress);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vbs_instore_pickup",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVbsInstorePickup()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVbsInstorePickup(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVbsInstorePickup(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVbsInstorePickup(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("SellerAddressOld")]
        [Bindable(true)]
        public string SellerAddressOld 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_address_old");
		    }
            set 
		    {
			    SetColumnValue("seller_address_old", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("ContactsOld")]
        [Bindable(true)]
        public string ContactsOld 
	    {
		    get
		    {
			    return GetColumnValue<string>("contacts_old");
		    }
            set 
		    {
			    SetColumnValue("contacts_old", value);
            }
        }
	      
        [XmlAttribute("CompanyID")]
        [Bindable(true)]
        public string CompanyID 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyID");
		    }
            set 
		    {
			    SetColumnValue("CompanyID", value);
            }
        }
	      
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ReturnCycle")]
        [Bindable(true)]
        public int? ReturnCycle 
	    {
		    get
		    {
			    return GetColumnValue<int?>("return_cycle");
		    }
            set 
		    {
			    SetColumnValue("return_cycle", value);
            }
        }
	      
        [XmlAttribute("ReturnType")]
        [Bindable(true)]
        public int? ReturnType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("return_type");
		    }
            set 
		    {
			    SetColumnValue("return_type", value);
            }
        }
	      
        [XmlAttribute("ReturnOther")]
        [Bindable(true)]
        public string ReturnOther 
	    {
		    get
		    {
			    return GetColumnValue<string>("return_other");
		    }
            set 
		    {
			    SetColumnValue("return_other", value);
            }
        }
	      
        [XmlAttribute("ServiceChannel")]
        [Bindable(true)]
        public int ServiceChannel 
	    {
		    get
		    {
			    return GetColumnValue<int>("service_channel");
		    }
            set 
		    {
			    SetColumnValue("service_channel", value);
            }
        }
	      
        [XmlAttribute("VerifyTime")]
        [Bindable(true)]
        public DateTime? VerifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("verify_time");
		    }
            set 
		    {
			    SetColumnValue("verify_time", value);
            }
        }
	      
        [XmlAttribute("VerifyMemo")]
        [Bindable(true)]
        public string VerifyMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("verify_memo");
		    }
            set 
		    {
			    SetColumnValue("verify_memo", value);
            }
        }
	      
        [XmlAttribute("StoreCreateTime")]
        [Bindable(true)]
        public DateTime? StoreCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("store_create_time");
		    }
            set 
		    {
			    SetColumnValue("store_create_time", value);
            }
        }
	      
        [XmlAttribute("StoreSubCode")]
        [Bindable(true)]
        public string StoreSubCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_sub_code");
		    }
            set 
		    {
			    SetColumnValue("store_sub_code", value);
            }
        }
	      
        [XmlAttribute("CheckStatus")]
        [Bindable(true)]
        public int? CheckStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("check_status");
		    }
            set 
		    {
			    SetColumnValue("check_status", value);
            }
        }
	      
        [XmlAttribute("CheckFinishTime")]
        [Bindable(true)]
        public DateTime? CheckFinishTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("check_finish_time");
		    }
            set 
		    {
			    SetColumnValue("check_finish_time", value);
            }
        }
	      
        [XmlAttribute("FinishTime")]
        [Bindable(true)]
        public DateTime? FinishTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("finish_time");
		    }
            set 
		    {
			    SetColumnValue("finish_time", value);
            }
        }
	      
        [XmlAttribute("IsEnabled")]
        [Bindable(true)]
        public bool? IsEnabled 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_enabled");
		    }
            set 
		    {
			    SetColumnValue("is_enabled", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("Contacts")]
        [Bindable(true)]
        public string Contacts 
	    {
		    get
		    {
			    return GetColumnValue<string>("contacts");
		    }
            set 
		    {
			    SetColumnValue("contacts", value);
            }
        }
	      
        [XmlAttribute("SellerAddress")]
        [Bindable(true)]
        public string SellerAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_address");
		    }
            set 
		    {
			    SetColumnValue("seller_address", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string SellerName = @"seller_name";
            
            public static string SellerId = @"seller_id";
            
            public static string SellerAddressOld = @"seller_address_old";
            
            public static string CityId = @"city_id";
            
            public static string ContactsOld = @"contacts_old";
            
            public static string CompanyID = @"CompanyID";
            
            public static string Id = @"id";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string Status = @"status";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ReturnCycle = @"return_cycle";
            
            public static string ReturnType = @"return_type";
            
            public static string ReturnOther = @"return_other";
            
            public static string ServiceChannel = @"service_channel";
            
            public static string VerifyTime = @"verify_time";
            
            public static string VerifyMemo = @"verify_memo";
            
            public static string StoreCreateTime = @"store_create_time";
            
            public static string StoreSubCode = @"store_sub_code";
            
            public static string CheckStatus = @"check_status";
            
            public static string CheckFinishTime = @"check_finish_time";
            
            public static string FinishTime = @"finish_time";
            
            public static string IsEnabled = @"is_enabled";
            
            public static string ModifyId = @"modify_id";
            
            public static string ModifyTime = @"modify_time";
            
            public static string Contacts = @"contacts";
            
            public static string SellerAddress = @"seller_address";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

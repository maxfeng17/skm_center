using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealReopen class.
	/// </summary>
    [Serializable]
	public partial class DealReopenCollection : RepositoryList<DealReopen, DealReopenCollection>
	{	   
		public DealReopenCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealReopenCollection</returns>
		public DealReopenCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealReopen o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_reopen table.
	/// </summary>
	[Serializable]
	public partial class DealReopen : RepositoryRecord<DealReopen>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealReopen()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DealReopen(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("deal_reopen", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
				colvarItemId.ColumnName = "item_id";
				colvarItemId.DataType = DbType.Int32;
				colvarItemId.MaxLength = 0;
				colvarItemId.AutoIncrement = false;
				colvarItemId.IsNullable = false;
				colvarItemId.IsPrimaryKey = false;
				colvarItemId.IsForeignKey = false;
				colvarItemId.IsReadOnly = false;
				colvarItemId.DefaultSetting = @"";
				colvarItemId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemId);
				
				TableSchema.TableColumn colvarMainBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarMainBusinessHourGuid.ColumnName = "main_business_hour_guid";
				colvarMainBusinessHourGuid.DataType = DbType.Guid;
				colvarMainBusinessHourGuid.MaxLength = 0;
				colvarMainBusinessHourGuid.AutoIncrement = false;
				colvarMainBusinessHourGuid.IsNullable = true;
				colvarMainBusinessHourGuid.IsPrimaryKey = false;
				colvarMainBusinessHourGuid.IsForeignKey = false;
				colvarMainBusinessHourGuid.IsReadOnly = false;
				colvarMainBusinessHourGuid.DefaultSetting = @"";
				colvarMainBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainBusinessHourGuid);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
				colvarCouponUsage.ColumnName = "coupon_usage";
				colvarCouponUsage.DataType = DbType.String;
				colvarCouponUsage.MaxLength = 500;
				colvarCouponUsage.AutoIncrement = false;
				colvarCouponUsage.IsNullable = false;
				colvarCouponUsage.IsPrimaryKey = false;
				colvarCouponUsage.IsForeignKey = false;
				colvarCouponUsage.IsReadOnly = false;
				colvarCouponUsage.DefaultSetting = @"";
				colvarCouponUsage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponUsage);
				
				TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
				colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeS.MaxLength = 0;
				colvarBusinessHourOrderTimeS.AutoIncrement = false;
				colvarBusinessHourOrderTimeS.IsNullable = false;
				colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeS.IsForeignKey = false;
				colvarBusinessHourOrderTimeS.IsReadOnly = false;
				colvarBusinessHourOrderTimeS.DefaultSetting = @"";
				colvarBusinessHourOrderTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeS);
				
				TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
				colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeE.MaxLength = 0;
				colvarBusinessHourOrderTimeE.AutoIncrement = false;
				colvarBusinessHourOrderTimeE.IsNullable = false;
				colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeE.IsForeignKey = false;
				colvarBusinessHourOrderTimeE.IsReadOnly = false;
				colvarBusinessHourOrderTimeE.DefaultSetting = @"";
				colvarBusinessHourOrderTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeE);
				
				TableSchema.TableColumn colvarConfirmLimitDate = new TableSchema.TableColumn(schema);
				colvarConfirmLimitDate.ColumnName = "confirm_limit_date";
				colvarConfirmLimitDate.DataType = DbType.DateTime;
				colvarConfirmLimitDate.MaxLength = 0;
				colvarConfirmLimitDate.AutoIncrement = false;
				colvarConfirmLimitDate.IsNullable = false;
				colvarConfirmLimitDate.IsPrimaryKey = false;
				colvarConfirmLimitDate.IsForeignKey = false;
				colvarConfirmLimitDate.IsReadOnly = false;
				colvarConfirmLimitDate.DefaultSetting = @"";
				colvarConfirmLimitDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConfirmLimitDate);
				
				TableSchema.TableColumn colvarConfirmStatus = new TableSchema.TableColumn(schema);
				colvarConfirmStatus.ColumnName = "confirm_status";
				colvarConfirmStatus.DataType = DbType.Byte;
				colvarConfirmStatus.MaxLength = 0;
				colvarConfirmStatus.AutoIncrement = false;
				colvarConfirmStatus.IsNullable = false;
				colvarConfirmStatus.IsPrimaryKey = false;
				colvarConfirmStatus.IsForeignKey = false;
				colvarConfirmStatus.IsReadOnly = false;
				
						colvarConfirmStatus.DefaultSetting = @"((0))";
				colvarConfirmStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConfirmStatus);
				
				TableSchema.TableColumn colvarOptionSetupText = new TableSchema.TableColumn(schema);
				colvarOptionSetupText.ColumnName = "option_setup_text";
				colvarOptionSetupText.DataType = DbType.String;
				colvarOptionSetupText.MaxLength = -1;
				colvarOptionSetupText.AutoIncrement = false;
				colvarOptionSetupText.IsNullable = false;
				colvarOptionSetupText.IsPrimaryKey = false;
				colvarOptionSetupText.IsForeignKey = false;
				colvarOptionSetupText.IsReadOnly = false;
				
						colvarOptionSetupText.DefaultSetting = @"('')";
				colvarOptionSetupText.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOptionSetupText);
				
				TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
				colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeS.MaxLength = 0;
				colvarBusinessHourDeliverTimeS.AutoIncrement = false;
				colvarBusinessHourDeliverTimeS.IsNullable = true;
				colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeS.IsForeignKey = false;
				colvarBusinessHourDeliverTimeS.IsReadOnly = false;
				
						colvarBusinessHourDeliverTimeS.DefaultSetting = @"(getdate())";
				colvarBusinessHourDeliverTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeS);
				
				TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
				colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeE.MaxLength = 0;
				colvarBusinessHourDeliverTimeE.AutoIncrement = false;
				colvarBusinessHourDeliverTimeE.IsNullable = true;
				colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeE.IsForeignKey = false;
				colvarBusinessHourDeliverTimeE.IsReadOnly = false;
				
						colvarBusinessHourDeliverTimeE.DefaultSetting = @"(getdate())";
				colvarBusinessHourDeliverTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeE);
				
				TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
				colvarOrderTotalLimit.ColumnName = "order_total_limit";
				colvarOrderTotalLimit.DataType = DbType.Currency;
				colvarOrderTotalLimit.MaxLength = 0;
				colvarOrderTotalLimit.AutoIncrement = false;
				colvarOrderTotalLimit.IsNullable = true;
				colvarOrderTotalLimit.IsPrimaryKey = false;
				colvarOrderTotalLimit.IsForeignKey = false;
				colvarOrderTotalLimit.IsReadOnly = false;
				
						colvarOrderTotalLimit.DefaultSetting = @"((0))";
				colvarOrderTotalLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderTotalLimit);
				
				TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
				colvarSellerId.ColumnName = "seller_id";
				colvarSellerId.DataType = DbType.AnsiString;
				colvarSellerId.MaxLength = 20;
				colvarSellerId.AutoIncrement = false;
				colvarSellerId.IsNullable = false;
				colvarSellerId.IsPrimaryKey = false;
				colvarSellerId.IsForeignKey = false;
				colvarSellerId.IsReadOnly = false;
				colvarSellerId.DefaultSetting = @"";
				colvarSellerId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerId);
				
				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Byte;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = false;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				
						colvarDeliveryType.DefaultSetting = @"((0))";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);
				
				TableSchema.TableColumn colvarRejectReason = new TableSchema.TableColumn(schema);
				colvarRejectReason.ColumnName = "reject_reason";
				colvarRejectReason.DataType = DbType.String;
				colvarRejectReason.MaxLength = -1;
				colvarRejectReason.AutoIncrement = false;
				colvarRejectReason.IsNullable = false;
				colvarRejectReason.IsPrimaryKey = false;
				colvarRejectReason.IsForeignKey = false;
				colvarRejectReason.IsReadOnly = false;
				
						colvarRejectReason.DefaultSetting = @"('')";
				colvarRejectReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRejectReason);
				
				TableSchema.TableColumn colvarIsComboDeal = new TableSchema.TableColumn(schema);
				colvarIsComboDeal.ColumnName = "is_combo_deal";
				colvarIsComboDeal.DataType = DbType.Boolean;
				colvarIsComboDeal.MaxLength = 0;
				colvarIsComboDeal.AutoIncrement = false;
				colvarIsComboDeal.IsNullable = false;
				colvarIsComboDeal.IsPrimaryKey = false;
				colvarIsComboDeal.IsForeignKey = false;
				colvarIsComboDeal.IsReadOnly = false;
				
						colvarIsComboDeal.DefaultSetting = @"((0))";
				colvarIsComboDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsComboDeal);
				
				TableSchema.TableColumn colvarIsSend = new TableSchema.TableColumn(schema);
				colvarIsSend.ColumnName = "is_send";
				colvarIsSend.DataType = DbType.Boolean;
				colvarIsSend.MaxLength = 0;
				colvarIsSend.AutoIncrement = false;
				colvarIsSend.IsNullable = false;
				colvarIsSend.IsPrimaryKey = false;
				colvarIsSend.IsForeignKey = false;
				colvarIsSend.IsReadOnly = false;
				
						colvarIsSend.DefaultSetting = @"((0))";
				colvarIsSend.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSend);
				
				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 100;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = true;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				
						colvarSellerName.DefaultSetting = @"('')";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);
				
				TableSchema.TableColumn colvarDealEmpName = new TableSchema.TableColumn(schema);
				colvarDealEmpName.ColumnName = "deal_emp_name";
				colvarDealEmpName.DataType = DbType.String;
				colvarDealEmpName.MaxLength = 50;
				colvarDealEmpName.AutoIncrement = false;
				colvarDealEmpName.IsNullable = true;
				colvarDealEmpName.IsPrimaryKey = false;
				colvarDealEmpName.IsForeignKey = false;
				colvarDealEmpName.IsReadOnly = false;
				
						colvarDealEmpName.DefaultSetting = @"('')";
				colvarDealEmpName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealEmpName);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("deal_reopen",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ItemId")]
		[Bindable(true)]
		public int ItemId 
		{
			get { return GetColumnValue<int>(Columns.ItemId); }
			set { SetColumnValue(Columns.ItemId, value); }
		}
		  
		[XmlAttribute("MainBusinessHourGuid")]
		[Bindable(true)]
		public Guid? MainBusinessHourGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.MainBusinessHourGuid); }
			set { SetColumnValue(Columns.MainBusinessHourGuid, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("CouponUsage")]
		[Bindable(true)]
		public string CouponUsage 
		{
			get { return GetColumnValue<string>(Columns.CouponUsage); }
			set { SetColumnValue(Columns.CouponUsage, value); }
		}
		  
		[XmlAttribute("BusinessHourOrderTimeS")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeS 
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeS); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeS, value); }
		}
		  
		[XmlAttribute("BusinessHourOrderTimeE")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeE 
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeE); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeE, value); }
		}
		  
		[XmlAttribute("ConfirmLimitDate")]
		[Bindable(true)]
		public DateTime ConfirmLimitDate 
		{
			get { return GetColumnValue<DateTime>(Columns.ConfirmLimitDate); }
			set { SetColumnValue(Columns.ConfirmLimitDate, value); }
		}
		  
		[XmlAttribute("ConfirmStatus")]
		[Bindable(true)]
		public byte ConfirmStatus 
		{
			get { return GetColumnValue<byte>(Columns.ConfirmStatus); }
			set { SetColumnValue(Columns.ConfirmStatus, value); }
		}
		  
		[XmlAttribute("OptionSetupText")]
		[Bindable(true)]
		public string OptionSetupText 
		{
			get { return GetColumnValue<string>(Columns.OptionSetupText); }
			set { SetColumnValue(Columns.OptionSetupText, value); }
		}
		  
		[XmlAttribute("BusinessHourDeliverTimeS")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeS 
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeS); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeS, value); }
		}
		  
		[XmlAttribute("BusinessHourDeliverTimeE")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeE 
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeE); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeE, value); }
		}
		  
		[XmlAttribute("OrderTotalLimit")]
		[Bindable(true)]
		public decimal? OrderTotalLimit 
		{
			get { return GetColumnValue<decimal?>(Columns.OrderTotalLimit); }
			set { SetColumnValue(Columns.OrderTotalLimit, value); }
		}
		  
		[XmlAttribute("SellerId")]
		[Bindable(true)]
		public string SellerId 
		{
			get { return GetColumnValue<string>(Columns.SellerId); }
			set { SetColumnValue(Columns.SellerId, value); }
		}
		  
		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public byte DeliveryType 
		{
			get { return GetColumnValue<byte>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}
		  
		[XmlAttribute("RejectReason")]
		[Bindable(true)]
		public string RejectReason 
		{
			get { return GetColumnValue<string>(Columns.RejectReason); }
			set { SetColumnValue(Columns.RejectReason, value); }
		}
		  
		[XmlAttribute("IsComboDeal")]
		[Bindable(true)]
		public bool IsComboDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsComboDeal); }
			set { SetColumnValue(Columns.IsComboDeal, value); }
		}
		  
		[XmlAttribute("IsSend")]
		[Bindable(true)]
		public bool IsSend 
		{
			get { return GetColumnValue<bool>(Columns.IsSend); }
			set { SetColumnValue(Columns.IsSend, value); }
		}
		  
		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName 
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}
		  
		[XmlAttribute("DealEmpName")]
		[Bindable(true)]
		public string DealEmpName 
		{
			get { return GetColumnValue<string>(Columns.DealEmpName); }
			set { SetColumnValue(Columns.DealEmpName, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn MainBusinessHourGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponUsageColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourOrderTimeSColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourOrderTimeEColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ConfirmLimitDateColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ConfirmStatusColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn OptionSetupTextColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourDeliverTimeSColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourDeliverTimeEColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderTotalLimitColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryTypeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn RejectReasonColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn IsComboDealColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn IsSendColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerNameColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn DealEmpNameColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ItemId = @"item_id";
			 public static string MainBusinessHourGuid = @"main_business_hour_guid";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string CouponUsage = @"coupon_usage";
			 public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
			 public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
			 public static string ConfirmLimitDate = @"confirm_limit_date";
			 public static string ConfirmStatus = @"confirm_status";
			 public static string OptionSetupText = @"option_setup_text";
			 public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
			 public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
			 public static string OrderTotalLimit = @"order_total_limit";
			 public static string SellerId = @"seller_id";
			 public static string DeliveryType = @"delivery_type";
			 public static string RejectReason = @"reject_reason";
			 public static string IsComboDeal = @"is_combo_deal";
			 public static string IsSend = @"is_send";
			 public static string SellerName = @"seller_name";
			 public static string DealEmpName = @"deal_emp_name";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmEventPrizeWinner class.
	/// </summary>
    [Serializable]
	public partial class SkmEventPrizeWinnerCollection : RepositoryList<SkmEventPrizeWinner, SkmEventPrizeWinnerCollection>
	{	   
		public SkmEventPrizeWinnerCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmEventPrizeWinnerCollection</returns>
		public SkmEventPrizeWinnerCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmEventPrizeWinner o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_event_prize_winner table.
	/// </summary>
	[Serializable]
	public partial class SkmEventPrizeWinner : RepositoryRecord<SkmEventPrizeWinner>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmEventPrizeWinner()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmEventPrizeWinner(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_event_prize_winner", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarPrizeName = new TableSchema.TableColumn(schema);
				colvarPrizeName.ColumnName = "prize_name";
				colvarPrizeName.DataType = DbType.String;
				colvarPrizeName.MaxLength = 256;
				colvarPrizeName.AutoIncrement = false;
				colvarPrizeName.IsNullable = false;
				colvarPrizeName.IsPrimaryKey = false;
				colvarPrizeName.IsForeignKey = false;
				colvarPrizeName.IsReadOnly = false;
				colvarPrizeName.DefaultSetting = @"";
				colvarPrizeName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrizeName);
				
				TableSchema.TableColumn colvarPrizeIdNumber = new TableSchema.TableColumn(schema);
				colvarPrizeIdNumber.ColumnName = "prize_id_number";
				colvarPrizeIdNumber.DataType = DbType.String;
				colvarPrizeIdNumber.MaxLength = 256;
				colvarPrizeIdNumber.AutoIncrement = false;
				colvarPrizeIdNumber.IsNullable = false;
				colvarPrizeIdNumber.IsPrimaryKey = false;
				colvarPrizeIdNumber.IsForeignKey = false;
				colvarPrizeIdNumber.IsReadOnly = false;
				colvarPrizeIdNumber.DefaultSetting = @"";
				colvarPrizeIdNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrizeIdNumber);
				
				TableSchema.TableColumn colvarIsUsed = new TableSchema.TableColumn(schema);
				colvarIsUsed.ColumnName = "is_used";
				colvarIsUsed.DataType = DbType.Boolean;
				colvarIsUsed.MaxLength = 0;
				colvarIsUsed.AutoIncrement = false;
				colvarIsUsed.IsNullable = false;
				colvarIsUsed.IsPrimaryKey = false;
				colvarIsUsed.IsForeignKey = false;
				colvarIsUsed.IsReadOnly = false;
				
						colvarIsUsed.DefaultSetting = @"((0))";
				colvarIsUsed.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsUsed);
				
				TableSchema.TableColumn colvarVerificationName = new TableSchema.TableColumn(schema);
				colvarVerificationName.ColumnName = "Verification_name";
				colvarVerificationName.DataType = DbType.String;
				colvarVerificationName.MaxLength = 256;
				colvarVerificationName.AutoIncrement = false;
				colvarVerificationName.IsNullable = true;
				colvarVerificationName.IsPrimaryKey = false;
				colvarVerificationName.IsForeignKey = false;
				colvarVerificationName.IsReadOnly = false;
				colvarVerificationName.DefaultSetting = @"";
				colvarVerificationName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerificationName);
				
				TableSchema.TableColumn colvarVerificationIdNumber = new TableSchema.TableColumn(schema);
				colvarVerificationIdNumber.ColumnName = "verification_id_number";
				colvarVerificationIdNumber.DataType = DbType.String;
				colvarVerificationIdNumber.MaxLength = 256;
				colvarVerificationIdNumber.AutoIncrement = false;
				colvarVerificationIdNumber.IsNullable = true;
				colvarVerificationIdNumber.IsPrimaryKey = false;
				colvarVerificationIdNumber.IsForeignKey = false;
				colvarVerificationIdNumber.IsReadOnly = false;
				colvarVerificationIdNumber.DefaultSetting = @"";
				colvarVerificationIdNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerificationIdNumber);
				
				TableSchema.TableColumn colvarVerificationEmail = new TableSchema.TableColumn(schema);
				colvarVerificationEmail.ColumnName = "verification_email";
				colvarVerificationEmail.DataType = DbType.String;
				colvarVerificationEmail.MaxLength = 256;
				colvarVerificationEmail.AutoIncrement = false;
				colvarVerificationEmail.IsNullable = true;
				colvarVerificationEmail.IsPrimaryKey = false;
				colvarVerificationEmail.IsForeignKey = false;
				colvarVerificationEmail.IsReadOnly = false;
				colvarVerificationEmail.DefaultSetting = @"";
				colvarVerificationEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerificationEmail);
				
				TableSchema.TableColumn colvarVerificationTime = new TableSchema.TableColumn(schema);
				colvarVerificationTime.ColumnName = "verification_time";
				colvarVerificationTime.DataType = DbType.DateTime;
				colvarVerificationTime.MaxLength = 0;
				colvarVerificationTime.AutoIncrement = false;
				colvarVerificationTime.IsNullable = true;
				colvarVerificationTime.IsPrimaryKey = false;
				colvarVerificationTime.IsForeignKey = false;
				colvarVerificationTime.IsReadOnly = false;
				colvarVerificationTime.DefaultSetting = @"";
				colvarVerificationTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerificationTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_event_prize_winner",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("PrizeName")]
		[Bindable(true)]
		public string PrizeName 
		{
			get { return GetColumnValue<string>(Columns.PrizeName); }
			set { SetColumnValue(Columns.PrizeName, value); }
		}
		  
		[XmlAttribute("PrizeIdNumber")]
		[Bindable(true)]
		public string PrizeIdNumber 
		{
			get { return GetColumnValue<string>(Columns.PrizeIdNumber); }
			set { SetColumnValue(Columns.PrizeIdNumber, value); }
		}
		  
		[XmlAttribute("IsUsed")]
		[Bindable(true)]
		public bool IsUsed 
		{
			get { return GetColumnValue<bool>(Columns.IsUsed); }
			set { SetColumnValue(Columns.IsUsed, value); }
		}
		  
		[XmlAttribute("VerificationName")]
		[Bindable(true)]
		public string VerificationName 
		{
			get { return GetColumnValue<string>(Columns.VerificationName); }
			set { SetColumnValue(Columns.VerificationName, value); }
		}
		  
		[XmlAttribute("VerificationIdNumber")]
		[Bindable(true)]
		public string VerificationIdNumber 
		{
			get { return GetColumnValue<string>(Columns.VerificationIdNumber); }
			set { SetColumnValue(Columns.VerificationIdNumber, value); }
		}
		  
		[XmlAttribute("VerificationEmail")]
		[Bindable(true)]
		public string VerificationEmail 
		{
			get { return GetColumnValue<string>(Columns.VerificationEmail); }
			set { SetColumnValue(Columns.VerificationEmail, value); }
		}
		  
		[XmlAttribute("VerificationTime")]
		[Bindable(true)]
		public DateTime? VerificationTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.VerificationTime); }
			set { SetColumnValue(Columns.VerificationTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PrizeNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PrizeIdNumberColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IsUsedColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn VerificationNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn VerificationIdNumberColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn VerificationEmailColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn VerificationTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string PrizeName = @"prize_name";
			 public static string PrizeIdNumber = @"prize_id_number";
			 public static string IsUsed = @"is_used";
			 public static string VerificationName = @"Verification_name";
			 public static string VerificationIdNumber = @"verification_id_number";
			 public static string VerificationEmail = @"verification_email";
			 public static string VerificationTime = @"verification_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

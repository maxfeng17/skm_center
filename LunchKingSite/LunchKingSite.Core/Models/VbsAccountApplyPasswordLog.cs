﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the VbsAccountApplyPasswordLog class.
    /// </summary>
    [Serializable]
    public partial class VbsAccountApplyPasswordLogCollection : RepositoryList<VbsAccountApplyPasswordLog, VbsAccountApplyPasswordLogCollection>
    {
        public VbsAccountApplyPasswordLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VbsAccountApplyPasswordLogCollection</returns>
        public VbsAccountApplyPasswordLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VbsAccountApplyPasswordLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the vbs_account_apply_password_log table.
    /// </summary>
    [Serializable]
    public partial class VbsAccountApplyPasswordLog : RepositoryRecord<VbsAccountApplyPasswordLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public VbsAccountApplyPasswordLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public VbsAccountApplyPasswordLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("vbs_account_apply_password_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = false;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                colvarAccountId.DefaultSetting = @"";
                colvarAccountId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAccountId);

                TableSchema.TableColumn colvarApplyPassword = new TableSchema.TableColumn(schema);
                colvarApplyPassword.ColumnName = "apply_password";
                colvarApplyPassword.DataType = DbType.AnsiString;
                colvarApplyPassword.MaxLength = 200;
                colvarApplyPassword.AutoIncrement = false;
                colvarApplyPassword.IsNullable = false;
                colvarApplyPassword.IsPrimaryKey = false;
                colvarApplyPassword.IsForeignKey = false;
                colvarApplyPassword.IsReadOnly = false;
                colvarApplyPassword.DefaultSetting = @"";
                colvarApplyPassword.ForeignKeyTableName = "";
                schema.Columns.Add(colvarApplyPassword);

                TableSchema.TableColumn colvarApplyTime = new TableSchema.TableColumn(schema);
                colvarApplyTime.ColumnName = "apply_time";
                colvarApplyTime.DataType = DbType.DateTime;
                colvarApplyTime.MaxLength = 0;
                colvarApplyTime.AutoIncrement = false;
                colvarApplyTime.IsNullable = false;
                colvarApplyTime.IsPrimaryKey = false;
                colvarApplyTime.IsForeignKey = false;
                colvarApplyTime.IsReadOnly = false;
                colvarApplyTime.DefaultSetting = @"";
                colvarApplyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarApplyTime);

                TableSchema.TableColumn colvarSourceIp = new TableSchema.TableColumn(schema);
                colvarSourceIp.ColumnName = "source_ip";
                colvarSourceIp.DataType = DbType.AnsiString;
                colvarSourceIp.MaxLength = 39;
                colvarSourceIp.AutoIncrement = false;
                colvarSourceIp.IsNullable = false;
                colvarSourceIp.IsPrimaryKey = false;
                colvarSourceIp.IsForeignKey = false;
                colvarSourceIp.IsReadOnly = false;
                colvarSourceIp.DefaultSetting = @"";
                colvarSourceIp.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSourceIp);

                TableSchema.TableColumn colvarHost = new TableSchema.TableColumn(schema);
                colvarHost.ColumnName = "host";
                colvarHost.DataType = DbType.String;
                colvarHost.MaxLength = 50;
                colvarHost.AutoIncrement = false;
                colvarHost.IsNullable = true;
                colvarHost.IsPrimaryKey = false;
                colvarHost.IsForeignKey = false;
                colvarHost.IsReadOnly = false;
                colvarHost.DefaultSetting = @"";
                colvarHost.ForeignKeyTableName = "";
                schema.Columns.Add(colvarHost);

                TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
                colvarMemo.ColumnName = "memo";
                colvarMemo.DataType = DbType.AnsiString;
                colvarMemo.MaxLength = 1000;
                colvarMemo.AutoIncrement = false;
                colvarMemo.IsNullable = true;
                colvarMemo.IsPrimaryKey = false;
                colvarMemo.IsForeignKey = false;
                colvarMemo.IsReadOnly = false;
                colvarMemo.DefaultSetting = @"";
                colvarMemo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMemo);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("vbs_account_apply_password_log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId
        {
            get { return GetColumnValue<string>(Columns.AccountId); }
            set { SetColumnValue(Columns.AccountId, value); }
        }

        [XmlAttribute("ApplyPassword")]
        [Bindable(true)]
        public string ApplyPassword
        {
            get { return GetColumnValue<string>(Columns.ApplyPassword); }
            set { SetColumnValue(Columns.ApplyPassword, value); }
        }

        [XmlAttribute("ApplyTime")]
        [Bindable(true)]
        public DateTime ApplyTime
        {
            get { return GetColumnValue<DateTime>(Columns.ApplyTime); }
            set { SetColumnValue(Columns.ApplyTime, value); }
        }

        [XmlAttribute("SourceIp")]
        [Bindable(true)]
        public string SourceIp
        {
            get { return GetColumnValue<string>(Columns.SourceIp); }
            set { SetColumnValue(Columns.SourceIp, value); }
        }

        [XmlAttribute("Host")]
        [Bindable(true)]
        public string Host
        {
            get { return GetColumnValue<string>(Columns.Host); }
            set { SetColumnValue(Columns.Host, value); }
        }

        [XmlAttribute("Memo")]
        [Bindable(true)]
        public string Memo
        {
            get { return GetColumnValue<string>(Columns.Memo); }
            set { SetColumnValue(Columns.Memo, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn AccountIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ApplyPasswordColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn ApplyTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn SourceIpColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn HostColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[6]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"guid";
            public static string AccountId = @"account_id";
            public static string ApplyPassword = @"apply_password";
            public static string ApplyTime = @"apply_time";
            public static string SourceIp = @"source_ip";
            public static string Host = @"host";
            public static string Memo = @"memo";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ThirdPartyPayRefundLog class.
	/// </summary>
    [Serializable]
	public partial class ThirdPartyPayRefundLogCollection : RepositoryList<ThirdPartyPayRefundLog, ThirdPartyPayRefundLogCollection>
	{	   
		public ThirdPartyPayRefundLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ThirdPartyPayRefundLogCollection</returns>
		public ThirdPartyPayRefundLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ThirdPartyPayRefundLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the third_party_pay_refund_log table.
	/// </summary>
	[Serializable]
	public partial class ThirdPartyPayRefundLog : RepositoryRecord<ThirdPartyPayRefundLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ThirdPartyPayRefundLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ThirdPartyPayRefundLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("third_party_pay_refund_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = false;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTransLogId = new TableSchema.TableColumn(schema);
				colvarTransLogId.ColumnName = "trans_log_id";
				colvarTransLogId.DataType = DbType.Int32;
				colvarTransLogId.MaxLength = 0;
				colvarTransLogId.AutoIncrement = false;
				colvarTransLogId.IsNullable = false;
				colvarTransLogId.IsPrimaryKey = false;
				colvarTransLogId.IsForeignKey = false;
				colvarTransLogId.IsReadOnly = false;
				colvarTransLogId.DefaultSetting = @"";
				colvarTransLogId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransLogId);
				
				TableSchema.TableColumn colvarPaymentTransactionId = new TableSchema.TableColumn(schema);
				colvarPaymentTransactionId.ColumnName = "payment_transaction_id";
				colvarPaymentTransactionId.DataType = DbType.Int32;
				colvarPaymentTransactionId.MaxLength = 0;
				colvarPaymentTransactionId.AutoIncrement = false;
				colvarPaymentTransactionId.IsNullable = false;
				colvarPaymentTransactionId.IsPrimaryKey = false;
				colvarPaymentTransactionId.IsForeignKey = false;
				colvarPaymentTransactionId.IsReadOnly = false;
				colvarPaymentTransactionId.DefaultSetting = @"";
				colvarPaymentTransactionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentTransactionId);
				
				TableSchema.TableColumn colvarRefundStatus = new TableSchema.TableColumn(schema);
				colvarRefundStatus.ColumnName = "refund_status";
				colvarRefundStatus.DataType = DbType.Int32;
				colvarRefundStatus.MaxLength = 0;
				colvarRefundStatus.AutoIncrement = false;
				colvarRefundStatus.IsNullable = false;
				colvarRefundStatus.IsPrimaryKey = false;
				colvarRefundStatus.IsForeignKey = false;
				colvarRefundStatus.IsReadOnly = false;
				colvarRefundStatus.DefaultSetting = @"";
				colvarRefundStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundStatus);
				
				TableSchema.TableColumn colvarRefundMsg = new TableSchema.TableColumn(schema);
				colvarRefundMsg.ColumnName = "refund_msg";
				colvarRefundMsg.DataType = DbType.String;
				colvarRefundMsg.MaxLength = 100;
				colvarRefundMsg.AutoIncrement = false;
				colvarRefundMsg.IsNullable = false;
				colvarRefundMsg.IsPrimaryKey = false;
				colvarRefundMsg.IsForeignKey = false;
				colvarRefundMsg.IsReadOnly = false;
				colvarRefundMsg.DefaultSetting = @"";
				colvarRefundMsg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundMsg);
				
				TableSchema.TableColumn colvarRetryTime = new TableSchema.TableColumn(schema);
				colvarRetryTime.ColumnName = "retry_time";
				colvarRetryTime.DataType = DbType.Int32;
				colvarRetryTime.MaxLength = 0;
				colvarRetryTime.AutoIncrement = false;
				colvarRetryTime.IsNullable = false;
				colvarRetryTime.IsPrimaryKey = false;
				colvarRetryTime.IsForeignKey = false;
				colvarRetryTime.IsReadOnly = false;
				colvarRetryTime.DefaultSetting = @"";
				colvarRetryTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRetryTime);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);
				
				TableSchema.TableColumn colvarCanRetry = new TableSchema.TableColumn(schema);
				colvarCanRetry.ColumnName = "can_retry";
				colvarCanRetry.DataType = DbType.Int32;
				colvarCanRetry.MaxLength = 0;
				colvarCanRetry.AutoIncrement = false;
				colvarCanRetry.IsNullable = false;
				colvarCanRetry.IsPrimaryKey = false;
				colvarCanRetry.IsForeignKey = false;
				colvarCanRetry.IsReadOnly = false;
				colvarCanRetry.DefaultSetting = @"";
				colvarCanRetry.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCanRetry);
				
				TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
				colvarReturnFormId.ColumnName = "return_form_id";
				colvarReturnFormId.DataType = DbType.Int32;
				colvarReturnFormId.MaxLength = 0;
				colvarReturnFormId.AutoIncrement = false;
				colvarReturnFormId.IsNullable = false;
				colvarReturnFormId.IsPrimaryKey = false;
				colvarReturnFormId.IsForeignKey = false;
				colvarReturnFormId.IsReadOnly = false;
				colvarReturnFormId.DefaultSetting = @"";
				colvarReturnFormId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnFormId);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = false;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("third_party_pay_refund_log",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("TransLogId")]
		[Bindable(true)]
		public int TransLogId 
		{
			get { return GetColumnValue<int>(Columns.TransLogId); }
			set { SetColumnValue(Columns.TransLogId, value); }
		}
		
		[XmlAttribute("PaymentTransactionId")]
		[Bindable(true)]
		public int PaymentTransactionId 
		{
			get { return GetColumnValue<int>(Columns.PaymentTransactionId); }
			set { SetColumnValue(Columns.PaymentTransactionId, value); }
		}
		
		[XmlAttribute("RefundStatus")]
		[Bindable(true)]
		public int RefundStatus 
		{
			get { return GetColumnValue<int>(Columns.RefundStatus); }
			set { SetColumnValue(Columns.RefundStatus, value); }
		}
		
		[XmlAttribute("RefundMsg")]
		[Bindable(true)]
		public string RefundMsg 
		{
			get { return GetColumnValue<string>(Columns.RefundMsg); }
			set { SetColumnValue(Columns.RefundMsg, value); }
		}
		
		[XmlAttribute("RetryTime")]
		[Bindable(true)]
		public int RetryTime 
		{
			get { return GetColumnValue<int>(Columns.RetryTime); }
			set { SetColumnValue(Columns.RetryTime, value); }
		}
		
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		
		[XmlAttribute("CanRetry")]
		[Bindable(true)]
		public int CanRetry 
		{
			get { return GetColumnValue<int>(Columns.CanRetry); }
			set { SetColumnValue(Columns.CanRetry, value); }
		}
		
		[XmlAttribute("ReturnFormId")]
		[Bindable(true)]
		public int ReturnFormId 
		{
			get { return GetColumnValue<int>(Columns.ReturnFormId); }
			set { SetColumnValue(Columns.ReturnFormId, value); }
		}
		
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TransLogIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PaymentTransactionIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundStatusColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundMsgColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn RetryTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CanRetryColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnFormIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TransLogId = @"trans_log_id";
			 public static string PaymentTransactionId = @"payment_transaction_id";
			 public static string RefundStatus = @"refund_status";
			 public static string RefundMsg = @"refund_msg";
			 public static string RetryTime = @"retry_time";
			 public static string TrustId = @"trust_id";
			 public static string CanRetry = @"can_retry";
			 public static string ReturnFormId = @"return_form_id";
			 public static string CreateId = @"create_id";
			 public static string ModifyId = @"modify_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

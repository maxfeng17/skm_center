using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using Microsoft.SqlServer.Types;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponStore class.
    /// </summary>
    [Serializable]
    public partial class ViewPponStoreCollection : ReadOnlyList<ViewPponStore, ViewPponStoreCollection>
    {        
        public ViewPponStoreCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_store view.
    /// </summary>
    [Serializable]
    public partial class ViewPponStore : ReadOnlyRecord<ViewPponStore>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_store", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = false;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarTotalQuantity = new TableSchema.TableColumn(schema);
                colvarTotalQuantity.ColumnName = "total_quantity";
                colvarTotalQuantity.DataType = DbType.Int32;
                colvarTotalQuantity.MaxLength = 0;
                colvarTotalQuantity.AutoIncrement = false;
                colvarTotalQuantity.IsNullable = true;
                colvarTotalQuantity.IsPrimaryKey = false;
                colvarTotalQuantity.IsForeignKey = false;
                colvarTotalQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalQuantity);
                
                TableSchema.TableColumn colvarOrderedQuantity = new TableSchema.TableColumn(schema);
                colvarOrderedQuantity.ColumnName = "ordered_quantity";
                colvarOrderedQuantity.DataType = DbType.Int32;
                colvarOrderedQuantity.MaxLength = 0;
                colvarOrderedQuantity.AutoIncrement = false;
                colvarOrderedQuantity.IsNullable = true;
                colvarOrderedQuantity.IsPrimaryKey = false;
                colvarOrderedQuantity.IsForeignKey = false;
                colvarOrderedQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderedQuantity);
                
                TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarChangedExpireDate.ColumnName = "changed_expire_date";
                colvarChangedExpireDate.DataType = DbType.DateTime;
                colvarChangedExpireDate.MaxLength = 0;
                colvarChangedExpireDate.AutoIncrement = false;
                colvarChangedExpireDate.IsNullable = true;
                colvarChangedExpireDate.IsPrimaryKey = false;
                colvarChangedExpireDate.IsForeignKey = false;
                colvarChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarChangedExpireDate);
                
                TableSchema.TableColumn colvarSortOrder = new TableSchema.TableColumn(schema);
                colvarSortOrder.ColumnName = "sort_order";
                colvarSortOrder.DataType = DbType.Int32;
                colvarSortOrder.MaxLength = 0;
                colvarSortOrder.AutoIncrement = false;
                colvarSortOrder.IsNullable = true;
                colvarSortOrder.IsPrimaryKey = false;
                colvarSortOrder.IsForeignKey = false;
                colvarSortOrder.IsReadOnly = false;
                
                schema.Columns.Add(colvarSortOrder);
                
                TableSchema.TableColumn colvarUseTime = new TableSchema.TableColumn(schema);
                colvarUseTime.ColumnName = "use_time";
                colvarUseTime.DataType = DbType.String;
                colvarUseTime.MaxLength = 250;
                colvarUseTime.AutoIncrement = false;
                colvarUseTime.IsNullable = true;
                colvarUseTime.IsPrimaryKey = false;
                colvarUseTime.IsForeignKey = false;
                colvarUseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseTime);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = true;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 20;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = true;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                TableSchema.TableColumn colvarCityShortName = new TableSchema.TableColumn(schema);
                colvarCityShortName.ColumnName = "city_short_name";
                colvarCityShortName.DataType = DbType.String;
                colvarCityShortName.MaxLength = 10;
                colvarCityShortName.AutoIncrement = false;
                colvarCityShortName.IsNullable = true;
                colvarCityShortName.IsPrimaryKey = false;
                colvarCityShortName.IsForeignKey = false;
                colvarCityShortName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityShortName);
                
                TableSchema.TableColumn colvarTownshipName = new TableSchema.TableColumn(schema);
                colvarTownshipName.ColumnName = "township_name";
                colvarTownshipName.DataType = DbType.String;
                colvarTownshipName.MaxLength = 20;
                colvarTownshipName.AutoIncrement = false;
                colvarTownshipName.IsNullable = true;
                colvarTownshipName.IsPrimaryKey = false;
                colvarTownshipName.IsForeignKey = false;
                colvarTownshipName.IsReadOnly = false;
                
                schema.Columns.Add(colvarTownshipName);
                
                TableSchema.TableColumn colvarTownshipShortName = new TableSchema.TableColumn(schema);
                colvarTownshipShortName.ColumnName = "township_short_name";
                colvarTownshipShortName.DataType = DbType.String;
                colvarTownshipShortName.MaxLength = 10;
                colvarTownshipShortName.AutoIncrement = false;
                colvarTownshipShortName.IsNullable = true;
                colvarTownshipShortName.IsPrimaryKey = false;
                colvarTownshipShortName.IsForeignKey = false;
                colvarTownshipShortName.IsReadOnly = false;
                
                schema.Columns.Add(colvarTownshipShortName);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 100;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = false;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
                colvarPhone.ColumnName = "phone";
                colvarPhone.DataType = DbType.AnsiString;
                colvarPhone.MaxLength = 100;
                colvarPhone.AutoIncrement = false;
                colvarPhone.IsNullable = true;
                colvarPhone.IsPrimaryKey = false;
                colvarPhone.IsForeignKey = false;
                colvarPhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhone);
                
                TableSchema.TableColumn colvarTownshipId = new TableSchema.TableColumn(schema);
                colvarTownshipId.ColumnName = "township_id";
                colvarTownshipId.DataType = DbType.Int32;
                colvarTownshipId.MaxLength = 0;
                colvarTownshipId.AutoIncrement = false;
                colvarTownshipId.IsNullable = true;
                colvarTownshipId.IsPrimaryKey = false;
                colvarTownshipId.IsForeignKey = false;
                colvarTownshipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTownshipId);
                
                TableSchema.TableColumn colvarAddressString = new TableSchema.TableColumn(schema);
                colvarAddressString.ColumnName = "address_string";
                colvarAddressString.DataType = DbType.String;
                colvarAddressString.MaxLength = 100;
                colvarAddressString.AutoIncrement = false;
                colvarAddressString.IsNullable = true;
                colvarAddressString.IsPrimaryKey = false;
                colvarAddressString.IsForeignKey = false;
                colvarAddressString.IsReadOnly = false;
                
                schema.Columns.Add(colvarAddressString);
                
                TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
                colvarOpenTime.ColumnName = "open_time";
                colvarOpenTime.DataType = DbType.String;
                colvarOpenTime.MaxLength = 256;
                colvarOpenTime.AutoIncrement = false;
                colvarOpenTime.IsNullable = true;
                colvarOpenTime.IsPrimaryKey = false;
                colvarOpenTime.IsForeignKey = false;
                colvarOpenTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpenTime);
                
                TableSchema.TableColumn colvarCloseDate = new TableSchema.TableColumn(schema);
                colvarCloseDate.ColumnName = "close_date";
                colvarCloseDate.DataType = DbType.String;
                colvarCloseDate.MaxLength = 256;
                colvarCloseDate.AutoIncrement = false;
                colvarCloseDate.IsNullable = true;
                colvarCloseDate.IsPrimaryKey = false;
                colvarCloseDate.IsForeignKey = false;
                colvarCloseDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCloseDate);
                
                TableSchema.TableColumn colvarRemarks = new TableSchema.TableColumn(schema);
                colvarRemarks.ColumnName = "remarks";
                colvarRemarks.DataType = DbType.String;
                colvarRemarks.MaxLength = 256;
                colvarRemarks.AutoIncrement = false;
                colvarRemarks.IsNullable = true;
                colvarRemarks.IsPrimaryKey = false;
                colvarRemarks.IsForeignKey = false;
                colvarRemarks.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemarks);
                
                TableSchema.TableColumn colvarMrt = new TableSchema.TableColumn(schema);
                colvarMrt.ColumnName = "mrt";
                colvarMrt.DataType = DbType.String;
                colvarMrt.MaxLength = 256;
                colvarMrt.AutoIncrement = false;
                colvarMrt.IsNullable = true;
                colvarMrt.IsPrimaryKey = false;
                colvarMrt.IsForeignKey = false;
                colvarMrt.IsReadOnly = false;
                
                schema.Columns.Add(colvarMrt);
                
                TableSchema.TableColumn colvarCar = new TableSchema.TableColumn(schema);
                colvarCar.ColumnName = "car";
                colvarCar.DataType = DbType.String;
                colvarCar.MaxLength = 256;
                colvarCar.AutoIncrement = false;
                colvarCar.IsNullable = true;
                colvarCar.IsPrimaryKey = false;
                colvarCar.IsForeignKey = false;
                colvarCar.IsReadOnly = false;
                
                schema.Columns.Add(colvarCar);
                
                TableSchema.TableColumn colvarBus = new TableSchema.TableColumn(schema);
                colvarBus.ColumnName = "bus";
                colvarBus.DataType = DbType.String;
                colvarBus.MaxLength = 256;
                colvarBus.AutoIncrement = false;
                colvarBus.IsNullable = true;
                colvarBus.IsPrimaryKey = false;
                colvarBus.IsForeignKey = false;
                colvarBus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBus);
                
                TableSchema.TableColumn colvarOtherVehicles = new TableSchema.TableColumn(schema);
                colvarOtherVehicles.ColumnName = "other_vehicles";
                colvarOtherVehicles.DataType = DbType.String;
                colvarOtherVehicles.MaxLength = 256;
                colvarOtherVehicles.AutoIncrement = false;
                colvarOtherVehicles.IsNullable = true;
                colvarOtherVehicles.IsPrimaryKey = false;
                colvarOtherVehicles.IsForeignKey = false;
                colvarOtherVehicles.IsReadOnly = false;
                
                schema.Columns.Add(colvarOtherVehicles);
                
                TableSchema.TableColumn colvarWebUrl = new TableSchema.TableColumn(schema);
                colvarWebUrl.ColumnName = "web_url";
                colvarWebUrl.DataType = DbType.AnsiString;
                colvarWebUrl.MaxLength = 4000;
                colvarWebUrl.AutoIncrement = false;
                colvarWebUrl.IsNullable = true;
                colvarWebUrl.IsPrimaryKey = false;
                colvarWebUrl.IsForeignKey = false;
                colvarWebUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarWebUrl);
                
                TableSchema.TableColumn colvarFacebookUrl = new TableSchema.TableColumn(schema);
                colvarFacebookUrl.ColumnName = "facebook_url";
                colvarFacebookUrl.DataType = DbType.AnsiString;
                colvarFacebookUrl.MaxLength = 4000;
                colvarFacebookUrl.AutoIncrement = false;
                colvarFacebookUrl.IsNullable = true;
                colvarFacebookUrl.IsPrimaryKey = false;
                colvarFacebookUrl.IsForeignKey = false;
                colvarFacebookUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarFacebookUrl);
                
                TableSchema.TableColumn colvarPlurkUrl = new TableSchema.TableColumn(schema);
                colvarPlurkUrl.ColumnName = "plurk_url";
                colvarPlurkUrl.DataType = DbType.AnsiString;
                colvarPlurkUrl.MaxLength = 1;
                colvarPlurkUrl.AutoIncrement = false;
                colvarPlurkUrl.IsNullable = false;
                colvarPlurkUrl.IsPrimaryKey = false;
                colvarPlurkUrl.IsForeignKey = false;
                colvarPlurkUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarPlurkUrl);
                
                TableSchema.TableColumn colvarBlogUrl = new TableSchema.TableColumn(schema);
                colvarBlogUrl.ColumnName = "blog_url";
                colvarBlogUrl.DataType = DbType.AnsiString;
                colvarBlogUrl.MaxLength = 4000;
                colvarBlogUrl.AutoIncrement = false;
                colvarBlogUrl.IsNullable = true;
                colvarBlogUrl.IsPrimaryKey = false;
                colvarBlogUrl.IsForeignKey = false;
                colvarBlogUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarBlogUrl);
                
                TableSchema.TableColumn colvarOtherUrl = new TableSchema.TableColumn(schema);
                colvarOtherUrl.ColumnName = "other_url";
                colvarOtherUrl.DataType = DbType.AnsiString;
                colvarOtherUrl.MaxLength = 4000;
                colvarOtherUrl.AutoIncrement = false;
                colvarOtherUrl.IsNullable = true;
                colvarOtherUrl.IsPrimaryKey = false;
                colvarOtherUrl.IsForeignKey = false;
                colvarOtherUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarOtherUrl);
                
                TableSchema.TableColumn colvarIsCloseDown = new TableSchema.TableColumn(schema);
                colvarIsCloseDown.ColumnName = "is_close_down";
                colvarIsCloseDown.DataType = DbType.Boolean;
                colvarIsCloseDown.MaxLength = 0;
                colvarIsCloseDown.AutoIncrement = false;
                colvarIsCloseDown.IsNullable = false;
                colvarIsCloseDown.IsPrimaryKey = false;
                colvarIsCloseDown.IsForeignKey = false;
                colvarIsCloseDown.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCloseDown);
                
                TableSchema.TableColumn colvarCloseDownDate = new TableSchema.TableColumn(schema);
                colvarCloseDownDate.ColumnName = "close_down_date";
                colvarCloseDownDate.DataType = DbType.DateTime;
                colvarCloseDownDate.MaxLength = 0;
                colvarCloseDownDate.AutoIncrement = false;
                colvarCloseDownDate.IsNullable = true;
                colvarCloseDownDate.IsPrimaryKey = false;
                colvarCloseDownDate.IsForeignKey = false;
                colvarCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCloseDownDate);
                
                TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
                colvarCoordinate.ColumnName = "coordinate";
                colvarCoordinate.DataType = DbType.AnsiString;
                colvarCoordinate.MaxLength = -1;
                colvarCoordinate.AutoIncrement = false;
                colvarCoordinate.IsNullable = true;
                colvarCoordinate.IsPrimaryKey = false;
                colvarCoordinate.IsForeignKey = false;
                colvarCoordinate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCoordinate);
                
                TableSchema.TableColumn colvarCreditcardAvailable = new TableSchema.TableColumn(schema);
                colvarCreditcardAvailable.ColumnName = "creditcard_available";
                colvarCreditcardAvailable.DataType = DbType.Boolean;
                colvarCreditcardAvailable.MaxLength = 0;
                colvarCreditcardAvailable.AutoIncrement = false;
                colvarCreditcardAvailable.IsNullable = false;
                colvarCreditcardAvailable.IsPrimaryKey = false;
                colvarCreditcardAvailable.IsForeignKey = false;
                colvarCreditcardAvailable.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreditcardAvailable);
                
                TableSchema.TableColumn colvarCompanyEmail = new TableSchema.TableColumn(schema);
                colvarCompanyEmail.ColumnName = "CompanyEmail";
                colvarCompanyEmail.DataType = DbType.String;
                colvarCompanyEmail.MaxLength = 250;
                colvarCompanyEmail.AutoIncrement = false;
                colvarCompanyEmail.IsNullable = true;
                colvarCompanyEmail.IsPrimaryKey = false;
                colvarCompanyEmail.IsForeignKey = false;
                colvarCompanyEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyEmail);
                
                TableSchema.TableColumn colvarVbsRight = new TableSchema.TableColumn(schema);
                colvarVbsRight.ColumnName = "vbs_right";
                colvarVbsRight.DataType = DbType.Int32;
                colvarVbsRight.MaxLength = 0;
                colvarVbsRight.AutoIncrement = false;
                colvarVbsRight.IsNullable = false;
                colvarVbsRight.IsPrimaryKey = false;
                colvarVbsRight.IsForeignKey = false;
                colvarVbsRight.IsReadOnly = false;
                
                schema.Columns.Add(colvarVbsRight);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_store",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponStore()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponStore(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponStore(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponStore(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("TotalQuantity")]
        [Bindable(true)]
        public int? TotalQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("total_quantity");
		    }
            set 
		    {
			    SetColumnValue("total_quantity", value);
            }
        }
	      
        [XmlAttribute("OrderedQuantity")]
        [Bindable(true)]
        public int? OrderedQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ordered_quantity");
		    }
            set 
		    {
			    SetColumnValue("ordered_quantity", value);
            }
        }
	      
        [XmlAttribute("ChangedExpireDate")]
        [Bindable(true)]
        public DateTime? ChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("SortOrder")]
        [Bindable(true)]
        public int? SortOrder 
	    {
		    get
		    {
			    return GetColumnValue<int?>("sort_order");
		    }
            set 
		    {
			    SetColumnValue("sort_order", value);
            }
        }
	      
        [XmlAttribute("UseTime")]
        [Bindable(true)]
        public string UseTime 
	    {
		    get
		    {
			    return GetColumnValue<string>("use_time");
		    }
            set 
		    {
			    SetColumnValue("use_time", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int? CityId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	      
        [XmlAttribute("CityShortName")]
        [Bindable(true)]
        public string CityShortName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_short_name");
		    }
            set 
		    {
			    SetColumnValue("city_short_name", value);
            }
        }
	      
        [XmlAttribute("TownshipName")]
        [Bindable(true)]
        public string TownshipName 
	    {
		    get
		    {
			    return GetColumnValue<string>("township_name");
		    }
            set 
		    {
			    SetColumnValue("township_name", value);
            }
        }
	      
        [XmlAttribute("TownshipShortName")]
        [Bindable(true)]
        public string TownshipShortName 
	    {
		    get
		    {
			    return GetColumnValue<string>("township_short_name");
		    }
            set 
		    {
			    SetColumnValue("township_short_name", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("Phone")]
        [Bindable(true)]
        public string Phone 
	    {
		    get
		    {
			    return GetColumnValue<string>("phone");
		    }
            set 
		    {
			    SetColumnValue("phone", value);
            }
        }
	      
        [XmlAttribute("TownshipId")]
        [Bindable(true)]
        public int? TownshipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("township_id");
		    }
            set 
		    {
			    SetColumnValue("township_id", value);
            }
        }
	      
        [XmlAttribute("AddressString")]
        [Bindable(true)]
        public string AddressString 
	    {
		    get
		    {
			    return GetColumnValue<string>("address_string");
		    }
            set 
		    {
			    SetColumnValue("address_string", value);
            }
        }
	      
        [XmlAttribute("OpenTime")]
        [Bindable(true)]
        public string OpenTime 
	    {
		    get
		    {
			    return GetColumnValue<string>("open_time");
		    }
            set 
		    {
			    SetColumnValue("open_time", value);
            }
        }
	      
        [XmlAttribute("CloseDate")]
        [Bindable(true)]
        public string CloseDate 
	    {
		    get
		    {
			    return GetColumnValue<string>("close_date");
		    }
            set 
		    {
			    SetColumnValue("close_date", value);
            }
        }
	      
        [XmlAttribute("Remarks")]
        [Bindable(true)]
        public string Remarks 
	    {
		    get
		    {
			    return GetColumnValue<string>("remarks");
		    }
            set 
		    {
			    SetColumnValue("remarks", value);
            }
        }
	      
        [XmlAttribute("Mrt")]
        [Bindable(true)]
        public string Mrt 
	    {
		    get
		    {
			    return GetColumnValue<string>("mrt");
		    }
            set 
		    {
			    SetColumnValue("mrt", value);
            }
        }
	      
        [XmlAttribute("Car")]
        [Bindable(true)]
        public string Car 
	    {
		    get
		    {
			    return GetColumnValue<string>("car");
		    }
            set 
		    {
			    SetColumnValue("car", value);
            }
        }
	      
        [XmlAttribute("Bus")]
        [Bindable(true)]
        public string Bus 
	    {
		    get
		    {
			    return GetColumnValue<string>("bus");
		    }
            set 
		    {
			    SetColumnValue("bus", value);
            }
        }
	      
        [XmlAttribute("OtherVehicles")]
        [Bindable(true)]
        public string OtherVehicles 
	    {
		    get
		    {
			    return GetColumnValue<string>("other_vehicles");
		    }
            set 
		    {
			    SetColumnValue("other_vehicles", value);
            }
        }
	      
        [XmlAttribute("WebUrl")]
        [Bindable(true)]
        public string WebUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("web_url");
		    }
            set 
		    {
			    SetColumnValue("web_url", value);
            }
        }
	      
        [XmlAttribute("FacebookUrl")]
        [Bindable(true)]
        public string FacebookUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("facebook_url");
		    }
            set 
		    {
			    SetColumnValue("facebook_url", value);
            }
        }
	      
        [XmlAttribute("PlurkUrl")]
        [Bindable(true)]
        public string PlurkUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("plurk_url");
		    }
            set 
		    {
			    SetColumnValue("plurk_url", value);
            }
        }
	      
        [XmlAttribute("BlogUrl")]
        [Bindable(true)]
        public string BlogUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("blog_url");
		    }
            set 
		    {
			    SetColumnValue("blog_url", value);
            }
        }
	      
        [XmlAttribute("OtherUrl")]
        [Bindable(true)]
        public string OtherUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("other_url");
		    }
            set 
		    {
			    SetColumnValue("other_url", value);
            }
        }
	      
        [XmlAttribute("IsCloseDown")]
        [Bindable(true)]
        public bool IsCloseDown 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_close_down");
		    }
            set 
		    {
			    SetColumnValue("is_close_down", value);
            }
        }
	      
        [XmlAttribute("CloseDownDate")]
        [Bindable(true)]
        public DateTime? CloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("close_down_date");
		    }
            set 
		    {
			    SetColumnValue("close_down_date", value);
            }
        }
	      
        [XmlAttribute("Coordinate")]
        [Bindable(true)]
        public string Coordinate 
	    {
		    get
		    {
                return GetColumnValue<object>(Columns.Coordinate) != null ?
                    GetColumnValue<object>(Columns.Coordinate).ToString() : string.Empty;
		    }
            set 
		    {
			    SetColumnValue("coordinate", value);
            }
        }
	      
        [XmlAttribute("CreditcardAvailable")]
        [Bindable(true)]
        public bool CreditcardAvailable 
	    {
		    get
		    {
			    return GetColumnValue<bool>("creditcard_available");
		    }
            set 
		    {
			    SetColumnValue("creditcard_available", value);
            }
        }
	      
        [XmlAttribute("CompanyEmail")]
        [Bindable(true)]
        public string CompanyEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyEmail");
		    }
            set 
		    {
			    SetColumnValue("CompanyEmail", value);
            }
        }
	      
        [XmlAttribute("VbsRight")]
        [Bindable(true)]
        public int VbsRight 
	    {
		    get
		    {
			    return GetColumnValue<int>("vbs_right");
		    }
            set 
		    {
			    SetColumnValue("vbs_right", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string StoreGuid = @"store_guid";
            
            public static string TotalQuantity = @"total_quantity";
            
            public static string OrderedQuantity = @"ordered_quantity";
            
            public static string ChangedExpireDate = @"changed_expire_date";
            
            public static string SortOrder = @"sort_order";
            
            public static string UseTime = @"use_time";
            
            public static string CityId = @"city_id";
            
            public static string CityName = @"city_name";
            
            public static string CityShortName = @"city_short_name";
            
            public static string TownshipName = @"township_name";
            
            public static string TownshipShortName = @"township_short_name";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string StoreName = @"store_name";
            
            public static string Phone = @"phone";
            
            public static string TownshipId = @"township_id";
            
            public static string AddressString = @"address_string";
            
            public static string OpenTime = @"open_time";
            
            public static string CloseDate = @"close_date";
            
            public static string Remarks = @"remarks";
            
            public static string Mrt = @"mrt";
            
            public static string Car = @"car";
            
            public static string Bus = @"bus";
            
            public static string OtherVehicles = @"other_vehicles";
            
            public static string WebUrl = @"web_url";
            
            public static string FacebookUrl = @"facebook_url";
            
            public static string PlurkUrl = @"plurk_url";
            
            public static string BlogUrl = @"blog_url";
            
            public static string OtherUrl = @"other_url";
            
            public static string IsCloseDown = @"is_close_down";
            
            public static string CloseDownDate = @"close_down_date";
            
            public static string Coordinate = @"coordinate";
            
            public static string CreditcardAvailable = @"creditcard_available";
            
            public static string CompanyEmail = @"CompanyEmail";
            
            public static string VbsRight = @"vbs_right";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

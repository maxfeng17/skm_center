using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBalanceDetail class.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceDetailCollection : ReadOnlyList<ViewBalanceDetail, ViewBalanceDetailCollection>
    {        
        public ViewBalanceDetailCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_balance_detail view.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceDetail : ReadOnlyRecord<ViewBalanceDetail>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_balance_detail", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBalanceSheetId = new TableSchema.TableColumn(schema);
                colvarBalanceSheetId.ColumnName = "balance_sheet_id";
                colvarBalanceSheetId.DataType = DbType.Int32;
                colvarBalanceSheetId.MaxLength = 0;
                colvarBalanceSheetId.AutoIncrement = false;
                colvarBalanceSheetId.IsNullable = false;
                colvarBalanceSheetId.IsPrimaryKey = false;
                colvarBalanceSheetId.IsForeignKey = false;
                colvarBalanceSheetId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBalanceSheetId);
                
                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustId);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarVerifyUserName = new TableSchema.TableColumn(schema);
                colvarVerifyUserName.ColumnName = "verify_user_name";
                colvarVerifyUserName.DataType = DbType.String;
                colvarVerifyUserName.MaxLength = 256;
                colvarVerifyUserName.AutoIncrement = false;
                colvarVerifyUserName.IsNullable = true;
                colvarVerifyUserName.IsPrimaryKey = false;
                colvarVerifyUserName.IsForeignKey = false;
                colvarVerifyUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifyUserName);
                
                TableSchema.TableColumn colvarVerifyTime = new TableSchema.TableColumn(schema);
                colvarVerifyTime.ColumnName = "verify_time";
                colvarVerifyTime.DataType = DbType.DateTime;
                colvarVerifyTime.MaxLength = 0;
                colvarVerifyTime.AutoIncrement = false;
                colvarVerifyTime.IsNullable = true;
                colvarVerifyTime.IsPrimaryKey = false;
                colvarVerifyTime.IsForeignKey = false;
                colvarVerifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifyTime);
                
                TableSchema.TableColumn colvarUndoUserName = new TableSchema.TableColumn(schema);
                colvarUndoUserName.ColumnName = "undo_user_name";
                colvarUndoUserName.DataType = DbType.String;
                colvarUndoUserName.MaxLength = 256;
                colvarUndoUserName.AutoIncrement = false;
                colvarUndoUserName.IsNullable = true;
                colvarUndoUserName.IsPrimaryKey = false;
                colvarUndoUserName.IsForeignKey = false;
                colvarUndoUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUndoUserName);
                
                TableSchema.TableColumn colvarUndoTime = new TableSchema.TableColumn(schema);
                colvarUndoTime.ColumnName = "undo_time";
                colvarUndoTime.DataType = DbType.DateTime;
                colvarUndoTime.MaxLength = 0;
                colvarUndoTime.AutoIncrement = false;
                colvarUndoTime.IsNullable = true;
                colvarUndoTime.IsPrimaryKey = false;
                colvarUndoTime.IsForeignKey = false;
                colvarUndoTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUndoTime);
                
                TableSchema.TableColumn colvarModifyUserName = new TableSchema.TableColumn(schema);
                colvarModifyUserName.ColumnName = "modify_user_name";
                colvarModifyUserName.DataType = DbType.String;
                colvarModifyUserName.MaxLength = 256;
                colvarModifyUserName.AutoIncrement = false;
                colvarModifyUserName.IsNullable = true;
                colvarModifyUserName.IsPrimaryKey = false;
                colvarModifyUserName.IsForeignKey = false;
                colvarModifyUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyUserName);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 100;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage);
                
                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponId);
                
                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.String;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = true;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponSequenceNumber);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_balance_detail",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBalanceDetail()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBalanceDetail(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBalanceDetail(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBalanceDetail(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BalanceSheetId")]
        [Bindable(true)]
        public int BalanceSheetId 
	    {
		    get
		    {
			    return GetColumnValue<int>("balance_sheet_id");
		    }
            set 
		    {
			    SetColumnValue("balance_sheet_id", value);
            }
        }
	      
        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("trust_id");
		    }
            set 
		    {
			    SetColumnValue("trust_id", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("VerifyUserName")]
        [Bindable(true)]
        public string VerifyUserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("verify_user_name");
		    }
            set 
		    {
			    SetColumnValue("verify_user_name", value);
            }
        }
	      
        [XmlAttribute("VerifyTime")]
        [Bindable(true)]
        public DateTime? VerifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("verify_time");
		    }
            set 
		    {
			    SetColumnValue("verify_time", value);
            }
        }
	      
        [XmlAttribute("UndoUserName")]
        [Bindable(true)]
        public string UndoUserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("undo_user_name");
		    }
            set 
		    {
			    SetColumnValue("undo_user_name", value);
            }
        }
	      
        [XmlAttribute("UndoTime")]
        [Bindable(true)]
        public DateTime? UndoTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("undo_time");
		    }
            set 
		    {
			    SetColumnValue("undo_time", value);
            }
        }
	      
        [XmlAttribute("ModifyUserName")]
        [Bindable(true)]
        public string ModifyUserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_user_name");
		    }
            set 
		    {
			    SetColumnValue("modify_user_name", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message 
	    {
		    get
		    {
			    return GetColumnValue<string>("message");
		    }
            set 
		    {
			    SetColumnValue("message", value);
            }
        }
	      
        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("coupon_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_id", value);
            }
        }
	      
        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_sequence_number");
		    }
            set 
		    {
			    SetColumnValue("coupon_sequence_number", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BalanceSheetId = @"balance_sheet_id";
            
            public static string TrustId = @"trust_id";
            
            public static string Status = @"status";
            
            public static string VerifyUserName = @"verify_user_name";
            
            public static string VerifyTime = @"verify_time";
            
            public static string UndoUserName = @"undo_user_name";
            
            public static string UndoTime = @"undo_time";
            
            public static string ModifyUserName = @"modify_user_name";
            
            public static string ModifyTime = @"modify_time";
            
            public static string Message = @"message";
            
            public static string CouponId = @"coupon_id";
            
            public static string CouponSequenceNumber = @"coupon_sequence_number";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

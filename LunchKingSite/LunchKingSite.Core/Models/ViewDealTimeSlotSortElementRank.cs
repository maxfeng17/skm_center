using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewDealTimeSlotSortElementRank class.
    /// </summary>
    [Serializable]
    public partial class ViewDealTimeSlotSortElementRankCollection : ReadOnlyList<ViewDealTimeSlotSortElementRank, ViewDealTimeSlotSortElementRankCollection>
    {        
        public ViewDealTimeSlotSortElementRankCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_deal_time_slot_sort_element_rank view.
    /// </summary>
    [Serializable]
    public partial class ViewDealTimeSlotSortElementRank : ReadOnlyRecord<ViewDealTimeSlotSortElementRank>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_deal_time_slot_sort_element_rank", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarElementId = new TableSchema.TableColumn(schema);
                colvarElementId.ColumnName = "element_id";
                colvarElementId.DataType = DbType.Int32;
                colvarElementId.MaxLength = 0;
                colvarElementId.AutoIncrement = false;
                colvarElementId.IsNullable = false;
                colvarElementId.IsPrimaryKey = false;
                colvarElementId.IsForeignKey = false;
                colvarElementId.IsReadOnly = false;
                
                schema.Columns.Add(colvarElementId);
                
                TableSchema.TableColumn colvarElementWeight = new TableSchema.TableColumn(schema);
                colvarElementWeight.ColumnName = "element_weight";
                colvarElementWeight.DataType = DbType.Double;
                colvarElementWeight.MaxLength = 0;
                colvarElementWeight.AutoIncrement = false;
                colvarElementWeight.IsNullable = false;
                colvarElementWeight.IsPrimaryKey = false;
                colvarElementWeight.IsForeignKey = false;
                colvarElementWeight.IsReadOnly = false;
                
                schema.Columns.Add(colvarElementWeight);
                
                TableSchema.TableColumn colvarRankStart = new TableSchema.TableColumn(schema);
                colvarRankStart.ColumnName = "rank_start";
                colvarRankStart.DataType = DbType.Double;
                colvarRankStart.MaxLength = 0;
                colvarRankStart.AutoIncrement = false;
                colvarRankStart.IsNullable = true;
                colvarRankStart.IsPrimaryKey = false;
                colvarRankStart.IsForeignKey = false;
                colvarRankStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarRankStart);
                
                TableSchema.TableColumn colvarRankEnd = new TableSchema.TableColumn(schema);
                colvarRankEnd.ColumnName = "rank_end";
                colvarRankEnd.DataType = DbType.Double;
                colvarRankEnd.MaxLength = 0;
                colvarRankEnd.AutoIncrement = false;
                colvarRankEnd.IsNullable = false;
                colvarRankEnd.IsPrimaryKey = false;
                colvarRankEnd.IsForeignKey = false;
                colvarRankEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarRankEnd);
                
                TableSchema.TableColumn colvarScore = new TableSchema.TableColumn(schema);
                colvarScore.ColumnName = "score";
                colvarScore.DataType = DbType.Double;
                colvarScore.MaxLength = 0;
                colvarScore.AutoIncrement = false;
                colvarScore.IsNullable = false;
                colvarScore.IsPrimaryKey = false;
                colvarScore.IsForeignKey = false;
                colvarScore.IsReadOnly = false;
                
                schema.Columns.Add(colvarScore);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_deal_time_slot_sort_element_rank",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewDealTimeSlotSortElementRank()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewDealTimeSlotSortElementRank(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewDealTimeSlotSortElementRank(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewDealTimeSlotSortElementRank(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ElementId")]
        [Bindable(true)]
        public int ElementId 
	    {
		    get
		    {
			    return GetColumnValue<int>("element_id");
		    }
            set 
		    {
			    SetColumnValue("element_id", value);
            }
        }
	      
        [XmlAttribute("ElementWeight")]
        [Bindable(true)]
        public double ElementWeight 
	    {
		    get
		    {
			    return GetColumnValue<double>("element_weight");
		    }
            set 
		    {
			    SetColumnValue("element_weight", value);
            }
        }
	      
        [XmlAttribute("RankStart")]
        [Bindable(true)]
        public double? RankStart 
	    {
		    get
		    {
			    return GetColumnValue<double?>("rank_start");
		    }
            set 
		    {
			    SetColumnValue("rank_start", value);
            }
        }
	      
        [XmlAttribute("RankEnd")]
        [Bindable(true)]
        public double RankEnd 
	    {
		    get
		    {
			    return GetColumnValue<double>("rank_end");
		    }
            set 
		    {
			    SetColumnValue("rank_end", value);
            }
        }
	      
        [XmlAttribute("Score")]
        [Bindable(true)]
        public double Score 
	    {
		    get
		    {
			    return GetColumnValue<double>("score");
		    }
            set 
		    {
			    SetColumnValue("score", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ElementId = @"element_id";
            
            public static string ElementWeight = @"element_weight";
            
            public static string RankStart = @"rank_start";
            
            public static string RankEnd = @"rank_end";
            
            public static string Score = @"score";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

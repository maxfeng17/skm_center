using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VbsMembershipBindAccount class.
	/// </summary>
    [Serializable]
	public partial class VbsMembershipBindAccountCollection : RepositoryList<VbsMembershipBindAccount, VbsMembershipBindAccountCollection>
	{	   
		public VbsMembershipBindAccountCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VbsMembershipBindAccountCollection</returns>
		public VbsMembershipBindAccountCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VbsMembershipBindAccount o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the vbs_membership_bind_account table.
	/// </summary>
	
	[Serializable]
	public partial class VbsMembershipBindAccount : RepositoryRecord<VbsMembershipBindAccount>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VbsMembershipBindAccount()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VbsMembershipBindAccount(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("vbs_membership_bind_account", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarVbsAccountId = new TableSchema.TableColumn(schema);
				colvarVbsAccountId.ColumnName = "vbs_account_id";
				colvarVbsAccountId.DataType = DbType.AnsiString;
				colvarVbsAccountId.MaxLength = 256;
				colvarVbsAccountId.AutoIncrement = false;
				colvarVbsAccountId.IsNullable = false;
				colvarVbsAccountId.IsPrimaryKey = false;
				colvarVbsAccountId.IsForeignKey = false;
				colvarVbsAccountId.IsReadOnly = false;
				colvarVbsAccountId.DefaultSetting = @"";
				colvarVbsAccountId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVbsAccountId);
				
				TableSchema.TableColumn colvarIsDelete = new TableSchema.TableColumn(schema);
				colvarIsDelete.ColumnName = "is_delete";
				colvarIsDelete.DataType = DbType.Boolean;
				colvarIsDelete.MaxLength = 0;
				colvarIsDelete.AutoIncrement = false;
				colvarIsDelete.IsNullable = false;
				colvarIsDelete.IsPrimaryKey = false;
				colvarIsDelete.IsForeignKey = false;
				colvarIsDelete.IsReadOnly = false;
				colvarIsDelete.DefaultSetting = @"";
				colvarIsDelete.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDelete);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				
						colvarModifyTime.DefaultSetting = @"(getdate())";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarBindType = new TableSchema.TableColumn(schema);
				colvarBindType.ColumnName = "bind_type";
				colvarBindType.DataType = DbType.Int32;
				colvarBindType.MaxLength = 0;
				colvarBindType.AutoIncrement = false;
				colvarBindType.IsNullable = false;
				colvarBindType.IsPrimaryKey = false;
				colvarBindType.IsForeignKey = false;
				colvarBindType.IsReadOnly = false;
				colvarBindType.DefaultSetting = @"";
				colvarBindType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBindType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("vbs_membership_bind_account",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("VbsAccountId")]
		[Bindable(true)]
		public string VbsAccountId 
		{
			get { return GetColumnValue<string>(Columns.VbsAccountId); }
			set { SetColumnValue(Columns.VbsAccountId, value); }
		}
		
		[XmlAttribute("IsDelete")]
		[Bindable(true)]
		public bool IsDelete 
		{
			get { return GetColumnValue<bool>(Columns.IsDelete); }
			set { SetColumnValue(Columns.IsDelete, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		[XmlAttribute("BindType")]
		[Bindable(true)]
		public int BindType 
		{
			get { return GetColumnValue<int>(Columns.BindType); }
			set { SetColumnValue(Columns.BindType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn VbsAccountIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDeleteColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BindTypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string VbsAccountId = @"vbs_account_id";
			 public static string IsDelete = @"is_delete";
			 public static string ModifyTime = @"modify_time";
			 public static string CreateTime = @"create_time";
			 public static string UserId = @"user_id";
			 public static string BindType = @"bind_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

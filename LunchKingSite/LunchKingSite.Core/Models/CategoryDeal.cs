using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CategoryDeal class.
	/// </summary>
    [Serializable]
	public partial class CategoryDealCollection : RepositoryList<CategoryDeal, CategoryDealCollection>
	{	   
		public CategoryDealCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CategoryDealCollection</returns>
		public CategoryDealCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CategoryDeal o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the category_deals table.
	/// </summary>
	[Serializable]
	public partial class CategoryDeal : RepositoryRecord<CategoryDeal>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CategoryDeal()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CategoryDeal(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("category_deals", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarCid = new TableSchema.TableColumn(schema);
				colvarCid.ColumnName = "cid";
				colvarCid.DataType = DbType.Int32;
				colvarCid.MaxLength = 0;
				colvarCid.AutoIncrement = false;
				colvarCid.IsNullable = false;
				colvarCid.IsPrimaryKey = false;
				colvarCid.IsForeignKey = false;
				colvarCid.IsReadOnly = false;
				colvarCid.DefaultSetting = @"";
				colvarCid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCid);
				
				TableSchema.TableColumn colvarStartdt = new TableSchema.TableColumn(schema);
				colvarStartdt.ColumnName = "startdt";
				colvarStartdt.DataType = DbType.DateTime;
				colvarStartdt.MaxLength = 0;
				colvarStartdt.AutoIncrement = false;
				colvarStartdt.IsNullable = true;
				colvarStartdt.IsPrimaryKey = false;
				colvarStartdt.IsForeignKey = false;
				colvarStartdt.IsReadOnly = false;
				colvarStartdt.DefaultSetting = @"";
				colvarStartdt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartdt);
				
				TableSchema.TableColumn colvarEnddt = new TableSchema.TableColumn(schema);
				colvarEnddt.ColumnName = "enddt";
				colvarEnddt.DataType = DbType.DateTime;
				colvarEnddt.MaxLength = 0;
				colvarEnddt.AutoIncrement = false;
				colvarEnddt.IsNullable = true;
				colvarEnddt.IsPrimaryKey = false;
				colvarEnddt.IsForeignKey = false;
				colvarEnddt.IsReadOnly = false;
				colvarEnddt.DefaultSetting = @"";
				colvarEnddt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnddt);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("category_deals",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("Cid")]
		[Bindable(true)]
		public int Cid 
		{
			get { return GetColumnValue<int>(Columns.Cid); }
			set { SetColumnValue(Columns.Cid, value); }
		}
		  
		[XmlAttribute("Startdt")]
		[Bindable(true)]
		public DateTime? Startdt 
		{
			get { return GetColumnValue<DateTime?>(Columns.Startdt); }
			set { SetColumnValue(Columns.Startdt, value); }
		}
		  
		[XmlAttribute("Enddt")]
		[Bindable(true)]
		public DateTime? Enddt 
		{
			get { return GetColumnValue<DateTime?>(Columns.Enddt); }
			set { SetColumnValue(Columns.Enddt, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn StartdtColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EnddtColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Bid = @"bid";
			 public static string Cid = @"cid";
			 public static string Startdt = @"startdt";
			 public static string Enddt = @"enddt";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MasterpassLogTemp class.
	/// </summary>
    [Serializable]
	public partial class MasterpassLogTempCollection : RepositoryList<MasterpassLogTemp, MasterpassLogTempCollection>
	{	   
		public MasterpassLogTempCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MasterpassLogTempCollection</returns>
		public MasterpassLogTempCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MasterpassLogTemp o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the masterpass_log_temp table.
	/// </summary>
	
	[Serializable]
	public partial class MasterpassLogTemp : RepositoryRecord<MasterpassLogTemp>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MasterpassLogTemp()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MasterpassLogTemp(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("masterpass_log_temp", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarLogId = new TableSchema.TableColumn(schema);
				colvarLogId.ColumnName = "log_id";
				colvarLogId.DataType = DbType.Int32;
				colvarLogId.MaxLength = 0;
				colvarLogId.AutoIncrement = false;
				colvarLogId.IsNullable = false;
				colvarLogId.IsPrimaryKey = true;
				colvarLogId.IsForeignKey = false;
				colvarLogId.IsReadOnly = false;
				colvarLogId.DefaultSetting = @"";
				colvarLogId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogId);
				
				TableSchema.TableColumn colvarAccountNumber = new TableSchema.TableColumn(schema);
				colvarAccountNumber.ColumnName = "account_number";
				colvarAccountNumber.DataType = DbType.AnsiString;
				colvarAccountNumber.MaxLength = 100;
				colvarAccountNumber.AutoIncrement = false;
				colvarAccountNumber.IsNullable = false;
				colvarAccountNumber.IsPrimaryKey = false;
				colvarAccountNumber.IsForeignKey = false;
				colvarAccountNumber.IsReadOnly = false;
				colvarAccountNumber.DefaultSetting = @"";
				colvarAccountNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountNumber);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("masterpass_log_temp",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("LogId")]
		[Bindable(true)]
		public int LogId 
		{
			get { return GetColumnValue<int>(Columns.LogId); }
			set { SetColumnValue(Columns.LogId, value); }
		}
		
		[XmlAttribute("AccountNumber")]
		[Bindable(true)]
		public string AccountNumber 
		{
			get { return GetColumnValue<string>(Columns.AccountNumber); }
			set { SetColumnValue(Columns.AccountNumber, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn LogIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountNumberColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string LogId = @"log_id";
			 public static string AccountNumber = @"account_number";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

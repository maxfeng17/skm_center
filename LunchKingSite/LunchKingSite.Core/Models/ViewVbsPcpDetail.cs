using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVbsPcpDetail class.
    /// </summary>
    [Serializable]
    public partial class ViewVbsPcpDetailCollection : ReadOnlyList<ViewVbsPcpDetail, ViewVbsPcpDetailCollection>
    {        
        public ViewVbsPcpDetailCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vbs_pcp_detail view.
    /// </summary>
    [Serializable]
    public partial class ViewVbsPcpDetail : ReadOnlyRecord<ViewVbsPcpDetail>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vbs_pcp_detail", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCategoryList = new TableSchema.TableColumn(schema);
                colvarCategoryList.ColumnName = "category_list";
                colvarCategoryList.DataType = DbType.AnsiString;
                colvarCategoryList.MaxLength = -1;
                colvarCategoryList.AutoIncrement = false;
                colvarCategoryList.IsNullable = false;
                colvarCategoryList.IsPrimaryKey = false;
                colvarCategoryList.IsForeignKey = false;
                colvarCategoryList.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryList);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
                colvarEmail.ColumnName = "email";
                colvarEmail.DataType = DbType.String;
                colvarEmail.MaxLength = 256;
                colvarEmail.AutoIncrement = false;
                colvarEmail.IsNullable = true;
                colvarEmail.IsPrimaryKey = false;
                colvarEmail.IsForeignKey = false;
                colvarEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarEmail);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 20;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                TableSchema.TableColumn colvarTownshipId = new TableSchema.TableColumn(schema);
                colvarTownshipId.ColumnName = "township_id";
                colvarTownshipId.DataType = DbType.Int32;
                colvarTownshipId.MaxLength = 0;
                colvarTownshipId.AutoIncrement = false;
                colvarTownshipId.IsNullable = true;
                colvarTownshipId.IsPrimaryKey = false;
                colvarTownshipId.IsForeignKey = false;
                colvarTownshipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTownshipId);
                
                TableSchema.TableColumn colvarInvoiceTitle = new TableSchema.TableColumn(schema);
                colvarInvoiceTitle.ColumnName = "invoice_title";
                colvarInvoiceTitle.DataType = DbType.String;
                colvarInvoiceTitle.MaxLength = 250;
                colvarInvoiceTitle.AutoIncrement = false;
                colvarInvoiceTitle.IsNullable = true;
                colvarInvoiceTitle.IsPrimaryKey = false;
                colvarInvoiceTitle.IsForeignKey = false;
                colvarInvoiceTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceTitle);
                
                TableSchema.TableColumn colvarInvoiceComId = new TableSchema.TableColumn(schema);
                colvarInvoiceComId.ColumnName = "invoice_com_id";
                colvarInvoiceComId.DataType = DbType.AnsiString;
                colvarInvoiceComId.MaxLength = 10;
                colvarInvoiceComId.AutoIncrement = false;
                colvarInvoiceComId.IsNullable = true;
                colvarInvoiceComId.IsPrimaryKey = false;
                colvarInvoiceComId.IsForeignKey = false;
                colvarInvoiceComId.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceComId);
                
                TableSchema.TableColumn colvarInvoiceName = new TableSchema.TableColumn(schema);
                colvarInvoiceName.ColumnName = "invoice_name";
                colvarInvoiceName.DataType = DbType.String;
                colvarInvoiceName.MaxLength = 50;
                colvarInvoiceName.AutoIncrement = false;
                colvarInvoiceName.IsNullable = true;
                colvarInvoiceName.IsPrimaryKey = false;
                colvarInvoiceName.IsForeignKey = false;
                colvarInvoiceName.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceName);
                
                TableSchema.TableColumn colvarInvoiceAddress = new TableSchema.TableColumn(schema);
                colvarInvoiceAddress.ColumnName = "invoice_address";
                colvarInvoiceAddress.DataType = DbType.String;
                colvarInvoiceAddress.MaxLength = 250;
                colvarInvoiceAddress.AutoIncrement = false;
                colvarInvoiceAddress.IsNullable = true;
                colvarInvoiceAddress.IsPrimaryKey = false;
                colvarInvoiceAddress.IsForeignKey = false;
                colvarInvoiceAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceAddress);
                
                TableSchema.TableColumn colvarAccountNo = new TableSchema.TableColumn(schema);
                colvarAccountNo.ColumnName = "account_no";
                colvarAccountNo.DataType = DbType.AnsiString;
                colvarAccountNo.MaxLength = 20;
                colvarAccountNo.AutoIncrement = false;
                colvarAccountNo.IsNullable = true;
                colvarAccountNo.IsPrimaryKey = false;
                colvarAccountNo.IsForeignKey = false;
                colvarAccountNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountNo);
                
                TableSchema.TableColumn colvarAccountBankCode = new TableSchema.TableColumn(schema);
                colvarAccountBankCode.ColumnName = "account_bank_code";
                colvarAccountBankCode.DataType = DbType.AnsiString;
                colvarAccountBankCode.MaxLength = 10;
                colvarAccountBankCode.AutoIncrement = false;
                colvarAccountBankCode.IsNullable = true;
                colvarAccountBankCode.IsPrimaryKey = false;
                colvarAccountBankCode.IsForeignKey = false;
                colvarAccountBankCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountBankCode);
                
                TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
                colvarAccountName.ColumnName = "account_name";
                colvarAccountName.DataType = DbType.String;
                colvarAccountName.MaxLength = 50;
                colvarAccountName.AutoIncrement = false;
                colvarAccountName.IsNullable = true;
                colvarAccountName.IsPrimaryKey = false;
                colvarAccountName.IsForeignKey = false;
                colvarAccountName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountName);
                
                TableSchema.TableColumn colvarAccountBranchCode = new TableSchema.TableColumn(schema);
                colvarAccountBranchCode.ColumnName = "account_branch_code";
                colvarAccountBranchCode.DataType = DbType.AnsiString;
                colvarAccountBranchCode.MaxLength = 10;
                colvarAccountBranchCode.AutoIncrement = false;
                colvarAccountBranchCode.IsNullable = true;
                colvarAccountBranchCode.IsPrimaryKey = false;
                colvarAccountBranchCode.IsForeignKey = false;
                colvarAccountBranchCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountBranchCode);
                
                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.AnsiString;
                colvarAccountId.MaxLength = 20;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = true;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountId);
                
                TableSchema.TableColumn colvarInContractWith = new TableSchema.TableColumn(schema);
                colvarInContractWith.ColumnName = "in_contract_with";
                colvarInContractWith.DataType = DbType.String;
                colvarInContractWith.MaxLength = 80;
                colvarInContractWith.AutoIncrement = false;
                colvarInContractWith.IsNullable = true;
                colvarInContractWith.IsPrimaryKey = false;
                colvarInContractWith.IsForeignKey = false;
                colvarInContractWith.IsReadOnly = false;
                
                schema.Columns.Add(colvarInContractWith);
                
                TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
                colvarSellerEmail.ColumnName = "seller_email";
                colvarSellerEmail.DataType = DbType.String;
                colvarSellerEmail.MaxLength = 256;
                colvarSellerEmail.AutoIncrement = false;
                colvarSellerEmail.IsNullable = true;
                colvarSellerEmail.IsPrimaryKey = false;
                colvarSellerEmail.IsForeignKey = false;
                colvarSellerEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerEmail);
                
                TableSchema.TableColumn colvarSellerMobile = new TableSchema.TableColumn(schema);
                colvarSellerMobile.ColumnName = "seller_mobile";
                colvarSellerMobile.DataType = DbType.AnsiString;
                colvarSellerMobile.MaxLength = 20;
                colvarSellerMobile.AutoIncrement = false;
                colvarSellerMobile.IsNullable = true;
                colvarSellerMobile.IsPrimaryKey = false;
                colvarSellerMobile.IsForeignKey = false;
                colvarSellerMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMobile);
                
                TableSchema.TableColumn colvarContractValidDate = new TableSchema.TableColumn(schema);
                colvarContractValidDate.ColumnName = "contract_valid_date";
                colvarContractValidDate.DataType = DbType.DateTime;
                colvarContractValidDate.MaxLength = 0;
                colvarContractValidDate.AutoIncrement = false;
                colvarContractValidDate.IsNullable = true;
                colvarContractValidDate.IsPrimaryKey = false;
                colvarContractValidDate.IsForeignKey = false;
                colvarContractValidDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarContractValidDate);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
                colvarGroupId.ColumnName = "group_id";
                colvarGroupId.DataType = DbType.Int32;
                colvarGroupId.MaxLength = 0;
                colvarGroupId.AutoIncrement = false;
                colvarGroupId.IsNullable = false;
                colvarGroupId.IsPrimaryKey = false;
                colvarGroupId.IsForeignKey = false;
                colvarGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupId);
                
                TableSchema.TableColumn colvarCardType = new TableSchema.TableColumn(schema);
                colvarCardType.ColumnName = "card_type";
                colvarCardType.DataType = DbType.Int32;
                colvarCardType.MaxLength = 0;
                colvarCardType.AutoIncrement = false;
                colvarCardType.IsNullable = false;
                colvarCardType.IsPrimaryKey = false;
                colvarCardType.IsForeignKey = false;
                colvarCardType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardType);
                
                TableSchema.TableColumn colvarIsPromo = new TableSchema.TableColumn(schema);
                colvarIsPromo.ColumnName = "is_promo";
                colvarIsPromo.DataType = DbType.Boolean;
                colvarIsPromo.MaxLength = 0;
                colvarIsPromo.AutoIncrement = false;
                colvarIsPromo.IsNullable = false;
                colvarIsPromo.IsPrimaryKey = false;
                colvarIsPromo.IsForeignKey = false;
                colvarIsPromo.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsPromo);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vbs_pcp_detail",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVbsPcpDetail()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVbsPcpDetail(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVbsPcpDetail(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVbsPcpDetail(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CategoryList")]
        [Bindable(true)]
        public string CategoryList 
	    {
		    get
		    {
			    return GetColumnValue<string>("category_list");
		    }
            set 
		    {
			    SetColumnValue("category_list", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("Email")]
        [Bindable(true)]
        public string Email 
	    {
		    get
		    {
			    return GetColumnValue<string>("email");
		    }
            set 
		    {
			    SetColumnValue("email", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	      
        [XmlAttribute("TownshipId")]
        [Bindable(true)]
        public int? TownshipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("township_id");
		    }
            set 
		    {
			    SetColumnValue("township_id", value);
            }
        }
	      
        [XmlAttribute("InvoiceTitle")]
        [Bindable(true)]
        public string InvoiceTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_title");
		    }
            set 
		    {
			    SetColumnValue("invoice_title", value);
            }
        }
	      
        [XmlAttribute("InvoiceComId")]
        [Bindable(true)]
        public string InvoiceComId 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_com_id");
		    }
            set 
		    {
			    SetColumnValue("invoice_com_id", value);
            }
        }
	      
        [XmlAttribute("InvoiceName")]
        [Bindable(true)]
        public string InvoiceName 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_name");
		    }
            set 
		    {
			    SetColumnValue("invoice_name", value);
            }
        }
	      
        [XmlAttribute("InvoiceAddress")]
        [Bindable(true)]
        public string InvoiceAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_address");
		    }
            set 
		    {
			    SetColumnValue("invoice_address", value);
            }
        }
	      
        [XmlAttribute("AccountNo")]
        [Bindable(true)]
        public string AccountNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_no");
		    }
            set 
		    {
			    SetColumnValue("account_no", value);
            }
        }
	      
        [XmlAttribute("AccountBankCode")]
        [Bindable(true)]
        public string AccountBankCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_bank_code");
		    }
            set 
		    {
			    SetColumnValue("account_bank_code", value);
            }
        }
	      
        [XmlAttribute("AccountName")]
        [Bindable(true)]
        public string AccountName 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_name");
		    }
            set 
		    {
			    SetColumnValue("account_name", value);
            }
        }
	      
        [XmlAttribute("AccountBranchCode")]
        [Bindable(true)]
        public string AccountBranchCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_branch_code");
		    }
            set 
		    {
			    SetColumnValue("account_branch_code", value);
            }
        }
	      
        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_id");
		    }
            set 
		    {
			    SetColumnValue("account_id", value);
            }
        }
	      
        [XmlAttribute("InContractWith")]
        [Bindable(true)]
        public string InContractWith 
	    {
		    get
		    {
			    return GetColumnValue<string>("in_contract_with");
		    }
            set 
		    {
			    SetColumnValue("in_contract_with", value);
            }
        }
	      
        [XmlAttribute("SellerEmail")]
        [Bindable(true)]
        public string SellerEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_email");
		    }
            set 
		    {
			    SetColumnValue("seller_email", value);
            }
        }
	      
        [XmlAttribute("SellerMobile")]
        [Bindable(true)]
        public string SellerMobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_mobile");
		    }
            set 
		    {
			    SetColumnValue("seller_mobile", value);
            }
        }
	      
        [XmlAttribute("ContractValidDate")]
        [Bindable(true)]
        public DateTime? ContractValidDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("contract_valid_date");
		    }
            set 
		    {
			    SetColumnValue("contract_valid_date", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("GroupId")]
        [Bindable(true)]
        public int GroupId 
	    {
		    get
		    {
			    return GetColumnValue<int>("group_id");
		    }
            set 
		    {
			    SetColumnValue("group_id", value);
            }
        }
	      
        [XmlAttribute("CardType")]
        [Bindable(true)]
        public int CardType 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_type");
		    }
            set 
		    {
			    SetColumnValue("card_type", value);
            }
        }
	      
        [XmlAttribute("IsPromo")]
        [Bindable(true)]
        public bool IsPromo 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_promo");
		    }
            set 
		    {
			    SetColumnValue("is_promo", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CategoryList = @"category_list";
            
            public static string UserId = @"user_id";
            
            public static string Email = @"email";
            
            public static string Mobile = @"mobile";
            
            public static string TownshipId = @"township_id";
            
            public static string InvoiceTitle = @"invoice_title";
            
            public static string InvoiceComId = @"invoice_com_id";
            
            public static string InvoiceName = @"invoice_name";
            
            public static string InvoiceAddress = @"invoice_address";
            
            public static string AccountNo = @"account_no";
            
            public static string AccountBankCode = @"account_bank_code";
            
            public static string AccountName = @"account_name";
            
            public static string AccountBranchCode = @"account_branch_code";
            
            public static string AccountId = @"account_id";
            
            public static string InContractWith = @"in_contract_with";
            
            public static string SellerEmail = @"seller_email";
            
            public static string SellerMobile = @"seller_mobile";
            
            public static string ContractValidDate = @"contract_valid_date";
            
            public static string SellerName = @"seller_name";
            
            public static string GroupId = @"group_id";
            
            public static string CardType = @"card_type";
            
            public static string IsPromo = @"is_promo";
            
            public static string SellerGuid = @"seller_guid";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

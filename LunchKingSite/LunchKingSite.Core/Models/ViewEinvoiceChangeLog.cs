using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewEinvoiceChangeLog class.
    /// </summary>
    [Serializable]
    public partial class ViewEinvoiceChangeLogCollection : ReadOnlyList<ViewEinvoiceChangeLog, ViewEinvoiceChangeLogCollection>
    {
        public ViewEinvoiceChangeLogCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_einvoice_change_log view.
    /// </summary>
    [Serializable]
    public partial class ViewEinvoiceChangeLog : ReadOnlyRecord<ViewEinvoiceChangeLog>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_einvoice_change_log", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarInvoiceNumber = new TableSchema.TableColumn(schema);
                colvarInvoiceNumber.ColumnName = "invoice_number";
                colvarInvoiceNumber.DataType = DbType.String;
                colvarInvoiceNumber.MaxLength = 10;
                colvarInvoiceNumber.AutoIncrement = false;
                colvarInvoiceNumber.IsNullable = true;
                colvarInvoiceNumber.IsPrimaryKey = false;
                colvarInvoiceNumber.IsForeignKey = false;
                colvarInvoiceNumber.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceNumber);

                TableSchema.TableColumn colvarMainId = new TableSchema.TableColumn(schema);
                colvarMainId.ColumnName = "main_id";
                colvarMainId.DataType = DbType.Int32;
                colvarMainId.MaxLength = 0;
                colvarMainId.AutoIncrement = false;
                colvarMainId.IsNullable = true;
                colvarMainId.IsPrimaryKey = false;
                colvarMainId.IsForeignKey = false;
                colvarMainId.IsReadOnly = false;

                schema.Columns.Add(colvarMainId);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;

                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarActionType = new TableSchema.TableColumn(schema);
                colvarActionType.ColumnName = "action_type";
                colvarActionType.DataType = DbType.Int32;
                colvarActionType.MaxLength = 0;
                colvarActionType.AutoIncrement = false;
                colvarActionType.IsNullable = false;
                colvarActionType.IsPrimaryKey = false;
                colvarActionType.IsForeignKey = false;
                colvarActionType.IsReadOnly = false;

                schema.Columns.Add(colvarActionType);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;

                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_einvoice_change_log", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewEinvoiceChangeLog()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEinvoiceChangeLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewEinvoiceChangeLog(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewEinvoiceChangeLog(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("InvoiceNumber")]
        [Bindable(true)]
        public string InvoiceNumber
        {
            get
            {
                return GetColumnValue<string>("invoice_number");
            }
            set
            {
                SetColumnValue("invoice_number", value);
            }
        }

        [XmlAttribute("MainId")]
        [Bindable(true)]
        public int? MainId
        {
            get
            {
                return GetColumnValue<int?>("main_id");
            }
            set
            {
                SetColumnValue("main_id", value);
            }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid
        {
            get
            {
                return GetColumnValue<Guid>("order_guid");
            }
            set
            {
                SetColumnValue("order_guid", value);
            }
        }

        [XmlAttribute("ActionType")]
        [Bindable(true)]
        public int ActionType
        {
            get
            {
                return GetColumnValue<int>("action_type");
            }
            set
            {
                SetColumnValue("action_type", value);
            }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get
            {
                return GetColumnValue<string>("create_id");
            }
            set
            {
                SetColumnValue("create_id", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string InvoiceNumber = @"invoice_number";

            public static string MainId = @"main_id";

            public static string OrderGuid = @"order_guid";

            public static string ActionType = @"action_type";

            public static string CreateId = @"create_id";

            public static string CreateTime = @"create_time";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

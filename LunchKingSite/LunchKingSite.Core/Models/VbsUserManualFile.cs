using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VbsUserManualFile class.
	/// </summary>
    [Serializable]
	public partial class VbsUserManualFileCollection : RepositoryList<VbsUserManualFile, VbsUserManualFileCollection>
	{	   
		public VbsUserManualFileCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VbsUserManualFileCollection</returns>
		public VbsUserManualFileCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VbsUserManualFile o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the vbs_user_manual_file table.
	/// </summary>
	[Serializable]
	public partial class VbsUserManualFile : RepositoryRecord<VbsUserManualFile>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VbsUserManualFile()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VbsUserManualFile(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("vbs_user_manual_file", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
				colvarCategoryId.ColumnName = "category_id";
				colvarCategoryId.DataType = DbType.Int32;
				colvarCategoryId.MaxLength = 0;
				colvarCategoryId.AutoIncrement = false;
				colvarCategoryId.IsNullable = false;
				colvarCategoryId.IsPrimaryKey = false;
				colvarCategoryId.IsForeignKey = false;
				colvarCategoryId.IsReadOnly = false;
				colvarCategoryId.DefaultSetting = @"";
				colvarCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryId);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = false;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				colvarSeq.DefaultSetting = @"";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 50;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = false;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);
				
				TableSchema.TableColumn colvarReference = new TableSchema.TableColumn(schema);
				colvarReference.ColumnName = "reference";
				colvarReference.DataType = DbType.String;
				colvarReference.MaxLength = 50;
				colvarReference.AutoIncrement = false;
				colvarReference.IsNullable = false;
				colvarReference.IsPrimaryKey = false;
				colvarReference.IsForeignKey = false;
				colvarReference.IsReadOnly = false;
				colvarReference.DefaultSetting = @"";
				colvarReference.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReference);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = 100;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarIsDisabled = new TableSchema.TableColumn(schema);
				colvarIsDisabled.ColumnName = "is_disabled";
				colvarIsDisabled.DataType = DbType.Boolean;
				colvarIsDisabled.MaxLength = 0;
				colvarIsDisabled.AutoIncrement = false;
				colvarIsDisabled.IsNullable = false;
				colvarIsDisabled.IsPrimaryKey = false;
				colvarIsDisabled.IsForeignKey = false;
				colvarIsDisabled.IsReadOnly = false;
				
						colvarIsDisabled.DefaultSetting = @"((0))";
				colvarIsDisabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDisabled);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("vbs_user_manual_file",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CategoryId")]
		[Bindable(true)]
		public int CategoryId 
		{
			get { return GetColumnValue<int>(Columns.CategoryId); }
			set { SetColumnValue(Columns.CategoryId, value); }
		}
		  
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int Seq 
		{
			get { return GetColumnValue<int>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		  
		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name 
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}
		  
		[XmlAttribute("Reference")]
		[Bindable(true)]
		public string Reference 
		{
			get { return GetColumnValue<string>(Columns.Reference); }
			set { SetColumnValue(Columns.Reference, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("IsDisabled")]
		[Bindable(true)]
		public bool IsDisabled 
		{
			get { return GetColumnValue<bool>(Columns.IsDisabled); }
			set { SetColumnValue(Columns.IsDisabled, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferenceColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDisabledColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CategoryId = @"category_id";
			 public static string Seq = @"seq";
			 public static string Name = @"name";
			 public static string Reference = @"reference";
			 public static string Description = @"description";
			 public static string IsDisabled = @"is_disabled";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

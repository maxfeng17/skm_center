using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpUserPointAlterPair class.
	/// </summary>
    [Serializable]
	public partial class PcpUserPointAlterPairCollection : RepositoryList<PcpUserPointAlterPair, PcpUserPointAlterPairCollection>
	{	   
		public PcpUserPointAlterPairCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpUserPointAlterPairCollection</returns>
		public PcpUserPointAlterPairCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpUserPointAlterPair o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_user_point_alter_pair table.
	/// </summary>
	[Serializable]
	public partial class PcpUserPointAlterPair : RepositoryRecord<PcpUserPointAlterPair>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpUserPointAlterPair()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpUserPointAlterPair(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_user_point_alter_pair", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarUserPointIdNegative = new TableSchema.TableColumn(schema);
				colvarUserPointIdNegative.ColumnName = "user_point_id_negative";
				colvarUserPointIdNegative.DataType = DbType.Int32;
				colvarUserPointIdNegative.MaxLength = 0;
				colvarUserPointIdNegative.AutoIncrement = false;
				colvarUserPointIdNegative.IsNullable = false;
				colvarUserPointIdNegative.IsPrimaryKey = false;
				colvarUserPointIdNegative.IsForeignKey = false;
				colvarUserPointIdNegative.IsReadOnly = false;
				colvarUserPointIdNegative.DefaultSetting = @"";
				colvarUserPointIdNegative.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserPointIdNegative);
				
				TableSchema.TableColumn colvarUserPointIdPositive = new TableSchema.TableColumn(schema);
				colvarUserPointIdPositive.ColumnName = "user_point_id_positive";
				colvarUserPointIdPositive.DataType = DbType.Int32;
				colvarUserPointIdPositive.MaxLength = 0;
				colvarUserPointIdPositive.AutoIncrement = false;
				colvarUserPointIdPositive.IsNullable = false;
				colvarUserPointIdPositive.IsPrimaryKey = false;
				colvarUserPointIdPositive.IsForeignKey = false;
				colvarUserPointIdPositive.IsReadOnly = false;
				colvarUserPointIdPositive.DefaultSetting = @"";
				colvarUserPointIdPositive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserPointIdPositive);
				
				TableSchema.TableColumn colvarPointConsume = new TableSchema.TableColumn(schema);
				colvarPointConsume.ColumnName = "point_consume";
				colvarPointConsume.DataType = DbType.Int32;
				colvarPointConsume.MaxLength = 0;
				colvarPointConsume.AutoIncrement = false;
				colvarPointConsume.IsNullable = false;
				colvarPointConsume.IsPrimaryKey = false;
				colvarPointConsume.IsForeignKey = false;
				colvarPointConsume.IsReadOnly = false;
				colvarPointConsume.DefaultSetting = @"";
				colvarPointConsume.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPointConsume);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_user_point_alter_pair",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("UserPointIdNegative")]
		[Bindable(true)]
		public int UserPointIdNegative 
		{
			get { return GetColumnValue<int>(Columns.UserPointIdNegative); }
			set { SetColumnValue(Columns.UserPointIdNegative, value); }
		}
		  
		[XmlAttribute("UserPointIdPositive")]
		[Bindable(true)]
		public int UserPointIdPositive 
		{
			get { return GetColumnValue<int>(Columns.UserPointIdPositive); }
			set { SetColumnValue(Columns.UserPointIdPositive, value); }
		}
		  
		[XmlAttribute("PointConsume")]
		[Bindable(true)]
		public int PointConsume 
		{
			get { return GetColumnValue<int>(Columns.PointConsume); }
			set { SetColumnValue(Columns.PointConsume, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserPointIdNegativeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UserPointIdPositiveColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PointConsumeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string UserPointIdNegative = @"user_point_id_negative";
			 public static string UserPointIdPositive = @"user_point_id_positive";
			 public static string PointConsume = @"point_consume";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

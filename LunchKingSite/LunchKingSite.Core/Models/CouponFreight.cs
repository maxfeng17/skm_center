using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the CouponFreight class.
    /// </summary>
    [Serializable]
    public partial class CouponFreightCollection : RepositoryList<CouponFreight, CouponFreightCollection>
    {
        public CouponFreightCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CouponFreightCollection</returns>
        public CouponFreightCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CouponFreight o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the coupon_freight table.
    /// </summary>
    [Serializable]
    public partial class CouponFreight : RepositoryRecord<CouponFreight>, IRecordBase
    {
        #region .ctors and Default Settings

        public CouponFreight()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public CouponFreight(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("coupon_freight", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = true;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";

                colvarBusinessHourGuid.ForeignKeyTableName = "business_hour";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarStartAmount = new TableSchema.TableColumn(schema);
                colvarStartAmount.ColumnName = "start_amount";
                colvarStartAmount.DataType = DbType.Currency;
                colvarStartAmount.MaxLength = 0;
                colvarStartAmount.AutoIncrement = false;
                colvarStartAmount.IsNullable = false;
                colvarStartAmount.IsPrimaryKey = false;
                colvarStartAmount.IsForeignKey = false;
                colvarStartAmount.IsReadOnly = false;
                colvarStartAmount.DefaultSetting = @"";
                colvarStartAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStartAmount);

                TableSchema.TableColumn colvarEndAmount = new TableSchema.TableColumn(schema);
                colvarEndAmount.ColumnName = "end_amount";
                colvarEndAmount.DataType = DbType.Currency;
                colvarEndAmount.MaxLength = 0;
                colvarEndAmount.AutoIncrement = false;
                colvarEndAmount.IsNullable = false;
                colvarEndAmount.IsPrimaryKey = false;
                colvarEndAmount.IsForeignKey = false;
                colvarEndAmount.IsReadOnly = false;
                colvarEndAmount.DefaultSetting = @"";
                colvarEndAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEndAmount);

                TableSchema.TableColumn colvarFreightAmount = new TableSchema.TableColumn(schema);
                colvarFreightAmount.ColumnName = "freight_amount";
                colvarFreightAmount.DataType = DbType.Currency;
                colvarFreightAmount.MaxLength = 0;
                colvarFreightAmount.AutoIncrement = false;
                colvarFreightAmount.IsNullable = false;
                colvarFreightAmount.IsPrimaryKey = false;
                colvarFreightAmount.IsForeignKey = false;
                colvarFreightAmount.IsReadOnly = false;
                colvarFreightAmount.DefaultSetting = @"";
                colvarFreightAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFreightAmount);

                TableSchema.TableColumn colvarFreightType = new TableSchema.TableColumn(schema);
                colvarFreightType.ColumnName = "freight_type";
                colvarFreightType.DataType = DbType.Int32;
                colvarFreightType.MaxLength = 0;
                colvarFreightType.AutoIncrement = false;
                colvarFreightType.IsNullable = false;
                colvarFreightType.IsPrimaryKey = false;
                colvarFreightType.IsForeignKey = false;
                colvarFreightType.IsReadOnly = false;
                colvarFreightType.DefaultSetting = @"";
                colvarFreightType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFreightType);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarPayTo = new TableSchema.TableColumn(schema);
                colvarPayTo.ColumnName = "pay_to";
                colvarPayTo.DataType = DbType.Int32;
                colvarPayTo.MaxLength = 0;
                colvarPayTo.AutoIncrement = false;
                colvarPayTo.IsNullable = true;
                colvarPayTo.IsPrimaryKey = false;
                colvarPayTo.IsForeignKey = false;
                colvarPayTo.IsReadOnly = false;
                colvarPayTo.DefaultSetting = @"";
                colvarPayTo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPayTo);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("coupon_freight", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid
        {
            get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        [XmlAttribute("StartAmount")]
        [Bindable(true)]
        public decimal StartAmount
        {
            get { return GetColumnValue<decimal>(Columns.StartAmount); }
            set { SetColumnValue(Columns.StartAmount, value); }
        }

        [XmlAttribute("EndAmount")]
        [Bindable(true)]
        public decimal EndAmount
        {
            get { return GetColumnValue<decimal>(Columns.EndAmount); }
            set { SetColumnValue(Columns.EndAmount, value); }
        }

        [XmlAttribute("FreightAmount")]
        [Bindable(true)]
        public decimal FreightAmount
        {
            get { return GetColumnValue<decimal>(Columns.FreightAmount); }
            set { SetColumnValue(Columns.FreightAmount, value); }
        }

        [XmlAttribute("FreightType")]
        [Bindable(true)]
        public int FreightType
        {
            get { return GetColumnValue<int>(Columns.FreightType); }
            set { SetColumnValue(Columns.FreightType, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("PayTo")]
        [Bindable(true)]
        public int? PayTo
        {
            get { return GetColumnValue<int?>(Columns.PayTo); }
            set { SetColumnValue(Columns.PayTo, value); }
        }

        #endregion




        //no foreign key tables defined (1)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn StartAmountColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn EndAmountColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn FreightAmountColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn FreightTypeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn PayToColumn
        {
            get { return Schema.Columns[8]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"guid";
            public static string BusinessHourGuid = @"business_hour_guid";
            public static string StartAmount = @"start_amount";
            public static string EndAmount = @"end_amount";
            public static string FreightAmount = @"freight_amount";
            public static string FreightType = @"freight_type";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string PayTo = @"pay_to";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

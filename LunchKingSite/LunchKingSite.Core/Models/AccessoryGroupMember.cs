using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the AccessoryGroupMember class.
	/// </summary>
    [Serializable]
	public partial class AccessoryGroupMemberCollection : RepositoryList<AccessoryGroupMember, AccessoryGroupMemberCollection>
	{	   
		public AccessoryGroupMemberCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>AccessoryGroupMemberCollection</returns>
		public AccessoryGroupMemberCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                AccessoryGroupMember o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the accessory_group_member table.
	/// </summary>
	[Serializable]
	public partial class AccessoryGroupMember : RepositoryRecord<AccessoryGroupMember>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public AccessoryGroupMember()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public AccessoryGroupMember(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("accessory_group_member", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarAccessoryGroupGuid = new TableSchema.TableColumn(schema);
				colvarAccessoryGroupGuid.ColumnName = "accessory_group_GUID";
				colvarAccessoryGroupGuid.DataType = DbType.Guid;
				colvarAccessoryGroupGuid.MaxLength = 0;
				colvarAccessoryGroupGuid.AutoIncrement = false;
				colvarAccessoryGroupGuid.IsNullable = false;
				colvarAccessoryGroupGuid.IsPrimaryKey = false;
				colvarAccessoryGroupGuid.IsForeignKey = true;
				colvarAccessoryGroupGuid.IsReadOnly = false;
				colvarAccessoryGroupGuid.DefaultSetting = @"";
				
					colvarAccessoryGroupGuid.ForeignKeyTableName = "accessory_group";
				schema.Columns.Add(colvarAccessoryGroupGuid);
				
				TableSchema.TableColumn colvarAccessoryGuid = new TableSchema.TableColumn(schema);
				colvarAccessoryGuid.ColumnName = "accessory_GUID";
				colvarAccessoryGuid.DataType = DbType.Guid;
				colvarAccessoryGuid.MaxLength = 0;
				colvarAccessoryGuid.AutoIncrement = false;
				colvarAccessoryGuid.IsNullable = false;
				colvarAccessoryGuid.IsPrimaryKey = false;
				colvarAccessoryGuid.IsForeignKey = true;
				colvarAccessoryGuid.IsReadOnly = false;
				colvarAccessoryGuid.DefaultSetting = @"";
				
					colvarAccessoryGuid.ForeignKeyTableName = "accessory";
				schema.Columns.Add(colvarAccessoryGuid);
				
				TableSchema.TableColumn colvarAccessoryGroupMemberId = new TableSchema.TableColumn(schema);
				colvarAccessoryGroupMemberId.ColumnName = "accessory_group_member_id";
				colvarAccessoryGroupMemberId.DataType = DbType.AnsiString;
				colvarAccessoryGroupMemberId.MaxLength = 20;
				colvarAccessoryGroupMemberId.AutoIncrement = false;
				colvarAccessoryGroupMemberId.IsNullable = true;
				colvarAccessoryGroupMemberId.IsPrimaryKey = false;
				colvarAccessoryGroupMemberId.IsForeignKey = false;
				colvarAccessoryGroupMemberId.IsReadOnly = false;
				colvarAccessoryGroupMemberId.DefaultSetting = @"";
				colvarAccessoryGroupMemberId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessoryGroupMemberId);
				
				TableSchema.TableColumn colvarAccessoryGroupMemberPrice = new TableSchema.TableColumn(schema);
				colvarAccessoryGroupMemberPrice.ColumnName = "accessory_group_member_price";
				colvarAccessoryGroupMemberPrice.DataType = DbType.Currency;
				colvarAccessoryGroupMemberPrice.MaxLength = 0;
				colvarAccessoryGroupMemberPrice.AutoIncrement = false;
				colvarAccessoryGroupMemberPrice.IsNullable = false;
				colvarAccessoryGroupMemberPrice.IsPrimaryKey = false;
				colvarAccessoryGroupMemberPrice.IsForeignKey = false;
				colvarAccessoryGroupMemberPrice.IsReadOnly = false;
				colvarAccessoryGroupMemberPrice.DefaultSetting = @"";
				colvarAccessoryGroupMemberPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessoryGroupMemberPrice);
				
				TableSchema.TableColumn colvarAccessoryGroupMemberNote = new TableSchema.TableColumn(schema);
				colvarAccessoryGroupMemberNote.ColumnName = "accessory_group_member_note";
				colvarAccessoryGroupMemberNote.DataType = DbType.String;
				colvarAccessoryGroupMemberNote.MaxLength = 50;
				colvarAccessoryGroupMemberNote.AutoIncrement = false;
				colvarAccessoryGroupMemberNote.IsNullable = true;
				colvarAccessoryGroupMemberNote.IsPrimaryKey = false;
				colvarAccessoryGroupMemberNote.IsForeignKey = false;
				colvarAccessoryGroupMemberNote.IsReadOnly = false;
				colvarAccessoryGroupMemberNote.DefaultSetting = @"";
				colvarAccessoryGroupMemberNote.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessoryGroupMemberNote);
				
				TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
				colvarSequence.ColumnName = "sequence";
				colvarSequence.DataType = DbType.Int32;
				colvarSequence.MaxLength = 0;
				colvarSequence.AutoIncrement = false;
				colvarSequence.IsNullable = true;
				colvarSequence.IsPrimaryKey = false;
				colvarSequence.IsForeignKey = false;
				colvarSequence.IsReadOnly = false;
				colvarSequence.DefaultSetting = @"";
				colvarSequence.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSequence);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 30;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
				colvarQuantity.ColumnName = "quantity";
				colvarQuantity.DataType = DbType.Int32;
				colvarQuantity.MaxLength = 0;
				colvarQuantity.AutoIncrement = false;
				colvarQuantity.IsNullable = true;
				colvarQuantity.IsPrimaryKey = false;
				colvarQuantity.IsForeignKey = false;
				colvarQuantity.IsReadOnly = false;
				colvarQuantity.DefaultSetting = @"";
				colvarQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQuantity);
				
				TableSchema.TableColumn colvarPponBid = new TableSchema.TableColumn(schema);
				colvarPponBid.ColumnName = "ppon_bid";
				colvarPponBid.DataType = DbType.Guid;
				colvarPponBid.MaxLength = 0;
				colvarPponBid.AutoIncrement = false;
				colvarPponBid.IsNullable = true;
				colvarPponBid.IsPrimaryKey = false;
				colvarPponBid.IsForeignKey = false;
				colvarPponBid.IsReadOnly = false;
				colvarPponBid.DefaultSetting = @"";
				colvarPponBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPponBid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("accessory_group_member",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("AccessoryGroupGuid")]
		[Bindable(true)]
		public Guid AccessoryGroupGuid 
		{
			get { return GetColumnValue<Guid>(Columns.AccessoryGroupGuid); }
			set { SetColumnValue(Columns.AccessoryGroupGuid, value); }
		}
		  
		[XmlAttribute("AccessoryGuid")]
		[Bindable(true)]
		public Guid AccessoryGuid 
		{
			get { return GetColumnValue<Guid>(Columns.AccessoryGuid); }
			set { SetColumnValue(Columns.AccessoryGuid, value); }
		}
		  
		[XmlAttribute("AccessoryGroupMemberId")]
		[Bindable(true)]
		public string AccessoryGroupMemberId 
		{
			get { return GetColumnValue<string>(Columns.AccessoryGroupMemberId); }
			set { SetColumnValue(Columns.AccessoryGroupMemberId, value); }
		}
		  
		[XmlAttribute("AccessoryGroupMemberPrice")]
		[Bindable(true)]
		public decimal AccessoryGroupMemberPrice 
		{
			get { return GetColumnValue<decimal>(Columns.AccessoryGroupMemberPrice); }
			set { SetColumnValue(Columns.AccessoryGroupMemberPrice, value); }
		}
		  
		[XmlAttribute("AccessoryGroupMemberNote")]
		[Bindable(true)]
		public string AccessoryGroupMemberNote 
		{
			get { return GetColumnValue<string>(Columns.AccessoryGroupMemberNote); }
			set { SetColumnValue(Columns.AccessoryGroupMemberNote, value); }
		}
		  
		[XmlAttribute("Sequence")]
		[Bindable(true)]
		public int? Sequence 
		{
			get { return GetColumnValue<int?>(Columns.Sequence); }
			set { SetColumnValue(Columns.Sequence, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("Quantity")]
		[Bindable(true)]
		public int? Quantity 
		{
			get { return GetColumnValue<int?>(Columns.Quantity); }
			set { SetColumnValue(Columns.Quantity, value); }
		}
		  
		[XmlAttribute("PponBid")]
		[Bindable(true)]
		public Guid? PponBid 
		{
			get { return GetColumnValue<Guid?>(Columns.PponBid); }
			set { SetColumnValue(Columns.PponBid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessoryGroupGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessoryGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessoryGroupMemberIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessoryGroupMemberPriceColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessoryGroupMemberNoteColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn QuantityColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn PponBidColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string AccessoryGroupGuid = @"accessory_group_GUID";
			 public static string AccessoryGuid = @"accessory_GUID";
			 public static string AccessoryGroupMemberId = @"accessory_group_member_id";
			 public static string AccessoryGroupMemberPrice = @"accessory_group_member_price";
			 public static string AccessoryGroupMemberNote = @"accessory_group_member_note";
			 public static string Sequence = @"sequence";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string Quantity = @"quantity";
			 public static string PponBid = @"ppon_bid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ChannelClientPropertyCollection : RepositoryList<ChannelClientProperty, ChannelClientPropertyCollection>
	{
			public ChannelClientPropertyCollection() {}

			public ChannelClientPropertyCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							ChannelClientProperty o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class ChannelClientProperty : RepositoryRecord<ChannelClientProperty>, IRecordBase
	{
		#region .ctors and Default Settings
		public ChannelClientProperty()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ChannelClientProperty(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("channel_client_property", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarAppId = new TableSchema.TableColumn(schema);
				colvarAppId.ColumnName = "app_id";
				colvarAppId.DataType = DbType.String;
				colvarAppId.MaxLength = 100;
				colvarAppId.AutoIncrement = false;
				colvarAppId.IsNullable = true;
				colvarAppId.IsPrimaryKey = false;
				colvarAppId.IsForeignKey = false;
				colvarAppId.IsReadOnly = false;
				colvarAppId.DefaultSetting = @"";
				colvarAppId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppId);

				TableSchema.TableColumn colvarMinGrossMargin = new TableSchema.TableColumn(schema);
				colvarMinGrossMargin.ColumnName = "min_gross_margin";
				colvarMinGrossMargin.DataType = DbType.Decimal;
				colvarMinGrossMargin.MaxLength = 0;
				colvarMinGrossMargin.AutoIncrement = false;
				colvarMinGrossMargin.IsNullable = true;
				colvarMinGrossMargin.IsPrimaryKey = false;
				colvarMinGrossMargin.IsForeignKey = false;
				colvarMinGrossMargin.IsReadOnly = false;
				colvarMinGrossMargin.DefaultSetting = @"";
				colvarMinGrossMargin.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMinGrossMargin);

				TableSchema.TableColumn colvarGeneralGrossMargin = new TableSchema.TableColumn(schema);
				colvarGeneralGrossMargin.ColumnName = "general_gross_margin";
				colvarGeneralGrossMargin.DataType = DbType.Decimal;
				colvarGeneralGrossMargin.MaxLength = 0;
				colvarGeneralGrossMargin.AutoIncrement = false;
				colvarGeneralGrossMargin.IsNullable = true;
				colvarGeneralGrossMargin.IsPrimaryKey = false;
				colvarGeneralGrossMargin.IsForeignKey = false;
				colvarGeneralGrossMargin.IsReadOnly = false;
				colvarGeneralGrossMargin.DefaultSetting = @"";
				colvarGeneralGrossMargin.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGeneralGrossMargin);

				TableSchema.TableColumn colvarMinCommission = new TableSchema.TableColumn(schema);
				colvarMinCommission.ColumnName = "min_commission";
				colvarMinCommission.DataType = DbType.Decimal;
				colvarMinCommission.MaxLength = 0;
				colvarMinCommission.AutoIncrement = false;
				colvarMinCommission.IsNullable = true;
				colvarMinCommission.IsPrimaryKey = false;
				colvarMinCommission.IsForeignKey = false;
				colvarMinCommission.IsReadOnly = false;
				colvarMinCommission.DefaultSetting = @"";
				colvarMinCommission.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMinCommission);

				TableSchema.TableColumn colvarGeneralCommission = new TableSchema.TableColumn(schema);
				colvarGeneralCommission.ColumnName = "general_commission";
				colvarGeneralCommission.DataType = DbType.Decimal;
				colvarGeneralCommission.MaxLength = 0;
				colvarGeneralCommission.AutoIncrement = false;
				colvarGeneralCommission.IsNullable = true;
				colvarGeneralCommission.IsPrimaryKey = false;
				colvarGeneralCommission.IsForeignKey = false;
				colvarGeneralCommission.IsReadOnly = false;
				colvarGeneralCommission.DefaultSetting = @"";
				colvarGeneralCommission.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGeneralCommission);

				TableSchema.TableColumn colvarCheckoutDay = new TableSchema.TableColumn(schema);
				colvarCheckoutDay.ColumnName = "checkout_day";
				colvarCheckoutDay.DataType = DbType.AnsiString;
				colvarCheckoutDay.MaxLength = 50;
				colvarCheckoutDay.AutoIncrement = false;
				colvarCheckoutDay.IsNullable = true;
				colvarCheckoutDay.IsPrimaryKey = false;
				colvarCheckoutDay.IsForeignKey = false;
				colvarCheckoutDay.IsReadOnly = false;
				colvarCheckoutDay.DefaultSetting = @"";
				colvarCheckoutDay.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCheckoutDay);

				TableSchema.TableColumn colvarCheckType = new TableSchema.TableColumn(schema);
				colvarCheckType.ColumnName = "check_type";
				colvarCheckType.DataType = DbType.Int32;
				colvarCheckType.MaxLength = 0;
				colvarCheckType.AutoIncrement = false;
				colvarCheckType.IsNullable = true;
				colvarCheckType.IsPrimaryKey = false;
				colvarCheckType.IsForeignKey = false;
				colvarCheckType.IsReadOnly = false;
				colvarCheckType.DefaultSetting = @"";
				colvarCheckType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCheckType);

				TableSchema.TableColumn colvarCheckCycle = new TableSchema.TableColumn(schema);
				colvarCheckCycle.ColumnName = "check_cycle";
				colvarCheckCycle.DataType = DbType.AnsiString;
				colvarCheckCycle.MaxLength = 50;
				colvarCheckCycle.AutoIncrement = false;
				colvarCheckCycle.IsNullable = true;
				colvarCheckCycle.IsPrimaryKey = false;
				colvarCheckCycle.IsForeignKey = false;
				colvarCheckCycle.IsReadOnly = false;
				colvarCheckCycle.DefaultSetting = @"";
				colvarCheckCycle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCheckCycle);

				TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
				colvarSellerUserId.ColumnName = "seller_user_id";
				colvarSellerUserId.DataType = DbType.Int32;
				colvarSellerUserId.MaxLength = 0;
				colvarSellerUserId.AutoIncrement = false;
				colvarSellerUserId.IsNullable = true;
				colvarSellerUserId.IsPrimaryKey = false;
				colvarSellerUserId.IsForeignKey = false;
				colvarSellerUserId.IsReadOnly = false;
				colvarSellerUserId.DefaultSetting = @"";
				colvarSellerUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerUserId);

				TableSchema.TableColumn colvarGeneralCouponGrossMargin = new TableSchema.TableColumn(schema);
				colvarGeneralCouponGrossMargin.ColumnName = "general_coupon_gross_margin";
				colvarGeneralCouponGrossMargin.DataType = DbType.Decimal;
				colvarGeneralCouponGrossMargin.MaxLength = 0;
				colvarGeneralCouponGrossMargin.AutoIncrement = false;
				colvarGeneralCouponGrossMargin.IsNullable = true;
				colvarGeneralCouponGrossMargin.IsPrimaryKey = false;
				colvarGeneralCouponGrossMargin.IsForeignKey = false;
				colvarGeneralCouponGrossMargin.IsReadOnly = false;
				colvarGeneralCouponGrossMargin.DefaultSetting = @"";
				colvarGeneralCouponGrossMargin.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGeneralCouponGrossMargin);

				TableSchema.TableColumn colvarGeneralDeliveryGrossMargin = new TableSchema.TableColumn(schema);
				colvarGeneralDeliveryGrossMargin.ColumnName = "general_delivery_gross_margin";
				colvarGeneralDeliveryGrossMargin.DataType = DbType.Decimal;
				colvarGeneralDeliveryGrossMargin.MaxLength = 0;
				colvarGeneralDeliveryGrossMargin.AutoIncrement = false;
				colvarGeneralDeliveryGrossMargin.IsNullable = true;
				colvarGeneralDeliveryGrossMargin.IsPrimaryKey = false;
				colvarGeneralDeliveryGrossMargin.IsForeignKey = false;
				colvarGeneralDeliveryGrossMargin.IsReadOnly = false;
				colvarGeneralDeliveryGrossMargin.DefaultSetting = @"";
				colvarGeneralDeliveryGrossMargin.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGeneralDeliveryGrossMargin);

				TableSchema.TableColumn colvarCpaCode = new TableSchema.TableColumn(schema);
				colvarCpaCode.ColumnName = "cpa_code";
				colvarCpaCode.DataType = DbType.AnsiString;
				colvarCpaCode.MaxLength = 50;
				colvarCpaCode.AutoIncrement = false;
				colvarCpaCode.IsNullable = true;
				colvarCpaCode.IsPrimaryKey = false;
				colvarCpaCode.IsForeignKey = false;
				colvarCpaCode.IsReadOnly = false;
				colvarCpaCode.DefaultSetting = @"";
				colvarCpaCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCpaCode);

				TableSchema.TableColumn colvarUsingTokenToGuestId = new TableSchema.TableColumn(schema);
				colvarUsingTokenToGuestId.ColumnName = "using_token_to_guest_id";
				colvarUsingTokenToGuestId.DataType = DbType.Boolean;
				colvarUsingTokenToGuestId.MaxLength = 0;
				colvarUsingTokenToGuestId.AutoIncrement = false;
				colvarUsingTokenToGuestId.IsNullable = false;
				colvarUsingTokenToGuestId.IsPrimaryKey = false;
				colvarUsingTokenToGuestId.IsForeignKey = false;
				colvarUsingTokenToGuestId.IsReadOnly = false;
				colvarUsingTokenToGuestId.DefaultSetting = @"((0))";
				colvarUsingTokenToGuestId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUsingTokenToGuestId);

				TableSchema.TableColumn colvarGroupCoupon = new TableSchema.TableColumn(schema);
				colvarGroupCoupon.ColumnName = "group_coupon";
				colvarGroupCoupon.DataType = DbType.Boolean;
				colvarGroupCoupon.MaxLength = 0;
				colvarGroupCoupon.AutoIncrement = false;
				colvarGroupCoupon.IsNullable = false;
				colvarGroupCoupon.IsPrimaryKey = false;
				colvarGroupCoupon.IsForeignKey = false;
				colvarGroupCoupon.IsReadOnly = false;
				colvarGroupCoupon.DefaultSetting = @"((0))";
				colvarGroupCoupon.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupCoupon);

				TableSchema.TableColumn colvarPezGroupCoupon = new TableSchema.TableColumn(schema);
				colvarPezGroupCoupon.ColumnName = "pez_group_coupon";
				colvarPezGroupCoupon.DataType = DbType.Boolean;
				colvarPezGroupCoupon.MaxLength = 0;
				colvarPezGroupCoupon.AutoIncrement = false;
				colvarPezGroupCoupon.IsNullable = false;
				colvarPezGroupCoupon.IsPrimaryKey = false;
				colvarPezGroupCoupon.IsForeignKey = false;
				colvarPezGroupCoupon.IsReadOnly = false;
				colvarPezGroupCoupon.DefaultSetting = @"((0))";
				colvarPezGroupCoupon.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPezGroupCoupon);

				TableSchema.TableColumn colvarFamiGroupCoupon = new TableSchema.TableColumn(schema);
				colvarFamiGroupCoupon.ColumnName = "fami_group_coupon";
				colvarFamiGroupCoupon.DataType = DbType.Boolean;
				colvarFamiGroupCoupon.MaxLength = 0;
				colvarFamiGroupCoupon.AutoIncrement = false;
				colvarFamiGroupCoupon.IsNullable = false;
				colvarFamiGroupCoupon.IsPrimaryKey = false;
				colvarFamiGroupCoupon.IsForeignKey = false;
				colvarFamiGroupCoupon.IsReadOnly = false;
				colvarFamiGroupCoupon.DefaultSetting = @"((0))";
				colvarFamiGroupCoupon.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamiGroupCoupon);

				TableSchema.TableColumn colvarHiLifeGroupCoupon = new TableSchema.TableColumn(schema);
				colvarHiLifeGroupCoupon.ColumnName = "hi_life_group_coupon";
				colvarHiLifeGroupCoupon.DataType = DbType.Boolean;
				colvarHiLifeGroupCoupon.MaxLength = 0;
				colvarHiLifeGroupCoupon.AutoIncrement = false;
				colvarHiLifeGroupCoupon.IsNullable = false;
				colvarHiLifeGroupCoupon.IsPrimaryKey = false;
				colvarHiLifeGroupCoupon.IsForeignKey = false;
				colvarHiLifeGroupCoupon.IsReadOnly = false;
				colvarHiLifeGroupCoupon.DefaultSetting = @"((0))";
				colvarHiLifeGroupCoupon.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHiLifeGroupCoupon);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("channel_client_property",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("AppId")]
		[Bindable(true)]
		public string AppId
		{
			get { return GetColumnValue<string>(Columns.AppId); }
			set { SetColumnValue(Columns.AppId, value); }
		}

		[XmlAttribute("MinGrossMargin")]
		[Bindable(true)]
		public decimal? MinGrossMargin
		{
			get { return GetColumnValue<decimal?>(Columns.MinGrossMargin); }
			set { SetColumnValue(Columns.MinGrossMargin, value); }
		}

		[XmlAttribute("GeneralGrossMargin")]
		[Bindable(true)]
		public decimal? GeneralGrossMargin
		{
			get { return GetColumnValue<decimal?>(Columns.GeneralGrossMargin); }
			set { SetColumnValue(Columns.GeneralGrossMargin, value); }
		}

		[XmlAttribute("MinCommission")]
		[Bindable(true)]
		public decimal? MinCommission
		{
			get { return GetColumnValue<decimal?>(Columns.MinCommission); }
			set { SetColumnValue(Columns.MinCommission, value); }
		}

		[XmlAttribute("GeneralCommission")]
		[Bindable(true)]
		public decimal? GeneralCommission
		{
			get { return GetColumnValue<decimal?>(Columns.GeneralCommission); }
			set { SetColumnValue(Columns.GeneralCommission, value); }
		}

		[XmlAttribute("CheckoutDay")]
		[Bindable(true)]
		public string CheckoutDay
		{
			get { return GetColumnValue<string>(Columns.CheckoutDay); }
			set { SetColumnValue(Columns.CheckoutDay, value); }
		}

		[XmlAttribute("CheckType")]
		[Bindable(true)]
		public int? CheckType
		{
			get { return GetColumnValue<int?>(Columns.CheckType); }
			set { SetColumnValue(Columns.CheckType, value); }
		}

		[XmlAttribute("CheckCycle")]
		[Bindable(true)]
		public string CheckCycle
		{
			get { return GetColumnValue<string>(Columns.CheckCycle); }
			set { SetColumnValue(Columns.CheckCycle, value); }
		}

		[XmlAttribute("SellerUserId")]
		[Bindable(true)]
		public int? SellerUserId
		{
			get { return GetColumnValue<int?>(Columns.SellerUserId); }
			set { SetColumnValue(Columns.SellerUserId, value); }
		}

		[XmlAttribute("GeneralCouponGrossMargin")]
		[Bindable(true)]
		public decimal? GeneralCouponGrossMargin
		{
			get { return GetColumnValue<decimal?>(Columns.GeneralCouponGrossMargin); }
			set { SetColumnValue(Columns.GeneralCouponGrossMargin, value); }
		}

		[XmlAttribute("GeneralDeliveryGrossMargin")]
		[Bindable(true)]
		public decimal? GeneralDeliveryGrossMargin
		{
			get { return GetColumnValue<decimal?>(Columns.GeneralDeliveryGrossMargin); }
			set { SetColumnValue(Columns.GeneralDeliveryGrossMargin, value); }
		}

		[XmlAttribute("CpaCode")]
		[Bindable(true)]
		public string CpaCode
		{
			get { return GetColumnValue<string>(Columns.CpaCode); }
			set { SetColumnValue(Columns.CpaCode, value); }
		}

		[XmlAttribute("UsingTokenToGuestId")]
		[Bindable(true)]
		public bool UsingTokenToGuestId
		{
			get { return GetColumnValue<bool>(Columns.UsingTokenToGuestId); }
			set { SetColumnValue(Columns.UsingTokenToGuestId, value); }
		}

		[XmlAttribute("GroupCoupon")]
		[Bindable(true)]
		public bool GroupCoupon
		{
			get { return GetColumnValue<bool>(Columns.GroupCoupon); }
			set { SetColumnValue(Columns.GroupCoupon, value); }
		}

		[XmlAttribute("PezGroupCoupon")]
		[Bindable(true)]
		public bool PezGroupCoupon
		{
			get { return GetColumnValue<bool>(Columns.PezGroupCoupon); }
			set { SetColumnValue(Columns.PezGroupCoupon, value); }
		}

		[XmlAttribute("FamiGroupCoupon")]
		[Bindable(true)]
		public bool FamiGroupCoupon
		{
			get { return GetColumnValue<bool>(Columns.FamiGroupCoupon); }
			set { SetColumnValue(Columns.FamiGroupCoupon, value); }
		}

		[XmlAttribute("HiLifeGroupCoupon")]
		[Bindable(true)]
		public bool HiLifeGroupCoupon
		{
			get { return GetColumnValue<bool>(Columns.HiLifeGroupCoupon); }
			set { SetColumnValue(Columns.HiLifeGroupCoupon, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn AppIdColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn MinGrossMarginColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn GeneralGrossMarginColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn MinCommissionColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn GeneralCommissionColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn CheckoutDayColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn CheckTypeColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn CheckCycleColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn SellerUserIdColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn GeneralCouponGrossMarginColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn GeneralDeliveryGrossMarginColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn CpaCodeColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn UsingTokenToGuestIdColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn GroupCouponColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn PezGroupCouponColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn FamiGroupCouponColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn HiLifeGroupCouponColumn
		{
			get { return Schema.Columns[17]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string AppId = @"app_id";
			public static string MinGrossMargin = @"min_gross_margin";
			public static string GeneralGrossMargin = @"general_gross_margin";
			public static string MinCommission = @"min_commission";
			public static string GeneralCommission = @"general_commission";
			public static string CheckoutDay = @"checkout_day";
			public static string CheckType = @"check_type";
			public static string CheckCycle = @"check_cycle";
			public static string SellerUserId = @"seller_user_id";
			public static string GeneralCouponGrossMargin = @"general_coupon_gross_margin";
			public static string GeneralDeliveryGrossMargin = @"general_delivery_gross_margin";
			public static string CpaCode = @"cpa_code";
			public static string UsingTokenToGuestId = @"using_token_to_guest_id";
			public static string GroupCoupon = @"group_coupon";
			public static string PezGroupCoupon = @"pez_group_coupon";
			public static string FamiGroupCoupon = @"fami_group_coupon";
			public static string HiLifeGroupCoupon = @"hi_life_group_coupon";
		}

		#endregion

	}
}

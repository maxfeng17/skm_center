using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewPponDealCollection : ReadOnlyList<ViewPponDeal, ViewPponDealCollection>
	{
			public ViewPponDealCollection() {}

	}

	[Serializable]
	public partial class ViewPponDeal : ReadOnlyRecord<ViewPponDeal>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewPponDeal()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewPponDeal(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewPponDeal(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewPponDeal(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_ppon_deal", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_GUID";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
				colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeS.MaxLength = 0;
				colvarBusinessHourOrderTimeS.AutoIncrement = false;
				colvarBusinessHourOrderTimeS.IsNullable = false;
				colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeS.IsForeignKey = false;
				colvarBusinessHourOrderTimeS.IsReadOnly = false;
				colvarBusinessHourOrderTimeS.DefaultSetting = @"";
				colvarBusinessHourOrderTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeS);

				TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
				colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeE.MaxLength = 0;
				colvarBusinessHourOrderTimeE.AutoIncrement = false;
				colvarBusinessHourOrderTimeE.IsNullable = false;
				colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeE.IsForeignKey = false;
				colvarBusinessHourOrderTimeE.IsReadOnly = false;
				colvarBusinessHourOrderTimeE.DefaultSetting = @"";
				colvarBusinessHourOrderTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeE);

				TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
				colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeS.MaxLength = 0;
				colvarBusinessHourDeliverTimeS.AutoIncrement = false;
				colvarBusinessHourDeliverTimeS.IsNullable = true;
				colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeS.IsForeignKey = false;
				colvarBusinessHourDeliverTimeS.IsReadOnly = false;
				colvarBusinessHourDeliverTimeS.DefaultSetting = @"";
				colvarBusinessHourDeliverTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeS);

				TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
				colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeE.MaxLength = 0;
				colvarBusinessHourDeliverTimeE.AutoIncrement = false;
				colvarBusinessHourDeliverTimeE.IsNullable = true;
				colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeE.IsForeignKey = false;
				colvarBusinessHourDeliverTimeE.IsReadOnly = false;
				colvarBusinessHourDeliverTimeE.DefaultSetting = @"";
				colvarBusinessHourDeliverTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeE);

				TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
				colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
				colvarBusinessHourOrderMinimum.MaxLength = 0;
				colvarBusinessHourOrderMinimum.AutoIncrement = false;
				colvarBusinessHourOrderMinimum.IsNullable = false;
				colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
				colvarBusinessHourOrderMinimum.IsForeignKey = false;
				colvarBusinessHourOrderMinimum.IsReadOnly = false;
				colvarBusinessHourOrderMinimum.DefaultSetting = @"";
				colvarBusinessHourOrderMinimum.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderMinimum);

				TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
				colvarBusinessHourStatus.ColumnName = "business_hour_status";
				colvarBusinessHourStatus.DataType = DbType.Int32;
				colvarBusinessHourStatus.MaxLength = 0;
				colvarBusinessHourStatus.AutoIncrement = false;
				colvarBusinessHourStatus.IsNullable = false;
				colvarBusinessHourStatus.IsPrimaryKey = false;
				colvarBusinessHourStatus.IsForeignKey = false;
				colvarBusinessHourStatus.IsReadOnly = false;
				colvarBusinessHourStatus.DefaultSetting = @"";
				colvarBusinessHourStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourStatus);

				TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
				colvarOrderTotalLimit.ColumnName = "order_total_limit";
				colvarOrderTotalLimit.DataType = DbType.Currency;
				colvarOrderTotalLimit.MaxLength = 0;
				colvarOrderTotalLimit.AutoIncrement = false;
				colvarOrderTotalLimit.IsNullable = true;
				colvarOrderTotalLimit.IsPrimaryKey = false;
				colvarOrderTotalLimit.IsForeignKey = false;
				colvarOrderTotalLimit.IsReadOnly = false;
				colvarOrderTotalLimit.DefaultSetting = @"";
				colvarOrderTotalLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderTotalLimit);

				TableSchema.TableColumn colvarBusinessHourDeliveryCharge = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliveryCharge.ColumnName = "business_hour_delivery_charge";
				colvarBusinessHourDeliveryCharge.DataType = DbType.Currency;
				colvarBusinessHourDeliveryCharge.MaxLength = 0;
				colvarBusinessHourDeliveryCharge.AutoIncrement = false;
				colvarBusinessHourDeliveryCharge.IsNullable = false;
				colvarBusinessHourDeliveryCharge.IsPrimaryKey = false;
				colvarBusinessHourDeliveryCharge.IsForeignKey = false;
				colvarBusinessHourDeliveryCharge.IsReadOnly = false;
				colvarBusinessHourDeliveryCharge.DefaultSetting = @"";
				colvarBusinessHourDeliveryCharge.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliveryCharge);

				TableSchema.TableColumn colvarBusinessHourAtmMaximum = new TableSchema.TableColumn(schema);
				colvarBusinessHourAtmMaximum.ColumnName = "business_hour_atm_maximum";
				colvarBusinessHourAtmMaximum.DataType = DbType.Int32;
				colvarBusinessHourAtmMaximum.MaxLength = 0;
				colvarBusinessHourAtmMaximum.AutoIncrement = false;
				colvarBusinessHourAtmMaximum.IsNullable = false;
				colvarBusinessHourAtmMaximum.IsPrimaryKey = false;
				colvarBusinessHourAtmMaximum.IsForeignKey = false;
				colvarBusinessHourAtmMaximum.IsReadOnly = false;
				colvarBusinessHourAtmMaximum.DefaultSetting = @"";
				colvarBusinessHourAtmMaximum.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourAtmMaximum);

				TableSchema.TableColumn colvarPageTitle = new TableSchema.TableColumn(schema);
				colvarPageTitle.ColumnName = "page_title";
				colvarPageTitle.DataType = DbType.String;
				colvarPageTitle.MaxLength = 36;
				colvarPageTitle.AutoIncrement = false;
				colvarPageTitle.IsNullable = true;
				colvarPageTitle.IsPrimaryKey = false;
				colvarPageTitle.IsForeignKey = false;
				colvarPageTitle.IsReadOnly = false;
				colvarPageTitle.DefaultSetting = @"";
				colvarPageTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPageTitle);

				TableSchema.TableColumn colvarPageDesc = new TableSchema.TableColumn(schema);
				colvarPageDesc.ColumnName = "page_desc";
				colvarPageDesc.DataType = DbType.String;
				colvarPageDesc.MaxLength = 350;
				colvarPageDesc.AutoIncrement = false;
				colvarPageDesc.IsNullable = true;
				colvarPageDesc.IsPrimaryKey = false;
				colvarPageDesc.IsForeignKey = false;
				colvarPageDesc.IsReadOnly = false;
				colvarPageDesc.DefaultSetting = @"";
				colvarPageDesc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPageDesc);

				TableSchema.TableColumn colvarPicAlt = new TableSchema.TableColumn(schema);
				colvarPicAlt.ColumnName = "pic_alt";
				colvarPicAlt.DataType = DbType.String;
				colvarPicAlt.MaxLength = 120;
				colvarPicAlt.AutoIncrement = false;
				colvarPicAlt.IsNullable = true;
				colvarPicAlt.IsPrimaryKey = false;
				colvarPicAlt.IsForeignKey = false;
				colvarPicAlt.IsReadOnly = false;
				colvarPicAlt.DefaultSetting = @"";
				colvarPicAlt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPicAlt);

				TableSchema.TableColumn colvarSettlementTime = new TableSchema.TableColumn(schema);
				colvarSettlementTime.ColumnName = "settlement_time";
				colvarSettlementTime.DataType = DbType.DateTime;
				colvarSettlementTime.MaxLength = 0;
				colvarSettlementTime.AutoIncrement = false;
				colvarSettlementTime.IsNullable = true;
				colvarSettlementTime.IsPrimaryKey = false;
				colvarSettlementTime.IsForeignKey = false;
				colvarSettlementTime.IsReadOnly = false;
				colvarSettlementTime.DefaultSetting = @"";
				colvarSettlementTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSettlementTime);

				TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
				colvarChangedExpireDate.ColumnName = "changed_expire_date";
				colvarChangedExpireDate.DataType = DbType.DateTime;
				colvarChangedExpireDate.MaxLength = 0;
				colvarChangedExpireDate.AutoIncrement = false;
				colvarChangedExpireDate.IsNullable = true;
				colvarChangedExpireDate.IsPrimaryKey = false;
				colvarChangedExpireDate.IsForeignKey = false;
				colvarChangedExpireDate.IsReadOnly = false;
				colvarChangedExpireDate.DefaultSetting = @"";
				colvarChangedExpireDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChangedExpireDate);

				TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
				colvarSellerId.ColumnName = "seller_id";
				colvarSellerId.DataType = DbType.AnsiString;
				colvarSellerId.MaxLength = 20;
				colvarSellerId.AutoIncrement = false;
				colvarSellerId.IsNullable = true;
				colvarSellerId.IsPrimaryKey = false;
				colvarSellerId.IsForeignKey = false;
				colvarSellerId.IsReadOnly = false;
				colvarSellerId.DefaultSetting = @"";
				colvarSellerId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerId);

				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 100;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);

				TableSchema.TableColumn colvarSellerTel = new TableSchema.TableColumn(schema);
				colvarSellerTel.ColumnName = "seller_tel";
				colvarSellerTel.DataType = DbType.AnsiString;
				colvarSellerTel.MaxLength = 100;
				colvarSellerTel.AutoIncrement = false;
				colvarSellerTel.IsNullable = true;
				colvarSellerTel.IsPrimaryKey = false;
				colvarSellerTel.IsForeignKey = false;
				colvarSellerTel.IsReadOnly = false;
				colvarSellerTel.DefaultSetting = @"";
				colvarSellerTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerTel);

				TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
				colvarSellerEmail.ColumnName = "seller_email";
				colvarSellerEmail.DataType = DbType.String;
				colvarSellerEmail.MaxLength = 200;
				colvarSellerEmail.AutoIncrement = false;
				colvarSellerEmail.IsNullable = true;
				colvarSellerEmail.IsPrimaryKey = false;
				colvarSellerEmail.IsForeignKey = false;
				colvarSellerEmail.IsReadOnly = false;
				colvarSellerEmail.DefaultSetting = @"";
				colvarSellerEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerEmail);

				TableSchema.TableColumn colvarSellerAddress = new TableSchema.TableColumn(schema);
				colvarSellerAddress.ColumnName = "seller_address";
				colvarSellerAddress.DataType = DbType.String;
				colvarSellerAddress.MaxLength = 100;
				colvarSellerAddress.AutoIncrement = false;
				colvarSellerAddress.IsNullable = true;
				colvarSellerAddress.IsPrimaryKey = false;
				colvarSellerAddress.IsForeignKey = false;
				colvarSellerAddress.IsReadOnly = false;
				colvarSellerAddress.DefaultSetting = @"";
				colvarSellerAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerAddress);

				TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
				colvarDepartment.ColumnName = "department";
				colvarDepartment.DataType = DbType.Int32;
				colvarDepartment.MaxLength = 0;
				colvarDepartment.AutoIncrement = false;
				colvarDepartment.IsNullable = false;
				colvarDepartment.IsPrimaryKey = false;
				colvarDepartment.IsForeignKey = false;
				colvarDepartment.IsReadOnly = false;
				colvarDepartment.DefaultSetting = @"";
				colvarDepartment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDepartment);

				TableSchema.TableColumn colvarSellerCityId = new TableSchema.TableColumn(schema);
				colvarSellerCityId.ColumnName = "seller_city_id";
				colvarSellerCityId.DataType = DbType.Int32;
				colvarSellerCityId.MaxLength = 0;
				colvarSellerCityId.AutoIncrement = false;
				colvarSellerCityId.IsNullable = false;
				colvarSellerCityId.IsPrimaryKey = false;
				colvarSellerCityId.IsForeignKey = false;
				colvarSellerCityId.IsReadOnly = false;
				colvarSellerCityId.DefaultSetting = @"";
				colvarSellerCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerCityId);

				TableSchema.TableColumn colvarIsCloseDown = new TableSchema.TableColumn(schema);
				colvarIsCloseDown.ColumnName = "is_close_down";
				colvarIsCloseDown.DataType = DbType.Boolean;
				colvarIsCloseDown.MaxLength = 0;
				colvarIsCloseDown.AutoIncrement = false;
				colvarIsCloseDown.IsNullable = false;
				colvarIsCloseDown.IsPrimaryKey = false;
				colvarIsCloseDown.IsForeignKey = false;
				colvarIsCloseDown.IsReadOnly = false;
				colvarIsCloseDown.DefaultSetting = @"";
				colvarIsCloseDown.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCloseDown);

				TableSchema.TableColumn colvarCloseDownDate = new TableSchema.TableColumn(schema);
				colvarCloseDownDate.ColumnName = "close_down_date";
				colvarCloseDownDate.DataType = DbType.DateTime;
				colvarCloseDownDate.MaxLength = 0;
				colvarCloseDownDate.AutoIncrement = false;
				colvarCloseDownDate.IsNullable = true;
				colvarCloseDownDate.IsPrimaryKey = false;
				colvarCloseDownDate.IsForeignKey = false;
				colvarCloseDownDate.IsReadOnly = false;
				colvarCloseDownDate.DefaultSetting = @"";
				colvarCloseDownDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCloseDownDate);

				TableSchema.TableColumn colvarCompanyId = new TableSchema.TableColumn(schema);
				colvarCompanyId.ColumnName = "company_id";
				colvarCompanyId.DataType = DbType.AnsiString;
				colvarCompanyId.MaxLength = 20;
				colvarCompanyId.AutoIncrement = false;
				colvarCompanyId.IsNullable = true;
				colvarCompanyId.IsPrimaryKey = false;
				colvarCompanyId.IsForeignKey = false;
				colvarCompanyId.IsReadOnly = false;
				colvarCompanyId.DefaultSetting = @"";
				colvarCompanyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyId);

				TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
				colvarCompanyName.ColumnName = "company_name";
				colvarCompanyName.DataType = DbType.String;
				colvarCompanyName.MaxLength = 50;
				colvarCompanyName.AutoIncrement = false;
				colvarCompanyName.IsNullable = true;
				colvarCompanyName.IsPrimaryKey = false;
				colvarCompanyName.IsForeignKey = false;
				colvarCompanyName.IsReadOnly = false;
				colvarCompanyName.DefaultSetting = @"";
				colvarCompanyName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyName);

				TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
				colvarItemGuid.ColumnName = "item_guid";
				colvarItemGuid.DataType = DbType.Guid;
				colvarItemGuid.MaxLength = 0;
				colvarItemGuid.AutoIncrement = false;
				colvarItemGuid.IsNullable = false;
				colvarItemGuid.IsPrimaryKey = false;
				colvarItemGuid.IsForeignKey = false;
				colvarItemGuid.IsReadOnly = false;
				colvarItemGuid.DefaultSetting = @"";
				colvarItemGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemGuid);

				TableSchema.TableColumn colvarMaxItemCount = new TableSchema.TableColumn(schema);
				colvarMaxItemCount.ColumnName = "max_item_count";
				colvarMaxItemCount.DataType = DbType.Int32;
				colvarMaxItemCount.MaxLength = 0;
				colvarMaxItemCount.AutoIncrement = false;
				colvarMaxItemCount.IsNullable = true;
				colvarMaxItemCount.IsPrimaryKey = false;
				colvarMaxItemCount.IsForeignKey = false;
				colvarMaxItemCount.IsReadOnly = false;
				colvarMaxItemCount.DefaultSetting = @"";
				colvarMaxItemCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMaxItemCount);

				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 750;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = false;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);

				TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
				colvarItemOrigPrice.ColumnName = "item_orig_price";
				colvarItemOrigPrice.DataType = DbType.Currency;
				colvarItemOrigPrice.MaxLength = 0;
				colvarItemOrigPrice.AutoIncrement = false;
				colvarItemOrigPrice.IsNullable = false;
				colvarItemOrigPrice.IsPrimaryKey = false;
				colvarItemOrigPrice.IsForeignKey = false;
				colvarItemOrigPrice.IsReadOnly = false;
				colvarItemOrigPrice.DefaultSetting = @"";
				colvarItemOrigPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemOrigPrice);

				TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
				colvarItemPrice.ColumnName = "item_price";
				colvarItemPrice.DataType = DbType.Currency;
				colvarItemPrice.MaxLength = 0;
				colvarItemPrice.AutoIncrement = false;
				colvarItemPrice.IsNullable = false;
				colvarItemPrice.IsPrimaryKey = false;
				colvarItemPrice.IsForeignKey = false;
				colvarItemPrice.IsReadOnly = false;
				colvarItemPrice.DefaultSetting = @"";
				colvarItemPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemPrice);

				TableSchema.TableColumn colvarItemDefaultDailyAmount = new TableSchema.TableColumn(schema);
				colvarItemDefaultDailyAmount.ColumnName = "item_default_daily_amount";
				colvarItemDefaultDailyAmount.DataType = DbType.Int32;
				colvarItemDefaultDailyAmount.MaxLength = 0;
				colvarItemDefaultDailyAmount.AutoIncrement = false;
				colvarItemDefaultDailyAmount.IsNullable = true;
				colvarItemDefaultDailyAmount.IsPrimaryKey = false;
				colvarItemDefaultDailyAmount.IsForeignKey = false;
				colvarItemDefaultDailyAmount.IsReadOnly = false;
				colvarItemDefaultDailyAmount.DefaultSetting = @"";
				colvarItemDefaultDailyAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemDefaultDailyAmount);

				TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
				colvarEventName.ColumnName = "event_name";
				colvarEventName.DataType = DbType.String;
				colvarEventName.MaxLength = 400;
				colvarEventName.AutoIncrement = false;
				colvarEventName.IsNullable = true;
				colvarEventName.IsPrimaryKey = false;
				colvarEventName.IsForeignKey = false;
				colvarEventName.IsReadOnly = false;
				colvarEventName.DefaultSetting = @"";
				colvarEventName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventName);

				TableSchema.TableColumn colvarEventTitle = new TableSchema.TableColumn(schema);
				colvarEventTitle.ColumnName = "event_title";
				colvarEventTitle.DataType = DbType.String;
				colvarEventTitle.MaxLength = 120;
				colvarEventTitle.AutoIncrement = false;
				colvarEventTitle.IsNullable = true;
				colvarEventTitle.IsPrimaryKey = false;
				colvarEventTitle.IsForeignKey = false;
				colvarEventTitle.IsReadOnly = false;
				colvarEventTitle.DefaultSetting = @"";
				colvarEventTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventTitle);

				TableSchema.TableColumn colvarEventImagePath = new TableSchema.TableColumn(schema);
				colvarEventImagePath.ColumnName = "event_image_path";
				colvarEventImagePath.DataType = DbType.String;
				colvarEventImagePath.MaxLength = 1000;
				colvarEventImagePath.AutoIncrement = false;
				colvarEventImagePath.IsNullable = true;
				colvarEventImagePath.IsPrimaryKey = false;
				colvarEventImagePath.IsForeignKey = false;
				colvarEventImagePath.IsReadOnly = false;
				colvarEventImagePath.DefaultSetting = @"";
				colvarEventImagePath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventImagePath);

                TableSchema.TableColumn colvarEventOriImagePath = new TableSchema.TableColumn(schema);
                colvarEventOriImagePath.ColumnName = "event_ori_image_path";
                colvarEventOriImagePath.DataType = DbType.String;
                colvarEventOriImagePath.MaxLength = 1000;
                colvarEventOriImagePath.AutoIncrement = false;
                colvarEventOriImagePath.IsNullable = true;
                colvarEventOriImagePath.IsPrimaryKey = false;
                colvarEventOriImagePath.IsForeignKey = false;
                colvarEventOriImagePath.IsReadOnly = false;
                colvarEventOriImagePath.DefaultSetting = @"";
                colvarEventOriImagePath.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventOriImagePath);

                TableSchema.TableColumn colvarIntroduction = new TableSchema.TableColumn(schema);
				colvarIntroduction.ColumnName = "introduction";
				colvarIntroduction.DataType = DbType.String;
				colvarIntroduction.MaxLength = 1073741823;
				colvarIntroduction.AutoIncrement = false;
				colvarIntroduction.IsNullable = true;
				colvarIntroduction.IsPrimaryKey = false;
				colvarIntroduction.IsForeignKey = false;
				colvarIntroduction.IsReadOnly = false;
				colvarIntroduction.DefaultSetting = @"";
				colvarIntroduction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntroduction);

				TableSchema.TableColumn colvarRestrictions = new TableSchema.TableColumn(schema);
				colvarRestrictions.ColumnName = "restrictions";
				colvarRestrictions.DataType = DbType.String;
				colvarRestrictions.MaxLength = 1073741823;
				colvarRestrictions.AutoIncrement = false;
				colvarRestrictions.IsNullable = true;
				colvarRestrictions.IsPrimaryKey = false;
				colvarRestrictions.IsForeignKey = false;
				colvarRestrictions.IsReadOnly = false;
				colvarRestrictions.DefaultSetting = @"";
				colvarRestrictions.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRestrictions);

				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = 1;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = false;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);

				TableSchema.TableColumn colvarAppDescription = new TableSchema.TableColumn(schema);
				colvarAppDescription.ColumnName = "app_description";
				colvarAppDescription.DataType = DbType.String;
				colvarAppDescription.MaxLength = 1;
				colvarAppDescription.AutoIncrement = false;
				colvarAppDescription.IsNullable = false;
				colvarAppDescription.IsPrimaryKey = false;
				colvarAppDescription.IsForeignKey = false;
				colvarAppDescription.IsReadOnly = false;
				colvarAppDescription.DefaultSetting = @"";
				colvarAppDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppDescription);

				TableSchema.TableColumn colvarReferenceText = new TableSchema.TableColumn(schema);
				colvarReferenceText.ColumnName = "reference_text";
				colvarReferenceText.DataType = DbType.String;
				colvarReferenceText.MaxLength = 1073741823;
				colvarReferenceText.AutoIncrement = false;
				colvarReferenceText.IsNullable = true;
				colvarReferenceText.IsPrimaryKey = false;
				colvarReferenceText.IsForeignKey = false;
				colvarReferenceText.IsReadOnly = false;
				colvarReferenceText.DefaultSetting = @"";
				colvarReferenceText.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferenceText);

				TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
				colvarCouponUsage.ColumnName = "coupon_usage";
				colvarCouponUsage.DataType = DbType.String;
				colvarCouponUsage.MaxLength = 500;
				colvarCouponUsage.AutoIncrement = false;
				colvarCouponUsage.IsNullable = true;
				colvarCouponUsage.IsPrimaryKey = false;
				colvarCouponUsage.IsForeignKey = false;
				colvarCouponUsage.IsReadOnly = false;
				colvarCouponUsage.DefaultSetting = @"";
				colvarCouponUsage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponUsage);

				TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
				colvarRemark.ColumnName = "remark";
				colvarRemark.DataType = DbType.String;
				colvarRemark.MaxLength = 1073741823;
				colvarRemark.AutoIncrement = false;
				colvarRemark.IsNullable = true;
				colvarRemark.IsPrimaryKey = false;
				colvarRemark.IsForeignKey = false;
				colvarRemark.IsReadOnly = false;
				colvarRemark.DefaultSetting = @"";
				colvarRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemark);

				TableSchema.TableColumn colvarReasons = new TableSchema.TableColumn(schema);
				colvarReasons.ColumnName = "reasons";
				colvarReasons.DataType = DbType.String;
				colvarReasons.MaxLength = 1073741823;
				colvarReasons.AutoIncrement = false;
				colvarReasons.IsNullable = true;
				colvarReasons.IsPrimaryKey = false;
				colvarReasons.IsForeignKey = false;
				colvarReasons.IsReadOnly = false;
				colvarReasons.DefaultSetting = @"";
				colvarReasons.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReasons);

				TableSchema.TableColumn colvarSubjectName = new TableSchema.TableColumn(schema);
				colvarSubjectName.ColumnName = "subject_name";
				colvarSubjectName.DataType = DbType.String;
				colvarSubjectName.MaxLength = 150;
				colvarSubjectName.AutoIncrement = false;
				colvarSubjectName.IsNullable = true;
				colvarSubjectName.IsPrimaryKey = false;
				colvarSubjectName.IsForeignKey = false;
				colvarSubjectName.IsReadOnly = false;
				colvarSubjectName.DefaultSetting = @"";
				colvarSubjectName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubjectName);

				TableSchema.TableColumn colvarEventSpecialImagePath = new TableSchema.TableColumn(schema);
				colvarEventSpecialImagePath.ColumnName = "event_special_image_path";
				colvarEventSpecialImagePath.DataType = DbType.String;
				colvarEventSpecialImagePath.MaxLength = 500;
				colvarEventSpecialImagePath.AutoIncrement = false;
				colvarEventSpecialImagePath.IsNullable = true;
				colvarEventSpecialImagePath.IsPrimaryKey = false;
				colvarEventSpecialImagePath.IsForeignKey = false;
				colvarEventSpecialImagePath.IsReadOnly = false;
				colvarEventSpecialImagePath.DefaultSetting = @"";
				colvarEventSpecialImagePath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventSpecialImagePath);

				TableSchema.TableColumn colvarAppDealPic = new TableSchema.TableColumn(schema);
				colvarAppDealPic.ColumnName = "app_deal_pic";
				colvarAppDealPic.DataType = DbType.String;
				colvarAppDealPic.MaxLength = 200;
				colvarAppDealPic.AutoIncrement = false;
				colvarAppDealPic.IsNullable = true;
				colvarAppDealPic.IsPrimaryKey = false;
				colvarAppDealPic.IsForeignKey = false;
				colvarAppDealPic.IsReadOnly = false;
				colvarAppDealPic.DefaultSetting = @"";
				colvarAppDealPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppDealPic);

				TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
				colvarAppTitle.ColumnName = "app_title";
				colvarAppTitle.DataType = DbType.String;
				colvarAppTitle.MaxLength = 120;
				colvarAppTitle.AutoIncrement = false;
				colvarAppTitle.IsNullable = true;
				colvarAppTitle.IsPrimaryKey = false;
				colvarAppTitle.IsForeignKey = false;
				colvarAppTitle.IsReadOnly = false;
				colvarAppTitle.DefaultSetting = @"";
				colvarAppTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppTitle);

				TableSchema.TableColumn colvarAvailability = new TableSchema.TableColumn(schema);
				colvarAvailability.ColumnName = "availability";
				colvarAvailability.DataType = DbType.String;
				colvarAvailability.MaxLength = 1073741823;
				colvarAvailability.AutoIncrement = false;
				colvarAvailability.IsNullable = true;
				colvarAvailability.IsPrimaryKey = false;
				colvarAvailability.IsForeignKey = false;
				colvarAvailability.IsReadOnly = false;
				colvarAvailability.DefaultSetting = @"";
				colvarAvailability.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAvailability);

				TableSchema.TableColumn colvarEventModifyTime = new TableSchema.TableColumn(schema);
				colvarEventModifyTime.ColumnName = "event_modify_time";
				colvarEventModifyTime.DataType = DbType.DateTime;
				colvarEventModifyTime.MaxLength = 0;
				colvarEventModifyTime.AutoIncrement = false;
				colvarEventModifyTime.IsNullable = false;
				colvarEventModifyTime.IsPrimaryKey = false;
				colvarEventModifyTime.IsForeignKey = false;
				colvarEventModifyTime.IsReadOnly = false;
				colvarEventModifyTime.DefaultSetting = @"";
				colvarEventModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventModifyTime);

				TableSchema.TableColumn colvarProductSpec = new TableSchema.TableColumn(schema);
				colvarProductSpec.ColumnName = "product_spec";
				colvarProductSpec.DataType = DbType.String;
				colvarProductSpec.MaxLength = 2147483647;
				colvarProductSpec.AutoIncrement = false;
				colvarProductSpec.IsNullable = true;
				colvarProductSpec.IsPrimaryKey = false;
				colvarProductSpec.IsForeignKey = false;
				colvarProductSpec.IsReadOnly = false;
				colvarProductSpec.DefaultSetting = @"";
				colvarProductSpec.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductSpec);

				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = true;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);

				TableSchema.TableColumn colvarShoppingCart = new TableSchema.TableColumn(schema);
				colvarShoppingCart.ColumnName = "shopping_cart";
				colvarShoppingCart.DataType = DbType.Boolean;
				colvarShoppingCart.MaxLength = 0;
				colvarShoppingCart.AutoIncrement = false;
				colvarShoppingCart.IsNullable = true;
				colvarShoppingCart.IsPrimaryKey = false;
				colvarShoppingCart.IsForeignKey = false;
				colvarShoppingCart.IsReadOnly = false;
				colvarShoppingCart.DefaultSetting = @"";
				colvarShoppingCart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShoppingCart);

				TableSchema.TableColumn colvarComboPackCount = new TableSchema.TableColumn(schema);
				colvarComboPackCount.ColumnName = "combo_pack_count";
				colvarComboPackCount.DataType = DbType.Int32;
				colvarComboPackCount.MaxLength = 0;
				colvarComboPackCount.AutoIncrement = false;
				colvarComboPackCount.IsNullable = true;
				colvarComboPackCount.IsPrimaryKey = false;
				colvarComboPackCount.IsForeignKey = false;
				colvarComboPackCount.IsReadOnly = false;
				colvarComboPackCount.DefaultSetting = @"";
				colvarComboPackCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarComboPackCount);

				TableSchema.TableColumn colvarIsTravelcard = new TableSchema.TableColumn(schema);
				colvarIsTravelcard.ColumnName = "is_travelcard";
				colvarIsTravelcard.DataType = DbType.Boolean;
				colvarIsTravelcard.MaxLength = 0;
				colvarIsTravelcard.AutoIncrement = false;
				colvarIsTravelcard.IsNullable = true;
				colvarIsTravelcard.IsPrimaryKey = false;
				colvarIsTravelcard.IsForeignKey = false;
				colvarIsTravelcard.IsReadOnly = false;
				colvarIsTravelcard.DefaultSetting = @"";
				colvarIsTravelcard.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTravelcard);

				TableSchema.TableColumn colvarQuantityMultiplier = new TableSchema.TableColumn(schema);
				colvarQuantityMultiplier.ColumnName = "quantity_multiplier";
				colvarQuantityMultiplier.DataType = DbType.Int32;
				colvarQuantityMultiplier.MaxLength = 0;
				colvarQuantityMultiplier.AutoIncrement = false;
				colvarQuantityMultiplier.IsNullable = true;
				colvarQuantityMultiplier.IsPrimaryKey = false;
				colvarQuantityMultiplier.IsForeignKey = false;
				colvarQuantityMultiplier.IsReadOnly = false;
				colvarQuantityMultiplier.DefaultSetting = @"";
				colvarQuantityMultiplier.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQuantityMultiplier);

				TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
				colvarUniqueId.ColumnName = "unique_id";
				colvarUniqueId.DataType = DbType.Int32;
				colvarUniqueId.MaxLength = 0;
				colvarUniqueId.AutoIncrement = false;
				colvarUniqueId.IsNullable = true;
				colvarUniqueId.IsPrimaryKey = false;
				colvarUniqueId.IsForeignKey = false;
				colvarUniqueId.IsReadOnly = false;
				colvarUniqueId.DefaultSetting = @"";
				colvarUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniqueId);

				TableSchema.TableColumn colvarIsDailyRestriction = new TableSchema.TableColumn(schema);
				colvarIsDailyRestriction.ColumnName = "is_daily_restriction";
				colvarIsDailyRestriction.DataType = DbType.Boolean;
				colvarIsDailyRestriction.MaxLength = 0;
				colvarIsDailyRestriction.AutoIncrement = false;
				colvarIsDailyRestriction.IsNullable = true;
				colvarIsDailyRestriction.IsPrimaryKey = false;
				colvarIsDailyRestriction.IsForeignKey = false;
				colvarIsDailyRestriction.IsReadOnly = false;
				colvarIsDailyRestriction.DefaultSetting = @"";
				colvarIsDailyRestriction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDailyRestriction);

				TableSchema.TableColumn colvarIsContinuedQuantity = new TableSchema.TableColumn(schema);
				colvarIsContinuedQuantity.ColumnName = "is_continued_quantity";
				colvarIsContinuedQuantity.DataType = DbType.Boolean;
				colvarIsContinuedQuantity.MaxLength = 0;
				colvarIsContinuedQuantity.AutoIncrement = false;
				colvarIsContinuedQuantity.IsNullable = true;
				colvarIsContinuedQuantity.IsPrimaryKey = false;
				colvarIsContinuedQuantity.IsForeignKey = false;
				colvarIsContinuedQuantity.IsReadOnly = false;
				colvarIsContinuedQuantity.DefaultSetting = @"";
				colvarIsContinuedQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsContinuedQuantity);

				TableSchema.TableColumn colvarContinuedQuantity = new TableSchema.TableColumn(schema);
				colvarContinuedQuantity.ColumnName = "continued_quantity";
				colvarContinuedQuantity.DataType = DbType.Int32;
				colvarContinuedQuantity.MaxLength = 0;
				colvarContinuedQuantity.AutoIncrement = false;
				colvarContinuedQuantity.IsNullable = true;
				colvarContinuedQuantity.IsPrimaryKey = false;
				colvarContinuedQuantity.IsForeignKey = false;
				colvarContinuedQuantity.IsReadOnly = false;
				colvarContinuedQuantity.DefaultSetting = @"";
				colvarContinuedQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContinuedQuantity);

				TableSchema.TableColumn colvarCityList = new TableSchema.TableColumn(schema);
				colvarCityList.ColumnName = "city_list";
				colvarCityList.DataType = DbType.AnsiString;
				colvarCityList.MaxLength = 255;
				colvarCityList.AutoIncrement = false;
				colvarCityList.IsNullable = true;
				colvarCityList.IsPrimaryKey = false;
				colvarCityList.IsForeignKey = false;
				colvarCityList.IsReadOnly = false;
				colvarCityList.DefaultSetting = @"";
				colvarCityList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityList);

				TableSchema.TableColumn colvarActivityUrl = new TableSchema.TableColumn(schema);
				colvarActivityUrl.ColumnName = "activity_url";
				colvarActivityUrl.DataType = DbType.String;
				colvarActivityUrl.MaxLength = 256;
				colvarActivityUrl.AutoIncrement = false;
				colvarActivityUrl.IsNullable = true;
				colvarActivityUrl.IsPrimaryKey = false;
				colvarActivityUrl.IsForeignKey = false;
				colvarActivityUrl.IsReadOnly = false;
				colvarActivityUrl.DefaultSetting = @"";
				colvarActivityUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActivityUrl);

				TableSchema.TableColumn colvarLabelIconList = new TableSchema.TableColumn(schema);
				colvarLabelIconList.ColumnName = "label_icon_list";
				colvarLabelIconList.DataType = DbType.AnsiString;
				colvarLabelIconList.MaxLength = 255;
				colvarLabelIconList.AutoIncrement = false;
				colvarLabelIconList.IsNullable = true;
				colvarLabelIconList.IsPrimaryKey = false;
				colvarLabelIconList.IsForeignKey = false;
				colvarLabelIconList.IsReadOnly = false;
				colvarLabelIconList.DefaultSetting = @"";
				colvarLabelIconList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLabelIconList);

				TableSchema.TableColumn colvarLabelTagList = new TableSchema.TableColumn(schema);
				colvarLabelTagList.ColumnName = "label_tag_list";
				colvarLabelTagList.DataType = DbType.AnsiString;
				colvarLabelTagList.MaxLength = 255;
				colvarLabelTagList.AutoIncrement = false;
				colvarLabelTagList.IsNullable = true;
				colvarLabelTagList.IsPrimaryKey = false;
				colvarLabelTagList.IsForeignKey = false;
				colvarLabelTagList.IsReadOnly = false;
				colvarLabelTagList.DefaultSetting = @"";
				colvarLabelTagList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLabelTagList);

				TableSchema.TableColumn colvarCouponCodeType = new TableSchema.TableColumn(schema);
				colvarCouponCodeType.ColumnName = "coupon_code_type";
				colvarCouponCodeType.DataType = DbType.Int32;
				colvarCouponCodeType.MaxLength = 0;
				colvarCouponCodeType.AutoIncrement = false;
				colvarCouponCodeType.IsNullable = true;
				colvarCouponCodeType.IsPrimaryKey = false;
				colvarCouponCodeType.IsForeignKey = false;
				colvarCouponCodeType.IsReadOnly = false;
				colvarCouponCodeType.DefaultSetting = @"";
				colvarCouponCodeType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponCodeType);

				TableSchema.TableColumn colvarPinType = new TableSchema.TableColumn(schema);
				colvarPinType.ColumnName = "pin_type";
				colvarPinType.DataType = DbType.Int32;
				colvarPinType.MaxLength = 0;
				colvarPinType.AutoIncrement = false;
				colvarPinType.IsNullable = true;
				colvarPinType.IsPrimaryKey = false;
				colvarPinType.IsForeignKey = false;
				colvarPinType.IsReadOnly = false;
				colvarPinType.DefaultSetting = @"";
				colvarPinType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPinType);

				TableSchema.TableColumn colvarCustomTag = new TableSchema.TableColumn(schema);
				colvarCustomTag.ColumnName = "custom_tag";
				colvarCustomTag.DataType = DbType.String;
				colvarCustomTag.MaxLength = 10;
				colvarCustomTag.AutoIncrement = false;
				colvarCustomTag.IsNullable = true;
				colvarCustomTag.IsPrimaryKey = false;
				colvarCustomTag.IsForeignKey = false;
				colvarCustomTag.IsReadOnly = false;
				colvarCustomTag.DefaultSetting = @"";
				colvarCustomTag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCustomTag);

				TableSchema.TableColumn colvarExchangePrice = new TableSchema.TableColumn(schema);
				colvarExchangePrice.ColumnName = "exchange_price";
				colvarExchangePrice.DataType = DbType.Decimal;
				colvarExchangePrice.MaxLength = 0;
				colvarExchangePrice.AutoIncrement = false;
				colvarExchangePrice.IsNullable = true;
				colvarExchangePrice.IsPrimaryKey = false;
				colvarExchangePrice.IsForeignKey = false;
				colvarExchangePrice.IsReadOnly = false;
				colvarExchangePrice.DefaultSetting = @"";
				colvarExchangePrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExchangePrice);

				TableSchema.TableColumn colvarIsQuantityMultiplier = new TableSchema.TableColumn(schema);
				colvarIsQuantityMultiplier.ColumnName = "is_quantity_multiplier";
				colvarIsQuantityMultiplier.DataType = DbType.Boolean;
				colvarIsQuantityMultiplier.MaxLength = 0;
				colvarIsQuantityMultiplier.AutoIncrement = false;
				colvarIsQuantityMultiplier.IsNullable = true;
				colvarIsQuantityMultiplier.IsPrimaryKey = false;
				colvarIsQuantityMultiplier.IsForeignKey = false;
				colvarIsQuantityMultiplier.IsReadOnly = false;
				colvarIsQuantityMultiplier.DefaultSetting = @"";
				colvarIsQuantityMultiplier.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsQuantityMultiplier);

				TableSchema.TableColumn colvarIsAveragePrice = new TableSchema.TableColumn(schema);
				colvarIsAveragePrice.ColumnName = "is_average_price";
				colvarIsAveragePrice.DataType = DbType.Boolean;
				colvarIsAveragePrice.MaxLength = 0;
				colvarIsAveragePrice.AutoIncrement = false;
				colvarIsAveragePrice.IsNullable = true;
				colvarIsAveragePrice.IsPrimaryKey = false;
				colvarIsAveragePrice.IsForeignKey = false;
				colvarIsAveragePrice.IsReadOnly = false;
				colvarIsAveragePrice.DefaultSetting = @"";
				colvarIsAveragePrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsAveragePrice);

				TableSchema.TableColumn colvarIsZeroActivityShowCoupon = new TableSchema.TableColumn(schema);
				colvarIsZeroActivityShowCoupon.ColumnName = "is_zero_activity_show_coupon";
				colvarIsZeroActivityShowCoupon.DataType = DbType.Boolean;
				colvarIsZeroActivityShowCoupon.MaxLength = 0;
				colvarIsZeroActivityShowCoupon.AutoIncrement = false;
				colvarIsZeroActivityShowCoupon.IsNullable = true;
				colvarIsZeroActivityShowCoupon.IsPrimaryKey = false;
				colvarIsZeroActivityShowCoupon.IsForeignKey = false;
				colvarIsZeroActivityShowCoupon.IsReadOnly = false;
				colvarIsZeroActivityShowCoupon.DefaultSetting = @"";
				colvarIsZeroActivityShowCoupon.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsZeroActivityShowCoupon);

				TableSchema.TableColumn colvarDealAccBusinessGroupId = new TableSchema.TableColumn(schema);
				colvarDealAccBusinessGroupId.ColumnName = "deal_acc_business_group_id";
				colvarDealAccBusinessGroupId.DataType = DbType.Int32;
				colvarDealAccBusinessGroupId.MaxLength = 0;
				colvarDealAccBusinessGroupId.AutoIncrement = false;
				colvarDealAccBusinessGroupId.IsNullable = true;
				colvarDealAccBusinessGroupId.IsPrimaryKey = false;
				colvarDealAccBusinessGroupId.IsForeignKey = false;
				colvarDealAccBusinessGroupId.IsReadOnly = false;
				colvarDealAccBusinessGroupId.DefaultSetting = @"";
				colvarDealAccBusinessGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealAccBusinessGroupId);

				TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
				colvarDealType.ColumnName = "deal_type";
				colvarDealType.DataType = DbType.Int32;
				colvarDealType.MaxLength = 0;
				colvarDealType.AutoIncrement = false;
				colvarDealType.IsNullable = true;
				colvarDealType.IsPrimaryKey = false;
				colvarDealType.IsForeignKey = false;
				colvarDealType.IsReadOnly = false;
				colvarDealType.DefaultSetting = @"";
				colvarDealType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealType);

				TableSchema.TableColumn colvarDealTypeDetail = new TableSchema.TableColumn(schema);
				colvarDealTypeDetail.ColumnName = "deal_type_detail";
				colvarDealTypeDetail.DataType = DbType.Int32;
				colvarDealTypeDetail.MaxLength = 0;
				colvarDealTypeDetail.AutoIncrement = false;
				colvarDealTypeDetail.IsNullable = true;
				colvarDealTypeDetail.IsPrimaryKey = false;
				colvarDealTypeDetail.IsForeignKey = false;
				colvarDealTypeDetail.IsReadOnly = false;
				colvarDealTypeDetail.DefaultSetting = @"";
				colvarDealTypeDetail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealTypeDetail);

				TableSchema.TableColumn colvarIsTravelDeal = new TableSchema.TableColumn(schema);
				colvarIsTravelDeal.ColumnName = "is_travel_deal";
				colvarIsTravelDeal.DataType = DbType.Boolean;
				colvarIsTravelDeal.MaxLength = 0;
				colvarIsTravelDeal.AutoIncrement = false;
				colvarIsTravelDeal.IsNullable = true;
				colvarIsTravelDeal.IsPrimaryKey = false;
				colvarIsTravelDeal.IsForeignKey = false;
				colvarIsTravelDeal.IsReadOnly = false;
				colvarIsTravelDeal.DefaultSetting = @"";
				colvarIsTravelDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTravelDeal);

				TableSchema.TableColumn colvarDealEmpName = new TableSchema.TableColumn(schema);
				colvarDealEmpName.ColumnName = "deal_emp_name";
				colvarDealEmpName.DataType = DbType.String;
				colvarDealEmpName.MaxLength = 50;
				colvarDealEmpName.AutoIncrement = false;
				colvarDealEmpName.IsNullable = true;
				colvarDealEmpName.IsPrimaryKey = false;
				colvarDealEmpName.IsForeignKey = false;
				colvarDealEmpName.IsReadOnly = false;
				colvarDealEmpName.DefaultSetting = @"";
				colvarDealEmpName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealEmpName);

				TableSchema.TableColumn colvarInstallment3months = new TableSchema.TableColumn(schema);
				colvarInstallment3months.ColumnName = "installment_3months";
				colvarInstallment3months.DataType = DbType.Boolean;
				colvarInstallment3months.MaxLength = 0;
				colvarInstallment3months.AutoIncrement = false;
				colvarInstallment3months.IsNullable = true;
				colvarInstallment3months.IsPrimaryKey = false;
				colvarInstallment3months.IsForeignKey = false;
				colvarInstallment3months.IsReadOnly = false;
				colvarInstallment3months.DefaultSetting = @"";
				colvarInstallment3months.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInstallment3months);

				TableSchema.TableColumn colvarInstallment6months = new TableSchema.TableColumn(schema);
				colvarInstallment6months.ColumnName = "installment_6months";
				colvarInstallment6months.DataType = DbType.Boolean;
				colvarInstallment6months.MaxLength = 0;
				colvarInstallment6months.AutoIncrement = false;
				colvarInstallment6months.IsNullable = true;
				colvarInstallment6months.IsPrimaryKey = false;
				colvarInstallment6months.IsForeignKey = false;
				colvarInstallment6months.IsReadOnly = false;
				colvarInstallment6months.DefaultSetting = @"";
				colvarInstallment6months.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInstallment6months);

				TableSchema.TableColumn colvarInstallment12months = new TableSchema.TableColumn(schema);
				colvarInstallment12months.ColumnName = "installment_12months";
				colvarInstallment12months.DataType = DbType.Boolean;
				colvarInstallment12months.MaxLength = 0;
				colvarInstallment12months.AutoIncrement = false;
				colvarInstallment12months.IsNullable = true;
				colvarInstallment12months.IsPrimaryKey = false;
				colvarInstallment12months.IsForeignKey = false;
				colvarInstallment12months.IsReadOnly = false;
				colvarInstallment12months.DefaultSetting = @"";
				colvarInstallment12months.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInstallment12months);

				TableSchema.TableColumn colvarDenyInstallment = new TableSchema.TableColumn(schema);
				colvarDenyInstallment.ColumnName = "deny_installment";
				colvarDenyInstallment.DataType = DbType.Boolean;
				colvarDenyInstallment.MaxLength = 0;
				colvarDenyInstallment.AutoIncrement = false;
				colvarDenyInstallment.IsNullable = true;
				colvarDenyInstallment.IsPrimaryKey = false;
				colvarDenyInstallment.IsForeignKey = false;
				colvarDenyInstallment.IsReadOnly = false;
				colvarDenyInstallment.DefaultSetting = @"";
				colvarDenyInstallment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDenyInstallment);

				TableSchema.TableColumn colvarSaleMultipleBase = new TableSchema.TableColumn(schema);
				colvarSaleMultipleBase.ColumnName = "sale_multiple_base";
				colvarSaleMultipleBase.DataType = DbType.Int32;
				colvarSaleMultipleBase.MaxLength = 0;
				colvarSaleMultipleBase.AutoIncrement = false;
				colvarSaleMultipleBase.IsNullable = true;
				colvarSaleMultipleBase.IsPrimaryKey = false;
				colvarSaleMultipleBase.IsForeignKey = false;
				colvarSaleMultipleBase.IsReadOnly = false;
				colvarSaleMultipleBase.DefaultSetting = @"";
				colvarSaleMultipleBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSaleMultipleBase);

				TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
				colvarShipType.ColumnName = "ship_type";
				colvarShipType.DataType = DbType.Int32;
				colvarShipType.MaxLength = 0;
				colvarShipType.AutoIncrement = false;
				colvarShipType.IsNullable = true;
				colvarShipType.IsPrimaryKey = false;
				colvarShipType.IsForeignKey = false;
				colvarShipType.IsReadOnly = false;
				colvarShipType.DefaultSetting = @"";
				colvarShipType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipType);

				TableSchema.TableColumn colvarShippingdateType = new TableSchema.TableColumn(schema);
				colvarShippingdateType.ColumnName = "shippingdate_type";
				colvarShippingdateType.DataType = DbType.Int32;
				colvarShippingdateType.MaxLength = 0;
				colvarShippingdateType.AutoIncrement = false;
				colvarShippingdateType.IsNullable = true;
				colvarShippingdateType.IsPrimaryKey = false;
				colvarShippingdateType.IsForeignKey = false;
				colvarShippingdateType.IsReadOnly = false;
				colvarShippingdateType.DefaultSetting = @"";
				colvarShippingdateType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShippingdateType);

				TableSchema.TableColumn colvarShippingdate = new TableSchema.TableColumn(schema);
				colvarShippingdate.ColumnName = "shippingdate";
				colvarShippingdate.DataType = DbType.Int32;
				colvarShippingdate.MaxLength = 0;
				colvarShippingdate.AutoIncrement = false;
				colvarShippingdate.IsNullable = true;
				colvarShippingdate.IsPrimaryKey = false;
				colvarShippingdate.IsForeignKey = false;
				colvarShippingdate.IsReadOnly = false;
				colvarShippingdate.DefaultSetting = @"";
				colvarShippingdate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShippingdate);

				TableSchema.TableColumn colvarProductUseDateEndSet = new TableSchema.TableColumn(schema);
				colvarProductUseDateEndSet.ColumnName = "product_use_date_end_set";
				colvarProductUseDateEndSet.DataType = DbType.Int32;
				colvarProductUseDateEndSet.MaxLength = 0;
				colvarProductUseDateEndSet.AutoIncrement = false;
				colvarProductUseDateEndSet.IsNullable = true;
				colvarProductUseDateEndSet.IsPrimaryKey = false;
				colvarProductUseDateEndSet.IsForeignKey = false;
				colvarProductUseDateEndSet.IsReadOnly = false;
				colvarProductUseDateEndSet.DefaultSetting = @"";
				colvarProductUseDateEndSet.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductUseDateEndSet);

				TableSchema.TableColumn colvarPresentQuantity = new TableSchema.TableColumn(schema);
				colvarPresentQuantity.ColumnName = "present_quantity";
				colvarPresentQuantity.DataType = DbType.Int32;
				colvarPresentQuantity.MaxLength = 0;
				colvarPresentQuantity.AutoIncrement = false;
				colvarPresentQuantity.IsNullable = true;
				colvarPresentQuantity.IsPrimaryKey = false;
				colvarPresentQuantity.IsForeignKey = false;
				colvarPresentQuantity.IsReadOnly = false;
				colvarPresentQuantity.DefaultSetting = @"";
				colvarPresentQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPresentQuantity);

				TableSchema.TableColumn colvarGroupCouponAppStyle = new TableSchema.TableColumn(schema);
				colvarGroupCouponAppStyle.ColumnName = "group_coupon_app_style";
				colvarGroupCouponAppStyle.DataType = DbType.Int32;
				colvarGroupCouponAppStyle.MaxLength = 0;
				colvarGroupCouponAppStyle.AutoIncrement = false;
				colvarGroupCouponAppStyle.IsNullable = true;
				colvarGroupCouponAppStyle.IsPrimaryKey = false;
				colvarGroupCouponAppStyle.IsForeignKey = false;
				colvarGroupCouponAppStyle.IsReadOnly = false;
				colvarGroupCouponAppStyle.DefaultSetting = @"";
				colvarGroupCouponAppStyle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupCouponAppStyle);

				TableSchema.TableColumn colvarNewDealType = new TableSchema.TableColumn(schema);
				colvarNewDealType.ColumnName = "new_deal_type";
				colvarNewDealType.DataType = DbType.Int32;
				colvarNewDealType.MaxLength = 0;
				colvarNewDealType.AutoIncrement = false;
				colvarNewDealType.IsNullable = true;
				colvarNewDealType.IsPrimaryKey = false;
				colvarNewDealType.IsForeignKey = false;
				colvarNewDealType.IsReadOnly = false;
				colvarNewDealType.DefaultSetting = @"";
				colvarNewDealType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNewDealType);

				TableSchema.TableColumn colvarIsExperience = new TableSchema.TableColumn(schema);
				colvarIsExperience.ColumnName = "is_experience";
				colvarIsExperience.DataType = DbType.Boolean;
				colvarIsExperience.MaxLength = 0;
				colvarIsExperience.AutoIncrement = false;
				colvarIsExperience.IsNullable = true;
				colvarIsExperience.IsPrimaryKey = false;
				colvarIsExperience.IsForeignKey = false;
				colvarIsExperience.IsReadOnly = false;
				colvarIsExperience.DefaultSetting = @"";
				colvarIsExperience.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsExperience);

				TableSchema.TableColumn colvarDiscountType = new TableSchema.TableColumn(schema);
				colvarDiscountType.ColumnName = "discount_type";
				colvarDiscountType.DataType = DbType.Int32;
				colvarDiscountType.MaxLength = 0;
				colvarDiscountType.AutoIncrement = false;
				colvarDiscountType.IsNullable = true;
				colvarDiscountType.IsPrimaryKey = false;
				colvarDiscountType.IsForeignKey = false;
				colvarDiscountType.IsReadOnly = false;
				colvarDiscountType.DefaultSetting = @"";
				colvarDiscountType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountType);

				TableSchema.TableColumn colvarDiscountValue = new TableSchema.TableColumn(schema);
				colvarDiscountValue.ColumnName = "discount_value";
				colvarDiscountValue.DataType = DbType.Int32;
				colvarDiscountValue.MaxLength = 0;
				colvarDiscountValue.AutoIncrement = false;
				colvarDiscountValue.IsNullable = true;
				colvarDiscountValue.IsPrimaryKey = false;
				colvarDiscountValue.IsForeignKey = false;
				colvarDiscountValue.IsReadOnly = false;
				colvarDiscountValue.DefaultSetting = @"";
				colvarDiscountValue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountValue);

				TableSchema.TableColumn colvarTravelPlace = new TableSchema.TableColumn(schema);
				colvarTravelPlace.ColumnName = "travel_place";
				colvarTravelPlace.DataType = DbType.String;
				colvarTravelPlace.MaxLength = 100;
				colvarTravelPlace.AutoIncrement = false;
				colvarTravelPlace.IsNullable = true;
				colvarTravelPlace.IsPrimaryKey = false;
				colvarTravelPlace.IsForeignKey = false;
				colvarTravelPlace.IsReadOnly = false;
				colvarTravelPlace.DefaultSetting = @"";
				colvarTravelPlace.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTravelPlace);

				TableSchema.TableColumn colvarEntrustSell = new TableSchema.TableColumn(schema);
				colvarEntrustSell.ColumnName = "entrust_sell";
				colvarEntrustSell.DataType = DbType.Int32;
				colvarEntrustSell.MaxLength = 0;
				colvarEntrustSell.AutoIncrement = false;
				colvarEntrustSell.IsNullable = true;
				colvarEntrustSell.IsPrimaryKey = false;
				colvarEntrustSell.IsForeignKey = false;
				colvarEntrustSell.IsReadOnly = false;
				colvarEntrustSell.DefaultSetting = @"";
				colvarEntrustSell.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEntrustSell);

				TableSchema.TableColumn colvarMultipleBranch = new TableSchema.TableColumn(schema);
				colvarMultipleBranch.ColumnName = "multiple_branch";
				colvarMultipleBranch.DataType = DbType.Boolean;
				colvarMultipleBranch.MaxLength = 0;
				colvarMultipleBranch.AutoIncrement = false;
				colvarMultipleBranch.IsNullable = true;
				colvarMultipleBranch.IsPrimaryKey = false;
				colvarMultipleBranch.IsForeignKey = false;
				colvarMultipleBranch.IsReadOnly = false;
				colvarMultipleBranch.DefaultSetting = @"";
				colvarMultipleBranch.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMultipleBranch);

				TableSchema.TableColumn colvarIsLongContract = new TableSchema.TableColumn(schema);
				colvarIsLongContract.ColumnName = "is_long_contract";
				colvarIsLongContract.DataType = DbType.Boolean;
				colvarIsLongContract.MaxLength = 0;
				colvarIsLongContract.AutoIncrement = false;
				colvarIsLongContract.IsNullable = true;
				colvarIsLongContract.IsPrimaryKey = false;
				colvarIsLongContract.IsForeignKey = false;
				colvarIsLongContract.IsReadOnly = false;
				colvarIsLongContract.DefaultSetting = @"";
				colvarIsLongContract.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsLongContract);

				TableSchema.TableColumn colvarCompleteCopy = new TableSchema.TableColumn(schema);
				colvarCompleteCopy.ColumnName = "complete_copy";
				colvarCompleteCopy.DataType = DbType.Boolean;
				colvarCompleteCopy.MaxLength = 0;
				colvarCompleteCopy.AutoIncrement = false;
				colvarCompleteCopy.IsNullable = true;
				colvarCompleteCopy.IsPrimaryKey = false;
				colvarCompleteCopy.IsForeignKey = false;
				colvarCompleteCopy.IsReadOnly = false;
				colvarCompleteCopy.DefaultSetting = @"";
				colvarCompleteCopy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompleteCopy);

				TableSchema.TableColumn colvarDevelopeSalesId = new TableSchema.TableColumn(schema);
				colvarDevelopeSalesId.ColumnName = "develope_sales_id";
				colvarDevelopeSalesId.DataType = DbType.Int32;
				colvarDevelopeSalesId.MaxLength = 0;
				colvarDevelopeSalesId.AutoIncrement = false;
				colvarDevelopeSalesId.IsNullable = true;
				colvarDevelopeSalesId.IsPrimaryKey = false;
				colvarDevelopeSalesId.IsForeignKey = false;
				colvarDevelopeSalesId.IsReadOnly = false;
				colvarDevelopeSalesId.DefaultSetting = @"";
				colvarDevelopeSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDevelopeSalesId);

				TableSchema.TableColumn colvarOperationSalesId = new TableSchema.TableColumn(schema);
				colvarOperationSalesId.ColumnName = "operation_sales_id";
				colvarOperationSalesId.DataType = DbType.Int32;
				colvarOperationSalesId.MaxLength = 0;
				colvarOperationSalesId.AutoIncrement = false;
				colvarOperationSalesId.IsNullable = true;
				colvarOperationSalesId.IsPrimaryKey = false;
				colvarOperationSalesId.IsForeignKey = false;
				colvarOperationSalesId.IsReadOnly = false;
				colvarOperationSalesId.DefaultSetting = @"";
				colvarOperationSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOperationSalesId);

				TableSchema.TableColumn colvarIsFreightsDeal = new TableSchema.TableColumn(schema);
				colvarIsFreightsDeal.ColumnName = "is_freights_deal";
				colvarIsFreightsDeal.DataType = DbType.Boolean;
				colvarIsFreightsDeal.MaxLength = 0;
				colvarIsFreightsDeal.AutoIncrement = false;
				colvarIsFreightsDeal.IsNullable = true;
				colvarIsFreightsDeal.IsPrimaryKey = false;
				colvarIsFreightsDeal.IsForeignKey = false;
				colvarIsFreightsDeal.IsReadOnly = false;
				colvarIsFreightsDeal.DefaultSetting = @"";
				colvarIsFreightsDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsFreightsDeal);

				TableSchema.TableColumn colvarCchannel = new TableSchema.TableColumn(schema);
				colvarCchannel.ColumnName = "cchannel";
				colvarCchannel.DataType = DbType.Boolean;
				colvarCchannel.MaxLength = 0;
				colvarCchannel.AutoIncrement = false;
				colvarCchannel.IsNullable = true;
				colvarCchannel.IsPrimaryKey = false;
				colvarCchannel.IsForeignKey = false;
				colvarCchannel.IsReadOnly = false;
				colvarCchannel.DefaultSetting = @"";
				colvarCchannel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCchannel);

				TableSchema.TableColumn colvarCchannelLink = new TableSchema.TableColumn(schema);
				colvarCchannelLink.ColumnName = "cchannel_link";
				colvarCchannelLink.DataType = DbType.AnsiString;
				colvarCchannelLink.MaxLength = 255;
				colvarCchannelLink.AutoIncrement = false;
				colvarCchannelLink.IsNullable = true;
				colvarCchannelLink.IsPrimaryKey = false;
				colvarCchannelLink.IsForeignKey = false;
				colvarCchannelLink.IsReadOnly = false;
				colvarCchannelLink.DefaultSetting = @"";
				colvarCchannelLink.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCchannelLink);

				TableSchema.TableColumn colvarIsExpiredDeal = new TableSchema.TableColumn(schema);
				colvarIsExpiredDeal.ColumnName = "is_expired_deal";
				colvarIsExpiredDeal.DataType = DbType.Boolean;
				colvarIsExpiredDeal.MaxLength = 0;
				colvarIsExpiredDeal.AutoIncrement = false;
				colvarIsExpiredDeal.IsNullable = true;
				colvarIsExpiredDeal.IsPrimaryKey = false;
				colvarIsExpiredDeal.IsForeignKey = false;
				colvarIsExpiredDeal.IsReadOnly = false;
				colvarIsExpiredDeal.DefaultSetting = @"";
				colvarIsExpiredDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsExpiredDeal);

                TableSchema.TableColumn colvarIsChosen = new TableSchema.TableColumn(schema);
                colvarIsChosen.ColumnName = "is_chosen";
                colvarIsChosen.DataType = DbType.Boolean;
                colvarIsChosen.MaxLength = 0;
                colvarIsChosen.AutoIncrement = false;
                colvarIsChosen.IsNullable = true;
                colvarIsChosen.IsPrimaryKey = false;
                colvarIsChosen.IsForeignKey = false;
                colvarIsChosen.IsReadOnly = false;
                colvarIsChosen.DefaultSetting = @"";
                colvarIsChosen.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsChosen);

                TableSchema.TableColumn colvarBookingSystemType = new TableSchema.TableColumn(schema);
				colvarBookingSystemType.ColumnName = "booking_system_type";
				colvarBookingSystemType.DataType = DbType.Int32;
				colvarBookingSystemType.MaxLength = 0;
				colvarBookingSystemType.AutoIncrement = false;
				colvarBookingSystemType.IsNullable = false;
				colvarBookingSystemType.IsPrimaryKey = false;
				colvarBookingSystemType.IsForeignKey = false;
				colvarBookingSystemType.IsReadOnly = false;
				colvarBookingSystemType.DefaultSetting = @"";
				colvarBookingSystemType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBookingSystemType);

				TableSchema.TableColumn colvarCouponSeparateDigits = new TableSchema.TableColumn(schema);
				colvarCouponSeparateDigits.ColumnName = "coupon_separate_digits";
				colvarCouponSeparateDigits.DataType = DbType.Int32;
				colvarCouponSeparateDigits.MaxLength = 0;
				colvarCouponSeparateDigits.AutoIncrement = false;
				colvarCouponSeparateDigits.IsNullable = false;
				colvarCouponSeparateDigits.IsPrimaryKey = false;
				colvarCouponSeparateDigits.IsForeignKey = false;
				colvarCouponSeparateDigits.IsReadOnly = false;
				colvarCouponSeparateDigits.DefaultSetting = @"";
				colvarCouponSeparateDigits.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponSeparateDigits);

				TableSchema.TableColumn colvarGroupOrderGuid = new TableSchema.TableColumn(schema);
				colvarGroupOrderGuid.ColumnName = "group_order_guid";
				colvarGroupOrderGuid.DataType = DbType.Guid;
				colvarGroupOrderGuid.MaxLength = 0;
				colvarGroupOrderGuid.AutoIncrement = false;
				colvarGroupOrderGuid.IsNullable = true;
				colvarGroupOrderGuid.IsPrimaryKey = false;
				colvarGroupOrderGuid.IsForeignKey = false;
				colvarGroupOrderGuid.IsReadOnly = false;
				colvarGroupOrderGuid.DefaultSetting = @"";
				colvarGroupOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupOrderGuid);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarCloseTime = new TableSchema.TableColumn(schema);
				colvarCloseTime.ColumnName = "close_time";
				colvarCloseTime.DataType = DbType.DateTime;
				colvarCloseTime.MaxLength = 0;
				colvarCloseTime.AutoIncrement = false;
				colvarCloseTime.IsNullable = true;
				colvarCloseTime.IsPrimaryKey = false;
				colvarCloseTime.IsForeignKey = false;
				colvarCloseTime.IsReadOnly = false;
				colvarCloseTime.DefaultSetting = @"";
				colvarCloseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCloseTime);

				TableSchema.TableColumn colvarGroupOrderStatus = new TableSchema.TableColumn(schema);
				colvarGroupOrderStatus.ColumnName = "group_order_status";
				colvarGroupOrderStatus.DataType = DbType.Int32;
				colvarGroupOrderStatus.MaxLength = 0;
				colvarGroupOrderStatus.AutoIncrement = false;
				colvarGroupOrderStatus.IsNullable = true;
				colvarGroupOrderStatus.IsPrimaryKey = false;
				colvarGroupOrderStatus.IsForeignKey = false;
				colvarGroupOrderStatus.IsReadOnly = false;
				colvarGroupOrderStatus.DefaultSetting = @"";
				colvarGroupOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupOrderStatus);

				TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
				colvarSlug.ColumnName = "slug";
				colvarSlug.DataType = DbType.String;
				colvarSlug.MaxLength = 100;
				colvarSlug.AutoIncrement = false;
				colvarSlug.IsNullable = true;
				colvarSlug.IsPrimaryKey = false;
				colvarSlug.IsForeignKey = false;
				colvarSlug.IsReadOnly = false;
				colvarSlug.DefaultSetting = @"";
				colvarSlug.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSlug);

				TableSchema.TableColumn colvarIntro = new TableSchema.TableColumn(schema);
				colvarIntro.ColumnName = "intro";
				colvarIntro.DataType = DbType.String;
				colvarIntro.MaxLength = 100;
				colvarIntro.AutoIncrement = false;
				colvarIntro.IsNullable = true;
				colvarIntro.IsPrimaryKey = false;
				colvarIntro.IsForeignKey = false;
				colvarIntro.IsReadOnly = false;
				colvarIntro.DefaultSetting = @"";
				colvarIntro.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntro);

				TableSchema.TableColumn colvarPriceDesc = new TableSchema.TableColumn(schema);
				colvarPriceDesc.ColumnName = "price_desc";
				colvarPriceDesc.DataType = DbType.String;
				colvarPriceDesc.MaxLength = 100;
				colvarPriceDesc.AutoIncrement = false;
				colvarPriceDesc.IsNullable = true;
				colvarPriceDesc.IsPrimaryKey = false;
				colvarPriceDesc.IsForeignKey = false;
				colvarPriceDesc.IsReadOnly = false;
				colvarPriceDesc.DefaultSetting = @"";
				colvarPriceDesc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPriceDesc);

				TableSchema.TableColumn colvarIsHideContent = new TableSchema.TableColumn(schema);
				colvarIsHideContent.ColumnName = "is_hide_content";
				colvarIsHideContent.DataType = DbType.Boolean;
				colvarIsHideContent.MaxLength = 0;
				colvarIsHideContent.AutoIncrement = false;
				colvarIsHideContent.IsNullable = true;
				colvarIsHideContent.IsPrimaryKey = false;
				colvarIsHideContent.IsForeignKey = false;
				colvarIsHideContent.IsReadOnly = false;
				colvarIsHideContent.DefaultSetting = @"";
				colvarIsHideContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsHideContent);

				TableSchema.TableColumn colvarContentName = new TableSchema.TableColumn(schema);
				colvarContentName.ColumnName = "content_name";
				colvarContentName.DataType = DbType.String;
				colvarContentName.MaxLength = 500;
				colvarContentName.AutoIncrement = false;
				colvarContentName.IsNullable = true;
				colvarContentName.IsPrimaryKey = false;
				colvarContentName.IsForeignKey = false;
				colvarContentName.IsReadOnly = false;
				colvarContentName.DefaultSetting = @"";
				colvarContentName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContentName);

				TableSchema.TableColumn colvarIsCloseMenu = new TableSchema.TableColumn(schema);
				colvarIsCloseMenu.ColumnName = "is_close_menu";
				colvarIsCloseMenu.DataType = DbType.Boolean;
				colvarIsCloseMenu.MaxLength = 0;
				colvarIsCloseMenu.AutoIncrement = false;
				colvarIsCloseMenu.IsNullable = true;
				colvarIsCloseMenu.IsPrimaryKey = false;
				colvarIsCloseMenu.IsForeignKey = false;
				colvarIsCloseMenu.IsReadOnly = false;
				colvarIsCloseMenu.DefaultSetting = @"";
				colvarIsCloseMenu.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCloseMenu);

				TableSchema.TableColumn colvarOrderedQuantity = new TableSchema.TableColumn(schema);
				colvarOrderedQuantity.ColumnName = "ordered_quantity";
				colvarOrderedQuantity.DataType = DbType.Int32;
				colvarOrderedQuantity.MaxLength = 0;
				colvarOrderedQuantity.AutoIncrement = false;
				colvarOrderedQuantity.IsNullable = true;
				colvarOrderedQuantity.IsPrimaryKey = false;
				colvarOrderedQuantity.IsForeignKey = false;
				colvarOrderedQuantity.IsReadOnly = false;
				colvarOrderedQuantity.DefaultSetting = @"";
				colvarOrderedQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderedQuantity);

				TableSchema.TableColumn colvarOrderedTotal = new TableSchema.TableColumn(schema);
				colvarOrderedTotal.ColumnName = "ordered_total";
				colvarOrderedTotal.DataType = DbType.Currency;
				colvarOrderedTotal.MaxLength = 0;
				colvarOrderedTotal.AutoIncrement = false;
				colvarOrderedTotal.IsNullable = true;
				colvarOrderedTotal.IsPrimaryKey = false;
				colvarOrderedTotal.IsForeignKey = false;
				colvarOrderedTotal.IsReadOnly = false;
				colvarOrderedTotal.DefaultSetting = @"";
				colvarOrderedTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderedTotal);

				TableSchema.TableColumn colvarOrderedIncludeRefundQuantity = new TableSchema.TableColumn(schema);
				colvarOrderedIncludeRefundQuantity.ColumnName = "ordered_include_refund_quantity";
				colvarOrderedIncludeRefundQuantity.DataType = DbType.Int32;
				colvarOrderedIncludeRefundQuantity.MaxLength = 0;
				colvarOrderedIncludeRefundQuantity.AutoIncrement = false;
				colvarOrderedIncludeRefundQuantity.IsNullable = true;
				colvarOrderedIncludeRefundQuantity.IsPrimaryKey = false;
				colvarOrderedIncludeRefundQuantity.IsForeignKey = false;
				colvarOrderedIncludeRefundQuantity.IsReadOnly = false;
				colvarOrderedIncludeRefundQuantity.DefaultSetting = @"";
				colvarOrderedIncludeRefundQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderedIncludeRefundQuantity);

				TableSchema.TableColumn colvarOrderedIncludeRefundTotal = new TableSchema.TableColumn(schema);
				colvarOrderedIncludeRefundTotal.ColumnName = "ordered_include_refund_total";
				colvarOrderedIncludeRefundTotal.DataType = DbType.Currency;
				colvarOrderedIncludeRefundTotal.MaxLength = 0;
				colvarOrderedIncludeRefundTotal.AutoIncrement = false;
				colvarOrderedIncludeRefundTotal.IsNullable = true;
				colvarOrderedIncludeRefundTotal.IsPrimaryKey = false;
				colvarOrderedIncludeRefundTotal.IsForeignKey = false;
				colvarOrderedIncludeRefundTotal.IsReadOnly = false;
				colvarOrderedIncludeRefundTotal.DefaultSetting = @"";
				colvarOrderedIncludeRefundTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderedIncludeRefundTotal);

				TableSchema.TableColumn colvarDealPromoTitle = new TableSchema.TableColumn(schema);
				colvarDealPromoTitle.ColumnName = "deal_promo_title";
				colvarDealPromoTitle.DataType = DbType.String;
				colvarDealPromoTitle.MaxLength = 50;
				colvarDealPromoTitle.AutoIncrement = false;
				colvarDealPromoTitle.IsNullable = true;
				colvarDealPromoTitle.IsPrimaryKey = false;
				colvarDealPromoTitle.IsForeignKey = false;
				colvarDealPromoTitle.IsReadOnly = false;
				colvarDealPromoTitle.DefaultSetting = @"";
				colvarDealPromoTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealPromoTitle);

				TableSchema.TableColumn colvarDealPromoDescription = new TableSchema.TableColumn(schema);
				colvarDealPromoDescription.ColumnName = "deal_promo_description";
				colvarDealPromoDescription.DataType = DbType.String;
				colvarDealPromoDescription.MaxLength = 2147483647;
				colvarDealPromoDescription.AutoIncrement = false;
				colvarDealPromoDescription.IsNullable = true;
				colvarDealPromoDescription.IsPrimaryKey = false;
				colvarDealPromoDescription.IsForeignKey = false;
				colvarDealPromoDescription.IsReadOnly = false;
				colvarDealPromoDescription.DefaultSetting = @"";
				colvarDealPromoDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealPromoDescription);

				TableSchema.TableColumn colvarDealPromoImage = new TableSchema.TableColumn(schema);
				colvarDealPromoImage.ColumnName = "deal_promo_image";
				colvarDealPromoImage.DataType = DbType.String;
				colvarDealPromoImage.MaxLength = 120;
				colvarDealPromoImage.AutoIncrement = false;
				colvarDealPromoImage.IsNullable = true;
				colvarDealPromoImage.IsPrimaryKey = false;
				colvarDealPromoImage.IsForeignKey = false;
				colvarDealPromoImage.IsReadOnly = false;
				colvarDealPromoImage.DefaultSetting = @"";
				colvarDealPromoImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealPromoImage);

				TableSchema.TableColumn colvarPdfItemName = new TableSchema.TableColumn(schema);
				colvarPdfItemName.ColumnName = "pdf_item_name";
				colvarPdfItemName.DataType = DbType.String;
				colvarPdfItemName.MaxLength = 750;
				colvarPdfItemName.AutoIncrement = false;
				colvarPdfItemName.IsNullable = true;
				colvarPdfItemName.IsPrimaryKey = false;
				colvarPdfItemName.IsForeignKey = false;
				colvarPdfItemName.IsReadOnly = false;
				colvarPdfItemName.DefaultSetting = @"";
				colvarPdfItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPdfItemName);

				TableSchema.TableColumn colvarMainBid = new TableSchema.TableColumn(schema);
				colvarMainBid.ColumnName = "main_bid";
				colvarMainBid.DataType = DbType.Guid;
				colvarMainBid.MaxLength = 0;
				colvarMainBid.AutoIncrement = false;
				colvarMainBid.IsNullable = true;
				colvarMainBid.IsPrimaryKey = false;
				colvarMainBid.IsForeignKey = false;
				colvarMainBid.IsReadOnly = false;
				colvarMainBid.DefaultSetting = @"";
				colvarMainBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainBid);

				TableSchema.TableColumn colvarComboDealSeq = new TableSchema.TableColumn(schema);
				colvarComboDealSeq.ColumnName = "combo_deal_seq";
				colvarComboDealSeq.DataType = DbType.Int32;
				colvarComboDealSeq.MaxLength = 0;
				colvarComboDealSeq.AutoIncrement = false;
				colvarComboDealSeq.IsNullable = true;
				colvarComboDealSeq.IsPrimaryKey = false;
				colvarComboDealSeq.IsForeignKey = false;
				colvarComboDealSeq.IsReadOnly = false;
				colvarComboDealSeq.DefaultSetting = @"";
				colvarComboDealSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarComboDealSeq);

				TableSchema.TableColumn colvarGroupCouponDealType = new TableSchema.TableColumn(schema);
				colvarGroupCouponDealType.ColumnName = "group_coupon_deal_type";
				colvarGroupCouponDealType.DataType = DbType.Int32;
				colvarGroupCouponDealType.MaxLength = 0;
				colvarGroupCouponDealType.AutoIncrement = false;
				colvarGroupCouponDealType.IsNullable = true;
				colvarGroupCouponDealType.IsPrimaryKey = false;
				colvarGroupCouponDealType.IsForeignKey = false;
				colvarGroupCouponDealType.IsReadOnly = false;
				colvarGroupCouponDealType.DefaultSetting = @"";
				colvarGroupCouponDealType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupCouponDealType);

				TableSchema.TableColumn colvarCategoryList = new TableSchema.TableColumn(schema);
				colvarCategoryList.ColumnName = "category_list";
				colvarCategoryList.DataType = DbType.AnsiString;
				colvarCategoryList.MaxLength = 2000;
				colvarCategoryList.AutoIncrement = false;
				colvarCategoryList.IsNullable = true;
				colvarCategoryList.IsPrimaryKey = false;
				colvarCategoryList.IsForeignKey = false;
				colvarCategoryList.IsReadOnly = false;
				colvarCategoryList.DefaultSetting = @"";
				colvarCategoryList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryList);

				TableSchema.TableColumn colvarConsignment = new TableSchema.TableColumn(schema);
				colvarConsignment.ColumnName = "consignment";
				colvarConsignment.DataType = DbType.Boolean;
				colvarConsignment.MaxLength = 0;
				colvarConsignment.AutoIncrement = false;
				colvarConsignment.IsNullable = true;
				colvarConsignment.IsPrimaryKey = false;
				colvarConsignment.IsForeignKey = false;
				colvarConsignment.IsReadOnly = false;
				colvarConsignment.DefaultSetting = @"";
				colvarConsignment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsignment);

				TableSchema.TableColumn colvarFreightsId = new TableSchema.TableColumn(schema);
				colvarFreightsId.ColumnName = "freights_id";
				colvarFreightsId.DataType = DbType.Int32;
				colvarFreightsId.MaxLength = 0;
				colvarFreightsId.AutoIncrement = false;
				colvarFreightsId.IsNullable = true;
				colvarFreightsId.IsPrimaryKey = false;
				colvarFreightsId.IsForeignKey = false;
				colvarFreightsId.IsReadOnly = false;
				colvarFreightsId.DefaultSetting = @"";
				colvarFreightsId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFreightsId);

				TableSchema.TableColumn colvarBankId = new TableSchema.TableColumn(schema);
				colvarBankId.ColumnName = "bank_id";
				colvarBankId.DataType = DbType.Int32;
				colvarBankId.MaxLength = 0;
				colvarBankId.AutoIncrement = false;
				colvarBankId.IsNullable = true;
				colvarBankId.IsPrimaryKey = false;
				colvarBankId.IsForeignKey = false;
				colvarBankId.IsReadOnly = false;
				colvarBankId.DefaultSetting = @"";
				colvarBankId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBankId);

				TableSchema.TableColumn colvarIsBankDeal = new TableSchema.TableColumn(schema);
				colvarIsBankDeal.ColumnName = "is_bank_deal";
				colvarIsBankDeal.DataType = DbType.Boolean;
				colvarIsBankDeal.MaxLength = 0;
				colvarIsBankDeal.AutoIncrement = false;
				colvarIsBankDeal.IsNullable = true;
				colvarIsBankDeal.IsPrimaryKey = false;
				colvarIsBankDeal.IsForeignKey = false;
				colvarIsBankDeal.IsReadOnly = false;
				colvarIsBankDeal.DefaultSetting = @"";
				colvarIsBankDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsBankDeal);

				TableSchema.TableColumn colvarAllowGuestBuy = new TableSchema.TableColumn(schema);
				colvarAllowGuestBuy.ColumnName = "allow_guest_buy";
				colvarAllowGuestBuy.DataType = DbType.Boolean;
				colvarAllowGuestBuy.MaxLength = 0;
				colvarAllowGuestBuy.AutoIncrement = false;
				colvarAllowGuestBuy.IsNullable = true;
				colvarAllowGuestBuy.IsPrimaryKey = false;
				colvarAllowGuestBuy.IsForeignKey = false;
				colvarAllowGuestBuy.IsReadOnly = false;
				colvarAllowGuestBuy.DefaultSetting = @"";
				colvarAllowGuestBuy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAllowGuestBuy);

				TableSchema.TableColumn colvarDiscountUseType = new TableSchema.TableColumn(schema);
				colvarDiscountUseType.ColumnName = "discount_use_type";
				colvarDiscountUseType.DataType = DbType.Boolean;
				colvarDiscountUseType.MaxLength = 0;
				colvarDiscountUseType.AutoIncrement = false;
				colvarDiscountUseType.IsNullable = true;
				colvarDiscountUseType.IsPrimaryKey = false;
				colvarDiscountUseType.IsForeignKey = false;
				colvarDiscountUseType.IsReadOnly = false;
				colvarDiscountUseType.DefaultSetting = @"";
				colvarDiscountUseType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountUseType);

				TableSchema.TableColumn colvarVerifyActionType = new TableSchema.TableColumn(schema);
				colvarVerifyActionType.ColumnName = "verify_action_type";
				colvarVerifyActionType.DataType = DbType.Int32;
				colvarVerifyActionType.MaxLength = 0;
				colvarVerifyActionType.AutoIncrement = false;
				colvarVerifyActionType.IsNullable = true;
				colvarVerifyActionType.IsPrimaryKey = false;
				colvarVerifyActionType.IsForeignKey = false;
				colvarVerifyActionType.IsReadOnly = false;
				colvarVerifyActionType.DefaultSetting = @"";
				colvarVerifyActionType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifyActionType);

				TableSchema.TableColumn colvarIsHouseNewVer = new TableSchema.TableColumn(schema);
				colvarIsHouseNewVer.ColumnName = "is_house_new_ver";
				colvarIsHouseNewVer.DataType = DbType.Boolean;
				colvarIsHouseNewVer.MaxLength = 0;
				colvarIsHouseNewVer.AutoIncrement = false;
				colvarIsHouseNewVer.IsNullable = true;
				colvarIsHouseNewVer.IsPrimaryKey = false;
				colvarIsHouseNewVer.IsForeignKey = false;
				colvarIsHouseNewVer.IsReadOnly = false;
				colvarIsHouseNewVer.DefaultSetting = @"";
				colvarIsHouseNewVer.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsHouseNewVer);

				TableSchema.TableColumn colvarFreightAmount = new TableSchema.TableColumn(schema);
				colvarFreightAmount.ColumnName = "freight_amount";
				colvarFreightAmount.DataType = DbType.Currency;
				colvarFreightAmount.MaxLength = 0;
				colvarFreightAmount.AutoIncrement = false;
				colvarFreightAmount.IsNullable = false;
				colvarFreightAmount.IsPrimaryKey = false;
				colvarFreightAmount.IsForeignKey = false;
				colvarFreightAmount.IsReadOnly = false;
				colvarFreightAmount.DefaultSetting = @"";
				colvarFreightAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFreightAmount);

				TableSchema.TableColumn colvarEnableDelivery = new TableSchema.TableColumn(schema);
				colvarEnableDelivery.ColumnName = "enable_delivery";
				colvarEnableDelivery.DataType = DbType.Boolean;
				colvarEnableDelivery.MaxLength = 0;
				colvarEnableDelivery.AutoIncrement = false;
				colvarEnableDelivery.IsNullable = false;
				colvarEnableDelivery.IsPrimaryKey = false;
				colvarEnableDelivery.IsForeignKey = false;
				colvarEnableDelivery.IsReadOnly = false;
				colvarEnableDelivery.DefaultSetting = @"";
				colvarEnableDelivery.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnableDelivery);

				TableSchema.TableColumn colvarIspQuantityLimit = new TableSchema.TableColumn(schema);
				colvarIspQuantityLimit.ColumnName = "isp_quantity_limit";
				colvarIspQuantityLimit.DataType = DbType.Int32;
				colvarIspQuantityLimit.MaxLength = 0;
				colvarIspQuantityLimit.AutoIncrement = false;
				colvarIspQuantityLimit.IsNullable = false;
				colvarIspQuantityLimit.IsPrimaryKey = false;
				colvarIspQuantityLimit.IsForeignKey = false;
				colvarIspQuantityLimit.IsReadOnly = false;
				colvarIspQuantityLimit.DefaultSetting = @"";
				colvarIspQuantityLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIspQuantityLimit);

				TableSchema.TableColumn colvarEnableIsp = new TableSchema.TableColumn(schema);
				colvarEnableIsp.ColumnName = "enable_isp";
				colvarEnableIsp.DataType = DbType.Boolean;
				colvarEnableIsp.MaxLength = 0;
				colvarEnableIsp.AutoIncrement = false;
				colvarEnableIsp.IsNullable = false;
				colvarEnableIsp.IsPrimaryKey = false;
				colvarEnableIsp.IsForeignKey = false;
				colvarEnableIsp.IsReadOnly = false;
				colvarEnableIsp.DefaultSetting = @"";
				colvarEnableIsp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnableIsp);

				TableSchema.TableColumn colvarRemoveBgPic = new TableSchema.TableColumn(schema);
				colvarRemoveBgPic.ColumnName = "remove_bg_pic";
				colvarRemoveBgPic.DataType = DbType.String;
				colvarRemoveBgPic.MaxLength = 200;
				colvarRemoveBgPic.AutoIncrement = false;
				colvarRemoveBgPic.IsNullable = true;
				colvarRemoveBgPic.IsPrimaryKey = false;
				colvarRemoveBgPic.IsForeignKey = false;
				colvarRemoveBgPic.IsReadOnly = false;
				colvarRemoveBgPic.DefaultSetting = @"";
				colvarRemoveBgPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemoveBgPic);

				TableSchema.TableColumn colvarDiscountPrice = new TableSchema.TableColumn(schema);
				colvarDiscountPrice.ColumnName = "discount_price";
				colvarDiscountPrice.DataType = DbType.Decimal;
				colvarDiscountPrice.MaxLength = 0;
				colvarDiscountPrice.AutoIncrement = false;
				colvarDiscountPrice.IsNullable = true;
				colvarDiscountPrice.IsPrimaryKey = false;
				colvarDiscountPrice.IsForeignKey = false;
				colvarDiscountPrice.IsReadOnly = false;
				colvarDiscountPrice.DefaultSetting = @"";
				colvarDiscountPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountPrice);

				TableSchema.TableColumn colvarIsGame = new TableSchema.TableColumn(schema);
				colvarIsGame.ColumnName = "is_game";
				colvarIsGame.DataType = DbType.Boolean;
				colvarIsGame.MaxLength = 0;
				colvarIsGame.AutoIncrement = false;
				colvarIsGame.IsNullable = true;
				colvarIsGame.IsPrimaryKey = false;
				colvarIsGame.IsForeignKey = false;
				colvarIsGame.IsReadOnly = false;
				colvarIsGame.DefaultSetting = @"";
				colvarIsGame.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsGame);

			    TableSchema.TableColumn colvarExpireRedirectDisplay = new TableSchema.TableColumn(schema);
			    colvarExpireRedirectDisplay.ColumnName = "expire_redirect_display";
			    colvarExpireRedirectDisplay.DataType = DbType.Int32;
			    colvarExpireRedirectDisplay.MaxLength = 0;
			    colvarExpireRedirectDisplay.AutoIncrement = false;
			    colvarExpireRedirectDisplay.IsNullable = true;
			    colvarExpireRedirectDisplay.IsPrimaryKey = false;
			    colvarExpireRedirectDisplay.IsForeignKey = false;
			    colvarExpireRedirectDisplay.IsReadOnly = false;
			    colvarExpireRedirectDisplay.DefaultSetting = @"";
			    colvarExpireRedirectDisplay.ForeignKeyTableName = "";
			    schema.Columns.Add(colvarExpireRedirectDisplay);

			    TableSchema.TableColumn colvarExpireRedirectUrl = new TableSchema.TableColumn(schema);
			    colvarExpireRedirectUrl.ColumnName = "expire_redirect_url";
			    colvarExpireRedirectUrl.DataType = DbType.String;
			    colvarExpireRedirectUrl.MaxLength = 500;
			    colvarExpireRedirectUrl.AutoIncrement = false;
			    colvarExpireRedirectUrl.IsNullable = true;
			    colvarExpireRedirectUrl.IsPrimaryKey = false;
			    colvarExpireRedirectUrl.IsForeignKey = false;
			    colvarExpireRedirectUrl.IsReadOnly = false;
			    colvarExpireRedirectUrl.DefaultSetting = @"";
			    colvarExpireRedirectUrl.ForeignKeyTableName = "";
			    schema.Columns.Add(colvarExpireRedirectUrl);

			    TableSchema.TableColumn colvarUseExpireRedirectUrlAsCanonical = new TableSchema.TableColumn(schema);
			    colvarUseExpireRedirectUrlAsCanonical.ColumnName = "use_expire_redirect_url_as_canonical";
			    colvarUseExpireRedirectUrlAsCanonical.DataType = DbType.Boolean;
			    colvarUseExpireRedirectUrlAsCanonical.MaxLength = 0;
			    colvarUseExpireRedirectUrlAsCanonical.AutoIncrement = false;
			    colvarUseExpireRedirectUrlAsCanonical.IsNullable = true;
			    colvarUseExpireRedirectUrlAsCanonical.IsPrimaryKey = false;
			    colvarUseExpireRedirectUrlAsCanonical.IsForeignKey = false;
			    colvarUseExpireRedirectUrlAsCanonical.IsReadOnly = false;
			    colvarUseExpireRedirectUrlAsCanonical.DefaultSetting = @"";
			    colvarUseExpireRedirectUrlAsCanonical.ForeignKeyTableName = "";
			    schema.Columns.Add(colvarUseExpireRedirectUrlAsCanonical);

                TableSchema.TableColumn colvarIsWms = new TableSchema.TableColumn(schema);
				colvarIsWms.ColumnName = "is_wms";
				colvarIsWms.DataType = DbType.Boolean;
				colvarIsWms.MaxLength = 0;
				colvarIsWms.AutoIncrement = false;
				colvarIsWms.IsNullable = false;
				colvarIsWms.IsPrimaryKey = false;
				colvarIsWms.IsForeignKey = false;
				colvarIsWms.IsReadOnly = false;
				colvarIsWms.DefaultSetting = @"0";
				colvarIsWms.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsWms);

                TableSchema.TableColumn colvarAgentChannels = new TableSchema.TableColumn(schema);
                colvarAgentChannels.ColumnName = "agent_channels";
                colvarAgentChannels.DataType = DbType.AnsiString;
                colvarAgentChannels.MaxLength = 50;
                colvarAgentChannels.AutoIncrement = false;
                colvarAgentChannels.IsNullable = true;
                colvarAgentChannels.IsPrimaryKey = false;
                colvarAgentChannels.IsForeignKey = false;
                colvarAgentChannels.IsReadOnly = false;
                colvarAgentChannels.DefaultSetting = @"0";
                colvarAgentChannels.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAgentChannels);

                TableSchema.TableColumn colvarIsChannelGift = new TableSchema.TableColumn(schema);
                colvarIsChannelGift.ColumnName = "is_channel_gift";
                colvarIsChannelGift.DataType = DbType.Boolean;
                colvarIsChannelGift.MaxLength = 0;
                colvarIsChannelGift.AutoIncrement = false;
                colvarIsChannelGift.IsNullable = true;
                colvarIsChannelGift.IsPrimaryKey = false;
                colvarIsChannelGift.IsForeignKey = false;
                colvarIsChannelGift.IsReadOnly = false;
                colvarIsChannelGift.DefaultSetting = @"0";
                colvarIsChannelGift.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsChannelGift);


                BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_ppon_deal",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("BusinessHourOrderTimeS")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeS
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeS); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeS, value); }
		}

		[XmlAttribute("BusinessHourOrderTimeE")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeE
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeE); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeE, value); }
		}

		[XmlAttribute("BusinessHourDeliverTimeS")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeS
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeS); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeS, value); }
		}

		[XmlAttribute("BusinessHourDeliverTimeE")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeE
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeE); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeE, value); }
		}

		[XmlAttribute("BusinessHourOrderMinimum")]
		[Bindable(true)]
		public decimal BusinessHourOrderMinimum
		{
			get { return GetColumnValue<decimal>(Columns.BusinessHourOrderMinimum); }
			set { SetColumnValue(Columns.BusinessHourOrderMinimum, value); }
		}

		[XmlAttribute("BusinessHourStatus")]
		[Bindable(true)]
		public int BusinessHourStatus
		{
			get { return GetColumnValue<int>(Columns.BusinessHourStatus); }
			set { SetColumnValue(Columns.BusinessHourStatus, value); }
		}

		[XmlAttribute("OrderTotalLimit")]
		[Bindable(true)]
		public decimal? OrderTotalLimit
		{
			get { return GetColumnValue<decimal?>(Columns.OrderTotalLimit); }
			set { SetColumnValue(Columns.OrderTotalLimit, value); }
		}

		[XmlAttribute("BusinessHourDeliveryCharge")]
		[Bindable(true)]
		public decimal BusinessHourDeliveryCharge
		{
			get { return GetColumnValue<decimal>(Columns.BusinessHourDeliveryCharge); }
			set { SetColumnValue(Columns.BusinessHourDeliveryCharge, value); }
		}

		[XmlAttribute("BusinessHourAtmMaximum")]
		[Bindable(true)]
		public int BusinessHourAtmMaximum
		{
			get { return GetColumnValue<int>(Columns.BusinessHourAtmMaximum); }
			set { SetColumnValue(Columns.BusinessHourAtmMaximum, value); }
		}

		[XmlAttribute("PageTitle")]
		[Bindable(true)]
		public string PageTitle
		{
			get { return GetColumnValue<string>(Columns.PageTitle); }
			set { SetColumnValue(Columns.PageTitle, value); }
		}

		[XmlAttribute("PageDesc")]
		[Bindable(true)]
		public string PageDesc
		{
			get { return GetColumnValue<string>(Columns.PageDesc); }
			set { SetColumnValue(Columns.PageDesc, value); }
		}

		[XmlAttribute("PicAlt")]
		[Bindable(true)]
		public string PicAlt
		{
			get { return GetColumnValue<string>(Columns.PicAlt); }
			set { SetColumnValue(Columns.PicAlt, value); }
		}

		[XmlAttribute("SettlementTime")]
		[Bindable(true)]
		public DateTime? SettlementTime
		{
			get { return GetColumnValue<DateTime?>(Columns.SettlementTime); }
			set { SetColumnValue(Columns.SettlementTime, value); }
		}

		[XmlAttribute("ChangedExpireDate")]
		[Bindable(true)]
		public DateTime? ChangedExpireDate
		{
			get { return GetColumnValue<DateTime?>(Columns.ChangedExpireDate); }
			set { SetColumnValue(Columns.ChangedExpireDate, value); }
		}

		[XmlAttribute("SellerId")]
		[Bindable(true)]
		public string SellerId
		{
			get { return GetColumnValue<string>(Columns.SellerId); }
			set { SetColumnValue(Columns.SellerId, value); }
		}

		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}

		[XmlAttribute("SellerTel")]
		[Bindable(true)]
		public string SellerTel
		{
			get { return GetColumnValue<string>(Columns.SellerTel); }
			set { SetColumnValue(Columns.SellerTel, value); }
		}

		[XmlAttribute("SellerEmail")]
		[Bindable(true)]
		public string SellerEmail
		{
			get { return GetColumnValue<string>(Columns.SellerEmail); }
			set { SetColumnValue(Columns.SellerEmail, value); }
		}

		[XmlAttribute("SellerAddress")]
		[Bindable(true)]
		public string SellerAddress
		{
			get { return GetColumnValue<string>(Columns.SellerAddress); }
			set { SetColumnValue(Columns.SellerAddress, value); }
		}

		[XmlAttribute("Department")]
		[Bindable(true)]
		public int Department
		{
			get { return GetColumnValue<int>(Columns.Department); }
			set { SetColumnValue(Columns.Department, value); }
		}

		[XmlAttribute("SellerCityId")]
		[Bindable(true)]
		public int SellerCityId
		{
			get { return GetColumnValue<int>(Columns.SellerCityId); }
			set { SetColumnValue(Columns.SellerCityId, value); }
		}

		[XmlAttribute("IsCloseDown")]
		[Bindable(true)]
		public bool IsCloseDown
		{
			get { return GetColumnValue<bool>(Columns.IsCloseDown); }
			set { SetColumnValue(Columns.IsCloseDown, value); }
		}

		[XmlAttribute("CloseDownDate")]
		[Bindable(true)]
		public DateTime? CloseDownDate
		{
			get { return GetColumnValue<DateTime?>(Columns.CloseDownDate); }
			set { SetColumnValue(Columns.CloseDownDate, value); }
		}

		[XmlAttribute("CompanyId")]
		[Bindable(true)]
		public string CompanyId
		{
			get { return GetColumnValue<string>(Columns.CompanyId); }
			set { SetColumnValue(Columns.CompanyId, value); }
		}

		[XmlAttribute("CompanyName")]
		[Bindable(true)]
		public string CompanyName
		{
			get { return GetColumnValue<string>(Columns.CompanyName); }
			set { SetColumnValue(Columns.CompanyName, value); }
		}

		[XmlAttribute("ItemGuid")]
		[Bindable(true)]
		public Guid ItemGuid
		{
			get { return GetColumnValue<Guid>(Columns.ItemGuid); }
			set { SetColumnValue(Columns.ItemGuid, value); }
		}

		[XmlAttribute("MaxItemCount")]
		[Bindable(true)]
		public int? MaxItemCount
		{
			get { return GetColumnValue<int?>(Columns.MaxItemCount); }
			set { SetColumnValue(Columns.MaxItemCount, value); }
		}

		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}

		[XmlAttribute("ItemOrigPrice")]
		[Bindable(true)]
		public decimal ItemOrigPrice
		{
			get { return GetColumnValue<decimal>(Columns.ItemOrigPrice); }
			set { SetColumnValue(Columns.ItemOrigPrice, value); }
		}

		[XmlAttribute("ItemPrice")]
		[Bindable(true)]
		public decimal ItemPrice
		{
			get { return GetColumnValue<decimal>(Columns.ItemPrice); }
			set { SetColumnValue(Columns.ItemPrice, value); }
		}

		[XmlAttribute("ItemDefaultDailyAmount")]
		[Bindable(true)]
		public int? ItemDefaultDailyAmount
		{
			get { return GetColumnValue<int?>(Columns.ItemDefaultDailyAmount); }
			set { SetColumnValue(Columns.ItemDefaultDailyAmount, value); }
		}

		[XmlAttribute("EventName")]
		[Bindable(true)]
		public string EventName
		{
			get { return GetColumnValue<string>(Columns.EventName); }
			set { SetColumnValue(Columns.EventName, value); }
		}

		[XmlAttribute("EventTitle")]
		[Bindable(true)]
		public string EventTitle
		{
			get { return GetColumnValue<string>(Columns.EventTitle); }
			set { SetColumnValue(Columns.EventTitle, value); }
		}

		[XmlAttribute("EventImagePath")]
		[Bindable(true)]
		public string EventImagePath
		{
			get { return GetColumnValue<string>(Columns.EventImagePath); }
			set { SetColumnValue(Columns.EventImagePath, value); }
		}

        [XmlAttribute("EventOriImagePath")]
        [Bindable(true)]
        public string EventOriImagePath
        {
            get { return GetColumnValue<string>("event_ori_image_path"); }
            set { SetColumnValue("event_ori_image_path", value); }
        }

        [XmlAttribute("Introduction")]
		[Bindable(true)]
		public string Introduction
		{
			get { return GetColumnValue<string>(Columns.Introduction); }
			set { SetColumnValue(Columns.Introduction, value); }
		}

		[XmlAttribute("Restrictions")]
		[Bindable(true)]
		public string Restrictions
		{
			get { return GetColumnValue<string>(Columns.Restrictions); }
			set { SetColumnValue(Columns.Restrictions, value); }
		}

		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}

		[XmlAttribute("AppDescription")]
		[Bindable(true)]
		public string AppDescription
		{
			get { return GetColumnValue<string>(Columns.AppDescription); }
			set { SetColumnValue(Columns.AppDescription, value); }
		}

		[XmlAttribute("ReferenceText")]
		[Bindable(true)]
		public string ReferenceText
		{
			get { return GetColumnValue<string>(Columns.ReferenceText); }
			set { SetColumnValue(Columns.ReferenceText, value); }
		}

		[XmlAttribute("CouponUsage")]
		[Bindable(true)]
		public string CouponUsage
		{
			get { return GetColumnValue<string>(Columns.CouponUsage); }
			set { SetColumnValue(Columns.CouponUsage, value); }
		}

		[XmlAttribute("Remark")]
		[Bindable(true)]
		public string Remark
		{
			get { return GetColumnValue<string>(Columns.Remark); }
			set { SetColumnValue(Columns.Remark, value); }
		}

		[XmlAttribute("Reasons")]
		[Bindable(true)]
		public string Reasons
		{
			get { return GetColumnValue<string>(Columns.Reasons); }
			set { SetColumnValue(Columns.Reasons, value); }
		}

		[XmlAttribute("SubjectName")]
		[Bindable(true)]
		public string SubjectName
		{
			get { return GetColumnValue<string>(Columns.SubjectName); }
			set { SetColumnValue(Columns.SubjectName, value); }
		}

		[XmlAttribute("EventSpecialImagePath")]
		[Bindable(true)]
		public string EventSpecialImagePath
		{
			get { return GetColumnValue<string>(Columns.EventSpecialImagePath); }
			set { SetColumnValue(Columns.EventSpecialImagePath, value); }
		}

		[XmlAttribute("AppDealPic")]
		[Bindable(true)]
		public string AppDealPic
		{
			get { return GetColumnValue<string>(Columns.AppDealPic); }
			set { SetColumnValue(Columns.AppDealPic, value); }
		}

		[XmlAttribute("AppTitle")]
		[Bindable(true)]
		public string AppTitle
		{
			get { return GetColumnValue<string>(Columns.AppTitle); }
			set { SetColumnValue(Columns.AppTitle, value); }
		}

		[XmlAttribute("Availability")]
		[Bindable(true)]
		public string Availability
		{
			get { return GetColumnValue<string>(Columns.Availability); }
			set { SetColumnValue(Columns.Availability, value); }
		}

		[XmlAttribute("EventModifyTime")]
		[Bindable(true)]
		public DateTime EventModifyTime
		{
			get { return GetColumnValue<DateTime>(Columns.EventModifyTime); }
			set { SetColumnValue(Columns.EventModifyTime, value); }
		}

		[XmlAttribute("ProductSpec")]
		[Bindable(true)]
		public string ProductSpec
		{
			get { return GetColumnValue<string>(Columns.ProductSpec); }
			set { SetColumnValue(Columns.ProductSpec, value); }
		}

		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int? DeliveryType
		{
			get { return GetColumnValue<int?>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}

		[XmlAttribute("ShoppingCart")]
		[Bindable(true)]
		public bool? ShoppingCart
		{
			get { return GetColumnValue<bool?>(Columns.ShoppingCart); }
			set { SetColumnValue(Columns.ShoppingCart, value); }
		}

		[XmlAttribute("ComboPackCount")]
		[Bindable(true)]
		public int? ComboPackCount
		{
			get { return GetColumnValue<int?>(Columns.ComboPackCount); }
			set { SetColumnValue(Columns.ComboPackCount, value); }
		}

		[XmlAttribute("IsTravelcard")]
		[Bindable(true)]
		public bool? IsTravelcard
		{
			get { return GetColumnValue<bool?>(Columns.IsTravelcard); }
			set { SetColumnValue(Columns.IsTravelcard, value); }
		}

		[XmlAttribute("QuantityMultiplier")]
		[Bindable(true)]
		public int? QuantityMultiplier
		{
			get { return GetColumnValue<int?>(Columns.QuantityMultiplier); }
			set { SetColumnValue(Columns.QuantityMultiplier, value); }
		}

		[XmlAttribute("UniqueId")]
		[Bindable(true)]
		public int? UniqueId
		{
			get { return GetColumnValue<int?>(Columns.UniqueId); }
			set { SetColumnValue(Columns.UniqueId, value); }
		}

		[XmlAttribute("IsDailyRestriction")]
		[Bindable(true)]
		public bool? IsDailyRestriction
		{
			get { return GetColumnValue<bool?>(Columns.IsDailyRestriction); }
			set { SetColumnValue(Columns.IsDailyRestriction, value); }
		}

		[XmlAttribute("IsContinuedQuantity")]
		[Bindable(true)]
		public bool? IsContinuedQuantity
		{
			get { return GetColumnValue<bool?>(Columns.IsContinuedQuantity); }
			set { SetColumnValue(Columns.IsContinuedQuantity, value); }
		}

		[XmlAttribute("ContinuedQuantity")]
		[Bindable(true)]
		public int? ContinuedQuantity
		{
			get { return GetColumnValue<int?>(Columns.ContinuedQuantity); }
			set { SetColumnValue(Columns.ContinuedQuantity, value); }
		}

		[XmlAttribute("CityList")]
		[Bindable(true)]
		public string CityList
		{
			get { return GetColumnValue<string>(Columns.CityList); }
			set { SetColumnValue(Columns.CityList, value); }
		}

		[XmlAttribute("ActivityUrl")]
		[Bindable(true)]
		public string ActivityUrl
		{
			get { return GetColumnValue<string>(Columns.ActivityUrl); }
			set { SetColumnValue(Columns.ActivityUrl, value); }
		}

		[XmlAttribute("LabelIconList")]
		[Bindable(true)]
		public string LabelIconList
		{
			get { return GetColumnValue<string>(Columns.LabelIconList); }
			set { SetColumnValue(Columns.LabelIconList, value); }
		}

		[XmlAttribute("LabelTagList")]
		[Bindable(true)]
		public string LabelTagList
		{
			get { return GetColumnValue<string>(Columns.LabelTagList); }
			set { SetColumnValue(Columns.LabelTagList, value); }
		}

		[XmlAttribute("CouponCodeType")]
		[Bindable(true)]
		public int? CouponCodeType
		{
			get { return GetColumnValue<int?>(Columns.CouponCodeType); }
			set { SetColumnValue(Columns.CouponCodeType, value); }
		}

		[XmlAttribute("PinType")]
		[Bindable(true)]
		public int? PinType
		{
			get { return GetColumnValue<int?>(Columns.PinType); }
			set { SetColumnValue(Columns.PinType, value); }
		}

		[XmlAttribute("CustomTag")]
		[Bindable(true)]
		public string CustomTag
		{
			get { return GetColumnValue<string>(Columns.CustomTag); }
			set { SetColumnValue(Columns.CustomTag, value); }
		}

		[XmlAttribute("ExchangePrice")]
		[Bindable(true)]
		public decimal? ExchangePrice
		{
			get { return GetColumnValue<decimal?>(Columns.ExchangePrice); }
			set { SetColumnValue(Columns.ExchangePrice, value); }
		}

		[XmlAttribute("IsQuantityMultiplier")]
		[Bindable(true)]
		public bool? IsQuantityMultiplier
		{
			get { return GetColumnValue<bool?>(Columns.IsQuantityMultiplier); }
			set { SetColumnValue(Columns.IsQuantityMultiplier, value); }
		}

		[XmlAttribute("IsAveragePrice")]
		[Bindable(true)]
		public bool? IsAveragePrice
		{
			get { return GetColumnValue<bool?>(Columns.IsAveragePrice); }
			set { SetColumnValue(Columns.IsAveragePrice, value); }
		}

		[XmlAttribute("IsZeroActivityShowCoupon")]
		[Bindable(true)]
		public bool? IsZeroActivityShowCoupon
		{
			get { return GetColumnValue<bool?>(Columns.IsZeroActivityShowCoupon); }
			set { SetColumnValue(Columns.IsZeroActivityShowCoupon, value); }
		}

		[XmlAttribute("DealAccBusinessGroupId")]
		[Bindable(true)]
		public int? DealAccBusinessGroupId
		{
			get { return GetColumnValue<int?>(Columns.DealAccBusinessGroupId); }
			set { SetColumnValue(Columns.DealAccBusinessGroupId, value); }
		}

		[XmlAttribute("DealType")]
		[Bindable(true)]
		public int? DealType
		{
			get { return GetColumnValue<int?>(Columns.DealType); }
			set { SetColumnValue(Columns.DealType, value); }
		}

		[XmlAttribute("DealTypeDetail")]
		[Bindable(true)]
		public int? DealTypeDetail
		{
			get { return GetColumnValue<int?>(Columns.DealTypeDetail); }
			set { SetColumnValue(Columns.DealTypeDetail, value); }
		}

		[XmlAttribute("IsTravelDeal")]
		[Bindable(true)]
		public bool? IsTravelDeal
		{
			get { return GetColumnValue<bool?>(Columns.IsTravelDeal); }
			set { SetColumnValue(Columns.IsTravelDeal, value); }
		}

		[XmlAttribute("DealEmpName")]
		[Bindable(true)]
		public string DealEmpName
		{
			get { return GetColumnValue<string>(Columns.DealEmpName); }
			set { SetColumnValue(Columns.DealEmpName, value); }
		}

		[XmlAttribute("Installment3months")]
		[Bindable(true)]
		public bool? Installment3months
		{
			get { return GetColumnValue<bool?>(Columns.Installment3months); }
			set { SetColumnValue(Columns.Installment3months, value); }
		}

		[XmlAttribute("Installment6months")]
		[Bindable(true)]
		public bool? Installment6months
		{
			get { return GetColumnValue<bool?>(Columns.Installment6months); }
			set { SetColumnValue(Columns.Installment6months, value); }
		}

		[XmlAttribute("Installment12months")]
		[Bindable(true)]
		public bool? Installment12months
		{
			get { return GetColumnValue<bool?>(Columns.Installment12months); }
			set { SetColumnValue(Columns.Installment12months, value); }
		}

		[XmlAttribute("DenyInstallment")]
		[Bindable(true)]
		public bool? DenyInstallment
		{
			get { return GetColumnValue<bool?>(Columns.DenyInstallment); }
			set { SetColumnValue(Columns.DenyInstallment, value); }
		}

		[XmlAttribute("SaleMultipleBase")]
		[Bindable(true)]
		public int? SaleMultipleBase
		{
			get { return GetColumnValue<int?>(Columns.SaleMultipleBase); }
			set { SetColumnValue(Columns.SaleMultipleBase, value); }
		}

		[XmlAttribute("ShipType")]
		[Bindable(true)]
		public int? ShipType
		{
			get { return GetColumnValue<int?>(Columns.ShipType); }
			set { SetColumnValue(Columns.ShipType, value); }
		}

		[XmlAttribute("ShippingdateType")]
		[Bindable(true)]
		public int? ShippingdateType
		{
			get { return GetColumnValue<int?>(Columns.ShippingdateType); }
			set { SetColumnValue(Columns.ShippingdateType, value); }
		}

		[XmlAttribute("Shippingdate")]
		[Bindable(true)]
		public int? Shippingdate
		{
			get { return GetColumnValue<int?>(Columns.Shippingdate); }
			set { SetColumnValue(Columns.Shippingdate, value); }
		}

		[XmlAttribute("ProductUseDateEndSet")]
		[Bindable(true)]
		public int? ProductUseDateEndSet
		{
			get { return GetColumnValue<int?>(Columns.ProductUseDateEndSet); }
			set { SetColumnValue(Columns.ProductUseDateEndSet, value); }
		}

		[XmlAttribute("PresentQuantity")]
		[Bindable(true)]
		public int? PresentQuantity
		{
			get { return GetColumnValue<int?>(Columns.PresentQuantity); }
			set { SetColumnValue(Columns.PresentQuantity, value); }
		}

		[XmlAttribute("GroupCouponAppStyle")]
		[Bindable(true)]
		public int? GroupCouponAppStyle
		{
			get { return GetColumnValue<int?>(Columns.GroupCouponAppStyle); }
			set { SetColumnValue(Columns.GroupCouponAppStyle, value); }
		}

		[XmlAttribute("NewDealType")]
		[Bindable(true)]
		public int? NewDealType
		{
			get { return GetColumnValue<int?>(Columns.NewDealType); }
			set { SetColumnValue(Columns.NewDealType, value); }
		}

		[XmlAttribute("IsExperience")]
		[Bindable(true)]
		public bool? IsExperience
		{
			get { return GetColumnValue<bool?>(Columns.IsExperience); }
			set { SetColumnValue(Columns.IsExperience, value); }
		}

		[XmlAttribute("DiscountType")]
		[Bindable(true)]
		public int? DiscountType
		{
			get { return GetColumnValue<int?>(Columns.DiscountType); }
			set { SetColumnValue(Columns.DiscountType, value); }
		}

		[XmlAttribute("DiscountValue")]
		[Bindable(true)]
		public int? DiscountValue
		{
			get { return GetColumnValue<int?>(Columns.DiscountValue); }
			set { SetColumnValue(Columns.DiscountValue, value); }
		}

		[XmlAttribute("TravelPlace")]
		[Bindable(true)]
		public string TravelPlace
		{
			get { return GetColumnValue<string>(Columns.TravelPlace); }
			set { SetColumnValue(Columns.TravelPlace, value); }
		}

		[XmlAttribute("EntrustSell")]
		[Bindable(true)]
		public int? EntrustSell
		{
			get { return GetColumnValue<int?>(Columns.EntrustSell); }
			set { SetColumnValue(Columns.EntrustSell, value); }
		}

		[XmlAttribute("MultipleBranch")]
		[Bindable(true)]
		public bool? MultipleBranch
		{
			get { return GetColumnValue<bool?>(Columns.MultipleBranch); }
			set { SetColumnValue(Columns.MultipleBranch, value); }
		}

		[XmlAttribute("IsLongContract")]
		[Bindable(true)]
		public bool? IsLongContract
		{
			get { return GetColumnValue<bool?>(Columns.IsLongContract); }
			set { SetColumnValue(Columns.IsLongContract, value); }
		}

		[XmlAttribute("CompleteCopy")]
		[Bindable(true)]
		public bool? CompleteCopy
		{
			get { return GetColumnValue<bool?>(Columns.CompleteCopy); }
			set { SetColumnValue(Columns.CompleteCopy, value); }
		}

		[XmlAttribute("DevelopeSalesId")]
		[Bindable(true)]
		public int? DevelopeSalesId
		{
			get { return GetColumnValue<int?>(Columns.DevelopeSalesId); }
			set { SetColumnValue(Columns.DevelopeSalesId, value); }
		}

		[XmlAttribute("OperationSalesId")]
		[Bindable(true)]
		public int? OperationSalesId
		{
			get { return GetColumnValue<int?>(Columns.OperationSalesId); }
			set { SetColumnValue(Columns.OperationSalesId, value); }
		}

		[XmlAttribute("IsFreightsDeal")]
		[Bindable(true)]
		public bool? IsFreightsDeal
		{
			get { return GetColumnValue<bool?>(Columns.IsFreightsDeal); }
			set { SetColumnValue(Columns.IsFreightsDeal, value); }
		}

		[XmlAttribute("Cchannel")]
		[Bindable(true)]
		public bool? Cchannel
		{
			get { return GetColumnValue<bool?>(Columns.Cchannel); }
			set { SetColumnValue(Columns.Cchannel, value); }
		}

		[XmlAttribute("CchannelLink")]
		[Bindable(true)]
		public string CchannelLink
		{
			get { return GetColumnValue<string>(Columns.CchannelLink); }
			set { SetColumnValue(Columns.CchannelLink, value); }
		}

		[XmlAttribute("IsExpiredDeal")]
		[Bindable(true)]
		public bool? IsExpiredDeal
		{
			get { return GetColumnValue<bool?>(Columns.IsExpiredDeal); }
			set { SetColumnValue(Columns.IsExpiredDeal, value); }
		}

        [XmlAttribute("IsChosen")]
        [Bindable(true)]
        public bool? IsChosen
        {
            get
            {
                return GetColumnValue<bool?>("is_chosen");
            }
            set
            {
                SetColumnValue("is_chosen", value);
            }
        }

        [XmlAttribute("BookingSystemType")]
		[Bindable(true)]
		public int BookingSystemType
		{
			get { return GetColumnValue<int>(Columns.BookingSystemType); }
			set { SetColumnValue(Columns.BookingSystemType, value); }
		}

		[XmlAttribute("CouponSeparateDigits")]
		[Bindable(true)]
		public int CouponSeparateDigits
		{
			get { return GetColumnValue<int>(Columns.CouponSeparateDigits); }
			set { SetColumnValue(Columns.CouponSeparateDigits, value); }
		}

		[XmlAttribute("GroupOrderGuid")]
		[Bindable(true)]
		public Guid? GroupOrderGuid
		{
			get { return GetColumnValue<Guid?>(Columns.GroupOrderGuid); }
			set { SetColumnValue(Columns.GroupOrderGuid, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("CloseTime")]
		[Bindable(true)]
		public DateTime? CloseTime
		{
			get { return GetColumnValue<DateTime?>(Columns.CloseTime); }
			set { SetColumnValue(Columns.CloseTime, value); }
		}

		[XmlAttribute("GroupOrderStatus")]
		[Bindable(true)]
		public int? GroupOrderStatus
		{
			get { return GetColumnValue<int?>(Columns.GroupOrderStatus); }
			set { SetColumnValue(Columns.GroupOrderStatus, value); }
		}

		[XmlAttribute("Slug")]
		[Bindable(true)]
		public string Slug
		{
			get { return GetColumnValue<string>(Columns.Slug); }
			set { SetColumnValue(Columns.Slug, value); }
		}

		[XmlAttribute("Intro")]
		[Bindable(true)]
		public string Intro
		{
			get { return GetColumnValue<string>(Columns.Intro); }
			set { SetColumnValue(Columns.Intro, value); }
		}

		[XmlAttribute("PriceDesc")]
		[Bindable(true)]
		public string PriceDesc
		{
			get { return GetColumnValue<string>(Columns.PriceDesc); }
			set { SetColumnValue(Columns.PriceDesc, value); }
		}

		[XmlAttribute("IsHideContent")]
		[Bindable(true)]
		public bool? IsHideContent
		{
			get { return GetColumnValue<bool?>(Columns.IsHideContent); }
			set { SetColumnValue(Columns.IsHideContent, value); }
		}

		[XmlAttribute("ContentName")]
		[Bindable(true)]
		public string ContentName
		{
			get { return GetColumnValue<string>(Columns.ContentName); }
			set { SetColumnValue(Columns.ContentName, value); }
		}

		[XmlAttribute("IsCloseMenu")]
		[Bindable(true)]
		public bool? IsCloseMenu
		{
			get { return GetColumnValue<bool?>(Columns.IsCloseMenu); }
			set { SetColumnValue(Columns.IsCloseMenu, value); }
		}

		[XmlAttribute("OrderedQuantity")]
		[Bindable(true)]
		public int? OrderedQuantity
		{
			get { return GetColumnValue<int?>(Columns.OrderedQuantity); }
			set { SetColumnValue(Columns.OrderedQuantity, value); }
		}

		[XmlAttribute("OrderedTotal")]
		[Bindable(true)]
		public decimal? OrderedTotal
		{
			get { return GetColumnValue<decimal?>(Columns.OrderedTotal); }
			set { SetColumnValue(Columns.OrderedTotal, value); }
		}

		[XmlAttribute("OrderedIncludeRefundQuantity")]
		[Bindable(true)]
		public int? OrderedIncludeRefundQuantity
		{
			get { return GetColumnValue<int?>(Columns.OrderedIncludeRefundQuantity); }
			set { SetColumnValue(Columns.OrderedIncludeRefundQuantity, value); }
		}

		[XmlAttribute("OrderedIncludeRefundTotal")]
		[Bindable(true)]
		public decimal? OrderedIncludeRefundTotal
		{
			get { return GetColumnValue<decimal?>(Columns.OrderedIncludeRefundTotal); }
			set { SetColumnValue(Columns.OrderedIncludeRefundTotal, value); }
		}

		[XmlAttribute("DealPromoTitle")]
		[Bindable(true)]
		public string DealPromoTitle
		{
			get { return GetColumnValue<string>(Columns.DealPromoTitle); }
			set { SetColumnValue(Columns.DealPromoTitle, value); }
		}

		[XmlAttribute("DealPromoDescription")]
		[Bindable(true)]
		public string DealPromoDescription
		{
			get { return GetColumnValue<string>(Columns.DealPromoDescription); }
			set { SetColumnValue(Columns.DealPromoDescription, value); }
		}

		[XmlAttribute("DealPromoImage")]
		[Bindable(true)]
		public string DealPromoImage
		{
			get { return GetColumnValue<string>(Columns.DealPromoImage); }
			set { SetColumnValue(Columns.DealPromoImage, value); }
		}

		[XmlAttribute("PdfItemName")]
		[Bindable(true)]
		public string PdfItemName
		{
			get { return GetColumnValue<string>(Columns.PdfItemName); }
			set { SetColumnValue(Columns.PdfItemName, value); }
		}

		[XmlAttribute("MainBid")]
		[Bindable(true)]
		public Guid? MainBid
		{
			get { return GetColumnValue<Guid?>(Columns.MainBid); }
			set { SetColumnValue(Columns.MainBid, value); }
		}

		[XmlAttribute("ComboDealSeq")]
		[Bindable(true)]
		public int? ComboDealSeq
		{
			get { return GetColumnValue<int?>(Columns.ComboDealSeq); }
			set { SetColumnValue(Columns.ComboDealSeq, value); }
		}

		[XmlAttribute("GroupCouponDealType")]
		[Bindable(true)]
		public int? GroupCouponDealType
		{
			get { return GetColumnValue<int?>(Columns.GroupCouponDealType); }
			set { SetColumnValue(Columns.GroupCouponDealType, value); }
		}

		[XmlAttribute("CategoryList")]
		[Bindable(true)]
		public string CategoryList
		{
			get { return GetColumnValue<string>(Columns.CategoryList); }
			set { SetColumnValue(Columns.CategoryList, value); }
		}

		[XmlAttribute("Consignment")]
		[Bindable(true)]
		public bool? Consignment
		{
			get { return GetColumnValue<bool?>(Columns.Consignment); }
			set { SetColumnValue(Columns.Consignment, value); }
		}

		[XmlAttribute("FreightsId")]
		[Bindable(true)]
		public int? FreightsId
		{
			get { return GetColumnValue<int?>(Columns.FreightsId); }
			set { SetColumnValue(Columns.FreightsId, value); }
		}

		[XmlAttribute("BankId")]
		[Bindable(true)]
		public int? BankId
		{
			get { return GetColumnValue<int?>(Columns.BankId); }
			set { SetColumnValue(Columns.BankId, value); }
		}

		[XmlAttribute("IsBankDeal")]
		[Bindable(true)]
		public bool? IsBankDeal
		{
			get { return GetColumnValue<bool?>(Columns.IsBankDeal); }
			set { SetColumnValue(Columns.IsBankDeal, value); }
		}

		[XmlAttribute("AllowGuestBuy")]
		[Bindable(true)]
		public bool? AllowGuestBuy
		{
			get { return GetColumnValue<bool?>(Columns.AllowGuestBuy); }
			set { SetColumnValue(Columns.AllowGuestBuy, value); }
		}

		[XmlAttribute("DiscountUseType")]
		[Bindable(true)]
		public bool? DiscountUseType
		{
			get { return GetColumnValue<bool?>(Columns.DiscountUseType); }
			set { SetColumnValue(Columns.DiscountUseType, value); }
		}

		[XmlAttribute("VerifyActionType")]
		[Bindable(true)]
		public int? VerifyActionType
		{
			get { return GetColumnValue<int?>(Columns.VerifyActionType); }
			set { SetColumnValue(Columns.VerifyActionType, value); }
		}

		[XmlAttribute("IsHouseNewVer")]
		[Bindable(true)]
		public bool? IsHouseNewVer
		{
			get { return GetColumnValue<bool?>(Columns.IsHouseNewVer); }
			set { SetColumnValue(Columns.IsHouseNewVer, value); }
		}

		[XmlAttribute("FreightAmount")]
		[Bindable(true)]
		public decimal FreightAmount
		{
			get { return GetColumnValue<decimal>(Columns.FreightAmount); }
			set { SetColumnValue(Columns.FreightAmount, value); }
		}

		[XmlAttribute("EnableDelivery")]
		[Bindable(true)]
		public bool EnableDelivery
		{
			get { return GetColumnValue<bool>(Columns.EnableDelivery); }
			set { SetColumnValue(Columns.EnableDelivery, value); }
		}

		[XmlAttribute("IspQuantityLimit")]
		[Bindable(true)]
		public int IspQuantityLimit
		{
			get { return GetColumnValue<int>(Columns.IspQuantityLimit); }
			set { SetColumnValue(Columns.IspQuantityLimit, value); }
		}

		[XmlAttribute("EnableIsp")]
		[Bindable(true)]
		public bool EnableIsp
		{
			get { return GetColumnValue<bool>(Columns.EnableIsp); }
			set { SetColumnValue(Columns.EnableIsp, value); }
		}

		[XmlAttribute("RemoveBgPic")]
		[Bindable(true)]
		public string RemoveBgPic
		{
			get { return GetColumnValue<string>(Columns.RemoveBgPic); }
			set { SetColumnValue(Columns.RemoveBgPic, value); }
		}

		[XmlAttribute("DiscountPrice")]
		[Bindable(true)]
		public decimal? DiscountPrice
		{
			get { return GetColumnValue<decimal?>(Columns.DiscountPrice); }
			set { SetColumnValue(Columns.DiscountPrice, value); }
		}

		[XmlAttribute("IsGame")]
		[Bindable(true)]
		public bool? IsGame
		{
			get { return GetColumnValue<bool?>(Columns.IsGame); }
			set { SetColumnValue(Columns.IsGame, value); }
		}

	    [XmlAttribute("ExpireRedirectDisplay")]
	    [Bindable(true)]
	    public int? ExpireRedirectDisplay
	    {
	        get { return GetColumnValue<int?>(Columns.ExpireRedirectDisplay); }
	        set { SetColumnValue(Columns.ExpireRedirectDisplay, value); }
	    }

	    [XmlAttribute("ExpireRedirectUrl")]
	    [Bindable(true)]
	    public string ExpireRedirectUrl
	    {
	        get { return GetColumnValue<string>(Columns.ExpireRedirectUrl); }
	        set { SetColumnValue(Columns.ExpireRedirectUrl, value); }
	    }

	    [XmlAttribute("UseExpireRedirectUrlAsCanonical")]
	    [Bindable(true)]
	    public bool? UseExpireRedirectUrlAsCanonical
	    {
	        get { return GetColumnValue<bool?>(Columns.UseExpireRedirectUrlAsCanonical); }
	        set { SetColumnValue(Columns.UseExpireRedirectUrlAsCanonical, value); }
	    }

        [XmlAttribute("IsWms")]
		[Bindable(true)]
		public bool IsWms
		{
			get { return GetColumnValue<bool>(Columns.IsWms); }
			set { SetColumnValue(Columns.IsWms, value); }
		}

        [XmlAttribute("AgentChannels")]
        [Bindable(true)]
        public string AgentChannels
        {
            get { return GetColumnValue<string>("agent_channels"); }
            set { SetColumnValue("agent_channels", value); }
        }

        [XmlAttribute("IsChannelGift")]
        [Bindable(true)]
        public bool IsChannelGift
        {
            get { return GetColumnValue<bool>("is_channel_gift"); }
            set { SetColumnValue("is_channel_gift", value); }
        }
        #endregion

        #region Typed Columns

        public static TableSchema.TableColumn BusinessHourGuidColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderTimeSColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderTimeEColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn BusinessHourDeliverTimeSColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn BusinessHourDeliverTimeEColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderMinimumColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn BusinessHourStatusColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn OrderTotalLimitColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn BusinessHourDeliveryChargeColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn BusinessHourAtmMaximumColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn PageTitleColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn PageDescColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn PicAltColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn SettlementTimeColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn ChangedExpireDateColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn SellerIdColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn SellerNameColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn SellerTelColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn SellerEmailColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn SellerAddressColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn DepartmentColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn SellerCityIdColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn IsCloseDownColumn
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn CloseDownDateColumn
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn CompanyIdColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn CompanyNameColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn ItemGuidColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn MaxItemCountColumn
		{
			get { return Schema.Columns[28]; }
		}

		public static TableSchema.TableColumn ItemNameColumn
		{
			get { return Schema.Columns[29]; }
		}

		public static TableSchema.TableColumn ItemOrigPriceColumn
		{
			get { return Schema.Columns[30]; }
		}

		public static TableSchema.TableColumn ItemPriceColumn
		{
			get { return Schema.Columns[31]; }
		}

		public static TableSchema.TableColumn ItemDefaultDailyAmountColumn
		{
			get { return Schema.Columns[32]; }
		}

		public static TableSchema.TableColumn EventNameColumn
		{
			get { return Schema.Columns[33]; }
		}

		public static TableSchema.TableColumn EventTitleColumn
		{
			get { return Schema.Columns[34]; }
		}

		public static TableSchema.TableColumn EventImagePathColumn
		{
			get { return Schema.Columns[35]; }
		}

		public static TableSchema.TableColumn IntroductionColumn
		{
			get { return Schema.Columns[36]; }
		}

		public static TableSchema.TableColumn RestrictionsColumn
		{
			get { return Schema.Columns[37]; }
		}

		public static TableSchema.TableColumn DescriptionColumn
		{
			get { return Schema.Columns[38]; }
		}

		public static TableSchema.TableColumn AppDescriptionColumn
		{
			get { return Schema.Columns[39]; }
		}

		public static TableSchema.TableColumn ReferenceTextColumn
		{
			get { return Schema.Columns[40]; }
		}

		public static TableSchema.TableColumn CouponUsageColumn
		{
			get { return Schema.Columns[41]; }
		}

		public static TableSchema.TableColumn RemarkColumn
		{
			get { return Schema.Columns[42]; }
		}

		public static TableSchema.TableColumn ReasonsColumn
		{
			get { return Schema.Columns[43]; }
		}

		public static TableSchema.TableColumn SubjectNameColumn
		{
			get { return Schema.Columns[44]; }
		}

		public static TableSchema.TableColumn EventSpecialImagePathColumn
		{
			get { return Schema.Columns[45]; }
		}

		public static TableSchema.TableColumn AppDealPicColumn
		{
			get { return Schema.Columns[46]; }
		}

		public static TableSchema.TableColumn AppTitleColumn
		{
			get { return Schema.Columns[47]; }
		}

		public static TableSchema.TableColumn AvailabilityColumn
		{
			get { return Schema.Columns[48]; }
		}

		public static TableSchema.TableColumn EventModifyTimeColumn
		{
			get { return Schema.Columns[49]; }
		}

		public static TableSchema.TableColumn ProductSpecColumn
		{
			get { return Schema.Columns[50]; }
		}

		public static TableSchema.TableColumn DeliveryTypeColumn
		{
			get { return Schema.Columns[51]; }
		}

		public static TableSchema.TableColumn ShoppingCartColumn
		{
			get { return Schema.Columns[52]; }
		}

		public static TableSchema.TableColumn ComboPackCountColumn
		{
			get { return Schema.Columns[53]; }
		}

		public static TableSchema.TableColumn IsTravelcardColumn
		{
			get { return Schema.Columns[54]; }
		}

		public static TableSchema.TableColumn QuantityMultiplierColumn
		{
			get { return Schema.Columns[55]; }
		}

		public static TableSchema.TableColumn UniqueIdColumn
		{
			get { return Schema.Columns[56]; }
		}

		public static TableSchema.TableColumn IsDailyRestrictionColumn
		{
			get { return Schema.Columns[57]; }
		}

		public static TableSchema.TableColumn IsContinuedQuantityColumn
		{
			get { return Schema.Columns[58]; }
		}

		public static TableSchema.TableColumn ContinuedQuantityColumn
		{
			get { return Schema.Columns[59]; }
		}

		public static TableSchema.TableColumn CityListColumn
		{
			get { return Schema.Columns[60]; }
		}

		public static TableSchema.TableColumn ActivityUrlColumn
		{
			get { return Schema.Columns[61]; }
		}

		public static TableSchema.TableColumn LabelIconListColumn
		{
			get { return Schema.Columns[62]; }
		}

		public static TableSchema.TableColumn LabelTagListColumn
		{
			get { return Schema.Columns[63]; }
		}

		public static TableSchema.TableColumn CouponCodeTypeColumn
		{
			get { return Schema.Columns[64]; }
		}

		public static TableSchema.TableColumn PinTypeColumn
		{
			get { return Schema.Columns[65]; }
		}

		public static TableSchema.TableColumn CustomTagColumn
		{
			get { return Schema.Columns[66]; }
		}

		public static TableSchema.TableColumn ExchangePriceColumn
		{
			get { return Schema.Columns[67]; }
		}

		public static TableSchema.TableColumn IsQuantityMultiplierColumn
		{
			get { return Schema.Columns[68]; }
		}

		public static TableSchema.TableColumn IsAveragePriceColumn
		{
			get { return Schema.Columns[69]; }
		}

		public static TableSchema.TableColumn IsZeroActivityShowCouponColumn
		{
			get { return Schema.Columns[70]; }
		}

		public static TableSchema.TableColumn DealAccBusinessGroupIdColumn
		{
			get { return Schema.Columns[71]; }
		}

		public static TableSchema.TableColumn DealTypeColumn
		{
			get { return Schema.Columns[72]; }
		}

		public static TableSchema.TableColumn DealTypeDetailColumn
		{
			get { return Schema.Columns[73]; }
		}

		public static TableSchema.TableColumn IsTravelDealColumn
		{
			get { return Schema.Columns[74]; }
		}

		public static TableSchema.TableColumn DealEmpNameColumn
		{
			get { return Schema.Columns[75]; }
		}

		public static TableSchema.TableColumn Installment3monthsColumn
		{
			get { return Schema.Columns[76]; }
		}

		public static TableSchema.TableColumn Installment6monthsColumn
		{
			get { return Schema.Columns[77]; }
		}

		public static TableSchema.TableColumn Installment12monthsColumn
		{
			get { return Schema.Columns[78]; }
		}

		public static TableSchema.TableColumn DenyInstallmentColumn
		{
			get { return Schema.Columns[79]; }
		}

		public static TableSchema.TableColumn SaleMultipleBaseColumn
		{
			get { return Schema.Columns[80]; }
		}

		public static TableSchema.TableColumn ShipTypeColumn
		{
			get { return Schema.Columns[81]; }
		}

		public static TableSchema.TableColumn ShippingdateTypeColumn
		{
			get { return Schema.Columns[82]; }
		}

		public static TableSchema.TableColumn ShippingdateColumn
		{
			get { return Schema.Columns[83]; }
		}

		public static TableSchema.TableColumn ProductUseDateEndSetColumn
		{
			get { return Schema.Columns[84]; }
		}

		public static TableSchema.TableColumn PresentQuantityColumn
		{
			get { return Schema.Columns[85]; }
		}

		public static TableSchema.TableColumn GroupCouponAppStyleColumn
		{
			get { return Schema.Columns[86]; }
		}

		public static TableSchema.TableColumn NewDealTypeColumn
		{
			get { return Schema.Columns[87]; }
		}

		public static TableSchema.TableColumn IsExperienceColumn
		{
			get { return Schema.Columns[88]; }
		}

		public static TableSchema.TableColumn DiscountTypeColumn
		{
			get { return Schema.Columns[89]; }
		}

		public static TableSchema.TableColumn DiscountValueColumn
		{
			get { return Schema.Columns[90]; }
		}

		public static TableSchema.TableColumn TravelPlaceColumn
		{
			get { return Schema.Columns[91]; }
		}

		public static TableSchema.TableColumn EntrustSellColumn
		{
			get { return Schema.Columns[92]; }
		}

		public static TableSchema.TableColumn MultipleBranchColumn
		{
			get { return Schema.Columns[93]; }
		}

		public static TableSchema.TableColumn IsLongContractColumn
		{
			get { return Schema.Columns[94]; }
		}

		public static TableSchema.TableColumn CompleteCopyColumn
		{
			get { return Schema.Columns[95]; }
		}

		public static TableSchema.TableColumn DevelopeSalesIdColumn
		{
			get { return Schema.Columns[96]; }
		}

		public static TableSchema.TableColumn OperationSalesIdColumn
		{
			get { return Schema.Columns[97]; }
		}

		public static TableSchema.TableColumn IsFreightsDealColumn
		{
			get { return Schema.Columns[98]; }
		}

		public static TableSchema.TableColumn CchannelColumn
		{
			get { return Schema.Columns[99]; }
		}

		public static TableSchema.TableColumn CchannelLinkColumn
		{
			get { return Schema.Columns[100]; }
		}

		public static TableSchema.TableColumn IsExpiredDealColumn
		{
			get { return Schema.Columns[101]; }
		}

		public static TableSchema.TableColumn BookingSystemTypeColumn
		{
			get { return Schema.Columns[102]; }
		}

		public static TableSchema.TableColumn CouponSeparateDigitsColumn
		{
			get { return Schema.Columns[103]; }
		}

		public static TableSchema.TableColumn GroupOrderGuidColumn
		{
			get { return Schema.Columns[104]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[105]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[106]; }
		}

		public static TableSchema.TableColumn CloseTimeColumn
		{
			get { return Schema.Columns[107]; }
		}

		public static TableSchema.TableColumn GroupOrderStatusColumn
		{
			get { return Schema.Columns[108]; }
		}

		public static TableSchema.TableColumn SlugColumn
		{
			get { return Schema.Columns[109]; }
		}

		public static TableSchema.TableColumn IntroColumn
		{
			get { return Schema.Columns[110]; }
		}

		public static TableSchema.TableColumn PriceDescColumn
		{
			get { return Schema.Columns[111]; }
		}

		public static TableSchema.TableColumn IsHideContentColumn
		{
			get { return Schema.Columns[112]; }
		}

		public static TableSchema.TableColumn ContentNameColumn
		{
			get { return Schema.Columns[113]; }
		}

		public static TableSchema.TableColumn IsCloseMenuColumn
		{
			get { return Schema.Columns[114]; }
		}

		public static TableSchema.TableColumn OrderedQuantityColumn
		{
			get { return Schema.Columns[115]; }
		}

		public static TableSchema.TableColumn OrderedTotalColumn
		{
			get { return Schema.Columns[116]; }
		}

		public static TableSchema.TableColumn OrderedIncludeRefundQuantityColumn
		{
			get { return Schema.Columns[117]; }
		}

		public static TableSchema.TableColumn OrderedIncludeRefundTotalColumn
		{
			get { return Schema.Columns[118]; }
		}

		public static TableSchema.TableColumn DealPromoTitleColumn
		{
			get { return Schema.Columns[119]; }
		}

		public static TableSchema.TableColumn DealPromoDescriptionColumn
		{
			get { return Schema.Columns[120]; }
		}

		public static TableSchema.TableColumn DealPromoImageColumn
		{
			get { return Schema.Columns[121]; }
		}

		public static TableSchema.TableColumn PdfItemNameColumn
		{
			get { return Schema.Columns[122]; }
		}

		public static TableSchema.TableColumn MainBidColumn
		{
			get { return Schema.Columns[123]; }
		}

		public static TableSchema.TableColumn ComboDealSeqColumn
		{
			get { return Schema.Columns[124]; }
		}

		public static TableSchema.TableColumn GroupCouponDealTypeColumn
		{
			get { return Schema.Columns[125]; }
		}

		public static TableSchema.TableColumn CategoryListColumn
		{
			get { return Schema.Columns[126]; }
		}

		public static TableSchema.TableColumn ConsignmentColumn
		{
			get { return Schema.Columns[127]; }
		}

		public static TableSchema.TableColumn FreightsIdColumn
		{
			get { return Schema.Columns[128]; }
		}

		public static TableSchema.TableColumn BankIdColumn
		{
			get { return Schema.Columns[129]; }
		}

		public static TableSchema.TableColumn IsBankDealColumn
		{
			get { return Schema.Columns[130]; }
		}

		public static TableSchema.TableColumn AllowGuestBuyColumn
		{
			get { return Schema.Columns[131]; }
		}

		public static TableSchema.TableColumn DiscountUseTypeColumn
		{
			get { return Schema.Columns[132]; }
		}

		public static TableSchema.TableColumn VerifyActionTypeColumn
		{
			get { return Schema.Columns[133]; }
		}

		public static TableSchema.TableColumn IsHouseNewVerColumn
		{
			get { return Schema.Columns[134]; }
		}

		public static TableSchema.TableColumn FreightAmountColumn
		{
			get { return Schema.Columns[135]; }
		}

		public static TableSchema.TableColumn EnableDeliveryColumn
		{
			get { return Schema.Columns[136]; }
		}

		public static TableSchema.TableColumn IspQuantityLimitColumn
		{
			get { return Schema.Columns[137]; }
		}

		public static TableSchema.TableColumn EnableIspColumn
		{
			get { return Schema.Columns[138]; }
		}

		public static TableSchema.TableColumn RemoveBgPicColumn
		{
			get { return Schema.Columns[139]; }
		}

		public static TableSchema.TableColumn DiscountPriceColumn
		{
			get { return Schema.Columns[140]; }
		}

		public static TableSchema.TableColumn IsGameColumn
		{
			get { return Schema.Columns[141]; }
		}

        public static TableSchema.TableColumn IsWmsColumn
		{
			get { return Schema.Columns[142]; }
		}

        public static TableSchema.TableColumn IsChannelGiftColumn
        {
            get { return Schema.Columns[143]; }
        }

        #endregion

        #region Columns Struct

        public struct Columns
		{
			public static string BusinessHourGuid = @"business_hour_guid";
			public static string SellerGuid = @"seller_GUID";
			public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
			public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
			public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
			public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
			public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
			public static string BusinessHourStatus = @"business_hour_status";
			public static string OrderTotalLimit = @"order_total_limit";
			public static string BusinessHourDeliveryCharge = @"business_hour_delivery_charge";
			public static string BusinessHourAtmMaximum = @"business_hour_atm_maximum";
			public static string PageTitle = @"page_title";
			public static string PageDesc = @"page_desc";
			public static string PicAlt = @"pic_alt";
			public static string SettlementTime = @"settlement_time";
			public static string ChangedExpireDate = @"changed_expire_date";
			public static string SellerId = @"seller_id";
			public static string SellerName = @"seller_name";
			public static string SellerTel = @"seller_tel";
			public static string SellerEmail = @"seller_email";
			public static string SellerAddress = @"seller_address";
			public static string Department = @"department";
			public static string SellerCityId = @"seller_city_id";
			public static string IsCloseDown = @"is_close_down";
			public static string CloseDownDate = @"close_down_date";
			public static string CompanyId = @"company_id";
			public static string CompanyName = @"company_name";
			public static string ItemGuid = @"item_guid";
			public static string MaxItemCount = @"max_item_count";
			public static string ItemName = @"item_name";
			public static string ItemOrigPrice = @"item_orig_price";
			public static string ItemPrice = @"item_price";
			public static string ItemDefaultDailyAmount = @"item_default_daily_amount";
			public static string EventName = @"event_name";
			public static string EventTitle = @"event_title";
			public static string EventImagePath = @"event_image_path";
            public static string EventOriImagePath = @"event_ori_image_path";
            public static string Introduction = @"introduction";
			public static string Restrictions = @"restrictions";
			public static string Description = @"description";
			public static string AppDescription = @"app_description";
			public static string ReferenceText = @"reference_text";
			public static string CouponUsage = @"coupon_usage";
			public static string Remark = @"remark";
			public static string Reasons = @"reasons";
			public static string SubjectName = @"subject_name";
			public static string EventSpecialImagePath = @"event_special_image_path";
			public static string AppDealPic = @"app_deal_pic";
			public static string AppTitle = @"app_title";
			public static string Availability = @"availability";
			public static string EventModifyTime = @"event_modify_time";
			public static string ProductSpec = @"product_spec";
			public static string DeliveryType = @"delivery_type";
			public static string ShoppingCart = @"shopping_cart";
			public static string ComboPackCount = @"combo_pack_count";
			public static string IsTravelcard = @"is_travelcard";
			public static string QuantityMultiplier = @"quantity_multiplier";
			public static string UniqueId = @"unique_id";
			public static string IsDailyRestriction = @"is_daily_restriction";
			public static string IsContinuedQuantity = @"is_continued_quantity";
			public static string ContinuedQuantity = @"continued_quantity";
			public static string CityList = @"city_list";
			public static string ActivityUrl = @"activity_url";
			public static string LabelIconList = @"label_icon_list";
			public static string LabelTagList = @"label_tag_list";
			public static string CouponCodeType = @"coupon_code_type";
			public static string PinType = @"pin_type";
			public static string CustomTag = @"custom_tag";
			public static string ExchangePrice = @"exchange_price";
			public static string IsQuantityMultiplier = @"is_quantity_multiplier";
			public static string IsAveragePrice = @"is_average_price";
			public static string IsZeroActivityShowCoupon = @"is_zero_activity_show_coupon";
			public static string DealAccBusinessGroupId = @"deal_acc_business_group_id";
			public static string DealType = @"deal_type";
			public static string DealTypeDetail = @"deal_type_detail";
			public static string IsTravelDeal = @"is_travel_deal";
			public static string DealEmpName = @"deal_emp_name";
			public static string Installment3months = @"installment_3months";
			public static string Installment6months = @"installment_6months";
			public static string Installment12months = @"installment_12months";
			public static string DenyInstallment = @"deny_installment";
			public static string SaleMultipleBase = @"sale_multiple_base";
			public static string ShipType = @"ship_type";
			public static string ShippingdateType = @"shippingdate_type";
			public static string Shippingdate = @"shippingdate";
			public static string ProductUseDateEndSet = @"product_use_date_end_set";
			public static string PresentQuantity = @"present_quantity";
			public static string GroupCouponAppStyle = @"group_coupon_app_style";
			public static string NewDealType = @"new_deal_type";
			public static string IsExperience = @"is_experience";
			public static string DiscountType = @"discount_type";
			public static string DiscountValue = @"discount_value";
			public static string TravelPlace = @"travel_place";
			public static string EntrustSell = @"entrust_sell";
			public static string MultipleBranch = @"multiple_branch";
			public static string IsLongContract = @"is_long_contract";
			public static string CompleteCopy = @"complete_copy";
			public static string DevelopeSalesId = @"develope_sales_id";
			public static string OperationSalesId = @"operation_sales_id";
			public static string IsFreightsDeal = @"is_freights_deal";
			public static string Cchannel = @"cchannel";
			public static string CchannelLink = @"cchannel_link";
			public static string IsExpiredDeal = @"is_expired_deal";
            public static string IsChosen = @"is_chosen";
            public static string BookingSystemType = @"booking_system_type";
			public static string CouponSeparateDigits = @"coupon_separate_digits";
			public static string GroupOrderGuid = @"group_order_guid";
			public static string OrderGuid = @"order_guid";
			public static string CreateTime = @"create_time";
			public static string CloseTime = @"close_time";
			public static string GroupOrderStatus = @"group_order_status";
			public static string Slug = @"slug";
			public static string Intro = @"intro";
			public static string PriceDesc = @"price_desc";
			public static string IsHideContent = @"is_hide_content";
			public static string ContentName = @"content_name";
			public static string IsCloseMenu = @"is_close_menu";
			public static string OrderedQuantity = @"ordered_quantity";
			public static string OrderedTotal = @"ordered_total";
			public static string OrderedIncludeRefundQuantity = @"ordered_include_refund_quantity";
			public static string OrderedIncludeRefundTotal = @"ordered_include_refund_total";
			public static string DealPromoTitle = @"deal_promo_title";
			public static string DealPromoDescription = @"deal_promo_description";
			public static string DealPromoImage = @"deal_promo_image";
			public static string PdfItemName = @"pdf_item_name";
			public static string MainBid = @"main_bid";
			public static string ComboDealSeq = @"combo_deal_seq";
			public static string GroupCouponDealType = @"group_coupon_deal_type";
			public static string CategoryList = @"category_list";
			public static string Consignment = @"consignment";
			public static string FreightsId = @"freights_id";
			public static string BankId = @"bank_id";
			public static string IsBankDeal = @"is_bank_deal";
			public static string AllowGuestBuy = @"allow_guest_buy";
			public static string DiscountUseType = @"discount_use_type";
			public static string VerifyActionType = @"verify_action_type";
			public static string IsHouseNewVer = @"is_house_new_ver";
			public static string FreightAmount = @"freight_amount";
			public static string EnableDelivery = @"enable_delivery";
			public static string IspQuantityLimit = @"isp_quantity_limit";
			public static string EnableIsp = @"enable_isp";
			public static string RemoveBgPic = @"remove_bg_pic";
			public static string DiscountPrice = @"discount_price";
			public static string IsGame = @"is_game";
            public static string ExpireRedirectDisplay = @"expire_redirect_display";
            public static string ExpireRedirectUrl = @"expire_redirect_url";
		    public static string UseExpireRedirectUrlAsCanonical = @"use_expire_redirect_url_as_canonical";
			public static string IsWms = @"is_wms";
            public static string AgentChannels = @"agent_channels";
            public static string IsChannelGift = @"is_channel_gift";
        }

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

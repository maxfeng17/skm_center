using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MemberCollectDeal class.
	/// </summary>
    [Serializable]
	public partial class MemberCollectDealCollection : RepositoryList<MemberCollectDeal, MemberCollectDealCollection>
	{	   
		public MemberCollectDealCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberCollectDealCollection</returns>
		public MemberCollectDealCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberCollectDeal o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the member_collect_deal table.
	/// </summary>
	[Serializable]
	public partial class MemberCollectDeal : RepositoryRecord<MemberCollectDeal>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MemberCollectDeal()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MemberCollectDeal(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_collect_deal", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarMemberUniqueId = new TableSchema.TableColumn(schema);
				colvarMemberUniqueId.ColumnName = "member_unique_id";
				colvarMemberUniqueId.DataType = DbType.Int32;
				colvarMemberUniqueId.MaxLength = 0;
				colvarMemberUniqueId.AutoIncrement = false;
				colvarMemberUniqueId.IsNullable = false;
				colvarMemberUniqueId.IsPrimaryKey = true;
				colvarMemberUniqueId.IsForeignKey = false;
				colvarMemberUniqueId.IsReadOnly = false;
				colvarMemberUniqueId.DefaultSetting = @"";
				colvarMemberUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberUniqueId);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = true;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = false;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				
						colvarCityId.DefaultSetting = @"((0))";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarCollectStatus = new TableSchema.TableColumn(schema);
				colvarCollectStatus.ColumnName = "collect_status";
				colvarCollectStatus.DataType = DbType.Byte;
				colvarCollectStatus.MaxLength = 0;
				colvarCollectStatus.AutoIncrement = false;
				colvarCollectStatus.IsNullable = false;
				colvarCollectStatus.IsPrimaryKey = false;
				colvarCollectStatus.IsForeignKey = false;
				colvarCollectStatus.IsReadOnly = false;
				
						colvarCollectStatus.DefaultSetting = @"((0))";
				colvarCollectStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCollectStatus);
				
				TableSchema.TableColumn colvarCollectTime = new TableSchema.TableColumn(schema);
				colvarCollectTime.ColumnName = "collect_time";
				colvarCollectTime.DataType = DbType.DateTime;
				colvarCollectTime.MaxLength = 0;
				colvarCollectTime.AutoIncrement = false;
				colvarCollectTime.IsNullable = false;
				colvarCollectTime.IsPrimaryKey = false;
				colvarCollectTime.IsForeignKey = false;
				colvarCollectTime.IsReadOnly = false;
				colvarCollectTime.DefaultSetting = @"";
				colvarCollectTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCollectTime);
				
				TableSchema.TableColumn colvarLastAccessTime = new TableSchema.TableColumn(schema);
				colvarLastAccessTime.ColumnName = "last_access_time";
				colvarLastAccessTime.DataType = DbType.DateTime;
				colvarLastAccessTime.MaxLength = 0;
				colvarLastAccessTime.AutoIncrement = false;
				colvarLastAccessTime.IsNullable = false;
				colvarLastAccessTime.IsPrimaryKey = false;
				colvarLastAccessTime.IsForeignKey = false;
				colvarLastAccessTime.IsReadOnly = false;
				
						colvarLastAccessTime.DefaultSetting = @"(getdate())";
				colvarLastAccessTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastAccessTime);
				
				TableSchema.TableColumn colvarAppNotice = new TableSchema.TableColumn(schema);
				colvarAppNotice.ColumnName = "app_notice";
				colvarAppNotice.DataType = DbType.Boolean;
				colvarAppNotice.MaxLength = 0;
				colvarAppNotice.AutoIncrement = false;
				colvarAppNotice.IsNullable = false;
				colvarAppNotice.IsPrimaryKey = false;
				colvarAppNotice.IsForeignKey = false;
				colvarAppNotice.IsReadOnly = false;
				
						colvarAppNotice.DefaultSetting = @"((0))";
				colvarAppNotice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppNotice);
				
				TableSchema.TableColumn colvarPushTime = new TableSchema.TableColumn(schema);
				colvarPushTime.ColumnName = "push_time";
				colvarPushTime.DataType = DbType.DateTime;
				colvarPushTime.MaxLength = 0;
				colvarPushTime.AutoIncrement = false;
				colvarPushTime.IsNullable = true;
				colvarPushTime.IsPrimaryKey = false;
				colvarPushTime.IsForeignKey = false;
				colvarPushTime.IsReadOnly = false;
				colvarPushTime.DefaultSetting = @"";
				colvarPushTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPushTime);
				
				TableSchema.TableColumn colvarCollectType = new TableSchema.TableColumn(schema);
				colvarCollectType.ColumnName = "collect_type";
				colvarCollectType.DataType = DbType.Byte;
				colvarCollectType.MaxLength = 0;
				colvarCollectType.AutoIncrement = false;
				colvarCollectType.IsNullable = false;
				colvarCollectType.IsPrimaryKey = false;
				colvarCollectType.IsForeignKey = false;
				colvarCollectType.IsReadOnly = false;
				
						colvarCollectType.DefaultSetting = @"((0))";
				colvarCollectType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCollectType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_collect_deal",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("MemberUniqueId")]
		[Bindable(true)]
		public int MemberUniqueId 
		{
			get { return GetColumnValue<int>(Columns.MemberUniqueId); }
			set { SetColumnValue(Columns.MemberUniqueId, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int CityId 
		{
			get { return GetColumnValue<int>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("CollectStatus")]
		[Bindable(true)]
		public byte CollectStatus 
		{
			get { return GetColumnValue<byte>(Columns.CollectStatus); }
			set { SetColumnValue(Columns.CollectStatus, value); }
		}
		  
		[XmlAttribute("CollectTime")]
		[Bindable(true)]
		public DateTime CollectTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CollectTime); }
			set { SetColumnValue(Columns.CollectTime, value); }
		}
		  
		[XmlAttribute("LastAccessTime")]
		[Bindable(true)]
		public DateTime LastAccessTime 
		{
			get { return GetColumnValue<DateTime>(Columns.LastAccessTime); }
			set { SetColumnValue(Columns.LastAccessTime, value); }
		}
		  
		[XmlAttribute("AppNotice")]
		[Bindable(true)]
		public bool AppNotice 
		{
			get { return GetColumnValue<bool>(Columns.AppNotice); }
			set { SetColumnValue(Columns.AppNotice, value); }
		}
		  
		[XmlAttribute("PushTime")]
		[Bindable(true)]
		public DateTime? PushTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.PushTime); }
			set { SetColumnValue(Columns.PushTime, value); }
		}
		  
		[XmlAttribute("CollectType")]
		[Bindable(true)]
		public byte CollectType 
		{
			get { return GetColumnValue<byte>(Columns.CollectType); }
			set { SetColumnValue(Columns.CollectType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn MemberUniqueIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CollectStatusColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CollectTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn LastAccessTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn AppNoticeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn PushTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CollectTypeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string MemberUniqueId = @"member_unique_id";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string CityId = @"city_id";
			 public static string CollectStatus = @"collect_status";
			 public static string CollectTime = @"collect_time";
			 public static string LastAccessTime = @"last_access_time";
			 public static string AppNotice = @"app_notice";
			 public static string PushTime = @"push_time";
			 public static string CollectType = @"collect_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

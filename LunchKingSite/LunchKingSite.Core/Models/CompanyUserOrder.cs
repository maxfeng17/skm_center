using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CompanyUserOrder class.
	/// </summary>
    [Serializable]
	public partial class CompanyUserOrderCollection : RepositoryList<CompanyUserOrder, CompanyUserOrderCollection>
	{	   
		public CompanyUserOrderCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CompanyUserOrderCollection</returns>
		public CompanyUserOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CompanyUserOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the company_user_order table.
	/// </summary>
	[Serializable]
	public partial class CompanyUserOrder : RepositoryRecord<CompanyUserOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CompanyUserOrder()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CompanyUserOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("company_user_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarCompanyUserId = new TableSchema.TableColumn(schema);
				colvarCompanyUserId.ColumnName = "company_user_id";
				colvarCompanyUserId.DataType = DbType.Int32;
				colvarCompanyUserId.MaxLength = 0;
				colvarCompanyUserId.AutoIncrement = false;
				colvarCompanyUserId.IsNullable = false;
				colvarCompanyUserId.IsPrimaryKey = false;
				colvarCompanyUserId.IsForeignKey = false;
				colvarCompanyUserId.IsReadOnly = false;
				colvarCompanyUserId.DefaultSetting = @"";
				colvarCompanyUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyUserId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = true;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarPurchaserEmail = new TableSchema.TableColumn(schema);
				colvarPurchaserEmail.ColumnName = "purchaser_email";
				colvarPurchaserEmail.DataType = DbType.String;
				colvarPurchaserEmail.MaxLength = 255;
				colvarPurchaserEmail.AutoIncrement = false;
				colvarPurchaserEmail.IsNullable = true;
				colvarPurchaserEmail.IsPrimaryKey = false;
				colvarPurchaserEmail.IsForeignKey = false;
				colvarPurchaserEmail.IsReadOnly = false;
				colvarPurchaserEmail.DefaultSetting = @"";
				colvarPurchaserEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPurchaserEmail);
				
				TableSchema.TableColumn colvarPurchaserName = new TableSchema.TableColumn(schema);
				colvarPurchaserName.ColumnName = "purchaser_name";
				colvarPurchaserName.DataType = DbType.String;
				colvarPurchaserName.MaxLength = 255;
				colvarPurchaserName.AutoIncrement = false;
				colvarPurchaserName.IsNullable = true;
				colvarPurchaserName.IsPrimaryKey = false;
				colvarPurchaserName.IsForeignKey = false;
				colvarPurchaserName.IsReadOnly = false;
				colvarPurchaserName.DefaultSetting = @"";
				colvarPurchaserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPurchaserName);
				
				TableSchema.TableColumn colvarPurchaserPhone = new TableSchema.TableColumn(schema);
				colvarPurchaserPhone.ColumnName = "purchaser_phone";
				colvarPurchaserPhone.DataType = DbType.String;
				colvarPurchaserPhone.MaxLength = 20;
				colvarPurchaserPhone.AutoIncrement = false;
				colvarPurchaserPhone.IsNullable = true;
				colvarPurchaserPhone.IsPrimaryKey = false;
				colvarPurchaserPhone.IsForeignKey = false;
				colvarPurchaserPhone.IsReadOnly = false;
				colvarPurchaserPhone.DefaultSetting = @"";
				colvarPurchaserPhone.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPurchaserPhone);
				
				TableSchema.TableColumn colvarPurchaserAddress = new TableSchema.TableColumn(schema);
				colvarPurchaserAddress.ColumnName = "purchaser_address";
				colvarPurchaserAddress.DataType = DbType.String;
				colvarPurchaserAddress.MaxLength = 100;
				colvarPurchaserAddress.AutoIncrement = false;
				colvarPurchaserAddress.IsNullable = true;
				colvarPurchaserAddress.IsPrimaryKey = false;
				colvarPurchaserAddress.IsForeignKey = false;
				colvarPurchaserAddress.IsReadOnly = false;
				colvarPurchaserAddress.DefaultSetting = @"";
				colvarPurchaserAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPurchaserAddress);
				
				TableSchema.TableColumn colvarWaitUpdate = new TableSchema.TableColumn(schema);
				colvarWaitUpdate.ColumnName = "wait_update";
				colvarWaitUpdate.DataType = DbType.Boolean;
				colvarWaitUpdate.MaxLength = 0;
				colvarWaitUpdate.AutoIncrement = false;
				colvarWaitUpdate.IsNullable = false;
				colvarWaitUpdate.IsPrimaryKey = false;
				colvarWaitUpdate.IsForeignKey = false;
				colvarWaitUpdate.IsReadOnly = false;
				
						colvarWaitUpdate.DefaultSetting = @"((0))";
				colvarWaitUpdate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWaitUpdate);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("company_user_order",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("CompanyUserId")]
		[Bindable(true)]
		public int CompanyUserId 
		{
			get { return GetColumnValue<int>(Columns.CompanyUserId); }
			set { SetColumnValue(Columns.CompanyUserId, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("PurchaserEmail")]
		[Bindable(true)]
		public string PurchaserEmail 
		{
			get { return GetColumnValue<string>(Columns.PurchaserEmail); }
			set { SetColumnValue(Columns.PurchaserEmail, value); }
		}
		  
		[XmlAttribute("PurchaserName")]
		[Bindable(true)]
		public string PurchaserName 
		{
			get { return GetColumnValue<string>(Columns.PurchaserName); }
			set { SetColumnValue(Columns.PurchaserName, value); }
		}
		  
		[XmlAttribute("PurchaserPhone")]
		[Bindable(true)]
		public string PurchaserPhone 
		{
			get { return GetColumnValue<string>(Columns.PurchaserPhone); }
			set { SetColumnValue(Columns.PurchaserPhone, value); }
		}
		  
		[XmlAttribute("PurchaserAddress")]
		[Bindable(true)]
		public string PurchaserAddress 
		{
			get { return GetColumnValue<string>(Columns.PurchaserAddress); }
			set { SetColumnValue(Columns.PurchaserAddress, value); }
		}
		  
		[XmlAttribute("WaitUpdate")]
		[Bindable(true)]
		public bool WaitUpdate 
		{
			get { return GetColumnValue<bool>(Columns.WaitUpdate); }
			set { SetColumnValue(Columns.WaitUpdate, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn CompanyUserIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PurchaserEmailColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PurchaserNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PurchaserPhoneColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PurchaserAddressColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn WaitUpdateColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string CompanyUserId = @"company_user_id";
			 public static string OrderGuid = @"order_guid";
			 public static string PurchaserEmail = @"purchaser_email";
			 public static string PurchaserName = @"purchaser_name";
			 public static string PurchaserPhone = @"purchaser_phone";
			 public static string PurchaserAddress = @"purchaser_address";
			 public static string WaitUpdate = @"wait_update";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ProductInfoCollection : RepositoryList<ProductInfo, ProductInfoCollection>
	{
			public ProductInfoCollection() {}

			public ProductInfoCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							ProductInfo o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class ProductInfo : RepositoryRecord<ProductInfo>, IRecordBase
	{
		#region .ctors and Default Settings
		public ProductInfo()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ProductInfo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("product_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarIsMulti = new TableSchema.TableColumn(schema);
				colvarIsMulti.ColumnName = "is_multi";
				colvarIsMulti.DataType = DbType.Boolean;
				colvarIsMulti.MaxLength = 0;
				colvarIsMulti.AutoIncrement = false;
				colvarIsMulti.IsNullable = false;
				colvarIsMulti.IsPrimaryKey = false;
				colvarIsMulti.IsForeignKey = false;
				colvarIsMulti.IsReadOnly = false;
				colvarIsMulti.DefaultSetting = @"";
				colvarIsMulti.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsMulti);

				TableSchema.TableColumn colvarProductBrandName = new TableSchema.TableColumn(schema);
				colvarProductBrandName.ColumnName = "product_brand_name";
				colvarProductBrandName.DataType = DbType.String;
				colvarProductBrandName.MaxLength = 70;
				colvarProductBrandName.AutoIncrement = false;
				colvarProductBrandName.IsNullable = false;
				colvarProductBrandName.IsPrimaryKey = false;
				colvarProductBrandName.IsForeignKey = false;
				colvarProductBrandName.IsReadOnly = false;
				colvarProductBrandName.DefaultSetting = @"";
				colvarProductBrandName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductBrandName);

				TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
				colvarProductName.ColumnName = "product_name";
				colvarProductName.DataType = DbType.String;
				colvarProductName.MaxLength = 200;
				colvarProductName.AutoIncrement = false;
				colvarProductName.IsNullable = false;
				colvarProductName.IsPrimaryKey = false;
				colvarProductName.IsForeignKey = false;
				colvarProductName.IsReadOnly = false;
				colvarProductName.DefaultSetting = @"";
				colvarProductName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductName);

				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.AnsiString;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.AnsiString;
				colvarModifyId.MaxLength = 50;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);

				TableSchema.TableColumn colvarDataSource = new TableSchema.TableColumn(schema);
				colvarDataSource.ColumnName = "data_source";
				colvarDataSource.DataType = DbType.AnsiString;
				colvarDataSource.MaxLength = 1;
				colvarDataSource.AutoIncrement = false;
				colvarDataSource.IsNullable = false;
				colvarDataSource.IsPrimaryKey = false;
				colvarDataSource.IsForeignKey = false;
				colvarDataSource.IsReadOnly = false;
				colvarDataSource.DefaultSetting = @"";
				colvarDataSource.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDataSource);

				TableSchema.TableColumn colvarWarehouseType = new TableSchema.TableColumn(schema);
				colvarWarehouseType.ColumnName = "warehouse_type";
				colvarWarehouseType.DataType = DbType.Int32;
				colvarWarehouseType.MaxLength = 0;
				colvarWarehouseType.AutoIncrement = false;
				colvarWarehouseType.IsNullable = false;
				colvarWarehouseType.IsPrimaryKey = false;
				colvarWarehouseType.IsForeignKey = false;
				colvarWarehouseType.IsReadOnly = false;
				colvarWarehouseType.DefaultSetting = @"((0))";
				colvarWarehouseType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWarehouseType);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("product_info",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("IsMulti")]
		[Bindable(true)]
		public bool IsMulti
		{
			get { return GetColumnValue<bool>(Columns.IsMulti); }
			set { SetColumnValue(Columns.IsMulti, value); }
		}

		[XmlAttribute("ProductBrandName")]
		[Bindable(true)]
		public string ProductBrandName
		{
			get { return GetColumnValue<string>(Columns.ProductBrandName); }
			set { SetColumnValue(Columns.ProductBrandName, value); }
		}

		[XmlAttribute("ProductName")]
		[Bindable(true)]
		public string ProductName
		{
			get { return GetColumnValue<string>(Columns.ProductName); }
			set { SetColumnValue(Columns.ProductName, value); }
		}

		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}

		[XmlAttribute("DataSource")]
		[Bindable(true)]
		public string DataSource
		{
			get { return GetColumnValue<string>(Columns.DataSource); }
			set { SetColumnValue(Columns.DataSource, value); }
		}

		[XmlAttribute("WarehouseType")]
		[Bindable(true)]
		public int WarehouseType
		{
			get { return GetColumnValue<int>(Columns.WarehouseType); }
			set { SetColumnValue(Columns.WarehouseType, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn GuidColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn IsMultiColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn ProductBrandNameColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn ProductNameColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn StatusColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn ModifyIdColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn DataSourceColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn WarehouseTypeColumn
		{
			get { return Schema.Columns[11]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Guid = @"guid";
			public static string SellerGuid = @"seller_guid";
			public static string IsMulti = @"is_multi";
			public static string ProductBrandName = @"product_brand_name";
			public static string ProductName = @"product_name";
			public static string Status = @"status";
			public static string CreateTime = @"create_time";
			public static string CreateId = @"create_id";
			public static string ModifyTime = @"modify_time";
			public static string ModifyId = @"modify_id";
			public static string DataSource = @"data_source";
			public static string WarehouseType = @"warehouse_type";
		}

		#endregion

	}
}

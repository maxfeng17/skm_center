using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewBuildingSellerCount class.
    /// </summary>
    [Serializable]
    public partial class ViewBuildingSellerCountCollection : ReadOnlyList<ViewBuildingSellerCount, ViewBuildingSellerCountCollection>
    {        
        public ViewBuildingSellerCountCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the View_Building_Seller_Count view.
    /// </summary>
    [Serializable]
    public partial class ViewBuildingSellerCount : ReadOnlyRecord<ViewBuildingSellerCount>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("View_Building_Seller_Count", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBuildingGuid = new TableSchema.TableColumn(schema);
                colvarBuildingGuid.ColumnName = "building_guid";
                colvarBuildingGuid.DataType = DbType.Guid;
                colvarBuildingGuid.MaxLength = 0;
                colvarBuildingGuid.AutoIncrement = false;
                colvarBuildingGuid.IsNullable = false;
                colvarBuildingGuid.IsPrimaryKey = false;
                colvarBuildingGuid.IsForeignKey = false;
                colvarBuildingGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingGuid);
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 20;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = false;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                TableSchema.TableColumn colvarBuildingName = new TableSchema.TableColumn(schema);
                colvarBuildingName.ColumnName = "building_name";
                colvarBuildingName.DataType = DbType.String;
                colvarBuildingName.MaxLength = 50;
                colvarBuildingName.AutoIncrement = false;
                colvarBuildingName.IsNullable = false;
                colvarBuildingName.IsPrimaryKey = false;
                colvarBuildingName.IsForeignKey = false;
                colvarBuildingName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingName);
                
                TableSchema.TableColumn colvarBuildingAddressNumber = new TableSchema.TableColumn(schema);
                colvarBuildingAddressNumber.ColumnName = "building_address_number";
                colvarBuildingAddressNumber.DataType = DbType.String;
                colvarBuildingAddressNumber.MaxLength = 100;
                colvarBuildingAddressNumber.AutoIncrement = false;
                colvarBuildingAddressNumber.IsNullable = true;
                colvarBuildingAddressNumber.IsPrimaryKey = false;
                colvarBuildingAddressNumber.IsForeignKey = false;
                colvarBuildingAddressNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingAddressNumber);
                
                TableSchema.TableColumn colvarBuildingRank = new TableSchema.TableColumn(schema);
                colvarBuildingRank.ColumnName = "building_rank";
                colvarBuildingRank.DataType = DbType.Int32;
                colvarBuildingRank.MaxLength = 0;
                colvarBuildingRank.AutoIncrement = false;
                colvarBuildingRank.IsNullable = false;
                colvarBuildingRank.IsPrimaryKey = false;
                colvarBuildingRank.IsForeignKey = false;
                colvarBuildingRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingRank);
                
                TableSchema.TableColumn colvarSellerCount = new TableSchema.TableColumn(schema);
                colvarSellerCount.ColumnName = "seller_count";
                colvarSellerCount.DataType = DbType.Int32;
                colvarSellerCount.MaxLength = 0;
                colvarSellerCount.AutoIncrement = false;
                colvarSellerCount.IsNullable = true;
                colvarSellerCount.IsPrimaryKey = false;
                colvarSellerCount.IsForeignKey = false;
                colvarSellerCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCount);
                
                TableSchema.TableColumn colvarBuildingStreetName = new TableSchema.TableColumn(schema);
                colvarBuildingStreetName.ColumnName = "building_street_name";
                colvarBuildingStreetName.DataType = DbType.String;
                colvarBuildingStreetName.MaxLength = 100;
                colvarBuildingStreetName.AutoIncrement = false;
                colvarBuildingStreetName.IsNullable = false;
                colvarBuildingStreetName.IsPrimaryKey = false;
                colvarBuildingStreetName.IsForeignKey = false;
                colvarBuildingStreetName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingStreetName);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("View_Building_Seller_Count",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBuildingSellerCount()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBuildingSellerCount(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBuildingSellerCount(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBuildingSellerCount(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BuildingGuid")]
        [Bindable(true)]
        public Guid BuildingGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("building_guid");
		    }
            set 
		    {
			    SetColumnValue("building_guid", value);
            }
        }
	      
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	      
        [XmlAttribute("BuildingName")]
        [Bindable(true)]
        public string BuildingName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_name");
		    }
            set 
		    {
			    SetColumnValue("building_name", value);
            }
        }
	      
        [XmlAttribute("BuildingAddressNumber")]
        [Bindable(true)]
        public string BuildingAddressNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_address_number");
		    }
            set 
		    {
			    SetColumnValue("building_address_number", value);
            }
        }
	      
        [XmlAttribute("BuildingRank")]
        [Bindable(true)]
        public int BuildingRank 
	    {
		    get
		    {
			    return GetColumnValue<int>("building_rank");
		    }
            set 
		    {
			    SetColumnValue("building_rank", value);
            }
        }
	      
        [XmlAttribute("SellerCount")]
        [Bindable(true)]
        public int? SellerCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seller_count");
		    }
            set 
		    {
			    SetColumnValue("seller_count", value);
            }
        }
	      
        [XmlAttribute("BuildingStreetName")]
        [Bindable(true)]
        public string BuildingStreetName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_street_name");
		    }
            set 
		    {
			    SetColumnValue("building_street_name", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BuildingGuid = @"building_guid";
            
            public static string CityName = @"city_name";
            
            public static string BuildingName = @"building_name";
            
            public static string BuildingAddressNumber = @"building_address_number";
            
            public static string BuildingRank = @"building_rank";
            
            public static string SellerCount = @"seller_count";
            
            public static string BuildingStreetName = @"building_street_name";
            
            public static string CityId = @"city_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the Item class.
    /// </summary>
    [Serializable]
    public partial class ItemCollection : RepositoryList<Item, ItemCollection>
    {
        public ItemCollection() {}

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ItemCollection</returns>
        public ItemCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Item o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the item table.
    /// </summary>
    [Serializable]
    public partial class Item : RepositoryRecord<Item>, IRecordBase
    {
        #region .ctors and Default Settings

        public Item()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public Item(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("item", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.String;
                colvarProductId.MaxLength = 50;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = true;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                colvarProductId.DefaultSetting = @"";
                colvarProductId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProductId);

                TableSchema.TableColumn colvarMenuGuid = new TableSchema.TableColumn(schema);
                colvarMenuGuid.ColumnName = "menu_GUID";
                colvarMenuGuid.DataType = DbType.Guid;
                colvarMenuGuid.MaxLength = 0;
                colvarMenuGuid.AutoIncrement = false;
                colvarMenuGuid.IsNullable = false;
                colvarMenuGuid.IsPrimaryKey = false;
                colvarMenuGuid.IsForeignKey = false;
                colvarMenuGuid.IsReadOnly = false;
                colvarMenuGuid.DefaultSetting = @"";
                colvarMenuGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMenuGuid);

                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.AnsiString;
                colvarItemId.MaxLength = 20;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = true;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                colvarItemId.DefaultSetting = @"";
                colvarItemId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemId);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                colvarItemName.DefaultSetting = @"";
                colvarItemName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
                colvarItemOrigPrice.ColumnName = "item_orig_price";
                colvarItemOrigPrice.DataType = DbType.Currency;
                colvarItemOrigPrice.MaxLength = 0;
                colvarItemOrigPrice.AutoIncrement = false;
                colvarItemOrigPrice.IsNullable = false;
                colvarItemOrigPrice.IsPrimaryKey = false;
                colvarItemOrigPrice.IsForeignKey = false;
                colvarItemOrigPrice.IsReadOnly = false;
                colvarItemOrigPrice.DefaultSetting = @"";
                colvarItemOrigPrice.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemOrigPrice);

                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                colvarItemPrice.DefaultSetting = @"";
                colvarItemPrice.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemPrice);

                TableSchema.TableColumn colvarItemDescription = new TableSchema.TableColumn(schema);
                colvarItemDescription.ColumnName = "item_description";
                colvarItemDescription.DataType = DbType.String;
                colvarItemDescription.MaxLength = 1073741823;
                colvarItemDescription.AutoIncrement = false;
                colvarItemDescription.IsNullable = true;
                colvarItemDescription.IsPrimaryKey = false;
                colvarItemDescription.IsForeignKey = false;
                colvarItemDescription.IsReadOnly = false;
                colvarItemDescription.DefaultSetting = @"";
                colvarItemDescription.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemDescription);

                TableSchema.TableColumn colvarItemDefaultDailyAmount = new TableSchema.TableColumn(schema);
                colvarItemDefaultDailyAmount.ColumnName = "item_default_daily_amount";
                colvarItemDefaultDailyAmount.DataType = DbType.Int32;
                colvarItemDefaultDailyAmount.MaxLength = 0;
                colvarItemDefaultDailyAmount.AutoIncrement = false;
                colvarItemDefaultDailyAmount.IsNullable = true;
                colvarItemDefaultDailyAmount.IsPrimaryKey = false;
                colvarItemDefaultDailyAmount.IsForeignKey = false;
                colvarItemDefaultDailyAmount.IsReadOnly = false;
                colvarItemDefaultDailyAmount.DefaultSetting = @"";
                colvarItemDefaultDailyAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemDefaultDailyAmount);

                TableSchema.TableColumn colvarItemImgPath = new TableSchema.TableColumn(schema);
                colvarItemImgPath.ColumnName = "item_img_path";
                colvarItemImgPath.DataType = DbType.String;
                colvarItemImgPath.MaxLength = 500;
                colvarItemImgPath.AutoIncrement = false;
                colvarItemImgPath.IsNullable = true;
                colvarItemImgPath.IsPrimaryKey = false;
                colvarItemImgPath.IsForeignKey = false;
                colvarItemImgPath.IsReadOnly = false;
                colvarItemImgPath.DefaultSetting = @"";
                colvarItemImgPath.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemImgPath);

                TableSchema.TableColumn colvarItemUrl = new TableSchema.TableColumn(schema);
                colvarItemUrl.ColumnName = "item_url";
                colvarItemUrl.DataType = DbType.String;
                colvarItemUrl.MaxLength = 500;
                colvarItemUrl.AutoIncrement = false;
                colvarItemUrl.IsNullable = true;
                colvarItemUrl.IsPrimaryKey = false;
                colvarItemUrl.IsForeignKey = false;
                colvarItemUrl.IsReadOnly = false;
                colvarItemUrl.DefaultSetting = @"";
                colvarItemUrl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemUrl);

                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = true;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                colvarSequence.DefaultSetting = @"";
                colvarSequence.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSequence);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 30;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 30;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarMaxItemCount = new TableSchema.TableColumn(schema);
                colvarMaxItemCount.ColumnName = "max_item_count";
                colvarMaxItemCount.DataType = DbType.Int32;
                colvarMaxItemCount.MaxLength = 0;
                colvarMaxItemCount.AutoIncrement = false;
                colvarMaxItemCount.IsNullable = true;
                colvarMaxItemCount.IsPrimaryKey = false;
                colvarMaxItemCount.IsForeignKey = false;
                colvarMaxItemCount.IsReadOnly = false;
                colvarMaxItemCount.DefaultSetting = @"";
                colvarMaxItemCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMaxItemCount);

                TableSchema.TableColumn colvarPdfItemName = new TableSchema.TableColumn(schema);
                colvarPdfItemName.ColumnName = "pdf_item_name";
                colvarPdfItemName.DataType = DbType.String;
                colvarPdfItemName.MaxLength = 750;
                colvarPdfItemName.AutoIncrement = false;
                colvarPdfItemName.IsNullable = true;
                colvarPdfItemName.IsPrimaryKey = false;
                colvarPdfItemName.IsForeignKey = false;
                colvarPdfItemName.IsReadOnly = false;
                colvarPdfItemName.DefaultSetting = @"";
                colvarPdfItemName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPdfItemName);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("item", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public string ProductId
        {
            get { return GetColumnValue<string>(Columns.ProductId); }
            set { SetColumnValue(Columns.ProductId, value); }
        }

        [XmlAttribute("MenuGuid")]
        [Bindable(true)]
        public Guid MenuGuid
        {
            get { return GetColumnValue<Guid>(Columns.MenuGuid); }
            set { SetColumnValue(Columns.MenuGuid, value); }
        }

        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public string ItemId
        {
            get { return GetColumnValue<string>(Columns.ItemId); }
            set { SetColumnValue(Columns.ItemId, value); }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get { return GetColumnValue<string>(Columns.ItemName); }
            set { SetColumnValue(Columns.ItemName, value); }
        }

        [XmlAttribute("ItemOrigPrice")]
        [Bindable(true)]
        public decimal ItemOrigPrice
        {
            get { return GetColumnValue<decimal>(Columns.ItemOrigPrice); }
            set { SetColumnValue(Columns.ItemOrigPrice, value); }
        }

        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice
        {
            get { return GetColumnValue<decimal>(Columns.ItemPrice); }
            set { SetColumnValue(Columns.ItemPrice, value); }
        }

        [XmlAttribute("ItemDescription")]
        [Bindable(true)]
        public string ItemDescription
        {
            get { return GetColumnValue<string>(Columns.ItemDescription); }
            set { SetColumnValue(Columns.ItemDescription, value); }
        }

        [XmlAttribute("ItemDefaultDailyAmount")]
        [Bindable(true)]
        public int? ItemDefaultDailyAmount
        {
            get { return GetColumnValue<int?>(Columns.ItemDefaultDailyAmount); }
            set { SetColumnValue(Columns.ItemDefaultDailyAmount, value); }
        }

        [XmlAttribute("ItemImgPath")]
        [Bindable(true)]
        public string ItemImgPath
        {
            get { return GetColumnValue<string>(Columns.ItemImgPath); }
            set { SetColumnValue(Columns.ItemImgPath, value); }
        }

        [XmlAttribute("ItemUrl")]
        [Bindable(true)]
        public string ItemUrl
        {
            get { return GetColumnValue<string>(Columns.ItemUrl); }
            set { SetColumnValue(Columns.ItemUrl, value); }
        }

        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public int? Sequence
        {
            get { return GetColumnValue<int?>(Columns.Sequence); }
            set { SetColumnValue(Columns.Sequence, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid
        {
            get { return GetColumnValue<Guid?>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        [XmlAttribute("MaxItemCount")]
        [Bindable(true)]
        public int? MaxItemCount
        {
            get { return GetColumnValue<int?>(Columns.MaxItemCount); }
            set { SetColumnValue(Columns.MaxItemCount, value); }
        }

        [XmlAttribute("PdfItemName")]
        [Bindable(true)]
        public string PdfItemName
        {
            get { return GetColumnValue<string>(Columns.PdfItemName); }
            set { SetColumnValue(Columns.PdfItemName, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ProductIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn MenuGuidColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn ItemIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ItemOrigPriceColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ItemPriceColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ItemDescriptionColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ItemDefaultDailyAmountColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn ItemImgPathColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn ItemUrlColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn MaxItemCountColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn PdfItemNameColumn
        {
            get { return Schema.Columns[19]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"GUID";
            public static string ProductId = @"product_id";
            public static string MenuGuid = @"menu_GUID";
            public static string ItemId = @"item_id";
            public static string ItemName = @"item_name";
            public static string ItemOrigPrice = @"item_orig_price";
            public static string ItemPrice = @"item_price";
            public static string ItemDescription = @"item_description";
            public static string ItemDefaultDailyAmount = @"item_default_daily_amount";
            public static string ItemImgPath = @"item_img_path";
            public static string ItemUrl = @"item_url";
            public static string Sequence = @"sequence";
            public static string Status = @"status";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string BusinessHourGuid = @"business_hour_guid";
            public static string MaxItemCount = @"max_item_count";
            public static string PdfItemName = @"pdf_item_name";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

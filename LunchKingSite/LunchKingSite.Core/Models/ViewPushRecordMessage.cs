using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    [Serializable]
    public partial class ViewPushRecordMessageCollection : ReadOnlyList<ViewPushRecordMessage, ViewPushRecordMessageCollection>
    {
        public ViewPushRecordMessageCollection() { }

    }

    [Serializable]
    public partial class ViewPushRecordMessage : ReadOnlyRecord<ViewPushRecordMessage>, IReadOnlyRecord
    {
        #region .ctors and Default Settings
        public ViewPushRecordMessage()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ViewPushRecordMessage(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        public ViewPushRecordMessage(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewPushRecordMessage(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }
        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_push_record_message", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                TableSchema.TableColumn colvarPushRecordId = new TableSchema.TableColumn(schema);
                colvarPushRecordId.ColumnName = "push_record_id";
                colvarPushRecordId.DataType = DbType.Int32;
                colvarPushRecordId.MaxLength = 0;
                colvarPushRecordId.AutoIncrement = false;
                colvarPushRecordId.IsNullable = false;
                colvarPushRecordId.IsPrimaryKey = false;
                colvarPushRecordId.IsForeignKey = false;
                colvarPushRecordId.IsReadOnly = false;
                colvarPushRecordId.DefaultSetting = @"";
                colvarPushRecordId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPushRecordId);

                TableSchema.TableColumn colvarIsRead = new TableSchema.TableColumn(schema);
                colvarIsRead.ColumnName = "is_read";
                colvarIsRead.DataType = DbType.Boolean;
                colvarIsRead.MaxLength = 0;
                colvarIsRead.AutoIncrement = false;
                colvarIsRead.IsNullable = false;
                colvarIsRead.IsPrimaryKey = false;
                colvarIsRead.IsForeignKey = false;
                colvarIsRead.IsReadOnly = false;
                colvarIsRead.DefaultSetting = @"";
                colvarIsRead.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsRead);

                TableSchema.TableColumn colvarPushTime = new TableSchema.TableColumn(schema);
                colvarPushTime.ColumnName = "push_time";
                colvarPushTime.DataType = DbType.DateTime;
                colvarPushTime.MaxLength = 0;
                colvarPushTime.AutoIncrement = false;
                colvarPushTime.IsNullable = false;
                colvarPushTime.IsPrimaryKey = false;
                colvarPushTime.IsForeignKey = false;
                colvarPushTime.IsReadOnly = false;
                colvarPushTime.DefaultSetting = @"";
                colvarPushTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPushTime);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = true;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarDeviceId = new TableSchema.TableColumn(schema);
                colvarDeviceId.ColumnName = "device_id";
                colvarDeviceId.DataType = DbType.Int32;
                colvarDeviceId.MaxLength = 0;
                colvarDeviceId.AutoIncrement = false;
                colvarDeviceId.IsNullable = false;
                colvarDeviceId.IsPrimaryKey = false;
                colvarDeviceId.IsForeignKey = false;
                colvarDeviceId.IsReadOnly = false;
                colvarDeviceId.DefaultSetting = @"";
                colvarDeviceId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDeviceId);

                TableSchema.TableColumn colvarIsRemove = new TableSchema.TableColumn(schema);
                colvarIsRemove.ColumnName = "is_remove";
                colvarIsRemove.DataType = DbType.Boolean;
                colvarIsRemove.MaxLength = 0;
                colvarIsRemove.AutoIncrement = false;
                colvarIsRemove.IsNullable = false;
                colvarIsRemove.IsPrimaryKey = false;
                colvarIsRemove.IsForeignKey = false;
                colvarIsRemove.IsReadOnly = false;
                colvarIsRemove.DefaultSetting = @"";
                colvarIsRemove.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsRemove);

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarEventType = new TableSchema.TableColumn(schema);
                colvarEventType.ColumnName = "event_type";
                colvarEventType.DataType = DbType.Int32;
                colvarEventType.MaxLength = 0;
                colvarEventType.AutoIncrement = false;
                colvarEventType.IsNullable = false;
                colvarEventType.IsPrimaryKey = false;
                colvarEventType.IsForeignKey = false;
                colvarEventType.IsReadOnly = false;
                colvarEventType.DefaultSetting = @"";
                colvarEventType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventType);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Boolean;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarSubject = new TableSchema.TableColumn(schema);
                colvarSubject.ColumnName = "subject";
                colvarSubject.DataType = DbType.String;
                colvarSubject.MaxLength = 100;
                colvarSubject.AutoIncrement = false;
                colvarSubject.IsNullable = false;
                colvarSubject.IsPrimaryKey = false;
                colvarSubject.IsForeignKey = false;
                colvarSubject.IsReadOnly = false;
                colvarSubject.DefaultSetting = @"";
                colvarSubject.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSubject);

                TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
                colvarContent.ColumnName = "content";
                colvarContent.DataType = DbType.String;
                colvarContent.MaxLength = 250;
                colvarContent.AutoIncrement = false;
                colvarContent.IsNullable = false;
                colvarContent.IsPrimaryKey = false;
                colvarContent.IsForeignKey = false;
                colvarContent.IsReadOnly = false;
                colvarContent.DefaultSetting = @"";
                colvarContent.ForeignKeyTableName = "";
                schema.Columns.Add(colvarContent);

                TableSchema.TableColumn colvarAction = new TableSchema.TableColumn(schema);
                colvarAction.ColumnName = "action";
                colvarAction.DataType = DbType.String;
                colvarAction.MaxLength = 2147483647;
                colvarAction.AutoIncrement = false;
                colvarAction.IsNullable = false;
                colvarAction.IsPrimaryKey = false;
                colvarAction.IsForeignKey = false;
                colvarAction.IsReadOnly = false;
                colvarAction.DefaultSetting = @"";
                colvarAction.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAction);

                TableSchema.TableColumn colvarSendStartTime = new TableSchema.TableColumn(schema);
                colvarSendStartTime.ColumnName = "send_start_time";
                colvarSendStartTime.DataType = DbType.DateTime;
                colvarSendStartTime.MaxLength = 0;
                colvarSendStartTime.AutoIncrement = false;
                colvarSendStartTime.IsNullable = false;
                colvarSendStartTime.IsPrimaryKey = false;
                colvarSendStartTime.IsForeignKey = false;
                colvarSendStartTime.IsReadOnly = false;
                colvarSendStartTime.DefaultSetting = @"";
                colvarSendStartTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSendStartTime);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarSendEndTime = new TableSchema.TableColumn(schema);
                colvarSendEndTime.ColumnName = "send_end_time";
                colvarSendEndTime.DataType = DbType.DateTime;
                colvarSendEndTime.MaxLength = 0;
                colvarSendEndTime.AutoIncrement = false;
                colvarSendEndTime.IsNullable = false;
                colvarSendEndTime.IsPrimaryKey = false;
                colvarSendEndTime.IsForeignKey = false;
                colvarSendEndTime.IsReadOnly = false;
                colvarSendEndTime.DefaultSetting = @"";
                colvarSendEndTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSendEndTime);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                colvarOrderGuid.DefaultSetting = @"";
                colvarOrderGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarServicePara = new TableSchema.TableColumn(schema);
                colvarServicePara.ColumnName = "service_para";
                colvarServicePara.DataType = DbType.AnsiString;
                colvarServicePara.MaxLength = 50;
                colvarServicePara.AutoIncrement = false;
                colvarServicePara.IsNullable = true;
                colvarServicePara.IsPrimaryKey = false;
                colvarServicePara.IsForeignKey = false;
                colvarServicePara.IsReadOnly = false;
                colvarServicePara.DefaultSetting = @"";
                colvarServicePara.ForeignKeyTableName = "";
                schema.Columns.Add(colvarServicePara);

                TableSchema.TableColumn colvarServiceType = new TableSchema.TableColumn(schema);
                colvarServiceType.ColumnName = "service_type";
                colvarServiceType.DataType = DbType.Int32;
                colvarServiceType.MaxLength = 0;
                colvarServiceType.AutoIncrement = false;
                colvarServiceType.IsNullable = true;
                colvarServiceType.IsPrimaryKey = false;
                colvarServiceType.IsForeignKey = false;
                colvarServiceType.IsReadOnly = false;
                colvarServiceType.DefaultSetting = @"";
                colvarServiceType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarServiceType);

                TableSchema.TableColumn colvarMsgId = new TableSchema.TableColumn(schema);
                colvarMsgId.ColumnName = "msg_id";
                colvarMsgId.DataType = DbType.String;
                colvarMsgId.MaxLength = 50;
                colvarMsgId.AutoIncrement = false;
                colvarMsgId.IsNullable = true;
                colvarMsgId.IsPrimaryKey = false;
                colvarMsgId.IsForeignKey = false;
                colvarMsgId.IsReadOnly = false;
                colvarMsgId.DefaultSetting = @"";
                colvarMsgId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMsgId);

                TableSchema.TableColumn colvarCardType = new TableSchema.TableColumn(schema);
                colvarCardType.ColumnName = "card_type";
                colvarCardType.DataType = DbType.Int32;
                colvarCardType.MaxLength = 0;
                colvarCardType.AutoIncrement = false;
                colvarCardType.IsNullable = true;
                colvarCardType.IsPrimaryKey = false;
                colvarCardType.IsForeignKey = false;
                colvarCardType.IsReadOnly = false;
                colvarCardType.DefaultSetting = @"";
                colvarCardType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardType);

                TableSchema.TableColumn colvarSkmBeaconMessageId = new TableSchema.TableColumn(schema);
                colvarSkmBeaconMessageId.ColumnName = "skm_beacon_message_id";
                colvarSkmBeaconMessageId.DataType = DbType.Int32;
                colvarSkmBeaconMessageId.MaxLength = 0;
                colvarSkmBeaconMessageId.AutoIncrement = false;
                colvarSkmBeaconMessageId.IsNullable = true;
                colvarSkmBeaconMessageId.IsPrimaryKey = false;
                colvarSkmBeaconMessageId.IsForeignKey = false;
                colvarSkmBeaconMessageId.IsReadOnly = false;
                colvarSkmBeaconMessageId.DefaultSetting = @"";
                colvarSkmBeaconMessageId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSkmBeaconMessageId);

                TableSchema.TableColumn colvarBrandPromoId = new TableSchema.TableColumn(schema);
                colvarBrandPromoId.ColumnName = "brand_promo_id";
                colvarBrandPromoId.DataType = DbType.Int32;
                colvarBrandPromoId.MaxLength = 0;
                colvarBrandPromoId.AutoIncrement = false;
                colvarBrandPromoId.IsNullable = true;
                colvarBrandPromoId.IsPrimaryKey = false;
                colvarBrandPromoId.IsForeignKey = false;
                colvarBrandPromoId.IsReadOnly = false;
                colvarBrandPromoId.DefaultSetting = @"";
                colvarBrandPromoId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBrandPromoId);

                TableSchema.TableColumn colvarCustomUrl = new TableSchema.TableColumn(schema);
                colvarCustomUrl.ColumnName = "custom_url";
                colvarCustomUrl.DataType = DbType.AnsiString;
                colvarCustomUrl.MaxLength = 200;
                colvarCustomUrl.AutoIncrement = false;
                colvarCustomUrl.IsNullable = true;
                colvarCustomUrl.IsPrimaryKey = false;
                colvarCustomUrl.IsForeignKey = false;
                colvarCustomUrl.IsReadOnly = false;
                colvarCustomUrl.DefaultSetting = @"";
                colvarCustomUrl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCustomUrl);

                TableSchema.TableColumn colvarEventPromoId = new TableSchema.TableColumn(schema);
                colvarEventPromoId.ColumnName = "event_promo_id";
                colvarEventPromoId.DataType = DbType.Int32;
                colvarEventPromoId.MaxLength = 0;
                colvarEventPromoId.AutoIncrement = false;
                colvarEventPromoId.IsNullable = true;
                colvarEventPromoId.IsPrimaryKey = false;
                colvarEventPromoId.IsForeignKey = false;
                colvarEventPromoId.IsReadOnly = false;
                colvarEventPromoId.DefaultSetting = @"";
                colvarEventPromoId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventPromoId);

                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                colvarSellerId.DefaultSetting = @"";
                colvarSellerId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerId);

                TableSchema.TableColumn colvarVourcherId = new TableSchema.TableColumn(schema);
                colvarVourcherId.ColumnName = "vourcher_id";
                colvarVourcherId.DataType = DbType.Int32;
                colvarVourcherId.MaxLength = 0;
                colvarVourcherId.AutoIncrement = false;
                colvarVourcherId.IsNullable = true;
                colvarVourcherId.IsPrimaryKey = false;
                colvarVourcherId.IsForeignKey = false;
                colvarVourcherId.IsReadOnly = false;
                colvarVourcherId.DefaultSetting = @"";
                colvarVourcherId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVourcherId);

                TableSchema.TableColumn colvarAreaId = new TableSchema.TableColumn(schema);
                colvarAreaId.ColumnName = "area_id";
                colvarAreaId.DataType = DbType.Int32;
                colvarAreaId.MaxLength = 0;
                colvarAreaId.AutoIncrement = false;
                colvarAreaId.IsNullable = true;
                colvarAreaId.IsPrimaryKey = false;
                colvarAreaId.IsForeignKey = false;
                colvarAreaId.IsReadOnly = false;
                colvarAreaId.DefaultSetting = @"";
                colvarAreaId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAreaId);

                TableSchema.TableColumn colvarChannelId = new TableSchema.TableColumn(schema);
                colvarChannelId.ColumnName = "channel_id";
                colvarChannelId.DataType = DbType.Int32;
                colvarChannelId.MaxLength = 0;
                colvarChannelId.AutoIncrement = false;
                colvarChannelId.IsNullable = true;
                colvarChannelId.IsPrimaryKey = false;
                colvarChannelId.IsForeignKey = false;
                colvarChannelId.IsReadOnly = false;
                colvarChannelId.DefaultSetting = @"";
                colvarChannelId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarChannelId);

                TableSchema.TableColumn colvarFamportId = new TableSchema.TableColumn(schema);
                colvarFamportId.ColumnName = "famport_id";
                colvarFamportId.DataType = DbType.Int32;
                colvarFamportId.MaxLength = 0;
                colvarFamportId.AutoIncrement = false;
                colvarFamportId.IsNullable = true;
                colvarFamportId.IsPrimaryKey = false;
                colvarFamportId.IsForeignKey = false;
                colvarFamportId.IsReadOnly = false;
                colvarFamportId.DefaultSetting = @"";
                colvarFamportId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFamportId);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_push_record_message", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("PushRecordId")]
        [Bindable(true)]
        public int PushRecordId
        {
            get { return GetColumnValue<int>(Columns.PushRecordId); }
            set { SetColumnValue(Columns.PushRecordId, value); }
        }

        [XmlAttribute("IsRead")]
        [Bindable(true)]
        public bool IsRead
        {
            get { return GetColumnValue<bool>(Columns.IsRead); }
            set { SetColumnValue(Columns.IsRead, value); }
        }

        [XmlAttribute("PushTime")]
        [Bindable(true)]
        public DateTime PushTime
        {
            get { return GetColumnValue<DateTime>(Columns.PushTime); }
            set { SetColumnValue(Columns.PushTime, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int? UserId
        {
            get { return GetColumnValue<int?>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("DeviceId")]
        [Bindable(true)]
        public int DeviceId
        {
            get { return GetColumnValue<int>(Columns.DeviceId); }
            set { SetColumnValue(Columns.DeviceId, value); }
        }

        [XmlAttribute("IsRemove")]
        [Bindable(true)]
        public bool IsRemove
        {
            get { return GetColumnValue<bool>(Columns.IsRemove); }
            set { SetColumnValue(Columns.IsRemove, value); }
        }

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("EventType")]
        [Bindable(true)]
        public int EventType
        {
            get { return GetColumnValue<int>(Columns.EventType); }
            set { SetColumnValue(Columns.EventType, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public bool Status
        {
            get { return GetColumnValue<bool>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("Subject")]
        [Bindable(true)]
        public string Subject
        {
            get { return GetColumnValue<string>(Columns.Subject); }
            set { SetColumnValue(Columns.Subject, value); }
        }

        [XmlAttribute("Content")]
        [Bindable(true)]
        public string Content
        {
            get { return GetColumnValue<string>(Columns.Content); }
            set { SetColumnValue(Columns.Content, value); }
        }

        [XmlAttribute("Action")]
        [Bindable(true)]
        public string Action
        {
            get { return GetColumnValue<string>(Columns.Action); }
            set { SetColumnValue(Columns.Action, value); }
        }

        [XmlAttribute("SendStartTime")]
        [Bindable(true)]
        public DateTime SendStartTime
        {
            get { return GetColumnValue<DateTime>(Columns.SendStartTime); }
            set { SetColumnValue(Columns.SendStartTime, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId
        {
            get { return GetColumnValue<int>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("SendEndTime")]
        [Bindable(true)]
        public DateTime SendEndTime
        {
            get { return GetColumnValue<DateTime>(Columns.SendEndTime); }
            set { SetColumnValue(Columns.SendEndTime, value); }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid
        {
            get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
            set { SetColumnValue(Columns.OrderGuid, value); }
        }

        [XmlAttribute("ServicePara")]
        [Bindable(true)]
        public string ServicePara
        {
            get { return GetColumnValue<string>(Columns.ServicePara); }
            set { SetColumnValue(Columns.ServicePara, value); }
        }

        [XmlAttribute("ServiceType")]
        [Bindable(true)]
        public int? ServiceType
        {
            get { return GetColumnValue<int?>(Columns.ServiceType); }
            set { SetColumnValue(Columns.ServiceType, value); }
        }

        [XmlAttribute("MsgId")]
        [Bindable(true)]
        public string MsgId
        {
            get { return GetColumnValue<string>(Columns.MsgId); }
            set { SetColumnValue(Columns.MsgId, value); }
        }

        [XmlAttribute("CardType")]
        [Bindable(true)]
        public int? CardType
        {
            get { return GetColumnValue<int?>(Columns.CardType); }
            set { SetColumnValue(Columns.CardType, value); }
        }

        [XmlAttribute("SkmBeaconMessageId")]
        [Bindable(true)]
        public int? SkmBeaconMessageId
        {
            get { return GetColumnValue<int?>(Columns.SkmBeaconMessageId); }
            set { SetColumnValue(Columns.SkmBeaconMessageId, value); }
        }

        [XmlAttribute("BrandPromoId")]
        [Bindable(true)]
        public int? BrandPromoId
        {
            get { return GetColumnValue<int?>(Columns.BrandPromoId); }
            set { SetColumnValue(Columns.BrandPromoId, value); }
        }

        [XmlAttribute("CustomUrl")]
        [Bindable(true)]
        public string CustomUrl
        {
            get { return GetColumnValue<string>(Columns.CustomUrl); }
            set { SetColumnValue(Columns.CustomUrl, value); }
        }

        [XmlAttribute("EventPromoId")]
        [Bindable(true)]
        public int? EventPromoId
        {
            get { return GetColumnValue<int?>(Columns.EventPromoId); }
            set { SetColumnValue(Columns.EventPromoId, value); }
        }

        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId
        {
            get { return GetColumnValue<string>(Columns.SellerId); }
            set { SetColumnValue(Columns.SellerId, value); }
        }

        [XmlAttribute("VourcherId")]
        [Bindable(true)]
        public int? VourcherId
        {
            get { return GetColumnValue<int?>(Columns.VourcherId); }
            set { SetColumnValue(Columns.VourcherId, value); }
        }

        [XmlAttribute("AreaId")]
        [Bindable(true)]
        public int? AreaId
        {
            get { return GetColumnValue<int?>(Columns.AreaId); }
            set { SetColumnValue(Columns.AreaId, value); }
        }

        [XmlAttribute("ChannelId")]
        [Bindable(true)]
        public int? ChannelId
        {
            get { return GetColumnValue<int?>(Columns.ChannelId); }
            set { SetColumnValue(Columns.ChannelId, value); }
        }

        [XmlAttribute("FamportId")]
        [Bindable(true)]
        public int? FamportId
        {
            get { return GetColumnValue<int?>(Columns.FamportId); }
            set { SetColumnValue(Columns.FamportId, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid
        {
            get { return GetColumnValue<Guid?>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        #endregion

        #region Typed Columns

        public static TableSchema.TableColumn PushRecordIdColumn
        {
            get { return Schema.Columns[0]; }
        }

        public static TableSchema.TableColumn IsReadColumn
        {
            get { return Schema.Columns[1]; }
        }

        public static TableSchema.TableColumn PushTimeColumn
        {
            get { return Schema.Columns[2]; }
        }

        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[3]; }
        }

        public static TableSchema.TableColumn DeviceIdColumn
        {
            get { return Schema.Columns[4]; }
        }

        public static TableSchema.TableColumn IsRemoveColumn
        {
            get { return Schema.Columns[5]; }
        }

        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[6]; }
        }

        public static TableSchema.TableColumn EventTypeColumn
        {
            get { return Schema.Columns[7]; }
        }

        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[8]; }
        }

        public static TableSchema.TableColumn SubjectColumn
        {
            get { return Schema.Columns[9]; }
        }

        public static TableSchema.TableColumn ContentColumn
        {
            get { return Schema.Columns[10]; }
        }

        public static TableSchema.TableColumn ActionColumn
        {
            get { return Schema.Columns[11]; }
        }

        public static TableSchema.TableColumn SendStartTimeColumn
        {
            get { return Schema.Columns[12]; }
        }

        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[13]; }
        }

        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[14]; }
        }

        public static TableSchema.TableColumn SendEndTimeColumn
        {
            get { return Schema.Columns[15]; }
        }

        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[16]; }
        }

        public static TableSchema.TableColumn ServiceParaColumn
        {
            get { return Schema.Columns[17]; }
        }

        public static TableSchema.TableColumn ServiceTypeColumn
        {
            get { return Schema.Columns[18]; }
        }

        public static TableSchema.TableColumn MsgIdColumn
        {
            get { return Schema.Columns[19]; }
        }

        public static TableSchema.TableColumn CardTypeColumn
        {
            get { return Schema.Columns[20]; }
        }

        public static TableSchema.TableColumn SkmBeaconMessageIdColumn
        {
            get { return Schema.Columns[21]; }
        }

        public static TableSchema.TableColumn BrandPromoIdColumn
        {
            get { return Schema.Columns[22]; }
        }

        public static TableSchema.TableColumn CustomUrlColumn
        {
            get { return Schema.Columns[23]; }
        }

        public static TableSchema.TableColumn EventPromoIdColumn
        {
            get { return Schema.Columns[24]; }
        }

        public static TableSchema.TableColumn SellerIdColumn
        {
            get { return Schema.Columns[25]; }
        }

        public static TableSchema.TableColumn VourcherIdColumn
        {
            get { return Schema.Columns[26]; }
        }

        public static TableSchema.TableColumn AreaIdColumn
        {
            get { return Schema.Columns[27]; }
        }

        public static TableSchema.TableColumn ChannelIdColumn
        {
            get { return Schema.Columns[28]; }
        }

        public static TableSchema.TableColumn FamportIdColumn
        {
            get { return Schema.Columns[29]; }
        }

        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[30]; }
        }

        #endregion

        #region Columns Struct

        public struct Columns
        {
            public static string PushRecordId = @"push_record_id";
            public static string IsRead = @"is_read";
            public static string PushTime = @"push_time";
            public static string UserId = @"user_id";
            public static string DeviceId = @"device_id";
            public static string IsRemove = @"is_remove";
            public static string Id = @"id";
            public static string EventType = @"event_type";
            public static string Status = @"status";
            public static string Subject = @"subject";
            public static string Content = @"content";
            public static string Action = @"action";
            public static string SendStartTime = @"send_start_time";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string SendEndTime = @"send_end_time";
            public static string OrderGuid = @"order_guid";
            public static string ServicePara = @"service_para";
            public static string ServiceType = @"service_type";
            public static string MsgId = @"msg_id";
            public static string CardType = @"card_type";
            public static string SkmBeaconMessageId = @"skm_beacon_message_id";
            public static string BrandPromoId = @"brand_promo_id";
            public static string CustomUrl = @"custom_url";
            public static string EventPromoId = @"event_promo_id";
            public static string SellerId = @"seller_id";
            public static string VourcherId = @"vourcher_id";
            public static string AreaId = @"area_id";
            public static string ChannelId = @"channel_id";
            public static string FamportId = @"famport_id";
            public static string BusinessHourGuid = @"business_hour_guid";
        }

        #endregion

        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

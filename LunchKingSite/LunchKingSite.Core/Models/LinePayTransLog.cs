using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the LinePayTransLog class.
	/// </summary>
    [Serializable]
	public partial class LinePayTransLogCollection : RepositoryList<LinePayTransLog, LinePayTransLogCollection>
	{	   
		public LinePayTransLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>LinePayTransLogCollection</returns>
		public LinePayTransLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                LinePayTransLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the line_pay_trans_log table.
	/// </summary>
	[Serializable]
	public partial class LinePayTransLog : RepositoryRecord<LinePayTransLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public LinePayTransLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public LinePayTransLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("line_pay_trans_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTicketId = new TableSchema.TableColumn(schema);
				colvarTicketId.ColumnName = "ticket_id";
				colvarTicketId.DataType = DbType.AnsiString;
				colvarTicketId.MaxLength = 100;
				colvarTicketId.AutoIncrement = false;
				colvarTicketId.IsNullable = false;
				colvarTicketId.IsPrimaryKey = false;
				colvarTicketId.IsForeignKey = false;
				colvarTicketId.IsReadOnly = false;
				colvarTicketId.DefaultSetting = @"";
				colvarTicketId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTicketId);
				
				TableSchema.TableColumn colvarTransactionId = new TableSchema.TableColumn(schema);
				colvarTransactionId.ColumnName = "transaction_id";
				colvarTransactionId.DataType = DbType.AnsiString;
				colvarTransactionId.MaxLength = 40;
				colvarTransactionId.AutoIncrement = false;
				colvarTransactionId.IsNullable = false;
				colvarTransactionId.IsPrimaryKey = false;
				colvarTransactionId.IsForeignKey = false;
				colvarTransactionId.IsReadOnly = false;
				colvarTransactionId.DefaultSetting = @"";
				colvarTransactionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransactionId);
				
				TableSchema.TableColumn colvarLinePayTransactionId = new TableSchema.TableColumn(schema);
				colvarLinePayTransactionId.ColumnName = "line_pay_transaction_id";
				colvarLinePayTransactionId.DataType = DbType.AnsiString;
				colvarLinePayTransactionId.MaxLength = 20;
				colvarLinePayTransactionId.AutoIncrement = false;
				colvarLinePayTransactionId.IsNullable = true;
				colvarLinePayTransactionId.IsPrimaryKey = false;
				colvarLinePayTransactionId.IsForeignKey = false;
				colvarLinePayTransactionId.IsReadOnly = false;
				colvarLinePayTransactionId.DefaultSetting = @"";
				colvarLinePayTransactionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLinePayTransactionId);
				
				TableSchema.TableColumn colvarTransStatus = new TableSchema.TableColumn(schema);
				colvarTransStatus.ColumnName = "trans_status";
				colvarTransStatus.DataType = DbType.Byte;
				colvarTransStatus.MaxLength = 0;
				colvarTransStatus.AutoIncrement = false;
				colvarTransStatus.IsNullable = false;
				colvarTransStatus.IsPrimaryKey = false;
				colvarTransStatus.IsForeignKey = false;
				colvarTransStatus.IsReadOnly = false;
				
						colvarTransStatus.DefaultSetting = @"((0))";
				colvarTransStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransStatus);
				
				TableSchema.TableColumn colvarTransMsg = new TableSchema.TableColumn(schema);
				colvarTransMsg.ColumnName = "trans_msg";
				colvarTransMsg.DataType = DbType.String;
				colvarTransMsg.MaxLength = 100;
				colvarTransMsg.AutoIncrement = false;
				colvarTransMsg.IsNullable = true;
				colvarTransMsg.IsPrimaryKey = false;
				colvarTransMsg.IsForeignKey = false;
				colvarTransMsg.IsReadOnly = false;
				colvarTransMsg.DefaultSetting = @"";
				colvarTransMsg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransMsg);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCaptureRetryTimes = new TableSchema.TableColumn(schema);
				colvarCaptureRetryTimes.ColumnName = "capture_retry_times";
				colvarCaptureRetryTimes.DataType = DbType.Int32;
				colvarCaptureRetryTimes.MaxLength = 0;
				colvarCaptureRetryTimes.AutoIncrement = false;
				colvarCaptureRetryTimes.IsNullable = false;
				colvarCaptureRetryTimes.IsPrimaryKey = false;
				colvarCaptureRetryTimes.IsForeignKey = false;
				colvarCaptureRetryTimes.IsReadOnly = false;
				
						colvarCaptureRetryTimes.DefaultSetting = @"((0))";
				colvarCaptureRetryTimes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCaptureRetryTimes);
				
				TableSchema.TableColumn colvarVoidRetryTimes = new TableSchema.TableColumn(schema);
				colvarVoidRetryTimes.ColumnName = "void_retry_times";
				colvarVoidRetryTimes.DataType = DbType.Int32;
				colvarVoidRetryTimes.MaxLength = 0;
				colvarVoidRetryTimes.AutoIncrement = false;
				colvarVoidRetryTimes.IsNullable = false;
				colvarVoidRetryTimes.IsPrimaryKey = false;
				colvarVoidRetryTimes.IsForeignKey = false;
				colvarVoidRetryTimes.IsReadOnly = false;
				
						colvarVoidRetryTimes.DefaultSetting = @"((0))";
				colvarVoidRetryTimes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVoidRetryTimes);
				
				TableSchema.TableColumn colvarBuyFailRefundRetryTimes = new TableSchema.TableColumn(schema);
				colvarBuyFailRefundRetryTimes.ColumnName = "buy_fail_refund_retry_times";
				colvarBuyFailRefundRetryTimes.DataType = DbType.Int32;
				colvarBuyFailRefundRetryTimes.MaxLength = 0;
				colvarBuyFailRefundRetryTimes.AutoIncrement = false;
				colvarBuyFailRefundRetryTimes.IsNullable = false;
				colvarBuyFailRefundRetryTimes.IsPrimaryKey = false;
				colvarBuyFailRefundRetryTimes.IsForeignKey = false;
				colvarBuyFailRefundRetryTimes.IsReadOnly = false;
				
						colvarBuyFailRefundRetryTimes.DefaultSetting = @"((0))";
				colvarBuyFailRefundRetryTimes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuyFailRefundRetryTimes);
				
				TableSchema.TableColumn colvarIsCaptureSeparate = new TableSchema.TableColumn(schema);
				colvarIsCaptureSeparate.ColumnName = "is_capture_separate";
				colvarIsCaptureSeparate.DataType = DbType.Boolean;
				colvarIsCaptureSeparate.MaxLength = 0;
				colvarIsCaptureSeparate.AutoIncrement = false;
				colvarIsCaptureSeparate.IsNullable = false;
				colvarIsCaptureSeparate.IsPrimaryKey = false;
				colvarIsCaptureSeparate.IsForeignKey = false;
				colvarIsCaptureSeparate.IsReadOnly = false;
				
						colvarIsCaptureSeparate.DefaultSetting = @"((0))";
				colvarIsCaptureSeparate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCaptureSeparate);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				
						colvarModifyTime.DefaultSetting = @"(getdate())";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarAlreadyRefundTotal = new TableSchema.TableColumn(schema);
				colvarAlreadyRefundTotal.ColumnName = "already_refund_total";
				colvarAlreadyRefundTotal.DataType = DbType.Int32;
				colvarAlreadyRefundTotal.MaxLength = 0;
				colvarAlreadyRefundTotal.AutoIncrement = false;
				colvarAlreadyRefundTotal.IsNullable = false;
				colvarAlreadyRefundTotal.IsPrimaryKey = false;
				colvarAlreadyRefundTotal.IsForeignKey = false;
				colvarAlreadyRefundTotal.IsReadOnly = false;
				
						colvarAlreadyRefundTotal.DefaultSetting = @"((0))";
				colvarAlreadyRefundTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAlreadyRefundTotal);
				
				TableSchema.TableColumn colvarCanRetry = new TableSchema.TableColumn(schema);
				colvarCanRetry.ColumnName = "can_retry";
				colvarCanRetry.DataType = DbType.Boolean;
				colvarCanRetry.MaxLength = 0;
				colvarCanRetry.AutoIncrement = false;
				colvarCanRetry.IsNullable = false;
				colvarCanRetry.IsPrimaryKey = false;
				colvarCanRetry.IsForeignKey = false;
				colvarCanRetry.IsReadOnly = false;
				
						colvarCanRetry.DefaultSetting = @"((0))";
				colvarCanRetry.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCanRetry);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("line_pay_trans_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TicketId")]
		[Bindable(true)]
		public string TicketId 
		{
			get { return GetColumnValue<string>(Columns.TicketId); }
			set { SetColumnValue(Columns.TicketId, value); }
		}
		  
		[XmlAttribute("TransactionId")]
		[Bindable(true)]
		public string TransactionId 
		{
			get { return GetColumnValue<string>(Columns.TransactionId); }
			set { SetColumnValue(Columns.TransactionId, value); }
		}
		  
		[XmlAttribute("LinePayTransactionId")]
		[Bindable(true)]
		public string LinePayTransactionId 
		{
			get { return GetColumnValue<string>(Columns.LinePayTransactionId); }
			set { SetColumnValue(Columns.LinePayTransactionId, value); }
		}
		  
		[XmlAttribute("TransStatus")]
		[Bindable(true)]
		public byte TransStatus 
		{
			get { return GetColumnValue<byte>(Columns.TransStatus); }
			set { SetColumnValue(Columns.TransStatus, value); }
		}
		  
		[XmlAttribute("TransMsg")]
		[Bindable(true)]
		public string TransMsg 
		{
			get { return GetColumnValue<string>(Columns.TransMsg); }
			set { SetColumnValue(Columns.TransMsg, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CaptureRetryTimes")]
		[Bindable(true)]
		public int CaptureRetryTimes 
		{
			get { return GetColumnValue<int>(Columns.CaptureRetryTimes); }
			set { SetColumnValue(Columns.CaptureRetryTimes, value); }
		}
		  
		[XmlAttribute("VoidRetryTimes")]
		[Bindable(true)]
		public int VoidRetryTimes 
		{
			get { return GetColumnValue<int>(Columns.VoidRetryTimes); }
			set { SetColumnValue(Columns.VoidRetryTimes, value); }
		}
		  
		[XmlAttribute("BuyFailRefundRetryTimes")]
		[Bindable(true)]
		public int BuyFailRefundRetryTimes 
		{
			get { return GetColumnValue<int>(Columns.BuyFailRefundRetryTimes); }
			set { SetColumnValue(Columns.BuyFailRefundRetryTimes, value); }
		}
		  
		[XmlAttribute("IsCaptureSeparate")]
		[Bindable(true)]
		public bool IsCaptureSeparate 
		{
			get { return GetColumnValue<bool>(Columns.IsCaptureSeparate); }
			set { SetColumnValue(Columns.IsCaptureSeparate, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("AlreadyRefundTotal")]
		[Bindable(true)]
		public int AlreadyRefundTotal 
		{
			get { return GetColumnValue<int>(Columns.AlreadyRefundTotal); }
			set { SetColumnValue(Columns.AlreadyRefundTotal, value); }
		}
		  
		[XmlAttribute("CanRetry")]
		[Bindable(true)]
		public bool CanRetry 
		{
			get { return GetColumnValue<bool>(Columns.CanRetry); }
			set { SetColumnValue(Columns.CanRetry, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TicketIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TransactionIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn LinePayTransactionIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn TransStatusColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn TransMsgColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CaptureRetryTimesColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn VoidRetryTimesColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn BuyFailRefundRetryTimesColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCaptureSeparateColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn AlreadyRefundTotalColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CanRetryColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TicketId = @"ticket_id";
			 public static string TransactionId = @"transaction_id";
			 public static string LinePayTransactionId = @"line_pay_transaction_id";
			 public static string TransStatus = @"trans_status";
			 public static string TransMsg = @"trans_msg";
			 public static string CreateTime = @"create_time";
			 public static string CaptureRetryTimes = @"capture_retry_times";
			 public static string VoidRetryTimes = @"void_retry_times";
			 public static string BuyFailRefundRetryTimes = @"buy_fail_refund_retry_times";
			 public static string IsCaptureSeparate = @"is_capture_separate";
			 public static string ModifyTime = @"modify_time";
			 public static string AlreadyRefundTotal = @"already_refund_total";
			 public static string CanRetry = @"can_retry";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

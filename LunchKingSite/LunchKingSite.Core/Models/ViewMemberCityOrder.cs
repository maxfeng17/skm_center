using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewMemberCityOrder class.
    /// </summary>
    [Serializable]
    public partial class ViewMemberCityOrderCollection : ReadOnlyList<ViewMemberCityOrder, ViewMemberCityOrderCollection>
    {        
        public ViewMemberCityOrderCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_member_city_order view.
    /// </summary>
    [Serializable]
    public partial class ViewMemberCityOrder : ReadOnlyRecord<ViewMemberCityOrder>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_member_city_order", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 20;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = false;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 10;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = true;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
                colvarLastName.ColumnName = "last_name";
                colvarLastName.DataType = DbType.String;
                colvarLastName.MaxLength = 50;
                colvarLastName.AutoIncrement = false;
                colvarLastName.IsNullable = true;
                colvarLastName.IsPrimaryKey = false;
                colvarLastName.IsForeignKey = false;
                colvarLastName.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastName);
                
                TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
                colvarFirstName.ColumnName = "first_name";
                colvarFirstName.DataType = DbType.String;
                colvarFirstName.MaxLength = 50;
                colvarFirstName.AutoIncrement = false;
                colvarFirstName.IsNullable = true;
                colvarFirstName.IsPrimaryKey = false;
                colvarFirstName.IsForeignKey = false;
                colvarFirstName.IsReadOnly = false;
                
                schema.Columns.Add(colvarFirstName);
                
                TableSchema.TableColumn colvarBuildingGuid = new TableSchema.TableColumn(schema);
                colvarBuildingGuid.ColumnName = "building_GUID";
                colvarBuildingGuid.DataType = DbType.Guid;
                colvarBuildingGuid.MaxLength = 0;
                colvarBuildingGuid.AutoIncrement = false;
                colvarBuildingGuid.IsNullable = true;
                colvarBuildingGuid.IsPrimaryKey = false;
                colvarBuildingGuid.IsForeignKey = false;
                colvarBuildingGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingGuid);
                
                TableSchema.TableColumn colvarBirthday = new TableSchema.TableColumn(schema);
                colvarBirthday.ColumnName = "birthday";
                colvarBirthday.DataType = DbType.DateTime;
                colvarBirthday.MaxLength = 0;
                colvarBirthday.AutoIncrement = false;
                colvarBirthday.IsNullable = true;
                colvarBirthday.IsPrimaryKey = false;
                colvarBirthday.IsForeignKey = false;
                colvarBirthday.IsReadOnly = false;
                
                schema.Columns.Add(colvarBirthday);
                
                TableSchema.TableColumn colvarGender = new TableSchema.TableColumn(schema);
                colvarGender.ColumnName = "gender";
                colvarGender.DataType = DbType.Int32;
                colvarGender.MaxLength = 0;
                colvarGender.AutoIncrement = false;
                colvarGender.IsNullable = true;
                colvarGender.IsPrimaryKey = false;
                colvarGender.IsForeignKey = false;
                colvarGender.IsReadOnly = false;
                
                schema.Columns.Add(colvarGender);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "company_name";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 50;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyName);
                
                TableSchema.TableColumn colvarCompanyDepartment = new TableSchema.TableColumn(schema);
                colvarCompanyDepartment.ColumnName = "company_department";
                colvarCompanyDepartment.DataType = DbType.String;
                colvarCompanyDepartment.MaxLength = 50;
                colvarCompanyDepartment.AutoIncrement = false;
                colvarCompanyDepartment.IsNullable = true;
                colvarCompanyDepartment.IsPrimaryKey = false;
                colvarCompanyDepartment.IsForeignKey = false;
                colvarCompanyDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyDepartment);
                
                TableSchema.TableColumn colvarCompanyTel = new TableSchema.TableColumn(schema);
                colvarCompanyTel.ColumnName = "company_tel";
                colvarCompanyTel.DataType = DbType.AnsiString;
                colvarCompanyTel.MaxLength = 20;
                colvarCompanyTel.AutoIncrement = false;
                colvarCompanyTel.IsNullable = true;
                colvarCompanyTel.IsPrimaryKey = false;
                colvarCompanyTel.IsForeignKey = false;
                colvarCompanyTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyTel);
                
                TableSchema.TableColumn colvarCompanyTelExt = new TableSchema.TableColumn(schema);
                colvarCompanyTelExt.ColumnName = "company_tel_ext";
                colvarCompanyTelExt.DataType = DbType.AnsiString;
                colvarCompanyTelExt.MaxLength = 50;
                colvarCompanyTelExt.AutoIncrement = false;
                colvarCompanyTelExt.IsNullable = true;
                colvarCompanyTelExt.IsPrimaryKey = false;
                colvarCompanyTelExt.IsForeignKey = false;
                colvarCompanyTelExt.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyTelExt);
                
                TableSchema.TableColumn colvarCompanyAddress = new TableSchema.TableColumn(schema);
                colvarCompanyAddress.ColumnName = "company_address";
                colvarCompanyAddress.DataType = DbType.String;
                colvarCompanyAddress.MaxLength = 100;
                colvarCompanyAddress.AutoIncrement = false;
                colvarCompanyAddress.IsNullable = true;
                colvarCompanyAddress.IsPrimaryKey = false;
                colvarCompanyAddress.IsForeignKey = false;
                colvarCompanyAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAddress);
                
                TableSchema.TableColumn colvarDeliveryMethod = new TableSchema.TableColumn(schema);
                colvarDeliveryMethod.ColumnName = "delivery_method";
                colvarDeliveryMethod.DataType = DbType.String;
                colvarDeliveryMethod.MaxLength = 200;
                colvarDeliveryMethod.AutoIncrement = false;
                colvarDeliveryMethod.IsNullable = true;
                colvarDeliveryMethod.IsPrimaryKey = false;
                colvarDeliveryMethod.IsForeignKey = false;
                colvarDeliveryMethod.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryMethod);
                
                TableSchema.TableColumn colvarPrimaryContactMethod = new TableSchema.TableColumn(schema);
                colvarPrimaryContactMethod.ColumnName = "primary_contact_method";
                colvarPrimaryContactMethod.DataType = DbType.Int32;
                colvarPrimaryContactMethod.MaxLength = 0;
                colvarPrimaryContactMethod.AutoIncrement = false;
                colvarPrimaryContactMethod.IsNullable = true;
                colvarPrimaryContactMethod.IsPrimaryKey = false;
                colvarPrimaryContactMethod.IsForeignKey = false;
                colvarPrimaryContactMethod.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrimaryContactMethod);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 30;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarComment = new TableSchema.TableColumn(schema);
                colvarComment.ColumnName = "comment";
                colvarComment.DataType = DbType.String;
                colvarComment.MaxLength = 100;
                colvarComment.AutoIncrement = false;
                colvarComment.IsNullable = true;
                colvarComment.IsPrimaryKey = false;
                colvarComment.IsForeignKey = false;
                colvarComment.IsReadOnly = false;
                
                schema.Columns.Add(colvarComment);
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarMemberEmail = new TableSchema.TableColumn(schema);
                colvarMemberEmail.ColumnName = "member_email";
                colvarMemberEmail.DataType = DbType.AnsiString;
                colvarMemberEmail.MaxLength = 50;
                colvarMemberEmail.AutoIncrement = false;
                colvarMemberEmail.IsNullable = false;
                colvarMemberEmail.IsPrimaryKey = false;
                colvarMemberEmail.IsForeignKey = false;
                colvarMemberEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberEmail);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarPhoneNumber = new TableSchema.TableColumn(schema);
                colvarPhoneNumber.ColumnName = "phone_number";
                colvarPhoneNumber.DataType = DbType.AnsiString;
                colvarPhoneNumber.MaxLength = 50;
                colvarPhoneNumber.AutoIncrement = false;
                colvarPhoneNumber.IsNullable = true;
                colvarPhoneNumber.IsPrimaryKey = false;
                colvarPhoneNumber.IsForeignKey = false;
                colvarPhoneNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhoneNumber);
                
                TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
                colvarMobileNumber.ColumnName = "mobile_number";
                colvarMobileNumber.DataType = DbType.AnsiString;
                colvarMobileNumber.MaxLength = 50;
                colvarMobileNumber.AutoIncrement = false;
                colvarMobileNumber.IsNullable = true;
                colvarMobileNumber.IsPrimaryKey = false;
                colvarMobileNumber.IsForeignKey = false;
                colvarMobileNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobileNumber);
                
                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryAddress);
                
                TableSchema.TableColumn colvarDeliveryTime = new TableSchema.TableColumn(schema);
                colvarDeliveryTime.ColumnName = "delivery_time";
                colvarDeliveryTime.DataType = DbType.DateTime;
                colvarDeliveryTime.MaxLength = 0;
                colvarDeliveryTime.AutoIncrement = false;
                colvarDeliveryTime.IsNullable = true;
                colvarDeliveryTime.IsPrimaryKey = false;
                colvarDeliveryTime.IsForeignKey = false;
                colvarDeliveryTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryTime);
                
                TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
                colvarSubtotal.ColumnName = "subtotal";
                colvarSubtotal.DataType = DbType.Currency;
                colvarSubtotal.MaxLength = 0;
                colvarSubtotal.AutoIncrement = false;
                colvarSubtotal.IsNullable = false;
                colvarSubtotal.IsPrimaryKey = false;
                colvarSubtotal.IsForeignKey = false;
                colvarSubtotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubtotal);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = false;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarUserMemo = new TableSchema.TableColumn(schema);
                colvarUserMemo.ColumnName = "user_memo";
                colvarUserMemo.DataType = DbType.String;
                colvarUserMemo.MaxLength = 1073741823;
                colvarUserMemo.AutoIncrement = false;
                colvarUserMemo.IsNullable = true;
                colvarUserMemo.IsPrimaryKey = false;
                colvarUserMemo.IsForeignKey = false;
                colvarUserMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserMemo);
                
                TableSchema.TableColumn colvarOrderMemo = new TableSchema.TableColumn(schema);
                colvarOrderMemo.ColumnName = "order_memo";
                colvarOrderMemo.DataType = DbType.String;
                colvarOrderMemo.MaxLength = 1073741823;
                colvarOrderMemo.AutoIncrement = false;
                colvarOrderMemo.IsNullable = true;
                colvarOrderMemo.IsPrimaryKey = false;
                colvarOrderMemo.IsForeignKey = false;
                colvarOrderMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderMemo);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 30;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarOrderCreateTime = new TableSchema.TableColumn(schema);
                colvarOrderCreateTime.ColumnName = "order_create_time";
                colvarOrderCreateTime.DataType = DbType.DateTime;
                colvarOrderCreateTime.MaxLength = 0;
                colvarOrderCreateTime.AutoIncrement = false;
                colvarOrderCreateTime.IsNullable = false;
                colvarOrderCreateTime.IsPrimaryKey = false;
                colvarOrderCreateTime.IsForeignKey = false;
                colvarOrderCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCreateTime);
                
                TableSchema.TableColumn colvarOrderModifyId = new TableSchema.TableColumn(schema);
                colvarOrderModifyId.ColumnName = "order_modify_id";
                colvarOrderModifyId.DataType = DbType.String;
                colvarOrderModifyId.MaxLength = 30;
                colvarOrderModifyId.AutoIncrement = false;
                colvarOrderModifyId.IsNullable = true;
                colvarOrderModifyId.IsPrimaryKey = false;
                colvarOrderModifyId.IsForeignKey = false;
                colvarOrderModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderModifyId);
                
                TableSchema.TableColumn colvarOrderModifyTime = new TableSchema.TableColumn(schema);
                colvarOrderModifyTime.ColumnName = "order_modify_time";
                colvarOrderModifyTime.DataType = DbType.DateTime;
                colvarOrderModifyTime.MaxLength = 0;
                colvarOrderModifyTime.AutoIncrement = false;
                colvarOrderModifyTime.IsNullable = true;
                colvarOrderModifyTime.IsPrimaryKey = false;
                colvarOrderModifyTime.IsForeignKey = false;
                colvarOrderModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderModifyTime);
                
                TableSchema.TableColumn colvarReviewScore = new TableSchema.TableColumn(schema);
                colvarReviewScore.ColumnName = "review_score";
                colvarReviewScore.DataType = DbType.Int32;
                colvarReviewScore.MaxLength = 0;
                colvarReviewScore.AutoIncrement = false;
                colvarReviewScore.IsNullable = true;
                colvarReviewScore.IsPrimaryKey = false;
                colvarReviewScore.IsForeignKey = false;
                colvarReviewScore.IsReadOnly = false;
                
                schema.Columns.Add(colvarReviewScore);
                
                TableSchema.TableColumn colvarReview = new TableSchema.TableColumn(schema);
                colvarReview.ColumnName = "review";
                colvarReview.DataType = DbType.String;
                colvarReview.MaxLength = 1073741823;
                colvarReview.AutoIncrement = false;
                colvarReview.IsNullable = true;
                colvarReview.IsPrimaryKey = false;
                colvarReview.IsForeignKey = false;
                colvarReview.IsReadOnly = false;
                
                schema.Columns.Add(colvarReview);
                
                TableSchema.TableColumn colvarReviewDate = new TableSchema.TableColumn(schema);
                colvarReviewDate.ColumnName = "review_date";
                colvarReviewDate.DataType = DbType.DateTime;
                colvarReviewDate.MaxLength = 0;
                colvarReviewDate.AutoIncrement = false;
                colvarReviewDate.IsNullable = true;
                colvarReviewDate.IsPrimaryKey = false;
                colvarReviewDate.IsForeignKey = false;
                colvarReviewDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarReviewDate);
                
                TableSchema.TableColumn colvarReviewReply = new TableSchema.TableColumn(schema);
                colvarReviewReply.ColumnName = "review_reply";
                colvarReviewReply.DataType = DbType.String;
                colvarReviewReply.MaxLength = 1073741823;
                colvarReviewReply.AutoIncrement = false;
                colvarReviewReply.IsNullable = true;
                colvarReviewReply.IsPrimaryKey = false;
                colvarReviewReply.IsForeignKey = false;
                colvarReviewReply.IsReadOnly = false;
                
                schema.Columns.Add(colvarReviewReply);
                
                TableSchema.TableColumn colvarReviewReplyDate = new TableSchema.TableColumn(schema);
                colvarReviewReplyDate.ColumnName = "review_reply_date";
                colvarReviewReplyDate.DataType = DbType.DateTime;
                colvarReviewReplyDate.MaxLength = 0;
                colvarReviewReplyDate.AutoIncrement = false;
                colvarReviewReplyDate.IsNullable = true;
                colvarReviewReplyDate.IsPrimaryKey = false;
                colvarReviewReplyDate.IsForeignKey = false;
                colvarReviewReplyDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarReviewReplyDate);
                
                TableSchema.TableColumn colvarOrderAccessLock = new TableSchema.TableColumn(schema);
                colvarOrderAccessLock.ColumnName = "order_access_lock";
                colvarOrderAccessLock.DataType = DbType.String;
                colvarOrderAccessLock.MaxLength = 50;
                colvarOrderAccessLock.AutoIncrement = false;
                colvarOrderAccessLock.IsNullable = true;
                colvarOrderAccessLock.IsPrimaryKey = false;
                colvarOrderAccessLock.IsForeignKey = false;
                colvarOrderAccessLock.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderAccessLock);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_member_city_order",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMemberCityOrder()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMemberCityOrder(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMemberCityOrder(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMemberCityOrder(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("LastName")]
        [Bindable(true)]
        public string LastName 
	    {
		    get
		    {
			    return GetColumnValue<string>("last_name");
		    }
            set 
		    {
			    SetColumnValue("last_name", value);
            }
        }
	      
        [XmlAttribute("FirstName")]
        [Bindable(true)]
        public string FirstName 
	    {
		    get
		    {
			    return GetColumnValue<string>("first_name");
		    }
            set 
		    {
			    SetColumnValue("first_name", value);
            }
        }
	      
        [XmlAttribute("BuildingGuid")]
        [Bindable(true)]
        public Guid? BuildingGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("building_GUID");
		    }
            set 
		    {
			    SetColumnValue("building_GUID", value);
            }
        }
	      
        [XmlAttribute("Birthday")]
        [Bindable(true)]
        public DateTime? Birthday 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("birthday");
		    }
            set 
		    {
			    SetColumnValue("birthday", value);
            }
        }
	      
        [XmlAttribute("Gender")]
        [Bindable(true)]
        public int? Gender 
	    {
		    get
		    {
			    return GetColumnValue<int?>("gender");
		    }
            set 
		    {
			    SetColumnValue("gender", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	      
        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_name");
		    }
            set 
		    {
			    SetColumnValue("company_name", value);
            }
        }
	      
        [XmlAttribute("CompanyDepartment")]
        [Bindable(true)]
        public string CompanyDepartment 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_department");
		    }
            set 
		    {
			    SetColumnValue("company_department", value);
            }
        }
	      
        [XmlAttribute("CompanyTel")]
        [Bindable(true)]
        public string CompanyTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_tel");
		    }
            set 
		    {
			    SetColumnValue("company_tel", value);
            }
        }
	      
        [XmlAttribute("CompanyTelExt")]
        [Bindable(true)]
        public string CompanyTelExt 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_tel_ext");
		    }
            set 
		    {
			    SetColumnValue("company_tel_ext", value);
            }
        }
	      
        [XmlAttribute("CompanyAddress")]
        [Bindable(true)]
        public string CompanyAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_address");
		    }
            set 
		    {
			    SetColumnValue("company_address", value);
            }
        }
	      
        [XmlAttribute("DeliveryMethod")]
        [Bindable(true)]
        public string DeliveryMethod 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_method");
		    }
            set 
		    {
			    SetColumnValue("delivery_method", value);
            }
        }
	      
        [XmlAttribute("PrimaryContactMethod")]
        [Bindable(true)]
        public int? PrimaryContactMethod 
	    {
		    get
		    {
			    return GetColumnValue<int?>("primary_contact_method");
		    }
            set 
		    {
			    SetColumnValue("primary_contact_method", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int? Status 
	    {
		    get
		    {
			    return GetColumnValue<int?>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("Comment")]
        [Bindable(true)]
        public string Comment 
	    {
		    get
		    {
			    return GetColumnValue<string>("comment");
		    }
            set 
		    {
			    SetColumnValue("comment", value);
            }
        }
	      
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("GUID");
		    }
            set 
		    {
			    SetColumnValue("GUID", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("MemberEmail")]
        [Bindable(true)]
        public string MemberEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_email");
		    }
            set 
		    {
			    SetColumnValue("member_email", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("PhoneNumber")]
        [Bindable(true)]
        public string PhoneNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("phone_number");
		    }
            set 
		    {
			    SetColumnValue("phone_number", value);
            }
        }
	      
        [XmlAttribute("MobileNumber")]
        [Bindable(true)]
        public string MobileNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile_number");
		    }
            set 
		    {
			    SetColumnValue("mobile_number", value);
            }
        }
	      
        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_address");
		    }
            set 
		    {
			    SetColumnValue("delivery_address", value);
            }
        }
	      
        [XmlAttribute("DeliveryTime")]
        [Bindable(true)]
        public DateTime? DeliveryTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("delivery_time");
		    }
            set 
		    {
			    SetColumnValue("delivery_time", value);
            }
        }
	      
        [XmlAttribute("Subtotal")]
        [Bindable(true)]
        public decimal Subtotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("subtotal");
		    }
            set 
		    {
			    SetColumnValue("subtotal", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal Total 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("UserMemo")]
        [Bindable(true)]
        public string UserMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_memo");
		    }
            set 
		    {
			    SetColumnValue("user_memo", value);
            }
        }
	      
        [XmlAttribute("OrderMemo")]
        [Bindable(true)]
        public string OrderMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_memo");
		    }
            set 
		    {
			    SetColumnValue("order_memo", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("OrderCreateTime")]
        [Bindable(true)]
        public DateTime OrderCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("order_create_time");
		    }
            set 
		    {
			    SetColumnValue("order_create_time", value);
            }
        }
	      
        [XmlAttribute("OrderModifyId")]
        [Bindable(true)]
        public string OrderModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_modify_id");
		    }
            set 
		    {
			    SetColumnValue("order_modify_id", value);
            }
        }
	      
        [XmlAttribute("OrderModifyTime")]
        [Bindable(true)]
        public DateTime? OrderModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("order_modify_time");
		    }
            set 
		    {
			    SetColumnValue("order_modify_time", value);
            }
        }
	      
        [XmlAttribute("ReviewScore")]
        [Bindable(true)]
        public int? ReviewScore 
	    {
		    get
		    {
			    return GetColumnValue<int?>("review_score");
		    }
            set 
		    {
			    SetColumnValue("review_score", value);
            }
        }
	      
        [XmlAttribute("Review")]
        [Bindable(true)]
        public string Review 
	    {
		    get
		    {
			    return GetColumnValue<string>("review");
		    }
            set 
		    {
			    SetColumnValue("review", value);
            }
        }
	      
        [XmlAttribute("ReviewDate")]
        [Bindable(true)]
        public DateTime? ReviewDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("review_date");
		    }
            set 
		    {
			    SetColumnValue("review_date", value);
            }
        }
	      
        [XmlAttribute("ReviewReply")]
        [Bindable(true)]
        public string ReviewReply 
	    {
		    get
		    {
			    return GetColumnValue<string>("review_reply");
		    }
            set 
		    {
			    SetColumnValue("review_reply", value);
            }
        }
	      
        [XmlAttribute("ReviewReplyDate")]
        [Bindable(true)]
        public DateTime? ReviewReplyDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("review_reply_date");
		    }
            set 
		    {
			    SetColumnValue("review_reply_date", value);
            }
        }
	      
        [XmlAttribute("OrderAccessLock")]
        [Bindable(true)]
        public string OrderAccessLock 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_access_lock");
		    }
            set 
		    {
			    SetColumnValue("order_access_lock", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CityName = @"city_name";
            
            public static string Code = @"code";
            
            public static string UserName = @"user_name";
            
            public static string LastName = @"last_name";
            
            public static string FirstName = @"first_name";
            
            public static string BuildingGuid = @"building_GUID";
            
            public static string Birthday = @"birthday";
            
            public static string Gender = @"gender";
            
            public static string Mobile = @"mobile";
            
            public static string CompanyName = @"company_name";
            
            public static string CompanyDepartment = @"company_department";
            
            public static string CompanyTel = @"company_tel";
            
            public static string CompanyTelExt = @"company_tel_ext";
            
            public static string CompanyAddress = @"company_address";
            
            public static string DeliveryMethod = @"delivery_method";
            
            public static string PrimaryContactMethod = @"primary_contact_method";
            
            public static string Status = @"status";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyTime = @"modify_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string Comment = @"comment";
            
            public static string Guid = @"GUID";
            
            public static string OrderId = @"order_id";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string SellerName = @"seller_name";
            
            public static string MemberEmail = @"member_email";
            
            public static string MemberName = @"member_name";
            
            public static string PhoneNumber = @"phone_number";
            
            public static string MobileNumber = @"mobile_number";
            
            public static string DeliveryAddress = @"delivery_address";
            
            public static string DeliveryTime = @"delivery_time";
            
            public static string Subtotal = @"subtotal";
            
            public static string Total = @"total";
            
            public static string UserMemo = @"user_memo";
            
            public static string OrderMemo = @"order_memo";
            
            public static string OrderStatus = @"order_status";
            
            public static string CreateId = @"create_id";
            
            public static string OrderCreateTime = @"order_create_time";
            
            public static string OrderModifyId = @"order_modify_id";
            
            public static string OrderModifyTime = @"order_modify_time";
            
            public static string ReviewScore = @"review_score";
            
            public static string Review = @"review";
            
            public static string ReviewDate = @"review_date";
            
            public static string ReviewReply = @"review_reply";
            
            public static string ReviewReplyDate = @"review_reply_date";
            
            public static string OrderAccessLock = @"order_access_lock";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

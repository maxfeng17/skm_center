using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmBurningCostCenter class.
	/// </summary>
    [Serializable]
	public partial class SkmBurningCostCenterCollection : RepositoryList<SkmBurningCostCenter, SkmBurningCostCenterCollection>
	{	   
		public SkmBurningCostCenterCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmBurningCostCenterCollection</returns>
		public SkmBurningCostCenterCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmBurningCostCenter o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_burning_cost_center table.
	/// </summary>
	
	[Serializable]
	public partial class SkmBurningCostCenter : RepositoryRecord<SkmBurningCostCenter>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmBurningCostCenter()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmBurningCostCenter(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_burning_cost_center", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBurningEventId = new TableSchema.TableColumn(schema);
				colvarBurningEventId.ColumnName = "burning_event_id";
				colvarBurningEventId.DataType = DbType.Int32;
				colvarBurningEventId.MaxLength = 0;
				colvarBurningEventId.AutoIncrement = false;
				colvarBurningEventId.IsNullable = false;
				colvarBurningEventId.IsPrimaryKey = false;
				colvarBurningEventId.IsForeignKey = false;
				colvarBurningEventId.IsReadOnly = false;
				colvarBurningEventId.DefaultSetting = @"";
				colvarBurningEventId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBurningEventId);
				
				TableSchema.TableColumn colvarStoreCode = new TableSchema.TableColumn(schema);
				colvarStoreCode.ColumnName = "store_code";
				colvarStoreCode.DataType = DbType.AnsiString;
				colvarStoreCode.MaxLength = 4;
				colvarStoreCode.AutoIncrement = false;
				colvarStoreCode.IsNullable = false;
				colvarStoreCode.IsPrimaryKey = false;
				colvarStoreCode.IsForeignKey = false;
				colvarStoreCode.IsReadOnly = false;
				colvarStoreCode.DefaultSetting = @"";
				colvarStoreCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreCode);
				
				TableSchema.TableColumn colvarCostCenterCode = new TableSchema.TableColumn(schema);
				colvarCostCenterCode.ColumnName = "cost_center_code";
				colvarCostCenterCode.DataType = DbType.AnsiString;
				colvarCostCenterCode.MaxLength = 20;
				colvarCostCenterCode.AutoIncrement = false;
				colvarCostCenterCode.IsNullable = false;
				colvarCostCenterCode.IsPrimaryKey = false;
				colvarCostCenterCode.IsForeignKey = false;
				colvarCostCenterCode.IsReadOnly = false;
				colvarCostCenterCode.DefaultSetting = @"";
				colvarCostCenterCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostCenterCode);
				
				TableSchema.TableColumn colvarOrderCode = new TableSchema.TableColumn(schema);
				colvarOrderCode.ColumnName = "order_code";
				colvarOrderCode.DataType = DbType.AnsiString;
				colvarOrderCode.MaxLength = 20;
				colvarOrderCode.AutoIncrement = false;
				colvarOrderCode.IsNullable = false;
				colvarOrderCode.IsPrimaryKey = false;
				colvarOrderCode.IsForeignKey = false;
				colvarOrderCode.IsReadOnly = false;
				colvarOrderCode.DefaultSetting = @"";
				colvarOrderCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderCode);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_burning_cost_center",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("BurningEventId")]
		[Bindable(true)]
		public int BurningEventId 
		{
			get { return GetColumnValue<int>(Columns.BurningEventId); }
			set { SetColumnValue(Columns.BurningEventId, value); }
		}
		
		[XmlAttribute("StoreCode")]
		[Bindable(true)]
		public string StoreCode 
		{
			get { return GetColumnValue<string>(Columns.StoreCode); }
			set { SetColumnValue(Columns.StoreCode, value); }
		}
		
		[XmlAttribute("CostCenterCode")]
		[Bindable(true)]
		public string CostCenterCode 
		{
			get { return GetColumnValue<string>(Columns.CostCenterCode); }
			set { SetColumnValue(Columns.CostCenterCode, value); }
		}
		
		[XmlAttribute("OrderCode")]
		[Bindable(true)]
		public string OrderCode 
		{
			get { return GetColumnValue<string>(Columns.OrderCode); }
			set { SetColumnValue(Columns.OrderCode, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BurningEventIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreCodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CostCenterCodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderCodeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BurningEventId = @"burning_event_id";
			 public static string StoreCode = @"store_code";
			 public static string CostCenterCode = @"cost_center_code";
			 public static string OrderCode = @"order_code";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

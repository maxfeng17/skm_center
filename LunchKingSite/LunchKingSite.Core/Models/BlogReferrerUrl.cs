﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the BlogReferrerUrl class.
    /// </summary>
    [Serializable]
    public partial class BlogReferrerUrlCollection : RepositoryList<BlogReferrerUrl, BlogReferrerUrlCollection>
    {
        public BlogReferrerUrlCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BlogReferrerUrlCollection</returns>
        public BlogReferrerUrlCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BlogReferrerUrl o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the blog_referrer_url table.
    /// </summary>
    [Serializable]
    public partial class BlogReferrerUrl : RepositoryRecord<BlogReferrerUrl>, IRecordBase
    {
        #region .ctors and Default Settings

        public BlogReferrerUrl()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public BlogReferrerUrl(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("blog_referrer_url", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarOriUrl = new TableSchema.TableColumn(schema);
                colvarOriUrl.ColumnName = "ori_url";
                colvarOriUrl.DataType = DbType.AnsiString;
                colvarOriUrl.MaxLength = 500;
                colvarOriUrl.AutoIncrement = false;
                colvarOriUrl.IsNullable = false;
                colvarOriUrl.IsPrimaryKey = false;
                colvarOriUrl.IsForeignKey = false;
                colvarOriUrl.IsReadOnly = false;
                colvarOriUrl.DefaultSetting = @"";
                colvarOriUrl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOriUrl);

                TableSchema.TableColumn colvarShortUrl = new TableSchema.TableColumn(schema);
                colvarShortUrl.ColumnName = "short_url";
                colvarShortUrl.DataType = DbType.AnsiString;
                colvarShortUrl.MaxLength = 500;
                colvarShortUrl.AutoIncrement = false;
                colvarShortUrl.IsNullable = false;
                colvarShortUrl.IsPrimaryKey = false;
                colvarShortUrl.IsForeignKey = false;
                colvarShortUrl.IsReadOnly = false;
                colvarShortUrl.DefaultSetting = @"";
                colvarShortUrl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShortUrl);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;

                colvarType.DefaultSetting = @"((0))";
                colvarType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarBlogId = new TableSchema.TableColumn(schema);
                colvarBlogId.ColumnName = "blog_id";
                colvarBlogId.DataType = DbType.Guid;
                colvarBlogId.MaxLength = 0;
                colvarBlogId.AutoIncrement = false;
                colvarBlogId.IsNullable = false;
                colvarBlogId.IsPrimaryKey = false;
                colvarBlogId.IsForeignKey = false;
                colvarBlogId.IsReadOnly = false;
                colvarBlogId.DefaultSetting = @"";
                colvarBlogId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBlogId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("blog_referrer_url", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("OriUrl")]
        [Bindable(true)]
        public string OriUrl
        {
            get { return GetColumnValue<string>(Columns.OriUrl); }
            set { SetColumnValue(Columns.OriUrl, value); }
        }

        [XmlAttribute("ShortUrl")]
        [Bindable(true)]
        public string ShortUrl
        {
            get { return GetColumnValue<string>(Columns.ShortUrl); }
            set { SetColumnValue(Columns.ShortUrl, value); }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get { return GetColumnValue<int>(Columns.Type); }
            set { SetColumnValue(Columns.Type, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("BlogId")]
        [Bindable(true)]
        public Guid BlogId
        {
            get { return GetColumnValue<Guid>(Columns.BlogId); }
            set { SetColumnValue(Columns.BlogId, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn OriUrlColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ShortUrlColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn BlogIdColumn
        {
            get { return Schema.Columns[6]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string OriUrl = @"ori_url";
            public static string ShortUrl = @"short_url";
            public static string Type = @"type";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string BlogId = @"blog_id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

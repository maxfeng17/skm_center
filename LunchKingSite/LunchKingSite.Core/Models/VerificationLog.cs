using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the VerificationLog class.
    /// </summary>
    [Serializable]
    public partial class VerificationLogCollection : RepositoryList<VerificationLog, VerificationLogCollection>
    {
        public VerificationLogCollection() {}

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VerificationLogCollection</returns>
        public VerificationLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VerificationLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the verification_log table.
    /// </summary>
    [Serializable]
    public partial class VerificationLog : RepositoryRecord<VerificationLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public VerificationLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public VerificationLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("verification_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Guid;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = true;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                colvarSellerGuid.DefaultSetting = @"";
                colvarSellerGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                colvarCouponId.DefaultSetting = @"";
                colvarCouponId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCouponId);

                TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
                colvarSequenceNumber.ColumnName = "sequence_number";
                colvarSequenceNumber.DataType = DbType.AnsiString;
                colvarSequenceNumber.MaxLength = 50;
                colvarSequenceNumber.AutoIncrement = false;
                colvarSequenceNumber.IsNullable = true;
                colvarSequenceNumber.IsPrimaryKey = false;
                colvarSequenceNumber.IsForeignKey = false;
                colvarSequenceNumber.IsReadOnly = false;
                colvarSequenceNumber.DefaultSetting = @"";
                colvarSequenceNumber.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSequenceNumber);

                TableSchema.TableColumn colvarCouponCode = new TableSchema.TableColumn(schema);
                colvarCouponCode.ColumnName = "coupon_code";
                colvarCouponCode.DataType = DbType.AnsiString;
                colvarCouponCode.MaxLength = 50;
                colvarCouponCode.AutoIncrement = false;
                colvarCouponCode.IsNullable = true;
                colvarCouponCode.IsPrimaryKey = false;
                colvarCouponCode.IsForeignKey = false;
                colvarCouponCode.IsReadOnly = false;
                colvarCouponCode.DefaultSetting = @"";
                colvarCouponCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCouponCode);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = 50;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = true;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                colvarDescription.DefaultSetting = @"";
                colvarDescription.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDescription);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarIp = new TableSchema.TableColumn(schema);
                colvarIp.ColumnName = "ip";
                colvarIp.DataType = DbType.String;
                colvarIp.MaxLength = 50;
                colvarIp.AutoIncrement = false;
                colvarIp.IsNullable = true;
                colvarIp.IsPrimaryKey = false;
                colvarIp.IsForeignKey = false;
                colvarIp.IsReadOnly = false;
                colvarIp.DefaultSetting = @"";
                colvarIp.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIp);

                TableSchema.TableColumn colvarUndoFlag = new TableSchema.TableColumn(schema);
                colvarUndoFlag.ColumnName = "undo_flag";
                colvarUndoFlag.DataType = DbType.Int32;
                colvarUndoFlag.MaxLength = 0;
                colvarUndoFlag.AutoIncrement = false;
                colvarUndoFlag.IsNullable = false;
                colvarUndoFlag.IsPrimaryKey = false;
                colvarUndoFlag.IsForeignKey = false;
                colvarUndoFlag.IsReadOnly = false;

                colvarUndoFlag.DefaultSetting = @"((0))";
                colvarUndoFlag.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUndoFlag);

                TableSchema.TableColumn colvarUndoTime = new TableSchema.TableColumn(schema);
                colvarUndoTime.ColumnName = "undo_time";
                colvarUndoTime.DataType = DbType.DateTime;
                colvarUndoTime.MaxLength = 0;
                colvarUndoTime.AutoIncrement = false;
                colvarUndoTime.IsNullable = true;
                colvarUndoTime.IsPrimaryKey = false;
                colvarUndoTime.IsForeignKey = false;
                colvarUndoTime.IsReadOnly = false;
                colvarUndoTime.DefaultSetting = @"";
                colvarUndoTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUndoTime);

                TableSchema.TableColumn colvarIsHideal = new TableSchema.TableColumn(schema);
                colvarIsHideal.ColumnName = "is_hideal";
                colvarIsHideal.DataType = DbType.Boolean;
                colvarIsHideal.MaxLength = 0;
                colvarIsHideal.AutoIncrement = false;
                colvarIsHideal.IsNullable = false;
                colvarIsHideal.IsPrimaryKey = false;
                colvarIsHideal.IsForeignKey = false;
                colvarIsHideal.IsReadOnly = false;

                colvarIsHideal.DefaultSetting = @"((0))";
                colvarIsHideal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsHideal);

                TableSchema.TableColumn colvarDeviceType = new TableSchema.TableColumn(schema);
                colvarDeviceType.ColumnName = "device_type";
                colvarDeviceType.DataType = DbType.Int32;
                colvarDeviceType.MaxLength = 0;
                colvarDeviceType.AutoIncrement = false;
                colvarDeviceType.IsNullable = true;
                colvarDeviceType.IsPrimaryKey = false;
                colvarDeviceType.IsForeignKey = false;
                colvarDeviceType.IsReadOnly = false;
                colvarDeviceType.DefaultSetting = @"";
                colvarDeviceType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDeviceType);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("verification_log",schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public Guid Id
        {
            get { return GetColumnValue<Guid>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid? SellerGuid
        {
            get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
            set { SetColumnValue(Columns.SellerGuid, value); }
        }

        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId
        {
            get { return GetColumnValue<int?>(Columns.CouponId); }
            set { SetColumnValue(Columns.CouponId, value); }
        }

        [XmlAttribute("SequenceNumber")]
        [Bindable(true)]
        public string SequenceNumber
        {
            get { return GetColumnValue<string>(Columns.SequenceNumber); }
            set { SetColumnValue(Columns.SequenceNumber, value); }
        }

        [XmlAttribute("CouponCode")]
        [Bindable(true)]
        public string CouponCode
        {
            get { return GetColumnValue<string>(Columns.CouponCode); }
            set { SetColumnValue(Columns.CouponCode, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int? Status
        {
            get { return GetColumnValue<int?>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description
        {
            get { return GetColumnValue<string>(Columns.Description); }
            set { SetColumnValue(Columns.Description, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Ip")]
        [Bindable(true)]
        public string Ip
        {
            get { return GetColumnValue<string>(Columns.Ip); }
            set { SetColumnValue(Columns.Ip, value); }
        }

        [XmlAttribute("UndoFlag")]
        [Bindable(true)]
        public int UndoFlag
        {
            get { return GetColumnValue<int>(Columns.UndoFlag); }
            set { SetColumnValue(Columns.UndoFlag, value); }
        }

        [XmlAttribute("UndoTime")]
        [Bindable(true)]
        public DateTime? UndoTime
        {
            get { return GetColumnValue<DateTime?>(Columns.UndoTime); }
            set { SetColumnValue(Columns.UndoTime, value); }
        }

        [XmlAttribute("IsHideal")]
        [Bindable(true)]
        public bool IsHideal
        {
            get { return GetColumnValue<bool>(Columns.IsHideal); }
            set { SetColumnValue(Columns.IsHideal, value); }
        }

        [XmlAttribute("DeviceType")]
        [Bindable(true)]
        public int? DeviceType
        {
            get { return GetColumnValue<int?>(Columns.DeviceType); }
            set { SetColumnValue(Columns.DeviceType, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn SequenceNumberColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CouponCodeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn IpColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn UndoFlagColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn UndoTimeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn IsHidealColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn DeviceTypeColumn
        {
            get { return Schema.Columns[12]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string SellerGuid = @"seller_guid";
            public static string CouponId = @"coupon_id";
            public static string SequenceNumber = @"sequence_number";
            public static string CouponCode = @"coupon_code";
            public static string Status = @"status";
            public static string Description = @"description";
            public static string CreateTime = @"create_time";
            public static string Ip = @"ip";
            public static string UndoFlag = @"undo_flag";
            public static string UndoTime = @"undo_time";
            public static string IsHideal = @"is_hideal";
            public static string DeviceType = @"device_type";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

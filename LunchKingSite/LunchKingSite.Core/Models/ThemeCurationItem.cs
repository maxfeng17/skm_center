using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ThemeCurationItem class.
	/// </summary>
    [Serializable]
	public partial class ThemeCurationItemCollection : RepositoryList<ThemeCurationItem, ThemeCurationItemCollection>
	{	   
		public ThemeCurationItemCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ThemeCurationItemCollection</returns>
		public ThemeCurationItemCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ThemeCurationItem o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the theme_curation_item table.
	/// </summary>
	[Serializable]
	public partial class ThemeCurationItem : RepositoryRecord<ThemeCurationItem>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ThemeCurationItem()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ThemeCurationItem(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("theme_curation_item", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
				colvarGroupId.ColumnName = "group_id";
				colvarGroupId.DataType = DbType.Int32;
				colvarGroupId.MaxLength = 0;
				colvarGroupId.AutoIncrement = false;
				colvarGroupId.IsNullable = false;
				colvarGroupId.IsPrimaryKey = false;
				colvarGroupId.IsForeignKey = false;
				colvarGroupId.IsReadOnly = false;
				colvarGroupId.DefaultSetting = @"";
				colvarGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupId);
				
				TableSchema.TableColumn colvarCurationType = new TableSchema.TableColumn(schema);
				colvarCurationType.ColumnName = "curation_type";
				colvarCurationType.DataType = DbType.Int32;
				colvarCurationType.MaxLength = 0;
				colvarCurationType.AutoIncrement = false;
				colvarCurationType.IsNullable = false;
				colvarCurationType.IsPrimaryKey = false;
				colvarCurationType.IsForeignKey = false;
				colvarCurationType.IsReadOnly = false;
				colvarCurationType.DefaultSetting = @"";
				colvarCurationType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCurationType);
				
				TableSchema.TableColumn colvarCurationId = new TableSchema.TableColumn(schema);
				colvarCurationId.ColumnName = "curation_id";
				colvarCurationId.DataType = DbType.Int32;
				colvarCurationId.MaxLength = 0;
				colvarCurationId.AutoIncrement = false;
				colvarCurationId.IsNullable = false;
				colvarCurationId.IsPrimaryKey = false;
				colvarCurationId.IsForeignKey = false;
				colvarCurationId.IsReadOnly = false;
				colvarCurationId.DefaultSetting = @"";
				colvarCurationId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCurationId);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = true;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				colvarSeq.DefaultSetting = @"";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarModifyUser = new TableSchema.TableColumn(schema);
				colvarModifyUser.ColumnName = "modify_user";
				colvarModifyUser.DataType = DbType.AnsiString;
				colvarModifyUser.MaxLength = 50;
				colvarModifyUser.AutoIncrement = false;
				colvarModifyUser.IsNullable = true;
				colvarModifyUser.IsPrimaryKey = false;
				colvarModifyUser.IsForeignKey = false;
				colvarModifyUser.IsReadOnly = false;
				colvarModifyUser.DefaultSetting = @"";
				colvarModifyUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyUser);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("theme_curation_item",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("GroupId")]
		[Bindable(true)]
		public int GroupId 
		{
			get { return GetColumnValue<int>(Columns.GroupId); }
			set { SetColumnValue(Columns.GroupId, value); }
		}
		  
		[XmlAttribute("CurationType")]
		[Bindable(true)]
		public int CurationType 
		{
			get { return GetColumnValue<int>(Columns.CurationType); }
			set { SetColumnValue(Columns.CurationType, value); }
		}
		  
		[XmlAttribute("CurationId")]
		[Bindable(true)]
		public int CurationId 
		{
			get { return GetColumnValue<int>(Columns.CurationId); }
			set { SetColumnValue(Columns.CurationId, value); }
		}
		  
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int? Seq 
		{
			get { return GetColumnValue<int?>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ModifyUser")]
		[Bindable(true)]
		public string ModifyUser 
		{
			get { return GetColumnValue<string>(Columns.ModifyUser); }
			set { SetColumnValue(Columns.ModifyUser, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GroupIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CurationTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CurationIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyUserColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string GroupId = @"group_id";
			 public static string CurationType = @"curation_type";
			 public static string CurationId = @"curation_id";
			 public static string Seq = @"seq";
			 public static string CreateTime = @"create_time";
			 public static string ModifyTime = @"modify_time";
			 public static string ModifyUser = @"modify_user";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

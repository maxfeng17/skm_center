using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewBuildingSellerDeliveryHour class.
    /// </summary>
    [Serializable]
    public partial class ViewBuildingSellerDeliveryHourCollection : ReadOnlyList<ViewBuildingSellerDeliveryHour, ViewBuildingSellerDeliveryHourCollection>
    {        
        public ViewBuildingSellerDeliveryHourCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_building_seller_delivery_hour view.
    /// </summary>
    [Serializable]
    public partial class ViewBuildingSellerDeliveryHour : ReadOnlyRecord<ViewBuildingSellerDeliveryHour>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_building_seller_delivery_hour", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBuildingId = new TableSchema.TableColumn(schema);
                colvarBuildingId.ColumnName = "building_id";
                colvarBuildingId.DataType = DbType.AnsiString;
                colvarBuildingId.MaxLength = 20;
                colvarBuildingId.AutoIncrement = false;
                colvarBuildingId.IsNullable = true;
                colvarBuildingId.IsPrimaryKey = false;
                colvarBuildingId.IsForeignKey = false;
                colvarBuildingId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingId);
                
                TableSchema.TableColumn colvarBuildingName = new TableSchema.TableColumn(schema);
                colvarBuildingName.ColumnName = "building_name";
                colvarBuildingName.DataType = DbType.String;
                colvarBuildingName.MaxLength = 50;
                colvarBuildingName.AutoIncrement = false;
                colvarBuildingName.IsNullable = false;
                colvarBuildingName.IsPrimaryKey = false;
                colvarBuildingName.IsForeignKey = false;
                colvarBuildingName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingName);
                
                TableSchema.TableColumn colvarBuildingStreetName = new TableSchema.TableColumn(schema);
                colvarBuildingStreetName.ColumnName = "building_street_name";
                colvarBuildingStreetName.DataType = DbType.String;
                colvarBuildingStreetName.MaxLength = 100;
                colvarBuildingStreetName.AutoIncrement = false;
                colvarBuildingStreetName.IsNullable = false;
                colvarBuildingStreetName.IsPrimaryKey = false;
                colvarBuildingStreetName.IsForeignKey = false;
                colvarBuildingStreetName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingStreetName);
                
                TableSchema.TableColumn colvarBuildingAddressNumber = new TableSchema.TableColumn(schema);
                colvarBuildingAddressNumber.ColumnName = "building_address_number";
                colvarBuildingAddressNumber.DataType = DbType.String;
                colvarBuildingAddressNumber.MaxLength = 100;
                colvarBuildingAddressNumber.AutoIncrement = false;
                colvarBuildingAddressNumber.IsNullable = true;
                colvarBuildingAddressNumber.IsPrimaryKey = false;
                colvarBuildingAddressNumber.IsForeignKey = false;
                colvarBuildingAddressNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingAddressNumber);
                
                TableSchema.TableColumn colvarBuildingOnline = new TableSchema.TableColumn(schema);
                colvarBuildingOnline.ColumnName = "building_online";
                colvarBuildingOnline.DataType = DbType.Boolean;
                colvarBuildingOnline.MaxLength = 0;
                colvarBuildingOnline.AutoIncrement = false;
                colvarBuildingOnline.IsNullable = false;
                colvarBuildingOnline.IsPrimaryKey = false;
                colvarBuildingOnline.IsForeignKey = false;
                colvarBuildingOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingOnline);
                
                TableSchema.TableColumn colvarBuildingRank = new TableSchema.TableColumn(schema);
                colvarBuildingRank.ColumnName = "building_rank";
                colvarBuildingRank.DataType = DbType.Int32;
                colvarBuildingRank.MaxLength = 0;
                colvarBuildingRank.AutoIncrement = false;
                colvarBuildingRank.IsNullable = false;
                colvarBuildingRank.IsPrimaryKey = false;
                colvarBuildingRank.IsForeignKey = false;
                colvarBuildingRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingRank);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerBossName = new TableSchema.TableColumn(schema);
                colvarSellerBossName.ColumnName = "seller_boss_name";
                colvarSellerBossName.DataType = DbType.String;
                colvarSellerBossName.MaxLength = 30;
                colvarSellerBossName.AutoIncrement = false;
                colvarSellerBossName.IsNullable = true;
                colvarSellerBossName.IsPrimaryKey = false;
                colvarSellerBossName.IsForeignKey = false;
                colvarSellerBossName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerBossName);
                
                TableSchema.TableColumn colvarSellerTel = new TableSchema.TableColumn(schema);
                colvarSellerTel.ColumnName = "seller_tel";
                colvarSellerTel.DataType = DbType.AnsiString;
                colvarSellerTel.MaxLength = 20;
                colvarSellerTel.AutoIncrement = false;
                colvarSellerTel.IsNullable = true;
                colvarSellerTel.IsPrimaryKey = false;
                colvarSellerTel.IsForeignKey = false;
                colvarSellerTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerTel);
                
                TableSchema.TableColumn colvarSellerTel2 = new TableSchema.TableColumn(schema);
                colvarSellerTel2.ColumnName = "seller_tel2";
                colvarSellerTel2.DataType = DbType.AnsiString;
                colvarSellerTel2.MaxLength = 20;
                colvarSellerTel2.AutoIncrement = false;
                colvarSellerTel2.IsNullable = true;
                colvarSellerTel2.IsPrimaryKey = false;
                colvarSellerTel2.IsForeignKey = false;
                colvarSellerTel2.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerTel2);
                
                TableSchema.TableColumn colvarSellerFax = new TableSchema.TableColumn(schema);
                colvarSellerFax.ColumnName = "seller_fax";
                colvarSellerFax.DataType = DbType.AnsiString;
                colvarSellerFax.MaxLength = 20;
                colvarSellerFax.AutoIncrement = false;
                colvarSellerFax.IsNullable = true;
                colvarSellerFax.IsPrimaryKey = false;
                colvarSellerFax.IsForeignKey = false;
                colvarSellerFax.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerFax);
                
                TableSchema.TableColumn colvarSellerMobile = new TableSchema.TableColumn(schema);
                colvarSellerMobile.ColumnName = "seller_mobile";
                colvarSellerMobile.DataType = DbType.AnsiString;
                colvarSellerMobile.MaxLength = 20;
                colvarSellerMobile.AutoIncrement = false;
                colvarSellerMobile.IsNullable = true;
                colvarSellerMobile.IsPrimaryKey = false;
                colvarSellerMobile.IsForeignKey = false;
                colvarSellerMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMobile);
                
                TableSchema.TableColumn colvarSellerAddress = new TableSchema.TableColumn(schema);
                colvarSellerAddress.ColumnName = "seller_address";
                colvarSellerAddress.DataType = DbType.String;
                colvarSellerAddress.MaxLength = 100;
                colvarSellerAddress.AutoIncrement = false;
                colvarSellerAddress.IsNullable = true;
                colvarSellerAddress.IsPrimaryKey = false;
                colvarSellerAddress.IsForeignKey = false;
                colvarSellerAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerAddress);
                
                TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
                colvarSellerEmail.ColumnName = "seller_email";
                colvarSellerEmail.DataType = DbType.AnsiString;
                colvarSellerEmail.MaxLength = 50;
                colvarSellerEmail.AutoIncrement = false;
                colvarSellerEmail.IsNullable = true;
                colvarSellerEmail.IsPrimaryKey = false;
                colvarSellerEmail.IsForeignKey = false;
                colvarSellerEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerEmail);
                
                TableSchema.TableColumn colvarSellerBlog = new TableSchema.TableColumn(schema);
                colvarSellerBlog.ColumnName = "seller_blog";
                colvarSellerBlog.DataType = DbType.AnsiString;
                colvarSellerBlog.MaxLength = 100;
                colvarSellerBlog.AutoIncrement = false;
                colvarSellerBlog.IsNullable = true;
                colvarSellerBlog.IsPrimaryKey = false;
                colvarSellerBlog.IsForeignKey = false;
                colvarSellerBlog.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerBlog);
                
                TableSchema.TableColumn colvarSellerInvoice = new TableSchema.TableColumn(schema);
                colvarSellerInvoice.ColumnName = "seller_invoice";
                colvarSellerInvoice.DataType = DbType.String;
                colvarSellerInvoice.MaxLength = 50;
                colvarSellerInvoice.AutoIncrement = false;
                colvarSellerInvoice.IsNullable = true;
                colvarSellerInvoice.IsPrimaryKey = false;
                colvarSellerInvoice.IsForeignKey = false;
                colvarSellerInvoice.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerInvoice);
                
                TableSchema.TableColumn colvarSellerDescription = new TableSchema.TableColumn(schema);
                colvarSellerDescription.ColumnName = "seller_description";
                colvarSellerDescription.DataType = DbType.String;
                colvarSellerDescription.MaxLength = 1073741823;
                colvarSellerDescription.AutoIncrement = false;
                colvarSellerDescription.IsNullable = true;
                colvarSellerDescription.IsPrimaryKey = false;
                colvarSellerDescription.IsForeignKey = false;
                colvarSellerDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerDescription);
                
                TableSchema.TableColumn colvarSellerStatus = new TableSchema.TableColumn(schema);
                colvarSellerStatus.ColumnName = "seller_status";
                colvarSellerStatus.DataType = DbType.Int32;
                colvarSellerStatus.MaxLength = 0;
                colvarSellerStatus.AutoIncrement = false;
                colvarSellerStatus.IsNullable = false;
                colvarSellerStatus.IsPrimaryKey = false;
                colvarSellerStatus.IsForeignKey = false;
                colvarSellerStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerStatus);
                
                TableSchema.TableColumn colvarSellerRemark = new TableSchema.TableColumn(schema);
                colvarSellerRemark.ColumnName = "seller_remark";
                colvarSellerRemark.DataType = DbType.String;
                colvarSellerRemark.MaxLength = 200;
                colvarSellerRemark.AutoIncrement = false;
                colvarSellerRemark.IsNullable = true;
                colvarSellerRemark.IsPrimaryKey = false;
                colvarSellerRemark.IsForeignKey = false;
                colvarSellerRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerRemark);
                
                TableSchema.TableColumn colvarSellerSales = new TableSchema.TableColumn(schema);
                colvarSellerSales.ColumnName = "seller_sales";
                colvarSellerSales.DataType = DbType.String;
                colvarSellerSales.MaxLength = 50;
                colvarSellerSales.AutoIncrement = false;
                colvarSellerSales.IsNullable = true;
                colvarSellerSales.IsPrimaryKey = false;
                colvarSellerSales.IsForeignKey = false;
                colvarSellerSales.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerSales);
                
                TableSchema.TableColumn colvarSellerLogoimgPath = new TableSchema.TableColumn(schema);
                colvarSellerLogoimgPath.ColumnName = "seller_logoimg_path";
                colvarSellerLogoimgPath.DataType = DbType.AnsiString;
                colvarSellerLogoimgPath.MaxLength = 500;
                colvarSellerLogoimgPath.AutoIncrement = false;
                colvarSellerLogoimgPath.IsNullable = true;
                colvarSellerLogoimgPath.IsPrimaryKey = false;
                colvarSellerLogoimgPath.IsForeignKey = false;
                colvarSellerLogoimgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerLogoimgPath);
                
                TableSchema.TableColumn colvarSellerVideoPath = new TableSchema.TableColumn(schema);
                colvarSellerVideoPath.ColumnName = "seller_video_path";
                colvarSellerVideoPath.DataType = DbType.AnsiString;
                colvarSellerVideoPath.MaxLength = 100;
                colvarSellerVideoPath.AutoIncrement = false;
                colvarSellerVideoPath.IsNullable = true;
                colvarSellerVideoPath.IsPrimaryKey = false;
                colvarSellerVideoPath.IsForeignKey = false;
                colvarSellerVideoPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerVideoPath);
                
                TableSchema.TableColumn colvarBusinessHourTypeId = new TableSchema.TableColumn(schema);
                colvarBusinessHourTypeId.ColumnName = "business_hour_type_id";
                colvarBusinessHourTypeId.DataType = DbType.Int32;
                colvarBusinessHourTypeId.MaxLength = 0;
                colvarBusinessHourTypeId.AutoIncrement = false;
                colvarBusinessHourTypeId.IsNullable = false;
                colvarBusinessHourTypeId.IsPrimaryKey = false;
                colvarBusinessHourTypeId.IsForeignKey = false;
                colvarBusinessHourTypeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourTypeId);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
                colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeS.MaxLength = 0;
                colvarBusinessHourDeliverTimeS.AutoIncrement = false;
                colvarBusinessHourDeliverTimeS.IsNullable = true;
                colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeS.IsForeignKey = false;
                colvarBusinessHourDeliverTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeS);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliveryCharge = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliveryCharge.ColumnName = "business_hour_delivery_charge";
                colvarBusinessHourDeliveryCharge.DataType = DbType.Currency;
                colvarBusinessHourDeliveryCharge.MaxLength = 0;
                colvarBusinessHourDeliveryCharge.AutoIncrement = false;
                colvarBusinessHourDeliveryCharge.IsNullable = false;
                colvarBusinessHourDeliveryCharge.IsPrimaryKey = false;
                colvarBusinessHourDeliveryCharge.IsForeignKey = false;
                colvarBusinessHourDeliveryCharge.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliveryCharge);
                
                TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
                colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
                colvarBusinessHourOrderMinimum.MaxLength = 0;
                colvarBusinessHourOrderMinimum.AutoIncrement = false;
                colvarBusinessHourOrderMinimum.IsNullable = false;
                colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
                colvarBusinessHourOrderMinimum.IsForeignKey = false;
                colvarBusinessHourOrderMinimum.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderMinimum);
                
                TableSchema.TableColumn colvarBusinessHourPreparationTime = new TableSchema.TableColumn(schema);
                colvarBusinessHourPreparationTime.ColumnName = "business_hour_preparation_time";
                colvarBusinessHourPreparationTime.DataType = DbType.Int32;
                colvarBusinessHourPreparationTime.MaxLength = 0;
                colvarBusinessHourPreparationTime.AutoIncrement = false;
                colvarBusinessHourPreparationTime.IsNullable = false;
                colvarBusinessHourPreparationTime.IsPrimaryKey = false;
                colvarBusinessHourPreparationTime.IsForeignKey = false;
                colvarBusinessHourPreparationTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourPreparationTime);
                
                TableSchema.TableColumn colvarBusinessHourOnline = new TableSchema.TableColumn(schema);
                colvarBusinessHourOnline.ColumnName = "business_hour_online";
                colvarBusinessHourOnline.DataType = DbType.Boolean;
                colvarBusinessHourOnline.MaxLength = 0;
                colvarBusinessHourOnline.AutoIncrement = false;
                colvarBusinessHourOnline.IsNullable = false;
                colvarBusinessHourOnline.IsPrimaryKey = false;
                colvarBusinessHourOnline.IsForeignKey = false;
                colvarBusinessHourOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOnline);
                
                TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
                colvarOrderTotalLimit.ColumnName = "order_total_limit";
                colvarOrderTotalLimit.DataType = DbType.Currency;
                colvarOrderTotalLimit.MaxLength = 0;
                colvarOrderTotalLimit.AutoIncrement = false;
                colvarOrderTotalLimit.IsNullable = true;
                colvarOrderTotalLimit.IsPrimaryKey = false;
                colvarOrderTotalLimit.IsForeignKey = false;
                colvarOrderTotalLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTotalLimit);
                
                TableSchema.TableColumn colvarDeliveryLimit = new TableSchema.TableColumn(schema);
                colvarDeliveryLimit.ColumnName = "delivery_limit";
                colvarDeliveryLimit.DataType = DbType.Int32;
                colvarDeliveryLimit.MaxLength = 0;
                colvarDeliveryLimit.AutoIncrement = false;
                colvarDeliveryLimit.IsNullable = true;
                colvarDeliveryLimit.IsPrimaryKey = false;
                colvarDeliveryLimit.IsForeignKey = false;
                colvarDeliveryLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryLimit);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarHoliday = new TableSchema.TableColumn(schema);
                colvarHoliday.ColumnName = "holiday";
                colvarHoliday.DataType = DbType.Int32;
                colvarHoliday.MaxLength = 0;
                colvarHoliday.AutoIncrement = false;
                colvarHoliday.IsNullable = false;
                colvarHoliday.IsPrimaryKey = false;
                colvarHoliday.IsForeignKey = false;
                colvarHoliday.IsReadOnly = false;
                
                schema.Columns.Add(colvarHoliday);
                
                TableSchema.TableColumn colvarBuildingGuid = new TableSchema.TableColumn(schema);
                colvarBuildingGuid.ColumnName = "building_GUID";
                colvarBuildingGuid.DataType = DbType.Guid;
                colvarBuildingGuid.MaxLength = 0;
                colvarBuildingGuid.AutoIncrement = false;
                colvarBuildingGuid.IsNullable = false;
                colvarBuildingGuid.IsPrimaryKey = false;
                colvarBuildingGuid.IsForeignKey = false;
                colvarBuildingGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingGuid);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_GUID";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarBuildingDeliveryId = new TableSchema.TableColumn(schema);
                colvarBuildingDeliveryId.ColumnName = "building_delivery_id";
                colvarBuildingDeliveryId.DataType = DbType.Int32;
                colvarBuildingDeliveryId.MaxLength = 0;
                colvarBuildingDeliveryId.AutoIncrement = false;
                colvarBuildingDeliveryId.IsNullable = false;
                colvarBuildingDeliveryId.IsPrimaryKey = false;
                colvarBuildingDeliveryId.IsForeignKey = false;
                colvarBuildingDeliveryId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingDeliveryId);
                
                TableSchema.TableColumn colvarDeparment = new TableSchema.TableColumn(schema);
                colvarDeparment.ColumnName = "deparment";
                colvarDeparment.DataType = DbType.Int32;
                colvarDeparment.MaxLength = 0;
                colvarDeparment.AutoIncrement = false;
                colvarDeparment.IsNullable = false;
                colvarDeparment.IsPrimaryKey = false;
                colvarDeparment.IsForeignKey = false;
                colvarDeparment.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeparment);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarSellerCityId = new TableSchema.TableColumn(schema);
                colvarSellerCityId.ColumnName = "seller_city_id";
                colvarSellerCityId.DataType = DbType.Int32;
                colvarSellerCityId.MaxLength = 0;
                colvarSellerCityId.AutoIncrement = false;
                colvarSellerCityId.IsNullable = false;
                colvarSellerCityId.IsPrimaryKey = false;
                colvarSellerCityId.IsForeignKey = false;
                colvarSellerCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCityId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_building_seller_delivery_hour",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBuildingSellerDeliveryHour()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBuildingSellerDeliveryHour(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBuildingSellerDeliveryHour(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBuildingSellerDeliveryHour(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BuildingId")]
        [Bindable(true)]
        public string BuildingId 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_id");
		    }
            set 
		    {
			    SetColumnValue("building_id", value);
            }
        }
	      
        [XmlAttribute("BuildingName")]
        [Bindable(true)]
        public string BuildingName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_name");
		    }
            set 
		    {
			    SetColumnValue("building_name", value);
            }
        }
	      
        [XmlAttribute("BuildingStreetName")]
        [Bindable(true)]
        public string BuildingStreetName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_street_name");
		    }
            set 
		    {
			    SetColumnValue("building_street_name", value);
            }
        }
	      
        [XmlAttribute("BuildingAddressNumber")]
        [Bindable(true)]
        public string BuildingAddressNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_address_number");
		    }
            set 
		    {
			    SetColumnValue("building_address_number", value);
            }
        }
	      
        [XmlAttribute("BuildingOnline")]
        [Bindable(true)]
        public bool BuildingOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool>("building_online");
		    }
            set 
		    {
			    SetColumnValue("building_online", value);
            }
        }
	      
        [XmlAttribute("BuildingRank")]
        [Bindable(true)]
        public int BuildingRank 
	    {
		    get
		    {
			    return GetColumnValue<int>("building_rank");
		    }
            set 
		    {
			    SetColumnValue("building_rank", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerBossName")]
        [Bindable(true)]
        public string SellerBossName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_boss_name");
		    }
            set 
		    {
			    SetColumnValue("seller_boss_name", value);
            }
        }
	      
        [XmlAttribute("SellerTel")]
        [Bindable(true)]
        public string SellerTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_tel");
		    }
            set 
		    {
			    SetColumnValue("seller_tel", value);
            }
        }
	      
        [XmlAttribute("SellerTel2")]
        [Bindable(true)]
        public string SellerTel2 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_tel2");
		    }
            set 
		    {
			    SetColumnValue("seller_tel2", value);
            }
        }
	      
        [XmlAttribute("SellerFax")]
        [Bindable(true)]
        public string SellerFax 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_fax");
		    }
            set 
		    {
			    SetColumnValue("seller_fax", value);
            }
        }
	      
        [XmlAttribute("SellerMobile")]
        [Bindable(true)]
        public string SellerMobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_mobile");
		    }
            set 
		    {
			    SetColumnValue("seller_mobile", value);
            }
        }
	      
        [XmlAttribute("SellerAddress")]
        [Bindable(true)]
        public string SellerAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_address");
		    }
            set 
		    {
			    SetColumnValue("seller_address", value);
            }
        }
	      
        [XmlAttribute("SellerEmail")]
        [Bindable(true)]
        public string SellerEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_email");
		    }
            set 
		    {
			    SetColumnValue("seller_email", value);
            }
        }
	      
        [XmlAttribute("SellerBlog")]
        [Bindable(true)]
        public string SellerBlog 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_blog");
		    }
            set 
		    {
			    SetColumnValue("seller_blog", value);
            }
        }
	      
        [XmlAttribute("SellerInvoice")]
        [Bindable(true)]
        public string SellerInvoice 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_invoice");
		    }
            set 
		    {
			    SetColumnValue("seller_invoice", value);
            }
        }
	      
        [XmlAttribute("SellerDescription")]
        [Bindable(true)]
        public string SellerDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_description");
		    }
            set 
		    {
			    SetColumnValue("seller_description", value);
            }
        }
	      
        [XmlAttribute("SellerStatus")]
        [Bindable(true)]
        public int SellerStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_status");
		    }
            set 
		    {
			    SetColumnValue("seller_status", value);
            }
        }
	      
        [XmlAttribute("SellerRemark")]
        [Bindable(true)]
        public string SellerRemark 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_remark");
		    }
            set 
		    {
			    SetColumnValue("seller_remark", value);
            }
        }
	      
        [XmlAttribute("SellerSales")]
        [Bindable(true)]
        public string SellerSales 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_sales");
		    }
            set 
		    {
			    SetColumnValue("seller_sales", value);
            }
        }
	      
        [XmlAttribute("SellerLogoimgPath")]
        [Bindable(true)]
        public string SellerLogoimgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_logoimg_path");
		    }
            set 
		    {
			    SetColumnValue("seller_logoimg_path", value);
            }
        }
	      
        [XmlAttribute("SellerVideoPath")]
        [Bindable(true)]
        public string SellerVideoPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_video_path");
		    }
            set 
		    {
			    SetColumnValue("seller_video_path", value);
            }
        }
	      
        [XmlAttribute("BusinessHourTypeId")]
        [Bindable(true)]
        public int BusinessHourTypeId 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_type_id");
		    }
            set 
		    {
			    SetColumnValue("business_hour_type_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliveryCharge")]
        [Bindable(true)]
        public decimal BusinessHourDeliveryCharge 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_delivery_charge");
		    }
            set 
		    {
			    SetColumnValue("business_hour_delivery_charge", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderMinimum")]
        [Bindable(true)]
        public decimal BusinessHourOrderMinimum 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_order_minimum");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_minimum", value);
            }
        }
	      
        [XmlAttribute("BusinessHourPreparationTime")]
        [Bindable(true)]
        public int BusinessHourPreparationTime 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_preparation_time");
		    }
            set 
		    {
			    SetColumnValue("business_hour_preparation_time", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOnline")]
        [Bindable(true)]
        public bool BusinessHourOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool>("business_hour_online");
		    }
            set 
		    {
			    SetColumnValue("business_hour_online", value);
            }
        }
	      
        [XmlAttribute("OrderTotalLimit")]
        [Bindable(true)]
        public decimal? OrderTotalLimit 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("order_total_limit");
		    }
            set 
		    {
			    SetColumnValue("order_total_limit", value);
            }
        }
	      
        [XmlAttribute("DeliveryLimit")]
        [Bindable(true)]
        public int? DeliveryLimit 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_limit");
		    }
            set 
		    {
			    SetColumnValue("delivery_limit", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("Holiday")]
        [Bindable(true)]
        public int Holiday 
	    {
		    get
		    {
			    return GetColumnValue<int>("holiday");
		    }
            set 
		    {
			    SetColumnValue("holiday", value);
            }
        }
	      
        [XmlAttribute("BuildingGuid")]
        [Bindable(true)]
        public Guid BuildingGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("building_GUID");
		    }
            set 
		    {
			    SetColumnValue("building_GUID", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_GUID");
		    }
            set 
		    {
			    SetColumnValue("business_hour_GUID", value);
            }
        }
	      
        [XmlAttribute("BuildingDeliveryId")]
        [Bindable(true)]
        public int BuildingDeliveryId 
	    {
		    get
		    {
			    return GetColumnValue<int>("building_delivery_id");
		    }
            set 
		    {
			    SetColumnValue("building_delivery_id", value);
            }
        }
	      
        [XmlAttribute("Deparment")]
        [Bindable(true)]
        public int Deparment 
	    {
		    get
		    {
			    return GetColumnValue<int>("deparment");
		    }
            set 
		    {
			    SetColumnValue("deparment", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("SellerCityId")]
        [Bindable(true)]
        public int SellerCityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_city_id");
		    }
            set 
		    {
			    SetColumnValue("seller_city_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BuildingId = @"building_id";
            
            public static string BuildingName = @"building_name";
            
            public static string BuildingStreetName = @"building_street_name";
            
            public static string BuildingAddressNumber = @"building_address_number";
            
            public static string BuildingOnline = @"building_online";
            
            public static string BuildingRank = @"building_rank";
            
            public static string SellerId = @"seller_id";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerBossName = @"seller_boss_name";
            
            public static string SellerTel = @"seller_tel";
            
            public static string SellerTel2 = @"seller_tel2";
            
            public static string SellerFax = @"seller_fax";
            
            public static string SellerMobile = @"seller_mobile";
            
            public static string SellerAddress = @"seller_address";
            
            public static string SellerEmail = @"seller_email";
            
            public static string SellerBlog = @"seller_blog";
            
            public static string SellerInvoice = @"seller_invoice";
            
            public static string SellerDescription = @"seller_description";
            
            public static string SellerStatus = @"seller_status";
            
            public static string SellerRemark = @"seller_remark";
            
            public static string SellerSales = @"seller_sales";
            
            public static string SellerLogoimgPath = @"seller_logoimg_path";
            
            public static string SellerVideoPath = @"seller_video_path";
            
            public static string BusinessHourTypeId = @"business_hour_type_id";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
            public static string BusinessHourDeliveryCharge = @"business_hour_delivery_charge";
            
            public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
            
            public static string BusinessHourPreparationTime = @"business_hour_preparation_time";
            
            public static string BusinessHourOnline = @"business_hour_online";
            
            public static string OrderTotalLimit = @"order_total_limit";
            
            public static string DeliveryLimit = @"delivery_limit";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string Holiday = @"holiday";
            
            public static string BuildingGuid = @"building_GUID";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string BusinessHourGuid = @"business_hour_GUID";
            
            public static string BuildingDeliveryId = @"building_delivery_id";
            
            public static string Deparment = @"deparment";
            
            public static string CityId = @"city_id";
            
            public static string SellerCityId = @"seller_city_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

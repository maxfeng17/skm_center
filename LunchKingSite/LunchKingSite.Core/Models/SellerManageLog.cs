using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SellerManageLog class.
	/// </summary>
    [Serializable]
	public partial class SellerManageLogCollection : RepositoryList<SellerManageLog, SellerManageLogCollection>
	{	   
		public SellerManageLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SellerManageLogCollection</returns>
		public SellerManageLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SellerManageLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the seller_manage_log table.
	/// </summary>
	[Serializable]
	public partial class SellerManageLog : RepositoryRecord<SellerManageLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SellerManageLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SellerManageLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("seller_manage_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarDevelopStatus = new TableSchema.TableColumn(schema);
				colvarDevelopStatus.ColumnName = "develop_status";
				colvarDevelopStatus.DataType = DbType.Int16;
				colvarDevelopStatus.MaxLength = 0;
				colvarDevelopStatus.AutoIncrement = false;
				colvarDevelopStatus.IsNullable = false;
				colvarDevelopStatus.IsPrimaryKey = false;
				colvarDevelopStatus.IsForeignKey = false;
				colvarDevelopStatus.IsReadOnly = false;
				colvarDevelopStatus.DefaultSetting = @"";
				colvarDevelopStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDevelopStatus);
				
				TableSchema.TableColumn colvarSellerGrade = new TableSchema.TableColumn(schema);
				colvarSellerGrade.ColumnName = "seller_grade";
				colvarSellerGrade.DataType = DbType.Int16;
				colvarSellerGrade.MaxLength = 0;
				colvarSellerGrade.AutoIncrement = false;
				colvarSellerGrade.IsNullable = false;
				colvarSellerGrade.IsPrimaryKey = false;
				colvarSellerGrade.IsForeignKey = false;
				colvarSellerGrade.IsReadOnly = false;
				colvarSellerGrade.DefaultSetting = @"";
				colvarSellerGrade.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGrade);
				
				TableSchema.TableColumn colvarChangeLog = new TableSchema.TableColumn(schema);
				colvarChangeLog.ColumnName = "change_log";
				colvarChangeLog.DataType = DbType.String;
				colvarChangeLog.MaxLength = 2000;
				colvarChangeLog.AutoIncrement = false;
				colvarChangeLog.IsNullable = false;
				colvarChangeLog.IsPrimaryKey = false;
				colvarChangeLog.IsForeignKey = false;
				colvarChangeLog.IsReadOnly = false;
				colvarChangeLog.DefaultSetting = @"";
				colvarChangeLog.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChangeLog);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 100;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("seller_manage_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("DevelopStatus")]
		[Bindable(true)]
		public short DevelopStatus 
		{
			get { return GetColumnValue<short>(Columns.DevelopStatus); }
			set { SetColumnValue(Columns.DevelopStatus, value); }
		}
		  
		[XmlAttribute("SellerGrade")]
		[Bindable(true)]
		public short SellerGrade 
		{
			get { return GetColumnValue<short>(Columns.SellerGrade); }
			set { SetColumnValue(Columns.SellerGrade, value); }
		}
		  
		[XmlAttribute("ChangeLog")]
		[Bindable(true)]
		public string ChangeLog 
		{
			get { return GetColumnValue<string>(Columns.ChangeLog); }
			set { SetColumnValue(Columns.ChangeLog, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DevelopStatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGradeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ChangeLogColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SellerGuid = @"seller_guid";
			 public static string DevelopStatus = @"develop_status";
			 public static string SellerGrade = @"seller_grade";
			 public static string ChangeLog = @"change_log";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the TrustRecord class.
    /// </summary>
    [Serializable]
    public partial class TrustRecordCollection : RepositoryList<TrustRecord, TrustRecordCollection>
    {
        public TrustRecordCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TrustRecordCollection</returns>
        public TrustRecordCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TrustRecord o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the trust_record table.
    /// </summary>
    [Serializable]
    public partial class TrustRecord : RepositoryRecord<TrustRecord>, IRecordBase
    {
        #region .ctors and Default Settings

        public TrustRecord()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public TrustRecord(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("trust_record", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Int32;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                colvarAmount.DefaultSetting = @"";
                colvarAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarBankStatus = new TableSchema.TableColumn(schema);
                colvarBankStatus.ColumnName = "bank_status";
                colvarBankStatus.DataType = DbType.Byte;
                colvarBankStatus.MaxLength = 0;
                colvarBankStatus.AutoIncrement = false;
                colvarBankStatus.IsNullable = false;
                colvarBankStatus.IsPrimaryKey = false;
                colvarBankStatus.IsForeignKey = false;
                colvarBankStatus.IsReadOnly = false;
                colvarBankStatus.DefaultSetting = @"";
                colvarBankStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBankStatus);

                TableSchema.TableColumn colvarTrustedTime = new TableSchema.TableColumn(schema);
                colvarTrustedTime.ColumnName = "trusted_time";
                colvarTrustedTime.DataType = DbType.DateTime;
                colvarTrustedTime.MaxLength = 0;
                colvarTrustedTime.AutoIncrement = false;
                colvarTrustedTime.IsNullable = true;
                colvarTrustedTime.IsPrimaryKey = false;
                colvarTrustedTime.IsForeignKey = false;
                colvarTrustedTime.IsReadOnly = false;
                colvarTrustedTime.DefaultSetting = @"";
                colvarTrustedTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustedTime);

                TableSchema.TableColumn colvarTrustedBankTime = new TableSchema.TableColumn(schema);
                colvarTrustedBankTime.ColumnName = "trusted_bank_time";
                colvarTrustedBankTime.DataType = DbType.DateTime;
                colvarTrustedBankTime.MaxLength = 0;
                colvarTrustedBankTime.AutoIncrement = false;
                colvarTrustedBankTime.IsNullable = true;
                colvarTrustedBankTime.IsPrimaryKey = false;
                colvarTrustedBankTime.IsForeignKey = false;
                colvarTrustedBankTime.IsReadOnly = false;
                colvarTrustedBankTime.DefaultSetting = @"";
                colvarTrustedBankTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustedBankTime);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 100;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = false;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                colvarMessage.DefaultSetting = @"";
                colvarMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarTrustProvider = new TableSchema.TableColumn(schema);
                colvarTrustProvider.ColumnName = "trust_provider";
                colvarTrustProvider.DataType = DbType.Byte;
                colvarTrustProvider.MaxLength = 0;
                colvarTrustProvider.AutoIncrement = false;
                colvarTrustProvider.IsNullable = false;
                colvarTrustProvider.IsPrimaryKey = false;
                colvarTrustProvider.IsForeignKey = false;
                colvarTrustProvider.IsReadOnly = false;
                colvarTrustProvider.DefaultSetting = @"";
                colvarTrustProvider.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustProvider);

                TableSchema.TableColumn colvarTrustedReportGuid = new TableSchema.TableColumn(schema);
                colvarTrustedReportGuid.ColumnName = "trusted_report_guid";
                colvarTrustedReportGuid.DataType = DbType.Guid;
                colvarTrustedReportGuid.MaxLength = 0;
                colvarTrustedReportGuid.AutoIncrement = false;
                colvarTrustedReportGuid.IsNullable = true;
                colvarTrustedReportGuid.IsPrimaryKey = false;
                colvarTrustedReportGuid.IsForeignKey = false;
                colvarTrustedReportGuid.IsReadOnly = false;
                colvarTrustedReportGuid.DefaultSetting = @"";
                colvarTrustedReportGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustedReportGuid);

                TableSchema.TableColumn colvarSourceId = new TableSchema.TableColumn(schema);
                colvarSourceId.ColumnName = "source_id";
                colvarSourceId.DataType = DbType.Int32;
                colvarSourceId.MaxLength = 0;
                colvarSourceId.AutoIncrement = false;
                colvarSourceId.IsNullable = false;
                colvarSourceId.IsPrimaryKey = false;
                colvarSourceId.IsForeignKey = false;
                colvarSourceId.IsReadOnly = false;
                colvarSourceId.DefaultSetting = @"";
                colvarSourceId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSourceId);

                TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
                colvarOrderClassification.ColumnName = "order_classification";
                colvarOrderClassification.DataType = DbType.Byte;
                colvarOrderClassification.MaxLength = 0;
                colvarOrderClassification.AutoIncrement = false;
                colvarOrderClassification.IsNullable = false;
                colvarOrderClassification.IsPrimaryKey = false;
                colvarOrderClassification.IsForeignKey = false;
                colvarOrderClassification.IsReadOnly = false;
                colvarOrderClassification.DefaultSetting = @"";
                colvarOrderClassification.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderClassification);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("trust_record", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get { return GetColumnValue<int>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public int Amount
        {
            get { return GetColumnValue<int>(Columns.Amount); }
            set { SetColumnValue(Columns.Amount, value); }
        }

        [XmlAttribute("BankStatus")]
        [Bindable(true)]
        public byte BankStatus
        {
            get { return GetColumnValue<byte>(Columns.BankStatus); }
            set { SetColumnValue(Columns.BankStatus, value); }
        }

        [XmlAttribute("TrustedTime")]
        [Bindable(true)]
        public DateTime? TrustedTime
        {
            get { return GetColumnValue<DateTime?>(Columns.TrustedTime); }
            set { SetColumnValue(Columns.TrustedTime, value); }
        }

        [XmlAttribute("TrustedBankTime")]
        [Bindable(true)]
        public DateTime? TrustedBankTime
        {
            get { return GetColumnValue<DateTime?>(Columns.TrustedBankTime); }
            set { SetColumnValue(Columns.TrustedBankTime, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get { return GetColumnValue<string>(Columns.Message); }
            set { SetColumnValue(Columns.Message, value); }
        }

        [XmlAttribute("TrustProvider")]
        [Bindable(true)]
        public byte TrustProvider
        {
            get { return GetColumnValue<byte>(Columns.TrustProvider); }
            set { SetColumnValue(Columns.TrustProvider, value); }
        }

        [XmlAttribute("TrustedReportGuid")]
        [Bindable(true)]
        public Guid? TrustedReportGuid
        {
            get { return GetColumnValue<Guid?>(Columns.TrustedReportGuid); }
            set { SetColumnValue(Columns.TrustedReportGuid, value); }
        }

        [XmlAttribute("SourceId")]
        [Bindable(true)]
        public int SourceId
        {
            get { return GetColumnValue<int>(Columns.SourceId); }
            set { SetColumnValue(Columns.SourceId, value); }
        }

        [XmlAttribute("OrderClassification")]
        [Bindable(true)]
        public byte OrderClassification
        {
            get { return GetColumnValue<byte>(Columns.OrderClassification); }
            set { SetColumnValue(Columns.OrderClassification, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn BankStatusColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn TrustedTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn TrustedBankTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn TrustProviderColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn TrustedReportGuidColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn SourceIdColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn OrderClassificationColumn
        {
            get { return Schema.Columns[11]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string UserId = @"user_id";
            public static string Amount = @"amount";
            public static string BankStatus = @"bank_status";
            public static string TrustedTime = @"trusted_time";
            public static string TrustedBankTime = @"trusted_bank_time";
            public static string CreateTime = @"create_time";
            public static string Message = @"message";
            public static string TrustProvider = @"trust_provider";
            public static string TrustedReportGuid = @"trusted_report_guid";
            public static string SourceId = @"source_id";
            public static string OrderClassification = @"order_classification";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

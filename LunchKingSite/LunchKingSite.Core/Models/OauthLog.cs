using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OauthLog class.
	/// </summary>
    [Serializable]
	public partial class OauthLogCollection : RepositoryList<OauthLog, OauthLogCollection>
	{	   
		public OauthLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OauthLogCollection</returns>
		public OauthLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OauthLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the oauth_log table.
	/// </summary>
	[Serializable]
	public partial class OauthLog : RepositoryRecord<OauthLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OauthLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OauthLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("oauth_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarController = new TableSchema.TableColumn(schema);
				colvarController.ColumnName = "controller";
				colvarController.DataType = DbType.String;
				colvarController.MaxLength = 100;
				colvarController.AutoIncrement = false;
				colvarController.IsNullable = true;
				colvarController.IsPrimaryKey = false;
				colvarController.IsForeignKey = false;
				colvarController.IsReadOnly = false;
				colvarController.DefaultSetting = @"";
				colvarController.ForeignKeyTableName = "";
				schema.Columns.Add(colvarController);
				
				TableSchema.TableColumn colvarAction = new TableSchema.TableColumn(schema);
				colvarAction.ColumnName = "action";
				colvarAction.DataType = DbType.String;
				colvarAction.MaxLength = 100;
				colvarAction.AutoIncrement = false;
				colvarAction.IsNullable = true;
				colvarAction.IsPrimaryKey = false;
				colvarAction.IsForeignKey = false;
				colvarAction.IsReadOnly = false;
				colvarAction.DefaultSetting = @"";
				colvarAction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAction);
				
				TableSchema.TableColumn colvarParamsX = new TableSchema.TableColumn(schema);
				colvarParamsX.ColumnName = "params";
				colvarParamsX.DataType = DbType.String;
				colvarParamsX.MaxLength = 400;
				colvarParamsX.AutoIncrement = false;
				colvarParamsX.IsNullable = true;
				colvarParamsX.IsPrimaryKey = false;
				colvarParamsX.IsForeignKey = false;
				colvarParamsX.IsReadOnly = false;
				colvarParamsX.DefaultSetting = @"";
				colvarParamsX.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParamsX);
				
				TableSchema.TableColumn colvarLogData = new TableSchema.TableColumn(schema);
				colvarLogData.ColumnName = "log_data";
				colvarLogData.DataType = DbType.String;
				colvarLogData.MaxLength = -1;
				colvarLogData.AutoIncrement = false;
				colvarLogData.IsNullable = true;
				colvarLogData.IsPrimaryKey = false;
				colvarLogData.IsForeignKey = false;
				colvarLogData.IsReadOnly = false;
				colvarLogData.DefaultSetting = @"";
				colvarLogData.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogData);
				
				TableSchema.TableColumn colvarClientIp = new TableSchema.TableColumn(schema);
				colvarClientIp.ColumnName = "client_ip";
				colvarClientIp.DataType = DbType.String;
				colvarClientIp.MaxLength = 40;
				colvarClientIp.AutoIncrement = false;
				colvarClientIp.IsNullable = true;
				colvarClientIp.IsPrimaryKey = false;
				colvarClientIp.IsForeignKey = false;
				colvarClientIp.IsReadOnly = false;
				colvarClientIp.DefaultSetting = @"";
				colvarClientIp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarClientIp);
				
				TableSchema.TableColumn colvarIsSuccess = new TableSchema.TableColumn(schema);
				colvarIsSuccess.ColumnName = "is_success";
				colvarIsSuccess.DataType = DbType.Boolean;
				colvarIsSuccess.MaxLength = 0;
				colvarIsSuccess.AutoIncrement = false;
				colvarIsSuccess.IsNullable = false;
				colvarIsSuccess.IsPrimaryKey = false;
				colvarIsSuccess.IsForeignKey = false;
				colvarIsSuccess.IsReadOnly = false;
				colvarIsSuccess.DefaultSetting = @"";
				colvarIsSuccess.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSuccess);
				
				TableSchema.TableColumn colvarRequestTime = new TableSchema.TableColumn(schema);
				colvarRequestTime.ColumnName = "request_time";
				colvarRequestTime.DataType = DbType.DateTime;
				colvarRequestTime.MaxLength = 0;
				colvarRequestTime.AutoIncrement = false;
				colvarRequestTime.IsNullable = false;
				colvarRequestTime.IsPrimaryKey = false;
				colvarRequestTime.IsForeignKey = false;
				colvarRequestTime.IsReadOnly = false;
				colvarRequestTime.DefaultSetting = @"";
				colvarRequestTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRequestTime);
				
				TableSchema.TableColumn colvarResponseTime = new TableSchema.TableColumn(schema);
				colvarResponseTime.ColumnName = "response_time";
				colvarResponseTime.DataType = DbType.DateTime;
				colvarResponseTime.MaxLength = 0;
				colvarResponseTime.AutoIncrement = false;
				colvarResponseTime.IsNullable = true;
				colvarResponseTime.IsPrimaryKey = false;
				colvarResponseTime.IsForeignKey = false;
				colvarResponseTime.IsReadOnly = false;
				colvarResponseTime.DefaultSetting = @"";
				colvarResponseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResponseTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("oauth_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Controller")]
		[Bindable(true)]
		public string Controller 
		{
			get { return GetColumnValue<string>(Columns.Controller); }
			set { SetColumnValue(Columns.Controller, value); }
		}
		  
		[XmlAttribute("Action")]
		[Bindable(true)]
		public string Action 
		{
			get { return GetColumnValue<string>(Columns.Action); }
			set { SetColumnValue(Columns.Action, value); }
		}
		  
		[XmlAttribute("ParamsX")]
		[Bindable(true)]
		public string ParamsX 
		{
			get { return GetColumnValue<string>(Columns.ParamsX); }
			set { SetColumnValue(Columns.ParamsX, value); }
		}
		  
		[XmlAttribute("LogData")]
		[Bindable(true)]
		public string LogData 
		{
			get { return GetColumnValue<string>(Columns.LogData); }
			set { SetColumnValue(Columns.LogData, value); }
		}
		  
		[XmlAttribute("ClientIp")]
		[Bindable(true)]
		public string ClientIp 
		{
			get { return GetColumnValue<string>(Columns.ClientIp); }
			set { SetColumnValue(Columns.ClientIp, value); }
		}
		  
		[XmlAttribute("IsSuccess")]
		[Bindable(true)]
		public bool IsSuccess 
		{
			get { return GetColumnValue<bool>(Columns.IsSuccess); }
			set { SetColumnValue(Columns.IsSuccess, value); }
		}
		  
		[XmlAttribute("RequestTime")]
		[Bindable(true)]
		public DateTime RequestTime 
		{
			get { return GetColumnValue<DateTime>(Columns.RequestTime); }
			set { SetColumnValue(Columns.RequestTime, value); }
		}
		  
		[XmlAttribute("ResponseTime")]
		[Bindable(true)]
		public DateTime? ResponseTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ResponseTime); }
			set { SetColumnValue(Columns.ResponseTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ControllerColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ParamsXColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn LogDataColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ClientIpColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IsSuccessColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn RequestTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ResponseTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Controller = @"controller";
			 public static string Action = @"action";
			 public static string ParamsX = @"params";
			 public static string LogData = @"log_data";
			 public static string ClientIp = @"client_ip";
			 public static string IsSuccess = @"is_success";
			 public static string RequestTime = @"request_time";
			 public static string ResponseTime = @"response_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CmsRandomContent class.
	/// </summary>
    [Serializable]
	public partial class CmsRandomContentCollection : RepositoryList<CmsRandomContent, CmsRandomContentCollection>
	{	   
		public CmsRandomContentCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CmsRandomContentCollection</returns>
		public CmsRandomContentCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CmsRandomContent o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the cms_random_content table.
	/// </summary>
	[Serializable]
	public partial class CmsRandomContent : RepositoryRecord<CmsRandomContent>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CmsRandomContent()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CmsRandomContent(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("cms_random_content", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 500;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarContentTitle = new TableSchema.TableColumn(schema);
				colvarContentTitle.ColumnName = "content_title";
				colvarContentTitle.DataType = DbType.String;
				colvarContentTitle.MaxLength = 200;
				colvarContentTitle.AutoIncrement = false;
				colvarContentTitle.IsNullable = false;
				colvarContentTitle.IsPrimaryKey = false;
				colvarContentTitle.IsForeignKey = false;
				colvarContentTitle.IsReadOnly = false;
				colvarContentTitle.DefaultSetting = @"";
				colvarContentTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContentTitle);
				
				TableSchema.TableColumn colvarContentName = new TableSchema.TableColumn(schema);
				colvarContentName.ColumnName = "content_name";
				colvarContentName.DataType = DbType.String;
				colvarContentName.MaxLength = 250;
				colvarContentName.AutoIncrement = false;
				colvarContentName.IsNullable = false;
				colvarContentName.IsPrimaryKey = false;
				colvarContentName.IsForeignKey = false;
				colvarContentName.IsReadOnly = false;
				colvarContentName.DefaultSetting = @"";
				colvarContentName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContentName);
				
				TableSchema.TableColumn colvarBody = new TableSchema.TableColumn(schema);
				colvarBody.ColumnName = "body";
				colvarBody.DataType = DbType.String;
				colvarBody.MaxLength = -1;
				colvarBody.AutoIncrement = false;
				colvarBody.IsNullable = true;
				colvarBody.IsPrimaryKey = false;
				colvarBody.IsForeignKey = false;
				colvarBody.IsReadOnly = false;
				colvarBody.DefaultSetting = @"";
				colvarBody.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBody);
				
				TableSchema.TableColumn colvarLocale = new TableSchema.TableColumn(schema);
				colvarLocale.ColumnName = "locale";
				colvarLocale.DataType = DbType.String;
				colvarLocale.MaxLength = 50;
				colvarLocale.AutoIncrement = false;
				colvarLocale.IsNullable = false;
				colvarLocale.IsPrimaryKey = false;
				colvarLocale.IsForeignKey = false;
				colvarLocale.IsReadOnly = false;
				
						colvarLocale.DefaultSetting = @"(N'zh_TW')";
				colvarLocale.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLocale);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Boolean;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((1))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreatedOn = new TableSchema.TableColumn(schema);
				colvarCreatedOn.ColumnName = "created_on";
				colvarCreatedOn.DataType = DbType.DateTime;
				colvarCreatedOn.MaxLength = 0;
				colvarCreatedOn.AutoIncrement = false;
				colvarCreatedOn.IsNullable = true;
				colvarCreatedOn.IsPrimaryKey = false;
				colvarCreatedOn.IsForeignKey = false;
				colvarCreatedOn.IsReadOnly = false;
				colvarCreatedOn.DefaultSetting = @"";
				colvarCreatedOn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedOn);
				
				TableSchema.TableColumn colvarCreatedBy = new TableSchema.TableColumn(schema);
				colvarCreatedBy.ColumnName = "created_by";
				colvarCreatedBy.DataType = DbType.String;
				colvarCreatedBy.MaxLength = 50;
				colvarCreatedBy.AutoIncrement = false;
				colvarCreatedBy.IsNullable = true;
				colvarCreatedBy.IsPrimaryKey = false;
				colvarCreatedBy.IsForeignKey = false;
				colvarCreatedBy.IsReadOnly = false;
				colvarCreatedBy.DefaultSetting = @"";
				colvarCreatedBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedBy);
				
				TableSchema.TableColumn colvarModifiedOn = new TableSchema.TableColumn(schema);
				colvarModifiedOn.ColumnName = "modified_on";
				colvarModifiedOn.DataType = DbType.DateTime;
				colvarModifiedOn.MaxLength = 0;
				colvarModifiedOn.AutoIncrement = false;
				colvarModifiedOn.IsNullable = true;
				colvarModifiedOn.IsPrimaryKey = false;
				colvarModifiedOn.IsForeignKey = false;
				colvarModifiedOn.IsReadOnly = false;
				colvarModifiedOn.DefaultSetting = @"";
				colvarModifiedOn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedOn);
				
				TableSchema.TableColumn colvarModifiedBy = new TableSchema.TableColumn(schema);
				colvarModifiedBy.ColumnName = "modified_by";
				colvarModifiedBy.DataType = DbType.String;
				colvarModifiedBy.MaxLength = 50;
				colvarModifiedBy.AutoIncrement = false;
				colvarModifiedBy.IsNullable = true;
				colvarModifiedBy.IsPrimaryKey = false;
				colvarModifiedBy.IsForeignKey = false;
				colvarModifiedBy.IsReadOnly = false;
				colvarModifiedBy.DefaultSetting = @"";
				colvarModifiedBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedBy);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarEventPromoId = new TableSchema.TableColumn(schema);
				colvarEventPromoId.ColumnName = "event_promo_id";
				colvarEventPromoId.DataType = DbType.Int32;
				colvarEventPromoId.MaxLength = 0;
				colvarEventPromoId.AutoIncrement = false;
				colvarEventPromoId.IsNullable = true;
				colvarEventPromoId.IsPrimaryKey = false;
				colvarEventPromoId.IsForeignKey = false;
				colvarEventPromoId.IsReadOnly = false;
				colvarEventPromoId.DefaultSetting = @"";
				colvarEventPromoId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventPromoId);

                TableSchema.TableColumn colvarBrandId = new TableSchema.TableColumn(schema);
                colvarBrandId.ColumnName = "brand_id";
                colvarBrandId.DataType = DbType.Int32;
                colvarBrandId.MaxLength = 0;
                colvarBrandId.AutoIncrement = false;
                colvarBrandId.IsNullable = true;
                colvarBrandId.IsPrimaryKey = false;
                colvarBrandId.IsForeignKey = false;
                colvarBrandId.IsReadOnly = false;
                colvarBrandId.DefaultSetting = @"";
                colvarBrandId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBrandId);

                BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("cms_random_content",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		
		[XmlAttribute("ContentTitle")]
		[Bindable(true)]
		public string ContentTitle 
		{
			get { return GetColumnValue<string>(Columns.ContentTitle); }
			set { SetColumnValue(Columns.ContentTitle, value); }
		}
		
		[XmlAttribute("ContentName")]
		[Bindable(true)]
		public string ContentName 
		{
			get { return GetColumnValue<string>(Columns.ContentName); }
			set { SetColumnValue(Columns.ContentName, value); }
		}
		
		[XmlAttribute("Body")]
		[Bindable(true)]
		public string Body 
		{
			get { return GetColumnValue<string>(Columns.Body); }
			set { SetColumnValue(Columns.Body, value); }
		}
		
		[XmlAttribute("Locale")]
		[Bindable(true)]
		public string Locale 
		{
			get { return GetColumnValue<string>(Columns.Locale); }
			set { SetColumnValue(Columns.Locale, value); }
		}
		
		[XmlAttribute("Status")]
		[Bindable(true)]
		public bool Status 
		{
			get { return GetColumnValue<bool>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		
		[XmlAttribute("CreatedOn")]
		[Bindable(true)]
		public DateTime? CreatedOn 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreatedOn); }
			set { SetColumnValue(Columns.CreatedOn, value); }
		}
		
		[XmlAttribute("CreatedBy")]
		[Bindable(true)]
		public string CreatedBy 
		{
			get { return GetColumnValue<string>(Columns.CreatedBy); }
			set { SetColumnValue(Columns.CreatedBy, value); }
		}
		
		[XmlAttribute("ModifiedOn")]
		[Bindable(true)]
		public DateTime? ModifiedOn 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifiedOn); }
			set { SetColumnValue(Columns.ModifiedOn, value); }
		}
		
		[XmlAttribute("ModifiedBy")]
		[Bindable(true)]
		public string ModifiedBy 
		{
			get { return GetColumnValue<string>(Columns.ModifiedBy); }
			set { SetColumnValue(Columns.ModifiedBy, value); }
		}
		
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		
		[XmlAttribute("EventPromoId")]
		[Bindable(true)]
		public int? EventPromoId 
		{
			get { return GetColumnValue<int?>(Columns.EventPromoId); }
			set { SetColumnValue(Columns.EventPromoId, value); }
        }

        [XmlAttribute("BrandId")]
        [Bindable(true)]
        public int? BrandId
        {
            get { return GetColumnValue<int?>(Columns.BrandId); }
            set { SetColumnValue(Columns.BrandId, value); }
        }
        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ContentTitleColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ContentNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn BodyColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn LocaleColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedOnColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedByColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedOnColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedByColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn EventPromoIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        


        public static TableSchema.TableColumn BrandIdColumn
        {
            get { return Schema.Columns[13]; }
        }

        #endregion
        #region Columns Struct
        public struct Columns
		{
			 public static string Id = @"id";
			 public static string Title = @"title";
			 public static string ContentTitle = @"content_title";
			 public static string ContentName = @"content_name";
			 public static string Body = @"body";
			 public static string Locale = @"locale";
			 public static string Status = @"status";
			 public static string CreatedOn = @"created_on";
			 public static string CreatedBy = @"created_by";
			 public static string ModifiedOn = @"modified_on";
			 public static string ModifiedBy = @"modified_by";
			 public static string Type = @"type";
			 public static string EventPromoId = @"event_promo_id";
             public static string BrandId = @"brand_id";

        }
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

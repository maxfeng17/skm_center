using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponDealEvaluateStar class.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealEvaluateStarCollection : ReadOnlyList<ViewPponDealEvaluateStar, ViewPponDealEvaluateStarCollection>
    {        
        public ViewPponDealEvaluateStarCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_deal_evaluate_star view.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealEvaluateStar : ReadOnlyRecord<ViewPponDealEvaluateStar>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_deal_evaluate_star", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBid);
                
                TableSchema.TableColumn colvarCnt = new TableSchema.TableColumn(schema);
                colvarCnt.ColumnName = "cnt";
                colvarCnt.DataType = DbType.Int32;
                colvarCnt.MaxLength = 0;
                colvarCnt.AutoIncrement = false;
                colvarCnt.IsNullable = false;
                colvarCnt.IsPrimaryKey = false;
                colvarCnt.IsForeignKey = false;
                colvarCnt.IsReadOnly = false;
                
                schema.Columns.Add(colvarCnt);
                
                TableSchema.TableColumn colvarStar = new TableSchema.TableColumn(schema);
                colvarStar.ColumnName = "star";
                colvarStar.DataType = DbType.Decimal;
                colvarStar.MaxLength = 0;
                colvarStar.AutoIncrement = false;
                colvarStar.IsNullable = false;
                colvarStar.IsPrimaryKey = false;
                colvarStar.IsForeignKey = false;
                colvarStar.IsReadOnly = false;
                
                schema.Columns.Add(colvarStar);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_deal_evaluate_star",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponDealEvaluateStar()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponDealEvaluateStar(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponDealEvaluateStar(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponDealEvaluateStar(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("bid");
		    }
            set 
		    {
			    SetColumnValue("bid", value);
            }
        }
	      
        [XmlAttribute("Cnt")]
        [Bindable(true)]
        public int Cnt 
	    {
		    get
		    {
			    return GetColumnValue<int>("cnt");
		    }
            set 
		    {
			    SetColumnValue("cnt", value);
            }
        }
	      
        [XmlAttribute("Star")]
        [Bindable(true)]
        public decimal Star 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("star");
		    }
            set 
		    {
			    SetColumnValue("star", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Bid = @"bid";
            
            public static string Cnt = @"cnt";
            
            public static string Star = @"star";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

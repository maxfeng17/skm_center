using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CategoryVourcher class.
	/// </summary>
    [Serializable]
	public partial class CategoryVourcherCollection : RepositoryList<CategoryVourcher, CategoryVourcherCollection>
	{	   
		public CategoryVourcherCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CategoryVourcherCollection</returns>
		public CategoryVourcherCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CategoryVourcher o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the category_vourcher table.
	/// </summary>
	[Serializable]
	public partial class CategoryVourcher : RepositoryRecord<CategoryVourcher>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CategoryVourcher()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CategoryVourcher(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("category_vourcher", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarVourcherId = new TableSchema.TableColumn(schema);
				colvarVourcherId.ColumnName = "vourcher_id";
				colvarVourcherId.DataType = DbType.Int32;
				colvarVourcherId.MaxLength = 0;
				colvarVourcherId.AutoIncrement = false;
				colvarVourcherId.IsNullable = false;
				colvarVourcherId.IsPrimaryKey = false;
				colvarVourcherId.IsForeignKey = false;
				colvarVourcherId.IsReadOnly = false;
				colvarVourcherId.DefaultSetting = @"";
				colvarVourcherId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVourcherId);
				
				TableSchema.TableColumn colvarCid = new TableSchema.TableColumn(schema);
				colvarCid.ColumnName = "cid";
				colvarCid.DataType = DbType.Int32;
				colvarCid.MaxLength = 0;
				colvarCid.AutoIncrement = false;
				colvarCid.IsNullable = false;
				colvarCid.IsPrimaryKey = false;
				colvarCid.IsForeignKey = false;
				colvarCid.IsReadOnly = false;
				colvarCid.DefaultSetting = @"";
				colvarCid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("category_vourcher",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("VourcherId")]
		[Bindable(true)]
		public int VourcherId 
		{
			get { return GetColumnValue<int>(Columns.VourcherId); }
			set { SetColumnValue(Columns.VourcherId, value); }
		}
		  
		[XmlAttribute("Cid")]
		[Bindable(true)]
		public int Cid 
		{
			get { return GetColumnValue<int>(Columns.Cid); }
			set { SetColumnValue(Columns.Cid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn VourcherIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string VourcherId = @"vourcher_id";
			 public static string Cid = @"cid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

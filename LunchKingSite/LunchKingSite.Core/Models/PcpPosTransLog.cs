using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpPosTransLog class.
	/// </summary>
    [Serializable]
	public partial class PcpPosTransLogCollection : RepositoryList<PcpPosTransLog, PcpPosTransLogCollection>
	{	   
		public PcpPosTransLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpPosTransLogCollection</returns>
		public PcpPosTransLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpPosTransLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_pos_trans_log table.
	/// </summary>
	[Serializable]
	public partial class PcpPosTransLog : RepositoryRecord<PcpPosTransLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpPosTransLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpPosTransLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_pos_trans_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTransType = new TableSchema.TableColumn(schema);
				colvarTransType.ColumnName = "trans_type";
				colvarTransType.DataType = DbType.Byte;
				colvarTransType.MaxLength = 0;
				colvarTransType.AutoIncrement = false;
				colvarTransType.IsNullable = false;
				colvarTransType.IsPrimaryKey = false;
				colvarTransType.IsForeignKey = false;
				colvarTransType.IsReadOnly = false;
				colvarTransType.DefaultSetting = @"";
				colvarTransType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransType);
				
				TableSchema.TableColumn colvarStoreCode = new TableSchema.TableColumn(schema);
				colvarStoreCode.ColumnName = "store_code";
				colvarStoreCode.DataType = DbType.AnsiString;
				colvarStoreCode.MaxLength = 50;
				colvarStoreCode.AutoIncrement = false;
				colvarStoreCode.IsNullable = false;
				colvarStoreCode.IsPrimaryKey = false;
				colvarStoreCode.IsForeignKey = false;
				colvarStoreCode.IsReadOnly = false;
				colvarStoreCode.DefaultSetting = @"";
				colvarStoreCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreCode);
				
				TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
				colvarStoreName.ColumnName = "store_name";
				colvarStoreName.DataType = DbType.String;
				colvarStoreName.MaxLength = 50;
				colvarStoreName.AutoIncrement = false;
				colvarStoreName.IsNullable = false;
				colvarStoreName.IsPrimaryKey = false;
				colvarStoreName.IsForeignKey = false;
				colvarStoreName.IsReadOnly = false;
				colvarStoreName.DefaultSetting = @"";
				colvarStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreName);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = false;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarIndentityCode = new TableSchema.TableColumn(schema);
				colvarIndentityCode.ColumnName = "indentity_code";
				colvarIndentityCode.DataType = DbType.Int32;
				colvarIndentityCode.MaxLength = 0;
				colvarIndentityCode.AutoIncrement = false;
				colvarIndentityCode.IsNullable = false;
				colvarIndentityCode.IsPrimaryKey = false;
				colvarIndentityCode.IsForeignKey = false;
				colvarIndentityCode.IsReadOnly = false;
				colvarIndentityCode.DefaultSetting = @"";
				colvarIndentityCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIndentityCode);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarInputData = new TableSchema.TableColumn(schema);
				colvarInputData.ColumnName = "input_data";
				colvarInputData.DataType = DbType.String;
				colvarInputData.MaxLength = 120;
				colvarInputData.AutoIncrement = false;
				colvarInputData.IsNullable = false;
				colvarInputData.IsPrimaryKey = false;
				colvarInputData.IsForeignKey = false;
				colvarInputData.IsReadOnly = false;
				colvarInputData.DefaultSetting = @"";
				colvarInputData.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInputData);
				
				TableSchema.TableColumn colvarResult = new TableSchema.TableColumn(schema);
				colvarResult.ColumnName = "result";
				colvarResult.DataType = DbType.String;
				colvarResult.MaxLength = 120;
				colvarResult.AutoIncrement = false;
				colvarResult.IsNullable = false;
				colvarResult.IsPrimaryKey = false;
				colvarResult.IsForeignKey = false;
				colvarResult.IsReadOnly = false;
				colvarResult.DefaultSetting = @"";
				colvarResult.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResult);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarOauthClientAppId = new TableSchema.TableColumn(schema);
				colvarOauthClientAppId.ColumnName = "oauth_client_app_id";
				colvarOauthClientAppId.DataType = DbType.String;
				colvarOauthClientAppId.MaxLength = 100;
				colvarOauthClientAppId.AutoIncrement = false;
				colvarOauthClientAppId.IsNullable = true;
				colvarOauthClientAppId.IsPrimaryKey = false;
				colvarOauthClientAppId.IsForeignKey = false;
				colvarOauthClientAppId.IsReadOnly = false;
				colvarOauthClientAppId.DefaultSetting = @"";
				colvarOauthClientAppId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOauthClientAppId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_pos_trans_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TransType")]
		[Bindable(true)]
		public byte TransType 
		{
			get { return GetColumnValue<byte>(Columns.TransType); }
			set { SetColumnValue(Columns.TransType, value); }
		}
		  
		[XmlAttribute("StoreCode")]
		[Bindable(true)]
		public string StoreCode 
		{
			get { return GetColumnValue<string>(Columns.StoreCode); }
			set { SetColumnValue(Columns.StoreCode, value); }
		}
		  
		[XmlAttribute("StoreName")]
		[Bindable(true)]
		public string StoreName 
		{
			get { return GetColumnValue<string>(Columns.StoreName); }
			set { SetColumnValue(Columns.StoreName, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid StoreGuid 
		{
			get { return GetColumnValue<Guid>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("IndentityCode")]
		[Bindable(true)]
		public int IndentityCode 
		{
			get { return GetColumnValue<int>(Columns.IndentityCode); }
			set { SetColumnValue(Columns.IndentityCode, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("InputData")]
		[Bindable(true)]
		public string InputData 
		{
			get { return GetColumnValue<string>(Columns.InputData); }
			set { SetColumnValue(Columns.InputData, value); }
		}
		  
		[XmlAttribute("Result")]
		[Bindable(true)]
		public string Result 
		{
			get { return GetColumnValue<string>(Columns.Result); }
			set { SetColumnValue(Columns.Result, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("OauthClientAppId")]
		[Bindable(true)]
		public string OauthClientAppId 
		{
			get { return GetColumnValue<string>(Columns.OauthClientAppId); }
			set { SetColumnValue(Columns.OauthClientAppId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TransTypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreCodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IndentityCodeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn InputDataColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ResultColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn OauthClientAppIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TransType = @"trans_type";
			 public static string StoreCode = @"store_code";
			 public static string StoreName = @"store_name";
			 public static string SellerGuid = @"seller_guid";
			 public static string StoreGuid = @"store_guid";
			 public static string IndentityCode = @"indentity_code";
			 public static string OrderGuid = @"order_guid";
			 public static string InputData = @"input_data";
			 public static string Result = @"result";
			 public static string CreateTime = @"create_time";
			 public static string OauthClientAppId = @"oauth_client_app_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

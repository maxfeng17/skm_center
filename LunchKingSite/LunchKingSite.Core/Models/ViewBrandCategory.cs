using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBrandCategory class.
    /// </summary>
    [Serializable]
    public partial class ViewBrandCategoryCollection : ReadOnlyList<ViewBrandCategory, ViewBrandCategoryCollection>
    {        
        public ViewBrandCategoryCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_brand_category view.
    /// </summary>
    [Serializable]
    public partial class ViewBrandCategory : ReadOnlyRecord<ViewBrandCategory>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_brand_category", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBrandId = new TableSchema.TableColumn(schema);
                colvarBrandId.ColumnName = "brand_id";
                colvarBrandId.DataType = DbType.Int32;
                colvarBrandId.MaxLength = 0;
                colvarBrandId.AutoIncrement = false;
                colvarBrandId.IsNullable = true;
                colvarBrandId.IsPrimaryKey = false;
                colvarBrandId.IsForeignKey = false;
                colvarBrandId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBrandId);
                
                TableSchema.TableColumn colvarBrandItemId = new TableSchema.TableColumn(schema);
                colvarBrandItemId.ColumnName = "brand_item_id";
                colvarBrandItemId.DataType = DbType.Int32;
                colvarBrandItemId.MaxLength = 0;
                colvarBrandItemId.AutoIncrement = false;
                colvarBrandItemId.IsNullable = false;
                colvarBrandItemId.IsPrimaryKey = false;
                colvarBrandItemId.IsForeignKey = false;
                colvarBrandItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBrandItemId);
                
                TableSchema.TableColumn colvarBrandItemCategoryId = new TableSchema.TableColumn(schema);
                colvarBrandItemCategoryId.ColumnName = "brand_item_category_id";
                colvarBrandItemCategoryId.DataType = DbType.Int32;
                colvarBrandItemCategoryId.MaxLength = 0;
                colvarBrandItemCategoryId.AutoIncrement = false;
                colvarBrandItemCategoryId.IsNullable = true;
                colvarBrandItemCategoryId.IsPrimaryKey = false;
                colvarBrandItemCategoryId.IsForeignKey = false;
                colvarBrandItemCategoryId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBrandItemCategoryId);
                
                TableSchema.TableColumn colvarCategoryName = new TableSchema.TableColumn(schema);
                colvarCategoryName.ColumnName = "category_name";
                colvarCategoryName.DataType = DbType.String;
                colvarCategoryName.MaxLength = 50;
                colvarCategoryName.AutoIncrement = false;
                colvarCategoryName.IsNullable = true;
                colvarCategoryName.IsPrimaryKey = false;
                colvarCategoryName.IsForeignKey = false;
                colvarCategoryName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryName);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = false;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarSeq);
                
                TableSchema.TableColumn colvarItemStatus = new TableSchema.TableColumn(schema);
                colvarItemStatus.ColumnName = "item_status";
                colvarItemStatus.DataType = DbType.Boolean;
                colvarItemStatus.MaxLength = 0;
                colvarItemStatus.AutoIncrement = false;
                colvarItemStatus.IsNullable = false;
                colvarItemStatus.IsPrimaryKey = false;
                colvarItemStatus.IsForeignKey = false;
                colvarItemStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemStatus);
                
                TableSchema.TableColumn colvarBrandItemCategorySeq = new TableSchema.TableColumn(schema);
                colvarBrandItemCategorySeq.ColumnName = "brand_item_category_seq";
                colvarBrandItemCategorySeq.DataType = DbType.Int32;
                colvarBrandItemCategorySeq.MaxLength = 0;
                colvarBrandItemCategorySeq.AutoIncrement = false;
                colvarBrandItemCategorySeq.IsNullable = true;
                colvarBrandItemCategorySeq.IsPrimaryKey = false;
                colvarBrandItemCategorySeq.IsForeignKey = false;
                colvarBrandItemCategorySeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarBrandItemCategorySeq);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_brand_category",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBrandCategory()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBrandCategory(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBrandCategory(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBrandCategory(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BrandId")]
        [Bindable(true)]
        public int? BrandId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("brand_id");
		    }
            set 
		    {
			    SetColumnValue("brand_id", value);
            }
        }
	      
        [XmlAttribute("BrandItemId")]
        [Bindable(true)]
        public int BrandItemId 
	    {
		    get
		    {
			    return GetColumnValue<int>("brand_item_id");
		    }
            set 
		    {
			    SetColumnValue("brand_item_id", value);
            }
        }
	      
        [XmlAttribute("BrandItemCategoryId")]
        [Bindable(true)]
        public int? BrandItemCategoryId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("brand_item_category_id");
		    }
            set 
		    {
			    SetColumnValue("brand_item_category_id", value);
            }
        }
	      
        [XmlAttribute("CategoryName")]
        [Bindable(true)]
        public string CategoryName 
	    {
		    get
		    {
			    return GetColumnValue<string>("category_name");
		    }
            set 
		    {
			    SetColumnValue("category_name", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int Seq 
	    {
		    get
		    {
			    return GetColumnValue<int>("seq");
		    }
            set 
		    {
			    SetColumnValue("seq", value);
            }
        }
	      
        [XmlAttribute("ItemStatus")]
        [Bindable(true)]
        public bool ItemStatus 
	    {
		    get
		    {
			    return GetColumnValue<bool>("item_status");
		    }
            set 
		    {
			    SetColumnValue("item_status", value);
            }
        }
	      
        [XmlAttribute("BrandItemCategorySeq")]
        [Bindable(true)]
        public int? BrandItemCategorySeq 
	    {
		    get
		    {
			    return GetColumnValue<int?>("brand_item_category_seq");
		    }
            set 
		    {
			    SetColumnValue("brand_item_category_seq", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BrandId = @"brand_id";
            
            public static string BrandItemId = @"brand_item_id";
            
            public static string BrandItemCategoryId = @"brand_item_category_id";
            
            public static string CategoryName = @"category_name";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string Seq = @"seq";
            
            public static string ItemStatus = @"item_status";
            
            public static string BrandItemCategorySeq = @"brand_item_category_seq";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

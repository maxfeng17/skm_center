using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewPponCashTrustLog class.
    /// </summary>
    [Serializable]
    public partial class ViewPponCashTrustLogCollection : ReadOnlyList<ViewPponCashTrustLog, ViewPponCashTrustLogCollection>
    {
        public ViewPponCashTrustLogCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_cash_trust_log view.
    /// </summary>
    [Serializable]
    public partial class ViewPponCashTrustLog : ReadOnlyRecord<ViewPponCashTrustLog>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_cash_trust_log", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;

                schema.Columns.Add(colvarTrustId);

                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;

                schema.Columns.Add(colvarCouponId);

                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.String;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = true;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;

                schema.Columns.Add(colvarCouponSequenceNumber);

                TableSchema.TableColumn colvarCouponCode = new TableSchema.TableColumn(schema);
                colvarCouponCode.ColumnName = "coupon_code";
                colvarCouponCode.DataType = DbType.AnsiString;
                colvarCouponCode.MaxLength = 50;
                colvarCouponCode.AutoIncrement = false;
                colvarCouponCode.IsNullable = true;
                colvarCouponCode.IsPrimaryKey = false;
                colvarCouponCode.IsForeignKey = false;
                colvarCouponCode.IsReadOnly = false;

                schema.Columns.Add(colvarCouponCode);

                TableSchema.TableColumn colvarCashTrustLogStatus = new TableSchema.TableColumn(schema);
                colvarCashTrustLogStatus.ColumnName = "cash_trust_log_status";
                colvarCashTrustLogStatus.DataType = DbType.Int32;
                colvarCashTrustLogStatus.MaxLength = 0;
                colvarCashTrustLogStatus.AutoIncrement = false;
                colvarCashTrustLogStatus.IsNullable = false;
                colvarCashTrustLogStatus.IsPrimaryKey = false;
                colvarCashTrustLogStatus.IsForeignKey = false;
                colvarCashTrustLogStatus.IsReadOnly = false;

                schema.Columns.Add(colvarCashTrustLogStatus);

                TableSchema.TableColumn colvarCashTrustLogSpecialStatus = new TableSchema.TableColumn(schema);
                colvarCashTrustLogSpecialStatus.ColumnName = "cash_trust_log_special_status";
                colvarCashTrustLogSpecialStatus.DataType = DbType.Int32;
                colvarCashTrustLogSpecialStatus.MaxLength = 0;
                colvarCashTrustLogSpecialStatus.AutoIncrement = false;
                colvarCashTrustLogSpecialStatus.IsNullable = false;
                colvarCashTrustLogSpecialStatus.IsPrimaryKey = false;
                colvarCashTrustLogSpecialStatus.IsForeignKey = false;
                colvarCashTrustLogSpecialStatus.IsReadOnly = false;

                schema.Columns.Add(colvarCashTrustLogSpecialStatus);

                TableSchema.TableColumn colvarCashTrustLogSpecialOperatedTime = new TableSchema.TableColumn(schema);
                colvarCashTrustLogSpecialOperatedTime.ColumnName = "cash_trust_log_special_operated_time";
                colvarCashTrustLogSpecialOperatedTime.DataType = DbType.DateTime;
                colvarCashTrustLogSpecialOperatedTime.MaxLength = 0;
                colvarCashTrustLogSpecialOperatedTime.AutoIncrement = false;
                colvarCashTrustLogSpecialOperatedTime.IsNullable = true;
                colvarCashTrustLogSpecialOperatedTime.IsPrimaryKey = false;
                colvarCashTrustLogSpecialOperatedTime.IsForeignKey = false;
                colvarCashTrustLogSpecialOperatedTime.IsReadOnly = false;

                schema.Columns.Add(colvarCashTrustLogSpecialOperatedTime);

                TableSchema.TableColumn colvarGroupOrderStatus = new TableSchema.TableColumn(schema);
                colvarGroupOrderStatus.ColumnName = "group_order_status";
                colvarGroupOrderStatus.DataType = DbType.Int32;
                colvarGroupOrderStatus.MaxLength = 0;
                colvarGroupOrderStatus.AutoIncrement = false;
                colvarGroupOrderStatus.IsNullable = true;
                colvarGroupOrderStatus.IsPrimaryKey = false;
                colvarGroupOrderStatus.IsForeignKey = false;
                colvarGroupOrderStatus.IsReadOnly = false;

                schema.Columns.Add(colvarGroupOrderStatus);

                TableSchema.TableColumn colvarDealAccountingFlag = new TableSchema.TableColumn(schema);
                colvarDealAccountingFlag.ColumnName = "deal_accounting_flag";
                colvarDealAccountingFlag.DataType = DbType.Int32;
                colvarDealAccountingFlag.MaxLength = 0;
                colvarDealAccountingFlag.AutoIncrement = false;
                colvarDealAccountingFlag.IsNullable = false;
                colvarDealAccountingFlag.IsPrimaryKey = false;
                colvarDealAccountingFlag.IsForeignKey = false;
                colvarDealAccountingFlag.IsReadOnly = false;

                schema.Columns.Add(colvarDealAccountingFlag);

                TableSchema.TableColumn colvarOid = new TableSchema.TableColumn(schema);
                colvarOid.ColumnName = "oid";
                colvarOid.DataType = DbType.Guid;
                colvarOid.MaxLength = 0;
                colvarOid.AutoIncrement = false;
                colvarOid.IsNullable = false;
                colvarOid.IsPrimaryKey = false;
                colvarOid.IsForeignKey = false;
                colvarOid.IsReadOnly = false;

                schema.Columns.Add(colvarOid);

                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;

                schema.Columns.Add(colvarOrderId);

                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;

                schema.Columns.Add(colvarBid);

                TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
                colvarEventName.ColumnName = "event_name";
                colvarEventName.DataType = DbType.String;
                colvarEventName.MaxLength = 400;
                colvarEventName.AutoIncrement = false;
                colvarEventName.IsNullable = true;
                colvarEventName.IsPrimaryKey = false;
                colvarEventName.IsForeignKey = false;
                colvarEventName.IsReadOnly = false;

                schema.Columns.Add(colvarEventName);

                TableSchema.TableColumn colvarOrderTimeS = new TableSchema.TableColumn(schema);
                colvarOrderTimeS.ColumnName = "order_time_s";
                colvarOrderTimeS.DataType = DbType.DateTime;
                colvarOrderTimeS.MaxLength = 0;
                colvarOrderTimeS.AutoIncrement = false;
                colvarOrderTimeS.IsNullable = false;
                colvarOrderTimeS.IsPrimaryKey = false;
                colvarOrderTimeS.IsForeignKey = false;
                colvarOrderTimeS.IsReadOnly = false;

                schema.Columns.Add(colvarOrderTimeS);

                TableSchema.TableColumn colvarOrderTimeE = new TableSchema.TableColumn(schema);
                colvarOrderTimeE.ColumnName = "order_time_e";
                colvarOrderTimeE.DataType = DbType.DateTime;
                colvarOrderTimeE.MaxLength = 0;
                colvarOrderTimeE.AutoIncrement = false;
                colvarOrderTimeE.IsNullable = false;
                colvarOrderTimeE.IsPrimaryKey = false;
                colvarOrderTimeE.IsForeignKey = false;
                colvarOrderTimeE.IsReadOnly = false;

                schema.Columns.Add(colvarOrderTimeE);

                TableSchema.TableColumn colvarDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarDeliverTimeS.ColumnName = "deliver_time_s";
                colvarDeliverTimeS.DataType = DbType.DateTime;
                colvarDeliverTimeS.MaxLength = 0;
                colvarDeliverTimeS.AutoIncrement = false;
                colvarDeliverTimeS.IsNullable = true;
                colvarDeliverTimeS.IsPrimaryKey = false;
                colvarDeliverTimeS.IsForeignKey = false;
                colvarDeliverTimeS.IsReadOnly = false;

                schema.Columns.Add(colvarDeliverTimeS);

                TableSchema.TableColumn colvarDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarDeliverTimeE.ColumnName = "deliver_time_e";
                colvarDeliverTimeE.DataType = DbType.DateTime;
                colvarDeliverTimeE.MaxLength = 0;
                colvarDeliverTimeE.AutoIncrement = false;
                colvarDeliverTimeE.IsNullable = true;
                colvarDeliverTimeE.IsPrimaryKey = false;
                colvarDeliverTimeE.IsForeignKey = false;
                colvarDeliverTimeE.IsReadOnly = false;

                schema.Columns.Add(colvarDeliverTimeE);

                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.Guid;
                colvarSellerId.MaxLength = 0;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = false;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;

                schema.Columns.Add(colvarSellerId);

                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;

                schema.Columns.Add(colvarSellerName);

                TableSchema.TableColumn colvarSettleDay1 = new TableSchema.TableColumn(schema);
                colvarSettleDay1.ColumnName = "settle_day1";
                colvarSettleDay1.DataType = DbType.Int32;
                colvarSettleDay1.MaxLength = 0;
                colvarSettleDay1.AutoIncrement = false;
                colvarSettleDay1.IsNullable = true;
                colvarSettleDay1.IsPrimaryKey = false;
                colvarSettleDay1.IsForeignKey = false;
                colvarSettleDay1.IsReadOnly = false;

                schema.Columns.Add(colvarSettleDay1);

                TableSchema.TableColumn colvarSettleDay2 = new TableSchema.TableColumn(schema);
                colvarSettleDay2.ColumnName = "settle_day2";
                colvarSettleDay2.DataType = DbType.Int32;
                colvarSettleDay2.MaxLength = 0;
                colvarSettleDay2.AutoIncrement = false;
                colvarSettleDay2.IsNullable = true;
                colvarSettleDay2.IsPrimaryKey = false;
                colvarSettleDay2.IsForeignKey = false;
                colvarSettleDay2.IsReadOnly = false;

                schema.Columns.Add(colvarSettleDay2);

                TableSchema.TableColumn colvarSettleDay3 = new TableSchema.TableColumn(schema);
                colvarSettleDay3.ColumnName = "settle_day3";
                colvarSettleDay3.DataType = DbType.Int32;
                colvarSettleDay3.MaxLength = 0;
                colvarSettleDay3.AutoIncrement = false;
                colvarSettleDay3.IsNullable = true;
                colvarSettleDay3.IsPrimaryKey = false;
                colvarSettleDay3.IsForeignKey = false;
                colvarSettleDay3.IsReadOnly = false;

                schema.Columns.Add(colvarSettleDay3);

                TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
                colvarSlug.ColumnName = "slug";
                colvarSlug.DataType = DbType.String;
                colvarSlug.MaxLength = 100;
                colvarSlug.AutoIncrement = false;
                colvarSlug.IsNullable = true;
                colvarSlug.IsPrimaryKey = false;
                colvarSlug.IsForeignKey = false;
                colvarSlug.IsReadOnly = false;

                schema.Columns.Add(colvarSlug);

                TableSchema.TableColumn colvarSellerVerifiedQuantity = new TableSchema.TableColumn(schema);
                colvarSellerVerifiedQuantity.ColumnName = "seller_verified_quantity";
                colvarSellerVerifiedQuantity.DataType = DbType.Int32;
                colvarSellerVerifiedQuantity.MaxLength = 0;
                colvarSellerVerifiedQuantity.AutoIncrement = false;
                colvarSellerVerifiedQuantity.IsNullable = true;
                colvarSellerVerifiedQuantity.IsPrimaryKey = false;
                colvarSellerVerifiedQuantity.IsForeignKey = false;
                colvarSellerVerifiedQuantity.IsReadOnly = false;

                schema.Columns.Add(colvarSellerVerifiedQuantity);

                TableSchema.TableColumn colvarSellerVerifiedTime = new TableSchema.TableColumn(schema);
                colvarSellerVerifiedTime.ColumnName = "seller_verified_time";
                colvarSellerVerifiedTime.DataType = DbType.DateTime;
                colvarSellerVerifiedTime.MaxLength = 0;
                colvarSellerVerifiedTime.AutoIncrement = false;
                colvarSellerVerifiedTime.IsNullable = true;
                colvarSellerVerifiedTime.IsPrimaryKey = false;
                colvarSellerVerifiedTime.IsForeignKey = false;
                colvarSellerVerifiedTime.IsReadOnly = false;

                schema.Columns.Add(colvarSellerVerifiedTime);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;

                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarMemberEmail = new TableSchema.TableColumn(schema);
                colvarMemberEmail.ColumnName = "member_email";
                colvarMemberEmail.DataType = DbType.String;
                colvarMemberEmail.MaxLength = 256;
                colvarMemberEmail.AutoIncrement = false;
                colvarMemberEmail.IsNullable = false;
                colvarMemberEmail.IsPrimaryKey = false;
                colvarMemberEmail.IsForeignKey = false;
                colvarMemberEmail.IsReadOnly = false;

                schema.Columns.Add(colvarMemberEmail);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Int32;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;

                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarTrustProvider = new TableSchema.TableColumn(schema);
                colvarTrustProvider.ColumnName = "trust_provider";
                colvarTrustProvider.DataType = DbType.Int32;
                colvarTrustProvider.MaxLength = 0;
                colvarTrustProvider.AutoIncrement = false;
                colvarTrustProvider.IsNullable = false;
                colvarTrustProvider.IsPrimaryKey = false;
                colvarTrustProvider.IsForeignKey = false;
                colvarTrustProvider.IsReadOnly = false;

                schema.Columns.Add(colvarTrustProvider);

                TableSchema.TableColumn colvarBankStatus = new TableSchema.TableColumn(schema);
                colvarBankStatus.ColumnName = "bank_status";
                colvarBankStatus.DataType = DbType.Int32;
                colvarBankStatus.MaxLength = 0;
                colvarBankStatus.AutoIncrement = false;
                colvarBankStatus.IsNullable = false;
                colvarBankStatus.IsPrimaryKey = false;
                colvarBankStatus.IsForeignKey = false;
                colvarBankStatus.IsReadOnly = false;

                schema.Columns.Add(colvarBankStatus);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;

                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarTrustedTime = new TableSchema.TableColumn(schema);
                colvarTrustedTime.ColumnName = "trusted_time";
                colvarTrustedTime.DataType = DbType.DateTime;
                colvarTrustedTime.MaxLength = 0;
                colvarTrustedTime.AutoIncrement = false;
                colvarTrustedTime.IsNullable = true;
                colvarTrustedTime.IsPrimaryKey = false;
                colvarTrustedTime.IsForeignKey = false;
                colvarTrustedTime.IsReadOnly = false;

                schema.Columns.Add(colvarTrustedTime);

                TableSchema.TableColumn colvarTrustedBankTime = new TableSchema.TableColumn(schema);
                colvarTrustedBankTime.ColumnName = "trusted_bank_time";
                colvarTrustedBankTime.DataType = DbType.DateTime;
                colvarTrustedBankTime.MaxLength = 0;
                colvarTrustedBankTime.AutoIncrement = false;
                colvarTrustedBankTime.IsNullable = true;
                colvarTrustedBankTime.IsPrimaryKey = false;
                colvarTrustedBankTime.IsForeignKey = false;
                colvarTrustedBankTime.IsReadOnly = false;

                schema.Columns.Add(colvarTrustedBankTime);

                TableSchema.TableColumn colvarVerifiedTime = new TableSchema.TableColumn(schema);
                colvarVerifiedTime.ColumnName = "verified_time";
                colvarVerifiedTime.DataType = DbType.DateTime;
                colvarVerifiedTime.MaxLength = 0;
                colvarVerifiedTime.AutoIncrement = false;
                colvarVerifiedTime.IsNullable = true;
                colvarVerifiedTime.IsPrimaryKey = false;
                colvarVerifiedTime.IsForeignKey = false;
                colvarVerifiedTime.IsReadOnly = false;

                schema.Columns.Add(colvarVerifiedTime);

                TableSchema.TableColumn colvarVerifiedBankTime = new TableSchema.TableColumn(schema);
                colvarVerifiedBankTime.ColumnName = "verified_bank_time";
                colvarVerifiedBankTime.DataType = DbType.DateTime;
                colvarVerifiedBankTime.MaxLength = 0;
                colvarVerifiedBankTime.AutoIncrement = false;
                colvarVerifiedBankTime.IsNullable = true;
                colvarVerifiedBankTime.IsPrimaryKey = false;
                colvarVerifiedBankTime.IsForeignKey = false;
                colvarVerifiedBankTime.IsReadOnly = false;

                schema.Columns.Add(colvarVerifiedBankTime);

                TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
                colvarOrderDetailGuid.ColumnName = "order_detail_guid";
                colvarOrderDetailGuid.DataType = DbType.Guid;
                colvarOrderDetailGuid.MaxLength = 0;
                colvarOrderDetailGuid.AutoIncrement = false;
                colvarOrderDetailGuid.IsNullable = false;
                colvarOrderDetailGuid.IsPrimaryKey = false;
                colvarOrderDetailGuid.IsForeignKey = false;
                colvarOrderDetailGuid.IsReadOnly = false;

                schema.Columns.Add(colvarOrderDetailGuid);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = -1;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;

                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarPcash = new TableSchema.TableColumn(schema);
                colvarPcash.ColumnName = "pcash";
                colvarPcash.DataType = DbType.Int32;
                colvarPcash.MaxLength = 0;
                colvarPcash.AutoIncrement = false;
                colvarPcash.IsNullable = false;
                colvarPcash.IsPrimaryKey = false;
                colvarPcash.IsForeignKey = false;
                colvarPcash.IsReadOnly = false;

                schema.Columns.Add(colvarPcash);

                TableSchema.TableColumn colvarScash = new TableSchema.TableColumn(schema);
                colvarScash.ColumnName = "scash";
                colvarScash.DataType = DbType.Int32;
                colvarScash.MaxLength = 0;
                colvarScash.AutoIncrement = false;
                colvarScash.IsNullable = false;
                colvarScash.IsPrimaryKey = false;
                colvarScash.IsForeignKey = false;
                colvarScash.IsReadOnly = false;

                schema.Columns.Add(colvarScash);

                TableSchema.TableColumn colvarBcash = new TableSchema.TableColumn(schema);
                colvarBcash.ColumnName = "bcash";
                colvarBcash.DataType = DbType.Int32;
                colvarBcash.MaxLength = 0;
                colvarBcash.AutoIncrement = false;
                colvarBcash.IsNullable = false;
                colvarBcash.IsPrimaryKey = false;
                colvarBcash.IsForeignKey = false;
                colvarBcash.IsReadOnly = false;

                schema.Columns.Add(colvarBcash);

                TableSchema.TableColumn colvarTcash = new TableSchema.TableColumn(schema);
                colvarTcash.ColumnName = "tcash";
                colvarTcash.DataType = DbType.Int32;
                colvarTcash.MaxLength = 0;
                colvarTcash.AutoIncrement = false;
                colvarTcash.IsNullable = false;
                colvarTcash.IsPrimaryKey = false;
                colvarTcash.IsForeignKey = false;
                colvarTcash.IsReadOnly = false;

                schema.Columns.Add(colvarTcash);

                TableSchema.TableColumn colvarFamilyIsp = new TableSchema.TableColumn(schema);
                colvarFamilyIsp.ColumnName = "family_isp";
                colvarFamilyIsp.DataType = DbType.Int32;
                colvarFamilyIsp.MaxLength = 0;
                colvarFamilyIsp.AutoIncrement = false;
                colvarFamilyIsp.IsNullable = false;
                colvarFamilyIsp.IsPrimaryKey = false;
                colvarFamilyIsp.IsForeignKey = false;
                colvarFamilyIsp.IsReadOnly = false;

                schema.Columns.Add(colvarFamilyIsp);

                TableSchema.TableColumn colvarSevenIsp = new TableSchema.TableColumn(schema);
                colvarSevenIsp.ColumnName = "seven_isp";
                colvarSevenIsp.DataType = DbType.Int32;
                colvarSevenIsp.MaxLength = 0;
                colvarSevenIsp.AutoIncrement = false;
                colvarSevenIsp.IsNullable = false;
                colvarSevenIsp.IsPrimaryKey = false;
                colvarSevenIsp.IsForeignKey = false;
                colvarSevenIsp.IsReadOnly = false;

                schema.Columns.Add(colvarSevenIsp);

                TableSchema.TableColumn colvarCreditCard = new TableSchema.TableColumn(schema);
                colvarCreditCard.ColumnName = "credit_card";
                colvarCreditCard.DataType = DbType.Int32;
                colvarCreditCard.MaxLength = 0;
                colvarCreditCard.AutoIncrement = false;
                colvarCreditCard.IsNullable = false;
                colvarCreditCard.IsPrimaryKey = false;
                colvarCreditCard.IsForeignKey = false;
                colvarCreditCard.IsReadOnly = false;

                schema.Columns.Add(colvarCreditCard);

                TableSchema.TableColumn colvarAtm = new TableSchema.TableColumn(schema);
                colvarAtm.ColumnName = "atm";
                colvarAtm.DataType = DbType.Int32;
                colvarAtm.MaxLength = 0;
                colvarAtm.AutoIncrement = false;
                colvarAtm.IsNullable = false;
                colvarAtm.IsPrimaryKey = false;
                colvarAtm.IsForeignKey = false;
                colvarAtm.IsReadOnly = false;

                schema.Columns.Add(colvarAtm);

                TableSchema.TableColumn colvarReportTrustedGuid = new TableSchema.TableColumn(schema);
                colvarReportTrustedGuid.ColumnName = "report_trusted_guid";
                colvarReportTrustedGuid.DataType = DbType.Guid;
                colvarReportTrustedGuid.MaxLength = 0;
                colvarReportTrustedGuid.AutoIncrement = false;
                colvarReportTrustedGuid.IsNullable = true;
                colvarReportTrustedGuid.IsPrimaryKey = false;
                colvarReportTrustedGuid.IsForeignKey = false;
                colvarReportTrustedGuid.IsReadOnly = false;

                schema.Columns.Add(colvarReportTrustedGuid);

                TableSchema.TableColumn colvarReportVerifiedGuid = new TableSchema.TableColumn(schema);
                colvarReportVerifiedGuid.ColumnName = "report_verified_guid";
                colvarReportVerifiedGuid.DataType = DbType.Guid;
                colvarReportVerifiedGuid.MaxLength = 0;
                colvarReportVerifiedGuid.AutoIncrement = false;
                colvarReportVerifiedGuid.IsNullable = true;
                colvarReportVerifiedGuid.IsPrimaryKey = false;
                colvarReportVerifiedGuid.IsForeignKey = false;
                colvarReportVerifiedGuid.IsReadOnly = false;

                schema.Columns.Add(colvarReportVerifiedGuid);

                TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
                colvarDiscountAmount.ColumnName = "discount_amount";
                colvarDiscountAmount.DataType = DbType.Int32;
                colvarDiscountAmount.MaxLength = 0;
                colvarDiscountAmount.AutoIncrement = false;
                colvarDiscountAmount.IsNullable = false;
                colvarDiscountAmount.IsPrimaryKey = false;
                colvarDiscountAmount.IsForeignKey = false;
                colvarDiscountAmount.IsReadOnly = false;

                schema.Columns.Add(colvarDiscountAmount);

                TableSchema.TableColumn colvarStoreVerifiedGuid = new TableSchema.TableColumn(schema);
                colvarStoreVerifiedGuid.ColumnName = "store_verified_guid";
                colvarStoreVerifiedGuid.DataType = DbType.Guid;
                colvarStoreVerifiedGuid.MaxLength = 0;
                colvarStoreVerifiedGuid.AutoIncrement = false;
                colvarStoreVerifiedGuid.IsNullable = true;
                colvarStoreVerifiedGuid.IsPrimaryKey = false;
                colvarStoreVerifiedGuid.IsForeignKey = false;
                colvarStoreVerifiedGuid.IsReadOnly = false;

                schema.Columns.Add(colvarStoreVerifiedGuid);

                TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
                colvarOrderClassification.ColumnName = "order_classification";
                colvarOrderClassification.DataType = DbType.Int32;
                colvarOrderClassification.MaxLength = 0;
                colvarOrderClassification.AutoIncrement = false;
                colvarOrderClassification.IsNullable = false;
                colvarOrderClassification.IsPrimaryKey = false;
                colvarOrderClassification.IsForeignKey = false;
                colvarOrderClassification.IsReadOnly = false;

                schema.Columns.Add(colvarOrderClassification);

                TableSchema.TableColumn colvarReceivableId = new TableSchema.TableColumn(schema);
                colvarReceivableId.ColumnName = "receivable_id";
                colvarReceivableId.DataType = DbType.Int32;
                colvarReceivableId.MaxLength = 0;
                colvarReceivableId.AutoIncrement = false;
                colvarReceivableId.IsNullable = true;
                colvarReceivableId.IsPrimaryKey = false;
                colvarReceivableId.IsForeignKey = false;
                colvarReceivableId.IsReadOnly = false;

                schema.Columns.Add(colvarReceivableId);

                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;

                schema.Columns.Add(colvarOrderStatus);

                TableSchema.TableColumn colvarOrderCreateId = new TableSchema.TableColumn(schema);
                colvarOrderCreateId.ColumnName = "order_create_id";
                colvarOrderCreateId.DataType = DbType.String;
                colvarOrderCreateId.MaxLength = 30;
                colvarOrderCreateId.AutoIncrement = false;
                colvarOrderCreateId.IsNullable = false;
                colvarOrderCreateId.IsPrimaryKey = false;
                colvarOrderCreateId.IsForeignKey = false;
                colvarOrderCreateId.IsReadOnly = false;

                schema.Columns.Add(colvarOrderCreateId);

                TableSchema.TableColumn colvarOrderCreateTime = new TableSchema.TableColumn(schema);
                colvarOrderCreateTime.ColumnName = "order_create_time";
                colvarOrderCreateTime.DataType = DbType.DateTime;
                colvarOrderCreateTime.MaxLength = 0;
                colvarOrderCreateTime.AutoIncrement = false;
                colvarOrderCreateTime.IsNullable = false;
                colvarOrderCreateTime.IsPrimaryKey = false;
                colvarOrderCreateTime.IsForeignKey = false;
                colvarOrderCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarOrderCreateTime);

                TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
                colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
                colvarBusinessHourOrderMinimum.MaxLength = 0;
                colvarBusinessHourOrderMinimum.AutoIncrement = false;
                colvarBusinessHourOrderMinimum.IsNullable = false;
                colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
                colvarBusinessHourOrderMinimum.IsForeignKey = false;
                colvarBusinessHourOrderMinimum.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourOrderMinimum);

                TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
                colvarOrderTotalLimit.ColumnName = "order_total_limit";
                colvarOrderTotalLimit.DataType = DbType.Currency;
                colvarOrderTotalLimit.MaxLength = 0;
                colvarOrderTotalLimit.AutoIncrement = false;
                colvarOrderTotalLimit.IsNullable = true;
                colvarOrderTotalLimit.IsPrimaryKey = false;
                colvarOrderTotalLimit.IsForeignKey = false;
                colvarOrderTotalLimit.IsReadOnly = false;

                schema.Columns.Add(colvarOrderTotalLimit);

                TableSchema.TableColumn colvarBusinessHourAtmMaximum = new TableSchema.TableColumn(schema);
                colvarBusinessHourAtmMaximum.ColumnName = "business_hour_atm_maximum";
                colvarBusinessHourAtmMaximum.DataType = DbType.Int32;
                colvarBusinessHourAtmMaximum.MaxLength = 0;
                colvarBusinessHourAtmMaximum.AutoIncrement = false;
                colvarBusinessHourAtmMaximum.IsNullable = false;
                colvarBusinessHourAtmMaximum.IsPrimaryKey = false;
                colvarBusinessHourAtmMaximum.IsForeignKey = false;
                colvarBusinessHourAtmMaximum.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourAtmMaximum);

                TableSchema.TableColumn colvarCheckoutType = new TableSchema.TableColumn(schema);
                colvarCheckoutType.ColumnName = "checkout_type";
                colvarCheckoutType.DataType = DbType.Int32;
                colvarCheckoutType.MaxLength = 0;
                colvarCheckoutType.AutoIncrement = false;
                colvarCheckoutType.IsNullable = false;
                colvarCheckoutType.IsPrimaryKey = false;
                colvarCheckoutType.IsForeignKey = false;
                colvarCheckoutType.IsReadOnly = false;

                schema.Columns.Add(colvarCheckoutType);

                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Int32;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = false;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;

                schema.Columns.Add(colvarCost);

                TableSchema.TableColumn colvarUninvoicedAmount = new TableSchema.TableColumn(schema);
                colvarUninvoicedAmount.ColumnName = "uninvoiced_amount";
                colvarUninvoicedAmount.DataType = DbType.Int32;
                colvarUninvoicedAmount.MaxLength = 0;
                colvarUninvoicedAmount.AutoIncrement = false;
                colvarUninvoicedAmount.IsNullable = false;
                colvarUninvoicedAmount.IsPrimaryKey = false;
                colvarUninvoicedAmount.IsForeignKey = false;
                colvarUninvoicedAmount.IsReadOnly = false;

                schema.Columns.Add(colvarUninvoicedAmount);

                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = true;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;

                schema.Columns.Add(colvarDeliveryType);

                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;

                schema.Columns.Add(colvarUniqueId);

                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;

                schema.Columns.Add(colvarStoreGuid);

                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourStatus);

                TableSchema.TableColumn colvarIsComplete = new TableSchema.TableColumn(schema);
                colvarIsComplete.ColumnName = "is_complete";
                colvarIsComplete.DataType = DbType.Boolean;
                colvarIsComplete.MaxLength = 0;
                colvarIsComplete.AutoIncrement = false;
                colvarIsComplete.IsNullable = false;
                colvarIsComplete.IsPrimaryKey = false;
                colvarIsComplete.IsForeignKey = false;
                colvarIsComplete.IsReadOnly = false;

                schema.Columns.Add(colvarIsComplete);

                TableSchema.TableColumn colvarFirstRsrc = new TableSchema.TableColumn(schema);
                colvarFirstRsrc.ColumnName = "first_rsrc";
                colvarFirstRsrc.DataType = DbType.String;
                colvarFirstRsrc.MaxLength = 50;
                colvarFirstRsrc.AutoIncrement = false;
                colvarFirstRsrc.IsNullable = true;
                colvarFirstRsrc.IsPrimaryKey = false;
                colvarFirstRsrc.IsForeignKey = false;
                colvarFirstRsrc.IsReadOnly = false;

                schema.Columns.Add(colvarFirstRsrc);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_cash_trust_log", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewPponCashTrustLog()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponCashTrustLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewPponCashTrustLog(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewPponCashTrustLog(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId
        {
            get
            {
                return GetColumnValue<Guid>("trust_id");
            }
            set
            {
                SetColumnValue("trust_id", value);
            }
        }

        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId
        {
            get
            {
                return GetColumnValue<int?>("coupon_id");
            }
            set
            {
                SetColumnValue("coupon_id", value);
            }
        }

        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber
        {
            get
            {
                return GetColumnValue<string>("coupon_sequence_number");
            }
            set
            {
                SetColumnValue("coupon_sequence_number", value);
            }
        }

        [XmlAttribute("CouponCode")]
        [Bindable(true)]
        public string CouponCode
        {
            get
            {
                return GetColumnValue<string>("coupon_code");
            }
            set
            {
                SetColumnValue("coupon_code", value);
            }
        }

        [XmlAttribute("CashTrustLogStatus")]
        [Bindable(true)]
        public int CashTrustLogStatus
        {
            get
            {
                return GetColumnValue<int>("cash_trust_log_status");
            }
            set
            {
                SetColumnValue("cash_trust_log_status", value);
            }
        }

        [XmlAttribute("CashTrustLogSpecialStatus")]
        [Bindable(true)]
        public int CashTrustLogSpecialStatus
        {
            get
            {
                return GetColumnValue<int>("cash_trust_log_special_status");
            }
            set
            {
                SetColumnValue("cash_trust_log_special_status", value);
            }
        }

        [XmlAttribute("CashTrustLogSpecialOperatedTime")]
        [Bindable(true)]
        public DateTime? CashTrustLogSpecialOperatedTime
        {
            get
            {
                return GetColumnValue<DateTime?>("cash_trust_log_special_operated_time");
            }
            set
            {
                SetColumnValue("cash_trust_log_special_operated_time", value);
            }
        }

        [XmlAttribute("GroupOrderStatus")]
        [Bindable(true)]
        public int? GroupOrderStatus
        {
            get
            {
                return GetColumnValue<int?>("group_order_status");
            }
            set
            {
                SetColumnValue("group_order_status", value);
            }
        }

        [XmlAttribute("DealAccountingFlag")]
        [Bindable(true)]
        public int DealAccountingFlag
        {
            get
            {
                return GetColumnValue<int>("deal_accounting_flag");
            }
            set
            {
                SetColumnValue("deal_accounting_flag", value);
            }
        }

        [XmlAttribute("Oid")]
        [Bindable(true)]
        public Guid Oid
        {
            get
            {
                return GetColumnValue<Guid>("oid");
            }
            set
            {
                SetColumnValue("oid", value);
            }
        }

        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId
        {
            get
            {
                return GetColumnValue<string>("order_id");
            }
            set
            {
                SetColumnValue("order_id", value);
            }
        }

        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid
        {
            get
            {
                return GetColumnValue<Guid>("bid");
            }
            set
            {
                SetColumnValue("bid", value);
            }
        }

        [XmlAttribute("EventName")]
        [Bindable(true)]
        public string EventName
        {
            get
            {
                return GetColumnValue<string>("event_name");
            }
            set
            {
                SetColumnValue("event_name", value);
            }
        }

        [XmlAttribute("OrderTimeS")]
        [Bindable(true)]
        public DateTime OrderTimeS
        {
            get
            {
                return GetColumnValue<DateTime>("order_time_s");
            }
            set
            {
                SetColumnValue("order_time_s", value);
            }
        }

        [XmlAttribute("OrderTimeE")]
        [Bindable(true)]
        public DateTime OrderTimeE
        {
            get
            {
                return GetColumnValue<DateTime>("order_time_e");
            }
            set
            {
                SetColumnValue("order_time_e", value);
            }
        }

        [XmlAttribute("DeliverTimeS")]
        [Bindable(true)]
        public DateTime? DeliverTimeS
        {
            get
            {
                return GetColumnValue<DateTime?>("deliver_time_s");
            }
            set
            {
                SetColumnValue("deliver_time_s", value);
            }
        }

        [XmlAttribute("DeliverTimeE")]
        [Bindable(true)]
        public DateTime? DeliverTimeE
        {
            get
            {
                return GetColumnValue<DateTime?>("deliver_time_e");
            }
            set
            {
                SetColumnValue("deliver_time_e", value);
            }
        }

        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public Guid SellerId
        {
            get
            {
                return GetColumnValue<Guid>("seller_id");
            }
            set
            {
                SetColumnValue("seller_id", value);
            }
        }

        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName
        {
            get
            {
                return GetColumnValue<string>("seller_name");
            }
            set
            {
                SetColumnValue("seller_name", value);
            }
        }

        [XmlAttribute("SettleDay1")]
        [Bindable(true)]
        public int? SettleDay1
        {
            get
            {
                return GetColumnValue<int?>("settle_day1");
            }
            set
            {
                SetColumnValue("settle_day1", value);
            }
        }

        [XmlAttribute("SettleDay2")]
        [Bindable(true)]
        public int? SettleDay2
        {
            get
            {
                return GetColumnValue<int?>("settle_day2");
            }
            set
            {
                SetColumnValue("settle_day2", value);
            }
        }

        [XmlAttribute("SettleDay3")]
        [Bindable(true)]
        public int? SettleDay3
        {
            get
            {
                return GetColumnValue<int?>("settle_day3");
            }
            set
            {
                SetColumnValue("settle_day3", value);
            }
        }

        [XmlAttribute("Slug")]
        [Bindable(true)]
        public string Slug
        {
            get
            {
                return GetColumnValue<string>("slug");
            }
            set
            {
                SetColumnValue("slug", value);
            }
        }

        [XmlAttribute("SellerVerifiedQuantity")]
        [Bindable(true)]
        public int? SellerVerifiedQuantity
        {
            get
            {
                return GetColumnValue<int?>("seller_verified_quantity");
            }
            set
            {
                SetColumnValue("seller_verified_quantity", value);
            }
        }

        [XmlAttribute("SellerVerifiedTime")]
        [Bindable(true)]
        public DateTime? SellerVerifiedTime
        {
            get
            {
                return GetColumnValue<DateTime?>("seller_verified_time");
            }
            set
            {
                SetColumnValue("seller_verified_time", value);
            }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get
            {
                return GetColumnValue<int>("user_id");
            }
            set
            {
                SetColumnValue("user_id", value);
            }
        }

        [XmlAttribute("MemberEmail")]
        [Bindable(true)]
        public string MemberEmail
        {
            get
            {
                return GetColumnValue<string>("member_email");
            }
            set
            {
                SetColumnValue("member_email", value);
            }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public int Amount
        {
            get
            {
                return GetColumnValue<int>("amount");
            }
            set
            {
                SetColumnValue("amount", value);
            }
        }

        [XmlAttribute("TrustProvider")]
        [Bindable(true)]
        public int TrustProvider
        {
            get
            {
                return GetColumnValue<int>("trust_provider");
            }
            set
            {
                SetColumnValue("trust_provider", value);
            }
        }

        [XmlAttribute("BankStatus")]
        [Bindable(true)]
        public int BankStatus
        {
            get
            {
                return GetColumnValue<int>("bank_status");
            }
            set
            {
                SetColumnValue("bank_status", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime
        {
            get
            {
                return GetColumnValue<DateTime>("modify_time");
            }
            set
            {
                SetColumnValue("modify_time", value);
            }
        }

        [XmlAttribute("TrustedTime")]
        [Bindable(true)]
        public DateTime? TrustedTime
        {
            get
            {
                return GetColumnValue<DateTime?>("trusted_time");
            }
            set
            {
                SetColumnValue("trusted_time", value);
            }
        }

        [XmlAttribute("TrustedBankTime")]
        [Bindable(true)]
        public DateTime? TrustedBankTime
        {
            get
            {
                return GetColumnValue<DateTime?>("trusted_bank_time");
            }
            set
            {
                SetColumnValue("trusted_bank_time", value);
            }
        }

        [XmlAttribute("VerifiedTime")]
        [Bindable(true)]
        public DateTime? VerifiedTime
        {
            get
            {
                return GetColumnValue<DateTime?>("verified_time");
            }
            set
            {
                SetColumnValue("verified_time", value);
            }
        }

        [XmlAttribute("VerifiedBankTime")]
        [Bindable(true)]
        public DateTime? VerifiedBankTime
        {
            get
            {
                return GetColumnValue<DateTime?>("verified_bank_time");
            }
            set
            {
                SetColumnValue("verified_bank_time", value);
            }
        }

        [XmlAttribute("OrderDetailGuid")]
        [Bindable(true)]
        public Guid OrderDetailGuid
        {
            get
            {
                return GetColumnValue<Guid>("order_detail_guid");
            }
            set
            {
                SetColumnValue("order_detail_guid", value);
            }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get
            {
                return GetColumnValue<string>("item_name");
            }
            set
            {
                SetColumnValue("item_name", value);
            }
        }

        [XmlAttribute("Pcash")]
        [Bindable(true)]
        public int Pcash
        {
            get
            {
                return GetColumnValue<int>("pcash");
            }
            set
            {
                SetColumnValue("pcash", value);
            }
        }

        [XmlAttribute("Scash")]
        [Bindable(true)]
        public int Scash
        {
            get
            {
                return GetColumnValue<int>("scash");
            }
            set
            {
                SetColumnValue("scash", value);
            }
        }

        [XmlAttribute("Bcash")]
        [Bindable(true)]
        public int Bcash
        {
            get
            {
                return GetColumnValue<int>("bcash");
            }
            set
            {
                SetColumnValue("bcash", value);
            }
        }

        [XmlAttribute("Tcash")]
        [Bindable(true)]
        public int Tcash
        {
            get
            {
                return GetColumnValue<int>("tcash");
            }
            set
            {
                SetColumnValue("tcash", value);
            }
        }

        [XmlAttribute("FamilyIsp")]
        [Bindable(true)]
        public int FamilyIsp
        {
            get
            {
                return GetColumnValue<int>("family_isp");
            }
            set
            {
                SetColumnValue("family_isp", value);
            }
        }

        [XmlAttribute("SevenIsp")]
        [Bindable(true)]
        public int SevenIsp
        {
            get
            {
                return GetColumnValue<int>("seven_isp");
            }
            set
            {
                SetColumnValue("seven_isp", value);
            }
        }

        [XmlAttribute("CreditCard")]
        [Bindable(true)]
        public int CreditCard
        {
            get
            {
                return GetColumnValue<int>("credit_card");
            }
            set
            {
                SetColumnValue("credit_card", value);
            }
        }

        [XmlAttribute("Atm")]
        [Bindable(true)]
        public int Atm
        {
            get
            {
                return GetColumnValue<int>("atm");
            }
            set
            {
                SetColumnValue("atm", value);
            }
        }

        [XmlAttribute("ReportTrustedGuid")]
        [Bindable(true)]
        public Guid? ReportTrustedGuid
        {
            get
            {
                return GetColumnValue<Guid?>("report_trusted_guid");
            }
            set
            {
                SetColumnValue("report_trusted_guid", value);
            }
        }

        [XmlAttribute("ReportVerifiedGuid")]
        [Bindable(true)]
        public Guid? ReportVerifiedGuid
        {
            get
            {
                return GetColumnValue<Guid?>("report_verified_guid");
            }
            set
            {
                SetColumnValue("report_verified_guid", value);
            }
        }

        [XmlAttribute("DiscountAmount")]
        [Bindable(true)]
        public int DiscountAmount
        {
            get
            {
                return GetColumnValue<int>("discount_amount");
            }
            set
            {
                SetColumnValue("discount_amount", value);
            }
        }

        [XmlAttribute("StoreVerifiedGuid")]
        [Bindable(true)]
        public Guid? StoreVerifiedGuid
        {
            get
            {
                return GetColumnValue<Guid?>("store_verified_guid");
            }
            set
            {
                SetColumnValue("store_verified_guid", value);
            }
        }

        [XmlAttribute("OrderClassification")]
        [Bindable(true)]
        public int OrderClassification
        {
            get
            {
                return GetColumnValue<int>("order_classification");
            }
            set
            {
                SetColumnValue("order_classification", value);
            }
        }

        [XmlAttribute("ReceivableId")]
        [Bindable(true)]
        public int? ReceivableId
        {
            get
            {
                return GetColumnValue<int?>("receivable_id");
            }
            set
            {
                SetColumnValue("receivable_id", value);
            }
        }

        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus
        {
            get
            {
                return GetColumnValue<int>("order_status");
            }
            set
            {
                SetColumnValue("order_status", value);
            }
        }

        [XmlAttribute("OrderCreateId")]
        [Bindable(true)]
        public string OrderCreateId
        {
            get
            {
                return GetColumnValue<string>("order_create_id");
            }
            set
            {
                SetColumnValue("order_create_id", value);
            }
        }

        [XmlAttribute("OrderCreateTime")]
        [Bindable(true)]
        public DateTime OrderCreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("order_create_time");
            }
            set
            {
                SetColumnValue("order_create_time", value);
            }
        }

        [XmlAttribute("BusinessHourOrderMinimum")]
        [Bindable(true)]
        public decimal BusinessHourOrderMinimum
        {
            get
            {
                return GetColumnValue<decimal>("business_hour_order_minimum");
            }
            set
            {
                SetColumnValue("business_hour_order_minimum", value);
            }
        }

        [XmlAttribute("OrderTotalLimit")]
        [Bindable(true)]
        public decimal? OrderTotalLimit
        {
            get
            {
                return GetColumnValue<decimal?>("order_total_limit");
            }
            set
            {
                SetColumnValue("order_total_limit", value);
            }
        }

        [XmlAttribute("BusinessHourAtmMaximum")]
        [Bindable(true)]
        public int BusinessHourAtmMaximum
        {
            get
            {
                return GetColumnValue<int>("business_hour_atm_maximum");
            }
            set
            {
                SetColumnValue("business_hour_atm_maximum", value);
            }
        }

        [XmlAttribute("CheckoutType")]
        [Bindable(true)]
        public int CheckoutType
        {
            get
            {
                return GetColumnValue<int>("checkout_type");
            }
            set
            {
                SetColumnValue("checkout_type", value);
            }
        }

        [XmlAttribute("Cost")]
        [Bindable(true)]
        public int Cost
        {
            get
            {
                return GetColumnValue<int>("cost");
            }
            set
            {
                SetColumnValue("cost", value);
            }
        }

        [XmlAttribute("UninvoicedAmount")]
        [Bindable(true)]
        public int UninvoicedAmount
        {
            get
            {
                return GetColumnValue<int>("uninvoiced_amount");
            }
            set
            {
                SetColumnValue("uninvoiced_amount", value);
            }
        }

        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int? DeliveryType
        {
            get
            {
                return GetColumnValue<int?>("delivery_type");
            }
            set
            {
                SetColumnValue("delivery_type", value);
            }
        }

        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId
        {
            get
            {
                return GetColumnValue<int>("unique_id");
            }
            set
            {
                SetColumnValue("unique_id", value);
            }
        }

        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid
        {
            get
            {
                return GetColumnValue<Guid?>("store_guid");
            }
            set
            {
                SetColumnValue("store_guid", value);
            }
        }

        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus
        {
            get
            {
                return GetColumnValue<int>("business_hour_status");
            }
            set
            {
                SetColumnValue("business_hour_status", value);
            }
        }

        [XmlAttribute("IsComplete")]
        [Bindable(true)]
        public bool IsComplete
        {
            get
            {
                return GetColumnValue<bool>("is_complete");
            }
            set
            {
                SetColumnValue("is_complete", value);
            }
        }

        [XmlAttribute("FirstRsrc")]
        [Bindable(true)]
        public string FirstRsrc
        {
            get
            {
                return GetColumnValue<string>("first_rsrc");
            }
            set
            {
                SetColumnValue("first_rsrc", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string TrustId = @"trust_id";

            public static string CouponId = @"coupon_id";

            public static string CouponSequenceNumber = @"coupon_sequence_number";

            public static string CouponCode = @"coupon_code";

            public static string CashTrustLogStatus = @"cash_trust_log_status";

            public static string CashTrustLogSpecialStatus = @"cash_trust_log_special_status";

            public static string CashTrustLogSpecialOperatedTime = @"cash_trust_log_special_operated_time";

            public static string GroupOrderStatus = @"group_order_status";

            public static string DealAccountingFlag = @"deal_accounting_flag";

            public static string Oid = @"oid";

            public static string OrderId = @"order_id";

            public static string Bid = @"bid";

            public static string EventName = @"event_name";

            public static string OrderTimeS = @"order_time_s";

            public static string OrderTimeE = @"order_time_e";

            public static string DeliverTimeS = @"deliver_time_s";

            public static string DeliverTimeE = @"deliver_time_e";

            public static string SellerId = @"seller_id";

            public static string SellerName = @"seller_name";

            public static string SettleDay1 = @"settle_day1";

            public static string SettleDay2 = @"settle_day2";

            public static string SettleDay3 = @"settle_day3";

            public static string Slug = @"slug";

            public static string SellerVerifiedQuantity = @"seller_verified_quantity";

            public static string SellerVerifiedTime = @"seller_verified_time";

            public static string UserId = @"user_id";

            public static string MemberEmail = @"member_email";

            public static string Amount = @"amount";

            public static string TrustProvider = @"trust_provider";

            public static string BankStatus = @"bank_status";

            public static string CreateTime = @"create_time";

            public static string ModifyTime = @"modify_time";

            public static string TrustedTime = @"trusted_time";

            public static string TrustedBankTime = @"trusted_bank_time";

            public static string VerifiedTime = @"verified_time";

            public static string VerifiedBankTime = @"verified_bank_time";

            public static string OrderDetailGuid = @"order_detail_guid";

            public static string ItemName = @"item_name";

            public static string Pcash = @"pcash";

            public static string Scash = @"scash";

            public static string Bcash = @"bcash";

            public static string Tcash = @"tcash";

            public static string FamilyIsp = @"family_isp";

            public static string SevenIsp = @"seven_isp";

            public static string CreditCard = @"credit_card";

            public static string Atm = @"atm";

            public static string ReportTrustedGuid = @"report_trusted_guid";

            public static string ReportVerifiedGuid = @"report_verified_guid";

            public static string DiscountAmount = @"discount_amount";

            public static string StoreVerifiedGuid = @"store_verified_guid";

            public static string OrderClassification = @"order_classification";

            public static string ReceivableId = @"receivable_id";

            public static string OrderStatus = @"order_status";

            public static string OrderCreateId = @"order_create_id";

            public static string OrderCreateTime = @"order_create_time";

            public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";

            public static string OrderTotalLimit = @"order_total_limit";

            public static string BusinessHourAtmMaximum = @"business_hour_atm_maximum";

            public static string CheckoutType = @"checkout_type";

            public static string Cost = @"cost";

            public static string UninvoicedAmount = @"uninvoiced_amount";

            public static string DeliveryType = @"delivery_type";

            public static string UniqueId = @"unique_id";

            public static string StoreGuid = @"store_guid";

            public static string BusinessHourStatus = @"business_hour_status";

            public static string IsComplete = @"is_complete";

            public static string FirstRsrc = @"first_rsrc";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

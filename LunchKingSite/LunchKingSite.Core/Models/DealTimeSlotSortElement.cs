using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealTimeSlotSortElement class.
	/// </summary>
    [Serializable]
	public partial class DealTimeSlotSortElementCollection : RepositoryList<DealTimeSlotSortElement, DealTimeSlotSortElementCollection>
	{	   
		public DealTimeSlotSortElementCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealTimeSlotSortElementCollection</returns>
		public DealTimeSlotSortElementCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealTimeSlotSortElement o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_time_slot_sort_element table.
	/// </summary>
	[Serializable]
	public partial class DealTimeSlotSortElement : RepositoryRecord<DealTimeSlotSortElement>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealTimeSlotSortElement()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DealTimeSlotSortElement(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("deal_time_slot_sort_element", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = false;
				colvarCityId.IsPrimaryKey = true;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarElementId = new TableSchema.TableColumn(schema);
				colvarElementId.ColumnName = "element_id";
				colvarElementId.DataType = DbType.Int32;
				colvarElementId.MaxLength = 0;
				colvarElementId.AutoIncrement = false;
				colvarElementId.IsNullable = false;
				colvarElementId.IsPrimaryKey = true;
				colvarElementId.IsForeignKey = false;
				colvarElementId.IsReadOnly = false;
				colvarElementId.DefaultSetting = @"";
				colvarElementId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarElementId);
				
				TableSchema.TableColumn colvarElementWeight = new TableSchema.TableColumn(schema);
				colvarElementWeight.ColumnName = "element_weight";
				colvarElementWeight.DataType = DbType.Decimal;
				colvarElementWeight.MaxLength = 0;
				colvarElementWeight.AutoIncrement = false;
				colvarElementWeight.IsNullable = false;
				colvarElementWeight.IsPrimaryKey = false;
				colvarElementWeight.IsForeignKey = false;
				colvarElementWeight.IsReadOnly = false;
				
						colvarElementWeight.DefaultSetting = @"((1))";
				colvarElementWeight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarElementWeight);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("deal_time_slot_sort_element",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int CityId 
		{
			get { return GetColumnValue<int>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("ElementId")]
		[Bindable(true)]
		public int ElementId 
		{
			get { return GetColumnValue<int>(Columns.ElementId); }
			set { SetColumnValue(Columns.ElementId, value); }
		}
		  
		[XmlAttribute("ElementWeight")]
		[Bindable(true)]
		public decimal ElementWeight 
		{
			get { return GetColumnValue<decimal>(Columns.ElementWeight); }
			set { SetColumnValue(Columns.ElementWeight, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ElementIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ElementWeightColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string CityId = @"city_id";
			 public static string ElementId = @"element_id";
			 public static string ElementWeight = @"element_weight";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

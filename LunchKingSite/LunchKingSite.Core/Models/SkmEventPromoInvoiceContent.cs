using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmEventPromoInvoiceContent class.
	/// </summary>
    [Serializable]
	public partial class SkmEventPromoInvoiceContentCollection : RepositoryList<SkmEventPromoInvoiceContent, SkmEventPromoInvoiceContentCollection>
	{	   
		public SkmEventPromoInvoiceContentCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmEventPromoInvoiceContentCollection</returns>
		public SkmEventPromoInvoiceContentCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmEventPromoInvoiceContent o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_event_promo_invoice_content table.
	/// </summary>
	[Serializable]
	public partial class SkmEventPromoInvoiceContent : RepositoryRecord<SkmEventPromoInvoiceContent>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmEventPromoInvoiceContent()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmEventPromoInvoiceContent(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_event_promo_invoice_content", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTaishinCardNumber = new TableSchema.TableColumn(schema);
				colvarTaishinCardNumber.ColumnName = "taishin_card_number";
				colvarTaishinCardNumber.DataType = DbType.String;
				colvarTaishinCardNumber.MaxLength = 16;
				colvarTaishinCardNumber.AutoIncrement = false;
				colvarTaishinCardNumber.IsNullable = false;
				colvarTaishinCardNumber.IsPrimaryKey = false;
				colvarTaishinCardNumber.IsForeignKey = false;
				colvarTaishinCardNumber.IsReadOnly = false;
				colvarTaishinCardNumber.DefaultSetting = @"";
				colvarTaishinCardNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTaishinCardNumber);
				
				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 100;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);
				
				TableSchema.TableColumn colvarInvoiceNumber = new TableSchema.TableColumn(schema);
				colvarInvoiceNumber.ColumnName = "invoice_number";
				colvarInvoiceNumber.DataType = DbType.String;
				colvarInvoiceNumber.MaxLength = 10;
				colvarInvoiceNumber.AutoIncrement = false;
				colvarInvoiceNumber.IsNullable = false;
				colvarInvoiceNumber.IsPrimaryKey = false;
				colvarInvoiceNumber.IsForeignKey = false;
				colvarInvoiceNumber.IsReadOnly = false;
				colvarInvoiceNumber.DefaultSetting = @"";
				colvarInvoiceNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceNumber);
				
				TableSchema.TableColumn colvarLoginUserName = new TableSchema.TableColumn(schema);
				colvarLoginUserName.ColumnName = "login_user_name";
				colvarLoginUserName.DataType = DbType.String;
				colvarLoginUserName.MaxLength = 100;
				colvarLoginUserName.AutoIncrement = false;
				colvarLoginUserName.IsNullable = false;
				colvarLoginUserName.IsPrimaryKey = false;
				colvarLoginUserName.IsForeignKey = false;
				colvarLoginUserName.IsReadOnly = false;
				colvarLoginUserName.DefaultSetting = @"";
				colvarLoginUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLoginUserName);
				
				TableSchema.TableColumn colvarLoginUserAddress = new TableSchema.TableColumn(schema);
				colvarLoginUserAddress.ColumnName = "login_user_address";
				colvarLoginUserAddress.DataType = DbType.String;
				colvarLoginUserAddress.MaxLength = 100;
				colvarLoginUserAddress.AutoIncrement = false;
				colvarLoginUserAddress.IsNullable = false;
				colvarLoginUserAddress.IsPrimaryKey = false;
				colvarLoginUserAddress.IsForeignKey = false;
				colvarLoginUserAddress.IsReadOnly = false;
				colvarLoginUserAddress.DefaultSetting = @"";
				colvarLoginUserAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLoginUserAddress);
				
				TableSchema.TableColumn colvarLoginUserMobile = new TableSchema.TableColumn(schema);
				colvarLoginUserMobile.ColumnName = "login_user_mobile";
				colvarLoginUserMobile.DataType = DbType.AnsiString;
				colvarLoginUserMobile.MaxLength = 50;
				colvarLoginUserMobile.AutoIncrement = false;
				colvarLoginUserMobile.IsNullable = false;
				colvarLoginUserMobile.IsPrimaryKey = false;
				colvarLoginUserMobile.IsForeignKey = false;
				colvarLoginUserMobile.IsReadOnly = false;
				colvarLoginUserMobile.DefaultSetting = @"";
				colvarLoginUserMobile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLoginUserMobile);
				
				TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
				colvarUniqueId.ColumnName = "unique_id";
				colvarUniqueId.DataType = DbType.Int32;
				colvarUniqueId.MaxLength = 0;
				colvarUniqueId.AutoIncrement = false;
				colvarUniqueId.IsNullable = false;
				colvarUniqueId.IsPrimaryKey = false;
				colvarUniqueId.IsForeignKey = false;
				colvarUniqueId.IsReadOnly = false;
				colvarUniqueId.DefaultSetting = @"";
				colvarUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniqueId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_event_promo_invoice_content",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TaishinCardNumber")]
		[Bindable(true)]
		public string TaishinCardNumber 
		{
			get { return GetColumnValue<string>(Columns.TaishinCardNumber); }
			set { SetColumnValue(Columns.TaishinCardNumber, value); }
		}
		  
		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName 
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}
		  
		[XmlAttribute("InvoiceNumber")]
		[Bindable(true)]
		public string InvoiceNumber 
		{
			get { return GetColumnValue<string>(Columns.InvoiceNumber); }
			set { SetColumnValue(Columns.InvoiceNumber, value); }
		}
		  
		[XmlAttribute("LoginUserName")]
		[Bindable(true)]
		public string LoginUserName 
		{
			get { return GetColumnValue<string>(Columns.LoginUserName); }
			set { SetColumnValue(Columns.LoginUserName, value); }
		}
		  
		[XmlAttribute("LoginUserAddress")]
		[Bindable(true)]
		public string LoginUserAddress 
		{
			get { return GetColumnValue<string>(Columns.LoginUserAddress); }
			set { SetColumnValue(Columns.LoginUserAddress, value); }
		}
		  
		[XmlAttribute("LoginUserMobile")]
		[Bindable(true)]
		public string LoginUserMobile 
		{
			get { return GetColumnValue<string>(Columns.LoginUserMobile); }
			set { SetColumnValue(Columns.LoginUserMobile, value); }
		}
		  
		[XmlAttribute("UniqueId")]
		[Bindable(true)]
		public int UniqueId 
		{
			get { return GetColumnValue<int>(Columns.UniqueId); }
			set { SetColumnValue(Columns.UniqueId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TaishinCardNumberColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceNumberColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn LoginUserNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn LoginUserAddressColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn LoginUserMobileColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn UniqueIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TaishinCardNumber = @"taishin_card_number";
			 public static string SellerName = @"seller_name";
			 public static string InvoiceNumber = @"invoice_number";
			 public static string LoginUserName = @"login_user_name";
			 public static string LoginUserAddress = @"login_user_address";
			 public static string LoginUserMobile = @"login_user_mobile";
			 public static string UniqueId = @"unique_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

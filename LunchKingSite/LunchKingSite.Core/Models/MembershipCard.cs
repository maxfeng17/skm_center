using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the MembershipCard class.
    /// </summary>
    [Serializable]
    public partial class MembershipCardCollection : RepositoryList<MembershipCard, MembershipCardCollection>
    {
        public MembershipCardCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MembershipCardCollection</returns>
        public MembershipCardCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MembershipCard o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the membership_card table.
    /// </summary>
    [Serializable]
    public partial class MembershipCard : RepositoryRecord<MembershipCard>, IRecordBase
    {
        #region .ctors and Default Settings

        public MembershipCard()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public MembershipCard(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("membership_card", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
                colvarCardGroupId.ColumnName = "card_group_id";
                colvarCardGroupId.DataType = DbType.Int32;
                colvarCardGroupId.MaxLength = 0;
                colvarCardGroupId.AutoIncrement = false;
                colvarCardGroupId.IsNullable = false;
                colvarCardGroupId.IsPrimaryKey = false;
                colvarCardGroupId.IsForeignKey = false;
                colvarCardGroupId.IsReadOnly = false;
                colvarCardGroupId.DefaultSetting = @"";
                colvarCardGroupId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardGroupId);

                TableSchema.TableColumn colvarVersionId = new TableSchema.TableColumn(schema);
                colvarVersionId.ColumnName = "version_id";
                colvarVersionId.DataType = DbType.Int32;
                colvarVersionId.MaxLength = 0;
                colvarVersionId.AutoIncrement = false;
                colvarVersionId.IsNullable = false;
                colvarVersionId.IsPrimaryKey = false;
                colvarVersionId.IsForeignKey = false;
                colvarVersionId.IsReadOnly = false;
                colvarVersionId.DefaultSetting = @"";
                colvarVersionId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVersionId);

                TableSchema.TableColumn colvarLevel = new TableSchema.TableColumn(schema);
                colvarLevel.ColumnName = "level";
                colvarLevel.DataType = DbType.Int32;
                colvarLevel.MaxLength = 0;
                colvarLevel.AutoIncrement = false;
                colvarLevel.IsNullable = false;
                colvarLevel.IsPrimaryKey = false;
                colvarLevel.IsForeignKey = false;
                colvarLevel.IsReadOnly = false;
                colvarLevel.DefaultSetting = @"";
                colvarLevel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLevel);

                TableSchema.TableColumn colvarPaymentPercent = new TableSchema.TableColumn(schema);
                colvarPaymentPercent.ColumnName = "payment_percent";
                colvarPaymentPercent.DataType = DbType.Double;
                colvarPaymentPercent.MaxLength = 0;
                colvarPaymentPercent.AutoIncrement = false;
                colvarPaymentPercent.IsNullable = false;
                colvarPaymentPercent.IsPrimaryKey = false;
                colvarPaymentPercent.IsForeignKey = false;
                colvarPaymentPercent.IsReadOnly = false;
                colvarPaymentPercent.DefaultSetting = @"";
                colvarPaymentPercent.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPaymentPercent);

                TableSchema.TableColumn colvarInstruction = new TableSchema.TableColumn(schema);
                colvarInstruction.ColumnName = "instruction";
                colvarInstruction.DataType = DbType.String;
                colvarInstruction.MaxLength = -1;
                colvarInstruction.AutoIncrement = false;
                colvarInstruction.IsNullable = false;
                colvarInstruction.IsPrimaryKey = false;
                colvarInstruction.IsForeignKey = false;
                colvarInstruction.IsReadOnly = false;
                colvarInstruction.DefaultSetting = @"";
                colvarInstruction.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInstruction);

                TableSchema.TableColumn colvarOthers = new TableSchema.TableColumn(schema);
                colvarOthers.ColumnName = "others";
                colvarOthers.DataType = DbType.String;
                colvarOthers.MaxLength = -1;
                colvarOthers.AutoIncrement = false;
                colvarOthers.IsNullable = true;
                colvarOthers.IsPrimaryKey = false;
                colvarOthers.IsForeignKey = false;
                colvarOthers.IsReadOnly = false;
                colvarOthers.DefaultSetting = @"";
                colvarOthers.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOthers);

                TableSchema.TableColumn colvarImage = new TableSchema.TableColumn(schema);
                colvarImage.ColumnName = "image";
                colvarImage.DataType = DbType.String;
                colvarImage.MaxLength = 250;
                colvarImage.AutoIncrement = false;
                colvarImage.IsNullable = true;
                colvarImage.IsPrimaryKey = false;
                colvarImage.IsForeignKey = false;
                colvarImage.IsReadOnly = false;
                colvarImage.DefaultSetting = @"";
                colvarImage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarImage);

                TableSchema.TableColumn colvarAvailableDateType = new TableSchema.TableColumn(schema);
                colvarAvailableDateType.ColumnName = "available_date_type";
                colvarAvailableDateType.DataType = DbType.Int32;
                colvarAvailableDateType.MaxLength = 0;
                colvarAvailableDateType.AutoIncrement = false;
                colvarAvailableDateType.IsNullable = false;
                colvarAvailableDateType.IsPrimaryKey = false;
                colvarAvailableDateType.IsForeignKey = false;
                colvarAvailableDateType.IsReadOnly = false;
                colvarAvailableDateType.DefaultSetting = @"";
                colvarAvailableDateType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAvailableDateType);

                TableSchema.TableColumn colvarOrderNeeded = new TableSchema.TableColumn(schema);
                colvarOrderNeeded.ColumnName = "order_needed";
                colvarOrderNeeded.DataType = DbType.Int32;
                colvarOrderNeeded.MaxLength = 0;
                colvarOrderNeeded.AutoIncrement = false;
                colvarOrderNeeded.IsNullable = true;
                colvarOrderNeeded.IsPrimaryKey = false;
                colvarOrderNeeded.IsForeignKey = false;
                colvarOrderNeeded.IsReadOnly = false;
                colvarOrderNeeded.DefaultSetting = @"";
                colvarOrderNeeded.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderNeeded);

                TableSchema.TableColumn colvarAmountNeeded = new TableSchema.TableColumn(schema);
                colvarAmountNeeded.ColumnName = "amount_needed";
                colvarAmountNeeded.DataType = DbType.Currency;
                colvarAmountNeeded.MaxLength = 0;
                colvarAmountNeeded.AutoIncrement = false;
                colvarAmountNeeded.IsNullable = true;
                colvarAmountNeeded.IsPrimaryKey = false;
                colvarAmountNeeded.IsForeignKey = false;
                colvarAmountNeeded.IsReadOnly = false;
                colvarAmountNeeded.DefaultSetting = @"";
                colvarAmountNeeded.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAmountNeeded);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
                colvarEnabled.ColumnName = "enabled";
                colvarEnabled.DataType = DbType.Boolean;
                colvarEnabled.MaxLength = 0;
                colvarEnabled.AutoIncrement = false;
                colvarEnabled.IsNullable = false;
                colvarEnabled.IsPrimaryKey = false;
                colvarEnabled.IsForeignKey = false;
                colvarEnabled.IsReadOnly = false;

                colvarEnabled.DefaultSetting = @"((1))";
                colvarEnabled.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEnabled);

                TableSchema.TableColumn colvarConditionalLogic = new TableSchema.TableColumn(schema);
                colvarConditionalLogic.ColumnName = "conditional_logic";
                colvarConditionalLogic.DataType = DbType.Int32;
                colvarConditionalLogic.MaxLength = 0;
                colvarConditionalLogic.AutoIncrement = false;
                colvarConditionalLogic.IsNullable = false;
                colvarConditionalLogic.IsPrimaryKey = false;
                colvarConditionalLogic.IsForeignKey = false;
                colvarConditionalLogic.IsReadOnly = false;

                colvarConditionalLogic.DefaultSetting = @"((0))";
                colvarConditionalLogic.ForeignKeyTableName = "";
                schema.Columns.Add(colvarConditionalLogic);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Byte;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                colvarStatus.DefaultSetting = @"((0))";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
                colvarOpenTime.ColumnName = "open_time";
                colvarOpenTime.DataType = DbType.DateTime;
                colvarOpenTime.MaxLength = 0;
                colvarOpenTime.AutoIncrement = false;
                colvarOpenTime.IsNullable = false;
                colvarOpenTime.IsPrimaryKey = false;
                colvarOpenTime.IsForeignKey = false;
                colvarOpenTime.IsReadOnly = false;

                colvarOpenTime.DefaultSetting = @"(getdate())";
                colvarOpenTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOpenTime);

                TableSchema.TableColumn colvarCloseTime = new TableSchema.TableColumn(schema);
                colvarCloseTime.ColumnName = "close_time";
                colvarCloseTime.DataType = DbType.DateTime;
                colvarCloseTime.MaxLength = 0;
                colvarCloseTime.AutoIncrement = false;
                colvarCloseTime.IsNullable = false;
                colvarCloseTime.IsPrimaryKey = false;
                colvarCloseTime.IsForeignKey = false;
                colvarCloseTime.IsReadOnly = false;

                colvarCloseTime.DefaultSetting = @"(getdate())";
                colvarCloseTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCloseTime);

                TableSchema.TableColumn colvarCombineUse = new TableSchema.TableColumn(schema);
                colvarCombineUse.ColumnName = "combine_use";
                colvarCombineUse.DataType = DbType.Boolean;
                colvarCombineUse.MaxLength = 0;
                colvarCombineUse.AutoIncrement = false;
                colvarCombineUse.IsNullable = false;
                colvarCombineUse.IsPrimaryKey = false;
                colvarCombineUse.IsForeignKey = false;
                colvarCombineUse.IsReadOnly = false;

                colvarCombineUse.DefaultSetting = @"((0))";
                colvarCombineUse.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCombineUse);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.Int32;
                colvarModifyId.MaxLength = 0;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarReleaseTime = new TableSchema.TableColumn(schema);
                colvarReleaseTime.ColumnName = "release_time";
                colvarReleaseTime.DataType = DbType.DateTime;
                colvarReleaseTime.MaxLength = 0;
                colvarReleaseTime.AutoIncrement = false;
                colvarReleaseTime.IsNullable = true;
                colvarReleaseTime.IsPrimaryKey = false;
                colvarReleaseTime.IsForeignKey = false;
                colvarReleaseTime.IsReadOnly = false;
                colvarReleaseTime.DefaultSetting = @"";
                colvarReleaseTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReleaseTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("membership_card", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("CardGroupId")]
        [Bindable(true)]
        public int CardGroupId
        {
            get { return GetColumnValue<int>(Columns.CardGroupId); }
            set { SetColumnValue(Columns.CardGroupId, value); }
        }

        [XmlAttribute("VersionId")]
        [Bindable(true)]
        public int VersionId
        {
            get { return GetColumnValue<int>(Columns.VersionId); }
            set { SetColumnValue(Columns.VersionId, value); }
        }

        [XmlAttribute("Level")]
        [Bindable(true)]
        public int Level
        {
            get { return GetColumnValue<int>(Columns.Level); }
            set { SetColumnValue(Columns.Level, value); }
        }

        [XmlAttribute("PaymentPercent")]
        [Bindable(true)]
        public double PaymentPercent
        {
            get { return GetColumnValue<double>(Columns.PaymentPercent); }
            set { SetColumnValue(Columns.PaymentPercent, value); }
        }

        [XmlAttribute("Instruction")]
        [Bindable(true)]
        public string Instruction
        {
            get { return GetColumnValue<string>(Columns.Instruction); }
            set { SetColumnValue(Columns.Instruction, value); }
        }

        [XmlAttribute("Others")]
        [Bindable(true)]
        public string Others
        {
            get { return GetColumnValue<string>(Columns.Others); }
            set { SetColumnValue(Columns.Others, value); }
        }

        [XmlAttribute("Image")]
        [Bindable(true)]
        public string Image
        {
            get { return GetColumnValue<string>(Columns.Image); }
            set { SetColumnValue(Columns.Image, value); }
        }

        [XmlAttribute("AvailableDateType")]
        [Bindable(true)]
        public int AvailableDateType
        {
            get { return GetColumnValue<int>(Columns.AvailableDateType); }
            set { SetColumnValue(Columns.AvailableDateType, value); }
        }

        [XmlAttribute("OrderNeeded")]
        [Bindable(true)]
        public int? OrderNeeded
        {
            get { return GetColumnValue<int?>(Columns.OrderNeeded); }
            set { SetColumnValue(Columns.OrderNeeded, value); }
        }

        [XmlAttribute("AmountNeeded")]
        [Bindable(true)]
        public decimal? AmountNeeded
        {
            get { return GetColumnValue<decimal?>(Columns.AmountNeeded); }
            set { SetColumnValue(Columns.AmountNeeded, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId
        {
            get { return GetColumnValue<int>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Enabled")]
        [Bindable(true)]
        public bool Enabled
        {
            get { return GetColumnValue<bool>(Columns.Enabled); }
            set { SetColumnValue(Columns.Enabled, value); }
        }

        [XmlAttribute("ConditionalLogic")]
        [Bindable(true)]
        public int ConditionalLogic
        {
            get { return GetColumnValue<int>(Columns.ConditionalLogic); }
            set { SetColumnValue(Columns.ConditionalLogic, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public byte Status
        {
            get { return GetColumnValue<byte>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("OpenTime")]
        [Bindable(true)]
        public DateTime OpenTime
        {
            get { return GetColumnValue<DateTime>(Columns.OpenTime); }
            set { SetColumnValue(Columns.OpenTime, value); }
        }

        [XmlAttribute("CloseTime")]
        [Bindable(true)]
        public DateTime CloseTime
        {
            get { return GetColumnValue<DateTime>(Columns.CloseTime); }
            set { SetColumnValue(Columns.CloseTime, value); }
        }

        [XmlAttribute("CombineUse")]
        [Bindable(true)]
        public bool CombineUse
        {
            get { return GetColumnValue<bool>(Columns.CombineUse); }
            set { SetColumnValue(Columns.CombineUse, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public int? ModifyId
        {
            get { return GetColumnValue<int?>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ReleaseTime")]
        [Bindable(true)]
        public DateTime? ReleaseTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ReleaseTime); }
            set { SetColumnValue(Columns.ReleaseTime, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn CardGroupIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn VersionIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn LevelColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn PaymentPercentColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn InstructionColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn OthersColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ImageColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn AvailableDateTypeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn OrderNeededColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn AmountNeededColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn ConditionalLogicColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn OpenTimeColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn CloseTimeColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn CombineUseColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn ReleaseTimeColumn
        {
            get { return Schema.Columns[21]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string CardGroupId = @"card_group_id";
            public static string VersionId = @"version_id";
            public static string Level = @"level";
            public static string PaymentPercent = @"payment_percent";
            public static string Instruction = @"instruction";
            public static string Others = @"others";
            public static string Image = @"image";
            public static string AvailableDateType = @"available_date_type";
            public static string OrderNeeded = @"order_needed";
            public static string AmountNeeded = @"amount_needed";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string Enabled = @"enabled";
            public static string ConditionalLogic = @"conditional_logic";
            public static string Status = @"status";
            public static string OpenTime = @"open_time";
            public static string CloseTime = @"close_time";
            public static string CombineUse = @"combine_use";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string ReleaseTime = @"release_time";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBookingSystemCoupon class.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemCouponCollection : ReadOnlyList<ViewBookingSystemCoupon, ViewBookingSystemCouponCollection>
    {        
        public ViewBookingSystemCouponCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_booking_system_coupon view.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemCoupon : ReadOnlyRecord<ViewBookingSystemCoupon>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_booking_system_coupon", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.AnsiString;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = false;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponSequenceNumber);
                
                TableSchema.TableColumn colvarOrderKey = new TableSchema.TableColumn(schema);
                colvarOrderKey.ColumnName = "order_key";
                colvarOrderKey.DataType = DbType.AnsiString;
                colvarOrderKey.MaxLength = 50;
                colvarOrderKey.AutoIncrement = false;
                colvarOrderKey.IsNullable = true;
                colvarOrderKey.IsPrimaryKey = false;
                colvarOrderKey.IsForeignKey = false;
                colvarOrderKey.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderKey);
                
                TableSchema.TableColumn colvarMemberKey = new TableSchema.TableColumn(schema);
                colvarMemberKey.ColumnName = "member_key";
                colvarMemberKey.DataType = DbType.AnsiString;
                colvarMemberKey.MaxLength = 50;
                colvarMemberKey.AutoIncrement = false;
                colvarMemberKey.IsNullable = true;
                colvarMemberKey.IsPrimaryKey = false;
                colvarMemberKey.IsForeignKey = false;
                colvarMemberKey.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberKey);
                
                TableSchema.TableColumn colvarApiId = new TableSchema.TableColumn(schema);
                colvarApiId.ColumnName = "api_id";
                colvarApiId.DataType = DbType.Int32;
                colvarApiId.MaxLength = 0;
                colvarApiId.AutoIncrement = false;
                colvarApiId.IsNullable = true;
                colvarApiId.IsPrimaryKey = false;
                colvarApiId.IsForeignKey = false;
                colvarApiId.IsReadOnly = false;
                
                schema.Columns.Add(colvarApiId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_booking_system_coupon",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBookingSystemCoupon()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBookingSystemCoupon(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBookingSystemCoupon(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBookingSystemCoupon(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_sequence_number");
		    }
            set 
		    {
			    SetColumnValue("coupon_sequence_number", value);
            }
        }
	      
        [XmlAttribute("OrderKey")]
        [Bindable(true)]
        public string OrderKey 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_key");
		    }
            set 
		    {
			    SetColumnValue("order_key", value);
            }
        }
	      
        [XmlAttribute("MemberKey")]
        [Bindable(true)]
        public string MemberKey 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_key");
		    }
            set 
		    {
			    SetColumnValue("member_key", value);
            }
        }
	      
        [XmlAttribute("ApiId")]
        [Bindable(true)]
        public int? ApiId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("api_id");
		    }
            set 
		    {
			    SetColumnValue("api_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CouponSequenceNumber = @"coupon_sequence_number";
            
            public static string OrderKey = @"order_key";
            
            public static string MemberKey = @"member_key";
            
            public static string ApiId = @"api_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MemberMessageTemplate class.
	/// </summary>
    [Serializable]
	public partial class MemberMessageTemplateCollection : RepositoryList<MemberMessageTemplate, MemberMessageTemplateCollection>
	{	   
		public MemberMessageTemplateCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberMessageTemplateCollection</returns>
		public MemberMessageTemplateCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberMessageTemplate o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the member_message_template table.
	/// </summary>
	[Serializable]
	public partial class MemberMessageTemplate : RepositoryRecord<MemberMessageTemplate>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MemberMessageTemplate()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MemberMessageTemplate(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_message_template", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarLogoPic = new TableSchema.TableColumn(schema);
				colvarLogoPic.ColumnName = "logo_pic";
				colvarLogoPic.DataType = DbType.String;
				colvarLogoPic.MaxLength = 200;
				colvarLogoPic.AutoIncrement = false;
				colvarLogoPic.IsNullable = false;
				colvarLogoPic.IsPrimaryKey = false;
				colvarLogoPic.IsForeignKey = false;
				colvarLogoPic.IsReadOnly = false;
				colvarLogoPic.DefaultSetting = @"";
				colvarLogoPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogoPic);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 50;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarAvailableTime = new TableSchema.TableColumn(schema);
				colvarAvailableTime.ColumnName = "available_time";
				colvarAvailableTime.DataType = DbType.DateTime;
				colvarAvailableTime.MaxLength = 0;
				colvarAvailableTime.AutoIncrement = false;
				colvarAvailableTime.IsNullable = false;
				colvarAvailableTime.IsPrimaryKey = false;
				colvarAvailableTime.IsForeignKey = false;
				colvarAvailableTime.IsReadOnly = false;
				colvarAvailableTime.DefaultSetting = @"";
				colvarAvailableTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAvailableTime);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = true;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				colvarEnabled.DefaultSetting = @"";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				TableSchema.TableColumn colvarBackgroundColor = new TableSchema.TableColumn(schema);
				colvarBackgroundColor.ColumnName = "background_color";
				colvarBackgroundColor.DataType = DbType.AnsiString;
				colvarBackgroundColor.MaxLength = 20;
				colvarBackgroundColor.AutoIncrement = false;
				colvarBackgroundColor.IsNullable = true;
				colvarBackgroundColor.IsPrimaryKey = false;
				colvarBackgroundColor.IsForeignKey = false;
				colvarBackgroundColor.IsReadOnly = false;
				colvarBackgroundColor.DefaultSetting = @"";
				colvarBackgroundColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBackgroundColor);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_message_template",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("LogoPic")]
		[Bindable(true)]
		public string LogoPic 
		{
			get { return GetColumnValue<string>(Columns.LogoPic); }
			set { SetColumnValue(Columns.LogoPic, value); }
		}
		  
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		  
		[XmlAttribute("AvailableTime")]
		[Bindable(true)]
		public DateTime AvailableTime 
		{
			get { return GetColumnValue<DateTime>(Columns.AvailableTime); }
			set { SetColumnValue(Columns.AvailableTime, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool? Enabled 
		{
			get { return GetColumnValue<bool?>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		  
		[XmlAttribute("BackgroundColor")]
		[Bindable(true)]
		public string BackgroundColor 
		{
			get { return GetColumnValue<string>(Columns.BackgroundColor); }
			set { SetColumnValue(Columns.BackgroundColor, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn LogoPicColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AvailableTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn BackgroundColorColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string LogoPic = @"logo_pic";
			 public static string Title = @"title";
			 public static string AvailableTime = @"available_time";
			 public static string Enabled = @"enabled";
			 public static string BackgroundColor = @"background_color";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

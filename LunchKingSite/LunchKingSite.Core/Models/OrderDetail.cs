using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OrderDetail class.
	/// </summary>
    [Serializable]
	public partial class OrderDetailCollection : RepositoryList<OrderDetail, OrderDetailCollection>
	{	   
		public OrderDetailCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OrderDetailCollection</returns>
		public OrderDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OrderDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the order_detail table.
	/// </summary>
	[Serializable]
	public partial class OrderDetail : RepositoryRecord<OrderDetail>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OrderDetail()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OrderDetail(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_GUID";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = true;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				
					colvarOrderGuid.ForeignKeyTableName = "order";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 4000;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = false;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);
				
				TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
				colvarItemGuid.ColumnName = "item_GUID";
				colvarItemGuid.DataType = DbType.Guid;
				colvarItemGuid.MaxLength = 0;
				colvarItemGuid.AutoIncrement = false;
				colvarItemGuid.IsNullable = true;
				colvarItemGuid.IsPrimaryKey = false;
				colvarItemGuid.IsForeignKey = false;
				colvarItemGuid.IsReadOnly = false;
				colvarItemGuid.DefaultSetting = @"";
				colvarItemGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemGuid);
				
				TableSchema.TableColumn colvarItemUnitPrice = new TableSchema.TableColumn(schema);
				colvarItemUnitPrice.ColumnName = "item_unit_price";
				colvarItemUnitPrice.DataType = DbType.Currency;
				colvarItemUnitPrice.MaxLength = 0;
				colvarItemUnitPrice.AutoIncrement = false;
				colvarItemUnitPrice.IsNullable = false;
				colvarItemUnitPrice.IsPrimaryKey = false;
				colvarItemUnitPrice.IsForeignKey = false;
				colvarItemUnitPrice.IsReadOnly = false;
				colvarItemUnitPrice.DefaultSetting = @"";
				colvarItemUnitPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemUnitPrice);
				
				TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
				colvarItemQuantity.ColumnName = "item_quantity";
				colvarItemQuantity.DataType = DbType.Int32;
				colvarItemQuantity.MaxLength = 0;
				colvarItemQuantity.AutoIncrement = false;
				colvarItemQuantity.IsNullable = false;
				colvarItemQuantity.IsPrimaryKey = false;
				colvarItemQuantity.IsForeignKey = false;
				colvarItemQuantity.IsReadOnly = false;
				colvarItemQuantity.DefaultSetting = @"";
				colvarItemQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemQuantity);
				
				TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
				colvarTotal.ColumnName = "total";
				colvarTotal.DataType = DbType.Currency;
				colvarTotal.MaxLength = 0;
				colvarTotal.AutoIncrement = false;
				colvarTotal.IsNullable = true;
				colvarTotal.IsPrimaryKey = false;
				colvarTotal.IsForeignKey = false;
				colvarTotal.IsReadOnly = true;
				colvarTotal.DefaultSetting = @"";
				colvarTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotal);
				
				TableSchema.TableColumn colvarConsumerName = new TableSchema.TableColumn(schema);
				colvarConsumerName.ColumnName = "consumer_name";
				colvarConsumerName.DataType = DbType.String;
				colvarConsumerName.MaxLength = 100;
				colvarConsumerName.AutoIncrement = false;
				colvarConsumerName.IsNullable = true;
				colvarConsumerName.IsPrimaryKey = false;
				colvarConsumerName.IsForeignKey = false;
				colvarConsumerName.IsReadOnly = false;
				colvarConsumerName.DefaultSetting = @"";
				colvarConsumerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsumerName);
				
				TableSchema.TableColumn colvarConsumerTeleExt = new TableSchema.TableColumn(schema);
				colvarConsumerTeleExt.ColumnName = "consumer_tele_ext";
				colvarConsumerTeleExt.DataType = DbType.AnsiString;
				colvarConsumerTeleExt.MaxLength = 10;
				colvarConsumerTeleExt.AutoIncrement = false;
				colvarConsumerTeleExt.IsNullable = true;
				colvarConsumerTeleExt.IsPrimaryKey = false;
				colvarConsumerTeleExt.IsForeignKey = false;
				colvarConsumerTeleExt.IsReadOnly = false;
				colvarConsumerTeleExt.DefaultSetting = @"";
				colvarConsumerTeleExt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsumerTeleExt);
				
				TableSchema.TableColumn colvarConsumerGroup = new TableSchema.TableColumn(schema);
				colvarConsumerGroup.ColumnName = "consumer_group";
				colvarConsumerGroup.DataType = DbType.String;
				colvarConsumerGroup.MaxLength = 50;
				colvarConsumerGroup.AutoIncrement = false;
				colvarConsumerGroup.IsNullable = true;
				colvarConsumerGroup.IsPrimaryKey = false;
				colvarConsumerGroup.IsForeignKey = false;
				colvarConsumerGroup.IsReadOnly = false;
				colvarConsumerGroup.DefaultSetting = @"";
				colvarConsumerGroup.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsumerGroup);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				
						colvarCreateId.DefaultSetting = @"(N'sys')";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 30;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
				colvarItemId.ColumnName = "item_id";
				colvarItemId.DataType = DbType.String;
				colvarItemId.MaxLength = 100;
				colvarItemId.AutoIncrement = false;
				colvarItemId.IsNullable = true;
				colvarItemId.IsPrimaryKey = false;
				colvarItemId.IsForeignKey = false;
				colvarItemId.IsReadOnly = false;
				colvarItemId.DefaultSetting = @"";
				colvarItemId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemId);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = true;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order_detail",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName 
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}
		  
		[XmlAttribute("ItemGuid")]
		[Bindable(true)]
		public Guid? ItemGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.ItemGuid); }
			set { SetColumnValue(Columns.ItemGuid, value); }
		}
		  
		[XmlAttribute("ItemUnitPrice")]
		[Bindable(true)]
		public decimal ItemUnitPrice 
		{
			get { return GetColumnValue<decimal>(Columns.ItemUnitPrice); }
			set { SetColumnValue(Columns.ItemUnitPrice, value); }
		}
		  
		[XmlAttribute("ItemQuantity")]
		[Bindable(true)]
		public int ItemQuantity 
		{
			get { return GetColumnValue<int>(Columns.ItemQuantity); }
			set { SetColumnValue(Columns.ItemQuantity, value); }
		}
		  
		[XmlAttribute("Total")]
		[Bindable(true)]
		public decimal? Total 
		{
			get { return GetColumnValue<decimal?>(Columns.Total); }
			set { SetColumnValue(Columns.Total, value); }
		}
		  
		[XmlAttribute("ConsumerName")]
		[Bindable(true)]
		public string ConsumerName 
		{
			get { return GetColumnValue<string>(Columns.ConsumerName); }
			set { SetColumnValue(Columns.ConsumerName, value); }
		}
		  
		[XmlAttribute("ConsumerTeleExt")]
		[Bindable(true)]
		public string ConsumerTeleExt 
		{
			get { return GetColumnValue<string>(Columns.ConsumerTeleExt); }
			set { SetColumnValue(Columns.ConsumerTeleExt, value); }
		}
		  
		[XmlAttribute("ConsumerGroup")]
		[Bindable(true)]
		public string ConsumerGroup 
		{
			get { return GetColumnValue<string>(Columns.ConsumerGroup); }
			set { SetColumnValue(Columns.ConsumerGroup, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ItemId")]
		[Bindable(true)]
		public string ItemId 
		{
			get { return GetColumnValue<string>(Columns.ItemId); }
			set { SetColumnValue(Columns.ItemId, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid? StoreGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemUnitPriceColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemQuantityColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ConsumerNameColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ConsumerTeleExtColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ConsumerGroupColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemIdColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string OrderGuid = @"order_GUID";
			 public static string ItemName = @"item_name";
			 public static string ItemGuid = @"item_GUID";
			 public static string ItemUnitPrice = @"item_unit_price";
			 public static string ItemQuantity = @"item_quantity";
			 public static string Total = @"total";
			 public static string ConsumerName = @"consumer_name";
			 public static string ConsumerTeleExt = @"consumer_tele_ext";
			 public static string ConsumerGroup = @"consumer_group";
			 public static string Status = @"status";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string ItemId = @"item_id";
			 public static string StoreGuid = @"store_guid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

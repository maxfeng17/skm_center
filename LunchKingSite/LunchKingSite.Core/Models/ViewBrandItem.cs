using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewBrandItem class.
    /// </summary>
    [Serializable]
    public partial class ViewBrandItemCollection : ReadOnlyList<ViewBrandItem, ViewBrandItemCollection>
    {
        public ViewBrandItemCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_brand_item view.
    /// </summary>
    [Serializable]
    public partial class ViewBrandItem : ReadOnlyRecord<ViewBrandItem>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_brand_item", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarMainId = new TableSchema.TableColumn(schema);
                colvarMainId.ColumnName = "main_id";
                colvarMainId.DataType = DbType.Int32;
                colvarMainId.MaxLength = 0;
                colvarMainId.AutoIncrement = false;
                colvarMainId.IsNullable = false;
                colvarMainId.IsPrimaryKey = false;
                colvarMainId.IsForeignKey = false;
                colvarMainId.IsReadOnly = false;

                schema.Columns.Add(colvarMainId);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;

                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = false;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;

                schema.Columns.Add(colvarSeq);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Boolean;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourOrderTimeS);

                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourOrderTimeE);

                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = false;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;

                schema.Columns.Add(colvarItemId);

                TableSchema.TableColumn colvarSeqStatus = new TableSchema.TableColumn(schema);
                colvarSeqStatus.ColumnName = "seq_status";
                colvarSeqStatus.DataType = DbType.Boolean;
                colvarSeqStatus.MaxLength = 0;
                colvarSeqStatus.AutoIncrement = false;
                colvarSeqStatus.IsNullable = false;
                colvarSeqStatus.IsPrimaryKey = false;
                colvarSeqStatus.IsForeignKey = false;
                colvarSeqStatus.IsReadOnly = false;

                schema.Columns.Add(colvarSeqStatus);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_brand_item", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewBrandItem()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBrandItem(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewBrandItem(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewBrandItem(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("MainId")]
        [Bindable(true)]
        public int MainId
        {
            get
            {
                return GetColumnValue<int>("main_id");
            }
            set
            {
                SetColumnValue("main_id", value);
            }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get
            {
                return GetColumnValue<string>("item_name");
            }
            set
            {
                SetColumnValue("item_name", value);
            }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid
        {
            get
            {
                return GetColumnValue<Guid>("business_hour_guid");
            }
            set
            {
                SetColumnValue("business_hour_guid", value);
            }
        }

        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int Seq
        {
            get
            {
                return GetColumnValue<int>("seq");
            }
            set
            {
                SetColumnValue("seq", value);
            }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public bool Status
        {
            get
            {
                return GetColumnValue<bool>("status");
            }
            set
            {
                SetColumnValue("status", value);
            }
        }

        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS
        {
            get
            {
                return GetColumnValue<DateTime>("business_hour_order_time_s");
            }
            set
            {
                SetColumnValue("business_hour_order_time_s", value);
            }
        }

        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE
        {
            get
            {
                return GetColumnValue<DateTime>("business_hour_order_time_e");
            }
            set
            {
                SetColumnValue("business_hour_order_time_e", value);
            }
        }

        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int ItemId
        {
            get
            {
                return GetColumnValue<int>("item_id");
            }
            set
            {
                SetColumnValue("item_id", value);
            }
        }

        [XmlAttribute("SeqStatus")]
        [Bindable(true)]
        public bool SeqStatus
        {
            get
            {
                return GetColumnValue<bool>("seq_status");
            }
            set
            {
                SetColumnValue("seq_status", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Id = @"id";

            public static string MainId = @"main_id";

            public static string ItemName = @"item_name";

            public static string BusinessHourGuid = @"business_hour_guid";

            public static string Seq = @"seq";

            public static string Status = @"status";

            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";

            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";

            public static string ItemId = @"item_id";

            public static string SeqStatus = @"seq_status";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

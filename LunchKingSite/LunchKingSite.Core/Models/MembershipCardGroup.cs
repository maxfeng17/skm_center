using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MembershipCardGroup class.
	/// </summary>
    [Serializable]
	public partial class MembershipCardGroupCollection : RepositoryList<MembershipCardGroup, MembershipCardGroupCollection>
	{	   
		public MembershipCardGroupCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MembershipCardGroupCollection</returns>
		public MembershipCardGroupCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MembershipCardGroup o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the membership_card_group table.
	/// </summary>
	
	[Serializable]
	public partial class MembershipCardGroup : RepositoryRecord<MembershipCardGroup>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MembershipCardGroup()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MembershipCardGroup(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("membership_card_group", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
				colvarSellerUserId.ColumnName = "seller_user_id";
				colvarSellerUserId.DataType = DbType.Int32;
				colvarSellerUserId.MaxLength = 0;
				colvarSellerUserId.AutoIncrement = false;
				colvarSellerUserId.IsNullable = false;
				colvarSellerUserId.IsPrimaryKey = false;
				colvarSellerUserId.IsForeignKey = false;
				colvarSellerUserId.IsReadOnly = false;
				colvarSellerUserId.DefaultSetting = @"";
				colvarSellerUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerUserId);
				
				TableSchema.TableColumn colvarCategoryList = new TableSchema.TableColumn(schema);
				colvarCategoryList.ColumnName = "category_list";
				colvarCategoryList.DataType = DbType.AnsiString;
				colvarCategoryList.MaxLength = -1;
				colvarCategoryList.AutoIncrement = false;
				colvarCategoryList.IsNullable = false;
				colvarCategoryList.IsPrimaryKey = false;
				colvarCategoryList.IsForeignKey = false;
				colvarCategoryList.IsReadOnly = false;
				
						colvarCategoryList.DefaultSetting = @"('')";
				colvarCategoryList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryList);
				
				TableSchema.TableColumn colvarHotPoint = new TableSchema.TableColumn(schema);
				colvarHotPoint.ColumnName = "hot_point";
				colvarHotPoint.DataType = DbType.Int32;
				colvarHotPoint.MaxLength = 0;
				colvarHotPoint.AutoIncrement = false;
				colvarHotPoint.IsNullable = false;
				colvarHotPoint.IsPrimaryKey = false;
				colvarHotPoint.IsForeignKey = false;
				colvarHotPoint.IsReadOnly = false;
				
						colvarHotPoint.DefaultSetting = @"((0))";
				colvarHotPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHotPoint);
				
				TableSchema.TableColumn colvarCardType = new TableSchema.TableColumn(schema);
				colvarCardType.ColumnName = "card_type";
				colvarCardType.DataType = DbType.Int32;
				colvarCardType.MaxLength = 0;
				colvarCardType.AutoIncrement = false;
				colvarCardType.IsNullable = false;
				colvarCardType.IsPrimaryKey = false;
				colvarCardType.IsForeignKey = false;
				colvarCardType.IsReadOnly = false;
				
						colvarCardType.DefaultSetting = @"((0))";
				colvarCardType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardType);
				
				TableSchema.TableColumn colvarIsPromo = new TableSchema.TableColumn(schema);
				colvarIsPromo.ColumnName = "is_promo";
				colvarIsPromo.DataType = DbType.Boolean;
				colvarIsPromo.MaxLength = 0;
				colvarIsPromo.AutoIncrement = false;
				colvarIsPromo.IsNullable = false;
				colvarIsPromo.IsPrimaryKey = false;
				colvarIsPromo.IsForeignKey = false;
				colvarIsPromo.IsReadOnly = false;
				
						colvarIsPromo.DefaultSetting = @"((0))";
				colvarIsPromo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsPromo);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				
						colvarSellerGuid.DefaultSetting = @"(CONVERT([uniqueidentifier],CONVERT([binary],(0),0),0))";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarPublishTime = new TableSchema.TableColumn(schema);
				colvarPublishTime.ColumnName = "publish_time";
				colvarPublishTime.DataType = DbType.DateTime;
				colvarPublishTime.MaxLength = 0;
				colvarPublishTime.AutoIncrement = false;
				colvarPublishTime.IsNullable = true;
				colvarPublishTime.IsPrimaryKey = false;
				colvarPublishTime.IsForeignKey = false;
				colvarPublishTime.IsReadOnly = false;
				colvarPublishTime.DefaultSetting = @"";
				colvarPublishTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPublishTime);
				
				TableSchema.TableColumn colvarIsPos = new TableSchema.TableColumn(schema);
				colvarIsPos.ColumnName = "is_pos";
				colvarIsPos.DataType = DbType.Boolean;
				colvarIsPos.MaxLength = 0;
				colvarIsPos.AutoIncrement = false;
				colvarIsPos.IsNullable = false;
				colvarIsPos.IsPrimaryKey = false;
				colvarIsPos.IsForeignKey = false;
				colvarIsPos.IsReadOnly = false;
				
						colvarIsPos.DefaultSetting = @"((0))";
				colvarIsPos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsPos);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("membership_card_group",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("SellerUserId")]
		[Bindable(true)]
		public int SellerUserId 
		{
			get { return GetColumnValue<int>(Columns.SellerUserId); }
			set { SetColumnValue(Columns.SellerUserId, value); }
		}
		
		[XmlAttribute("CategoryList")]
		[Bindable(true)]
		public string CategoryList 
		{
			get { return GetColumnValue<string>(Columns.CategoryList); }
			set { SetColumnValue(Columns.CategoryList, value); }
		}
		
		[XmlAttribute("HotPoint")]
		[Bindable(true)]
		public int HotPoint 
		{
			get { return GetColumnValue<int>(Columns.HotPoint); }
			set { SetColumnValue(Columns.HotPoint, value); }
		}
		
		[XmlAttribute("CardType")]
		[Bindable(true)]
		public int CardType 
		{
			get { return GetColumnValue<int>(Columns.CardType); }
			set { SetColumnValue(Columns.CardType, value); }
		}
		
		[XmlAttribute("IsPromo")]
		[Bindable(true)]
		public bool IsPromo 
		{
			get { return GetColumnValue<bool>(Columns.IsPromo); }
			set { SetColumnValue(Columns.IsPromo, value); }
		}
		
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		
		[XmlAttribute("PublishTime")]
		[Bindable(true)]
		public DateTime? PublishTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.PublishTime); }
			set { SetColumnValue(Columns.PublishTime, value); }
		}
		
		[XmlAttribute("IsPos")]
		[Bindable(true)]
		public bool IsPos 
		{
			get { return GetColumnValue<bool>(Columns.IsPos); }
			set { SetColumnValue(Columns.IsPos, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerUserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryListColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn HotPointColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CardTypeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IsPromoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn PublishTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn IsPosColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SellerUserId = @"seller_user_id";
			 public static string CategoryList = @"category_list";
			 public static string HotPoint = @"hot_point";
			 public static string CardType = @"card_type";
			 public static string IsPromo = @"is_promo";
			 public static string SellerGuid = @"seller_guid";
			 public static string Status = @"status";
			 public static string PublishTime = @"publish_time";
			 public static string IsPos = @"is_pos";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DiscountCode class.
	/// </summary>
    [Serializable]
	public partial class DiscountCodeCollection : RepositoryList<DiscountCode, DiscountCodeCollection>
	{	   
		public DiscountCodeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DiscountCodeCollection</returns>
		public DiscountCodeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DiscountCode o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the discount_code table.
	/// </summary>
	[Serializable]
	public partial class DiscountCode : RepositoryRecord<DiscountCode>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DiscountCode()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DiscountCode(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("discount_code", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCampaignId = new TableSchema.TableColumn(schema);
				colvarCampaignId.ColumnName = "campaign_id";
				colvarCampaignId.DataType = DbType.Int32;
				colvarCampaignId.MaxLength = 0;
				colvarCampaignId.AutoIncrement = false;
				colvarCampaignId.IsNullable = true;
				colvarCampaignId.IsPrimaryKey = false;
				colvarCampaignId.IsForeignKey = false;
				colvarCampaignId.IsReadOnly = false;
				colvarCampaignId.DefaultSetting = @"";
				colvarCampaignId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCampaignId);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "code";
				colvarCode.DataType = DbType.AnsiString;
				colvarCode.MaxLength = 20;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = true;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Currency;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = true;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);
				
				TableSchema.TableColumn colvarUseTime = new TableSchema.TableColumn(schema);
				colvarUseTime.ColumnName = "use_time";
				colvarUseTime.DataType = DbType.DateTime;
				colvarUseTime.MaxLength = 0;
				colvarUseTime.AutoIncrement = false;
				colvarUseTime.IsNullable = true;
				colvarUseTime.IsPrimaryKey = false;
				colvarUseTime.IsForeignKey = false;
				colvarUseTime.IsReadOnly = false;
				colvarUseTime.DefaultSetting = @"";
				colvarUseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUseTime);
				
				TableSchema.TableColumn colvarUseAmount = new TableSchema.TableColumn(schema);
				colvarUseAmount.ColumnName = "use_amount";
				colvarUseAmount.DataType = DbType.Currency;
				colvarUseAmount.MaxLength = 0;
				colvarUseAmount.AutoIncrement = false;
				colvarUseAmount.IsNullable = true;
				colvarUseAmount.IsPrimaryKey = false;
				colvarUseAmount.IsForeignKey = false;
				colvarUseAmount.IsReadOnly = false;
				colvarUseAmount.DefaultSetting = @"";
				colvarUseAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUseAmount);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarOrderAmount = new TableSchema.TableColumn(schema);
				colvarOrderAmount.ColumnName = "order_amount";
				colvarOrderAmount.DataType = DbType.Currency;
				colvarOrderAmount.MaxLength = 0;
				colvarOrderAmount.AutoIncrement = false;
				colvarOrderAmount.IsNullable = true;
				colvarOrderAmount.IsPrimaryKey = false;
				colvarOrderAmount.IsForeignKey = false;
				colvarOrderAmount.IsReadOnly = false;
				colvarOrderAmount.DefaultSetting = @"";
				colvarOrderAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderAmount);
				
				TableSchema.TableColumn colvarCancelTime = new TableSchema.TableColumn(schema);
				colvarCancelTime.ColumnName = "cancel_time";
				colvarCancelTime.DataType = DbType.DateTime;
				colvarCancelTime.MaxLength = 0;
				colvarCancelTime.AutoIncrement = false;
				colvarCancelTime.IsNullable = true;
				colvarCancelTime.IsPrimaryKey = false;
				colvarCancelTime.IsForeignKey = false;
				colvarCancelTime.IsReadOnly = false;
				colvarCancelTime.DefaultSetting = @"";
				colvarCancelTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelTime);
				
				TableSchema.TableColumn colvarOrderCost = new TableSchema.TableColumn(schema);
				colvarOrderCost.ColumnName = "order_cost";
				colvarOrderCost.DataType = DbType.Currency;
				colvarOrderCost.MaxLength = 0;
				colvarOrderCost.AutoIncrement = false;
				colvarOrderCost.IsNullable = true;
				colvarOrderCost.IsPrimaryKey = false;
				colvarOrderCost.IsForeignKey = false;
				colvarOrderCost.IsReadOnly = false;
				colvarOrderCost.DefaultSetting = @"";
				colvarOrderCost.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderCost);
				
				TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
				colvarOrderClassification.ColumnName = "order_classification";
				colvarOrderClassification.DataType = DbType.Int32;
				colvarOrderClassification.MaxLength = 0;
				colvarOrderClassification.AutoIncrement = false;
				colvarOrderClassification.IsNullable = true;
				colvarOrderClassification.IsPrimaryKey = false;
				colvarOrderClassification.IsForeignKey = false;
				colvarOrderClassification.IsReadOnly = false;
				colvarOrderClassification.DefaultSetting = @"";
				colvarOrderClassification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderClassification);
				
				TableSchema.TableColumn colvarReceivableId = new TableSchema.TableColumn(schema);
				colvarReceivableId.ColumnName = "receivable_id";
				colvarReceivableId.DataType = DbType.Int32;
				colvarReceivableId.MaxLength = 0;
				colvarReceivableId.AutoIncrement = false;
				colvarReceivableId.IsNullable = true;
				colvarReceivableId.IsPrimaryKey = false;
				colvarReceivableId.IsForeignKey = false;
				colvarReceivableId.IsReadOnly = false;
				colvarReceivableId.DefaultSetting = @"";
				colvarReceivableId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceivableId);
				
				TableSchema.TableColumn colvarSender = new TableSchema.TableColumn(schema);
				colvarSender.ColumnName = "sender";
				colvarSender.DataType = DbType.AnsiString;
				colvarSender.MaxLength = 150;
				colvarSender.AutoIncrement = false;
				colvarSender.IsNullable = true;
				colvarSender.IsPrimaryKey = false;
				colvarSender.IsForeignKey = false;
				colvarSender.IsReadOnly = false;
				colvarSender.DefaultSetting = @"";
				colvarSender.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSender);
				
				TableSchema.TableColumn colvarSendDate = new TableSchema.TableColumn(schema);
				colvarSendDate.ColumnName = "send_date";
				colvarSendDate.DataType = DbType.DateTime;
				colvarSendDate.MaxLength = 0;
				colvarSendDate.AutoIncrement = false;
				colvarSendDate.IsNullable = true;
				colvarSendDate.IsPrimaryKey = false;
				colvarSendDate.IsForeignKey = false;
				colvarSendDate.IsReadOnly = false;
				colvarSendDate.DefaultSetting = @"";
				colvarSendDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendDate);
				
				TableSchema.TableColumn colvarUseId = new TableSchema.TableColumn(schema);
				colvarUseId.ColumnName = "use_id";
				colvarUseId.DataType = DbType.Int32;
				colvarUseId.MaxLength = 0;
				colvarUseId.AutoIncrement = false;
				colvarUseId.IsNullable = true;
				colvarUseId.IsPrimaryKey = false;
				colvarUseId.IsForeignKey = true;
				colvarUseId.IsReadOnly = false;
				colvarUseId.DefaultSetting = @"";
				
					colvarUseId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUseId);
				
				TableSchema.TableColumn colvarOwner = new TableSchema.TableColumn(schema);
				colvarOwner.ColumnName = "owner";
				colvarOwner.DataType = DbType.Int32;
				colvarOwner.MaxLength = 0;
				colvarOwner.AutoIncrement = false;
				colvarOwner.IsNullable = true;
				colvarOwner.IsPrimaryKey = false;
				colvarOwner.IsForeignKey = true;
				colvarOwner.IsReadOnly = false;
				colvarOwner.DefaultSetting = @"";
				
					colvarOwner.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOwner);
				
				TableSchema.TableColumn colvarNickName = new TableSchema.TableColumn(schema);
				colvarNickName.ColumnName = "nick_name";
				colvarNickName.DataType = DbType.String;
				colvarNickName.MaxLength = 100;
				colvarNickName.AutoIncrement = false;
				colvarNickName.IsNullable = true;
				colvarNickName.IsPrimaryKey = false;
				colvarNickName.IsForeignKey = false;
				colvarNickName.IsReadOnly = false;
				colvarNickName.DefaultSetting = @"";
				colvarNickName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNickName);
				
				TableSchema.TableColumn colvarReferrerId = new TableSchema.TableColumn(schema);
				colvarReferrerId.ColumnName = "referrer_id";
				colvarReferrerId.DataType = DbType.Int32;
				colvarReferrerId.MaxLength = 0;
				colvarReferrerId.AutoIncrement = false;
				colvarReferrerId.IsNullable = true;
				colvarReferrerId.IsPrimaryKey = false;
				colvarReferrerId.IsForeignKey = false;
				colvarReferrerId.IsReadOnly = false;
				colvarReferrerId.DefaultSetting = @"";
				colvarReferrerId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferrerId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("discount_code",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CampaignId")]
		[Bindable(true)]
		public int? CampaignId 
		{
			get { return GetColumnValue<int?>(Columns.CampaignId); }
			set { SetColumnValue(Columns.CampaignId, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public string Code 
		{
			get { return GetColumnValue<string>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("Amount")]
		[Bindable(true)]
		public decimal? Amount 
		{
			get { return GetColumnValue<decimal?>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}
		  
		[XmlAttribute("UseTime")]
		[Bindable(true)]
		public DateTime? UseTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UseTime); }
			set { SetColumnValue(Columns.UseTime, value); }
		}
		  
		[XmlAttribute("UseAmount")]
		[Bindable(true)]
		public decimal? UseAmount 
		{
			get { return GetColumnValue<decimal?>(Columns.UseAmount); }
			set { SetColumnValue(Columns.UseAmount, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("OrderAmount")]
		[Bindable(true)]
		public decimal? OrderAmount 
		{
			get { return GetColumnValue<decimal?>(Columns.OrderAmount); }
			set { SetColumnValue(Columns.OrderAmount, value); }
		}
		  
		[XmlAttribute("CancelTime")]
		[Bindable(true)]
		public DateTime? CancelTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CancelTime); }
			set { SetColumnValue(Columns.CancelTime, value); }
		}
		  
		[XmlAttribute("OrderCost")]
		[Bindable(true)]
		public decimal? OrderCost 
		{
			get { return GetColumnValue<decimal?>(Columns.OrderCost); }
			set { SetColumnValue(Columns.OrderCost, value); }
		}
		  
		[XmlAttribute("OrderClassification")]
		[Bindable(true)]
		public int? OrderClassification 
		{
			get { return GetColumnValue<int?>(Columns.OrderClassification); }
			set { SetColumnValue(Columns.OrderClassification, value); }
		}
		  
		[XmlAttribute("ReceivableId")]
		[Bindable(true)]
		public int? ReceivableId 
		{
			get { return GetColumnValue<int?>(Columns.ReceivableId); }
			set { SetColumnValue(Columns.ReceivableId, value); }
		}
		  
		[XmlAttribute("Sender")]
		[Bindable(true)]
		public string Sender 
		{
			get { return GetColumnValue<string>(Columns.Sender); }
			set { SetColumnValue(Columns.Sender, value); }
		}
		  
		[XmlAttribute("SendDate")]
		[Bindable(true)]
		public DateTime? SendDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.SendDate); }
			set { SetColumnValue(Columns.SendDate, value); }
		}
		  
		[XmlAttribute("UseId")]
		[Bindable(true)]
		public int? UseId 
		{
			get { return GetColumnValue<int?>(Columns.UseId); }
			set { SetColumnValue(Columns.UseId, value); }
		}
		  
		[XmlAttribute("Owner")]
		[Bindable(true)]
		public int? Owner 
		{
			get { return GetColumnValue<int?>(Columns.Owner); }
			set { SetColumnValue(Columns.Owner, value); }
		}
		  
		[XmlAttribute("NickName")]
		[Bindable(true)]
		public string NickName 
		{
			get { return GetColumnValue<string>(Columns.NickName); }
			set { SetColumnValue(Columns.NickName, value); }
		}
		  
		[XmlAttribute("ReferrerId")]
		[Bindable(true)]
		public int? ReferrerId 
		{
			get { return GetColumnValue<int?>(Columns.ReferrerId); }
			set { SetColumnValue(Columns.ReferrerId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CampaignIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn UseTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn UseAmountColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderAmountColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderCostColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderClassificationColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceivableIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn SenderColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn SendDateColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn UseIdColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn OwnerColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn NickNameColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferrerIdColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CampaignId = @"campaign_id";
			 public static string Code = @"code";
			 public static string Amount = @"amount";
			 public static string UseTime = @"use_time";
			 public static string UseAmount = @"use_amount";
			 public static string OrderGuid = @"order_guid";
			 public static string OrderAmount = @"order_amount";
			 public static string CancelTime = @"cancel_time";
			 public static string OrderCost = @"order_cost";
			 public static string OrderClassification = @"order_classification";
			 public static string ReceivableId = @"receivable_id";
			 public static string Sender = @"sender";
			 public static string SendDate = @"send_date";
			 public static string UseId = @"use_id";
			 public static string Owner = @"owner";
			 public static string NickName = @"nick_name";
			 public static string ReferrerId = @"referrer_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

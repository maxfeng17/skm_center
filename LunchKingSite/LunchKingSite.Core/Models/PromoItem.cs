using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PromoItem class.
	/// </summary>
    [Serializable]
	public partial class PromoItemCollection : RepositoryList<PromoItem, PromoItemCollection>
	{	   
		public PromoItemCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PromoItemCollection</returns>
		public PromoItemCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PromoItem o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the promo_item table.
	/// </summary>
	[Serializable]
	public partial class PromoItem : RepositoryRecord<PromoItem>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PromoItem()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PromoItem(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("promo_item", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "code";
				colvarCode.DataType = DbType.AnsiString;
				colvarCode.MaxLength = 50;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = true;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarEventActivityId = new TableSchema.TableColumn(schema);
				colvarEventActivityId.ColumnName = "event_activity_id";
				colvarEventActivityId.DataType = DbType.Int32;
				colvarEventActivityId.MaxLength = 0;
				colvarEventActivityId.AutoIncrement = false;
				colvarEventActivityId.IsNullable = true;
				colvarEventActivityId.IsPrimaryKey = false;
				colvarEventActivityId.IsForeignKey = false;
				colvarEventActivityId.IsReadOnly = false;
				colvarEventActivityId.DefaultSetting = @"";
				colvarEventActivityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventActivityId);
				
				TableSchema.TableColumn colvarIsUsed = new TableSchema.TableColumn(schema);
				colvarIsUsed.ColumnName = "is_used";
				colvarIsUsed.DataType = DbType.Boolean;
				colvarIsUsed.MaxLength = 0;
				colvarIsUsed.AutoIncrement = false;
				colvarIsUsed.IsNullable = false;
				colvarIsUsed.IsPrimaryKey = false;
				colvarIsUsed.IsForeignKey = false;
				colvarIsUsed.IsReadOnly = false;
				
						colvarIsUsed.DefaultSetting = @"((0))";
				colvarIsUsed.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsUsed);
				
				TableSchema.TableColumn colvarUseTime = new TableSchema.TableColumn(schema);
				colvarUseTime.ColumnName = "use_time";
				colvarUseTime.DataType = DbType.DateTime;
				colvarUseTime.MaxLength = 0;
				colvarUseTime.AutoIncrement = false;
				colvarUseTime.IsNullable = true;
				colvarUseTime.IsPrimaryKey = false;
				colvarUseTime.IsForeignKey = false;
				colvarUseTime.IsReadOnly = false;
				colvarUseTime.DefaultSetting = @"";
				colvarUseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUseTime);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 400;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = true;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = true;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = true;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				
					colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("promo_item",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public string Code 
		{
			get { return GetColumnValue<string>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("EventActivityId")]
		[Bindable(true)]
		public int? EventActivityId 
		{
			get { return GetColumnValue<int?>(Columns.EventActivityId); }
			set { SetColumnValue(Columns.EventActivityId, value); }
		}
		  
		[XmlAttribute("IsUsed")]
		[Bindable(true)]
		public bool IsUsed 
		{
			get { return GetColumnValue<bool>(Columns.IsUsed); }
			set { SetColumnValue(Columns.IsUsed, value); }
		}
		  
		[XmlAttribute("UseTime")]
		[Bindable(true)]
		public DateTime? UseTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UseTime); }
			set { SetColumnValue(Columns.UseTime, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int? UserId 
		{
			get { return GetColumnValue<int?>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn EventActivityIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IsUsedColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn UseTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Code = @"code";
			 public static string EventActivityId = @"event_activity_id";
			 public static string IsUsed = @"is_used";
			 public static string UseTime = @"use_time";
			 public static string Memo = @"memo";
			 public static string UserId = @"user_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

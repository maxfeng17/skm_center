using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewIspMakeOrderInfo class.
    /// </summary>
    [Serializable]
    public partial class ViewIspMakeOrderInfoCollection : ReadOnlyList<ViewIspMakeOrderInfo, ViewIspMakeOrderInfoCollection>
    {        
        public ViewIspMakeOrderInfoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_isp_make_order_info view.
    /// </summary>
    [Serializable]
    public partial class ViewIspMakeOrderInfo : ReadOnlyRecord<ViewIspMakeOrderInfo>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_isp_make_order_info", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderKey = new TableSchema.TableColumn(schema);
                colvarOrderKey.ColumnName = "order_key";
                colvarOrderKey.DataType = DbType.AnsiString;
                colvarOrderKey.MaxLength = 30;
                colvarOrderKey.AutoIncrement = false;
                colvarOrderKey.IsNullable = true;
                colvarOrderKey.IsPrimaryKey = false;
                colvarOrderKey.IsForeignKey = false;
                colvarOrderKey.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderKey);
                
                TableSchema.TableColumn colvarStoreKey = new TableSchema.TableColumn(schema);
                colvarStoreKey.ColumnName = "store_key";
                colvarStoreKey.DataType = DbType.Guid;
                colvarStoreKey.MaxLength = 0;
                colvarStoreKey.AutoIncrement = false;
                colvarStoreKey.IsNullable = false;
                colvarStoreKey.IsPrimaryKey = false;
                colvarStoreKey.IsForeignKey = false;
                colvarStoreKey.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreKey);
                
                TableSchema.TableColumn colvarServiceChannel = new TableSchema.TableColumn(schema);
                colvarServiceChannel.ColumnName = "service_channel";
                colvarServiceChannel.DataType = DbType.Int32;
                colvarServiceChannel.MaxLength = 0;
                colvarServiceChannel.AutoIncrement = false;
                colvarServiceChannel.IsNullable = false;
                colvarServiceChannel.IsPrimaryKey = false;
                colvarServiceChannel.IsForeignKey = false;
                colvarServiceChannel.IsReadOnly = false;
                
                schema.Columns.Add(colvarServiceChannel);
                
                TableSchema.TableColumn colvarIsApplicationServerConnected = new TableSchema.TableColumn(schema);
                colvarIsApplicationServerConnected.ColumnName = "is_application_server_connected";
                colvarIsApplicationServerConnected.DataType = DbType.Int32;
                colvarIsApplicationServerConnected.MaxLength = 0;
                colvarIsApplicationServerConnected.AutoIncrement = false;
                colvarIsApplicationServerConnected.IsNullable = false;
                colvarIsApplicationServerConnected.IsPrimaryKey = false;
                colvarIsApplicationServerConnected.IsForeignKey = false;
                colvarIsApplicationServerConnected.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsApplicationServerConnected);
                
                TableSchema.TableColumn colvarChannelStoreCode = new TableSchema.TableColumn(schema);
                colvarChannelStoreCode.ColumnName = "channel_store_code";
                colvarChannelStoreCode.DataType = DbType.String;
                colvarChannelStoreCode.MaxLength = 10;
                colvarChannelStoreCode.AutoIncrement = false;
                colvarChannelStoreCode.IsNullable = true;
                colvarChannelStoreCode.IsPrimaryKey = false;
                colvarChannelStoreCode.IsForeignKey = false;
                colvarChannelStoreCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarChannelStoreCode);
                
                TableSchema.TableColumn colvarSenderName = new TableSchema.TableColumn(schema);
                colvarSenderName.ColumnName = "sender_name";
                colvarSenderName.DataType = DbType.String;
                colvarSenderName.MaxLength = 100;
                colvarSenderName.AutoIncrement = false;
                colvarSenderName.IsNullable = true;
                colvarSenderName.IsPrimaryKey = false;
                colvarSenderName.IsForeignKey = false;
                colvarSenderName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSenderName);
                
                TableSchema.TableColumn colvarReceiverEmail = new TableSchema.TableColumn(schema);
                colvarReceiverEmail.ColumnName = "receiver_email";
                colvarReceiverEmail.DataType = DbType.String;
                colvarReceiverEmail.MaxLength = 256;
                colvarReceiverEmail.AutoIncrement = false;
                colvarReceiverEmail.IsNullable = false;
                colvarReceiverEmail.IsPrimaryKey = false;
                colvarReceiverEmail.IsForeignKey = false;
                colvarReceiverEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverEmail);
                
                TableSchema.TableColumn colvarReceiverName = new TableSchema.TableColumn(schema);
                colvarReceiverName.ColumnName = "receiver_name";
                colvarReceiverName.DataType = DbType.String;
                colvarReceiverName.MaxLength = 50;
                colvarReceiverName.AutoIncrement = false;
                colvarReceiverName.IsNullable = false;
                colvarReceiverName.IsPrimaryKey = false;
                colvarReceiverName.IsForeignKey = false;
                colvarReceiverName.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverName);
                
                TableSchema.TableColumn colvarReceiverMPhone = new TableSchema.TableColumn(schema);
                colvarReceiverMPhone.ColumnName = "receiver_m_phone";
                colvarReceiverMPhone.DataType = DbType.AnsiString;
                colvarReceiverMPhone.MaxLength = 50;
                colvarReceiverMPhone.AutoIncrement = false;
                colvarReceiverMPhone.IsNullable = false;
                colvarReceiverMPhone.IsPrimaryKey = false;
                colvarReceiverMPhone.IsForeignKey = false;
                colvarReceiverMPhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverMPhone);
                
                TableSchema.TableColumn colvarIsCashOnPickup = new TableSchema.TableColumn(schema);
                colvarIsCashOnPickup.ColumnName = "is_cash_on_pickup";
                colvarIsCashOnPickup.DataType = DbType.Int32;
                colvarIsCashOnPickup.MaxLength = 0;
                colvarIsCashOnPickup.AutoIncrement = false;
                colvarIsCashOnPickup.IsNullable = false;
                colvarIsCashOnPickup.IsPrimaryKey = false;
                colvarIsCashOnPickup.IsForeignKey = false;
                colvarIsCashOnPickup.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCashOnPickup);
                
                TableSchema.TableColumn colvarPickupAmount = new TableSchema.TableColumn(schema);
                colvarPickupAmount.ColumnName = "pickup_amount";
                colvarPickupAmount.DataType = DbType.Int32;
                colvarPickupAmount.MaxLength = 0;
                colvarPickupAmount.AutoIncrement = false;
                colvarPickupAmount.IsNullable = true;
                colvarPickupAmount.IsPrimaryKey = false;
                colvarPickupAmount.IsForeignKey = false;
                colvarPickupAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarPickupAmount);
                
                TableSchema.TableColumn colvarProductAmount = new TableSchema.TableColumn(schema);
                colvarProductAmount.ColumnName = "product_amount";
                colvarProductAmount.DataType = DbType.Currency;
                colvarProductAmount.MaxLength = 0;
                colvarProductAmount.AutoIncrement = false;
                colvarProductAmount.IsNullable = false;
                colvarProductAmount.IsPrimaryKey = false;
                colvarProductAmount.IsForeignKey = false;
                colvarProductAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductAmount);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_isp_make_order_info",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewIspMakeOrderInfo()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewIspMakeOrderInfo(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewIspMakeOrderInfo(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewIspMakeOrderInfo(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderKey")]
        [Bindable(true)]
        public string OrderKey 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_key");
		    }
            set 
		    {
			    SetColumnValue("order_key", value);
            }
        }
	      
        [XmlAttribute("StoreKey")]
        [Bindable(true)]
        public Guid StoreKey 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("store_key");
		    }
            set 
		    {
			    SetColumnValue("store_key", value);
            }
        }
	      
        [XmlAttribute("ServiceChannel")]
        [Bindable(true)]
        public int ServiceChannel 
	    {
		    get
		    {
			    return GetColumnValue<int>("service_channel");
		    }
            set 
		    {
			    SetColumnValue("service_channel", value);
            }
        }
	      
        [XmlAttribute("IsApplicationServerConnected")]
        [Bindable(true)]
        public int IsApplicationServerConnected 
	    {
		    get
		    {
			    return GetColumnValue<int>("is_application_server_connected");
		    }
            set 
		    {
			    SetColumnValue("is_application_server_connected", value);
            }
        }
	      
        [XmlAttribute("ChannelStoreCode")]
        [Bindable(true)]
        public string ChannelStoreCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("channel_store_code");
		    }
            set 
		    {
			    SetColumnValue("channel_store_code", value);
            }
        }
	      
        [XmlAttribute("SenderName")]
        [Bindable(true)]
        public string SenderName 
	    {
		    get
		    {
			    return GetColumnValue<string>("sender_name");
		    }
            set 
		    {
			    SetColumnValue("sender_name", value);
            }
        }
	      
        [XmlAttribute("ReceiverEmail")]
        [Bindable(true)]
        public string ReceiverEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_email");
		    }
            set 
		    {
			    SetColumnValue("receiver_email", value);
            }
        }
	      
        [XmlAttribute("ReceiverName")]
        [Bindable(true)]
        public string ReceiverName 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_name");
		    }
            set 
		    {
			    SetColumnValue("receiver_name", value);
            }
        }
	      
        [XmlAttribute("ReceiverMPhone")]
        [Bindable(true)]
        public string ReceiverMPhone 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_m_phone");
		    }
            set 
		    {
			    SetColumnValue("receiver_m_phone", value);
            }
        }
	      
        [XmlAttribute("IsCashOnPickup")]
        [Bindable(true)]
        public int IsCashOnPickup 
	    {
		    get
		    {
			    return GetColumnValue<int>("is_cash_on_pickup");
		    }
            set 
		    {
			    SetColumnValue("is_cash_on_pickup", value);
            }
        }
	      
        [XmlAttribute("PickupAmount")]
        [Bindable(true)]
        public int? PickupAmount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("pickup_amount");
		    }
            set 
		    {
			    SetColumnValue("pickup_amount", value);
            }
        }
	      
        [XmlAttribute("ProductAmount")]
        [Bindable(true)]
        public decimal ProductAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("product_amount");
		    }
            set 
		    {
			    SetColumnValue("product_amount", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderGuid = @"order_guid";
            
            public static string OrderKey = @"order_key";
            
            public static string StoreKey = @"store_key";
            
            public static string ServiceChannel = @"service_channel";
            
            public static string IsApplicationServerConnected = @"is_application_server_connected";
            
            public static string ChannelStoreCode = @"channel_store_code";
            
            public static string SenderName = @"sender_name";
            
            public static string ReceiverEmail = @"receiver_email";
            
            public static string ReceiverName = @"receiver_name";
            
            public static string ReceiverMPhone = @"receiver_m_phone";
            
            public static string IsCashOnPickup = @"is_cash_on_pickup";
            
            public static string PickupAmount = @"pickup_amount";
            
            public static string ProductAmount = @"product_amount";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

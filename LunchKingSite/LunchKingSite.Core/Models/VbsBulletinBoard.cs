using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VbsBulletinBoard class.
	/// </summary>
    [Serializable]
	public partial class VbsBulletinBoardCollection : RepositoryList<VbsBulletinBoard, VbsBulletinBoardCollection>
	{	   
		public VbsBulletinBoardCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VbsBulletinBoardCollection</returns>
		public VbsBulletinBoardCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VbsBulletinBoard o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the vbs_bulletin_board table.
	/// </summary>
	[Serializable]
	public partial class VbsBulletinBoard : RepositoryRecord<VbsBulletinBoard>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VbsBulletinBoard()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VbsBulletinBoard(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("vbs_bulletin_board", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarPublishSellerType = new TableSchema.TableColumn(schema);
				colvarPublishSellerType.ColumnName = "publish_seller_type";
				colvarPublishSellerType.DataType = DbType.Int32;
				colvarPublishSellerType.MaxLength = 0;
				colvarPublishSellerType.AutoIncrement = false;
				colvarPublishSellerType.IsNullable = false;
				colvarPublishSellerType.IsPrimaryKey = false;
				colvarPublishSellerType.IsForeignKey = false;
				colvarPublishSellerType.IsReadOnly = false;
				colvarPublishSellerType.DefaultSetting = @"";
				colvarPublishSellerType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPublishSellerType);
				
				TableSchema.TableColumn colvarPublishContent = new TableSchema.TableColumn(schema);
				colvarPublishContent.ColumnName = "publish_content";
				colvarPublishContent.DataType = DbType.String;
				colvarPublishContent.MaxLength = -1;
				colvarPublishContent.AutoIncrement = false;
				colvarPublishContent.IsNullable = false;
				colvarPublishContent.IsPrimaryKey = false;
				colvarPublishContent.IsForeignKey = false;
				colvarPublishContent.IsReadOnly = false;
				colvarPublishContent.DefaultSetting = @"";
				colvarPublishContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPublishContent);
				
				TableSchema.TableColumn colvarIsSticky = new TableSchema.TableColumn(schema);
				colvarIsSticky.ColumnName = "is_sticky";
				colvarIsSticky.DataType = DbType.Boolean;
				colvarIsSticky.MaxLength = 0;
				colvarIsSticky.AutoIncrement = false;
				colvarIsSticky.IsNullable = false;
				colvarIsSticky.IsPrimaryKey = false;
				colvarIsSticky.IsForeignKey = false;
				colvarIsSticky.IsReadOnly = false;
				
						colvarIsSticky.DefaultSetting = @"((0))";
				colvarIsSticky.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSticky);
				
				TableSchema.TableColumn colvarPublishStatus = new TableSchema.TableColumn(schema);
				colvarPublishStatus.ColumnName = "publish_status";
				colvarPublishStatus.DataType = DbType.Int32;
				colvarPublishStatus.MaxLength = 0;
				colvarPublishStatus.AutoIncrement = false;
				colvarPublishStatus.IsNullable = false;
				colvarPublishStatus.IsPrimaryKey = false;
				colvarPublishStatus.IsForeignKey = false;
				colvarPublishStatus.IsReadOnly = false;
				
						colvarPublishStatus.DefaultSetting = @"((1))";
				colvarPublishStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPublishStatus);
				
				TableSchema.TableColumn colvarPublishTime = new TableSchema.TableColumn(schema);
				colvarPublishTime.ColumnName = "publish_time";
				colvarPublishTime.DataType = DbType.DateTime;
				colvarPublishTime.MaxLength = 0;
				colvarPublishTime.AutoIncrement = false;
				colvarPublishTime.IsNullable = false;
				colvarPublishTime.IsPrimaryKey = false;
				colvarPublishTime.IsForeignKey = false;
				colvarPublishTime.IsReadOnly = false;
				colvarPublishTime.DefaultSetting = @"";
				colvarPublishTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPublishTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.Int32;
				colvarModifyId.MaxLength = 0;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarLinkTitle = new TableSchema.TableColumn(schema);
				colvarLinkTitle.ColumnName = "link_title";
				colvarLinkTitle.DataType = DbType.String;
				colvarLinkTitle.MaxLength = 200;
				colvarLinkTitle.AutoIncrement = false;
				colvarLinkTitle.IsNullable = true;
				colvarLinkTitle.IsPrimaryKey = false;
				colvarLinkTitle.IsForeignKey = false;
				colvarLinkTitle.IsReadOnly = false;
				colvarLinkTitle.DefaultSetting = @"";
				colvarLinkTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLinkTitle);
				
				TableSchema.TableColumn colvarLinkUrl = new TableSchema.TableColumn(schema);
				colvarLinkUrl.ColumnName = "link_url";
				colvarLinkUrl.DataType = DbType.String;
				colvarLinkUrl.MaxLength = 200;
				colvarLinkUrl.AutoIncrement = false;
				colvarLinkUrl.IsNullable = true;
				colvarLinkUrl.IsPrimaryKey = false;
				colvarLinkUrl.IsForeignKey = false;
				colvarLinkUrl.IsReadOnly = false;
				colvarLinkUrl.DefaultSetting = @"";
				colvarLinkUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLinkUrl);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("vbs_bulletin_board",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("PublishSellerType")]
		[Bindable(true)]
		public int PublishSellerType 
		{
			get { return GetColumnValue<int>(Columns.PublishSellerType); }
			set { SetColumnValue(Columns.PublishSellerType, value); }
		}
		  
		[XmlAttribute("PublishContent")]
		[Bindable(true)]
		public string PublishContent 
		{
			get { return GetColumnValue<string>(Columns.PublishContent); }
			set { SetColumnValue(Columns.PublishContent, value); }
		}
		  
		[XmlAttribute("IsSticky")]
		[Bindable(true)]
		public bool IsSticky 
		{
			get { return GetColumnValue<bool>(Columns.IsSticky); }
			set { SetColumnValue(Columns.IsSticky, value); }
		}
		  
		[XmlAttribute("PublishStatus")]
		[Bindable(true)]
		public int PublishStatus 
		{
			get { return GetColumnValue<int>(Columns.PublishStatus); }
			set { SetColumnValue(Columns.PublishStatus, value); }
		}
		  
		[XmlAttribute("PublishTime")]
		[Bindable(true)]
		public DateTime PublishTime 
		{
			get { return GetColumnValue<DateTime>(Columns.PublishTime); }
			set { SetColumnValue(Columns.PublishTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public int? ModifyId 
		{
			get { return GetColumnValue<int?>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("LinkTitle")]
		[Bindable(true)]
		public string LinkTitle 
		{
			get { return GetColumnValue<string>(Columns.LinkTitle); }
			set { SetColumnValue(Columns.LinkTitle, value); }
		}
		  
		[XmlAttribute("LinkUrl")]
		[Bindable(true)]
		public string LinkUrl 
		{
			get { return GetColumnValue<string>(Columns.LinkUrl); }
			set { SetColumnValue(Columns.LinkUrl, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PublishSellerTypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PublishContentColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IsStickyColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PublishStatusColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PublishTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn LinkTitleColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn LinkUrlColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string PublishSellerType = @"publish_seller_type";
			 public static string PublishContent = @"publish_content";
			 public static string IsSticky = @"is_sticky";
			 public static string PublishStatus = @"publish_status";
			 public static string PublishTime = @"publish_time";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string LinkTitle = @"link_title";
			 public static string LinkUrl = @"link_url";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

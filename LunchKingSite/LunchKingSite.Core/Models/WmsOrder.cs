using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class WmsOrderCollection : RepositoryList<WmsOrder, WmsOrderCollection>
	{
			public WmsOrderCollection() {}

			public WmsOrderCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							WmsOrder o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class WmsOrder : RepositoryRecord<WmsOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		public WmsOrder()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public WmsOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("wms_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);

				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 2147483647;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarSyncTime = new TableSchema.TableColumn(schema);
				colvarSyncTime.ColumnName = "sync_time";
				colvarSyncTime.DataType = DbType.DateTime;
				colvarSyncTime.MaxLength = 0;
				colvarSyncTime.AutoIncrement = false;
				colvarSyncTime.IsNullable = true;
				colvarSyncTime.IsPrimaryKey = false;
				colvarSyncTime.IsForeignKey = false;
				colvarSyncTime.IsReadOnly = false;
				colvarSyncTime.DefaultSetting = @"";
				colvarSyncTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSyncTime);

				TableSchema.TableColumn colvarWmsOrderId = new TableSchema.TableColumn(schema);
				colvarWmsOrderId.ColumnName = "wms_order_id";
				colvarWmsOrderId.DataType = DbType.String;
				colvarWmsOrderId.MaxLength = 20;
				colvarWmsOrderId.AutoIncrement = false;
				colvarWmsOrderId.IsNullable = true;
				colvarWmsOrderId.IsPrimaryKey = false;
				colvarWmsOrderId.IsForeignKey = false;
				colvarWmsOrderId.IsReadOnly = false;
				colvarWmsOrderId.DefaultSetting = @"";
				colvarWmsOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWmsOrderId);

				TableSchema.TableColumn colvarShipStatus = new TableSchema.TableColumn(schema);
				colvarShipStatus.ColumnName = "ship_status";
				colvarShipStatus.DataType = DbType.Int32;
				colvarShipStatus.MaxLength = 0;
				colvarShipStatus.AutoIncrement = false;
				colvarShipStatus.IsNullable = false;
				colvarShipStatus.IsPrimaryKey = false;
				colvarShipStatus.IsForeignKey = false;
				colvarShipStatus.IsReadOnly = false;
				colvarShipStatus.DefaultSetting = @"((0))";
				colvarShipStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipStatus);

				TableSchema.TableColumn colvarShipDate = new TableSchema.TableColumn(schema);
				colvarShipDate.ColumnName = "ship_date";
				colvarShipDate.DataType = DbType.DateTime;
				colvarShipDate.MaxLength = 0;
				colvarShipDate.AutoIncrement = false;
				colvarShipDate.IsNullable = true;
				colvarShipDate.IsPrimaryKey = false;
				colvarShipDate.IsForeignKey = false;
				colvarShipDate.IsReadOnly = false;
				colvarShipDate.DefaultSetting = @"";
				colvarShipDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipDate);

				TableSchema.TableColumn colvarSendNotification = new TableSchema.TableColumn(schema);
				colvarSendNotification.ColumnName = "send_notification";
				colvarSendNotification.DataType = DbType.Boolean;
				colvarSendNotification.MaxLength = 0;
				colvarSendNotification.AutoIncrement = false;
				colvarSendNotification.IsNullable = false;
				colvarSendNotification.IsPrimaryKey = false;
				colvarSendNotification.IsForeignKey = false;
				colvarSendNotification.IsReadOnly = false;
				colvarSendNotification.DefaultSetting = @"((0))";
				colvarSendNotification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendNotification);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("wms_order",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}

		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("SyncTime")]
		[Bindable(true)]
		public DateTime? SyncTime
		{
			get { return GetColumnValue<DateTime?>(Columns.SyncTime); }
			set { SetColumnValue(Columns.SyncTime, value); }
		}

		[XmlAttribute("WmsOrderId")]
		[Bindable(true)]
		public string WmsOrderId
		{
			get { return GetColumnValue<string>(Columns.WmsOrderId); }
			set { SetColumnValue(Columns.WmsOrderId, value); }
		}

		[XmlAttribute("ShipStatus")]
		[Bindable(true)]
		public int ShipStatus
		{
			get { return GetColumnValue<int>(Columns.ShipStatus); }
			set { SetColumnValue(Columns.ShipStatus, value); }
		}

		[XmlAttribute("ShipDate")]
		[Bindable(true)]
		public DateTime? ShipDate
		{
			get { return GetColumnValue<DateTime?>(Columns.ShipDate); }
			set { SetColumnValue(Columns.ShipDate, value); }
		}

		[XmlAttribute("SendNotification")]
		[Bindable(true)]
		public bool SendNotification
		{
			get { return GetColumnValue<bool>(Columns.SendNotification); }
			set { SetColumnValue(Columns.SendNotification, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn StatusColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn MessageColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn SyncTimeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn WmsOrderIdColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn ShipStatusColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn ShipDateColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn SendNotificationColumn
		{
			get { return Schema.Columns[9]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string OrderGuid = @"order_guid";
			public static string Status = @"status";
			public static string Message = @"message";
			public static string CreateTime = @"create_time";
			public static string SyncTime = @"sync_time";
			public static string WmsOrderId = @"wms_order_id";
			public static string ShipStatus = @"ship_status";
			public static string ShipDate = @"ship_date";
			public static string SendNotification = @"send_notification";
		}

		#endregion

	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DiscountUseType class.
	/// </summary>
    [Serializable]
	public partial class DiscountUseTypeCollection : RepositoryList<DiscountUseType, DiscountUseTypeCollection>
	{	   
		public DiscountUseTypeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DiscountUseTypeCollection</returns>
		public DiscountUseTypeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DiscountUseType o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the discount_use_type table.
	/// </summary>
	[Serializable]
	public partial class DiscountUseType : RepositoryRecord<DiscountUseType>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DiscountUseType()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DiscountUseType(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("discount_use_type", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = true;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarUseType = new TableSchema.TableColumn(schema);
				colvarUseType.ColumnName = "use_type";
				colvarUseType.DataType = DbType.Boolean;
				colvarUseType.MaxLength = 0;
				colvarUseType.AutoIncrement = false;
				colvarUseType.IsNullable = false;
				colvarUseType.IsPrimaryKey = false;
				colvarUseType.IsForeignKey = false;
				colvarUseType.IsReadOnly = false;
				colvarUseType.DefaultSetting = @"";
				colvarUseType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUseType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("discount_use_type",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("UseType")]
		[Bindable(true)]
		public bool UseType 
		{
			get { return GetColumnValue<bool>(Columns.UseType); }
			set { SetColumnValue(Columns.UseType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UseTypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Bid = @"bid";
			 public static string UseType = @"use_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

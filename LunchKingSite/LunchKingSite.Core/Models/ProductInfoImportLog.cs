using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ProductInfoImportLog class.
	/// </summary>
    [Serializable]
	public partial class ProductInfoImportLogCollection : RepositoryList<ProductInfoImportLog, ProductInfoImportLogCollection>
	{	   
		public ProductInfoImportLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProductInfoImportLogCollection</returns>
		public ProductInfoImportLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProductInfoImportLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the product_info_import_log table.
	/// </summary>
	[Serializable]
	public partial class ProductInfoImportLog : RepositoryRecord<ProductInfoImportLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ProductInfoImportLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ProductInfoImportLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("product_info_import_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
				colvarGroupId.ColumnName = "group_id";
				colvarGroupId.DataType = DbType.String;
				colvarGroupId.MaxLength = 200;
				colvarGroupId.AutoIncrement = false;
				colvarGroupId.IsNullable = true;
				colvarGroupId.IsPrimaryKey = false;
				colvarGroupId.IsForeignKey = false;
				colvarGroupId.IsReadOnly = false;
				colvarGroupId.DefaultSetting = @"";
				colvarGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupId);
				
				TableSchema.TableColumn colvarSpecType = new TableSchema.TableColumn(schema);
				colvarSpecType.ColumnName = "spec_type";
				colvarSpecType.DataType = DbType.String;
				colvarSpecType.MaxLength = 200;
				colvarSpecType.AutoIncrement = false;
				colvarSpecType.IsNullable = true;
				colvarSpecType.IsPrimaryKey = false;
				colvarSpecType.IsForeignKey = false;
				colvarSpecType.IsReadOnly = false;
				colvarSpecType.DefaultSetting = @"";
				colvarSpecType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecType);
				
				TableSchema.TableColumn colvarProductBrandName = new TableSchema.TableColumn(schema);
				colvarProductBrandName.ColumnName = "product_brand_name";
				colvarProductBrandName.DataType = DbType.String;
				colvarProductBrandName.MaxLength = 200;
				colvarProductBrandName.AutoIncrement = false;
				colvarProductBrandName.IsNullable = true;
				colvarProductBrandName.IsPrimaryKey = false;
				colvarProductBrandName.IsForeignKey = false;
				colvarProductBrandName.IsReadOnly = false;
				colvarProductBrandName.DefaultSetting = @"";
				colvarProductBrandName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductBrandName);
				
				TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
				colvarProductName.ColumnName = "product_name";
				colvarProductName.DataType = DbType.String;
				colvarProductName.MaxLength = 200;
				colvarProductName.AutoIncrement = false;
				colvarProductName.IsNullable = true;
				colvarProductName.IsPrimaryKey = false;
				colvarProductName.IsForeignKey = false;
				colvarProductName.IsReadOnly = false;
				colvarProductName.DefaultSetting = @"";
				colvarProductName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductName);
				
				TableSchema.TableColumn colvarCatgName = new TableSchema.TableColumn(schema);
				colvarCatgName.ColumnName = "catg_name";
				colvarCatgName.DataType = DbType.String;
				colvarCatgName.MaxLength = 200;
				colvarCatgName.AutoIncrement = false;
				colvarCatgName.IsNullable = true;
				colvarCatgName.IsPrimaryKey = false;
				colvarCatgName.IsForeignKey = false;
				colvarCatgName.IsReadOnly = false;
				colvarCatgName.DefaultSetting = @"";
				colvarCatgName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCatgName);
				
				TableSchema.TableColumn colvarSpecName = new TableSchema.TableColumn(schema);
				colvarSpecName.ColumnName = "spec_name";
				colvarSpecName.DataType = DbType.String;
				colvarSpecName.MaxLength = 200;
				colvarSpecName.AutoIncrement = false;
				colvarSpecName.IsNullable = true;
				colvarSpecName.IsPrimaryKey = false;
				colvarSpecName.IsForeignKey = false;
				colvarSpecName.IsReadOnly = false;
				colvarSpecName.DefaultSetting = @"";
				colvarSpecName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecName);
				
				TableSchema.TableColumn colvarGtins = new TableSchema.TableColumn(schema);
				colvarGtins.ColumnName = "gtins";
				colvarGtins.DataType = DbType.String;
				colvarGtins.MaxLength = 200;
				colvarGtins.AutoIncrement = false;
				colvarGtins.IsNullable = true;
				colvarGtins.IsPrimaryKey = false;
				colvarGtins.IsForeignKey = false;
				colvarGtins.IsReadOnly = false;
				colvarGtins.DefaultSetting = @"";
				colvarGtins.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGtins);
				
				TableSchema.TableColumn colvarProductCode = new TableSchema.TableColumn(schema);
				colvarProductCode.ColumnName = "product_code";
				colvarProductCode.DataType = DbType.String;
				colvarProductCode.MaxLength = 200;
				colvarProductCode.AutoIncrement = false;
				colvarProductCode.IsNullable = true;
				colvarProductCode.IsPrimaryKey = false;
				colvarProductCode.IsForeignKey = false;
				colvarProductCode.IsReadOnly = false;
				colvarProductCode.DefaultSetting = @"";
				colvarProductCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductCode);
				
				TableSchema.TableColumn colvarStock = new TableSchema.TableColumn(schema);
				colvarStock.ColumnName = "stock";
				colvarStock.DataType = DbType.String;
				colvarStock.MaxLength = 200;
				colvarStock.AutoIncrement = false;
				colvarStock.IsNullable = true;
				colvarStock.IsPrimaryKey = false;
				colvarStock.IsForeignKey = false;
				colvarStock.IsReadOnly = false;
				colvarStock.DefaultSetting = @"";
				colvarStock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStock);
				
				TableSchema.TableColumn colvarSaftyStock = new TableSchema.TableColumn(schema);
				colvarSaftyStock.ColumnName = "safty_stock";
				colvarSaftyStock.DataType = DbType.String;
				colvarSaftyStock.MaxLength = 200;
				colvarSaftyStock.AutoIncrement = false;
				colvarSaftyStock.IsNullable = true;
				colvarSaftyStock.IsPrimaryKey = false;
				colvarSaftyStock.IsForeignKey = false;
				colvarSaftyStock.IsReadOnly = false;
				colvarSaftyStock.DefaultSetting = @"";
				colvarSaftyStock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSaftyStock);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 200;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = true;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.AnsiString;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarMpn = new TableSchema.TableColumn(schema);
				colvarMpn.ColumnName = "mpn";
				colvarMpn.DataType = DbType.AnsiString;
				colvarMpn.MaxLength = 70;
				colvarMpn.AutoIncrement = false;
				colvarMpn.IsNullable = true;
				colvarMpn.IsPrimaryKey = false;
				colvarMpn.IsForeignKey = false;
				colvarMpn.IsReadOnly = false;
				colvarMpn.DefaultSetting = @"";
				colvarMpn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMpn);
				
				TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
				colvarPrice.ColumnName = "price";
				colvarPrice.DataType = DbType.AnsiString;
				colvarPrice.MaxLength = 10;
				colvarPrice.AutoIncrement = false;
				colvarPrice.IsNullable = true;
				colvarPrice.IsPrimaryKey = false;
				colvarPrice.IsForeignKey = false;
				colvarPrice.IsReadOnly = false;
				colvarPrice.DefaultSetting = @"";
				colvarPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrice);
				
				TableSchema.TableColumn colvarShelfLife = new TableSchema.TableColumn(schema);
				colvarShelfLife.ColumnName = "shelf_life";
				colvarShelfLife.DataType = DbType.AnsiString;
				colvarShelfLife.MaxLength = 10;
				colvarShelfLife.AutoIncrement = false;
				colvarShelfLife.IsNullable = true;
				colvarShelfLife.IsPrimaryKey = false;
				colvarShelfLife.IsForeignKey = false;
				colvarShelfLife.IsReadOnly = false;
				colvarShelfLife.DefaultSetting = @"";
				colvarShelfLife.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShelfLife);
				
				TableSchema.TableColumn colvarWarehouseType = new TableSchema.TableColumn(schema);
				colvarWarehouseType.ColumnName = "warehouse_type";
				colvarWarehouseType.DataType = DbType.AnsiString;
				colvarWarehouseType.MaxLength = 1;
				colvarWarehouseType.AutoIncrement = false;
				colvarWarehouseType.IsNullable = true;
				colvarWarehouseType.IsPrimaryKey = false;
				colvarWarehouseType.IsForeignKey = false;
				colvarWarehouseType.IsReadOnly = false;
				colvarWarehouseType.DefaultSetting = @"";
				colvarWarehouseType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWarehouseType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("product_info_import_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("GroupId")]
		[Bindable(true)]
		public string GroupId 
		{
			get { return GetColumnValue<string>(Columns.GroupId); }
			set { SetColumnValue(Columns.GroupId, value); }
		}
		  
		[XmlAttribute("SpecType")]
		[Bindable(true)]
		public string SpecType 
		{
			get { return GetColumnValue<string>(Columns.SpecType); }
			set { SetColumnValue(Columns.SpecType, value); }
		}
		  
		[XmlAttribute("ProductBrandName")]
		[Bindable(true)]
		public string ProductBrandName 
		{
			get { return GetColumnValue<string>(Columns.ProductBrandName); }
			set { SetColumnValue(Columns.ProductBrandName, value); }
		}
		  
		[XmlAttribute("ProductName")]
		[Bindable(true)]
		public string ProductName 
		{
			get { return GetColumnValue<string>(Columns.ProductName); }
			set { SetColumnValue(Columns.ProductName, value); }
		}
		  
		[XmlAttribute("CatgName")]
		[Bindable(true)]
		public string CatgName 
		{
			get { return GetColumnValue<string>(Columns.CatgName); }
			set { SetColumnValue(Columns.CatgName, value); }
		}
		  
		[XmlAttribute("SpecName")]
		[Bindable(true)]
		public string SpecName 
		{
			get { return GetColumnValue<string>(Columns.SpecName); }
			set { SetColumnValue(Columns.SpecName, value); }
		}
		  
		[XmlAttribute("Gtins")]
		[Bindable(true)]
		public string Gtins 
		{
			get { return GetColumnValue<string>(Columns.Gtins); }
			set { SetColumnValue(Columns.Gtins, value); }
		}
		  
		[XmlAttribute("ProductCode")]
		[Bindable(true)]
		public string ProductCode 
		{
			get { return GetColumnValue<string>(Columns.ProductCode); }
			set { SetColumnValue(Columns.ProductCode, value); }
		}
		  
		[XmlAttribute("Stock")]
		[Bindable(true)]
		public string Stock 
		{
			get { return GetColumnValue<string>(Columns.Stock); }
			set { SetColumnValue(Columns.Stock, value); }
		}
		  
		[XmlAttribute("SaftyStock")]
		[Bindable(true)]
		public string SaftyStock 
		{
			get { return GetColumnValue<string>(Columns.SaftyStock); }
			set { SetColumnValue(Columns.SaftyStock, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("Mpn")]
		[Bindable(true)]
		public string Mpn 
		{
			get { return GetColumnValue<string>(Columns.Mpn); }
			set { SetColumnValue(Columns.Mpn, value); }
		}
		  
		[XmlAttribute("Price")]
		[Bindable(true)]
		public string Price 
		{
			get { return GetColumnValue<string>(Columns.Price); }
			set { SetColumnValue(Columns.Price, value); }
		}
		  
		[XmlAttribute("ShelfLife")]
		[Bindable(true)]
		public string ShelfLife 
		{
			get { return GetColumnValue<string>(Columns.ShelfLife); }
			set { SetColumnValue(Columns.ShelfLife, value); }
		}
		  
		[XmlAttribute("WarehouseType")]
		[Bindable(true)]
		public string WarehouseType 
		{
			get { return GetColumnValue<string>(Columns.WarehouseType); }
			set { SetColumnValue(Columns.WarehouseType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn GroupIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductBrandNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductNameColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CatgNameColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecNameColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn GtinsColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductCodeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn StockColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn SaftyStockColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn MpnColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn PriceColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ShelfLifeColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn WarehouseTypeColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Guid = @"guid";
			 public static string GroupId = @"group_id";
			 public static string SpecType = @"spec_type";
			 public static string ProductBrandName = @"product_brand_name";
			 public static string ProductName = @"product_name";
			 public static string CatgName = @"catg_name";
			 public static string SpecName = @"spec_name";
			 public static string Gtins = @"gtins";
			 public static string ProductCode = @"product_code";
			 public static string Stock = @"stock";
			 public static string SaftyStock = @"safty_stock";
			 public static string Memo = @"memo";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
			 public static string Mpn = @"mpn";
			 public static string Price = @"price";
			 public static string ShelfLife = @"shelf_life";
			 public static string WarehouseType = @"warehouse_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

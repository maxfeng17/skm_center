using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMemberBuildingCityParentCity class.
    /// </summary>
    [Serializable]
    public partial class ViewMemberBuildingCityParentCityCollection : ReadOnlyList<ViewMemberBuildingCityParentCity, ViewMemberBuildingCityParentCityCollection>
    {        
        public ViewMemberBuildingCityParentCityCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_member_building_city_parent_city view.
    /// </summary>
    [Serializable]
    public partial class ViewMemberBuildingCityParentCity : ReadOnlyRecord<ViewMemberBuildingCityParentCity>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_member_building_city_parent_city", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
                colvarLastName.ColumnName = "last_name";
                colvarLastName.DataType = DbType.String;
                colvarLastName.MaxLength = 50;
                colvarLastName.AutoIncrement = false;
                colvarLastName.IsNullable = true;
                colvarLastName.IsPrimaryKey = false;
                colvarLastName.IsForeignKey = false;
                colvarLastName.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastName);
                
                TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
                colvarFirstName.ColumnName = "first_name";
                colvarFirstName.DataType = DbType.String;
                colvarFirstName.MaxLength = 50;
                colvarFirstName.AutoIncrement = false;
                colvarFirstName.IsNullable = true;
                colvarFirstName.IsPrimaryKey = false;
                colvarFirstName.IsForeignKey = false;
                colvarFirstName.IsReadOnly = false;
                
                schema.Columns.Add(colvarFirstName);
                
                TableSchema.TableColumn colvarBuildingGuid = new TableSchema.TableColumn(schema);
                colvarBuildingGuid.ColumnName = "building_GUID";
                colvarBuildingGuid.DataType = DbType.Guid;
                colvarBuildingGuid.MaxLength = 0;
                colvarBuildingGuid.AutoIncrement = false;
                colvarBuildingGuid.IsNullable = true;
                colvarBuildingGuid.IsPrimaryKey = false;
                colvarBuildingGuid.IsForeignKey = false;
                colvarBuildingGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingGuid);
                
                TableSchema.TableColumn colvarBirthday = new TableSchema.TableColumn(schema);
                colvarBirthday.ColumnName = "birthday";
                colvarBirthday.DataType = DbType.DateTime;
                colvarBirthday.MaxLength = 0;
                colvarBirthday.AutoIncrement = false;
                colvarBirthday.IsNullable = true;
                colvarBirthday.IsPrimaryKey = false;
                colvarBirthday.IsForeignKey = false;
                colvarBirthday.IsReadOnly = false;
                
                schema.Columns.Add(colvarBirthday);
                
                TableSchema.TableColumn colvarGender = new TableSchema.TableColumn(schema);
                colvarGender.ColumnName = "gender";
                colvarGender.DataType = DbType.Int32;
                colvarGender.MaxLength = 0;
                colvarGender.AutoIncrement = false;
                colvarGender.IsNullable = true;
                colvarGender.IsPrimaryKey = false;
                colvarGender.IsForeignKey = false;
                colvarGender.IsReadOnly = false;
                
                schema.Columns.Add(colvarGender);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "company_name";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 50;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyName);
                
                TableSchema.TableColumn colvarCompanyDepartment = new TableSchema.TableColumn(schema);
                colvarCompanyDepartment.ColumnName = "company_department";
                colvarCompanyDepartment.DataType = DbType.String;
                colvarCompanyDepartment.MaxLength = 50;
                colvarCompanyDepartment.AutoIncrement = false;
                colvarCompanyDepartment.IsNullable = true;
                colvarCompanyDepartment.IsPrimaryKey = false;
                colvarCompanyDepartment.IsForeignKey = false;
                colvarCompanyDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyDepartment);
                
                TableSchema.TableColumn colvarCompanyTel = new TableSchema.TableColumn(schema);
                colvarCompanyTel.ColumnName = "company_tel";
                colvarCompanyTel.DataType = DbType.AnsiString;
                colvarCompanyTel.MaxLength = 20;
                colvarCompanyTel.AutoIncrement = false;
                colvarCompanyTel.IsNullable = true;
                colvarCompanyTel.IsPrimaryKey = false;
                colvarCompanyTel.IsForeignKey = false;
                colvarCompanyTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyTel);
                
                TableSchema.TableColumn colvarCompanyTelExt = new TableSchema.TableColumn(schema);
                colvarCompanyTelExt.ColumnName = "company_tel_ext";
                colvarCompanyTelExt.DataType = DbType.AnsiString;
                colvarCompanyTelExt.MaxLength = 50;
                colvarCompanyTelExt.AutoIncrement = false;
                colvarCompanyTelExt.IsNullable = true;
                colvarCompanyTelExt.IsPrimaryKey = false;
                colvarCompanyTelExt.IsForeignKey = false;
                colvarCompanyTelExt.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyTelExt);
                
                TableSchema.TableColumn colvarCompanyAddress = new TableSchema.TableColumn(schema);
                colvarCompanyAddress.ColumnName = "company_address";
                colvarCompanyAddress.DataType = DbType.String;
                colvarCompanyAddress.MaxLength = 100;
                colvarCompanyAddress.AutoIncrement = false;
                colvarCompanyAddress.IsNullable = true;
                colvarCompanyAddress.IsPrimaryKey = false;
                colvarCompanyAddress.IsForeignKey = false;
                colvarCompanyAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAddress);
                
                TableSchema.TableColumn colvarDeliveryMethod = new TableSchema.TableColumn(schema);
                colvarDeliveryMethod.ColumnName = "delivery_method";
                colvarDeliveryMethod.DataType = DbType.String;
                colvarDeliveryMethod.MaxLength = 200;
                colvarDeliveryMethod.AutoIncrement = false;
                colvarDeliveryMethod.IsNullable = true;
                colvarDeliveryMethod.IsPrimaryKey = false;
                colvarDeliveryMethod.IsForeignKey = false;
                colvarDeliveryMethod.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryMethod);
                
                TableSchema.TableColumn colvarPrimaryContactMethod = new TableSchema.TableColumn(schema);
                colvarPrimaryContactMethod.ColumnName = "primary_contact_method";
                colvarPrimaryContactMethod.DataType = DbType.Int32;
                colvarPrimaryContactMethod.MaxLength = 0;
                colvarPrimaryContactMethod.AutoIncrement = false;
                colvarPrimaryContactMethod.IsNullable = false;
                colvarPrimaryContactMethod.IsPrimaryKey = false;
                colvarPrimaryContactMethod.IsForeignKey = false;
                colvarPrimaryContactMethod.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrimaryContactMethod);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 30;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarComment = new TableSchema.TableColumn(schema);
                colvarComment.ColumnName = "comment";
                colvarComment.DataType = DbType.String;
                colvarComment.MaxLength = 100;
                colvarComment.AutoIncrement = false;
                colvarComment.IsNullable = true;
                colvarComment.IsPrimaryKey = false;
                colvarComment.IsForeignKey = false;
                colvarComment.IsReadOnly = false;
                
                schema.Columns.Add(colvarComment);
                
                TableSchema.TableColumn colvarPic = new TableSchema.TableColumn(schema);
                colvarPic.ColumnName = "pic";
                colvarPic.DataType = DbType.AnsiString;
                colvarPic.MaxLength = 500;
                colvarPic.AutoIncrement = false;
                colvarPic.IsNullable = true;
                colvarPic.IsPrimaryKey = false;
                colvarPic.IsForeignKey = false;
                colvarPic.IsReadOnly = false;
                
                schema.Columns.Add(colvarPic);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
                colvarOrderCount.ColumnName = "order_count";
                colvarOrderCount.DataType = DbType.Int32;
                colvarOrderCount.MaxLength = 0;
                colvarOrderCount.AutoIncrement = false;
                colvarOrderCount.IsNullable = true;
                colvarOrderCount.IsPrimaryKey = false;
                colvarOrderCount.IsForeignKey = false;
                colvarOrderCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCount);
                
                TableSchema.TableColumn colvarOrderTotal = new TableSchema.TableColumn(schema);
                colvarOrderTotal.ColumnName = "order_total";
                colvarOrderTotal.DataType = DbType.Int32;
                colvarOrderTotal.MaxLength = 0;
                colvarOrderTotal.AutoIncrement = false;
                colvarOrderTotal.IsNullable = true;
                colvarOrderTotal.IsPrimaryKey = false;
                colvarOrderTotal.IsForeignKey = false;
                colvarOrderTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTotal);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = true;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = true;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarBuildingId = new TableSchema.TableColumn(schema);
                colvarBuildingId.ColumnName = "building_id";
                colvarBuildingId.DataType = DbType.AnsiString;
                colvarBuildingId.MaxLength = 20;
                colvarBuildingId.AutoIncrement = false;
                colvarBuildingId.IsNullable = true;
                colvarBuildingId.IsPrimaryKey = false;
                colvarBuildingId.IsForeignKey = false;
                colvarBuildingId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingId);
                
                TableSchema.TableColumn colvarBuildingName = new TableSchema.TableColumn(schema);
                colvarBuildingName.ColumnName = "building_name";
                colvarBuildingName.DataType = DbType.String;
                colvarBuildingName.MaxLength = 50;
                colvarBuildingName.AutoIncrement = false;
                colvarBuildingName.IsNullable = true;
                colvarBuildingName.IsPrimaryKey = false;
                colvarBuildingName.IsForeignKey = false;
                colvarBuildingName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingName);
                
                TableSchema.TableColumn colvarBCityId = new TableSchema.TableColumn(schema);
                colvarBCityId.ColumnName = "b_city_id";
                colvarBCityId.DataType = DbType.Int32;
                colvarBCityId.MaxLength = 0;
                colvarBCityId.AutoIncrement = false;
                colvarBCityId.IsNullable = true;
                colvarBCityId.IsPrimaryKey = false;
                colvarBCityId.IsForeignKey = false;
                colvarBCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBCityId);
                
                TableSchema.TableColumn colvarBuildingStreetName = new TableSchema.TableColumn(schema);
                colvarBuildingStreetName.ColumnName = "building_street_name";
                colvarBuildingStreetName.DataType = DbType.String;
                colvarBuildingStreetName.MaxLength = 100;
                colvarBuildingStreetName.AutoIncrement = false;
                colvarBuildingStreetName.IsNullable = true;
                colvarBuildingStreetName.IsPrimaryKey = false;
                colvarBuildingStreetName.IsForeignKey = false;
                colvarBuildingStreetName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingStreetName);
                
                TableSchema.TableColumn colvarBuildingAddressNumber = new TableSchema.TableColumn(schema);
                colvarBuildingAddressNumber.ColumnName = "building_address_number";
                colvarBuildingAddressNumber.DataType = DbType.String;
                colvarBuildingAddressNumber.MaxLength = 100;
                colvarBuildingAddressNumber.AutoIncrement = false;
                colvarBuildingAddressNumber.IsNullable = true;
                colvarBuildingAddressNumber.IsPrimaryKey = false;
                colvarBuildingAddressNumber.IsForeignKey = false;
                colvarBuildingAddressNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingAddressNumber);
                
                TableSchema.TableColumn colvarBuildingOnline = new TableSchema.TableColumn(schema);
                colvarBuildingOnline.ColumnName = "building_online";
                colvarBuildingOnline.DataType = DbType.Boolean;
                colvarBuildingOnline.MaxLength = 0;
                colvarBuildingOnline.AutoIncrement = false;
                colvarBuildingOnline.IsNullable = true;
                colvarBuildingOnline.IsPrimaryKey = false;
                colvarBuildingOnline.IsForeignKey = false;
                colvarBuildingOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingOnline);
                
                TableSchema.TableColumn colvarBuildingRank = new TableSchema.TableColumn(schema);
                colvarBuildingRank.ColumnName = "building_rank";
                colvarBuildingRank.DataType = DbType.Int32;
                colvarBuildingRank.MaxLength = 0;
                colvarBuildingRank.AutoIncrement = false;
                colvarBuildingRank.IsNullable = true;
                colvarBuildingRank.IsPrimaryKey = false;
                colvarBuildingRank.IsForeignKey = false;
                colvarBuildingRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingRank);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 30;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarBCreateTime = new TableSchema.TableColumn(schema);
                colvarBCreateTime.ColumnName = "b_create_time";
                colvarBCreateTime.DataType = DbType.DateTime;
                colvarBCreateTime.MaxLength = 0;
                colvarBCreateTime.AutoIncrement = false;
                colvarBCreateTime.IsNullable = true;
                colvarBCreateTime.IsPrimaryKey = false;
                colvarBCreateTime.IsForeignKey = false;
                colvarBCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarBCreateTime);
                
                TableSchema.TableColumn colvarBModifyId = new TableSchema.TableColumn(schema);
                colvarBModifyId.ColumnName = "b_modify_id";
                colvarBModifyId.DataType = DbType.String;
                colvarBModifyId.MaxLength = 30;
                colvarBModifyId.AutoIncrement = false;
                colvarBModifyId.IsNullable = true;
                colvarBModifyId.IsPrimaryKey = false;
                colvarBModifyId.IsForeignKey = false;
                colvarBModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBModifyId);
                
                TableSchema.TableColumn colvarBModifyTime = new TableSchema.TableColumn(schema);
                colvarBModifyTime.ColumnName = "b_modify_time";
                colvarBModifyTime.DataType = DbType.DateTime;
                colvarBModifyTime.MaxLength = 0;
                colvarBModifyTime.AutoIncrement = false;
                colvarBModifyTime.IsNullable = true;
                colvarBModifyTime.IsPrimaryKey = false;
                colvarBModifyTime.IsForeignKey = false;
                colvarBModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarBModifyTime);
                
                TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
                colvarCoordinate.ColumnName = "coordinate";
                colvarCoordinate.DataType = DbType.AnsiString;
                colvarCoordinate.MaxLength = -1;
                colvarCoordinate.AutoIncrement = false;
                colvarCoordinate.IsNullable = true;
                colvarCoordinate.IsPrimaryKey = false;
                colvarCoordinate.IsForeignKey = false;
                colvarCoordinate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCoordinate);
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = true;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarParentId = new TableSchema.TableColumn(schema);
                colvarParentId.ColumnName = "parent_id";
                colvarParentId.DataType = DbType.Int32;
                colvarParentId.MaxLength = 0;
                colvarParentId.AutoIncrement = false;
                colvarParentId.IsNullable = true;
                colvarParentId.IsPrimaryKey = false;
                colvarParentId.IsForeignKey = false;
                colvarParentId.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentId);
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 20;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = true;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 10;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarImgPath = new TableSchema.TableColumn(schema);
                colvarImgPath.ColumnName = "img_path";
                colvarImgPath.DataType = DbType.String;
                colvarImgPath.MaxLength = 500;
                colvarImgPath.AutoIncrement = false;
                colvarImgPath.IsNullable = true;
                colvarImgPath.IsPrimaryKey = false;
                colvarImgPath.IsForeignKey = false;
                colvarImgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarImgPath);
                
                TableSchema.TableColumn colvarCStatus = new TableSchema.TableColumn(schema);
                colvarCStatus.ColumnName = "c_status";
                colvarCStatus.DataType = DbType.Int32;
                colvarCStatus.MaxLength = 0;
                colvarCStatus.AutoIncrement = false;
                colvarCStatus.IsNullable = true;
                colvarCStatus.IsPrimaryKey = false;
                colvarCStatus.IsForeignKey = false;
                colvarCStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCStatus);
                
                TableSchema.TableColumn colvarRank = new TableSchema.TableColumn(schema);
                colvarRank.ColumnName = "rank";
                colvarRank.DataType = DbType.Int32;
                colvarRank.MaxLength = 0;
                colvarRank.AutoIncrement = false;
                colvarRank.IsNullable = true;
                colvarRank.IsPrimaryKey = false;
                colvarRank.IsForeignKey = false;
                colvarRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarRank);
                
                TableSchema.TableColumn colvarZipCode = new TableSchema.TableColumn(schema);
                colvarZipCode.ColumnName = "zip_code";
                colvarZipCode.DataType = DbType.AnsiString;
                colvarZipCode.MaxLength = 5;
                colvarZipCode.AutoIncrement = false;
                colvarZipCode.IsNullable = true;
                colvarZipCode.IsPrimaryKey = false;
                colvarZipCode.IsForeignKey = false;
                colvarZipCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarZipCode);
                
                TableSchema.TableColumn colvarPcCityId = new TableSchema.TableColumn(schema);
                colvarPcCityId.ColumnName = "pc_city_id";
                colvarPcCityId.DataType = DbType.Int32;
                colvarPcCityId.MaxLength = 0;
                colvarPcCityId.AutoIncrement = false;
                colvarPcCityId.IsNullable = true;
                colvarPcCityId.IsPrimaryKey = false;
                colvarPcCityId.IsForeignKey = false;
                colvarPcCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcCityId);
                
                TableSchema.TableColumn colvarPcParentId = new TableSchema.TableColumn(schema);
                colvarPcParentId.ColumnName = "pc_parent_id";
                colvarPcParentId.DataType = DbType.Int32;
                colvarPcParentId.MaxLength = 0;
                colvarPcParentId.AutoIncrement = false;
                colvarPcParentId.IsNullable = true;
                colvarPcParentId.IsPrimaryKey = false;
                colvarPcParentId.IsForeignKey = false;
                colvarPcParentId.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcParentId);
                
                TableSchema.TableColumn colvarPcCityName = new TableSchema.TableColumn(schema);
                colvarPcCityName.ColumnName = "pc_city_name";
                colvarPcCityName.DataType = DbType.String;
                colvarPcCityName.MaxLength = 20;
                colvarPcCityName.AutoIncrement = false;
                colvarPcCityName.IsNullable = true;
                colvarPcCityName.IsPrimaryKey = false;
                colvarPcCityName.IsForeignKey = false;
                colvarPcCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcCityName);
                
                TableSchema.TableColumn colvarPcCode = new TableSchema.TableColumn(schema);
                colvarPcCode.ColumnName = "pc_code";
                colvarPcCode.DataType = DbType.AnsiString;
                colvarPcCode.MaxLength = 10;
                colvarPcCode.AutoIncrement = false;
                colvarPcCode.IsNullable = true;
                colvarPcCode.IsPrimaryKey = false;
                colvarPcCode.IsForeignKey = false;
                colvarPcCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcCode);
                
                TableSchema.TableColumn colvarPcImgPath = new TableSchema.TableColumn(schema);
                colvarPcImgPath.ColumnName = "pc_img_path";
                colvarPcImgPath.DataType = DbType.String;
                colvarPcImgPath.MaxLength = 500;
                colvarPcImgPath.AutoIncrement = false;
                colvarPcImgPath.IsNullable = true;
                colvarPcImgPath.IsPrimaryKey = false;
                colvarPcImgPath.IsForeignKey = false;
                colvarPcImgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcImgPath);
                
                TableSchema.TableColumn colvarPcStatus = new TableSchema.TableColumn(schema);
                colvarPcStatus.ColumnName = "pc_status";
                colvarPcStatus.DataType = DbType.Int32;
                colvarPcStatus.MaxLength = 0;
                colvarPcStatus.AutoIncrement = false;
                colvarPcStatus.IsNullable = true;
                colvarPcStatus.IsPrimaryKey = false;
                colvarPcStatus.IsForeignKey = false;
                colvarPcStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcStatus);
                
                TableSchema.TableColumn colvarPcRank = new TableSchema.TableColumn(schema);
                colvarPcRank.ColumnName = "pc_rank";
                colvarPcRank.DataType = DbType.Int32;
                colvarPcRank.MaxLength = 0;
                colvarPcRank.AutoIncrement = false;
                colvarPcRank.IsNullable = true;
                colvarPcRank.IsPrimaryKey = false;
                colvarPcRank.IsForeignKey = false;
                colvarPcRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcRank);
                
                TableSchema.TableColumn colvarPcZipCode = new TableSchema.TableColumn(schema);
                colvarPcZipCode.ColumnName = "pc_zip_code";
                colvarPcZipCode.DataType = DbType.AnsiString;
                colvarPcZipCode.MaxLength = 5;
                colvarPcZipCode.AutoIncrement = false;
                colvarPcZipCode.IsNullable = true;
                colvarPcZipCode.IsPrimaryKey = false;
                colvarPcZipCode.IsForeignKey = false;
                colvarPcZipCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcZipCode);
                
                TableSchema.TableColumn colvarEdmType = new TableSchema.TableColumn(schema);
                colvarEdmType.ColumnName = "edm_type";
                colvarEdmType.DataType = DbType.Int32;
                colvarEdmType.MaxLength = 0;
                colvarEdmType.AutoIncrement = false;
                colvarEdmType.IsNullable = false;
                colvarEdmType.IsPrimaryKey = false;
                colvarEdmType.IsForeignKey = false;
                colvarEdmType.IsReadOnly = false;
                
                schema.Columns.Add(colvarEdmType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_member_building_city_parent_city",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMemberBuildingCityParentCity()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMemberBuildingCityParentCity(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMemberBuildingCityParentCity(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMemberBuildingCityParentCity(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("LastName")]
        [Bindable(true)]
        public string LastName 
	    {
		    get
		    {
			    return GetColumnValue<string>("last_name");
		    }
            set 
		    {
			    SetColumnValue("last_name", value);
            }
        }
	      
        [XmlAttribute("FirstName")]
        [Bindable(true)]
        public string FirstName 
	    {
		    get
		    {
			    return GetColumnValue<string>("first_name");
		    }
            set 
		    {
			    SetColumnValue("first_name", value);
            }
        }
	      
        [XmlAttribute("BuildingGuid")]
        [Bindable(true)]
        public Guid? BuildingGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("building_GUID");
		    }
            set 
		    {
			    SetColumnValue("building_GUID", value);
            }
        }
	      
        [XmlAttribute("Birthday")]
        [Bindable(true)]
        public DateTime? Birthday 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("birthday");
		    }
            set 
		    {
			    SetColumnValue("birthday", value);
            }
        }
	      
        [XmlAttribute("Gender")]
        [Bindable(true)]
        public int? Gender 
	    {
		    get
		    {
			    return GetColumnValue<int?>("gender");
		    }
            set 
		    {
			    SetColumnValue("gender", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	      
        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_name");
		    }
            set 
		    {
			    SetColumnValue("company_name", value);
            }
        }
	      
        [XmlAttribute("CompanyDepartment")]
        [Bindable(true)]
        public string CompanyDepartment 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_department");
		    }
            set 
		    {
			    SetColumnValue("company_department", value);
            }
        }
	      
        [XmlAttribute("CompanyTel")]
        [Bindable(true)]
        public string CompanyTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_tel");
		    }
            set 
		    {
			    SetColumnValue("company_tel", value);
            }
        }
	      
        [XmlAttribute("CompanyTelExt")]
        [Bindable(true)]
        public string CompanyTelExt 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_tel_ext");
		    }
            set 
		    {
			    SetColumnValue("company_tel_ext", value);
            }
        }
	      
        [XmlAttribute("CompanyAddress")]
        [Bindable(true)]
        public string CompanyAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_address");
		    }
            set 
		    {
			    SetColumnValue("company_address", value);
            }
        }
	      
        [XmlAttribute("DeliveryMethod")]
        [Bindable(true)]
        public string DeliveryMethod 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_method");
		    }
            set 
		    {
			    SetColumnValue("delivery_method", value);
            }
        }
	      
        [XmlAttribute("PrimaryContactMethod")]
        [Bindable(true)]
        public int PrimaryContactMethod 
	    {
		    get
		    {
			    return GetColumnValue<int>("primary_contact_method");
		    }
            set 
		    {
			    SetColumnValue("primary_contact_method", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("Comment")]
        [Bindable(true)]
        public string Comment 
	    {
		    get
		    {
			    return GetColumnValue<string>("comment");
		    }
            set 
		    {
			    SetColumnValue("comment", value);
            }
        }
	      
        [XmlAttribute("Pic")]
        [Bindable(true)]
        public string Pic 
	    {
		    get
		    {
			    return GetColumnValue<string>("pic");
		    }
            set 
		    {
			    SetColumnValue("pic", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("OrderCount")]
        [Bindable(true)]
        public int? OrderCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_count");
		    }
            set 
		    {
			    SetColumnValue("order_count", value);
            }
        }
	      
        [XmlAttribute("OrderTotal")]
        [Bindable(true)]
        public int? OrderTotal 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_total");
		    }
            set 
		    {
			    SetColumnValue("order_total", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int? CityId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid? Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("GUID");
		    }
            set 
		    {
			    SetColumnValue("GUID", value);
            }
        }
	      
        [XmlAttribute("BuildingId")]
        [Bindable(true)]
        public string BuildingId 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_id");
		    }
            set 
		    {
			    SetColumnValue("building_id", value);
            }
        }
	      
        [XmlAttribute("BuildingName")]
        [Bindable(true)]
        public string BuildingName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_name");
		    }
            set 
		    {
			    SetColumnValue("building_name", value);
            }
        }
	      
        [XmlAttribute("BCityId")]
        [Bindable(true)]
        public int? BCityId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("b_city_id");
		    }
            set 
		    {
			    SetColumnValue("b_city_id", value);
            }
        }
	      
        [XmlAttribute("BuildingStreetName")]
        [Bindable(true)]
        public string BuildingStreetName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_street_name");
		    }
            set 
		    {
			    SetColumnValue("building_street_name", value);
            }
        }
	      
        [XmlAttribute("BuildingAddressNumber")]
        [Bindable(true)]
        public string BuildingAddressNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_address_number");
		    }
            set 
		    {
			    SetColumnValue("building_address_number", value);
            }
        }
	      
        [XmlAttribute("BuildingOnline")]
        [Bindable(true)]
        public bool? BuildingOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("building_online");
		    }
            set 
		    {
			    SetColumnValue("building_online", value);
            }
        }
	      
        [XmlAttribute("BuildingRank")]
        [Bindable(true)]
        public int? BuildingRank 
	    {
		    get
		    {
			    return GetColumnValue<int?>("building_rank");
		    }
            set 
		    {
			    SetColumnValue("building_rank", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("BCreateTime")]
        [Bindable(true)]
        public DateTime? BCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("b_create_time");
		    }
            set 
		    {
			    SetColumnValue("b_create_time", value);
            }
        }
	      
        [XmlAttribute("BModifyId")]
        [Bindable(true)]
        public string BModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("b_modify_id");
		    }
            set 
		    {
			    SetColumnValue("b_modify_id", value);
            }
        }
	      
        [XmlAttribute("BModifyTime")]
        [Bindable(true)]
        public DateTime? BModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("b_modify_time");
		    }
            set 
		    {
			    SetColumnValue("b_modify_time", value);
            }
        }
	      
        [XmlAttribute("Coordinate")]
        [Bindable(true)]
        public string Coordinate 
	    {
		    get
		    {
			    return GetColumnValue<string>("coordinate");
		    }
            set 
		    {
			    SetColumnValue("coordinate", value);
            }
        }
	      
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int? Id 
	    {
		    get
		    {
			    return GetColumnValue<int?>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("ParentId")]
        [Bindable(true)]
        public int? ParentId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("parent_id");
		    }
            set 
		    {
			    SetColumnValue("parent_id", value);
            }
        }
	      
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("ImgPath")]
        [Bindable(true)]
        public string ImgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("img_path");
		    }
            set 
		    {
			    SetColumnValue("img_path", value);
            }
        }
	      
        [XmlAttribute("CStatus")]
        [Bindable(true)]
        public int? CStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("c_status");
		    }
            set 
		    {
			    SetColumnValue("c_status", value);
            }
        }
	      
        [XmlAttribute("Rank")]
        [Bindable(true)]
        public int? Rank 
	    {
		    get
		    {
			    return GetColumnValue<int?>("rank");
		    }
            set 
		    {
			    SetColumnValue("rank", value);
            }
        }
	      
        [XmlAttribute("ZipCode")]
        [Bindable(true)]
        public string ZipCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("zip_code");
		    }
            set 
		    {
			    SetColumnValue("zip_code", value);
            }
        }
	      
        [XmlAttribute("PcCityId")]
        [Bindable(true)]
        public int? PcCityId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("pc_city_id");
		    }
            set 
		    {
			    SetColumnValue("pc_city_id", value);
            }
        }
	      
        [XmlAttribute("PcParentId")]
        [Bindable(true)]
        public int? PcParentId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("pc_parent_id");
		    }
            set 
		    {
			    SetColumnValue("pc_parent_id", value);
            }
        }
	      
        [XmlAttribute("PcCityName")]
        [Bindable(true)]
        public string PcCityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("pc_city_name");
		    }
            set 
		    {
			    SetColumnValue("pc_city_name", value);
            }
        }
	      
        [XmlAttribute("PcCode")]
        [Bindable(true)]
        public string PcCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("pc_code");
		    }
            set 
		    {
			    SetColumnValue("pc_code", value);
            }
        }
	      
        [XmlAttribute("PcImgPath")]
        [Bindable(true)]
        public string PcImgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("pc_img_path");
		    }
            set 
		    {
			    SetColumnValue("pc_img_path", value);
            }
        }
	      
        [XmlAttribute("PcStatus")]
        [Bindable(true)]
        public int? PcStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("pc_status");
		    }
            set 
		    {
			    SetColumnValue("pc_status", value);
            }
        }
	      
        [XmlAttribute("PcRank")]
        [Bindable(true)]
        public int? PcRank 
	    {
		    get
		    {
			    return GetColumnValue<int?>("pc_rank");
		    }
            set 
		    {
			    SetColumnValue("pc_rank", value);
            }
        }
	      
        [XmlAttribute("PcZipCode")]
        [Bindable(true)]
        public string PcZipCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("pc_zip_code");
		    }
            set 
		    {
			    SetColumnValue("pc_zip_code", value);
            }
        }
	      
        [XmlAttribute("EdmType")]
        [Bindable(true)]
        public int EdmType 
	    {
		    get
		    {
			    return GetColumnValue<int>("edm_type");
		    }
            set 
		    {
			    SetColumnValue("edm_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string UserName = @"user_name";
            
            public static string LastName = @"last_name";
            
            public static string FirstName = @"first_name";
            
            public static string BuildingGuid = @"building_GUID";
            
            public static string Birthday = @"birthday";
            
            public static string Gender = @"gender";
            
            public static string Mobile = @"mobile";
            
            public static string CompanyName = @"company_name";
            
            public static string CompanyDepartment = @"company_department";
            
            public static string CompanyTel = @"company_tel";
            
            public static string CompanyTelExt = @"company_tel_ext";
            
            public static string CompanyAddress = @"company_address";
            
            public static string DeliveryMethod = @"delivery_method";
            
            public static string PrimaryContactMethod = @"primary_contact_method";
            
            public static string Status = @"status";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyTime = @"modify_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string Comment = @"comment";
            
            public static string Pic = @"pic";
            
            public static string UniqueId = @"unique_id";
            
            public static string OrderCount = @"order_count";
            
            public static string OrderTotal = @"order_total";
            
            public static string CityId = @"city_id";
            
            public static string Guid = @"GUID";
            
            public static string BuildingId = @"building_id";
            
            public static string BuildingName = @"building_name";
            
            public static string BCityId = @"b_city_id";
            
            public static string BuildingStreetName = @"building_street_name";
            
            public static string BuildingAddressNumber = @"building_address_number";
            
            public static string BuildingOnline = @"building_online";
            
            public static string BuildingRank = @"building_rank";
            
            public static string CreateId = @"create_id";
            
            public static string BCreateTime = @"b_create_time";
            
            public static string BModifyId = @"b_modify_id";
            
            public static string BModifyTime = @"b_modify_time";
            
            public static string Coordinate = @"coordinate";
            
            public static string Id = @"id";
            
            public static string ParentId = @"parent_id";
            
            public static string CityName = @"city_name";
            
            public static string Code = @"code";
            
            public static string ImgPath = @"img_path";
            
            public static string CStatus = @"c_status";
            
            public static string Rank = @"rank";
            
            public static string ZipCode = @"zip_code";
            
            public static string PcCityId = @"pc_city_id";
            
            public static string PcParentId = @"pc_parent_id";
            
            public static string PcCityName = @"pc_city_name";
            
            public static string PcCode = @"pc_code";
            
            public static string PcImgPath = @"pc_img_path";
            
            public static string PcStatus = @"pc_status";
            
            public static string PcRank = @"pc_rank";
            
            public static string PcZipCode = @"pc_zip_code";
            
            public static string EdmType = @"edm_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewUserMembershipCardStore class.
    /// </summary>
    [Serializable]
    public partial class ViewUserMembershipCardStoreCollection : ReadOnlyList<ViewUserMembershipCardStore, ViewUserMembershipCardStoreCollection>
    {        
        public ViewUserMembershipCardStoreCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_user_membership_card_store view.
    /// </summary>
    [Serializable]
    public partial class ViewUserMembershipCardStore : ReadOnlyRecord<ViewUserMembershipCardStore>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_user_membership_card_store", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
                colvarCardGroupId.ColumnName = "card_group_id";
                colvarCardGroupId.DataType = DbType.Int32;
                colvarCardGroupId.MaxLength = 0;
                colvarCardGroupId.AutoIncrement = false;
                colvarCardGroupId.IsNullable = false;
                colvarCardGroupId.IsPrimaryKey = false;
                colvarCardGroupId.IsForeignKey = false;
                colvarCardGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardGroupId);
                
                TableSchema.TableColumn colvarCardId = new TableSchema.TableColumn(schema);
                colvarCardId.ColumnName = "card_id";
                colvarCardId.DataType = DbType.Int32;
                colvarCardId.MaxLength = 0;
                colvarCardId.AutoIncrement = false;
                colvarCardId.IsNullable = false;
                colvarCardId.IsPrimaryKey = false;
                colvarCardId.IsForeignKey = false;
                colvarCardId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardId);
                
                TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
                colvarOrderCount.ColumnName = "order_count";
                colvarOrderCount.DataType = DbType.Int32;
                colvarOrderCount.MaxLength = 0;
                colvarOrderCount.AutoIncrement = false;
                colvarOrderCount.IsNullable = false;
                colvarOrderCount.IsPrimaryKey = false;
                colvarOrderCount.IsForeignKey = false;
                colvarOrderCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCount);
                
                TableSchema.TableColumn colvarAmountTotal = new TableSchema.TableColumn(schema);
                colvarAmountTotal.ColumnName = "amount_total";
                colvarAmountTotal.DataType = DbType.Currency;
                colvarAmountTotal.MaxLength = 0;
                colvarAmountTotal.AutoIncrement = false;
                colvarAmountTotal.IsNullable = false;
                colvarAmountTotal.IsPrimaryKey = false;
                colvarAmountTotal.IsForeignKey = false;
                colvarAmountTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmountTotal);
                
                TableSchema.TableColumn colvarLastUseTime = new TableSchema.TableColumn(schema);
                colvarLastUseTime.ColumnName = "last_use_time";
                colvarLastUseTime.DataType = DbType.DateTime;
                colvarLastUseTime.MaxLength = 0;
                colvarLastUseTime.AutoIncrement = false;
                colvarLastUseTime.IsNullable = true;
                colvarLastUseTime.IsPrimaryKey = false;
                colvarLastUseTime.IsForeignKey = false;
                colvarLastUseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastUseTime);
                
                TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
                colvarEnabled.ColumnName = "enabled";
                colvarEnabled.DataType = DbType.Boolean;
                colvarEnabled.MaxLength = 0;
                colvarEnabled.AutoIncrement = false;
                colvarEnabled.IsNullable = false;
                colvarEnabled.IsPrimaryKey = false;
                colvarEnabled.IsForeignKey = false;
                colvarEnabled.IsReadOnly = false;
                
                schema.Columns.Add(colvarEnabled);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = false;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = false;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.Int32;
                colvarModifyId.MaxLength = 0;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarRequestTime = new TableSchema.TableColumn(schema);
                colvarRequestTime.ColumnName = "request_time";
                colvarRequestTime.DataType = DbType.DateTime;
                colvarRequestTime.MaxLength = 0;
                colvarRequestTime.AutoIncrement = false;
                colvarRequestTime.IsNullable = false;
                colvarRequestTime.IsPrimaryKey = false;
                colvarRequestTime.IsForeignKey = false;
                colvarRequestTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarRequestTime);
                
                TableSchema.TableColumn colvarCardNo = new TableSchema.TableColumn(schema);
                colvarCardNo.ColumnName = "card_no";
                colvarCardNo.DataType = DbType.AnsiString;
                colvarCardNo.MaxLength = 15;
                colvarCardNo.AutoIncrement = false;
                colvarCardNo.IsNullable = false;
                colvarCardNo.IsPrimaryKey = false;
                colvarCardNo.IsForeignKey = false;
                colvarCardNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardNo);
                
                TableSchema.TableColumn colvarUserSeq = new TableSchema.TableColumn(schema);
                colvarUserSeq.ColumnName = "user_seq";
                colvarUserSeq.DataType = DbType.Int32;
                colvarUserSeq.MaxLength = 0;
                colvarUserSeq.AutoIncrement = false;
                colvarUserSeq.IsNullable = false;
                colvarUserSeq.IsPrimaryKey = false;
                colvarUserSeq.IsForeignKey = false;
                colvarUserSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserSeq);
                
                TableSchema.TableColumn colvarVersionId = new TableSchema.TableColumn(schema);
                colvarVersionId.ColumnName = "version_id";
                colvarVersionId.DataType = DbType.Int32;
                colvarVersionId.MaxLength = 0;
                colvarVersionId.AutoIncrement = false;
                colvarVersionId.IsNullable = false;
                colvarVersionId.IsPrimaryKey = false;
                colvarVersionId.IsForeignKey = false;
                colvarVersionId.IsReadOnly = false;
                
                schema.Columns.Add(colvarVersionId);
                
                TableSchema.TableColumn colvarLevel = new TableSchema.TableColumn(schema);
                colvarLevel.ColumnName = "level";
                colvarLevel.DataType = DbType.Int32;
                colvarLevel.MaxLength = 0;
                colvarLevel.AutoIncrement = false;
                colvarLevel.IsNullable = false;
                colvarLevel.IsPrimaryKey = false;
                colvarLevel.IsForeignKey = false;
                colvarLevel.IsReadOnly = false;
                
                schema.Columns.Add(colvarLevel);
                
                TableSchema.TableColumn colvarPaymentPercent = new TableSchema.TableColumn(schema);
                colvarPaymentPercent.ColumnName = "payment_percent";
                colvarPaymentPercent.DataType = DbType.Double;
                colvarPaymentPercent.MaxLength = 0;
                colvarPaymentPercent.AutoIncrement = false;
                colvarPaymentPercent.IsNullable = false;
                colvarPaymentPercent.IsPrimaryKey = false;
                colvarPaymentPercent.IsForeignKey = false;
                colvarPaymentPercent.IsReadOnly = false;
                
                schema.Columns.Add(colvarPaymentPercent);
                
                TableSchema.TableColumn colvarInstruction = new TableSchema.TableColumn(schema);
                colvarInstruction.ColumnName = "instruction";
                colvarInstruction.DataType = DbType.String;
                colvarInstruction.MaxLength = -1;
                colvarInstruction.AutoIncrement = false;
                colvarInstruction.IsNullable = false;
                colvarInstruction.IsPrimaryKey = false;
                colvarInstruction.IsForeignKey = false;
                colvarInstruction.IsReadOnly = false;
                
                schema.Columns.Add(colvarInstruction);
                
                TableSchema.TableColumn colvarOthers = new TableSchema.TableColumn(schema);
                colvarOthers.ColumnName = "others";
                colvarOthers.DataType = DbType.String;
                colvarOthers.MaxLength = -1;
                colvarOthers.AutoIncrement = false;
                colvarOthers.IsNullable = true;
                colvarOthers.IsPrimaryKey = false;
                colvarOthers.IsForeignKey = false;
                colvarOthers.IsReadOnly = false;
                
                schema.Columns.Add(colvarOthers);
                
                TableSchema.TableColumn colvarImage = new TableSchema.TableColumn(schema);
                colvarImage.ColumnName = "image";
                colvarImage.DataType = DbType.String;
                colvarImage.MaxLength = 250;
                colvarImage.AutoIncrement = false;
                colvarImage.IsNullable = true;
                colvarImage.IsPrimaryKey = false;
                colvarImage.IsForeignKey = false;
                colvarImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarImage);
                
                TableSchema.TableColumn colvarAvailableDateType = new TableSchema.TableColumn(schema);
                colvarAvailableDateType.ColumnName = "available_date_type";
                colvarAvailableDateType.DataType = DbType.Int32;
                colvarAvailableDateType.MaxLength = 0;
                colvarAvailableDateType.AutoIncrement = false;
                colvarAvailableDateType.IsNullable = false;
                colvarAvailableDateType.IsPrimaryKey = false;
                colvarAvailableDateType.IsForeignKey = false;
                colvarAvailableDateType.IsReadOnly = false;
                
                schema.Columns.Add(colvarAvailableDateType);
                
                TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
                colvarOpenTime.ColumnName = "open_time";
                colvarOpenTime.DataType = DbType.DateTime;
                colvarOpenTime.MaxLength = 0;
                colvarOpenTime.AutoIncrement = false;
                colvarOpenTime.IsNullable = false;
                colvarOpenTime.IsPrimaryKey = false;
                colvarOpenTime.IsForeignKey = false;
                colvarOpenTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpenTime);
                
                TableSchema.TableColumn colvarCloseTime = new TableSchema.TableColumn(schema);
                colvarCloseTime.ColumnName = "close_time";
                colvarCloseTime.DataType = DbType.DateTime;
                colvarCloseTime.MaxLength = 0;
                colvarCloseTime.AutoIncrement = false;
                colvarCloseTime.IsNullable = false;
                colvarCloseTime.IsPrimaryKey = false;
                colvarCloseTime.IsForeignKey = false;
                colvarCloseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCloseTime);
                
                TableSchema.TableColumn colvarMembershipCardEnabled = new TableSchema.TableColumn(schema);
                colvarMembershipCardEnabled.ColumnName = "membership_card_enabled";
                colvarMembershipCardEnabled.DataType = DbType.Boolean;
                colvarMembershipCardEnabled.MaxLength = 0;
                colvarMembershipCardEnabled.AutoIncrement = false;
                colvarMembershipCardEnabled.IsNullable = false;
                colvarMembershipCardEnabled.IsPrimaryKey = false;
                colvarMembershipCardEnabled.IsForeignKey = false;
                colvarMembershipCardEnabled.IsReadOnly = false;
                
                schema.Columns.Add(colvarMembershipCardEnabled);
                
                TableSchema.TableColumn colvarConditionalLogic = new TableSchema.TableColumn(schema);
                colvarConditionalLogic.ColumnName = "conditional_logic";
                colvarConditionalLogic.DataType = DbType.Int32;
                colvarConditionalLogic.MaxLength = 0;
                colvarConditionalLogic.AutoIncrement = false;
                colvarConditionalLogic.IsNullable = false;
                colvarConditionalLogic.IsPrimaryKey = false;
                colvarConditionalLogic.IsForeignKey = false;
                colvarConditionalLogic.IsReadOnly = false;
                
                schema.Columns.Add(colvarConditionalLogic);
                
                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = false;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerUserId);
                
                TableSchema.TableColumn colvarDepositService = new TableSchema.TableColumn(schema);
                colvarDepositService.ColumnName = "deposit_service";
                colvarDepositService.DataType = DbType.Int32;
                colvarDepositService.MaxLength = 0;
                colvarDepositService.AutoIncrement = false;
                colvarDepositService.IsNullable = true;
                colvarDepositService.IsPrimaryKey = false;
                colvarDepositService.IsForeignKey = false;
                colvarDepositService.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepositService);
                
                TableSchema.TableColumn colvarPointService = new TableSchema.TableColumn(schema);
                colvarPointService.ColumnName = "point_service";
                colvarPointService.DataType = DbType.Int32;
                colvarPointService.MaxLength = 0;
                colvarPointService.AutoIncrement = false;
                colvarPointService.IsNullable = true;
                colvarPointService.IsPrimaryKey = false;
                colvarPointService.IsForeignKey = false;
                colvarPointService.IsReadOnly = false;
                
                schema.Columns.Add(colvarPointService);
                
                TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
                colvarImagePath.ColumnName = "image_path";
                colvarImagePath.DataType = DbType.String;
                colvarImagePath.MaxLength = 250;
                colvarImagePath.AutoIncrement = false;
                colvarImagePath.IsNullable = true;
                colvarImagePath.IsPrimaryKey = false;
                colvarImagePath.IsForeignKey = false;
                colvarImagePath.IsReadOnly = false;
                
                schema.Columns.Add(colvarImagePath);
                
                TableSchema.TableColumn colvarBackgroundColorValue = new TableSchema.TableColumn(schema);
                colvarBackgroundColorValue.ColumnName = "background_color_value";
                colvarBackgroundColorValue.DataType = DbType.String;
                colvarBackgroundColorValue.MaxLength = -1;
                colvarBackgroundColorValue.AutoIncrement = false;
                colvarBackgroundColorValue.IsNullable = false;
                colvarBackgroundColorValue.IsPrimaryKey = false;
                colvarBackgroundColorValue.IsForeignKey = false;
                colvarBackgroundColorValue.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundColorValue);
                
                TableSchema.TableColumn colvarBackgroundImageValue = new TableSchema.TableColumn(schema);
                colvarBackgroundImageValue.ColumnName = "background_image_value";
                colvarBackgroundImageValue.DataType = DbType.String;
                colvarBackgroundImageValue.MaxLength = -1;
                colvarBackgroundImageValue.AutoIncrement = false;
                colvarBackgroundImageValue.IsNullable = false;
                colvarBackgroundImageValue.IsPrimaryKey = false;
                colvarBackgroundImageValue.IsForeignKey = false;
                colvarBackgroundImageValue.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundImageValue);
                
                TableSchema.TableColumn colvarIconImageValue = new TableSchema.TableColumn(schema);
                colvarIconImageValue.ColumnName = "icon_image_value";
                colvarIconImageValue.DataType = DbType.String;
                colvarIconImageValue.MaxLength = -1;
                colvarIconImageValue.AutoIncrement = false;
                colvarIconImageValue.IsNullable = false;
                colvarIconImageValue.IsPrimaryKey = false;
                colvarIconImageValue.IsForeignKey = false;
                colvarIconImageValue.IsReadOnly = false;
                
                schema.Columns.Add(colvarIconImageValue);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 100;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = false;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
                colvarCoordinate.ColumnName = "coordinate";
                colvarCoordinate.DataType = DbType.String;
                colvarCoordinate.MaxLength = -1;
                colvarCoordinate.AutoIncrement = false;
                colvarCoordinate.IsNullable = true;
                colvarCoordinate.IsPrimaryKey = false;
                colvarCoordinate.IsForeignKey = false;
                colvarCoordinate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCoordinate);
                
                TableSchema.TableColumn colvarCardType = new TableSchema.TableColumn(schema);
                colvarCardType.ColumnName = "card_type";
                colvarCardType.DataType = DbType.Int32;
                colvarCardType.MaxLength = 0;
                colvarCardType.AutoIncrement = false;
                colvarCardType.IsNullable = false;
                colvarCardType.IsPrimaryKey = false;
                colvarCardType.IsForeignKey = false;
                colvarCardType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardType);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Byte;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_user_membership_card_store",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewUserMembershipCardStore()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewUserMembershipCardStore(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewUserMembershipCardStore(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewUserMembershipCardStore(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("CardGroupId")]
        [Bindable(true)]
        public int CardGroupId 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_group_id");
		    }
            set 
		    {
			    SetColumnValue("card_group_id", value);
            }
        }
	      
        [XmlAttribute("CardId")]
        [Bindable(true)]
        public int CardId 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_id");
		    }
            set 
		    {
			    SetColumnValue("card_id", value);
            }
        }
	      
        [XmlAttribute("OrderCount")]
        [Bindable(true)]
        public int OrderCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_count");
		    }
            set 
		    {
			    SetColumnValue("order_count", value);
            }
        }
	      
        [XmlAttribute("AmountTotal")]
        [Bindable(true)]
        public decimal AmountTotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("amount_total");
		    }
            set 
		    {
			    SetColumnValue("amount_total", value);
            }
        }
	      
        [XmlAttribute("LastUseTime")]
        [Bindable(true)]
        public DateTime? LastUseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("last_use_time");
		    }
            set 
		    {
			    SetColumnValue("last_use_time", value);
            }
        }
	      
        [XmlAttribute("Enabled")]
        [Bindable(true)]
        public bool Enabled 
	    {
		    get
		    {
			    return GetColumnValue<bool>("enabled");
		    }
            set 
		    {
			    SetColumnValue("enabled", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId 
	    {
		    get
		    {
			    return GetColumnValue<int>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public int? ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("RequestTime")]
        [Bindable(true)]
        public DateTime RequestTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("request_time");
		    }
            set 
		    {
			    SetColumnValue("request_time", value);
            }
        }
	      
        [XmlAttribute("CardNo")]
        [Bindable(true)]
        public string CardNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("card_no");
		    }
            set 
		    {
			    SetColumnValue("card_no", value);
            }
        }
	      
        [XmlAttribute("UserSeq")]
        [Bindable(true)]
        public int UserSeq 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_seq");
		    }
            set 
		    {
			    SetColumnValue("user_seq", value);
            }
        }
	      
        [XmlAttribute("VersionId")]
        [Bindable(true)]
        public int VersionId 
	    {
		    get
		    {
			    return GetColumnValue<int>("version_id");
		    }
            set 
		    {
			    SetColumnValue("version_id", value);
            }
        }
	      
        [XmlAttribute("Level")]
        [Bindable(true)]
        public int Level 
	    {
		    get
		    {
			    return GetColumnValue<int>("level");
		    }
            set 
		    {
			    SetColumnValue("level", value);
            }
        }
	      
        [XmlAttribute("PaymentPercent")]
        [Bindable(true)]
        public double PaymentPercent 
	    {
		    get
		    {
			    return GetColumnValue<double>("payment_percent");
		    }
            set 
		    {
			    SetColumnValue("payment_percent", value);
            }
        }
	      
        [XmlAttribute("Instruction")]
        [Bindable(true)]
        public string Instruction 
	    {
		    get
		    {
			    return GetColumnValue<string>("instruction");
		    }
            set 
		    {
			    SetColumnValue("instruction", value);
            }
        }
	      
        [XmlAttribute("Others")]
        [Bindable(true)]
        public string Others 
	    {
		    get
		    {
			    return GetColumnValue<string>("others");
		    }
            set 
		    {
			    SetColumnValue("others", value);
            }
        }
	      
        [XmlAttribute("Image")]
        [Bindable(true)]
        public string Image 
	    {
		    get
		    {
			    return GetColumnValue<string>("image");
		    }
            set 
		    {
			    SetColumnValue("image", value);
            }
        }
	      
        [XmlAttribute("AvailableDateType")]
        [Bindable(true)]
        public int AvailableDateType 
	    {
		    get
		    {
			    return GetColumnValue<int>("available_date_type");
		    }
            set 
		    {
			    SetColumnValue("available_date_type", value);
            }
        }
	      
        [XmlAttribute("OpenTime")]
        [Bindable(true)]
        public DateTime OpenTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("open_time");
		    }
            set 
		    {
			    SetColumnValue("open_time", value);
            }
        }
	      
        [XmlAttribute("CloseTime")]
        [Bindable(true)]
        public DateTime CloseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("close_time");
		    }
            set 
		    {
			    SetColumnValue("close_time", value);
            }
        }
	      
        [XmlAttribute("MembershipCardEnabled")]
        [Bindable(true)]
        public bool MembershipCardEnabled 
	    {
		    get
		    {
			    return GetColumnValue<bool>("membership_card_enabled");
		    }
            set 
		    {
			    SetColumnValue("membership_card_enabled", value);
            }
        }
	      
        [XmlAttribute("ConditionalLogic")]
        [Bindable(true)]
        public int ConditionalLogic 
	    {
		    get
		    {
			    return GetColumnValue<int>("conditional_logic");
		    }
            set 
		    {
			    SetColumnValue("conditional_logic", value);
            }
        }
	      
        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int SellerUserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_user_id");
		    }
            set 
		    {
			    SetColumnValue("seller_user_id", value);
            }
        }
	      
        [XmlAttribute("DepositService")]
        [Bindable(true)]
        public int? DepositService 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deposit_service");
		    }
            set 
		    {
			    SetColumnValue("deposit_service", value);
            }
        }
	      
        [XmlAttribute("PointService")]
        [Bindable(true)]
        public int? PointService 
	    {
		    get
		    {
			    return GetColumnValue<int?>("point_service");
		    }
            set 
		    {
			    SetColumnValue("point_service", value);
            }
        }
	      
        [XmlAttribute("ImagePath")]
        [Bindable(true)]
        public string ImagePath 
	    {
		    get
		    {
			    return GetColumnValue<string>("image_path");
		    }
            set 
		    {
			    SetColumnValue("image_path", value);
            }
        }
	      
        [XmlAttribute("BackgroundColorValue")]
        [Bindable(true)]
        public string BackgroundColorValue 
	    {
		    get
		    {
			    return GetColumnValue<string>("background_color_value");
		    }
            set 
		    {
			    SetColumnValue("background_color_value", value);
            }
        }
	      
        [XmlAttribute("BackgroundImageValue")]
        [Bindable(true)]
        public string BackgroundImageValue 
	    {
		    get
		    {
			    return GetColumnValue<string>("background_image_value");
		    }
            set 
		    {
			    SetColumnValue("background_image_value", value);
            }
        }
	      
        [XmlAttribute("IconImageValue")]
        [Bindable(true)]
        public string IconImageValue 
	    {
		    get
		    {
			    return GetColumnValue<string>("icon_image_value");
		    }
            set 
		    {
			    SetColumnValue("icon_image_value", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("Coordinate")]
        [Bindable(true)]
        public string Coordinate 
	    {
		    get
		    {
			    return GetColumnValue<string>("coordinate");
		    }
            set 
		    {
			    SetColumnValue("coordinate", value);
            }
        }
	      
        [XmlAttribute("CardType")]
        [Bindable(true)]
        public int CardType 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_type");
		    }
            set 
		    {
			    SetColumnValue("card_type", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public byte Status 
	    {
		    get
		    {
			    return GetColumnValue<byte>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string UserId = @"user_id";
            
            public static string CardGroupId = @"card_group_id";
            
            public static string CardId = @"card_id";
            
            public static string OrderCount = @"order_count";
            
            public static string AmountTotal = @"amount_total";
            
            public static string LastUseTime = @"last_use_time";
            
            public static string Enabled = @"enabled";
            
            public static string StartTime = @"start_time";
            
            public static string EndTime = @"end_time";
            
            public static string CreateTime = @"create_time";
            
            public static string CreateId = @"create_id";
            
            public static string ModifyTime = @"modify_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string RequestTime = @"request_time";
            
            public static string CardNo = @"card_no";
            
            public static string UserSeq = @"user_seq";
            
            public static string VersionId = @"version_id";
            
            public static string Level = @"level";
            
            public static string PaymentPercent = @"payment_percent";
            
            public static string Instruction = @"instruction";
            
            public static string Others = @"others";
            
            public static string Image = @"image";
            
            public static string AvailableDateType = @"available_date_type";
            
            public static string OpenTime = @"open_time";
            
            public static string CloseTime = @"close_time";
            
            public static string MembershipCardEnabled = @"membership_card_enabled";
            
            public static string ConditionalLogic = @"conditional_logic";
            
            public static string SellerUserId = @"seller_user_id";
            
            public static string DepositService = @"deposit_service";
            
            public static string PointService = @"point_service";
            
            public static string ImagePath = @"image_path";
            
            public static string BackgroundColorValue = @"background_color_value";
            
            public static string BackgroundImageValue = @"background_image_value";
            
            public static string IconImageValue = @"icon_image_value";
            
            public static string SellerName = @"seller_name";
            
            public static string StoreName = @"store_name";
            
            public static string Coordinate = @"coordinate";
            
            public static string CardType = @"card_type";
            
            public static string Status = @"status";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

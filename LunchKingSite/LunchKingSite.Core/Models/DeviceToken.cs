using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DeviceToken class.
	/// </summary>
    [Serializable]
	public partial class DeviceTokenCollection : RepositoryList<DeviceToken, DeviceTokenCollection>
	{	   
		public DeviceTokenCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DeviceTokenCollection</returns>
		public DeviceTokenCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DeviceToken o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the device_token table.
	/// </summary>
	[Serializable]
	public partial class DeviceToken : RepositoryRecord<DeviceToken>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DeviceToken()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DeviceToken(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("device_token", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarDevice = new TableSchema.TableColumn(schema);
				colvarDevice.ColumnName = "device";
				colvarDevice.DataType = DbType.AnsiString;
				colvarDevice.MaxLength = 50;
				colvarDevice.AutoIncrement = false;
				colvarDevice.IsNullable = false;
				colvarDevice.IsPrimaryKey = false;
				colvarDevice.IsForeignKey = false;
				colvarDevice.IsReadOnly = false;
				colvarDevice.DefaultSetting = @"";
				colvarDevice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDevice);
				
				TableSchema.TableColumn colvarToken = new TableSchema.TableColumn(schema);
				colvarToken.ColumnName = "token";
				colvarToken.DataType = DbType.String;
				colvarToken.MaxLength = 256;
				colvarToken.AutoIncrement = false;
				colvarToken.IsNullable = false;
				colvarToken.IsPrimaryKey = false;
				colvarToken.IsForeignKey = false;
				colvarToken.IsReadOnly = false;
				colvarToken.DefaultSetting = @"";
				colvarToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarToken);
				
				TableSchema.TableColumn colvarTokenStatus = new TableSchema.TableColumn(schema);
				colvarTokenStatus.ColumnName = "token_status";
				colvarTokenStatus.DataType = DbType.Int32;
				colvarTokenStatus.MaxLength = 0;
				colvarTokenStatus.AutoIncrement = false;
				colvarTokenStatus.IsNullable = false;
				colvarTokenStatus.IsPrimaryKey = false;
				colvarTokenStatus.IsForeignKey = false;
				colvarTokenStatus.IsReadOnly = false;
				colvarTokenStatus.DefaultSetting = @"";
				colvarTokenStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTokenStatus);
				
				TableSchema.TableColumn colvarMobileOsType = new TableSchema.TableColumn(schema);
				colvarMobileOsType.ColumnName = "mobile_os_type";
				colvarMobileOsType.DataType = DbType.Int32;
				colvarMobileOsType.MaxLength = 0;
				colvarMobileOsType.AutoIncrement = false;
				colvarMobileOsType.IsNullable = false;
				colvarMobileOsType.IsPrimaryKey = false;
				colvarMobileOsType.IsForeignKey = false;
				colvarMobileOsType.IsReadOnly = false;
				colvarMobileOsType.DefaultSetting = @"";
				colvarMobileOsType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileOsType);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("device_token",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Device")]
		[Bindable(true)]
		public string Device 
		{
			get { return GetColumnValue<string>(Columns.Device); }
			set { SetColumnValue(Columns.Device, value); }
		}
		  
		[XmlAttribute("Token")]
		[Bindable(true)]
		public string Token 
		{
			get { return GetColumnValue<string>(Columns.Token); }
			set { SetColumnValue(Columns.Token, value); }
		}
		  
		[XmlAttribute("TokenStatus")]
		[Bindable(true)]
		public int TokenStatus 
		{
			get { return GetColumnValue<int>(Columns.TokenStatus); }
			set { SetColumnValue(Columns.TokenStatus, value); }
		}
		  
		[XmlAttribute("MobileOsType")]
		[Bindable(true)]
		public int MobileOsType 
		{
			get { return GetColumnValue<int>(Columns.MobileOsType); }
			set { SetColumnValue(Columns.MobileOsType, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DeviceColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TokenColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn TokenStatusColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn MobileOsTypeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Device = @"device";
			 public static string Token = @"token";
			 public static string TokenStatus = @"token_status";
			 public static string MobileOsType = @"mobile_os_type";
			 public static string CreateTime = @"create_time";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewSellerCategoryCity class.
    /// </summary>
    [Serializable]
    public partial class ViewSellerCategoryCityCollection : ReadOnlyList<ViewSellerCategoryCity, ViewSellerCategoryCityCollection>
    {        
        public ViewSellerCategoryCityCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_seller_category_city view.
    /// </summary>
    [Serializable]
    public partial class ViewSellerCategoryCity : ReadOnlyRecord<ViewSellerCategoryCity>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_seller_category_city", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerAddress = new TableSchema.TableColumn(schema);
                colvarSellerAddress.ColumnName = "seller_address";
                colvarSellerAddress.DataType = DbType.String;
                colvarSellerAddress.MaxLength = 100;
                colvarSellerAddress.AutoIncrement = false;
                colvarSellerAddress.IsNullable = true;
                colvarSellerAddress.IsPrimaryKey = false;
                colvarSellerAddress.IsForeignKey = false;
                colvarSellerAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerAddress);
                
                TableSchema.TableColumn colvarSellerStatus = new TableSchema.TableColumn(schema);
                colvarSellerStatus.ColumnName = "seller_status";
                colvarSellerStatus.DataType = DbType.Int32;
                colvarSellerStatus.MaxLength = 0;
                colvarSellerStatus.AutoIncrement = false;
                colvarSellerStatus.IsNullable = false;
                colvarSellerStatus.IsPrimaryKey = false;
                colvarSellerStatus.IsForeignKey = false;
                colvarSellerStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerStatus);
                
                TableSchema.TableColumn colvarSellerSales = new TableSchema.TableColumn(schema);
                colvarSellerSales.ColumnName = "seller_sales";
                colvarSellerSales.DataType = DbType.String;
                colvarSellerSales.MaxLength = 50;
                colvarSellerSales.AutoIncrement = false;
                colvarSellerSales.IsNullable = true;
                colvarSellerSales.IsPrimaryKey = false;
                colvarSellerSales.IsForeignKey = false;
                colvarSellerSales.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerSales);
                
                TableSchema.TableColumn colvarSellerLogoimgPath = new TableSchema.TableColumn(schema);
                colvarSellerLogoimgPath.ColumnName = "seller_logoimg_path";
                colvarSellerLogoimgPath.DataType = DbType.AnsiString;
                colvarSellerLogoimgPath.MaxLength = 500;
                colvarSellerLogoimgPath.AutoIncrement = false;
                colvarSellerLogoimgPath.IsNullable = true;
                colvarSellerLogoimgPath.IsPrimaryKey = false;
                colvarSellerLogoimgPath.IsForeignKey = false;
                colvarSellerLogoimgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerLogoimgPath);
                
                TableSchema.TableColumn colvarSellerVideoPath = new TableSchema.TableColumn(schema);
                colvarSellerVideoPath.ColumnName = "seller_video_path";
                colvarSellerVideoPath.DataType = DbType.AnsiString;
                colvarSellerVideoPath.MaxLength = 100;
                colvarSellerVideoPath.AutoIncrement = false;
                colvarSellerVideoPath.IsNullable = true;
                colvarSellerVideoPath.IsPrimaryKey = false;
                colvarSellerVideoPath.IsForeignKey = false;
                colvarSellerVideoPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerVideoPath);
                
                TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
                colvarCategoryId.ColumnName = "category_id";
                colvarCategoryId.DataType = DbType.Int32;
                colvarCategoryId.MaxLength = 0;
                colvarCategoryId.AutoIncrement = false;
                colvarCategoryId.IsNullable = false;
                colvarCategoryId.IsPrimaryKey = false;
                colvarCategoryId.IsForeignKey = false;
                colvarCategoryId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryId);
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 20;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = false;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
                colvarDepartment.ColumnName = "department";
                colvarDepartment.DataType = DbType.Int32;
                colvarDepartment.MaxLength = 0;
                colvarDepartment.AutoIncrement = false;
                colvarDepartment.IsNullable = false;
                colvarDepartment.IsPrimaryKey = false;
                colvarDepartment.IsForeignKey = false;
                colvarDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepartment);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_seller_category_city",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewSellerCategoryCity()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewSellerCategoryCity(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewSellerCategoryCity(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewSellerCategoryCity(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerAddress")]
        [Bindable(true)]
        public string SellerAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_address");
		    }
            set 
		    {
			    SetColumnValue("seller_address", value);
            }
        }
	      
        [XmlAttribute("SellerStatus")]
        [Bindable(true)]
        public int SellerStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_status");
		    }
            set 
		    {
			    SetColumnValue("seller_status", value);
            }
        }
	      
        [XmlAttribute("SellerSales")]
        [Bindable(true)]
        public string SellerSales 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_sales");
		    }
            set 
		    {
			    SetColumnValue("seller_sales", value);
            }
        }
	      
        [XmlAttribute("SellerLogoimgPath")]
        [Bindable(true)]
        public string SellerLogoimgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_logoimg_path");
		    }
            set 
		    {
			    SetColumnValue("seller_logoimg_path", value);
            }
        }
	      
        [XmlAttribute("SellerVideoPath")]
        [Bindable(true)]
        public string SellerVideoPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_video_path");
		    }
            set 
		    {
			    SetColumnValue("seller_video_path", value);
            }
        }
	      
        [XmlAttribute("CategoryId")]
        [Bindable(true)]
        public int CategoryId 
	    {
		    get
		    {
			    return GetColumnValue<int>("category_id");
		    }
            set 
		    {
			    SetColumnValue("category_id", value);
            }
        }
	      
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	      
        [XmlAttribute("Department")]
        [Bindable(true)]
        public int Department 
	    {
		    get
		    {
			    return GetColumnValue<int>("department");
		    }
            set 
		    {
			    SetColumnValue("department", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string SellerGuid = @"seller_guid";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerAddress = @"seller_address";
            
            public static string SellerStatus = @"seller_status";
            
            public static string SellerSales = @"seller_sales";
            
            public static string SellerLogoimgPath = @"seller_logoimg_path";
            
            public static string SellerVideoPath = @"seller_video_path";
            
            public static string CategoryId = @"category_id";
            
            public static string CityName = @"city_name";
            
            public static string Department = @"department";
            
            public static string CityId = @"city_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

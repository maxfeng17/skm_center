using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the StoreCategory class.
	/// </summary>
    [Serializable]
	public partial class StoreCategoryCollection : RepositoryList<StoreCategory, StoreCategoryCollection>
	{	   
		public StoreCategoryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>StoreCategoryCollection</returns>
		public StoreCategoryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                StoreCategory o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the store_category table.
	/// </summary>
	[Serializable]
	public partial class StoreCategory : RepositoryRecord<StoreCategory>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public StoreCategory()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public StoreCategory(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("store_category", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = false;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarCategoryCode = new TableSchema.TableColumn(schema);
				colvarCategoryCode.ColumnName = "category_code";
				colvarCategoryCode.DataType = DbType.Int32;
				colvarCategoryCode.MaxLength = 0;
				colvarCategoryCode.AutoIncrement = false;
				colvarCategoryCode.IsNullable = false;
				colvarCategoryCode.IsPrimaryKey = false;
				colvarCategoryCode.IsForeignKey = false;
				colvarCategoryCode.IsReadOnly = false;
				colvarCategoryCode.DefaultSetting = @"";
				colvarCategoryCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryCode);
				
				TableSchema.TableColumn colvarValid = new TableSchema.TableColumn(schema);
				colvarValid.ColumnName = "valid";
				colvarValid.DataType = DbType.Int32;
				colvarValid.MaxLength = 0;
				colvarValid.AutoIncrement = false;
				colvarValid.IsNullable = false;
				colvarValid.IsPrimaryKey = false;
				colvarValid.IsForeignKey = false;
				colvarValid.IsReadOnly = false;
				
						colvarValid.DefaultSetting = @"((0))";
				colvarValid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValid);
				
				TableSchema.TableColumn colvarCreateType = new TableSchema.TableColumn(schema);
				colvarCreateType.ColumnName = "create_type";
				colvarCreateType.DataType = DbType.Int32;
				colvarCreateType.MaxLength = 0;
				colvarCreateType.AutoIncrement = false;
				colvarCreateType.IsNullable = false;
				colvarCreateType.IsPrimaryKey = false;
				colvarCreateType.IsForeignKey = false;
				colvarCreateType.IsReadOnly = false;
				
						colvarCreateType.DefaultSetting = @"((0))";
				colvarCreateType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateType);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("store_category",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid StoreGuid 
		{
			get { return GetColumnValue<Guid>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("CategoryCode")]
		[Bindable(true)]
		public int CategoryCode 
		{
			get { return GetColumnValue<int>(Columns.CategoryCode); }
			set { SetColumnValue(Columns.CategoryCode, value); }
		}
		  
		[XmlAttribute("Valid")]
		[Bindable(true)]
		public int Valid 
		{
			get { return GetColumnValue<int>(Columns.Valid); }
			set { SetColumnValue(Columns.Valid, value); }
		}
		  
		[XmlAttribute("CreateType")]
		[Bindable(true)]
		public int CreateType 
		{
			get { return GetColumnValue<int>(Columns.CreateType); }
			set { SetColumnValue(Columns.CreateType, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryCodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ValidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTypeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string StoreGuid = @"store_guid";
			 public static string CategoryCode = @"category_code";
			 public static string Valid = @"valid";
			 public static string CreateType = @"create_type";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

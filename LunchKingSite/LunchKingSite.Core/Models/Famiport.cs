using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Famiport class.
	/// </summary>
    [Serializable]
	public partial class FamiportCollection : RepositoryList<Famiport, FamiportCollection>
	{	   
		public FamiportCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FamiportCollection</returns>
		public FamiportCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Famiport o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the famiport table.
	/// </summary>
	[Serializable]
	public partial class Famiport : RepositoryRecord<Famiport>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Famiport()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Famiport(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("famiport", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
				colvarEventId.ColumnName = "event_id";
				colvarEventId.DataType = DbType.String;
				colvarEventId.MaxLength = 12;
				colvarEventId.AutoIncrement = false;
				colvarEventId.IsNullable = false;
				colvarEventId.IsPrimaryKey = false;
				colvarEventId.IsForeignKey = false;
				colvarEventId.IsReadOnly = false;
				colvarEventId.DefaultSetting = @"";
				colvarEventId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventId);
				
				TableSchema.TableColumn colvarPassword = new TableSchema.TableColumn(schema);
				colvarPassword.ColumnName = "password";
				colvarPassword.DataType = DbType.AnsiString;
				colvarPassword.MaxLength = 30;
				colvarPassword.AutoIncrement = false;
				colvarPassword.IsNullable = true;
				colvarPassword.IsPrimaryKey = false;
				colvarPassword.IsForeignKey = false;
				colvarPassword.IsReadOnly = false;
				colvarPassword.DefaultSetting = @"";
				colvarPassword.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPassword);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
				colvarRemark.ColumnName = "remark";
				colvarRemark.DataType = DbType.String;
				colvarRemark.MaxLength = 500;
				colvarRemark.AutoIncrement = false;
				colvarRemark.IsNullable = true;
				colvarRemark.IsPrimaryKey = false;
				colvarRemark.IsForeignKey = false;
				colvarRemark.IsReadOnly = false;
				colvarRemark.DefaultSetting = @"";
				colvarRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemark);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarLatestUpdateTime = new TableSchema.TableColumn(schema);
				colvarLatestUpdateTime.ColumnName = "latest_update_time";
				colvarLatestUpdateTime.DataType = DbType.DateTime;
				colvarLatestUpdateTime.MaxLength = 0;
				colvarLatestUpdateTime.AutoIncrement = false;
				colvarLatestUpdateTime.IsNullable = true;
				colvarLatestUpdateTime.IsPrimaryKey = false;
				colvarLatestUpdateTime.IsForeignKey = false;
				colvarLatestUpdateTime.IsReadOnly = false;
				colvarLatestUpdateTime.DefaultSetting = @"";
				colvarLatestUpdateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLatestUpdateTime);
				
				TableSchema.TableColumn colvarIsImportSequencenumber = new TableSchema.TableColumn(schema);
				colvarIsImportSequencenumber.ColumnName = "is_import_sequencenumber";
				colvarIsImportSequencenumber.DataType = DbType.Boolean;
				colvarIsImportSequencenumber.MaxLength = 0;
				colvarIsImportSequencenumber.AutoIncrement = false;
				colvarIsImportSequencenumber.IsNullable = false;
				colvarIsImportSequencenumber.IsPrimaryKey = false;
				colvarIsImportSequencenumber.IsForeignKey = false;
				colvarIsImportSequencenumber.IsReadOnly = false;
				
						colvarIsImportSequencenumber.DefaultSetting = @"((0))";
				colvarIsImportSequencenumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsImportSequencenumber);
				
				TableSchema.TableColumn colvarVendorId = new TableSchema.TableColumn(schema);
				colvarVendorId.ColumnName = "vendor_id";
				colvarVendorId.DataType = DbType.AnsiString;
				colvarVendorId.MaxLength = 15;
				colvarVendorId.AutoIncrement = false;
				colvarVendorId.IsNullable = true;
				colvarVendorId.IsPrimaryKey = false;
				colvarVendorId.IsForeignKey = false;
				colvarVendorId.IsReadOnly = false;
				colvarVendorId.DefaultSetting = @"";
				colvarVendorId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorId);
				
				TableSchema.TableColumn colvarEventAttention1 = new TableSchema.TableColumn(schema);
				colvarEventAttention1.ColumnName = "event_attention_1";
				colvarEventAttention1.DataType = DbType.String;
				colvarEventAttention1.MaxLength = 80;
				colvarEventAttention1.AutoIncrement = false;
				colvarEventAttention1.IsNullable = true;
				colvarEventAttention1.IsPrimaryKey = false;
				colvarEventAttention1.IsForeignKey = false;
				colvarEventAttention1.IsReadOnly = false;
				colvarEventAttention1.DefaultSetting = @"";
				colvarEventAttention1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventAttention1);
				
				TableSchema.TableColumn colvarEventAttention2 = new TableSchema.TableColumn(schema);
				colvarEventAttention2.ColumnName = "event_attention_2";
				colvarEventAttention2.DataType = DbType.String;
				colvarEventAttention2.MaxLength = 80;
				colvarEventAttention2.AutoIncrement = false;
				colvarEventAttention2.IsNullable = true;
				colvarEventAttention2.IsPrimaryKey = false;
				colvarEventAttention2.IsForeignKey = false;
				colvarEventAttention2.IsReadOnly = false;
				colvarEventAttention2.DefaultSetting = @"";
				colvarEventAttention2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventAttention2);
				
				TableSchema.TableColumn colvarEventAttention3 = new TableSchema.TableColumn(schema);
				colvarEventAttention3.ColumnName = "event_attention_3";
				colvarEventAttention3.DataType = DbType.String;
				colvarEventAttention3.MaxLength = 80;
				colvarEventAttention3.AutoIncrement = false;
				colvarEventAttention3.IsNullable = true;
				colvarEventAttention3.IsPrimaryKey = false;
				colvarEventAttention3.IsForeignKey = false;
				colvarEventAttention3.IsReadOnly = false;
				colvarEventAttention3.DefaultSetting = @"";
				colvarEventAttention3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventAttention3);
				
				TableSchema.TableColumn colvarEventAttention4 = new TableSchema.TableColumn(schema);
				colvarEventAttention4.ColumnName = "event_attention_4";
				colvarEventAttention4.DataType = DbType.String;
				colvarEventAttention4.MaxLength = 80;
				colvarEventAttention4.AutoIncrement = false;
				colvarEventAttention4.IsNullable = true;
				colvarEventAttention4.IsPrimaryKey = false;
				colvarEventAttention4.IsForeignKey = false;
				colvarEventAttention4.IsReadOnly = false;
				colvarEventAttention4.DefaultSetting = @"";
				colvarEventAttention4.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventAttention4);
				
				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 15;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = true;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);
				
				TableSchema.TableColumn colvarItemCode = new TableSchema.TableColumn(schema);
				colvarItemCode.ColumnName = "item_code";
				colvarItemCode.DataType = DbType.AnsiString;
				colvarItemCode.MaxLength = 7;
				colvarItemCode.AutoIncrement = false;
				colvarItemCode.IsNullable = true;
				colvarItemCode.IsPrimaryKey = false;
				colvarItemCode.IsForeignKey = false;
				colvarItemCode.IsReadOnly = false;
				colvarItemCode.DefaultSetting = @"";
				colvarItemCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemCode);
				
				TableSchema.TableColumn colvarPincodeQty = new TableSchema.TableColumn(schema);
				colvarPincodeQty.ColumnName = "pincode_qty";
				colvarPincodeQty.DataType = DbType.Int32;
				colvarPincodeQty.MaxLength = 0;
				colvarPincodeQty.AutoIncrement = false;
				colvarPincodeQty.IsNullable = false;
				colvarPincodeQty.IsPrimaryKey = false;
				colvarPincodeQty.IsForeignKey = false;
				colvarPincodeQty.IsReadOnly = false;
				
						colvarPincodeQty.DefaultSetting = @"((0))";
				colvarPincodeQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPincodeQty);
				
				TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
				colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeS.MaxLength = 0;
				colvarBusinessHourOrderTimeS.AutoIncrement = false;
				colvarBusinessHourOrderTimeS.IsNullable = true;
				colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeS.IsForeignKey = false;
				colvarBusinessHourOrderTimeS.IsReadOnly = false;
				colvarBusinessHourOrderTimeS.DefaultSetting = @"";
				colvarBusinessHourOrderTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeS);
				
				TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
				colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeE.MaxLength = 0;
				colvarBusinessHourOrderTimeE.AutoIncrement = false;
				colvarBusinessHourOrderTimeE.IsNullable = true;
				colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeE.IsForeignKey = false;
				colvarBusinessHourOrderTimeE.IsReadOnly = false;
				colvarBusinessHourOrderTimeE.DefaultSetting = @"";
				colvarBusinessHourOrderTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeE);
				
				TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
				colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeS.MaxLength = 0;
				colvarBusinessHourDeliverTimeS.AutoIncrement = false;
				colvarBusinessHourDeliverTimeS.IsNullable = true;
				colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeS.IsForeignKey = false;
				colvarBusinessHourDeliverTimeS.IsReadOnly = false;
				colvarBusinessHourDeliverTimeS.DefaultSetting = @"";
				colvarBusinessHourDeliverTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeS);
				
				TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
				colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeE.MaxLength = 0;
				colvarBusinessHourDeliverTimeE.AutoIncrement = false;
				colvarBusinessHourDeliverTimeE.IsNullable = true;
				colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeE.IsForeignKey = false;
				colvarBusinessHourDeliverTimeE.IsReadOnly = false;
				colvarBusinessHourDeliverTimeE.DefaultSetting = @"";
				colvarBusinessHourDeliverTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeE);
				
				TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
				colvarItemOrigPrice.ColumnName = "item_orig_price";
				colvarItemOrigPrice.DataType = DbType.Currency;
				colvarItemOrigPrice.MaxLength = 0;
				colvarItemOrigPrice.AutoIncrement = false;
				colvarItemOrigPrice.IsNullable = false;
				colvarItemOrigPrice.IsPrimaryKey = false;
				colvarItemOrigPrice.IsForeignKey = false;
				colvarItemOrigPrice.IsReadOnly = false;
				
						colvarItemOrigPrice.DefaultSetting = @"((0))";
				colvarItemOrigPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemOrigPrice);
				
				TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
				colvarItemPrice.ColumnName = "item_price";
				colvarItemPrice.DataType = DbType.Currency;
				colvarItemPrice.MaxLength = 0;
				colvarItemPrice.AutoIncrement = false;
				colvarItemPrice.IsNullable = false;
				colvarItemPrice.IsPrimaryKey = false;
				colvarItemPrice.IsForeignKey = false;
				colvarItemPrice.IsReadOnly = false;
				
						colvarItemPrice.DefaultSetting = @"((0))";
				colvarItemPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemPrice);
				
				TableSchema.TableColumn colvarItemAttention1 = new TableSchema.TableColumn(schema);
				colvarItemAttention1.ColumnName = "item_attention_1";
				colvarItemAttention1.DataType = DbType.String;
				colvarItemAttention1.MaxLength = 80;
				colvarItemAttention1.AutoIncrement = false;
				colvarItemAttention1.IsNullable = true;
				colvarItemAttention1.IsPrimaryKey = false;
				colvarItemAttention1.IsForeignKey = false;
				colvarItemAttention1.IsReadOnly = false;
				colvarItemAttention1.DefaultSetting = @"";
				colvarItemAttention1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemAttention1);
				
				TableSchema.TableColumn colvarItemAttention2 = new TableSchema.TableColumn(schema);
				colvarItemAttention2.ColumnName = "item_attention_2";
				colvarItemAttention2.DataType = DbType.String;
				colvarItemAttention2.MaxLength = 80;
				colvarItemAttention2.AutoIncrement = false;
				colvarItemAttention2.IsNullable = true;
				colvarItemAttention2.IsPrimaryKey = false;
				colvarItemAttention2.IsForeignKey = false;
				colvarItemAttention2.IsReadOnly = false;
				colvarItemAttention2.DefaultSetting = @"";
				colvarItemAttention2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemAttention2);
				
				TableSchema.TableColumn colvarItemAttention3 = new TableSchema.TableColumn(schema);
				colvarItemAttention3.ColumnName = "item_attention_3";
				colvarItemAttention3.DataType = DbType.String;
				colvarItemAttention3.MaxLength = 80;
				colvarItemAttention3.AutoIncrement = false;
				colvarItemAttention3.IsNullable = true;
				colvarItemAttention3.IsPrimaryKey = false;
				colvarItemAttention3.IsForeignKey = false;
				colvarItemAttention3.IsReadOnly = false;
				colvarItemAttention3.DefaultSetting = @"";
				colvarItemAttention3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemAttention3);
				
				TableSchema.TableColumn colvarItemAttention4 = new TableSchema.TableColumn(schema);
				colvarItemAttention4.ColumnName = "item_attention_4";
				colvarItemAttention4.DataType = DbType.String;
				colvarItemAttention4.MaxLength = 80;
				colvarItemAttention4.AutoIncrement = false;
				colvarItemAttention4.IsNullable = true;
				colvarItemAttention4.IsPrimaryKey = false;
				colvarItemAttention4.IsForeignKey = false;
				colvarItemAttention4.IsReadOnly = false;
				colvarItemAttention4.DefaultSetting = @"";
				colvarItemAttention4.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemAttention4);
				
				TableSchema.TableColumn colvarItemRemark = new TableSchema.TableColumn(schema);
				colvarItemRemark.ColumnName = "item_remark";
				colvarItemRemark.DataType = DbType.String;
				colvarItemRemark.MaxLength = -1;
				colvarItemRemark.AutoIncrement = false;
				colvarItemRemark.IsNullable = true;
				colvarItemRemark.IsPrimaryKey = false;
				colvarItemRemark.IsForeignKey = false;
				colvarItemRemark.IsReadOnly = false;
				colvarItemRemark.DefaultSetting = @"";
				colvarItemRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemRemark);
				
				TableSchema.TableColumn colvarIsOutOfDate = new TableSchema.TableColumn(schema);
				colvarIsOutOfDate.ColumnName = "is_out_of_date";
				colvarIsOutOfDate.DataType = DbType.Boolean;
				colvarIsOutOfDate.MaxLength = 0;
				colvarIsOutOfDate.AutoIncrement = false;
				colvarIsOutOfDate.IsNullable = false;
				colvarIsOutOfDate.IsPrimaryKey = false;
				colvarIsOutOfDate.IsForeignKey = false;
				colvarIsOutOfDate.IsReadOnly = false;
				
						colvarIsOutOfDate.DefaultSetting = @"((1))";
				colvarIsOutOfDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOutOfDate);
				
				TableSchema.TableColumn colvarIsProducedCode = new TableSchema.TableColumn(schema);
				colvarIsProducedCode.ColumnName = "is_produced_code";
				colvarIsProducedCode.DataType = DbType.Boolean;
				colvarIsProducedCode.MaxLength = 0;
				colvarIsProducedCode.AutoIncrement = false;
				colvarIsProducedCode.IsNullable = false;
				colvarIsProducedCode.IsPrimaryKey = false;
				colvarIsProducedCode.IsForeignKey = false;
				colvarIsProducedCode.IsReadOnly = false;
				
						colvarIsProducedCode.DefaultSetting = @"((1))";
				colvarIsProducedCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsProducedCode);
				
				TableSchema.TableColumn colvarTestCodeQty = new TableSchema.TableColumn(schema);
				colvarTestCodeQty.ColumnName = "test_code_qty";
				colvarTestCodeQty.DataType = DbType.Int32;
				colvarTestCodeQty.MaxLength = 0;
				colvarTestCodeQty.AutoIncrement = false;
				colvarTestCodeQty.IsNullable = false;
				colvarTestCodeQty.IsPrimaryKey = false;
				colvarTestCodeQty.IsForeignKey = false;
				colvarTestCodeQty.IsReadOnly = false;
				
						colvarTestCodeQty.DefaultSetting = @"((0))";
				colvarTestCodeQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTestCodeQty);
				
				TableSchema.TableColumn colvarDisplayType = new TableSchema.TableColumn(schema);
				colvarDisplayType.ColumnName = "display_type";
				colvarDisplayType.DataType = DbType.Int32;
				colvarDisplayType.MaxLength = 0;
				colvarDisplayType.AutoIncrement = false;
				colvarDisplayType.IsNullable = false;
				colvarDisplayType.IsPrimaryKey = false;
				colvarDisplayType.IsForeignKey = false;
				colvarDisplayType.IsReadOnly = false;
				
						colvarDisplayType.DefaultSetting = @"((0))";
				colvarDisplayType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDisplayType);
				
				TableSchema.TableColumn colvarIsLock = new TableSchema.TableColumn(schema);
				colvarIsLock.ColumnName = "is_lock";
				colvarIsLock.DataType = DbType.Boolean;
				colvarIsLock.MaxLength = 0;
				colvarIsLock.AutoIncrement = false;
				colvarIsLock.IsNullable = false;
				colvarIsLock.IsPrimaryKey = false;
				colvarIsLock.IsForeignKey = false;
				colvarIsLock.IsReadOnly = false;
				
						colvarIsLock.DefaultSetting = @"((0))";
				colvarIsLock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsLock);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("famiport",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("EventId")]
		[Bindable(true)]
		public string EventId 
		{
			get { return GetColumnValue<string>(Columns.EventId); }
			set { SetColumnValue(Columns.EventId, value); }
		}
		  
		[XmlAttribute("Password")]
		[Bindable(true)]
		public string Password 
		{
			get { return GetColumnValue<string>(Columns.Password); }
			set { SetColumnValue(Columns.Password, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Remark")]
		[Bindable(true)]
		public string Remark 
		{
			get { return GetColumnValue<string>(Columns.Remark); }
			set { SetColumnValue(Columns.Remark, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("LatestUpdateTime")]
		[Bindable(true)]
		public DateTime? LatestUpdateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.LatestUpdateTime); }
			set { SetColumnValue(Columns.LatestUpdateTime, value); }
		}
		  
		[XmlAttribute("IsImportSequencenumber")]
		[Bindable(true)]
		public bool IsImportSequencenumber 
		{
			get { return GetColumnValue<bool>(Columns.IsImportSequencenumber); }
			set { SetColumnValue(Columns.IsImportSequencenumber, value); }
		}
		  
		[XmlAttribute("VendorId")]
		[Bindable(true)]
		public string VendorId 
		{
			get { return GetColumnValue<string>(Columns.VendorId); }
			set { SetColumnValue(Columns.VendorId, value); }
		}
		  
		[XmlAttribute("EventAttention1")]
		[Bindable(true)]
		public string EventAttention1 
		{
			get { return GetColumnValue<string>(Columns.EventAttention1); }
			set { SetColumnValue(Columns.EventAttention1, value); }
		}
		  
		[XmlAttribute("EventAttention2")]
		[Bindable(true)]
		public string EventAttention2 
		{
			get { return GetColumnValue<string>(Columns.EventAttention2); }
			set { SetColumnValue(Columns.EventAttention2, value); }
		}
		  
		[XmlAttribute("EventAttention3")]
		[Bindable(true)]
		public string EventAttention3 
		{
			get { return GetColumnValue<string>(Columns.EventAttention3); }
			set { SetColumnValue(Columns.EventAttention3, value); }
		}
		  
		[XmlAttribute("EventAttention4")]
		[Bindable(true)]
		public string EventAttention4 
		{
			get { return GetColumnValue<string>(Columns.EventAttention4); }
			set { SetColumnValue(Columns.EventAttention4, value); }
		}
		  
		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName 
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}
		  
		[XmlAttribute("ItemCode")]
		[Bindable(true)]
		public string ItemCode 
		{
			get { return GetColumnValue<string>(Columns.ItemCode); }
			set { SetColumnValue(Columns.ItemCode, value); }
		}
		  
		[XmlAttribute("PincodeQty")]
		[Bindable(true)]
		public int PincodeQty 
		{
			get { return GetColumnValue<int>(Columns.PincodeQty); }
			set { SetColumnValue(Columns.PincodeQty, value); }
		}
		  
		[XmlAttribute("BusinessHourOrderTimeS")]
		[Bindable(true)]
		public DateTime? BusinessHourOrderTimeS 
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourOrderTimeS); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeS, value); }
		}
		  
		[XmlAttribute("BusinessHourOrderTimeE")]
		[Bindable(true)]
		public DateTime? BusinessHourOrderTimeE 
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourOrderTimeE); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeE, value); }
		}
		  
		[XmlAttribute("BusinessHourDeliverTimeS")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeS 
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeS); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeS, value); }
		}
		  
		[XmlAttribute("BusinessHourDeliverTimeE")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeE 
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeE); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeE, value); }
		}
		  
		[XmlAttribute("ItemOrigPrice")]
		[Bindable(true)]
		public decimal ItemOrigPrice 
		{
			get { return GetColumnValue<decimal>(Columns.ItemOrigPrice); }
			set { SetColumnValue(Columns.ItemOrigPrice, value); }
		}
		  
		[XmlAttribute("ItemPrice")]
		[Bindable(true)]
		public decimal ItemPrice 
		{
			get { return GetColumnValue<decimal>(Columns.ItemPrice); }
			set { SetColumnValue(Columns.ItemPrice, value); }
		}
		  
		[XmlAttribute("ItemAttention1")]
		[Bindable(true)]
		public string ItemAttention1 
		{
			get { return GetColumnValue<string>(Columns.ItemAttention1); }
			set { SetColumnValue(Columns.ItemAttention1, value); }
		}
		  
		[XmlAttribute("ItemAttention2")]
		[Bindable(true)]
		public string ItemAttention2 
		{
			get { return GetColumnValue<string>(Columns.ItemAttention2); }
			set { SetColumnValue(Columns.ItemAttention2, value); }
		}
		  
		[XmlAttribute("ItemAttention3")]
		[Bindable(true)]
		public string ItemAttention3 
		{
			get { return GetColumnValue<string>(Columns.ItemAttention3); }
			set { SetColumnValue(Columns.ItemAttention3, value); }
		}
		  
		[XmlAttribute("ItemAttention4")]
		[Bindable(true)]
		public string ItemAttention4 
		{
			get { return GetColumnValue<string>(Columns.ItemAttention4); }
			set { SetColumnValue(Columns.ItemAttention4, value); }
		}
		  
		[XmlAttribute("ItemRemark")]
		[Bindable(true)]
		public string ItemRemark 
		{
			get { return GetColumnValue<string>(Columns.ItemRemark); }
			set { SetColumnValue(Columns.ItemRemark, value); }
		}
		  
		[XmlAttribute("IsOutOfDate")]
		[Bindable(true)]
		public bool IsOutOfDate 
		{
			get { return GetColumnValue<bool>(Columns.IsOutOfDate); }
			set { SetColumnValue(Columns.IsOutOfDate, value); }
		}
		  
		[XmlAttribute("IsProducedCode")]
		[Bindable(true)]
		public bool IsProducedCode 
		{
			get { return GetColumnValue<bool>(Columns.IsProducedCode); }
			set { SetColumnValue(Columns.IsProducedCode, value); }
		}
		  
		[XmlAttribute("TestCodeQty")]
		[Bindable(true)]
		public int TestCodeQty 
		{
			get { return GetColumnValue<int>(Columns.TestCodeQty); }
			set { SetColumnValue(Columns.TestCodeQty, value); }
		}
		  
		[XmlAttribute("DisplayType")]
		[Bindable(true)]
		public int DisplayType 
		{
			get { return GetColumnValue<int>(Columns.DisplayType); }
			set { SetColumnValue(Columns.DisplayType, value); }
		}
		  
		[XmlAttribute("IsLock")]
		[Bindable(true)]
		public bool IsLock 
		{
			get { return GetColumnValue<bool>(Columns.IsLock); }
			set { SetColumnValue(Columns.IsLock, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn EventIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn LatestUpdateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IsImportSequencenumberColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn EventAttention1Column
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn EventAttention2Column
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn EventAttention3Column
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn EventAttention4Column
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemCodeColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn PincodeQtyColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourOrderTimeSColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourOrderTimeEColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourDeliverTimeSColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourDeliverTimeEColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemOrigPriceColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemPriceColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemAttention1Column
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemAttention2Column
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemAttention3Column
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemAttention4Column
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemRemarkColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOutOfDateColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn IsProducedCodeColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn TestCodeQtyColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn DisplayTypeColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn IsLockColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string EventId = @"event_id";
			 public static string Password = @"password";
			 public static string Type = @"type";
			 public static string Status = @"status";
			 public static string Remark = @"remark";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string LatestUpdateTime = @"latest_update_time";
			 public static string IsImportSequencenumber = @"is_import_sequencenumber";
			 public static string VendorId = @"vendor_id";
			 public static string EventAttention1 = @"event_attention_1";
			 public static string EventAttention2 = @"event_attention_2";
			 public static string EventAttention3 = @"event_attention_3";
			 public static string EventAttention4 = @"event_attention_4";
			 public static string ItemName = @"item_name";
			 public static string ItemCode = @"item_code";
			 public static string PincodeQty = @"pincode_qty";
			 public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
			 public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
			 public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
			 public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
			 public static string ItemOrigPrice = @"item_orig_price";
			 public static string ItemPrice = @"item_price";
			 public static string ItemAttention1 = @"item_attention_1";
			 public static string ItemAttention2 = @"item_attention_2";
			 public static string ItemAttention3 = @"item_attention_3";
			 public static string ItemAttention4 = @"item_attention_4";
			 public static string ItemRemark = @"item_remark";
			 public static string IsOutOfDate = @"is_out_of_date";
			 public static string IsProducedCode = @"is_produced_code";
			 public static string TestCodeQty = @"test_code_qty";
			 public static string DisplayType = @"display_type";
			 public static string IsLock = @"is_lock";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

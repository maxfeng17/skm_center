using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MembershipCardGroupVersion class.
	/// </summary>
    [Serializable]
	public partial class MembershipCardGroupVersionCollection : RepositoryList<MembershipCardGroupVersion, MembershipCardGroupVersionCollection>
	{	   
		public MembershipCardGroupVersionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MembershipCardGroupVersionCollection</returns>
		public MembershipCardGroupVersionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MembershipCardGroupVersion o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the membership_card_group_version table.
	/// </summary>
	[Serializable]
	public partial class MembershipCardGroupVersion : RepositoryRecord<MembershipCardGroupVersion>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MembershipCardGroupVersion()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MembershipCardGroupVersion(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("membership_card_group_version", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
				colvarCardGroupId.ColumnName = "card_group_id";
				colvarCardGroupId.DataType = DbType.Int32;
				colvarCardGroupId.MaxLength = 0;
				colvarCardGroupId.AutoIncrement = false;
				colvarCardGroupId.IsNullable = false;
				colvarCardGroupId.IsPrimaryKey = false;
				colvarCardGroupId.IsForeignKey = false;
				colvarCardGroupId.IsReadOnly = false;
				colvarCardGroupId.DefaultSetting = @"";
				colvarCardGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardGroupId);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
				colvarOpenTime.ColumnName = "open_time";
				colvarOpenTime.DataType = DbType.DateTime;
				colvarOpenTime.MaxLength = 0;
				colvarOpenTime.AutoIncrement = false;
				colvarOpenTime.IsNullable = false;
				colvarOpenTime.IsPrimaryKey = false;
				colvarOpenTime.IsForeignKey = false;
				colvarOpenTime.IsReadOnly = false;
				colvarOpenTime.DefaultSetting = @"";
				colvarOpenTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOpenTime);
				
				TableSchema.TableColumn colvarCloseTime = new TableSchema.TableColumn(schema);
				colvarCloseTime.ColumnName = "close_time";
				colvarCloseTime.DataType = DbType.DateTime;
				colvarCloseTime.MaxLength = 0;
				colvarCloseTime.AutoIncrement = false;
				colvarCloseTime.IsNullable = false;
				colvarCloseTime.IsPrimaryKey = false;
				colvarCloseTime.IsForeignKey = false;
				colvarCloseTime.IsReadOnly = false;
				colvarCloseTime.DefaultSetting = @"";
				colvarCloseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCloseTime);
				
				TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
				colvarImagePath.ColumnName = "image_path";
				colvarImagePath.DataType = DbType.String;
				colvarImagePath.MaxLength = 250;
				colvarImagePath.AutoIncrement = false;
				colvarImagePath.IsNullable = true;
				colvarImagePath.IsPrimaryKey = false;
				colvarImagePath.IsForeignKey = false;
				colvarImagePath.IsReadOnly = false;
				colvarImagePath.DefaultSetting = @"";
				colvarImagePath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImagePath);
				
				TableSchema.TableColumn colvarBackgroundColor = new TableSchema.TableColumn(schema);
				colvarBackgroundColor.ColumnName = "background_color";
				colvarBackgroundColor.DataType = DbType.String;
				colvarBackgroundColor.MaxLength = 9;
				colvarBackgroundColor.AutoIncrement = false;
				colvarBackgroundColor.IsNullable = true;
				colvarBackgroundColor.IsPrimaryKey = false;
				colvarBackgroundColor.IsForeignKey = false;
				colvarBackgroundColor.IsReadOnly = false;
				colvarBackgroundColor.DefaultSetting = @"";
				colvarBackgroundColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBackgroundColor);
				
				TableSchema.TableColumn colvarBackgroundImage = new TableSchema.TableColumn(schema);
				colvarBackgroundImage.ColumnName = "background_image";
				colvarBackgroundImage.DataType = DbType.String;
				colvarBackgroundImage.MaxLength = 250;
				colvarBackgroundImage.AutoIncrement = false;
				colvarBackgroundImage.IsNullable = true;
				colvarBackgroundImage.IsPrimaryKey = false;
				colvarBackgroundImage.IsForeignKey = false;
				colvarBackgroundImage.IsReadOnly = false;
				colvarBackgroundImage.DefaultSetting = @"";
				colvarBackgroundImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBackgroundImage);
				
				TableSchema.TableColumn colvarIconImage = new TableSchema.TableColumn(schema);
				colvarIconImage.ColumnName = "icon_image";
				colvarIconImage.DataType = DbType.String;
				colvarIconImage.MaxLength = 250;
				colvarIconImage.AutoIncrement = false;
				colvarIconImage.IsNullable = true;
				colvarIconImage.IsPrimaryKey = false;
				colvarIconImage.IsForeignKey = false;
				colvarIconImage.IsReadOnly = false;
				colvarIconImage.DefaultSetting = @"";
				colvarIconImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIconImage);
				
				TableSchema.TableColumn colvarBackgroundColorId = new TableSchema.TableColumn(schema);
				colvarBackgroundColorId.ColumnName = "background_color_id";
				colvarBackgroundColorId.DataType = DbType.Int32;
				colvarBackgroundColorId.MaxLength = 0;
				colvarBackgroundColorId.AutoIncrement = false;
				colvarBackgroundColorId.IsNullable = false;
				colvarBackgroundColorId.IsPrimaryKey = false;
				colvarBackgroundColorId.IsForeignKey = false;
				colvarBackgroundColorId.IsReadOnly = false;
				
						colvarBackgroundColorId.DefaultSetting = @"((0))";
				colvarBackgroundColorId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBackgroundColorId);
				
				TableSchema.TableColumn colvarBackgroundImageId = new TableSchema.TableColumn(schema);
				colvarBackgroundImageId.ColumnName = "background_image_id";
				colvarBackgroundImageId.DataType = DbType.Int32;
				colvarBackgroundImageId.MaxLength = 0;
				colvarBackgroundImageId.AutoIncrement = false;
				colvarBackgroundImageId.IsNullable = false;
				colvarBackgroundImageId.IsPrimaryKey = false;
				colvarBackgroundImageId.IsForeignKey = false;
				colvarBackgroundImageId.IsReadOnly = false;
				
						colvarBackgroundImageId.DefaultSetting = @"((0))";
				colvarBackgroundImageId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBackgroundImageId);
				
				TableSchema.TableColumn colvarIconImageId = new TableSchema.TableColumn(schema);
				colvarIconImageId.ColumnName = "icon_image_id";
				colvarIconImageId.DataType = DbType.Int32;
				colvarIconImageId.MaxLength = 0;
				colvarIconImageId.AutoIncrement = false;
				colvarIconImageId.IsNullable = false;
				colvarIconImageId.IsPrimaryKey = false;
				colvarIconImageId.IsForeignKey = false;
				colvarIconImageId.IsReadOnly = false;
				
						colvarIconImageId.DefaultSetting = @"((0))";
				colvarIconImageId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIconImageId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("membership_card_group_version",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CardGroupId")]
		[Bindable(true)]
		public int CardGroupId 
		{
			get { return GetColumnValue<int>(Columns.CardGroupId); }
			set { SetColumnValue(Columns.CardGroupId, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("OpenTime")]
		[Bindable(true)]
		public DateTime OpenTime 
		{
			get { return GetColumnValue<DateTime>(Columns.OpenTime); }
			set { SetColumnValue(Columns.OpenTime, value); }
		}
		  
		[XmlAttribute("CloseTime")]
		[Bindable(true)]
		public DateTime CloseTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CloseTime); }
			set { SetColumnValue(Columns.CloseTime, value); }
		}
		  
		[XmlAttribute("ImagePath")]
		[Bindable(true)]
		public string ImagePath 
		{
			get { return GetColumnValue<string>(Columns.ImagePath); }
			set { SetColumnValue(Columns.ImagePath, value); }
		}
		  
		[XmlAttribute("BackgroundColor")]
		[Bindable(true)]
		public string BackgroundColor 
		{
			get { return GetColumnValue<string>(Columns.BackgroundColor); }
			set { SetColumnValue(Columns.BackgroundColor, value); }
		}
		  
		[XmlAttribute("BackgroundImage")]
		[Bindable(true)]
		public string BackgroundImage 
		{
			get { return GetColumnValue<string>(Columns.BackgroundImage); }
			set { SetColumnValue(Columns.BackgroundImage, value); }
		}
		  
		[XmlAttribute("IconImage")]
		[Bindable(true)]
		public string IconImage 
		{
			get { return GetColumnValue<string>(Columns.IconImage); }
			set { SetColumnValue(Columns.IconImage, value); }
		}
		  
		[XmlAttribute("BackgroundColorId")]
		[Bindable(true)]
		public int BackgroundColorId 
		{
			get { return GetColumnValue<int>(Columns.BackgroundColorId); }
			set { SetColumnValue(Columns.BackgroundColorId, value); }
		}
		  
		[XmlAttribute("BackgroundImageId")]
		[Bindable(true)]
		public int BackgroundImageId 
		{
			get { return GetColumnValue<int>(Columns.BackgroundImageId); }
			set { SetColumnValue(Columns.BackgroundImageId, value); }
		}
		  
		[XmlAttribute("IconImageId")]
		[Bindable(true)]
		public int IconImageId 
		{
			get { return GetColumnValue<int>(Columns.IconImageId); }
			set { SetColumnValue(Columns.IconImageId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CardGroupIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OpenTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CloseTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ImagePathColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BackgroundColorColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn BackgroundImageColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IconImageColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn BackgroundColorIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn BackgroundImageIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn IconImageIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CardGroupId = @"card_group_id";
			 public static string Status = @"status";
			 public static string OpenTime = @"open_time";
			 public static string CloseTime = @"close_time";
			 public static string ImagePath = @"image_path";
			 public static string BackgroundColor = @"background_color";
			 public static string BackgroundImage = @"background_image";
			 public static string IconImage = @"icon_image";
			 public static string BackgroundColorId = @"background_color_id";
			 public static string BackgroundImageId = @"background_image_id";
			 public static string IconImageId = @"icon_image_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewExternalDealCollection : ReadOnlyList<ViewExternalDeal, ViewExternalDealCollection>
	{
			public ViewExternalDealCollection() { }

	}

	[Serializable]
	public partial class ViewExternalDeal : ReadOnlyRecord<ViewExternalDeal>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewExternalDeal()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewExternalDeal(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if (useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewExternalDeal(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewExternalDeal(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName, columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if (!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_external_deal", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "Guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);

				TableSchema.TableColumn colvarActiveType = new TableSchema.TableColumn(schema);
				colvarActiveType.ColumnName = "active_type";
				colvarActiveType.DataType = DbType.Int32;
				colvarActiveType.MaxLength = 0;
				colvarActiveType.AutoIncrement = false;
				colvarActiveType.IsNullable = false;
				colvarActiveType.IsPrimaryKey = false;
				colvarActiveType.IsForeignKey = false;
				colvarActiveType.IsReadOnly = false;
				colvarActiveType.DefaultSetting = @"";
				colvarActiveType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActiveType);

				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 100;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = true;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);

				TableSchema.TableColumn colvarProductCode = new TableSchema.TableColumn(schema);
				colvarProductCode.ColumnName = "product_code";
				colvarProductCode.DataType = DbType.String;
				colvarProductCode.MaxLength = 50;
				colvarProductCode.AutoIncrement = false;
				colvarProductCode.IsNullable = true;
				colvarProductCode.IsPrimaryKey = false;
				colvarProductCode.IsForeignKey = false;
				colvarProductCode.IsReadOnly = false;
				colvarProductCode.DefaultSetting = @"";
				colvarProductCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductCode);

				TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
				colvarItemOrigPrice.ColumnName = "item_orig_price";
				colvarItemOrigPrice.DataType = DbType.Currency;
				colvarItemOrigPrice.MaxLength = 0;
				colvarItemOrigPrice.AutoIncrement = false;
				colvarItemOrigPrice.IsNullable = true;
				colvarItemOrigPrice.IsPrimaryKey = false;
				colvarItemOrigPrice.IsForeignKey = false;
				colvarItemOrigPrice.IsReadOnly = false;
				colvarItemOrigPrice.DefaultSetting = @"";
				colvarItemOrigPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemOrigPrice);

				TableSchema.TableColumn colvarDiscountType = new TableSchema.TableColumn(schema);
				colvarDiscountType.ColumnName = "discount_type";
				colvarDiscountType.DataType = DbType.Int32;
				colvarDiscountType.MaxLength = 0;
				colvarDiscountType.AutoIncrement = false;
				colvarDiscountType.IsNullable = false;
				colvarDiscountType.IsPrimaryKey = false;
				colvarDiscountType.IsForeignKey = false;
				colvarDiscountType.IsReadOnly = false;
				colvarDiscountType.DefaultSetting = @"";
				colvarDiscountType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountType);

				TableSchema.TableColumn colvarDiscount = new TableSchema.TableColumn(schema);
				colvarDiscount.ColumnName = "discount";
				colvarDiscount.DataType = DbType.Int32;
				colvarDiscount.MaxLength = 0;
				colvarDiscount.AutoIncrement = false;
				colvarDiscount.IsNullable = false;
				colvarDiscount.IsPrimaryKey = false;
				colvarDiscount.IsForeignKey = false;
				colvarDiscount.IsReadOnly = false;
				colvarDiscount.DefaultSetting = @"";
				colvarDiscount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscount);

				TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
				colvarOrderTotalLimit.ColumnName = "order_total_limit";
				colvarOrderTotalLimit.DataType = DbType.Int32;
				colvarOrderTotalLimit.MaxLength = 0;
				colvarOrderTotalLimit.AutoIncrement = false;
				colvarOrderTotalLimit.IsNullable = false;
				colvarOrderTotalLimit.IsPrimaryKey = false;
				colvarOrderTotalLimit.IsForeignKey = false;
				colvarOrderTotalLimit.IsReadOnly = false;
				colvarOrderTotalLimit.DefaultSetting = @"";
				colvarOrderTotalLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderTotalLimit);

				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = 1073741823;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);

				TableSchema.TableColumn colvarIntroduction = new TableSchema.TableColumn(schema);
				colvarIntroduction.ColumnName = "introduction";
				colvarIntroduction.DataType = DbType.String;
				colvarIntroduction.MaxLength = 1073741823;
				colvarIntroduction.AutoIncrement = false;
				colvarIntroduction.IsNullable = true;
				colvarIntroduction.IsPrimaryKey = false;
				colvarIntroduction.IsForeignKey = false;
				colvarIntroduction.IsReadOnly = false;
				colvarIntroduction.DefaultSetting = @"";
				colvarIntroduction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntroduction);

				TableSchema.TableColumn colvarCategoryList = new TableSchema.TableColumn(schema);
				colvarCategoryList.ColumnName = "category_list";
				colvarCategoryList.DataType = DbType.AnsiString;
				colvarCategoryList.MaxLength = 255;
				colvarCategoryList.AutoIncrement = false;
				colvarCategoryList.IsNullable = true;
				colvarCategoryList.IsPrimaryKey = false;
				colvarCategoryList.IsForeignKey = false;
				colvarCategoryList.IsReadOnly = false;
				colvarCategoryList.DefaultSetting = @"";
				colvarCategoryList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryList);

				TableSchema.TableColumn colvarTagList = new TableSchema.TableColumn(schema);
				colvarTagList.ColumnName = "tag_list";
				colvarTagList.DataType = DbType.AnsiString;
				colvarTagList.MaxLength = 255;
				colvarTagList.AutoIncrement = false;
				colvarTagList.IsNullable = true;
				colvarTagList.IsPrimaryKey = false;
				colvarTagList.IsForeignKey = false;
				colvarTagList.IsReadOnly = false;
				colvarTagList.DefaultSetting = @"";
				colvarTagList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTagList);

				TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
				colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeS.MaxLength = 0;
				colvarBusinessHourOrderTimeS.AutoIncrement = false;
				colvarBusinessHourOrderTimeS.IsNullable = false;
				colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeS.IsForeignKey = false;
				colvarBusinessHourOrderTimeS.IsReadOnly = false;
				colvarBusinessHourOrderTimeS.DefaultSetting = @"";
				colvarBusinessHourOrderTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeS);

				TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
				colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
				colvarBusinessHourOrderTimeE.MaxLength = 0;
				colvarBusinessHourOrderTimeE.AutoIncrement = false;
				colvarBusinessHourOrderTimeE.IsNullable = false;
				colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
				colvarBusinessHourOrderTimeE.IsForeignKey = false;
				colvarBusinessHourOrderTimeE.IsReadOnly = false;
				colvarBusinessHourOrderTimeE.DefaultSetting = @"";
				colvarBusinessHourOrderTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourOrderTimeE);

				TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
				colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeS.MaxLength = 0;
				colvarBusinessHourDeliverTimeS.AutoIncrement = false;
				colvarBusinessHourDeliverTimeS.IsNullable = true;
				colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeS.IsForeignKey = false;
				colvarBusinessHourDeliverTimeS.IsReadOnly = false;
				colvarBusinessHourDeliverTimeS.DefaultSetting = @"";
				colvarBusinessHourDeliverTimeS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeS);

				TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
				colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
				colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
				colvarBusinessHourDeliverTimeE.MaxLength = 0;
				colvarBusinessHourDeliverTimeE.AutoIncrement = false;
				colvarBusinessHourDeliverTimeE.IsNullable = true;
				colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
				colvarBusinessHourDeliverTimeE.IsForeignKey = false;
				colvarBusinessHourDeliverTimeE.IsReadOnly = false;
				colvarBusinessHourDeliverTimeE.DefaultSetting = @"";
				colvarBusinessHourDeliverTimeE.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourDeliverTimeE);

				TableSchema.TableColumn colvarSettlementTime = new TableSchema.TableColumn(schema);
				colvarSettlementTime.ColumnName = "settlement_time";
				colvarSettlementTime.DataType = DbType.DateTime;
				colvarSettlementTime.MaxLength = 0;
				colvarSettlementTime.AutoIncrement = false;
				colvarSettlementTime.IsNullable = true;
				colvarSettlementTime.IsPrimaryKey = false;
				colvarSettlementTime.IsForeignKey = false;
				colvarSettlementTime.IsReadOnly = false;
				colvarSettlementTime.DefaultSetting = @"";
				colvarSettlementTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSettlementTime);

				TableSchema.TableColumn colvarBeaconType = new TableSchema.TableColumn(schema);
				colvarBeaconType.ColumnName = "beacon_type";
				colvarBeaconType.DataType = DbType.Int32;
				colvarBeaconType.MaxLength = 0;
				colvarBeaconType.AutoIncrement = false;
				colvarBeaconType.IsNullable = false;
				colvarBeaconType.IsPrimaryKey = false;
				colvarBeaconType.IsForeignKey = false;
				colvarBeaconType.IsReadOnly = false;
				colvarBeaconType.DefaultSetting = @"";
				colvarBeaconType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBeaconType);

				TableSchema.TableColumn colvarBeaconMessage = new TableSchema.TableColumn(schema);
				colvarBeaconMessage.ColumnName = "beacon_message";
				colvarBeaconMessage.DataType = DbType.String;
				colvarBeaconMessage.MaxLength = 1073741823;
				colvarBeaconMessage.AutoIncrement = false;
				colvarBeaconMessage.IsNullable = true;
				colvarBeaconMessage.IsPrimaryKey = false;
				colvarBeaconMessage.IsForeignKey = false;
				colvarBeaconMessage.IsReadOnly = false;
				colvarBeaconMessage.DefaultSetting = @"";
				colvarBeaconMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBeaconMessage);

				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 30;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarComeFromType = new TableSchema.TableColumn(schema);
				colvarComeFromType.ColumnName = "come_from_type";
				colvarComeFromType.DataType = DbType.Int32;
				colvarComeFromType.MaxLength = 0;
				colvarComeFromType.AutoIncrement = false;
				colvarComeFromType.IsNullable = false;
				colvarComeFromType.IsPrimaryKey = false;
				colvarComeFromType.IsForeignKey = false;
				colvarComeFromType.IsReadOnly = false;
				colvarComeFromType.DefaultSetting = @"";
				colvarComeFromType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarComeFromType);

				TableSchema.TableColumn colvarItemNo = new TableSchema.TableColumn(schema);
				colvarItemNo.ColumnName = "item_no";
				colvarItemNo.DataType = DbType.String;
				colvarItemNo.MaxLength = 50;
				colvarItemNo.AutoIncrement = false;
				colvarItemNo.IsNullable = true;
				colvarItemNo.IsPrimaryKey = false;
				colvarItemNo.IsForeignKey = false;
				colvarItemNo.IsReadOnly = false;
				colvarItemNo.DefaultSetting = @"";
				colvarItemNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemNo);

				TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
				colvarRemark.ColumnName = "remark";
				colvarRemark.DataType = DbType.String;
				colvarRemark.MaxLength = 1073741823;
				colvarRemark.AutoIncrement = false;
				colvarRemark.IsNullable = true;
				colvarRemark.IsPrimaryKey = false;
				colvarRemark.IsForeignKey = false;
				colvarRemark.IsReadOnly = false;
				colvarRemark.DefaultSetting = @"";
				colvarRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemark);

				TableSchema.TableColumn colvarSpecialItemNo = new TableSchema.TableColumn(schema);
				colvarSpecialItemNo.ColumnName = "special_item_no";
				colvarSpecialItemNo.DataType = DbType.String;
				colvarSpecialItemNo.MaxLength = 50;
				colvarSpecialItemNo.AutoIncrement = false;
				colvarSpecialItemNo.IsNullable = true;
				colvarSpecialItemNo.IsPrimaryKey = false;
				colvarSpecialItemNo.IsForeignKey = false;
				colvarSpecialItemNo.IsReadOnly = false;
				colvarSpecialItemNo.DefaultSetting = @"";
				colvarSpecialItemNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialItemNo);

				TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
				colvarImagePath.ColumnName = "image_path";
				colvarImagePath.DataType = DbType.String;
				colvarImagePath.MaxLength = 1000;
				colvarImagePath.AutoIncrement = false;
				colvarImagePath.IsNullable = true;
				colvarImagePath.IsPrimaryKey = false;
				colvarImagePath.IsForeignKey = false;
				colvarImagePath.IsReadOnly = false;
				colvarImagePath.DefaultSetting = @"";
				colvarImagePath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImagePath);

				TableSchema.TableColumn colvarAppDealPic = new TableSchema.TableColumn(schema);
				colvarAppDealPic.ColumnName = "app_deal_pic";
				colvarAppDealPic.DataType = DbType.String;
				colvarAppDealPic.MaxLength = 200;
				colvarAppDealPic.AutoIncrement = false;
				colvarAppDealPic.IsNullable = true;
				colvarAppDealPic.IsPrimaryKey = false;
				colvarAppDealPic.IsForeignKey = false;
				colvarAppDealPic.IsReadOnly = false;
				colvarAppDealPic.DefaultSetting = @"";
				colvarAppDealPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppDealPic);

				TableSchema.TableColumn colvarBuy = new TableSchema.TableColumn(schema);
				colvarBuy.ColumnName = "buy";
				colvarBuy.DataType = DbType.Int32;
				colvarBuy.MaxLength = 0;
				colvarBuy.AutoIncrement = false;
				colvarBuy.IsNullable = true;
				colvarBuy.IsPrimaryKey = false;
				colvarBuy.IsForeignKey = false;
				colvarBuy.IsReadOnly = false;
				colvarBuy.DefaultSetting = @"";
				colvarBuy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuy);

				TableSchema.TableColumn colvarFree = new TableSchema.TableColumn(schema);
				colvarFree.ColumnName = "free";
				colvarFree.DataType = DbType.Int32;
				colvarFree.MaxLength = 0;
				colvarFree.AutoIncrement = false;
				colvarFree.IsNullable = true;
				colvarFree.IsPrimaryKey = false;
				colvarFree.IsForeignKey = false;
				colvarFree.IsReadOnly = false;
				colvarFree.DefaultSetting = @"";
				colvarFree.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFree);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = false;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);

				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = true;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);

				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 100;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);

				TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
				colvarStoreName.ColumnName = "store_name";
				colvarStoreName.DataType = DbType.String;
				colvarStoreName.MaxLength = 100;
				colvarStoreName.AutoIncrement = false;
				colvarStoreName.IsNullable = false;
				colvarStoreName.IsPrimaryKey = false;
				colvarStoreName.IsForeignKey = false;
				colvarStoreName.IsReadOnly = false;
				colvarStoreName.DefaultSetting = @"";
				colvarStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreName);

				TableSchema.TableColumn colvarBrandCounterCode = new TableSchema.TableColumn(schema);
				colvarBrandCounterCode.ColumnName = "brand_counter_code";
				colvarBrandCounterCode.DataType = DbType.AnsiString;
				colvarBrandCounterCode.MaxLength = 10;
				colvarBrandCounterCode.AutoIncrement = false;
				colvarBrandCounterCode.IsNullable = false;
				colvarBrandCounterCode.IsPrimaryKey = false;
				colvarBrandCounterCode.IsForeignKey = false;
				colvarBrandCounterCode.IsReadOnly = false;
				colvarBrandCounterCode.DefaultSetting = @"";
				colvarBrandCounterCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandCounterCode);

				TableSchema.TableColumn colvarBrandCounterName = new TableSchema.TableColumn(schema);
				colvarBrandCounterName.ColumnName = "brand_counter_name";
				colvarBrandCounterName.DataType = DbType.String;
				colvarBrandCounterName.MaxLength = 50;
				colvarBrandCounterName.AutoIncrement = false;
				colvarBrandCounterName.IsNullable = true;
				colvarBrandCounterName.IsPrimaryKey = false;
				colvarBrandCounterName.IsForeignKey = false;
				colvarBrandCounterName.IsReadOnly = false;
				colvarBrandCounterName.DefaultSetting = @"";
				colvarBrandCounterName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandCounterName);

				TableSchema.TableColumn colvarShopCode = new TableSchema.TableColumn(schema);
				colvarShopCode.ColumnName = "shop_code";
				colvarShopCode.DataType = DbType.AnsiString;
				colvarShopCode.MaxLength = 4;
				colvarShopCode.AutoIncrement = false;
				colvarShopCode.IsNullable = false;
				colvarShopCode.IsPrimaryKey = false;
				colvarShopCode.IsForeignKey = false;
				colvarShopCode.IsReadOnly = false;
				colvarShopCode.DefaultSetting = @"";
				colvarShopCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShopCode);

				TableSchema.TableColumn colvarShopName = new TableSchema.TableColumn(schema);
				colvarShopName.ColumnName = "shop_name";
				colvarShopName.DataType = DbType.String;
				colvarShopName.MaxLength = 50;
				colvarShopName.AutoIncrement = false;
				colvarShopName.IsNullable = true;
				colvarShopName.IsPrimaryKey = false;
				colvarShopName.IsForeignKey = false;
				colvarShopName.IsReadOnly = false;
				colvarShopName.DefaultSetting = @"";
				colvarShopName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShopName);

				TableSchema.TableColumn colvarSkmStoreId = new TableSchema.TableColumn(schema);
				colvarSkmStoreId.ColumnName = "skm_store_id";
				colvarSkmStoreId.DataType = DbType.AnsiString;
				colvarSkmStoreId.MaxLength = 10;
				colvarSkmStoreId.AutoIncrement = false;
				colvarSkmStoreId.IsNullable = true;
				colvarSkmStoreId.IsPrimaryKey = false;
				colvarSkmStoreId.IsForeignKey = false;
				colvarSkmStoreId.IsReadOnly = false;
				colvarSkmStoreId.DefaultSetting = @"";
				colvarSkmStoreId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmStoreId);

				TableSchema.TableColumn colvarFloor = new TableSchema.TableColumn(schema);
				colvarFloor.ColumnName = "floor";
				colvarFloor.DataType = DbType.String;
				colvarFloor.MaxLength = 10;
				colvarFloor.AutoIncrement = false;
				colvarFloor.IsNullable = true;
				colvarFloor.IsPrimaryKey = false;
				colvarFloor.IsForeignKey = false;
				colvarFloor.IsReadOnly = false;
				colvarFloor.DefaultSetting = @"";
				colvarFloor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFloor);

				TableSchema.TableColumn colvarIsShoppe = new TableSchema.TableColumn(schema);
				colvarIsShoppe.ColumnName = "is_shoppe";
				colvarIsShoppe.DataType = DbType.Boolean;
				colvarIsShoppe.MaxLength = 0;
				colvarIsShoppe.AutoIncrement = false;
				colvarIsShoppe.IsNullable = false;
				colvarIsShoppe.IsPrimaryKey = false;
				colvarIsShoppe.IsForeignKey = false;
				colvarIsShoppe.IsReadOnly = false;
				colvarIsShoppe.DefaultSetting = @"";
				colvarIsShoppe.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsShoppe);

				TableSchema.TableColumn colvarIsAvailable = new TableSchema.TableColumn(schema);
				colvarIsAvailable.ColumnName = "is_available";
				colvarIsAvailable.DataType = DbType.Boolean;
				colvarIsAvailable.MaxLength = 0;
				colvarIsAvailable.AutoIncrement = false;
				colvarIsAvailable.IsNullable = false;
				colvarIsAvailable.IsPrimaryKey = false;
				colvarIsAvailable.IsForeignKey = false;
				colvarIsAvailable.IsReadOnly = false;
				colvarIsAvailable.DefaultSetting = @"";
				colvarIsAvailable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsAvailable);

				TableSchema.TableColumn colvarIsDel = new TableSchema.TableColumn(schema);
				colvarIsDel.ColumnName = "is_del";
				colvarIsDel.DataType = DbType.Boolean;
				colvarIsDel.MaxLength = 0;
				colvarIsDel.AutoIncrement = false;
				colvarIsDel.IsNullable = false;
				colvarIsDel.IsPrimaryKey = false;
				colvarIsDel.IsForeignKey = false;
				colvarIsDel.IsReadOnly = false;
				colvarIsDel.DefaultSetting = @"";
				colvarIsDel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDel);

				TableSchema.TableColumn colvarParentSellerGuid = new TableSchema.TableColumn(schema);
				colvarParentSellerGuid.ColumnName = "parent_seller_guid";
				colvarParentSellerGuid.DataType = DbType.Guid;
				colvarParentSellerGuid.MaxLength = 0;
				colvarParentSellerGuid.AutoIncrement = false;
				colvarParentSellerGuid.IsNullable = false;
				colvarParentSellerGuid.IsPrimaryKey = false;
				colvarParentSellerGuid.IsForeignKey = false;
				colvarParentSellerGuid.IsReadOnly = false;
				colvarParentSellerGuid.DefaultSetting = @"";
				colvarParentSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentSellerGuid);

				TableSchema.TableColumn colvarDealVersion = new TableSchema.TableColumn(schema);
				colvarDealVersion.ColumnName = "deal_version";
				colvarDealVersion.DataType = DbType.Int32;
				colvarDealVersion.MaxLength = 0;
				colvarDealVersion.AutoIncrement = false;
				colvarDealVersion.IsNullable = false;
				colvarDealVersion.IsPrimaryKey = false;
				colvarDealVersion.IsForeignKey = false;
				colvarDealVersion.IsReadOnly = false;
				colvarDealVersion.DefaultSetting = @"";
				colvarDealVersion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealVersion);

				TableSchema.TableColumn colvarIsPrizeDeal = new TableSchema.TableColumn(schema);
				colvarIsPrizeDeal.ColumnName = "is_prize_deal";
				colvarIsPrizeDeal.DataType = DbType.Boolean;
				colvarIsPrizeDeal.MaxLength = 0;
				colvarIsPrizeDeal.AutoIncrement = false;
				colvarIsPrizeDeal.IsNullable = false;
				colvarIsPrizeDeal.IsPrimaryKey = false;
				colvarIsPrizeDeal.IsForeignKey = false;
				colvarIsPrizeDeal.IsReadOnly = false;
				colvarIsPrizeDeal.DefaultSetting = @"";
				colvarIsPrizeDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsPrizeDeal);

				TableSchema.TableColumn colvarIsHqDeal = new TableSchema.TableColumn(schema);
				colvarIsHqDeal.ColumnName = "is_hq_deal";
				colvarIsHqDeal.DataType = DbType.Boolean;
				colvarIsHqDeal.MaxLength = 0;
				colvarIsHqDeal.AutoIncrement = false;
				colvarIsHqDeal.IsNullable = false;
				colvarIsHqDeal.IsPrimaryKey = false;
				colvarIsHqDeal.IsForeignKey = false;
				colvarIsHqDeal.IsReadOnly = false;
				colvarIsHqDeal.DefaultSetting = @"";
				colvarIsHqDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsHqDeal);

				TableSchema.TableColumn colvarDealIconId = new TableSchema.TableColumn(schema);
				colvarDealIconId.ColumnName = "deal_icon_id";
				colvarDealIconId.DataType = DbType.Int32;
				colvarDealIconId.MaxLength = 0;
				colvarDealIconId.AutoIncrement = false;
				colvarDealIconId.IsNullable = false;
				colvarDealIconId.IsPrimaryKey = false;
				colvarDealIconId.IsForeignKey = false;
				colvarDealIconId.IsReadOnly = false;
				colvarDealIconId.DefaultSetting = @"";
				colvarDealIconId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealIconId);

				TableSchema.TableColumn colvarIsCrmDeal = new TableSchema.TableColumn(schema);
				colvarIsCrmDeal.ColumnName = "is_crm_deal";
				colvarIsCrmDeal.DataType = DbType.Boolean;
				colvarIsCrmDeal.MaxLength = 0;
				colvarIsCrmDeal.AutoIncrement = false;
				colvarIsCrmDeal.IsNullable = false;
				colvarIsCrmDeal.IsPrimaryKey = false;
				colvarIsCrmDeal.IsForeignKey = false;
				colvarIsCrmDeal.IsReadOnly = false;
				colvarIsCrmDeal.DefaultSetting = @"";
				colvarIsCrmDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCrmDeal);

				TableSchema.TableColumn colvarIsSkmPay = new TableSchema.TableColumn(schema);
				colvarIsSkmPay.ColumnName = "is_skm_pay";
				colvarIsSkmPay.DataType = DbType.Boolean;
				colvarIsSkmPay.MaxLength = 0;
				colvarIsSkmPay.AutoIncrement = false;
				colvarIsSkmPay.IsNullable = false;
				colvarIsSkmPay.IsPrimaryKey = false;
				colvarIsSkmPay.IsForeignKey = false;
				colvarIsSkmPay.IsReadOnly = false;
				colvarIsSkmPay.DefaultSetting = @"";
				colvarIsSkmPay.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSkmPay);

				TableSchema.TableColumn colvarSkmPayBurningPoint = new TableSchema.TableColumn(schema);
				colvarSkmPayBurningPoint.ColumnName = "skm_pay_burning_point";
				colvarSkmPayBurningPoint.DataType = DbType.Int32;
				colvarSkmPayBurningPoint.MaxLength = 0;
				colvarSkmPayBurningPoint.AutoIncrement = false;
				colvarSkmPayBurningPoint.IsNullable = false;
				colvarSkmPayBurningPoint.IsPrimaryKey = false;
				colvarSkmPayBurningPoint.IsForeignKey = false;
				colvarSkmPayBurningPoint.IsReadOnly = false;
				colvarSkmPayBurningPoint.DefaultSetting = @"";
				colvarSkmPayBurningPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmPayBurningPoint);

				TableSchema.TableColumn colvarExcludingTaxPrice = new TableSchema.TableColumn(schema);
				colvarExcludingTaxPrice.ColumnName = "excluding_tax_price";
				colvarExcludingTaxPrice.DataType = DbType.Int32;
				colvarExcludingTaxPrice.MaxLength = 0;
				colvarExcludingTaxPrice.AutoIncrement = false;
				colvarExcludingTaxPrice.IsNullable = false;
				colvarExcludingTaxPrice.IsPrimaryKey = false;
				colvarExcludingTaxPrice.IsForeignKey = false;
				colvarExcludingTaxPrice.IsReadOnly = false;
				colvarExcludingTaxPrice.DefaultSetting = @"";
				colvarExcludingTaxPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExcludingTaxPrice);

				TableSchema.TableColumn colvarTaxRate = new TableSchema.TableColumn(schema);
				colvarTaxRate.ColumnName = "tax_rate";
				colvarTaxRate.DataType = DbType.Decimal;
				colvarTaxRate.MaxLength = 0;
				colvarTaxRate.AutoIncrement = false;
				colvarTaxRate.IsNullable = false;
				colvarTaxRate.IsPrimaryKey = false;
				colvarTaxRate.IsForeignKey = false;
				colvarTaxRate.IsReadOnly = false;
				colvarTaxRate.DefaultSetting = @"";
				colvarTaxRate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTaxRate);

				TableSchema.TableColumn colvarIsRepeatPurchase = new TableSchema.TableColumn(schema);
				colvarIsRepeatPurchase.ColumnName = "is_repeat_purchase";
				colvarIsRepeatPurchase.DataType = DbType.Boolean;
				colvarIsRepeatPurchase.MaxLength = 0;
				colvarIsRepeatPurchase.AutoIncrement = false;
				colvarIsRepeatPurchase.IsNullable = false;
				colvarIsRepeatPurchase.IsPrimaryKey = false;
				colvarIsRepeatPurchase.IsForeignKey = false;
				colvarIsRepeatPurchase.IsReadOnly = false;
				colvarIsRepeatPurchase.DefaultSetting = @"";
				colvarIsRepeatPurchase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsRepeatPurchase);

				TableSchema.TableColumn colvarSkmOrigPrice = new TableSchema.TableColumn(schema);
				colvarSkmOrigPrice.ColumnName = "skm_orig_price";
				colvarSkmOrigPrice.DataType = DbType.Currency;
				colvarSkmOrigPrice.MaxLength = 0;
				colvarSkmOrigPrice.AutoIncrement = false;
				colvarSkmOrigPrice.IsNullable = true;
				colvarSkmOrigPrice.IsPrimaryKey = false;
				colvarSkmOrigPrice.IsForeignKey = false;
				colvarSkmOrigPrice.IsReadOnly = false;
				colvarSkmOrigPrice.DefaultSetting = @"";
				colvarSkmOrigPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmOrigPrice);

				TableSchema.TableColumn colvarRepeatPurchaseType = new TableSchema.TableColumn(schema);
				colvarRepeatPurchaseType.ColumnName = "repeat_purchase_type";
				colvarRepeatPurchaseType.DataType = DbType.Int32;
				colvarRepeatPurchaseType.MaxLength = 0;
				colvarRepeatPurchaseType.AutoIncrement = false;
				colvarRepeatPurchaseType.IsNullable = true;
				colvarRepeatPurchaseType.IsPrimaryKey = false;
				colvarRepeatPurchaseType.IsForeignKey = false;
				colvarRepeatPurchaseType.IsReadOnly = false;
				colvarRepeatPurchaseType.DefaultSetting = @"";
				colvarRepeatPurchaseType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRepeatPurchaseType);

				TableSchema.TableColumn colvarRepeatPurchaseCount = new TableSchema.TableColumn(schema);
				colvarRepeatPurchaseCount.ColumnName = "repeat_purchase_count";
				colvarRepeatPurchaseCount.DataType = DbType.Int32;
				colvarRepeatPurchaseCount.MaxLength = 0;
				colvarRepeatPurchaseCount.AutoIncrement = false;
				colvarRepeatPurchaseCount.IsNullable = true;
				colvarRepeatPurchaseCount.IsPrimaryKey = false;
				colvarRepeatPurchaseCount.IsForeignKey = false;
				colvarRepeatPurchaseCount.IsReadOnly = false;
				colvarRepeatPurchaseCount.DefaultSetting = @"";
				colvarRepeatPurchaseCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRepeatPurchaseCount);

				TableSchema.TableColumn colvarEnableInstall = new TableSchema.TableColumn(schema);
				colvarEnableInstall.ColumnName = "enable_install";
				colvarEnableInstall.DataType = DbType.Boolean;
				colvarEnableInstall.MaxLength = 0;
				colvarEnableInstall.AutoIncrement = false;
				colvarEnableInstall.IsNullable = false;
				colvarEnableInstall.IsPrimaryKey = false;
				colvarEnableInstall.IsForeignKey = false;
				colvarEnableInstall.IsReadOnly = false;
				colvarEnableInstall.DefaultSetting = @"";
				colvarEnableInstall.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnableInstall);

				TableSchema.TableColumn colvarPh = new TableSchema.TableColumn(schema);
				colvarPh.ColumnName = "ph";
				colvarPh.DataType = DbType.String;
				colvarPh.MaxLength = 2;
				colvarPh.AutoIncrement = false;
				colvarPh.IsNullable = true;
				colvarPh.IsPrimaryKey = false;
				colvarPh.IsForeignKey = false;
				colvarPh.IsReadOnly = false;
				colvarPh.DefaultSetting = @"";
				colvarPh.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPh);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_external_deal", schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}

		[XmlAttribute("ActiveType")]
		[Bindable(true)]
		public int ActiveType
		{
			get { return GetColumnValue<int>(Columns.ActiveType); }
			set { SetColumnValue(Columns.ActiveType, value); }
		}

		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}

		[XmlAttribute("ProductCode")]
		[Bindable(true)]
		public string ProductCode
		{
			get { return GetColumnValue<string>(Columns.ProductCode); }
			set { SetColumnValue(Columns.ProductCode, value); }
		}

		[XmlAttribute("ItemOrigPrice")]
		[Bindable(true)]
		public decimal? ItemOrigPrice
		{
			get { return GetColumnValue<decimal?>(Columns.ItemOrigPrice); }
			set { SetColumnValue(Columns.ItemOrigPrice, value); }
		}

		[XmlAttribute("DiscountType")]
		[Bindable(true)]
		public int DiscountType
		{
			get { return GetColumnValue<int>(Columns.DiscountType); }
			set { SetColumnValue(Columns.DiscountType, value); }
		}

		[XmlAttribute("Discount")]
		[Bindable(true)]
		public int Discount
		{
			get { return GetColumnValue<int>(Columns.Discount); }
			set { SetColumnValue(Columns.Discount, value); }
		}

		[XmlAttribute("OrderTotalLimit")]
		[Bindable(true)]
		public int OrderTotalLimit
		{
			get { return GetColumnValue<int>(Columns.OrderTotalLimit); }
			set { SetColumnValue(Columns.OrderTotalLimit, value); }
		}

		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}

		[XmlAttribute("Introduction")]
		[Bindable(true)]
		public string Introduction
		{
			get { return GetColumnValue<string>(Columns.Introduction); }
			set { SetColumnValue(Columns.Introduction, value); }
		}

		[XmlAttribute("CategoryList")]
		[Bindable(true)]
		public string CategoryList
		{
			get { return GetColumnValue<string>(Columns.CategoryList); }
			set { SetColumnValue(Columns.CategoryList, value); }
		}

		[XmlAttribute("TagList")]
		[Bindable(true)]
		public string TagList
		{
			get { return GetColumnValue<string>(Columns.TagList); }
			set { SetColumnValue(Columns.TagList, value); }
		}

		[XmlAttribute("BusinessHourOrderTimeS")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeS
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeS); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeS, value); }
		}

		[XmlAttribute("BusinessHourOrderTimeE")]
		[Bindable(true)]
		public DateTime BusinessHourOrderTimeE
		{
			get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeE); }
			set { SetColumnValue(Columns.BusinessHourOrderTimeE, value); }
		}

		[XmlAttribute("BusinessHourDeliverTimeS")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeS
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeS); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeS, value); }
		}

		[XmlAttribute("BusinessHourDeliverTimeE")]
		[Bindable(true)]
		public DateTime? BusinessHourDeliverTimeE
		{
			get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeE); }
			set { SetColumnValue(Columns.BusinessHourDeliverTimeE, value); }
		}

		[XmlAttribute("SettlementTime")]
		[Bindable(true)]
		public DateTime? SettlementTime
		{
			get { return GetColumnValue<DateTime?>(Columns.SettlementTime); }
			set { SetColumnValue(Columns.SettlementTime, value); }
		}

		[XmlAttribute("BeaconType")]
		[Bindable(true)]
		public int BeaconType
		{
			get { return GetColumnValue<int>(Columns.BeaconType); }
			set { SetColumnValue(Columns.BeaconType, value); }
		}

		[XmlAttribute("BeaconMessage")]
		[Bindable(true)]
		public string BeaconMessage
		{
			get { return GetColumnValue<string>(Columns.BeaconMessage); }
			set { SetColumnValue(Columns.BeaconMessage, value); }
		}

		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("ComeFromType")]
		[Bindable(true)]
		public int ComeFromType
		{
			get { return GetColumnValue<int>(Columns.ComeFromType); }
			set { SetColumnValue(Columns.ComeFromType, value); }
		}

		[XmlAttribute("ItemNo")]
		[Bindable(true)]
		public string ItemNo
		{
			get { return GetColumnValue<string>(Columns.ItemNo); }
			set { SetColumnValue(Columns.ItemNo, value); }
		}

		[XmlAttribute("Remark")]
		[Bindable(true)]
		public string Remark
		{
			get { return GetColumnValue<string>(Columns.Remark); }
			set { SetColumnValue(Columns.Remark, value); }
		}

		[XmlAttribute("SpecialItemNo")]
		[Bindable(true)]
		public string SpecialItemNo
		{
			get { return GetColumnValue<string>(Columns.SpecialItemNo); }
			set { SetColumnValue(Columns.SpecialItemNo, value); }
		}

		[XmlAttribute("ImagePath")]
		[Bindable(true)]
		public string ImagePath
		{
			get { return GetColumnValue<string>(Columns.ImagePath); }
			set { SetColumnValue(Columns.ImagePath, value); }
		}

		[XmlAttribute("AppDealPic")]
		[Bindable(true)]
		public string AppDealPic
		{
			get { return GetColumnValue<string>(Columns.AppDealPic); }
			set { SetColumnValue(Columns.AppDealPic, value); }
		}

		[XmlAttribute("Buy")]
		[Bindable(true)]
		public int? Buy
		{
			get { return GetColumnValue<int?>(Columns.Buy); }
			set { SetColumnValue(Columns.Buy, value); }
		}

		[XmlAttribute("Free")]
		[Bindable(true)]
		public int? Free
		{
			get { return GetColumnValue<int?>(Columns.Free); }
			set { SetColumnValue(Columns.Free, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid StoreGuid
		{
			get { return GetColumnValue<Guid>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}

		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid? Bid
		{
			get { return GetColumnValue<Guid?>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}

		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}

		[XmlAttribute("StoreName")]
		[Bindable(true)]
		public string StoreName
		{
			get { return GetColumnValue<string>(Columns.StoreName); }
			set { SetColumnValue(Columns.StoreName, value); }
		}

		[XmlAttribute("BrandCounterCode")]
		[Bindable(true)]
		public string BrandCounterCode
		{
			get { return GetColumnValue<string>(Columns.BrandCounterCode); }
			set { SetColumnValue(Columns.BrandCounterCode, value); }
		}

		[XmlAttribute("BrandCounterName")]
		[Bindable(true)]
		public string BrandCounterName
		{
			get { return GetColumnValue<string>(Columns.BrandCounterName); }
			set { SetColumnValue(Columns.BrandCounterName, value); }
		}

		[XmlAttribute("ShopCode")]
		[Bindable(true)]
		public string ShopCode
		{
			get { return GetColumnValue<string>(Columns.ShopCode); }
			set { SetColumnValue(Columns.ShopCode, value); }
		}

		[XmlAttribute("ShopName")]
		[Bindable(true)]
		public string ShopName
		{
			get { return GetColumnValue<string>(Columns.ShopName); }
			set { SetColumnValue(Columns.ShopName, value); }
		}

		[XmlAttribute("SkmStoreId")]
		[Bindable(true)]
		public string SkmStoreId
		{
			get { return GetColumnValue<string>(Columns.SkmStoreId); }
			set { SetColumnValue(Columns.SkmStoreId, value); }
		}

		[XmlAttribute("Floor")]
		[Bindable(true)]
		public string Floor
		{
			get { return GetColumnValue<string>(Columns.Floor); }
			set { SetColumnValue(Columns.Floor, value); }
		}

		[XmlAttribute("IsShoppe")]
		[Bindable(true)]
		public bool IsShoppe
		{
			get { return GetColumnValue<bool>(Columns.IsShoppe); }
			set { SetColumnValue(Columns.IsShoppe, value); }
		}

		[XmlAttribute("IsAvailable")]
		[Bindable(true)]
		public bool IsAvailable
		{
			get { return GetColumnValue<bool>(Columns.IsAvailable); }
			set { SetColumnValue(Columns.IsAvailable, value); }
		}

		[XmlAttribute("IsDel")]
		[Bindable(true)]
		public bool IsDel
		{
			get { return GetColumnValue<bool>(Columns.IsDel); }
			set { SetColumnValue(Columns.IsDel, value); }
		}

		[XmlAttribute("ParentSellerGuid")]
		[Bindable(true)]
		public Guid ParentSellerGuid
		{
			get { return GetColumnValue<Guid>(Columns.ParentSellerGuid); }
			set { SetColumnValue(Columns.ParentSellerGuid, value); }
		}

		[XmlAttribute("DealVersion")]
		[Bindable(true)]
		public int DealVersion
		{
			get { return GetColumnValue<int>(Columns.DealVersion); }
			set { SetColumnValue(Columns.DealVersion, value); }
		}

		[XmlAttribute("IsPrizeDeal")]
		[Bindable(true)]
		public bool IsPrizeDeal
		{
			get { return GetColumnValue<bool>(Columns.IsPrizeDeal); }
			set { SetColumnValue(Columns.IsPrizeDeal, value); }
		}

		[XmlAttribute("IsHqDeal")]
		[Bindable(true)]
		public bool IsHqDeal
		{
			get { return GetColumnValue<bool>(Columns.IsHqDeal); }
			set { SetColumnValue(Columns.IsHqDeal, value); }
		}

		[XmlAttribute("DealIconId")]
		[Bindable(true)]
		public int DealIconId
		{
			get { return GetColumnValue<int>(Columns.DealIconId); }
			set { SetColumnValue(Columns.DealIconId, value); }
		}

		[XmlAttribute("IsCrmDeal")]
		[Bindable(true)]
		public bool IsCrmDeal
		{
			get { return GetColumnValue<bool>(Columns.IsCrmDeal); }
			set { SetColumnValue(Columns.IsCrmDeal, value); }
		}

		[XmlAttribute("IsSkmPay")]
		[Bindable(true)]
		public bool IsSkmPay
		{
			get { return GetColumnValue<bool>(Columns.IsSkmPay); }
			set { SetColumnValue(Columns.IsSkmPay, value); }
		}

		[XmlAttribute("SkmPayBurningPoint")]
		[Bindable(true)]
		public int SkmPayBurningPoint
		{
			get { return GetColumnValue<int>(Columns.SkmPayBurningPoint); }
			set { SetColumnValue(Columns.SkmPayBurningPoint, value); }
		}

		[XmlAttribute("ExcludingTaxPrice")]
		[Bindable(true)]
		public int ExcludingTaxPrice
		{
			get { return GetColumnValue<int>(Columns.ExcludingTaxPrice); }
			set { SetColumnValue(Columns.ExcludingTaxPrice, value); }
		}

		[XmlAttribute("TaxRate")]
		[Bindable(true)]
		public decimal TaxRate
		{
			get { return GetColumnValue<decimal>(Columns.TaxRate); }
			set { SetColumnValue(Columns.TaxRate, value); }
		}

		[XmlAttribute("IsRepeatPurchase")]
		[Bindable(true)]
		public bool IsRepeatPurchase
		{
			get { return GetColumnValue<bool>(Columns.IsRepeatPurchase); }
			set { SetColumnValue(Columns.IsRepeatPurchase, value); }
		}

		[XmlAttribute("SkmOrigPrice")]
		[Bindable(true)]
		public decimal? SkmOrigPrice
		{
			get { return GetColumnValue<decimal?>(Columns.SkmOrigPrice); }
			set { SetColumnValue(Columns.SkmOrigPrice, value); }
		}

		[XmlAttribute("RepeatPurchaseType")]
		[Bindable(true)]
		public int? RepeatPurchaseType
		{
			get { return GetColumnValue<int?>(Columns.RepeatPurchaseType); }
			set { SetColumnValue(Columns.RepeatPurchaseType, value); }
		}

		[XmlAttribute("RepeatPurchaseCount")]
		[Bindable(true)]
		public int? RepeatPurchaseCount
		{
			get { return GetColumnValue<int?>(Columns.RepeatPurchaseCount); }
			set { SetColumnValue(Columns.RepeatPurchaseCount, value); }
		}

		[XmlAttribute("EnableInstall")]
		[Bindable(true)]
		public bool EnableInstall
		{
			get { return GetColumnValue<bool>(Columns.EnableInstall); }
			set { SetColumnValue(Columns.EnableInstall, value); }
		}

		[XmlAttribute("Ph")]
		[Bindable(true)]
		public string Ph
		{
			get { return GetColumnValue<string>(Columns.Ph); }
			set { SetColumnValue(Columns.Ph, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn GuidColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn ActiveTypeColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn TitleColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn ProductCodeColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn ItemOrigPriceColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn DiscountTypeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn DiscountColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn OrderTotalLimitColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn DescriptionColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn IntroductionColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn CategoryListColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn TagListColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderTimeSColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn BusinessHourOrderTimeEColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn BusinessHourDeliverTimeSColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn BusinessHourDeliverTimeEColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn SettlementTimeColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn BeaconTypeColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn BeaconMessageColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn StatusColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn ModifyIdColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn ComeFromTypeColumn
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn ItemNoColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn RemarkColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn SpecialItemNoColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn ImagePathColumn
		{
			get { return Schema.Columns[28]; }
		}

		public static TableSchema.TableColumn AppDealPicColumn
		{
			get { return Schema.Columns[29]; }
		}

		public static TableSchema.TableColumn BuyColumn
		{
			get { return Schema.Columns[30]; }
		}

		public static TableSchema.TableColumn FreeColumn
		{
			get { return Schema.Columns[31]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[32]; }
		}

		public static TableSchema.TableColumn StoreGuidColumn
		{
			get { return Schema.Columns[33]; }
		}

		public static TableSchema.TableColumn BidColumn
		{
			get { return Schema.Columns[34]; }
		}

		public static TableSchema.TableColumn SellerNameColumn
		{
			get { return Schema.Columns[35]; }
		}

		public static TableSchema.TableColumn StoreNameColumn
		{
			get { return Schema.Columns[36]; }
		}

		public static TableSchema.TableColumn BrandCounterCodeColumn
		{
			get { return Schema.Columns[37]; }
		}

		public static TableSchema.TableColumn BrandCounterNameColumn
		{
			get { return Schema.Columns[38]; }
		}

		public static TableSchema.TableColumn ShopCodeColumn
		{
			get { return Schema.Columns[39]; }
		}

		public static TableSchema.TableColumn ShopNameColumn
		{
			get { return Schema.Columns[40]; }
		}

		public static TableSchema.TableColumn SkmStoreIdColumn
		{
			get { return Schema.Columns[41]; }
		}

		public static TableSchema.TableColumn FloorColumn
		{
			get { return Schema.Columns[42]; }
		}

		public static TableSchema.TableColumn IsShoppeColumn
		{
			get { return Schema.Columns[43]; }
		}

		public static TableSchema.TableColumn IsAvailableColumn
		{
			get { return Schema.Columns[44]; }
		}

		public static TableSchema.TableColumn IsDelColumn
		{
			get { return Schema.Columns[45]; }
		}

		public static TableSchema.TableColumn ParentSellerGuidColumn
		{
			get { return Schema.Columns[46]; }
		}

		public static TableSchema.TableColumn DealVersionColumn
		{
			get { return Schema.Columns[47]; }
		}

		public static TableSchema.TableColumn IsPrizeDealColumn
		{
			get { return Schema.Columns[48]; }
		}

		public static TableSchema.TableColumn IsHqDealColumn
		{
			get { return Schema.Columns[49]; }
		}

		public static TableSchema.TableColumn DealIconIdColumn
		{
			get { return Schema.Columns[50]; }
		}

		public static TableSchema.TableColumn IsCrmDealColumn
		{
			get { return Schema.Columns[51]; }
		}

		public static TableSchema.TableColumn IsSkmPayColumn
		{
			get { return Schema.Columns[52]; }
		}

		public static TableSchema.TableColumn SkmPayBurningPointColumn
		{
			get { return Schema.Columns[53]; }
		}

		public static TableSchema.TableColumn ExcludingTaxPriceColumn
		{
			get { return Schema.Columns[54]; }
		}

		public static TableSchema.TableColumn TaxRateColumn
		{
			get { return Schema.Columns[55]; }
		}

		public static TableSchema.TableColumn IsRepeatPurchaseColumn
		{
			get { return Schema.Columns[56]; }
		}

		public static TableSchema.TableColumn SkmOrigPriceColumn
		{
			get { return Schema.Columns[57]; }
		}

		public static TableSchema.TableColumn RepeatPurchaseTypeColumn
		{
			get { return Schema.Columns[58]; }
		}

		public static TableSchema.TableColumn RepeatPurchaseCountColumn
		{
			get { return Schema.Columns[59]; }
		}

		public static TableSchema.TableColumn EnableInstallColumn
		{
			get { return Schema.Columns[60]; }
		}

		public static TableSchema.TableColumn PhColumn
		{
			get { return Schema.Columns[61]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Guid = @"Guid";
			public static string ActiveType = @"active_type";
			public static string Title = @"title";
			public static string ProductCode = @"product_code";
			public static string ItemOrigPrice = @"item_orig_price";
			public static string DiscountType = @"discount_type";
			public static string Discount = @"discount";
			public static string OrderTotalLimit = @"order_total_limit";
			public static string Description = @"description";
			public static string Introduction = @"introduction";
			public static string CategoryList = @"category_list";
			public static string TagList = @"tag_list";
			public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
			public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
			public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
			public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
			public static string SettlementTime = @"settlement_time";
			public static string BeaconType = @"beacon_type";
			public static string BeaconMessage = @"beacon_message";
			public static string Status = @"status";
			public static string CreateId = @"create_id";
			public static string CreateTime = @"create_time";
			public static string ModifyId = @"modify_id";
			public static string ModifyTime = @"modify_time";
			public static string ComeFromType = @"come_from_type";
			public static string ItemNo = @"item_no";
			public static string Remark = @"remark";
			public static string SpecialItemNo = @"special_item_no";
			public static string ImagePath = @"image_path";
			public static string AppDealPic = @"app_deal_pic";
			public static string Buy = @"buy";
			public static string Free = @"free";
			public static string SellerGuid = @"seller_guid";
			public static string StoreGuid = @"store_guid";
			public static string Bid = @"bid";
			public static string SellerName = @"seller_name";
			public static string StoreName = @"store_name";
			public static string BrandCounterCode = @"brand_counter_code";
			public static string BrandCounterName = @"brand_counter_name";
			public static string ShopCode = @"shop_code";
			public static string ShopName = @"shop_name";
			public static string SkmStoreId = @"skm_store_id";
			public static string Floor = @"floor";
			public static string IsShoppe = @"is_shoppe";
			public static string IsAvailable = @"is_available";
			public static string IsDel = @"is_del";
			public static string ParentSellerGuid = @"parent_seller_guid";
			public static string DealVersion = @"deal_version";
			public static string IsPrizeDeal = @"is_prize_deal";
			public static string IsHqDeal = @"is_hq_deal";
			public static string DealIconId = @"deal_icon_id";
			public static string IsCrmDeal = @"is_crm_deal";
			public static string IsSkmPay = @"is_skm_pay";
			public static string SkmPayBurningPoint = @"skm_pay_burning_point";
			public static string ExcludingTaxPrice = @"excluding_tax_price";
			public static string TaxRate = @"tax_rate";
			public static string IsRepeatPurchase = @"is_repeat_purchase";
			public static string SkmOrigPrice = @"skm_orig_price";
			public static string RepeatPurchaseType = @"repeat_purchase_type";
			public static string RepeatPurchaseCount = @"repeat_purchase_count";
			public static string EnableInstall = @"enable_install";
			public static string Ph = @"ph";
			public static string Skuid = @"sku_id";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

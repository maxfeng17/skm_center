using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpPointTransactionExchangeOrder class.
	/// </summary>
    [Serializable]
	public partial class PcpPointTransactionExchangeOrderCollection : RepositoryList<PcpPointTransactionExchangeOrder, PcpPointTransactionExchangeOrderCollection>
	{	   
		public PcpPointTransactionExchangeOrderCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpPointTransactionExchangeOrderCollection</returns>
		public PcpPointTransactionExchangeOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpPointTransactionExchangeOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_point_transaction_exchange_order table.
	/// </summary>
	[Serializable]
	public partial class PcpPointTransactionExchangeOrder : RepositoryRecord<PcpPointTransactionExchangeOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpPointTransactionExchangeOrder()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpPointTransactionExchangeOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_point_transaction_exchange_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
				colvarSellerUserId.ColumnName = "seller_user_id";
				colvarSellerUserId.DataType = DbType.Int32;
				colvarSellerUserId.MaxLength = 0;
				colvarSellerUserId.AutoIncrement = false;
				colvarSellerUserId.IsNullable = false;
				colvarSellerUserId.IsPrimaryKey = false;
				colvarSellerUserId.IsForeignKey = false;
				colvarSellerUserId.IsReadOnly = false;
				colvarSellerUserId.DefaultSetting = @"";
				colvarSellerUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerUserId);
				
				TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
				colvarOrderStatus.ColumnName = "order_status";
				colvarOrderStatus.DataType = DbType.Byte;
				colvarOrderStatus.MaxLength = 0;
				colvarOrderStatus.AutoIncrement = false;
				colvarOrderStatus.IsNullable = false;
				colvarOrderStatus.IsPrimaryKey = false;
				colvarOrderStatus.IsForeignKey = false;
				colvarOrderStatus.IsReadOnly = false;
				colvarOrderStatus.DefaultSetting = @"";
				colvarOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStatus);
				
				TableSchema.TableColumn colvarTransactionAmount = new TableSchema.TableColumn(schema);
				colvarTransactionAmount.ColumnName = "transaction_amount";
				colvarTransactionAmount.DataType = DbType.Currency;
				colvarTransactionAmount.MaxLength = 0;
				colvarTransactionAmount.AutoIncrement = false;
				colvarTransactionAmount.IsNullable = false;
				colvarTransactionAmount.IsPrimaryKey = false;
				colvarTransactionAmount.IsForeignKey = false;
				colvarTransactionAmount.IsReadOnly = false;
				colvarTransactionAmount.DefaultSetting = @"";
				colvarTransactionAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransactionAmount);
				
				TableSchema.TableColumn colvarSuperBonus = new TableSchema.TableColumn(schema);
				colvarSuperBonus.ColumnName = "super_bonus";
				colvarSuperBonus.DataType = DbType.Double;
				colvarSuperBonus.MaxLength = 0;
				colvarSuperBonus.AutoIncrement = false;
				colvarSuperBonus.IsNullable = false;
				colvarSuperBonus.IsPrimaryKey = false;
				colvarSuperBonus.IsForeignKey = false;
				colvarSuperBonus.IsReadOnly = false;
				
						colvarSuperBonus.DefaultSetting = @"((0))";
				colvarSuperBonus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSuperBonus);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = -1;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_point_transaction_exchange_order",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("SellerUserId")]
		[Bindable(true)]
		public int SellerUserId 
		{
			get { return GetColumnValue<int>(Columns.SellerUserId); }
			set { SetColumnValue(Columns.SellerUserId, value); }
		}
		  
		[XmlAttribute("OrderStatus")]
		[Bindable(true)]
		public byte OrderStatus 
		{
			get { return GetColumnValue<byte>(Columns.OrderStatus); }
			set { SetColumnValue(Columns.OrderStatus, value); }
		}
		  
		[XmlAttribute("TransactionAmount")]
		[Bindable(true)]
		public decimal TransactionAmount 
		{
			get { return GetColumnValue<decimal>(Columns.TransactionAmount); }
			set { SetColumnValue(Columns.TransactionAmount, value); }
		}
		  
		[XmlAttribute("SuperBonus")]
		[Bindable(true)]
		public double SuperBonus 
		{
			get { return GetColumnValue<double>(Columns.SuperBonus); }
			set { SetColumnValue(Columns.SuperBonus, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerUserIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderStatusColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn TransactionAmountColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn SuperBonusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Guid = @"guid";
			 public static string SellerUserId = @"seller_user_id";
			 public static string OrderStatus = @"order_status";
			 public static string TransactionAmount = @"transaction_amount";
			 public static string SuperBonus = @"super_bonus";
			 public static string Description = @"description";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmVerifyReply class.
	/// </summary>
    [Serializable]
	public partial class SkmVerifyReplyCollection : RepositoryList<SkmVerifyReply, SkmVerifyReplyCollection>
	{	   
		public SkmVerifyReplyCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmVerifyReplyCollection</returns>
		public SkmVerifyReplyCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmVerifyReply o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_verify_reply table.
	/// </summary>
	
	[Serializable]
	public partial class SkmVerifyReply : RepositoryRecord<SkmVerifyReply>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmVerifyReply()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmVerifyReply(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_verify_reply", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);
				
				TableSchema.TableColumn colvarSkmTransId = new TableSchema.TableColumn(schema);
				colvarSkmTransId.ColumnName = "skm_trans_id";
				colvarSkmTransId.DataType = DbType.AnsiString;
				colvarSkmTransId.MaxLength = 40;
				colvarSkmTransId.AutoIncrement = false;
				colvarSkmTransId.IsNullable = false;
				colvarSkmTransId.IsPrimaryKey = false;
				colvarSkmTransId.IsForeignKey = false;
				colvarSkmTransId.IsReadOnly = false;
				colvarSkmTransId.DefaultSetting = @"";
				colvarSkmTransId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmTransId);
				
				TableSchema.TableColumn colvarShopCode = new TableSchema.TableColumn(schema);
				colvarShopCode.ColumnName = "shop_code";
				colvarShopCode.DataType = DbType.AnsiString;
				colvarShopCode.MaxLength = 4;
				colvarShopCode.AutoIncrement = false;
				colvarShopCode.IsNullable = false;
				colvarShopCode.IsPrimaryKey = false;
				colvarShopCode.IsForeignKey = false;
				colvarShopCode.IsReadOnly = false;
				colvarShopCode.DefaultSetting = @"";
				colvarShopCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShopCode);
				
				TableSchema.TableColumn colvarBrandCounterCode = new TableSchema.TableColumn(schema);
				colvarBrandCounterCode.ColumnName = "brand_counter_code";
				colvarBrandCounterCode.DataType = DbType.AnsiString;
				colvarBrandCounterCode.MaxLength = 10;
				colvarBrandCounterCode.AutoIncrement = false;
				colvarBrandCounterCode.IsNullable = false;
				colvarBrandCounterCode.IsPrimaryKey = false;
				colvarBrandCounterCode.IsForeignKey = false;
				colvarBrandCounterCode.IsReadOnly = false;
				
						colvarBrandCounterCode.DefaultSetting = @"('')";
				colvarBrandCounterCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandCounterCode);
				
				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "coupon_id";
				colvarCouponId.DataType = DbType.Int32;
				colvarCouponId.MaxLength = 0;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = false;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = false;
				colvarCouponId.IsReadOnly = false;
				colvarCouponId.DefaultSetting = @"";
				colvarCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponId);
				
				TableSchema.TableColumn colvarBurningEventSid = new TableSchema.TableColumn(schema);
				colvarBurningEventSid.ColumnName = "burning_event_sid";
				colvarBurningEventSid.DataType = DbType.AnsiString;
				colvarBurningEventSid.MaxLength = 20;
				colvarBurningEventSid.AutoIncrement = false;
				colvarBurningEventSid.IsNullable = false;
				colvarBurningEventSid.IsPrimaryKey = false;
				colvarBurningEventSid.IsForeignKey = false;
				colvarBurningEventSid.IsReadOnly = false;
				colvarBurningEventSid.DefaultSetting = @"";
				colvarBurningEventSid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBurningEventSid);
				
				TableSchema.TableColumn colvarDiscountType = new TableSchema.TableColumn(schema);
				colvarDiscountType.ColumnName = "discount_type";
				colvarDiscountType.DataType = DbType.Int32;
				colvarDiscountType.MaxLength = 0;
				colvarDiscountType.AutoIncrement = false;
				colvarDiscountType.IsNullable = false;
				colvarDiscountType.IsPrimaryKey = false;
				colvarDiscountType.IsForeignKey = false;
				colvarDiscountType.IsReadOnly = false;
				colvarDiscountType.DefaultSetting = @"";
				colvarDiscountType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountType);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarVerifyDate = new TableSchema.TableColumn(schema);
				colvarVerifyDate.ColumnName = "verify_date";
				colvarVerifyDate.DataType = DbType.DateTime;
				colvarVerifyDate.MaxLength = 0;
				colvarVerifyDate.AutoIncrement = false;
				colvarVerifyDate.IsNullable = false;
				colvarVerifyDate.IsPrimaryKey = false;
				colvarVerifyDate.IsForeignKey = false;
				colvarVerifyDate.IsReadOnly = false;
				colvarVerifyDate.DefaultSetting = @"";
				colvarVerifyDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifyDate);
				
				TableSchema.TableColumn colvarVerifySuccess = new TableSchema.TableColumn(schema);
				colvarVerifySuccess.ColumnName = "verify_success";
				colvarVerifySuccess.DataType = DbType.Boolean;
				colvarVerifySuccess.MaxLength = 0;
				colvarVerifySuccess.AutoIncrement = false;
				colvarVerifySuccess.IsNullable = false;
				colvarVerifySuccess.IsPrimaryKey = false;
				colvarVerifySuccess.IsForeignKey = false;
				colvarVerifySuccess.IsReadOnly = false;
				
						colvarVerifySuccess.DefaultSetting = @"((0))";
				colvarVerifySuccess.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifySuccess);
				
				TableSchema.TableColumn colvarReplyStatus = new TableSchema.TableColumn(schema);
				colvarReplyStatus.ColumnName = "reply_status";
				colvarReplyStatus.DataType = DbType.Byte;
				colvarReplyStatus.MaxLength = 0;
				colvarReplyStatus.AutoIncrement = false;
				colvarReplyStatus.IsNullable = false;
				colvarReplyStatus.IsPrimaryKey = false;
				colvarReplyStatus.IsForeignKey = false;
				colvarReplyStatus.IsReadOnly = false;
				
						colvarReplyStatus.DefaultSetting = @"((0))";
				colvarReplyStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReplyStatus);
				
				TableSchema.TableColumn colvarReplyMsg = new TableSchema.TableColumn(schema);
				colvarReplyMsg.ColumnName = "reply_msg";
				colvarReplyMsg.DataType = DbType.String;
				colvarReplyMsg.MaxLength = -1;
				colvarReplyMsg.AutoIncrement = false;
				colvarReplyMsg.IsNullable = false;
				colvarReplyMsg.IsPrimaryKey = false;
				colvarReplyMsg.IsForeignKey = false;
				colvarReplyMsg.IsReadOnly = false;
				
						colvarReplyMsg.DefaultSetting = @"('')";
				colvarReplyMsg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReplyMsg);
				
				TableSchema.TableColumn colvarReplyCount = new TableSchema.TableColumn(schema);
				colvarReplyCount.ColumnName = "reply_count";
				colvarReplyCount.DataType = DbType.Int32;
				colvarReplyCount.MaxLength = 0;
				colvarReplyCount.AutoIncrement = false;
				colvarReplyCount.IsNullable = false;
				colvarReplyCount.IsPrimaryKey = false;
				colvarReplyCount.IsForeignKey = false;
				colvarReplyCount.IsReadOnly = false;
				colvarReplyCount.DefaultSetting = @"";
				colvarReplyCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReplyCount);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarSkmToken = new TableSchema.TableColumn(schema);
				colvarSkmToken.ColumnName = "skm_token";
				colvarSkmToken.DataType = DbType.String;
				colvarSkmToken.MaxLength = 50;
				colvarSkmToken.AutoIncrement = false;
				colvarSkmToken.IsNullable = false;
				colvarSkmToken.IsPrimaryKey = false;
				colvarSkmToken.IsForeignKey = false;
				colvarSkmToken.IsReadOnly = false;
				colvarSkmToken.DefaultSetting = @"";
				colvarSkmToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmToken);
				
				TableSchema.TableColumn colvarSkmEventId = new TableSchema.TableColumn(schema);
				colvarSkmEventId.ColumnName = "skm_event_id";
				colvarSkmEventId.DataType = DbType.AnsiString;
				colvarSkmEventId.MaxLength = 20;
				colvarSkmEventId.AutoIncrement = false;
				colvarSkmEventId.IsNullable = false;
				colvarSkmEventId.IsPrimaryKey = false;
				colvarSkmEventId.IsForeignKey = false;
				colvarSkmEventId.IsReadOnly = false;
				
						colvarSkmEventId.DefaultSetting = @"('')";
				colvarSkmEventId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmEventId);
				
				TableSchema.TableColumn colvarSendData = new TableSchema.TableColumn(schema);
				colvarSendData.ColumnName = "send_data";
				colvarSendData.DataType = DbType.AnsiString;
				colvarSendData.MaxLength = -1;
				colvarSendData.AutoIncrement = false;
				colvarSendData.IsNullable = true;
				colvarSendData.IsPrimaryKey = false;
				colvarSendData.IsForeignKey = false;
				colvarSendData.IsReadOnly = false;
				colvarSendData.DefaultSetting = @"";
				colvarSendData.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendData);
				
				TableSchema.TableColumn colvarSkmTmCode = new TableSchema.TableColumn(schema);
				colvarSkmTmCode.ColumnName = "skm_tm_code";
				colvarSkmTmCode.DataType = DbType.AnsiString;
				colvarSkmTmCode.MaxLength = 20;
				colvarSkmTmCode.AutoIncrement = false;
				colvarSkmTmCode.IsNullable = true;
				colvarSkmTmCode.IsPrimaryKey = false;
				colvarSkmTmCode.IsForeignKey = false;
				colvarSkmTmCode.IsReadOnly = false;
				
						colvarSkmTmCode.DefaultSetting = @"('')";
				colvarSkmTmCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmTmCode);
				
				TableSchema.TableColumn colvarSkmReplySn = new TableSchema.TableColumn(schema);
				colvarSkmReplySn.ColumnName = "skm_reply_sn";
				colvarSkmReplySn.DataType = DbType.AnsiString;
				colvarSkmReplySn.MaxLength = 10;
				colvarSkmReplySn.AutoIncrement = false;
				colvarSkmReplySn.IsNullable = false;
				colvarSkmReplySn.IsPrimaryKey = false;
				colvarSkmReplySn.IsForeignKey = false;
				colvarSkmReplySn.IsReadOnly = false;
				
						colvarSkmReplySn.DefaultSetting = @"('')";
				colvarSkmReplySn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmReplySn);
				
				TableSchema.TableColumn colvarIsCancel = new TableSchema.TableColumn(schema);
				colvarIsCancel.ColumnName = "is_cancel";
				colvarIsCancel.DataType = DbType.Boolean;
				colvarIsCancel.MaxLength = 0;
				colvarIsCancel.AutoIncrement = false;
				colvarIsCancel.IsNullable = false;
				colvarIsCancel.IsPrimaryKey = false;
				colvarIsCancel.IsForeignKey = false;
				colvarIsCancel.IsReadOnly = false;
				
						colvarIsCancel.DefaultSetting = @"((0))";
				colvarIsCancel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCancel);
				
				TableSchema.TableColumn colvarCancelTime = new TableSchema.TableColumn(schema);
				colvarCancelTime.ColumnName = "cancel_time";
				colvarCancelTime.DataType = DbType.DateTime;
				colvarCancelTime.MaxLength = 0;
				colvarCancelTime.AutoIncrement = false;
				colvarCancelTime.IsNullable = true;
				colvarCancelTime.IsPrimaryKey = false;
				colvarCancelTime.IsForeignKey = false;
				colvarCancelTime.IsReadOnly = false;
				colvarCancelTime.DefaultSetting = @"";
				colvarCancelTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelTime);
				
				TableSchema.TableColumn colvarCancelSendData = new TableSchema.TableColumn(schema);
				colvarCancelSendData.ColumnName = "cancel_send_data";
				colvarCancelSendData.DataType = DbType.AnsiString;
				colvarCancelSendData.MaxLength = -1;
				colvarCancelSendData.AutoIncrement = false;
				colvarCancelSendData.IsNullable = true;
				colvarCancelSendData.IsPrimaryKey = false;
				colvarCancelSendData.IsForeignKey = false;
				colvarCancelSendData.IsReadOnly = false;
				colvarCancelSendData.DefaultSetting = @"";
				colvarCancelSendData.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelSendData);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_verify_reply",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		
		[XmlAttribute("SkmTransId")]
		[Bindable(true)]
		public string SkmTransId 
		{
			get { return GetColumnValue<string>(Columns.SkmTransId); }
			set { SetColumnValue(Columns.SkmTransId, value); }
		}
		
		[XmlAttribute("ShopCode")]
		[Bindable(true)]
		public string ShopCode 
		{
			get { return GetColumnValue<string>(Columns.ShopCode); }
			set { SetColumnValue(Columns.ShopCode, value); }
		}
		
		[XmlAttribute("BrandCounterCode")]
		[Bindable(true)]
		public string BrandCounterCode 
		{
			get { return GetColumnValue<string>(Columns.BrandCounterCode); }
			set { SetColumnValue(Columns.BrandCounterCode, value); }
		}
		
		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public int CouponId 
		{
			get { return GetColumnValue<int>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}
		
		[XmlAttribute("BurningEventSid")]
		[Bindable(true)]
		public string BurningEventSid 
		{
			get { return GetColumnValue<string>(Columns.BurningEventSid); }
			set { SetColumnValue(Columns.BurningEventSid, value); }
		}
		
		[XmlAttribute("DiscountType")]
		[Bindable(true)]
		public int DiscountType 
		{
			get { return GetColumnValue<int>(Columns.DiscountType); }
			set { SetColumnValue(Columns.DiscountType, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("VerifyDate")]
		[Bindable(true)]
		public DateTime VerifyDate 
		{
			get { return GetColumnValue<DateTime>(Columns.VerifyDate); }
			set { SetColumnValue(Columns.VerifyDate, value); }
		}
		
		[XmlAttribute("VerifySuccess")]
		[Bindable(true)]
		public bool VerifySuccess 
		{
			get { return GetColumnValue<bool>(Columns.VerifySuccess); }
			set { SetColumnValue(Columns.VerifySuccess, value); }
		}
		
		[XmlAttribute("ReplyStatus")]
		[Bindable(true)]
		public byte ReplyStatus 
		{
			get { return GetColumnValue<byte>(Columns.ReplyStatus); }
			set { SetColumnValue(Columns.ReplyStatus, value); }
		}
		
		[XmlAttribute("ReplyMsg")]
		[Bindable(true)]
		public string ReplyMsg 
		{
			get { return GetColumnValue<string>(Columns.ReplyMsg); }
			set { SetColumnValue(Columns.ReplyMsg, value); }
		}
		
		[XmlAttribute("ReplyCount")]
		[Bindable(true)]
		public int ReplyCount 
		{
			get { return GetColumnValue<int>(Columns.ReplyCount); }
			set { SetColumnValue(Columns.ReplyCount, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		[XmlAttribute("SkmToken")]
		[Bindable(true)]
		public string SkmToken 
		{
			get { return GetColumnValue<string>(Columns.SkmToken); }
			set { SetColumnValue(Columns.SkmToken, value); }
		}
		
		[XmlAttribute("SkmEventId")]
		[Bindable(true)]
		public string SkmEventId 
		{
			get { return GetColumnValue<string>(Columns.SkmEventId); }
			set { SetColumnValue(Columns.SkmEventId, value); }
		}
		
		[XmlAttribute("SendData")]
		[Bindable(true)]
		public string SendData 
		{
			get { return GetColumnValue<string>(Columns.SendData); }
			set { SetColumnValue(Columns.SendData, value); }
		}
		
		[XmlAttribute("SkmTmCode")]
		[Bindable(true)]
		public string SkmTmCode 
		{
			get { return GetColumnValue<string>(Columns.SkmTmCode); }
			set { SetColumnValue(Columns.SkmTmCode, value); }
		}
		
		[XmlAttribute("SkmReplySn")]
		[Bindable(true)]
		public string SkmReplySn 
		{
			get { return GetColumnValue<string>(Columns.SkmReplySn); }
			set { SetColumnValue(Columns.SkmReplySn, value); }
		}
		
		[XmlAttribute("IsCancel")]
		[Bindable(true)]
		public bool IsCancel 
		{
			get { return GetColumnValue<bool>(Columns.IsCancel); }
			set { SetColumnValue(Columns.IsCancel, value); }
		}
		
		[XmlAttribute("CancelTime")]
		[Bindable(true)]
		public DateTime? CancelTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CancelTime); }
			set { SetColumnValue(Columns.CancelTime, value); }
		}
		
		[XmlAttribute("CancelSendData")]
		[Bindable(true)]
		public string CancelSendData 
		{
			get { return GetColumnValue<string>(Columns.CancelSendData); }
			set { SetColumnValue(Columns.CancelSendData, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmTransIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ShopCodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn BrandCounterCodeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BurningEventSidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountTypeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn VerifyDateColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn VerifySuccessColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ReplyStatusColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ReplyMsgColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ReplyCountColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmTokenColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmEventIdColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn SendDataColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmTmCodeColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmReplySnColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCancelColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelTimeColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelSendDataColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TrustId = @"trust_id";
			 public static string SkmTransId = @"skm_trans_id";
			 public static string ShopCode = @"shop_code";
			 public static string BrandCounterCode = @"brand_counter_code";
			 public static string CouponId = @"coupon_id";
			 public static string BurningEventSid = @"burning_event_sid";
			 public static string DiscountType = @"discount_type";
			 public static string CreateTime = @"create_time";
			 public static string VerifyDate = @"verify_date";
			 public static string VerifySuccess = @"verify_success";
			 public static string ReplyStatus = @"reply_status";
			 public static string ReplyMsg = @"reply_msg";
			 public static string ReplyCount = @"reply_count";
			 public static string ModifyTime = @"modify_time";
			 public static string SkmToken = @"skm_token";
			 public static string SkmEventId = @"skm_event_id";
			 public static string SendData = @"send_data";
			 public static string SkmTmCode = @"skm_tm_code";
			 public static string SkmReplySn = @"skm_reply_sn";
			 public static string IsCancel = @"is_cancel";
			 public static string CancelTime = @"cancel_time";
			 public static string CancelSendData = @"cancel_send_data";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ProductItem class.
	/// </summary>
    [Serializable]
	public partial class ProductItemCollection : RepositoryList<ProductItem, ProductItemCollection>
	{	   
		public ProductItemCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProductItemCollection</returns>
		public ProductItemCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProductItem o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the product_item table.
	/// </summary>
	[Serializable]
	public partial class ProductItem : RepositoryRecord<ProductItem>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ProductItem()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ProductItem(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("product_item", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarInfoGuid = new TableSchema.TableColumn(schema);
				colvarInfoGuid.ColumnName = "info_guid";
				colvarInfoGuid.DataType = DbType.Guid;
				colvarInfoGuid.MaxLength = 0;
				colvarInfoGuid.AutoIncrement = false;
				colvarInfoGuid.IsNullable = false;
				colvarInfoGuid.IsPrimaryKey = false;
				colvarInfoGuid.IsForeignKey = false;
				colvarInfoGuid.IsReadOnly = false;
				colvarInfoGuid.DefaultSetting = @"";
				colvarInfoGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInfoGuid);
				
				TableSchema.TableColumn colvarProductNo = new TableSchema.TableColumn(schema);
				colvarProductNo.ColumnName = "product_no";
				colvarProductNo.DataType = DbType.Int32;
				colvarProductNo.MaxLength = 0;
				colvarProductNo.AutoIncrement = true;
				colvarProductNo.IsNullable = false;
				colvarProductNo.IsPrimaryKey = false;
				colvarProductNo.IsForeignKey = false;
				colvarProductNo.IsReadOnly = false;
				colvarProductNo.DefaultSetting = @"";
				colvarProductNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductNo);
				
				TableSchema.TableColumn colvarProductCode = new TableSchema.TableColumn(schema);
				colvarProductCode.ColumnName = "product_code";
				colvarProductCode.DataType = DbType.AnsiString;
				colvarProductCode.MaxLength = 50;
				colvarProductCode.AutoIncrement = false;
				colvarProductCode.IsNullable = true;
				colvarProductCode.IsPrimaryKey = false;
				colvarProductCode.IsForeignKey = false;
				colvarProductCode.IsReadOnly = false;
				colvarProductCode.DefaultSetting = @"";
				colvarProductCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductCode);
				
				TableSchema.TableColumn colvarGtins = new TableSchema.TableColumn(schema);
				colvarGtins.ColumnName = "gtins";
				colvarGtins.DataType = DbType.AnsiString;
				colvarGtins.MaxLength = 20;
				colvarGtins.AutoIncrement = false;
				colvarGtins.IsNullable = true;
				colvarGtins.IsPrimaryKey = false;
				colvarGtins.IsForeignKey = false;
				colvarGtins.IsReadOnly = false;
				colvarGtins.DefaultSetting = @"";
				colvarGtins.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGtins);
				
				TableSchema.TableColumn colvarOriStock = new TableSchema.TableColumn(schema);
				colvarOriStock.ColumnName = "ori_stock";
				colvarOriStock.DataType = DbType.Int32;
				colvarOriStock.MaxLength = 0;
				colvarOriStock.AutoIncrement = false;
				colvarOriStock.IsNullable = false;
				colvarOriStock.IsPrimaryKey = false;
				colvarOriStock.IsForeignKey = false;
				colvarOriStock.IsReadOnly = false;
				colvarOriStock.DefaultSetting = @"";
				colvarOriStock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOriStock);
				
				TableSchema.TableColumn colvarStock = new TableSchema.TableColumn(schema);
				colvarStock.ColumnName = "stock";
				colvarStock.DataType = DbType.Int32;
				colvarStock.MaxLength = 0;
				colvarStock.AutoIncrement = false;
				colvarStock.IsNullable = false;
				colvarStock.IsPrimaryKey = false;
				colvarStock.IsForeignKey = false;
				colvarStock.IsReadOnly = false;
				colvarStock.DefaultSetting = @"";
				colvarStock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStock);
				
				TableSchema.TableColumn colvarSales = new TableSchema.TableColumn(schema);
				colvarSales.ColumnName = "sales";
				colvarSales.DataType = DbType.Int32;
				colvarSales.MaxLength = 0;
				colvarSales.AutoIncrement = false;
				colvarSales.IsNullable = false;
				colvarSales.IsPrimaryKey = false;
				colvarSales.IsForeignKey = false;
				colvarSales.IsReadOnly = false;
				colvarSales.DefaultSetting = @"";
				colvarSales.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSales);
				
				TableSchema.TableColumn colvarSafetyStock = new TableSchema.TableColumn(schema);
				colvarSafetyStock.ColumnName = "safety_stock";
				colvarSafetyStock.DataType = DbType.Int32;
				colvarSafetyStock.MaxLength = 0;
				colvarSafetyStock.AutoIncrement = false;
				colvarSafetyStock.IsNullable = false;
				colvarSafetyStock.IsPrimaryKey = false;
				colvarSafetyStock.IsForeignKey = false;
				colvarSafetyStock.IsReadOnly = false;
				colvarSafetyStock.DefaultSetting = @"";
				colvarSafetyStock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSafetyStock);
				
				TableSchema.TableColumn colvarSort = new TableSchema.TableColumn(schema);
				colvarSort.ColumnName = "sort";
				colvarSort.DataType = DbType.Int32;
				colvarSort.MaxLength = 0;
				colvarSort.AutoIncrement = false;
				colvarSort.IsNullable = false;
				colvarSort.IsPrimaryKey = false;
				colvarSort.IsForeignKey = false;
				colvarSort.IsReadOnly = false;
				colvarSort.DefaultSetting = @"";
				colvarSort.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSort);
				
				TableSchema.TableColumn colvarItemStatus = new TableSchema.TableColumn(schema);
				colvarItemStatus.ColumnName = "item_status";
				colvarItemStatus.DataType = DbType.Int32;
				colvarItemStatus.MaxLength = 0;
				colvarItemStatus.AutoIncrement = false;
				colvarItemStatus.IsNullable = false;
				colvarItemStatus.IsPrimaryKey = false;
				colvarItemStatus.IsForeignKey = false;
				colvarItemStatus.IsReadOnly = false;
				colvarItemStatus.DefaultSetting = @"";
				colvarItemStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemStatus);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.AnsiString;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.AnsiString;
				colvarModifyId.MaxLength = 50;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarSpecName = new TableSchema.TableColumn(schema);
				colvarSpecName.ColumnName = "spec_name";
				colvarSpecName.DataType = DbType.String;
				colvarSpecName.MaxLength = 100;
				colvarSpecName.AutoIncrement = false;
				colvarSpecName.IsNullable = true;
				colvarSpecName.IsPrimaryKey = false;
				colvarSpecName.IsForeignKey = false;
				colvarSpecName.IsReadOnly = false;
				colvarSpecName.DefaultSetting = @"";
				colvarSpecName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecName);
				
				TableSchema.TableColumn colvarMpn = new TableSchema.TableColumn(schema);
				colvarMpn.ColumnName = "mpn";
				colvarMpn.DataType = DbType.AnsiString;
				colvarMpn.MaxLength = 70;
				colvarMpn.AutoIncrement = false;
				colvarMpn.IsNullable = true;
				colvarMpn.IsPrimaryKey = false;
				colvarMpn.IsForeignKey = false;
				colvarMpn.IsReadOnly = false;
				colvarMpn.DefaultSetting = @"";
				colvarMpn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMpn);
				
				TableSchema.TableColumn colvarWarehouseType = new TableSchema.TableColumn(schema);
				colvarWarehouseType.ColumnName = "warehouse_type";
				colvarWarehouseType.DataType = DbType.Int32;
				colvarWarehouseType.MaxLength = 0;
				colvarWarehouseType.AutoIncrement = false;
				colvarWarehouseType.IsNullable = true;
				colvarWarehouseType.IsPrimaryKey = false;
				colvarWarehouseType.IsForeignKey = false;
				colvarWarehouseType.IsReadOnly = false;
				colvarWarehouseType.DefaultSetting = @"";
				colvarWarehouseType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWarehouseType);
				
				TableSchema.TableColumn colvarShelfLife = new TableSchema.TableColumn(schema);
				colvarShelfLife.ColumnName = "shelf_life";
				colvarShelfLife.DataType = DbType.Int32;
				colvarShelfLife.MaxLength = 0;
				colvarShelfLife.AutoIncrement = false;
				colvarShelfLife.IsNullable = true;
				colvarShelfLife.IsPrimaryKey = false;
				colvarShelfLife.IsForeignKey = false;
				colvarShelfLife.IsReadOnly = false;
				colvarShelfLife.DefaultSetting = @"";
				colvarShelfLife.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShelfLife);
				
				TableSchema.TableColumn colvarPchomeProdId = new TableSchema.TableColumn(schema);
				colvarPchomeProdId.ColumnName = "pchome_prod_id";
				colvarPchomeProdId.DataType = DbType.String;
				colvarPchomeProdId.MaxLength = 50;
				colvarPchomeProdId.AutoIncrement = false;
				colvarPchomeProdId.IsNullable = true;
				colvarPchomeProdId.IsPrimaryKey = false;
				colvarPchomeProdId.IsForeignKey = false;
				colvarPchomeProdId.IsReadOnly = false;
				colvarPchomeProdId.DefaultSetting = @"";
				colvarPchomeProdId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPchomeProdId);
				
				TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
				colvarPrice.ColumnName = "price";
				colvarPrice.DataType = DbType.Int32;
				colvarPrice.MaxLength = 0;
				colvarPrice.AutoIncrement = false;
				colvarPrice.IsNullable = true;
				colvarPrice.IsPrimaryKey = false;
				colvarPrice.IsForeignKey = false;
				colvarPrice.IsReadOnly = false;
				colvarPrice.DefaultSetting = @"";
				colvarPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrice);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("product_item",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("InfoGuid")]
		[Bindable(true)]
		public Guid InfoGuid 
		{
			get { return GetColumnValue<Guid>(Columns.InfoGuid); }
			set { SetColumnValue(Columns.InfoGuid, value); }
		}
		  
		[XmlAttribute("ProductNo")]
		[Bindable(true)]
		public int ProductNo 
		{
			get { return GetColumnValue<int>(Columns.ProductNo); }
			set { SetColumnValue(Columns.ProductNo, value); }
		}
		  
		[XmlAttribute("ProductCode")]
		[Bindable(true)]
		public string ProductCode 
		{
			get { return GetColumnValue<string>(Columns.ProductCode); }
			set { SetColumnValue(Columns.ProductCode, value); }
		}
		  
		[XmlAttribute("Gtins")]
		[Bindable(true)]
		public string Gtins 
		{
			get { return GetColumnValue<string>(Columns.Gtins); }
			set { SetColumnValue(Columns.Gtins, value); }
		}
		  
		[XmlAttribute("OriStock")]
		[Bindable(true)]
		public int OriStock 
		{
			get { return GetColumnValue<int>(Columns.OriStock); }
			set { SetColumnValue(Columns.OriStock, value); }
		}
		  
		[XmlAttribute("Stock")]
		[Bindable(true)]
		public int Stock 
		{
			get { return GetColumnValue<int>(Columns.Stock); }
			set { SetColumnValue(Columns.Stock, value); }
		}
		  
		[XmlAttribute("Sales")]
		[Bindable(true)]
		public int Sales 
		{
			get { return GetColumnValue<int>(Columns.Sales); }
			set { SetColumnValue(Columns.Sales, value); }
		}
		  
		[XmlAttribute("SafetyStock")]
		[Bindable(true)]
		public int SafetyStock 
		{
			get { return GetColumnValue<int>(Columns.SafetyStock); }
			set { SetColumnValue(Columns.SafetyStock, value); }
		}
		  
		[XmlAttribute("Sort")]
		[Bindable(true)]
		public int Sort 
		{
			get { return GetColumnValue<int>(Columns.Sort); }
			set { SetColumnValue(Columns.Sort, value); }
		}
		  
		[XmlAttribute("ItemStatus")]
		[Bindable(true)]
		public int ItemStatus 
		{
			get { return GetColumnValue<int>(Columns.ItemStatus); }
			set { SetColumnValue(Columns.ItemStatus, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("SpecName")]
		[Bindable(true)]
		public string SpecName 
		{
			get { return GetColumnValue<string>(Columns.SpecName); }
			set { SetColumnValue(Columns.SpecName, value); }
		}
		  
		[XmlAttribute("Mpn")]
		[Bindable(true)]
		public string Mpn 
		{
			get { return GetColumnValue<string>(Columns.Mpn); }
			set { SetColumnValue(Columns.Mpn, value); }
		}
		  
		[XmlAttribute("WarehouseType")]
		[Bindable(true)]
		public int? WarehouseType 
		{
			get { return GetColumnValue<int?>(Columns.WarehouseType); }
			set { SetColumnValue(Columns.WarehouseType, value); }
		}
		  
		[XmlAttribute("ShelfLife")]
		[Bindable(true)]
		public int? ShelfLife 
		{
			get { return GetColumnValue<int?>(Columns.ShelfLife); }
			set { SetColumnValue(Columns.ShelfLife, value); }
		}
		  
		[XmlAttribute("PchomeProdId")]
		[Bindable(true)]
		public string PchomeProdId 
		{
			get { return GetColumnValue<string>(Columns.PchomeProdId); }
			set { SetColumnValue(Columns.PchomeProdId, value); }
		}
		  
		[XmlAttribute("Price")]
		[Bindable(true)]
		public int? Price 
		{
			get { return GetColumnValue<int?>(Columns.Price); }
			set { SetColumnValue(Columns.Price, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn InfoGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductNoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductCodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn GtinsColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn OriStockColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StockColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn SafetyStockColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SortColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemStatusColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecNameColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn MpnColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn WarehouseTypeColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ShelfLifeColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn PchomeProdIdColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn PriceColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"guid";
			 public static string InfoGuid = @"info_guid";
			 public static string ProductNo = @"product_no";
			 public static string ProductCode = @"product_code";
			 public static string Gtins = @"gtins";
			 public static string OriStock = @"ori_stock";
			 public static string Stock = @"stock";
			 public static string Sales = @"sales";
			 public static string SafetyStock = @"safety_stock";
			 public static string Sort = @"sort";
			 public static string ItemStatus = @"item_status";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
			 public static string ModifyTime = @"modify_time";
			 public static string ModifyId = @"modify_id";
			 public static string SpecName = @"spec_name";
			 public static string Mpn = @"mpn";
			 public static string WarehouseType = @"warehouse_type";
			 public static string ShelfLife = @"shelf_life";
			 public static string PchomeProdId = @"pchome_prod_id";
			 public static string Price = @"price";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

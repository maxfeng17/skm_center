using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealTimeSlotHotDealOrderedTotal class.
	/// </summary>
    [Serializable]
	public partial class DealTimeSlotHotDealOrderedTotalCollection : RepositoryList<DealTimeSlotHotDealOrderedTotal, DealTimeSlotHotDealOrderedTotalCollection>
	{	   
		public DealTimeSlotHotDealOrderedTotalCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealTimeSlotHotDealOrderedTotalCollection</returns>
		public DealTimeSlotHotDealOrderedTotalCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealTimeSlotHotDealOrderedTotal o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_time_slot_hot_deal_ordered_total table.
	/// </summary>
	[Serializable]
	public partial class DealTimeSlotHotDealOrderedTotal : RepositoryRecord<DealTimeSlotHotDealOrderedTotal>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealTimeSlotHotDealOrderedTotal()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DealTimeSlotHotDealOrderedTotal(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("deal_time_slot_hot_deal_ordered_total", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = false;
				colvarCityId.IsPrimaryKey = true;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = true;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarEffectiveStart = new TableSchema.TableColumn(schema);
				colvarEffectiveStart.ColumnName = "effective_start";
				colvarEffectiveStart.DataType = DbType.DateTime;
				colvarEffectiveStart.MaxLength = 0;
				colvarEffectiveStart.AutoIncrement = false;
				colvarEffectiveStart.IsNullable = false;
				colvarEffectiveStart.IsPrimaryKey = true;
				colvarEffectiveStart.IsForeignKey = false;
				colvarEffectiveStart.IsReadOnly = false;
				colvarEffectiveStart.DefaultSetting = @"";
				colvarEffectiveStart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEffectiveStart);
				
				TableSchema.TableColumn colvarOrderedTotal = new TableSchema.TableColumn(schema);
				colvarOrderedTotal.ColumnName = "ordered_total";
				colvarOrderedTotal.DataType = DbType.Decimal;
				colvarOrderedTotal.MaxLength = 0;
				colvarOrderedTotal.AutoIncrement = false;
				colvarOrderedTotal.IsNullable = false;
				colvarOrderedTotal.IsPrimaryKey = false;
				colvarOrderedTotal.IsForeignKey = false;
				colvarOrderedTotal.IsReadOnly = false;
				colvarOrderedTotal.DefaultSetting = @"";
				colvarOrderedTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderedTotal);
				
				TableSchema.TableColumn colvarAvgTotal = new TableSchema.TableColumn(schema);
				colvarAvgTotal.ColumnName = "avg_total";
				colvarAvgTotal.DataType = DbType.Decimal;
				colvarAvgTotal.MaxLength = 0;
				colvarAvgTotal.AutoIncrement = false;
				colvarAvgTotal.IsNullable = false;
				colvarAvgTotal.IsPrimaryKey = false;
				colvarAvgTotal.IsForeignKey = false;
				colvarAvgTotal.IsReadOnly = false;
				colvarAvgTotal.DefaultSetting = @"";
				colvarAvgTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAvgTotal);
				
				TableSchema.TableColumn colvarSellDays = new TableSchema.TableColumn(schema);
				colvarSellDays.ColumnName = "sell_days";
				colvarSellDays.DataType = DbType.Decimal;
				colvarSellDays.MaxLength = 0;
				colvarSellDays.AutoIncrement = false;
				colvarSellDays.IsNullable = false;
				colvarSellDays.IsPrimaryKey = false;
				colvarSellDays.IsForeignKey = false;
				colvarSellDays.IsReadOnly = false;
				colvarSellDays.DefaultSetting = @"";
				colvarSellDays.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellDays);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("deal_time_slot_hot_deal_ordered_total",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int CityId 
		{
			get { return GetColumnValue<int>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("EffectiveStart")]
		[Bindable(true)]
		public DateTime EffectiveStart 
		{
			get { return GetColumnValue<DateTime>(Columns.EffectiveStart); }
			set { SetColumnValue(Columns.EffectiveStart, value); }
		}
		  
		[XmlAttribute("OrderedTotal")]
		[Bindable(true)]
		public decimal OrderedTotal 
		{
			get { return GetColumnValue<decimal>(Columns.OrderedTotal); }
			set { SetColumnValue(Columns.OrderedTotal, value); }
		}
		  
		[XmlAttribute("AvgTotal")]
		[Bindable(true)]
		public decimal AvgTotal 
		{
			get { return GetColumnValue<decimal>(Columns.AvgTotal); }
			set { SetColumnValue(Columns.AvgTotal, value); }
		}
		  
		[XmlAttribute("SellDays")]
		[Bindable(true)]
		public decimal SellDays 
		{
			get { return GetColumnValue<decimal>(Columns.SellDays); }
			set { SetColumnValue(Columns.SellDays, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn EffectiveStartColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderedTotalColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AvgTotalColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn SellDaysColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string CityId = @"city_id";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string EffectiveStart = @"effective_start";
			 public static string OrderedTotal = @"ordered_total";
			 public static string AvgTotal = @"avg_total";
			 public static string SellDays = @"sell_days";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

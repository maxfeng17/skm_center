using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    [Serializable]
    public partial class ReferralCampaignCollection : RepositoryList<ReferralCampaign, ReferralCampaignCollection>
    {
        public ReferralCampaignCollection() { }

        public ReferralCampaignCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ReferralCampaign o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }

    [Serializable]
    public partial class ReferralCampaign : RepositoryRecord<ReferralCampaign>, IRecordBase
    {
        #region .ctors and Default Settings
        public ReferralCampaign()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ReferralCampaign(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("referral_campaign", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarReferrerGuid = new TableSchema.TableColumn(schema);
                colvarReferrerGuid.ColumnName = "referrer_guid";
                colvarReferrerGuid.DataType = DbType.Guid;
                colvarReferrerGuid.MaxLength = 0;
                colvarReferrerGuid.AutoIncrement = false;
                colvarReferrerGuid.IsNullable = false;
                colvarReferrerGuid.IsPrimaryKey = false;
                colvarReferrerGuid.IsForeignKey = false;
                colvarReferrerGuid.IsReadOnly = false;
                colvarReferrerGuid.DefaultSetting = @"";
                colvarReferrerGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReferrerGuid);

                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 50;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = false;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                colvarCode.DefaultSetting = @"";
                colvarCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCode);

                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 50;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = false;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                colvarName.DefaultSetting = @"";
                colvarName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarName);

                TableSchema.TableColumn colvarSessionDuration = new TableSchema.TableColumn(schema);
                colvarSessionDuration.ColumnName = "session_duration";
                colvarSessionDuration.DataType = DbType.Int32;
                colvarSessionDuration.MaxLength = 0;
                colvarSessionDuration.AutoIncrement = false;
                colvarSessionDuration.IsNullable = false;
                colvarSessionDuration.IsPrimaryKey = false;
                colvarSessionDuration.IsForeignKey = false;
                colvarSessionDuration.IsReadOnly = false;
                colvarSessionDuration.DefaultSetting = @"";
                colvarSessionDuration.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSessionDuration);

                TableSchema.TableColumn colvarOneShotAction = new TableSchema.TableColumn(schema);
                colvarOneShotAction.ColumnName = "one_shot_action";
                colvarOneShotAction.DataType = DbType.Boolean;
                colvarOneShotAction.MaxLength = 0;
                colvarOneShotAction.AutoIncrement = false;
                colvarOneShotAction.IsNullable = false;
                colvarOneShotAction.IsPrimaryKey = false;
                colvarOneShotAction.IsForeignKey = false;
                colvarOneShotAction.IsReadOnly = false;
                colvarOneShotAction.DefaultSetting = @"";
                colvarOneShotAction.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOneShotAction);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 256;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarRsrc = new TableSchema.TableColumn(schema);
                colvarRsrc.ColumnName = "rsrc";
                colvarRsrc.DataType = DbType.String;
                colvarRsrc.MaxLength = 50;
                colvarRsrc.AutoIncrement = false;
                colvarRsrc.IsNullable = true;
                colvarRsrc.IsPrimaryKey = false;
                colvarRsrc.IsForeignKey = false;
                colvarRsrc.IsReadOnly = false;
                colvarRsrc.DefaultSetting = @"";
                colvarRsrc.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRsrc);

                TableSchema.TableColumn colvarIsDeleted = new TableSchema.TableColumn(schema);
                colvarIsDeleted.ColumnName = "is_deleted";
                colvarIsDeleted.DataType = DbType.Boolean;
                colvarIsDeleted.MaxLength = 0;
                colvarIsDeleted.AutoIncrement = false;
                colvarIsDeleted.IsNullable = false;
                colvarIsDeleted.IsPrimaryKey = false;
                colvarIsDeleted.IsForeignKey = false;
                colvarIsDeleted.IsReadOnly = false;
                colvarIsDeleted.DefaultSetting = @"((0))";
                colvarIsDeleted.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsDeleted);

                TableSchema.TableColumn colvarIsExternal = new TableSchema.TableColumn(schema);
                colvarIsExternal.ColumnName = "is_external";
                colvarIsExternal.DataType = DbType.Boolean;
                colvarIsExternal.MaxLength = 0;
                colvarIsExternal.AutoIncrement = false;
                colvarIsExternal.IsNullable = false;
                colvarIsExternal.IsPrimaryKey = false;
                colvarIsExternal.IsForeignKey = false;
                colvarIsExternal.IsReadOnly = false;
                colvarIsExternal.DefaultSetting = @"((0))";
                colvarIsExternal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsExternal);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("referral_campaign", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("ReferrerGuid")]
        [Bindable(true)]
        public Guid ReferrerGuid
        {
            get { return GetColumnValue<Guid>(Columns.ReferrerGuid); }
            set { SetColumnValue(Columns.ReferrerGuid, value); }
        }

        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code
        {
            get { return GetColumnValue<string>(Columns.Code); }
            set { SetColumnValue(Columns.Code, value); }
        }

        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name
        {
            get { return GetColumnValue<string>(Columns.Name); }
            set { SetColumnValue(Columns.Name, value); }
        }

        [XmlAttribute("SessionDuration")]
        [Bindable(true)]
        public int SessionDuration
        {
            get { return GetColumnValue<int>(Columns.SessionDuration); }
            set { SetColumnValue(Columns.SessionDuration, value); }
        }

        [XmlAttribute("OneShotAction")]
        [Bindable(true)]
        public bool OneShotAction
        {
            get { return GetColumnValue<bool>(Columns.OneShotAction); }
            set { SetColumnValue(Columns.OneShotAction, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("Rsrc")]
        [Bindable(true)]
        public string Rsrc
        {
            get { return GetColumnValue<string>(Columns.Rsrc); }
            set { SetColumnValue(Columns.Rsrc, value); }
        }

        [XmlAttribute("IsDeleted")]
        [Bindable(true)]
        public bool IsDeleted
        {
            get { return GetColumnValue<bool>(Columns.IsDeleted); }
            set { SetColumnValue(Columns.IsDeleted, value); }
        }

        [XmlAttribute("IsExternal")]
        [Bindable(true)]
        public bool IsExternal
        {
            get { return GetColumnValue<bool>(Columns.IsExternal); }
            set { SetColumnValue(Columns.IsExternal, value); }
        }
        #endregion

        #region Typed Columns

        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }

        public static TableSchema.TableColumn ReferrerGuidColumn
        {
            get { return Schema.Columns[1]; }
        }

        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[2]; }
        }

        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[3]; }
        }

        public static TableSchema.TableColumn SessionDurationColumn
        {
            get { return Schema.Columns[4]; }
        }

        public static TableSchema.TableColumn OneShotActionColumn
        {
            get { return Schema.Columns[5]; }
        }

        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }

        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }

        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[8]; }
        }

        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[9]; }
        }

        public static TableSchema.TableColumn RsrcColumn
        {
            get { return Schema.Columns[10]; }
        }

        public static TableSchema.TableColumn IsDeletedColumn
        {
            get { return Schema.Columns[11]; }
        }

        public static TableSchema.TableColumn IsExternalColumn
        {
            get { return Schema.Columns[12]; }
        }
        #endregion

        #region Columns Struct

        public struct Columns
        {
            public static string Guid = @"guid";
            public static string ReferrerGuid = @"referrer_guid";
            public static string Code = @"code";
            public static string Name = @"name";
            public static string SessionDuration = @"session_duration";
            public static string OneShotAction = @"one_shot_action";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string Rsrc = @"rsrc";
            public static string IsDeleted = @"is_deleted";
            public static string IsExternal = @"is_external";
        }

        #endregion

    }
}

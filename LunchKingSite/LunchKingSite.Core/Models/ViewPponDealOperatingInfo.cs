using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponDealOperatingInfo class.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealOperatingInfoCollection : ReadOnlyList<ViewPponDealOperatingInfo, ViewPponDealOperatingInfoCollection>
    {        
        public ViewPponDealOperatingInfoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_deal_operating_info view.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealOperatingInfo : ReadOnlyRecord<ViewPponDealOperatingInfo>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_deal_operating_info", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarBaseCost = new TableSchema.TableColumn(schema);
                colvarBaseCost.ColumnName = "base_cost";
                colvarBaseCost.DataType = DbType.Currency;
                colvarBaseCost.MaxLength = 0;
                colvarBaseCost.AutoIncrement = false;
                colvarBaseCost.IsNullable = false;
                colvarBaseCost.IsPrimaryKey = false;
                colvarBaseCost.IsForeignKey = false;
                colvarBaseCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarBaseCost);
                
                TableSchema.TableColumn colvarMultistepCost = new TableSchema.TableColumn(schema);
                colvarMultistepCost.ColumnName = "multistep_cost";
                colvarMultistepCost.DataType = DbType.Currency;
                colvarMultistepCost.MaxLength = 0;
                colvarMultistepCost.AutoIncrement = false;
                colvarMultistepCost.IsNullable = false;
                colvarMultistepCost.IsPrimaryKey = false;
                colvarMultistepCost.IsForeignKey = false;
                colvarMultistepCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarMultistepCost);
                
                TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
                colvarItemOrigPrice.ColumnName = "item_orig_price";
                colvarItemOrigPrice.DataType = DbType.Currency;
                colvarItemOrigPrice.MaxLength = 0;
                colvarItemOrigPrice.AutoIncrement = false;
                colvarItemOrigPrice.IsNullable = false;
                colvarItemOrigPrice.IsPrimaryKey = false;
                colvarItemOrigPrice.IsForeignKey = false;
                colvarItemOrigPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemOrigPrice);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarOrderedQuantity = new TableSchema.TableColumn(schema);
                colvarOrderedQuantity.ColumnName = "ordered_quantity";
                colvarOrderedQuantity.DataType = DbType.Int32;
                colvarOrderedQuantity.MaxLength = 0;
                colvarOrderedQuantity.AutoIncrement = false;
                colvarOrderedQuantity.IsNullable = false;
                colvarOrderedQuantity.IsPrimaryKey = false;
                colvarOrderedQuantity.IsForeignKey = false;
                colvarOrderedQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderedQuantity);
                
                TableSchema.TableColumn colvarGrossMargin = new TableSchema.TableColumn(schema);
                colvarGrossMargin.ColumnName = "gross_margin";
                colvarGrossMargin.DataType = DbType.Currency;
                colvarGrossMargin.MaxLength = 0;
                colvarGrossMargin.AutoIncrement = false;
                colvarGrossMargin.IsNullable = true;
                colvarGrossMargin.IsPrimaryKey = false;
                colvarGrossMargin.IsForeignKey = false;
                colvarGrossMargin.IsReadOnly = false;
                
                schema.Columns.Add(colvarGrossMargin);
                
                TableSchema.TableColumn colvarGrossProfit = new TableSchema.TableColumn(schema);
                colvarGrossProfit.ColumnName = "gross_profit";
                colvarGrossProfit.DataType = DbType.Currency;
                colvarGrossProfit.MaxLength = 0;
                colvarGrossProfit.AutoIncrement = false;
                colvarGrossProfit.IsNullable = false;
                colvarGrossProfit.IsPrimaryKey = false;
                colvarGrossProfit.IsForeignKey = false;
                colvarGrossProfit.IsReadOnly = false;
                
                schema.Columns.Add(colvarGrossProfit);
                
                TableSchema.TableColumn colvarMultistepGrossMargin = new TableSchema.TableColumn(schema);
                colvarMultistepGrossMargin.ColumnName = "multistep_gross_margin";
                colvarMultistepGrossMargin.DataType = DbType.Currency;
                colvarMultistepGrossMargin.MaxLength = 0;
                colvarMultistepGrossMargin.AutoIncrement = false;
                colvarMultistepGrossMargin.IsNullable = true;
                colvarMultistepGrossMargin.IsPrimaryKey = false;
                colvarMultistepGrossMargin.IsForeignKey = false;
                colvarMultistepGrossMargin.IsReadOnly = false;
                
                schema.Columns.Add(colvarMultistepGrossMargin);
                
                TableSchema.TableColumn colvarMultistepGrossProfit = new TableSchema.TableColumn(schema);
                colvarMultistepGrossProfit.ColumnName = "multistep_gross_profit";
                colvarMultistepGrossProfit.DataType = DbType.Currency;
                colvarMultistepGrossProfit.MaxLength = 0;
                colvarMultistepGrossProfit.AutoIncrement = false;
                colvarMultistepGrossProfit.IsNullable = false;
                colvarMultistepGrossProfit.IsPrimaryKey = false;
                colvarMultistepGrossProfit.IsForeignKey = false;
                colvarMultistepGrossProfit.IsReadOnly = false;
                
                schema.Columns.Add(colvarMultistepGrossProfit);
                
                TableSchema.TableColumn colvarOrderedTotal = new TableSchema.TableColumn(schema);
                colvarOrderedTotal.ColumnName = "ordered_total";
                colvarOrderedTotal.DataType = DbType.Currency;
                colvarOrderedTotal.MaxLength = 0;
                colvarOrderedTotal.AutoIncrement = false;
                colvarOrderedTotal.IsNullable = false;
                colvarOrderedTotal.IsPrimaryKey = false;
                colvarOrderedTotal.IsForeignKey = false;
                colvarOrderedTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderedTotal);
                
                TableSchema.TableColumn colvarDiscountRate = new TableSchema.TableColumn(schema);
                colvarDiscountRate.ColumnName = "discount_rate";
                colvarDiscountRate.DataType = DbType.Currency;
                colvarDiscountRate.MaxLength = 0;
                colvarDiscountRate.AutoIncrement = false;
                colvarDiscountRate.IsNullable = true;
                colvarDiscountRate.IsPrimaryKey = false;
                colvarDiscountRate.IsForeignKey = false;
                colvarDiscountRate.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountRate);
                
                TableSchema.TableColumn colvarSellDays = new TableSchema.TableColumn(schema);
                colvarSellDays.ColumnName = "sell_days";
                colvarSellDays.DataType = DbType.Decimal;
                colvarSellDays.MaxLength = 0;
                colvarSellDays.AutoIncrement = false;
                colvarSellDays.IsNullable = true;
                colvarSellDays.IsPrimaryKey = false;
                colvarSellDays.IsForeignKey = false;
                colvarSellDays.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellDays);
                
                TableSchema.TableColumn colvarSellerLevel = new TableSchema.TableColumn(schema);
                colvarSellerLevel.ColumnName = "seller_level";
                colvarSellerLevel.DataType = DbType.Int32;
                colvarSellerLevel.MaxLength = 0;
                colvarSellerLevel.AutoIncrement = false;
                colvarSellerLevel.IsNullable = false;
                colvarSellerLevel.IsPrimaryKey = false;
                colvarSellerLevel.IsForeignKey = false;
                colvarSellerLevel.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerLevel);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_deal_operating_info",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponDealOperatingInfo()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponDealOperatingInfo(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponDealOperatingInfo(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponDealOperatingInfo(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("BaseCost")]
        [Bindable(true)]
        public decimal BaseCost 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("base_cost");
		    }
            set 
		    {
			    SetColumnValue("base_cost", value);
            }
        }
	      
        [XmlAttribute("MultistepCost")]
        [Bindable(true)]
        public decimal MultistepCost 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("multistep_cost");
		    }
            set 
		    {
			    SetColumnValue("multistep_cost", value);
            }
        }
	      
        [XmlAttribute("ItemOrigPrice")]
        [Bindable(true)]
        public decimal ItemOrigPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_orig_price");
		    }
            set 
		    {
			    SetColumnValue("item_orig_price", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("OrderedQuantity")]
        [Bindable(true)]
        public int OrderedQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("ordered_quantity");
		    }
            set 
		    {
			    SetColumnValue("ordered_quantity", value);
            }
        }
	      
        [XmlAttribute("GrossMargin")]
        [Bindable(true)]
        public decimal? GrossMargin 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("gross_margin");
		    }
            set 
		    {
			    SetColumnValue("gross_margin", value);
            }
        }
	      
        [XmlAttribute("GrossProfit")]
        [Bindable(true)]
        public decimal GrossProfit 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("gross_profit");
		    }
            set 
		    {
			    SetColumnValue("gross_profit", value);
            }
        }
	      
        [XmlAttribute("MultistepGrossMargin")]
        [Bindable(true)]
        public decimal? MultistepGrossMargin 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("multistep_gross_margin");
		    }
            set 
		    {
			    SetColumnValue("multistep_gross_margin", value);
            }
        }
	      
        [XmlAttribute("MultistepGrossProfit")]
        [Bindable(true)]
        public decimal MultistepGrossProfit 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("multistep_gross_profit");
		    }
            set 
		    {
			    SetColumnValue("multistep_gross_profit", value);
            }
        }
	      
        [XmlAttribute("OrderedTotal")]
        [Bindable(true)]
        public decimal OrderedTotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("ordered_total");
		    }
            set 
		    {
			    SetColumnValue("ordered_total", value);
            }
        }
	      
        [XmlAttribute("DiscountRate")]
        [Bindable(true)]
        public decimal? DiscountRate 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("discount_rate");
		    }
            set 
		    {
			    SetColumnValue("discount_rate", value);
            }
        }
	      
        [XmlAttribute("SellDays")]
        [Bindable(true)]
        public decimal? SellDays 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("sell_days");
		    }
            set 
		    {
			    SetColumnValue("sell_days", value);
            }
        }
	      
        [XmlAttribute("SellerLevel")]
        [Bindable(true)]
        public int SellerLevel 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_level");
		    }
            set 
		    {
			    SetColumnValue("seller_level", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string BaseCost = @"base_cost";
            
            public static string MultistepCost = @"multistep_cost";
            
            public static string ItemOrigPrice = @"item_orig_price";
            
            public static string ItemPrice = @"item_price";
            
            public static string OrderedQuantity = @"ordered_quantity";
            
            public static string GrossMargin = @"gross_margin";
            
            public static string GrossProfit = @"gross_profit";
            
            public static string MultistepGrossMargin = @"multistep_gross_margin";
            
            public static string MultistepGrossProfit = @"multistep_gross_profit";
            
            public static string OrderedTotal = @"ordered_total";
            
            public static string DiscountRate = @"discount_rate";
            
            public static string SellDays = @"sell_days";
            
            public static string SellerLevel = @"seller_level";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

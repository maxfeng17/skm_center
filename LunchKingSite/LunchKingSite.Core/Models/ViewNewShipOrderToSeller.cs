using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewNewShipOrderToSeller class.
    /// </summary>
    [Serializable]
    public partial class ViewNewShipOrderToSellerCollection : ReadOnlyList<ViewNewShipOrderToSeller, ViewNewShipOrderToSellerCollection>
    {
        public ViewNewShipOrderToSellerCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_new_ship_order_to_seller view.
    /// </summary>
    [Serializable]
    public partial class ViewNewShipOrderToSeller : ReadOnlyRecord<ViewNewShipOrderToSeller>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_new_ship_order_to_seller", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;

                schema.Columns.Add(colvarOrderId);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;

                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;

                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;

                schema.Columns.Add(colvarMemberName);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;

                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;

                schema.Columns.Add(colvarSellerName);

                TableSchema.TableColumn colvarTotalCount = new TableSchema.TableColumn(schema);
                colvarTotalCount.ColumnName = "total_count";
                colvarTotalCount.DataType = DbType.Int32;
                colvarTotalCount.MaxLength = 0;
                colvarTotalCount.AutoIncrement = false;
                colvarTotalCount.IsNullable = true;
                colvarTotalCount.IsPrimaryKey = false;
                colvarTotalCount.IsForeignKey = false;
                colvarTotalCount.IsReadOnly = false;

                schema.Columns.Add(colvarTotalCount);

                TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
                colvarShipType.ColumnName = "ship_type";
                colvarShipType.DataType = DbType.Int32;
                colvarShipType.MaxLength = 0;
                colvarShipType.AutoIncrement = false;
                colvarShipType.IsNullable = true;
                colvarShipType.IsPrimaryKey = false;
                colvarShipType.IsForeignKey = false;
                colvarShipType.IsReadOnly = false;

                schema.Columns.Add(colvarShipType);

                TableSchema.TableColumn colvarShippingdateType = new TableSchema.TableColumn(schema);
                colvarShippingdateType.ColumnName = "shippingdate_type";
                colvarShippingdateType.DataType = DbType.Int32;
                colvarShippingdateType.MaxLength = 0;
                colvarShippingdateType.AutoIncrement = false;
                colvarShippingdateType.IsNullable = true;
                colvarShippingdateType.IsPrimaryKey = false;
                colvarShippingdateType.IsForeignKey = false;
                colvarShippingdateType.IsReadOnly = false;

                schema.Columns.Add(colvarShippingdateType);

                TableSchema.TableColumn colvarShippingdate = new TableSchema.TableColumn(schema);
                colvarShippingdate.ColumnName = "shippingdate";
                colvarShippingdate.DataType = DbType.Int32;
                colvarShippingdate.MaxLength = 0;
                colvarShippingdate.AutoIncrement = false;
                colvarShippingdate.IsNullable = true;
                colvarShippingdate.IsPrimaryKey = false;
                colvarShippingdate.IsForeignKey = false;
                colvarShippingdate.IsReadOnly = false;

                schema.Columns.Add(colvarShippingdate);

                TableSchema.TableColumn colvarProductUseDateStartSet = new TableSchema.TableColumn(schema);
                colvarProductUseDateStartSet.ColumnName = "product_use_date_start_set";
                colvarProductUseDateStartSet.DataType = DbType.Int32;
                colvarProductUseDateStartSet.MaxLength = 0;
                colvarProductUseDateStartSet.AutoIncrement = false;
                colvarProductUseDateStartSet.IsNullable = true;
                colvarProductUseDateStartSet.IsPrimaryKey = false;
                colvarProductUseDateStartSet.IsForeignKey = false;
                colvarProductUseDateStartSet.IsReadOnly = false;

                schema.Columns.Add(colvarProductUseDateStartSet);

                TableSchema.TableColumn colvarProductUseDateEndSet = new TableSchema.TableColumn(schema);
                colvarProductUseDateEndSet.ColumnName = "product_use_date_end_set";
                colvarProductUseDateEndSet.DataType = DbType.Int32;
                colvarProductUseDateEndSet.MaxLength = 0;
                colvarProductUseDateEndSet.AutoIncrement = false;
                colvarProductUseDateEndSet.IsNullable = true;
                colvarProductUseDateEndSet.IsPrimaryKey = false;
                colvarProductUseDateEndSet.IsForeignKey = false;
                colvarProductUseDateEndSet.IsReadOnly = false;

                schema.Columns.Add(colvarProductUseDateEndSet);

                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourOrderTimeE);

                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourDeliverTimeE);

                TableSchema.TableColumn colvarIsWms = new TableSchema.TableColumn(schema);
                colvarIsWms.ColumnName = "is_wms";
                colvarIsWms.DataType = DbType.Boolean;
                colvarIsWms.MaxLength = 0;
                colvarIsWms.AutoIncrement = false;
                colvarIsWms.IsNullable = false;
                colvarIsWms.IsPrimaryKey = false;
                colvarIsWms.IsForeignKey = false;
                colvarIsWms.IsReadOnly = false;

                schema.Columns.Add(colvarIsWms);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_new_ship_order_to_seller",schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewNewShipOrderToSeller()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewNewShipOrderToSeller(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewNewShipOrderToSeller(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewNewShipOrderToSeller(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId
        {
            get
            {
                return GetColumnValue<string>("order_id");
            }
            set
            {
                SetColumnValue("order_id", value);
            }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get
            {
                return GetColumnValue<Guid>("GUID");
            }
            set
            {
                SetColumnValue("GUID", value);
            }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get
            {
                return GetColumnValue<string>("item_name");
            }
            set
            {
                SetColumnValue("item_name", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName
        {
            get
            {
                return GetColumnValue<string>("member_name");
            }
            set
            {
                SetColumnValue("member_name", value);
            }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid
        {
            get
            {
                return GetColumnValue<Guid>("seller_GUID");
            }
            set
            {
                SetColumnValue("seller_GUID", value);
            }
        }

        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName
        {
            get
            {
                return GetColumnValue<string>("seller_name");
            }
            set
            {
                SetColumnValue("seller_name", value);
            }
        }

        [XmlAttribute("TotalCount")]
        [Bindable(true)]
        public int? TotalCount
        {
            get
            {
                return GetColumnValue<int?>("total_count");
            }
            set
            {
                SetColumnValue("total_count", value);
            }
        }

        [XmlAttribute("ShipType")]
        [Bindable(true)]
        public int? ShipType
        {
            get
            {
                return GetColumnValue<int?>("ship_type");
            }
            set
            {
                SetColumnValue("ship_type", value);
            }
        }

        [XmlAttribute("ShippingdateType")]
        [Bindable(true)]
        public int? ShippingdateType
        {
            get
            {
                return GetColumnValue<int?>("shippingdate_type");
            }
            set
            {
                SetColumnValue("shippingdate_type", value);
            }
        }

        [XmlAttribute("Shippingdate")]
        [Bindable(true)]
        public int? Shippingdate
        {
            get
            {
                return GetColumnValue<int?>("shippingdate");
            }
            set
            {
                SetColumnValue("shippingdate", value);
            }
        }

        [XmlAttribute("ProductUseDateStartSet")]
        [Bindable(true)]
        public int? ProductUseDateStartSet
        {
            get
            {
                return GetColumnValue<int?>("product_use_date_start_set");
            }
            set
            {
                SetColumnValue("product_use_date_start_set", value);
            }
        }

        [XmlAttribute("ProductUseDateEndSet")]
        [Bindable(true)]
        public int? ProductUseDateEndSet
        {
            get
            {
                return GetColumnValue<int?>("product_use_date_end_set");
            }
            set
            {
                SetColumnValue("product_use_date_end_set", value);
            }
        }

        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE
        {
            get
            {
                return GetColumnValue<DateTime>("business_hour_order_time_e");
            }
            set
            {
                SetColumnValue("business_hour_order_time_e", value);
            }
        }

        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE
        {
            get
            {
                return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
            }
            set
            {
                SetColumnValue("business_hour_deliver_time_e", value);
            }
        }

        [XmlAttribute("IsWms")]
        [Bindable(true)]
        public bool IsWms
        {
            get
            {
                return GetColumnValue<bool>("is_wms");
            }
            set
            {
                SetColumnValue("is_wms", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string OrderId = @"order_id";

            public static string Guid = @"GUID";

            public static string ItemName = @"item_name";

            public static string CreateTime = @"create_time";

            public static string MemberName = @"member_name";

            public static string SellerGuid = @"seller_GUID";

            public static string SellerName = @"seller_name";

            public static string TotalCount = @"total_count";

            public static string ShipType = @"ship_type";

            public static string ShippingdateType = @"shippingdate_type";

            public static string Shippingdate = @"shippingdate";

            public static string ProductUseDateStartSet = @"product_use_date_start_set";

            public static string ProductUseDateEndSet = @"product_use_date_end_set";

            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";

            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";

            public static string IsWms = @"is_wms";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

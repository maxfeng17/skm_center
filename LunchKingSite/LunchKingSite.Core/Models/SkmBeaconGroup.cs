﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the SkmBeaconGroup class.
    /// </summary>
    [Serializable]
    public partial class SkmBeaconGroupCollection : RepositoryList<SkmBeaconGroup, SkmBeaconGroupCollection>
    {
        public SkmBeaconGroupCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmBeaconGroupCollection</returns>
        public SkmBeaconGroupCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmBeaconGroup o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the skm_beacon_groups table.
    /// </summary>
    [Serializable]
    public partial class SkmBeaconGroup : RepositoryRecord<SkmBeaconGroup>, IRecordBase
    {
        #region .ctors and Default Settings

        public SkmBeaconGroup()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SkmBeaconGroup(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("skm_beacon_groups", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int64;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarMemberId = new TableSchema.TableColumn(schema);
                colvarMemberId.ColumnName = "member_id";
                colvarMemberId.DataType = DbType.AnsiString;
                colvarMemberId.MaxLength = 50;
                colvarMemberId.AutoIncrement = false;
                colvarMemberId.IsNullable = false;
                colvarMemberId.IsPrimaryKey = false;
                colvarMemberId.IsForeignKey = false;
                colvarMemberId.IsReadOnly = false;
                colvarMemberId.DefaultSetting = @"";
                colvarMemberId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMemberId);

                TableSchema.TableColumn colvarBsLabel = new TableSchema.TableColumn(schema);
                colvarBsLabel.ColumnName = "bs_label";
                colvarBsLabel.DataType = DbType.String;
                colvarBsLabel.MaxLength = 50;
                colvarBsLabel.AutoIncrement = false;
                colvarBsLabel.IsNullable = true;
                colvarBsLabel.IsPrimaryKey = false;
                colvarBsLabel.IsForeignKey = false;
                colvarBsLabel.IsReadOnly = false;
                colvarBsLabel.DefaultSetting = @"";
                colvarBsLabel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBsLabel);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("skm_beacon_groups", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public long Id
        {
            get { return GetColumnValue<long>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("MemberId")]
        [Bindable(true)]
        public string MemberId
        {
            get { return GetColumnValue<string>(Columns.MemberId); }
            set { SetColumnValue(Columns.MemberId, value); }
        }

        [XmlAttribute("BsLabel")]
        [Bindable(true)]
        public string BsLabel
        {
            get { return GetColumnValue<string>(Columns.BsLabel); }
            set { SetColumnValue(Columns.BsLabel, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn MemberIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn BsLabelColumn
        {
            get { return Schema.Columns[3]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Guid = @"guid";
            public static string MemberId = @"member_id";
            public static string BsLabel = @"bs_label";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBalanceSheetFundsTransferPpon class.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetFundsTransferPponCollection : ReadOnlyList<ViewBalanceSheetFundsTransferPpon, ViewBalanceSheetFundsTransferPponCollection>
    {        
        public ViewBalanceSheetFundsTransferPponCollection() {} 
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_balance_sheet_funds_transfer_ppon view.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetFundsTransferPpon : ReadOnlyRecord<ViewBalanceSheetFundsTransferPpon>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_balance_sheet_funds_transfer_ppon", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBsId = new TableSchema.TableColumn(schema);
                colvarBsId.ColumnName = "bs_id";
                colvarBsId.DataType = DbType.Int32;
                colvarBsId.MaxLength = 0;
                colvarBsId.AutoIncrement = false;
                colvarBsId.IsNullable = false;
                colvarBsId.IsPrimaryKey = false;
                colvarBsId.IsForeignKey = false;
                colvarBsId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBsId);
                
                TableSchema.TableColumn colvarDealGuid = new TableSchema.TableColumn(schema);
                colvarDealGuid.ColumnName = "deal_guid";
                colvarDealGuid.DataType = DbType.Guid;
                colvarDealGuid.MaxLength = 0;
                colvarDealGuid.AutoIncrement = false;
                colvarDealGuid.IsNullable = false;
                colvarDealGuid.IsPrimaryKey = false;
                colvarDealGuid.IsForeignKey = false;
                colvarDealGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealGuid);
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarProductType = new TableSchema.TableColumn(schema);
                colvarProductType.ColumnName = "product_type";
                colvarProductType.DataType = DbType.Int32;
                colvarProductType.MaxLength = 0;
                colvarProductType.AutoIncrement = false;
                colvarProductType.IsNullable = false;
                colvarProductType.IsPrimaryKey = false;
                colvarProductType.IsForeignKey = false;
                colvarProductType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductType);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarBalanceSheetType = new TableSchema.TableColumn(schema);
                colvarBalanceSheetType.ColumnName = "balance_sheet_type";
                colvarBalanceSheetType.DataType = DbType.Int32;
                colvarBalanceSheetType.MaxLength = 0;
                colvarBalanceSheetType.AutoIncrement = false;
                colvarBalanceSheetType.IsNullable = false;
                colvarBalanceSheetType.IsPrimaryKey = false;
                colvarBalanceSheetType.IsForeignKey = false;
                colvarBalanceSheetType.IsReadOnly = false;
                
                schema.Columns.Add(colvarBalanceSheetType);
                
                TableSchema.TableColumn colvarBsYear = new TableSchema.TableColumn(schema);
                colvarBsYear.ColumnName = "bs_year";
                colvarBsYear.DataType = DbType.Int32;
                colvarBsYear.MaxLength = 0;
                colvarBsYear.AutoIncrement = false;
                colvarBsYear.IsNullable = true;
                colvarBsYear.IsPrimaryKey = false;
                colvarBsYear.IsForeignKey = false;
                colvarBsYear.IsReadOnly = false;
                
                schema.Columns.Add(colvarBsYear);
                
                TableSchema.TableColumn colvarBsMonth = new TableSchema.TableColumn(schema);
                colvarBsMonth.ColumnName = "bs_month";
                colvarBsMonth.DataType = DbType.Int32;
                colvarBsMonth.MaxLength = 0;
                colvarBsMonth.AutoIncrement = false;
                colvarBsMonth.IsNullable = true;
                colvarBsMonth.IsPrimaryKey = false;
                colvarBsMonth.IsForeignKey = false;
                colvarBsMonth.IsReadOnly = false;
                
                schema.Columns.Add(colvarBsMonth);
                
                TableSchema.TableColumn colvarBsIntervalStart = new TableSchema.TableColumn(schema);
                colvarBsIntervalStart.ColumnName = "bs_interval_start";
                colvarBsIntervalStart.DataType = DbType.DateTime;
                colvarBsIntervalStart.MaxLength = 0;
                colvarBsIntervalStart.AutoIncrement = false;
                colvarBsIntervalStart.IsNullable = false;
                colvarBsIntervalStart.IsPrimaryKey = false;
                colvarBsIntervalStart.IsForeignKey = false;
                colvarBsIntervalStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarBsIntervalStart);
                
                TableSchema.TableColumn colvarBsIntervalEnd = new TableSchema.TableColumn(schema);
                colvarBsIntervalEnd.ColumnName = "bs_interval_end";
                colvarBsIntervalEnd.DataType = DbType.DateTime;
                colvarBsIntervalEnd.MaxLength = 0;
                colvarBsIntervalEnd.AutoIncrement = false;
                colvarBsIntervalEnd.IsNullable = false;
                colvarBsIntervalEnd.IsPrimaryKey = false;
                colvarBsIntervalEnd.IsForeignKey = false;
                colvarBsIntervalEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarBsIntervalEnd);
                
                TableSchema.TableColumn colvarBsGenerationFrequency = new TableSchema.TableColumn(schema);
                colvarBsGenerationFrequency.ColumnName = "bs_generation_frequency";
                colvarBsGenerationFrequency.DataType = DbType.Int32;
                colvarBsGenerationFrequency.MaxLength = 0;
                colvarBsGenerationFrequency.AutoIncrement = false;
                colvarBsGenerationFrequency.IsNullable = false;
                colvarBsGenerationFrequency.IsPrimaryKey = false;
                colvarBsGenerationFrequency.IsForeignKey = false;
                colvarBsGenerationFrequency.IsReadOnly = false;
                
                schema.Columns.Add(colvarBsGenerationFrequency);
                
                TableSchema.TableColumn colvarBsIsManual = new TableSchema.TableColumn(schema);
                colvarBsIsManual.ColumnName = "bs_is_manual";
                colvarBsIsManual.DataType = DbType.Boolean;
                colvarBsIsManual.MaxLength = 0;
                colvarBsIsManual.AutoIncrement = false;
                colvarBsIsManual.IsNullable = false;
                colvarBsIsManual.IsPrimaryKey = false;
                colvarBsIsManual.IsForeignKey = false;
                colvarBsIsManual.IsReadOnly = false;
                
                schema.Columns.Add(colvarBsIsManual);
                
                TableSchema.TableColumn colvarIsConfirmedReadyToPay = new TableSchema.TableColumn(schema);
                colvarIsConfirmedReadyToPay.ColumnName = "is_confirmed_ready_to_pay";
                colvarIsConfirmedReadyToPay.DataType = DbType.Boolean;
                colvarIsConfirmedReadyToPay.MaxLength = 0;
                colvarIsConfirmedReadyToPay.AutoIncrement = false;
                colvarIsConfirmedReadyToPay.IsNullable = false;
                colvarIsConfirmedReadyToPay.IsPrimaryKey = false;
                colvarIsConfirmedReadyToPay.IsForeignKey = false;
                colvarIsConfirmedReadyToPay.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsConfirmedReadyToPay);
                
                TableSchema.TableColumn colvarEstAmount = new TableSchema.TableColumn(schema);
                colvarEstAmount.ColumnName = "est_amount";
                colvarEstAmount.DataType = DbType.Int32;
                colvarEstAmount.MaxLength = 0;
                colvarEstAmount.AutoIncrement = false;
                colvarEstAmount.IsNullable = true;
                colvarEstAmount.IsPrimaryKey = false;
                colvarEstAmount.IsForeignKey = false;
                colvarEstAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarEstAmount);
                
                TableSchema.TableColumn colvarDefaultPaymentTime = new TableSchema.TableColumn(schema);
                colvarDefaultPaymentTime.ColumnName = "default_payment_time";
                colvarDefaultPaymentTime.DataType = DbType.DateTime;
                colvarDefaultPaymentTime.MaxLength = 0;
                colvarDefaultPaymentTime.AutoIncrement = false;
                colvarDefaultPaymentTime.IsNullable = true;
                colvarDefaultPaymentTime.IsPrimaryKey = false;
                colvarDefaultPaymentTime.IsForeignKey = false;
                colvarDefaultPaymentTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDefaultPaymentTime);
                
                TableSchema.TableColumn colvarIsPastConfirmedReadyToPay = new TableSchema.TableColumn(schema);
                colvarIsPastConfirmedReadyToPay.ColumnName = "is_past_confirmed_ready_to_pay";
                colvarIsPastConfirmedReadyToPay.DataType = DbType.Boolean;
                colvarIsPastConfirmedReadyToPay.MaxLength = 0;
                colvarIsPastConfirmedReadyToPay.AutoIncrement = false;
                colvarIsPastConfirmedReadyToPay.IsNullable = true;
                colvarIsPastConfirmedReadyToPay.IsPrimaryKey = false;
                colvarIsPastConfirmedReadyToPay.IsForeignKey = false;
                colvarIsPastConfirmedReadyToPay.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsPastConfirmedReadyToPay);
                
                TableSchema.TableColumn colvarUnconfirmedMbsCount = new TableSchema.TableColumn(schema);
                colvarUnconfirmedMbsCount.ColumnName = "unconfirmed_mbs_count";
                colvarUnconfirmedMbsCount.DataType = DbType.Int32;
                colvarUnconfirmedMbsCount.MaxLength = 0;
                colvarUnconfirmedMbsCount.AutoIncrement = false;
                colvarUnconfirmedMbsCount.IsNullable = true;
                colvarUnconfirmedMbsCount.IsPrimaryKey = false;
                colvarUnconfirmedMbsCount.IsForeignKey = false;
                colvarUnconfirmedMbsCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarUnconfirmedMbsCount);
                
                TableSchema.TableColumn colvarPayToCompany = new TableSchema.TableColumn(schema);
                colvarPayToCompany.ColumnName = "pay_to_company";
                colvarPayToCompany.DataType = DbType.Int32;
                colvarPayToCompany.MaxLength = 0;
                colvarPayToCompany.AutoIncrement = false;
                colvarPayToCompany.IsNullable = false;
                colvarPayToCompany.IsPrimaryKey = false;
                colvarPayToCompany.IsForeignKey = false;
                colvarPayToCompany.IsReadOnly = false;
                
                schema.Columns.Add(colvarPayToCompany);
                
                TableSchema.TableColumn colvarAccountTitle = new TableSchema.TableColumn(schema);
                colvarAccountTitle.ColumnName = "account_title";
                colvarAccountTitle.DataType = DbType.String;
                colvarAccountTitle.MaxLength = 100;
                colvarAccountTitle.AutoIncrement = false;
                colvarAccountTitle.IsNullable = true;
                colvarAccountTitle.IsPrimaryKey = false;
                colvarAccountTitle.IsForeignKey = false;
                colvarAccountTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountTitle);
                
                TableSchema.TableColumn colvarCompanyId = new TableSchema.TableColumn(schema);
                colvarCompanyId.ColumnName = "company_id";
                colvarCompanyId.DataType = DbType.AnsiString;
                colvarCompanyId.MaxLength = 20;
                colvarCompanyId.AutoIncrement = false;
                colvarCompanyId.IsNullable = true;
                colvarCompanyId.IsPrimaryKey = false;
                colvarCompanyId.IsForeignKey = false;
                colvarCompanyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyId);
                
                TableSchema.TableColumn colvarBankNo = new TableSchema.TableColumn(schema);
                colvarBankNo.ColumnName = "bank_no";
                colvarBankNo.DataType = DbType.AnsiString;
                colvarBankNo.MaxLength = 20;
                colvarBankNo.AutoIncrement = false;
                colvarBankNo.IsNullable = true;
                colvarBankNo.IsPrimaryKey = false;
                colvarBankNo.IsForeignKey = false;
                colvarBankNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarBankNo);
                
                TableSchema.TableColumn colvarBranchNo = new TableSchema.TableColumn(schema);
                colvarBranchNo.ColumnName = "branch_no";
                colvarBranchNo.DataType = DbType.AnsiString;
                colvarBranchNo.MaxLength = 20;
                colvarBranchNo.AutoIncrement = false;
                colvarBranchNo.IsNullable = true;
                colvarBranchNo.IsPrimaryKey = false;
                colvarBranchNo.IsForeignKey = false;
                colvarBranchNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarBranchNo);
                
                TableSchema.TableColumn colvarAccountNo = new TableSchema.TableColumn(schema);
                colvarAccountNo.ColumnName = "account_no";
                colvarAccountNo.DataType = DbType.AnsiString;
                colvarAccountNo.MaxLength = 50;
                colvarAccountNo.AutoIncrement = false;
                colvarAccountNo.IsNullable = true;
                colvarAccountNo.IsPrimaryKey = false;
                colvarAccountNo.IsForeignKey = false;
                colvarAccountNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountNo);
                
                TableSchema.TableColumn colvarPayId = new TableSchema.TableColumn(schema);
                colvarPayId.ColumnName = "pay_id";
                colvarPayId.DataType = DbType.Int32;
                colvarPayId.MaxLength = 0;
                colvarPayId.AutoIncrement = false;
                colvarPayId.IsNullable = true;
                colvarPayId.IsPrimaryKey = false;
                colvarPayId.IsForeignKey = false;
                colvarPayId.IsReadOnly = false;
                
                schema.Columns.Add(colvarPayId);
                
                TableSchema.TableColumn colvarReceiverTitle = new TableSchema.TableColumn(schema);
                colvarReceiverTitle.ColumnName = "receiver_title";
                colvarReceiverTitle.DataType = DbType.String;
                colvarReceiverTitle.MaxLength = 50;
                colvarReceiverTitle.AutoIncrement = false;
                colvarReceiverTitle.IsNullable = true;
                colvarReceiverTitle.IsPrimaryKey = false;
                colvarReceiverTitle.IsForeignKey = false;
                colvarReceiverTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverTitle);
                
                TableSchema.TableColumn colvarReceiverCompanyId = new TableSchema.TableColumn(schema);
                colvarReceiverCompanyId.ColumnName = "receiver_company_id";
                colvarReceiverCompanyId.DataType = DbType.AnsiString;
                colvarReceiverCompanyId.MaxLength = 10;
                colvarReceiverCompanyId.AutoIncrement = false;
                colvarReceiverCompanyId.IsNullable = true;
                colvarReceiverCompanyId.IsPrimaryKey = false;
                colvarReceiverCompanyId.IsForeignKey = false;
                colvarReceiverCompanyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverCompanyId);
                
                TableSchema.TableColumn colvarReceiverBankNo = new TableSchema.TableColumn(schema);
                colvarReceiverBankNo.ColumnName = "receiver_bank_no";
                colvarReceiverBankNo.DataType = DbType.AnsiString;
                colvarReceiverBankNo.MaxLength = 3;
                colvarReceiverBankNo.AutoIncrement = false;
                colvarReceiverBankNo.IsNullable = true;
                colvarReceiverBankNo.IsPrimaryKey = false;
                colvarReceiverBankNo.IsForeignKey = false;
                colvarReceiverBankNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverBankNo);
                
                TableSchema.TableColumn colvarReceiverBranchNo = new TableSchema.TableColumn(schema);
                colvarReceiverBranchNo.ColumnName = "receiver_branch_no";
                colvarReceiverBranchNo.DataType = DbType.AnsiString;
                colvarReceiverBranchNo.MaxLength = 4;
                colvarReceiverBranchNo.AutoIncrement = false;
                colvarReceiverBranchNo.IsNullable = true;
                colvarReceiverBranchNo.IsPrimaryKey = false;
                colvarReceiverBranchNo.IsForeignKey = false;
                colvarReceiverBranchNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverBranchNo);
                
                TableSchema.TableColumn colvarReceiverAccountNo = new TableSchema.TableColumn(schema);
                colvarReceiverAccountNo.ColumnName = "receiver_account_no";
                colvarReceiverAccountNo.DataType = DbType.AnsiString;
                colvarReceiverAccountNo.MaxLength = 14;
                colvarReceiverAccountNo.AutoIncrement = false;
                colvarReceiverAccountNo.IsNullable = true;
                colvarReceiverAccountNo.IsPrimaryKey = false;
                colvarReceiverAccountNo.IsForeignKey = false;
                colvarReceiverAccountNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverAccountNo);
                
                TableSchema.TableColumn colvarTotalSum = new TableSchema.TableColumn(schema);
                colvarTotalSum.ColumnName = "total_sum";
                colvarTotalSum.DataType = DbType.Decimal;
                colvarTotalSum.MaxLength = 0;
                colvarTotalSum.AutoIncrement = false;
                colvarTotalSum.IsNullable = true;
                colvarTotalSum.IsPrimaryKey = false;
                colvarTotalSum.IsForeignKey = false;
                colvarTotalSum.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalSum);
                
                TableSchema.TableColumn colvarTransferAmount = new TableSchema.TableColumn(schema);
                colvarTransferAmount.ColumnName = "transfer_amount";
                colvarTransferAmount.DataType = DbType.Decimal;
                colvarTransferAmount.MaxLength = 0;
                colvarTransferAmount.AutoIncrement = false;
                colvarTransferAmount.IsNullable = true;
                colvarTransferAmount.IsPrimaryKey = false;
                colvarTransferAmount.IsForeignKey = false;
                colvarTransferAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarTransferAmount);
                
                TableSchema.TableColumn colvarFundTransferType = new TableSchema.TableColumn(schema);
                colvarFundTransferType.ColumnName = "fund_transfer_type";
                colvarFundTransferType.DataType = DbType.Int32;
                colvarFundTransferType.MaxLength = 0;
                colvarFundTransferType.AutoIncrement = false;
                colvarFundTransferType.IsNullable = true;
                colvarFundTransferType.IsPrimaryKey = false;
                colvarFundTransferType.IsForeignKey = false;
                colvarFundTransferType.IsReadOnly = false;
                
                schema.Columns.Add(colvarFundTransferType);
                
                TableSchema.TableColumn colvarAchResponseResult = new TableSchema.TableColumn(schema);
                colvarAchResponseResult.ColumnName = "ach_response_result";
                colvarAchResponseResult.DataType = DbType.String;
                colvarAchResponseResult.MaxLength = 10;
                colvarAchResponseResult.AutoIncrement = false;
                colvarAchResponseResult.IsNullable = true;
                colvarAchResponseResult.IsPrimaryKey = false;
                colvarAchResponseResult.IsForeignKey = false;
                colvarAchResponseResult.IsReadOnly = false;
                
                schema.Columns.Add(colvarAchResponseResult);
                
                TableSchema.TableColumn colvarResponseTime = new TableSchema.TableColumn(schema);
                colvarResponseTime.ColumnName = "response_time";
                colvarResponseTime.DataType = DbType.DateTime;
                colvarResponseTime.MaxLength = 0;
                colvarResponseTime.AutoIncrement = false;
                colvarResponseTime.IsNullable = true;
                colvarResponseTime.IsPrimaryKey = false;
                colvarResponseTime.IsForeignKey = false;
                colvarResponseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarResponseTime);
                
                TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
                colvarMemo.ColumnName = "memo";
                colvarMemo.DataType = DbType.String;
                colvarMemo.MaxLength = 200;
                colvarMemo.AutoIncrement = false;
                colvarMemo.IsNullable = true;
                colvarMemo.IsPrimaryKey = false;
                colvarMemo.IsForeignKey = false;
                colvarMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemo);
                
                TableSchema.TableColumn colvarIsTransferComplete = new TableSchema.TableColumn(schema);
                colvarIsTransferComplete.ColumnName = "is_transfer_complete";
                colvarIsTransferComplete.DataType = DbType.Boolean;
                colvarIsTransferComplete.MaxLength = 0;
                colvarIsTransferComplete.AutoIncrement = false;
                colvarIsTransferComplete.IsNullable = true;
                colvarIsTransferComplete.IsPrimaryKey = false;
                colvarIsTransferComplete.IsForeignKey = false;
                colvarIsTransferComplete.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsTransferComplete);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_balance_sheet_funds_transfer_ppon",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBalanceSheetFundsTransferPpon()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBalanceSheetFundsTransferPpon(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBalanceSheetFundsTransferPpon(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBalanceSheetFundsTransferPpon(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BsId")]
        [Bindable(true)]
        public int BsId 
	    {
		    get
		    {
			    return GetColumnValue<int>("bs_id");
		    }
            set 
		    {
			    SetColumnValue("bs_id", value);
            }
        }
	      
        [XmlAttribute("DealGuid")]
        [Bindable(true)]
        public Guid DealGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("deal_guid");
		    }
            set 
		    {
			    SetColumnValue("deal_guid", value);
            }
        }
	      
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("ProductType")]
        [Bindable(true)]
        public int ProductType 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_type");
		    }
            set 
		    {
			    SetColumnValue("product_type", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("BalanceSheetType")]
        [Bindable(true)]
        public int BalanceSheetType 
	    {
		    get
		    {
			    return GetColumnValue<int>("balance_sheet_type");
		    }
            set 
		    {
			    SetColumnValue("balance_sheet_type", value);
            }
        }
	      
        [XmlAttribute("BsYear")]
        [Bindable(true)]
        public int? BsYear 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bs_year");
		    }
            set 
		    {
			    SetColumnValue("bs_year", value);
            }
        }
	      
        [XmlAttribute("BsMonth")]
        [Bindable(true)]
        public int? BsMonth 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bs_month");
		    }
            set 
		    {
			    SetColumnValue("bs_month", value);
            }
        }
	      
        [XmlAttribute("BsIntervalStart")]
        [Bindable(true)]
        public DateTime BsIntervalStart 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("bs_interval_start");
		    }
            set 
		    {
			    SetColumnValue("bs_interval_start", value);
            }
        }
	      
        [XmlAttribute("BsIntervalEnd")]
        [Bindable(true)]
        public DateTime BsIntervalEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("bs_interval_end");
		    }
            set 
		    {
			    SetColumnValue("bs_interval_end", value);
            }
        }
	      
        [XmlAttribute("BsGenerationFrequency")]
        [Bindable(true)]
        public int BsGenerationFrequency 
	    {
		    get
		    {
			    return GetColumnValue<int>("bs_generation_frequency");
		    }
            set 
		    {
			    SetColumnValue("bs_generation_frequency", value);
            }
        }
	      
        [XmlAttribute("BsIsManual")]
        [Bindable(true)]
        public bool BsIsManual 
	    {
		    get
		    {
			    return GetColumnValue<bool>("bs_is_manual");
		    }
            set 
		    {
			    SetColumnValue("bs_is_manual", value);
            }
        }
	      
        [XmlAttribute("IsConfirmedReadyToPay")]
        [Bindable(true)]
        public bool IsConfirmedReadyToPay 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_confirmed_ready_to_pay");
		    }
            set 
		    {
			    SetColumnValue("is_confirmed_ready_to_pay", value);
            }
        }
	      
        [XmlAttribute("EstAmount")]
        [Bindable(true)]
        public int? EstAmount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("est_amount");
		    }
            set 
		    {
			    SetColumnValue("est_amount", value);
            }
        }
	      
        [XmlAttribute("DefaultPaymentTime")]
        [Bindable(true)]
        public DateTime? DefaultPaymentTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("default_payment_time");
		    }
            set 
		    {
			    SetColumnValue("default_payment_time", value);
            }
        }
	      
        [XmlAttribute("IsPastConfirmedReadyToPay")]
        [Bindable(true)]
        public bool? IsPastConfirmedReadyToPay 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_past_confirmed_ready_to_pay");
		    }
            set 
		    {
			    SetColumnValue("is_past_confirmed_ready_to_pay", value);
            }
        }
	      
        [XmlAttribute("UnconfirmedMbsCount")]
        [Bindable(true)]
        public int? UnconfirmedMbsCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("unconfirmed_mbs_count");
		    }
            set 
		    {
			    SetColumnValue("unconfirmed_mbs_count", value);
            }
        }
	      
        [XmlAttribute("PayToCompany")]
        [Bindable(true)]
        public int PayToCompany 
	    {
		    get
		    {
			    return GetColumnValue<int>("pay_to_company");
		    }
            set 
		    {
			    SetColumnValue("pay_to_company", value);
            }
        }
	      
        [XmlAttribute("AccountTitle")]
        [Bindable(true)]
        public string AccountTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_title");
		    }
            set 
		    {
			    SetColumnValue("account_title", value);
            }
        }
	      
        [XmlAttribute("CompanyId")]
        [Bindable(true)]
        public string CompanyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_id");
		    }
            set 
		    {
			    SetColumnValue("company_id", value);
            }
        }
	      
        [XmlAttribute("BankNo")]
        [Bindable(true)]
        public string BankNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("bank_no");
		    }
            set 
		    {
			    SetColumnValue("bank_no", value);
            }
        }
	      
        [XmlAttribute("BranchNo")]
        [Bindable(true)]
        public string BranchNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("branch_no");
		    }
            set 
		    {
			    SetColumnValue("branch_no", value);
            }
        }
	      
        [XmlAttribute("AccountNo")]
        [Bindable(true)]
        public string AccountNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_no");
		    }
            set 
		    {
			    SetColumnValue("account_no", value);
            }
        }
	      
        [XmlAttribute("PayId")]
        [Bindable(true)]
        public int? PayId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("pay_id");
		    }
            set 
		    {
			    SetColumnValue("pay_id", value);
            }
        }
	      
        [XmlAttribute("ReceiverTitle")]
        [Bindable(true)]
        public string ReceiverTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_title");
		    }
            set 
		    {
			    SetColumnValue("receiver_title", value);
            }
        }
	      
        [XmlAttribute("ReceiverCompanyId")]
        [Bindable(true)]
        public string ReceiverCompanyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_company_id");
		    }
            set 
		    {
			    SetColumnValue("receiver_company_id", value);
            }
        }
	      
        [XmlAttribute("ReceiverBankNo")]
        [Bindable(true)]
        public string ReceiverBankNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_bank_no");
		    }
            set 
		    {
			    SetColumnValue("receiver_bank_no", value);
            }
        }
	      
        [XmlAttribute("ReceiverBranchNo")]
        [Bindable(true)]
        public string ReceiverBranchNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_branch_no");
		    }
            set 
		    {
			    SetColumnValue("receiver_branch_no", value);
            }
        }
	      
        [XmlAttribute("ReceiverAccountNo")]
        [Bindable(true)]
        public string ReceiverAccountNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_account_no");
		    }
            set 
		    {
			    SetColumnValue("receiver_account_no", value);
            }
        }
	      
        [XmlAttribute("TotalSum")]
        [Bindable(true)]
        public decimal? TotalSum 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("total_sum");
		    }
            set 
		    {
			    SetColumnValue("total_sum", value);
            }
        }
	      
        [XmlAttribute("TransferAmount")]
        [Bindable(true)]
        public decimal? TransferAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("transfer_amount");
		    }
            set 
		    {
			    SetColumnValue("transfer_amount", value);
            }
        }
	      
        [XmlAttribute("FundTransferType")]
        [Bindable(true)]
        public int? FundTransferType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("fund_transfer_type");
		    }
            set 
		    {
			    SetColumnValue("fund_transfer_type", value);
            }
        }
	      
        [XmlAttribute("AchResponseResult")]
        [Bindable(true)]
        public string AchResponseResult 
	    {
		    get
		    {
			    return GetColumnValue<string>("ach_response_result");
		    }
            set 
		    {
			    SetColumnValue("ach_response_result", value);
            }
        }
	      
        [XmlAttribute("ResponseTime")]
        [Bindable(true)]
        public DateTime? ResponseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("response_time");
		    }
            set 
		    {
			    SetColumnValue("response_time", value);
            }
        }
	      
        [XmlAttribute("Memo")]
        [Bindable(true)]
        public string Memo 
	    {
		    get
		    {
			    return GetColumnValue<string>("memo");
		    }
            set 
		    {
			    SetColumnValue("memo", value);
            }
        }
	      
        [XmlAttribute("IsTransferComplete")]
        [Bindable(true)]
        public bool? IsTransferComplete 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_transfer_complete");
		    }
            set 
		    {
			    SetColumnValue("is_transfer_complete", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BsId = @"bs_id";
            
            public static string DealGuid = @"deal_guid";
            
            public static string DealId = @"deal_id";
            
            public static string ProductType = @"product_type";
            
            public static string StoreGuid = @"store_guid";
            
            public static string StoreName = @"store_name";
            
            public static string BalanceSheetType = @"balance_sheet_type";
            
            public static string BsYear = @"bs_year";
            
            public static string BsMonth = @"bs_month";
            
            public static string BsIntervalStart = @"bs_interval_start";
            
            public static string BsIntervalEnd = @"bs_interval_end";
            
            public static string BsGenerationFrequency = @"bs_generation_frequency";
            
            public static string BsIsManual = @"bs_is_manual";
            
            public static string IsConfirmedReadyToPay = @"is_confirmed_ready_to_pay";
            
            public static string EstAmount = @"est_amount";
            
            public static string DefaultPaymentTime = @"default_payment_time";
            
            public static string IsPastConfirmedReadyToPay = @"is_past_confirmed_ready_to_pay";
            
            public static string UnconfirmedMbsCount = @"unconfirmed_mbs_count";
            
            public static string PayToCompany = @"pay_to_company";
            
            public static string AccountTitle = @"account_title";
            
            public static string CompanyId = @"company_id";
            
            public static string BankNo = @"bank_no";
            
            public static string BranchNo = @"branch_no";
            
            public static string AccountNo = @"account_no";
            
            public static string PayId = @"pay_id";
            
            public static string ReceiverTitle = @"receiver_title";
            
            public static string ReceiverCompanyId = @"receiver_company_id";
            
            public static string ReceiverBankNo = @"receiver_bank_no";
            
            public static string ReceiverBranchNo = @"receiver_branch_no";
            
            public static string ReceiverAccountNo = @"receiver_account_no";
            
            public static string TotalSum = @"total_sum";
            
            public static string TransferAmount = @"transfer_amount";
            
            public static string FundTransferType = @"fund_transfer_type";
            
            public static string AchResponseResult = @"ach_response_result";
            
            public static string ResponseTime = @"response_time";
            
            public static string Memo = @"memo";
            
            public static string IsTransferComplete = @"is_transfer_complete";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

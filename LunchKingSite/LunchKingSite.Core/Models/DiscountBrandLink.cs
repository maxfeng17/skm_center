using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DiscountBrandLink class.
	/// </summary>
    [Serializable]
	public partial class DiscountBrandLinkCollection : RepositoryList<DiscountBrandLink, DiscountBrandLinkCollection>
	{	   
		public DiscountBrandLinkCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DiscountBrandLinkCollection</returns>
		public DiscountBrandLinkCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DiscountBrandLink o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the discount_brand_link table.
	/// </summary>
	[Serializable]
	public partial class DiscountBrandLink : RepositoryRecord<DiscountBrandLink>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DiscountBrandLink()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DiscountBrandLink(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("discount_brand_link", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCampaignId = new TableSchema.TableColumn(schema);
				colvarCampaignId.ColumnName = "campaign_id";
				colvarCampaignId.DataType = DbType.Int32;
				colvarCampaignId.MaxLength = 0;
				colvarCampaignId.AutoIncrement = false;
				colvarCampaignId.IsNullable = false;
				colvarCampaignId.IsPrimaryKey = false;
				colvarCampaignId.IsForeignKey = false;
				colvarCampaignId.IsReadOnly = false;
				colvarCampaignId.DefaultSetting = @"";
				colvarCampaignId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCampaignId);
				
				TableSchema.TableColumn colvarBrandId = new TableSchema.TableColumn(schema);
				colvarBrandId.ColumnName = "brand_id";
				colvarBrandId.DataType = DbType.Int32;
				colvarBrandId.MaxLength = 0;
				colvarBrandId.AutoIncrement = false;
				colvarBrandId.IsNullable = false;
				colvarBrandId.IsPrimaryKey = false;
				colvarBrandId.IsForeignKey = false;
				colvarBrandId.IsReadOnly = false;
				colvarBrandId.DefaultSetting = @"";
				colvarBrandId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("discount_brand_link",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CampaignId")]
		[Bindable(true)]
		public int CampaignId 
		{
			get { return GetColumnValue<int>(Columns.CampaignId); }
			set { SetColumnValue(Columns.CampaignId, value); }
		}
		  
		[XmlAttribute("BrandId")]
		[Bindable(true)]
		public int BrandId 
		{
			get { return GetColumnValue<int>(Columns.BrandId); }
			set { SetColumnValue(Columns.BrandId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CampaignIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BrandIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CampaignId = @"campaign_id";
			 public static string BrandId = @"brand_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealOrder class.
	/// </summary>
    [Serializable]
	public partial class HiDealOrderCollection : RepositoryList<HiDealOrder, HiDealOrderCollection>
	{	   
		public HiDealOrderCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealOrderCollection</returns>
		public HiDealOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_order table.
	/// </summary>
	[Serializable]
	public partial class HiDealOrder : RepositoryRecord<HiDealOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealOrder()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarPk = new TableSchema.TableColumn(schema);
				colvarPk.ColumnName = "pk";
				colvarPk.DataType = DbType.Int32;
				colvarPk.MaxLength = 0;
				colvarPk.AutoIncrement = true;
				colvarPk.IsNullable = false;
				colvarPk.IsPrimaryKey = true;
				colvarPk.IsForeignKey = false;
				colvarPk.IsReadOnly = false;
				colvarPk.DefaultSetting = @"";
				colvarPk.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPk);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = false;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);
				
				TableSchema.TableColumn colvarHiDealId = new TableSchema.TableColumn(schema);
				colvarHiDealId.ColumnName = "hi_deal_id";
				colvarHiDealId.DataType = DbType.Int32;
				colvarHiDealId.MaxLength = 0;
				colvarHiDealId.AutoIncrement = false;
				colvarHiDealId.IsNullable = false;
				colvarHiDealId.IsPrimaryKey = false;
				colvarHiDealId.IsForeignKey = false;
				colvarHiDealId.IsReadOnly = false;
				colvarHiDealId.DefaultSetting = @"";
				colvarHiDealId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHiDealId);
				
				TableSchema.TableColumn colvarTotalAmount = new TableSchema.TableColumn(schema);
				colvarTotalAmount.ColumnName = "total_amount";
				colvarTotalAmount.DataType = DbType.Currency;
				colvarTotalAmount.MaxLength = 0;
				colvarTotalAmount.AutoIncrement = false;
				colvarTotalAmount.IsNullable = false;
				colvarTotalAmount.IsPrimaryKey = false;
				colvarTotalAmount.IsForeignKey = false;
				colvarTotalAmount.IsReadOnly = false;
				
						colvarTotalAmount.DefaultSetting = @"((0))";
				colvarTotalAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalAmount);
				
				TableSchema.TableColumn colvarPcash = new TableSchema.TableColumn(schema);
				colvarPcash.ColumnName = "pcash";
				colvarPcash.DataType = DbType.Currency;
				colvarPcash.MaxLength = 0;
				colvarPcash.AutoIncrement = false;
				colvarPcash.IsNullable = false;
				colvarPcash.IsPrimaryKey = false;
				colvarPcash.IsForeignKey = false;
				colvarPcash.IsReadOnly = false;
				
						colvarPcash.DefaultSetting = @"((0))";
				colvarPcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPcash);
				
				TableSchema.TableColumn colvarScash = new TableSchema.TableColumn(schema);
				colvarScash.ColumnName = "scash";
				colvarScash.DataType = DbType.Currency;
				colvarScash.MaxLength = 0;
				colvarScash.AutoIncrement = false;
				colvarScash.IsNullable = false;
				colvarScash.IsPrimaryKey = false;
				colvarScash.IsForeignKey = false;
				colvarScash.IsReadOnly = false;
				
						colvarScash.DefaultSetting = @"((0))";
				colvarScash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScash);
				
				TableSchema.TableColumn colvarBcash = new TableSchema.TableColumn(schema);
				colvarBcash.ColumnName = "bcash";
				colvarBcash.DataType = DbType.Currency;
				colvarBcash.MaxLength = 0;
				colvarBcash.AutoIncrement = false;
				colvarBcash.IsNullable = false;
				colvarBcash.IsPrimaryKey = false;
				colvarBcash.IsForeignKey = false;
				colvarBcash.IsReadOnly = false;
				
						colvarBcash.DefaultSetting = @"((0))";
				colvarBcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBcash);
				
				TableSchema.TableColumn colvarCreditCardAmt = new TableSchema.TableColumn(schema);
				colvarCreditCardAmt.ColumnName = "credit_card_amt";
				colvarCreditCardAmt.DataType = DbType.Currency;
				colvarCreditCardAmt.MaxLength = 0;
				colvarCreditCardAmt.AutoIncrement = false;
				colvarCreditCardAmt.IsNullable = false;
				colvarCreditCardAmt.IsPrimaryKey = false;
				colvarCreditCardAmt.IsForeignKey = false;
				colvarCreditCardAmt.IsReadOnly = false;
				
						colvarCreditCardAmt.DefaultSetting = @"((0))";
				colvarCreditCardAmt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditCardAmt);
				
				TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
				colvarDiscountAmount.ColumnName = "discount_amount";
				colvarDiscountAmount.DataType = DbType.Currency;
				colvarDiscountAmount.MaxLength = 0;
				colvarDiscountAmount.AutoIncrement = false;
				colvarDiscountAmount.IsNullable = false;
				colvarDiscountAmount.IsPrimaryKey = false;
				colvarDiscountAmount.IsForeignKey = false;
				colvarDiscountAmount.IsReadOnly = false;
				
						colvarDiscountAmount.DefaultSetting = @"((0))";
				colvarDiscountAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountAmount);
				
				TableSchema.TableColumn colvarAtmAmount = new TableSchema.TableColumn(schema);
				colvarAtmAmount.ColumnName = "atm_amount";
				colvarAtmAmount.DataType = DbType.Currency;
				colvarAtmAmount.MaxLength = 0;
				colvarAtmAmount.AutoIncrement = false;
				colvarAtmAmount.IsNullable = false;
				colvarAtmAmount.IsPrimaryKey = false;
				colvarAtmAmount.IsForeignKey = false;
				colvarAtmAmount.IsReadOnly = false;
				
						colvarAtmAmount.DefaultSetting = @"((0))";
				colvarAtmAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAtmAmount);
				
				TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
				colvarOrderStatus.ColumnName = "order_status";
				colvarOrderStatus.DataType = DbType.Int32;
				colvarOrderStatus.MaxLength = 0;
				colvarOrderStatus.AutoIncrement = false;
				colvarOrderStatus.IsNullable = false;
				colvarOrderStatus.IsPrimaryKey = false;
				colvarOrderStatus.IsForeignKey = false;
				colvarOrderStatus.IsReadOnly = false;
				colvarOrderStatus.DefaultSetting = @"";
				colvarOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStatus);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarCompletedTime = new TableSchema.TableColumn(schema);
				colvarCompletedTime.ColumnName = "completed_time";
				colvarCompletedTime.DataType = DbType.DateTime;
				colvarCompletedTime.MaxLength = 0;
				colvarCompletedTime.AutoIncrement = false;
				colvarCompletedTime.IsNullable = true;
				colvarCompletedTime.IsPrimaryKey = false;
				colvarCompletedTime.IsForeignKey = false;
				colvarCompletedTime.IsReadOnly = false;
				colvarCompletedTime.DefaultSetting = @"";
				colvarCompletedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompletedTime);
				
				TableSchema.TableColumn colvarCancelTime = new TableSchema.TableColumn(schema);
				colvarCancelTime.ColumnName = "cancel_time";
				colvarCancelTime.DataType = DbType.DateTime;
				colvarCancelTime.MaxLength = 0;
				colvarCancelTime.AutoIncrement = false;
				colvarCancelTime.IsNullable = true;
				colvarCancelTime.IsPrimaryKey = false;
				colvarCancelTime.IsForeignKey = false;
				colvarCancelTime.IsReadOnly = false;
				colvarCancelTime.DefaultSetting = @"";
				colvarCancelTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelTime);
				
				TableSchema.TableColumn colvarCancelReason = new TableSchema.TableColumn(schema);
				colvarCancelReason.ColumnName = "cancel_reason";
				colvarCancelReason.DataType = DbType.String;
				colvarCancelReason.MaxLength = -1;
				colvarCancelReason.AutoIncrement = false;
				colvarCancelReason.IsNullable = true;
				colvarCancelReason.IsPrimaryKey = false;
				colvarCancelReason.IsForeignKey = false;
				colvarCancelReason.IsReadOnly = false;
				colvarCancelReason.DefaultSetting = @"";
				colvarCancelReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelReason);
				
				TableSchema.TableColumn colvarCancelUserName = new TableSchema.TableColumn(schema);
				colvarCancelUserName.ColumnName = "cancel_user_name";
				colvarCancelUserName.DataType = DbType.String;
				colvarCancelUserName.MaxLength = 256;
				colvarCancelUserName.AutoIncrement = false;
				colvarCancelUserName.IsNullable = true;
				colvarCancelUserName.IsPrimaryKey = false;
				colvarCancelUserName.IsForeignKey = false;
				colvarCancelUserName.IsReadOnly = false;
				colvarCancelUserName.DefaultSetting = @"";
				colvarCancelUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelUserName);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				
						colvarUserId.DefaultSetting = @"((0))";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_order",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Pk")]
		[Bindable(true)]
		public int Pk 
		{
			get { return GetColumnValue<int>(Columns.Pk); }
			set { SetColumnValue(Columns.Pk, value); }
		}
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId 
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}
		  
		[XmlAttribute("HiDealId")]
		[Bindable(true)]
		public int HiDealId 
		{
			get { return GetColumnValue<int>(Columns.HiDealId); }
			set { SetColumnValue(Columns.HiDealId, value); }
		}
		  
		[XmlAttribute("TotalAmount")]
		[Bindable(true)]
		public decimal TotalAmount 
		{
			get { return GetColumnValue<decimal>(Columns.TotalAmount); }
			set { SetColumnValue(Columns.TotalAmount, value); }
		}
		  
		[XmlAttribute("Pcash")]
		[Bindable(true)]
		public decimal Pcash 
		{
			get { return GetColumnValue<decimal>(Columns.Pcash); }
			set { SetColumnValue(Columns.Pcash, value); }
		}
		  
		[XmlAttribute("Scash")]
		[Bindable(true)]
		public decimal Scash 
		{
			get { return GetColumnValue<decimal>(Columns.Scash); }
			set { SetColumnValue(Columns.Scash, value); }
		}
		  
		[XmlAttribute("Bcash")]
		[Bindable(true)]
		public decimal Bcash 
		{
			get { return GetColumnValue<decimal>(Columns.Bcash); }
			set { SetColumnValue(Columns.Bcash, value); }
		}
		  
		[XmlAttribute("CreditCardAmt")]
		[Bindable(true)]
		public decimal CreditCardAmt 
		{
			get { return GetColumnValue<decimal>(Columns.CreditCardAmt); }
			set { SetColumnValue(Columns.CreditCardAmt, value); }
		}
		  
		[XmlAttribute("DiscountAmount")]
		[Bindable(true)]
		public decimal DiscountAmount 
		{
			get { return GetColumnValue<decimal>(Columns.DiscountAmount); }
			set { SetColumnValue(Columns.DiscountAmount, value); }
		}
		  
		[XmlAttribute("AtmAmount")]
		[Bindable(true)]
		public decimal AtmAmount 
		{
			get { return GetColumnValue<decimal>(Columns.AtmAmount); }
			set { SetColumnValue(Columns.AtmAmount, value); }
		}
		  
		[XmlAttribute("OrderStatus")]
		[Bindable(true)]
		public int OrderStatus 
		{
			get { return GetColumnValue<int>(Columns.OrderStatus); }
			set { SetColumnValue(Columns.OrderStatus, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("CompletedTime")]
		[Bindable(true)]
		public DateTime? CompletedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CompletedTime); }
			set { SetColumnValue(Columns.CompletedTime, value); }
		}
		  
		[XmlAttribute("CancelTime")]
		[Bindable(true)]
		public DateTime? CancelTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CancelTime); }
			set { SetColumnValue(Columns.CancelTime, value); }
		}
		  
		[XmlAttribute("CancelReason")]
		[Bindable(true)]
		public string CancelReason 
		{
			get { return GetColumnValue<string>(Columns.CancelReason); }
			set { SetColumnValue(Columns.CancelReason, value); }
		}
		  
		[XmlAttribute("CancelUserName")]
		[Bindable(true)]
		public string CancelUserName 
		{
			get { return GetColumnValue<string>(Columns.CancelUserName); }
			set { SetColumnValue(Columns.CancelUserName, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn PkColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn HiDealIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalAmountColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PcashColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ScashColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn BcashColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreditCardAmtColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountAmountColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn AtmAmountColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderStatusColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CompletedTimeColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelTimeColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelReasonColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelUserNameColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Pk = @"pk";
			 public static string Guid = @"guid";
			 public static string OrderId = @"order_id";
			 public static string HiDealId = @"hi_deal_id";
			 public static string TotalAmount = @"total_amount";
			 public static string Pcash = @"pcash";
			 public static string Scash = @"scash";
			 public static string Bcash = @"bcash";
			 public static string CreditCardAmt = @"credit_card_amt";
			 public static string DiscountAmount = @"discount_amount";
			 public static string AtmAmount = @"atm_amount";
			 public static string OrderStatus = @"order_status";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string CompletedTime = @"completed_time";
			 public static string CancelTime = @"cancel_time";
			 public static string CancelReason = @"cancel_reason";
			 public static string CancelUserName = @"cancel_user_name";
			 public static string UserId = @"user_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

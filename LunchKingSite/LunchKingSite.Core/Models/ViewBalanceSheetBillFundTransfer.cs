using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBalanceSheetBillFundTransfer class.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetBillFundTransferCollection : ReadOnlyList<ViewBalanceSheetBillFundTransfer, ViewBalanceSheetBillFundTransferCollection>
    {        
        public ViewBalanceSheetBillFundTransferCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_balance_sheet_bill_fund_transfer view.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetBillFundTransfer : ReadOnlyRecord<ViewBalanceSheetBillFundTransfer>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_balance_sheet_bill_fund_transfer", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBalanceSheetId = new TableSchema.TableColumn(schema);
                colvarBalanceSheetId.ColumnName = "balance_sheet_id";
                colvarBalanceSheetId.DataType = DbType.Int32;
                colvarBalanceSheetId.MaxLength = 0;
                colvarBalanceSheetId.AutoIncrement = false;
                colvarBalanceSheetId.IsNullable = false;
                colvarBalanceSheetId.IsPrimaryKey = false;
                colvarBalanceSheetId.IsForeignKey = false;
                colvarBalanceSheetId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBalanceSheetId);
                
                TableSchema.TableColumn colvarBalanceSheetType = new TableSchema.TableColumn(schema);
                colvarBalanceSheetType.ColumnName = "balance_sheet_type";
                colvarBalanceSheetType.DataType = DbType.Int32;
                colvarBalanceSheetType.MaxLength = 0;
                colvarBalanceSheetType.AutoIncrement = false;
                colvarBalanceSheetType.IsNullable = false;
                colvarBalanceSheetType.IsPrimaryKey = false;
                colvarBalanceSheetType.IsForeignKey = false;
                colvarBalanceSheetType.IsReadOnly = false;
                
                schema.Columns.Add(colvarBalanceSheetType);
                
                TableSchema.TableColumn colvarGenerationFrequency = new TableSchema.TableColumn(schema);
                colvarGenerationFrequency.ColumnName = "generation_frequency";
                colvarGenerationFrequency.DataType = DbType.Int32;
                colvarGenerationFrequency.MaxLength = 0;
                colvarGenerationFrequency.AutoIncrement = false;
                colvarGenerationFrequency.IsNullable = false;
                colvarGenerationFrequency.IsPrimaryKey = false;
                colvarGenerationFrequency.IsForeignKey = false;
                colvarGenerationFrequency.IsReadOnly = false;
                
                schema.Columns.Add(colvarGenerationFrequency);
                
                TableSchema.TableColumn colvarProductType = new TableSchema.TableColumn(schema);
                colvarProductType.ColumnName = "product_type";
                colvarProductType.DataType = DbType.Int32;
                colvarProductType.MaxLength = 0;
                colvarProductType.AutoIncrement = false;
                colvarProductType.IsNullable = false;
                colvarProductType.IsPrimaryKey = false;
                colvarProductType.IsForeignKey = false;
                colvarProductType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductType);
                
                TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
                colvarProductGuid.ColumnName = "product_guid";
                colvarProductGuid.DataType = DbType.Guid;
                colvarProductGuid.MaxLength = 0;
                colvarProductGuid.AutoIncrement = false;
                colvarProductGuid.IsNullable = false;
                colvarProductGuid.IsPrimaryKey = false;
                colvarProductGuid.IsForeignKey = false;
                colvarProductGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductGuid);
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 301;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
                colvarRemittanceType.ColumnName = "remittance_type";
                colvarRemittanceType.DataType = DbType.Int32;
                colvarRemittanceType.MaxLength = 0;
                colvarRemittanceType.AutoIncrement = false;
                colvarRemittanceType.IsNullable = false;
                colvarRemittanceType.IsPrimaryKey = false;
                colvarRemittanceType.IsForeignKey = false;
                colvarRemittanceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemittanceType);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarIntervalStart = new TableSchema.TableColumn(schema);
                colvarIntervalStart.ColumnName = "interval_start";
                colvarIntervalStart.DataType = DbType.DateTime;
                colvarIntervalStart.MaxLength = 0;
                colvarIntervalStart.AutoIncrement = false;
                colvarIntervalStart.IsNullable = true;
                colvarIntervalStart.IsPrimaryKey = false;
                colvarIntervalStart.IsForeignKey = false;
                colvarIntervalStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarIntervalStart);
                
                TableSchema.TableColumn colvarIntervalEnd = new TableSchema.TableColumn(schema);
                colvarIntervalEnd.ColumnName = "interval_end";
                colvarIntervalEnd.DataType = DbType.DateTime;
                colvarIntervalEnd.MaxLength = 0;
                colvarIntervalEnd.AutoIncrement = false;
                colvarIntervalEnd.IsNullable = true;
                colvarIntervalEnd.IsPrimaryKey = false;
                colvarIntervalEnd.IsForeignKey = false;
                colvarIntervalEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarIntervalEnd);
                
                TableSchema.TableColumn colvarEstAmount = new TableSchema.TableColumn(schema);
                colvarEstAmount.ColumnName = "est_amount";
                colvarEstAmount.DataType = DbType.Int32;
                colvarEstAmount.MaxLength = 0;
                colvarEstAmount.AutoIncrement = false;
                colvarEstAmount.IsNullable = true;
                colvarEstAmount.IsPrimaryKey = false;
                colvarEstAmount.IsForeignKey = false;
                colvarEstAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarEstAmount);
                
                TableSchema.TableColumn colvarPayReportId = new TableSchema.TableColumn(schema);
                colvarPayReportId.ColumnName = "pay_report_id";
                colvarPayReportId.DataType = DbType.Int32;
                colvarPayReportId.MaxLength = 0;
                colvarPayReportId.AutoIncrement = false;
                colvarPayReportId.IsNullable = true;
                colvarPayReportId.IsPrimaryKey = false;
                colvarPayReportId.IsForeignKey = false;
                colvarPayReportId.IsReadOnly = false;
                
                schema.Columns.Add(colvarPayReportId);
                
                TableSchema.TableColumn colvarIsConfirmedReadyToPay = new TableSchema.TableColumn(schema);
                colvarIsConfirmedReadyToPay.ColumnName = "is_confirmed_ready_to_pay";
                colvarIsConfirmedReadyToPay.DataType = DbType.Boolean;
                colvarIsConfirmedReadyToPay.MaxLength = 0;
                colvarIsConfirmedReadyToPay.AutoIncrement = false;
                colvarIsConfirmedReadyToPay.IsNullable = false;
                colvarIsConfirmedReadyToPay.IsPrimaryKey = false;
                colvarIsConfirmedReadyToPay.IsForeignKey = false;
                colvarIsConfirmedReadyToPay.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsConfirmedReadyToPay);
                
                TableSchema.TableColumn colvarTransferAmount = new TableSchema.TableColumn(schema);
                colvarTransferAmount.ColumnName = "transfer_amount";
                colvarTransferAmount.DataType = DbType.Int32;
                colvarTransferAmount.MaxLength = 0;
                colvarTransferAmount.AutoIncrement = false;
                colvarTransferAmount.IsNullable = true;
                colvarTransferAmount.IsPrimaryKey = false;
                colvarTransferAmount.IsForeignKey = false;
                colvarTransferAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarTransferAmount);
                
                TableSchema.TableColumn colvarIsTransferComplete = new TableSchema.TableColumn(schema);
                colvarIsTransferComplete.ColumnName = "is_transfer_complete";
                colvarIsTransferComplete.DataType = DbType.Boolean;
                colvarIsTransferComplete.MaxLength = 0;
                colvarIsTransferComplete.AutoIncrement = false;
                colvarIsTransferComplete.IsNullable = true;
                colvarIsTransferComplete.IsPrimaryKey = false;
                colvarIsTransferComplete.IsForeignKey = false;
                colvarIsTransferComplete.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsTransferComplete);
                
                TableSchema.TableColumn colvarTransferTime = new TableSchema.TableColumn(schema);
                colvarTransferTime.ColumnName = "transfer_time";
                colvarTransferTime.DataType = DbType.DateTime;
                colvarTransferTime.MaxLength = 0;
                colvarTransferTime.AutoIncrement = false;
                colvarTransferTime.IsNullable = true;
                colvarTransferTime.IsPrimaryKey = false;
                colvarTransferTime.IsForeignKey = false;
                colvarTransferTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarTransferTime);
                
                TableSchema.TableColumn colvarFundMemo = new TableSchema.TableColumn(schema);
                colvarFundMemo.ColumnName = "fund_memo";
                colvarFundMemo.DataType = DbType.String;
                colvarFundMemo.MaxLength = 200;
                colvarFundMemo.AutoIncrement = false;
                colvarFundMemo.IsNullable = true;
                colvarFundMemo.IsPrimaryKey = false;
                colvarFundMemo.IsForeignKey = false;
                colvarFundMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarFundMemo);
                
                TableSchema.TableColumn colvarPayToCompany = new TableSchema.TableColumn(schema);
                colvarPayToCompany.ColumnName = "pay_to_company";
                colvarPayToCompany.DataType = DbType.Int32;
                colvarPayToCompany.MaxLength = 0;
                colvarPayToCompany.AutoIncrement = false;
                colvarPayToCompany.IsNullable = false;
                colvarPayToCompany.IsPrimaryKey = false;
                colvarPayToCompany.IsForeignKey = false;
                colvarPayToCompany.IsReadOnly = false;
                
                schema.Columns.Add(colvarPayToCompany);
                
                TableSchema.TableColumn colvarDealAccountTitle = new TableSchema.TableColumn(schema);
                colvarDealAccountTitle.ColumnName = "deal_account_title";
                colvarDealAccountTitle.DataType = DbType.String;
                colvarDealAccountTitle.MaxLength = 50;
                colvarDealAccountTitle.AutoIncrement = false;
                colvarDealAccountTitle.IsNullable = true;
                colvarDealAccountTitle.IsPrimaryKey = false;
                colvarDealAccountTitle.IsForeignKey = false;
                colvarDealAccountTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealAccountTitle);
                
                TableSchema.TableColumn colvarDealCompanyId = new TableSchema.TableColumn(schema);
                colvarDealCompanyId.ColumnName = "deal_company_id";
                colvarDealCompanyId.DataType = DbType.AnsiString;
                colvarDealCompanyId.MaxLength = 20;
                colvarDealCompanyId.AutoIncrement = false;
                colvarDealCompanyId.IsNullable = true;
                colvarDealCompanyId.IsPrimaryKey = false;
                colvarDealCompanyId.IsForeignKey = false;
                colvarDealCompanyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealCompanyId);
                
                TableSchema.TableColumn colvarDealBankNo = new TableSchema.TableColumn(schema);
                colvarDealBankNo.ColumnName = "deal_bank_no";
                colvarDealBankNo.DataType = DbType.AnsiString;
                colvarDealBankNo.MaxLength = 10;
                colvarDealBankNo.AutoIncrement = false;
                colvarDealBankNo.IsNullable = true;
                colvarDealBankNo.IsPrimaryKey = false;
                colvarDealBankNo.IsForeignKey = false;
                colvarDealBankNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealBankNo);
                
                TableSchema.TableColumn colvarDealBranchNo = new TableSchema.TableColumn(schema);
                colvarDealBranchNo.ColumnName = "deal_branch_no";
                colvarDealBranchNo.DataType = DbType.AnsiString;
                colvarDealBranchNo.MaxLength = 10;
                colvarDealBranchNo.AutoIncrement = false;
                colvarDealBranchNo.IsNullable = true;
                colvarDealBranchNo.IsPrimaryKey = false;
                colvarDealBranchNo.IsForeignKey = false;
                colvarDealBranchNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealBranchNo);
                
                TableSchema.TableColumn colvarDealAccountNo = new TableSchema.TableColumn(schema);
                colvarDealAccountNo.ColumnName = "deal_account_no";
                colvarDealAccountNo.DataType = DbType.AnsiString;
                colvarDealAccountNo.MaxLength = 20;
                colvarDealAccountNo.AutoIncrement = false;
                colvarDealAccountNo.IsNullable = true;
                colvarDealAccountNo.IsPrimaryKey = false;
                colvarDealAccountNo.IsForeignKey = false;
                colvarDealAccountNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealAccountNo);
                
                TableSchema.TableColumn colvarSignCompanyId = new TableSchema.TableColumn(schema);
                colvarSignCompanyId.ColumnName = "sign_company_id";
                colvarSignCompanyId.DataType = DbType.AnsiString;
                colvarSignCompanyId.MaxLength = 20;
                colvarSignCompanyId.AutoIncrement = false;
                colvarSignCompanyId.IsNullable = true;
                colvarSignCompanyId.IsPrimaryKey = false;
                colvarSignCompanyId.IsForeignKey = false;
                colvarSignCompanyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSignCompanyId);
                
                TableSchema.TableColumn colvarSignCompanyName = new TableSchema.TableColumn(schema);
                colvarSignCompanyName.ColumnName = "sign_company_name";
                colvarSignCompanyName.DataType = DbType.String;
                colvarSignCompanyName.MaxLength = 50;
                colvarSignCompanyName.AutoIncrement = false;
                colvarSignCompanyName.IsNullable = true;
                colvarSignCompanyName.IsPrimaryKey = false;
                colvarSignCompanyName.IsForeignKey = false;
                colvarSignCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSignCompanyName);
                
                TableSchema.TableColumn colvarReceiverAccountTitle = new TableSchema.TableColumn(schema);
                colvarReceiverAccountTitle.ColumnName = "receiver_account_title";
                colvarReceiverAccountTitle.DataType = DbType.String;
                colvarReceiverAccountTitle.MaxLength = 50;
                colvarReceiverAccountTitle.AutoIncrement = false;
                colvarReceiverAccountTitle.IsNullable = true;
                colvarReceiverAccountTitle.IsPrimaryKey = false;
                colvarReceiverAccountTitle.IsForeignKey = false;
                colvarReceiverAccountTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverAccountTitle);
                
                TableSchema.TableColumn colvarReceiverCompanyId = new TableSchema.TableColumn(schema);
                colvarReceiverCompanyId.ColumnName = "receiver_company_id";
                colvarReceiverCompanyId.DataType = DbType.AnsiString;
                colvarReceiverCompanyId.MaxLength = 10;
                colvarReceiverCompanyId.AutoIncrement = false;
                colvarReceiverCompanyId.IsNullable = true;
                colvarReceiverCompanyId.IsPrimaryKey = false;
                colvarReceiverCompanyId.IsForeignKey = false;
                colvarReceiverCompanyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverCompanyId);
                
                TableSchema.TableColumn colvarReceiverBankNo = new TableSchema.TableColumn(schema);
                colvarReceiverBankNo.ColumnName = "receiver_bank_no";
                colvarReceiverBankNo.DataType = DbType.AnsiString;
                colvarReceiverBankNo.MaxLength = 3;
                colvarReceiverBankNo.AutoIncrement = false;
                colvarReceiverBankNo.IsNullable = true;
                colvarReceiverBankNo.IsPrimaryKey = false;
                colvarReceiverBankNo.IsForeignKey = false;
                colvarReceiverBankNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverBankNo);
                
                TableSchema.TableColumn colvarReceiverBranchNo = new TableSchema.TableColumn(schema);
                colvarReceiverBranchNo.ColumnName = "receiver_branch_no";
                colvarReceiverBranchNo.DataType = DbType.AnsiString;
                colvarReceiverBranchNo.MaxLength = 4;
                colvarReceiverBranchNo.AutoIncrement = false;
                colvarReceiverBranchNo.IsNullable = true;
                colvarReceiverBranchNo.IsPrimaryKey = false;
                colvarReceiverBranchNo.IsForeignKey = false;
                colvarReceiverBranchNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverBranchNo);
                
                TableSchema.TableColumn colvarReceiverAccountNo = new TableSchema.TableColumn(schema);
                colvarReceiverAccountNo.ColumnName = "receiver_account_no";
                colvarReceiverAccountNo.DataType = DbType.AnsiString;
                colvarReceiverAccountNo.MaxLength = 14;
                colvarReceiverAccountNo.AutoIncrement = false;
                colvarReceiverAccountNo.IsNullable = true;
                colvarReceiverAccountNo.IsPrimaryKey = false;
                colvarReceiverAccountNo.IsForeignKey = false;
                colvarReceiverAccountNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverAccountNo);
                
                TableSchema.TableColumn colvarBillCreator = new TableSchema.TableColumn(schema);
                colvarBillCreator.ColumnName = "bill_creator";
                colvarBillCreator.DataType = DbType.String;
                colvarBillCreator.MaxLength = 256;
                colvarBillCreator.AutoIncrement = false;
                colvarBillCreator.IsNullable = true;
                colvarBillCreator.IsPrimaryKey = false;
                colvarBillCreator.IsForeignKey = false;
                colvarBillCreator.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillCreator);
                
                TableSchema.TableColumn colvarBillCreateTime = new TableSchema.TableColumn(schema);
                colvarBillCreateTime.ColumnName = "bill_create_time";
                colvarBillCreateTime.DataType = DbType.DateTime;
                colvarBillCreateTime.MaxLength = 0;
                colvarBillCreateTime.AutoIncrement = false;
                colvarBillCreateTime.IsNullable = true;
                colvarBillCreateTime.IsPrimaryKey = false;
                colvarBillCreateTime.IsForeignKey = false;
                colvarBillCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillCreateTime);
                
                TableSchema.TableColumn colvarSalesName = new TableSchema.TableColumn(schema);
                colvarSalesName.ColumnName = "sales_name";
                colvarSalesName.DataType = DbType.String;
                colvarSalesName.MaxLength = 100;
                colvarSalesName.AutoIncrement = false;
                colvarSalesName.IsNullable = true;
                colvarSalesName.IsPrimaryKey = false;
                colvarSalesName.IsForeignKey = false;
                colvarSalesName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSalesName);
                
                TableSchema.TableColumn colvarBillTotal = new TableSchema.TableColumn(schema);
                colvarBillTotal.ColumnName = "bill_total";
                colvarBillTotal.DataType = DbType.Int32;
                colvarBillTotal.MaxLength = 0;
                colvarBillTotal.AutoIncrement = false;
                colvarBillTotal.IsNullable = true;
                colvarBillTotal.IsPrimaryKey = false;
                colvarBillTotal.IsForeignKey = false;
                colvarBillTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillTotal);
                
                TableSchema.TableColumn colvarBillSubtotal = new TableSchema.TableColumn(schema);
                colvarBillSubtotal.ColumnName = "bill_subtotal";
                colvarBillSubtotal.DataType = DbType.Int32;
                colvarBillSubtotal.MaxLength = 0;
                colvarBillSubtotal.AutoIncrement = false;
                colvarBillSubtotal.IsNullable = true;
                colvarBillSubtotal.IsPrimaryKey = false;
                colvarBillSubtotal.IsForeignKey = false;
                colvarBillSubtotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillSubtotal);
                
                TableSchema.TableColumn colvarBillTax = new TableSchema.TableColumn(schema);
                colvarBillTax.ColumnName = "bill_tax";
                colvarBillTax.DataType = DbType.Int32;
                colvarBillTax.MaxLength = 0;
                colvarBillTax.AutoIncrement = false;
                colvarBillTax.IsNullable = true;
                colvarBillTax.IsPrimaryKey = false;
                colvarBillTax.IsForeignKey = false;
                colvarBillTax.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillTax);
                
                TableSchema.TableColumn colvarBillNumber = new TableSchema.TableColumn(schema);
                colvarBillNumber.ColumnName = "bill_number";
                colvarBillNumber.DataType = DbType.String;
                colvarBillNumber.MaxLength = 10;
                colvarBillNumber.AutoIncrement = false;
                colvarBillNumber.IsNullable = true;
                colvarBillNumber.IsPrimaryKey = false;
                colvarBillNumber.IsForeignKey = false;
                colvarBillNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillNumber);
                
                TableSchema.TableColumn colvarBillDate = new TableSchema.TableColumn(schema);
                colvarBillDate.ColumnName = "bill_date";
                colvarBillDate.DataType = DbType.DateTime;
                colvarBillDate.MaxLength = 0;
                colvarBillDate.AutoIncrement = false;
                colvarBillDate.IsNullable = true;
                colvarBillDate.IsPrimaryKey = false;
                colvarBillDate.IsForeignKey = false;
                colvarBillDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillDate);
                
                TableSchema.TableColumn colvarBillRemark = new TableSchema.TableColumn(schema);
                colvarBillRemark.ColumnName = "bill_remark";
                colvarBillRemark.DataType = DbType.String;
                colvarBillRemark.MaxLength = 250;
                colvarBillRemark.AutoIncrement = false;
                colvarBillRemark.IsNullable = true;
                colvarBillRemark.IsPrimaryKey = false;
                colvarBillRemark.IsForeignKey = false;
                colvarBillRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillRemark);
                
                TableSchema.TableColumn colvarDistributedTotal = new TableSchema.TableColumn(schema);
                colvarDistributedTotal.ColumnName = "distributed_total";
                colvarDistributedTotal.DataType = DbType.Int32;
                colvarDistributedTotal.MaxLength = 0;
                colvarDistributedTotal.AutoIncrement = false;
                colvarDistributedTotal.IsNullable = true;
                colvarDistributedTotal.IsPrimaryKey = false;
                colvarDistributedTotal.IsForeignKey = false;
                colvarDistributedTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarDistributedTotal);
                
                TableSchema.TableColumn colvarDistributedSubtotal = new TableSchema.TableColumn(schema);
                colvarDistributedSubtotal.ColumnName = "distributed_subtotal";
                colvarDistributedSubtotal.DataType = DbType.Int32;
                colvarDistributedSubtotal.MaxLength = 0;
                colvarDistributedSubtotal.AutoIncrement = false;
                colvarDistributedSubtotal.IsNullable = true;
                colvarDistributedSubtotal.IsPrimaryKey = false;
                colvarDistributedSubtotal.IsForeignKey = false;
                colvarDistributedSubtotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarDistributedSubtotal);
                
                TableSchema.TableColumn colvarDistributedTax = new TableSchema.TableColumn(schema);
                colvarDistributedTax.ColumnName = "distributed_tax";
                colvarDistributedTax.DataType = DbType.Int32;
                colvarDistributedTax.MaxLength = 0;
                colvarDistributedTax.AutoIncrement = false;
                colvarDistributedTax.IsNullable = true;
                colvarDistributedTax.IsPrimaryKey = false;
                colvarDistributedTax.IsForeignKey = false;
                colvarDistributedTax.IsReadOnly = false;
                
                schema.Columns.Add(colvarDistributedTax);
                
                TableSchema.TableColumn colvarIsPastConfirmedReadyToPay = new TableSchema.TableColumn(schema);
                colvarIsPastConfirmedReadyToPay.ColumnName = "is_past_confirmed_ready_to_pay";
                colvarIsPastConfirmedReadyToPay.DataType = DbType.Boolean;
                colvarIsPastConfirmedReadyToPay.MaxLength = 0;
                colvarIsPastConfirmedReadyToPay.AutoIncrement = false;
                colvarIsPastConfirmedReadyToPay.IsNullable = true;
                colvarIsPastConfirmedReadyToPay.IsPrimaryKey = false;
                colvarIsPastConfirmedReadyToPay.IsForeignKey = false;
                colvarIsPastConfirmedReadyToPay.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsPastConfirmedReadyToPay);
                
                TableSchema.TableColumn colvarUnconfirmedMbsCount = new TableSchema.TableColumn(schema);
                colvarUnconfirmedMbsCount.ColumnName = "unconfirmed_mbs_count";
                colvarUnconfirmedMbsCount.DataType = DbType.Int32;
                colvarUnconfirmedMbsCount.MaxLength = 0;
                colvarUnconfirmedMbsCount.AutoIncrement = false;
                colvarUnconfirmedMbsCount.IsNullable = true;
                colvarUnconfirmedMbsCount.IsPrimaryKey = false;
                colvarUnconfirmedMbsCount.IsForeignKey = false;
                colvarUnconfirmedMbsCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarUnconfirmedMbsCount);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_balance_sheet_bill_fund_transfer",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBalanceSheetBillFundTransfer()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBalanceSheetBillFundTransfer(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBalanceSheetBillFundTransfer(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBalanceSheetBillFundTransfer(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BalanceSheetId")]
        [Bindable(true)]
        public int BalanceSheetId 
	    {
		    get
		    {
			    return GetColumnValue<int>("balance_sheet_id");
		    }
            set 
		    {
			    SetColumnValue("balance_sheet_id", value);
            }
        }
	      
        [XmlAttribute("BalanceSheetType")]
        [Bindable(true)]
        public int BalanceSheetType 
	    {
		    get
		    {
			    return GetColumnValue<int>("balance_sheet_type");
		    }
            set 
		    {
			    SetColumnValue("balance_sheet_type", value);
            }
        }
	      
        [XmlAttribute("GenerationFrequency")]
        [Bindable(true)]
        public int GenerationFrequency 
	    {
		    get
		    {
			    return GetColumnValue<int>("generation_frequency");
		    }
            set 
		    {
			    SetColumnValue("generation_frequency", value);
            }
        }
	      
        [XmlAttribute("ProductType")]
        [Bindable(true)]
        public int ProductType 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_type");
		    }
            set 
		    {
			    SetColumnValue("product_type", value);
            }
        }
	      
        [XmlAttribute("ProductGuid")]
        [Bindable(true)]
        public Guid ProductGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("product_guid");
		    }
            set 
		    {
			    SetColumnValue("product_guid", value);
            }
        }
	      
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_name");
		    }
            set 
		    {
			    SetColumnValue("deal_name", value);
            }
        }
	      
        [XmlAttribute("RemittanceType")]
        [Bindable(true)]
        public int RemittanceType 
	    {
		    get
		    {
			    return GetColumnValue<int>("remittance_type");
		    }
            set 
		    {
			    SetColumnValue("remittance_type", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("IntervalStart")]
        [Bindable(true)]
        public DateTime? IntervalStart 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("interval_start");
		    }
            set 
		    {
			    SetColumnValue("interval_start", value);
            }
        }
	      
        [XmlAttribute("IntervalEnd")]
        [Bindable(true)]
        public DateTime? IntervalEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("interval_end");
		    }
            set 
		    {
			    SetColumnValue("interval_end", value);
            }
        }
	      
        [XmlAttribute("EstAmount")]
        [Bindable(true)]
        public int? EstAmount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("est_amount");
		    }
            set 
		    {
			    SetColumnValue("est_amount", value);
            }
        }
	      
        [XmlAttribute("PayReportId")]
        [Bindable(true)]
        public int? PayReportId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("pay_report_id");
		    }
            set 
		    {
			    SetColumnValue("pay_report_id", value);
            }
        }
	      
        [XmlAttribute("IsConfirmedReadyToPay")]
        [Bindable(true)]
        public bool IsConfirmedReadyToPay 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_confirmed_ready_to_pay");
		    }
            set 
		    {
			    SetColumnValue("is_confirmed_ready_to_pay", value);
            }
        }
	      
        [XmlAttribute("TransferAmount")]
        [Bindable(true)]
        public int? TransferAmount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("transfer_amount");
		    }
            set 
		    {
			    SetColumnValue("transfer_amount", value);
            }
        }
	      
        [XmlAttribute("IsTransferComplete")]
        [Bindable(true)]
        public bool? IsTransferComplete 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_transfer_complete");
		    }
            set 
		    {
			    SetColumnValue("is_transfer_complete", value);
            }
        }
	      
        [XmlAttribute("TransferTime")]
        [Bindable(true)]
        public DateTime? TransferTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("transfer_time");
		    }
            set 
		    {
			    SetColumnValue("transfer_time", value);
            }
        }
	      
        [XmlAttribute("FundMemo")]
        [Bindable(true)]
        public string FundMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("fund_memo");
		    }
            set 
		    {
			    SetColumnValue("fund_memo", value);
            }
        }
	      
        [XmlAttribute("PayToCompany")]
        [Bindable(true)]
        public int PayToCompany 
	    {
		    get
		    {
			    return GetColumnValue<int>("pay_to_company");
		    }
            set 
		    {
			    SetColumnValue("pay_to_company", value);
            }
        }
	      
        [XmlAttribute("DealAccountTitle")]
        [Bindable(true)]
        public string DealAccountTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_account_title");
		    }
            set 
		    {
			    SetColumnValue("deal_account_title", value);
            }
        }
	      
        [XmlAttribute("DealCompanyId")]
        [Bindable(true)]
        public string DealCompanyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_company_id");
		    }
            set 
		    {
			    SetColumnValue("deal_company_id", value);
            }
        }
	      
        [XmlAttribute("DealBankNo")]
        [Bindable(true)]
        public string DealBankNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_bank_no");
		    }
            set 
		    {
			    SetColumnValue("deal_bank_no", value);
            }
        }
	      
        [XmlAttribute("DealBranchNo")]
        [Bindable(true)]
        public string DealBranchNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_branch_no");
		    }
            set 
		    {
			    SetColumnValue("deal_branch_no", value);
            }
        }
	      
        [XmlAttribute("DealAccountNo")]
        [Bindable(true)]
        public string DealAccountNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_account_no");
		    }
            set 
		    {
			    SetColumnValue("deal_account_no", value);
            }
        }
	      
        [XmlAttribute("SignCompanyId")]
        [Bindable(true)]
        public string SignCompanyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("sign_company_id");
		    }
            set 
		    {
			    SetColumnValue("sign_company_id", value);
            }
        }
	      
        [XmlAttribute("SignCompanyName")]
        [Bindable(true)]
        public string SignCompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("sign_company_name");
		    }
            set 
		    {
			    SetColumnValue("sign_company_name", value);
            }
        }
	      
        [XmlAttribute("ReceiverAccountTitle")]
        [Bindable(true)]
        public string ReceiverAccountTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_account_title");
		    }
            set 
		    {
			    SetColumnValue("receiver_account_title", value);
            }
        }
	      
        [XmlAttribute("ReceiverCompanyId")]
        [Bindable(true)]
        public string ReceiverCompanyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_company_id");
		    }
            set 
		    {
			    SetColumnValue("receiver_company_id", value);
            }
        }
	      
        [XmlAttribute("ReceiverBankNo")]
        [Bindable(true)]
        public string ReceiverBankNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_bank_no");
		    }
            set 
		    {
			    SetColumnValue("receiver_bank_no", value);
            }
        }
	      
        [XmlAttribute("ReceiverBranchNo")]
        [Bindable(true)]
        public string ReceiverBranchNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_branch_no");
		    }
            set 
		    {
			    SetColumnValue("receiver_branch_no", value);
            }
        }
	      
        [XmlAttribute("ReceiverAccountNo")]
        [Bindable(true)]
        public string ReceiverAccountNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_account_no");
		    }
            set 
		    {
			    SetColumnValue("receiver_account_no", value);
            }
        }
	      
        [XmlAttribute("BillCreator")]
        [Bindable(true)]
        public string BillCreator 
	    {
		    get
		    {
			    return GetColumnValue<string>("bill_creator");
		    }
            set 
		    {
			    SetColumnValue("bill_creator", value);
            }
        }
	      
        [XmlAttribute("BillCreateTime")]
        [Bindable(true)]
        public DateTime? BillCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bill_create_time");
		    }
            set 
		    {
			    SetColumnValue("bill_create_time", value);
            }
        }
	      
        [XmlAttribute("SalesName")]
        [Bindable(true)]
        public string SalesName 
	    {
		    get
		    {
			    return GetColumnValue<string>("sales_name");
		    }
            set 
		    {
			    SetColumnValue("sales_name", value);
            }
        }
	      
        [XmlAttribute("BillTotal")]
        [Bindable(true)]
        public int? BillTotal 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bill_total");
		    }
            set 
		    {
			    SetColumnValue("bill_total", value);
            }
        }
	      
        [XmlAttribute("BillSubtotal")]
        [Bindable(true)]
        public int? BillSubtotal 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bill_subtotal");
		    }
            set 
		    {
			    SetColumnValue("bill_subtotal", value);
            }
        }
	      
        [XmlAttribute("BillTax")]
        [Bindable(true)]
        public int? BillTax 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bill_tax");
		    }
            set 
		    {
			    SetColumnValue("bill_tax", value);
            }
        }
	      
        [XmlAttribute("BillNumber")]
        [Bindable(true)]
        public string BillNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("bill_number");
		    }
            set 
		    {
			    SetColumnValue("bill_number", value);
            }
        }
	      
        [XmlAttribute("BillDate")]
        [Bindable(true)]
        public DateTime? BillDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bill_date");
		    }
            set 
		    {
			    SetColumnValue("bill_date", value);
            }
        }
	      
        [XmlAttribute("BillRemark")]
        [Bindable(true)]
        public string BillRemark 
	    {
		    get
		    {
			    return GetColumnValue<string>("bill_remark");
		    }
            set 
		    {
			    SetColumnValue("bill_remark", value);
            }
        }
	      
        [XmlAttribute("DistributedTotal")]
        [Bindable(true)]
        public int? DistributedTotal 
	    {
		    get
		    {
			    return GetColumnValue<int?>("distributed_total");
		    }
            set 
		    {
			    SetColumnValue("distributed_total", value);
            }
        }
	      
        [XmlAttribute("DistributedSubtotal")]
        [Bindable(true)]
        public int? DistributedSubtotal 
	    {
		    get
		    {
			    return GetColumnValue<int?>("distributed_subtotal");
		    }
            set 
		    {
			    SetColumnValue("distributed_subtotal", value);
            }
        }
	      
        [XmlAttribute("DistributedTax")]
        [Bindable(true)]
        public int? DistributedTax 
	    {
		    get
		    {
			    return GetColumnValue<int?>("distributed_tax");
		    }
            set 
		    {
			    SetColumnValue("distributed_tax", value);
            }
        }
	      
        [XmlAttribute("IsPastConfirmedReadyToPay")]
        [Bindable(true)]
        public bool? IsPastConfirmedReadyToPay 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_past_confirmed_ready_to_pay");
		    }
            set 
		    {
			    SetColumnValue("is_past_confirmed_ready_to_pay", value);
            }
        }
	      
        [XmlAttribute("UnconfirmedMbsCount")]
        [Bindable(true)]
        public int? UnconfirmedMbsCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("unconfirmed_mbs_count");
		    }
            set 
		    {
			    SetColumnValue("unconfirmed_mbs_count", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BalanceSheetId = @"balance_sheet_id";
            
            public static string BalanceSheetType = @"balance_sheet_type";
            
            public static string GenerationFrequency = @"generation_frequency";
            
            public static string ProductType = @"product_type";
            
            public static string ProductGuid = @"product_guid";
            
            public static string DealId = @"deal_id";
            
            public static string DealName = @"deal_name";
            
            public static string RemittanceType = @"remittance_type";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string StoreGuid = @"store_guid";
            
            public static string StoreName = @"store_name";
            
            public static string IntervalStart = @"interval_start";
            
            public static string IntervalEnd = @"interval_end";
            
            public static string EstAmount = @"est_amount";
            
            public static string PayReportId = @"pay_report_id";
            
            public static string IsConfirmedReadyToPay = @"is_confirmed_ready_to_pay";
            
            public static string TransferAmount = @"transfer_amount";
            
            public static string IsTransferComplete = @"is_transfer_complete";
            
            public static string TransferTime = @"transfer_time";
            
            public static string FundMemo = @"fund_memo";
            
            public static string PayToCompany = @"pay_to_company";
            
            public static string DealAccountTitle = @"deal_account_title";
            
            public static string DealCompanyId = @"deal_company_id";
            
            public static string DealBankNo = @"deal_bank_no";
            
            public static string DealBranchNo = @"deal_branch_no";
            
            public static string DealAccountNo = @"deal_account_no";
            
            public static string SignCompanyId = @"sign_company_id";
            
            public static string SignCompanyName = @"sign_company_name";
            
            public static string ReceiverAccountTitle = @"receiver_account_title";
            
            public static string ReceiverCompanyId = @"receiver_company_id";
            
            public static string ReceiverBankNo = @"receiver_bank_no";
            
            public static string ReceiverBranchNo = @"receiver_branch_no";
            
            public static string ReceiverAccountNo = @"receiver_account_no";
            
            public static string BillCreator = @"bill_creator";
            
            public static string BillCreateTime = @"bill_create_time";
            
            public static string SalesName = @"sales_name";
            
            public static string BillTotal = @"bill_total";
            
            public static string BillSubtotal = @"bill_subtotal";
            
            public static string BillTax = @"bill_tax";
            
            public static string BillNumber = @"bill_number";
            
            public static string BillDate = @"bill_date";
            
            public static string BillRemark = @"bill_remark";
            
            public static string DistributedTotal = @"distributed_total";
            
            public static string DistributedSubtotal = @"distributed_subtotal";
            
            public static string DistributedTax = @"distributed_tax";
            
            public static string IsPastConfirmedReadyToPay = @"is_past_confirmed_ready_to_pay";
            
            public static string UnconfirmedMbsCount = @"unconfirmed_mbs_count";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

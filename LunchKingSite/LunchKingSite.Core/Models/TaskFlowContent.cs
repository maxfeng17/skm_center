using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the TaskFlowContent class.
    /// </summary>
    [Serializable]
    public partial class TaskFlowContentCollection : RepositoryList<TaskFlowContent, TaskFlowContentCollection>
    {
        public TaskFlowContentCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TaskFlowContentCollection</returns>
        public TaskFlowContentCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TaskFlowContent o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the task_flow_content table.
    /// </summary>
    [Serializable]
    public partial class TaskFlowContent : RepositoryRecord<TaskFlowContent>, IRecordBase
    {
        #region .ctors and Default Settings

        public TaskFlowContent()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public TaskFlowContent(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("task_flow_content", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarProcessId = new TableSchema.TableColumn(schema);
                colvarProcessId.ColumnName = "process_id";
                colvarProcessId.DataType = DbType.Guid;
                colvarProcessId.MaxLength = 0;
                colvarProcessId.AutoIncrement = false;
                colvarProcessId.IsNullable = false;
                colvarProcessId.IsPrimaryKey = false;
                colvarProcessId.IsForeignKey = false;
                colvarProcessId.IsReadOnly = false;
                colvarProcessId.DefaultSetting = @"";
                colvarProcessId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProcessId);

                TableSchema.TableColumn colvarTaskId = new TableSchema.TableColumn(schema);
                colvarTaskId.ColumnName = "task_id";
                colvarTaskId.DataType = DbType.Guid;
                colvarTaskId.MaxLength = 0;
                colvarTaskId.AutoIncrement = false;
                colvarTaskId.IsNullable = false;
                colvarTaskId.IsPrimaryKey = true;
                colvarTaskId.IsForeignKey = false;
                colvarTaskId.IsReadOnly = false;
                colvarTaskId.DefaultSetting = @"";
                colvarTaskId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTaskId);

                TableSchema.TableColumn colvarParentTaskId = new TableSchema.TableColumn(schema);
                colvarParentTaskId.ColumnName = "parent_task_id";
                colvarParentTaskId.DataType = DbType.Guid;
                colvarParentTaskId.MaxLength = 0;
                colvarParentTaskId.AutoIncrement = false;
                colvarParentTaskId.IsNullable = false;
                colvarParentTaskId.IsPrimaryKey = false;
                colvarParentTaskId.IsForeignKey = false;
                colvarParentTaskId.IsReadOnly = false;
                colvarParentTaskId.DefaultSetting = @"";
                colvarParentTaskId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarParentTaskId);

                TableSchema.TableColumn colvarApplyUser = new TableSchema.TableColumn(schema);
                colvarApplyUser.ColumnName = "apply_user";
                colvarApplyUser.DataType = DbType.Guid;
                colvarApplyUser.MaxLength = 0;
                colvarApplyUser.AutoIncrement = false;
                colvarApplyUser.IsNullable = false;
                colvarApplyUser.IsPrimaryKey = false;
                colvarApplyUser.IsForeignKey = false;
                colvarApplyUser.IsReadOnly = false;
                colvarApplyUser.DefaultSetting = @"";
                colvarApplyUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarApplyUser);

                TableSchema.TableColumn colvarExecUser = new TableSchema.TableColumn(schema);
                colvarExecUser.ColumnName = "exec_user";
                colvarExecUser.DataType = DbType.Guid;
                colvarExecUser.MaxLength = 0;
                colvarExecUser.AutoIncrement = false;
                colvarExecUser.IsNullable = false;
                colvarExecUser.IsPrimaryKey = false;
                colvarExecUser.IsForeignKey = false;
                colvarExecUser.IsReadOnly = false;
                colvarExecUser.DefaultSetting = @"";
                colvarExecUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarExecUser);

                TableSchema.TableColumn colvarRealExecUser = new TableSchema.TableColumn(schema);
                colvarRealExecUser.ColumnName = "real_exec_user";
                colvarRealExecUser.DataType = DbType.Guid;
                colvarRealExecUser.MaxLength = 0;
                colvarRealExecUser.AutoIncrement = false;
                colvarRealExecUser.IsNullable = false;
                colvarRealExecUser.IsPrimaryKey = false;
                colvarRealExecUser.IsForeignKey = false;
                colvarRealExecUser.IsReadOnly = false;
                colvarRealExecUser.DefaultSetting = @"";
                colvarRealExecUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRealExecUser);

                TableSchema.TableColumn colvarDataContent = new TableSchema.TableColumn(schema);
                colvarDataContent.ColumnName = "data_content";
                colvarDataContent.DataType = DbType.String;
                colvarDataContent.MaxLength = -1;
                colvarDataContent.AutoIncrement = false;
                colvarDataContent.IsNullable = false;
                colvarDataContent.IsPrimaryKey = false;
                colvarDataContent.IsForeignKey = false;
                colvarDataContent.IsReadOnly = false;
                colvarDataContent.DefaultSetting = @"";
                colvarDataContent.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDataContent);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Guid;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarExecTime = new TableSchema.TableColumn(schema);
                colvarExecTime.ColumnName = "exec_time";
                colvarExecTime.DataType = DbType.DateTime;
                colvarExecTime.MaxLength = 0;
                colvarExecTime.AutoIncrement = false;
                colvarExecTime.IsNullable = true;
                colvarExecTime.IsPrimaryKey = false;
                colvarExecTime.IsForeignKey = false;
                colvarExecTime.IsReadOnly = false;
                colvarExecTime.DefaultSetting = @"";
                colvarExecTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarExecTime);

                TableSchema.TableColumn colvarEipTaskStatus = new TableSchema.TableColumn(schema);
                colvarEipTaskStatus.ColumnName = "eip_task_status";
                colvarEipTaskStatus.DataType = DbType.Int32;
                colvarEipTaskStatus.MaxLength = 0;
                colvarEipTaskStatus.AutoIncrement = false;
                colvarEipTaskStatus.IsNullable = false;
                colvarEipTaskStatus.IsPrimaryKey = false;
                colvarEipTaskStatus.IsForeignKey = false;
                colvarEipTaskStatus.IsReadOnly = false;
                colvarEipTaskStatus.DefaultSetting = @"";
                colvarEipTaskStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEipTaskStatus);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("task_flow_content", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("ProcessId")]
        [Bindable(true)]
        public Guid ProcessId
        {
            get { return GetColumnValue<Guid>(Columns.ProcessId); }
            set { SetColumnValue(Columns.ProcessId, value); }
        }

        [XmlAttribute("TaskId")]
        [Bindable(true)]
        public Guid TaskId
        {
            get { return GetColumnValue<Guid>(Columns.TaskId); }
            set { SetColumnValue(Columns.TaskId, value); }
        }

        [XmlAttribute("ParentTaskId")]
        [Bindable(true)]
        public Guid ParentTaskId
        {
            get { return GetColumnValue<Guid>(Columns.ParentTaskId); }
            set { SetColumnValue(Columns.ParentTaskId, value); }
        }

        [XmlAttribute("ApplyUser")]
        [Bindable(true)]
        public Guid ApplyUser
        {
            get { return GetColumnValue<Guid>(Columns.ApplyUser); }
            set { SetColumnValue(Columns.ApplyUser, value); }
        }

        [XmlAttribute("ExecUser")]
        [Bindable(true)]
        public Guid ExecUser
        {
            get { return GetColumnValue<Guid>(Columns.ExecUser); }
            set { SetColumnValue(Columns.ExecUser, value); }
        }

        [XmlAttribute("RealExecUser")]
        [Bindable(true)]
        public Guid RealExecUser
        {
            get { return GetColumnValue<Guid>(Columns.RealExecUser); }
            set { SetColumnValue(Columns.RealExecUser, value); }
        }

        [XmlAttribute("DataContent")]
        [Bindable(true)]
        public string DataContent
        {
            get { return GetColumnValue<string>(Columns.DataContent); }
            set { SetColumnValue(Columns.DataContent, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public Guid CreateId
        {
            get { return GetColumnValue<Guid>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ExecTime")]
        [Bindable(true)]
        public DateTime? ExecTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ExecTime); }
            set { SetColumnValue(Columns.ExecTime, value); }
        }

        [XmlAttribute("EipTaskStatus")]
        [Bindable(true)]
        public int EipTaskStatus
        {
            get { return GetColumnValue<int>(Columns.EipTaskStatus); }
            set { SetColumnValue(Columns.EipTaskStatus, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn ProcessIdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn TaskIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ParentTaskIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn ApplyUserColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ExecUserColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn RealExecUserColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn DataContentColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn ExecTimeColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn EipTaskStatusColumn
        {
            get { return Schema.Columns[10]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string ProcessId = @"process_id";
            public static string TaskId = @"task_id";
            public static string ParentTaskId = @"parent_task_id";
            public static string ApplyUser = @"apply_user";
            public static string ExecUser = @"exec_user";
            public static string RealExecUser = @"real_exec_user";
            public static string DataContent = @"data_content";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ExecTime = @"exec_time";
            public static string EipTaskStatus = @"eip_task_status";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

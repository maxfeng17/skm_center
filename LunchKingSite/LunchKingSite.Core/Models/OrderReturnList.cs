using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OrderReturnList class.
	/// </summary>
    [Serializable]
	public partial class OrderReturnListCollection : RepositoryList<OrderReturnList, OrderReturnListCollection>
	{	   
		public OrderReturnListCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OrderReturnListCollection</returns>
		public OrderReturnListCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OrderReturnList o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the order_return_list table.
	/// </summary>
	[Serializable]
	public partial class OrderReturnList : RepositoryRecord<OrderReturnList>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OrderReturnList()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OrderReturnList(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order_return_list", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = true;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				
					colvarOrderGuid.ForeignKeyTableName = "order";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
				colvarFlag.ColumnName = "flag";
				colvarFlag.DataType = DbType.Int32;
				colvarFlag.MaxLength = 0;
				colvarFlag.AutoIncrement = false;
				colvarFlag.IsNullable = false;
				colvarFlag.IsPrimaryKey = false;
				colvarFlag.IsForeignKey = false;
				colvarFlag.IsReadOnly = false;
				
						colvarFlag.DefaultSetting = @"((0))";
				colvarFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFlag);
				
				TableSchema.TableColumn colvarReason = new TableSchema.TableColumn(schema);
				colvarReason.ColumnName = "reason";
				colvarReason.DataType = DbType.String;
				colvarReason.MaxLength = -1;
				colvarReason.AutoIncrement = false;
				colvarReason.IsNullable = true;
				colvarReason.IsPrimaryKey = false;
				colvarReason.IsForeignKey = false;
				colvarReason.IsReadOnly = false;
				colvarReason.DefaultSetting = @"";
				colvarReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReason);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = -1;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 50;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarCouponIds = new TableSchema.TableColumn(schema);
				colvarCouponIds.ColumnName = "coupon_ids";
				colvarCouponIds.DataType = DbType.AnsiString;
				colvarCouponIds.MaxLength = -1;
				colvarCouponIds.AutoIncrement = false;
				colvarCouponIds.IsNullable = true;
				colvarCouponIds.IsPrimaryKey = false;
				colvarCouponIds.IsForeignKey = false;
				colvarCouponIds.IsReadOnly = false;
				colvarCouponIds.DefaultSetting = @"";
				colvarCouponIds.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponIds);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"('1')";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarExchangeQuantity = new TableSchema.TableColumn(schema);
				colvarExchangeQuantity.ColumnName = "exchange_quantity";
				colvarExchangeQuantity.DataType = DbType.Int32;
				colvarExchangeQuantity.MaxLength = 0;
				colvarExchangeQuantity.AutoIncrement = false;
				colvarExchangeQuantity.IsNullable = false;
				colvarExchangeQuantity.IsPrimaryKey = false;
				colvarExchangeQuantity.IsForeignKey = false;
				colvarExchangeQuantity.IsReadOnly = false;
				
						colvarExchangeQuantity.DefaultSetting = @"('0')";
				colvarExchangeQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExchangeQuantity);
				
				TableSchema.TableColumn colvarVendorProgressStatus = new TableSchema.TableColumn(schema);
				colvarVendorProgressStatus.ColumnName = "vendor_progress_status";
				colvarVendorProgressStatus.DataType = DbType.Int32;
				colvarVendorProgressStatus.MaxLength = 0;
				colvarVendorProgressStatus.AutoIncrement = false;
				colvarVendorProgressStatus.IsNullable = true;
				colvarVendorProgressStatus.IsPrimaryKey = false;
				colvarVendorProgressStatus.IsForeignKey = false;
				colvarVendorProgressStatus.IsReadOnly = false;
				colvarVendorProgressStatus.DefaultSetting = @"";
				colvarVendorProgressStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorProgressStatus);
				
				TableSchema.TableColumn colvarVendorProcessTime = new TableSchema.TableColumn(schema);
				colvarVendorProcessTime.ColumnName = "vendor_process_time";
				colvarVendorProcessTime.DataType = DbType.DateTime;
				colvarVendorProcessTime.MaxLength = 0;
				colvarVendorProcessTime.AutoIncrement = false;
				colvarVendorProcessTime.IsNullable = true;
				colvarVendorProcessTime.IsPrimaryKey = false;
				colvarVendorProcessTime.IsForeignKey = false;
				colvarVendorProcessTime.IsReadOnly = false;
				colvarVendorProcessTime.DefaultSetting = @"";
				colvarVendorProcessTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorProcessTime);
				
				TableSchema.TableColumn colvarVendorMemo = new TableSchema.TableColumn(schema);
				colvarVendorMemo.ColumnName = "vendor_memo";
				colvarVendorMemo.DataType = DbType.String;
				colvarVendorMemo.MaxLength = 500;
				colvarVendorMemo.AutoIncrement = false;
				colvarVendorMemo.IsNullable = true;
				colvarVendorMemo.IsPrimaryKey = false;
				colvarVendorMemo.IsForeignKey = false;
				colvarVendorMemo.IsReadOnly = false;
				colvarVendorMemo.DefaultSetting = @"";
				colvarVendorMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorMemo);
				
				TableSchema.TableColumn colvarOrderShipId = new TableSchema.TableColumn(schema);
				colvarOrderShipId.ColumnName = "order_ship_id";
				colvarOrderShipId.DataType = DbType.Int32;
				colvarOrderShipId.MaxLength = 0;
				colvarOrderShipId.AutoIncrement = false;
				colvarOrderShipId.IsNullable = true;
				colvarOrderShipId.IsPrimaryKey = false;
				colvarOrderShipId.IsForeignKey = false;
				colvarOrderShipId.IsReadOnly = false;
				colvarOrderShipId.DefaultSetting = @"";
				colvarOrderShipId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderShipId);
				
				TableSchema.TableColumn colvarMessageUpdate = new TableSchema.TableColumn(schema);
				colvarMessageUpdate.ColumnName = "message_update";
				colvarMessageUpdate.DataType = DbType.Boolean;
				colvarMessageUpdate.MaxLength = 0;
				colvarMessageUpdate.AutoIncrement = false;
				colvarMessageUpdate.IsNullable = false;
				colvarMessageUpdate.IsPrimaryKey = false;
				colvarMessageUpdate.IsForeignKey = false;
				colvarMessageUpdate.IsReadOnly = false;
				
						colvarMessageUpdate.DefaultSetting = @"((0))";
				colvarMessageUpdate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessageUpdate);
				
				TableSchema.TableColumn colvarReceiverName = new TableSchema.TableColumn(schema);
				colvarReceiverName.ColumnName = "receiver_name";
				colvarReceiverName.DataType = DbType.String;
				colvarReceiverName.MaxLength = 50;
				colvarReceiverName.AutoIncrement = false;
				colvarReceiverName.IsNullable = true;
				colvarReceiverName.IsPrimaryKey = false;
				colvarReceiverName.IsForeignKey = false;
				colvarReceiverName.IsReadOnly = false;
				colvarReceiverName.DefaultSetting = @"";
				colvarReceiverName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverName);
				
				TableSchema.TableColumn colvarReceiverAddress = new TableSchema.TableColumn(schema);
				colvarReceiverAddress.ColumnName = "receiver_address";
				colvarReceiverAddress.DataType = DbType.String;
				colvarReceiverAddress.MaxLength = 200;
				colvarReceiverAddress.AutoIncrement = false;
				colvarReceiverAddress.IsNullable = true;
				colvarReceiverAddress.IsPrimaryKey = false;
				colvarReceiverAddress.IsForeignKey = false;
				colvarReceiverAddress.IsReadOnly = false;
				colvarReceiverAddress.DefaultSetting = @"";
				colvarReceiverAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverAddress);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order_return_list",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Flag")]
		[Bindable(true)]
		public int Flag 
		{
			get { return GetColumnValue<int>(Columns.Flag); }
			set { SetColumnValue(Columns.Flag, value); }
		}
		  
		[XmlAttribute("Reason")]
		[Bindable(true)]
		public string Reason 
		{
			get { return GetColumnValue<string>(Columns.Reason); }
			set { SetColumnValue(Columns.Reason, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("CouponIds")]
		[Bindable(true)]
		public string CouponIds 
		{
			get { return GetColumnValue<string>(Columns.CouponIds); }
			set { SetColumnValue(Columns.CouponIds, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("ExchangeQuantity")]
		[Bindable(true)]
		public int ExchangeQuantity 
		{
			get { return GetColumnValue<int>(Columns.ExchangeQuantity); }
			set { SetColumnValue(Columns.ExchangeQuantity, value); }
		}
		  
		[XmlAttribute("VendorProgressStatus")]
		[Bindable(true)]
		public int? VendorProgressStatus 
		{
			get { return GetColumnValue<int?>(Columns.VendorProgressStatus); }
			set { SetColumnValue(Columns.VendorProgressStatus, value); }
		}
		  
		[XmlAttribute("VendorProcessTime")]
		[Bindable(true)]
		public DateTime? VendorProcessTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.VendorProcessTime); }
			set { SetColumnValue(Columns.VendorProcessTime, value); }
		}
		  
		[XmlAttribute("VendorMemo")]
		[Bindable(true)]
		public string VendorMemo 
		{
			get { return GetColumnValue<string>(Columns.VendorMemo); }
			set { SetColumnValue(Columns.VendorMemo, value); }
		}
		  
		[XmlAttribute("OrderShipId")]
		[Bindable(true)]
		public int? OrderShipId 
		{
			get { return GetColumnValue<int?>(Columns.OrderShipId); }
			set { SetColumnValue(Columns.OrderShipId, value); }
		}
		  
		[XmlAttribute("MessageUpdate")]
		[Bindable(true)]
		public bool MessageUpdate 
		{
			get { return GetColumnValue<bool>(Columns.MessageUpdate); }
			set { SetColumnValue(Columns.MessageUpdate, value); }
		}
		  
		[XmlAttribute("ReceiverName")]
		[Bindable(true)]
		public string ReceiverName 
		{
			get { return GetColumnValue<string>(Columns.ReceiverName); }
			set { SetColumnValue(Columns.ReceiverName, value); }
		}
		  
		[XmlAttribute("ReceiverAddress")]
		[Bindable(true)]
		public string ReceiverAddress 
		{
			get { return GetColumnValue<string>(Columns.ReceiverAddress); }
			set { SetColumnValue(Columns.ReceiverAddress, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn FlagColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ReasonColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponIdsColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ExchangeQuantityColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorProgressStatusColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorProcessTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorMemoColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderShipIdColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageUpdateColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverNameColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverAddressColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderGuid = @"order_guid";
			 public static string Status = @"status";
			 public static string Flag = @"flag";
			 public static string Reason = @"reason";
			 public static string Message = @"message";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string CouponIds = @"coupon_ids";
			 public static string Type = @"type";
			 public static string ExchangeQuantity = @"exchange_quantity";
			 public static string VendorProgressStatus = @"vendor_progress_status";
			 public static string VendorProcessTime = @"vendor_process_time";
			 public static string VendorMemo = @"vendor_memo";
			 public static string OrderShipId = @"order_ship_id";
			 public static string MessageUpdate = @"message_update";
			 public static string ReceiverName = @"receiver_name";
			 public static string ReceiverAddress = @"receiver_address";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewStoreCategory class.
    /// </summary>
    [Serializable]
    public partial class ViewStoreCategoryCollection : ReadOnlyList<ViewStoreCategory, ViewStoreCategoryCollection>
    {        
        public ViewStoreCategoryCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_store_category view.
    /// </summary>
    [Serializable]
    public partial class ViewStoreCategory : ReadOnlyRecord<ViewStoreCategory>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_store_category", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = false;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarCategoryCode = new TableSchema.TableColumn(schema);
                colvarCategoryCode.ColumnName = "category_code";
                colvarCategoryCode.DataType = DbType.Int32;
                colvarCategoryCode.MaxLength = 0;
                colvarCategoryCode.AutoIncrement = false;
                colvarCategoryCode.IsNullable = false;
                colvarCategoryCode.IsPrimaryKey = false;
                colvarCategoryCode.IsForeignKey = false;
                colvarCategoryCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryCode);
                
                TableSchema.TableColumn colvarCreateType = new TableSchema.TableColumn(schema);
                colvarCreateType.ColumnName = "create_type";
                colvarCreateType.DataType = DbType.Int32;
                colvarCreateType.MaxLength = 0;
                colvarCreateType.AutoIncrement = false;
                colvarCreateType.IsNullable = false;
                colvarCreateType.IsPrimaryKey = false;
                colvarCreateType.IsForeignKey = false;
                colvarCreateType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateType);
                
                TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
                colvarCategoryId.ColumnName = "category_id";
                colvarCategoryId.DataType = DbType.Int32;
                colvarCategoryId.MaxLength = 0;
                colvarCategoryId.AutoIncrement = false;
                colvarCategoryId.IsNullable = true;
                colvarCategoryId.IsPrimaryKey = false;
                colvarCategoryId.IsForeignKey = false;
                colvarCategoryId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryId);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 50;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarNameInConsole = new TableSchema.TableColumn(schema);
                colvarNameInConsole.ColumnName = "name_in_console";
                colvarNameInConsole.DataType = DbType.String;
                colvarNameInConsole.MaxLength = 50;
                colvarNameInConsole.AutoIncrement = false;
                colvarNameInConsole.IsNullable = true;
                colvarNameInConsole.IsPrimaryKey = false;
                colvarNameInConsole.IsForeignKey = false;
                colvarNameInConsole.IsReadOnly = false;
                
                schema.Columns.Add(colvarNameInConsole);
                
                TableSchema.TableColumn colvarIsFinal = new TableSchema.TableColumn(schema);
                colvarIsFinal.ColumnName = "is_final";
                colvarIsFinal.DataType = DbType.Boolean;
                colvarIsFinal.MaxLength = 0;
                colvarIsFinal.AutoIncrement = false;
                colvarIsFinal.IsNullable = true;
                colvarIsFinal.IsPrimaryKey = false;
                colvarIsFinal.IsForeignKey = false;
                colvarIsFinal.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsFinal);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = true;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarRank = new TableSchema.TableColumn(schema);
                colvarRank.ColumnName = "rank";
                colvarRank.DataType = DbType.Int32;
                colvarRank.MaxLength = 0;
                colvarRank.AutoIncrement = false;
                colvarRank.IsNullable = true;
                colvarRank.IsPrimaryKey = false;
                colvarRank.IsForeignKey = false;
                colvarRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarRank);
                
                TableSchema.TableColumn colvarParentId = new TableSchema.TableColumn(schema);
                colvarParentId.ColumnName = "parent_id";
                colvarParentId.DataType = DbType.Int32;
                colvarParentId.MaxLength = 0;
                colvarParentId.AutoIncrement = false;
                colvarParentId.IsNullable = false;
                colvarParentId.IsPrimaryKey = false;
                colvarParentId.IsForeignKey = false;
                colvarParentId.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_store_category",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewStoreCategory()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewStoreCategory(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewStoreCategory(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewStoreCategory(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("CategoryCode")]
        [Bindable(true)]
        public int CategoryCode 
	    {
		    get
		    {
			    return GetColumnValue<int>("category_code");
		    }
            set 
		    {
			    SetColumnValue("category_code", value);
            }
        }
	      
        [XmlAttribute("CreateType")]
        [Bindable(true)]
        public int CreateType 
	    {
		    get
		    {
			    return GetColumnValue<int>("create_type");
		    }
            set 
		    {
			    SetColumnValue("create_type", value);
            }
        }
	      
        [XmlAttribute("CategoryId")]
        [Bindable(true)]
        public int? CategoryId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category_id");
		    }
            set 
		    {
			    SetColumnValue("category_id", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("NameInConsole")]
        [Bindable(true)]
        public string NameInConsole 
	    {
		    get
		    {
			    return GetColumnValue<string>("name_in_console");
		    }
            set 
		    {
			    SetColumnValue("name_in_console", value);
            }
        }
	      
        [XmlAttribute("IsFinal")]
        [Bindable(true)]
        public bool? IsFinal 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_final");
		    }
            set 
		    {
			    SetColumnValue("is_final", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int? Type 
	    {
		    get
		    {
			    return GetColumnValue<int?>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("Rank")]
        [Bindable(true)]
        public int? Rank 
	    {
		    get
		    {
			    return GetColumnValue<int?>("rank");
		    }
            set 
		    {
			    SetColumnValue("rank", value);
            }
        }
	      
        [XmlAttribute("ParentId")]
        [Bindable(true)]
        public int ParentId 
	    {
		    get
		    {
			    return GetColumnValue<int>("parent_id");
		    }
            set 
		    {
			    SetColumnValue("parent_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string StoreGuid = @"store_guid";
            
            public static string CategoryCode = @"category_code";
            
            public static string CreateType = @"create_type";
            
            public static string CategoryId = @"category_id";
            
            public static string Name = @"name";
            
            public static string NameInConsole = @"name_in_console";
            
            public static string IsFinal = @"is_final";
            
            public static string Type = @"type";
            
            public static string Rank = @"rank";
            
            public static string ParentId = @"parent_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealCityseq class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealCityseqCollection : ReadOnlyList<ViewHiDealCityseq, ViewHiDealCityseqCollection>
    {        
        public ViewHiDealCityseqCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_cityseq view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealCityseq : ReadOnlyRecord<ViewHiDealCityseq>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_cityseq", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = true;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarSeq);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = false;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                TableSchema.TableColumn colvarIsMain = new TableSchema.TableColumn(schema);
                colvarIsMain.ColumnName = "is_main";
                colvarIsMain.DataType = DbType.Boolean;
                colvarIsMain.MaxLength = 0;
                colvarIsMain.AutoIncrement = false;
                colvarIsMain.IsNullable = false;
                colvarIsMain.IsPrimaryKey = false;
                colvarIsMain.IsForeignKey = false;
                colvarIsMain.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsMain);
                
                TableSchema.TableColumn colvarCity = new TableSchema.TableColumn(schema);
                colvarCity.ColumnName = "city";
                colvarCity.DataType = DbType.String;
                colvarCity.MaxLength = 50;
                colvarCity.AutoIncrement = false;
                colvarCity.IsNullable = false;
                colvarCity.IsPrimaryKey = false;
                colvarCity.IsForeignKey = false;
                colvarCity.IsReadOnly = false;
                
                schema.Columns.Add(colvarCity);
                
                TableSchema.TableColumn colvarCityCodeId = new TableSchema.TableColumn(schema);
                colvarCityCodeId.ColumnName = "city_code_id";
                colvarCityCodeId.DataType = DbType.Int32;
                colvarCityCodeId.MaxLength = 0;
                colvarCityCodeId.AutoIncrement = false;
                colvarCityCodeId.IsNullable = false;
                colvarCityCodeId.IsPrimaryKey = false;
                colvarCityCodeId.IsForeignKey = false;
                colvarCityCodeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityCodeId);
                
                TableSchema.TableColumn colvarCityCodeGroup = new TableSchema.TableColumn(schema);
                colvarCityCodeGroup.ColumnName = "city_code_group";
                colvarCityCodeGroup.DataType = DbType.AnsiString;
                colvarCityCodeGroup.MaxLength = 50;
                colvarCityCodeGroup.AutoIncrement = false;
                colvarCityCodeGroup.IsNullable = false;
                colvarCityCodeGroup.IsPrimaryKey = false;
                colvarCityCodeGroup.IsForeignKey = false;
                colvarCityCodeGroup.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityCodeGroup);
                
                TableSchema.TableColumn colvarHiDealId = new TableSchema.TableColumn(schema);
                colvarHiDealId.ColumnName = "hi_deal_id";
                colvarHiDealId.DataType = DbType.Int32;
                colvarHiDealId.MaxLength = 0;
                colvarHiDealId.AutoIncrement = false;
                colvarHiDealId.IsNullable = false;
                colvarHiDealId.IsPrimaryKey = false;
                colvarHiDealId.IsForeignKey = false;
                colvarHiDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealId);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 50;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "product_name";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 250;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = true;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_cityseq",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealCityseq()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealCityseq(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealCityseq(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealCityseq(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int? Seq 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seq");
		    }
            set 
		    {
			    SetColumnValue("seq", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	      
        [XmlAttribute("IsMain")]
        [Bindable(true)]
        public bool IsMain 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_main");
		    }
            set 
		    {
			    SetColumnValue("is_main", value);
            }
        }
	      
        [XmlAttribute("City")]
        [Bindable(true)]
        public string City 
	    {
		    get
		    {
			    return GetColumnValue<string>("city");
		    }
            set 
		    {
			    SetColumnValue("city", value);
            }
        }
	      
        [XmlAttribute("CityCodeId")]
        [Bindable(true)]
        public int CityCodeId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_code_id");
		    }
            set 
		    {
			    SetColumnValue("city_code_id", value);
            }
        }
	      
        [XmlAttribute("CityCodeGroup")]
        [Bindable(true)]
        public string CityCodeGroup 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_code_group");
		    }
            set 
		    {
			    SetColumnValue("city_code_group", value);
            }
        }
	      
        [XmlAttribute("HiDealId")]
        [Bindable(true)]
        public int HiDealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("hi_deal_id");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_id", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_start_time", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_name");
		    }
            set 
		    {
			    SetColumnValue("product_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string Seq = @"seq";
            
            public static string StartTime = @"start_time";
            
            public static string EndTime = @"end_time";
            
            public static string IsMain = @"is_main";
            
            public static string City = @"city";
            
            public static string CityCodeId = @"city_code_id";
            
            public static string CityCodeGroup = @"city_code_group";
            
            public static string HiDealId = @"hi_deal_id";
            
            public static string Name = @"name";
            
            public static string DealStartTime = @"deal_start_time";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string ProductName = @"product_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

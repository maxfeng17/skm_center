using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmShoppeChangeLog class.
	/// </summary>
    [Serializable]
	public partial class SkmShoppeChangeLogCollection : RepositoryList<SkmShoppeChangeLog, SkmShoppeChangeLogCollection>
	{	   
		public SkmShoppeChangeLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmShoppeChangeLogCollection</returns>
		public SkmShoppeChangeLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmShoppeChangeLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_shoppe_change_log table.
	/// </summary>
	
	[Serializable]
	public partial class SkmShoppeChangeLog : RepositoryRecord<SkmShoppeChangeLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmShoppeChangeLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmShoppeChangeLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_shoppe_change_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBrandCounterCode = new TableSchema.TableColumn(schema);
				colvarBrandCounterCode.ColumnName = "brand_counter_code";
				colvarBrandCounterCode.DataType = DbType.AnsiString;
				colvarBrandCounterCode.MaxLength = 10;
				colvarBrandCounterCode.AutoIncrement = false;
				colvarBrandCounterCode.IsNullable = true;
				colvarBrandCounterCode.IsPrimaryKey = false;
				colvarBrandCounterCode.IsForeignKey = false;
				colvarBrandCounterCode.IsReadOnly = false;
				colvarBrandCounterCode.DefaultSetting = @"";
				colvarBrandCounterCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandCounterCode);
				
				TableSchema.TableColumn colvarShopCode = new TableSchema.TableColumn(schema);
				colvarShopCode.ColumnName = "shop_code";
				colvarShopCode.DataType = DbType.AnsiString;
				colvarShopCode.MaxLength = 4;
				colvarShopCode.AutoIncrement = false;
				colvarShopCode.IsNullable = true;
				colvarShopCode.IsPrimaryKey = false;
				colvarShopCode.IsForeignKey = false;
				colvarShopCode.IsReadOnly = false;
				colvarShopCode.DefaultSetting = @"";
				colvarShopCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShopCode);
				
				TableSchema.TableColumn colvarChangeMemo = new TableSchema.TableColumn(schema);
				colvarChangeMemo.ColumnName = "change_memo";
				colvarChangeMemo.DataType = DbType.AnsiString;
				colvarChangeMemo.MaxLength = 200;
				colvarChangeMemo.AutoIncrement = false;
				colvarChangeMemo.IsNullable = true;
				colvarChangeMemo.IsPrimaryKey = false;
				colvarChangeMemo.IsForeignKey = false;
				colvarChangeMemo.IsReadOnly = false;
				colvarChangeMemo.DefaultSetting = @"";
				colvarChangeMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChangeMemo);
				
				TableSchema.TableColumn colvarPreviousObj = new TableSchema.TableColumn(schema);
				colvarPreviousObj.ColumnName = "previous_obj";
				colvarPreviousObj.DataType = DbType.String;
				colvarPreviousObj.MaxLength = -1;
				colvarPreviousObj.AutoIncrement = false;
				colvarPreviousObj.IsNullable = true;
				colvarPreviousObj.IsPrimaryKey = false;
				colvarPreviousObj.IsForeignKey = false;
				colvarPreviousObj.IsReadOnly = false;
				colvarPreviousObj.DefaultSetting = @"";
				colvarPreviousObj.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPreviousObj);
				
				TableSchema.TableColumn colvarNextObj = new TableSchema.TableColumn(schema);
				colvarNextObj.ColumnName = "next_obj";
				colvarNextObj.DataType = DbType.String;
				colvarNextObj.MaxLength = -1;
				colvarNextObj.AutoIncrement = false;
				colvarNextObj.IsNullable = true;
				colvarNextObj.IsPrimaryKey = false;
				colvarNextObj.IsForeignKey = false;
				colvarNextObj.IsReadOnly = false;
				colvarNextObj.DefaultSetting = @"";
				colvarNextObj.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNextObj);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_shoppe_change_log",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("BrandCounterCode")]
		[Bindable(true)]
		public string BrandCounterCode 
		{
			get { return GetColumnValue<string>(Columns.BrandCounterCode); }
			set { SetColumnValue(Columns.BrandCounterCode, value); }
		}
		
		[XmlAttribute("ShopCode")]
		[Bindable(true)]
		public string ShopCode 
		{
			get { return GetColumnValue<string>(Columns.ShopCode); }
			set { SetColumnValue(Columns.ShopCode, value); }
		}
		
		[XmlAttribute("ChangeMemo")]
		[Bindable(true)]
		public string ChangeMemo 
		{
			get { return GetColumnValue<string>(Columns.ChangeMemo); }
			set { SetColumnValue(Columns.ChangeMemo, value); }
		}
		
		[XmlAttribute("PreviousObj")]
		[Bindable(true)]
		public string PreviousObj 
		{
			get { return GetColumnValue<string>(Columns.PreviousObj); }
			set { SetColumnValue(Columns.PreviousObj, value); }
		}
		
		[XmlAttribute("NextObj")]
		[Bindable(true)]
		public string NextObj 
		{
			get { return GetColumnValue<string>(Columns.NextObj); }
			set { SetColumnValue(Columns.NextObj, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BrandCounterCodeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ShopCodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ChangeMemoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PreviousObjColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn NextObjColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BrandCounterCode = @"brand_counter_code";
			 public static string ShopCode = @"shop_code";
			 public static string ChangeMemo = @"change_memo";
			 public static string PreviousObj = @"previous_obj";
			 public static string NextObj = @"next_obj";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

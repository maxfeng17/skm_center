using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewReturnFormProductOptionList class.
    /// </summary>
    [Serializable]
    public partial class ViewReturnFormProductOptionListCollection : ReadOnlyList<ViewReturnFormProductOptionList, ViewReturnFormProductOptionListCollection>
    {
        public ViewReturnFormProductOptionListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_return_form_product_option_list view.
    /// </summary>
    [Serializable]
    public partial class ViewReturnFormProductOptionList : ReadOnlyRecord<ViewReturnFormProductOptionList>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_return_form_product_option_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
                colvarReturnFormId.ColumnName = "return_form_id";
                colvarReturnFormId.DataType = DbType.Int32;
                colvarReturnFormId.MaxLength = 0;
                colvarReturnFormId.AutoIncrement = false;
                colvarReturnFormId.IsNullable = false;
                colvarReturnFormId.IsPrimaryKey = false;
                colvarReturnFormId.IsForeignKey = false;
                colvarReturnFormId.IsReadOnly = false;

                schema.Columns.Add(colvarReturnFormId);

                TableSchema.TableColumn colvarCatgSeq = new TableSchema.TableColumn(schema);
                colvarCatgSeq.ColumnName = "catg_seq";
                colvarCatgSeq.DataType = DbType.Int32;
                colvarCatgSeq.MaxLength = 0;
                colvarCatgSeq.AutoIncrement = false;
                colvarCatgSeq.IsNullable = true;
                colvarCatgSeq.IsPrimaryKey = false;
                colvarCatgSeq.IsForeignKey = false;
                colvarCatgSeq.IsReadOnly = false;

                schema.Columns.Add(colvarCatgSeq);

                TableSchema.TableColumn colvarOptionName = new TableSchema.TableColumn(schema);
                colvarOptionName.ColumnName = "option_name";
                colvarOptionName.DataType = DbType.String;
                colvarOptionName.MaxLength = 50;
                colvarOptionName.AutoIncrement = false;
                colvarOptionName.IsNullable = true;
                colvarOptionName.IsPrimaryKey = false;
                colvarOptionName.IsForeignKey = false;
                colvarOptionName.IsReadOnly = false;

                schema.Columns.Add(colvarOptionName);

                TableSchema.TableColumn colvarOrderProductId = new TableSchema.TableColumn(schema);
                colvarOrderProductId.ColumnName = "order_product_id";
                colvarOrderProductId.DataType = DbType.Int32;
                colvarOrderProductId.MaxLength = 0;
                colvarOrderProductId.AutoIncrement = false;
                colvarOrderProductId.IsNullable = false;
                colvarOrderProductId.IsPrimaryKey = false;
                colvarOrderProductId.IsForeignKey = false;
                colvarOrderProductId.IsReadOnly = false;

                schema.Columns.Add(colvarOrderProductId);

                TableSchema.TableColumn colvarIsCollected = new TableSchema.TableColumn(schema);
                colvarIsCollected.ColumnName = "is_collected";
                colvarIsCollected.DataType = DbType.Boolean;
                colvarIsCollected.MaxLength = 0;
                colvarIsCollected.AutoIncrement = false;
                colvarIsCollected.IsNullable = true;
                colvarIsCollected.IsPrimaryKey = false;
                colvarIsCollected.IsForeignKey = false;
                colvarIsCollected.IsReadOnly = false;

                schema.Columns.Add(colvarIsCollected);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_return_form_product_option_list",schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewReturnFormProductOptionList()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewReturnFormProductOptionList(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewReturnFormProductOptionList(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewReturnFormProductOptionList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("ReturnFormId")]
        [Bindable(true)]
        public int ReturnFormId
        {
            get
            {
                return GetColumnValue<int>("return_form_id");
            }
            set
            {
                SetColumnValue("return_form_id", value);
            }
        }

        [XmlAttribute("CatgSeq")]
        [Bindable(true)]
        public int? CatgSeq
        {
            get
            {
                return GetColumnValue<int?>("catg_seq");
            }
            set
            {
                SetColumnValue("catg_seq", value);
            }
        }

        [XmlAttribute("OptionName")]
        [Bindable(true)]
        public string OptionName
        {
            get
            {
                return GetColumnValue<string>("option_name");
            }
            set
            {
                SetColumnValue("option_name", value);
            }
        }

        [XmlAttribute("OrderProductId")]
        [Bindable(true)]
        public int OrderProductId
        {
            get
            {
                return GetColumnValue<int>("order_product_id");
            }
            set
            {
                SetColumnValue("order_product_id", value);
            }
        }

        [XmlAttribute("IsCollected")]
        [Bindable(true)]
        public bool? IsCollected
        {
            get
            {
                return GetColumnValue<bool?>("is_collected");
            }
            set
            {
                SetColumnValue("is_collected", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string ReturnFormId = @"return_form_id";

            public static string CatgSeq = @"catg_seq";

            public static string OptionName = @"option_name";

            public static string OrderProductId = @"order_product_id";

            public static string IsCollected = @"is_collected";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

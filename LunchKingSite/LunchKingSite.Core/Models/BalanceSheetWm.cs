using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BalanceSheetWm class.
	/// </summary>
    [Serializable]
	public partial class BalanceSheetWmCollection : RepositoryList<BalanceSheetWm, BalanceSheetWmCollection>
	{	   
		public BalanceSheetWmCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BalanceSheetWmCollection</returns>
		public BalanceSheetWmCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BalanceSheetWm o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the balance_sheet_wms table.
	/// </summary>
	[Serializable]
	public partial class BalanceSheetWm : RepositoryRecord<BalanceSheetWm>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BalanceSheetWm()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BalanceSheetWm(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("balance_sheet_wms", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarYear = new TableSchema.TableColumn(schema);
				colvarYear.ColumnName = "year";
				colvarYear.DataType = DbType.Int32;
				colvarYear.MaxLength = 0;
				colvarYear.AutoIncrement = false;
				colvarYear.IsNullable = false;
				colvarYear.IsPrimaryKey = false;
				colvarYear.IsForeignKey = false;
				colvarYear.IsReadOnly = false;
				colvarYear.DefaultSetting = @"";
				colvarYear.ForeignKeyTableName = "";
				schema.Columns.Add(colvarYear);
				
				TableSchema.TableColumn colvarMonth = new TableSchema.TableColumn(schema);
				colvarMonth.ColumnName = "month";
				colvarMonth.DataType = DbType.Int32;
				colvarMonth.MaxLength = 0;
				colvarMonth.AutoIncrement = false;
				colvarMonth.IsNullable = false;
				colvarMonth.IsPrimaryKey = false;
				colvarMonth.IsForeignKey = false;
				colvarMonth.IsReadOnly = false;
				colvarMonth.DefaultSetting = @"";
				colvarMonth.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMonth);
				
				TableSchema.TableColumn colvarGenerationFrequency = new TableSchema.TableColumn(schema);
				colvarGenerationFrequency.ColumnName = "generation_frequency";
				colvarGenerationFrequency.DataType = DbType.Int32;
				colvarGenerationFrequency.MaxLength = 0;
				colvarGenerationFrequency.AutoIncrement = false;
				colvarGenerationFrequency.IsNullable = false;
				colvarGenerationFrequency.IsPrimaryKey = false;
				colvarGenerationFrequency.IsForeignKey = false;
				colvarGenerationFrequency.IsReadOnly = false;
				colvarGenerationFrequency.DefaultSetting = @"";
				colvarGenerationFrequency.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGenerationFrequency);
				
				TableSchema.TableColumn colvarIntervalStart = new TableSchema.TableColumn(schema);
				colvarIntervalStart.ColumnName = "interval_start";
				colvarIntervalStart.DataType = DbType.DateTime;
				colvarIntervalStart.MaxLength = 0;
				colvarIntervalStart.AutoIncrement = false;
				colvarIntervalStart.IsNullable = false;
				colvarIntervalStart.IsPrimaryKey = false;
				colvarIntervalStart.IsForeignKey = false;
				colvarIntervalStart.IsReadOnly = false;
				colvarIntervalStart.DefaultSetting = @"";
				colvarIntervalStart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntervalStart);
				
				TableSchema.TableColumn colvarIntervalEnd = new TableSchema.TableColumn(schema);
				colvarIntervalEnd.ColumnName = "interval_end";
				colvarIntervalEnd.DataType = DbType.DateTime;
				colvarIntervalEnd.MaxLength = 0;
				colvarIntervalEnd.AutoIncrement = false;
				colvarIntervalEnd.IsNullable = false;
				colvarIntervalEnd.IsPrimaryKey = false;
				colvarIntervalEnd.IsForeignKey = false;
				colvarIntervalEnd.IsReadOnly = false;
				colvarIntervalEnd.DefaultSetting = @"";
				colvarIntervalEnd.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntervalEnd);
				
				TableSchema.TableColumn colvarIsConfirmedReadyToPay = new TableSchema.TableColumn(schema);
				colvarIsConfirmedReadyToPay.ColumnName = "is_confirmed_ready_to_pay";
				colvarIsConfirmedReadyToPay.DataType = DbType.Boolean;
				colvarIsConfirmedReadyToPay.MaxLength = 0;
				colvarIsConfirmedReadyToPay.AutoIncrement = false;
				colvarIsConfirmedReadyToPay.IsNullable = false;
				colvarIsConfirmedReadyToPay.IsPrimaryKey = false;
				colvarIsConfirmedReadyToPay.IsForeignKey = false;
				colvarIsConfirmedReadyToPay.IsReadOnly = false;
				colvarIsConfirmedReadyToPay.DefaultSetting = @"";
				colvarIsConfirmedReadyToPay.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsConfirmedReadyToPay);
				
				TableSchema.TableColumn colvarIsReceiptReceived = new TableSchema.TableColumn(schema);
				colvarIsReceiptReceived.ColumnName = "is_receipt_received";
				colvarIsReceiptReceived.DataType = DbType.Boolean;
				colvarIsReceiptReceived.MaxLength = 0;
				colvarIsReceiptReceived.AutoIncrement = false;
				colvarIsReceiptReceived.IsNullable = false;
				colvarIsReceiptReceived.IsPrimaryKey = false;
				colvarIsReceiptReceived.IsForeignKey = false;
				colvarIsReceiptReceived.IsReadOnly = false;
				colvarIsReceiptReceived.DefaultSetting = @"";
				colvarIsReceiptReceived.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReceiptReceived);
				
				TableSchema.TableColumn colvarConfirmedTime = new TableSchema.TableColumn(schema);
				colvarConfirmedTime.ColumnName = "confirmed_time";
				colvarConfirmedTime.DataType = DbType.DateTime;
				colvarConfirmedTime.MaxLength = 0;
				colvarConfirmedTime.AutoIncrement = false;
				colvarConfirmedTime.IsNullable = true;
				colvarConfirmedTime.IsPrimaryKey = false;
				colvarConfirmedTime.IsForeignKey = false;
				colvarConfirmedTime.IsReadOnly = false;
				colvarConfirmedTime.DefaultSetting = @"";
				colvarConfirmedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConfirmedTime);
				
				TableSchema.TableColumn colvarConfirmedUserName = new TableSchema.TableColumn(schema);
				colvarConfirmedUserName.ColumnName = "confirmed_user_name";
				colvarConfirmedUserName.DataType = DbType.String;
				colvarConfirmedUserName.MaxLength = 256;
				colvarConfirmedUserName.AutoIncrement = false;
				colvarConfirmedUserName.IsNullable = true;
				colvarConfirmedUserName.IsPrimaryKey = false;
				colvarConfirmedUserName.IsForeignKey = false;
				colvarConfirmedUserName.IsReadOnly = false;
				colvarConfirmedUserName.DefaultSetting = @"";
				colvarConfirmedUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConfirmedUserName);
				
				TableSchema.TableColumn colvarPayReportId = new TableSchema.TableColumn(schema);
				colvarPayReportId.ColumnName = "pay_report_id";
				colvarPayReportId.DataType = DbType.Int32;
				colvarPayReportId.MaxLength = 0;
				colvarPayReportId.AutoIncrement = false;
				colvarPayReportId.IsNullable = true;
				colvarPayReportId.IsPrimaryKey = false;
				colvarPayReportId.IsForeignKey = false;
				colvarPayReportId.IsReadOnly = false;
				colvarPayReportId.DefaultSetting = @"";
				colvarPayReportId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayReportId);
				
				TableSchema.TableColumn colvarBalanceSheetType = new TableSchema.TableColumn(schema);
				colvarBalanceSheetType.ColumnName = "balance_sheet_type";
				colvarBalanceSheetType.DataType = DbType.Int32;
				colvarBalanceSheetType.MaxLength = 0;
				colvarBalanceSheetType.AutoIncrement = false;
				colvarBalanceSheetType.IsNullable = false;
				colvarBalanceSheetType.IsPrimaryKey = false;
				colvarBalanceSheetType.IsForeignKey = false;
				colvarBalanceSheetType.IsReadOnly = false;
				colvarBalanceSheetType.DefaultSetting = @"";
				colvarBalanceSheetType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBalanceSheetType);
				
				TableSchema.TableColumn colvarEstAmount = new TableSchema.TableColumn(schema);
				colvarEstAmount.ColumnName = "est_amount";
				colvarEstAmount.DataType = DbType.Int32;
				colvarEstAmount.MaxLength = 0;
				colvarEstAmount.AutoIncrement = false;
				colvarEstAmount.IsNullable = false;
				colvarEstAmount.IsPrimaryKey = false;
				colvarEstAmount.IsForeignKey = false;
				colvarEstAmount.IsReadOnly = false;
				colvarEstAmount.DefaultSetting = @"";
				colvarEstAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEstAmount);
				
				TableSchema.TableColumn colvarDefaultPaymentTime = new TableSchema.TableColumn(schema);
				colvarDefaultPaymentTime.ColumnName = "default_payment_time";
				colvarDefaultPaymentTime.DataType = DbType.DateTime;
				colvarDefaultPaymentTime.MaxLength = 0;
				colvarDefaultPaymentTime.AutoIncrement = false;
				colvarDefaultPaymentTime.IsNullable = true;
				colvarDefaultPaymentTime.IsPrimaryKey = false;
				colvarDefaultPaymentTime.IsForeignKey = false;
				colvarDefaultPaymentTime.IsReadOnly = false;
				colvarDefaultPaymentTime.DefaultSetting = @"";
				colvarDefaultPaymentTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDefaultPaymentTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("balance_sheet_wms",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("Year")]
		[Bindable(true)]
		public int Year 
		{
			get { return GetColumnValue<int>(Columns.Year); }
			set { SetColumnValue(Columns.Year, value); }
		}
		  
		[XmlAttribute("Month")]
		[Bindable(true)]
		public int Month 
		{
			get { return GetColumnValue<int>(Columns.Month); }
			set { SetColumnValue(Columns.Month, value); }
		}
		  
		[XmlAttribute("GenerationFrequency")]
		[Bindable(true)]
		public int GenerationFrequency 
		{
			get { return GetColumnValue<int>(Columns.GenerationFrequency); }
			set { SetColumnValue(Columns.GenerationFrequency, value); }
		}
		  
		[XmlAttribute("IntervalStart")]
		[Bindable(true)]
		public DateTime IntervalStart 
		{
			get { return GetColumnValue<DateTime>(Columns.IntervalStart); }
			set { SetColumnValue(Columns.IntervalStart, value); }
		}
		  
		[XmlAttribute("IntervalEnd")]
		[Bindable(true)]
		public DateTime IntervalEnd 
		{
			get { return GetColumnValue<DateTime>(Columns.IntervalEnd); }
			set { SetColumnValue(Columns.IntervalEnd, value); }
		}
		  
		[XmlAttribute("IsConfirmedReadyToPay")]
		[Bindable(true)]
		public bool IsConfirmedReadyToPay 
		{
			get { return GetColumnValue<bool>(Columns.IsConfirmedReadyToPay); }
			set { SetColumnValue(Columns.IsConfirmedReadyToPay, value); }
		}
		  
		[XmlAttribute("IsReceiptReceived")]
		[Bindable(true)]
		public bool IsReceiptReceived 
		{
			get { return GetColumnValue<bool>(Columns.IsReceiptReceived); }
			set { SetColumnValue(Columns.IsReceiptReceived, value); }
		}
		  
		[XmlAttribute("ConfirmedTime")]
		[Bindable(true)]
		public DateTime? ConfirmedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ConfirmedTime); }
			set { SetColumnValue(Columns.ConfirmedTime, value); }
		}
		  
		[XmlAttribute("ConfirmedUserName")]
		[Bindable(true)]
		public string ConfirmedUserName 
		{
			get { return GetColumnValue<string>(Columns.ConfirmedUserName); }
			set { SetColumnValue(Columns.ConfirmedUserName, value); }
		}
		  
		[XmlAttribute("PayReportId")]
		[Bindable(true)]
		public int? PayReportId 
		{
			get { return GetColumnValue<int?>(Columns.PayReportId); }
			set { SetColumnValue(Columns.PayReportId, value); }
		}
		  
		[XmlAttribute("BalanceSheetType")]
		[Bindable(true)]
		public int BalanceSheetType 
		{
			get { return GetColumnValue<int>(Columns.BalanceSheetType); }
			set { SetColumnValue(Columns.BalanceSheetType, value); }
		}
		  
		[XmlAttribute("EstAmount")]
		[Bindable(true)]
		public int EstAmount 
		{
			get { return GetColumnValue<int>(Columns.EstAmount); }
			set { SetColumnValue(Columns.EstAmount, value); }
		}
		  
		[XmlAttribute("DefaultPaymentTime")]
		[Bindable(true)]
		public DateTime? DefaultPaymentTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.DefaultPaymentTime); }
			set { SetColumnValue(Columns.DefaultPaymentTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn YearColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MonthColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn GenerationFrequencyColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IntervalStartColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IntervalEndColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IsConfirmedReadyToPayColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReceiptReceivedColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ConfirmedTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ConfirmedUserNameColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn PayReportIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn BalanceSheetTypeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn EstAmountColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn DefaultPaymentTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SellerGuid = @"seller_guid";
			 public static string Year = @"year";
			 public static string Month = @"month";
			 public static string GenerationFrequency = @"generation_frequency";
			 public static string IntervalStart = @"interval_start";
			 public static string IntervalEnd = @"interval_end";
			 public static string IsConfirmedReadyToPay = @"is_confirmed_ready_to_pay";
			 public static string IsReceiptReceived = @"is_receipt_received";
			 public static string ConfirmedTime = @"confirmed_time";
			 public static string ConfirmedUserName = @"confirmed_user_name";
			 public static string PayReportId = @"pay_report_id";
			 public static string BalanceSheetType = @"balance_sheet_type";
			 public static string EstAmount = @"est_amount";
			 public static string DefaultPaymentTime = @"default_payment_time";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the WeeklyPayReport class.
	/// </summary>
    [Serializable]
	public partial class WeeklyPayReportCollection : RepositoryList<WeeklyPayReport, WeeklyPayReportCollection>
	{	   
		public WeeklyPayReportCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>WeeklyPayReportCollection</returns>
		public WeeklyPayReportCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                WeeklyPayReport o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the weekly_pay_report table.
	/// </summary>
	[Serializable]
	public partial class WeeklyPayReport : RepositoryRecord<WeeklyPayReport>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public WeeklyPayReport()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public WeeklyPayReport(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("weekly_pay_report", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarIntervalStart = new TableSchema.TableColumn(schema);
				colvarIntervalStart.ColumnName = "interval_start";
				colvarIntervalStart.DataType = DbType.DateTime;
				colvarIntervalStart.MaxLength = 0;
				colvarIntervalStart.AutoIncrement = false;
				colvarIntervalStart.IsNullable = false;
				colvarIntervalStart.IsPrimaryKey = false;
				colvarIntervalStart.IsForeignKey = false;
				colvarIntervalStart.IsReadOnly = false;
				colvarIntervalStart.DefaultSetting = @"";
				colvarIntervalStart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntervalStart);
				
				TableSchema.TableColumn colvarIntervalEnd = new TableSchema.TableColumn(schema);
				colvarIntervalEnd.ColumnName = "interval_end";
				colvarIntervalEnd.DataType = DbType.DateTime;
				colvarIntervalEnd.MaxLength = 0;
				colvarIntervalEnd.AutoIncrement = false;
				colvarIntervalEnd.IsNullable = false;
				colvarIntervalEnd.IsPrimaryKey = false;
				colvarIntervalEnd.IsForeignKey = false;
				colvarIntervalEnd.IsReadOnly = false;
				colvarIntervalEnd.DefaultSetting = @"";
				colvarIntervalEnd.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntervalEnd);
				
				TableSchema.TableColumn colvarCreditCard = new TableSchema.TableColumn(schema);
				colvarCreditCard.ColumnName = "credit_card";
				colvarCreditCard.DataType = DbType.Int32;
				colvarCreditCard.MaxLength = 0;
				colvarCreditCard.AutoIncrement = false;
				colvarCreditCard.IsNullable = false;
				colvarCreditCard.IsPrimaryKey = false;
				colvarCreditCard.IsForeignKey = false;
				colvarCreditCard.IsReadOnly = false;
				
						colvarCreditCard.DefaultSetting = @"((0))";
				colvarCreditCard.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditCard);
				
				TableSchema.TableColumn colvarAtm = new TableSchema.TableColumn(schema);
				colvarAtm.ColumnName = "atm";
				colvarAtm.DataType = DbType.Int32;
				colvarAtm.MaxLength = 0;
				colvarAtm.AutoIncrement = false;
				colvarAtm.IsNullable = false;
				colvarAtm.IsPrimaryKey = false;
				colvarAtm.IsForeignKey = false;
				colvarAtm.IsReadOnly = false;
				
						colvarAtm.DefaultSetting = @"((0))";
				colvarAtm.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAtm);
				
				TableSchema.TableColumn colvarReportGuid = new TableSchema.TableColumn(schema);
				colvarReportGuid.ColumnName = "report_guid";
				colvarReportGuid.DataType = DbType.Guid;
				colvarReportGuid.MaxLength = 0;
				colvarReportGuid.AutoIncrement = false;
				colvarReportGuid.IsNullable = false;
				colvarReportGuid.IsPrimaryKey = false;
				colvarReportGuid.IsForeignKey = false;
				colvarReportGuid.IsReadOnly = false;
				colvarReportGuid.DefaultSetting = @"";
				colvarReportGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReportGuid);
				
				TableSchema.TableColumn colvarIsLastWeek = new TableSchema.TableColumn(schema);
				colvarIsLastWeek.ColumnName = "is_last_week";
				colvarIsLastWeek.DataType = DbType.Boolean;
				colvarIsLastWeek.MaxLength = 0;
				colvarIsLastWeek.AutoIncrement = false;
				colvarIsLastWeek.IsNullable = false;
				colvarIsLastWeek.IsPrimaryKey = false;
				colvarIsLastWeek.IsForeignKey = false;
				colvarIsLastWeek.IsReadOnly = false;
				
						colvarIsLastWeek.DefaultSetting = @"((0))";
				colvarIsLastWeek.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsLastWeek);
				
				TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
				colvarCost.ColumnName = "cost";
				colvarCost.DataType = DbType.Decimal;
				colvarCost.MaxLength = 0;
				colvarCost.AutoIncrement = false;
				colvarCost.IsNullable = false;
				colvarCost.IsPrimaryKey = false;
				colvarCost.IsForeignKey = false;
				colvarCost.IsReadOnly = false;
				
						colvarCost.DefaultSetting = @"((0))";
				colvarCost.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCost);
				
				TableSchema.TableColumn colvarTotalCount = new TableSchema.TableColumn(schema);
				colvarTotalCount.ColumnName = "total_count";
				colvarTotalCount.DataType = DbType.Int32;
				colvarTotalCount.MaxLength = 0;
				colvarTotalCount.AutoIncrement = false;
				colvarTotalCount.IsNullable = false;
				colvarTotalCount.IsPrimaryKey = false;
				colvarTotalCount.IsForeignKey = false;
				colvarTotalCount.IsReadOnly = false;
				
						colvarTotalCount.DefaultSetting = @"((0))";
				colvarTotalCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalCount);
				
				TableSchema.TableColumn colvarTotalSum = new TableSchema.TableColumn(schema);
				colvarTotalSum.ColumnName = "total_sum";
				colvarTotalSum.DataType = DbType.Decimal;
				colvarTotalSum.MaxLength = 0;
				colvarTotalSum.AutoIncrement = false;
				colvarTotalSum.IsNullable = false;
				colvarTotalSum.IsPrimaryKey = false;
				colvarTotalSum.IsForeignKey = false;
				colvarTotalSum.IsReadOnly = false;
				
						colvarTotalSum.DefaultSetting = @"((0))";
				colvarTotalSum.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalSum);
				
				TableSchema.TableColumn colvarErrorIn = new TableSchema.TableColumn(schema);
				colvarErrorIn.ColumnName = "error_in";
				colvarErrorIn.DataType = DbType.Decimal;
				colvarErrorIn.MaxLength = 0;
				colvarErrorIn.AutoIncrement = false;
				colvarErrorIn.IsNullable = false;
				colvarErrorIn.IsPrimaryKey = false;
				colvarErrorIn.IsForeignKey = false;
				colvarErrorIn.IsReadOnly = false;
				
						colvarErrorIn.DefaultSetting = @"((0))";
				colvarErrorIn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarErrorIn);
				
				TableSchema.TableColumn colvarErrorOut = new TableSchema.TableColumn(schema);
				colvarErrorOut.ColumnName = "error_out";
				colvarErrorOut.DataType = DbType.Decimal;
				colvarErrorOut.MaxLength = 0;
				colvarErrorOut.AutoIncrement = false;
				colvarErrorOut.IsNullable = false;
				colvarErrorOut.IsPrimaryKey = false;
				colvarErrorOut.IsForeignKey = false;
				colvarErrorOut.IsReadOnly = false;
				
						colvarErrorOut.DefaultSetting = @"((0))";
				colvarErrorOut.ForeignKeyTableName = "";
				schema.Columns.Add(colvarErrorOut);
				
				TableSchema.TableColumn colvarErrorInSum = new TableSchema.TableColumn(schema);
				colvarErrorInSum.ColumnName = "error_in_sum";
				colvarErrorInSum.DataType = DbType.Int32;
				colvarErrorInSum.MaxLength = 0;
				colvarErrorInSum.AutoIncrement = false;
				colvarErrorInSum.IsNullable = false;
				colvarErrorInSum.IsPrimaryKey = false;
				colvarErrorInSum.IsForeignKey = false;
				colvarErrorInSum.IsReadOnly = false;
				
						colvarErrorInSum.DefaultSetting = @"((0))";
				colvarErrorInSum.ForeignKeyTableName = "";
				schema.Columns.Add(colvarErrorInSum);
				
				TableSchema.TableColumn colvarErrorOutSum = new TableSchema.TableColumn(schema);
				colvarErrorOutSum.ColumnName = "error_out_sum";
				colvarErrorOutSum.DataType = DbType.Int32;
				colvarErrorOutSum.MaxLength = 0;
				colvarErrorOutSum.AutoIncrement = false;
				colvarErrorOutSum.IsNullable = false;
				colvarErrorOutSum.IsPrimaryKey = false;
				colvarErrorOutSum.IsForeignKey = false;
				colvarErrorOutSum.IsReadOnly = false;
				
						colvarErrorOutSum.DefaultSetting = @"((0))";
				colvarErrorOutSum.ForeignKeyTableName = "";
				schema.Columns.Add(colvarErrorOutSum);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.String;
				colvarUserId.MaxLength = 100;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = true;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarResult = new TableSchema.TableColumn(schema);
				colvarResult.ColumnName = "result";
				colvarResult.DataType = DbType.String;
				colvarResult.MaxLength = 10;
				colvarResult.AutoIncrement = false;
				colvarResult.IsNullable = true;
				colvarResult.IsPrimaryKey = false;
				colvarResult.IsForeignKey = false;
				colvarResult.IsReadOnly = false;
				colvarResult.DefaultSetting = @"";
				colvarResult.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResult);
				
				TableSchema.TableColumn colvarResponseTime = new TableSchema.TableColumn(schema);
				colvarResponseTime.ColumnName = "response_time";
				colvarResponseTime.DataType = DbType.DateTime;
				colvarResponseTime.MaxLength = 0;
				colvarResponseTime.AutoIncrement = false;
				colvarResponseTime.IsNullable = true;
				colvarResponseTime.IsPrimaryKey = false;
				colvarResponseTime.IsForeignKey = false;
				colvarResponseTime.IsReadOnly = false;
				colvarResponseTime.DefaultSetting = @"";
				colvarResponseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResponseTime);
				
				TableSchema.TableColumn colvarFundTransferType = new TableSchema.TableColumn(schema);
				colvarFundTransferType.ColumnName = "fund_transfer_type";
				colvarFundTransferType.DataType = DbType.Int32;
				colvarFundTransferType.MaxLength = 0;
				colvarFundTransferType.AutoIncrement = false;
				colvarFundTransferType.IsNullable = false;
				colvarFundTransferType.IsPrimaryKey = false;
				colvarFundTransferType.IsForeignKey = false;
				colvarFundTransferType.IsReadOnly = false;
				
						colvarFundTransferType.DefaultSetting = @"((1))";
				colvarFundTransferType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFundTransferType);
				
				TableSchema.TableColumn colvarTransferAmount = new TableSchema.TableColumn(schema);
				colvarTransferAmount.ColumnName = "transfer_amount";
				colvarTransferAmount.DataType = DbType.Decimal;
				colvarTransferAmount.MaxLength = 0;
				colvarTransferAmount.AutoIncrement = false;
				colvarTransferAmount.IsNullable = true;
				colvarTransferAmount.IsPrimaryKey = false;
				colvarTransferAmount.IsForeignKey = false;
				colvarTransferAmount.IsReadOnly = false;
				colvarTransferAmount.DefaultSetting = @"";
				colvarTransferAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransferAmount);
				
				TableSchema.TableColumn colvarReceiverBankNo = new TableSchema.TableColumn(schema);
				colvarReceiverBankNo.ColumnName = "receiver_bank_no";
				colvarReceiverBankNo.DataType = DbType.AnsiString;
				colvarReceiverBankNo.MaxLength = 3;
				colvarReceiverBankNo.AutoIncrement = false;
				colvarReceiverBankNo.IsNullable = true;
				colvarReceiverBankNo.IsPrimaryKey = false;
				colvarReceiverBankNo.IsForeignKey = false;
				colvarReceiverBankNo.IsReadOnly = false;
				colvarReceiverBankNo.DefaultSetting = @"";
				colvarReceiverBankNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverBankNo);
				
				TableSchema.TableColumn colvarReceiverBranchNo = new TableSchema.TableColumn(schema);
				colvarReceiverBranchNo.ColumnName = "receiver_branch_no";
				colvarReceiverBranchNo.DataType = DbType.AnsiString;
				colvarReceiverBranchNo.MaxLength = 4;
				colvarReceiverBranchNo.AutoIncrement = false;
				colvarReceiverBranchNo.IsNullable = true;
				colvarReceiverBranchNo.IsPrimaryKey = false;
				colvarReceiverBranchNo.IsForeignKey = false;
				colvarReceiverBranchNo.IsReadOnly = false;
				colvarReceiverBranchNo.DefaultSetting = @"";
				colvarReceiverBranchNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverBranchNo);
				
				TableSchema.TableColumn colvarReceiverAccountNo = new TableSchema.TableColumn(schema);
				colvarReceiverAccountNo.ColumnName = "receiver_account_no";
				colvarReceiverAccountNo.DataType = DbType.AnsiString;
				colvarReceiverAccountNo.MaxLength = 14;
				colvarReceiverAccountNo.AutoIncrement = false;
				colvarReceiverAccountNo.IsNullable = true;
				colvarReceiverAccountNo.IsPrimaryKey = false;
				colvarReceiverAccountNo.IsForeignKey = false;
				colvarReceiverAccountNo.IsReadOnly = false;
				colvarReceiverAccountNo.DefaultSetting = @"";
				colvarReceiverAccountNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverAccountNo);
				
				TableSchema.TableColumn colvarReceiverCompanyId = new TableSchema.TableColumn(schema);
				colvarReceiverCompanyId.ColumnName = "receiver_company_id";
				colvarReceiverCompanyId.DataType = DbType.AnsiString;
				colvarReceiverCompanyId.MaxLength = 10;
				colvarReceiverCompanyId.AutoIncrement = false;
				colvarReceiverCompanyId.IsNullable = true;
				colvarReceiverCompanyId.IsPrimaryKey = false;
				colvarReceiverCompanyId.IsForeignKey = false;
				colvarReceiverCompanyId.IsReadOnly = false;
				colvarReceiverCompanyId.DefaultSetting = @"";
				colvarReceiverCompanyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverCompanyId);
				
				TableSchema.TableColumn colvarUntransferredPayId = new TableSchema.TableColumn(schema);
				colvarUntransferredPayId.ColumnName = "untransferred_pay_id";
				colvarUntransferredPayId.DataType = DbType.Int32;
				colvarUntransferredPayId.MaxLength = 0;
				colvarUntransferredPayId.AutoIncrement = false;
				colvarUntransferredPayId.IsNullable = true;
				colvarUntransferredPayId.IsPrimaryKey = false;
				colvarUntransferredPayId.IsForeignKey = false;
				colvarUntransferredPayId.IsReadOnly = false;
				colvarUntransferredPayId.DefaultSetting = @"";
				colvarUntransferredPayId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUntransferredPayId);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 200;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = true;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarReceiverTitle = new TableSchema.TableColumn(schema);
				colvarReceiverTitle.ColumnName = "receiver_title";
				colvarReceiverTitle.DataType = DbType.String;
				colvarReceiverTitle.MaxLength = 50;
				colvarReceiverTitle.AutoIncrement = false;
				colvarReceiverTitle.IsNullable = true;
				colvarReceiverTitle.IsPrimaryKey = false;
				colvarReceiverTitle.IsForeignKey = false;
				colvarReceiverTitle.IsReadOnly = false;
				colvarReceiverTitle.DefaultSetting = @"";
				colvarReceiverTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverTitle);
				
				TableSchema.TableColumn colvarIsTransferComplete = new TableSchema.TableColumn(schema);
				colvarIsTransferComplete.ColumnName = "is_transfer_complete";
				colvarIsTransferComplete.DataType = DbType.Boolean;
				colvarIsTransferComplete.MaxLength = 0;
				colvarIsTransferComplete.AutoIncrement = false;
				colvarIsTransferComplete.IsNullable = false;
				colvarIsTransferComplete.IsPrimaryKey = false;
				colvarIsTransferComplete.IsForeignKey = false;
				colvarIsTransferComplete.IsReadOnly = false;
				
						colvarIsTransferComplete.DefaultSetting = @"((0))";
				colvarIsTransferComplete.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTransferComplete);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("weekly_pay_report",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("IntervalStart")]
		[Bindable(true)]
		public DateTime IntervalStart 
		{
			get { return GetColumnValue<DateTime>(Columns.IntervalStart); }
			set { SetColumnValue(Columns.IntervalStart, value); }
		}
		  
		[XmlAttribute("IntervalEnd")]
		[Bindable(true)]
		public DateTime IntervalEnd 
		{
			get { return GetColumnValue<DateTime>(Columns.IntervalEnd); }
			set { SetColumnValue(Columns.IntervalEnd, value); }
		}
		  
		[XmlAttribute("CreditCard")]
		[Bindable(true)]
		public int CreditCard 
		{
			get { return GetColumnValue<int>(Columns.CreditCard); }
			set { SetColumnValue(Columns.CreditCard, value); }
		}
		  
		[XmlAttribute("Atm")]
		[Bindable(true)]
		public int Atm 
		{
			get { return GetColumnValue<int>(Columns.Atm); }
			set { SetColumnValue(Columns.Atm, value); }
		}
		  
		[XmlAttribute("ReportGuid")]
		[Bindable(true)]
		public Guid ReportGuid 
		{
			get { return GetColumnValue<Guid>(Columns.ReportGuid); }
			set { SetColumnValue(Columns.ReportGuid, value); }
		}
		  
		[XmlAttribute("IsLastWeek")]
		[Bindable(true)]
		public bool IsLastWeek 
		{
			get { return GetColumnValue<bool>(Columns.IsLastWeek); }
			set { SetColumnValue(Columns.IsLastWeek, value); }
		}
		  
		[XmlAttribute("Cost")]
		[Bindable(true)]
		public decimal Cost 
		{
			get { return GetColumnValue<decimal>(Columns.Cost); }
			set { SetColumnValue(Columns.Cost, value); }
		}
		  
		[XmlAttribute("TotalCount")]
		[Bindable(true)]
		public int TotalCount 
		{
			get { return GetColumnValue<int>(Columns.TotalCount); }
			set { SetColumnValue(Columns.TotalCount, value); }
		}
		  
		[XmlAttribute("TotalSum")]
		[Bindable(true)]
		public decimal TotalSum 
		{
			get { return GetColumnValue<decimal>(Columns.TotalSum); }
			set { SetColumnValue(Columns.TotalSum, value); }
		}
		  
		[XmlAttribute("ErrorIn")]
		[Bindable(true)]
		public decimal ErrorIn 
		{
			get { return GetColumnValue<decimal>(Columns.ErrorIn); }
			set { SetColumnValue(Columns.ErrorIn, value); }
		}
		  
		[XmlAttribute("ErrorOut")]
		[Bindable(true)]
		public decimal ErrorOut 
		{
			get { return GetColumnValue<decimal>(Columns.ErrorOut); }
			set { SetColumnValue(Columns.ErrorOut, value); }
		}
		  
		[XmlAttribute("ErrorInSum")]
		[Bindable(true)]
		public int ErrorInSum 
		{
			get { return GetColumnValue<int>(Columns.ErrorInSum); }
			set { SetColumnValue(Columns.ErrorInSum, value); }
		}
		  
		[XmlAttribute("ErrorOutSum")]
		[Bindable(true)]
		public int ErrorOutSum 
		{
			get { return GetColumnValue<int>(Columns.ErrorOutSum); }
			set { SetColumnValue(Columns.ErrorOutSum, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public string UserId 
		{
			get { return GetColumnValue<string>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid? StoreGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("Result")]
		[Bindable(true)]
		public string Result 
		{
			get { return GetColumnValue<string>(Columns.Result); }
			set { SetColumnValue(Columns.Result, value); }
		}
		  
		[XmlAttribute("ResponseTime")]
		[Bindable(true)]
		public DateTime? ResponseTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ResponseTime); }
			set { SetColumnValue(Columns.ResponseTime, value); }
		}
		  
		[XmlAttribute("FundTransferType")]
		[Bindable(true)]
		public int FundTransferType 
		{
			get { return GetColumnValue<int>(Columns.FundTransferType); }
			set { SetColumnValue(Columns.FundTransferType, value); }
		}
		  
		[XmlAttribute("TransferAmount")]
		[Bindable(true)]
		public decimal? TransferAmount 
		{
			get { return GetColumnValue<decimal?>(Columns.TransferAmount); }
			set { SetColumnValue(Columns.TransferAmount, value); }
		}
		  
		[XmlAttribute("ReceiverBankNo")]
		[Bindable(true)]
		public string ReceiverBankNo 
		{
			get { return GetColumnValue<string>(Columns.ReceiverBankNo); }
			set { SetColumnValue(Columns.ReceiverBankNo, value); }
		}
		  
		[XmlAttribute("ReceiverBranchNo")]
		[Bindable(true)]
		public string ReceiverBranchNo 
		{
			get { return GetColumnValue<string>(Columns.ReceiverBranchNo); }
			set { SetColumnValue(Columns.ReceiverBranchNo, value); }
		}
		  
		[XmlAttribute("ReceiverAccountNo")]
		[Bindable(true)]
		public string ReceiverAccountNo 
		{
			get { return GetColumnValue<string>(Columns.ReceiverAccountNo); }
			set { SetColumnValue(Columns.ReceiverAccountNo, value); }
		}
		  
		[XmlAttribute("ReceiverCompanyId")]
		[Bindable(true)]
		public string ReceiverCompanyId 
		{
			get { return GetColumnValue<string>(Columns.ReceiverCompanyId); }
			set { SetColumnValue(Columns.ReceiverCompanyId, value); }
		}
		  
		[XmlAttribute("UntransferredPayId")]
		[Bindable(true)]
		public int? UntransferredPayId 
		{
			get { return GetColumnValue<int?>(Columns.UntransferredPayId); }
			set { SetColumnValue(Columns.UntransferredPayId, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("ReceiverTitle")]
		[Bindable(true)]
		public string ReceiverTitle 
		{
			get { return GetColumnValue<string>(Columns.ReceiverTitle); }
			set { SetColumnValue(Columns.ReceiverTitle, value); }
		}
		  
		[XmlAttribute("IsTransferComplete")]
		[Bindable(true)]
		public bool IsTransferComplete 
		{
			get { return GetColumnValue<bool>(Columns.IsTransferComplete); }
			set { SetColumnValue(Columns.IsTransferComplete, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IntervalStartColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IntervalEndColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreditCardColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AtmColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ReportGuidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IsLastWeekColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CostColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalCountColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalSumColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ErrorInColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ErrorOutColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ErrorInSumColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ErrorOutSumColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ResultColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn ResponseTimeColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn FundTransferTypeColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn TransferAmountColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverBankNoColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverBranchNoColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverAccountNoColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverCompanyIdColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn UntransferredPayIdColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverTitleColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn IsTransferCompleteColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string IntervalStart = @"interval_start";
			 public static string IntervalEnd = @"interval_end";
			 public static string CreditCard = @"credit_card";
			 public static string Atm = @"atm";
			 public static string ReportGuid = @"report_guid";
			 public static string IsLastWeek = @"is_last_week";
			 public static string Cost = @"cost";
			 public static string TotalCount = @"total_count";
			 public static string TotalSum = @"total_sum";
			 public static string ErrorIn = @"error_in";
			 public static string ErrorOut = @"error_out";
			 public static string ErrorInSum = @"error_in_sum";
			 public static string ErrorOutSum = @"error_out_sum";
			 public static string UserId = @"user_id";
			 public static string CreateTime = @"create_time";
			 public static string StoreGuid = @"store_guid";
			 public static string Result = @"result";
			 public static string ResponseTime = @"response_time";
			 public static string FundTransferType = @"fund_transfer_type";
			 public static string TransferAmount = @"transfer_amount";
			 public static string ReceiverBankNo = @"receiver_bank_no";
			 public static string ReceiverBranchNo = @"receiver_branch_no";
			 public static string ReceiverAccountNo = @"receiver_account_no";
			 public static string ReceiverCompanyId = @"receiver_company_id";
			 public static string UntransferredPayId = @"untransferred_pay_id";
			 public static string Memo = @"memo";
			 public static string ReceiverTitle = @"receiver_title";
			 public static string IsTransferComplete = @"is_transfer_complete";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

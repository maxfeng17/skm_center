using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Seller class.
	/// </summary>
    [Serializable]
	public partial class SellerCollection : RepositoryList<Seller, SellerCollection>
	{	   
		public SellerCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SellerCollection</returns>
		public SellerCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Seller o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the seller table.
	/// </summary>
	[Serializable]
	public partial class Seller : RepositoryRecord<Seller>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Seller()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Seller(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("seller", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
				colvarSellerId.ColumnName = "seller_id";
				colvarSellerId.DataType = DbType.AnsiString;
				colvarSellerId.MaxLength = 20;
				colvarSellerId.AutoIncrement = false;
				colvarSellerId.IsNullable = true;
				colvarSellerId.IsPrimaryKey = false;
				colvarSellerId.IsForeignKey = false;
				colvarSellerId.IsReadOnly = false;
				colvarSellerId.DefaultSetting = @"";
				colvarSellerId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerId);
				
				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 100;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);
				
				TableSchema.TableColumn colvarSellerBossName = new TableSchema.TableColumn(schema);
				colvarSellerBossName.ColumnName = "seller_boss_name";
				colvarSellerBossName.DataType = DbType.String;
				colvarSellerBossName.MaxLength = 30;
				colvarSellerBossName.AutoIncrement = false;
				colvarSellerBossName.IsNullable = true;
				colvarSellerBossName.IsPrimaryKey = false;
				colvarSellerBossName.IsForeignKey = false;
				colvarSellerBossName.IsReadOnly = false;
				colvarSellerBossName.DefaultSetting = @"";
				colvarSellerBossName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerBossName);
				
				TableSchema.TableColumn colvarSellerTel = new TableSchema.TableColumn(schema);
				colvarSellerTel.ColumnName = "seller_tel";
				colvarSellerTel.DataType = DbType.AnsiString;
				colvarSellerTel.MaxLength = 100;
				colvarSellerTel.AutoIncrement = false;
				colvarSellerTel.IsNullable = true;
				colvarSellerTel.IsPrimaryKey = false;
				colvarSellerTel.IsForeignKey = false;
				colvarSellerTel.IsReadOnly = false;
				colvarSellerTel.DefaultSetting = @"";
				colvarSellerTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerTel);
				
				TableSchema.TableColumn colvarSellerTel2 = new TableSchema.TableColumn(schema);
				colvarSellerTel2.ColumnName = "seller_tel2";
				colvarSellerTel2.DataType = DbType.AnsiString;
				colvarSellerTel2.MaxLength = 100;
				colvarSellerTel2.AutoIncrement = false;
				colvarSellerTel2.IsNullable = true;
				colvarSellerTel2.IsPrimaryKey = false;
				colvarSellerTel2.IsForeignKey = false;
				colvarSellerTel2.IsReadOnly = false;
				colvarSellerTel2.DefaultSetting = @"";
				colvarSellerTel2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerTel2);
				
				TableSchema.TableColumn colvarSellerFax = new TableSchema.TableColumn(schema);
				colvarSellerFax.ColumnName = "seller_fax";
				colvarSellerFax.DataType = DbType.AnsiString;
				colvarSellerFax.MaxLength = 20;
				colvarSellerFax.AutoIncrement = false;
				colvarSellerFax.IsNullable = true;
				colvarSellerFax.IsPrimaryKey = false;
				colvarSellerFax.IsForeignKey = false;
				colvarSellerFax.IsReadOnly = false;
				colvarSellerFax.DefaultSetting = @"";
				colvarSellerFax.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerFax);
				
				TableSchema.TableColumn colvarSellerMobile = new TableSchema.TableColumn(schema);
				colvarSellerMobile.ColumnName = "seller_mobile";
				colvarSellerMobile.DataType = DbType.AnsiString;
				colvarSellerMobile.MaxLength = 100;
				colvarSellerMobile.AutoIncrement = false;
				colvarSellerMobile.IsNullable = true;
				colvarSellerMobile.IsPrimaryKey = false;
				colvarSellerMobile.IsForeignKey = false;
				colvarSellerMobile.IsReadOnly = false;
				colvarSellerMobile.DefaultSetting = @"";
				colvarSellerMobile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerMobile);
				
				TableSchema.TableColumn colvarSellerAddress = new TableSchema.TableColumn(schema);
				colvarSellerAddress.ColumnName = "seller_address";
				colvarSellerAddress.DataType = DbType.String;
				colvarSellerAddress.MaxLength = 100;
				colvarSellerAddress.AutoIncrement = false;
				colvarSellerAddress.IsNullable = true;
				colvarSellerAddress.IsPrimaryKey = false;
				colvarSellerAddress.IsForeignKey = false;
				colvarSellerAddress.IsReadOnly = false;
				colvarSellerAddress.DefaultSetting = @"";
				colvarSellerAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerAddress);
				
				TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
				colvarSellerEmail.ColumnName = "seller_email";
				colvarSellerEmail.DataType = DbType.String;
				colvarSellerEmail.MaxLength = 200;
				colvarSellerEmail.AutoIncrement = false;
				colvarSellerEmail.IsNullable = true;
				colvarSellerEmail.IsPrimaryKey = false;
				colvarSellerEmail.IsForeignKey = false;
				colvarSellerEmail.IsReadOnly = false;
				colvarSellerEmail.DefaultSetting = @"";
				colvarSellerEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerEmail);
				
				TableSchema.TableColumn colvarSellerBlog = new TableSchema.TableColumn(schema);
				colvarSellerBlog.ColumnName = "seller_blog";
				colvarSellerBlog.DataType = DbType.AnsiString;
				colvarSellerBlog.MaxLength = 100;
				colvarSellerBlog.AutoIncrement = false;
				colvarSellerBlog.IsNullable = true;
				colvarSellerBlog.IsPrimaryKey = false;
				colvarSellerBlog.IsForeignKey = false;
				colvarSellerBlog.IsReadOnly = false;
				colvarSellerBlog.DefaultSetting = @"";
				colvarSellerBlog.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerBlog);
				
				TableSchema.TableColumn colvarSellerInvoice = new TableSchema.TableColumn(schema);
				colvarSellerInvoice.ColumnName = "seller_invoice";
				colvarSellerInvoice.DataType = DbType.String;
				colvarSellerInvoice.MaxLength = 50;
				colvarSellerInvoice.AutoIncrement = false;
				colvarSellerInvoice.IsNullable = true;
				colvarSellerInvoice.IsPrimaryKey = false;
				colvarSellerInvoice.IsForeignKey = false;
				colvarSellerInvoice.IsReadOnly = false;
				colvarSellerInvoice.DefaultSetting = @"";
				colvarSellerInvoice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerInvoice);
				
				TableSchema.TableColumn colvarSellerDescription = new TableSchema.TableColumn(schema);
				colvarSellerDescription.ColumnName = "seller_description";
				colvarSellerDescription.DataType = DbType.String;
				colvarSellerDescription.MaxLength = 1073741823;
				colvarSellerDescription.AutoIncrement = false;
				colvarSellerDescription.IsNullable = true;
				colvarSellerDescription.IsPrimaryKey = false;
				colvarSellerDescription.IsForeignKey = false;
				colvarSellerDescription.IsReadOnly = false;
				colvarSellerDescription.DefaultSetting = @"";
				colvarSellerDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerDescription);
				
				TableSchema.TableColumn colvarSellerStatus = new TableSchema.TableColumn(schema);
				colvarSellerStatus.ColumnName = "seller_status";
				colvarSellerStatus.DataType = DbType.Int32;
				colvarSellerStatus.MaxLength = 0;
				colvarSellerStatus.AutoIncrement = false;
				colvarSellerStatus.IsNullable = false;
				colvarSellerStatus.IsPrimaryKey = false;
				colvarSellerStatus.IsForeignKey = false;
				colvarSellerStatus.IsReadOnly = false;
				
						colvarSellerStatus.DefaultSetting = @"((0))";
				colvarSellerStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerStatus);
				
				TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
				colvarDepartment.ColumnName = "department";
				colvarDepartment.DataType = DbType.Int32;
				colvarDepartment.MaxLength = 0;
				colvarDepartment.AutoIncrement = false;
				colvarDepartment.IsNullable = false;
				colvarDepartment.IsPrimaryKey = false;
				colvarDepartment.IsForeignKey = false;
				colvarDepartment.IsReadOnly = false;
				
						colvarDepartment.DefaultSetting = @"((0))";
				colvarDepartment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDepartment);
				
				TableSchema.TableColumn colvarSellerRemark = new TableSchema.TableColumn(schema);
				colvarSellerRemark.ColumnName = "seller_remark";
				colvarSellerRemark.DataType = DbType.String;
				colvarSellerRemark.MaxLength = 200;
				colvarSellerRemark.AutoIncrement = false;
				colvarSellerRemark.IsNullable = true;
				colvarSellerRemark.IsPrimaryKey = false;
				colvarSellerRemark.IsForeignKey = false;
				colvarSellerRemark.IsReadOnly = false;
				colvarSellerRemark.DefaultSetting = @"";
				colvarSellerRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerRemark);
				
				TableSchema.TableColumn colvarSellerSales = new TableSchema.TableColumn(schema);
				colvarSellerSales.ColumnName = "seller_sales";
				colvarSellerSales.DataType = DbType.String;
				colvarSellerSales.MaxLength = 50;
				colvarSellerSales.AutoIncrement = false;
				colvarSellerSales.IsNullable = true;
				colvarSellerSales.IsPrimaryKey = false;
				colvarSellerSales.IsForeignKey = false;
				colvarSellerSales.IsReadOnly = false;
				colvarSellerSales.DefaultSetting = @"";
				colvarSellerSales.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerSales);
				
				TableSchema.TableColumn colvarSellerLogoimgPath = new TableSchema.TableColumn(schema);
				colvarSellerLogoimgPath.ColumnName = "seller_logoimg_path";
				colvarSellerLogoimgPath.DataType = DbType.AnsiString;
				colvarSellerLogoimgPath.MaxLength = 500;
				colvarSellerLogoimgPath.AutoIncrement = false;
				colvarSellerLogoimgPath.IsNullable = true;
				colvarSellerLogoimgPath.IsPrimaryKey = false;
				colvarSellerLogoimgPath.IsForeignKey = false;
				colvarSellerLogoimgPath.IsReadOnly = false;
				colvarSellerLogoimgPath.DefaultSetting = @"";
				colvarSellerLogoimgPath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerLogoimgPath);
				
				TableSchema.TableColumn colvarSellerVideoPath = new TableSchema.TableColumn(schema);
				colvarSellerVideoPath.ColumnName = "seller_video_path";
				colvarSellerVideoPath.DataType = DbType.AnsiString;
				colvarSellerVideoPath.MaxLength = 100;
				colvarSellerVideoPath.AutoIncrement = false;
				colvarSellerVideoPath.IsNullable = true;
				colvarSellerVideoPath.IsPrimaryKey = false;
				colvarSellerVideoPath.IsForeignKey = false;
				colvarSellerVideoPath.IsReadOnly = false;
				colvarSellerVideoPath.DefaultSetting = @"";
				colvarSellerVideoPath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerVideoPath);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 30;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarDefaultCommissionRate = new TableSchema.TableColumn(schema);
				colvarDefaultCommissionRate.ColumnName = "default_commission_rate";
				colvarDefaultCommissionRate.DataType = DbType.Double;
				colvarDefaultCommissionRate.MaxLength = 0;
				colvarDefaultCommissionRate.AutoIncrement = false;
				colvarDefaultCommissionRate.IsNullable = true;
				colvarDefaultCommissionRate.IsPrimaryKey = false;
				colvarDefaultCommissionRate.IsForeignKey = false;
				colvarDefaultCommissionRate.IsReadOnly = false;
				colvarDefaultCommissionRate.DefaultSetting = @"";
				colvarDefaultCommissionRate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDefaultCommissionRate);
				
				TableSchema.TableColumn colvarBillingCode = new TableSchema.TableColumn(schema);
				colvarBillingCode.ColumnName = "billing_code";
				colvarBillingCode.DataType = DbType.String;
				colvarBillingCode.MaxLength = 50;
				colvarBillingCode.AutoIncrement = false;
				colvarBillingCode.IsNullable = true;
				colvarBillingCode.IsPrimaryKey = false;
				colvarBillingCode.IsForeignKey = false;
				colvarBillingCode.IsReadOnly = false;
				colvarBillingCode.DefaultSetting = @"";
				colvarBillingCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBillingCode);
				
				TableSchema.TableColumn colvarWeight = new TableSchema.TableColumn(schema);
				colvarWeight.ColumnName = "weight";
				colvarWeight.DataType = DbType.Int32;
				colvarWeight.MaxLength = 0;
				colvarWeight.AutoIncrement = false;
				colvarWeight.IsNullable = true;
				colvarWeight.IsPrimaryKey = false;
				colvarWeight.IsForeignKey = false;
				colvarWeight.IsReadOnly = false;
				colvarWeight.DefaultSetting = @"";
				colvarWeight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWeight);
				
				TableSchema.TableColumn colvarPostCkoutAction = new TableSchema.TableColumn(schema);
				colvarPostCkoutAction.ColumnName = "post_ckout_action";
				colvarPostCkoutAction.DataType = DbType.Int32;
				colvarPostCkoutAction.MaxLength = 0;
				colvarPostCkoutAction.AutoIncrement = false;
				colvarPostCkoutAction.IsNullable = false;
				colvarPostCkoutAction.IsPrimaryKey = false;
				colvarPostCkoutAction.IsForeignKey = false;
				colvarPostCkoutAction.IsReadOnly = false;
				
						colvarPostCkoutAction.DefaultSetting = @"((0))";
				colvarPostCkoutAction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPostCkoutAction);
				
				TableSchema.TableColumn colvarPostCkoutArgs = new TableSchema.TableColumn(schema);
				colvarPostCkoutArgs.ColumnName = "post_ckout_args";
				colvarPostCkoutArgs.DataType = DbType.String;
				colvarPostCkoutArgs.MaxLength = 150;
				colvarPostCkoutArgs.AutoIncrement = false;
				colvarPostCkoutArgs.IsNullable = true;
				colvarPostCkoutArgs.IsPrimaryKey = false;
				colvarPostCkoutArgs.IsForeignKey = false;
				colvarPostCkoutArgs.IsReadOnly = false;
				colvarPostCkoutArgs.DefaultSetting = @"";
				colvarPostCkoutArgs.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPostCkoutArgs);
				
				TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
				colvarCoordinate.ColumnName = "coordinate";
				colvarCoordinate.DataType = DbType.AnsiString;
				colvarCoordinate.MaxLength = -1;
				colvarCoordinate.AutoIncrement = false;
				colvarCoordinate.IsNullable = true;
				colvarCoordinate.IsPrimaryKey = false;
				colvarCoordinate.IsForeignKey = false;
				colvarCoordinate.IsReadOnly = false;
				colvarCoordinate.DefaultSetting = @"";
				colvarCoordinate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCoordinate);
				
				TableSchema.TableColumn colvarDeliveryMinuteGap = new TableSchema.TableColumn(schema);
				colvarDeliveryMinuteGap.ColumnName = "delivery_minute_gap";
				colvarDeliveryMinuteGap.DataType = DbType.Int32;
				colvarDeliveryMinuteGap.MaxLength = 0;
				colvarDeliveryMinuteGap.AutoIncrement = false;
				colvarDeliveryMinuteGap.IsNullable = true;
				colvarDeliveryMinuteGap.IsPrimaryKey = false;
				colvarDeliveryMinuteGap.IsForeignKey = false;
				colvarDeliveryMinuteGap.IsReadOnly = false;
				colvarDeliveryMinuteGap.DefaultSetting = @"";
				colvarDeliveryMinuteGap.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryMinuteGap);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = false;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = true;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "city";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
				colvarCompanyName.ColumnName = "CompanyName";
				colvarCompanyName.DataType = DbType.String;
				colvarCompanyName.MaxLength = 50;
				colvarCompanyName.AutoIncrement = false;
				colvarCompanyName.IsNullable = true;
				colvarCompanyName.IsPrimaryKey = false;
				colvarCompanyName.IsForeignKey = false;
				colvarCompanyName.IsReadOnly = false;
				colvarCompanyName.DefaultSetting = @"";
				colvarCompanyName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyName);
				
				TableSchema.TableColumn colvarCompanyBossName = new TableSchema.TableColumn(schema);
				colvarCompanyBossName.ColumnName = "CompanyBossName";
				colvarCompanyBossName.DataType = DbType.String;
				colvarCompanyBossName.MaxLength = 30;
				colvarCompanyBossName.AutoIncrement = false;
				colvarCompanyBossName.IsNullable = true;
				colvarCompanyBossName.IsPrimaryKey = false;
				colvarCompanyBossName.IsForeignKey = false;
				colvarCompanyBossName.IsReadOnly = false;
				colvarCompanyBossName.DefaultSetting = @"";
				colvarCompanyBossName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyBossName);
				
				TableSchema.TableColumn colvarCompanyID = new TableSchema.TableColumn(schema);
				colvarCompanyID.ColumnName = "CompanyID";
				colvarCompanyID.DataType = DbType.AnsiString;
				colvarCompanyID.MaxLength = 20;
				colvarCompanyID.AutoIncrement = false;
				colvarCompanyID.IsNullable = true;
				colvarCompanyID.IsPrimaryKey = false;
				colvarCompanyID.IsForeignKey = false;
				colvarCompanyID.IsReadOnly = false;
				colvarCompanyID.DefaultSetting = @"";
				colvarCompanyID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyID);
				
				TableSchema.TableColumn colvarCompanyBankCode = new TableSchema.TableColumn(schema);
				colvarCompanyBankCode.ColumnName = "CompanyBankCode";
				colvarCompanyBankCode.DataType = DbType.AnsiString;
				colvarCompanyBankCode.MaxLength = 10;
				colvarCompanyBankCode.AutoIncrement = false;
				colvarCompanyBankCode.IsNullable = true;
				colvarCompanyBankCode.IsPrimaryKey = false;
				colvarCompanyBankCode.IsForeignKey = false;
				colvarCompanyBankCode.IsReadOnly = false;
				colvarCompanyBankCode.DefaultSetting = @"";
				colvarCompanyBankCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyBankCode);
				
				TableSchema.TableColumn colvarCompanyBranchCode = new TableSchema.TableColumn(schema);
				colvarCompanyBranchCode.ColumnName = "CompanyBranchCode";
				colvarCompanyBranchCode.DataType = DbType.AnsiString;
				colvarCompanyBranchCode.MaxLength = 10;
				colvarCompanyBranchCode.AutoIncrement = false;
				colvarCompanyBranchCode.IsNullable = true;
				colvarCompanyBranchCode.IsPrimaryKey = false;
				colvarCompanyBranchCode.IsForeignKey = false;
				colvarCompanyBranchCode.IsReadOnly = false;
				colvarCompanyBranchCode.DefaultSetting = @"";
				colvarCompanyBranchCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyBranchCode);
				
				TableSchema.TableColumn colvarCompanyAccount = new TableSchema.TableColumn(schema);
				colvarCompanyAccount.ColumnName = "CompanyAccount";
				colvarCompanyAccount.DataType = DbType.AnsiString;
				colvarCompanyAccount.MaxLength = 20;
				colvarCompanyAccount.AutoIncrement = false;
				colvarCompanyAccount.IsNullable = true;
				colvarCompanyAccount.IsPrimaryKey = false;
				colvarCompanyAccount.IsForeignKey = false;
				colvarCompanyAccount.IsReadOnly = false;
				colvarCompanyAccount.DefaultSetting = @"";
				colvarCompanyAccount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyAccount);
				
				TableSchema.TableColumn colvarCompanyAccountName = new TableSchema.TableColumn(schema);
				colvarCompanyAccountName.ColumnName = "CompanyAccountName";
				colvarCompanyAccountName.DataType = DbType.String;
				colvarCompanyAccountName.MaxLength = 50;
				colvarCompanyAccountName.AutoIncrement = false;
				colvarCompanyAccountName.IsNullable = true;
				colvarCompanyAccountName.IsPrimaryKey = false;
				colvarCompanyAccountName.IsForeignKey = false;
				colvarCompanyAccountName.IsReadOnly = false;
				colvarCompanyAccountName.DefaultSetting = @"";
				colvarCompanyAccountName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyAccountName);
				
				TableSchema.TableColumn colvarCompanyEmail = new TableSchema.TableColumn(schema);
				colvarCompanyEmail.ColumnName = "CompanyEmail";
				colvarCompanyEmail.DataType = DbType.String;
				colvarCompanyEmail.MaxLength = 250;
				colvarCompanyEmail.AutoIncrement = false;
				colvarCompanyEmail.IsNullable = true;
				colvarCompanyEmail.IsPrimaryKey = false;
				colvarCompanyEmail.IsForeignKey = false;
				colvarCompanyEmail.IsReadOnly = false;
				colvarCompanyEmail.DefaultSetting = @"";
				colvarCompanyEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyEmail);
				
				TableSchema.TableColumn colvarCompanyNotice = new TableSchema.TableColumn(schema);
				colvarCompanyNotice.ColumnName = "CompanyNotice";
				colvarCompanyNotice.DataType = DbType.String;
				colvarCompanyNotice.MaxLength = 250;
				colvarCompanyNotice.AutoIncrement = false;
				colvarCompanyNotice.IsNullable = true;
				colvarCompanyNotice.IsPrimaryKey = false;
				colvarCompanyNotice.IsForeignKey = false;
				colvarCompanyNotice.IsReadOnly = false;
				colvarCompanyNotice.DefaultSetting = @"";
				colvarCompanyNotice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyNotice);
				
				TableSchema.TableColumn colvarIsCloseDown = new TableSchema.TableColumn(schema);
				colvarIsCloseDown.ColumnName = "is_close_down";
				colvarIsCloseDown.DataType = DbType.Boolean;
				colvarIsCloseDown.MaxLength = 0;
				colvarIsCloseDown.AutoIncrement = false;
				colvarIsCloseDown.IsNullable = false;
				colvarIsCloseDown.IsPrimaryKey = false;
				colvarIsCloseDown.IsForeignKey = false;
				colvarIsCloseDown.IsReadOnly = false;
				colvarIsCloseDown.DefaultSetting = @"";
				colvarIsCloseDown.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCloseDown);
				
				TableSchema.TableColumn colvarCloseDownDate = new TableSchema.TableColumn(schema);
				colvarCloseDownDate.ColumnName = "close_down_date";
				colvarCloseDownDate.DataType = DbType.DateTime;
				colvarCloseDownDate.MaxLength = 0;
				colvarCloseDownDate.AutoIncrement = false;
				colvarCloseDownDate.IsNullable = true;
				colvarCloseDownDate.IsPrimaryKey = false;
				colvarCloseDownDate.IsForeignKey = false;
				colvarCloseDownDate.IsReadOnly = false;
				colvarCloseDownDate.DefaultSetting = @"";
				colvarCloseDownDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCloseDownDate);
				
				TableSchema.TableColumn colvarSignCompanyID = new TableSchema.TableColumn(schema);
				colvarSignCompanyID.ColumnName = "SignCompanyID";
				colvarSignCompanyID.DataType = DbType.AnsiString;
				colvarSignCompanyID.MaxLength = 20;
				colvarSignCompanyID.AutoIncrement = false;
				colvarSignCompanyID.IsNullable = true;
				colvarSignCompanyID.IsPrimaryKey = false;
				colvarSignCompanyID.IsForeignKey = false;
				colvarSignCompanyID.IsReadOnly = false;
				colvarSignCompanyID.DefaultSetting = @"";
				colvarSignCompanyID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSignCompanyID);
				
				TableSchema.TableColumn colvarSellerContactPerson = new TableSchema.TableColumn(schema);
				colvarSellerContactPerson.ColumnName = "seller_contact_person";
				colvarSellerContactPerson.DataType = DbType.String;
				colvarSellerContactPerson.MaxLength = 100;
				colvarSellerContactPerson.AutoIncrement = false;
				colvarSellerContactPerson.IsNullable = true;
				colvarSellerContactPerson.IsPrimaryKey = false;
				colvarSellerContactPerson.IsForeignKey = false;
				colvarSellerContactPerson.IsReadOnly = false;
				colvarSellerContactPerson.DefaultSetting = @"";
				colvarSellerContactPerson.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerContactPerson);
				
				TableSchema.TableColumn colvarReturnedPersonName = new TableSchema.TableColumn(schema);
				colvarReturnedPersonName.ColumnName = "returned_person_name";
				colvarReturnedPersonName.DataType = DbType.String;
				colvarReturnedPersonName.MaxLength = 100;
				colvarReturnedPersonName.AutoIncrement = false;
				colvarReturnedPersonName.IsNullable = true;
				colvarReturnedPersonName.IsPrimaryKey = false;
				colvarReturnedPersonName.IsForeignKey = false;
				colvarReturnedPersonName.IsReadOnly = false;
				colvarReturnedPersonName.DefaultSetting = @"";
				colvarReturnedPersonName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnedPersonName);
				
				TableSchema.TableColumn colvarReturnedPersonTel = new TableSchema.TableColumn(schema);
				colvarReturnedPersonTel.ColumnName = "returned_person_tel";
				colvarReturnedPersonTel.DataType = DbType.AnsiString;
				colvarReturnedPersonTel.MaxLength = 100;
				colvarReturnedPersonTel.AutoIncrement = false;
				colvarReturnedPersonTel.IsNullable = true;
				colvarReturnedPersonTel.IsPrimaryKey = false;
				colvarReturnedPersonTel.IsForeignKey = false;
				colvarReturnedPersonTel.IsReadOnly = false;
				colvarReturnedPersonTel.DefaultSetting = @"";
				colvarReturnedPersonTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnedPersonTel);
				
				TableSchema.TableColumn colvarReturnedPersonEmail = new TableSchema.TableColumn(schema);
				colvarReturnedPersonEmail.ColumnName = "returned_person_email";
				colvarReturnedPersonEmail.DataType = DbType.String;
				colvarReturnedPersonEmail.MaxLength = 200;
				colvarReturnedPersonEmail.AutoIncrement = false;
				colvarReturnedPersonEmail.IsNullable = true;
				colvarReturnedPersonEmail.IsPrimaryKey = false;
				colvarReturnedPersonEmail.IsForeignKey = false;
				colvarReturnedPersonEmail.IsReadOnly = false;
				colvarReturnedPersonEmail.DefaultSetting = @"";
				colvarReturnedPersonEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnedPersonEmail);
				
				TableSchema.TableColumn colvarAccountantName = new TableSchema.TableColumn(schema);
				colvarAccountantName.ColumnName = "accountant_name";
				colvarAccountantName.DataType = DbType.String;
				colvarAccountantName.MaxLength = 20;
				colvarAccountantName.AutoIncrement = false;
				colvarAccountantName.IsNullable = true;
				colvarAccountantName.IsPrimaryKey = false;
				colvarAccountantName.IsForeignKey = false;
				colvarAccountantName.IsReadOnly = false;
				colvarAccountantName.DefaultSetting = @"";
				colvarAccountantName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountantName);
				
				TableSchema.TableColumn colvarAccountantTel = new TableSchema.TableColumn(schema);
				colvarAccountantTel.ColumnName = "accountant_tel";
				colvarAccountantTel.DataType = DbType.AnsiString;
				colvarAccountantTel.MaxLength = 100;
				colvarAccountantTel.AutoIncrement = false;
				colvarAccountantTel.IsNullable = true;
				colvarAccountantTel.IsPrimaryKey = false;
				colvarAccountantTel.IsForeignKey = false;
				colvarAccountantTel.IsReadOnly = false;
				colvarAccountantTel.DefaultSetting = @"";
				colvarAccountantTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountantTel);
				
				TableSchema.TableColumn colvarTempStatus = new TableSchema.TableColumn(schema);
				colvarTempStatus.ColumnName = "temp_status";
				colvarTempStatus.DataType = DbType.Int32;
				colvarTempStatus.MaxLength = 0;
				colvarTempStatus.AutoIncrement = false;
				colvarTempStatus.IsNullable = false;
				colvarTempStatus.IsPrimaryKey = false;
				colvarTempStatus.IsForeignKey = false;
				colvarTempStatus.IsReadOnly = false;
				
						colvarTempStatus.DefaultSetting = @"((0))";
				colvarTempStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTempStatus);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 1073741823;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarNewCreated = new TableSchema.TableColumn(schema);
				colvarNewCreated.ColumnName = "new_created";
				colvarNewCreated.DataType = DbType.Boolean;
				colvarNewCreated.MaxLength = 0;
				colvarNewCreated.AutoIncrement = false;
				colvarNewCreated.IsNullable = false;
				colvarNewCreated.IsPrimaryKey = false;
				colvarNewCreated.IsForeignKey = false;
				colvarNewCreated.IsReadOnly = false;
				
						colvarNewCreated.DefaultSetting = @"((0))";
				colvarNewCreated.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNewCreated);
				
				TableSchema.TableColumn colvarApplyId = new TableSchema.TableColumn(schema);
				colvarApplyId.ColumnName = "apply_id";
				colvarApplyId.DataType = DbType.String;
				colvarApplyId.MaxLength = 100;
				colvarApplyId.AutoIncrement = false;
				colvarApplyId.IsNullable = true;
				colvarApplyId.IsPrimaryKey = false;
				colvarApplyId.IsForeignKey = false;
				colvarApplyId.IsReadOnly = false;
				colvarApplyId.DefaultSetting = @"";
				colvarApplyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApplyId);
				
				TableSchema.TableColumn colvarApplyTime = new TableSchema.TableColumn(schema);
				colvarApplyTime.ColumnName = "apply_time";
				colvarApplyTime.DataType = DbType.DateTime;
				colvarApplyTime.MaxLength = 0;
				colvarApplyTime.AutoIncrement = false;
				colvarApplyTime.IsNullable = true;
				colvarApplyTime.IsPrimaryKey = false;
				colvarApplyTime.IsForeignKey = false;
				colvarApplyTime.IsReadOnly = false;
				colvarApplyTime.DefaultSetting = @"";
				colvarApplyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApplyTime);
				
				TableSchema.TableColumn colvarReturnTime = new TableSchema.TableColumn(schema);
				colvarReturnTime.ColumnName = "return_time";
				colvarReturnTime.DataType = DbType.DateTime;
				colvarReturnTime.MaxLength = 0;
				colvarReturnTime.AutoIncrement = false;
				colvarReturnTime.IsNullable = true;
				colvarReturnTime.IsPrimaryKey = false;
				colvarReturnTime.IsForeignKey = false;
				colvarReturnTime.IsReadOnly = false;
				colvarReturnTime.DefaultSetting = @"";
				colvarReturnTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnTime);
				
				TableSchema.TableColumn colvarApproveTime = new TableSchema.TableColumn(schema);
				colvarApproveTime.ColumnName = "approve_time";
				colvarApproveTime.DataType = DbType.DateTime;
				colvarApproveTime.MaxLength = 0;
				colvarApproveTime.AutoIncrement = false;
				colvarApproveTime.IsNullable = true;
				colvarApproveTime.IsPrimaryKey = false;
				colvarApproveTime.IsForeignKey = false;
				colvarApproveTime.IsReadOnly = false;
				colvarApproveTime.DefaultSetting = @"";
				colvarApproveTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApproveTime);
				
				TableSchema.TableColumn colvarVbsPrefix = new TableSchema.TableColumn(schema);
				colvarVbsPrefix.ColumnName = "vbs_prefix";
				colvarVbsPrefix.DataType = DbType.AnsiStringFixedLength;
				colvarVbsPrefix.MaxLength = 6;
				colvarVbsPrefix.AutoIncrement = false;
				colvarVbsPrefix.IsNullable = true;
				colvarVbsPrefix.IsPrimaryKey = false;
				colvarVbsPrefix.IsForeignKey = false;
				colvarVbsPrefix.IsReadOnly = false;
				colvarVbsPrefix.DefaultSetting = @"";
				colvarVbsPrefix.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVbsPrefix);
				
				TableSchema.TableColumn colvarSellerCategory = new TableSchema.TableColumn(schema);
				colvarSellerCategory.ColumnName = "seller_category";
				colvarSellerCategory.DataType = DbType.Int32;
				colvarSellerCategory.MaxLength = 0;
				colvarSellerCategory.AutoIncrement = false;
				colvarSellerCategory.IsNullable = true;
				colvarSellerCategory.IsPrimaryKey = false;
				colvarSellerCategory.IsForeignKey = false;
				colvarSellerCategory.IsReadOnly = false;
				colvarSellerCategory.DefaultSetting = @"";
				colvarSellerCategory.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerCategory);
				
				TableSchema.TableColumn colvarSellerConsumptionAvg = new TableSchema.TableColumn(schema);
				colvarSellerConsumptionAvg.ColumnName = "seller_consumption_avg";
				colvarSellerConsumptionAvg.DataType = DbType.Int32;
				colvarSellerConsumptionAvg.MaxLength = 0;
				colvarSellerConsumptionAvg.AutoIncrement = false;
				colvarSellerConsumptionAvg.IsNullable = true;
				colvarSellerConsumptionAvg.IsPrimaryKey = false;
				colvarSellerConsumptionAvg.IsForeignKey = false;
				colvarSellerConsumptionAvg.IsReadOnly = false;
				colvarSellerConsumptionAvg.DefaultSetting = @"";
				colvarSellerConsumptionAvg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerConsumptionAvg);
				
				TableSchema.TableColumn colvarSellerLevel = new TableSchema.TableColumn(schema);
				colvarSellerLevel.ColumnName = "seller_level";
				colvarSellerLevel.DataType = DbType.Int32;
				colvarSellerLevel.MaxLength = 0;
				colvarSellerLevel.AutoIncrement = false;
				colvarSellerLevel.IsNullable = true;
				colvarSellerLevel.IsPrimaryKey = false;
				colvarSellerLevel.IsForeignKey = false;
				colvarSellerLevel.IsReadOnly = false;
				colvarSellerLevel.DefaultSetting = @"";
				colvarSellerLevel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerLevel);
				
				TableSchema.TableColumn colvarFmeNo = new TableSchema.TableColumn(schema);
				colvarFmeNo.ColumnName = "fme_no";
				colvarFmeNo.DataType = DbType.Int32;
				colvarFmeNo.MaxLength = 0;
				colvarFmeNo.AutoIncrement = false;
				colvarFmeNo.IsNullable = true;
				colvarFmeNo.IsPrimaryKey = false;
				colvarFmeNo.IsForeignKey = false;
				colvarFmeNo.IsReadOnly = false;
				colvarFmeNo.DefaultSetting = @"";
				colvarFmeNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFmeNo);
				
				TableSchema.TableColumn colvarSalesId = new TableSchema.TableColumn(schema);
				colvarSalesId.ColumnName = "sales_id";
				colvarSalesId.DataType = DbType.Int32;
				colvarSalesId.MaxLength = 0;
				colvarSalesId.AutoIncrement = false;
				colvarSalesId.IsNullable = true;
				colvarSalesId.IsPrimaryKey = false;
				colvarSalesId.IsForeignKey = false;
				colvarSalesId.IsReadOnly = false;
				colvarSalesId.DefaultSetting = @"";
				colvarSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesId);
				
				TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
				colvarRemittanceType.ColumnName = "remittance_type";
				colvarRemittanceType.DataType = DbType.Int32;
				colvarRemittanceType.MaxLength = 0;
				colvarRemittanceType.AutoIncrement = false;
				colvarRemittanceType.IsNullable = true;
				colvarRemittanceType.IsPrimaryKey = false;
				colvarRemittanceType.IsForeignKey = false;
				colvarRemittanceType.IsReadOnly = false;
				colvarRemittanceType.DefaultSetting = @"";
				colvarRemittanceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemittanceType);
				
				TableSchema.TableColumn colvarSellerFrom = new TableSchema.TableColumn(schema);
				colvarSellerFrom.ColumnName = "seller_from";
				colvarSellerFrom.DataType = DbType.Int16;
				colvarSellerFrom.MaxLength = 0;
				colvarSellerFrom.AutoIncrement = false;
				colvarSellerFrom.IsNullable = true;
				colvarSellerFrom.IsPrimaryKey = false;
				colvarSellerFrom.IsForeignKey = false;
				colvarSellerFrom.IsReadOnly = false;
				colvarSellerFrom.DefaultSetting = @"";
				colvarSellerFrom.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerFrom);
				
				TableSchema.TableColumn colvarSellerPorperty = new TableSchema.TableColumn(schema);
				colvarSellerPorperty.ColumnName = "seller_porperty";
				colvarSellerPorperty.DataType = DbType.AnsiString;
				colvarSellerPorperty.MaxLength = 50;
				colvarSellerPorperty.AutoIncrement = false;
				colvarSellerPorperty.IsNullable = true;
				colvarSellerPorperty.IsPrimaryKey = false;
				colvarSellerPorperty.IsForeignKey = false;
				colvarSellerPorperty.IsReadOnly = false;
				colvarSellerPorperty.DefaultSetting = @"";
				colvarSellerPorperty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerPorperty);
				
				TableSchema.TableColumn colvarContacts = new TableSchema.TableColumn(schema);
				colvarContacts.ColumnName = "contacts";
				colvarContacts.DataType = DbType.String;
				colvarContacts.MaxLength = 2000;
				colvarContacts.AutoIncrement = false;
				colvarContacts.IsNullable = true;
				colvarContacts.IsPrimaryKey = false;
				colvarContacts.IsForeignKey = false;
				colvarContacts.IsReadOnly = false;
				colvarContacts.DefaultSetting = @"";
				colvarContacts.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContacts);
				
				TableSchema.TableColumn colvarSellerInfo = new TableSchema.TableColumn(schema);
				colvarSellerInfo.ColumnName = "seller_info";
				colvarSellerInfo.DataType = DbType.String;
				colvarSellerInfo.MaxLength = -1;
				colvarSellerInfo.AutoIncrement = false;
				colvarSellerInfo.IsNullable = true;
				colvarSellerInfo.IsPrimaryKey = false;
				colvarSellerInfo.IsForeignKey = false;
				colvarSellerInfo.IsReadOnly = false;
				colvarSellerInfo.DefaultSetting = @"";
				colvarSellerInfo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerInfo);
				
				TableSchema.TableColumn colvarOauthClientAppId = new TableSchema.TableColumn(schema);
				colvarOauthClientAppId.ColumnName = "oauth_client_app_id";
				colvarOauthClientAppId.DataType = DbType.String;
				colvarOauthClientAppId.MaxLength = 100;
				colvarOauthClientAppId.AutoIncrement = false;
				colvarOauthClientAppId.IsNullable = true;
				colvarOauthClientAppId.IsPrimaryKey = false;
				colvarOauthClientAppId.IsForeignKey = false;
				colvarOauthClientAppId.IsReadOnly = false;
				colvarOauthClientAppId.DefaultSetting = @"";
				colvarOauthClientAppId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOauthClientAppId);
				
				TableSchema.TableColumn colvarVendorReceiptType = new TableSchema.TableColumn(schema);
				colvarVendorReceiptType.ColumnName = "vendor_receipt_type";
				colvarVendorReceiptType.DataType = DbType.Int32;
				colvarVendorReceiptType.MaxLength = 0;
				colvarVendorReceiptType.AutoIncrement = false;
				colvarVendorReceiptType.IsNullable = true;
				colvarVendorReceiptType.IsPrimaryKey = false;
				colvarVendorReceiptType.IsForeignKey = false;
				colvarVendorReceiptType.IsReadOnly = false;
				colvarVendorReceiptType.DefaultSetting = @"";
				colvarVendorReceiptType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorReceiptType);
				
				TableSchema.TableColumn colvarOthermessage = new TableSchema.TableColumn(schema);
				colvarOthermessage.ColumnName = "othermessage";
				colvarOthermessage.DataType = DbType.String;
				colvarOthermessage.MaxLength = 50;
				colvarOthermessage.AutoIncrement = false;
				colvarOthermessage.IsNullable = true;
				colvarOthermessage.IsPrimaryKey = false;
				colvarOthermessage.IsForeignKey = false;
				colvarOthermessage.IsReadOnly = false;
				colvarOthermessage.DefaultSetting = @"";
				colvarOthermessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOthermessage);
				
				TableSchema.TableColumn colvarStoreTel = new TableSchema.TableColumn(schema);
				colvarStoreTel.ColumnName = "store_tel";
				colvarStoreTel.DataType = DbType.AnsiString;
				colvarStoreTel.MaxLength = 100;
				colvarStoreTel.AutoIncrement = false;
				colvarStoreTel.IsNullable = true;
				colvarStoreTel.IsPrimaryKey = false;
				colvarStoreTel.IsForeignKey = false;
				colvarStoreTel.IsReadOnly = false;
				colvarStoreTel.DefaultSetting = @"";
				colvarStoreTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreTel);
				
				TableSchema.TableColumn colvarStoreAddress = new TableSchema.TableColumn(schema);
				colvarStoreAddress.ColumnName = "store_address";
				colvarStoreAddress.DataType = DbType.String;
				colvarStoreAddress.MaxLength = 100;
				colvarStoreAddress.AutoIncrement = false;
				colvarStoreAddress.IsNullable = true;
				colvarStoreAddress.IsPrimaryKey = false;
				colvarStoreAddress.IsForeignKey = false;
				colvarStoreAddress.IsReadOnly = false;
				colvarStoreAddress.DefaultSetting = @"";
				colvarStoreAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreAddress);
				
				TableSchema.TableColumn colvarStoreStatus = new TableSchema.TableColumn(schema);
				colvarStoreStatus.ColumnName = "store_status";
				colvarStoreStatus.DataType = DbType.Int32;
				colvarStoreStatus.MaxLength = 0;
				colvarStoreStatus.AutoIncrement = false;
				colvarStoreStatus.IsNullable = false;
				colvarStoreStatus.IsPrimaryKey = false;
				colvarStoreStatus.IsForeignKey = false;
				colvarStoreStatus.IsReadOnly = false;
				
						colvarStoreStatus.DefaultSetting = @"((0))";
				colvarStoreStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreStatus);
				
				TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
				colvarOpenTime.ColumnName = "open_time";
				colvarOpenTime.DataType = DbType.String;
				colvarOpenTime.MaxLength = 256;
				colvarOpenTime.AutoIncrement = false;
				colvarOpenTime.IsNullable = true;
				colvarOpenTime.IsPrimaryKey = false;
				colvarOpenTime.IsForeignKey = false;
				colvarOpenTime.IsReadOnly = false;
				colvarOpenTime.DefaultSetting = @"";
				colvarOpenTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOpenTime);
				
				TableSchema.TableColumn colvarCloseDate = new TableSchema.TableColumn(schema);
				colvarCloseDate.ColumnName = "close_date";
				colvarCloseDate.DataType = DbType.String;
				colvarCloseDate.MaxLength = 256;
				colvarCloseDate.AutoIncrement = false;
				colvarCloseDate.IsNullable = true;
				colvarCloseDate.IsPrimaryKey = false;
				colvarCloseDate.IsForeignKey = false;
				colvarCloseDate.IsReadOnly = false;
				colvarCloseDate.DefaultSetting = @"";
				colvarCloseDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCloseDate);
				
				TableSchema.TableColumn colvarMrt = new TableSchema.TableColumn(schema);
				colvarMrt.ColumnName = "mrt";
				colvarMrt.DataType = DbType.String;
				colvarMrt.MaxLength = 500;
				colvarMrt.AutoIncrement = false;
				colvarMrt.IsNullable = true;
				colvarMrt.IsPrimaryKey = false;
				colvarMrt.IsForeignKey = false;
				colvarMrt.IsReadOnly = false;
				colvarMrt.DefaultSetting = @"";
				colvarMrt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMrt);
				
				TableSchema.TableColumn colvarCar = new TableSchema.TableColumn(schema);
				colvarCar.ColumnName = "car";
				colvarCar.DataType = DbType.String;
				colvarCar.MaxLength = 500;
				colvarCar.AutoIncrement = false;
				colvarCar.IsNullable = true;
				colvarCar.IsPrimaryKey = false;
				colvarCar.IsForeignKey = false;
				colvarCar.IsReadOnly = false;
				colvarCar.DefaultSetting = @"";
				colvarCar.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCar);
				
				TableSchema.TableColumn colvarBus = new TableSchema.TableColumn(schema);
				colvarBus.ColumnName = "bus";
				colvarBus.DataType = DbType.String;
				colvarBus.MaxLength = 500;
				colvarBus.AutoIncrement = false;
				colvarBus.IsNullable = true;
				colvarBus.IsPrimaryKey = false;
				colvarBus.IsForeignKey = false;
				colvarBus.IsReadOnly = false;
				colvarBus.DefaultSetting = @"";
				colvarBus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBus);
				
				TableSchema.TableColumn colvarOtherVehicles = new TableSchema.TableColumn(schema);
				colvarOtherVehicles.ColumnName = "other_vehicles";
				colvarOtherVehicles.DataType = DbType.String;
				colvarOtherVehicles.MaxLength = 500;
				colvarOtherVehicles.AutoIncrement = false;
				colvarOtherVehicles.IsNullable = true;
				colvarOtherVehicles.IsPrimaryKey = false;
				colvarOtherVehicles.IsForeignKey = false;
				colvarOtherVehicles.IsReadOnly = false;
				colvarOtherVehicles.DefaultSetting = @"";
				colvarOtherVehicles.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOtherVehicles);
				
				TableSchema.TableColumn colvarWebUrl = new TableSchema.TableColumn(schema);
				colvarWebUrl.ColumnName = "web_url";
				colvarWebUrl.DataType = DbType.String;
				colvarWebUrl.MaxLength = 4000;
				colvarWebUrl.AutoIncrement = false;
				colvarWebUrl.IsNullable = true;
				colvarWebUrl.IsPrimaryKey = false;
				colvarWebUrl.IsForeignKey = false;
				colvarWebUrl.IsReadOnly = false;
				colvarWebUrl.DefaultSetting = @"";
				colvarWebUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWebUrl);
				
				TableSchema.TableColumn colvarFacebookUrl = new TableSchema.TableColumn(schema);
				colvarFacebookUrl.ColumnName = "facebook_url";
				colvarFacebookUrl.DataType = DbType.String;
				colvarFacebookUrl.MaxLength = 4000;
				colvarFacebookUrl.AutoIncrement = false;
				colvarFacebookUrl.IsNullable = true;
				colvarFacebookUrl.IsPrimaryKey = false;
				colvarFacebookUrl.IsForeignKey = false;
				colvarFacebookUrl.IsReadOnly = false;
				colvarFacebookUrl.DefaultSetting = @"";
				colvarFacebookUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFacebookUrl);
				
				TableSchema.TableColumn colvarBlogUrl = new TableSchema.TableColumn(schema);
				colvarBlogUrl.ColumnName = "blog_url";
				colvarBlogUrl.DataType = DbType.String;
				colvarBlogUrl.MaxLength = 4000;
				colvarBlogUrl.AutoIncrement = false;
				colvarBlogUrl.IsNullable = true;
				colvarBlogUrl.IsPrimaryKey = false;
				colvarBlogUrl.IsForeignKey = false;
				colvarBlogUrl.IsReadOnly = false;
				colvarBlogUrl.DefaultSetting = @"";
				colvarBlogUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBlogUrl);
				
				TableSchema.TableColumn colvarOtherUrl = new TableSchema.TableColumn(schema);
				colvarOtherUrl.ColumnName = "other_url";
				colvarOtherUrl.DataType = DbType.String;
				colvarOtherUrl.MaxLength = 4000;
				colvarOtherUrl.AutoIncrement = false;
				colvarOtherUrl.IsNullable = true;
				colvarOtherUrl.IsPrimaryKey = false;
				colvarOtherUrl.IsForeignKey = false;
				colvarOtherUrl.IsReadOnly = false;
				colvarOtherUrl.DefaultSetting = @"";
				colvarOtherUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOtherUrl);
				
				TableSchema.TableColumn colvarStoreRemark = new TableSchema.TableColumn(schema);
				colvarStoreRemark.ColumnName = "store_remark";
				colvarStoreRemark.DataType = DbType.String;
				colvarStoreRemark.MaxLength = 500;
				colvarStoreRemark.AutoIncrement = false;
				colvarStoreRemark.IsNullable = true;
				colvarStoreRemark.IsPrimaryKey = false;
				colvarStoreRemark.IsForeignKey = false;
				colvarStoreRemark.IsReadOnly = false;
				colvarStoreRemark.DefaultSetting = @"";
				colvarStoreRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreRemark);
				
				TableSchema.TableColumn colvarCreditcardAvailable = new TableSchema.TableColumn(schema);
				colvarCreditcardAvailable.ColumnName = "creditcard_available";
				colvarCreditcardAvailable.DataType = DbType.Boolean;
				colvarCreditcardAvailable.MaxLength = 0;
				colvarCreditcardAvailable.AutoIncrement = false;
				colvarCreditcardAvailable.IsNullable = false;
				colvarCreditcardAvailable.IsPrimaryKey = false;
				colvarCreditcardAvailable.IsForeignKey = false;
				colvarCreditcardAvailable.IsReadOnly = false;
				
						colvarCreditcardAvailable.DefaultSetting = @"((0))";
				colvarCreditcardAvailable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditcardAvailable);
				
				TableSchema.TableColumn colvarIsOpenReservationSetting = new TableSchema.TableColumn(schema);
				colvarIsOpenReservationSetting.ColumnName = "is_open_reservation_setting";
				colvarIsOpenReservationSetting.DataType = DbType.Boolean;
				colvarIsOpenReservationSetting.MaxLength = 0;
				colvarIsOpenReservationSetting.AutoIncrement = false;
				colvarIsOpenReservationSetting.IsNullable = false;
				colvarIsOpenReservationSetting.IsPrimaryKey = false;
				colvarIsOpenReservationSetting.IsForeignKey = false;
				colvarIsOpenReservationSetting.IsReadOnly = false;
				
						colvarIsOpenReservationSetting.DefaultSetting = @"((0))";
				colvarIsOpenReservationSetting.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOpenReservationSetting);
				
				TableSchema.TableColumn colvarIsOpenBooking = new TableSchema.TableColumn(schema);
				colvarIsOpenBooking.ColumnName = "is_open_booking";
				colvarIsOpenBooking.DataType = DbType.Boolean;
				colvarIsOpenBooking.MaxLength = 0;
				colvarIsOpenBooking.AutoIncrement = false;
				colvarIsOpenBooking.IsNullable = false;
				colvarIsOpenBooking.IsPrimaryKey = false;
				colvarIsOpenBooking.IsForeignKey = false;
				colvarIsOpenBooking.IsReadOnly = false;
				
						colvarIsOpenBooking.DefaultSetting = @"((0))";
				colvarIsOpenBooking.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOpenBooking);
				
				TableSchema.TableColumn colvarStoreCityId = new TableSchema.TableColumn(schema);
				colvarStoreCityId.ColumnName = "store_city_id";
				colvarStoreCityId.DataType = DbType.Int32;
				colvarStoreCityId.MaxLength = 0;
				colvarStoreCityId.AutoIncrement = false;
				colvarStoreCityId.IsNullable = true;
				colvarStoreCityId.IsPrimaryKey = false;
				colvarStoreCityId.IsForeignKey = false;
				colvarStoreCityId.IsReadOnly = false;
				colvarStoreCityId.DefaultSetting = @"";
				colvarStoreCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreCityId);
				
				TableSchema.TableColumn colvarStoreTownshipId = new TableSchema.TableColumn(schema);
				colvarStoreTownshipId.ColumnName = "store_township_id";
				colvarStoreTownshipId.DataType = DbType.Int32;
				colvarStoreTownshipId.MaxLength = 0;
				colvarStoreTownshipId.AutoIncrement = false;
				colvarStoreTownshipId.IsNullable = true;
				colvarStoreTownshipId.IsPrimaryKey = false;
				colvarStoreTownshipId.IsForeignKey = false;
				colvarStoreTownshipId.IsReadOnly = false;
				colvarStoreTownshipId.DefaultSetting = @"";
				colvarStoreTownshipId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreTownshipId);
				
				TableSchema.TableColumn colvarStoreRelationCode = new TableSchema.TableColumn(schema);
				colvarStoreRelationCode.ColumnName = "store_relation_code";
				colvarStoreRelationCode.DataType = DbType.String;
				colvarStoreRelationCode.MaxLength = 50;
				colvarStoreRelationCode.AutoIncrement = false;
				colvarStoreRelationCode.IsNullable = true;
				colvarStoreRelationCode.IsPrimaryKey = false;
				colvarStoreRelationCode.IsForeignKey = false;
				colvarStoreRelationCode.IsReadOnly = false;
				colvarStoreRelationCode.DefaultSetting = @"";
				colvarStoreRelationCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreRelationCode);
				
				TableSchema.TableColumn colvarReferralSale = new TableSchema.TableColumn(schema);
				colvarReferralSale.ColumnName = "referral_sale";
				colvarReferralSale.DataType = DbType.AnsiString;
				colvarReferralSale.MaxLength = 100;
				colvarReferralSale.AutoIncrement = false;
				colvarReferralSale.IsNullable = true;
				colvarReferralSale.IsPrimaryKey = false;
				colvarReferralSale.IsForeignKey = false;
				colvarReferralSale.IsReadOnly = false;
				colvarReferralSale.DefaultSetting = @"";
				colvarReferralSale.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferralSale);
				
				TableSchema.TableColumn colvarReferralSaleCreateTime = new TableSchema.TableColumn(schema);
				colvarReferralSaleCreateTime.ColumnName = "referral_sale_create_time";
				colvarReferralSaleCreateTime.DataType = DbType.DateTime;
				colvarReferralSaleCreateTime.MaxLength = 0;
				colvarReferralSaleCreateTime.AutoIncrement = false;
				colvarReferralSaleCreateTime.IsNullable = true;
				colvarReferralSaleCreateTime.IsPrimaryKey = false;
				colvarReferralSaleCreateTime.IsForeignKey = false;
				colvarReferralSaleCreateTime.IsReadOnly = false;
				colvarReferralSaleCreateTime.DefaultSetting = @"";
				colvarReferralSaleCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferralSaleCreateTime);
				
				TableSchema.TableColumn colvarReferralSaleBeginTime = new TableSchema.TableColumn(schema);
				colvarReferralSaleBeginTime.ColumnName = "referral_sale_begin_time";
				colvarReferralSaleBeginTime.DataType = DbType.DateTime;
				colvarReferralSaleBeginTime.MaxLength = 0;
				colvarReferralSaleBeginTime.AutoIncrement = false;
				colvarReferralSaleBeginTime.IsNullable = true;
				colvarReferralSaleBeginTime.IsPrimaryKey = false;
				colvarReferralSaleBeginTime.IsForeignKey = false;
				colvarReferralSaleBeginTime.IsReadOnly = false;
				colvarReferralSaleBeginTime.DefaultSetting = @"";
				colvarReferralSaleBeginTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferralSaleBeginTime);
				
				TableSchema.TableColumn colvarReferralSaleEndTime = new TableSchema.TableColumn(schema);
				colvarReferralSaleEndTime.ColumnName = "referral_sale_end_time";
				colvarReferralSaleEndTime.DataType = DbType.DateTime;
				colvarReferralSaleEndTime.MaxLength = 0;
				colvarReferralSaleEndTime.AutoIncrement = false;
				colvarReferralSaleEndTime.IsNullable = true;
				colvarReferralSaleEndTime.IsPrimaryKey = false;
				colvarReferralSaleEndTime.IsForeignKey = false;
				colvarReferralSaleEndTime.IsReadOnly = false;
				colvarReferralSaleEndTime.DefaultSetting = @"";
				colvarReferralSaleEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferralSaleEndTime);
				
				TableSchema.TableColumn colvarReferralSalePercent = new TableSchema.TableColumn(schema);
				colvarReferralSalePercent.ColumnName = "referral_sale_percent";
				colvarReferralSalePercent.DataType = DbType.Int32;
				colvarReferralSalePercent.MaxLength = 0;
				colvarReferralSalePercent.AutoIncrement = false;
				colvarReferralSalePercent.IsNullable = true;
				colvarReferralSalePercent.IsPrimaryKey = false;
				colvarReferralSalePercent.IsForeignKey = false;
				colvarReferralSalePercent.IsReadOnly = false;
				colvarReferralSalePercent.DefaultSetting = @"";
				colvarReferralSalePercent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferralSalePercent);
				
				TableSchema.TableColumn colvarSellerLevelDetail = new TableSchema.TableColumn(schema);
				colvarSellerLevelDetail.ColumnName = "seller_level_detail";
				colvarSellerLevelDetail.DataType = DbType.String;
				colvarSellerLevelDetail.MaxLength = 4000;
				colvarSellerLevelDetail.AutoIncrement = false;
				colvarSellerLevelDetail.IsNullable = true;
				colvarSellerLevelDetail.IsPrimaryKey = false;
				colvarSellerLevelDetail.IsForeignKey = false;
				colvarSellerLevelDetail.IsReadOnly = false;
				colvarSellerLevelDetail.DefaultSetting = @"";
				colvarSellerLevelDetail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerLevelDetail);
				
				TableSchema.TableColumn colvarContractVersionHouse = new TableSchema.TableColumn(schema);
				colvarContractVersionHouse.ColumnName = "contract_version_house";
				colvarContractVersionHouse.DataType = DbType.AnsiString;
				colvarContractVersionHouse.MaxLength = 20;
				colvarContractVersionHouse.AutoIncrement = false;
				colvarContractVersionHouse.IsNullable = true;
				colvarContractVersionHouse.IsPrimaryKey = false;
				colvarContractVersionHouse.IsForeignKey = false;
				colvarContractVersionHouse.IsReadOnly = false;
				colvarContractVersionHouse.DefaultSetting = @"";
				colvarContractVersionHouse.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContractVersionHouse);
				
				TableSchema.TableColumn colvarContractCkeckTimeHouse = new TableSchema.TableColumn(schema);
				colvarContractCkeckTimeHouse.ColumnName = "contract_ckeck_time_house";
				colvarContractCkeckTimeHouse.DataType = DbType.DateTime;
				colvarContractCkeckTimeHouse.MaxLength = 0;
				colvarContractCkeckTimeHouse.AutoIncrement = false;
				colvarContractCkeckTimeHouse.IsNullable = true;
				colvarContractCkeckTimeHouse.IsPrimaryKey = false;
				colvarContractCkeckTimeHouse.IsForeignKey = false;
				colvarContractCkeckTimeHouse.IsReadOnly = false;
				colvarContractCkeckTimeHouse.DefaultSetting = @"";
				colvarContractCkeckTimeHouse.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContractCkeckTimeHouse);
				
				TableSchema.TableColumn colvarItemPriceCondition = new TableSchema.TableColumn(schema);
				colvarItemPriceCondition.ColumnName = "item_price_condition";
				colvarItemPriceCondition.DataType = DbType.AnsiString;
				colvarItemPriceCondition.MaxLength = 100;
				colvarItemPriceCondition.AutoIncrement = false;
				colvarItemPriceCondition.IsNullable = true;
				colvarItemPriceCondition.IsPrimaryKey = false;
				colvarItemPriceCondition.IsForeignKey = false;
				colvarItemPriceCondition.IsReadOnly = false;
				colvarItemPriceCondition.DefaultSetting = @"";
				colvarItemPriceCondition.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemPriceCondition);
				
				TableSchema.TableColumn colvarContractVersionPpon = new TableSchema.TableColumn(schema);
				colvarContractVersionPpon.ColumnName = "contract_version_ppon";
				colvarContractVersionPpon.DataType = DbType.AnsiString;
				colvarContractVersionPpon.MaxLength = 20;
				colvarContractVersionPpon.AutoIncrement = false;
				colvarContractVersionPpon.IsNullable = true;
				colvarContractVersionPpon.IsPrimaryKey = false;
				colvarContractVersionPpon.IsForeignKey = false;
				colvarContractVersionPpon.IsReadOnly = false;
				colvarContractVersionPpon.DefaultSetting = @"";
				colvarContractVersionPpon.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContractVersionPpon);
				
				TableSchema.TableColumn colvarContractCkeckTimePpon = new TableSchema.TableColumn(schema);
				colvarContractCkeckTimePpon.ColumnName = "contract_ckeck_time_ppon";
				colvarContractCkeckTimePpon.DataType = DbType.DateTime;
				colvarContractCkeckTimePpon.MaxLength = 0;
				colvarContractCkeckTimePpon.AutoIncrement = false;
				colvarContractCkeckTimePpon.IsNullable = true;
				colvarContractCkeckTimePpon.IsPrimaryKey = false;
				colvarContractCkeckTimePpon.IsForeignKey = false;
				colvarContractCkeckTimePpon.IsReadOnly = false;
				colvarContractCkeckTimePpon.DefaultSetting = @"";
				colvarContractCkeckTimePpon.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContractCkeckTimePpon);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("seller",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("SellerId")]
		[Bindable(true)]
		public string SellerId 
		{
			get { return GetColumnValue<string>(Columns.SellerId); }
			set { SetColumnValue(Columns.SellerId, value); }
		}
		  
		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName 
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}
		  
		[XmlAttribute("SellerBossName")]
		[Bindable(true)]
		public string SellerBossName 
		{
			get { return GetColumnValue<string>(Columns.SellerBossName); }
			set { SetColumnValue(Columns.SellerBossName, value); }
		}
		  
		[XmlAttribute("SellerTel")]
		[Bindable(true)]
		public string SellerTel 
		{
			get { return GetColumnValue<string>(Columns.SellerTel); }
			set { SetColumnValue(Columns.SellerTel, value); }
		}
		  
		[XmlAttribute("SellerTel2")]
		[Bindable(true)]
		public string SellerTel2 
		{
			get { return GetColumnValue<string>(Columns.SellerTel2); }
			set { SetColumnValue(Columns.SellerTel2, value); }
		}
		  
		[XmlAttribute("SellerFax")]
		[Bindable(true)]
		public string SellerFax 
		{
			get { return GetColumnValue<string>(Columns.SellerFax); }
			set { SetColumnValue(Columns.SellerFax, value); }
		}
		  
		[XmlAttribute("SellerMobile")]
		[Bindable(true)]
		public string SellerMobile 
		{
			get { return GetColumnValue<string>(Columns.SellerMobile); }
			set { SetColumnValue(Columns.SellerMobile, value); }
		}
		  
		[XmlAttribute("SellerAddress")]
		[Bindable(true)]
		public string SellerAddress 
		{
			get { return GetColumnValue<string>(Columns.SellerAddress); }
			set { SetColumnValue(Columns.SellerAddress, value); }
		}
		  
		[XmlAttribute("SellerEmail")]
		[Bindable(true)]
		public string SellerEmail 
		{
			get { return GetColumnValue<string>(Columns.SellerEmail); }
			set { SetColumnValue(Columns.SellerEmail, value); }
		}
		  
		[XmlAttribute("SellerBlog")]
		[Bindable(true)]
		public string SellerBlog 
		{
			get { return GetColumnValue<string>(Columns.SellerBlog); }
			set { SetColumnValue(Columns.SellerBlog, value); }
		}
		  
		[XmlAttribute("SellerInvoice")]
		[Bindable(true)]
		public string SellerInvoice 
		{
			get { return GetColumnValue<string>(Columns.SellerInvoice); }
			set { SetColumnValue(Columns.SellerInvoice, value); }
		}
		  
		[XmlAttribute("SellerDescription")]
		[Bindable(true)]
		public string SellerDescription 
		{
			get { return GetColumnValue<string>(Columns.SellerDescription); }
			set { SetColumnValue(Columns.SellerDescription, value); }
		}
		  
		[XmlAttribute("SellerStatus")]
		[Bindable(true)]
		public int SellerStatus 
		{
			get { return GetColumnValue<int>(Columns.SellerStatus); }
			set { SetColumnValue(Columns.SellerStatus, value); }
		}
		  
		[XmlAttribute("Department")]
		[Bindable(true)]
		public int Department 
		{
			get { return GetColumnValue<int>(Columns.Department); }
			set { SetColumnValue(Columns.Department, value); }
		}
		  
		[XmlAttribute("SellerRemark")]
		[Bindable(true)]
		public string SellerRemark 
		{
			get { return GetColumnValue<string>(Columns.SellerRemark); }
			set { SetColumnValue(Columns.SellerRemark, value); }
		}
		  
		[XmlAttribute("SellerSales")]
		[Bindable(true)]
		public string SellerSales 
		{
			get { return GetColumnValue<string>(Columns.SellerSales); }
			set { SetColumnValue(Columns.SellerSales, value); }
		}
		  
		[XmlAttribute("SellerLogoimgPath")]
		[Bindable(true)]
		public string SellerLogoimgPath 
		{
			get { return GetColumnValue<string>(Columns.SellerLogoimgPath); }
			set { SetColumnValue(Columns.SellerLogoimgPath, value); }
		}
		  
		[XmlAttribute("SellerVideoPath")]
		[Bindable(true)]
		public string SellerVideoPath 
		{
			get { return GetColumnValue<string>(Columns.SellerVideoPath); }
			set { SetColumnValue(Columns.SellerVideoPath, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("DefaultCommissionRate")]
		[Bindable(true)]
		public double? DefaultCommissionRate 
		{
			get { return GetColumnValue<double?>(Columns.DefaultCommissionRate); }
			set { SetColumnValue(Columns.DefaultCommissionRate, value); }
		}
		  
		[XmlAttribute("BillingCode")]
		[Bindable(true)]
		public string BillingCode 
		{
			get { return GetColumnValue<string>(Columns.BillingCode); }
			set { SetColumnValue(Columns.BillingCode, value); }
		}
		  
		[XmlAttribute("Weight")]
		[Bindable(true)]
		public int? Weight 
		{
			get { return GetColumnValue<int?>(Columns.Weight); }
			set { SetColumnValue(Columns.Weight, value); }
		}
		  
		[XmlAttribute("PostCkoutAction")]
		[Bindable(true)]
		public int PostCkoutAction 
		{
			get { return GetColumnValue<int>(Columns.PostCkoutAction); }
			set { SetColumnValue(Columns.PostCkoutAction, value); }
		}
		  
		[XmlAttribute("PostCkoutArgs")]
		[Bindable(true)]
		public string PostCkoutArgs 
		{
			get { return GetColumnValue<string>(Columns.PostCkoutArgs); }
			set { SetColumnValue(Columns.PostCkoutArgs, value); }
		}
		  
		[XmlAttribute("Coordinate")]
		[Bindable(true)]
		public string Coordinate 
		{
            get { return GetColumnValue<object>(Columns.Coordinate) != null ? GetColumnValue<object>(Columns.Coordinate).ToString() : string.Empty; }
            set { SetColumnValue(Columns.Coordinate, value); }
		}
		  
		[XmlAttribute("DeliveryMinuteGap")]
		[Bindable(true)]
		public int? DeliveryMinuteGap 
		{
			get { return GetColumnValue<int?>(Columns.DeliveryMinuteGap); }
			set { SetColumnValue(Columns.DeliveryMinuteGap, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int CityId 
		{
			get { return GetColumnValue<int>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("CompanyName")]
		[Bindable(true)]
		public string CompanyName 
		{
			get { return GetColumnValue<string>(Columns.CompanyName); }
			set { SetColumnValue(Columns.CompanyName, value); }
		}
		  
		[XmlAttribute("CompanyBossName")]
		[Bindable(true)]
		public string CompanyBossName 
		{
			get { return GetColumnValue<string>(Columns.CompanyBossName); }
			set { SetColumnValue(Columns.CompanyBossName, value); }
		}
		  
		[XmlAttribute("CompanyID")]
		[Bindable(true)]
		public string CompanyID 
		{
			get { return GetColumnValue<string>(Columns.CompanyID); }
			set { SetColumnValue(Columns.CompanyID, value); }
		}
		  
		[XmlAttribute("CompanyBankCode")]
		[Bindable(true)]
		public string CompanyBankCode 
		{
			get { return GetColumnValue<string>(Columns.CompanyBankCode); }
			set { SetColumnValue(Columns.CompanyBankCode, value); }
		}
		  
		[XmlAttribute("CompanyBranchCode")]
		[Bindable(true)]
		public string CompanyBranchCode 
		{
			get { return GetColumnValue<string>(Columns.CompanyBranchCode); }
			set { SetColumnValue(Columns.CompanyBranchCode, value); }
		}
		  
		[XmlAttribute("CompanyAccount")]
		[Bindable(true)]
		public string CompanyAccount 
		{
			get { return GetColumnValue<string>(Columns.CompanyAccount); }
			set { SetColumnValue(Columns.CompanyAccount, value); }
		}
		  
		[XmlAttribute("CompanyAccountName")]
		[Bindable(true)]
		public string CompanyAccountName 
		{
			get { return GetColumnValue<string>(Columns.CompanyAccountName); }
			set { SetColumnValue(Columns.CompanyAccountName, value); }
		}
		  
		[XmlAttribute("CompanyEmail")]
		[Bindable(true)]
		public string CompanyEmail 
		{
			get { return GetColumnValue<string>(Columns.CompanyEmail); }
			set { SetColumnValue(Columns.CompanyEmail, value); }
		}
		  
		[XmlAttribute("CompanyNotice")]
		[Bindable(true)]
		public string CompanyNotice 
		{
			get { return GetColumnValue<string>(Columns.CompanyNotice); }
			set { SetColumnValue(Columns.CompanyNotice, value); }
		}
		  
		[XmlAttribute("IsCloseDown")]
		[Bindable(true)]
		public bool IsCloseDown 
		{
			get { return GetColumnValue<bool>(Columns.IsCloseDown); }
			set { SetColumnValue(Columns.IsCloseDown, value); }
		}
		  
		[XmlAttribute("CloseDownDate")]
		[Bindable(true)]
		public DateTime? CloseDownDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.CloseDownDate); }
			set { SetColumnValue(Columns.CloseDownDate, value); }
		}
		  
		[XmlAttribute("SignCompanyID")]
		[Bindable(true)]
		public string SignCompanyID 
		{
			get { return GetColumnValue<string>(Columns.SignCompanyID); }
			set { SetColumnValue(Columns.SignCompanyID, value); }
		}
		  
		[XmlAttribute("SellerContactPerson")]
		[Bindable(true)]
		public string SellerContactPerson 
		{
			get { return GetColumnValue<string>(Columns.SellerContactPerson); }
			set { SetColumnValue(Columns.SellerContactPerson, value); }
		}
		  
		[XmlAttribute("ReturnedPersonName")]
		[Bindable(true)]
		public string ReturnedPersonName 
		{
			get { return GetColumnValue<string>(Columns.ReturnedPersonName); }
			set { SetColumnValue(Columns.ReturnedPersonName, value); }
		}
		  
		[XmlAttribute("ReturnedPersonTel")]
		[Bindable(true)]
		public string ReturnedPersonTel 
		{
			get { return GetColumnValue<string>(Columns.ReturnedPersonTel); }
			set { SetColumnValue(Columns.ReturnedPersonTel, value); }
		}
		  
		[XmlAttribute("ReturnedPersonEmail")]
		[Bindable(true)]
		public string ReturnedPersonEmail 
		{
			get { return GetColumnValue<string>(Columns.ReturnedPersonEmail); }
			set { SetColumnValue(Columns.ReturnedPersonEmail, value); }
		}
		  
		[XmlAttribute("AccountantName")]
		[Bindable(true)]
		public string AccountantName 
		{
			get { return GetColumnValue<string>(Columns.AccountantName); }
			set { SetColumnValue(Columns.AccountantName, value); }
		}
		  
		[XmlAttribute("AccountantTel")]
		[Bindable(true)]
		public string AccountantTel 
		{
			get { return GetColumnValue<string>(Columns.AccountantTel); }
			set { SetColumnValue(Columns.AccountantTel, value); }
		}
		  
		[XmlAttribute("TempStatus")]
		[Bindable(true)]
		public int TempStatus 
		{
			get { return GetColumnValue<int>(Columns.TempStatus); }
			set { SetColumnValue(Columns.TempStatus, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("NewCreated")]
		[Bindable(true)]
		public bool NewCreated 
		{
			get { return GetColumnValue<bool>(Columns.NewCreated); }
			set { SetColumnValue(Columns.NewCreated, value); }
		}
		  
		[XmlAttribute("ApplyId")]
		[Bindable(true)]
		public string ApplyId 
		{
			get { return GetColumnValue<string>(Columns.ApplyId); }
			set { SetColumnValue(Columns.ApplyId, value); }
		}
		  
		[XmlAttribute("ApplyTime")]
		[Bindable(true)]
		public DateTime? ApplyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ApplyTime); }
			set { SetColumnValue(Columns.ApplyTime, value); }
		}
		  
		[XmlAttribute("ReturnTime")]
		[Bindable(true)]
		public DateTime? ReturnTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ReturnTime); }
			set { SetColumnValue(Columns.ReturnTime, value); }
		}
		  
		[XmlAttribute("ApproveTime")]
		[Bindable(true)]
		public DateTime? ApproveTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ApproveTime); }
			set { SetColumnValue(Columns.ApproveTime, value); }
		}
		  
		[XmlAttribute("VbsPrefix")]
		[Bindable(true)]
		public string VbsPrefix 
		{
			get { return GetColumnValue<string>(Columns.VbsPrefix); }
			set { SetColumnValue(Columns.VbsPrefix, value); }
		}
		  
		[XmlAttribute("SellerCategory")]
		[Bindable(true)]
		public int? SellerCategory 
		{
			get { return GetColumnValue<int?>(Columns.SellerCategory); }
			set { SetColumnValue(Columns.SellerCategory, value); }
		}
		  
		[XmlAttribute("SellerConsumptionAvg")]
		[Bindable(true)]
		public int? SellerConsumptionAvg 
		{
			get { return GetColumnValue<int?>(Columns.SellerConsumptionAvg); }
			set { SetColumnValue(Columns.SellerConsumptionAvg, value); }
		}
		  
		[XmlAttribute("SellerLevel")]
		[Bindable(true)]
		public int? SellerLevel 
		{
			get { return GetColumnValue<int?>(Columns.SellerLevel); }
			set { SetColumnValue(Columns.SellerLevel, value); }
		}
		  
		[XmlAttribute("FmeNo")]
		[Bindable(true)]
		public int? FmeNo 
		{
			get { return GetColumnValue<int?>(Columns.FmeNo); }
			set { SetColumnValue(Columns.FmeNo, value); }
		}
		  
		[XmlAttribute("SalesId")]
		[Bindable(true)]
		public int? SalesId 
		{
			get { return GetColumnValue<int?>(Columns.SalesId); }
			set { SetColumnValue(Columns.SalesId, value); }
		}
		  
		[XmlAttribute("RemittanceType")]
		[Bindable(true)]
		public int? RemittanceType 
		{
			get { return GetColumnValue<int?>(Columns.RemittanceType); }
			set { SetColumnValue(Columns.RemittanceType, value); }
		}
		  
		[XmlAttribute("SellerFrom")]
		[Bindable(true)]
		public short? SellerFrom 
		{
			get { return GetColumnValue<short?>(Columns.SellerFrom); }
			set { SetColumnValue(Columns.SellerFrom, value); }
		}
		  
		[XmlAttribute("SellerPorperty")]
		[Bindable(true)]
		public string SellerPorperty 
		{
			get { return GetColumnValue<string>(Columns.SellerPorperty); }
			set { SetColumnValue(Columns.SellerPorperty, value); }
		}
		  
		[XmlAttribute("Contacts")]
		[Bindable(true)]
		public string Contacts 
		{
			get { return GetColumnValue<string>(Columns.Contacts); }
			set { SetColumnValue(Columns.Contacts, value); }
		}
		  
		[XmlAttribute("SellerInfo")]
		[Bindable(true)]
		public string SellerInfo 
		{
			get { return GetColumnValue<string>(Columns.SellerInfo); }
			set { SetColumnValue(Columns.SellerInfo, value); }
		}
		  
		[XmlAttribute("OauthClientAppId")]
		[Bindable(true)]
		public string OauthClientAppId 
		{
			get { return GetColumnValue<string>(Columns.OauthClientAppId); }
			set { SetColumnValue(Columns.OauthClientAppId, value); }
		}
		  
		[XmlAttribute("VendorReceiptType")]
		[Bindable(true)]
		public int? VendorReceiptType 
		{
			get { return GetColumnValue<int?>(Columns.VendorReceiptType); }
			set { SetColumnValue(Columns.VendorReceiptType, value); }
		}
		  
		[XmlAttribute("Othermessage")]
		[Bindable(true)]
		public string Othermessage 
		{
			get { return GetColumnValue<string>(Columns.Othermessage); }
			set { SetColumnValue(Columns.Othermessage, value); }
		}
		  
		[XmlAttribute("StoreTel")]
		[Bindable(true)]
		public string StoreTel 
		{
			get { return GetColumnValue<string>(Columns.StoreTel); }
			set { SetColumnValue(Columns.StoreTel, value); }
		}
		  
		[XmlAttribute("StoreAddress")]
		[Bindable(true)]
		public string StoreAddress 
		{
			get { return GetColumnValue<string>(Columns.StoreAddress); }
			set { SetColumnValue(Columns.StoreAddress, value); }
		}
		  
		[XmlAttribute("StoreStatus")]
		[Bindable(true)]
		public int StoreStatus 
		{
			get { return GetColumnValue<int>(Columns.StoreStatus); }
			set { SetColumnValue(Columns.StoreStatus, value); }
		}
		  
		[XmlAttribute("OpenTime")]
		[Bindable(true)]
		public string OpenTime 
		{
			get { return GetColumnValue<string>(Columns.OpenTime); }
			set { SetColumnValue(Columns.OpenTime, value); }
		}
		  
		[XmlAttribute("CloseDate")]
		[Bindable(true)]
		public string CloseDate 
		{
			get { return GetColumnValue<string>(Columns.CloseDate); }
			set { SetColumnValue(Columns.CloseDate, value); }
		}
		  
		[XmlAttribute("Mrt")]
		[Bindable(true)]
		public string Mrt 
		{
			get { return GetColumnValue<string>(Columns.Mrt); }
			set { SetColumnValue(Columns.Mrt, value); }
		}
		  
		[XmlAttribute("Car")]
		[Bindable(true)]
		public string Car 
		{
			get { return GetColumnValue<string>(Columns.Car); }
			set { SetColumnValue(Columns.Car, value); }
		}
		  
		[XmlAttribute("Bus")]
		[Bindable(true)]
		public string Bus 
		{
			get { return GetColumnValue<string>(Columns.Bus); }
			set { SetColumnValue(Columns.Bus, value); }
		}
		  
		[XmlAttribute("OtherVehicles")]
		[Bindable(true)]
		public string OtherVehicles 
		{
			get { return GetColumnValue<string>(Columns.OtherVehicles); }
			set { SetColumnValue(Columns.OtherVehicles, value); }
		}
		  
		[XmlAttribute("WebUrl")]
		[Bindable(true)]
		public string WebUrl 
		{
			get { return GetColumnValue<string>(Columns.WebUrl); }
			set { SetColumnValue(Columns.WebUrl, value); }
		}
		  
		[XmlAttribute("FacebookUrl")]
		[Bindable(true)]
		public string FacebookUrl 
		{
			get { return GetColumnValue<string>(Columns.FacebookUrl); }
			set { SetColumnValue(Columns.FacebookUrl, value); }
		}
		  
		[XmlAttribute("BlogUrl")]
		[Bindable(true)]
		public string BlogUrl 
		{
			get { return GetColumnValue<string>(Columns.BlogUrl); }
			set { SetColumnValue(Columns.BlogUrl, value); }
		}
		  
		[XmlAttribute("OtherUrl")]
		[Bindable(true)]
		public string OtherUrl 
		{
			get { return GetColumnValue<string>(Columns.OtherUrl); }
			set { SetColumnValue(Columns.OtherUrl, value); }
		}
		  
		[XmlAttribute("StoreRemark")]
		[Bindable(true)]
		public string StoreRemark 
		{
			get { return GetColumnValue<string>(Columns.StoreRemark); }
			set { SetColumnValue(Columns.StoreRemark, value); }
		}
		  
		[XmlAttribute("CreditcardAvailable")]
		[Bindable(true)]
		public bool CreditcardAvailable 
		{
			get { return GetColumnValue<bool>(Columns.CreditcardAvailable); }
			set { SetColumnValue(Columns.CreditcardAvailable, value); }
		}
		  
		[XmlAttribute("IsOpenReservationSetting")]
		[Bindable(true)]
		public bool IsOpenReservationSetting 
		{
			get { return GetColumnValue<bool>(Columns.IsOpenReservationSetting); }
			set { SetColumnValue(Columns.IsOpenReservationSetting, value); }
		}
		  
		[XmlAttribute("IsOpenBooking")]
		[Bindable(true)]
		public bool IsOpenBooking 
		{
			get { return GetColumnValue<bool>(Columns.IsOpenBooking); }
			set { SetColumnValue(Columns.IsOpenBooking, value); }
		}
		  
		[XmlAttribute("StoreCityId")]
		[Bindable(true)]
		public int? StoreCityId 
		{
			get { return GetColumnValue<int?>(Columns.StoreCityId); }
			set { SetColumnValue(Columns.StoreCityId, value); }
		}
		  
		[XmlAttribute("StoreTownshipId")]
		[Bindable(true)]
		public int? StoreTownshipId 
		{
			get { return GetColumnValue<int?>(Columns.StoreTownshipId); }
			set { SetColumnValue(Columns.StoreTownshipId, value); }
		}
		  
		[XmlAttribute("StoreRelationCode")]
		[Bindable(true)]
		public string StoreRelationCode 
		{
			get { return GetColumnValue<string>(Columns.StoreRelationCode); }
			set { SetColumnValue(Columns.StoreRelationCode, value); }
		}
		  
		[XmlAttribute("ReferralSale")]
		[Bindable(true)]
		public string ReferralSale 
		{
			get { return GetColumnValue<string>(Columns.ReferralSale); }
			set { SetColumnValue(Columns.ReferralSale, value); }
		}
		  
		[XmlAttribute("ReferralSaleCreateTime")]
		[Bindable(true)]
		public DateTime? ReferralSaleCreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ReferralSaleCreateTime); }
			set { SetColumnValue(Columns.ReferralSaleCreateTime, value); }
		}
		  
		[XmlAttribute("ReferralSaleBeginTime")]
		[Bindable(true)]
		public DateTime? ReferralSaleBeginTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ReferralSaleBeginTime); }
			set { SetColumnValue(Columns.ReferralSaleBeginTime, value); }
		}
		  
		[XmlAttribute("ReferralSaleEndTime")]
		[Bindable(true)]
		public DateTime? ReferralSaleEndTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ReferralSaleEndTime); }
			set { SetColumnValue(Columns.ReferralSaleEndTime, value); }
		}
		  
		[XmlAttribute("ReferralSalePercent")]
		[Bindable(true)]
		public int? ReferralSalePercent 
		{
			get { return GetColumnValue<int?>(Columns.ReferralSalePercent); }
			set { SetColumnValue(Columns.ReferralSalePercent, value); }
		}
		  
		[XmlAttribute("SellerLevelDetail")]
		[Bindable(true)]
		public string SellerLevelDetail 
		{
			get { return GetColumnValue<string>(Columns.SellerLevelDetail); }
			set { SetColumnValue(Columns.SellerLevelDetail, value); }
		}
		  
		[XmlAttribute("ContractVersionHouse")]
		[Bindable(true)]
		public string ContractVersionHouse 
		{
			get { return GetColumnValue<string>(Columns.ContractVersionHouse); }
			set { SetColumnValue(Columns.ContractVersionHouse, value); }
		}
		  
		[XmlAttribute("ContractCkeckTimeHouse")]
		[Bindable(true)]
		public DateTime? ContractCkeckTimeHouse 
		{
			get { return GetColumnValue<DateTime?>(Columns.ContractCkeckTimeHouse); }
			set { SetColumnValue(Columns.ContractCkeckTimeHouse, value); }
		}
		  
		[XmlAttribute("ItemPriceCondition")]
		[Bindable(true)]
		public string ItemPriceCondition 
		{
			get { return GetColumnValue<string>(Columns.ItemPriceCondition); }
			set { SetColumnValue(Columns.ItemPriceCondition, value); }
		}
		  
		[XmlAttribute("ContractVersionPpon")]
		[Bindable(true)]
		public string ContractVersionPpon 
		{
			get { return GetColumnValue<string>(Columns.ContractVersionPpon); }
			set { SetColumnValue(Columns.ContractVersionPpon, value); }
		}
		  
		[XmlAttribute("ContractCkeckTimePpon")]
		[Bindable(true)]
		public DateTime? ContractCkeckTimePpon 
		{
			get { return GetColumnValue<DateTime?>(Columns.ContractCkeckTimePpon); }
			set { SetColumnValue(Columns.ContractCkeckTimePpon, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerBossNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerTelColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerTel2Column
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerFaxColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerMobileColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerAddressColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerEmailColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerBlogColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerInvoiceColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerDescriptionColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerStatusColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn DepartmentColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerRemarkColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerSalesColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerLogoimgPathColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerVideoPathColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn DefaultCommissionRateColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn BillingCodeColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn WeightColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn PostCkoutActionColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn PostCkoutArgsColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn CoordinateColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryMinuteGapColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyNameColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyBossNameColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyIDColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyBankCodeColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyBranchCodeColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyAccountColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyAccountNameColumn
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyEmailColumn
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyNoticeColumn
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCloseDownColumn
        {
            get { return Schema.Columns[40]; }
        }
        
        
        
        public static TableSchema.TableColumn CloseDownDateColumn
        {
            get { return Schema.Columns[41]; }
        }
        
        
        
        public static TableSchema.TableColumn SignCompanyIDColumn
        {
            get { return Schema.Columns[42]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerContactPersonColumn
        {
            get { return Schema.Columns[43]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnedPersonNameColumn
        {
            get { return Schema.Columns[44]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnedPersonTelColumn
        {
            get { return Schema.Columns[45]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnedPersonEmailColumn
        {
            get { return Schema.Columns[46]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountantNameColumn
        {
            get { return Schema.Columns[47]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountantTelColumn
        {
            get { return Schema.Columns[48]; }
        }
        
        
        
        public static TableSchema.TableColumn TempStatusColumn
        {
            get { return Schema.Columns[49]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[50]; }
        }
        
        
        
        public static TableSchema.TableColumn NewCreatedColumn
        {
            get { return Schema.Columns[51]; }
        }
        
        
        
        public static TableSchema.TableColumn ApplyIdColumn
        {
            get { return Schema.Columns[52]; }
        }
        
        
        
        public static TableSchema.TableColumn ApplyTimeColumn
        {
            get { return Schema.Columns[53]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnTimeColumn
        {
            get { return Schema.Columns[54]; }
        }
        
        
        
        public static TableSchema.TableColumn ApproveTimeColumn
        {
            get { return Schema.Columns[55]; }
        }
        
        
        
        public static TableSchema.TableColumn VbsPrefixColumn
        {
            get { return Schema.Columns[56]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerCategoryColumn
        {
            get { return Schema.Columns[57]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerConsumptionAvgColumn
        {
            get { return Schema.Columns[58]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerLevelColumn
        {
            get { return Schema.Columns[59]; }
        }
        
        
        
        public static TableSchema.TableColumn FmeNoColumn
        {
            get { return Schema.Columns[60]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesIdColumn
        {
            get { return Schema.Columns[61]; }
        }
        
        
        
        public static TableSchema.TableColumn RemittanceTypeColumn
        {
            get { return Schema.Columns[62]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerFromColumn
        {
            get { return Schema.Columns[63]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerPorpertyColumn
        {
            get { return Schema.Columns[64]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactsColumn
        {
            get { return Schema.Columns[65]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerInfoColumn
        {
            get { return Schema.Columns[66]; }
        }
        
        
        
        public static TableSchema.TableColumn OauthClientAppIdColumn
        {
            get { return Schema.Columns[67]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorReceiptTypeColumn
        {
            get { return Schema.Columns[68]; }
        }
        
        
        
        public static TableSchema.TableColumn OthermessageColumn
        {
            get { return Schema.Columns[69]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreTelColumn
        {
            get { return Schema.Columns[70]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreAddressColumn
        {
            get { return Schema.Columns[71]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreStatusColumn
        {
            get { return Schema.Columns[72]; }
        }
        
        
        
        public static TableSchema.TableColumn OpenTimeColumn
        {
            get { return Schema.Columns[73]; }
        }
        
        
        
        public static TableSchema.TableColumn CloseDateColumn
        {
            get { return Schema.Columns[74]; }
        }
        
        
        
        public static TableSchema.TableColumn MrtColumn
        {
            get { return Schema.Columns[75]; }
        }
        
        
        
        public static TableSchema.TableColumn CarColumn
        {
            get { return Schema.Columns[76]; }
        }
        
        
        
        public static TableSchema.TableColumn BusColumn
        {
            get { return Schema.Columns[77]; }
        }
        
        
        
        public static TableSchema.TableColumn OtherVehiclesColumn
        {
            get { return Schema.Columns[78]; }
        }
        
        
        
        public static TableSchema.TableColumn WebUrlColumn
        {
            get { return Schema.Columns[79]; }
        }
        
        
        
        public static TableSchema.TableColumn FacebookUrlColumn
        {
            get { return Schema.Columns[80]; }
        }
        
        
        
        public static TableSchema.TableColumn BlogUrlColumn
        {
            get { return Schema.Columns[81]; }
        }
        
        
        
        public static TableSchema.TableColumn OtherUrlColumn
        {
            get { return Schema.Columns[82]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreRemarkColumn
        {
            get { return Schema.Columns[83]; }
        }
        
        
        
        public static TableSchema.TableColumn CreditcardAvailableColumn
        {
            get { return Schema.Columns[84]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOpenReservationSettingColumn
        {
            get { return Schema.Columns[85]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOpenBookingColumn
        {
            get { return Schema.Columns[86]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreCityIdColumn
        {
            get { return Schema.Columns[87]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreTownshipIdColumn
        {
            get { return Schema.Columns[88]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreRelationCodeColumn
        {
            get { return Schema.Columns[89]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferralSaleColumn
        {
            get { return Schema.Columns[90]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferralSaleCreateTimeColumn
        {
            get { return Schema.Columns[91]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferralSaleBeginTimeColumn
        {
            get { return Schema.Columns[92]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferralSaleEndTimeColumn
        {
            get { return Schema.Columns[93]; }
        }
        
        
        
        public static TableSchema.TableColumn ReferralSalePercentColumn
        {
            get { return Schema.Columns[94]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerLevelDetailColumn
        {
            get { return Schema.Columns[95]; }
        }
        
        
        
        public static TableSchema.TableColumn ContractVersionHouseColumn
        {
            get { return Schema.Columns[96]; }
        }
        
        
        
        public static TableSchema.TableColumn ContractCkeckTimeHouseColumn
        {
            get { return Schema.Columns[97]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemPriceConditionColumn
        {
            get { return Schema.Columns[98]; }
        }
        
        
        
        public static TableSchema.TableColumn ContractVersionPponColumn
        {
            get { return Schema.Columns[99]; }
        }
        
        
        
        public static TableSchema.TableColumn ContractCkeckTimePponColumn
        {
            get { return Schema.Columns[100]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string SellerId = @"seller_id";
			 public static string SellerName = @"seller_name";
			 public static string SellerBossName = @"seller_boss_name";
			 public static string SellerTel = @"seller_tel";
			 public static string SellerTel2 = @"seller_tel2";
			 public static string SellerFax = @"seller_fax";
			 public static string SellerMobile = @"seller_mobile";
			 public static string SellerAddress = @"seller_address";
			 public static string SellerEmail = @"seller_email";
			 public static string SellerBlog = @"seller_blog";
			 public static string SellerInvoice = @"seller_invoice";
			 public static string SellerDescription = @"seller_description";
			 public static string SellerStatus = @"seller_status";
			 public static string Department = @"department";
			 public static string SellerRemark = @"seller_remark";
			 public static string SellerSales = @"seller_sales";
			 public static string SellerLogoimgPath = @"seller_logoimg_path";
			 public static string SellerVideoPath = @"seller_video_path";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string DefaultCommissionRate = @"default_commission_rate";
			 public static string BillingCode = @"billing_code";
			 public static string Weight = @"weight";
			 public static string PostCkoutAction = @"post_ckout_action";
			 public static string PostCkoutArgs = @"post_ckout_args";
			 public static string Coordinate = @"coordinate";
			 public static string DeliveryMinuteGap = @"delivery_minute_gap";
			 public static string CityId = @"city_id";
			 public static string CompanyName = @"CompanyName";
			 public static string CompanyBossName = @"CompanyBossName";
			 public static string CompanyID = @"CompanyID";
			 public static string CompanyBankCode = @"CompanyBankCode";
			 public static string CompanyBranchCode = @"CompanyBranchCode";
			 public static string CompanyAccount = @"CompanyAccount";
			 public static string CompanyAccountName = @"CompanyAccountName";
			 public static string CompanyEmail = @"CompanyEmail";
			 public static string CompanyNotice = @"CompanyNotice";
			 public static string IsCloseDown = @"is_close_down";
			 public static string CloseDownDate = @"close_down_date";
			 public static string SignCompanyID = @"SignCompanyID";
			 public static string SellerContactPerson = @"seller_contact_person";
			 public static string ReturnedPersonName = @"returned_person_name";
			 public static string ReturnedPersonTel = @"returned_person_tel";
			 public static string ReturnedPersonEmail = @"returned_person_email";
			 public static string AccountantName = @"accountant_name";
			 public static string AccountantTel = @"accountant_tel";
			 public static string TempStatus = @"temp_status";
			 public static string Message = @"message";
			 public static string NewCreated = @"new_created";
			 public static string ApplyId = @"apply_id";
			 public static string ApplyTime = @"apply_time";
			 public static string ReturnTime = @"return_time";
			 public static string ApproveTime = @"approve_time";
			 public static string VbsPrefix = @"vbs_prefix";
			 public static string SellerCategory = @"seller_category";
			 public static string SellerConsumptionAvg = @"seller_consumption_avg";
			 public static string SellerLevel = @"seller_level";
			 public static string FmeNo = @"fme_no";
			 public static string SalesId = @"sales_id";
			 public static string RemittanceType = @"remittance_type";
			 public static string SellerFrom = @"seller_from";
			 public static string SellerPorperty = @"seller_porperty";
			 public static string Contacts = @"contacts";
			 public static string SellerInfo = @"seller_info";
			 public static string OauthClientAppId = @"oauth_client_app_id";
			 public static string VendorReceiptType = @"vendor_receipt_type";
			 public static string Othermessage = @"othermessage";
			 public static string StoreTel = @"store_tel";
			 public static string StoreAddress = @"store_address";
			 public static string StoreStatus = @"store_status";
			 public static string OpenTime = @"open_time";
			 public static string CloseDate = @"close_date";
			 public static string Mrt = @"mrt";
			 public static string Car = @"car";
			 public static string Bus = @"bus";
			 public static string OtherVehicles = @"other_vehicles";
			 public static string WebUrl = @"web_url";
			 public static string FacebookUrl = @"facebook_url";
			 public static string BlogUrl = @"blog_url";
			 public static string OtherUrl = @"other_url";
			 public static string StoreRemark = @"store_remark";
			 public static string CreditcardAvailable = @"creditcard_available";
			 public static string IsOpenReservationSetting = @"is_open_reservation_setting";
			 public static string IsOpenBooking = @"is_open_booking";
			 public static string StoreCityId = @"store_city_id";
			 public static string StoreTownshipId = @"store_township_id";
			 public static string StoreRelationCode = @"store_relation_code";
			 public static string ReferralSale = @"referral_sale";
			 public static string ReferralSaleCreateTime = @"referral_sale_create_time";
			 public static string ReferralSaleBeginTime = @"referral_sale_begin_time";
			 public static string ReferralSaleEndTime = @"referral_sale_end_time";
			 public static string ReferralSalePercent = @"referral_sale_percent";
			 public static string SellerLevelDetail = @"seller_level_detail";
			 public static string ContractVersionHouse = @"contract_version_house";
			 public static string ContractCkeckTimeHouse = @"contract_ckeck_time_house";
			 public static string ItemPriceCondition = @"item_price_condition";
			 public static string ContractVersionPpon = @"contract_version_ppon";
			 public static string ContractCkeckTimePpon = @"contract_ckeck_time_ppon";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

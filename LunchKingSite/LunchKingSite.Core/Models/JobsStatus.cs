using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the JobsStatus class.
	/// </summary>
    [Serializable]
	public partial class JobsStatusCollection : RepositoryList<JobsStatus, JobsStatusCollection>
	{	   
		public JobsStatusCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>JobsStatusCollection</returns>
		public JobsStatusCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                JobsStatus o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the jobs_status table.
	/// </summary>
	
	[Serializable]
	public partial class JobsStatus : RepositoryRecord<JobsStatus>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public JobsStatus()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public JobsStatus(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("jobs_status", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarClassName = new TableSchema.TableColumn(schema);
				colvarClassName.ColumnName = "ClassName";
				colvarClassName.DataType = DbType.AnsiString;
				colvarClassName.MaxLength = 100;
				colvarClassName.AutoIncrement = false;
				colvarClassName.IsNullable = false;
				colvarClassName.IsPrimaryKey = true;
				colvarClassName.IsForeignKey = false;
				colvarClassName.IsReadOnly = false;
				colvarClassName.DefaultSetting = @"";
				colvarClassName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarClassName);
				
				TableSchema.TableColumn colvarLastTime = new TableSchema.TableColumn(schema);
				colvarLastTime.ColumnName = "LastTime";
				colvarLastTime.DataType = DbType.DateTime;
				colvarLastTime.MaxLength = 0;
				colvarLastTime.AutoIncrement = false;
				colvarLastTime.IsNullable = true;
				colvarLastTime.IsPrimaryKey = false;
				colvarLastTime.IsForeignKey = false;
				colvarLastTime.IsReadOnly = false;
				colvarLastTime.DefaultSetting = @"";
				colvarLastTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastTime);
				
				TableSchema.TableColumn colvarExecutionTime = new TableSchema.TableColumn(schema);
				colvarExecutionTime.ColumnName = "ExecutionTime";
				colvarExecutionTime.DataType = DbType.Double;
				colvarExecutionTime.MaxLength = 0;
				colvarExecutionTime.AutoIncrement = false;
				colvarExecutionTime.IsNullable = true;
				colvarExecutionTime.IsPrimaryKey = false;
				colvarExecutionTime.IsForeignKey = false;
				colvarExecutionTime.IsReadOnly = false;
				colvarExecutionTime.DefaultSetting = @"";
				colvarExecutionTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExecutionTime);
				
				TableSchema.TableColumn colvarProcessing = new TableSchema.TableColumn(schema);
				colvarProcessing.ColumnName = "processing";
				colvarProcessing.DataType = DbType.Boolean;
				colvarProcessing.MaxLength = 0;
				colvarProcessing.AutoIncrement = false;
				colvarProcessing.IsNullable = false;
				colvarProcessing.IsPrimaryKey = false;
				colvarProcessing.IsForeignKey = false;
				colvarProcessing.IsReadOnly = false;
				
						colvarProcessing.DefaultSetting = @"((0))";
				colvarProcessing.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProcessing);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				
						colvarModifyTime.DefaultSetting = @"(getdate())";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("jobs_status",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("ClassName")]
		[Bindable(true)]
		public string ClassName 
		{
			get { return GetColumnValue<string>(Columns.ClassName); }
			set { SetColumnValue(Columns.ClassName, value); }
		}
		
		[XmlAttribute("LastTime")]
		[Bindable(true)]
		public DateTime? LastTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastTime); }
			set { SetColumnValue(Columns.LastTime, value); }
		}
		
		[XmlAttribute("ExecutionTime")]
		[Bindable(true)]
		public double? ExecutionTime 
		{
			get { return GetColumnValue<double?>(Columns.ExecutionTime); }
			set { SetColumnValue(Columns.ExecutionTime, value); }
		}
		
		[XmlAttribute("Processing")]
		[Bindable(true)]
		public bool Processing 
		{
			get { return GetColumnValue<bool>(Columns.Processing); }
			set { SetColumnValue(Columns.Processing, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn ClassNameColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn LastTimeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ExecutionTimeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ProcessingColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string ClassName = @"ClassName";
			 public static string LastTime = @"LastTime";
			 public static string ExecutionTime = @"ExecutionTime";
			 public static string Processing = @"processing";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

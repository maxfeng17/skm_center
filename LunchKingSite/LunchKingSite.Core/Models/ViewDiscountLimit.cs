using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewDiscountLimit class.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountLimitCollection : ReadOnlyList<ViewDiscountLimit, ViewDiscountLimitCollection>
    {
        public ViewDiscountLimitCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_discount_limit view.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountLimit : ReadOnlyRecord<ViewDiscountLimit>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_discount_limit", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;

                schema.Columns.Add(colvarBid);

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;

                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;

                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.AnsiString;
                colvarModifyId.MaxLength = 50;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = false;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;

                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;

                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarGrossMargin = new TableSchema.TableColumn(schema);
                colvarGrossMargin.ColumnName = "gross_margin";
                colvarGrossMargin.DataType = DbType.Currency;
                colvarGrossMargin.MaxLength = 0;
                colvarGrossMargin.AutoIncrement = false;
                colvarGrossMargin.IsNullable = true;
                colvarGrossMargin.IsPrimaryKey = false;
                colvarGrossMargin.IsForeignKey = false;
                colvarGrossMargin.IsReadOnly = false;

                schema.Columns.Add(colvarGrossMargin);

                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;

                schema.Columns.Add(colvarCouponUsage);

                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourOrderTimeE);

                TableSchema.TableColumn colvarCidList = new TableSchema.TableColumn(schema);
                colvarCidList.ColumnName = "cid_list";
                colvarCidList.DataType = DbType.String;
                colvarCidList.MaxLength = -1;
                colvarCidList.AutoIncrement = false;
                colvarCidList.IsNullable = true;
                colvarCidList.IsPrimaryKey = false;
                colvarCidList.IsForeignKey = false;
                colvarCidList.IsReadOnly = false;

                schema.Columns.Add(colvarCidList);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_discount_limit", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewDiscountLimit()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewDiscountLimit(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewDiscountLimit(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewDiscountLimit(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid
        {
            get
            {
                return GetColumnValue<Guid>("bid");
            }
            set
            {
                SetColumnValue("bid", value);
            }
        }

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get
            {
                return GetColumnValue<int>("type");
            }
            set
            {
                SetColumnValue("type", value);
            }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get
            {
                return GetColumnValue<string>("create_id");
            }
            set
            {
                SetColumnValue("create_id", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get
            {
                return GetColumnValue<string>("modify_id");
            }
            set
            {
                SetColumnValue("modify_id", value);
            }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime
        {
            get
            {
                return GetColumnValue<DateTime>("modify_time");
            }
            set
            {
                SetColumnValue("modify_time", value);
            }
        }

        [XmlAttribute("GrossMargin")]
        [Bindable(true)]
        public decimal? GrossMargin
        {
            get
            {
                return GetColumnValue<decimal?>("gross_margin");
            }
            set
            {
                SetColumnValue("gross_margin", value);
            }
        }

        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage
        {
            get
            {
                return GetColumnValue<string>("coupon_usage");
            }
            set
            {
                SetColumnValue("coupon_usage", value);
            }
        }

        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE
        {
            get
            {
                return GetColumnValue<DateTime>("business_hour_order_time_e");
            }
            set
            {
                SetColumnValue("business_hour_order_time_e", value);
            }
        }

        [XmlAttribute("CidList")]
        [Bindable(true)]
        public string CidList
        {
            get
            {
                return GetColumnValue<string>("cid_list");
            }
            set
            {
                SetColumnValue("cid_list", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Bid = @"bid";

            public static string Id = @"id";

            public static string Type = @"type";

            public static string CreateId = @"create_id";

            public static string CreateTime = @"create_time";

            public static string ModifyId = @"modify_id";

            public static string ModifyTime = @"modify_time";

            public static string GrossMargin = @"gross_margin";

            public static string CouponUsage = @"coupon_usage";

            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";

            public static string CidList = @"cid_list";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the WmsPackFee class.
	/// </summary>
    [Serializable]
	public partial class WmsPackFeeCollection : RepositoryList<WmsPackFee, WmsPackFeeCollection>
	{	   
		public WmsPackFeeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>WmsPackFeeCollection</returns>
		public WmsPackFeeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                WmsPackFee o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the wms_pack_fee table.
	/// </summary>
	[Serializable]
	public partial class WmsPackFee : RepositoryRecord<WmsPackFee>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public WmsPackFee()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public WmsPackFee(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("wms_pack_fee", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderDate = new TableSchema.TableColumn(schema);
				colvarOrderDate.ColumnName = "order_date";
				colvarOrderDate.DataType = DbType.DateTime;
				colvarOrderDate.MaxLength = 0;
				colvarOrderDate.AutoIncrement = false;
				colvarOrderDate.IsNullable = false;
				colvarOrderDate.IsPrimaryKey = false;
				colvarOrderDate.IsForeignKey = false;
				colvarOrderDate.IsReadOnly = false;
				colvarOrderDate.DefaultSetting = @"";
				colvarOrderDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderDate);
				
				TableSchema.TableColumn colvarVendorOrderId = new TableSchema.TableColumn(schema);
				colvarVendorOrderId.ColumnName = "vendor_order_id";
				colvarVendorOrderId.DataType = DbType.String;
				colvarVendorOrderId.MaxLength = 66;
				colvarVendorOrderId.AutoIncrement = false;
				colvarVendorOrderId.IsNullable = false;
				colvarVendorOrderId.IsPrimaryKey = false;
				colvarVendorOrderId.IsForeignKey = false;
				colvarVendorOrderId.IsReadOnly = false;
				colvarVendorOrderId.DefaultSetting = @"";
				colvarVendorOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorOrderId);
				
				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.String;
				colvarOrderId.MaxLength = 24;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = false;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);
				
				TableSchema.TableColumn colvarFee = new TableSchema.TableColumn(schema);
				colvarFee.ColumnName = "fee";
				colvarFee.DataType = DbType.Int32;
				colvarFee.MaxLength = 0;
				colvarFee.AutoIncrement = false;
				colvarFee.IsNullable = true;
				colvarFee.IsPrimaryKey = false;
				colvarFee.IsForeignKey = false;
				colvarFee.IsReadOnly = false;
				colvarFee.DefaultSetting = @"";
				colvarFee.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFee);
				
				TableSchema.TableColumn colvarAccountingDate = new TableSchema.TableColumn(schema);
				colvarAccountingDate.ColumnName = "accounting_date";
				colvarAccountingDate.DataType = DbType.DateTime;
				colvarAccountingDate.MaxLength = 0;
				colvarAccountingDate.AutoIncrement = false;
				colvarAccountingDate.IsNullable = true;
				colvarAccountingDate.IsPrimaryKey = false;
				colvarAccountingDate.IsForeignKey = false;
				colvarAccountingDate.IsReadOnly = false;
				colvarAccountingDate.DefaultSetting = @"";
				colvarAccountingDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountingDate);
				
				TableSchema.TableColumn colvarVendorPid = new TableSchema.TableColumn(schema);
				colvarVendorPid.ColumnName = "vendor_pid";
				colvarVendorPid.DataType = DbType.String;
				colvarVendorPid.MaxLength = 50;
				colvarVendorPid.AutoIncrement = false;
				colvarVendorPid.IsNullable = true;
				colvarVendorPid.IsPrimaryKey = false;
				colvarVendorPid.IsForeignKey = false;
				colvarVendorPid.IsReadOnly = false;
				colvarVendorPid.DefaultSetting = @"";
				colvarVendorPid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorPid);
				
				TableSchema.TableColumn colvarProdId = new TableSchema.TableColumn(schema);
				colvarProdId.ColumnName = "prod_id";
				colvarProdId.DataType = DbType.String;
				colvarProdId.MaxLength = 20;
				colvarProdId.AutoIncrement = false;
				colvarProdId.IsNullable = true;
				colvarProdId.IsPrimaryKey = false;
				colvarProdId.IsForeignKey = false;
				colvarProdId.IsReadOnly = false;
				colvarProdId.DefaultSetting = @"";
				colvarProdId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProdId);
				
				TableSchema.TableColumn colvarProdName = new TableSchema.TableColumn(schema);
				colvarProdName.ColumnName = "prod_name";
				colvarProdName.DataType = DbType.String;
				colvarProdName.MaxLength = 256;
				colvarProdName.AutoIncrement = false;
				colvarProdName.IsNullable = true;
				colvarProdName.IsPrimaryKey = false;
				colvarProdName.IsForeignKey = false;
				colvarProdName.IsReadOnly = false;
				colvarProdName.DefaultSetting = @"";
				colvarProdName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProdName);
				
				TableSchema.TableColumn colvarProdSpec = new TableSchema.TableColumn(schema);
				colvarProdSpec.ColumnName = "prod_spec";
				colvarProdSpec.DataType = DbType.String;
				colvarProdSpec.MaxLength = 256;
				colvarProdSpec.AutoIncrement = false;
				colvarProdSpec.IsNullable = true;
				colvarProdSpec.IsPrimaryKey = false;
				colvarProdSpec.IsForeignKey = false;
				colvarProdSpec.IsReadOnly = false;
				colvarProdSpec.DefaultSetting = @"";
				colvarProdSpec.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProdSpec);
				
				TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
				colvarQty.ColumnName = "qty";
				colvarQty.DataType = DbType.Int32;
				colvarQty.MaxLength = 0;
				colvarQty.AutoIncrement = false;
				colvarQty.IsNullable = true;
				colvarQty.IsPrimaryKey = false;
				colvarQty.IsForeignKey = false;
				colvarQty.IsReadOnly = false;
				colvarQty.DefaultSetting = @"";
				colvarQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQty);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarBalanceSheetId = new TableSchema.TableColumn(schema);
				colvarBalanceSheetId.ColumnName = "balance_sheet_id";
				colvarBalanceSheetId.DataType = DbType.Int32;
				colvarBalanceSheetId.MaxLength = 0;
				colvarBalanceSheetId.AutoIncrement = false;
				colvarBalanceSheetId.IsNullable = true;
				colvarBalanceSheetId.IsPrimaryKey = false;
				colvarBalanceSheetId.IsForeignKey = false;
				colvarBalanceSheetId.IsReadOnly = false;
				colvarBalanceSheetId.DefaultSetting = @"";
				colvarBalanceSheetId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBalanceSheetId);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("wms_pack_fee",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderDate")]
		[Bindable(true)]
		public DateTime OrderDate 
		{
			get { return GetColumnValue<DateTime>(Columns.OrderDate); }
			set { SetColumnValue(Columns.OrderDate, value); }
		}
		  
		[XmlAttribute("VendorOrderId")]
		[Bindable(true)]
		public string VendorOrderId 
		{
			get { return GetColumnValue<string>(Columns.VendorOrderId); }
			set { SetColumnValue(Columns.VendorOrderId, value); }
		}
		  
		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId 
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}
		  
		[XmlAttribute("Fee")]
		[Bindable(true)]
		public int? Fee 
		{
			get { return GetColumnValue<int?>(Columns.Fee); }
			set { SetColumnValue(Columns.Fee, value); }
		}
		  
		[XmlAttribute("AccountingDate")]
		[Bindable(true)]
		public DateTime? AccountingDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.AccountingDate); }
			set { SetColumnValue(Columns.AccountingDate, value); }
		}
		  
		[XmlAttribute("VendorPid")]
		[Bindable(true)]
		public string VendorPid 
		{
			get { return GetColumnValue<string>(Columns.VendorPid); }
			set { SetColumnValue(Columns.VendorPid, value); }
		}
		  
		[XmlAttribute("ProdId")]
		[Bindable(true)]
		public string ProdId 
		{
			get { return GetColumnValue<string>(Columns.ProdId); }
			set { SetColumnValue(Columns.ProdId, value); }
		}
		  
		[XmlAttribute("ProdName")]
		[Bindable(true)]
		public string ProdName 
		{
			get { return GetColumnValue<string>(Columns.ProdName); }
			set { SetColumnValue(Columns.ProdName, value); }
		}
		  
		[XmlAttribute("ProdSpec")]
		[Bindable(true)]
		public string ProdSpec 
		{
			get { return GetColumnValue<string>(Columns.ProdSpec); }
			set { SetColumnValue(Columns.ProdSpec, value); }
		}
		  
		[XmlAttribute("Qty")]
		[Bindable(true)]
		public int? Qty 
		{
			get { return GetColumnValue<int?>(Columns.Qty); }
			set { SetColumnValue(Columns.Qty, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("BalanceSheetId")]
		[Bindable(true)]
		public int? BalanceSheetId 
		{
			get { return GetColumnValue<int?>(Columns.BalanceSheetId); }
			set { SetColumnValue(Columns.BalanceSheetId, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderDateColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorOrderIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn FeeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountingDateColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorPidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ProdIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ProdNameColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ProdSpecColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn QtyColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn BalanceSheetIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderDate = @"order_date";
			 public static string VendorOrderId = @"vendor_order_id";
			 public static string OrderId = @"order_id";
			 public static string Fee = @"fee";
			 public static string AccountingDate = @"accounting_date";
			 public static string VendorPid = @"vendor_pid";
			 public static string ProdId = @"prod_id";
			 public static string ProdName = @"prod_name";
			 public static string ProdSpec = @"prod_spec";
			 public static string Qty = @"qty";
			 public static string OrderGuid = @"order_guid";
			 public static string BalanceSheetId = @"balance_sheet_id";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponHotDealPreparation class.
    /// </summary>
    [Serializable]
    public partial class ViewPponHotDealPreparationCollection : ReadOnlyList<ViewPponHotDealPreparation, ViewPponHotDealPreparationCollection>
    {        
        public ViewPponHotDealPreparationCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_hot_deal_preparation view.
    /// </summary>
    [Serializable]
    public partial class ViewPponHotDealPreparation : ReadOnlyRecord<ViewPponHotDealPreparation>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_hot_deal_preparation", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarMainBid = new TableSchema.TableColumn(schema);
                colvarMainBid.ColumnName = "main_bid";
                colvarMainBid.DataType = DbType.Guid;
                colvarMainBid.MaxLength = 0;
                colvarMainBid.AutoIncrement = false;
                colvarMainBid.IsNullable = true;
                colvarMainBid.IsPrimaryKey = false;
                colvarMainBid.IsForeignKey = false;
                colvarMainBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainBid);
                
                TableSchema.TableColumn colvarOrderedTotal = new TableSchema.TableColumn(schema);
                colvarOrderedTotal.ColumnName = "ordered_total";
                colvarOrderedTotal.DataType = DbType.Int32;
                colvarOrderedTotal.MaxLength = 0;
                colvarOrderedTotal.AutoIncrement = false;
                colvarOrderedTotal.IsNullable = true;
                colvarOrderedTotal.IsPrimaryKey = false;
                colvarOrderedTotal.IsForeignKey = false;
                colvarOrderedTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderedTotal);
                
                TableSchema.TableColumn colvarSellDays = new TableSchema.TableColumn(schema);
                colvarSellDays.ColumnName = "sell_days";
                colvarSellDays.DataType = DbType.Decimal;
                colvarSellDays.MaxLength = 0;
                colvarSellDays.AutoIncrement = false;
                colvarSellDays.IsNullable = true;
                colvarSellDays.IsPrimaryKey = false;
                colvarSellDays.IsForeignKey = false;
                colvarSellDays.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellDays);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarEffectiveStart = new TableSchema.TableColumn(schema);
                colvarEffectiveStart.ColumnName = "effective_start";
                colvarEffectiveStart.DataType = DbType.DateTime;
                colvarEffectiveStart.MaxLength = 0;
                colvarEffectiveStart.AutoIncrement = false;
                colvarEffectiveStart.IsNullable = false;
                colvarEffectiveStart.IsPrimaryKey = false;
                colvarEffectiveStart.IsForeignKey = false;
                colvarEffectiveStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarEffectiveStart);
                
                TableSchema.TableColumn colvarEffectiveEnd = new TableSchema.TableColumn(schema);
                colvarEffectiveEnd.ColumnName = "effective_end";
                colvarEffectiveEnd.DataType = DbType.DateTime;
                colvarEffectiveEnd.MaxLength = 0;
                colvarEffectiveEnd.AutoIncrement = false;
                colvarEffectiveEnd.IsNullable = false;
                colvarEffectiveEnd.IsPrimaryKey = false;
                colvarEffectiveEnd.IsForeignKey = false;
                colvarEffectiveEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarEffectiveEnd);
                
                TableSchema.TableColumn colvarSalesDate = new TableSchema.TableColumn(schema);
                colvarSalesDate.ColumnName = "sales_date";
                colvarSalesDate.DataType = DbType.DateTime;
                colvarSalesDate.MaxLength = 0;
                colvarSalesDate.AutoIncrement = false;
                colvarSalesDate.IsNullable = true;
                colvarSalesDate.IsPrimaryKey = false;
                colvarSalesDate.IsForeignKey = false;
                colvarSalesDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarSalesDate);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_hot_deal_preparation",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponHotDealPreparation()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponHotDealPreparation(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponHotDealPreparation(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponHotDealPreparation(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("MainBid")]
        [Bindable(true)]
        public Guid? MainBid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("main_bid");
		    }
            set 
		    {
			    SetColumnValue("main_bid", value);
            }
        }
	      
        [XmlAttribute("OrderedTotal")]
        [Bindable(true)]
        public int? OrderedTotal 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ordered_total");
		    }
            set 
		    {
			    SetColumnValue("ordered_total", value);
            }
        }
	      
        [XmlAttribute("SellDays")]
        [Bindable(true)]
        public decimal? SellDays 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("sell_days");
		    }
            set 
		    {
			    SetColumnValue("sell_days", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("EffectiveStart")]
        [Bindable(true)]
        public DateTime EffectiveStart 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("effective_start");
		    }
            set 
		    {
			    SetColumnValue("effective_start", value);
            }
        }
	      
        [XmlAttribute("EffectiveEnd")]
        [Bindable(true)]
        public DateTime EffectiveEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("effective_end");
		    }
            set 
		    {
			    SetColumnValue("effective_end", value);
            }
        }
	      
        [XmlAttribute("SalesDate")]
        [Bindable(true)]
        public DateTime? SalesDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("sales_date");
		    }
            set 
		    {
			    SetColumnValue("sales_date", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CityId = @"city_id";
            
            public static string MainBid = @"main_bid";
            
            public static string OrderedTotal = @"ordered_total";
            
            public static string SellDays = @"sell_days";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string EffectiveStart = @"effective_start";
            
            public static string EffectiveEnd = @"effective_end";
            
            public static string SalesDate = @"sales_date";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Coupon class.
	/// </summary>
    [Serializable]
	public partial class CouponCollection : RepositoryList<Coupon, CouponCollection>
	{	   
		public CouponCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CouponCollection</returns>
		public CouponCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Coupon o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the coupon table.
	/// </summary>
	[Serializable]
	public partial class Coupon : RepositoryRecord<Coupon>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Coupon()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Coupon(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("coupon", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderDetailId = new TableSchema.TableColumn(schema);
				colvarOrderDetailId.ColumnName = "order_detail_id";
				colvarOrderDetailId.DataType = DbType.Guid;
				colvarOrderDetailId.MaxLength = 0;
				colvarOrderDetailId.AutoIncrement = false;
				colvarOrderDetailId.IsNullable = false;
				colvarOrderDetailId.IsPrimaryKey = false;
				colvarOrderDetailId.IsForeignKey = true;
				colvarOrderDetailId.IsReadOnly = false;
				colvarOrderDetailId.DefaultSetting = @"";
				
					colvarOrderDetailId.ForeignKeyTableName = "order_detail";
				schema.Columns.Add(colvarOrderDetailId);
				
				TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
				colvarSequenceNumber.ColumnName = "sequence_number";
				colvarSequenceNumber.DataType = DbType.AnsiString;
				colvarSequenceNumber.MaxLength = 50;
				colvarSequenceNumber.AutoIncrement = false;
				colvarSequenceNumber.IsNullable = false;
				colvarSequenceNumber.IsPrimaryKey = false;
				colvarSequenceNumber.IsForeignKey = false;
				colvarSequenceNumber.IsReadOnly = false;
				colvarSequenceNumber.DefaultSetting = @"";
				colvarSequenceNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSequenceNumber);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "code";
				colvarCode.DataType = DbType.AnsiString;
				colvarCode.MaxLength = 50;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = false;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = 50;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarAvailable = new TableSchema.TableColumn(schema);
				colvarAvailable.ColumnName = "available";
				colvarAvailable.DataType = DbType.Boolean;
				colvarAvailable.MaxLength = 0;
				colvarAvailable.AutoIncrement = false;
				colvarAvailable.IsNullable = true;
				colvarAvailable.IsPrimaryKey = false;
				colvarAvailable.IsForeignKey = false;
				colvarAvailable.IsReadOnly = false;
				
						colvarAvailable.DefaultSetting = @"((0))";
				colvarAvailable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAvailable);
				
				TableSchema.TableColumn colvarStoreSequence = new TableSchema.TableColumn(schema);
				colvarStoreSequence.ColumnName = "store_sequence";
				colvarStoreSequence.DataType = DbType.Int32;
				colvarStoreSequence.MaxLength = 0;
				colvarStoreSequence.AutoIncrement = false;
				colvarStoreSequence.IsNullable = true;
				colvarStoreSequence.IsPrimaryKey = false;
				colvarStoreSequence.IsForeignKey = false;
				colvarStoreSequence.IsReadOnly = false;
				colvarStoreSequence.DefaultSetting = @"";
				colvarStoreSequence.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreSequence);
				
				TableSchema.TableColumn colvarIsReservationLock = new TableSchema.TableColumn(schema);
				colvarIsReservationLock.ColumnName = "is_reservation_lock";
				colvarIsReservationLock.DataType = DbType.Boolean;
				colvarIsReservationLock.MaxLength = 0;
				colvarIsReservationLock.AutoIncrement = false;
				colvarIsReservationLock.IsNullable = false;
				colvarIsReservationLock.IsPrimaryKey = false;
				colvarIsReservationLock.IsForeignKey = false;
				colvarIsReservationLock.IsReadOnly = false;
				
						colvarIsReservationLock.DefaultSetting = @"((0))";
				colvarIsReservationLock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReservationLock);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("coupon",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderDetailId")]
		[Bindable(true)]
		public Guid OrderDetailId 
		{
			get { return GetColumnValue<Guid>(Columns.OrderDetailId); }
			set { SetColumnValue(Columns.OrderDetailId, value); }
		}
		  
		[XmlAttribute("SequenceNumber")]
		[Bindable(true)]
		public string SequenceNumber 
		{
			get { return GetColumnValue<string>(Columns.SequenceNumber); }
			set { SetColumnValue(Columns.SequenceNumber, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public string Code 
		{
			get { return GetColumnValue<string>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Available")]
		[Bindable(true)]
		public bool? Available 
		{
			get { return GetColumnValue<bool?>(Columns.Available); }
			set { SetColumnValue(Columns.Available, value); }
		}
		  
		[XmlAttribute("StoreSequence")]
		[Bindable(true)]
		public int? StoreSequence 
		{
			get { return GetColumnValue<int?>(Columns.StoreSequence); }
			set { SetColumnValue(Columns.StoreSequence, value); }
		}
		  
		[XmlAttribute("IsReservationLock")]
		[Bindable(true)]
		public bool IsReservationLock 
		{
			get { return GetColumnValue<bool>(Columns.IsReservationLock); }
			set { SetColumnValue(Columns.IsReservationLock, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderDetailIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SequenceNumberColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn AvailableColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreSequenceColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReservationLockColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderDetailId = @"order_detail_id";
			 public static string SequenceNumber = @"sequence_number";
			 public static string Code = @"code";
			 public static string Description = @"description";
			 public static string Status = @"status";
			 public static string Available = @"available";
			 public static string StoreSequence = @"store_sequence";
			 public static string IsReservationLock = @"is_reservation_lock";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

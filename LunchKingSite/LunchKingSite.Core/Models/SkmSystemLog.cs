using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmSystemLog class.
	/// </summary>
    [Serializable]
	public partial class SkmSystemLogCollection : RepositoryList<SkmSystemLog, SkmSystemLogCollection>
	{	   
		public SkmSystemLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmSystemLogCollection</returns>
		public SkmSystemLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmSystemLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_system_log table.
	/// </summary>
	
	[Serializable]
	public partial class SkmSystemLog : RepositoryRecord<SkmSystemLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmSystemLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmSystemLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_system_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarLogType = new TableSchema.TableColumn(schema);
				colvarLogType.ColumnName = "log_type";
				colvarLogType.DataType = DbType.Byte;
				colvarLogType.MaxLength = 0;
				colvarLogType.AutoIncrement = false;
				colvarLogType.IsNullable = false;
				colvarLogType.IsPrimaryKey = false;
				colvarLogType.IsForeignKey = false;
				colvarLogType.IsReadOnly = false;
				
						colvarLogType.DefaultSetting = @"((0))";
				colvarLogType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogType);
				
				TableSchema.TableColumn colvarLogId = new TableSchema.TableColumn(schema);
				colvarLogId.ColumnName = "log_id";
				colvarLogId.DataType = DbType.Int32;
				colvarLogId.MaxLength = 0;
				colvarLogId.AutoIncrement = false;
				colvarLogId.IsNullable = true;
				colvarLogId.IsPrimaryKey = false;
				colvarLogId.IsForeignKey = false;
				colvarLogId.IsReadOnly = false;
				colvarLogId.DefaultSetting = @"";
				colvarLogId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogId);
				
				TableSchema.TableColumn colvarLogContent = new TableSchema.TableColumn(schema);
				colvarLogContent.ColumnName = "log_content";
				colvarLogContent.DataType = DbType.String;
				colvarLogContent.MaxLength = -1;
				colvarLogContent.AutoIncrement = false;
				colvarLogContent.IsNullable = true;
				colvarLogContent.IsPrimaryKey = false;
				colvarLogContent.IsForeignKey = false;
				colvarLogContent.IsReadOnly = false;
				colvarLogContent.DefaultSetting = @"";
				colvarLogContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogContent);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_system_log",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("LogType")]
		[Bindable(true)]
		public byte LogType 
		{
			get { return GetColumnValue<byte>(Columns.LogType); }
			set { SetColumnValue(Columns.LogType, value); }
		}
		
		[XmlAttribute("LogId")]
		[Bindable(true)]
		public int? LogId 
		{
			get { return GetColumnValue<int?>(Columns.LogId); }
			set { SetColumnValue(Columns.LogId, value); }
		}
		
		[XmlAttribute("LogContent")]
		[Bindable(true)]
		public string LogContent 
		{
			get { return GetColumnValue<string>(Columns.LogContent); }
			set { SetColumnValue(Columns.LogContent, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn LogTypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn LogIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn LogContentColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string LogType = @"log_type";
			 public static string LogId = @"log_id";
			 public static string LogContent = @"log_content";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

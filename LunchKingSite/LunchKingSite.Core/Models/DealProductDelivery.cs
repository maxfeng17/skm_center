using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealProductDelivery class.
	/// </summary>
    [Serializable]
	public partial class DealProductDeliveryCollection : RepositoryList<DealProductDelivery, DealProductDeliveryCollection>
	{	   
		public DealProductDeliveryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealProductDeliveryCollection</returns>
		public DealProductDeliveryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealProductDelivery o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_product_delivery table.
	/// </summary>
	
	[Serializable]
	public partial class DealProductDelivery : RepositoryRecord<DealProductDelivery>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealProductDelivery()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DealProductDelivery(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("deal_product_delivery", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarEnableDelivery = new TableSchema.TableColumn(schema);
				colvarEnableDelivery.ColumnName = "enable_delivery";
				colvarEnableDelivery.DataType = DbType.Boolean;
				colvarEnableDelivery.MaxLength = 0;
				colvarEnableDelivery.AutoIncrement = false;
				colvarEnableDelivery.IsNullable = false;
				colvarEnableDelivery.IsPrimaryKey = false;
				colvarEnableDelivery.IsForeignKey = false;
				colvarEnableDelivery.IsReadOnly = false;
				
						colvarEnableDelivery.DefaultSetting = @"((1))";
				colvarEnableDelivery.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnableDelivery);
				
				TableSchema.TableColumn colvarIspQuantityLimit = new TableSchema.TableColumn(schema);
				colvarIspQuantityLimit.ColumnName = "isp_quantity_limit";
				colvarIspQuantityLimit.DataType = DbType.Int32;
				colvarIspQuantityLimit.MaxLength = 0;
				colvarIspQuantityLimit.AutoIncrement = false;
				colvarIspQuantityLimit.IsNullable = false;
				colvarIspQuantityLimit.IsPrimaryKey = false;
				colvarIspQuantityLimit.IsForeignKey = false;
				colvarIspQuantityLimit.IsReadOnly = false;
				colvarIspQuantityLimit.DefaultSetting = @"";
				colvarIspQuantityLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIspQuantityLimit);
				
				TableSchema.TableColumn colvarEnableIsp = new TableSchema.TableColumn(schema);
				colvarEnableIsp.ColumnName = "enable_isp";
				colvarEnableIsp.DataType = DbType.Boolean;
				colvarEnableIsp.MaxLength = 0;
				colvarEnableIsp.AutoIncrement = false;
				colvarEnableIsp.IsNullable = true;
				colvarEnableIsp.IsPrimaryKey = false;
				colvarEnableIsp.IsForeignKey = false;
				colvarEnableIsp.IsReadOnly = false;
				
						colvarEnableIsp.DefaultSetting = @"((0))";
				colvarEnableIsp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnableIsp);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("deal_product_delivery",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		
		[XmlAttribute("EnableDelivery")]
		[Bindable(true)]
		public bool EnableDelivery 
		{
			get { return GetColumnValue<bool>(Columns.EnableDelivery); }
			set { SetColumnValue(Columns.EnableDelivery, value); }
		}
		
		[XmlAttribute("IspQuantityLimit")]
		[Bindable(true)]
		public int IspQuantityLimit 
		{
			get { return GetColumnValue<int>(Columns.IspQuantityLimit); }
			set { SetColumnValue(Columns.IspQuantityLimit, value); }
		}
		
		[XmlAttribute("EnableIsp")]
		[Bindable(true)]
		public bool? EnableIsp 
		{
			get { return GetColumnValue<bool?>(Columns.EnableIsp); }
			set { SetColumnValue(Columns.EnableIsp, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn EnableDeliveryColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IspQuantityLimitColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EnableIspColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string EnableDelivery = @"enable_delivery";
			 public static string IspQuantityLimit = @"isp_quantity_limit";
			 public static string EnableIsp = @"enable_isp";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

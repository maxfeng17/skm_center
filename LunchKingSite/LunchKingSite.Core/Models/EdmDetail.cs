using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the EdmDetail class.
	/// </summary>
    [Serializable]
	public partial class EdmDetailCollection : RepositoryList<EdmDetail, EdmDetailCollection>
	{	   
		public EdmDetailCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EdmDetailCollection</returns>
		public EdmDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EdmDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the edm_detail table.
	/// </summary>
	[Serializable]
	public partial class EdmDetail : RepositoryRecord<EdmDetail>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public EdmDetail()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public EdmDetail(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("edm_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarPid = new TableSchema.TableColumn(schema);
				colvarPid.ColumnName = "pid";
				colvarPid.DataType = DbType.Int32;
				colvarPid.MaxLength = 0;
				colvarPid.AutoIncrement = false;
				colvarPid.IsNullable = false;
				colvarPid.IsPrimaryKey = false;
				colvarPid.IsForeignKey = false;
				colvarPid.IsReadOnly = false;
				colvarPid.DefaultSetting = @"";
				colvarPid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPid);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = true;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarAdId = new TableSchema.TableColumn(schema);
				colvarAdId.ColumnName = "ad_id";
				colvarAdId.DataType = DbType.Int32;
				colvarAdId.MaxLength = 0;
				colvarAdId.AutoIncrement = false;
				colvarAdId.IsNullable = true;
				colvarAdId.IsPrimaryKey = false;
				colvarAdId.IsForeignKey = false;
				colvarAdId.IsReadOnly = false;
				
						colvarAdId.DefaultSetting = @"((0))";
				colvarAdId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAdId);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 500;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = true;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = true;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
				colvarCityName.ColumnName = "city_name";
				colvarCityName.DataType = DbType.String;
				colvarCityName.MaxLength = 50;
				colvarCityName.AutoIncrement = false;
				colvarCityName.IsNullable = true;
				colvarCityName.IsPrimaryKey = false;
				colvarCityName.IsForeignKey = false;
				colvarCityName.IsReadOnly = false;
				colvarCityName.DefaultSetting = @"";
				colvarCityName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityName);
				
				TableSchema.TableColumn colvarDisplayCityName = new TableSchema.TableColumn(schema);
				colvarDisplayCityName.ColumnName = "display_city_name";
				colvarDisplayCityName.DataType = DbType.Boolean;
				colvarDisplayCityName.MaxLength = 0;
				colvarDisplayCityName.AutoIncrement = false;
				colvarDisplayCityName.IsNullable = false;
				colvarDisplayCityName.IsPrimaryKey = false;
				colvarDisplayCityName.IsForeignKey = false;
				colvarDisplayCityName.IsReadOnly = false;
				
						colvarDisplayCityName.DefaultSetting = @"((1))";
				colvarDisplayCityName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDisplayCityName);
				
				TableSchema.TableColumn colvarCpa = new TableSchema.TableColumn(schema);
				colvarCpa.ColumnName = "cpa";
				colvarCpa.DataType = DbType.AnsiString;
				colvarCpa.MaxLength = 150;
				colvarCpa.AutoIncrement = false;
				colvarCpa.IsNullable = true;
				colvarCpa.IsPrimaryKey = false;
				colvarCpa.IsForeignKey = false;
				colvarCpa.IsReadOnly = false;
				colvarCpa.DefaultSetting = @"";
				colvarCpa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCpa);
				
				TableSchema.TableColumn colvarColumnNumber = new TableSchema.TableColumn(schema);
				colvarColumnNumber.ColumnName = "column_number";
				colvarColumnNumber.DataType = DbType.Int32;
				colvarColumnNumber.MaxLength = 0;
				colvarColumnNumber.AutoIncrement = false;
				colvarColumnNumber.IsNullable = false;
				colvarColumnNumber.IsPrimaryKey = false;
				colvarColumnNumber.IsForeignKey = false;
				colvarColumnNumber.IsReadOnly = false;
				
						colvarColumnNumber.DefaultSetting = @"((1))";
				colvarColumnNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarColumnNumber);
				
				TableSchema.TableColumn colvarRowNumber = new TableSchema.TableColumn(schema);
				colvarRowNumber.ColumnName = "row_number";
				colvarRowNumber.DataType = DbType.Int32;
				colvarRowNumber.MaxLength = 0;
				colvarRowNumber.AutoIncrement = false;
				colvarRowNumber.IsNullable = false;
				colvarRowNumber.IsPrimaryKey = false;
				colvarRowNumber.IsForeignKey = false;
				colvarRowNumber.IsReadOnly = false;
				
						colvarRowNumber.DefaultSetting = @"((1))";
				colvarRowNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRowNumber);
				
				TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
				colvarSequence.ColumnName = "sequence";
				colvarSequence.DataType = DbType.Int32;
				colvarSequence.MaxLength = 0;
				colvarSequence.AutoIncrement = false;
				colvarSequence.IsNullable = false;
				colvarSequence.IsPrimaryKey = false;
				colvarSequence.IsForeignKey = false;
				colvarSequence.IsReadOnly = false;
				
						colvarSequence.DefaultSetting = @"((1))";
				colvarSequence.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSequence);
				
				TableSchema.TableColumn colvarSortType = new TableSchema.TableColumn(schema);
				colvarSortType.ColumnName = "sort_type";
				colvarSortType.DataType = DbType.Int32;
				colvarSortType.MaxLength = 0;
				colvarSortType.AutoIncrement = false;
				colvarSortType.IsNullable = true;
				colvarSortType.IsPrimaryKey = false;
				colvarSortType.IsForeignKey = false;
				colvarSortType.IsReadOnly = false;
				colvarSortType.DefaultSetting = @"";
				colvarSortType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSortType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("edm_detail",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Pid")]
		[Bindable(true)]
		public int Pid 
		{
			get { return GetColumnValue<int>(Columns.Pid); }
			set { SetColumnValue(Columns.Pid, value); }
		}
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid? Bid 
		{
			get { return GetColumnValue<Guid?>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("AdId")]
		[Bindable(true)]
		public int? AdId 
		{
			get { return GetColumnValue<int?>(Columns.AdId); }
			set { SetColumnValue(Columns.AdId, value); }
		}
		  
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int? CityId 
		{
			get { return GetColumnValue<int?>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("CityName")]
		[Bindable(true)]
		public string CityName 
		{
			get { return GetColumnValue<string>(Columns.CityName); }
			set { SetColumnValue(Columns.CityName, value); }
		}
		  
		[XmlAttribute("DisplayCityName")]
		[Bindable(true)]
		public bool DisplayCityName 
		{
			get { return GetColumnValue<bool>(Columns.DisplayCityName); }
			set { SetColumnValue(Columns.DisplayCityName, value); }
		}
		  
		[XmlAttribute("Cpa")]
		[Bindable(true)]
		public string Cpa 
		{
			get { return GetColumnValue<string>(Columns.Cpa); }
			set { SetColumnValue(Columns.Cpa, value); }
		}
		  
		[XmlAttribute("ColumnNumber")]
		[Bindable(true)]
		public int ColumnNumber 
		{
			get { return GetColumnValue<int>(Columns.ColumnNumber); }
			set { SetColumnValue(Columns.ColumnNumber, value); }
		}
		  
		[XmlAttribute("RowNumber")]
		[Bindable(true)]
		public int RowNumber 
		{
			get { return GetColumnValue<int>(Columns.RowNumber); }
			set { SetColumnValue(Columns.RowNumber, value); }
		}
		  
		[XmlAttribute("Sequence")]
		[Bindable(true)]
		public int Sequence 
		{
			get { return GetColumnValue<int>(Columns.Sequence); }
			set { SetColumnValue(Columns.Sequence, value); }
		}
		  
		[XmlAttribute("SortType")]
		[Bindable(true)]
		public int? SortType 
		{
			get { return GetColumnValue<int?>(Columns.SortType); }
			set { SetColumnValue(Columns.SortType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AdIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CityNameColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn DisplayCityNameColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CpaColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ColumnNumberColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn RowNumberColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn SortTypeColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Pid = @"pid";
			 public static string Bid = @"bid";
			 public static string AdId = @"ad_id";
			 public static string Title = @"title";
			 public static string Type = @"type";
			 public static string CityId = @"city_id";
			 public static string CityName = @"city_name";
			 public static string DisplayCityName = @"display_city_name";
			 public static string Cpa = @"cpa";
			 public static string ColumnNumber = @"column_number";
			 public static string RowNumber = @"row_number";
			 public static string Sequence = @"sequence";
			 public static string SortType = @"sort_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

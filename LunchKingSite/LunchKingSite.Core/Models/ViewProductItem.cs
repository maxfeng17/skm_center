using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewProductItem class.
    /// </summary>
    [Serializable]
    public partial class ViewProductItemCollection : ReadOnlyList<ViewProductItem, ViewProductItemCollection>
    {        
        public ViewProductItemCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_product_item view.
    /// </summary>
    [Serializable]
    public partial class ViewProductItem : ReadOnlyRecord<ViewProductItem>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_product_item", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = true;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarInfoGuid = new TableSchema.TableColumn(schema);
                colvarInfoGuid.ColumnName = "info_guid";
                colvarInfoGuid.DataType = DbType.Guid;
                colvarInfoGuid.MaxLength = 0;
                colvarInfoGuid.AutoIncrement = false;
                colvarInfoGuid.IsNullable = true;
                colvarInfoGuid.IsPrimaryKey = false;
                colvarInfoGuid.IsForeignKey = false;
                colvarInfoGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarInfoGuid);
                
                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_guid";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = false;
                colvarItemGuid.IsPrimaryKey = false;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemGuid);
                
                TableSchema.TableColumn colvarProductNo = new TableSchema.TableColumn(schema);
                colvarProductNo.ColumnName = "product_no";
                colvarProductNo.DataType = DbType.Int32;
                colvarProductNo.MaxLength = 0;
                colvarProductNo.AutoIncrement = false;
                colvarProductNo.IsNullable = false;
                colvarProductNo.IsPrimaryKey = false;
                colvarProductNo.IsForeignKey = false;
                colvarProductNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductNo);
                
                TableSchema.TableColumn colvarProductBrandName = new TableSchema.TableColumn(schema);
                colvarProductBrandName.ColumnName = "product_brand_name";
                colvarProductBrandName.DataType = DbType.String;
                colvarProductBrandName.MaxLength = 70;
                colvarProductBrandName.AutoIncrement = false;
                colvarProductBrandName.IsNullable = true;
                colvarProductBrandName.IsPrimaryKey = false;
                colvarProductBrandName.IsForeignKey = false;
                colvarProductBrandName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductBrandName);
                
                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "product_name";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 200;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = true;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductName);
                
                TableSchema.TableColumn colvarGtins = new TableSchema.TableColumn(schema);
                colvarGtins.ColumnName = "gtins";
                colvarGtins.DataType = DbType.AnsiString;
                colvarGtins.MaxLength = 20;
                colvarGtins.AutoIncrement = false;
                colvarGtins.IsNullable = true;
                colvarGtins.IsPrimaryKey = false;
                colvarGtins.IsForeignKey = false;
                colvarGtins.IsReadOnly = false;
                
                schema.Columns.Add(colvarGtins);
                
                TableSchema.TableColumn colvarProductCode = new TableSchema.TableColumn(schema);
                colvarProductCode.ColumnName = "product_code";
                colvarProductCode.DataType = DbType.AnsiString;
                colvarProductCode.MaxLength = 50;
                colvarProductCode.AutoIncrement = false;
                colvarProductCode.IsNullable = true;
                colvarProductCode.IsPrimaryKey = false;
                colvarProductCode.IsForeignKey = false;
                colvarProductCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductCode);
                
                TableSchema.TableColumn colvarStock = new TableSchema.TableColumn(schema);
                colvarStock.ColumnName = "stock";
                colvarStock.DataType = DbType.Int32;
                colvarStock.MaxLength = 0;
                colvarStock.AutoIncrement = false;
                colvarStock.IsNullable = false;
                colvarStock.IsPrimaryKey = false;
                colvarStock.IsForeignKey = false;
                colvarStock.IsReadOnly = false;
                
                schema.Columns.Add(colvarStock);
                
                TableSchema.TableColumn colvarSafetyStock = new TableSchema.TableColumn(schema);
                colvarSafetyStock.ColumnName = "safety_stock";
                colvarSafetyStock.DataType = DbType.Int32;
                colvarSafetyStock.MaxLength = 0;
                colvarSafetyStock.AutoIncrement = false;
                colvarSafetyStock.IsNullable = false;
                colvarSafetyStock.IsPrimaryKey = false;
                colvarSafetyStock.IsForeignKey = false;
                colvarSafetyStock.IsReadOnly = false;
                
                schema.Columns.Add(colvarSafetyStock);
                
                TableSchema.TableColumn colvarItemStatus = new TableSchema.TableColumn(schema);
                colvarItemStatus.ColumnName = "item_status";
                colvarItemStatus.DataType = DbType.Int32;
                colvarItemStatus.MaxLength = 0;
                colvarItemStatus.AutoIncrement = false;
                colvarItemStatus.IsNullable = false;
                colvarItemStatus.IsPrimaryKey = false;
                colvarItemStatus.IsForeignKey = false;
                colvarItemStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemStatus);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 100;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarSort = new TableSchema.TableColumn(schema);
                colvarSort.ColumnName = "sort";
                colvarSort.DataType = DbType.Int32;
                colvarSort.MaxLength = 0;
                colvarSort.AutoIncrement = false;
                colvarSort.IsNullable = false;
                colvarSort.IsPrimaryKey = false;
                colvarSort.IsForeignKey = false;
                colvarSort.IsReadOnly = false;
                
                schema.Columns.Add(colvarSort);
                
                TableSchema.TableColumn colvarIsMulti = new TableSchema.TableColumn(schema);
                colvarIsMulti.ColumnName = "is_multi";
                colvarIsMulti.DataType = DbType.Boolean;
                colvarIsMulti.MaxLength = 0;
                colvarIsMulti.AutoIncrement = false;
                colvarIsMulti.IsNullable = true;
                colvarIsMulti.IsPrimaryKey = false;
                colvarIsMulti.IsForeignKey = false;
                colvarIsMulti.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsMulti);
                
                TableSchema.TableColumn colvarMpn = new TableSchema.TableColumn(schema);
                colvarMpn.ColumnName = "mpn";
                colvarMpn.DataType = DbType.AnsiString;
                colvarMpn.MaxLength = 70;
                colvarMpn.AutoIncrement = false;
                colvarMpn.IsNullable = true;
                colvarMpn.IsPrimaryKey = false;
                colvarMpn.IsForeignKey = false;
                colvarMpn.IsReadOnly = false;
                
                schema.Columns.Add(colvarMpn);
                
                TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
                colvarPrice.ColumnName = "price";
                colvarPrice.DataType = DbType.Int32;
                colvarPrice.MaxLength = 0;
                colvarPrice.AutoIncrement = false;
                colvarPrice.IsNullable = true;
                colvarPrice.IsPrimaryKey = false;
                colvarPrice.IsForeignKey = false;
                colvarPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrice);
                
                TableSchema.TableColumn colvarWarehouseType = new TableSchema.TableColumn(schema);
                colvarWarehouseType.ColumnName = "warehouse_type";
                colvarWarehouseType.DataType = DbType.Int32;
                colvarWarehouseType.MaxLength = 0;
                colvarWarehouseType.AutoIncrement = false;
                colvarWarehouseType.IsNullable = true;
                colvarWarehouseType.IsPrimaryKey = false;
                colvarWarehouseType.IsForeignKey = false;
                colvarWarehouseType.IsReadOnly = false;
                
                schema.Columns.Add(colvarWarehouseType);
                
                TableSchema.TableColumn colvarShelfLife = new TableSchema.TableColumn(schema);
                colvarShelfLife.ColumnName = "shelf_life";
                colvarShelfLife.DataType = DbType.Int32;
                colvarShelfLife.MaxLength = 0;
                colvarShelfLife.AutoIncrement = false;
                colvarShelfLife.IsNullable = true;
                colvarShelfLife.IsPrimaryKey = false;
                colvarShelfLife.IsForeignKey = false;
                colvarShelfLife.IsReadOnly = false;
                
                schema.Columns.Add(colvarShelfLife);
                
                TableSchema.TableColumn colvarPchomeProdId = new TableSchema.TableColumn(schema);
                colvarPchomeProdId.ColumnName = "pchome_prod_id";
                colvarPchomeProdId.DataType = DbType.String;
                colvarPchomeProdId.MaxLength = 50;
                colvarPchomeProdId.AutoIncrement = false;
                colvarPchomeProdId.IsNullable = true;
                colvarPchomeProdId.IsPrimaryKey = false;
                colvarPchomeProdId.IsForeignKey = false;
                colvarPchomeProdId.IsReadOnly = false;
                
                schema.Columns.Add(colvarPchomeProdId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_product_item",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewProductItem()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewProductItem(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewProductItem(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewProductItem(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid? SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("InfoGuid")]
        [Bindable(true)]
        public Guid? InfoGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("info_guid");
		    }
            set 
		    {
			    SetColumnValue("info_guid", value);
            }
        }
	      
        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid ItemGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("item_guid");
		    }
            set 
		    {
			    SetColumnValue("item_guid", value);
            }
        }
	      
        [XmlAttribute("ProductNo")]
        [Bindable(true)]
        public int ProductNo 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_no");
		    }
            set 
		    {
			    SetColumnValue("product_no", value);
            }
        }
	      
        [XmlAttribute("ProductBrandName")]
        [Bindable(true)]
        public string ProductBrandName 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_brand_name");
		    }
            set 
		    {
			    SetColumnValue("product_brand_name", value);
            }
        }
	      
        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_name");
		    }
            set 
		    {
			    SetColumnValue("product_name", value);
            }
        }
	      
        [XmlAttribute("Gtins")]
        [Bindable(true)]
        public string Gtins 
	    {
		    get
		    {
			    return GetColumnValue<string>("gtins");
		    }
            set 
		    {
			    SetColumnValue("gtins", value);
            }
        }
	      
        [XmlAttribute("ProductCode")]
        [Bindable(true)]
        public string ProductCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_code");
		    }
            set 
		    {
			    SetColumnValue("product_code", value);
            }
        }
	      
        [XmlAttribute("Stock")]
        [Bindable(true)]
        public int Stock 
	    {
		    get
		    {
			    return GetColumnValue<int>("stock");
		    }
            set 
		    {
			    SetColumnValue("stock", value);
            }
        }
	      
        [XmlAttribute("SafetyStock")]
        [Bindable(true)]
        public int SafetyStock 
	    {
		    get
		    {
			    return GetColumnValue<int>("safety_stock");
		    }
            set 
		    {
			    SetColumnValue("safety_stock", value);
            }
        }
	      
        [XmlAttribute("ItemStatus")]
        [Bindable(true)]
        public int ItemStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_status");
		    }
            set 
		    {
			    SetColumnValue("item_status", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("Sort")]
        [Bindable(true)]
        public int Sort 
	    {
		    get
		    {
			    return GetColumnValue<int>("sort");
		    }
            set 
		    {
			    SetColumnValue("sort", value);
            }
        }
	      
        [XmlAttribute("IsMulti")]
        [Bindable(true)]
        public bool? IsMulti 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_multi");
		    }
            set 
		    {
			    SetColumnValue("is_multi", value);
            }
        }
	      
        [XmlAttribute("Mpn")]
        [Bindable(true)]
        public string Mpn 
	    {
		    get
		    {
			    return GetColumnValue<string>("mpn");
		    }
            set 
		    {
			    SetColumnValue("mpn", value);
            }
        }
	      
        [XmlAttribute("Price")]
        [Bindable(true)]
        public int? Price 
	    {
		    get
		    {
			    return GetColumnValue<int?>("price");
		    }
            set 
		    {
			    SetColumnValue("price", value);
            }
        }
	      
        [XmlAttribute("WarehouseType")]
        [Bindable(true)]
        public int? WarehouseType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("warehouse_type");
		    }
            set 
		    {
			    SetColumnValue("warehouse_type", value);
            }
        }
	      
        [XmlAttribute("ShelfLife")]
        [Bindable(true)]
        public int? ShelfLife 
	    {
		    get
		    {
			    return GetColumnValue<int?>("shelf_life");
		    }
            set 
		    {
			    SetColumnValue("shelf_life", value);
            }
        }
	      
        [XmlAttribute("PchomeProdId")]
        [Bindable(true)]
        public string PchomeProdId 
	    {
		    get
		    {
			    return GetColumnValue<string>("pchome_prod_id");
		    }
            set 
		    {
			    SetColumnValue("pchome_prod_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string SellerGuid = @"seller_guid";
            
            public static string InfoGuid = @"info_guid";
            
            public static string ItemGuid = @"item_guid";
            
            public static string ProductNo = @"product_no";
            
            public static string ProductBrandName = @"product_brand_name";
            
            public static string ProductName = @"product_name";
            
            public static string Gtins = @"gtins";
            
            public static string ProductCode = @"product_code";
            
            public static string Stock = @"stock";
            
            public static string SafetyStock = @"safety_stock";
            
            public static string ItemStatus = @"item_status";
            
            public static string ItemName = @"item_name";
            
            public static string Sort = @"sort";
            
            public static string IsMulti = @"is_multi";
            
            public static string Mpn = @"mpn";
            
            public static string Price = @"price";
            
            public static string WarehouseType = @"warehouse_type";
            
            public static string ShelfLife = @"shelf_life";
            
            public static string PchomeProdId = @"pchome_prod_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

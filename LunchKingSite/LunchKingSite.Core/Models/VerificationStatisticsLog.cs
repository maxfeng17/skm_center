using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the VerificationStatisticsLog class.
    /// </summary>
    [Serializable]
    public partial class VerificationStatisticsLogCollection : RepositoryList<VerificationStatisticsLog, VerificationStatisticsLogCollection>
    {
        public VerificationStatisticsLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VerificationStatisticsLogCollection</returns>
        public VerificationStatisticsLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VerificationStatisticsLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the verification_statistics_log table.
    /// </summary>
    [Serializable]
    public partial class VerificationStatisticsLog : RepositoryRecord<VerificationStatisticsLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public VerificationStatisticsLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public VerificationStatisticsLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("verification_statistics_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarUnverified = new TableSchema.TableColumn(schema);
                colvarUnverified.ColumnName = "unverified";
                colvarUnverified.DataType = DbType.Int32;
                colvarUnverified.MaxLength = 0;
                colvarUnverified.AutoIncrement = false;
                colvarUnverified.IsNullable = true;
                colvarUnverified.IsPrimaryKey = false;
                colvarUnverified.IsForeignKey = false;
                colvarUnverified.IsReadOnly = false;
                colvarUnverified.DefaultSetting = @"";
                colvarUnverified.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUnverified);

                TableSchema.TableColumn colvarVerified = new TableSchema.TableColumn(schema);
                colvarVerified.ColumnName = "verified";
                colvarVerified.DataType = DbType.Int32;
                colvarVerified.MaxLength = 0;
                colvarVerified.AutoIncrement = false;
                colvarVerified.IsNullable = true;
                colvarVerified.IsPrimaryKey = false;
                colvarVerified.IsForeignKey = false;
                colvarVerified.IsReadOnly = false;
                colvarVerified.DefaultSetting = @"";
                colvarVerified.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVerified);

                TableSchema.TableColumn colvarReturned = new TableSchema.TableColumn(schema);
                colvarReturned.ColumnName = "returned";
                colvarReturned.DataType = DbType.Int32;
                colvarReturned.MaxLength = 0;
                colvarReturned.AutoIncrement = false;
                colvarReturned.IsNullable = true;
                colvarReturned.IsPrimaryKey = false;
                colvarReturned.IsForeignKey = false;
                colvarReturned.IsReadOnly = false;
                colvarReturned.DefaultSetting = @"";
                colvarReturned.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReturned);

                TableSchema.TableColumn colvarForcedReturned = new TableSchema.TableColumn(schema);
                colvarForcedReturned.ColumnName = "forced_returned";
                colvarForcedReturned.DataType = DbType.Int32;
                colvarForcedReturned.MaxLength = 0;
                colvarForcedReturned.AutoIncrement = false;
                colvarForcedReturned.IsNullable = true;
                colvarForcedReturned.IsPrimaryKey = false;
                colvarForcedReturned.IsForeignKey = false;
                colvarForcedReturned.IsReadOnly = false;
                colvarForcedReturned.DefaultSetting = @"";
                colvarForcedReturned.ForeignKeyTableName = "";
                schema.Columns.Add(colvarForcedReturned);

                TableSchema.TableColumn colvarForcedVerified = new TableSchema.TableColumn(schema);
                colvarForcedVerified.ColumnName = "forced_verified";
                colvarForcedVerified.DataType = DbType.Int32;
                colvarForcedVerified.MaxLength = 0;
                colvarForcedVerified.AutoIncrement = false;
                colvarForcedVerified.IsNullable = true;
                colvarForcedVerified.IsPrimaryKey = false;
                colvarForcedVerified.IsForeignKey = false;
                colvarForcedVerified.IsReadOnly = false;
                colvarForcedVerified.DefaultSetting = @"";
                colvarForcedVerified.ForeignKeyTableName = "";
                schema.Columns.Add(colvarForcedVerified);

                TableSchema.TableColumn colvarLost = new TableSchema.TableColumn(schema);
                colvarLost.ColumnName = "lost";
                colvarLost.DataType = DbType.Int32;
                colvarLost.MaxLength = 0;
                colvarLost.AutoIncrement = false;
                colvarLost.IsNullable = true;
                colvarLost.IsPrimaryKey = false;
                colvarLost.IsForeignKey = false;
                colvarLost.IsReadOnly = false;
                colvarLost.DefaultSetting = @"";
                colvarLost.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLost);

                TableSchema.TableColumn colvarAdjustment = new TableSchema.TableColumn(schema);
                colvarAdjustment.ColumnName = "adjustment";
                colvarAdjustment.DataType = DbType.Int32;
                colvarAdjustment.MaxLength = 0;
                colvarAdjustment.AutoIncrement = false;
                colvarAdjustment.IsNullable = true;
                colvarAdjustment.IsPrimaryKey = false;
                colvarAdjustment.IsForeignKey = false;
                colvarAdjustment.IsReadOnly = false;
                colvarAdjustment.DefaultSetting = @"";
                colvarAdjustment.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAdjustment);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                colvarStatus.DefaultSetting = @"((0))";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.AnsiString;
                colvarModifyId.MaxLength = 50;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("verification_statistics_log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid
        {
            get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        [XmlAttribute("Unverified")]
        [Bindable(true)]
        public int? Unverified
        {
            get { return GetColumnValue<int?>(Columns.Unverified); }
            set { SetColumnValue(Columns.Unverified, value); }
        }

        [XmlAttribute("Verified")]
        [Bindable(true)]
        public int? Verified
        {
            get { return GetColumnValue<int?>(Columns.Verified); }
            set { SetColumnValue(Columns.Verified, value); }
        }

        [XmlAttribute("Returned")]
        [Bindable(true)]
        public int? Returned
        {
            get { return GetColumnValue<int?>(Columns.Returned); }
            set { SetColumnValue(Columns.Returned, value); }
        }

        [XmlAttribute("ForcedReturned")]
        [Bindable(true)]
        public int? ForcedReturned
        {
            get { return GetColumnValue<int?>(Columns.ForcedReturned); }
            set { SetColumnValue(Columns.ForcedReturned, value); }
        }

        [XmlAttribute("ForcedVerified")]
        [Bindable(true)]
        public int? ForcedVerified
        {
            get { return GetColumnValue<int?>(Columns.ForcedVerified); }
            set { SetColumnValue(Columns.ForcedVerified, value); }
        }

        [XmlAttribute("Lost")]
        [Bindable(true)]
        public int? Lost
        {
            get { return GetColumnValue<int?>(Columns.Lost); }
            set { SetColumnValue(Columns.Lost, value); }
        }

        [XmlAttribute("Adjustment")]
        [Bindable(true)]
        public int? Adjustment
        {
            get { return GetColumnValue<int?>(Columns.Adjustment); }
            set { SetColumnValue(Columns.Adjustment, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn UnverifiedColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn VerifiedColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ReturnedColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ForcedReturnedColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ForcedVerifiedColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn LostColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn AdjustmentColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[13]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string BusinessHourGuid = @"business_hour_guid";
            public static string Unverified = @"unverified";
            public static string Verified = @"verified";
            public static string Returned = @"returned";
            public static string ForcedReturned = @"forced_returned";
            public static string ForcedVerified = @"forced_verified";
            public static string Lost = @"lost";
            public static string Adjustment = @"adjustment";
            public static string Status = @"status";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

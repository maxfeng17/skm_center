using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewDiscountDetail class.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountDetailCollection : ReadOnlyList<ViewDiscountDetail, ViewDiscountDetailCollection>
    {
        public ViewDiscountDetailCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_discount_detail view.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountDetail : ReadOnlyRecord<ViewDiscountDetail>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_discount_detail", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 100;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;

                schema.Columns.Add(colvarName);

                TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
                colvarQty.ColumnName = "qty";
                colvarQty.DataType = DbType.Int32;
                colvarQty.MaxLength = 0;
                colvarQty.AutoIncrement = false;
                colvarQty.IsNullable = true;
                colvarQty.IsPrimaryKey = false;
                colvarQty.IsForeignKey = false;
                colvarQty.IsReadOnly = false;

                schema.Columns.Add(colvarQty);

                TableSchema.TableColumn colvarApplyTime = new TableSchema.TableColumn(schema);
                colvarApplyTime.ColumnName = "apply_time";
                colvarApplyTime.DataType = DbType.DateTime;
                colvarApplyTime.MaxLength = 0;
                colvarApplyTime.AutoIncrement = false;
                colvarApplyTime.IsNullable = true;
                colvarApplyTime.IsPrimaryKey = false;
                colvarApplyTime.IsForeignKey = false;
                colvarApplyTime.IsReadOnly = false;

                schema.Columns.Add(colvarApplyTime);

                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = true;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;

                schema.Columns.Add(colvarStartTime);

                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;

                schema.Columns.Add(colvarEndTime);

                TableSchema.TableColumn colvarCancelTime = new TableSchema.TableColumn(schema);
                colvarCancelTime.ColumnName = "cancel_time";
                colvarCancelTime.DataType = DbType.DateTime;
                colvarCancelTime.MaxLength = 0;
                colvarCancelTime.AutoIncrement = false;
                colvarCancelTime.IsNullable = true;
                colvarCancelTime.IsPrimaryKey = false;
                colvarCancelTime.IsForeignKey = false;
                colvarCancelTime.IsReadOnly = false;

                schema.Columns.Add(colvarCancelTime);

                TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
                colvarFlag.ColumnName = "flag";
                colvarFlag.DataType = DbType.Int32;
                colvarFlag.MaxLength = 0;
                colvarFlag.AutoIncrement = false;
                colvarFlag.IsNullable = false;
                colvarFlag.IsPrimaryKey = false;
                colvarFlag.IsForeignKey = false;
                colvarFlag.IsReadOnly = false;

                schema.Columns.Add(colvarFlag);

                TableSchema.TableColumn colvarCodeId = new TableSchema.TableColumn(schema);
                colvarCodeId.ColumnName = "code_id";
                colvarCodeId.DataType = DbType.Int32;
                colvarCodeId.MaxLength = 0;
                colvarCodeId.AutoIncrement = false;
                colvarCodeId.IsNullable = false;
                colvarCodeId.IsPrimaryKey = false;
                colvarCodeId.IsForeignKey = false;
                colvarCodeId.IsReadOnly = false;

                schema.Columns.Add(colvarCodeId);

                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 20;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;

                schema.Columns.Add(colvarCode);

                TableSchema.TableColumn colvarUseTime = new TableSchema.TableColumn(schema);
                colvarUseTime.ColumnName = "use_time";
                colvarUseTime.DataType = DbType.DateTime;
                colvarUseTime.MaxLength = 0;
                colvarUseTime.AutoIncrement = false;
                colvarUseTime.IsNullable = true;
                colvarUseTime.IsPrimaryKey = false;
                colvarUseTime.IsForeignKey = false;
                colvarUseTime.IsReadOnly = false;

                schema.Columns.Add(colvarUseTime);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;

                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = true;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;

                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarOwner = new TableSchema.TableColumn(schema);
                colvarOwner.ColumnName = "owner";
                colvarOwner.DataType = DbType.Int32;
                colvarOwner.MaxLength = 0;
                colvarOwner.AutoIncrement = false;
                colvarOwner.IsNullable = true;
                colvarOwner.IsPrimaryKey = false;
                colvarOwner.IsForeignKey = false;
                colvarOwner.IsReadOnly = false;

                schema.Columns.Add(colvarOwner);

                TableSchema.TableColumn colvarOwnerName = new TableSchema.TableColumn(schema);
                colvarOwnerName.ColumnName = "owner_name";
                colvarOwnerName.DataType = DbType.String;
                colvarOwnerName.MaxLength = 256;
                colvarOwnerName.AutoIncrement = false;
                colvarOwnerName.IsNullable = true;
                colvarOwnerName.IsPrimaryKey = false;
                colvarOwnerName.IsForeignKey = false;
                colvarOwnerName.IsReadOnly = false;

                schema.Columns.Add(colvarOwnerName);

                TableSchema.TableColumn colvarSendDate = new TableSchema.TableColumn(schema);
                colvarSendDate.ColumnName = "send_date";
                colvarSendDate.DataType = DbType.DateTime;
                colvarSendDate.MaxLength = 0;
                colvarSendDate.AutoIncrement = false;
                colvarSendDate.IsNullable = true;
                colvarSendDate.IsPrimaryKey = false;
                colvarSendDate.IsForeignKey = false;
                colvarSendDate.IsReadOnly = false;

                schema.Columns.Add(colvarSendDate);

                TableSchema.TableColumn colvarMinimumAmount = new TableSchema.TableColumn(schema);
                colvarMinimumAmount.ColumnName = "minimum_amount";
                colvarMinimumAmount.DataType = DbType.Int32;
                colvarMinimumAmount.MaxLength = 0;
                colvarMinimumAmount.AutoIncrement = false;
                colvarMinimumAmount.IsNullable = true;
                colvarMinimumAmount.IsPrimaryKey = false;
                colvarMinimumAmount.IsForeignKey = false;
                colvarMinimumAmount.IsReadOnly = false;

                schema.Columns.Add(colvarMinimumAmount);

                TableSchema.TableColumn colvarUseId = new TableSchema.TableColumn(schema);
                colvarUseId.ColumnName = "use_id";
                colvarUseId.DataType = DbType.Int32;
                colvarUseId.MaxLength = 0;
                colvarUseId.AutoIncrement = false;
                colvarUseId.IsNullable = true;
                colvarUseId.IsPrimaryKey = false;
                colvarUseId.IsForeignKey = false;
                colvarUseId.IsReadOnly = false;

                schema.Columns.Add(colvarUseId);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;

                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarCardCombineUse = new TableSchema.TableColumn(schema);
                colvarCardCombineUse.ColumnName = "card_combine_use";
                colvarCardCombineUse.DataType = DbType.Boolean;
                colvarCardCombineUse.MaxLength = 0;
                colvarCardCombineUse.AutoIncrement = false;
                colvarCardCombineUse.IsNullable = false;
                colvarCardCombineUse.IsPrimaryKey = false;
                colvarCardCombineUse.IsForeignKey = false;
                colvarCardCombineUse.IsReadOnly = false;

                schema.Columns.Add(colvarCardCombineUse);

                TableSchema.TableColumn colvarNickName = new TableSchema.TableColumn(schema);
                colvarNickName.ColumnName = "nick_name";
                colvarNickName.DataType = DbType.String;
                colvarNickName.MaxLength = 100;
                colvarNickName.AutoIncrement = false;
                colvarNickName.IsNullable = true;
                colvarNickName.IsPrimaryKey = false;
                colvarNickName.IsForeignKey = false;
                colvarNickName.IsReadOnly = false;

                schema.Columns.Add(colvarNickName);

                TableSchema.TableColumn colvarMinGrossMargin = new TableSchema.TableColumn(schema);
                colvarMinGrossMargin.ColumnName = "min_gross_margin";
                colvarMinGrossMargin.DataType = DbType.Int32;
                colvarMinGrossMargin.MaxLength = 0;
                colvarMinGrossMargin.AutoIncrement = false;
                colvarMinGrossMargin.IsNullable = true;
                colvarMinGrossMargin.IsPrimaryKey = false;
                colvarMinGrossMargin.IsForeignKey = false;
                colvarMinGrossMargin.IsReadOnly = false;

                schema.Columns.Add(colvarMinGrossMargin);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_discount_detail", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewDiscountDetail()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewDiscountDetail(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewDiscountDetail(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewDiscountDetail(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name
        {
            get
            {
                return GetColumnValue<string>("name");
            }
            set
            {
                SetColumnValue("name", value);
            }
        }

        [XmlAttribute("Qty")]
        [Bindable(true)]
        public int? Qty
        {
            get
            {
                return GetColumnValue<int?>("qty");
            }
            set
            {
                SetColumnValue("qty", value);
            }
        }

        [XmlAttribute("ApplyTime")]
        [Bindable(true)]
        public DateTime? ApplyTime
        {
            get
            {
                return GetColumnValue<DateTime?>("apply_time");
            }
            set
            {
                SetColumnValue("apply_time", value);
            }
        }

        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime? StartTime
        {
            get
            {
                return GetColumnValue<DateTime?>("start_time");
            }
            set
            {
                SetColumnValue("start_time", value);
            }
        }

        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime
        {
            get
            {
                return GetColumnValue<DateTime?>("end_time");
            }
            set
            {
                SetColumnValue("end_time", value);
            }
        }

        [XmlAttribute("CancelTime")]
        [Bindable(true)]
        public DateTime? CancelTime
        {
            get
            {
                return GetColumnValue<DateTime?>("cancel_time");
            }
            set
            {
                SetColumnValue("cancel_time", value);
            }
        }

        [XmlAttribute("Flag")]
        [Bindable(true)]
        public int Flag
        {
            get
            {
                return GetColumnValue<int>("flag");
            }
            set
            {
                SetColumnValue("flag", value);
            }
        }

        [XmlAttribute("CodeId")]
        [Bindable(true)]
        public int CodeId
        {
            get
            {
                return GetColumnValue<int>("code_id");
            }
            set
            {
                SetColumnValue("code_id", value);
            }
        }

        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code
        {
            get
            {
                return GetColumnValue<string>("code");
            }
            set
            {
                SetColumnValue("code", value);
            }
        }

        [XmlAttribute("UseTime")]
        [Bindable(true)]
        public DateTime? UseTime
        {
            get
            {
                return GetColumnValue<DateTime?>("use_time");
            }
            set
            {
                SetColumnValue("use_time", value);
            }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid
        {
            get
            {
                return GetColumnValue<Guid?>("order_guid");
            }
            set
            {
                SetColumnValue("order_guid", value);
            }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal? Amount
        {
            get
            {
                return GetColumnValue<decimal?>("amount");
            }
            set
            {
                SetColumnValue("amount", value);
            }
        }

        [XmlAttribute("Owner")]
        [Bindable(true)]
        public int? Owner
        {
            get
            {
                return GetColumnValue<int?>("owner");
            }
            set
            {
                SetColumnValue("owner", value);
            }
        }

        [XmlAttribute("OwnerName")]
        [Bindable(true)]
        public string OwnerName
        {
            get
            {
                return GetColumnValue<string>("owner_name");
            }
            set
            {
                SetColumnValue("owner_name", value);
            }
        }

        [XmlAttribute("SendDate")]
        [Bindable(true)]
        public DateTime? SendDate
        {
            get
            {
                return GetColumnValue<DateTime?>("send_date");
            }
            set
            {
                SetColumnValue("send_date", value);
            }
        }

        [XmlAttribute("MinimumAmount")]
        [Bindable(true)]
        public int? MinimumAmount
        {
            get
            {
                return GetColumnValue<int?>("minimum_amount");
            }
            set
            {
                SetColumnValue("minimum_amount", value);
            }
        }

        [XmlAttribute("UseId")]
        [Bindable(true)]
        public int? UseId
        {
            get
            {
                return GetColumnValue<int?>("use_id");
            }
            set
            {
                SetColumnValue("use_id", value);
            }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get
            {
                return GetColumnValue<int>("type");
            }
            set
            {
                SetColumnValue("type", value);
            }
        }

        [XmlAttribute("CardCombineUse")]
        [Bindable(true)]
        public bool CardCombineUse
        {
            get
            {
                return GetColumnValue<bool>("card_combine_use");
            }
            set
            {
                SetColumnValue("card_combine_use", value);
            }
        }

        [XmlAttribute("NickName")]
        [Bindable(true)]
        public string NickName
        {
            get
            {
                return GetColumnValue<string>("nick_name");
            }
            set
            {
                SetColumnValue("nick_name", value);
            }
        }

        [XmlAttribute("MinGrossMargin")]
        [Bindable(true)]
        public int? MinGrossMargin
        {
            get
            {
                return GetColumnValue<int?>("min_gross_margin");
            }
            set
            {
                SetColumnValue("min_gross_margin", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Id = @"id";

            public static string Name = @"name";

            public static string Qty = @"qty";

            public static string ApplyTime = @"apply_time";

            public static string StartTime = @"start_time";

            public static string EndTime = @"end_time";

            public static string CancelTime = @"cancel_time";

            public static string Flag = @"flag";

            public static string CodeId = @"code_id";

            public static string Code = @"code";

            public static string UseTime = @"use_time";

            public static string OrderGuid = @"order_guid";

            public static string Amount = @"amount";

            public static string Owner = @"owner";

            public static string OwnerName = @"owner_name";

            public static string SendDate = @"send_date";

            public static string MinimumAmount = @"minimum_amount";

            public static string UseId = @"use_id";

            public static string Type = @"type";

            public static string CardCombineUse = @"card_combine_use";

            public static string NickName = @"nick_name";

            public static string MinGrossMargin = @"min_gross_margin";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MemberDelivery class.
	/// </summary>
    [Serializable]
	public partial class MemberDeliveryCollection : RepositoryList<MemberDelivery, MemberDeliveryCollection>
	{	   
		public MemberDeliveryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberDeliveryCollection</returns>
		public MemberDeliveryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberDelivery o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the member_delivery table.
	/// </summary>
	[Serializable]
	public partial class MemberDelivery : RepositoryRecord<MemberDelivery>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MemberDelivery()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MemberDelivery(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_delivery", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarAddressAlias = new TableSchema.TableColumn(schema);
				colvarAddressAlias.ColumnName = "address_Alias";
				colvarAddressAlias.DataType = DbType.String;
				colvarAddressAlias.MaxLength = 100;
				colvarAddressAlias.AutoIncrement = false;
				colvarAddressAlias.IsNullable = true;
				colvarAddressAlias.IsPrimaryKey = false;
				colvarAddressAlias.IsForeignKey = false;
				colvarAddressAlias.IsReadOnly = false;
				colvarAddressAlias.DefaultSetting = @"";
				colvarAddressAlias.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAddressAlias);
				
				TableSchema.TableColumn colvarBuildingGuid = new TableSchema.TableColumn(schema);
				colvarBuildingGuid.ColumnName = "building_GUID";
				colvarBuildingGuid.DataType = DbType.Guid;
				colvarBuildingGuid.MaxLength = 0;
				colvarBuildingGuid.AutoIncrement = false;
				colvarBuildingGuid.IsNullable = false;
				colvarBuildingGuid.IsPrimaryKey = false;
				colvarBuildingGuid.IsForeignKey = true;
				colvarBuildingGuid.IsReadOnly = false;
				colvarBuildingGuid.DefaultSetting = @"";
				
					colvarBuildingGuid.ForeignKeyTableName = "building";
				schema.Columns.Add(colvarBuildingGuid);
				
				TableSchema.TableColumn colvarTel = new TableSchema.TableColumn(schema);
				colvarTel.ColumnName = "Tel";
				colvarTel.DataType = DbType.AnsiString;
				colvarTel.MaxLength = 20;
				colvarTel.AutoIncrement = false;
				colvarTel.IsNullable = true;
				colvarTel.IsPrimaryKey = false;
				colvarTel.IsForeignKey = false;
				colvarTel.IsReadOnly = false;
				colvarTel.DefaultSetting = @"";
				colvarTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTel);
				
				TableSchema.TableColumn colvarExt = new TableSchema.TableColumn(schema);
				colvarExt.ColumnName = "ext";
				colvarExt.DataType = DbType.AnsiString;
				colvarExt.MaxLength = 50;
				colvarExt.AutoIncrement = false;
				colvarExt.IsNullable = true;
				colvarExt.IsPrimaryKey = false;
				colvarExt.IsForeignKey = false;
				colvarExt.IsReadOnly = false;
				colvarExt.DefaultSetting = @"";
				colvarExt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExt);
				
				TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
				colvarMobile.ColumnName = "mobile";
				colvarMobile.DataType = DbType.AnsiString;
				colvarMobile.MaxLength = 50;
				colvarMobile.AutoIncrement = false;
				colvarMobile.IsNullable = true;
				colvarMobile.IsPrimaryKey = false;
				colvarMobile.IsForeignKey = false;
				colvarMobile.IsReadOnly = false;
				colvarMobile.DefaultSetting = @"";
				colvarMobile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobile);
				
				TableSchema.TableColumn colvarAddress = new TableSchema.TableColumn(schema);
				colvarAddress.ColumnName = "address";
				colvarAddress.DataType = DbType.String;
				colvarAddress.MaxLength = 100;
				colvarAddress.AutoIncrement = false;
				colvarAddress.IsNullable = true;
				colvarAddress.IsPrimaryKey = false;
				colvarAddress.IsForeignKey = false;
				colvarAddress.IsReadOnly = false;
				colvarAddress.DefaultSetting = @"";
				colvarAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAddress);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 200;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = true;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarBuildingName = new TableSchema.TableColumn(schema);
				colvarBuildingName.ColumnName = "building_Name";
				colvarBuildingName.DataType = DbType.String;
				colvarBuildingName.MaxLength = 50;
				colvarBuildingName.AutoIncrement = false;
				colvarBuildingName.IsNullable = true;
				colvarBuildingName.IsPrimaryKey = false;
				colvarBuildingName.IsForeignKey = false;
				colvarBuildingName.IsReadOnly = false;
				colvarBuildingName.DefaultSetting = @"";
				colvarBuildingName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuildingName);
				
				TableSchema.TableColumn colvarSection = new TableSchema.TableColumn(schema);
				colvarSection.ColumnName = "section";
				colvarSection.DataType = DbType.String;
				colvarSection.MaxLength = 50;
				colvarSection.AutoIncrement = false;
				colvarSection.IsNullable = true;
				colvarSection.IsPrimaryKey = false;
				colvarSection.IsForeignKey = false;
				colvarSection.IsReadOnly = false;
				colvarSection.DefaultSetting = @"";
				colvarSection.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSection);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 50;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarIsDefault = new TableSchema.TableColumn(schema);
				colvarIsDefault.ColumnName = "IsDefault";
				colvarIsDefault.DataType = DbType.Boolean;
				colvarIsDefault.MaxLength = 0;
				colvarIsDefault.AutoIncrement = false;
				colvarIsDefault.IsNullable = true;
				colvarIsDefault.IsPrimaryKey = false;
				colvarIsDefault.IsForeignKey = false;
				colvarIsDefault.IsReadOnly = false;
				colvarIsDefault.DefaultSetting = @"";
				colvarIsDefault.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDefault);
				
				TableSchema.TableColumn colvarContactName = new TableSchema.TableColumn(schema);
				colvarContactName.ColumnName = "contact_name";
				colvarContactName.DataType = DbType.String;
				colvarContactName.MaxLength = 50;
				colvarContactName.AutoIncrement = false;
				colvarContactName.IsNullable = true;
				colvarContactName.IsPrimaryKey = false;
				colvarContactName.IsForeignKey = false;
				colvarContactName.IsReadOnly = false;
				colvarContactName.DefaultSetting = @"";
				colvarContactName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContactName);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = true;
				colvarUserId.IsReadOnly = false;
				
						colvarUserId.DefaultSetting = @"((0))";
				
					colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_delivery",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("AddressAlias")]
		[Bindable(true)]
		public string AddressAlias 
		{
			get { return GetColumnValue<string>(Columns.AddressAlias); }
			set { SetColumnValue(Columns.AddressAlias, value); }
		}
		  
		[XmlAttribute("BuildingGuid")]
		[Bindable(true)]
		public Guid BuildingGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BuildingGuid); }
			set { SetColumnValue(Columns.BuildingGuid, value); }
		}
		  
		[XmlAttribute("Tel")]
		[Bindable(true)]
		public string Tel 
		{
			get { return GetColumnValue<string>(Columns.Tel); }
			set { SetColumnValue(Columns.Tel, value); }
		}
		  
		[XmlAttribute("Ext")]
		[Bindable(true)]
		public string Ext 
		{
			get { return GetColumnValue<string>(Columns.Ext); }
			set { SetColumnValue(Columns.Ext, value); }
		}
		  
		[XmlAttribute("Mobile")]
		[Bindable(true)]
		public string Mobile 
		{
			get { return GetColumnValue<string>(Columns.Mobile); }
			set { SetColumnValue(Columns.Mobile, value); }
		}
		  
		[XmlAttribute("Address")]
		[Bindable(true)]
		public string Address 
		{
			get { return GetColumnValue<string>(Columns.Address); }
			set { SetColumnValue(Columns.Address, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("BuildingName")]
		[Bindable(true)]
		public string BuildingName 
		{
			get { return GetColumnValue<string>(Columns.BuildingName); }
			set { SetColumnValue(Columns.BuildingName, value); }
		}
		  
		[XmlAttribute("Section")]
		[Bindable(true)]
		public string Section 
		{
			get { return GetColumnValue<string>(Columns.Section); }
			set { SetColumnValue(Columns.Section, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("IsDefault")]
		[Bindable(true)]
		public bool? IsDefault 
		{
			get { return GetColumnValue<bool?>(Columns.IsDefault); }
			set { SetColumnValue(Columns.IsDefault, value); }
		}
		  
		[XmlAttribute("ContactName")]
		[Bindable(true)]
		public string ContactName 
		{
			get { return GetColumnValue<string>(Columns.ContactName); }
			set { SetColumnValue(Columns.ContactName, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AddressAliasColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BuildingGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn TelColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MobileColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn AddressColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn BuildingNameColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SectionColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDefaultColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactNameColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string AddressAlias = @"address_Alias";
			 public static string BuildingGuid = @"building_GUID";
			 public static string Tel = @"Tel";
			 public static string Ext = @"ext";
			 public static string Mobile = @"mobile";
			 public static string Address = @"address";
			 public static string Memo = @"memo";
			 public static string BuildingName = @"building_Name";
			 public static string Section = @"section";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string IsDefault = @"IsDefault";
			 public static string ContactName = @"contact_name";
			 public static string UserId = @"user_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBalancingDeal class.
    /// </summary>
    [Serializable]
    public partial class ViewBalancingDealCollection : ReadOnlyList<ViewBalancingDeal, ViewBalancingDealCollection>
    {        
        public ViewBalancingDealCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_balancing_deal view.
    /// </summary>
    [Serializable]
    public partial class ViewBalancingDeal : ReadOnlyRecord<ViewBalancingDeal>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_balancing_deal", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarMerchandiseGuid = new TableSchema.TableColumn(schema);
                colvarMerchandiseGuid.ColumnName = "merchandise_guid";
                colvarMerchandiseGuid.DataType = DbType.Guid;
                colvarMerchandiseGuid.MaxLength = 0;
                colvarMerchandiseGuid.AutoIncrement = false;
                colvarMerchandiseGuid.IsNullable = false;
                colvarMerchandiseGuid.IsPrimaryKey = false;
                colvarMerchandiseGuid.IsForeignKey = false;
                colvarMerchandiseGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMerchandiseGuid);
                
                TableSchema.TableColumn colvarBusinessModel = new TableSchema.TableColumn(schema);
                colvarBusinessModel.ColumnName = "business_model";
                colvarBusinessModel.DataType = DbType.Int32;
                colvarBusinessModel.MaxLength = 0;
                colvarBusinessModel.AutoIncrement = false;
                colvarBusinessModel.IsNullable = false;
                colvarBusinessModel.IsPrimaryKey = false;
                colvarBusinessModel.IsForeignKey = false;
                colvarBusinessModel.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessModel);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 500;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarDealUseStartTime = new TableSchema.TableColumn(schema);
                colvarDealUseStartTime.ColumnName = "deal_use_start_time";
                colvarDealUseStartTime.DataType = DbType.DateTime;
                colvarDealUseStartTime.MaxLength = 0;
                colvarDealUseStartTime.AutoIncrement = false;
                colvarDealUseStartTime.IsNullable = true;
                colvarDealUseStartTime.IsPrimaryKey = false;
                colvarDealUseStartTime.IsForeignKey = false;
                colvarDealUseStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealUseStartTime);
                
                TableSchema.TableColumn colvarDealUseEndTime = new TableSchema.TableColumn(schema);
                colvarDealUseEndTime.ColumnName = "deal_use_end_time";
                colvarDealUseEndTime.DataType = DbType.DateTime;
                colvarDealUseEndTime.MaxLength = 0;
                colvarDealUseEndTime.AutoIncrement = false;
                colvarDealUseEndTime.IsNullable = true;
                colvarDealUseEndTime.IsPrimaryKey = false;
                colvarDealUseEndTime.IsForeignKey = false;
                colvarDealUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealUseEndTime);
                
                TableSchema.TableColumn colvarDealMaxUseEndTime = new TableSchema.TableColumn(schema);
                colvarDealMaxUseEndTime.ColumnName = "deal_max_use_end_time";
                colvarDealMaxUseEndTime.DataType = DbType.DateTime;
                colvarDealMaxUseEndTime.MaxLength = 0;
                colvarDealMaxUseEndTime.AutoIncrement = false;
                colvarDealMaxUseEndTime.IsNullable = true;
                colvarDealMaxUseEndTime.IsPrimaryKey = false;
                colvarDealMaxUseEndTime.IsForeignKey = false;
                colvarDealMaxUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealMaxUseEndTime);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarStoreCount = new TableSchema.TableColumn(schema);
                colvarStoreCount.ColumnName = "store_count";
                colvarStoreCount.DataType = DbType.Int32;
                colvarStoreCount.MaxLength = 0;
                colvarStoreCount.AutoIncrement = false;
                colvarStoreCount.IsNullable = true;
                colvarStoreCount.IsPrimaryKey = false;
                colvarStoreCount.IsForeignKey = false;
                colvarStoreCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreCount);
                
                TableSchema.TableColumn colvarIsConfirmedReadyToPay = new TableSchema.TableColumn(schema);
                colvarIsConfirmedReadyToPay.ColumnName = "is_confirmed_ready_to_pay";
                colvarIsConfirmedReadyToPay.DataType = DbType.Boolean;
                colvarIsConfirmedReadyToPay.MaxLength = 0;
                colvarIsConfirmedReadyToPay.AutoIncrement = false;
                colvarIsConfirmedReadyToPay.IsNullable = true;
                colvarIsConfirmedReadyToPay.IsPrimaryKey = false;
                colvarIsConfirmedReadyToPay.IsForeignKey = false;
                colvarIsConfirmedReadyToPay.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsConfirmedReadyToPay);
                
                TableSchema.TableColumn colvarGenerationFrequency = new TableSchema.TableColumn(schema);
                colvarGenerationFrequency.ColumnName = "generation_frequency";
                colvarGenerationFrequency.DataType = DbType.Int32;
                colvarGenerationFrequency.MaxLength = 0;
                colvarGenerationFrequency.AutoIncrement = false;
                colvarGenerationFrequency.IsNullable = true;
                colvarGenerationFrequency.IsPrimaryKey = false;
                colvarGenerationFrequency.IsForeignKey = false;
                colvarGenerationFrequency.IsReadOnly = false;
                
                schema.Columns.Add(colvarGenerationFrequency);
                
                TableSchema.TableColumn colvarIntervalEnd = new TableSchema.TableColumn(schema);
                colvarIntervalEnd.ColumnName = "interval_end";
                colvarIntervalEnd.DataType = DbType.DateTime;
                colvarIntervalEnd.MaxLength = 0;
                colvarIntervalEnd.AutoIncrement = false;
                colvarIntervalEnd.IsNullable = true;
                colvarIntervalEnd.IsPrimaryKey = false;
                colvarIntervalEnd.IsForeignKey = false;
                colvarIntervalEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarIntervalEnd);
                
                TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
                colvarRemittanceType.ColumnName = "remittance_type";
                colvarRemittanceType.DataType = DbType.Int32;
                colvarRemittanceType.MaxLength = 0;
                colvarRemittanceType.AutoIncrement = false;
                colvarRemittanceType.IsNullable = false;
                colvarRemittanceType.IsPrimaryKey = false;
                colvarRemittanceType.IsForeignKey = false;
                colvarRemittanceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemittanceType);
                
                TableSchema.TableColumn colvarDealEstablished = new TableSchema.TableColumn(schema);
                colvarDealEstablished.ColumnName = "deal_established";
                colvarDealEstablished.DataType = DbType.Int32;
                colvarDealEstablished.MaxLength = 0;
                colvarDealEstablished.AutoIncrement = false;
                colvarDealEstablished.IsNullable = true;
                colvarDealEstablished.IsPrimaryKey = false;
                colvarDealEstablished.IsForeignKey = false;
                colvarDealEstablished.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEstablished);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = true;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_balancing_deal",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBalancingDeal()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBalancingDeal(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBalancingDeal(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBalancingDeal(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("MerchandiseGuid")]
        [Bindable(true)]
        public Guid MerchandiseGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("merchandise_guid");
		    }
            set 
		    {
			    SetColumnValue("merchandise_guid", value);
            }
        }
	      
        [XmlAttribute("BusinessModel")]
        [Bindable(true)]
        public int BusinessModel 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_model");
		    }
            set 
		    {
			    SetColumnValue("business_model", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_name");
		    }
            set 
		    {
			    SetColumnValue("deal_name", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_start_time", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("DealUseStartTime")]
        [Bindable(true)]
        public DateTime? DealUseStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_use_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_use_start_time", value);
            }
        }
	      
        [XmlAttribute("DealUseEndTime")]
        [Bindable(true)]
        public DateTime? DealUseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_use_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_use_end_time", value);
            }
        }
	      
        [XmlAttribute("DealMaxUseEndTime")]
        [Bindable(true)]
        public DateTime? DealMaxUseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_max_use_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_max_use_end_time", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("StoreCount")]
        [Bindable(true)]
        public int? StoreCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("store_count");
		    }
            set 
		    {
			    SetColumnValue("store_count", value);
            }
        }
	      
        [XmlAttribute("IsConfirmedReadyToPay")]
        [Bindable(true)]
        public bool? IsConfirmedReadyToPay 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_confirmed_ready_to_pay");
		    }
            set 
		    {
			    SetColumnValue("is_confirmed_ready_to_pay", value);
            }
        }
	      
        [XmlAttribute("GenerationFrequency")]
        [Bindable(true)]
        public int? GenerationFrequency 
	    {
		    get
		    {
			    return GetColumnValue<int?>("generation_frequency");
		    }
            set 
		    {
			    SetColumnValue("generation_frequency", value);
            }
        }
	      
        [XmlAttribute("IntervalEnd")]
        [Bindable(true)]
        public DateTime? IntervalEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("interval_end");
		    }
            set 
		    {
			    SetColumnValue("interval_end", value);
            }
        }
	      
        [XmlAttribute("RemittanceType")]
        [Bindable(true)]
        public int RemittanceType 
	    {
		    get
		    {
			    return GetColumnValue<int>("remittance_type");
		    }
            set 
		    {
			    SetColumnValue("remittance_type", value);
            }
        }
	      
        [XmlAttribute("DealEstablished")]
        [Bindable(true)]
        public int? DealEstablished 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_established");
		    }
            set 
		    {
			    SetColumnValue("deal_established", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int? DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string DealId = @"deal_id";
            
            public static string MerchandiseGuid = @"merchandise_guid";
            
            public static string BusinessModel = @"business_model";
            
            public static string DealName = @"deal_name";
            
            public static string DealStartTime = @"deal_start_time";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string DealUseStartTime = @"deal_use_start_time";
            
            public static string DealUseEndTime = @"deal_use_end_time";
            
            public static string DealMaxUseEndTime = @"deal_max_use_end_time";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string SellerName = @"seller_name";
            
            public static string StoreCount = @"store_count";
            
            public static string IsConfirmedReadyToPay = @"is_confirmed_ready_to_pay";
            
            public static string GenerationFrequency = @"generation_frequency";
            
            public static string IntervalEnd = @"interval_end";
            
            public static string RemittanceType = @"remittance_type";
            
            public static string DealEstablished = @"deal_established";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string SellerId = @"seller_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

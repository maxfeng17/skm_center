using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class OrderShipCollection : RepositoryList<OrderShip, OrderShipCollection>
	{
			public OrderShipCollection() {}

			public OrderShipCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							OrderShip o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class OrderShip : RepositoryRecord<OrderShip>, IRecordBase
	{
		#region .ctors and Default Settings
		public OrderShip()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public OrderShip(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order_ship", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
				colvarOrderClassification.ColumnName = "order_classification";
				colvarOrderClassification.DataType = DbType.Int32;
				colvarOrderClassification.MaxLength = 0;
				colvarOrderClassification.AutoIncrement = false;
				colvarOrderClassification.IsNullable = false;
				colvarOrderClassification.IsPrimaryKey = false;
				colvarOrderClassification.IsForeignKey = false;
				colvarOrderClassification.IsReadOnly = false;
				colvarOrderClassification.DefaultSetting = @"";
				colvarOrderClassification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderClassification);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarShipCompanyId = new TableSchema.TableColumn(schema);
				colvarShipCompanyId.ColumnName = "ship_company_id";
				colvarShipCompanyId.DataType = DbType.Int32;
				colvarShipCompanyId.MaxLength = 0;
				colvarShipCompanyId.AutoIncrement = false;
				colvarShipCompanyId.IsNullable = true;
				colvarShipCompanyId.IsPrimaryKey = false;
				colvarShipCompanyId.IsForeignKey = false;
				colvarShipCompanyId.IsReadOnly = false;
				colvarShipCompanyId.DefaultSetting = @"";
				colvarShipCompanyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipCompanyId);

				TableSchema.TableColumn colvarShipTime = new TableSchema.TableColumn(schema);
				colvarShipTime.ColumnName = "ship_time";
				colvarShipTime.DataType = DbType.DateTime;
				colvarShipTime.MaxLength = 0;
				colvarShipTime.AutoIncrement = false;
				colvarShipTime.IsNullable = true;
				colvarShipTime.IsPrimaryKey = false;
				colvarShipTime.IsForeignKey = false;
				colvarShipTime.IsReadOnly = false;
				colvarShipTime.DefaultSetting = @"";
				colvarShipTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipTime);

				TableSchema.TableColumn colvarShipNo = new TableSchema.TableColumn(schema);
				colvarShipNo.ColumnName = "ship_no";
				colvarShipNo.DataType = DbType.String;
				colvarShipNo.MaxLength = 2147483647;
				colvarShipNo.AutoIncrement = false;
				colvarShipNo.IsNullable = true;
				colvarShipNo.IsPrimaryKey = false;
				colvarShipNo.IsForeignKey = false;
				colvarShipNo.IsReadOnly = false;
				colvarShipNo.DefaultSetting = @"";
				colvarShipNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipNo);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 100;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 100;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = false;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarShipMemo = new TableSchema.TableColumn(schema);
				colvarShipMemo.ColumnName = "ship_memo";
				colvarShipMemo.DataType = DbType.String;
				colvarShipMemo.MaxLength = 60;
				colvarShipMemo.AutoIncrement = false;
				colvarShipMemo.IsNullable = true;
				colvarShipMemo.IsPrimaryKey = false;
				colvarShipMemo.IsForeignKey = false;
				colvarShipMemo.IsReadOnly = false;
				colvarShipMemo.DefaultSetting = @"";
				colvarShipMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipMemo);

				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"((1))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);

				TableSchema.TableColumn colvarShipToStockTime = new TableSchema.TableColumn(schema);
				colvarShipToStockTime.ColumnName = "ship_to_stock_time";
				colvarShipToStockTime.DataType = DbType.DateTime;
				colvarShipToStockTime.MaxLength = 0;
				colvarShipToStockTime.AutoIncrement = false;
				colvarShipToStockTime.IsNullable = true;
				colvarShipToStockTime.IsPrimaryKey = false;
				colvarShipToStockTime.IsForeignKey = false;
				colvarShipToStockTime.IsReadOnly = false;
				colvarShipToStockTime.DefaultSetting = @"";
				colvarShipToStockTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipToStockTime);

				TableSchema.TableColumn colvarReceiptTime = new TableSchema.TableColumn(schema);
				colvarReceiptTime.ColumnName = "receipt_time";
				colvarReceiptTime.DataType = DbType.DateTime;
				colvarReceiptTime.MaxLength = 0;
				colvarReceiptTime.AutoIncrement = false;
				colvarReceiptTime.IsNullable = true;
				colvarReceiptTime.IsPrimaryKey = false;
				colvarReceiptTime.IsForeignKey = false;
				colvarReceiptTime.IsReadOnly = false;
				colvarReceiptTime.DefaultSetting = @"";
				colvarReceiptTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiptTime);

				TableSchema.TableColumn colvarIsReceipt = new TableSchema.TableColumn(schema);
				colvarIsReceipt.ColumnName = "is_receipt";
				colvarIsReceipt.DataType = DbType.Boolean;
				colvarIsReceipt.MaxLength = 0;
				colvarIsReceipt.AutoIncrement = false;
				colvarIsReceipt.IsNullable = true;
				colvarIsReceipt.IsPrimaryKey = false;
				colvarIsReceipt.IsForeignKey = false;
				colvarIsReceipt.IsReadOnly = false;
				colvarIsReceipt.DefaultSetting = @"";
				colvarIsReceipt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReceipt);

				TableSchema.TableColumn colvarSentArrivalNoticeSms = new TableSchema.TableColumn(schema);
				colvarSentArrivalNoticeSms.ColumnName = "sent_arrival_notice_sms";
				colvarSentArrivalNoticeSms.DataType = DbType.Boolean;
				colvarSentArrivalNoticeSms.MaxLength = 0;
				colvarSentArrivalNoticeSms.AutoIncrement = false;
				colvarSentArrivalNoticeSms.IsNullable = false;
				colvarSentArrivalNoticeSms.IsPrimaryKey = false;
				colvarSentArrivalNoticeSms.IsForeignKey = false;
				colvarSentArrivalNoticeSms.IsReadOnly = false;
				colvarSentArrivalNoticeSms.DefaultSetting = @"((0))";
				colvarSentArrivalNoticeSms.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSentArrivalNoticeSms);

				TableSchema.TableColumn colvarSentArrivalNoticePush = new TableSchema.TableColumn(schema);
				colvarSentArrivalNoticePush.ColumnName = "sent_arrival_notice_push";
				colvarSentArrivalNoticePush.DataType = DbType.Boolean;
				colvarSentArrivalNoticePush.MaxLength = 0;
				colvarSentArrivalNoticePush.AutoIncrement = false;
				colvarSentArrivalNoticePush.IsNullable = false;
				colvarSentArrivalNoticePush.IsPrimaryKey = false;
				colvarSentArrivalNoticePush.IsForeignKey = false;
				colvarSentArrivalNoticePush.IsReadOnly = false;
				colvarSentArrivalNoticePush.DefaultSetting = @"((0))";
				colvarSentArrivalNoticePush.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSentArrivalNoticePush);

				TableSchema.TableColumn colvarDcReceiveNoticePush = new TableSchema.TableColumn(schema);
				colvarDcReceiveNoticePush.ColumnName = "dc_receive_notice_push";
				colvarDcReceiveNoticePush.DataType = DbType.Boolean;
				colvarDcReceiveNoticePush.MaxLength = 0;
				colvarDcReceiveNoticePush.AutoIncrement = false;
				colvarDcReceiveNoticePush.IsNullable = false;
				colvarDcReceiveNoticePush.IsPrimaryKey = false;
				colvarDcReceiveNoticePush.IsForeignKey = false;
				colvarDcReceiveNoticePush.IsReadOnly = false;
				colvarDcReceiveNoticePush.DefaultSetting = @"((0))";
				colvarDcReceiveNoticePush.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDcReceiveNoticePush);

				TableSchema.TableColumn colvarPickId = new TableSchema.TableColumn(schema);
				colvarPickId.ColumnName = "pick_id";
				colvarPickId.DataType = DbType.String;
				colvarPickId.MaxLength = 30;
				colvarPickId.AutoIncrement = false;
				colvarPickId.IsNullable = true;
				colvarPickId.IsPrimaryKey = false;
				colvarPickId.IsForeignKey = false;
				colvarPickId.IsReadOnly = false;
				colvarPickId.DefaultSetting = @"";
				colvarPickId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPickId);

				TableSchema.TableColumn colvarProgressStatus = new TableSchema.TableColumn(schema);
				colvarProgressStatus.ColumnName = "progress_status";
				colvarProgressStatus.DataType = DbType.Int32;
				colvarProgressStatus.MaxLength = 0;
				colvarProgressStatus.AutoIncrement = false;
				colvarProgressStatus.IsNullable = true;
				colvarProgressStatus.IsPrimaryKey = false;
				colvarProgressStatus.IsForeignKey = false;
				colvarProgressStatus.IsReadOnly = false;
				colvarProgressStatus.DefaultSetting = @"((0))";
				colvarProgressStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProgressStatus);

				TableSchema.TableColumn colvarProgressDate = new TableSchema.TableColumn(schema);
				colvarProgressDate.ColumnName = "progress_date";
				colvarProgressDate.DataType = DbType.DateTime;
				colvarProgressDate.MaxLength = 0;
				colvarProgressDate.AutoIncrement = false;
				colvarProgressDate.IsNullable = true;
				colvarProgressDate.IsPrimaryKey = false;
				colvarProgressDate.IsForeignKey = false;
				colvarProgressDate.IsReadOnly = false;
				colvarProgressDate.DefaultSetting = @"";
				colvarProgressDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProgressDate);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order_ship",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("OrderClassification")]
		[Bindable(true)]
		public int OrderClassification
		{
			get { return GetColumnValue<int>(Columns.OrderClassification); }
			set { SetColumnValue(Columns.OrderClassification, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("ShipCompanyId")]
		[Bindable(true)]
		public int? ShipCompanyId
		{
			get { return GetColumnValue<int?>(Columns.ShipCompanyId); }
			set { SetColumnValue(Columns.ShipCompanyId, value); }
		}

		[XmlAttribute("ShipTime")]
		[Bindable(true)]
		public DateTime? ShipTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ShipTime); }
			set { SetColumnValue(Columns.ShipTime, value); }
		}

		[XmlAttribute("ShipNo")]
		[Bindable(true)]
		public string ShipNo
		{
			get { return GetColumnValue<string>(Columns.ShipNo); }
			set { SetColumnValue(Columns.ShipNo, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("ShipMemo")]
		[Bindable(true)]
		public string ShipMemo
		{
			get { return GetColumnValue<string>(Columns.ShipMemo); }
			set { SetColumnValue(Columns.ShipMemo, value); }
		}

		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}

		[XmlAttribute("ShipToStockTime")]
		[Bindable(true)]
		public DateTime? ShipToStockTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ShipToStockTime); }
			set { SetColumnValue(Columns.ShipToStockTime, value); }
		}

		[XmlAttribute("ReceiptTime")]
		[Bindable(true)]
		public DateTime? ReceiptTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ReceiptTime); }
			set { SetColumnValue(Columns.ReceiptTime, value); }
		}

		[XmlAttribute("IsReceipt")]
		[Bindable(true)]
		public bool? IsReceipt
		{
			get { return GetColumnValue<bool?>(Columns.IsReceipt); }
			set { SetColumnValue(Columns.IsReceipt, value); }
		}

		[XmlAttribute("SentArrivalNoticeSms")]
		[Bindable(true)]
		public bool SentArrivalNoticeSms
		{
			get { return GetColumnValue<bool>(Columns.SentArrivalNoticeSms); }
			set { SetColumnValue(Columns.SentArrivalNoticeSms, value); }
		}

		[XmlAttribute("SentArrivalNoticePush")]
		[Bindable(true)]
		public bool SentArrivalNoticePush
		{
			get { return GetColumnValue<bool>(Columns.SentArrivalNoticePush); }
			set { SetColumnValue(Columns.SentArrivalNoticePush, value); }
		}

		[XmlAttribute("DcReceiveNoticePush")]
		[Bindable(true)]
		public bool DcReceiveNoticePush
		{
			get { return GetColumnValue<bool>(Columns.DcReceiveNoticePush); }
			set { SetColumnValue(Columns.DcReceiveNoticePush, value); }
		}

		[XmlAttribute("PickId")]
		[Bindable(true)]
		public string PickId
		{
			get { return GetColumnValue<string>(Columns.PickId); }
			set { SetColumnValue(Columns.PickId, value); }
		}

		[XmlAttribute("ProgressStatus")]
		[Bindable(true)]
		public int? ProgressStatus
		{
			get { return GetColumnValue<int?>(Columns.ProgressStatus); }
			set { SetColumnValue(Columns.ProgressStatus, value); }
		}

		[XmlAttribute("ProgressDate")]
		[Bindable(true)]
		public DateTime? ProgressDate
		{
			get { return GetColumnValue<DateTime?>(Columns.ProgressDate); }
			set { SetColumnValue(Columns.ProgressDate, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn OrderClassificationColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn ShipCompanyIdColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn ShipTimeColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn ShipNoColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn ModifyIdColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn ShipMemoColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn TypeColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn ShipToStockTimeColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn ReceiptTimeColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn IsReceiptColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn SentArrivalNoticeSmsColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn SentArrivalNoticePushColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn DcReceiveNoticePushColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn PickIdColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn ProgressStatusColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn ProgressDateColumn
		{
			get { return Schema.Columns[20]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string OrderClassification = @"order_classification";
			public static string OrderGuid = @"order_guid";
			public static string ShipCompanyId = @"ship_company_id";
			public static string ShipTime = @"ship_time";
			public static string ShipNo = @"ship_no";
			public static string CreateId = @"create_id";
			public static string CreateTime = @"create_time";
			public static string ModifyId = @"modify_id";
			public static string ModifyTime = @"modify_time";
			public static string ShipMemo = @"ship_memo";
			public static string Type = @"type";
			public static string ShipToStockTime = @"ship_to_stock_time";
			public static string ReceiptTime = @"receipt_time";
			public static string IsReceipt = @"is_receipt";
			public static string SentArrivalNoticeSms = @"sent_arrival_notice_sms";
			public static string SentArrivalNoticePush = @"sent_arrival_notice_push";
			public static string DcReceiveNoticePush = @"dc_receive_notice_push";
			public static string PickId = @"pick_id";
			public static string ProgressStatus = @"progress_status";
			public static string ProgressDate = @"progress_date";
		}

		#endregion

	}
}

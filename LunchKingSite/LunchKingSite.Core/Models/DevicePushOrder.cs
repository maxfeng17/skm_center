using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    [Serializable]
    public partial class DevicePushOrderCollection : RepositoryList<DevicePushOrder, DevicePushOrderCollection>
    {
        public DevicePushOrderCollection() { }

        public DevicePushOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DevicePushOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }

    [Serializable]
    public partial class DevicePushOrder : RepositoryRecord<DevicePushOrder>, IRecordBase
    {
        #region .ctors and Default Settings
        public DevicePushOrder()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public DevicePushOrder(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("device_push_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                colvarOrderGuid.DefaultSetting = @"";
                colvarOrderGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarFirstPushRecordId = new TableSchema.TableColumn(schema);
                colvarFirstPushRecordId.ColumnName = "first_push_record_id";
                colvarFirstPushRecordId.DataType = DbType.Int32;
                colvarFirstPushRecordId.MaxLength = 0;
                colvarFirstPushRecordId.AutoIncrement = false;
                colvarFirstPushRecordId.IsNullable = false;
                colvarFirstPushRecordId.IsPrimaryKey = false;
                colvarFirstPushRecordId.IsForeignKey = false;
                colvarFirstPushRecordId.IsReadOnly = false;
                colvarFirstPushRecordId.DefaultSetting = @"";
                colvarFirstPushRecordId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFirstPushRecordId);

                TableSchema.TableColumn colvarLastPushRecordId = new TableSchema.TableColumn(schema);
                colvarLastPushRecordId.ColumnName = "last_push_record_id";
                colvarLastPushRecordId.DataType = DbType.Int32;
                colvarLastPushRecordId.MaxLength = 0;
                colvarLastPushRecordId.AutoIncrement = false;
                colvarLastPushRecordId.IsNullable = false;
                colvarLastPushRecordId.IsPrimaryKey = false;
                colvarLastPushRecordId.IsForeignKey = false;
                colvarLastPushRecordId.IsReadOnly = false;
                colvarLastPushRecordId.DefaultSetting = @"";
                colvarLastPushRecordId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastPushRecordId);

                TableSchema.TableColumn colvarFirstActionId = new TableSchema.TableColumn(schema);
                colvarFirstActionId.ColumnName = "first_action_id";
                colvarFirstActionId.DataType = DbType.Int32;
                colvarFirstActionId.MaxLength = 0;
                colvarFirstActionId.AutoIncrement = false;
                colvarFirstActionId.IsNullable = false;
                colvarFirstActionId.IsPrimaryKey = false;
                colvarFirstActionId.IsForeignKey = false;
                colvarFirstActionId.IsReadOnly = false;
                colvarFirstActionId.DefaultSetting = @"";
                colvarFirstActionId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFirstActionId);

                TableSchema.TableColumn colvarLastActionId = new TableSchema.TableColumn(schema);
                colvarLastActionId.ColumnName = "last_action_id";
                colvarLastActionId.DataType = DbType.Int32;
                colvarLastActionId.MaxLength = 0;
                colvarLastActionId.AutoIncrement = false;
                colvarLastActionId.IsNullable = false;
                colvarLastActionId.IsPrimaryKey = false;
                colvarLastActionId.IsForeignKey = false;
                colvarLastActionId.IsReadOnly = false;
                colvarLastActionId.DefaultSetting = @"";
                colvarLastActionId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastActionId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarFirstPushTime = new TableSchema.TableColumn(schema);
                colvarFirstPushTime.ColumnName = "first_push_time";
                colvarFirstPushTime.DataType = DbType.DateTime;
                colvarFirstPushTime.MaxLength = 0;
                colvarFirstPushTime.AutoIncrement = false;
                colvarFirstPushTime.IsNullable = false;
                colvarFirstPushTime.IsPrimaryKey = false;
                colvarFirstPushTime.IsForeignKey = false;
                colvarFirstPushTime.IsReadOnly = false;
                colvarFirstPushTime.DefaultSetting = @"";
                colvarFirstPushTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFirstPushTime);

                TableSchema.TableColumn colvarLastPushTime = new TableSchema.TableColumn(schema);
                colvarLastPushTime.ColumnName = "last_push_time";
                colvarLastPushTime.DataType = DbType.DateTime;
                colvarLastPushTime.MaxLength = 0;
                colvarLastPushTime.AutoIncrement = false;
                colvarLastPushTime.IsNullable = false;
                colvarLastPushTime.IsPrimaryKey = false;
                colvarLastPushTime.IsForeignKey = false;
                colvarLastPushTime.IsReadOnly = false;
                colvarLastPushTime.DefaultSetting = @"";
                colvarLastPushTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastPushTime);

                TableSchema.TableColumn colvarFirstReadTime = new TableSchema.TableColumn(schema);
                colvarFirstReadTime.ColumnName = "first_read_time";
                colvarFirstReadTime.DataType = DbType.DateTime;
                colvarFirstReadTime.MaxLength = 0;
                colvarFirstReadTime.AutoIncrement = false;
                colvarFirstReadTime.IsNullable = false;
                colvarFirstReadTime.IsPrimaryKey = false;
                colvarFirstReadTime.IsForeignKey = false;
                colvarFirstReadTime.IsReadOnly = false;
                colvarFirstReadTime.DefaultSetting = @"";
                colvarFirstReadTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFirstReadTime);

                TableSchema.TableColumn colvarLastReadTime = new TableSchema.TableColumn(schema);
                colvarLastReadTime.ColumnName = "last_read_time";
                colvarLastReadTime.DataType = DbType.DateTime;
                colvarLastReadTime.MaxLength = 0;
                colvarLastReadTime.AutoIncrement = false;
                colvarLastReadTime.IsNullable = false;
                colvarLastReadTime.IsPrimaryKey = false;
                colvarLastReadTime.IsForeignKey = false;
                colvarLastReadTime.IsReadOnly = false;
                colvarLastReadTime.DefaultSetting = @"";
                colvarLastReadTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastReadTime);

                TableSchema.TableColumn colvarFirstPushAppId = new TableSchema.TableColumn(schema);
                colvarFirstPushAppId.ColumnName = "first_push_app_id";
                colvarFirstPushAppId.DataType = DbType.Int32;
                colvarFirstPushAppId.MaxLength = 0;
                colvarFirstPushAppId.AutoIncrement = false;
                colvarFirstPushAppId.IsNullable = false;
                colvarFirstPushAppId.IsPrimaryKey = false;
                colvarFirstPushAppId.IsForeignKey = false;
                colvarFirstPushAppId.IsReadOnly = false;
                colvarFirstPushAppId.DefaultSetting = @"";
                colvarFirstPushAppId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFirstPushAppId);

                TableSchema.TableColumn colvarLastPushAppId = new TableSchema.TableColumn(schema);
                colvarLastPushAppId.ColumnName = "last_push_app_id";
                colvarLastPushAppId.DataType = DbType.Int32;
                colvarLastPushAppId.MaxLength = 0;
                colvarLastPushAppId.AutoIncrement = false;
                colvarLastPushAppId.IsNullable = false;
                colvarLastPushAppId.IsPrimaryKey = false;
                colvarLastPushAppId.IsForeignKey = false;
                colvarLastPushAppId.IsReadOnly = false;
                colvarLastPushAppId.DefaultSetting = @"";
                colvarLastPushAppId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastPushAppId);

                TableSchema.TableColumn colvarIsAnalyzed = new TableSchema.TableColumn(schema);
                colvarIsAnalyzed.ColumnName = "is_analyzed";
                colvarIsAnalyzed.DataType = DbType.Boolean;
                colvarIsAnalyzed.MaxLength = 0;
                colvarIsAnalyzed.AutoIncrement = false;
                colvarIsAnalyzed.IsNullable = false;
                colvarIsAnalyzed.IsPrimaryKey = false;
                colvarIsAnalyzed.IsForeignKey = false;
                colvarIsAnalyzed.IsReadOnly = false;
                colvarIsAnalyzed.DefaultSetting = @"";
                colvarIsAnalyzed.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsAnalyzed);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("device_push_order", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid
        {
            get { return GetColumnValue<Guid>(Columns.OrderGuid); }
            set { SetColumnValue(Columns.OrderGuid, value); }
        }

        [XmlAttribute("FirstPushRecordId")]
        [Bindable(true)]
        public int FirstPushRecordId
        {
            get { return GetColumnValue<int>(Columns.FirstPushRecordId); }
            set { SetColumnValue(Columns.FirstPushRecordId, value); }
        }

        [XmlAttribute("LastPushRecordId")]
        [Bindable(true)]
        public int LastPushRecordId
        {
            get { return GetColumnValue<int>(Columns.LastPushRecordId); }
            set { SetColumnValue(Columns.LastPushRecordId, value); }
        }

        [XmlAttribute("FirstActionId")]
        [Bindable(true)]
        public int FirstActionId
        {
            get { return GetColumnValue<int>(Columns.FirstActionId); }
            set { SetColumnValue(Columns.FirstActionId, value); }
        }

        [XmlAttribute("LastActionId")]
        [Bindable(true)]
        public int LastActionId
        {
            get { return GetColumnValue<int>(Columns.LastActionId); }
            set { SetColumnValue(Columns.LastActionId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("FirstPushTime")]
        [Bindable(true)]
        public DateTime FirstPushTime
        {
            get { return GetColumnValue<DateTime>(Columns.FirstPushTime); }
            set { SetColumnValue(Columns.FirstPushTime, value); }
        }

        [XmlAttribute("LastPushTime")]
        [Bindable(true)]
        public DateTime LastPushTime
        {
            get { return GetColumnValue<DateTime>(Columns.LastPushTime); }
            set { SetColumnValue(Columns.LastPushTime, value); }
        }

        [XmlAttribute("FirstReadTime")]
        [Bindable(true)]
        public DateTime FirstReadTime
        {
            get { return GetColumnValue<DateTime>(Columns.FirstReadTime); }
            set { SetColumnValue(Columns.FirstReadTime, value); }
        }

        [XmlAttribute("LastReadTime")]
        [Bindable(true)]
        public DateTime LastReadTime
        {
            get { return GetColumnValue<DateTime>(Columns.LastReadTime); }
            set { SetColumnValue(Columns.LastReadTime, value); }
        }

        [XmlAttribute("FirstPushAppId")]
        [Bindable(true)]
        public int FirstPushAppId
        {
            get { return GetColumnValue<int>(Columns.FirstPushAppId); }
            set { SetColumnValue(Columns.FirstPushAppId, value); }
        }

        [XmlAttribute("LastPushAppId")]
        [Bindable(true)]
        public int LastPushAppId
        {
            get { return GetColumnValue<int>(Columns.LastPushAppId); }
            set { SetColumnValue(Columns.LastPushAppId, value); }
        }

        [XmlAttribute("IsAnalyzed")]
        [Bindable(true)]
        public bool IsAnalyzed
        {
            get { return GetColumnValue<bool>(Columns.IsAnalyzed); }
            set { SetColumnValue(Columns.IsAnalyzed, value); }
        }

        #endregion

        #region Typed Columns

        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }

        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }

        public static TableSchema.TableColumn FirstPushRecordIdColumn
        {
            get { return Schema.Columns[2]; }
        }

        public static TableSchema.TableColumn LastPushRecordIdColumn
        {
            get { return Schema.Columns[3]; }
        }

        public static TableSchema.TableColumn FirstActionIdColumn
        {
            get { return Schema.Columns[4]; }
        }

        public static TableSchema.TableColumn LastActionIdColumn
        {
            get { return Schema.Columns[5]; }
        }

        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }

        public static TableSchema.TableColumn FirstPushTimeColumn
        {
            get { return Schema.Columns[7]; }
        }

        public static TableSchema.TableColumn LastPushTimeColumn
        {
            get { return Schema.Columns[8]; }
        }

        public static TableSchema.TableColumn FirstReadTimeColumn
        {
            get { return Schema.Columns[9]; }
        }

        public static TableSchema.TableColumn LastReadTimeColumn
        {
            get { return Schema.Columns[10]; }
        }

        public static TableSchema.TableColumn FirstPushAppIdColumn
        {
            get { return Schema.Columns[11]; }
        }

        public static TableSchema.TableColumn LastPushAppIdColumn
        {
            get { return Schema.Columns[12]; }
        }

        public static TableSchema.TableColumn IsAnalyzedColumn
        {
            get { return Schema.Columns[13]; }
        }

        #endregion

        #region Columns Struct

        public struct Columns
        {
            public static string Id = @"id";
            public static string OrderGuid = @"order_guid";
            public static string FirstPushRecordId = @"first_push_record_id";
            public static string LastPushRecordId = @"last_push_record_id";
            public static string FirstActionId = @"first_action_id";
            public static string LastActionId = @"last_action_id";
            public static string CreateTime = @"create_time";
            public static string FirstPushTime = @"first_push_time";
            public static string LastPushTime = @"last_push_time";
            public static string FirstReadTime = @"first_read_time";
            public static string LastReadTime = @"last_read_time";
            public static string FirstPushAppId = @"first_push_app_id";
            public static string LastPushAppId = @"last_push_app_id";
            public static string IsAnalyzed = @"is_analyzed";
        }

        #endregion

    }
}

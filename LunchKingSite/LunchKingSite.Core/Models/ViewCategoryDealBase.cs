using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewCategoryDealBase class.
    /// </summary>
    [Serializable]
    public partial class ViewCategoryDealBaseCollection : ReadOnlyList<ViewCategoryDealBase, ViewCategoryDealBaseCollection>
    {        
        public ViewCategoryDealBaseCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_category_deal_base view.
    /// </summary>
    [Serializable]
    public partial class ViewCategoryDealBase : ReadOnlyRecord<ViewCategoryDealBase>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_category_deal_base", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBid);
                
                TableSchema.TableColumn colvarCid = new TableSchema.TableColumn(schema);
                colvarCid.ColumnName = "cid";
                colvarCid.DataType = DbType.Int32;
                colvarCid.MaxLength = 0;
                colvarCid.AutoIncrement = false;
                colvarCid.IsNullable = false;
                colvarCid.IsPrimaryKey = false;
                colvarCid.IsForeignKey = false;
                colvarCid.IsReadOnly = false;
                
                schema.Columns.Add(colvarCid);
                
                TableSchema.TableColumn colvarStartdt = new TableSchema.TableColumn(schema);
                colvarStartdt.ColumnName = "startdt";
                colvarStartdt.DataType = DbType.DateTime;
                colvarStartdt.MaxLength = 0;
                colvarStartdt.AutoIncrement = false;
                colvarStartdt.IsNullable = true;
                colvarStartdt.IsPrimaryKey = false;
                colvarStartdt.IsForeignKey = false;
                colvarStartdt.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartdt);
                
                TableSchema.TableColumn colvarEnddt = new TableSchema.TableColumn(schema);
                colvarEnddt.ColumnName = "enddt";
                colvarEnddt.DataType = DbType.DateTime;
                colvarEnddt.MaxLength = 0;
                colvarEnddt.AutoIncrement = false;
                colvarEnddt.IsNullable = true;
                colvarEnddt.IsPrimaryKey = false;
                colvarEnddt.IsForeignKey = false;
                colvarEnddt.IsReadOnly = false;
                
                schema.Columns.Add(colvarEnddt);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 50;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarRank = new TableSchema.TableColumn(schema);
                colvarRank.ColumnName = "rank";
                colvarRank.DataType = DbType.Int32;
                colvarRank.MaxLength = 0;
                colvarRank.AutoIncrement = false;
                colvarRank.IsNullable = true;
                colvarRank.IsPrimaryKey = false;
                colvarRank.IsForeignKey = false;
                colvarRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarRank);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = true;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarIsFinal = new TableSchema.TableColumn(schema);
                colvarIsFinal.ColumnName = "is_final";
                colvarIsFinal.DataType = DbType.Boolean;
                colvarIsFinal.MaxLength = 0;
                colvarIsFinal.AutoIncrement = false;
                colvarIsFinal.IsNullable = false;
                colvarIsFinal.IsPrimaryKey = false;
                colvarIsFinal.IsForeignKey = false;
                colvarIsFinal.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsFinal);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_category_deal_base",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewCategoryDealBase()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewCategoryDealBase(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewCategoryDealBase(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewCategoryDealBase(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("bid");
		    }
            set 
		    {
			    SetColumnValue("bid", value);
            }
        }
	      
        [XmlAttribute("Cid")]
        [Bindable(true)]
        public int Cid 
	    {
		    get
		    {
			    return GetColumnValue<int>("cid");
		    }
            set 
		    {
			    SetColumnValue("cid", value);
            }
        }
	      
        [XmlAttribute("Startdt")]
        [Bindable(true)]
        public DateTime? Startdt 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("startdt");
		    }
            set 
		    {
			    SetColumnValue("startdt", value);
            }
        }
	      
        [XmlAttribute("Enddt")]
        [Bindable(true)]
        public DateTime? Enddt 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("enddt");
		    }
            set 
		    {
			    SetColumnValue("enddt", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("Rank")]
        [Bindable(true)]
        public int? Rank 
	    {
		    get
		    {
			    return GetColumnValue<int?>("rank");
		    }
            set 
		    {
			    SetColumnValue("rank", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int? Type 
	    {
		    get
		    {
			    return GetColumnValue<int?>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("IsFinal")]
        [Bindable(true)]
        public bool IsFinal 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_final");
		    }
            set 
		    {
			    SetColumnValue("is_final", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string Bid = @"bid";
            
            public static string Cid = @"cid";
            
            public static string Startdt = @"startdt";
            
            public static string Enddt = @"enddt";
            
            public static string Name = @"name";
            
            public static string Rank = @"rank";
            
            public static string Type = @"type";
            
            public static string Status = @"status";
            
            public static string IsFinal = @"is_final";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

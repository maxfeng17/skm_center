using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the DealLabel class.
    /// </summary>
    [Serializable]
    public partial class DealLabelCollection : RepositoryList<DealLabel, DealLabelCollection>
    {
        public DealLabelCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealLabelCollection</returns>
        public DealLabelCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealLabel o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the deal_label table.
    /// </summary>
    [Serializable]
    public partial class DealLabel : RepositoryRecord<DealLabel>, IRecordBase
    {
        #region .ctors and Default Settings

        public DealLabel()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public DealLabel(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("deal_label", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarSystemCodeId = new TableSchema.TableColumn(schema);
                colvarSystemCodeId.ColumnName = "system_code_id";
                colvarSystemCodeId.DataType = DbType.Int32;
                colvarSystemCodeId.MaxLength = 0;
                colvarSystemCodeId.AutoIncrement = false;
                colvarSystemCodeId.IsNullable = true;
                colvarSystemCodeId.IsPrimaryKey = false;
                colvarSystemCodeId.IsForeignKey = false;
                colvarSystemCodeId.IsReadOnly = false;
                colvarSystemCodeId.DefaultSetting = @"";
                colvarSystemCodeId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSystemCodeId);

                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = true;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                colvarDealType.DefaultSetting = @"";
                colvarDealType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDealType);

                TableSchema.TableColumn colvarDealTypeDescription = new TableSchema.TableColumn(schema);
                colvarDealTypeDescription.ColumnName = "deal_type_description";
                colvarDealTypeDescription.DataType = DbType.String;
                colvarDealTypeDescription.MaxLength = 50;
                colvarDealTypeDescription.AutoIncrement = false;
                colvarDealTypeDescription.IsNullable = true;
                colvarDealTypeDescription.IsPrimaryKey = false;
                colvarDealTypeDescription.IsForeignKey = false;
                colvarDealTypeDescription.IsReadOnly = false;
                colvarDealTypeDescription.DefaultSetting = @"";
                colvarDealTypeDescription.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDealTypeDescription);

                TableSchema.TableColumn colvarShowType = new TableSchema.TableColumn(schema);
                colvarShowType.ColumnName = "show_type";
                colvarShowType.DataType = DbType.AnsiString;
                colvarShowType.MaxLength = 50;
                colvarShowType.AutoIncrement = false;
                colvarShowType.IsNullable = true;
                colvarShowType.IsPrimaryKey = false;
                colvarShowType.IsForeignKey = false;
                colvarShowType.IsReadOnly = false;
                colvarShowType.DefaultSetting = @"";
                colvarShowType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShowType);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("deal_label", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("SystemCodeId")]
        [Bindable(true)]
        public int? SystemCodeId
        {
            get { return GetColumnValue<int?>(Columns.SystemCodeId); }
            set { SetColumnValue(Columns.SystemCodeId, value); }
        }

        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int? DealType
        {
            get { return GetColumnValue<int?>(Columns.DealType); }
            set { SetColumnValue(Columns.DealType, value); }
        }

        [XmlAttribute("DealTypeDescription")]
        [Bindable(true)]
        public string DealTypeDescription
        {
            get { return GetColumnValue<string>(Columns.DealTypeDescription); }
            set { SetColumnValue(Columns.DealTypeDescription, value); }
        }

        [XmlAttribute("ShowType")]
        [Bindable(true)]
        public string ShowType
        {
            get { return GetColumnValue<string>(Columns.ShowType); }
            set { SetColumnValue(Columns.ShowType, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn SystemCodeIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn DealTypeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn DealTypeDescriptionColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ShowTypeColumn
        {
            get { return Schema.Columns[4]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string SystemCodeId = @"system_code_id";
            public static string DealType = @"deal_type";
            public static string DealTypeDescription = @"deal_type_description";
            public static string ShowType = @"show_type";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

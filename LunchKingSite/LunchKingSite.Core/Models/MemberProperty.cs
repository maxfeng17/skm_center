using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class MemberPropertyCollection : RepositoryList<MemberProperty, MemberPropertyCollection>
	{
			public MemberPropertyCollection() {}

			public MemberPropertyCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							MemberProperty o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class MemberProperty : RepositoryRecord<MemberProperty>, IRecordBase
	{
		#region .ctors and Default Settings
		public MemberProperty()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public MemberProperty(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_property", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				TableSchema.TableColumn colvarRegisterDeviceType = new TableSchema.TableColumn(schema);
				colvarRegisterDeviceType.ColumnName = "register_device_type";
				colvarRegisterDeviceType.DataType = DbType.Int32;
				colvarRegisterDeviceType.MaxLength = 0;
				colvarRegisterDeviceType.AutoIncrement = false;
				colvarRegisterDeviceType.IsNullable = false;
				colvarRegisterDeviceType.IsPrimaryKey = false;
				colvarRegisterDeviceType.IsForeignKey = false;
				colvarRegisterDeviceType.IsReadOnly = false;
				colvarRegisterDeviceType.DefaultSetting = @"";
				colvarRegisterDeviceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRegisterDeviceType);

				TableSchema.TableColumn colvarRegisterSourceType = new TableSchema.TableColumn(schema);
				colvarRegisterSourceType.ColumnName = "register_source_type";
				colvarRegisterSourceType.DataType = DbType.Int32;
				colvarRegisterSourceType.MaxLength = 0;
				colvarRegisterSourceType.AutoIncrement = false;
				colvarRegisterSourceType.IsNullable = false;
				colvarRegisterSourceType.IsPrimaryKey = false;
				colvarRegisterSourceType.IsForeignKey = false;
				colvarRegisterSourceType.IsReadOnly = false;
				colvarRegisterSourceType.DefaultSetting = @"";
				colvarRegisterSourceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRegisterSourceType);

				TableSchema.TableColumn colvarRegisterTime = new TableSchema.TableColumn(schema);
				colvarRegisterTime.ColumnName = "register_time";
				colvarRegisterTime.DataType = DbType.DateTime;
				colvarRegisterTime.MaxLength = 0;
				colvarRegisterTime.AutoIncrement = false;
				colvarRegisterTime.IsNullable = false;
				colvarRegisterTime.IsPrimaryKey = false;
				colvarRegisterTime.IsForeignKey = false;
				colvarRegisterTime.IsReadOnly = false;
				colvarRegisterTime.DefaultSetting = @"";
				colvarRegisterTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRegisterTime);

				TableSchema.TableColumn colvarRiskScore = new TableSchema.TableColumn(schema);
				colvarRiskScore.ColumnName = "risk_score";
				colvarRiskScore.DataType = DbType.Int32;
				colvarRiskScore.MaxLength = 0;
				colvarRiskScore.AutoIncrement = false;
				colvarRiskScore.IsNullable = false;
				colvarRiskScore.IsPrimaryKey = false;
				colvarRiskScore.IsForeignKey = false;
				colvarRiskScore.IsReadOnly = false;
				colvarRiskScore.DefaultSetting = @"((0))";
				colvarRiskScore.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRiskScore);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_property",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		[XmlAttribute("RegisterDeviceType")]
		[Bindable(true)]
		public int RegisterDeviceType
		{
			get { return GetColumnValue<int>(Columns.RegisterDeviceType); }
			set { SetColumnValue(Columns.RegisterDeviceType, value); }
		}

		[XmlAttribute("RegisterSourceType")]
		[Bindable(true)]
		public int RegisterSourceType
		{
			get { return GetColumnValue<int>(Columns.RegisterSourceType); }
			set { SetColumnValue(Columns.RegisterSourceType, value); }
		}

		[XmlAttribute("RegisterTime")]
		[Bindable(true)]
		public DateTime RegisterTime
		{
			get { return GetColumnValue<DateTime>(Columns.RegisterTime); }
			set { SetColumnValue(Columns.RegisterTime, value); }
		}

		[XmlAttribute("RiskScore")]
		[Bindable(true)]
		public int RiskScore
		{
			get { return GetColumnValue<int>(Columns.RiskScore); }
			set { SetColumnValue(Columns.RiskScore, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn RegisterDeviceTypeColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn RegisterSourceTypeColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn RegisterTimeColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn RiskScoreColumn
		{
			get { return Schema.Columns[5]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string UserId = @"user_id";
			public static string RegisterDeviceType = @"register_device_type";
			public static string RegisterSourceType = @"register_source_type";
			public static string RegisterTime = @"register_time";
			public static string RiskScore = @"risk_score";
		}

		#endregion

	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewIdentityCode class.
    /// </summary>
    [Serializable]
    public partial class ViewIdentityCodeCollection : ReadOnlyList<ViewIdentityCode, ViewIdentityCodeCollection>
    {        
        public ViewIdentityCodeCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_identity_code view.
    /// </summary>
    [Serializable]
    public partial class ViewIdentityCode : ReadOnlyRecord<ViewIdentityCode>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_identity_code", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int64;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = false;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerUserId);
                
                TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
                colvarCardGroupId.ColumnName = "card_group_id";
                colvarCardGroupId.DataType = DbType.Int32;
                colvarCardGroupId.MaxLength = 0;
                colvarCardGroupId.AutoIncrement = false;
                colvarCardGroupId.IsNullable = false;
                colvarCardGroupId.IsPrimaryKey = false;
                colvarCardGroupId.IsForeignKey = false;
                colvarCardGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardGroupId);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.String;
                colvarCode.MaxLength = 10;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = false;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarCardId = new TableSchema.TableColumn(schema);
                colvarCardId.ColumnName = "card_id";
                colvarCardId.DataType = DbType.Int32;
                colvarCardId.MaxLength = 0;
                colvarCardId.AutoIncrement = false;
                colvarCardId.IsNullable = false;
                colvarCardId.IsPrimaryKey = false;
                colvarCardId.IsForeignKey = false;
                colvarCardId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardId);
                
                TableSchema.TableColumn colvarUserMembershipCardId = new TableSchema.TableColumn(schema);
                colvarUserMembershipCardId.ColumnName = "user_membership_card_id";
                colvarUserMembershipCardId.DataType = DbType.Int32;
                colvarUserMembershipCardId.MaxLength = 0;
                colvarUserMembershipCardId.AutoIncrement = false;
                colvarUserMembershipCardId.IsNullable = false;
                colvarUserMembershipCardId.IsPrimaryKey = false;
                colvarUserMembershipCardId.IsForeignKey = false;
                colvarUserMembershipCardId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserMembershipCardId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarExpiredTime = new TableSchema.TableColumn(schema);
                colvarExpiredTime.ColumnName = "expired_time";
                colvarExpiredTime.DataType = DbType.DateTime;
                colvarExpiredTime.MaxLength = 0;
                colvarExpiredTime.AutoIncrement = false;
                colvarExpiredTime.IsNullable = true;
                colvarExpiredTime.IsPrimaryKey = false;
                colvarExpiredTime.IsForeignKey = false;
                colvarExpiredTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarExpiredTime);
                
                TableSchema.TableColumn colvarDiscountCodeId = new TableSchema.TableColumn(schema);
                colvarDiscountCodeId.ColumnName = "discount_code_id";
                colvarDiscountCodeId.DataType = DbType.Int32;
                colvarDiscountCodeId.MaxLength = 0;
                colvarDiscountCodeId.AutoIncrement = false;
                colvarDiscountCodeId.IsNullable = true;
                colvarDiscountCodeId.IsPrimaryKey = false;
                colvarDiscountCodeId.IsForeignKey = false;
                colvarDiscountCodeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountCodeId);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarLevel = new TableSchema.TableColumn(schema);
                colvarLevel.ColumnName = "level";
                colvarLevel.DataType = DbType.Int32;
                colvarLevel.MaxLength = 0;
                colvarLevel.AutoIncrement = false;
                colvarLevel.IsNullable = false;
                colvarLevel.IsPrimaryKey = false;
                colvarLevel.IsForeignKey = false;
                colvarLevel.IsReadOnly = false;
                
                schema.Columns.Add(colvarLevel);
                
                TableSchema.TableColumn colvarPaymentPercent = new TableSchema.TableColumn(schema);
                colvarPaymentPercent.ColumnName = "payment_percent";
                colvarPaymentPercent.DataType = DbType.Double;
                colvarPaymentPercent.MaxLength = 0;
                colvarPaymentPercent.AutoIncrement = false;
                colvarPaymentPercent.IsNullable = false;
                colvarPaymentPercent.IsPrimaryKey = false;
                colvarPaymentPercent.IsForeignKey = false;
                colvarPaymentPercent.IsReadOnly = false;
                
                schema.Columns.Add(colvarPaymentPercent);
                
                TableSchema.TableColumn colvarOthers = new TableSchema.TableColumn(schema);
                colvarOthers.ColumnName = "others";
                colvarOthers.DataType = DbType.String;
                colvarOthers.MaxLength = -1;
                colvarOthers.AutoIncrement = false;
                colvarOthers.IsNullable = true;
                colvarOthers.IsPrimaryKey = false;
                colvarOthers.IsForeignKey = false;
                colvarOthers.IsReadOnly = false;
                
                schema.Columns.Add(colvarOthers);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Byte;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_identity_code",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewIdentityCode()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewIdentityCode(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewIdentityCode(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewIdentityCode(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public long Id 
	    {
		    get
		    {
			    return GetColumnValue<long>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int SellerUserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_user_id");
		    }
            set 
		    {
			    SetColumnValue("seller_user_id", value);
            }
        }
	      
        [XmlAttribute("CardGroupId")]
        [Bindable(true)]
        public int CardGroupId 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_group_id");
		    }
            set 
		    {
			    SetColumnValue("card_group_id", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("CardId")]
        [Bindable(true)]
        public int CardId 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_id");
		    }
            set 
		    {
			    SetColumnValue("card_id", value);
            }
        }
	      
        [XmlAttribute("UserMembershipCardId")]
        [Bindable(true)]
        public int UserMembershipCardId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_membership_card_id");
		    }
            set 
		    {
			    SetColumnValue("user_membership_card_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ExpiredTime")]
        [Bindable(true)]
        public DateTime? ExpiredTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("expired_time");
		    }
            set 
		    {
			    SetColumnValue("expired_time", value);
            }
        }
	      
        [XmlAttribute("DiscountCodeId")]
        [Bindable(true)]
        public int? DiscountCodeId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("discount_code_id");
		    }
            set 
		    {
			    SetColumnValue("discount_code_id", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("Level")]
        [Bindable(true)]
        public int Level 
	    {
		    get
		    {
			    return GetColumnValue<int>("level");
		    }
            set 
		    {
			    SetColumnValue("level", value);
            }
        }
	      
        [XmlAttribute("PaymentPercent")]
        [Bindable(true)]
        public double PaymentPercent 
	    {
		    get
		    {
			    return GetColumnValue<double>("payment_percent");
		    }
            set 
		    {
			    SetColumnValue("payment_percent", value);
            }
        }
	      
        [XmlAttribute("Others")]
        [Bindable(true)]
        public string Others 
	    {
		    get
		    {
			    return GetColumnValue<string>("others");
		    }
            set 
		    {
			    SetColumnValue("others", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public byte Status 
	    {
		    get
		    {
			    return GetColumnValue<byte>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string SellerUserId = @"seller_user_id";
            
            public static string CardGroupId = @"card_group_id";
            
            public static string Code = @"code";
            
            public static string CardId = @"card_id";
            
            public static string UserMembershipCardId = @"user_membership_card_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ExpiredTime = @"expired_time";
            
            public static string DiscountCodeId = @"discount_code_id";
            
            public static string UserId = @"user_id";
            
            public static string Level = @"level";
            
            public static string PaymentPercent = @"payment_percent";
            
            public static string Others = @"others";
            
            public static string Status = @"status";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

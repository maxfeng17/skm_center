using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PeztempBatchLog class.
	/// </summary>
    [Serializable]
	public partial class PeztempBatchLogCollection : RepositoryList<PeztempBatchLog, PeztempBatchLogCollection>
	{	   
		public PeztempBatchLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PeztempBatchLogCollection</returns>
		public PeztempBatchLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PeztempBatchLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the peztemp_batch_log table.
	/// </summary>
	[Serializable]
	public partial class PeztempBatchLog : RepositoryRecord<PeztempBatchLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PeztempBatchLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PeztempBatchLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("peztemp_batch_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarServiceCode = new TableSchema.TableColumn(schema);
				colvarServiceCode.ColumnName = "service_code";
				colvarServiceCode.DataType = DbType.Int32;
				colvarServiceCode.MaxLength = 0;
				colvarServiceCode.AutoIncrement = false;
				colvarServiceCode.IsNullable = false;
				colvarServiceCode.IsPrimaryKey = false;
				colvarServiceCode.IsForeignKey = false;
				colvarServiceCode.IsReadOnly = false;
				colvarServiceCode.DefaultSetting = @"";
				colvarServiceCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceCode);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarRemarks = new TableSchema.TableColumn(schema);
				colvarRemarks.ColumnName = "remarks";
				colvarRemarks.DataType = DbType.String;
				colvarRemarks.MaxLength = 50;
				colvarRemarks.AutoIncrement = false;
				colvarRemarks.IsNullable = false;
				colvarRemarks.IsPrimaryKey = false;
				colvarRemarks.IsForeignKey = false;
				colvarRemarks.IsReadOnly = false;
				colvarRemarks.DefaultSetting = @"";
				colvarRemarks.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemarks);
				
				TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
				colvarQty.ColumnName = "qty";
				colvarQty.DataType = DbType.Int32;
				colvarQty.MaxLength = 0;
				colvarQty.AutoIncrement = false;
				colvarQty.IsNullable = false;
				colvarQty.IsPrimaryKey = false;
				colvarQty.IsForeignKey = false;
				colvarQty.IsReadOnly = false;
				colvarQty.DefaultSetting = @"";
				colvarQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQty);
				
				TableSchema.TableColumn colvarMemberUniqueId = new TableSchema.TableColumn(schema);
				colvarMemberUniqueId.ColumnName = "member_unique_id";
				colvarMemberUniqueId.DataType = DbType.Int32;
				colvarMemberUniqueId.MaxLength = 0;
				colvarMemberUniqueId.AutoIncrement = false;
				colvarMemberUniqueId.IsNullable = false;
				colvarMemberUniqueId.IsPrimaryKey = false;
				colvarMemberUniqueId.IsForeignKey = false;
				colvarMemberUniqueId.IsReadOnly = false;
				colvarMemberUniqueId.DefaultSetting = @"";
				colvarMemberUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberUniqueId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarIsProducedBackup = new TableSchema.TableColumn(schema);
				colvarIsProducedBackup.ColumnName = "is_Produced_backup";
				colvarIsProducedBackup.DataType = DbType.Boolean;
				colvarIsProducedBackup.MaxLength = 0;
				colvarIsProducedBackup.AutoIncrement = false;
				colvarIsProducedBackup.IsNullable = false;
				colvarIsProducedBackup.IsPrimaryKey = false;
				colvarIsProducedBackup.IsForeignKey = false;
				colvarIsProducedBackup.IsReadOnly = false;
				
						colvarIsProducedBackup.DefaultSetting = @"((0))";
				colvarIsProducedBackup.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsProducedBackup);
				
				TableSchema.TableColumn colvarBackupCodeQty = new TableSchema.TableColumn(schema);
				colvarBackupCodeQty.ColumnName = "backup_code_qty";
				colvarBackupCodeQty.DataType = DbType.Int32;
				colvarBackupCodeQty.MaxLength = 0;
				colvarBackupCodeQty.AutoIncrement = false;
				colvarBackupCodeQty.IsNullable = false;
				colvarBackupCodeQty.IsPrimaryKey = false;
				colvarBackupCodeQty.IsForeignKey = false;
				colvarBackupCodeQty.IsReadOnly = false;
				
						colvarBackupCodeQty.DefaultSetting = @"((0))";
				colvarBackupCodeQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBackupCodeQty);
				
				TableSchema.TableColumn colvarProducedBackupCodeTime = new TableSchema.TableColumn(schema);
				colvarProducedBackupCodeTime.ColumnName = "produced_backup_code_time";
				colvarProducedBackupCodeTime.DataType = DbType.DateTime;
				colvarProducedBackupCodeTime.MaxLength = 0;
				colvarProducedBackupCodeTime.AutoIncrement = false;
				colvarProducedBackupCodeTime.IsNullable = true;
				colvarProducedBackupCodeTime.IsPrimaryKey = false;
				colvarProducedBackupCodeTime.IsForeignKey = false;
				colvarProducedBackupCodeTime.IsReadOnly = false;
				colvarProducedBackupCodeTime.DefaultSetting = @"";
				colvarProducedBackupCodeTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProducedBackupCodeTime);
				
				TableSchema.TableColumn colvarCompleteTime = new TableSchema.TableColumn(schema);
				colvarCompleteTime.ColumnName = "complete_time";
				colvarCompleteTime.DataType = DbType.DateTime;
				colvarCompleteTime.MaxLength = 0;
				colvarCompleteTime.AutoIncrement = false;
				colvarCompleteTime.IsNullable = true;
				colvarCompleteTime.IsPrimaryKey = false;
				colvarCompleteTime.IsForeignKey = false;
				colvarCompleteTime.IsReadOnly = false;
				colvarCompleteTime.DefaultSetting = @"";
				colvarCompleteTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompleteTime);
				
				TableSchema.TableColumn colvarIsBackup = new TableSchema.TableColumn(schema);
				colvarIsBackup.ColumnName = "is_backup";
				colvarIsBackup.DataType = DbType.Boolean;
				colvarIsBackup.MaxLength = 0;
				colvarIsBackup.AutoIncrement = false;
				colvarIsBackup.IsNullable = false;
				colvarIsBackup.IsPrimaryKey = false;
				colvarIsBackup.IsForeignKey = false;
				colvarIsBackup.IsReadOnly = false;
				
						colvarIsBackup.DefaultSetting = @"((0))";
				colvarIsBackup.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsBackup);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("peztemp_batch_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ServiceCode")]
		[Bindable(true)]
		public int ServiceCode 
		{
			get { return GetColumnValue<int>(Columns.ServiceCode); }
			set { SetColumnValue(Columns.ServiceCode, value); }
		}
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("Remarks")]
		[Bindable(true)]
		public string Remarks 
		{
			get { return GetColumnValue<string>(Columns.Remarks); }
			set { SetColumnValue(Columns.Remarks, value); }
		}
		  
		[XmlAttribute("Qty")]
		[Bindable(true)]
		public int Qty 
		{
			get { return GetColumnValue<int>(Columns.Qty); }
			set { SetColumnValue(Columns.Qty, value); }
		}
		  
		[XmlAttribute("MemberUniqueId")]
		[Bindable(true)]
		public int MemberUniqueId 
		{
			get { return GetColumnValue<int>(Columns.MemberUniqueId); }
			set { SetColumnValue(Columns.MemberUniqueId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("IsProducedBackup")]
		[Bindable(true)]
		public bool IsProducedBackup 
		{
			get { return GetColumnValue<bool>(Columns.IsProducedBackup); }
			set { SetColumnValue(Columns.IsProducedBackup, value); }
		}
		  
		[XmlAttribute("BackupCodeQty")]
		[Bindable(true)]
		public int BackupCodeQty 
		{
			get { return GetColumnValue<int>(Columns.BackupCodeQty); }
			set { SetColumnValue(Columns.BackupCodeQty, value); }
		}
		  
		[XmlAttribute("ProducedBackupCodeTime")]
		[Bindable(true)]
		public DateTime? ProducedBackupCodeTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ProducedBackupCodeTime); }
			set { SetColumnValue(Columns.ProducedBackupCodeTime, value); }
		}
		  
		[XmlAttribute("CompleteTime")]
		[Bindable(true)]
		public DateTime? CompleteTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CompleteTime); }
			set { SetColumnValue(Columns.CompleteTime, value); }
		}
		  
		[XmlAttribute("IsBackup")]
		[Bindable(true)]
		public bool IsBackup 
		{
			get { return GetColumnValue<bool>(Columns.IsBackup); }
			set { SetColumnValue(Columns.IsBackup, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ServiceCodeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RemarksColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn QtyColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberUniqueIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IsProducedBackupColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn BackupCodeQtyColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ProducedBackupCodeTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CompleteTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn IsBackupColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ServiceCode = @"service_code";
			 public static string Bid = @"bid";
			 public static string Remarks = @"remarks";
			 public static string Qty = @"qty";
			 public static string MemberUniqueId = @"member_unique_id";
			 public static string CreateTime = @"create_time";
			 public static string IsProducedBackup = @"is_Produced_backup";
			 public static string BackupCodeQty = @"backup_code_qty";
			 public static string ProducedBackupCodeTime = @"produced_backup_code_time";
			 public static string CompleteTime = @"complete_time";
			 public static string IsBackup = @"is_backup";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealReturned class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealReturnedCollection : ReadOnlyList<ViewHiDealReturned, ViewHiDealReturnedCollection>
    {        
        public ViewHiDealReturnedCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_returned view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealReturned : ReadOnlyRecord<ViewHiDealReturned>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_returned", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderPk = new TableSchema.TableColumn(schema);
                colvarOrderPk.ColumnName = "order_pk";
                colvarOrderPk.DataType = DbType.Int32;
                colvarOrderPk.MaxLength = 0;
                colvarOrderPk.AutoIncrement = false;
                colvarOrderPk.IsNullable = false;
                colvarOrderPk.IsPrimaryKey = false;
                colvarOrderPk.IsForeignKey = false;
                colvarOrderPk.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderPk);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = false;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarTotalAmount = new TableSchema.TableColumn(schema);
                colvarTotalAmount.ColumnName = "total_amount";
                colvarTotalAmount.DataType = DbType.Currency;
                colvarTotalAmount.MaxLength = 0;
                colvarTotalAmount.AutoIncrement = false;
                colvarTotalAmount.IsNullable = false;
                colvarTotalAmount.IsPrimaryKey = false;
                colvarTotalAmount.IsForeignKey = false;
                colvarTotalAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalAmount);
                
                TableSchema.TableColumn colvarPcash = new TableSchema.TableColumn(schema);
                colvarPcash.ColumnName = "pcash";
                colvarPcash.DataType = DbType.Currency;
                colvarPcash.MaxLength = 0;
                colvarPcash.AutoIncrement = false;
                colvarPcash.IsNullable = false;
                colvarPcash.IsPrimaryKey = false;
                colvarPcash.IsForeignKey = false;
                colvarPcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarPcash);
                
                TableSchema.TableColumn colvarScash = new TableSchema.TableColumn(schema);
                colvarScash.ColumnName = "scash";
                colvarScash.DataType = DbType.Currency;
                colvarScash.MaxLength = 0;
                colvarScash.AutoIncrement = false;
                colvarScash.IsNullable = false;
                colvarScash.IsPrimaryKey = false;
                colvarScash.IsForeignKey = false;
                colvarScash.IsReadOnly = false;
                
                schema.Columns.Add(colvarScash);
                
                TableSchema.TableColumn colvarBcash = new TableSchema.TableColumn(schema);
                colvarBcash.ColumnName = "bcash";
                colvarBcash.DataType = DbType.Currency;
                colvarBcash.MaxLength = 0;
                colvarBcash.AutoIncrement = false;
                colvarBcash.IsNullable = false;
                colvarBcash.IsPrimaryKey = false;
                colvarBcash.IsForeignKey = false;
                colvarBcash.IsReadOnly = false;
                
                schema.Columns.Add(colvarBcash);
                
                TableSchema.TableColumn colvarCreditCardAmt = new TableSchema.TableColumn(schema);
                colvarCreditCardAmt.ColumnName = "credit_card_amt";
                colvarCreditCardAmt.DataType = DbType.Currency;
                colvarCreditCardAmt.MaxLength = 0;
                colvarCreditCardAmt.AutoIncrement = false;
                colvarCreditCardAmt.IsNullable = false;
                colvarCreditCardAmt.IsPrimaryKey = false;
                colvarCreditCardAmt.IsForeignKey = false;
                colvarCreditCardAmt.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreditCardAmt);
                
                TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
                colvarDiscountAmount.ColumnName = "discount_amount";
                colvarDiscountAmount.DataType = DbType.Currency;
                colvarDiscountAmount.MaxLength = 0;
                colvarDiscountAmount.AutoIncrement = false;
                colvarDiscountAmount.IsNullable = false;
                colvarDiscountAmount.IsPrimaryKey = false;
                colvarDiscountAmount.IsForeignKey = false;
                colvarDiscountAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountAmount);
                
                TableSchema.TableColumn colvarAtmAmount = new TableSchema.TableColumn(schema);
                colvarAtmAmount.ColumnName = "atm_amount";
                colvarAtmAmount.DataType = DbType.Currency;
                colvarAtmAmount.MaxLength = 0;
                colvarAtmAmount.AutoIncrement = false;
                colvarAtmAmount.IsNullable = false;
                colvarAtmAmount.IsPrimaryKey = false;
                colvarAtmAmount.IsForeignKey = false;
                colvarAtmAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAtmAmount);
                
                TableSchema.TableColumn colvarReturnedStatus = new TableSchema.TableColumn(schema);
                colvarReturnedStatus.ColumnName = "returned_status";
                colvarReturnedStatus.DataType = DbType.Int32;
                colvarReturnedStatus.MaxLength = 0;
                colvarReturnedStatus.AutoIncrement = false;
                colvarReturnedStatus.IsNullable = false;
                colvarReturnedStatus.IsPrimaryKey = false;
                colvarReturnedStatus.IsForeignKey = false;
                colvarReturnedStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedStatus);
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarReturnedId = new TableSchema.TableColumn(schema);
                colvarReturnedId.ColumnName = "returned_id";
                colvarReturnedId.DataType = DbType.Int32;
                colvarReturnedId.MaxLength = 0;
                colvarReturnedId.AutoIncrement = false;
                colvarReturnedId.IsNullable = false;
                colvarReturnedId.IsPrimaryKey = false;
                colvarReturnedId.IsForeignKey = false;
                colvarReturnedId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedId);
                
                TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
                colvarOrderDetailGuid.ColumnName = "order_detail_guid";
                colvarOrderDetailGuid.DataType = DbType.Guid;
                colvarOrderDetailGuid.MaxLength = 0;
                colvarOrderDetailGuid.AutoIncrement = false;
                colvarOrderDetailGuid.IsNullable = false;
                colvarOrderDetailGuid.IsPrimaryKey = false;
                colvarOrderDetailGuid.IsForeignKey = false;
                colvarOrderDetailGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailGuid);
                
                TableSchema.TableColumn colvarHiDealId = new TableSchema.TableColumn(schema);
                colvarHiDealId.ColumnName = "hi_deal_id";
                colvarHiDealId.DataType = DbType.Int32;
                colvarHiDealId.MaxLength = 0;
                colvarHiDealId.AutoIncrement = false;
                colvarHiDealId.IsNullable = false;
                colvarHiDealId.IsPrimaryKey = false;
                colvarHiDealId.IsForeignKey = false;
                colvarHiDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = false;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "product_name";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 250;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = false;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductName);
                
                TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
                colvarItemQuantity.ColumnName = "item_quantity";
                colvarItemQuantity.DataType = DbType.Int32;
                colvarItemQuantity.MaxLength = 0;
                colvarItemQuantity.AutoIncrement = false;
                colvarItemQuantity.IsNullable = false;
                colvarItemQuantity.IsPrimaryKey = false;
                colvarItemQuantity.IsForeignKey = false;
                colvarItemQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemQuantity);
                
                TableSchema.TableColumn colvarUnitPrice = new TableSchema.TableColumn(schema);
                colvarUnitPrice.ColumnName = "unit_price";
                colvarUnitPrice.DataType = DbType.Currency;
                colvarUnitPrice.MaxLength = 0;
                colvarUnitPrice.AutoIncrement = false;
                colvarUnitPrice.IsNullable = false;
                colvarUnitPrice.IsPrimaryKey = false;
                colvarUnitPrice.IsForeignKey = false;
                colvarUnitPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarUnitPrice);
                
                TableSchema.TableColumn colvarDetailTotalAmt = new TableSchema.TableColumn(schema);
                colvarDetailTotalAmt.ColumnName = "detail_total_amt";
                colvarDetailTotalAmt.DataType = DbType.Currency;
                colvarDetailTotalAmt.MaxLength = 0;
                colvarDetailTotalAmt.AutoIncrement = false;
                colvarDetailTotalAmt.IsNullable = false;
                colvarDetailTotalAmt.IsPrimaryKey = false;
                colvarDetailTotalAmt.IsForeignKey = false;
                colvarDetailTotalAmt.IsReadOnly = false;
                
                schema.Columns.Add(colvarDetailTotalAmt);
                
                TableSchema.TableColumn colvarCategory1 = new TableSchema.TableColumn(schema);
                colvarCategory1.ColumnName = "category1";
                colvarCategory1.DataType = DbType.Int32;
                colvarCategory1.MaxLength = 0;
                colvarCategory1.AutoIncrement = false;
                colvarCategory1.IsNullable = true;
                colvarCategory1.IsPrimaryKey = false;
                colvarCategory1.IsForeignKey = false;
                colvarCategory1.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory1);
                
                TableSchema.TableColumn colvarOption1 = new TableSchema.TableColumn(schema);
                colvarOption1.ColumnName = "option1";
                colvarOption1.DataType = DbType.Int32;
                colvarOption1.MaxLength = 0;
                colvarOption1.AutoIncrement = false;
                colvarOption1.IsNullable = true;
                colvarOption1.IsPrimaryKey = false;
                colvarOption1.IsForeignKey = false;
                colvarOption1.IsReadOnly = false;
                
                schema.Columns.Add(colvarOption1);
                
                TableSchema.TableColumn colvarOptionName1 = new TableSchema.TableColumn(schema);
                colvarOptionName1.ColumnName = "option_name1";
                colvarOptionName1.DataType = DbType.String;
                colvarOptionName1.MaxLength = 50;
                colvarOptionName1.AutoIncrement = false;
                colvarOptionName1.IsNullable = true;
                colvarOptionName1.IsPrimaryKey = false;
                colvarOptionName1.IsForeignKey = false;
                colvarOptionName1.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName1);
                
                TableSchema.TableColumn colvarCategory2 = new TableSchema.TableColumn(schema);
                colvarCategory2.ColumnName = "category2";
                colvarCategory2.DataType = DbType.Int32;
                colvarCategory2.MaxLength = 0;
                colvarCategory2.AutoIncrement = false;
                colvarCategory2.IsNullable = true;
                colvarCategory2.IsPrimaryKey = false;
                colvarCategory2.IsForeignKey = false;
                colvarCategory2.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory2);
                
                TableSchema.TableColumn colvarOption2 = new TableSchema.TableColumn(schema);
                colvarOption2.ColumnName = "option2";
                colvarOption2.DataType = DbType.Int32;
                colvarOption2.MaxLength = 0;
                colvarOption2.AutoIncrement = false;
                colvarOption2.IsNullable = true;
                colvarOption2.IsPrimaryKey = false;
                colvarOption2.IsForeignKey = false;
                colvarOption2.IsReadOnly = false;
                
                schema.Columns.Add(colvarOption2);
                
                TableSchema.TableColumn colvarOptionName2 = new TableSchema.TableColumn(schema);
                colvarOptionName2.ColumnName = "option_name2";
                colvarOptionName2.DataType = DbType.String;
                colvarOptionName2.MaxLength = 50;
                colvarOptionName2.AutoIncrement = false;
                colvarOptionName2.IsNullable = true;
                colvarOptionName2.IsPrimaryKey = false;
                colvarOptionName2.IsForeignKey = false;
                colvarOptionName2.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName2);
                
                TableSchema.TableColumn colvarCategory3 = new TableSchema.TableColumn(schema);
                colvarCategory3.ColumnName = "category3";
                colvarCategory3.DataType = DbType.Int32;
                colvarCategory3.MaxLength = 0;
                colvarCategory3.AutoIncrement = false;
                colvarCategory3.IsNullable = true;
                colvarCategory3.IsPrimaryKey = false;
                colvarCategory3.IsForeignKey = false;
                colvarCategory3.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory3);
                
                TableSchema.TableColumn colvarOption3 = new TableSchema.TableColumn(schema);
                colvarOption3.ColumnName = "option3";
                colvarOption3.DataType = DbType.Int32;
                colvarOption3.MaxLength = 0;
                colvarOption3.AutoIncrement = false;
                colvarOption3.IsNullable = true;
                colvarOption3.IsPrimaryKey = false;
                colvarOption3.IsForeignKey = false;
                colvarOption3.IsReadOnly = false;
                
                schema.Columns.Add(colvarOption3);
                
                TableSchema.TableColumn colvarOptionName3 = new TableSchema.TableColumn(schema);
                colvarOptionName3.ColumnName = "option_name3";
                colvarOptionName3.DataType = DbType.String;
                colvarOptionName3.MaxLength = 50;
                colvarOptionName3.AutoIncrement = false;
                colvarOptionName3.IsNullable = true;
                colvarOptionName3.IsPrimaryKey = false;
                colvarOptionName3.IsForeignKey = false;
                colvarOptionName3.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName3);
                
                TableSchema.TableColumn colvarCategory4 = new TableSchema.TableColumn(schema);
                colvarCategory4.ColumnName = "category4";
                colvarCategory4.DataType = DbType.Int32;
                colvarCategory4.MaxLength = 0;
                colvarCategory4.AutoIncrement = false;
                colvarCategory4.IsNullable = true;
                colvarCategory4.IsPrimaryKey = false;
                colvarCategory4.IsForeignKey = false;
                colvarCategory4.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory4);
                
                TableSchema.TableColumn colvarOption4 = new TableSchema.TableColumn(schema);
                colvarOption4.ColumnName = "option4";
                colvarOption4.DataType = DbType.Int32;
                colvarOption4.MaxLength = 0;
                colvarOption4.AutoIncrement = false;
                colvarOption4.IsNullable = true;
                colvarOption4.IsPrimaryKey = false;
                colvarOption4.IsForeignKey = false;
                colvarOption4.IsReadOnly = false;
                
                schema.Columns.Add(colvarOption4);
                
                TableSchema.TableColumn colvarOptionName4 = new TableSchema.TableColumn(schema);
                colvarOptionName4.ColumnName = "option_name4";
                colvarOptionName4.DataType = DbType.String;
                colvarOptionName4.MaxLength = 50;
                colvarOptionName4.AutoIncrement = false;
                colvarOptionName4.IsNullable = true;
                colvarOptionName4.IsPrimaryKey = false;
                colvarOptionName4.IsForeignKey = false;
                colvarOptionName4.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName4);
                
                TableSchema.TableColumn colvarCategory5 = new TableSchema.TableColumn(schema);
                colvarCategory5.ColumnName = "category5";
                colvarCategory5.DataType = DbType.Int32;
                colvarCategory5.MaxLength = 0;
                colvarCategory5.AutoIncrement = false;
                colvarCategory5.IsNullable = true;
                colvarCategory5.IsPrimaryKey = false;
                colvarCategory5.IsForeignKey = false;
                colvarCategory5.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory5);
                
                TableSchema.TableColumn colvarOption5 = new TableSchema.TableColumn(schema);
                colvarOption5.ColumnName = "option5";
                colvarOption5.DataType = DbType.Int32;
                colvarOption5.MaxLength = 0;
                colvarOption5.AutoIncrement = false;
                colvarOption5.IsNullable = true;
                colvarOption5.IsPrimaryKey = false;
                colvarOption5.IsForeignKey = false;
                colvarOption5.IsReadOnly = false;
                
                schema.Columns.Add(colvarOption5);
                
                TableSchema.TableColumn colvarOptionName5 = new TableSchema.TableColumn(schema);
                colvarOptionName5.ColumnName = "option_name5";
                colvarOptionName5.DataType = DbType.String;
                colvarOptionName5.MaxLength = 50;
                colvarOptionName5.AutoIncrement = false;
                colvarOptionName5.IsNullable = true;
                colvarOptionName5.IsPrimaryKey = false;
                colvarOptionName5.IsForeignKey = false;
                colvarOptionName5.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName5);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = false;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 256;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarProductType = new TableSchema.TableColumn(schema);
                colvarProductType.ColumnName = "product_type";
                colvarProductType.DataType = DbType.Int32;
                colvarProductType.MaxLength = 0;
                colvarProductType.AutoIncrement = false;
                colvarProductType.IsNullable = false;
                colvarProductType.IsPrimaryKey = false;
                colvarProductType.IsForeignKey = false;
                colvarProductType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_returned",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealReturned()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealReturned(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealReturned(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealReturned(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderPk")]
        [Bindable(true)]
        public int OrderPk 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_pk");
		    }
            set 
		    {
			    SetColumnValue("order_pk", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("TotalAmount")]
        [Bindable(true)]
        public decimal TotalAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total_amount");
		    }
            set 
		    {
			    SetColumnValue("total_amount", value);
            }
        }
	      
        [XmlAttribute("Pcash")]
        [Bindable(true)]
        public decimal Pcash 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("pcash");
		    }
            set 
		    {
			    SetColumnValue("pcash", value);
            }
        }
	      
        [XmlAttribute("Scash")]
        [Bindable(true)]
        public decimal Scash 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("scash");
		    }
            set 
		    {
			    SetColumnValue("scash", value);
            }
        }
	      
        [XmlAttribute("Bcash")]
        [Bindable(true)]
        public decimal Bcash 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("bcash");
		    }
            set 
		    {
			    SetColumnValue("bcash", value);
            }
        }
	      
        [XmlAttribute("CreditCardAmt")]
        [Bindable(true)]
        public decimal CreditCardAmt 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("credit_card_amt");
		    }
            set 
		    {
			    SetColumnValue("credit_card_amt", value);
            }
        }
	      
        [XmlAttribute("DiscountAmount")]
        [Bindable(true)]
        public decimal DiscountAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("discount_amount");
		    }
            set 
		    {
			    SetColumnValue("discount_amount", value);
            }
        }
	      
        [XmlAttribute("AtmAmount")]
        [Bindable(true)]
        public decimal AtmAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("atm_amount");
		    }
            set 
		    {
			    SetColumnValue("atm_amount", value);
            }
        }
	      
        [XmlAttribute("ReturnedStatus")]
        [Bindable(true)]
        public int ReturnedStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("returned_status");
		    }
            set 
		    {
			    SetColumnValue("returned_status", value);
            }
        }
	      
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("ReturnedId")]
        [Bindable(true)]
        public int ReturnedId 
	    {
		    get
		    {
			    return GetColumnValue<int>("returned_id");
		    }
            set 
		    {
			    SetColumnValue("returned_id", value);
            }
        }
	      
        [XmlAttribute("OrderDetailGuid")]
        [Bindable(true)]
        public Guid OrderDetailGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_detail_guid");
		    }
            set 
		    {
			    SetColumnValue("order_detail_guid", value);
            }
        }
	      
        [XmlAttribute("HiDealId")]
        [Bindable(true)]
        public int HiDealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("hi_deal_id");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_name");
		    }
            set 
		    {
			    SetColumnValue("product_name", value);
            }
        }
	      
        [XmlAttribute("ItemQuantity")]
        [Bindable(true)]
        public int ItemQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_quantity");
		    }
            set 
		    {
			    SetColumnValue("item_quantity", value);
            }
        }
	      
        [XmlAttribute("UnitPrice")]
        [Bindable(true)]
        public decimal UnitPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("unit_price");
		    }
            set 
		    {
			    SetColumnValue("unit_price", value);
            }
        }
	      
        [XmlAttribute("DetailTotalAmt")]
        [Bindable(true)]
        public decimal DetailTotalAmt 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("detail_total_amt");
		    }
            set 
		    {
			    SetColumnValue("detail_total_amt", value);
            }
        }
	      
        [XmlAttribute("Category1")]
        [Bindable(true)]
        public int? Category1 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category1");
		    }
            set 
		    {
			    SetColumnValue("category1", value);
            }
        }
	      
        [XmlAttribute("Option1")]
        [Bindable(true)]
        public int? Option1 
	    {
		    get
		    {
			    return GetColumnValue<int?>("option1");
		    }
            set 
		    {
			    SetColumnValue("option1", value);
            }
        }
	      
        [XmlAttribute("OptionName1")]
        [Bindable(true)]
        public string OptionName1 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name1");
		    }
            set 
		    {
			    SetColumnValue("option_name1", value);
            }
        }
	      
        [XmlAttribute("Category2")]
        [Bindable(true)]
        public int? Category2 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category2");
		    }
            set 
		    {
			    SetColumnValue("category2", value);
            }
        }
	      
        [XmlAttribute("Option2")]
        [Bindable(true)]
        public int? Option2 
	    {
		    get
		    {
			    return GetColumnValue<int?>("option2");
		    }
            set 
		    {
			    SetColumnValue("option2", value);
            }
        }
	      
        [XmlAttribute("OptionName2")]
        [Bindable(true)]
        public string OptionName2 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name2");
		    }
            set 
		    {
			    SetColumnValue("option_name2", value);
            }
        }
	      
        [XmlAttribute("Category3")]
        [Bindable(true)]
        public int? Category3 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category3");
		    }
            set 
		    {
			    SetColumnValue("category3", value);
            }
        }
	      
        [XmlAttribute("Option3")]
        [Bindable(true)]
        public int? Option3 
	    {
		    get
		    {
			    return GetColumnValue<int?>("option3");
		    }
            set 
		    {
			    SetColumnValue("option3", value);
            }
        }
	      
        [XmlAttribute("OptionName3")]
        [Bindable(true)]
        public string OptionName3 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name3");
		    }
            set 
		    {
			    SetColumnValue("option_name3", value);
            }
        }
	      
        [XmlAttribute("Category4")]
        [Bindable(true)]
        public int? Category4 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category4");
		    }
            set 
		    {
			    SetColumnValue("category4", value);
            }
        }
	      
        [XmlAttribute("Option4")]
        [Bindable(true)]
        public int? Option4 
	    {
		    get
		    {
			    return GetColumnValue<int?>("option4");
		    }
            set 
		    {
			    SetColumnValue("option4", value);
            }
        }
	      
        [XmlAttribute("OptionName4")]
        [Bindable(true)]
        public string OptionName4 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name4");
		    }
            set 
		    {
			    SetColumnValue("option_name4", value);
            }
        }
	      
        [XmlAttribute("Category5")]
        [Bindable(true)]
        public int? Category5 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category5");
		    }
            set 
		    {
			    SetColumnValue("category5", value);
            }
        }
	      
        [XmlAttribute("Option5")]
        [Bindable(true)]
        public int? Option5 
	    {
		    get
		    {
			    return GetColumnValue<int?>("option5");
		    }
            set 
		    {
			    SetColumnValue("option5", value);
            }
        }
	      
        [XmlAttribute("OptionName5")]
        [Bindable(true)]
        public string OptionName5 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name5");
		    }
            set 
		    {
			    SetColumnValue("option_name5", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("ProductType")]
        [Bindable(true)]
        public int ProductType 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_type");
		    }
            set 
		    {
			    SetColumnValue("product_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderPk = @"order_pk";
            
            public static string OrderId = @"order_id";
            
            public static string UserName = @"user_name";
            
            public static string TotalAmount = @"total_amount";
            
            public static string Pcash = @"pcash";
            
            public static string Scash = @"scash";
            
            public static string Bcash = @"bcash";
            
            public static string CreditCardAmt = @"credit_card_amt";
            
            public static string DiscountAmount = @"discount_amount";
            
            public static string AtmAmount = @"atm_amount";
            
            public static string ReturnedStatus = @"returned_status";
            
            public static string Id = @"id";
            
            public static string ReturnedId = @"returned_id";
            
            public static string OrderDetailGuid = @"order_detail_guid";
            
            public static string HiDealId = @"hi_deal_id";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string SellerName = @"seller_name";
            
            public static string ProductId = @"product_id";
            
            public static string ProductName = @"product_name";
            
            public static string ItemQuantity = @"item_quantity";
            
            public static string UnitPrice = @"unit_price";
            
            public static string DetailTotalAmt = @"detail_total_amt";
            
            public static string Category1 = @"category1";
            
            public static string Option1 = @"option1";
            
            public static string OptionName1 = @"option_name1";
            
            public static string Category2 = @"category2";
            
            public static string Option2 = @"option2";
            
            public static string OptionName2 = @"option_name2";
            
            public static string Category3 = @"category3";
            
            public static string Option3 = @"option3";
            
            public static string OptionName3 = @"option_name3";
            
            public static string Category4 = @"category4";
            
            public static string Option4 = @"option4";
            
            public static string OptionName4 = @"option_name4";
            
            public static string Category5 = @"category5";
            
            public static string Option5 = @"option5";
            
            public static string OptionName5 = @"option_name5";
            
            public static string StoreGuid = @"store_guid";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string ModifyTime = @"modify_time";
            
            public static string ProductType = @"product_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

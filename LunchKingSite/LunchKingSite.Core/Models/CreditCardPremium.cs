using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CreditCardPremium class.
	/// </summary>
    [Serializable]
	public partial class CreditCardPremiumCollection : RepositoryList<CreditCardPremium, CreditCardPremiumCollection>
	{	   
		public CreditCardPremiumCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CreditCardPremiumCollection</returns>
		public CreditCardPremiumCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CreditCardPremium o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the credit_card_premium table.
	/// </summary>
	[Serializable]
	public partial class CreditCardPremium : RepositoryRecord<CreditCardPremium>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CreditCardPremium()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CreditCardPremium(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("credit_card_premium", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarActivityCode = new TableSchema.TableColumn(schema);
				colvarActivityCode.ColumnName = "activity_code";
				colvarActivityCode.DataType = DbType.AnsiString;
				colvarActivityCode.MaxLength = 50;
				colvarActivityCode.AutoIncrement = false;
				colvarActivityCode.IsNullable = false;
				colvarActivityCode.IsPrimaryKey = false;
				colvarActivityCode.IsForeignKey = false;
				colvarActivityCode.IsReadOnly = false;
				colvarActivityCode.DefaultSetting = @"";
				colvarActivityCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActivityCode);
				
				TableSchema.TableColumn colvarActivityName = new TableSchema.TableColumn(schema);
				colvarActivityName.ColumnName = "activity_name";
				colvarActivityName.DataType = DbType.String;
				colvarActivityName.MaxLength = 256;
				colvarActivityName.AutoIncrement = false;
				colvarActivityName.IsNullable = false;
				colvarActivityName.IsPrimaryKey = false;
				colvarActivityName.IsForeignKey = false;
				colvarActivityName.IsReadOnly = false;
				colvarActivityName.DefaultSetting = @"";
				colvarActivityName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActivityName);
				
				TableSchema.TableColumn colvarCardNum = new TableSchema.TableColumn(schema);
				colvarCardNum.ColumnName = "card_num";
				colvarCardNum.DataType = DbType.AnsiString;
				colvarCardNum.MaxLength = 16;
				colvarCardNum.AutoIncrement = false;
				colvarCardNum.IsNullable = false;
				colvarCardNum.IsPrimaryKey = false;
				colvarCardNum.IsForeignKey = false;
				colvarCardNum.IsReadOnly = false;
				colvarCardNum.DefaultSetting = @"";
				colvarCardNum.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardNum);
				
				TableSchema.TableColumn colvarCardType = new TableSchema.TableColumn(schema);
				colvarCardType.ColumnName = "card_type";
				colvarCardType.DataType = DbType.Int32;
				colvarCardType.MaxLength = 0;
				colvarCardType.AutoIncrement = false;
				colvarCardType.IsNullable = false;
				colvarCardType.IsPrimaryKey = false;
				colvarCardType.IsForeignKey = false;
				colvarCardType.IsReadOnly = false;
				
						colvarCardType.DefaultSetting = @"((0))";
				colvarCardType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardType);
				
				TableSchema.TableColumn colvarCardComment = new TableSchema.TableColumn(schema);
				colvarCardComment.ColumnName = "card_comment";
				colvarCardComment.DataType = DbType.String;
				colvarCardComment.MaxLength = 100;
				colvarCardComment.AutoIncrement = false;
				colvarCardComment.IsNullable = true;
				colvarCardComment.IsPrimaryKey = false;
				colvarCardComment.IsForeignKey = false;
				colvarCardComment.IsReadOnly = false;
				colvarCardComment.DefaultSetting = @"";
				colvarCardComment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardComment);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = false;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				
						colvarEnabled.DefaultSetting = @"((1))";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("credit_card_premium",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ActivityCode")]
		[Bindable(true)]
		public string ActivityCode 
		{
			get { return GetColumnValue<string>(Columns.ActivityCode); }
			set { SetColumnValue(Columns.ActivityCode, value); }
		}
		  
		[XmlAttribute("ActivityName")]
		[Bindable(true)]
		public string ActivityName 
		{
			get { return GetColumnValue<string>(Columns.ActivityName); }
			set { SetColumnValue(Columns.ActivityName, value); }
		}
		  
		[XmlAttribute("CardNum")]
		[Bindable(true)]
		public string CardNum 
		{
			get { return GetColumnValue<string>(Columns.CardNum); }
			set { SetColumnValue(Columns.CardNum, value); }
		}
		  
		[XmlAttribute("CardType")]
		[Bindable(true)]
		public int CardType 
		{
			get { return GetColumnValue<int>(Columns.CardType); }
			set { SetColumnValue(Columns.CardType, value); }
		}
		  
		[XmlAttribute("CardComment")]
		[Bindable(true)]
		public string CardComment 
		{
			get { return GetColumnValue<string>(Columns.CardComment); }
			set { SetColumnValue(Columns.CardComment, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool Enabled 
		{
			get { return GetColumnValue<bool>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ActivityCodeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ActivityNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CardNumColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CardTypeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CardCommentColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ActivityCode = @"activity_code";
			 public static string ActivityName = @"activity_name";
			 public static string CardNum = @"card_num";
			 public static string CardType = @"card_type";
			 public static string CardComment = @"card_comment";
			 public static string Enabled = @"enabled";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

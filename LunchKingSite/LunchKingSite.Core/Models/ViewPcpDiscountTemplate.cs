using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpDiscountTemplate class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpDiscountTemplateCollection : ReadOnlyList<ViewPcpDiscountTemplate, ViewPcpDiscountTemplateCollection>
    {        
        public ViewPcpDiscountTemplateCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_discount_template view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpDiscountTemplate : ReadOnlyRecord<ViewPcpDiscountTemplate>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_discount_template", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarMinimumAmount = new TableSchema.TableColumn(schema);
                colvarMinimumAmount.ColumnName = "minimum_amount";
                colvarMinimumAmount.DataType = DbType.Currency;
                colvarMinimumAmount.MaxLength = 0;
                colvarMinimumAmount.AutoIncrement = false;
                colvarMinimumAmount.IsNullable = true;
                colvarMinimumAmount.IsPrimaryKey = false;
                colvarMinimumAmount.IsForeignKey = false;
                colvarMinimumAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarMinimumAmount);
                
                TableSchema.TableColumn colvarAvailableDateType = new TableSchema.TableColumn(schema);
                colvarAvailableDateType.ColumnName = "available_date_type";
                colvarAvailableDateType.DataType = DbType.Int32;
                colvarAvailableDateType.MaxLength = 0;
                colvarAvailableDateType.AutoIncrement = false;
                colvarAvailableDateType.IsNullable = false;
                colvarAvailableDateType.IsPrimaryKey = false;
                colvarAvailableDateType.IsForeignKey = false;
                colvarAvailableDateType.IsReadOnly = false;
                
                schema.Columns.Add(colvarAvailableDateType);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = false;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = false;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                TableSchema.TableColumn colvarCardCombineUse = new TableSchema.TableColumn(schema);
                colvarCardCombineUse.ColumnName = "card_combine_use";
                colvarCardCombineUse.DataType = DbType.Boolean;
                colvarCardCombineUse.MaxLength = 0;
                colvarCardCombineUse.AutoIncrement = false;
                colvarCardCombineUse.IsNullable = false;
                colvarCardCombineUse.IsPrimaryKey = false;
                colvarCardCombineUse.IsForeignKey = false;
                colvarCardCombineUse.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardCombineUse);
                
                TableSchema.TableColumn colvarIsDraft = new TableSchema.TableColumn(schema);
                colvarIsDraft.ColumnName = "is_draft";
                colvarIsDraft.DataType = DbType.Boolean;
                colvarIsDraft.MaxLength = 0;
                colvarIsDraft.AutoIncrement = false;
                colvarIsDraft.IsNullable = false;
                colvarIsDraft.IsPrimaryKey = false;
                colvarIsDraft.IsForeignKey = false;
                colvarIsDraft.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsDraft);
                
                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = false;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerUserId);
                
                TableSchema.TableColumn colvarSituationType = new TableSchema.TableColumn(schema);
                colvarSituationType.ColumnName = "situation_type";
                colvarSituationType.DataType = DbType.Int32;
                colvarSituationType.MaxLength = 0;
                colvarSituationType.AutoIncrement = false;
                colvarSituationType.IsNullable = false;
                colvarSituationType.IsPrimaryKey = false;
                colvarSituationType.IsForeignKey = false;
                colvarSituationType.IsReadOnly = false;
                
                schema.Columns.Add(colvarSituationType);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_discount_template",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpDiscountTemplate()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpDiscountTemplate(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpDiscountTemplate(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpDiscountTemplate(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal Amount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("amount");
		    }
            set 
		    {
			    SetColumnValue("amount", value);
            }
        }
	      
        [XmlAttribute("MinimumAmount")]
        [Bindable(true)]
        public decimal? MinimumAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("minimum_amount");
		    }
            set 
		    {
			    SetColumnValue("minimum_amount", value);
            }
        }
	      
        [XmlAttribute("AvailableDateType")]
        [Bindable(true)]
        public int AvailableDateType 
	    {
		    get
		    {
			    return GetColumnValue<int>("available_date_type");
		    }
            set 
		    {
			    SetColumnValue("available_date_type", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	      
        [XmlAttribute("CardCombineUse")]
        [Bindable(true)]
        public bool CardCombineUse 
	    {
		    get
		    {
			    return GetColumnValue<bool>("card_combine_use");
		    }
            set 
		    {
			    SetColumnValue("card_combine_use", value);
            }
        }
	      
        [XmlAttribute("IsDraft")]
        [Bindable(true)]
        public bool IsDraft 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_draft");
		    }
            set 
		    {
			    SetColumnValue("is_draft", value);
            }
        }
	      
        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int SellerUserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_user_id");
		    }
            set 
		    {
			    SetColumnValue("seller_user_id", value);
            }
        }
	      
        [XmlAttribute("SituationType")]
        [Bindable(true)]
        public int SituationType 
	    {
		    get
		    {
			    return GetColumnValue<int>("situation_type");
		    }
            set 
		    {
			    SetColumnValue("situation_type", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string Amount = @"amount";
            
            public static string MinimumAmount = @"minimum_amount";
            
            public static string AvailableDateType = @"available_date_type";
            
            public static string StartTime = @"start_time";
            
            public static string EndTime = @"end_time";
            
            public static string CardCombineUse = @"card_combine_use";
            
            public static string IsDraft = @"is_draft";
            
            public static string SellerUserId = @"seller_user_id";
            
            public static string SituationType = @"situation_type";
            
            public static string SellerName = @"seller_name";
            
            public static string StoreGuid = @"store_guid";
            
            public static string StoreName = @"store_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the EventPremiumPromoItem class.
	/// </summary>
    [Serializable]
	public partial class EventPremiumPromoItemCollection : RepositoryList<EventPremiumPromoItem, EventPremiumPromoItemCollection>
	{	   
		public EventPremiumPromoItemCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EventPremiumPromoItemCollection</returns>
		public EventPremiumPromoItemCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EventPremiumPromoItem o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the event_premium_promo_item table.
	/// </summary>
	[Serializable]
	public partial class EventPremiumPromoItem : RepositoryRecord<EventPremiumPromoItem>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public EventPremiumPromoItem()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public EventPremiumPromoItem(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("event_premium_promo_item", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarEventPremiumPromoId = new TableSchema.TableColumn(schema);
				colvarEventPremiumPromoId.ColumnName = "event_premium_promo_id";
				colvarEventPremiumPromoId.DataType = DbType.Int32;
				colvarEventPremiumPromoId.MaxLength = 0;
				colvarEventPremiumPromoId.AutoIncrement = false;
				colvarEventPremiumPromoId.IsNullable = false;
				colvarEventPremiumPromoId.IsPrimaryKey = false;
				colvarEventPremiumPromoId.IsForeignKey = false;
				colvarEventPremiumPromoId.IsReadOnly = false;
				colvarEventPremiumPromoId.DefaultSetting = @"";
				colvarEventPremiumPromoId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventPremiumPromoId);
				
				TableSchema.TableColumn colvarAwardName = new TableSchema.TableColumn(schema);
				colvarAwardName.ColumnName = "award_name";
				colvarAwardName.DataType = DbType.String;
				colvarAwardName.MaxLength = 100;
				colvarAwardName.AutoIncrement = false;
				colvarAwardName.IsNullable = true;
				colvarAwardName.IsPrimaryKey = false;
				colvarAwardName.IsForeignKey = false;
				colvarAwardName.IsReadOnly = false;
				colvarAwardName.DefaultSetting = @"";
				colvarAwardName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAwardName);
				
				TableSchema.TableColumn colvarAwardEmail = new TableSchema.TableColumn(schema);
				colvarAwardEmail.ColumnName = "award_email";
				colvarAwardEmail.DataType = DbType.String;
				colvarAwardEmail.MaxLength = 256;
				colvarAwardEmail.AutoIncrement = false;
				colvarAwardEmail.IsNullable = true;
				colvarAwardEmail.IsPrimaryKey = false;
				colvarAwardEmail.IsForeignKey = false;
				colvarAwardEmail.IsReadOnly = false;
				colvarAwardEmail.DefaultSetting = @"";
				colvarAwardEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAwardEmail);
				
				TableSchema.TableColumn colvarAwardAddress = new TableSchema.TableColumn(schema);
				colvarAwardAddress.ColumnName = "award_address";
				colvarAwardAddress.DataType = DbType.String;
				colvarAwardAddress.MaxLength = 100;
				colvarAwardAddress.AutoIncrement = false;
				colvarAwardAddress.IsNullable = true;
				colvarAwardAddress.IsPrimaryKey = false;
				colvarAwardAddress.IsForeignKey = false;
				colvarAwardAddress.IsReadOnly = false;
				colvarAwardAddress.DefaultSetting = @"";
				colvarAwardAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAwardAddress);
				
				TableSchema.TableColumn colvarAwardMobile = new TableSchema.TableColumn(schema);
				colvarAwardMobile.ColumnName = "award_mobile";
				colvarAwardMobile.DataType = DbType.AnsiString;
				colvarAwardMobile.MaxLength = 50;
				colvarAwardMobile.AutoIncrement = false;
				colvarAwardMobile.IsNullable = true;
				colvarAwardMobile.IsPrimaryKey = false;
				colvarAwardMobile.IsForeignKey = false;
				colvarAwardMobile.IsReadOnly = false;
				colvarAwardMobile.DefaultSetting = @"";
				colvarAwardMobile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAwardMobile);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("event_premium_promo_item",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("EventPremiumPromoId")]
		[Bindable(true)]
		public int EventPremiumPromoId 
		{
			get { return GetColumnValue<int>(Columns.EventPremiumPromoId); }
			set { SetColumnValue(Columns.EventPremiumPromoId, value); }
		}
		  
		[XmlAttribute("AwardName")]
		[Bindable(true)]
		public string AwardName 
		{
			get { return GetColumnValue<string>(Columns.AwardName); }
			set { SetColumnValue(Columns.AwardName, value); }
		}
		  
		[XmlAttribute("AwardEmail")]
		[Bindable(true)]
		public string AwardEmail 
		{
			get { return GetColumnValue<string>(Columns.AwardEmail); }
			set { SetColumnValue(Columns.AwardEmail, value); }
		}
		  
		[XmlAttribute("AwardAddress")]
		[Bindable(true)]
		public string AwardAddress 
		{
			get { return GetColumnValue<string>(Columns.AwardAddress); }
			set { SetColumnValue(Columns.AwardAddress, value); }
		}
		  
		[XmlAttribute("AwardMobile")]
		[Bindable(true)]
		public string AwardMobile 
		{
			get { return GetColumnValue<string>(Columns.AwardMobile); }
			set { SetColumnValue(Columns.AwardMobile, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EventPremiumPromoIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AwardNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AwardEmailColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AwardAddressColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AwardMobileColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string EventPremiumPromoId = @"event_premium_promo_id";
			 public static string AwardName = @"award_name";
			 public static string AwardEmail = @"award_email";
			 public static string AwardAddress = @"award_address";
			 public static string AwardMobile = @"award_mobile";
			 public static string CreateTime = @"create_time";
			 public static string UserId = @"user_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

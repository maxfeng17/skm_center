using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    [Serializable]
    public partial class PushAppCollection : RepositoryList<PushApp, PushAppCollection>
    {
        public PushAppCollection() { }

        public PushAppCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PushApp o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }

    [Serializable]
    public partial class PushApp : RepositoryRecord<PushApp>, IRecordBase
    {
        #region .ctors and Default Settings
        public PushApp()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public PushApp(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("push_app", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarPushArea = new TableSchema.TableColumn(schema);
                colvarPushArea.ColumnName = "push_area";
                colvarPushArea.DataType = DbType.AnsiString;
                colvarPushArea.MaxLength = 256;
                colvarPushArea.AutoIncrement = false;
                colvarPushArea.IsNullable = false;
                colvarPushArea.IsPrimaryKey = false;
                colvarPushArea.IsForeignKey = false;
                colvarPushArea.IsReadOnly = false;
                colvarPushArea.DefaultSetting = @"";
                colvarPushArea.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPushArea);

                TableSchema.TableColumn colvarPushType = new TableSchema.TableColumn(schema);
                colvarPushType.ColumnName = "push_type";
                colvarPushType.DataType = DbType.Int32;
                colvarPushType.MaxLength = 0;
                colvarPushType.AutoIncrement = false;
                colvarPushType.IsNullable = false;
                colvarPushType.IsPrimaryKey = false;
                colvarPushType.IsForeignKey = false;
                colvarPushType.IsReadOnly = false;
                colvarPushType.DefaultSetting = @"";
                colvarPushType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPushType);

                TableSchema.TableColumn colvarPushDate = new TableSchema.TableColumn(schema);
                colvarPushDate.ColumnName = "push_date";
                colvarPushDate.DataType = DbType.DateTime;
                colvarPushDate.MaxLength = 0;
                colvarPushDate.AutoIncrement = false;
                colvarPushDate.IsNullable = false;
                colvarPushDate.IsPrimaryKey = false;
                colvarPushDate.IsForeignKey = false;
                colvarPushDate.IsReadOnly = false;
                colvarPushDate.DefaultSetting = @"";
                colvarPushDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPushDate);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = true;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                colvarCityId.DefaultSetting = @"";
                colvarCityId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCityId);

                TableSchema.TableColumn colvarVourcherId = new TableSchema.TableColumn(schema);
                colvarVourcherId.ColumnName = "vourcher_id";
                colvarVourcherId.DataType = DbType.Int32;
                colvarVourcherId.MaxLength = 0;
                colvarVourcherId.AutoIncrement = false;
                colvarVourcherId.IsNullable = true;
                colvarVourcherId.IsPrimaryKey = false;
                colvarVourcherId.IsForeignKey = false;
                colvarVourcherId.IsReadOnly = false;
                colvarVourcherId.DefaultSetting = @"";
                colvarVourcherId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVourcherId);

                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                colvarSellerId.DefaultSetting = @"";
                colvarSellerId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerId);

                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = 120;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = true;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                colvarDescription.DefaultSetting = @"";
                colvarDescription.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDescription);

                TableSchema.TableColumn colvarIosPushCount = new TableSchema.TableColumn(schema);
                colvarIosPushCount.ColumnName = "ios_push_count";
                colvarIosPushCount.DataType = DbType.Int32;
                colvarIosPushCount.MaxLength = 0;
                colvarIosPushCount.AutoIncrement = false;
                colvarIosPushCount.IsNullable = false;
                colvarIosPushCount.IsPrimaryKey = false;
                colvarIosPushCount.IsForeignKey = false;
                colvarIosPushCount.IsReadOnly = false;
                colvarIosPushCount.DefaultSetting = @"";
                colvarIosPushCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIosPushCount);

                TableSchema.TableColumn colvarAndriodPushCount = new TableSchema.TableColumn(schema);
                colvarAndriodPushCount.ColumnName = "andriod_push_count";
                colvarAndriodPushCount.DataType = DbType.Int32;
                colvarAndriodPushCount.MaxLength = 0;
                colvarAndriodPushCount.AutoIncrement = false;
                colvarAndriodPushCount.IsNullable = false;
                colvarAndriodPushCount.IsPrimaryKey = false;
                colvarAndriodPushCount.IsForeignKey = false;
                colvarAndriodPushCount.IsReadOnly = false;
                colvarAndriodPushCount.DefaultSetting = @"";
                colvarAndriodPushCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAndriodPushCount);

                TableSchema.TableColumn colvarIosViewCount = new TableSchema.TableColumn(schema);
                colvarIosViewCount.ColumnName = "ios_view_count";
                colvarIosViewCount.DataType = DbType.Int32;
                colvarIosViewCount.MaxLength = 0;
                colvarIosViewCount.AutoIncrement = false;
                colvarIosViewCount.IsNullable = false;
                colvarIosViewCount.IsPrimaryKey = false;
                colvarIosViewCount.IsForeignKey = false;
                colvarIosViewCount.IsReadOnly = false;
                colvarIosViewCount.DefaultSetting = @"";
                colvarIosViewCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIosViewCount);

                TableSchema.TableColumn colvarAndriodViewCount = new TableSchema.TableColumn(schema);
                colvarAndriodViewCount.ColumnName = "andriod_view_count";
                colvarAndriodViewCount.DataType = DbType.Int32;
                colvarAndriodViewCount.MaxLength = 0;
                colvarAndriodViewCount.AutoIncrement = false;
                colvarAndriodViewCount.IsNullable = false;
                colvarAndriodViewCount.IsPrimaryKey = false;
                colvarAndriodViewCount.IsForeignKey = false;
                colvarAndriodViewCount.IsReadOnly = false;
                colvarAndriodViewCount.DefaultSetting = @"";
                colvarAndriodViewCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAndriodViewCount);

                TableSchema.TableColumn colvarSubscribeCount = new TableSchema.TableColumn(schema);
                colvarSubscribeCount.ColumnName = "subscribe_count";
                colvarSubscribeCount.DataType = DbType.Int32;
                colvarSubscribeCount.MaxLength = 0;
                colvarSubscribeCount.AutoIncrement = false;
                colvarSubscribeCount.IsNullable = false;
                colvarSubscribeCount.IsPrimaryKey = false;
                colvarSubscribeCount.IsForeignKey = false;
                colvarSubscribeCount.IsReadOnly = false;
                colvarSubscribeCount.DefaultSetting = @"((0))";
                colvarSubscribeCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSubscribeCount);

                TableSchema.TableColumn colvarRealPushTime = new TableSchema.TableColumn(schema);
                colvarRealPushTime.ColumnName = "real_push_time";
                colvarRealPushTime.DataType = DbType.DateTime;
                colvarRealPushTime.MaxLength = 0;
                colvarRealPushTime.AutoIncrement = false;
                colvarRealPushTime.IsNullable = true;
                colvarRealPushTime.IsPrimaryKey = false;
                colvarRealPushTime.IsForeignKey = false;
                colvarRealPushTime.IsReadOnly = false;
                colvarRealPushTime.DefaultSetting = @"";
                colvarRealPushTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRealPushTime);

                TableSchema.TableColumn colvarCompleteTime = new TableSchema.TableColumn(schema);
                colvarCompleteTime.ColumnName = "complete_time";
                colvarCompleteTime.DataType = DbType.DateTime;
                colvarCompleteTime.MaxLength = 0;
                colvarCompleteTime.AutoIncrement = false;
                colvarCompleteTime.IsNullable = true;
                colvarCompleteTime.IsPrimaryKey = false;
                colvarCompleteTime.IsForeignKey = false;
                colvarCompleteTime.IsReadOnly = false;
                colvarCompleteTime.DefaultSetting = @"";
                colvarCompleteTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCompleteTime);

                TableSchema.TableColumn colvarEventPromoId = new TableSchema.TableColumn(schema);
                colvarEventPromoId.ColumnName = "event_promo_id";
                colvarEventPromoId.DataType = DbType.Int32;
                colvarEventPromoId.MaxLength = 0;
                colvarEventPromoId.AutoIncrement = false;
                colvarEventPromoId.IsNullable = true;
                colvarEventPromoId.IsPrimaryKey = false;
                colvarEventPromoId.IsForeignKey = false;
                colvarEventPromoId.IsReadOnly = false;
                colvarEventPromoId.DefaultSetting = @"";
                colvarEventPromoId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventPromoId);

                TableSchema.TableColumn colvarCustomUrl = new TableSchema.TableColumn(schema);
                colvarCustomUrl.ColumnName = "custom_url";
                colvarCustomUrl.DataType = DbType.AnsiString;
                colvarCustomUrl.MaxLength = 200;
                colvarCustomUrl.AutoIncrement = false;
                colvarCustomUrl.IsNullable = true;
                colvarCustomUrl.IsPrimaryKey = false;
                colvarCustomUrl.IsForeignKey = false;
                colvarCustomUrl.IsReadOnly = false;
                colvarCustomUrl.DefaultSetting = @"";
                colvarCustomUrl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCustomUrl);

                TableSchema.TableColumn colvarActionEventPushMessageId = new TableSchema.TableColumn(schema);
                colvarActionEventPushMessageId.ColumnName = "action_event_push_message_id";
                colvarActionEventPushMessageId.DataType = DbType.Int32;
                colvarActionEventPushMessageId.MaxLength = 0;
                colvarActionEventPushMessageId.AutoIncrement = false;
                colvarActionEventPushMessageId.IsNullable = true;
                colvarActionEventPushMessageId.IsPrimaryKey = false;
                colvarActionEventPushMessageId.IsForeignKey = false;
                colvarActionEventPushMessageId.IsReadOnly = false;
                colvarActionEventPushMessageId.DefaultSetting = @"";
                colvarActionEventPushMessageId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarActionEventPushMessageId);

                TableSchema.TableColumn colvarBrandPromoId = new TableSchema.TableColumn(schema);
                colvarBrandPromoId.ColumnName = "brand_promo_id";
                colvarBrandPromoId.DataType = DbType.Int32;
                colvarBrandPromoId.MaxLength = 0;
                colvarBrandPromoId.AutoIncrement = false;
                colvarBrandPromoId.IsNullable = true;
                colvarBrandPromoId.IsPrimaryKey = false;
                colvarBrandPromoId.IsForeignKey = false;
                colvarBrandPromoId.IsReadOnly = false;
                colvarBrandPromoId.DefaultSetting = @"";
                colvarBrandPromoId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBrandPromoId);

                TableSchema.TableColumn colvarTurnover = new TableSchema.TableColumn(schema);
                colvarTurnover.ColumnName = "turnover";
                colvarTurnover.DataType = DbType.Decimal;
                colvarTurnover.MaxLength = 0;
                colvarTurnover.AutoIncrement = false;
                colvarTurnover.IsNullable = false;
                colvarTurnover.IsPrimaryKey = false;
                colvarTurnover.IsForeignKey = false;
                colvarTurnover.IsReadOnly = false;
                colvarTurnover.DefaultSetting = @"((0))";
                colvarTurnover.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTurnover);

                TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
                colvarOrderCount.ColumnName = "order_count";
                colvarOrderCount.DataType = DbType.Int32;
                colvarOrderCount.MaxLength = 0;
                colvarOrderCount.AutoIncrement = false;
                colvarOrderCount.IsNullable = false;
                colvarOrderCount.IsPrimaryKey = false;
                colvarOrderCount.IsForeignKey = false;
                colvarOrderCount.IsReadOnly = false;
                colvarOrderCount.DefaultSetting = @"((0))";
                colvarOrderCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderCount);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("push_app", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("PushArea")]
        [Bindable(true)]
        public string PushArea
        {
            get { return GetColumnValue<string>(Columns.PushArea); }
            set { SetColumnValue(Columns.PushArea, value); }
        }

        [XmlAttribute("PushType")]
        [Bindable(true)]
        public int PushType
        {
            get { return GetColumnValue<int>(Columns.PushType); }
            set { SetColumnValue(Columns.PushType, value); }
        }

        [XmlAttribute("PushDate")]
        [Bindable(true)]
        public DateTime PushDate
        {
            get { return GetColumnValue<DateTime>(Columns.PushDate); }
            set { SetColumnValue(Columns.PushDate, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid
        {
            get { return GetColumnValue<Guid?>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int? CityId
        {
            get { return GetColumnValue<int?>(Columns.CityId); }
            set { SetColumnValue(Columns.CityId, value); }
        }

        [XmlAttribute("VourcherId")]
        [Bindable(true)]
        public int? VourcherId
        {
            get { return GetColumnValue<int?>(Columns.VourcherId); }
            set { SetColumnValue(Columns.VourcherId, value); }
        }

        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId
        {
            get { return GetColumnValue<string>(Columns.SellerId); }
            set { SetColumnValue(Columns.SellerId, value); }
        }

        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description
        {
            get { return GetColumnValue<string>(Columns.Description); }
            set { SetColumnValue(Columns.Description, value); }
        }

        [XmlAttribute("IosPushCount")]
        [Bindable(true)]
        public int IosPushCount
        {
            get { return GetColumnValue<int>(Columns.IosPushCount); }
            set { SetColumnValue(Columns.IosPushCount, value); }
        }

        [XmlAttribute("AndriodPushCount")]
        [Bindable(true)]
        public int AndriodPushCount
        {
            get { return GetColumnValue<int>(Columns.AndriodPushCount); }
            set { SetColumnValue(Columns.AndriodPushCount, value); }
        }

        [XmlAttribute("IosViewCount")]
        [Bindable(true)]
        public int IosViewCount
        {
            get { return GetColumnValue<int>(Columns.IosViewCount); }
            set { SetColumnValue(Columns.IosViewCount, value); }
        }

        [XmlAttribute("AndriodViewCount")]
        [Bindable(true)]
        public int AndriodViewCount
        {
            get { return GetColumnValue<int>(Columns.AndriodViewCount); }
            set { SetColumnValue(Columns.AndriodViewCount, value); }
        }

        [XmlAttribute("SubscribeCount")]
        [Bindable(true)]
        public int SubscribeCount
        {
            get { return GetColumnValue<int>(Columns.SubscribeCount); }
            set { SetColumnValue(Columns.SubscribeCount, value); }
        }

        [XmlAttribute("RealPushTime")]
        [Bindable(true)]
        public DateTime? RealPushTime
        {
            get { return GetColumnValue<DateTime?>(Columns.RealPushTime); }
            set { SetColumnValue(Columns.RealPushTime, value); }
        }

        [XmlAttribute("CompleteTime")]
        [Bindable(true)]
        public DateTime? CompleteTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CompleteTime); }
            set { SetColumnValue(Columns.CompleteTime, value); }
        }

        [XmlAttribute("EventPromoId")]
        [Bindable(true)]
        public int? EventPromoId
        {
            get { return GetColumnValue<int?>(Columns.EventPromoId); }
            set { SetColumnValue(Columns.EventPromoId, value); }
        }

        [XmlAttribute("CustomUrl")]
        [Bindable(true)]
        public string CustomUrl
        {
            get { return GetColumnValue<string>(Columns.CustomUrl); }
            set { SetColumnValue(Columns.CustomUrl, value); }
        }

        [XmlAttribute("ActionEventPushMessageId")]
        [Bindable(true)]
        public int? ActionEventPushMessageId
        {
            get { return GetColumnValue<int?>(Columns.ActionEventPushMessageId); }
            set { SetColumnValue(Columns.ActionEventPushMessageId, value); }
        }

        [XmlAttribute("BrandPromoId")]
        [Bindable(true)]
        public int? BrandPromoId
        {
            get { return GetColumnValue<int?>(Columns.BrandPromoId); }
            set { SetColumnValue(Columns.BrandPromoId, value); }
        }

        [XmlAttribute("Turnover")]
        [Bindable(true)]
        public decimal Turnover
        {
            get { return GetColumnValue<decimal>(Columns.Turnover); }
            set { SetColumnValue(Columns.Turnover, value); }
        }

        [XmlAttribute("OrderCount")]
        [Bindable(true)]
        public int OrderCount
        {
            get { return GetColumnValue<int>(Columns.OrderCount); }
            set { SetColumnValue(Columns.OrderCount, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        #endregion

        #region Typed Columns

        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }

        public static TableSchema.TableColumn PushAreaColumn
        {
            get { return Schema.Columns[1]; }
        }

        public static TableSchema.TableColumn PushTypeColumn
        {
            get { return Schema.Columns[2]; }
        }

        public static TableSchema.TableColumn PushDateColumn
        {
            get { return Schema.Columns[3]; }
        }

        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[4]; }
        }

        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[5]; }
        }

        public static TableSchema.TableColumn VourcherIdColumn
        {
            get { return Schema.Columns[6]; }
        }

        public static TableSchema.TableColumn SellerIdColumn
        {
            get { return Schema.Columns[7]; }
        }

        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[8]; }
        }

        public static TableSchema.TableColumn IosPushCountColumn
        {
            get { return Schema.Columns[9]; }
        }

        public static TableSchema.TableColumn AndriodPushCountColumn
        {
            get { return Schema.Columns[10]; }
        }

        public static TableSchema.TableColumn IosViewCountColumn
        {
            get { return Schema.Columns[11]; }
        }

        public static TableSchema.TableColumn AndriodViewCountColumn
        {
            get { return Schema.Columns[12]; }
        }

        public static TableSchema.TableColumn SubscribeCountColumn
        {
            get { return Schema.Columns[13]; }
        }

        public static TableSchema.TableColumn RealPushTimeColumn
        {
            get { return Schema.Columns[14]; }
        }

        public static TableSchema.TableColumn CompleteTimeColumn
        {
            get { return Schema.Columns[15]; }
        }

        public static TableSchema.TableColumn EventPromoIdColumn
        {
            get { return Schema.Columns[16]; }
        }

        public static TableSchema.TableColumn CustomUrlColumn
        {
            get { return Schema.Columns[17]; }
        }

        public static TableSchema.TableColumn ActionEventPushMessageIdColumn
        {
            get { return Schema.Columns[18]; }
        }

        public static TableSchema.TableColumn BrandPromoIdColumn
        {
            get { return Schema.Columns[19]; }
        }

        public static TableSchema.TableColumn TurnoverColumn
        {
            get { return Schema.Columns[20]; }
        }

        public static TableSchema.TableColumn OrderCountColumn
        {
            get { return Schema.Columns[21]; }
        }

        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[22]; }
        }

        #endregion

        #region Columns Struct

        public struct Columns
        {
            public static string Id = @"id";
            public static string PushArea = @"push_area";
            public static string PushType = @"push_type";
            public static string PushDate = @"push_date";
            public static string BusinessHourGuid = @"business_hour_guid";
            public static string CityId = @"city_id";
            public static string VourcherId = @"vourcher_id";
            public static string SellerId = @"seller_id";
            public static string Description = @"description";
            public static string IosPushCount = @"ios_push_count";
            public static string AndriodPushCount = @"andriod_push_count";
            public static string IosViewCount = @"ios_view_count";
            public static string AndriodViewCount = @"andriod_view_count";
            public static string SubscribeCount = @"subscribe_count";
            public static string RealPushTime = @"real_push_time";
            public static string CompleteTime = @"complete_time";
            public static string EventPromoId = @"event_promo_id";
            public static string CustomUrl = @"custom_url";
            public static string ActionEventPushMessageId = @"action_event_push_message_id";
            public static string BrandPromoId = @"brand_promo_id";
            public static string Turnover = @"turnover";
            public static string OrderCount = @"order_count";
            public static string ModifyTime = @"modify_time";
        }

        #endregion

    }
}

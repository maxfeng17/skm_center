using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealProductInformEmail class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealProductInformEmailCollection : ReadOnlyList<ViewHiDealProductInformEmail, ViewHiDealProductInformEmailCollection>
    {        
        public ViewHiDealProductInformEmailCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_product_inform_email view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealProductInformEmail : ReadOnlyRecord<ViewHiDealProductInformEmail>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_product_inform_email", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = true;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarSeq);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 250;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = -1;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = true;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescription);
                
                TableSchema.TableColumn colvarIsOnline = new TableSchema.TableColumn(schema);
                colvarIsOnline.ColumnName = "is_online";
                colvarIsOnline.DataType = DbType.Boolean;
                colvarIsOnline.MaxLength = 0;
                colvarIsOnline.AutoIncrement = false;
                colvarIsOnline.IsNullable = true;
                colvarIsOnline.IsPrimaryKey = false;
                colvarIsOnline.IsForeignKey = false;
                colvarIsOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsOnline);
                
                TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
                colvarUseStartTime.ColumnName = "use_start_time";
                colvarUseStartTime.DataType = DbType.DateTime;
                colvarUseStartTime.MaxLength = 0;
                colvarUseStartTime.AutoIncrement = false;
                colvarUseStartTime.IsNullable = true;
                colvarUseStartTime.IsPrimaryKey = false;
                colvarUseStartTime.IsForeignKey = false;
                colvarUseStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseStartTime);
                
                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.DateTime;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseEndTime);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = true;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarOriginalPrice = new TableSchema.TableColumn(schema);
                colvarOriginalPrice.ColumnName = "original_price";
                colvarOriginalPrice.DataType = DbType.Currency;
                colvarOriginalPrice.MaxLength = 0;
                colvarOriginalPrice.AutoIncrement = false;
                colvarOriginalPrice.IsNullable = true;
                colvarOriginalPrice.IsPrimaryKey = false;
                colvarOriginalPrice.IsForeignKey = false;
                colvarOriginalPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarOriginalPrice);
                
                TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
                colvarPrice.ColumnName = "price";
                colvarPrice.DataType = DbType.Currency;
                colvarPrice.MaxLength = 0;
                colvarPrice.AutoIncrement = false;
                colvarPrice.IsNullable = true;
                colvarPrice.IsPrimaryKey = false;
                colvarPrice.IsForeignKey = false;
                colvarPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrice);
                
                TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
                colvarQuantity.ColumnName = "quantity";
                colvarQuantity.DataType = DbType.Int32;
                colvarQuantity.MaxLength = 0;
                colvarQuantity.AutoIncrement = false;
                colvarQuantity.IsNullable = true;
                colvarQuantity.IsPrimaryKey = false;
                colvarQuantity.IsForeignKey = false;
                colvarQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarQuantity);
                
                TableSchema.TableColumn colvarOrderLimit = new TableSchema.TableColumn(schema);
                colvarOrderLimit.ColumnName = "order_limit";
                colvarOrderLimit.DataType = DbType.Int32;
                colvarOrderLimit.MaxLength = 0;
                colvarOrderLimit.AutoIncrement = false;
                colvarOrderLimit.IsNullable = true;
                colvarOrderLimit.IsPrimaryKey = false;
                colvarOrderLimit.IsForeignKey = false;
                colvarOrderLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderLimit);
                
                TableSchema.TableColumn colvarOrderLimitUser = new TableSchema.TableColumn(schema);
                colvarOrderLimitUser.ColumnName = "order_limit_user";
                colvarOrderLimitUser.DataType = DbType.Int32;
                colvarOrderLimitUser.MaxLength = 0;
                colvarOrderLimitUser.AutoIncrement = false;
                colvarOrderLimitUser.IsNullable = true;
                colvarOrderLimitUser.IsPrimaryKey = false;
                colvarOrderLimitUser.IsForeignKey = false;
                colvarOrderLimitUser.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderLimitUser);
                
                TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
                colvarOrderCount.ColumnName = "order_count";
                colvarOrderCount.DataType = DbType.Int32;
                colvarOrderCount.MaxLength = 0;
                colvarOrderCount.AutoIncrement = false;
                colvarOrderCount.IsNullable = true;
                colvarOrderCount.IsPrimaryKey = false;
                colvarOrderCount.IsForeignKey = false;
                colvarOrderCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCount);
                
                TableSchema.TableColumn colvarIsNoRefund = new TableSchema.TableColumn(schema);
                colvarIsNoRefund.ColumnName = "is_no_refund";
                colvarIsNoRefund.DataType = DbType.Boolean;
                colvarIsNoRefund.MaxLength = 0;
                colvarIsNoRefund.AutoIncrement = false;
                colvarIsNoRefund.IsNullable = false;
                colvarIsNoRefund.IsPrimaryKey = false;
                colvarIsNoRefund.IsForeignKey = false;
                colvarIsNoRefund.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsNoRefund);
                
                TableSchema.TableColumn colvarIsExpireNoRefund = new TableSchema.TableColumn(schema);
                colvarIsExpireNoRefund.ColumnName = "is_expire_no_refund";
                colvarIsExpireNoRefund.DataType = DbType.Boolean;
                colvarIsExpireNoRefund.MaxLength = 0;
                colvarIsExpireNoRefund.AutoIncrement = false;
                colvarIsExpireNoRefund.IsNullable = false;
                colvarIsExpireNoRefund.IsPrimaryKey = false;
                colvarIsExpireNoRefund.IsForeignKey = false;
                colvarIsExpireNoRefund.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsExpireNoRefund);
                
                TableSchema.TableColumn colvarIsTaxFree = new TableSchema.TableColumn(schema);
                colvarIsTaxFree.ColumnName = "is_tax_free";
                colvarIsTaxFree.DataType = DbType.Boolean;
                colvarIsTaxFree.MaxLength = 0;
                colvarIsTaxFree.AutoIncrement = false;
                colvarIsTaxFree.IsNullable = false;
                colvarIsTaxFree.IsPrimaryKey = false;
                colvarIsTaxFree.IsForeignKey = false;
                colvarIsTaxFree.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsTaxFree);
                
                TableSchema.TableColumn colvarIsSoldout = new TableSchema.TableColumn(schema);
                colvarIsSoldout.ColumnName = "is_soldout";
                colvarIsSoldout.DataType = DbType.Boolean;
                colvarIsSoldout.MaxLength = 0;
                colvarIsSoldout.AutoIncrement = false;
                colvarIsSoldout.IsNullable = true;
                colvarIsSoldout.IsPrimaryKey = false;
                colvarIsSoldout.IsForeignKey = false;
                colvarIsSoldout.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsSoldout);
                
                TableSchema.TableColumn colvarIsInStore = new TableSchema.TableColumn(schema);
                colvarIsInStore.ColumnName = "is_in_store";
                colvarIsInStore.DataType = DbType.Boolean;
                colvarIsInStore.MaxLength = 0;
                colvarIsInStore.AutoIncrement = false;
                colvarIsInStore.IsNullable = false;
                colvarIsInStore.IsPrimaryKey = false;
                colvarIsInStore.IsForeignKey = false;
                colvarIsInStore.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsInStore);
                
                TableSchema.TableColumn colvarInStoreDesc = new TableSchema.TableColumn(schema);
                colvarInStoreDesc.ColumnName = "in_store_desc";
                colvarInStoreDesc.DataType = DbType.String;
                colvarInStoreDesc.MaxLength = 100;
                colvarInStoreDesc.AutoIncrement = false;
                colvarInStoreDesc.IsNullable = true;
                colvarInStoreDesc.IsPrimaryKey = false;
                colvarInStoreDesc.IsForeignKey = false;
                colvarInStoreDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarInStoreDesc);
                
                TableSchema.TableColumn colvarIsHomeDelivery = new TableSchema.TableColumn(schema);
                colvarIsHomeDelivery.ColumnName = "is_home_delivery";
                colvarIsHomeDelivery.DataType = DbType.Boolean;
                colvarIsHomeDelivery.MaxLength = 0;
                colvarIsHomeDelivery.AutoIncrement = false;
                colvarIsHomeDelivery.IsNullable = false;
                colvarIsHomeDelivery.IsPrimaryKey = false;
                colvarIsHomeDelivery.IsForeignKey = false;
                colvarIsHomeDelivery.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsHomeDelivery);
                
                TableSchema.TableColumn colvarHomeDeliveryDesc = new TableSchema.TableColumn(schema);
                colvarHomeDeliveryDesc.ColumnName = "home_delivery_desc";
                colvarHomeDeliveryDesc.DataType = DbType.String;
                colvarHomeDeliveryDesc.MaxLength = 100;
                colvarHomeDeliveryDesc.AutoIncrement = false;
                colvarHomeDeliveryDesc.IsNullable = true;
                colvarHomeDeliveryDesc.IsPrimaryKey = false;
                colvarHomeDeliveryDesc.IsForeignKey = false;
                colvarHomeDeliveryDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarHomeDeliveryDesc);
                
                TableSchema.TableColumn colvarSms = new TableSchema.TableColumn(schema);
                colvarSms.ColumnName = "sms";
                colvarSms.DataType = DbType.String;
                colvarSms.MaxLength = 150;
                colvarSms.AutoIncrement = false;
                colvarSms.IsNullable = true;
                colvarSms.IsPrimaryKey = false;
                colvarSms.IsForeignKey = false;
                colvarSms.IsReadOnly = false;
                
                schema.Columns.Add(colvarSms);
                
                TableSchema.TableColumn colvarIsSmsClose = new TableSchema.TableColumn(schema);
                colvarIsSmsClose.ColumnName = "is_sms_close";
                colvarIsSmsClose.DataType = DbType.Boolean;
                colvarIsSmsClose.MaxLength = 0;
                colvarIsSmsClose.AutoIncrement = false;
                colvarIsSmsClose.IsNullable = false;
                colvarIsSmsClose.IsPrimaryKey = false;
                colvarIsSmsClose.IsForeignKey = false;
                colvarIsSmsClose.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsSmsClose);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 100;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 100;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = true;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                TableSchema.TableColumn colvarPic = new TableSchema.TableColumn(schema);
                colvarPic.ColumnName = "pic";
                colvarPic.DataType = DbType.String;
                colvarPic.MaxLength = -1;
                colvarPic.AutoIncrement = false;
                colvarPic.IsNullable = true;
                colvarPic.IsPrimaryKey = false;
                colvarPic.IsForeignKey = false;
                colvarPic.IsReadOnly = false;
                
                schema.Columns.Add(colvarPic);
                
                TableSchema.TableColumn colvarHasBranchStoreQuantityLimit = new TableSchema.TableColumn(schema);
                colvarHasBranchStoreQuantityLimit.ColumnName = "has_branch_store_quantity_limit";
                colvarHasBranchStoreQuantityLimit.DataType = DbType.Boolean;
                colvarHasBranchStoreQuantityLimit.MaxLength = 0;
                colvarHasBranchStoreQuantityLimit.AutoIncrement = false;
                colvarHasBranchStoreQuantityLimit.IsNullable = false;
                colvarHasBranchStoreQuantityLimit.IsPrimaryKey = false;
                colvarHasBranchStoreQuantityLimit.IsForeignKey = false;
                colvarHasBranchStoreQuantityLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarHasBranchStoreQuantityLimit);
                
                TableSchema.TableColumn colvarHomeDeliveryQuantity = new TableSchema.TableColumn(schema);
                colvarHomeDeliveryQuantity.ColumnName = "home_delivery_quantity";
                colvarHomeDeliveryQuantity.DataType = DbType.Int32;
                colvarHomeDeliveryQuantity.MaxLength = 0;
                colvarHomeDeliveryQuantity.AutoIncrement = false;
                colvarHomeDeliveryQuantity.IsNullable = true;
                colvarHomeDeliveryQuantity.IsPrimaryKey = false;
                colvarHomeDeliveryQuantity.IsForeignKey = false;
                colvarHomeDeliveryQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarHomeDeliveryQuantity);
                
                TableSchema.TableColumn colvarDeliveryIslands = new TableSchema.TableColumn(schema);
                colvarDeliveryIslands.ColumnName = "deliveryIslands";
                colvarDeliveryIslands.DataType = DbType.Boolean;
                colvarDeliveryIslands.MaxLength = 0;
                colvarDeliveryIslands.AutoIncrement = false;
                colvarDeliveryIslands.IsNullable = false;
                colvarDeliveryIslands.IsPrimaryKey = false;
                colvarDeliveryIslands.IsForeignKey = false;
                colvarDeliveryIslands.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryIslands);
                
                TableSchema.TableColumn colvarShippedDate = new TableSchema.TableColumn(schema);
                colvarShippedDate.ColumnName = "shipped_date";
                colvarShippedDate.DataType = DbType.DateTime;
                colvarShippedDate.MaxLength = 0;
                colvarShippedDate.AutoIncrement = false;
                colvarShippedDate.IsNullable = true;
                colvarShippedDate.IsPrimaryKey = false;
                colvarShippedDate.IsForeignKey = false;
                colvarShippedDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippedDate);
                
                TableSchema.TableColumn colvarTrustType = new TableSchema.TableColumn(schema);
                colvarTrustType.ColumnName = "trust_type";
                colvarTrustType.DataType = DbType.Int32;
                colvarTrustType.MaxLength = 0;
                colvarTrustType.AutoIncrement = false;
                colvarTrustType.IsNullable = false;
                colvarTrustType.IsPrimaryKey = false;
                colvarTrustType.IsForeignKey = false;
                colvarTrustType.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustType);
                
                TableSchema.TableColumn colvarIsShowPriceDiscount = new TableSchema.TableColumn(schema);
                colvarIsShowPriceDiscount.ColumnName = "is_show_Price_discount";
                colvarIsShowPriceDiscount.DataType = DbType.Boolean;
                colvarIsShowPriceDiscount.MaxLength = 0;
                colvarIsShowPriceDiscount.AutoIncrement = false;
                colvarIsShowPriceDiscount.IsNullable = false;
                colvarIsShowPriceDiscount.IsPrimaryKey = false;
                colvarIsShowPriceDiscount.IsForeignKey = false;
                colvarIsShowPriceDiscount.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsShowPriceDiscount);
                
                TableSchema.TableColumn colvarIsInvoiceCreate = new TableSchema.TableColumn(schema);
                colvarIsInvoiceCreate.ColumnName = "Is_Invoice_Create";
                colvarIsInvoiceCreate.DataType = DbType.Boolean;
                colvarIsInvoiceCreate.MaxLength = 0;
                colvarIsInvoiceCreate.AutoIncrement = false;
                colvarIsInvoiceCreate.IsNullable = false;
                colvarIsInvoiceCreate.IsPrimaryKey = false;
                colvarIsInvoiceCreate.IsForeignKey = false;
                colvarIsInvoiceCreate.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsInvoiceCreate);
                
                TableSchema.TableColumn colvarPayToCompany = new TableSchema.TableColumn(schema);
                colvarPayToCompany.ColumnName = "pay_to_company";
                colvarPayToCompany.DataType = DbType.Boolean;
                colvarPayToCompany.MaxLength = 0;
                colvarPayToCompany.AutoIncrement = false;
                colvarPayToCompany.IsNullable = false;
                colvarPayToCompany.IsPrimaryKey = false;
                colvarPayToCompany.IsForeignKey = false;
                colvarPayToCompany.IsReadOnly = false;
                
                schema.Columns.Add(colvarPayToCompany);
                
                TableSchema.TableColumn colvarIsCombo = new TableSchema.TableColumn(schema);
                colvarIsCombo.ColumnName = "is_combo";
                colvarIsCombo.DataType = DbType.Boolean;
                colvarIsCombo.MaxLength = 0;
                colvarIsCombo.AutoIncrement = false;
                colvarIsCombo.IsNullable = false;
                colvarIsCombo.IsPrimaryKey = false;
                colvarIsCombo.IsForeignKey = false;
                colvarIsCombo.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCombo);
                
                TableSchema.TableColumn colvarIsDaysNoRefund = new TableSchema.TableColumn(schema);
                colvarIsDaysNoRefund.ColumnName = "is_days_no_refund";
                colvarIsDaysNoRefund.DataType = DbType.Boolean;
                colvarIsDaysNoRefund.MaxLength = 0;
                colvarIsDaysNoRefund.AutoIncrement = false;
                colvarIsDaysNoRefund.IsNullable = false;
                colvarIsDaysNoRefund.IsPrimaryKey = false;
                colvarIsDaysNoRefund.IsForeignKey = false;
                colvarIsDaysNoRefund.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsDaysNoRefund);
                
                TableSchema.TableColumn colvarVendorBillingModel = new TableSchema.TableColumn(schema);
                colvarVendorBillingModel.ColumnName = "vendor_billing_model";
                colvarVendorBillingModel.DataType = DbType.Int32;
                colvarVendorBillingModel.MaxLength = 0;
                colvarVendorBillingModel.AutoIncrement = false;
                colvarVendorBillingModel.IsNullable = false;
                colvarVendorBillingModel.IsPrimaryKey = false;
                colvarVendorBillingModel.IsForeignKey = false;
                colvarVendorBillingModel.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorBillingModel);
                
                TableSchema.TableColumn colvarVendorReceiptType = new TableSchema.TableColumn(schema);
                colvarVendorReceiptType.ColumnName = "vendor_receipt_type";
                colvarVendorReceiptType.DataType = DbType.Int32;
                colvarVendorReceiptType.MaxLength = 0;
                colvarVendorReceiptType.AutoIncrement = false;
                colvarVendorReceiptType.IsNullable = false;
                colvarVendorReceiptType.IsPrimaryKey = false;
                colvarVendorReceiptType.IsForeignKey = false;
                colvarVendorReceiptType.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorReceiptType);
                
                TableSchema.TableColumn colvarChangedExpireTime = new TableSchema.TableColumn(schema);
                colvarChangedExpireTime.ColumnName = "changed_expire_time";
                colvarChangedExpireTime.DataType = DbType.DateTime;
                colvarChangedExpireTime.MaxLength = 0;
                colvarChangedExpireTime.AutoIncrement = false;
                colvarChangedExpireTime.IsNullable = true;
                colvarChangedExpireTime.IsPrimaryKey = false;
                colvarChangedExpireTime.IsForeignKey = false;
                colvarChangedExpireTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarChangedExpireTime);
                
                TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
                colvarRemittanceType.ColumnName = "remittance_type";
                colvarRemittanceType.DataType = DbType.Int32;
                colvarRemittanceType.MaxLength = 0;
                colvarRemittanceType.AutoIncrement = false;
                colvarRemittanceType.IsNullable = false;
                colvarRemittanceType.IsPrimaryKey = false;
                colvarRemittanceType.IsForeignKey = false;
                colvarRemittanceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemittanceType);
                
                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 50;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage);
                
                TableSchema.TableColumn colvarIsInputTaxFree = new TableSchema.TableColumn(schema);
                colvarIsInputTaxFree.ColumnName = "is_input_tax_free";
                colvarIsInputTaxFree.DataType = DbType.Boolean;
                colvarIsInputTaxFree.MaxLength = 0;
                colvarIsInputTaxFree.AutoIncrement = false;
                colvarIsInputTaxFree.IsNullable = false;
                colvarIsInputTaxFree.IsPrimaryKey = false;
                colvarIsInputTaxFree.IsForeignKey = false;
                colvarIsInputTaxFree.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsInputTaxFree);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "DealName";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 50;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarEmpEmail = new TableSchema.TableColumn(schema);
                colvarEmpEmail.ColumnName = "EmpEmail";
                colvarEmpEmail.DataType = DbType.String;
                colvarEmpEmail.MaxLength = 150;
                colvarEmpEmail.AutoIncrement = false;
                colvarEmpEmail.IsNullable = true;
                colvarEmpEmail.IsPrimaryKey = false;
                colvarEmpEmail.IsForeignKey = false;
                colvarEmpEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarEmpEmail);
                
                TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
                colvarSellerEmail.ColumnName = "SellerEmail";
                colvarSellerEmail.DataType = DbType.AnsiString;
                colvarSellerEmail.MaxLength = 200;
                colvarSellerEmail.AutoIncrement = false;
                colvarSellerEmail.IsNullable = true;
                colvarSellerEmail.IsPrimaryKey = false;
                colvarSellerEmail.IsForeignKey = false;
                colvarSellerEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerEmail);
                
                TableSchema.TableColumn colvarReturnedPersonEmail = new TableSchema.TableColumn(schema);
                colvarReturnedPersonEmail.ColumnName = "ReturnedPersonEmail";
                colvarReturnedPersonEmail.DataType = DbType.AnsiString;
                colvarReturnedPersonEmail.MaxLength = 200;
                colvarReturnedPersonEmail.AutoIncrement = false;
                colvarReturnedPersonEmail.IsNullable = true;
                colvarReturnedPersonEmail.IsPrimaryKey = false;
                colvarReturnedPersonEmail.IsForeignKey = false;
                colvarReturnedPersonEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonEmail);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_product_inform_email",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealProductInformEmail()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealProductInformEmail(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealProductInformEmail(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealProductInformEmail(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("guid");
		    }
            set 
		    {
			    SetColumnValue("guid", value);
            }
        }
	      
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int? Seq 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seq");
		    }
            set 
		    {
			    SetColumnValue("seq", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description 
	    {
		    get
		    {
			    return GetColumnValue<string>("description");
		    }
            set 
		    {
			    SetColumnValue("description", value);
            }
        }
	      
        [XmlAttribute("IsOnline")]
        [Bindable(true)]
        public bool? IsOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_online");
		    }
            set 
		    {
			    SetColumnValue("is_online", value);
            }
        }
	      
        [XmlAttribute("UseStartTime")]
        [Bindable(true)]
        public DateTime? UseStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_start_time");
		    }
            set 
		    {
			    SetColumnValue("use_start_time", value);
            }
        }
	      
        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public DateTime? UseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_end_time");
		    }
            set 
		    {
			    SetColumnValue("use_end_time", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int? Type 
	    {
		    get
		    {
			    return GetColumnValue<int?>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("OriginalPrice")]
        [Bindable(true)]
        public decimal? OriginalPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("original_price");
		    }
            set 
		    {
			    SetColumnValue("original_price", value);
            }
        }
	      
        [XmlAttribute("Price")]
        [Bindable(true)]
        public decimal? Price 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("price");
		    }
            set 
		    {
			    SetColumnValue("price", value);
            }
        }
	      
        [XmlAttribute("Quantity")]
        [Bindable(true)]
        public int? Quantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("quantity");
		    }
            set 
		    {
			    SetColumnValue("quantity", value);
            }
        }
	      
        [XmlAttribute("OrderLimit")]
        [Bindable(true)]
        public int? OrderLimit 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_limit");
		    }
            set 
		    {
			    SetColumnValue("order_limit", value);
            }
        }
	      
        [XmlAttribute("OrderLimitUser")]
        [Bindable(true)]
        public int? OrderLimitUser 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_limit_user");
		    }
            set 
		    {
			    SetColumnValue("order_limit_user", value);
            }
        }
	      
        [XmlAttribute("OrderCount")]
        [Bindable(true)]
        public int? OrderCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_count");
		    }
            set 
		    {
			    SetColumnValue("order_count", value);
            }
        }
	      
        [XmlAttribute("IsNoRefund")]
        [Bindable(true)]
        public bool IsNoRefund 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_no_refund");
		    }
            set 
		    {
			    SetColumnValue("is_no_refund", value);
            }
        }
	      
        [XmlAttribute("IsExpireNoRefund")]
        [Bindable(true)]
        public bool IsExpireNoRefund 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_expire_no_refund");
		    }
            set 
		    {
			    SetColumnValue("is_expire_no_refund", value);
            }
        }
	      
        [XmlAttribute("IsTaxFree")]
        [Bindable(true)]
        public bool IsTaxFree 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_tax_free");
		    }
            set 
		    {
			    SetColumnValue("is_tax_free", value);
            }
        }
	      
        [XmlAttribute("IsSoldout")]
        [Bindable(true)]
        public bool? IsSoldout 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_soldout");
		    }
            set 
		    {
			    SetColumnValue("is_soldout", value);
            }
        }
	      
        [XmlAttribute("IsInStore")]
        [Bindable(true)]
        public bool IsInStore 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_in_store");
		    }
            set 
		    {
			    SetColumnValue("is_in_store", value);
            }
        }
	      
        [XmlAttribute("InStoreDesc")]
        [Bindable(true)]
        public string InStoreDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("in_store_desc");
		    }
            set 
		    {
			    SetColumnValue("in_store_desc", value);
            }
        }
	      
        [XmlAttribute("IsHomeDelivery")]
        [Bindable(true)]
        public bool IsHomeDelivery 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_home_delivery");
		    }
            set 
		    {
			    SetColumnValue("is_home_delivery", value);
            }
        }
	      
        [XmlAttribute("HomeDeliveryDesc")]
        [Bindable(true)]
        public string HomeDeliveryDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("home_delivery_desc");
		    }
            set 
		    {
			    SetColumnValue("home_delivery_desc", value);
            }
        }
	      
        [XmlAttribute("Sms")]
        [Bindable(true)]
        public string Sms 
	    {
		    get
		    {
			    return GetColumnValue<string>("sms");
		    }
            set 
		    {
			    SetColumnValue("sms", value);
            }
        }
	      
        [XmlAttribute("IsSmsClose")]
        [Bindable(true)]
        public bool IsSmsClose 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_sms_close");
		    }
            set 
		    {
			    SetColumnValue("is_sms_close", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime? StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	      
        [XmlAttribute("Pic")]
        [Bindable(true)]
        public string Pic 
	    {
		    get
		    {
			    return GetColumnValue<string>("pic");
		    }
            set 
		    {
			    SetColumnValue("pic", value);
            }
        }
	      
        [XmlAttribute("HasBranchStoreQuantityLimit")]
        [Bindable(true)]
        public bool HasBranchStoreQuantityLimit 
	    {
		    get
		    {
			    return GetColumnValue<bool>("has_branch_store_quantity_limit");
		    }
            set 
		    {
			    SetColumnValue("has_branch_store_quantity_limit", value);
            }
        }
	      
        [XmlAttribute("HomeDeliveryQuantity")]
        [Bindable(true)]
        public int? HomeDeliveryQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("home_delivery_quantity");
		    }
            set 
		    {
			    SetColumnValue("home_delivery_quantity", value);
            }
        }
	      
        [XmlAttribute("DeliveryIslands")]
        [Bindable(true)]
        public bool DeliveryIslands 
	    {
		    get
		    {
			    return GetColumnValue<bool>("deliveryIslands");
		    }
            set 
		    {
			    SetColumnValue("deliveryIslands", value);
            }
        }
	      
        [XmlAttribute("ShippedDate")]
        [Bindable(true)]
        public DateTime? ShippedDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("shipped_date");
		    }
            set 
		    {
			    SetColumnValue("shipped_date", value);
            }
        }
	      
        [XmlAttribute("TrustType")]
        [Bindable(true)]
        public int TrustType 
	    {
		    get
		    {
			    return GetColumnValue<int>("trust_type");
		    }
            set 
		    {
			    SetColumnValue("trust_type", value);
            }
        }
	      
        [XmlAttribute("IsShowPriceDiscount")]
        [Bindable(true)]
        public bool IsShowPriceDiscount 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_show_Price_discount");
		    }
            set 
		    {
			    SetColumnValue("is_show_Price_discount", value);
            }
        }
	      
        [XmlAttribute("IsInvoiceCreate")]
        [Bindable(true)]
        public bool IsInvoiceCreate 
	    {
		    get
		    {
			    return GetColumnValue<bool>("Is_Invoice_Create");
		    }
            set 
		    {
			    SetColumnValue("Is_Invoice_Create", value);
            }
        }
	      
        [XmlAttribute("PayToCompany")]
        [Bindable(true)]
        public bool PayToCompany 
	    {
		    get
		    {
			    return GetColumnValue<bool>("pay_to_company");
		    }
            set 
		    {
			    SetColumnValue("pay_to_company", value);
            }
        }
	      
        [XmlAttribute("IsCombo")]
        [Bindable(true)]
        public bool IsCombo 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_combo");
		    }
            set 
		    {
			    SetColumnValue("is_combo", value);
            }
        }
	      
        [XmlAttribute("IsDaysNoRefund")]
        [Bindable(true)]
        public bool IsDaysNoRefund 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_days_no_refund");
		    }
            set 
		    {
			    SetColumnValue("is_days_no_refund", value);
            }
        }
	      
        [XmlAttribute("VendorBillingModel")]
        [Bindable(true)]
        public int VendorBillingModel 
	    {
		    get
		    {
			    return GetColumnValue<int>("vendor_billing_model");
		    }
            set 
		    {
			    SetColumnValue("vendor_billing_model", value);
            }
        }
	      
        [XmlAttribute("VendorReceiptType")]
        [Bindable(true)]
        public int VendorReceiptType 
	    {
		    get
		    {
			    return GetColumnValue<int>("vendor_receipt_type");
		    }
            set 
		    {
			    SetColumnValue("vendor_receipt_type", value);
            }
        }
	      
        [XmlAttribute("ChangedExpireTime")]
        [Bindable(true)]
        public DateTime? ChangedExpireTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("changed_expire_time");
		    }
            set 
		    {
			    SetColumnValue("changed_expire_time", value);
            }
        }
	      
        [XmlAttribute("RemittanceType")]
        [Bindable(true)]
        public int RemittanceType 
	    {
		    get
		    {
			    return GetColumnValue<int>("remittance_type");
		    }
            set 
		    {
			    SetColumnValue("remittance_type", value);
            }
        }
	      
        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message 
	    {
		    get
		    {
			    return GetColumnValue<string>("message");
		    }
            set 
		    {
			    SetColumnValue("message", value);
            }
        }
	      
        [XmlAttribute("IsInputTaxFree")]
        [Bindable(true)]
        public bool IsInputTaxFree 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_input_tax_free");
		    }
            set 
		    {
			    SetColumnValue("is_input_tax_free", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("DealName");
		    }
            set 
		    {
			    SetColumnValue("DealName", value);
            }
        }
	      
        [XmlAttribute("EmpEmail")]
        [Bindable(true)]
        public string EmpEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("EmpEmail");
		    }
            set 
		    {
			    SetColumnValue("EmpEmail", value);
            }
        }
	      
        [XmlAttribute("SellerEmail")]
        [Bindable(true)]
        public string SellerEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("SellerEmail");
		    }
            set 
		    {
			    SetColumnValue("SellerEmail", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonEmail")]
        [Bindable(true)]
        public string ReturnedPersonEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("ReturnedPersonEmail");
		    }
            set 
		    {
			    SetColumnValue("ReturnedPersonEmail", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string Guid = @"guid";
            
            public static string DealId = @"deal_id";
            
            public static string Seq = @"seq";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string Name = @"name";
            
            public static string Description = @"description";
            
            public static string IsOnline = @"is_online";
            
            public static string UseStartTime = @"use_start_time";
            
            public static string UseEndTime = @"use_end_time";
            
            public static string Type = @"type";
            
            public static string OriginalPrice = @"original_price";
            
            public static string Price = @"price";
            
            public static string Quantity = @"quantity";
            
            public static string OrderLimit = @"order_limit";
            
            public static string OrderLimitUser = @"order_limit_user";
            
            public static string OrderCount = @"order_count";
            
            public static string IsNoRefund = @"is_no_refund";
            
            public static string IsExpireNoRefund = @"is_expire_no_refund";
            
            public static string IsTaxFree = @"is_tax_free";
            
            public static string IsSoldout = @"is_soldout";
            
            public static string IsInStore = @"is_in_store";
            
            public static string InStoreDesc = @"in_store_desc";
            
            public static string IsHomeDelivery = @"is_home_delivery";
            
            public static string HomeDeliveryDesc = @"home_delivery_desc";
            
            public static string Sms = @"sms";
            
            public static string IsSmsClose = @"is_sms_close";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string ModifyTime = @"modify_time";
            
            public static string StartTime = @"start_time";
            
            public static string EndTime = @"end_time";
            
            public static string Pic = @"pic";
            
            public static string HasBranchStoreQuantityLimit = @"has_branch_store_quantity_limit";
            
            public static string HomeDeliveryQuantity = @"home_delivery_quantity";
            
            public static string DeliveryIslands = @"deliveryIslands";
            
            public static string ShippedDate = @"shipped_date";
            
            public static string TrustType = @"trust_type";
            
            public static string IsShowPriceDiscount = @"is_show_Price_discount";
            
            public static string IsInvoiceCreate = @"Is_Invoice_Create";
            
            public static string PayToCompany = @"pay_to_company";
            
            public static string IsCombo = @"is_combo";
            
            public static string IsDaysNoRefund = @"is_days_no_refund";
            
            public static string VendorBillingModel = @"vendor_billing_model";
            
            public static string VendorReceiptType = @"vendor_receipt_type";
            
            public static string ChangedExpireTime = @"changed_expire_time";
            
            public static string RemittanceType = @"remittance_type";
            
            public static string Message = @"message";
            
            public static string IsInputTaxFree = @"is_input_tax_free";
            
            public static string DealName = @"DealName";
            
            public static string EmpEmail = @"EmpEmail";
            
            public static string SellerEmail = @"SellerEmail";
            
            public static string ReturnedPersonEmail = @"ReturnedPersonEmail";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

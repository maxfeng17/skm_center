using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEventPromoVoteRecord class.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromoVoteRecordCollection : ReadOnlyList<ViewEventPromoVoteRecord, ViewEventPromoVoteRecordCollection>
    {        
        public ViewEventPromoVoteRecordCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_event_promo_vote_record view.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromoVoteRecord : ReadOnlyRecord<ViewEventPromoVoteRecord>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_event_promo_vote_record", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarEventPromoItemId = new TableSchema.TableColumn(schema);
                colvarEventPromoItemId.ColumnName = "event_promo_item_id";
                colvarEventPromoItemId.DataType = DbType.Int32;
                colvarEventPromoItemId.MaxLength = 0;
                colvarEventPromoItemId.AutoIncrement = false;
                colvarEventPromoItemId.IsNullable = false;
                colvarEventPromoItemId.IsPrimaryKey = false;
                colvarEventPromoItemId.IsForeignKey = false;
                colvarEventPromoItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventPromoItemId);
                
                TableSchema.TableColumn colvarMemberUniqueId = new TableSchema.TableColumn(schema);
                colvarMemberUniqueId.ColumnName = "member_unique_id";
                colvarMemberUniqueId.DataType = DbType.Int32;
                colvarMemberUniqueId.MaxLength = 0;
                colvarMemberUniqueId.AutoIncrement = false;
                colvarMemberUniqueId.IsNullable = false;
                colvarMemberUniqueId.IsPrimaryKey = false;
                colvarMemberUniqueId.IsForeignKey = false;
                colvarMemberUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberUniqueId);
                
                TableSchema.TableColumn colvarVoteTime = new TableSchema.TableColumn(schema);
                colvarVoteTime.ColumnName = "vote_time";
                colvarVoteTime.DataType = DbType.DateTime;
                colvarVoteTime.MaxLength = 0;
                colvarVoteTime.AutoIncrement = false;
                colvarVoteTime.IsNullable = false;
                colvarVoteTime.IsPrimaryKey = false;
                colvarVoteTime.IsForeignKey = false;
                colvarVoteTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVoteTime);
                
                TableSchema.TableColumn colvarIpAddress = new TableSchema.TableColumn(schema);
                colvarIpAddress.ColumnName = "ip_address";
                colvarIpAddress.DataType = DbType.AnsiString;
                colvarIpAddress.MaxLength = 40;
                colvarIpAddress.AutoIncrement = false;
                colvarIpAddress.IsNullable = true;
                colvarIpAddress.IsPrimaryKey = false;
                colvarIpAddress.IsForeignKey = false;
                colvarIpAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarIpAddress);
                
                TableSchema.TableColumn colvarDiscountCodeId = new TableSchema.TableColumn(schema);
                colvarDiscountCodeId.ColumnName = "discount_code_id";
                colvarDiscountCodeId.DataType = DbType.Int32;
                colvarDiscountCodeId.MaxLength = 0;
                colvarDiscountCodeId.AutoIncrement = false;
                colvarDiscountCodeId.IsNullable = false;
                colvarDiscountCodeId.IsPrimaryKey = false;
                colvarDiscountCodeId.IsForeignKey = false;
                colvarDiscountCodeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountCodeId);
                
                TableSchema.TableColumn colvarEventPromoId = new TableSchema.TableColumn(schema);
                colvarEventPromoId.ColumnName = "event_promo_id";
                colvarEventPromoId.DataType = DbType.Int32;
                colvarEventPromoId.MaxLength = 0;
                colvarEventPromoId.AutoIncrement = false;
                colvarEventPromoId.IsNullable = false;
                colvarEventPromoId.IsPrimaryKey = false;
                colvarEventPromoId.IsForeignKey = false;
                colvarEventPromoId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventPromoId);
                
                TableSchema.TableColumn colvarItemType = new TableSchema.TableColumn(schema);
                colvarItemType.ColumnName = "item_type";
                colvarItemType.DataType = DbType.Int32;
                colvarItemType.MaxLength = 0;
                colvarItemType.AutoIncrement = false;
                colvarItemType.IsNullable = false;
                colvarItemType.IsPrimaryKey = false;
                colvarItemType.IsForeignKey = false;
                colvarItemType.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_event_promo_vote_record",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewEventPromoVoteRecord()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEventPromoVoteRecord(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewEventPromoVoteRecord(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewEventPromoVoteRecord(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("EventPromoItemId")]
        [Bindable(true)]
        public int EventPromoItemId 
	    {
		    get
		    {
			    return GetColumnValue<int>("event_promo_item_id");
		    }
            set 
		    {
			    SetColumnValue("event_promo_item_id", value);
            }
        }
	      
        [XmlAttribute("MemberUniqueId")]
        [Bindable(true)]
        public int MemberUniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("member_unique_id");
		    }
            set 
		    {
			    SetColumnValue("member_unique_id", value);
            }
        }
	      
        [XmlAttribute("VoteTime")]
        [Bindable(true)]
        public DateTime VoteTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("vote_time");
		    }
            set 
		    {
			    SetColumnValue("vote_time", value);
            }
        }
	      
        [XmlAttribute("IpAddress")]
        [Bindable(true)]
        public string IpAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("ip_address");
		    }
            set 
		    {
			    SetColumnValue("ip_address", value);
            }
        }
	      
        [XmlAttribute("DiscountCodeId")]
        [Bindable(true)]
        public int DiscountCodeId 
	    {
		    get
		    {
			    return GetColumnValue<int>("discount_code_id");
		    }
            set 
		    {
			    SetColumnValue("discount_code_id", value);
            }
        }
	      
        [XmlAttribute("EventPromoId")]
        [Bindable(true)]
        public int EventPromoId 
	    {
		    get
		    {
			    return GetColumnValue<int>("event_promo_id");
		    }
            set 
		    {
			    SetColumnValue("event_promo_id", value);
            }
        }
	      
        [XmlAttribute("ItemType")]
        [Bindable(true)]
        public int ItemType 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_type");
		    }
            set 
		    {
			    SetColumnValue("item_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string EventPromoItemId = @"event_promo_item_id";
            
            public static string MemberUniqueId = @"member_unique_id";
            
            public static string VoteTime = @"vote_time";
            
            public static string IpAddress = @"ip_address";
            
            public static string DiscountCodeId = @"discount_code_id";
            
            public static string EventPromoId = @"event_promo_id";
            
            public static string ItemType = @"item_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

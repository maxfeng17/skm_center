using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class PcashXchOrderCollection : RepositoryList<PcashXchOrder, PcashXchOrderCollection>
	{
			public PcashXchOrderCollection() {}

			public PcashXchOrderCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							PcashXchOrder o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class PcashXchOrder : RepositoryRecord<PcashXchOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		public PcashXchOrder()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public PcashXchOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcash_xch_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);

				TableSchema.TableColumn colvarPezAuthCode = new TableSchema.TableColumn(schema);
				colvarPezAuthCode.ColumnName = "pez_auth_code";
				colvarPezAuthCode.DataType = DbType.String;
				colvarPezAuthCode.MaxLength = 200;
				colvarPezAuthCode.AutoIncrement = false;
				colvarPezAuthCode.IsNullable = true;
				colvarPezAuthCode.IsPrimaryKey = false;
				colvarPezAuthCode.IsForeignKey = false;
				colvarPezAuthCode.IsReadOnly = false;
				colvarPezAuthCode.DefaultSetting = @"";
				colvarPezAuthCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPezAuthCode);

				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Int32;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = true;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarIsCompleted = new TableSchema.TableColumn(schema);
				colvarIsCompleted.ColumnName = "is_completed";
				colvarIsCompleted.DataType = DbType.Boolean;
				colvarIsCompleted.MaxLength = 0;
				colvarIsCompleted.AutoIncrement = false;
				colvarIsCompleted.IsNullable = false;
				colvarIsCompleted.IsPrimaryKey = false;
				colvarIsCompleted.IsForeignKey = false;
				colvarIsCompleted.IsReadOnly = false;
				colvarIsCompleted.DefaultSetting = @"";
				colvarIsCompleted.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCompleted);

				TableSchema.TableColumn colvarCompletedTime = new TableSchema.TableColumn(schema);
				colvarCompletedTime.ColumnName = "completed_time";
				colvarCompletedTime.DataType = DbType.DateTime;
				colvarCompletedTime.MaxLength = 0;
				colvarCompletedTime.AutoIncrement = false;
				colvarCompletedTime.IsNullable = true;
				colvarCompletedTime.IsPrimaryKey = false;
				colvarCompletedTime.IsForeignKey = false;
				colvarCompletedTime.IsReadOnly = false;
				colvarCompletedTime.DefaultSetting = @"";
				colvarCompletedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompletedTime);

				TableSchema.TableColumn colvarIsRefund = new TableSchema.TableColumn(schema);
				colvarIsRefund.ColumnName = "is_refund";
				colvarIsRefund.DataType = DbType.Boolean;
				colvarIsRefund.MaxLength = 0;
				colvarIsRefund.AutoIncrement = false;
				colvarIsRefund.IsNullable = false;
				colvarIsRefund.IsPrimaryKey = false;
				colvarIsRefund.IsForeignKey = false;
				colvarIsRefund.IsReadOnly = false;
				colvarIsRefund.DefaultSetting = @"";
				colvarIsRefund.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsRefund);

				TableSchema.TableColumn colvarRefundAmount = new TableSchema.TableColumn(schema);
				colvarRefundAmount.ColumnName = "refund_amount";
				colvarRefundAmount.DataType = DbType.Int32;
				colvarRefundAmount.MaxLength = 0;
				colvarRefundAmount.AutoIncrement = false;
				colvarRefundAmount.IsNullable = true;
				colvarRefundAmount.IsPrimaryKey = false;
				colvarRefundAmount.IsForeignKey = false;
				colvarRefundAmount.IsReadOnly = false;
				colvarRefundAmount.DefaultSetting = @"";
				colvarRefundAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundAmount);

				TableSchema.TableColumn colvarRefundTime = new TableSchema.TableColumn(schema);
				colvarRefundTime.ColumnName = "refund_time";
				colvarRefundTime.DataType = DbType.DateTime;
				colvarRefundTime.MaxLength = 0;
				colvarRefundTime.AutoIncrement = false;
				colvarRefundTime.IsNullable = true;
				colvarRefundTime.IsPrimaryKey = false;
				colvarRefundTime.IsForeignKey = false;
				colvarRefundTime.IsReadOnly = false;
				colvarRefundTime.DefaultSetting = @"";
				colvarRefundTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundTime);

				TableSchema.TableColumn colvarRefundCreateId = new TableSchema.TableColumn(schema);
				colvarRefundCreateId.ColumnName = "refund_create_id";
				colvarRefundCreateId.DataType = DbType.String;
				colvarRefundCreateId.MaxLength = 200;
				colvarRefundCreateId.AutoIncrement = false;
				colvarRefundCreateId.IsNullable = true;
				colvarRefundCreateId.IsPrimaryKey = false;
				colvarRefundCreateId.IsForeignKey = false;
				colvarRefundCreateId.IsReadOnly = false;
				colvarRefundCreateId.DefaultSetting = @"";
				colvarRefundCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundCreateId);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcash_xch_order",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}

		[XmlAttribute("PezAuthCode")]
		[Bindable(true)]
		public string PezAuthCode
		{
			get { return GetColumnValue<string>(Columns.PezAuthCode); }
			set { SetColumnValue(Columns.PezAuthCode, value); }
		}

		[XmlAttribute("Amount")]
		[Bindable(true)]
		public int? Amount
		{
			get { return GetColumnValue<int?>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("IsCompleted")]
		[Bindable(true)]
		public bool IsCompleted
		{
			get { return GetColumnValue<bool>(Columns.IsCompleted); }
			set { SetColumnValue(Columns.IsCompleted, value); }
		}

		[XmlAttribute("CompletedTime")]
		[Bindable(true)]
		public DateTime? CompletedTime
		{
			get { return GetColumnValue<DateTime?>(Columns.CompletedTime); }
			set { SetColumnValue(Columns.CompletedTime, value); }
		}

		[XmlAttribute("IsRefund")]
		[Bindable(true)]
		public bool IsRefund
		{
			get { return GetColumnValue<bool>(Columns.IsRefund); }
			set { SetColumnValue(Columns.IsRefund, value); }
		}

		[XmlAttribute("RefundAmount")]
		[Bindable(true)]
		public int? RefundAmount
		{
			get { return GetColumnValue<int?>(Columns.RefundAmount); }
			set { SetColumnValue(Columns.RefundAmount, value); }
		}

		[XmlAttribute("RefundTime")]
		[Bindable(true)]
		public DateTime? RefundTime
		{
			get { return GetColumnValue<DateTime?>(Columns.RefundTime); }
			set { SetColumnValue(Columns.RefundTime, value); }
		}

		[XmlAttribute("RefundCreateId")]
		[Bindable(true)]
		public string RefundCreateId
		{
			get { return GetColumnValue<string>(Columns.RefundCreateId); }
			set { SetColumnValue(Columns.RefundCreateId, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn GuidColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn PezAuthCodeColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn AmountColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn IsCompletedColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn CompletedTimeColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn IsRefundColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn RefundAmountColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn RefundTimeColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn RefundCreateIdColumn
		{
			get { return Schema.Columns[11]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string Guid = @"guid";
			public static string PezAuthCode = @"pez_auth_code";
			public static string Amount = @"amount";
			public static string UserId = @"user_id";
			public static string CreateTime = @"create_time";
			public static string IsCompleted = @"is_completed";
			public static string CompletedTime = @"completed_time";
			public static string IsRefund = @"is_refund";
			public static string RefundAmount = @"refund_amount";
			public static string RefundTime = @"refund_time";
			public static string RefundCreateId = @"refund_create_id";
		}

		#endregion

	}
}

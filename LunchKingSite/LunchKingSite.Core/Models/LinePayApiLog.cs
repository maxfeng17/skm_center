using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the LinePayApiLog class.
	/// </summary>
    [Serializable]
	public partial class LinePayApiLogCollection : RepositoryList<LinePayApiLog, LinePayApiLogCollection>
	{	   
		public LinePayApiLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>LinePayApiLogCollection</returns>
		public LinePayApiLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                LinePayApiLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the line_pay_api_log table.
	/// </summary>
	[Serializable]
	public partial class LinePayApiLog : RepositoryRecord<LinePayApiLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public LinePayApiLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public LinePayApiLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("line_pay_api_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarLinePayTransId = new TableSchema.TableColumn(schema);
				colvarLinePayTransId.ColumnName = "line_pay_trans_id";
				colvarLinePayTransId.DataType = DbType.Int32;
				colvarLinePayTransId.MaxLength = 0;
				colvarLinePayTransId.AutoIncrement = false;
				colvarLinePayTransId.IsNullable = false;
				colvarLinePayTransId.IsPrimaryKey = false;
				colvarLinePayTransId.IsForeignKey = false;
				colvarLinePayTransId.IsReadOnly = false;
				colvarLinePayTransId.DefaultSetting = @"";
				colvarLinePayTransId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLinePayTransId);
				
				TableSchema.TableColumn colvarLogType = new TableSchema.TableColumn(schema);
				colvarLogType.ColumnName = "log_type";
				colvarLogType.DataType = DbType.Byte;
				colvarLogType.MaxLength = 0;
				colvarLogType.AutoIncrement = false;
				colvarLogType.IsNullable = false;
				colvarLogType.IsPrimaryKey = false;
				colvarLogType.IsForeignKey = false;
				colvarLogType.IsReadOnly = false;
				colvarLogType.DefaultSetting = @"";
				colvarLogType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogType);
				
				TableSchema.TableColumn colvarReturnCode = new TableSchema.TableColumn(schema);
				colvarReturnCode.ColumnName = "return_code";
				colvarReturnCode.DataType = DbType.AnsiString;
				colvarReturnCode.MaxLength = 10;
				colvarReturnCode.AutoIncrement = false;
				colvarReturnCode.IsNullable = false;
				colvarReturnCode.IsPrimaryKey = false;
				colvarReturnCode.IsForeignKey = false;
				colvarReturnCode.IsReadOnly = false;
				colvarReturnCode.DefaultSetting = @"";
				colvarReturnCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnCode);
				
				TableSchema.TableColumn colvarCodeMeaning = new TableSchema.TableColumn(schema);
				colvarCodeMeaning.ColumnName = "code_meaning";
				colvarCodeMeaning.DataType = DbType.String;
				colvarCodeMeaning.MaxLength = 100;
				colvarCodeMeaning.AutoIncrement = false;
				colvarCodeMeaning.IsNullable = true;
				colvarCodeMeaning.IsPrimaryKey = false;
				colvarCodeMeaning.IsForeignKey = false;
				colvarCodeMeaning.IsReadOnly = false;
				colvarCodeMeaning.DefaultSetting = @"";
				colvarCodeMeaning.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodeMeaning);
				
				TableSchema.TableColumn colvarReturnMsg = new TableSchema.TableColumn(schema);
				colvarReturnMsg.ColumnName = "return_msg";
				colvarReturnMsg.DataType = DbType.String;
				colvarReturnMsg.MaxLength = 100;
				colvarReturnMsg.AutoIncrement = false;
				colvarReturnMsg.IsNullable = false;
				colvarReturnMsg.IsPrimaryKey = false;
				colvarReturnMsg.IsForeignKey = false;
				colvarReturnMsg.IsReadOnly = false;
				colvarReturnMsg.DefaultSetting = @"";
				colvarReturnMsg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnMsg);
				
				TableSchema.TableColumn colvarReturnObj = new TableSchema.TableColumn(schema);
				colvarReturnObj.ColumnName = "return_obj";
				colvarReturnObj.DataType = DbType.String;
				colvarReturnObj.MaxLength = -1;
				colvarReturnObj.AutoIncrement = false;
				colvarReturnObj.IsNullable = false;
				colvarReturnObj.IsPrimaryKey = false;
				colvarReturnObj.IsForeignKey = false;
				colvarReturnObj.IsReadOnly = false;
				colvarReturnObj.DefaultSetting = @"";
				colvarReturnObj.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnObj);
				
				TableSchema.TableColumn colvarLogTime = new TableSchema.TableColumn(schema);
				colvarLogTime.ColumnName = "log_time";
				colvarLogTime.DataType = DbType.DateTime;
				colvarLogTime.MaxLength = 0;
				colvarLogTime.AutoIncrement = false;
				colvarLogTime.IsNullable = false;
				colvarLogTime.IsPrimaryKey = false;
				colvarLogTime.IsForeignKey = false;
				colvarLogTime.IsReadOnly = false;
				
						colvarLogTime.DefaultSetting = @"(getdate())";
				colvarLogTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("line_pay_api_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("LinePayTransId")]
		[Bindable(true)]
		public int LinePayTransId 
		{
			get { return GetColumnValue<int>(Columns.LinePayTransId); }
			set { SetColumnValue(Columns.LinePayTransId, value); }
		}
		  
		[XmlAttribute("LogType")]
		[Bindable(true)]
		public byte LogType 
		{
			get { return GetColumnValue<byte>(Columns.LogType); }
			set { SetColumnValue(Columns.LogType, value); }
		}
		  
		[XmlAttribute("ReturnCode")]
		[Bindable(true)]
		public string ReturnCode 
		{
			get { return GetColumnValue<string>(Columns.ReturnCode); }
			set { SetColumnValue(Columns.ReturnCode, value); }
		}
		  
		[XmlAttribute("CodeMeaning")]
		[Bindable(true)]
		public string CodeMeaning 
		{
			get { return GetColumnValue<string>(Columns.CodeMeaning); }
			set { SetColumnValue(Columns.CodeMeaning, value); }
		}
		  
		[XmlAttribute("ReturnMsg")]
		[Bindable(true)]
		public string ReturnMsg 
		{
			get { return GetColumnValue<string>(Columns.ReturnMsg); }
			set { SetColumnValue(Columns.ReturnMsg, value); }
		}
		  
		[XmlAttribute("ReturnObj")]
		[Bindable(true)]
		public string ReturnObj 
		{
			get { return GetColumnValue<string>(Columns.ReturnObj); }
			set { SetColumnValue(Columns.ReturnObj, value); }
		}
		  
		[XmlAttribute("LogTime")]
		[Bindable(true)]
		public DateTime LogTime 
		{
			get { return GetColumnValue<DateTime>(Columns.LogTime); }
			set { SetColumnValue(Columns.LogTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn LinePayTransIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn LogTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnCodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeMeaningColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnMsgColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnObjColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn LogTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string LinePayTransId = @"line_pay_trans_id";
			 public static string LogType = @"log_type";
			 public static string ReturnCode = @"return_code";
			 public static string CodeMeaning = @"code_meaning";
			 public static string ReturnMsg = @"return_msg";
			 public static string ReturnObj = @"return_obj";
			 public static string LogTime = @"log_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

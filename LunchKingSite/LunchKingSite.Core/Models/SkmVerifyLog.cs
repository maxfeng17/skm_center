using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmVerifyLog class.
	/// </summary>
    [Serializable]
	public partial class SkmVerifyLogCollection : RepositoryList<SkmVerifyLog, SkmVerifyLogCollection>
	{	   
		public SkmVerifyLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmVerifyLogCollection</returns>
		public SkmVerifyLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmVerifyLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_verify_log table.
	/// </summary>
	
	[Serializable]
	public partial class SkmVerifyLog : RepositoryRecord<SkmVerifyLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmVerifyLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmVerifyLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_verify_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSkmId = new TableSchema.TableColumn(schema);
				colvarSkmId.ColumnName = "skm_id";
				colvarSkmId.DataType = DbType.Int32;
				colvarSkmId.MaxLength = 0;
				colvarSkmId.AutoIncrement = false;
				colvarSkmId.IsNullable = false;
				colvarSkmId.IsPrimaryKey = false;
				colvarSkmId.IsForeignKey = false;
				colvarSkmId.IsReadOnly = false;
				colvarSkmId.DefaultSetting = @"";
				colvarSkmId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmId);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarIsForce = new TableSchema.TableColumn(schema);
				colvarIsForce.ColumnName = "is_force";
				colvarIsForce.DataType = DbType.Boolean;
				colvarIsForce.MaxLength = 0;
				colvarIsForce.AutoIncrement = false;
				colvarIsForce.IsNullable = false;
				colvarIsForce.IsPrimaryKey = false;
				colvarIsForce.IsForeignKey = false;
				colvarIsForce.IsReadOnly = false;
				colvarIsForce.DefaultSetting = @"";
				colvarIsForce.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsForce);
				
				TableSchema.TableColumn colvarSyncStatus = new TableSchema.TableColumn(schema);
				colvarSyncStatus.ColumnName = "sync_status";
				colvarSyncStatus.DataType = DbType.Int32;
				colvarSyncStatus.MaxLength = 0;
				colvarSyncStatus.AutoIncrement = false;
				colvarSyncStatus.IsNullable = false;
				colvarSyncStatus.IsPrimaryKey = false;
				colvarSyncStatus.IsForeignKey = false;
				colvarSyncStatus.IsReadOnly = false;
				colvarSyncStatus.DefaultSetting = @"";
				colvarSyncStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSyncStatus);
				
				TableSchema.TableColumn colvarVerifyAction = new TableSchema.TableColumn(schema);
				colvarVerifyAction.ColumnName = "verify_action";
				colvarVerifyAction.DataType = DbType.AnsiString;
				colvarVerifyAction.MaxLength = 20;
				colvarVerifyAction.AutoIncrement = false;
				colvarVerifyAction.IsNullable = true;
				colvarVerifyAction.IsPrimaryKey = false;
				colvarVerifyAction.IsForeignKey = false;
				colvarVerifyAction.IsReadOnly = false;
				colvarVerifyAction.DefaultSetting = @"";
				colvarVerifyAction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifyAction);
				
				TableSchema.TableColumn colvarSkmTransId = new TableSchema.TableColumn(schema);
				colvarSkmTransId.ColumnName = "skm_trans_id";
				colvarSkmTransId.DataType = DbType.AnsiString;
				colvarSkmTransId.MaxLength = 40;
				colvarSkmTransId.AutoIncrement = false;
				colvarSkmTransId.IsNullable = true;
				colvarSkmTransId.IsPrimaryKey = false;
				colvarSkmTransId.IsForeignKey = false;
				colvarSkmTransId.IsReadOnly = false;
				colvarSkmTransId.DefaultSetting = @"";
				colvarSkmTransId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmTransId);
				
				TableSchema.TableColumn colvarSyncMsg = new TableSchema.TableColumn(schema);
				colvarSyncMsg.ColumnName = "sync_msg";
				colvarSyncMsg.DataType = DbType.String;
				colvarSyncMsg.MaxLength = 500;
				colvarSyncMsg.AutoIncrement = false;
				colvarSyncMsg.IsNullable = true;
				colvarSyncMsg.IsPrimaryKey = false;
				colvarSyncMsg.IsForeignKey = false;
				colvarSyncMsg.IsReadOnly = false;
				colvarSyncMsg.DefaultSetting = @"";
				colvarSyncMsg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSyncMsg);
				
				TableSchema.TableColumn colvarShopeCode = new TableSchema.TableColumn(schema);
				colvarShopeCode.ColumnName = "shope_code";
				colvarShopeCode.DataType = DbType.AnsiString;
				colvarShopeCode.MaxLength = 4;
				colvarShopeCode.AutoIncrement = false;
				colvarShopeCode.IsNullable = true;
				colvarShopeCode.IsPrimaryKey = false;
				colvarShopeCode.IsForeignKey = false;
				colvarShopeCode.IsReadOnly = false;
				colvarShopeCode.DefaultSetting = @"";
				colvarShopeCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShopeCode);
				
				TableSchema.TableColumn colvarBrandCounterCode = new TableSchema.TableColumn(schema);
				colvarBrandCounterCode.ColumnName = "brand_counter_code";
				colvarBrandCounterCode.DataType = DbType.AnsiString;
				colvarBrandCounterCode.MaxLength = 10;
				colvarBrandCounterCode.AutoIncrement = false;
				colvarBrandCounterCode.IsNullable = true;
				colvarBrandCounterCode.IsPrimaryKey = false;
				colvarBrandCounterCode.IsForeignKey = false;
				colvarBrandCounterCode.IsReadOnly = false;
				colvarBrandCounterCode.DefaultSetting = @"";
				colvarBrandCounterCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandCounterCode);
				
				TableSchema.TableColumn colvarInputModel = new TableSchema.TableColumn(schema);
				colvarInputModel.ColumnName = "input_model";
				colvarInputModel.DataType = DbType.String;
				colvarInputModel.MaxLength = -1;
				colvarInputModel.AutoIncrement = false;
				colvarInputModel.IsNullable = true;
				colvarInputModel.IsPrimaryKey = false;
				colvarInputModel.IsForeignKey = false;
				colvarInputModel.IsReadOnly = false;
				colvarInputModel.DefaultSetting = @"";
				colvarInputModel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInputModel);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_verify_log",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("SkmId")]
		[Bindable(true)]
		public int SkmId 
		{
			get { return GetColumnValue<int>(Columns.SkmId); }
			set { SetColumnValue(Columns.SkmId, value); }
		}
		
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("IsForce")]
		[Bindable(true)]
		public bool IsForce 
		{
			get { return GetColumnValue<bool>(Columns.IsForce); }
			set { SetColumnValue(Columns.IsForce, value); }
		}
		
		[XmlAttribute("SyncStatus")]
		[Bindable(true)]
		public int SyncStatus 
		{
			get { return GetColumnValue<int>(Columns.SyncStatus); }
			set { SetColumnValue(Columns.SyncStatus, value); }
		}
		
		[XmlAttribute("VerifyAction")]
		[Bindable(true)]
		public string VerifyAction 
		{
			get { return GetColumnValue<string>(Columns.VerifyAction); }
			set { SetColumnValue(Columns.VerifyAction, value); }
		}
		
		[XmlAttribute("SkmTransId")]
		[Bindable(true)]
		public string SkmTransId 
		{
			get { return GetColumnValue<string>(Columns.SkmTransId); }
			set { SetColumnValue(Columns.SkmTransId, value); }
		}
		
		[XmlAttribute("SyncMsg")]
		[Bindable(true)]
		public string SyncMsg 
		{
			get { return GetColumnValue<string>(Columns.SyncMsg); }
			set { SetColumnValue(Columns.SyncMsg, value); }
		}
		
		[XmlAttribute("ShopeCode")]
		[Bindable(true)]
		public string ShopeCode 
		{
			get { return GetColumnValue<string>(Columns.ShopeCode); }
			set { SetColumnValue(Columns.ShopeCode, value); }
		}
		
		[XmlAttribute("BrandCounterCode")]
		[Bindable(true)]
		public string BrandCounterCode 
		{
			get { return GetColumnValue<string>(Columns.BrandCounterCode); }
			set { SetColumnValue(Columns.BrandCounterCode, value); }
		}
		
		[XmlAttribute("InputModel")]
		[Bindable(true)]
		public string InputModel 
		{
			get { return GetColumnValue<string>(Columns.InputModel); }
			set { SetColumnValue(Columns.InputModel, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IsForceColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn SyncStatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn VerifyActionColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmTransIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn SyncMsgColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ShopeCodeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn BrandCounterCodeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn InputModelColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SkmId = @"skm_id";
			 public static string TrustId = @"trust_id";
			 public static string CreateTime = @"create_time";
			 public static string IsForce = @"is_force";
			 public static string SyncStatus = @"sync_status";
			 public static string VerifyAction = @"verify_action";
			 public static string SkmTransId = @"skm_trans_id";
			 public static string SyncMsg = @"sync_msg";
			 public static string ShopeCode = @"shope_code";
			 public static string BrandCounterCode = @"brand_counter_code";
			 public static string InputModel = @"input_model";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

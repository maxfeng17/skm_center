using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealSalesInfo class.
	/// </summary>
    [Serializable]
	public partial class DealSalesInfoCollection : RepositoryList<DealSalesInfo, DealSalesInfoCollection>
	{	   
		public DealSalesInfoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealSalesInfoCollection</returns>
		public DealSalesInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealSalesInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_sales_info table.
	/// </summary>
	[Serializable]
	public partial class DealSalesInfo : RepositoryRecord<DealSalesInfo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealSalesInfo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DealSalesInfo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("deal_sales_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = true;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarOrderedQuantity = new TableSchema.TableColumn(schema);
				colvarOrderedQuantity.ColumnName = "ordered_quantity";
				colvarOrderedQuantity.DataType = DbType.Int32;
				colvarOrderedQuantity.MaxLength = 0;
				colvarOrderedQuantity.AutoIncrement = false;
				colvarOrderedQuantity.IsNullable = false;
				colvarOrderedQuantity.IsPrimaryKey = false;
				colvarOrderedQuantity.IsForeignKey = false;
				colvarOrderedQuantity.IsReadOnly = false;
				colvarOrderedQuantity.DefaultSetting = @"";
				colvarOrderedQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderedQuantity);
				
				TableSchema.TableColumn colvarOrderedTotal = new TableSchema.TableColumn(schema);
				colvarOrderedTotal.ColumnName = "ordered_total";
				colvarOrderedTotal.DataType = DbType.Currency;
				colvarOrderedTotal.MaxLength = 0;
				colvarOrderedTotal.AutoIncrement = false;
				colvarOrderedTotal.IsNullable = false;
				colvarOrderedTotal.IsPrimaryKey = false;
				colvarOrderedTotal.IsForeignKey = false;
				colvarOrderedTotal.IsReadOnly = false;
				colvarOrderedTotal.DefaultSetting = @"";
				colvarOrderedTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderedTotal);
				
				TableSchema.TableColumn colvarOrderedIncludeRefundQuantity = new TableSchema.TableColumn(schema);
				colvarOrderedIncludeRefundQuantity.ColumnName = "ordered_include_refund_quantity";
				colvarOrderedIncludeRefundQuantity.DataType = DbType.Int32;
				colvarOrderedIncludeRefundQuantity.MaxLength = 0;
				colvarOrderedIncludeRefundQuantity.AutoIncrement = false;
				colvarOrderedIncludeRefundQuantity.IsNullable = true;
				colvarOrderedIncludeRefundQuantity.IsPrimaryKey = false;
				colvarOrderedIncludeRefundQuantity.IsForeignKey = false;
				colvarOrderedIncludeRefundQuantity.IsReadOnly = false;
				colvarOrderedIncludeRefundQuantity.DefaultSetting = @"";
				colvarOrderedIncludeRefundQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderedIncludeRefundQuantity);
				
				TableSchema.TableColumn colvarOrderedIncludeRefundTotal = new TableSchema.TableColumn(schema);
				colvarOrderedIncludeRefundTotal.ColumnName = "ordered_include_refund_total";
				colvarOrderedIncludeRefundTotal.DataType = DbType.Currency;
				colvarOrderedIncludeRefundTotal.MaxLength = 0;
				colvarOrderedIncludeRefundTotal.AutoIncrement = false;
				colvarOrderedIncludeRefundTotal.IsNullable = true;
				colvarOrderedIncludeRefundTotal.IsPrimaryKey = false;
				colvarOrderedIncludeRefundTotal.IsForeignKey = false;
				colvarOrderedIncludeRefundTotal.IsReadOnly = false;
				colvarOrderedIncludeRefundTotal.DefaultSetting = @"";
				colvarOrderedIncludeRefundTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderedIncludeRefundTotal);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("deal_sales_info",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("OrderedQuantity")]
		[Bindable(true)]
		public int OrderedQuantity 
		{
			get { return GetColumnValue<int>(Columns.OrderedQuantity); }
			set { SetColumnValue(Columns.OrderedQuantity, value); }
		}
		  
		[XmlAttribute("OrderedTotal")]
		[Bindable(true)]
		public decimal OrderedTotal 
		{
			get { return GetColumnValue<decimal>(Columns.OrderedTotal); }
			set { SetColumnValue(Columns.OrderedTotal, value); }
		}
		  
		[XmlAttribute("OrderedIncludeRefundQuantity")]
		[Bindable(true)]
		public int? OrderedIncludeRefundQuantity 
		{
			get { return GetColumnValue<int?>(Columns.OrderedIncludeRefundQuantity); }
			set { SetColumnValue(Columns.OrderedIncludeRefundQuantity, value); }
		}
		  
		[XmlAttribute("OrderedIncludeRefundTotal")]
		[Bindable(true)]
		public decimal? OrderedIncludeRefundTotal 
		{
			get { return GetColumnValue<decimal?>(Columns.OrderedIncludeRefundTotal); }
			set { SetColumnValue(Columns.OrderedIncludeRefundTotal, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderedQuantityColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderedTotalColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderedIncludeRefundQuantityColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderedIncludeRefundTotalColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string OrderedQuantity = @"ordered_quantity";
			 public static string OrderedTotal = @"ordered_total";
			 public static string OrderedIncludeRefundQuantity = @"ordered_include_refund_quantity";
			 public static string OrderedIncludeRefundTotal = @"ordered_include_refund_total";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BankAccount class.
	/// </summary>
    [Serializable]
	public partial class BankAccountCollection : RepositoryList<BankAccount, BankAccountCollection>
	{	   
		public BankAccountCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BankAccountCollection</returns>
		public BankAccountCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BankAccount o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the bank_account table.
	/// </summary>
	[Serializable]
	public partial class BankAccount : RepositoryRecord<BankAccount>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BankAccount()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BankAccount(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("bank_account", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = false;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBankNo = new TableSchema.TableColumn(schema);
				colvarBankNo.ColumnName = "bank_no";
				colvarBankNo.DataType = DbType.AnsiString;
				colvarBankNo.MaxLength = 3;
				colvarBankNo.AutoIncrement = false;
				colvarBankNo.IsNullable = false;
				colvarBankNo.IsPrimaryKey = true;
				colvarBankNo.IsForeignKey = false;
				colvarBankNo.IsReadOnly = false;
				colvarBankNo.DefaultSetting = @"";
				colvarBankNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBankNo);
				
				TableSchema.TableColumn colvarBranchNo = new TableSchema.TableColumn(schema);
				colvarBranchNo.ColumnName = "branch_no";
				colvarBranchNo.DataType = DbType.AnsiString;
				colvarBranchNo.MaxLength = 4;
				colvarBranchNo.AutoIncrement = false;
				colvarBranchNo.IsNullable = false;
				colvarBranchNo.IsPrimaryKey = true;
				colvarBranchNo.IsForeignKey = false;
				colvarBranchNo.IsReadOnly = false;
				colvarBranchNo.DefaultSetting = @"";
				colvarBranchNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBranchNo);
				
				TableSchema.TableColumn colvarAccountNo = new TableSchema.TableColumn(schema);
				colvarAccountNo.ColumnName = "account_no";
				colvarAccountNo.DataType = DbType.AnsiString;
				colvarAccountNo.MaxLength = 14;
				colvarAccountNo.AutoIncrement = false;
				colvarAccountNo.IsNullable = false;
				colvarAccountNo.IsPrimaryKey = true;
				colvarAccountNo.IsForeignKey = false;
				colvarAccountNo.IsReadOnly = false;
				colvarAccountNo.DefaultSetting = @"";
				colvarAccountNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountNo);
				
				TableSchema.TableColumn colvarCompanyId = new TableSchema.TableColumn(schema);
				colvarCompanyId.ColumnName = "company_id";
				colvarCompanyId.DataType = DbType.AnsiString;
				colvarCompanyId.MaxLength = 10;
				colvarCompanyId.AutoIncrement = false;
				colvarCompanyId.IsNullable = false;
				colvarCompanyId.IsPrimaryKey = true;
				colvarCompanyId.IsForeignKey = false;
				colvarCompanyId.IsReadOnly = false;
				colvarCompanyId.DefaultSetting = @"";
				colvarCompanyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyId);
				
				TableSchema.TableColumn colvarAccountTitle = new TableSchema.TableColumn(schema);
				colvarAccountTitle.ColumnName = "account_title";
				colvarAccountTitle.DataType = DbType.String;
				colvarAccountTitle.MaxLength = 50;
				colvarAccountTitle.AutoIncrement = false;
				colvarAccountTitle.IsNullable = false;
				colvarAccountTitle.IsPrimaryKey = true;
				colvarAccountTitle.IsForeignKey = false;
				colvarAccountTitle.IsReadOnly = false;
				colvarAccountTitle.DefaultSetting = @"";
				colvarAccountTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountTitle);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("bank_account",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("BankNo")]
		[Bindable(true)]
		public string BankNo 
		{
			get { return GetColumnValue<string>(Columns.BankNo); }
			set { SetColumnValue(Columns.BankNo, value); }
		}
		  
		[XmlAttribute("BranchNo")]
		[Bindable(true)]
		public string BranchNo 
		{
			get { return GetColumnValue<string>(Columns.BranchNo); }
			set { SetColumnValue(Columns.BranchNo, value); }
		}
		  
		[XmlAttribute("AccountNo")]
		[Bindable(true)]
		public string AccountNo 
		{
			get { return GetColumnValue<string>(Columns.AccountNo); }
			set { SetColumnValue(Columns.AccountNo, value); }
		}
		  
		[XmlAttribute("CompanyId")]
		[Bindable(true)]
		public string CompanyId 
		{
			get { return GetColumnValue<string>(Columns.CompanyId); }
			set { SetColumnValue(Columns.CompanyId, value); }
		}
		  
		[XmlAttribute("AccountTitle")]
		[Bindable(true)]
		public string AccountTitle 
		{
			get { return GetColumnValue<string>(Columns.AccountTitle); }
			set { SetColumnValue(Columns.AccountTitle, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BankNoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BranchNoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountNoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountTitleColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BankNo = @"bank_no";
			 public static string BranchNo = @"branch_no";
			 public static string AccountNo = @"account_no";
			 public static string CompanyId = @"company_id";
			 public static string AccountTitle = @"account_title";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

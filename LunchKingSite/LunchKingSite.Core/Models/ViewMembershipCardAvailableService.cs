using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMembershipCardAvailableService class.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardAvailableServiceCollection : ReadOnlyList<ViewMembershipCardAvailableService, ViewMembershipCardAvailableServiceCollection>
    {        
        public ViewMembershipCardAvailableServiceCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_membership_card_available_service view.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardAvailableService : ReadOnlyRecord<ViewMembershipCardAvailableService>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_membership_card_available_service", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
                colvarGroupId.ColumnName = "group_id";
                colvarGroupId.DataType = DbType.Int32;
                colvarGroupId.MaxLength = 0;
                colvarGroupId.AutoIncrement = false;
                colvarGroupId.IsNullable = false;
                colvarGroupId.IsPrimaryKey = false;
                colvarGroupId.IsForeignKey = false;
                colvarGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupId);
                
                TableSchema.TableColumn colvarDepositService = new TableSchema.TableColumn(schema);
                colvarDepositService.ColumnName = "deposit_service";
                colvarDepositService.DataType = DbType.Int32;
                colvarDepositService.MaxLength = 0;
                colvarDepositService.AutoIncrement = false;
                colvarDepositService.IsNullable = false;
                colvarDepositService.IsPrimaryKey = false;
                colvarDepositService.IsForeignKey = false;
                colvarDepositService.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepositService);
                
                TableSchema.TableColumn colvarPointService = new TableSchema.TableColumn(schema);
                colvarPointService.ColumnName = "point_service";
                colvarPointService.DataType = DbType.Int32;
                colvarPointService.MaxLength = 0;
                colvarPointService.AutoIncrement = false;
                colvarPointService.IsNullable = false;
                colvarPointService.IsPrimaryKey = false;
                colvarPointService.IsForeignKey = false;
                colvarPointService.IsReadOnly = false;
                
                schema.Columns.Add(colvarPointService);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_membership_card_available_service",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMembershipCardAvailableService()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMembershipCardAvailableService(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMembershipCardAvailableService(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMembershipCardAvailableService(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("GroupId")]
        [Bindable(true)]
        public int GroupId 
	    {
		    get
		    {
			    return GetColumnValue<int>("group_id");
		    }
            set 
		    {
			    SetColumnValue("group_id", value);
            }
        }
	      
        [XmlAttribute("DepositService")]
        [Bindable(true)]
        public int DepositService 
	    {
		    get
		    {
			    return GetColumnValue<int>("deposit_service");
		    }
            set 
		    {
			    SetColumnValue("deposit_service", value);
            }
        }
	      
        [XmlAttribute("PointService")]
        [Bindable(true)]
        public int PointService 
	    {
		    get
		    {
			    return GetColumnValue<int>("point_service");
		    }
            set 
		    {
			    SetColumnValue("point_service", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string GroupId = @"group_id";
            
            public static string DepositService = @"deposit_service";
            
            public static string PointService = @"point_service";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

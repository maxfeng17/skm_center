﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the SkmBeaconMessage class.
    /// </summary>
    [Serializable]
    public partial class SkmBeaconMessageCollection : RepositoryList<SkmBeaconMessage, SkmBeaconMessageCollection>
    {
        public SkmBeaconMessageCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmBeaconMessageCollection</returns>
        public SkmBeaconMessageCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmBeaconMessage o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the skm_beacon_message table.
    /// </summary>
    [Serializable]
    public partial class SkmBeaconMessage : RepositoryRecord<SkmBeaconMessage>, IRecordBase
    {
        #region .ctors and Default Settings

        public SkmBeaconMessage()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SkmBeaconMessage(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("skm_beacon_message", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarSubject = new TableSchema.TableColumn(schema);
                colvarSubject.ColumnName = "subject";
                colvarSubject.DataType = DbType.String;
                colvarSubject.MaxLength = 500;
                colvarSubject.AutoIncrement = false;
                colvarSubject.IsNullable = false;
                colvarSubject.IsPrimaryKey = false;
                colvarSubject.IsForeignKey = false;
                colvarSubject.IsReadOnly = false;
                colvarSubject.DefaultSetting = @"";
                colvarSubject.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSubject);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarEffectiveStart = new TableSchema.TableColumn(schema);
                colvarEffectiveStart.ColumnName = "effective_start";
                colvarEffectiveStart.DataType = DbType.DateTime;
                colvarEffectiveStart.MaxLength = 0;
                colvarEffectiveStart.AutoIncrement = false;
                colvarEffectiveStart.IsNullable = false;
                colvarEffectiveStart.IsPrimaryKey = false;
                colvarEffectiveStart.IsForeignKey = false;
                colvarEffectiveStart.IsReadOnly = false;
                colvarEffectiveStart.DefaultSetting = @"";
                colvarEffectiveStart.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEffectiveStart);

                TableSchema.TableColumn colvarEffectiveEnd = new TableSchema.TableColumn(schema);
                colvarEffectiveEnd.ColumnName = "effective_end";
                colvarEffectiveEnd.DataType = DbType.DateTime;
                colvarEffectiveEnd.MaxLength = 0;
                colvarEffectiveEnd.AutoIncrement = false;
                colvarEffectiveEnd.IsNullable = false;
                colvarEffectiveEnd.IsPrimaryKey = false;
                colvarEffectiveEnd.IsForeignKey = false;
                colvarEffectiveEnd.IsReadOnly = false;
                colvarEffectiveEnd.DefaultSetting = @"";
                colvarEffectiveEnd.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEffectiveEnd);

                TableSchema.TableColumn colvarTimeStart = new TableSchema.TableColumn(schema);
                colvarTimeStart.ColumnName = "time_start";
                colvarTimeStart.DataType = DbType.AnsiString;
                colvarTimeStart.MaxLength = 5;
                colvarTimeStart.AutoIncrement = false;
                colvarTimeStart.IsNullable = true;
                colvarTimeStart.IsPrimaryKey = false;
                colvarTimeStart.IsForeignKey = false;
                colvarTimeStart.IsReadOnly = false;
                colvarTimeStart.DefaultSetting = @"";
                colvarTimeStart.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTimeStart);

                TableSchema.TableColumn colvarTimeEnd = new TableSchema.TableColumn(schema);
                colvarTimeEnd.ColumnName = "time_end";
                colvarTimeEnd.DataType = DbType.AnsiString;
                colvarTimeEnd.MaxLength = 5;
                colvarTimeEnd.AutoIncrement = false;
                colvarTimeEnd.IsNullable = true;
                colvarTimeEnd.IsPrimaryKey = false;
                colvarTimeEnd.IsForeignKey = false;
                colvarTimeEnd.IsReadOnly = false;
                colvarTimeEnd.DefaultSetting = @"";
                colvarTimeEnd.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTimeEnd);

                TableSchema.TableColumn colvarIsTop = new TableSchema.TableColumn(schema);
                colvarIsTop.ColumnName = "is_top";
                colvarIsTop.DataType = DbType.Boolean;
                colvarIsTop.MaxLength = 0;
                colvarIsTop.AutoIncrement = false;
                colvarIsTop.IsNullable = false;
                colvarIsTop.IsPrimaryKey = false;
                colvarIsTop.IsForeignKey = false;
                colvarIsTop.IsReadOnly = false;
                colvarIsTop.DefaultSetting = @"";
                colvarIsTop.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsTop);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarFloor = new TableSchema.TableColumn(schema);
                colvarFloor.ColumnName = "floor";
                colvarFloor.DataType = DbType.AnsiString;
                colvarFloor.MaxLength = 50;
                colvarFloor.AutoIncrement = false;
                colvarFloor.IsNullable = true;
                colvarFloor.IsPrimaryKey = false;
                colvarFloor.IsForeignKey = false;
                colvarFloor.IsReadOnly = false;
                colvarFloor.DefaultSetting = @"";
                colvarFloor.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFloor);

                TableSchema.TableColumn colvarBeacon = new TableSchema.TableColumn(schema);
                colvarBeacon.ColumnName = "beacon";
                colvarBeacon.DataType = DbType.AnsiString;
                colvarBeacon.MaxLength = -1;
                colvarBeacon.AutoIncrement = false;
                colvarBeacon.IsNullable = true;
                colvarBeacon.IsPrimaryKey = false;
                colvarBeacon.IsForeignKey = false;
                colvarBeacon.IsReadOnly = false;
                colvarBeacon.DefaultSetting = @"";
                colvarBeacon.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBeacon);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateUser = new TableSchema.TableColumn(schema);
                colvarCreateUser.ColumnName = "create_user";
                colvarCreateUser.DataType = DbType.AnsiString;
                colvarCreateUser.MaxLength = 50;
                colvarCreateUser.AutoIncrement = false;
                colvarCreateUser.IsNullable = true;
                colvarCreateUser.IsPrimaryKey = false;
                colvarCreateUser.IsForeignKey = false;
                colvarCreateUser.IsReadOnly = false;
                colvarCreateUser.DefaultSetting = @"";
                colvarCreateUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateUser);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarModifyUser = new TableSchema.TableColumn(schema);
                colvarModifyUser.ColumnName = "modify_user";
                colvarModifyUser.DataType = DbType.AnsiString;
                colvarModifyUser.MaxLength = 50;
                colvarModifyUser.AutoIncrement = false;
                colvarModifyUser.IsNullable = true;
                colvarModifyUser.IsPrimaryKey = false;
                colvarModifyUser.IsForeignKey = false;
                colvarModifyUser.IsReadOnly = false;
                colvarModifyUser.DefaultSetting = @"";
                colvarModifyUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyUser);

                TableSchema.TableColumn colvarShopCode = new TableSchema.TableColumn(schema);
                colvarShopCode.ColumnName = "shop_code";
                colvarShopCode.DataType = DbType.AnsiString;
                colvarShopCode.MaxLength = 4;
                colvarShopCode.AutoIncrement = false;
                colvarShopCode.IsNullable = true;
                colvarShopCode.IsPrimaryKey = false;
                colvarShopCode.IsForeignKey = false;
                colvarShopCode.IsReadOnly = false;
                colvarShopCode.DefaultSetting = @"";
                colvarShopCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShopCode);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = true;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarBeaconType = new TableSchema.TableColumn(schema);
                colvarBeaconType.ColumnName = "beacon_type";
                colvarBeaconType.DataType = DbType.Int32;
                colvarBeaconType.MaxLength = 0;
                colvarBeaconType.AutoIncrement = false;
                colvarBeaconType.IsNullable = false;
                colvarBeaconType.IsPrimaryKey = false;
                colvarBeaconType.IsForeignKey = false;
                colvarBeaconType.IsReadOnly = false;

                colvarBeaconType.DefaultSetting = @"((0))";
                colvarBeaconType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBeaconType);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("skm_beacon_message", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Subject")]
        [Bindable(true)]
        public string Subject
        {
            get { return GetColumnValue<string>(Columns.Subject); }
            set { SetColumnValue(Columns.Subject, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid
        {
            get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        [XmlAttribute("EffectiveStart")]
        [Bindable(true)]
        public DateTime EffectiveStart
        {
            get { return GetColumnValue<DateTime>(Columns.EffectiveStart); }
            set { SetColumnValue(Columns.EffectiveStart, value); }
        }

        [XmlAttribute("EffectiveEnd")]
        [Bindable(true)]
        public DateTime EffectiveEnd
        {
            get { return GetColumnValue<DateTime>(Columns.EffectiveEnd); }
            set { SetColumnValue(Columns.EffectiveEnd, value); }
        }

        [XmlAttribute("TimeStart")]
        [Bindable(true)]
        public string TimeStart
        {
            get { return GetColumnValue<string>(Columns.TimeStart); }
            set { SetColumnValue(Columns.TimeStart, value); }
        }

        [XmlAttribute("TimeEnd")]
        [Bindable(true)]
        public string TimeEnd
        {
            get { return GetColumnValue<string>(Columns.TimeEnd); }
            set { SetColumnValue(Columns.TimeEnd, value); }
        }

        [XmlAttribute("IsTop")]
        [Bindable(true)]
        public bool IsTop
        {
            get { return GetColumnValue<bool>(Columns.IsTop); }
            set { SetColumnValue(Columns.IsTop, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("Floor")]
        [Bindable(true)]
        public string Floor
        {
            get { return GetColumnValue<string>(Columns.Floor); }
            set { SetColumnValue(Columns.Floor, value); }
        }

        [XmlAttribute("Beacon")]
        [Bindable(true)]
        public string Beacon
        {
            get { return GetColumnValue<string>(Columns.Beacon); }
            set { SetColumnValue(Columns.Beacon, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateUser")]
        [Bindable(true)]
        public string CreateUser
        {
            get { return GetColumnValue<string>(Columns.CreateUser); }
            set { SetColumnValue(Columns.CreateUser, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ModifyUser")]
        [Bindable(true)]
        public string ModifyUser
        {
            get { return GetColumnValue<string>(Columns.ModifyUser); }
            set { SetColumnValue(Columns.ModifyUser, value); }
        }

        [XmlAttribute("ShopCode")]
        [Bindable(true)]
        public string ShopCode
        {
            get { return GetColumnValue<string>(Columns.ShopCode); }
            set { SetColumnValue(Columns.ShopCode, value); }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid? Guid
        {
            get { return GetColumnValue<Guid?>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("BeaconType")]
        [Bindable(true)]
        public int BeaconType
        {
            get { return GetColumnValue<int>(Columns.BeaconType); }
            set { SetColumnValue(Columns.BeaconType, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn SubjectColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn EffectiveStartColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn EffectiveEndColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn TimeStartColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn TimeEndColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn IsTopColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn FloorColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn BeaconColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn CreateUserColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn ModifyUserColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn ShopCodeColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn BeaconTypeColumn
        {
            get { return Schema.Columns[17]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Subject = @"subject";
            public static string BusinessHourGuid = @"business_hour_guid";
            public static string EffectiveStart = @"effective_start";
            public static string EffectiveEnd = @"effective_end";
            public static string TimeStart = @"time_start";
            public static string TimeEnd = @"time_end";
            public static string IsTop = @"is_top";
            public static string Status = @"status";
            public static string Floor = @"floor";
            public static string Beacon = @"beacon";
            public static string CreateTime = @"create_time";
            public static string CreateUser = @"create_user";
            public static string ModifyTime = @"modify_time";
            public static string ModifyUser = @"modify_user";
            public static string ShopCode = @"shop_code";
            public static string Guid = @"guid";
            public static string BeaconType = @"beacon_type";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

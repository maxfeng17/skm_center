using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the FamiApiLog class.
	/// </summary>
    [Serializable]
	public partial class FamiApiLogCollection : RepositoryList<FamiApiLog, FamiApiLogCollection>
	{	   
		public FamiApiLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FamiApiLogCollection</returns>
		public FamiApiLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                FamiApiLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the fami_api_log table.
	/// </summary>
	[Serializable]
	public partial class FamiApiLog : RepositoryRecord<FamiApiLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public FamiApiLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public FamiApiLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("fami_api_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTranNo = new TableSchema.TableColumn(schema);
				colvarTranNo.ColumnName = "tran_no";
				colvarTranNo.DataType = DbType.AnsiString;
				colvarTranNo.MaxLength = 20;
				colvarTranNo.AutoIncrement = false;
				colvarTranNo.IsNullable = false;
				colvarTranNo.IsPrimaryKey = false;
				colvarTranNo.IsForeignKey = false;
				colvarTranNo.IsReadOnly = false;
				colvarTranNo.DefaultSetting = @"";
				colvarTranNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTranNo);
				
				TableSchema.TableColumn colvarChannel = new TableSchema.TableColumn(schema);
				colvarChannel.ColumnName = "channel";
				colvarChannel.DataType = DbType.Byte;
				colvarChannel.MaxLength = 0;
				colvarChannel.AutoIncrement = false;
				colvarChannel.IsNullable = false;
				colvarChannel.IsPrimaryKey = false;
				colvarChannel.IsForeignKey = false;
				colvarChannel.IsReadOnly = false;
				
						colvarChannel.DefaultSetting = @"((0))";
				colvarChannel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChannel);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.AnsiString;
				colvarType.MaxLength = 50;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 150;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarLogMsg = new TableSchema.TableColumn(schema);
				colvarLogMsg.ColumnName = "log_msg";
				colvarLogMsg.DataType = DbType.String;
				colvarLogMsg.MaxLength = -1;
				colvarLogMsg.AutoIncrement = false;
				colvarLogMsg.IsNullable = true;
				colvarLogMsg.IsPrimaryKey = false;
				colvarLogMsg.IsForeignKey = false;
				colvarLogMsg.IsReadOnly = false;
				colvarLogMsg.DefaultSetting = @"";
				colvarLogMsg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogMsg);
				
				TableSchema.TableColumn colvarReturnCode = new TableSchema.TableColumn(schema);
				colvarReturnCode.ColumnName = "return_code";
				colvarReturnCode.DataType = DbType.AnsiString;
				colvarReturnCode.MaxLength = 50;
				colvarReturnCode.AutoIncrement = false;
				colvarReturnCode.IsNullable = true;
				colvarReturnCode.IsPrimaryKey = false;
				colvarReturnCode.IsForeignKey = false;
				colvarReturnCode.IsReadOnly = false;
				colvarReturnCode.DefaultSetting = @"";
				colvarReturnCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnCode);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("fami_api_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TranNo")]
		[Bindable(true)]
		public string TranNo 
		{
			get { return GetColumnValue<string>(Columns.TranNo); }
			set { SetColumnValue(Columns.TranNo, value); }
		}
		  
		[XmlAttribute("Channel")]
		[Bindable(true)]
		public byte Channel 
		{
			get { return GetColumnValue<byte>(Columns.Channel); }
			set { SetColumnValue(Columns.Channel, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public string Type 
		{
			get { return GetColumnValue<string>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("LogMsg")]
		[Bindable(true)]
		public string LogMsg 
		{
			get { return GetColumnValue<string>(Columns.LogMsg); }
			set { SetColumnValue(Columns.LogMsg, value); }
		}
		  
		[XmlAttribute("ReturnCode")]
		[Bindable(true)]
		public string ReturnCode 
		{
			get { return GetColumnValue<string>(Columns.ReturnCode); }
			set { SetColumnValue(Columns.ReturnCode, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TranNoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ChannelColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn LogMsgColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnCodeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TranNo = @"tran_no";
			 public static string Channel = @"channel";
			 public static string Type = @"type";
			 public static string CreateTime = @"create_time";
			 public static string Message = @"message";
			 public static string LogMsg = @"log_msg";
			 public static string ReturnCode = @"return_code";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the PromoSearchKeyRecord class.
    /// </summary>
    [Serializable]
    public partial class PromoSearchKeyRecordCollection : RepositoryList<PromoSearchKeyRecord, PromoSearchKeyRecordCollection>
    {
        public PromoSearchKeyRecordCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PromoSearchKeyRecordCollection</returns>
        public PromoSearchKeyRecordCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PromoSearchKeyRecord o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the promo_search_key_record table.
    /// </summary>
    [Serializable]
    public partial class PromoSearchKeyRecord : RepositoryRecord<PromoSearchKeyRecord>, IRecordBase
    {
        #region .ctors and Default Settings

        public PromoSearchKeyRecord()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public PromoSearchKeyRecord(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("promo_search_key_record", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarSearchKeyId = new TableSchema.TableColumn(schema);
                colvarSearchKeyId.ColumnName = "search_key_id";
                colvarSearchKeyId.DataType = DbType.Int32;
                colvarSearchKeyId.MaxLength = 0;
                colvarSearchKeyId.AutoIncrement = false;
                colvarSearchKeyId.IsNullable = false;
                colvarSearchKeyId.IsPrimaryKey = false;
                colvarSearchKeyId.IsForeignKey = false;
                colvarSearchKeyId.IsReadOnly = false;
                colvarSearchKeyId.DefaultSetting = @"";
                colvarSearchKeyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSearchKeyId);

                TableSchema.TableColumn colvarMemberUserId = new TableSchema.TableColumn(schema);
                colvarMemberUserId.ColumnName = "member_user_id";
                colvarMemberUserId.DataType = DbType.Int32;
                colvarMemberUserId.MaxLength = 0;
                colvarMemberUserId.AutoIncrement = false;
                colvarMemberUserId.IsNullable = true;
                colvarMemberUserId.IsPrimaryKey = false;
                colvarMemberUserId.IsForeignKey = false;
                colvarMemberUserId.IsReadOnly = false;
                colvarMemberUserId.DefaultSetting = @"";
                colvarMemberUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMemberUserId);

                TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
                colvarMemo.ColumnName = "memo";
                colvarMemo.DataType = DbType.String;
                colvarMemo.MaxLength = 100;
                colvarMemo.AutoIncrement = false;
                colvarMemo.IsNullable = true;
                colvarMemo.IsPrimaryKey = false;
                colvarMemo.IsForeignKey = false;
                colvarMemo.IsReadOnly = false;
                colvarMemo.DefaultSetting = @"";
                colvarMemo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMemo);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("promo_search_key_record", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("SearchKeyId")]
        [Bindable(true)]
        public int SearchKeyId
        {
            get { return GetColumnValue<int>(Columns.SearchKeyId); }
            set { SetColumnValue(Columns.SearchKeyId, value); }
        }

        [XmlAttribute("MemberUserId")]
        [Bindable(true)]
        public int? MemberUserId
        {
            get { return GetColumnValue<int?>(Columns.MemberUserId); }
            set { SetColumnValue(Columns.MemberUserId, value); }
        }

        [XmlAttribute("Memo")]
        [Bindable(true)]
        public string Memo
        {
            get { return GetColumnValue<string>(Columns.Memo); }
            set { SetColumnValue(Columns.Memo, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn SearchKeyIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn MemberUserIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string SearchKeyId = @"search_key_id";
            public static string MemberUserId = @"member_user_id";
            public static string Memo = @"memo";
            public static string CreateTime = @"create_time";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

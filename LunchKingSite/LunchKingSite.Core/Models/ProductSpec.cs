﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProductSpec class.
    /// </summary>
    [Serializable]
    public partial class ProductSpecCollection : RepositoryList<ProductSpec, ProductSpecCollection>
    {
        public ProductSpecCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProductSpecCollection</returns>
        public ProductSpecCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProductSpec o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the product_spec table.
    /// </summary>
    [Serializable]
    public partial class ProductSpec : RepositoryRecord<ProductSpec>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProductSpec()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProductSpec(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("product_spec", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_guid";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = false;
                colvarItemGuid.IsPrimaryKey = false;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;
                colvarItemGuid.DefaultSetting = @"";
                colvarItemGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemGuid);

                TableSchema.TableColumn colvarCatgName = new TableSchema.TableColumn(schema);
                colvarCatgName.ColumnName = "catg_name";
                colvarCatgName.DataType = DbType.String;
                colvarCatgName.MaxLength = 50;
                colvarCatgName.AutoIncrement = false;
                colvarCatgName.IsNullable = false;
                colvarCatgName.IsPrimaryKey = false;
                colvarCatgName.IsForeignKey = false;
                colvarCatgName.IsReadOnly = false;
                colvarCatgName.DefaultSetting = @"";
                colvarCatgName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCatgName);

                TableSchema.TableColumn colvarSpecName = new TableSchema.TableColumn(schema);
                colvarSpecName.ColumnName = "spec_name";
                colvarSpecName.DataType = DbType.String;
                colvarSpecName.MaxLength = 100;
                colvarSpecName.AutoIncrement = false;
                colvarSpecName.IsNullable = false;
                colvarSpecName.IsPrimaryKey = false;
                colvarSpecName.IsForeignKey = false;
                colvarSpecName.IsReadOnly = false;
                colvarSpecName.DefaultSetting = @"";
                colvarSpecName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSpecName);

                TableSchema.TableColumn colvarSort = new TableSchema.TableColumn(schema);
                colvarSort.ColumnName = "sort";
                colvarSort.DataType = DbType.Int32;
                colvarSort.MaxLength = 0;
                colvarSort.AutoIncrement = false;
                colvarSort.IsNullable = false;
                colvarSort.IsPrimaryKey = false;
                colvarSort.IsForeignKey = false;
                colvarSort.IsReadOnly = false;
                colvarSort.DefaultSetting = @"";
                colvarSort.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSort);

                TableSchema.TableColumn colvarSpecStatus = new TableSchema.TableColumn(schema);
                colvarSpecStatus.ColumnName = "spec_status";
                colvarSpecStatus.DataType = DbType.Int32;
                colvarSpecStatus.MaxLength = 0;
                colvarSpecStatus.AutoIncrement = false;
                colvarSpecStatus.IsNullable = false;
                colvarSpecStatus.IsPrimaryKey = false;
                colvarSpecStatus.IsForeignKey = false;
                colvarSpecStatus.IsReadOnly = false;
                colvarSpecStatus.DefaultSetting = @"";
                colvarSpecStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSpecStatus);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.AnsiString;
                colvarModifyId.MaxLength = 50;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("product_spec", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid ItemGuid
        {
            get { return GetColumnValue<Guid>(Columns.ItemGuid); }
            set { SetColumnValue(Columns.ItemGuid, value); }
        }

        [XmlAttribute("CatgName")]
        [Bindable(true)]
        public string CatgName
        {
            get { return GetColumnValue<string>(Columns.CatgName); }
            set { SetColumnValue(Columns.CatgName, value); }
        }

        [XmlAttribute("SpecName")]
        [Bindable(true)]
        public string SpecName
        {
            get { return GetColumnValue<string>(Columns.SpecName); }
            set { SetColumnValue(Columns.SpecName, value); }
        }

        [XmlAttribute("Sort")]
        [Bindable(true)]
        public int Sort
        {
            get { return GetColumnValue<int>(Columns.Sort); }
            set { SetColumnValue(Columns.Sort, value); }
        }

        [XmlAttribute("SpecStatus")]
        [Bindable(true)]
        public int SpecStatus
        {
            get { return GetColumnValue<int>(Columns.SpecStatus); }
            set { SetColumnValue(Columns.SpecStatus, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ItemGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn CatgNameColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn SpecNameColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn SortColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn SpecStatusColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[9]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"guid";
            public static string ItemGuid = @"item_guid";
            public static string CatgName = @"catg_name";
            public static string SpecName = @"spec_name";
            public static string Sort = @"sort";
            public static string SpecStatus = @"spec_status";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string ModifyTime = @"modify_time";
            public static string ModifyId = @"modify_id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

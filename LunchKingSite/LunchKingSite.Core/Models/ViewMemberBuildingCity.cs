using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMemberBuildingCity class.
    /// </summary>
    [Serializable]
    public partial class ViewMemberBuildingCityCollection : ReadOnlyList<ViewMemberBuildingCity, ViewMemberBuildingCityCollection>
    {        
        public ViewMemberBuildingCityCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_member_building_city view.
    /// </summary>
    [Serializable]
    public partial class ViewMemberBuildingCity : ReadOnlyRecord<ViewMemberBuildingCity>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_member_building_city", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
                colvarLastName.ColumnName = "last_name";
                colvarLastName.DataType = DbType.String;
                colvarLastName.MaxLength = 50;
                colvarLastName.AutoIncrement = false;
                colvarLastName.IsNullable = true;
                colvarLastName.IsPrimaryKey = false;
                colvarLastName.IsForeignKey = false;
                colvarLastName.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastName);
                
                TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
                colvarFirstName.ColumnName = "first_name";
                colvarFirstName.DataType = DbType.String;
                colvarFirstName.MaxLength = 50;
                colvarFirstName.AutoIncrement = false;
                colvarFirstName.IsNullable = true;
                colvarFirstName.IsPrimaryKey = false;
                colvarFirstName.IsForeignKey = false;
                colvarFirstName.IsReadOnly = false;
                
                schema.Columns.Add(colvarFirstName);
                
                TableSchema.TableColumn colvarBuildingGuid = new TableSchema.TableColumn(schema);
                colvarBuildingGuid.ColumnName = "building_GUID";
                colvarBuildingGuid.DataType = DbType.Guid;
                colvarBuildingGuid.MaxLength = 0;
                colvarBuildingGuid.AutoIncrement = false;
                colvarBuildingGuid.IsNullable = true;
                colvarBuildingGuid.IsPrimaryKey = false;
                colvarBuildingGuid.IsForeignKey = false;
                colvarBuildingGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingGuid);
                
                TableSchema.TableColumn colvarBirthday = new TableSchema.TableColumn(schema);
                colvarBirthday.ColumnName = "birthday";
                colvarBirthday.DataType = DbType.DateTime;
                colvarBirthday.MaxLength = 0;
                colvarBirthday.AutoIncrement = false;
                colvarBirthday.IsNullable = true;
                colvarBirthday.IsPrimaryKey = false;
                colvarBirthday.IsForeignKey = false;
                colvarBirthday.IsReadOnly = false;
                
                schema.Columns.Add(colvarBirthday);
                
                TableSchema.TableColumn colvarGender = new TableSchema.TableColumn(schema);
                colvarGender.ColumnName = "gender";
                colvarGender.DataType = DbType.Int32;
                colvarGender.MaxLength = 0;
                colvarGender.AutoIncrement = false;
                colvarGender.IsNullable = true;
                colvarGender.IsPrimaryKey = false;
                colvarGender.IsForeignKey = false;
                colvarGender.IsReadOnly = false;
                
                schema.Columns.Add(colvarGender);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "company_name";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 50;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyName);
                
                TableSchema.TableColumn colvarCompanyDepartment = new TableSchema.TableColumn(schema);
                colvarCompanyDepartment.ColumnName = "company_department";
                colvarCompanyDepartment.DataType = DbType.String;
                colvarCompanyDepartment.MaxLength = 50;
                colvarCompanyDepartment.AutoIncrement = false;
                colvarCompanyDepartment.IsNullable = true;
                colvarCompanyDepartment.IsPrimaryKey = false;
                colvarCompanyDepartment.IsForeignKey = false;
                colvarCompanyDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyDepartment);
                
                TableSchema.TableColumn colvarCompanyTel = new TableSchema.TableColumn(schema);
                colvarCompanyTel.ColumnName = "company_tel";
                colvarCompanyTel.DataType = DbType.AnsiString;
                colvarCompanyTel.MaxLength = 20;
                colvarCompanyTel.AutoIncrement = false;
                colvarCompanyTel.IsNullable = true;
                colvarCompanyTel.IsPrimaryKey = false;
                colvarCompanyTel.IsForeignKey = false;
                colvarCompanyTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyTel);
                
                TableSchema.TableColumn colvarCompanyTelExt = new TableSchema.TableColumn(schema);
                colvarCompanyTelExt.ColumnName = "company_tel_ext";
                colvarCompanyTelExt.DataType = DbType.AnsiString;
                colvarCompanyTelExt.MaxLength = 50;
                colvarCompanyTelExt.AutoIncrement = false;
                colvarCompanyTelExt.IsNullable = true;
                colvarCompanyTelExt.IsPrimaryKey = false;
                colvarCompanyTelExt.IsForeignKey = false;
                colvarCompanyTelExt.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyTelExt);
                
                TableSchema.TableColumn colvarCompanyAddress = new TableSchema.TableColumn(schema);
                colvarCompanyAddress.ColumnName = "company_address";
                colvarCompanyAddress.DataType = DbType.String;
                colvarCompanyAddress.MaxLength = 100;
                colvarCompanyAddress.AutoIncrement = false;
                colvarCompanyAddress.IsNullable = true;
                colvarCompanyAddress.IsPrimaryKey = false;
                colvarCompanyAddress.IsForeignKey = false;
                colvarCompanyAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAddress);
                
                TableSchema.TableColumn colvarDeliveryMethod = new TableSchema.TableColumn(schema);
                colvarDeliveryMethod.ColumnName = "delivery_method";
                colvarDeliveryMethod.DataType = DbType.String;
                colvarDeliveryMethod.MaxLength = 200;
                colvarDeliveryMethod.AutoIncrement = false;
                colvarDeliveryMethod.IsNullable = true;
                colvarDeliveryMethod.IsPrimaryKey = false;
                colvarDeliveryMethod.IsForeignKey = false;
                colvarDeliveryMethod.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryMethod);
                
                TableSchema.TableColumn colvarPrimaryContactMethod = new TableSchema.TableColumn(schema);
                colvarPrimaryContactMethod.ColumnName = "primary_contact_method";
                colvarPrimaryContactMethod.DataType = DbType.Int32;
                colvarPrimaryContactMethod.MaxLength = 0;
                colvarPrimaryContactMethod.AutoIncrement = false;
                colvarPrimaryContactMethod.IsNullable = false;
                colvarPrimaryContactMethod.IsPrimaryKey = false;
                colvarPrimaryContactMethod.IsForeignKey = false;
                colvarPrimaryContactMethod.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrimaryContactMethod);
                
                TableSchema.TableColumn colvarMemberStatus = new TableSchema.TableColumn(schema);
                colvarMemberStatus.ColumnName = "member_status";
                colvarMemberStatus.DataType = DbType.Int32;
                colvarMemberStatus.MaxLength = 0;
                colvarMemberStatus.AutoIncrement = false;
                colvarMemberStatus.IsNullable = false;
                colvarMemberStatus.IsPrimaryKey = false;
                colvarMemberStatus.IsForeignKey = false;
                colvarMemberStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberStatus);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 256;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 100;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = true;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarUserEmail = new TableSchema.TableColumn(schema);
                colvarUserEmail.ColumnName = "user_email";
                colvarUserEmail.DataType = DbType.String;
                colvarUserEmail.MaxLength = 256;
                colvarUserEmail.AutoIncrement = false;
                colvarUserEmail.IsNullable = false;
                colvarUserEmail.IsPrimaryKey = false;
                colvarUserEmail.IsForeignKey = false;
                colvarUserEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserEmail);
                
                TableSchema.TableColumn colvarIsGuest = new TableSchema.TableColumn(schema);
                colvarIsGuest.ColumnName = "is_guest";
                colvarIsGuest.DataType = DbType.Boolean;
                colvarIsGuest.MaxLength = 0;
                colvarIsGuest.AutoIncrement = false;
                colvarIsGuest.IsNullable = false;
                colvarIsGuest.IsPrimaryKey = false;
                colvarIsGuest.IsForeignKey = false;
                colvarIsGuest.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsGuest);
                
                TableSchema.TableColumn colvarBuildingId = new TableSchema.TableColumn(schema);
                colvarBuildingId.ColumnName = "building_id";
                colvarBuildingId.DataType = DbType.AnsiString;
                colvarBuildingId.MaxLength = 20;
                colvarBuildingId.AutoIncrement = false;
                colvarBuildingId.IsNullable = true;
                colvarBuildingId.IsPrimaryKey = false;
                colvarBuildingId.IsForeignKey = false;
                colvarBuildingId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingId);
                
                TableSchema.TableColumn colvarBuildingName = new TableSchema.TableColumn(schema);
                colvarBuildingName.ColumnName = "building_name";
                colvarBuildingName.DataType = DbType.String;
                colvarBuildingName.MaxLength = 50;
                colvarBuildingName.AutoIncrement = false;
                colvarBuildingName.IsNullable = true;
                colvarBuildingName.IsPrimaryKey = false;
                colvarBuildingName.IsForeignKey = false;
                colvarBuildingName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingName);
                
                TableSchema.TableColumn colvarBuildingStreetName = new TableSchema.TableColumn(schema);
                colvarBuildingStreetName.ColumnName = "building_street_name";
                colvarBuildingStreetName.DataType = DbType.String;
                colvarBuildingStreetName.MaxLength = 100;
                colvarBuildingStreetName.AutoIncrement = false;
                colvarBuildingStreetName.IsNullable = true;
                colvarBuildingStreetName.IsPrimaryKey = false;
                colvarBuildingStreetName.IsForeignKey = false;
                colvarBuildingStreetName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingStreetName);
                
                TableSchema.TableColumn colvarBuildingAddressNumber = new TableSchema.TableColumn(schema);
                colvarBuildingAddressNumber.ColumnName = "building_address_number";
                colvarBuildingAddressNumber.DataType = DbType.String;
                colvarBuildingAddressNumber.MaxLength = 100;
                colvarBuildingAddressNumber.AutoIncrement = false;
                colvarBuildingAddressNumber.IsNullable = true;
                colvarBuildingAddressNumber.IsPrimaryKey = false;
                colvarBuildingAddressNumber.IsForeignKey = false;
                colvarBuildingAddressNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingAddressNumber);
                
                TableSchema.TableColumn colvarBuildingAddress = new TableSchema.TableColumn(schema);
                colvarBuildingAddress.ColumnName = "building_address";
                colvarBuildingAddress.DataType = DbType.String;
                colvarBuildingAddress.MaxLength = 200;
                colvarBuildingAddress.AutoIncrement = false;
                colvarBuildingAddress.IsNullable = true;
                colvarBuildingAddress.IsPrimaryKey = false;
                colvarBuildingAddress.IsForeignKey = false;
                colvarBuildingAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingAddress);
                
                TableSchema.TableColumn colvarBuildingOnline = new TableSchema.TableColumn(schema);
                colvarBuildingOnline.ColumnName = "building_online";
                colvarBuildingOnline.DataType = DbType.Boolean;
                colvarBuildingOnline.MaxLength = 0;
                colvarBuildingOnline.AutoIncrement = false;
                colvarBuildingOnline.IsNullable = true;
                colvarBuildingOnline.IsPrimaryKey = false;
                colvarBuildingOnline.IsForeignKey = false;
                colvarBuildingOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingOnline);
                
                TableSchema.TableColumn colvarBuildingRank = new TableSchema.TableColumn(schema);
                colvarBuildingRank.ColumnName = "building_rank";
                colvarBuildingRank.DataType = DbType.Int32;
                colvarBuildingRank.MaxLength = 0;
                colvarBuildingRank.AutoIncrement = false;
                colvarBuildingRank.IsNullable = true;
                colvarBuildingRank.IsPrimaryKey = false;
                colvarBuildingRank.IsForeignKey = false;
                colvarBuildingRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingRank);
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 20;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = true;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 10;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarComment = new TableSchema.TableColumn(schema);
                colvarComment.ColumnName = "comment";
                colvarComment.DataType = DbType.String;
                colvarComment.MaxLength = 100;
                colvarComment.AutoIncrement = false;
                colvarComment.IsNullable = true;
                colvarComment.IsPrimaryKey = false;
                colvarComment.IsForeignKey = false;
                colvarComment.IsReadOnly = false;
                
                schema.Columns.Add(colvarComment);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = true;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarCityStatus = new TableSchema.TableColumn(schema);
                colvarCityStatus.ColumnName = "city_status";
                colvarCityStatus.DataType = DbType.Int32;
                colvarCityStatus.MaxLength = 0;
                colvarCityStatus.AutoIncrement = false;
                colvarCityStatus.IsNullable = true;
                colvarCityStatus.IsPrimaryKey = false;
                colvarCityStatus.IsForeignKey = false;
                colvarCityStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityStatus);
                
                TableSchema.TableColumn colvarCityParentId = new TableSchema.TableColumn(schema);
                colvarCityParentId.ColumnName = "city_parent_id";
                colvarCityParentId.DataType = DbType.Int32;
                colvarCityParentId.MaxLength = 0;
                colvarCityParentId.AutoIncrement = false;
                colvarCityParentId.IsNullable = true;
                colvarCityParentId.IsPrimaryKey = false;
                colvarCityParentId.IsForeignKey = false;
                colvarCityParentId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityParentId);
                
                TableSchema.TableColumn colvarCityRank = new TableSchema.TableColumn(schema);
                colvarCityRank.ColumnName = "city_rank";
                colvarCityRank.DataType = DbType.Int32;
                colvarCityRank.MaxLength = 0;
                colvarCityRank.AutoIncrement = false;
                colvarCityRank.IsNullable = true;
                colvarCityRank.IsPrimaryKey = false;
                colvarCityRank.IsForeignKey = false;
                colvarCityRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityRank);
                
                TableSchema.TableColumn colvarZipCode = new TableSchema.TableColumn(schema);
                colvarZipCode.ColumnName = "zip_code";
                colvarZipCode.DataType = DbType.AnsiString;
                colvarZipCode.MaxLength = 5;
                colvarZipCode.AutoIncrement = false;
                colvarZipCode.IsNullable = true;
                colvarZipCode.IsPrimaryKey = false;
                colvarZipCode.IsForeignKey = false;
                colvarZipCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarZipCode);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarCarrierType = new TableSchema.TableColumn(schema);
                colvarCarrierType.ColumnName = "carrier_type";
                colvarCarrierType.DataType = DbType.Int32;
                colvarCarrierType.MaxLength = 0;
                colvarCarrierType.AutoIncrement = false;
                colvarCarrierType.IsNullable = false;
                colvarCarrierType.IsPrimaryKey = false;
                colvarCarrierType.IsForeignKey = false;
                colvarCarrierType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCarrierType);
                
                TableSchema.TableColumn colvarCarrierId = new TableSchema.TableColumn(schema);
                colvarCarrierId.ColumnName = "carrier_id";
                colvarCarrierId.DataType = DbType.String;
                colvarCarrierId.MaxLength = 64;
                colvarCarrierId.AutoIncrement = false;
                colvarCarrierId.IsNullable = true;
                colvarCarrierId.IsPrimaryKey = false;
                colvarCarrierId.IsForeignKey = false;
                colvarCarrierId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCarrierId);
                
                TableSchema.TableColumn colvarInvoiceBuyerName = new TableSchema.TableColumn(schema);
                colvarInvoiceBuyerName.ColumnName = "invoice_buyer_name";
                colvarInvoiceBuyerName.DataType = DbType.String;
                colvarInvoiceBuyerName.MaxLength = 50;
                colvarInvoiceBuyerName.AutoIncrement = false;
                colvarInvoiceBuyerName.IsNullable = true;
                colvarInvoiceBuyerName.IsPrimaryKey = false;
                colvarInvoiceBuyerName.IsForeignKey = false;
                colvarInvoiceBuyerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceBuyerName);
                
                TableSchema.TableColumn colvarInvoiceBuyerAddress = new TableSchema.TableColumn(schema);
                colvarInvoiceBuyerAddress.ColumnName = "invoice_buyer_address";
                colvarInvoiceBuyerAddress.DataType = DbType.String;
                colvarInvoiceBuyerAddress.MaxLength = 250;
                colvarInvoiceBuyerAddress.AutoIncrement = false;
                colvarInvoiceBuyerAddress.IsNullable = true;
                colvarInvoiceBuyerAddress.IsPrimaryKey = false;
                colvarInvoiceBuyerAddress.IsForeignKey = false;
                colvarInvoiceBuyerAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceBuyerAddress);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_member_building_city",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMemberBuildingCity()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMemberBuildingCity(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMemberBuildingCity(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMemberBuildingCity(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("LastName")]
        [Bindable(true)]
        public string LastName 
	    {
		    get
		    {
			    return GetColumnValue<string>("last_name");
		    }
            set 
		    {
			    SetColumnValue("last_name", value);
            }
        }
	      
        [XmlAttribute("FirstName")]
        [Bindable(true)]
        public string FirstName 
	    {
		    get
		    {
			    return GetColumnValue<string>("first_name");
		    }
            set 
		    {
			    SetColumnValue("first_name", value);
            }
        }
	      
        [XmlAttribute("BuildingGuid")]
        [Bindable(true)]
        public Guid? BuildingGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("building_GUID");
		    }
            set 
		    {
			    SetColumnValue("building_GUID", value);
            }
        }
	      
        [XmlAttribute("Birthday")]
        [Bindable(true)]
        public DateTime? Birthday 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("birthday");
		    }
            set 
		    {
			    SetColumnValue("birthday", value);
            }
        }
	      
        [XmlAttribute("Gender")]
        [Bindable(true)]
        public int? Gender 
	    {
		    get
		    {
			    return GetColumnValue<int?>("gender");
		    }
            set 
		    {
			    SetColumnValue("gender", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	      
        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_name");
		    }
            set 
		    {
			    SetColumnValue("company_name", value);
            }
        }
	      
        [XmlAttribute("CompanyDepartment")]
        [Bindable(true)]
        public string CompanyDepartment 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_department");
		    }
            set 
		    {
			    SetColumnValue("company_department", value);
            }
        }
	      
        [XmlAttribute("CompanyTel")]
        [Bindable(true)]
        public string CompanyTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_tel");
		    }
            set 
		    {
			    SetColumnValue("company_tel", value);
            }
        }
	      
        [XmlAttribute("CompanyTelExt")]
        [Bindable(true)]
        public string CompanyTelExt 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_tel_ext");
		    }
            set 
		    {
			    SetColumnValue("company_tel_ext", value);
            }
        }
	      
        [XmlAttribute("CompanyAddress")]
        [Bindable(true)]
        public string CompanyAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("company_address");
		    }
            set 
		    {
			    SetColumnValue("company_address", value);
            }
        }
	      
        [XmlAttribute("DeliveryMethod")]
        [Bindable(true)]
        public string DeliveryMethod 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_method");
		    }
            set 
		    {
			    SetColumnValue("delivery_method", value);
            }
        }
	      
        [XmlAttribute("PrimaryContactMethod")]
        [Bindable(true)]
        public int PrimaryContactMethod 
	    {
		    get
		    {
			    return GetColumnValue<int>("primary_contact_method");
		    }
            set 
		    {
			    SetColumnValue("primary_contact_method", value);
            }
        }
	      
        [XmlAttribute("MemberStatus")]
        [Bindable(true)]
        public int MemberStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("member_status");
		    }
            set 
		    {
			    SetColumnValue("member_status", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("UserEmail")]
        [Bindable(true)]
        public string UserEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_email");
		    }
            set 
		    {
			    SetColumnValue("user_email", value);
            }
        }
	      
        [XmlAttribute("IsGuest")]
        [Bindable(true)]
        public bool IsGuest 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_guest");
		    }
            set 
		    {
			    SetColumnValue("is_guest", value);
            }
        }
	      
        [XmlAttribute("BuildingId")]
        [Bindable(true)]
        public string BuildingId 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_id");
		    }
            set 
		    {
			    SetColumnValue("building_id", value);
            }
        }
	      
        [XmlAttribute("BuildingName")]
        [Bindable(true)]
        public string BuildingName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_name");
		    }
            set 
		    {
			    SetColumnValue("building_name", value);
            }
        }
	      
        [XmlAttribute("BuildingStreetName")]
        [Bindable(true)]
        public string BuildingStreetName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_street_name");
		    }
            set 
		    {
			    SetColumnValue("building_street_name", value);
            }
        }
	      
        [XmlAttribute("BuildingAddressNumber")]
        [Bindable(true)]
        public string BuildingAddressNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_address_number");
		    }
            set 
		    {
			    SetColumnValue("building_address_number", value);
            }
        }
	      
        [XmlAttribute("BuildingAddress")]
        [Bindable(true)]
        public string BuildingAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_address");
		    }
            set 
		    {
			    SetColumnValue("building_address", value);
            }
        }
	      
        [XmlAttribute("BuildingOnline")]
        [Bindable(true)]
        public bool? BuildingOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("building_online");
		    }
            set 
		    {
			    SetColumnValue("building_online", value);
            }
        }
	      
        [XmlAttribute("BuildingRank")]
        [Bindable(true)]
        public int? BuildingRank 
	    {
		    get
		    {
			    return GetColumnValue<int?>("building_rank");
		    }
            set 
		    {
			    SetColumnValue("building_rank", value);
            }
        }
	      
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("Comment")]
        [Bindable(true)]
        public string Comment 
	    {
		    get
		    {
			    return GetColumnValue<string>("comment");
		    }
            set 
		    {
			    SetColumnValue("comment", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int? CityId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("CityStatus")]
        [Bindable(true)]
        public int? CityStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_status");
		    }
            set 
		    {
			    SetColumnValue("city_status", value);
            }
        }
	      
        [XmlAttribute("CityParentId")]
        [Bindable(true)]
        public int? CityParentId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_parent_id");
		    }
            set 
		    {
			    SetColumnValue("city_parent_id", value);
            }
        }
	      
        [XmlAttribute("CityRank")]
        [Bindable(true)]
        public int? CityRank 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_rank");
		    }
            set 
		    {
			    SetColumnValue("city_rank", value);
            }
        }
	      
        [XmlAttribute("ZipCode")]
        [Bindable(true)]
        public string ZipCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("zip_code");
		    }
            set 
		    {
			    SetColumnValue("zip_code", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("CarrierType")]
        [Bindable(true)]
        public int CarrierType 
	    {
		    get
		    {
			    return GetColumnValue<int>("carrier_type");
		    }
            set 
		    {
			    SetColumnValue("carrier_type", value);
            }
        }
	      
        [XmlAttribute("CarrierId")]
        [Bindable(true)]
        public string CarrierId 
	    {
		    get
		    {
			    return GetColumnValue<string>("carrier_id");
		    }
            set 
		    {
			    SetColumnValue("carrier_id", value);
            }
        }
	      
        [XmlAttribute("InvoiceBuyerName")]
        [Bindable(true)]
        public string InvoiceBuyerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_buyer_name");
		    }
            set 
		    {
			    SetColumnValue("invoice_buyer_name", value);
            }
        }
	      
        [XmlAttribute("InvoiceBuyerAddress")]
        [Bindable(true)]
        public string InvoiceBuyerAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_buyer_address");
		    }
            set 
		    {
			    SetColumnValue("invoice_buyer_address", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string UserName = @"user_name";
            
            public static string LastName = @"last_name";
            
            public static string FirstName = @"first_name";
            
            public static string BuildingGuid = @"building_GUID";
            
            public static string Birthday = @"birthday";
            
            public static string Gender = @"gender";
            
            public static string Mobile = @"mobile";
            
            public static string CompanyName = @"company_name";
            
            public static string CompanyDepartment = @"company_department";
            
            public static string CompanyTel = @"company_tel";
            
            public static string CompanyTelExt = @"company_tel_ext";
            
            public static string CompanyAddress = @"company_address";
            
            public static string DeliveryMethod = @"delivery_method";
            
            public static string PrimaryContactMethod = @"primary_contact_method";
            
            public static string MemberStatus = @"member_status";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyTime = @"modify_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string MemberName = @"member_name";
            
            public static string UserEmail = @"user_email";
            
            public static string IsGuest = @"is_guest";
            
            public static string BuildingId = @"building_id";
            
            public static string BuildingName = @"building_name";
            
            public static string BuildingStreetName = @"building_street_name";
            
            public static string BuildingAddressNumber = @"building_address_number";
            
            public static string BuildingAddress = @"building_address";
            
            public static string BuildingOnline = @"building_online";
            
            public static string BuildingRank = @"building_rank";
            
            public static string CityName = @"city_name";
            
            public static string Code = @"code";
            
            public static string Comment = @"comment";
            
            public static string CityId = @"city_id";
            
            public static string CityStatus = @"city_status";
            
            public static string CityParentId = @"city_parent_id";
            
            public static string CityRank = @"city_rank";
            
            public static string ZipCode = @"zip_code";
            
            public static string UniqueId = @"unique_id";
            
            public static string CarrierType = @"carrier_type";
            
            public static string CarrierId = @"carrier_id";
            
            public static string InvoiceBuyerName = @"invoice_buyer_name";
            
            public static string InvoiceBuyerAddress = @"invoice_buyer_address";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

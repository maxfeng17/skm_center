using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CtAtmRefund class.
	/// </summary>
    [Serializable]
	public partial class CtAtmRefundCollection : RepositoryList<CtAtmRefund, CtAtmRefundCollection>
	{	   
		public CtAtmRefundCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CtAtmRefundCollection</returns>
		public CtAtmRefundCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CtAtmRefund o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the ct_atm_refund table.
	/// </summary>
	[Serializable]
	public partial class CtAtmRefund : RepositoryRecord<CtAtmRefund>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CtAtmRefund()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CtAtmRefund(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("ct_atm_refund", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarSi = new TableSchema.TableColumn(schema);
				colvarSi.ColumnName = "si";
				colvarSi.DataType = DbType.Int32;
				colvarSi.MaxLength = 0;
				colvarSi.AutoIncrement = true;
				colvarSi.IsNullable = false;
				colvarSi.IsPrimaryKey = true;
				colvarSi.IsForeignKey = false;
				colvarSi.IsReadOnly = false;
				colvarSi.DefaultSetting = @"";
				colvarSi.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSi);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarPaymentId = new TableSchema.TableColumn(schema);
				colvarPaymentId.ColumnName = "payment_id";
				colvarPaymentId.DataType = DbType.AnsiString;
				colvarPaymentId.MaxLength = 50;
				colvarPaymentId.AutoIncrement = false;
				colvarPaymentId.IsNullable = false;
				colvarPaymentId.IsPrimaryKey = false;
				colvarPaymentId.IsForeignKey = false;
				colvarPaymentId.IsReadOnly = false;
				colvarPaymentId.DefaultSetting = @"";
				colvarPaymentId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentId);
				
				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Int32;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = false;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);
				
				TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
				colvarAccountName.ColumnName = "account_name";
				colvarAccountName.DataType = DbType.String;
				colvarAccountName.MaxLength = 50;
				colvarAccountName.AutoIncrement = false;
				colvarAccountName.IsNullable = false;
				colvarAccountName.IsPrimaryKey = false;
				colvarAccountName.IsForeignKey = false;
				colvarAccountName.IsReadOnly = false;
				colvarAccountName.DefaultSetting = @"";
				colvarAccountName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountName);
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.AnsiString;
				colvarId.MaxLength = 30;
				colvarId.AutoIncrement = false;
				colvarId.IsNullable = true;
				colvarId.IsPrimaryKey = false;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBankName = new TableSchema.TableColumn(schema);
				colvarBankName.ColumnName = "bank_name";
				colvarBankName.DataType = DbType.String;
				colvarBankName.MaxLength = 50;
				colvarBankName.AutoIncrement = false;
				colvarBankName.IsNullable = false;
				colvarBankName.IsPrimaryKey = false;
				colvarBankName.IsForeignKey = false;
				colvarBankName.IsReadOnly = false;
				colvarBankName.DefaultSetting = @"";
				colvarBankName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBankName);
				
				TableSchema.TableColumn colvarBranchName = new TableSchema.TableColumn(schema);
				colvarBranchName.ColumnName = "branch_name";
				colvarBranchName.DataType = DbType.String;
				colvarBranchName.MaxLength = 50;
				colvarBranchName.AutoIncrement = false;
				colvarBranchName.IsNullable = true;
				colvarBranchName.IsPrimaryKey = false;
				colvarBranchName.IsForeignKey = false;
				colvarBranchName.IsReadOnly = false;
				colvarBranchName.DefaultSetting = @"";
				colvarBranchName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBranchName);
				
				TableSchema.TableColumn colvarBankCode = new TableSchema.TableColumn(schema);
				colvarBankCode.ColumnName = "bank_code";
				colvarBankCode.DataType = DbType.AnsiString;
				colvarBankCode.MaxLength = 10;
				colvarBankCode.AutoIncrement = false;
				colvarBankCode.IsNullable = true;
				colvarBankCode.IsPrimaryKey = false;
				colvarBankCode.IsForeignKey = false;
				colvarBankCode.IsReadOnly = false;
				colvarBankCode.DefaultSetting = @"";
				colvarBankCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBankCode);
				
				TableSchema.TableColumn colvarAccountNumber = new TableSchema.TableColumn(schema);
				colvarAccountNumber.ColumnName = "account_number";
				colvarAccountNumber.DataType = DbType.AnsiString;
				colvarAccountNumber.MaxLength = 50;
				colvarAccountNumber.AutoIncrement = false;
				colvarAccountNumber.IsNullable = true;
				colvarAccountNumber.IsPrimaryKey = false;
				colvarAccountNumber.IsForeignKey = false;
				colvarAccountNumber.IsReadOnly = false;
				colvarAccountNumber.DefaultSetting = @"";
				colvarAccountNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountNumber);
				
				TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
				colvarPhone.ColumnName = "phone";
				colvarPhone.DataType = DbType.AnsiString;
				colvarPhone.MaxLength = 20;
				colvarPhone.AutoIncrement = false;
				colvarPhone.IsNullable = true;
				colvarPhone.IsPrimaryKey = false;
				colvarPhone.IsForeignKey = false;
				colvarPhone.IsReadOnly = false;
				colvarPhone.DefaultSetting = @"";
				colvarPhone.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhone);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarRefundProcessTime = new TableSchema.TableColumn(schema);
				colvarRefundProcessTime.ColumnName = "refund_process_time";
				colvarRefundProcessTime.DataType = DbType.DateTime;
				colvarRefundProcessTime.MaxLength = 0;
				colvarRefundProcessTime.AutoIncrement = false;
				colvarRefundProcessTime.IsNullable = true;
				colvarRefundProcessTime.IsPrimaryKey = false;
				colvarRefundProcessTime.IsForeignKey = false;
				colvarRefundProcessTime.IsReadOnly = false;
				colvarRefundProcessTime.DefaultSetting = @"";
				colvarRefundProcessTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundProcessTime);
				
				TableSchema.TableColumn colvarRefundSendTime = new TableSchema.TableColumn(schema);
				colvarRefundSendTime.ColumnName = "refund_send_time";
				colvarRefundSendTime.DataType = DbType.DateTime;
				colvarRefundSendTime.MaxLength = 0;
				colvarRefundSendTime.AutoIncrement = false;
				colvarRefundSendTime.IsNullable = true;
				colvarRefundSendTime.IsPrimaryKey = false;
				colvarRefundSendTime.IsForeignKey = false;
				colvarRefundSendTime.IsReadOnly = false;
				colvarRefundSendTime.DefaultSetting = @"";
				colvarRefundSendTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundSendTime);
				
				TableSchema.TableColumn colvarRefundResponseTime = new TableSchema.TableColumn(schema);
				colvarRefundResponseTime.ColumnName = "refund_response_time";
				colvarRefundResponseTime.DataType = DbType.DateTime;
				colvarRefundResponseTime.MaxLength = 0;
				colvarRefundResponseTime.AutoIncrement = false;
				colvarRefundResponseTime.IsNullable = true;
				colvarRefundResponseTime.IsPrimaryKey = false;
				colvarRefundResponseTime.IsForeignKey = false;
				colvarRefundResponseTime.IsReadOnly = false;
				colvarRefundResponseTime.DefaultSetting = @"";
				colvarRefundResponseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundResponseTime);
				
				TableSchema.TableColumn colvarTargetDate = new TableSchema.TableColumn(schema);
				colvarTargetDate.ColumnName = "target_date";
				colvarTargetDate.DataType = DbType.DateTime;
				colvarTargetDate.MaxLength = 0;
				colvarTargetDate.AutoIncrement = false;
				colvarTargetDate.IsNullable = true;
				colvarTargetDate.IsPrimaryKey = false;
				colvarTargetDate.IsForeignKey = false;
				colvarTargetDate.IsReadOnly = false;
				colvarTargetDate.DefaultSetting = @"";
				colvarTargetDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTargetDate);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarFailReason = new TableSchema.TableColumn(schema);
				colvarFailReason.ColumnName = "fail_reason";
				colvarFailReason.DataType = DbType.String;
				colvarFailReason.MaxLength = 50;
				colvarFailReason.AutoIncrement = false;
				colvarFailReason.IsNullable = true;
				colvarFailReason.IsPrimaryKey = false;
				colvarFailReason.IsForeignKey = false;
				colvarFailReason.IsReadOnly = false;
				colvarFailReason.DefaultSetting = @"";
				colvarFailReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailReason);
				
				TableSchema.TableColumn colvarCouponList = new TableSchema.TableColumn(schema);
				colvarCouponList.ColumnName = "coupon_list";
				colvarCouponList.DataType = DbType.AnsiString;
				colvarCouponList.MaxLength = -1;
				colvarCouponList.AutoIncrement = false;
				colvarCouponList.IsNullable = true;
				colvarCouponList.IsPrimaryKey = false;
				colvarCouponList.IsForeignKey = false;
				colvarCouponList.IsReadOnly = false;
				colvarCouponList.DefaultSetting = @"";
				colvarCouponList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponList);
				
				TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
				colvarFlag.ColumnName = "flag";
				colvarFlag.DataType = DbType.Int32;
				colvarFlag.MaxLength = 0;
				colvarFlag.AutoIncrement = false;
				colvarFlag.IsNullable = false;
				colvarFlag.IsPrimaryKey = false;
				colvarFlag.IsForeignKey = false;
				colvarFlag.IsReadOnly = false;
				
						colvarFlag.DefaultSetting = @"((0))";
				colvarFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFlag);
				
				TableSchema.TableColumn colvarProcessDate = new TableSchema.TableColumn(schema);
				colvarProcessDate.ColumnName = "process_date";
				colvarProcessDate.DataType = DbType.DateTime;
				colvarProcessDate.MaxLength = 0;
				colvarProcessDate.AutoIncrement = false;
				colvarProcessDate.IsNullable = true;
				colvarProcessDate.IsPrimaryKey = false;
				colvarProcessDate.IsForeignKey = false;
				colvarProcessDate.IsReadOnly = false;
				colvarProcessDate.DefaultSetting = @"";
				colvarProcessDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProcessDate);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = true;
				colvarUserId.IsReadOnly = false;
				
						colvarUserId.DefaultSetting = @"((0))";
				
					colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
				colvarReturnFormId.ColumnName = "return_form_id";
				colvarReturnFormId.DataType = DbType.Int32;
				colvarReturnFormId.MaxLength = 0;
				colvarReturnFormId.AutoIncrement = false;
				colvarReturnFormId.IsNullable = true;
				colvarReturnFormId.IsPrimaryKey = false;
				colvarReturnFormId.IsForeignKey = false;
				colvarReturnFormId.IsReadOnly = false;
				colvarReturnFormId.DefaultSetting = @"";
				colvarReturnFormId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnFormId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("ct_atm_refund",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Si")]
		[Bindable(true)]
		public int Si 
		{
			get { return GetColumnValue<int>(Columns.Si); }
			set { SetColumnValue(Columns.Si, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("PaymentId")]
		[Bindable(true)]
		public string PaymentId 
		{
			get { return GetColumnValue<string>(Columns.PaymentId); }
			set { SetColumnValue(Columns.PaymentId, value); }
		}
		  
		[XmlAttribute("Amount")]
		[Bindable(true)]
		public int Amount 
		{
			get { return GetColumnValue<int>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}
		  
		[XmlAttribute("AccountName")]
		[Bindable(true)]
		public string AccountName 
		{
			get { return GetColumnValue<string>(Columns.AccountName); }
			set { SetColumnValue(Columns.AccountName, value); }
		}
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public string Id 
		{
			get { return GetColumnValue<string>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("BankName")]
		[Bindable(true)]
		public string BankName 
		{
			get { return GetColumnValue<string>(Columns.BankName); }
			set { SetColumnValue(Columns.BankName, value); }
		}
		  
		[XmlAttribute("BranchName")]
		[Bindable(true)]
		public string BranchName 
		{
			get { return GetColumnValue<string>(Columns.BranchName); }
			set { SetColumnValue(Columns.BranchName, value); }
		}
		  
		[XmlAttribute("BankCode")]
		[Bindable(true)]
		public string BankCode 
		{
			get { return GetColumnValue<string>(Columns.BankCode); }
			set { SetColumnValue(Columns.BankCode, value); }
		}
		  
		[XmlAttribute("AccountNumber")]
		[Bindable(true)]
		public string AccountNumber 
		{
			get { return GetColumnValue<string>(Columns.AccountNumber); }
			set { SetColumnValue(Columns.AccountNumber, value); }
		}
		  
		[XmlAttribute("Phone")]
		[Bindable(true)]
		public string Phone 
		{
			get { return GetColumnValue<string>(Columns.Phone); }
			set { SetColumnValue(Columns.Phone, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("RefundProcessTime")]
		[Bindable(true)]
		public DateTime? RefundProcessTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.RefundProcessTime); }
			set { SetColumnValue(Columns.RefundProcessTime, value); }
		}
		  
		[XmlAttribute("RefundSendTime")]
		[Bindable(true)]
		public DateTime? RefundSendTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.RefundSendTime); }
			set { SetColumnValue(Columns.RefundSendTime, value); }
		}
		  
		[XmlAttribute("RefundResponseTime")]
		[Bindable(true)]
		public DateTime? RefundResponseTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.RefundResponseTime); }
			set { SetColumnValue(Columns.RefundResponseTime, value); }
		}
		  
		[XmlAttribute("TargetDate")]
		[Bindable(true)]
		public DateTime? TargetDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.TargetDate); }
			set { SetColumnValue(Columns.TargetDate, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("FailReason")]
		[Bindable(true)]
		public string FailReason 
		{
			get { return GetColumnValue<string>(Columns.FailReason); }
			set { SetColumnValue(Columns.FailReason, value); }
		}
		  
		[XmlAttribute("CouponList")]
		[Bindable(true)]
		public string CouponList 
		{
			get { return GetColumnValue<string>(Columns.CouponList); }
			set { SetColumnValue(Columns.CouponList, value); }
		}
		  
		[XmlAttribute("Flag")]
		[Bindable(true)]
		public int Flag 
		{
			get { return GetColumnValue<int>(Columns.Flag); }
			set { SetColumnValue(Columns.Flag, value); }
		}
		  
		[XmlAttribute("ProcessDate")]
		[Bindable(true)]
		public DateTime? ProcessDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ProcessDate); }
			set { SetColumnValue(Columns.ProcessDate, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("ReturnFormId")]
		[Bindable(true)]
		public int? ReturnFormId 
		{
			get { return GetColumnValue<int?>(Columns.ReturnFormId); }
			set { SetColumnValue(Columns.ReturnFormId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn SiColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PaymentIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BankNameColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn BranchNameColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn BankCodeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountNumberColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn PhoneColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundProcessTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundSendTimeColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundResponseTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn TargetDateColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn FailReasonColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponListColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn FlagColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn ProcessDateColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnFormIdColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Si = @"si";
			 public static string OrderGuid = @"order_guid";
			 public static string PaymentId = @"payment_id";
			 public static string Amount = @"amount";
			 public static string AccountName = @"account_name";
			 public static string Id = @"ID";
			 public static string BankName = @"bank_name";
			 public static string BranchName = @"branch_name";
			 public static string BankCode = @"bank_code";
			 public static string AccountNumber = @"account_number";
			 public static string Phone = @"phone";
			 public static string CreateTime = @"create_time";
			 public static string RefundProcessTime = @"refund_process_time";
			 public static string RefundSendTime = @"refund_send_time";
			 public static string RefundResponseTime = @"refund_response_time";
			 public static string TargetDate = @"target_date";
			 public static string Status = @"status";
			 public static string FailReason = @"fail_reason";
			 public static string CouponList = @"coupon_list";
			 public static string Flag = @"flag";
			 public static string ProcessDate = @"process_date";
			 public static string UserId = @"user_id";
			 public static string ReturnFormId = @"return_form_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

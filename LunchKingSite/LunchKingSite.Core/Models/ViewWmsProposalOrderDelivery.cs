using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    [Serializable]
    public partial class ViewWmsProposalOrderDeliveryCollection : ReadOnlyList<ViewWmsProposalOrderDelivery, ViewWmsProposalOrderDeliveryCollection>
    {
        public ViewWmsProposalOrderDeliveryCollection() {}

    }

    [Serializable]
    public partial class ViewWmsProposalOrderDelivery : ReadOnlyRecord<ViewWmsProposalOrderDelivery>, IReadOnlyRecord
    {
        #region .ctors and Default Settings
        public ViewWmsProposalOrderDelivery()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ViewWmsProposalOrderDelivery(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        public ViewWmsProposalOrderDelivery(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewWmsProposalOrderDelivery(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_wms_proposal_order_delivery", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarPid = new TableSchema.TableColumn(schema);
                colvarPid.ColumnName = "pid";
                colvarPid.DataType = DbType.Int32;
                colvarPid.MaxLength = 0;
                colvarPid.AutoIncrement = false;
                colvarPid.IsNullable = false;
                colvarPid.IsPrimaryKey = false;
                colvarPid.IsForeignKey = false;
                colvarPid.IsReadOnly = false;
                colvarPid.DefaultSetting = @"";
                colvarPid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPid);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                colvarUniqueId.DefaultSetting = @"";
                colvarUniqueId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUniqueId);

                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                colvarOrderId.DefaultSetting = @"";
                colvarOrderId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderId);

                TableSchema.TableColumn colvarOrderCreateTime = new TableSchema.TableColumn(schema);
                colvarOrderCreateTime.ColumnName = "order_create_time";
                colvarOrderCreateTime.DataType = DbType.DateTime;
                colvarOrderCreateTime.MaxLength = 0;
                colvarOrderCreateTime.AutoIncrement = false;
                colvarOrderCreateTime.IsNullable = false;
                colvarOrderCreateTime.IsPrimaryKey = false;
                colvarOrderCreateTime.IsForeignKey = false;
                colvarOrderCreateTime.IsReadOnly = false;
                colvarOrderCreateTime.DefaultSetting = @"";
                colvarOrderCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderCreateTime);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                colvarOrderGuid.DefaultSetting = @"";
                colvarOrderGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarPchomeProdId = new TableSchema.TableColumn(schema);
                colvarPchomeProdId.ColumnName = "pchome_prod_id";
                colvarPchomeProdId.DataType = DbType.String;
                colvarPchomeProdId.MaxLength = 50;
                colvarPchomeProdId.AutoIncrement = false;
                colvarPchomeProdId.IsNullable = true;
                colvarPchomeProdId.IsPrimaryKey = false;
                colvarPchomeProdId.IsForeignKey = false;
                colvarPchomeProdId.IsReadOnly = false;
                colvarPchomeProdId.DefaultSetting = @"";
                colvarPchomeProdId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPchomeProdId);

                TableSchema.TableColumn colvarOptionName = new TableSchema.TableColumn(schema);
                colvarOptionName.ColumnName = "option_name";
                colvarOptionName.DataType = DbType.String;
                colvarOptionName.MaxLength = 50;
                colvarOptionName.AutoIncrement = false;
                colvarOptionName.IsNullable = false;
                colvarOptionName.IsPrimaryKey = false;
                colvarOptionName.IsForeignKey = false;
                colvarOptionName.IsReadOnly = false;
                colvarOptionName.DefaultSetting = @"";
                colvarOptionName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOptionName);

                TableSchema.TableColumn colvarShipStatus = new TableSchema.TableColumn(schema);
                colvarShipStatus.ColumnName = "ship_status";
                colvarShipStatus.DataType = DbType.Int32;
                colvarShipStatus.MaxLength = 0;
                colvarShipStatus.AutoIncrement = false;
                colvarShipStatus.IsNullable = false;
                colvarShipStatus.IsPrimaryKey = false;
                colvarShipStatus.IsForeignKey = false;
                colvarShipStatus.IsReadOnly = false;
                colvarShipStatus.DefaultSetting = @"";
                colvarShipStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShipStatus);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                colvarSellerGuid.DefaultSetting = @"";
                colvarSellerGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarBrandName = new TableSchema.TableColumn(schema);
                colvarBrandName.ColumnName = "brand_name";
                colvarBrandName.DataType = DbType.String;
                colvarBrandName.MaxLength = 100;
                colvarBrandName.AutoIncrement = false;
                colvarBrandName.IsNullable = false;
                colvarBrandName.IsPrimaryKey = false;
                colvarBrandName.IsForeignKey = false;
                colvarBrandName.IsReadOnly = false;
                colvarBrandName.DefaultSetting = @"";
                colvarBrandName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBrandName);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                colvarItemName.DefaultSetting = @"";
                colvarItemName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
                colvarAppTitle.ColumnName = "app_title";
                colvarAppTitle.DataType = DbType.String;
                colvarAppTitle.MaxLength = 40;
                colvarAppTitle.AutoIncrement = false;
                colvarAppTitle.IsNullable = true;
                colvarAppTitle.IsPrimaryKey = false;
                colvarAppTitle.IsForeignKey = false;
                colvarAppTitle.IsReadOnly = false;
                colvarAppTitle.DefaultSetting = @"";
                colvarAppTitle.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAppTitle);

                TableSchema.TableColumn colvarShipCompanyName = new TableSchema.TableColumn(schema);
                colvarShipCompanyName.ColumnName = "ship_company_name";
                colvarShipCompanyName.DataType = DbType.String;
                colvarShipCompanyName.MaxLength = 100;
                colvarShipCompanyName.AutoIncrement = false;
                colvarShipCompanyName.IsNullable = true;
                colvarShipCompanyName.IsPrimaryKey = false;
                colvarShipCompanyName.IsForeignKey = false;
                colvarShipCompanyName.IsReadOnly = false;
                colvarShipCompanyName.DefaultSetting = @"";
                colvarShipCompanyName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShipCompanyName);

                TableSchema.TableColumn colvarShipNo = new TableSchema.TableColumn(schema);
                colvarShipNo.ColumnName = "ship_no";
                colvarShipNo.DataType = DbType.String;
                colvarShipNo.MaxLength = 2147483647;
                colvarShipNo.AutoIncrement = false;
                colvarShipNo.IsNullable = true;
                colvarShipNo.IsPrimaryKey = false;
                colvarShipNo.IsForeignKey = false;
                colvarShipNo.IsReadOnly = false;
                colvarShipNo.DefaultSetting = @"";
                colvarShipNo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShipNo);

                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;

                schema.Columns.Add(colvarOrderStatus);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_wms_proposal_order_delivery",schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("Pid")]
        [Bindable(true)]
        public int Pid
        {
            get { return GetColumnValue<int>(Columns.Pid); }
            set { SetColumnValue(Columns.Pid, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid
        {
            get { return GetColumnValue<Guid?>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId
        {
            get { return GetColumnValue<int>(Columns.UniqueId); }
            set { SetColumnValue(Columns.UniqueId, value); }
        }

        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId
        {
            get { return GetColumnValue<string>(Columns.OrderId); }
            set { SetColumnValue(Columns.OrderId, value); }
        }

        [XmlAttribute("OrderCreateTime")]
        [Bindable(true)]
        public DateTime OrderCreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.OrderCreateTime); }
            set { SetColumnValue(Columns.OrderCreateTime, value); }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid
        {
            get { return GetColumnValue<Guid>(Columns.OrderGuid); }
            set { SetColumnValue(Columns.OrderGuid, value); }
        }

        [XmlAttribute("PchomeProdId")]
        [Bindable(true)]
        public string PchomeProdId
        {
            get { return GetColumnValue<string>(Columns.PchomeProdId); }
            set { SetColumnValue(Columns.PchomeProdId, value); }
        }

        [XmlAttribute("OptionName")]
        [Bindable(true)]
        public string OptionName
        {
            get { return GetColumnValue<string>(Columns.OptionName); }
            set { SetColumnValue(Columns.OptionName, value); }
        }

        [XmlAttribute("ShipStatus")]
        [Bindable(true)]
        public int ShipStatus
        {
            get { return GetColumnValue<int>(Columns.ShipStatus); }
            set { SetColumnValue(Columns.ShipStatus, value); }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid
        {
            get { return GetColumnValue<Guid>(Columns.SellerGuid); }
            set { SetColumnValue(Columns.SellerGuid, value); }
        }

        [XmlAttribute("BrandName")]
        [Bindable(true)]
        public string BrandName
        {
            get { return GetColumnValue<string>(Columns.BrandName); }
            set { SetColumnValue(Columns.BrandName, value); }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get { return GetColumnValue<string>(Columns.ItemName); }
            set { SetColumnValue(Columns.ItemName, value); }
        }

        [XmlAttribute("AppTitle")]
        [Bindable(true)]
        public string AppTitle
        {
            get { return GetColumnValue<string>(Columns.AppTitle); }
            set { SetColumnValue(Columns.AppTitle, value); }
        }

        [XmlAttribute("ShipCompanyName")]
        [Bindable(true)]
        public string ShipCompanyName
        {
            get { return GetColumnValue<string>(Columns.ShipCompanyName); }
            set { SetColumnValue(Columns.ShipCompanyName, value); }
        }

        [XmlAttribute("ShipNo")]
        [Bindable(true)]
        public string ShipNo
        {
            get { return GetColumnValue<string>(Columns.ShipNo); }
            set { SetColumnValue(Columns.ShipNo, value); }
        }

        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus
        {
            get { return GetColumnValue<int>("order_status"); }
            set { SetColumnValue("order_status", value); }
        }

        #endregion

        #region Typed Columns

        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }

        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }

        public static TableSchema.TableColumn PidColumn
        {
            get { return Schema.Columns[2]; }
        }

        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[3]; }
        }

        public static TableSchema.TableColumn UniqueIdColumn
        {
            get { return Schema.Columns[4]; }
        }

        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[5]; }
        }

        public static TableSchema.TableColumn OrderCreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }

        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[7]; }
        }

        public static TableSchema.TableColumn PchomeProdIdColumn
        {
            get { return Schema.Columns[8]; }
        }

        public static TableSchema.TableColumn OptionNameColumn
        {
            get { return Schema.Columns[9]; }
        }

        public static TableSchema.TableColumn ShipStatusColumn
        {
            get { return Schema.Columns[10]; }
        }

        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[11]; }
        }

        public static TableSchema.TableColumn BrandNameColumn
        {
            get { return Schema.Columns[12]; }
        }

        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[13]; }
        }

        public static TableSchema.TableColumn AppTitleColumn
        {
            get { return Schema.Columns[14]; }
        }

        public static TableSchema.TableColumn ShipCompanyNameColumn
        {
            get { return Schema.Columns[15]; }
        }

        public static TableSchema.TableColumn ShipNoColumn
        {
            get { return Schema.Columns[16]; }
        }

        #endregion

        #region Columns Struct

        public struct Columns
        {
            public static string Id = @"id";
            public static string Guid = @"guid";
            public static string Pid = @"pid";
            public static string BusinessHourGuid = @"business_hour_guid";
            public static string UniqueId = @"unique_id";
            public static string OrderId = @"order_id";
            public static string OrderCreateTime = @"order_create_time";
            public static string OrderGuid = @"order_guid";
            public static string PchomeProdId = @"pchome_prod_id";
            public static string OptionName = @"option_name";
            public static string ShipStatus = @"ship_status";
            public static string SellerGuid = @"seller_GUID";
            public static string BrandName = @"brand_name";
            public static string ItemName = @"item_name";
            public static string AppTitle = @"app_title";
            public static string ShipCompanyName = @"ship_company_name";
            public static string ShipNo = @"ship_no";
            public static string OrderStatus = @"order_status";
        }

        #endregion

        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

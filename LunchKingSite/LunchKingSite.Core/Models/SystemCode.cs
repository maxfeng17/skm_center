using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SystemCode class.
	/// </summary>
    [Serializable]
	public partial class SystemCodeCollection : RepositoryList<SystemCode, SystemCodeCollection>
	{	   
		public SystemCodeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SystemCodeCollection</returns>
		public SystemCodeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SystemCode o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the system_code table.
	/// </summary>
	[Serializable]
	public partial class SystemCode : RepositoryRecord<SystemCode>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SystemCode()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SystemCode(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("system_code", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarCodeGroup = new TableSchema.TableColumn(schema);
				colvarCodeGroup.ColumnName = "code_group";
				colvarCodeGroup.DataType = DbType.AnsiString;
				colvarCodeGroup.MaxLength = 50;
				colvarCodeGroup.AutoIncrement = false;
				colvarCodeGroup.IsNullable = false;
				colvarCodeGroup.IsPrimaryKey = true;
				colvarCodeGroup.IsForeignKey = false;
				colvarCodeGroup.IsReadOnly = false;
				colvarCodeGroup.DefaultSetting = @"";
				colvarCodeGroup.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodeGroup);
				
				TableSchema.TableColumn colvarCodeGroupName = new TableSchema.TableColumn(schema);
				colvarCodeGroupName.ColumnName = "code_group_name";
				colvarCodeGroupName.DataType = DbType.String;
				colvarCodeGroupName.MaxLength = 50;
				colvarCodeGroupName.AutoIncrement = false;
				colvarCodeGroupName.IsNullable = false;
				colvarCodeGroupName.IsPrimaryKey = false;
				colvarCodeGroupName.IsForeignKey = false;
				colvarCodeGroupName.IsReadOnly = false;
				colvarCodeGroupName.DefaultSetting = @"";
				colvarCodeGroupName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodeGroupName);
				
				TableSchema.TableColumn colvarCodeId = new TableSchema.TableColumn(schema);
				colvarCodeId.ColumnName = "code_id";
				colvarCodeId.DataType = DbType.Int32;
				colvarCodeId.MaxLength = 0;
				colvarCodeId.AutoIncrement = false;
				colvarCodeId.IsNullable = false;
				colvarCodeId.IsPrimaryKey = true;
				colvarCodeId.IsForeignKey = false;
				colvarCodeId.IsReadOnly = false;
				colvarCodeId.DefaultSetting = @"";
				colvarCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodeId);
				
				TableSchema.TableColumn colvarCodeName = new TableSchema.TableColumn(schema);
				colvarCodeName.ColumnName = "code_name";
				colvarCodeName.DataType = DbType.String;
				colvarCodeName.MaxLength = 50;
				colvarCodeName.AutoIncrement = false;
				colvarCodeName.IsNullable = false;
				colvarCodeName.IsPrimaryKey = false;
				colvarCodeName.IsForeignKey = false;
				colvarCodeName.IsReadOnly = false;
				colvarCodeName.DefaultSetting = @"";
				colvarCodeName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodeName);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = false;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				colvarEnabled.DefaultSetting = @"";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				TableSchema.TableColumn colvarIsDefault = new TableSchema.TableColumn(schema);
				colvarIsDefault.ColumnName = "is_default";
				colvarIsDefault.DataType = DbType.Boolean;
				colvarIsDefault.MaxLength = 0;
				colvarIsDefault.AutoIncrement = false;
				colvarIsDefault.IsNullable = false;
				colvarIsDefault.IsPrimaryKey = false;
				colvarIsDefault.IsForeignKey = false;
				colvarIsDefault.IsReadOnly = false;
				colvarIsDefault.DefaultSetting = @"";
				colvarIsDefault.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDefault);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 255;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 255;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = true;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				colvarSeq.DefaultSetting = @"";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				TableSchema.TableColumn colvarShortName = new TableSchema.TableColumn(schema);
				colvarShortName.ColumnName = "short_name";
				colvarShortName.DataType = DbType.String;
				colvarShortName.MaxLength = 50;
				colvarShortName.AutoIncrement = false;
				colvarShortName.IsNullable = true;
				colvarShortName.IsPrimaryKey = false;
				colvarShortName.IsForeignKey = false;
				colvarShortName.IsReadOnly = false;
				colvarShortName.DefaultSetting = @"";
				colvarShortName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShortName);
				
				TableSchema.TableColumn colvarParentCodeId = new TableSchema.TableColumn(schema);
				colvarParentCodeId.ColumnName = "parent_code_id";
				colvarParentCodeId.DataType = DbType.Int32;
				colvarParentCodeId.MaxLength = 0;
				colvarParentCodeId.AutoIncrement = false;
				colvarParentCodeId.IsNullable = true;
				colvarParentCodeId.IsPrimaryKey = false;
				colvarParentCodeId.IsForeignKey = false;
				colvarParentCodeId.IsReadOnly = false;
				colvarParentCodeId.DefaultSetting = @"";
				colvarParentCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentCodeId);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "code";
				colvarCode.DataType = DbType.String;
				colvarCode.MaxLength = 500;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = true;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarExpiredTime = new TableSchema.TableColumn(schema);
				colvarExpiredTime.ColumnName = "expired_time";
				colvarExpiredTime.DataType = DbType.DateTime;
				colvarExpiredTime.MaxLength = 0;
				colvarExpiredTime.AutoIncrement = false;
				colvarExpiredTime.IsNullable = true;
				colvarExpiredTime.IsPrimaryKey = false;
				colvarExpiredTime.IsForeignKey = false;
				colvarExpiredTime.IsReadOnly = false;
				colvarExpiredTime.DefaultSetting = @"";
				colvarExpiredTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExpiredTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("system_code",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("CodeGroup")]
		[Bindable(true)]
		public string CodeGroup 
		{
			get { return GetColumnValue<string>(Columns.CodeGroup); }
			set { SetColumnValue(Columns.CodeGroup, value); }
		}
		  
		[XmlAttribute("CodeGroupName")]
		[Bindable(true)]
		public string CodeGroupName 
		{
			get { return GetColumnValue<string>(Columns.CodeGroupName); }
			set { SetColumnValue(Columns.CodeGroupName, value); }
		}
		  
		[XmlAttribute("CodeId")]
		[Bindable(true)]
		public int CodeId 
		{
			get { return GetColumnValue<int>(Columns.CodeId); }
			set { SetColumnValue(Columns.CodeId, value); }
		}
		  
		[XmlAttribute("CodeName")]
		[Bindable(true)]
		public string CodeName 
		{
			get { return GetColumnValue<string>(Columns.CodeName); }
			set { SetColumnValue(Columns.CodeName, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool Enabled 
		{
			get { return GetColumnValue<bool>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		  
		[XmlAttribute("IsDefault")]
		[Bindable(true)]
		public bool IsDefault 
		{
			get { return GetColumnValue<bool>(Columns.IsDefault); }
			set { SetColumnValue(Columns.IsDefault, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int? Seq 
		{
			get { return GetColumnValue<int?>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		  
		[XmlAttribute("ShortName")]
		[Bindable(true)]
		public string ShortName 
		{
			get { return GetColumnValue<string>(Columns.ShortName); }
			set { SetColumnValue(Columns.ShortName, value); }
		}
		  
		[XmlAttribute("ParentCodeId")]
		[Bindable(true)]
		public int? ParentCodeId 
		{
			get { return GetColumnValue<int?>(Columns.ParentCodeId); }
			set { SetColumnValue(Columns.ParentCodeId, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public string Code 
		{
			get { return GetColumnValue<string>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("ExpiredTime")]
		[Bindable(true)]
		public DateTime? ExpiredTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ExpiredTime); }
			set { SetColumnValue(Columns.ExpiredTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn CodeGroupColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeGroupNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDefaultColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ShortNameColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ParentCodeIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ExpiredTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string CodeGroup = @"code_group";
			 public static string CodeGroupName = @"code_group_name";
			 public static string CodeId = @"code_id";
			 public static string CodeName = @"code_name";
			 public static string Enabled = @"enabled";
			 public static string IsDefault = @"is_default";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string Seq = @"seq";
			 public static string ShortName = @"short_name";
			 public static string ParentCodeId = @"parent_code_id";
			 public static string Code = @"code";
			 public static string ExpiredTime = @"expired_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

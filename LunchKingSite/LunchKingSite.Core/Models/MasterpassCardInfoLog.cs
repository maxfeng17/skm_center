using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MasterpassCardInfoLog class.
	/// </summary>
    [Serializable]
	public partial class MasterpassCardInfoLogCollection : RepositoryList<MasterpassCardInfoLog, MasterpassCardInfoLogCollection>
	{	   
		public MasterpassCardInfoLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MasterpassCardInfoLogCollection</returns>
		public MasterpassCardInfoLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MasterpassCardInfoLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the masterpass_card_info_log table.
	/// </summary>
	
	[Serializable]
	public partial class MasterpassCardInfoLog : RepositoryRecord<MasterpassCardInfoLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MasterpassCardInfoLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MasterpassCardInfoLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("masterpass_card_info_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarVerifierCode = new TableSchema.TableColumn(schema);
				colvarVerifierCode.ColumnName = "verifier_code";
				colvarVerifierCode.DataType = DbType.Int32;
				colvarVerifierCode.MaxLength = 0;
				colvarVerifierCode.AutoIncrement = true;
				colvarVerifierCode.IsNullable = false;
				colvarVerifierCode.IsPrimaryKey = true;
				colvarVerifierCode.IsForeignKey = false;
				colvarVerifierCode.IsReadOnly = false;
				colvarVerifierCode.DefaultSetting = @"";
				colvarVerifierCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifierCode);
				
				TableSchema.TableColumn colvarAuthToken = new TableSchema.TableColumn(schema);
				colvarAuthToken.ColumnName = "auth_token";
				colvarAuthToken.DataType = DbType.String;
				colvarAuthToken.MaxLength = 50;
				colvarAuthToken.AutoIncrement = false;
				colvarAuthToken.IsNullable = false;
				colvarAuthToken.IsPrimaryKey = false;
				colvarAuthToken.IsForeignKey = false;
				colvarAuthToken.IsReadOnly = false;
				colvarAuthToken.DefaultSetting = @"";
				colvarAuthToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAuthToken);
				
				TableSchema.TableColumn colvarCardNumber = new TableSchema.TableColumn(schema);
				colvarCardNumber.ColumnName = "card_number";
				colvarCardNumber.DataType = DbType.AnsiString;
				colvarCardNumber.MaxLength = 100;
				colvarCardNumber.AutoIncrement = false;
				colvarCardNumber.IsNullable = true;
				colvarCardNumber.IsPrimaryKey = false;
				colvarCardNumber.IsForeignKey = false;
				colvarCardNumber.IsReadOnly = false;
				colvarCardNumber.DefaultSetting = @"";
				colvarCardNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardNumber);
				
				TableSchema.TableColumn colvarExpiryMonth = new TableSchema.TableColumn(schema);
				colvarExpiryMonth.ColumnName = "expiry_month";
				colvarExpiryMonth.DataType = DbType.AnsiString;
				colvarExpiryMonth.MaxLength = 2;
				colvarExpiryMonth.AutoIncrement = false;
				colvarExpiryMonth.IsNullable = false;
				colvarExpiryMonth.IsPrimaryKey = false;
				colvarExpiryMonth.IsForeignKey = false;
				colvarExpiryMonth.IsReadOnly = false;
				colvarExpiryMonth.DefaultSetting = @"";
				colvarExpiryMonth.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExpiryMonth);
				
				TableSchema.TableColumn colvarExpiryYear = new TableSchema.TableColumn(schema);
				colvarExpiryYear.ColumnName = "expiry_year";
				colvarExpiryYear.DataType = DbType.AnsiString;
				colvarExpiryYear.MaxLength = 4;
				colvarExpiryYear.AutoIncrement = false;
				colvarExpiryYear.IsNullable = false;
				colvarExpiryYear.IsPrimaryKey = false;
				colvarExpiryYear.IsForeignKey = false;
				colvarExpiryYear.IsReadOnly = false;
				colvarExpiryYear.DefaultSetting = @"";
				colvarExpiryYear.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExpiryYear);
				
				TableSchema.TableColumn colvarTransId = new TableSchema.TableColumn(schema);
				colvarTransId.ColumnName = "trans_id";
				colvarTransId.DataType = DbType.AnsiString;
				colvarTransId.MaxLength = 20;
				colvarTransId.AutoIncrement = false;
				colvarTransId.IsNullable = false;
				colvarTransId.IsPrimaryKey = false;
				colvarTransId.IsForeignKey = false;
				colvarTransId.IsReadOnly = false;
				colvarTransId.DefaultSetting = @"";
				colvarTransId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransId);
				
				TableSchema.TableColumn colvarIndicator = new TableSchema.TableColumn(schema);
				colvarIndicator.ColumnName = "indicator";
				colvarIndicator.DataType = DbType.String;
				colvarIndicator.MaxLength = 10;
				colvarIndicator.AutoIncrement = false;
				colvarIndicator.IsNullable = false;
				colvarIndicator.IsPrimaryKey = false;
				colvarIndicator.IsForeignKey = false;
				colvarIndicator.IsReadOnly = false;
				colvarIndicator.DefaultSetting = @"";
				colvarIndicator.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIndicator);
				
				TableSchema.TableColumn colvarCardBrandId = new TableSchema.TableColumn(schema);
				colvarCardBrandId.ColumnName = "card_brand_id";
				colvarCardBrandId.DataType = DbType.String;
				colvarCardBrandId.MaxLength = 20;
				colvarCardBrandId.AutoIncrement = false;
				colvarCardBrandId.IsNullable = false;
				colvarCardBrandId.IsPrimaryKey = false;
				colvarCardBrandId.IsForeignKey = false;
				colvarCardBrandId.IsReadOnly = false;
				colvarCardBrandId.DefaultSetting = @"";
				colvarCardBrandId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardBrandId);
				
				TableSchema.TableColumn colvarCardBrandName = new TableSchema.TableColumn(schema);
				colvarCardBrandName.ColumnName = "card_brand_name";
				colvarCardBrandName.DataType = DbType.String;
				colvarCardBrandName.MaxLength = 50;
				colvarCardBrandName.AutoIncrement = false;
				colvarCardBrandName.IsNullable = false;
				colvarCardBrandName.IsPrimaryKey = false;
				colvarCardBrandName.IsForeignKey = false;
				colvarCardBrandName.IsReadOnly = false;
				colvarCardBrandName.DefaultSetting = @"";
				colvarCardBrandName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardBrandName);
				
				TableSchema.TableColumn colvarCardHolder = new TableSchema.TableColumn(schema);
				colvarCardHolder.ColumnName = "card_holder";
				colvarCardHolder.DataType = DbType.String;
				colvarCardHolder.MaxLength = 50;
				colvarCardHolder.AutoIncrement = false;
				colvarCardHolder.IsNullable = false;
				colvarCardHolder.IsPrimaryKey = false;
				colvarCardHolder.IsForeignKey = false;
				colvarCardHolder.IsReadOnly = false;
				colvarCardHolder.DefaultSetting = @"";
				colvarCardHolder.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardHolder);
				
				TableSchema.TableColumn colvarOauthToken = new TableSchema.TableColumn(schema);
				colvarOauthToken.ColumnName = "oauth_token";
				colvarOauthToken.DataType = DbType.AnsiString;
				colvarOauthToken.MaxLength = 50;
				colvarOauthToken.AutoIncrement = false;
				colvarOauthToken.IsNullable = false;
				colvarOauthToken.IsPrimaryKey = false;
				colvarOauthToken.IsForeignKey = false;
				colvarOauthToken.IsReadOnly = false;
				colvarOauthToken.DefaultSetting = @"";
				colvarOauthToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOauthToken);
				
				TableSchema.TableColumn colvarCreateUserId = new TableSchema.TableColumn(schema);
				colvarCreateUserId.ColumnName = "create_user_id";
				colvarCreateUserId.DataType = DbType.Int32;
				colvarCreateUserId.MaxLength = 0;
				colvarCreateUserId.AutoIncrement = false;
				colvarCreateUserId.IsNullable = false;
				colvarCreateUserId.IsPrimaryKey = false;
				colvarCreateUserId.IsForeignKey = false;
				colvarCreateUserId.IsReadOnly = false;
				colvarCreateUserId.DefaultSetting = @"";
				colvarCreateUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateUserId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarIsUsed = new TableSchema.TableColumn(schema);
				colvarIsUsed.ColumnName = "is_used";
				colvarIsUsed.DataType = DbType.Boolean;
				colvarIsUsed.MaxLength = 0;
				colvarIsUsed.AutoIncrement = false;
				colvarIsUsed.IsNullable = false;
				colvarIsUsed.IsPrimaryKey = false;
				colvarIsUsed.IsForeignKey = false;
				colvarIsUsed.IsReadOnly = false;
				
						colvarIsUsed.DefaultSetting = @"((0))";
				colvarIsUsed.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsUsed);
				
				TableSchema.TableColumn colvarUsedTime = new TableSchema.TableColumn(schema);
				colvarUsedTime.ColumnName = "used_time";
				colvarUsedTime.DataType = DbType.DateTime;
				colvarUsedTime.MaxLength = 0;
				colvarUsedTime.AutoIncrement = false;
				colvarUsedTime.IsNullable = true;
				colvarUsedTime.IsPrimaryKey = false;
				colvarUsedTime.IsForeignKey = false;
				colvarUsedTime.IsReadOnly = false;
				colvarUsedTime.DefaultSetting = @"";
				colvarUsedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUsedTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("masterpass_card_info_log",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("VerifierCode")]
		[Bindable(true)]
		public int VerifierCode 
		{
			get { return GetColumnValue<int>(Columns.VerifierCode); }
			set { SetColumnValue(Columns.VerifierCode, value); }
		}
		
		[XmlAttribute("AuthToken")]
		[Bindable(true)]
		public string AuthToken 
		{
			get { return GetColumnValue<string>(Columns.AuthToken); }
			set { SetColumnValue(Columns.AuthToken, value); }
		}
		
		[XmlAttribute("CardNumber")]
		[Bindable(true)]
		public string CardNumber 
		{
			get { return GetColumnValue<string>(Columns.CardNumber); }
			set { SetColumnValue(Columns.CardNumber, value); }
		}
		
		[XmlAttribute("ExpiryMonth")]
		[Bindable(true)]
		public string ExpiryMonth 
		{
			get { return GetColumnValue<string>(Columns.ExpiryMonth); }
			set { SetColumnValue(Columns.ExpiryMonth, value); }
		}
		
		[XmlAttribute("ExpiryYear")]
		[Bindable(true)]
		public string ExpiryYear 
		{
			get { return GetColumnValue<string>(Columns.ExpiryYear); }
			set { SetColumnValue(Columns.ExpiryYear, value); }
		}
		
		[XmlAttribute("TransId")]
		[Bindable(true)]
		public string TransId 
		{
			get { return GetColumnValue<string>(Columns.TransId); }
			set { SetColumnValue(Columns.TransId, value); }
		}
		
		[XmlAttribute("Indicator")]
		[Bindable(true)]
		public string Indicator 
		{
			get { return GetColumnValue<string>(Columns.Indicator); }
			set { SetColumnValue(Columns.Indicator, value); }
		}
		
		[XmlAttribute("CardBrandId")]
		[Bindable(true)]
		public string CardBrandId 
		{
			get { return GetColumnValue<string>(Columns.CardBrandId); }
			set { SetColumnValue(Columns.CardBrandId, value); }
		}
		
		[XmlAttribute("CardBrandName")]
		[Bindable(true)]
		public string CardBrandName 
		{
			get { return GetColumnValue<string>(Columns.CardBrandName); }
			set { SetColumnValue(Columns.CardBrandName, value); }
		}
		
		[XmlAttribute("CardHolder")]
		[Bindable(true)]
		public string CardHolder 
		{
			get { return GetColumnValue<string>(Columns.CardHolder); }
			set { SetColumnValue(Columns.CardHolder, value); }
		}
		
		[XmlAttribute("OauthToken")]
		[Bindable(true)]
		public string OauthToken 
		{
			get { return GetColumnValue<string>(Columns.OauthToken); }
			set { SetColumnValue(Columns.OauthToken, value); }
		}
		
		[XmlAttribute("CreateUserId")]
		[Bindable(true)]
		public int CreateUserId 
		{
			get { return GetColumnValue<int>(Columns.CreateUserId); }
			set { SetColumnValue(Columns.CreateUserId, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("IsUsed")]
		[Bindable(true)]
		public bool IsUsed 
		{
			get { return GetColumnValue<bool>(Columns.IsUsed); }
			set { SetColumnValue(Columns.IsUsed, value); }
		}
		
		[XmlAttribute("UsedTime")]
		[Bindable(true)]
		public DateTime? UsedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UsedTime); }
			set { SetColumnValue(Columns.UsedTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn VerifierCodeColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AuthTokenColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CardNumberColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ExpiryMonthColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ExpiryYearColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn TransIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IndicatorColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CardBrandIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CardBrandNameColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CardHolderColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn OauthTokenColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateUserIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn IsUsedColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn UsedTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string VerifierCode = @"verifier_code";
			 public static string AuthToken = @"auth_token";
			 public static string CardNumber = @"card_number";
			 public static string ExpiryMonth = @"expiry_month";
			 public static string ExpiryYear = @"expiry_year";
			 public static string TransId = @"trans_id";
			 public static string Indicator = @"indicator";
			 public static string CardBrandId = @"card_brand_id";
			 public static string CardBrandName = @"card_brand_name";
			 public static string CardHolder = @"card_holder";
			 public static string OauthToken = @"oauth_token";
			 public static string CreateUserId = @"create_user_id";
			 public static string CreateTime = @"create_time";
			 public static string IsUsed = @"is_used";
			 public static string UsedTime = @"used_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the WeeklyPayLog class.
	/// </summary>
    [Serializable]
	public partial class WeeklyPayLogCollection : RepositoryList<WeeklyPayLog, WeeklyPayLogCollection>
	{	   
		public WeeklyPayLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>WeeklyPayLogCollection</returns>
		public WeeklyPayLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                WeeklyPayLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the weekly_pay_log table.
	/// </summary>
	[Serializable]
	public partial class WeeklyPayLog : RepositoryRecord<WeeklyPayLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public WeeklyPayLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public WeeklyPayLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("weekly_pay_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarSpecialStatus = new TableSchema.TableColumn(schema);
				colvarSpecialStatus.ColumnName = "special_status";
				colvarSpecialStatus.DataType = DbType.Int32;
				colvarSpecialStatus.MaxLength = 0;
				colvarSpecialStatus.AutoIncrement = false;
				colvarSpecialStatus.IsNullable = false;
				colvarSpecialStatus.IsPrimaryKey = false;
				colvarSpecialStatus.IsForeignKey = false;
				colvarSpecialStatus.IsReadOnly = false;
				
						colvarSpecialStatus.DefaultSetting = @"((0))";
				colvarSpecialStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialStatus);
				
				TableSchema.TableColumn colvarCreditCard = new TableSchema.TableColumn(schema);
				colvarCreditCard.ColumnName = "credit_card";
				colvarCreditCard.DataType = DbType.Int32;
				colvarCreditCard.MaxLength = 0;
				colvarCreditCard.AutoIncrement = false;
				colvarCreditCard.IsNullable = false;
				colvarCreditCard.IsPrimaryKey = false;
				colvarCreditCard.IsForeignKey = false;
				colvarCreditCard.IsReadOnly = false;
				
						colvarCreditCard.DefaultSetting = @"((0))";
				colvarCreditCard.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditCard);
				
				TableSchema.TableColumn colvarAtm = new TableSchema.TableColumn(schema);
				colvarAtm.ColumnName = "atm";
				colvarAtm.DataType = DbType.Int32;
				colvarAtm.MaxLength = 0;
				colvarAtm.AutoIncrement = false;
				colvarAtm.IsNullable = false;
				colvarAtm.IsPrimaryKey = false;
				colvarAtm.IsForeignKey = false;
				colvarAtm.IsReadOnly = false;
				
						colvarAtm.DefaultSetting = @"((0))";
				colvarAtm.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAtm);
				
				TableSchema.TableColumn colvarReportGuid = new TableSchema.TableColumn(schema);
				colvarReportGuid.ColumnName = "report_guid";
				colvarReportGuid.DataType = DbType.Guid;
				colvarReportGuid.MaxLength = 0;
				colvarReportGuid.AutoIncrement = false;
				colvarReportGuid.IsNullable = false;
				colvarReportGuid.IsPrimaryKey = false;
				colvarReportGuid.IsForeignKey = false;
				colvarReportGuid.IsReadOnly = false;
				colvarReportGuid.DefaultSetting = @"";
				colvarReportGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReportGuid);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);
				
				TableSchema.TableColumn colvarTrustModifyTime = new TableSchema.TableColumn(schema);
				colvarTrustModifyTime.ColumnName = "trust_modify_time";
				colvarTrustModifyTime.DataType = DbType.DateTime;
				colvarTrustModifyTime.MaxLength = 0;
				colvarTrustModifyTime.AutoIncrement = false;
				colvarTrustModifyTime.IsNullable = true;
				colvarTrustModifyTime.IsPrimaryKey = false;
				colvarTrustModifyTime.IsForeignKey = false;
				colvarTrustModifyTime.IsReadOnly = false;
				colvarTrustModifyTime.DefaultSetting = @"";
				colvarTrustModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustModifyTime);
				
				TableSchema.TableColumn colvarSpecialModifyTime = new TableSchema.TableColumn(schema);
				colvarSpecialModifyTime.ColumnName = "special_modify_time";
				colvarSpecialModifyTime.DataType = DbType.DateTime;
				colvarSpecialModifyTime.MaxLength = 0;
				colvarSpecialModifyTime.AutoIncrement = false;
				colvarSpecialModifyTime.IsNullable = true;
				colvarSpecialModifyTime.IsPrimaryKey = false;
				colvarSpecialModifyTime.IsForeignKey = false;
				colvarSpecialModifyTime.IsReadOnly = false;
				colvarSpecialModifyTime.DefaultSetting = @"";
				colvarSpecialModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialModifyTime);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 1000;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("weekly_pay_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("SpecialStatus")]
		[Bindable(true)]
		public int SpecialStatus 
		{
			get { return GetColumnValue<int>(Columns.SpecialStatus); }
			set { SetColumnValue(Columns.SpecialStatus, value); }
		}
		  
		[XmlAttribute("CreditCard")]
		[Bindable(true)]
		public int CreditCard 
		{
			get { return GetColumnValue<int>(Columns.CreditCard); }
			set { SetColumnValue(Columns.CreditCard, value); }
		}
		  
		[XmlAttribute("Atm")]
		[Bindable(true)]
		public int Atm 
		{
			get { return GetColumnValue<int>(Columns.Atm); }
			set { SetColumnValue(Columns.Atm, value); }
		}
		  
		[XmlAttribute("ReportGuid")]
		[Bindable(true)]
		public Guid ReportGuid 
		{
			get { return GetColumnValue<Guid>(Columns.ReportGuid); }
			set { SetColumnValue(Columns.ReportGuid, value); }
		}
		  
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		  
		[XmlAttribute("TrustModifyTime")]
		[Bindable(true)]
		public DateTime? TrustModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.TrustModifyTime); }
			set { SetColumnValue(Columns.TrustModifyTime, value); }
		}
		  
		[XmlAttribute("SpecialModifyTime")]
		[Bindable(true)]
		public DateTime? SpecialModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.SpecialModifyTime); }
			set { SetColumnValue(Columns.SpecialModifyTime, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialStatusColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreditCardColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AtmColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ReportGuidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustModifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialModifyTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string Status = @"status";
			 public static string SpecialStatus = @"special_status";
			 public static string CreditCard = @"credit_card";
			 public static string Atm = @"atm";
			 public static string ReportGuid = @"report_guid";
			 public static string TrustId = @"trust_id";
			 public static string TrustModifyTime = @"trust_modify_time";
			 public static string SpecialModifyTime = @"special_modify_time";
			 public static string CreateTime = @"create_time";
			 public static string Message = @"message";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

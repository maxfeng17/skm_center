using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DiscountStore class.
	/// </summary>
    [Serializable]
	public partial class DiscountStoreCollection : RepositoryList<DiscountStore, DiscountStoreCollection>
	{	   
		public DiscountStoreCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DiscountStoreCollection</returns>
		public DiscountStoreCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DiscountStore o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the discount_store table.
	/// </summary>
	[Serializable]
	public partial class DiscountStore : RepositoryRecord<DiscountStore>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DiscountStore()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DiscountStore(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("discount_store", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarCampaignId = new TableSchema.TableColumn(schema);
				colvarCampaignId.ColumnName = "campaign_id";
				colvarCampaignId.DataType = DbType.Int32;
				colvarCampaignId.MaxLength = 0;
				colvarCampaignId.AutoIncrement = false;
				colvarCampaignId.IsNullable = false;
				colvarCampaignId.IsPrimaryKey = true;
				colvarCampaignId.IsForeignKey = false;
				colvarCampaignId.IsReadOnly = false;
				colvarCampaignId.DefaultSetting = @"";
				colvarCampaignId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCampaignId);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = false;
				colvarStoreGuid.IsPrimaryKey = true;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("discount_store",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("CampaignId")]
		[Bindable(true)]
		public int CampaignId 
		{
			get { return GetColumnValue<int>(Columns.CampaignId); }
			set { SetColumnValue(Columns.CampaignId, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid StoreGuid 
		{
			get { return GetColumnValue<Guid>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn CampaignIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string CampaignId = @"campaign_id";
			 public static string StoreGuid = @"store_guid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

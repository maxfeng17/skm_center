using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewComboDeal class.
    /// </summary>
    [Serializable]
    public partial class ViewComboDealCollection : ReadOnlyList<ViewComboDeal, ViewComboDealCollection>
    {        
        public ViewComboDealCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_combo_deals view.
    /// </summary>
    [Serializable]
    public partial class ViewComboDeal : ReadOnlyRecord<ViewComboDeal>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_combo_deals", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "Id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "BusinessHourGuid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarMainBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarMainBusinessHourGuid.ColumnName = "MainBusinessHourGuid";
                colvarMainBusinessHourGuid.DataType = DbType.Guid;
                colvarMainBusinessHourGuid.MaxLength = 0;
                colvarMainBusinessHourGuid.AutoIncrement = false;
                colvarMainBusinessHourGuid.IsNullable = true;
                colvarMainBusinessHourGuid.IsPrimaryKey = false;
                colvarMainBusinessHourGuid.IsForeignKey = false;
                colvarMainBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainBusinessHourGuid);
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "Title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 500;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = false;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "Sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = false;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequence);
                
                TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
                colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
                colvarBusinessHourOrderMinimum.MaxLength = 0;
                colvarBusinessHourOrderMinimum.AutoIncrement = false;
                colvarBusinessHourOrderMinimum.IsNullable = false;
                colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
                colvarBusinessHourOrderMinimum.IsForeignKey = false;
                colvarBusinessHourOrderMinimum.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderMinimum);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
                colvarOrderTotalLimit.ColumnName = "order_total_limit";
                colvarOrderTotalLimit.DataType = DbType.Currency;
                colvarOrderTotalLimit.MaxLength = 0;
                colvarOrderTotalLimit.AutoIncrement = false;
                colvarOrderTotalLimit.IsNullable = true;
                colvarOrderTotalLimit.IsPrimaryKey = false;
                colvarOrderTotalLimit.IsForeignKey = false;
                colvarOrderTotalLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTotalLimit);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
                colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeS.MaxLength = 0;
                colvarBusinessHourDeliverTimeS.AutoIncrement = false;
                colvarBusinessHourDeliverTimeS.IsNullable = true;
                colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeS.IsForeignKey = false;
                colvarBusinessHourDeliverTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeS);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarChangedExpireDate.ColumnName = "changed_expire_date";
                colvarChangedExpireDate.DataType = DbType.DateTime;
                colvarChangedExpireDate.MaxLength = 0;
                colvarChangedExpireDate.AutoIncrement = false;
                colvarChangedExpireDate.IsNullable = true;
                colvarChangedExpireDate.IsPrimaryKey = false;
                colvarChangedExpireDate.IsForeignKey = false;
                colvarChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarChangedExpireDate);
                
                TableSchema.TableColumn colvarQuantityMultiplier = new TableSchema.TableColumn(schema);
                colvarQuantityMultiplier.ColumnName = "quantity_multiplier";
                colvarQuantityMultiplier.DataType = DbType.Int32;
                colvarQuantityMultiplier.MaxLength = 0;
                colvarQuantityMultiplier.AutoIncrement = false;
                colvarQuantityMultiplier.IsNullable = true;
                colvarQuantityMultiplier.IsPrimaryKey = false;
                colvarQuantityMultiplier.IsForeignKey = false;
                colvarQuantityMultiplier.IsReadOnly = false;
                
                schema.Columns.Add(colvarQuantityMultiplier);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
                colvarItemOrigPrice.ColumnName = "item_orig_price";
                colvarItemOrigPrice.DataType = DbType.Currency;
                colvarItemOrigPrice.MaxLength = 0;
                colvarItemOrigPrice.AutoIncrement = false;
                colvarItemOrigPrice.IsNullable = false;
                colvarItemOrigPrice.IsPrimaryKey = false;
                colvarItemOrigPrice.IsForeignKey = false;
                colvarItemOrigPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemOrigPrice);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarMaxItemCount = new TableSchema.TableColumn(schema);
                colvarMaxItemCount.ColumnName = "max_item_count";
                colvarMaxItemCount.DataType = DbType.Int32;
                colvarMaxItemCount.MaxLength = 0;
                colvarMaxItemCount.AutoIncrement = false;
                colvarMaxItemCount.IsNullable = true;
                colvarMaxItemCount.IsPrimaryKey = false;
                colvarMaxItemCount.IsForeignKey = false;
                colvarMaxItemCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarMaxItemCount);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
                colvarSlug.ColumnName = "slug";
                colvarSlug.DataType = DbType.String;
                colvarSlug.MaxLength = 100;
                colvarSlug.AutoIncrement = false;
                colvarSlug.IsNullable = true;
                colvarSlug.IsPrimaryKey = false;
                colvarSlug.IsForeignKey = false;
                colvarSlug.IsReadOnly = false;
                
                schema.Columns.Add(colvarSlug);
                
                TableSchema.TableColumn colvarOrderedQuantity = new TableSchema.TableColumn(schema);
                colvarOrderedQuantity.ColumnName = "ordered_quantity";
                colvarOrderedQuantity.DataType = DbType.Int32;
                colvarOrderedQuantity.MaxLength = 0;
                colvarOrderedQuantity.AutoIncrement = false;
                colvarOrderedQuantity.IsNullable = false;
                colvarOrderedQuantity.IsPrimaryKey = false;
                colvarOrderedQuantity.IsForeignKey = false;
                colvarOrderedQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderedQuantity);
                
                TableSchema.TableColumn colvarOrderedIncludeRefundQuantity = new TableSchema.TableColumn(schema);
                colvarOrderedIncludeRefundQuantity.ColumnName = "ordered_include_refund_quantity";
                colvarOrderedIncludeRefundQuantity.DataType = DbType.Int32;
                colvarOrderedIncludeRefundQuantity.MaxLength = 0;
                colvarOrderedIncludeRefundQuantity.AutoIncrement = false;
                colvarOrderedIncludeRefundQuantity.IsNullable = false;
                colvarOrderedIncludeRefundQuantity.IsPrimaryKey = false;
                colvarOrderedIncludeRefundQuantity.IsForeignKey = false;
                colvarOrderedIncludeRefundQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderedIncludeRefundQuantity);
                
                TableSchema.TableColumn colvarIsContinuedQuantity = new TableSchema.TableColumn(schema);
                colvarIsContinuedQuantity.ColumnName = "is_continued_quantity";
                colvarIsContinuedQuantity.DataType = DbType.Boolean;
                colvarIsContinuedQuantity.MaxLength = 0;
                colvarIsContinuedQuantity.AutoIncrement = false;
                colvarIsContinuedQuantity.IsNullable = false;
                colvarIsContinuedQuantity.IsPrimaryKey = false;
                colvarIsContinuedQuantity.IsForeignKey = false;
                colvarIsContinuedQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsContinuedQuantity);
                
                TableSchema.TableColumn colvarContinuedQuantity = new TableSchema.TableColumn(schema);
                colvarContinuedQuantity.ColumnName = "continued_quantity";
                colvarContinuedQuantity.DataType = DbType.Int32;
                colvarContinuedQuantity.MaxLength = 0;
                colvarContinuedQuantity.AutoIncrement = false;
                colvarContinuedQuantity.IsNullable = false;
                colvarContinuedQuantity.IsPrimaryKey = false;
                colvarContinuedQuantity.IsForeignKey = false;
                colvarContinuedQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarContinuedQuantity);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 1;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = false;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemark);
                
                TableSchema.TableColumn colvarIsQuantityMultiplier = new TableSchema.TableColumn(schema);
                colvarIsQuantityMultiplier.ColumnName = "is_quantity_multiplier";
                colvarIsQuantityMultiplier.DataType = DbType.Boolean;
                colvarIsQuantityMultiplier.MaxLength = 0;
                colvarIsQuantityMultiplier.AutoIncrement = false;
                colvarIsQuantityMultiplier.IsNullable = false;
                colvarIsQuantityMultiplier.IsPrimaryKey = false;
                colvarIsQuantityMultiplier.IsForeignKey = false;
                colvarIsQuantityMultiplier.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsQuantityMultiplier);
                
                TableSchema.TableColumn colvarIsAveragePrice = new TableSchema.TableColumn(schema);
                colvarIsAveragePrice.ColumnName = "is_average_price";
                colvarIsAveragePrice.DataType = DbType.Boolean;
                colvarIsAveragePrice.MaxLength = 0;
                colvarIsAveragePrice.AutoIncrement = false;
                colvarIsAveragePrice.IsNullable = false;
                colvarIsAveragePrice.IsPrimaryKey = false;
                colvarIsAveragePrice.IsForeignKey = false;
                colvarIsAveragePrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsAveragePrice);
                
                TableSchema.TableColumn colvarSaleMultipleBase = new TableSchema.TableColumn(schema);
                colvarSaleMultipleBase.ColumnName = "sale_multiple_base";
                colvarSaleMultipleBase.DataType = DbType.Int32;
                colvarSaleMultipleBase.MaxLength = 0;
                colvarSaleMultipleBase.AutoIncrement = false;
                colvarSaleMultipleBase.IsNullable = false;
                colvarSaleMultipleBase.IsPrimaryKey = false;
                colvarSaleMultipleBase.IsForeignKey = false;
                colvarSaleMultipleBase.IsReadOnly = false;
                
                schema.Columns.Add(colvarSaleMultipleBase);
                
                TableSchema.TableColumn colvarPresentQuantity = new TableSchema.TableColumn(schema);
                colvarPresentQuantity.ColumnName = "present_quantity";
                colvarPresentQuantity.DataType = DbType.Int32;
                colvarPresentQuantity.MaxLength = 0;
                colvarPresentQuantity.AutoIncrement = false;
                colvarPresentQuantity.IsNullable = false;
                colvarPresentQuantity.IsPrimaryKey = false;
                colvarPresentQuantity.IsForeignKey = false;
                colvarPresentQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarPresentQuantity);
                
                TableSchema.TableColumn colvarGroupCouponDealType = new TableSchema.TableColumn(schema);
                colvarGroupCouponDealType.ColumnName = "group_coupon_deal_type";
                colvarGroupCouponDealType.DataType = DbType.Int32;
                colvarGroupCouponDealType.MaxLength = 0;
                colvarGroupCouponDealType.AutoIncrement = false;
                colvarGroupCouponDealType.IsNullable = true;
                colvarGroupCouponDealType.IsPrimaryKey = false;
                colvarGroupCouponDealType.IsForeignKey = false;
                colvarGroupCouponDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupCouponDealType);
                
                TableSchema.TableColumn colvarEnableIsp = new TableSchema.TableColumn(schema);
                colvarEnableIsp.ColumnName = "enable_isp";
                colvarEnableIsp.DataType = DbType.Boolean;
                colvarEnableIsp.MaxLength = 0;
                colvarEnableIsp.AutoIncrement = false;
                colvarEnableIsp.IsNullable = false;
                colvarEnableIsp.IsPrimaryKey = false;
                colvarEnableIsp.IsForeignKey = false;
                colvarEnableIsp.IsReadOnly = false;
                
                schema.Columns.Add(colvarEnableIsp);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_combo_deals",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewComboDeal()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewComboDeal(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewComboDeal(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewComboDeal(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("Id");
		    }
            set 
		    {
			    SetColumnValue("Id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("BusinessHourGuid");
		    }
            set 
		    {
			    SetColumnValue("BusinessHourGuid", value);
            }
        }
	      
        [XmlAttribute("MainBusinessHourGuid")]
        [Bindable(true)]
        public Guid? MainBusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("MainBusinessHourGuid");
		    }
            set 
		    {
			    SetColumnValue("MainBusinessHourGuid", value);
            }
        }
	      
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("Title");
		    }
            set 
		    {
			    SetColumnValue("Title", value);
            }
        }
	      
        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public int Sequence 
	    {
		    get
		    {
			    return GetColumnValue<int>("Sequence");
		    }
            set 
		    {
			    SetColumnValue("Sequence", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderMinimum")]
        [Bindable(true)]
        public decimal BusinessHourOrderMinimum 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_order_minimum");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_minimum", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("OrderTotalLimit")]
        [Bindable(true)]
        public decimal? OrderTotalLimit 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("order_total_limit");
		    }
            set 
		    {
			    SetColumnValue("order_total_limit", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("ChangedExpireDate")]
        [Bindable(true)]
        public DateTime? ChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("QuantityMultiplier")]
        [Bindable(true)]
        public int? QuantityMultiplier 
	    {
		    get
		    {
			    return GetColumnValue<int?>("quantity_multiplier");
		    }
            set 
		    {
			    SetColumnValue("quantity_multiplier", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("ItemOrigPrice")]
        [Bindable(true)]
        public decimal ItemOrigPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_orig_price");
		    }
            set 
		    {
			    SetColumnValue("item_orig_price", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("MaxItemCount")]
        [Bindable(true)]
        public int? MaxItemCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("max_item_count");
		    }
            set 
		    {
			    SetColumnValue("max_item_count", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("Slug")]
        [Bindable(true)]
        public string Slug 
	    {
		    get
		    {
			    return GetColumnValue<string>("slug");
		    }
            set 
		    {
			    SetColumnValue("slug", value);
            }
        }
	      
        [XmlAttribute("OrderedQuantity")]
        [Bindable(true)]
        public int OrderedQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("ordered_quantity");
		    }
            set 
		    {
			    SetColumnValue("ordered_quantity", value);
            }
        }
	      
        [XmlAttribute("OrderedIncludeRefundQuantity")]
        [Bindable(true)]
        public int OrderedIncludeRefundQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("ordered_include_refund_quantity");
		    }
            set 
		    {
			    SetColumnValue("ordered_include_refund_quantity", value);
            }
        }
	      
        [XmlAttribute("IsContinuedQuantity")]
        [Bindable(true)]
        public bool IsContinuedQuantity 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_continued_quantity");
		    }
            set 
		    {
			    SetColumnValue("is_continued_quantity", value);
            }
        }
	      
        [XmlAttribute("ContinuedQuantity")]
        [Bindable(true)]
        public int ContinuedQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("continued_quantity");
		    }
            set 
		    {
			    SetColumnValue("continued_quantity", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	      
        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark 
	    {
		    get
		    {
			    return GetColumnValue<string>("remark");
		    }
            set 
		    {
			    SetColumnValue("remark", value);
            }
        }
	      
        [XmlAttribute("IsQuantityMultiplier")]
        [Bindable(true)]
        public bool IsQuantityMultiplier 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_quantity_multiplier");
		    }
            set 
		    {
			    SetColumnValue("is_quantity_multiplier", value);
            }
        }
	      
        [XmlAttribute("IsAveragePrice")]
        [Bindable(true)]
        public bool IsAveragePrice 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_average_price");
		    }
            set 
		    {
			    SetColumnValue("is_average_price", value);
            }
        }
	      
        [XmlAttribute("SaleMultipleBase")]
        [Bindable(true)]
        public int SaleMultipleBase 
	    {
		    get
		    {
			    return GetColumnValue<int>("sale_multiple_base");
		    }
            set 
		    {
			    SetColumnValue("sale_multiple_base", value);
            }
        }
	      
        [XmlAttribute("PresentQuantity")]
        [Bindable(true)]
        public int PresentQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("present_quantity");
		    }
            set 
		    {
			    SetColumnValue("present_quantity", value);
            }
        }
	      
        [XmlAttribute("GroupCouponDealType")]
        [Bindable(true)]
        public int? GroupCouponDealType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_coupon_deal_type");
		    }
            set 
		    {
			    SetColumnValue("group_coupon_deal_type", value);
            }
        }
	      
        [XmlAttribute("EnableIsp")]
        [Bindable(true)]
        public bool EnableIsp 
	    {
		    get
		    {
			    return GetColumnValue<bool>("enable_isp");
		    }
            set 
		    {
			    SetColumnValue("enable_isp", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"Id";
            
            public static string BusinessHourGuid = @"BusinessHourGuid";
            
            public static string MainBusinessHourGuid = @"MainBusinessHourGuid";
            
            public static string Title = @"Title";
            
            public static string Sequence = @"Sequence";
            
            public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string OrderTotalLimit = @"order_total_limit";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
            public static string ChangedExpireDate = @"changed_expire_date";
            
            public static string QuantityMultiplier = @"quantity_multiplier";
            
            public static string UniqueId = @"unique_id";
            
            public static string ItemOrigPrice = @"item_orig_price";
            
            public static string ItemPrice = @"item_price";
            
            public static string MaxItemCount = @"max_item_count";
            
            public static string ItemName = @"item_name";
            
            public static string Slug = @"slug";
            
            public static string OrderedQuantity = @"ordered_quantity";
            
            public static string OrderedIncludeRefundQuantity = @"ordered_include_refund_quantity";
            
            public static string IsContinuedQuantity = @"is_continued_quantity";
            
            public static string ContinuedQuantity = @"continued_quantity";
            
            public static string CouponUsage = @"coupon_usage";
            
            public static string Remark = @"remark";
            
            public static string IsQuantityMultiplier = @"is_quantity_multiplier";
            
            public static string IsAveragePrice = @"is_average_price";
            
            public static string SaleMultipleBase = @"sale_multiple_base";
            
            public static string PresentQuantity = @"present_quantity";
            
            public static string GroupCouponDealType = @"group_coupon_deal_type";
            
            public static string EnableIsp = @"enable_isp";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

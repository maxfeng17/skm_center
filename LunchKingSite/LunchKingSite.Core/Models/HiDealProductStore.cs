using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealProductStore class.
	/// </summary>
    [Serializable]
	public partial class HiDealProductStoreCollection : RepositoryList<HiDealProductStore, HiDealProductStoreCollection>
	{	   
		public HiDealProductStoreCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealProductStoreCollection</returns>
		public HiDealProductStoreCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealProductStore o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_product_store table.
	/// </summary>
	[Serializable]
	public partial class HiDealProductStore : RepositoryRecord<HiDealProductStore>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealProductStore()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealProductStore(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_product_store", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarHiDealProductId = new TableSchema.TableColumn(schema);
				colvarHiDealProductId.ColumnName = "hi_deal_product_id";
				colvarHiDealProductId.DataType = DbType.Int32;
				colvarHiDealProductId.MaxLength = 0;
				colvarHiDealProductId.AutoIncrement = false;
				colvarHiDealProductId.IsNullable = false;
				colvarHiDealProductId.IsPrimaryKey = false;
				colvarHiDealProductId.IsForeignKey = true;
				colvarHiDealProductId.IsReadOnly = false;
				colvarHiDealProductId.DefaultSetting = @"";
				
					colvarHiDealProductId.ForeignKeyTableName = "hi_deal_product";
				schema.Columns.Add(colvarHiDealProductId);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = false;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = true;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				
					colvarStoreGuid.ForeignKeyTableName = "store";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = true;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				colvarSeq.DefaultSetting = @"";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				TableSchema.TableColumn colvarQuantityLimit = new TableSchema.TableColumn(schema);
				colvarQuantityLimit.ColumnName = "quantity_limit";
				colvarQuantityLimit.DataType = DbType.Int32;
				colvarQuantityLimit.MaxLength = 0;
				colvarQuantityLimit.AutoIncrement = false;
				colvarQuantityLimit.IsNullable = true;
				colvarQuantityLimit.IsPrimaryKey = false;
				colvarQuantityLimit.IsForeignKey = false;
				colvarQuantityLimit.IsReadOnly = false;
				colvarQuantityLimit.DefaultSetting = @"";
				colvarQuantityLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQuantityLimit);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 100;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 100;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarResourceGuid = new TableSchema.TableColumn(schema);
				colvarResourceGuid.ColumnName = "resource_guid";
				colvarResourceGuid.DataType = DbType.Guid;
				colvarResourceGuid.MaxLength = 0;
				colvarResourceGuid.AutoIncrement = false;
				colvarResourceGuid.IsNullable = false;
				colvarResourceGuid.IsPrimaryKey = false;
				colvarResourceGuid.IsForeignKey = false;
				colvarResourceGuid.IsReadOnly = false;
				
						colvarResourceGuid.DefaultSetting = @"(newid())";
				colvarResourceGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResourceGuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_product_store",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("HiDealProductId")]
		[Bindable(true)]
		public int HiDealProductId 
		{
			get { return GetColumnValue<int>(Columns.HiDealProductId); }
			set { SetColumnValue(Columns.HiDealProductId, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid StoreGuid 
		{
			get { return GetColumnValue<Guid>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int? Seq 
		{
			get { return GetColumnValue<int?>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		  
		[XmlAttribute("QuantityLimit")]
		[Bindable(true)]
		public int? QuantityLimit 
		{
			get { return GetColumnValue<int?>(Columns.QuantityLimit); }
			set { SetColumnValue(Columns.QuantityLimit, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ResourceGuid")]
		[Bindable(true)]
		public Guid ResourceGuid 
		{
			get { return GetColumnValue<Guid>(Columns.ResourceGuid); }
			set { SetColumnValue(Columns.ResourceGuid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn HiDealProductIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn QuantityLimitColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ResourceGuidColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string HiDealProductId = @"hi_deal_product_id";
			 public static string StoreGuid = @"store_guid";
			 public static string Seq = @"seq";
			 public static string QuantityLimit = @"quantity_limit";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string ResourceGuid = @"resource_guid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

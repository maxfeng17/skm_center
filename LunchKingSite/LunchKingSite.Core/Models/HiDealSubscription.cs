using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealSubscription class.
	/// </summary>
    [Serializable]
	public partial class HiDealSubscriptionCollection : RepositoryList<HiDealSubscription, HiDealSubscriptionCollection>
	{	   
		public HiDealSubscriptionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealSubscriptionCollection</returns>
		public HiDealSubscriptionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealSubscription o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_subscription table.
	/// </summary>
	[Serializable]
	public partial class HiDealSubscription : RepositoryRecord<HiDealSubscription>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealSubscription()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealSubscription(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_subscription", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCity = new TableSchema.TableColumn(schema);
				colvarCity.ColumnName = "city";
				colvarCity.DataType = DbType.Int32;
				colvarCity.MaxLength = 0;
				colvarCity.AutoIncrement = false;
				colvarCity.IsNullable = true;
				colvarCity.IsPrimaryKey = false;
				colvarCity.IsForeignKey = false;
				colvarCity.IsReadOnly = false;
				colvarCity.DefaultSetting = @"";
				colvarCity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCity);
				
				TableSchema.TableColumn colvarRegionCodeGroup = new TableSchema.TableColumn(schema);
				colvarRegionCodeGroup.ColumnName = "region_code_group";
				colvarRegionCodeGroup.DataType = DbType.AnsiString;
				colvarRegionCodeGroup.MaxLength = 50;
				colvarRegionCodeGroup.AutoIncrement = false;
				colvarRegionCodeGroup.IsNullable = true;
				colvarRegionCodeGroup.IsPrimaryKey = false;
				colvarRegionCodeGroup.IsForeignKey = false;
				colvarRegionCodeGroup.IsReadOnly = false;
				colvarRegionCodeGroup.DefaultSetting = @"";
				colvarRegionCodeGroup.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRegionCodeGroup);
				
				TableSchema.TableColumn colvarRegionCodeId = new TableSchema.TableColumn(schema);
				colvarRegionCodeId.ColumnName = "region_code_id";
				colvarRegionCodeId.DataType = DbType.Int32;
				colvarRegionCodeId.MaxLength = 0;
				colvarRegionCodeId.AutoIncrement = false;
				colvarRegionCodeId.IsNullable = true;
				colvarRegionCodeId.IsPrimaryKey = false;
				colvarRegionCodeId.IsForeignKey = false;
				colvarRegionCodeId.IsReadOnly = false;
				colvarRegionCodeId.DefaultSetting = @"";
				colvarRegionCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRegionCodeId);
				
				TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
				colvarCategory.ColumnName = "category";
				colvarCategory.DataType = DbType.Int32;
				colvarCategory.MaxLength = 0;
				colvarCategory.AutoIncrement = false;
				colvarCategory.IsNullable = true;
				colvarCategory.IsPrimaryKey = false;
				colvarCategory.IsForeignKey = false;
				colvarCategory.IsReadOnly = false;
				colvarCategory.DefaultSetting = @"";
				colvarCategory.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategory);
				
				TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
				colvarEmail.ColumnName = "email";
				colvarEmail.DataType = DbType.String;
				colvarEmail.MaxLength = 250;
				colvarEmail.AutoIncrement = false;
				colvarEmail.IsNullable = true;
				colvarEmail.IsPrimaryKey = false;
				colvarEmail.IsForeignKey = false;
				colvarEmail.IsReadOnly = false;
				colvarEmail.DefaultSetting = @"";
				colvarEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmail);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCancelTime = new TableSchema.TableColumn(schema);
				colvarCancelTime.ColumnName = "cancel_time";
				colvarCancelTime.DataType = DbType.DateTime;
				colvarCancelTime.MaxLength = 0;
				colvarCancelTime.AutoIncrement = false;
				colvarCancelTime.IsNullable = true;
				colvarCancelTime.IsPrimaryKey = false;
				colvarCancelTime.IsForeignKey = false;
				colvarCancelTime.IsReadOnly = false;
				colvarCancelTime.DefaultSetting = @"";
				colvarCancelTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelTime);
				
				TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
				colvarFlag.ColumnName = "flag";
				colvarFlag.DataType = DbType.Int32;
				colvarFlag.MaxLength = 0;
				colvarFlag.AutoIncrement = false;
				colvarFlag.IsNullable = true;
				colvarFlag.IsPrimaryKey = false;
				colvarFlag.IsForeignKey = false;
				colvarFlag.IsReadOnly = false;
				colvarFlag.DefaultSetting = @"";
				colvarFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFlag);
				
				TableSchema.TableColumn colvarReliable = new TableSchema.TableColumn(schema);
				colvarReliable.ColumnName = "reliable";
				colvarReliable.DataType = DbType.Boolean;
				colvarReliable.MaxLength = 0;
				colvarReliable.AutoIncrement = false;
				colvarReliable.IsNullable = true;
				colvarReliable.IsPrimaryKey = false;
				colvarReliable.IsForeignKey = false;
				colvarReliable.IsReadOnly = false;
				colvarReliable.DefaultSetting = @"";
				colvarReliable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReliable);
				
				TableSchema.TableColumn colvarVisaMemberType = new TableSchema.TableColumn(schema);
				colvarVisaMemberType.ColumnName = "visa_member_type";
				colvarVisaMemberType.DataType = DbType.Int32;
				colvarVisaMemberType.MaxLength = 0;
				colvarVisaMemberType.AutoIncrement = false;
				colvarVisaMemberType.IsNullable = false;
				colvarVisaMemberType.IsPrimaryKey = false;
				colvarVisaMemberType.IsForeignKey = false;
				colvarVisaMemberType.IsReadOnly = false;
				
						colvarVisaMemberType.DefaultSetting = @"((0))";
				colvarVisaMemberType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVisaMemberType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_subscription",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("City")]
		[Bindable(true)]
		public int? City 
		{
			get { return GetColumnValue<int?>(Columns.City); }
			set { SetColumnValue(Columns.City, value); }
		}
		  
		[XmlAttribute("RegionCodeGroup")]
		[Bindable(true)]
		public string RegionCodeGroup 
		{
			get { return GetColumnValue<string>(Columns.RegionCodeGroup); }
			set { SetColumnValue(Columns.RegionCodeGroup, value); }
		}
		  
		[XmlAttribute("RegionCodeId")]
		[Bindable(true)]
		public int? RegionCodeId 
		{
			get { return GetColumnValue<int?>(Columns.RegionCodeId); }
			set { SetColumnValue(Columns.RegionCodeId, value); }
		}
		  
		[XmlAttribute("Category")]
		[Bindable(true)]
		public int? Category 
		{
			get { return GetColumnValue<int?>(Columns.Category); }
			set { SetColumnValue(Columns.Category, value); }
		}
		  
		[XmlAttribute("Email")]
		[Bindable(true)]
		public string Email 
		{
			get { return GetColumnValue<string>(Columns.Email); }
			set { SetColumnValue(Columns.Email, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CancelTime")]
		[Bindable(true)]
		public DateTime? CancelTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CancelTime); }
			set { SetColumnValue(Columns.CancelTime, value); }
		}
		  
		[XmlAttribute("Flag")]
		[Bindable(true)]
		public int? Flag 
		{
			get { return GetColumnValue<int?>(Columns.Flag); }
			set { SetColumnValue(Columns.Flag, value); }
		}
		  
		[XmlAttribute("Reliable")]
		[Bindable(true)]
		public bool? Reliable 
		{
			get { return GetColumnValue<bool?>(Columns.Reliable); }
			set { SetColumnValue(Columns.Reliable, value); }
		}
		  
		[XmlAttribute("VisaMemberType")]
		[Bindable(true)]
		public int VisaMemberType 
		{
			get { return GetColumnValue<int>(Columns.VisaMemberType); }
			set { SetColumnValue(Columns.VisaMemberType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CityColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn RegionCodeGroupColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RegionCodeIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn EmailColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn FlagColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ReliableColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn VisaMemberTypeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string City = @"city";
			 public static string RegionCodeGroup = @"region_code_group";
			 public static string RegionCodeId = @"region_code_id";
			 public static string Category = @"category";
			 public static string Email = @"email";
			 public static string CreateTime = @"create_time";
			 public static string CancelTime = @"cancel_time";
			 public static string Flag = @"flag";
			 public static string Reliable = @"reliable";
			 public static string VisaMemberType = @"visa_member_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

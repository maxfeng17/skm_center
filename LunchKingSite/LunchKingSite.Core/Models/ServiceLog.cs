using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ServiceLog class.
	/// </summary>
    [Serializable]
	public partial class ServiceLogCollection : RepositoryList<ServiceLog, ServiceLogCollection>
	{	   
		public ServiceLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ServiceLogCollection</returns>
		public ServiceLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ServiceLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the service_log table.
	/// </summary>
	[Serializable]
	public partial class ServiceLog : RepositoryRecord<ServiceLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ServiceLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ServiceLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("service_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSendTo = new TableSchema.TableColumn(schema);
				colvarSendTo.ColumnName = "send_to";
				colvarSendTo.DataType = DbType.String;
				colvarSendTo.MaxLength = 100;
				colvarSendTo.AutoIncrement = false;
				colvarSendTo.IsNullable = true;
				colvarSendTo.IsPrimaryKey = false;
				colvarSendTo.IsForeignKey = false;
				colvarSendTo.IsReadOnly = false;
				colvarSendTo.DefaultSetting = @"";
				colvarSendTo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendTo);
				
				TableSchema.TableColumn colvarRefX = new TableSchema.TableColumn(schema);
				colvarRefX.ColumnName = "ref";
				colvarRefX.DataType = DbType.Int32;
				colvarRefX.MaxLength = 0;
				colvarRefX.AutoIncrement = false;
				colvarRefX.IsNullable = false;
				colvarRefX.IsPrimaryKey = false;
				colvarRefX.IsForeignKey = false;
				colvarRefX.IsReadOnly = false;
				colvarRefX.DefaultSetting = @"";
				colvarRefX.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefX);
				
				TableSchema.TableColumn colvarSendType = new TableSchema.TableColumn(schema);
				colvarSendType.ColumnName = "send_type";
				colvarSendType.DataType = DbType.String;
				colvarSendType.MaxLength = 20;
				colvarSendType.AutoIncrement = false;
				colvarSendType.IsNullable = true;
				colvarSendType.IsPrimaryKey = false;
				colvarSendType.IsForeignKey = false;
				colvarSendType.IsReadOnly = false;
				colvarSendType.DefaultSetting = @"";
				colvarSendType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendType);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 1073741823;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
				colvarRemark.ColumnName = "remark";
				colvarRemark.DataType = DbType.String;
				colvarRemark.MaxLength = 1073741823;
				colvarRemark.AutoIncrement = false;
				colvarRemark.IsNullable = true;
				colvarRemark.IsPrimaryKey = false;
				colvarRemark.IsForeignKey = false;
				colvarRemark.IsReadOnly = false;
				colvarRemark.DefaultSetting = @"";
				colvarRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemark);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 100;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarRemarkSeller = new TableSchema.TableColumn(schema);
				colvarRemarkSeller.ColumnName = "remark_seller";
				colvarRemarkSeller.DataType = DbType.String;
				colvarRemarkSeller.MaxLength = 1073741823;
				colvarRemarkSeller.AutoIncrement = false;
				colvarRemarkSeller.IsNullable = true;
				colvarRemarkSeller.IsPrimaryKey = false;
				colvarRemarkSeller.IsForeignKey = false;
				colvarRemarkSeller.IsReadOnly = false;
				colvarRemarkSeller.DefaultSetting = @"";
				colvarRemarkSeller.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemarkSeller);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("service_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SendTo")]
		[Bindable(true)]
		public string SendTo 
		{
			get { return GetColumnValue<string>(Columns.SendTo); }
			set { SetColumnValue(Columns.SendTo, value); }
		}
		  
		[XmlAttribute("RefX")]
		[Bindable(true)]
		public int RefX 
		{
			get { return GetColumnValue<int>(Columns.RefX); }
			set { SetColumnValue(Columns.RefX, value); }
		}
		  
		[XmlAttribute("SendType")]
		[Bindable(true)]
		public string SendType 
		{
			get { return GetColumnValue<string>(Columns.SendType); }
			set { SetColumnValue(Columns.SendType, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("Remark")]
		[Bindable(true)]
		public string Remark 
		{
			get { return GetColumnValue<string>(Columns.Remark); }
			set { SetColumnValue(Columns.Remark, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("RemarkSeller")]
		[Bindable(true)]
		public string RemarkSeller 
		{
			get { return GetColumnValue<string>(Columns.RemarkSeller); }
			set { SetColumnValue(Columns.RemarkSeller, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SendToColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn RefXColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SendTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn RemarkSellerColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SendTo = @"send_to";
			 public static string RefX = @"ref";
			 public static string SendType = @"send_type";
			 public static string Message = @"message";
			 public static string Remark = @"remark";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string RemarkSeller = @"remark_seller";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CvsStore class.
	/// </summary>
    [Serializable]
	public partial class CvsStoreCollection : RepositoryList<CvsStore, CvsStoreCollection>
	{	   
		public CvsStoreCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CvsStoreCollection</returns>
		public CvsStoreCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CvsStore o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the cvs_store table.
	/// </summary>
	[Serializable]
	public partial class CvsStore : RepositoryRecord<CvsStore>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CvsStore()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CvsStore(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("cvs_store", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCvsType = new TableSchema.TableColumn(schema);
				colvarCvsType.ColumnName = "cvs_type";
				colvarCvsType.DataType = DbType.Int32;
				colvarCvsType.MaxLength = 0;
				colvarCvsType.AutoIncrement = false;
				colvarCvsType.IsNullable = false;
				colvarCvsType.IsPrimaryKey = false;
				colvarCvsType.IsForeignKey = false;
				colvarCvsType.IsReadOnly = false;
				colvarCvsType.DefaultSetting = @"";
				colvarCvsType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCvsType);
				
				TableSchema.TableColumn colvarCvsStoreId = new TableSchema.TableColumn(schema);
				colvarCvsStoreId.ColumnName = "cvs_store_id";
				colvarCvsStoreId.DataType = DbType.String;
				colvarCvsStoreId.MaxLength = 30;
				colvarCvsStoreId.AutoIncrement = false;
				colvarCvsStoreId.IsNullable = false;
				colvarCvsStoreId.IsPrimaryKey = false;
				colvarCvsStoreId.IsForeignKey = false;
				colvarCvsStoreId.IsReadOnly = false;
				colvarCvsStoreId.DefaultSetting = @"";
				colvarCvsStoreId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCvsStoreId);
				
				TableSchema.TableColumn colvarCvsStoreName = new TableSchema.TableColumn(schema);
				colvarCvsStoreName.ColumnName = "cvs_store_name";
				colvarCvsStoreName.DataType = DbType.String;
				colvarCvsStoreName.MaxLength = 30;
				colvarCvsStoreName.AutoIncrement = false;
				colvarCvsStoreName.IsNullable = true;
				colvarCvsStoreName.IsPrimaryKey = false;
				colvarCvsStoreName.IsForeignKey = false;
				colvarCvsStoreName.IsReadOnly = false;
				colvarCvsStoreName.DefaultSetting = @"";
				colvarCvsStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCvsStoreName);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = true;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = true;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				
					colvarCityId.ForeignKeyTableName = "city";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarCvsStoreAddress = new TableSchema.TableColumn(schema);
				colvarCvsStoreAddress.ColumnName = "cvs_store_address";
				colvarCvsStoreAddress.DataType = DbType.String;
				colvarCvsStoreAddress.MaxLength = 100;
				colvarCvsStoreAddress.AutoIncrement = false;
				colvarCvsStoreAddress.IsNullable = true;
				colvarCvsStoreAddress.IsPrimaryKey = false;
				colvarCvsStoreAddress.IsForeignKey = false;
				colvarCvsStoreAddress.IsReadOnly = false;
				colvarCvsStoreAddress.DefaultSetting = @"";
				colvarCvsStoreAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCvsStoreAddress);
				
				TableSchema.TableColumn colvarCvsStoreTel = new TableSchema.TableColumn(schema);
				colvarCvsStoreTel.ColumnName = "cvs_store_tel";
				colvarCvsStoreTel.DataType = DbType.AnsiString;
				colvarCvsStoreTel.MaxLength = 100;
				colvarCvsStoreTel.AutoIncrement = false;
				colvarCvsStoreTel.IsNullable = true;
				colvarCvsStoreTel.IsPrimaryKey = false;
				colvarCvsStoreTel.IsForeignKey = false;
				colvarCvsStoreTel.IsReadOnly = false;
				colvarCvsStoreTel.DefaultSetting = @"";
				colvarCvsStoreTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCvsStoreTel);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("cvs_store",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CvsType")]
		[Bindable(true)]
		public int CvsType 
		{
			get { return GetColumnValue<int>(Columns.CvsType); }
			set { SetColumnValue(Columns.CvsType, value); }
		}
		  
		[XmlAttribute("CvsStoreId")]
		[Bindable(true)]
		public string CvsStoreId 
		{
			get { return GetColumnValue<string>(Columns.CvsStoreId); }
			set { SetColumnValue(Columns.CvsStoreId, value); }
		}
		  
		[XmlAttribute("CvsStoreName")]
		[Bindable(true)]
		public string CvsStoreName 
		{
			get { return GetColumnValue<string>(Columns.CvsStoreName); }
			set { SetColumnValue(Columns.CvsStoreName, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int? CityId 
		{
			get { return GetColumnValue<int?>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("CvsStoreAddress")]
		[Bindable(true)]
		public string CvsStoreAddress 
		{
			get { return GetColumnValue<string>(Columns.CvsStoreAddress); }
			set { SetColumnValue(Columns.CvsStoreAddress, value); }
		}
		  
		[XmlAttribute("CvsStoreTel")]
		[Bindable(true)]
		public string CvsStoreTel 
		{
			get { return GetColumnValue<string>(Columns.CvsStoreTel); }
			set { SetColumnValue(Columns.CvsStoreTel, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CvsTypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CvsStoreIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CvsStoreNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CvsStoreAddressColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CvsStoreTelColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CvsType = @"cvs_type";
			 public static string CvsStoreId = @"cvs_store_id";
			 public static string CvsStoreName = @"cvs_store_name";
			 public static string CityId = @"city_id";
			 public static string CvsStoreAddress = @"cvs_store_address";
			 public static string CvsStoreTel = @"cvs_store_tel";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

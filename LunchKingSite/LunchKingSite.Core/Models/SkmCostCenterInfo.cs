using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmCostCenterInfo class.
	/// </summary>
    [Serializable]
	public partial class SkmCostCenterInfoCollection : RepositoryList<SkmCostCenterInfo, SkmCostCenterInfoCollection>
	{	   
		public SkmCostCenterInfoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmCostCenterInfoCollection</returns>
		public SkmCostCenterInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmCostCenterInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_cost_center_info table.
	/// </summary>
	
	[Serializable]
	public partial class SkmCostCenterInfo : RepositoryRecord<SkmCostCenterInfo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmCostCenterInfo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmCostCenterInfo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_cost_center_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarStoreCode = new TableSchema.TableColumn(schema);
				colvarStoreCode.ColumnName = "store_code";
				colvarStoreCode.DataType = DbType.AnsiString;
				colvarStoreCode.MaxLength = 4;
				colvarStoreCode.AutoIncrement = false;
				colvarStoreCode.IsNullable = false;
				colvarStoreCode.IsPrimaryKey = false;
				colvarStoreCode.IsForeignKey = false;
				colvarStoreCode.IsReadOnly = false;
				colvarStoreCode.DefaultSetting = @"";
				colvarStoreCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreCode);
				
				TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
				colvarStoreName.ColumnName = "store_name";
				colvarStoreName.DataType = DbType.String;
				colvarStoreName.MaxLength = 50;
				colvarStoreName.AutoIncrement = false;
				colvarStoreName.IsNullable = false;
				colvarStoreName.IsPrimaryKey = false;
				colvarStoreName.IsForeignKey = false;
				colvarStoreName.IsReadOnly = false;
				colvarStoreName.DefaultSetting = @"";
				colvarStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreName);
				
				TableSchema.TableColumn colvarCostCenterCode = new TableSchema.TableColumn(schema);
				colvarCostCenterCode.ColumnName = "cost_center_code";
				colvarCostCenterCode.DataType = DbType.AnsiString;
				colvarCostCenterCode.MaxLength = 20;
				colvarCostCenterCode.AutoIncrement = false;
				colvarCostCenterCode.IsNullable = true;
				colvarCostCenterCode.IsPrimaryKey = false;
				colvarCostCenterCode.IsForeignKey = false;
				colvarCostCenterCode.IsReadOnly = false;
				colvarCostCenterCode.DefaultSetting = @"";
				colvarCostCenterCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostCenterCode);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_cost_center_info",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("StoreCode")]
		[Bindable(true)]
		public string StoreCode 
		{
			get { return GetColumnValue<string>(Columns.StoreCode); }
			set { SetColumnValue(Columns.StoreCode, value); }
		}
		
		[XmlAttribute("StoreName")]
		[Bindable(true)]
		public string StoreName 
		{
			get { return GetColumnValue<string>(Columns.StoreName); }
			set { SetColumnValue(Columns.StoreName, value); }
		}
		
		[XmlAttribute("CostCenterCode")]
		[Bindable(true)]
		public string CostCenterCode 
		{
			get { return GetColumnValue<string>(Columns.CostCenterCode); }
			set { SetColumnValue(Columns.CostCenterCode, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreCodeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CostCenterCodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string StoreCode = @"store_code";
			 public static string StoreName = @"store_name";
			 public static string CostCenterCode = @"cost_center_code";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class BusinessChangeLogCollection : RepositoryList<BusinessChangeLog, BusinessChangeLogCollection>
	{
			public BusinessChangeLogCollection() {}

			public BusinessChangeLogCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							BusinessChangeLog o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class BusinessChangeLog : RepositoryRecord<BusinessChangeLog>, IRecordBase
	{
		#region .ctors and Default Settings
		public BusinessChangeLog()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public BusinessChangeLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("business_change_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);

				TableSchema.TableColumn colvarChangeLog = new TableSchema.TableColumn(schema);
				colvarChangeLog.ColumnName = "change_log";
				colvarChangeLog.DataType = DbType.String;
				colvarChangeLog.MaxLength = 2147483647;
				colvarChangeLog.AutoIncrement = false;
				colvarChangeLog.IsNullable = false;
				colvarChangeLog.IsPrimaryKey = false;
				colvarChangeLog.IsForeignKey = false;
				colvarChangeLog.IsReadOnly = false;
				colvarChangeLog.DefaultSetting = @"";
				colvarChangeLog.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChangeLog);

				TableSchema.TableColumn colvarDept = new TableSchema.TableColumn(schema);
				colvarDept.ColumnName = "dept";
				colvarDept.DataType = DbType.String;
				colvarDept.MaxLength = 250;
				colvarDept.AutoIncrement = false;
				colvarDept.IsNullable = false;
				colvarDept.IsPrimaryKey = false;
				colvarDept.IsForeignKey = false;
				colvarDept.IsReadOnly = false;
				colvarDept.DefaultSetting = @"";
				colvarDept.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDept);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 250;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("business_change_log",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}

		[XmlAttribute("ChangeLog")]
		[Bindable(true)]
		public string ChangeLog
		{
			get { return GetColumnValue<string>(Columns.ChangeLog); }
			set { SetColumnValue(Columns.ChangeLog, value); }
		}

		[XmlAttribute("Dept")]
		[Bindable(true)]
		public string Dept
		{
			get { return GetColumnValue<string>(Columns.Dept); }
			set { SetColumnValue(Columns.Dept, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn BusinessHourGuidColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn ChangeLogColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn DeptColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[5]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string BusinessHourGuid = @"business_hour_guid";
			public static string ChangeLog = @"change_log";
			public static string Dept = @"dept";
			public static string CreateId = @"create_id";
			public static string CreateTime = @"create_time";
		}

		#endregion

	}
}

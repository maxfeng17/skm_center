using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewSellerCityBizHour class.
    /// </summary>
    [Serializable]
    public partial class ViewSellerCityBizHourCollection : ReadOnlyList<ViewSellerCityBizHour, ViewSellerCityBizHourCollection>
    {        
        public ViewSellerCityBizHourCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_seller_city_biz_hour view.
    /// </summary>
    [Serializable]
    public partial class ViewSellerCityBizHour : ReadOnlyRecord<ViewSellerCityBizHour>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_seller_city_biz_hour", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 20;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = false;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 10;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerBossName = new TableSchema.TableColumn(schema);
                colvarSellerBossName.ColumnName = "seller_boss_name";
                colvarSellerBossName.DataType = DbType.String;
                colvarSellerBossName.MaxLength = 30;
                colvarSellerBossName.AutoIncrement = false;
                colvarSellerBossName.IsNullable = true;
                colvarSellerBossName.IsPrimaryKey = false;
                colvarSellerBossName.IsForeignKey = false;
                colvarSellerBossName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerBossName);
                
                TableSchema.TableColumn colvarSellerTel = new TableSchema.TableColumn(schema);
                colvarSellerTel.ColumnName = "seller_tel";
                colvarSellerTel.DataType = DbType.AnsiString;
                colvarSellerTel.MaxLength = 100;
                colvarSellerTel.AutoIncrement = false;
                colvarSellerTel.IsNullable = true;
                colvarSellerTel.IsPrimaryKey = false;
                colvarSellerTel.IsForeignKey = false;
                colvarSellerTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerTel);
                
                TableSchema.TableColumn colvarSellerTel2 = new TableSchema.TableColumn(schema);
                colvarSellerTel2.ColumnName = "seller_tel2";
                colvarSellerTel2.DataType = DbType.AnsiString;
                colvarSellerTel2.MaxLength = 100;
                colvarSellerTel2.AutoIncrement = false;
                colvarSellerTel2.IsNullable = true;
                colvarSellerTel2.IsPrimaryKey = false;
                colvarSellerTel2.IsForeignKey = false;
                colvarSellerTel2.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerTel2);
                
                TableSchema.TableColumn colvarSellerFax = new TableSchema.TableColumn(schema);
                colvarSellerFax.ColumnName = "seller_fax";
                colvarSellerFax.DataType = DbType.AnsiString;
                colvarSellerFax.MaxLength = 20;
                colvarSellerFax.AutoIncrement = false;
                colvarSellerFax.IsNullable = true;
                colvarSellerFax.IsPrimaryKey = false;
                colvarSellerFax.IsForeignKey = false;
                colvarSellerFax.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerFax);
                
                TableSchema.TableColumn colvarSellerMobile = new TableSchema.TableColumn(schema);
                colvarSellerMobile.ColumnName = "seller_mobile";
                colvarSellerMobile.DataType = DbType.AnsiString;
                colvarSellerMobile.MaxLength = 100;
                colvarSellerMobile.AutoIncrement = false;
                colvarSellerMobile.IsNullable = true;
                colvarSellerMobile.IsPrimaryKey = false;
                colvarSellerMobile.IsForeignKey = false;
                colvarSellerMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMobile);
                
                TableSchema.TableColumn colvarSellerAddress = new TableSchema.TableColumn(schema);
                colvarSellerAddress.ColumnName = "seller_address";
                colvarSellerAddress.DataType = DbType.String;
                colvarSellerAddress.MaxLength = 100;
                colvarSellerAddress.AutoIncrement = false;
                colvarSellerAddress.IsNullable = true;
                colvarSellerAddress.IsPrimaryKey = false;
                colvarSellerAddress.IsForeignKey = false;
                colvarSellerAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerAddress);
                
                TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
                colvarSellerEmail.ColumnName = "seller_email";
                colvarSellerEmail.DataType = DbType.AnsiString;
                colvarSellerEmail.MaxLength = 200;
                colvarSellerEmail.AutoIncrement = false;
                colvarSellerEmail.IsNullable = true;
                colvarSellerEmail.IsPrimaryKey = false;
                colvarSellerEmail.IsForeignKey = false;
                colvarSellerEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerEmail);
                
                TableSchema.TableColumn colvarSellerBlog = new TableSchema.TableColumn(schema);
                colvarSellerBlog.ColumnName = "seller_blog";
                colvarSellerBlog.DataType = DbType.AnsiString;
                colvarSellerBlog.MaxLength = 100;
                colvarSellerBlog.AutoIncrement = false;
                colvarSellerBlog.IsNullable = true;
                colvarSellerBlog.IsPrimaryKey = false;
                colvarSellerBlog.IsForeignKey = false;
                colvarSellerBlog.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerBlog);
                
                TableSchema.TableColumn colvarSellerInvoice = new TableSchema.TableColumn(schema);
                colvarSellerInvoice.ColumnName = "seller_invoice";
                colvarSellerInvoice.DataType = DbType.String;
                colvarSellerInvoice.MaxLength = 50;
                colvarSellerInvoice.AutoIncrement = false;
                colvarSellerInvoice.IsNullable = true;
                colvarSellerInvoice.IsPrimaryKey = false;
                colvarSellerInvoice.IsForeignKey = false;
                colvarSellerInvoice.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerInvoice);
                
                TableSchema.TableColumn colvarSellerDescription = new TableSchema.TableColumn(schema);
                colvarSellerDescription.ColumnName = "seller_description";
                colvarSellerDescription.DataType = DbType.String;
                colvarSellerDescription.MaxLength = 1073741823;
                colvarSellerDescription.AutoIncrement = false;
                colvarSellerDescription.IsNullable = true;
                colvarSellerDescription.IsPrimaryKey = false;
                colvarSellerDescription.IsForeignKey = false;
                colvarSellerDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerDescription);
                
                TableSchema.TableColumn colvarSellerStatus = new TableSchema.TableColumn(schema);
                colvarSellerStatus.ColumnName = "seller_status";
                colvarSellerStatus.DataType = DbType.Int32;
                colvarSellerStatus.MaxLength = 0;
                colvarSellerStatus.AutoIncrement = false;
                colvarSellerStatus.IsNullable = false;
                colvarSellerStatus.IsPrimaryKey = false;
                colvarSellerStatus.IsForeignKey = false;
                colvarSellerStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerStatus);
                
                TableSchema.TableColumn colvarSellerRemark = new TableSchema.TableColumn(schema);
                colvarSellerRemark.ColumnName = "seller_remark";
                colvarSellerRemark.DataType = DbType.String;
                colvarSellerRemark.MaxLength = 200;
                colvarSellerRemark.AutoIncrement = false;
                colvarSellerRemark.IsNullable = true;
                colvarSellerRemark.IsPrimaryKey = false;
                colvarSellerRemark.IsForeignKey = false;
                colvarSellerRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerRemark);
                
                TableSchema.TableColumn colvarSellerSales = new TableSchema.TableColumn(schema);
                colvarSellerSales.ColumnName = "seller_sales";
                colvarSellerSales.DataType = DbType.String;
                colvarSellerSales.MaxLength = 50;
                colvarSellerSales.AutoIncrement = false;
                colvarSellerSales.IsNullable = true;
                colvarSellerSales.IsPrimaryKey = false;
                colvarSellerSales.IsForeignKey = false;
                colvarSellerSales.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerSales);
                
                TableSchema.TableColumn colvarSellerLogoimgPath = new TableSchema.TableColumn(schema);
                colvarSellerLogoimgPath.ColumnName = "seller_logoimg_path";
                colvarSellerLogoimgPath.DataType = DbType.AnsiString;
                colvarSellerLogoimgPath.MaxLength = 500;
                colvarSellerLogoimgPath.AutoIncrement = false;
                colvarSellerLogoimgPath.IsNullable = true;
                colvarSellerLogoimgPath.IsPrimaryKey = false;
                colvarSellerLogoimgPath.IsForeignKey = false;
                colvarSellerLogoimgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerLogoimgPath);
                
                TableSchema.TableColumn colvarSellerVideoPath = new TableSchema.TableColumn(schema);
                colvarSellerVideoPath.ColumnName = "seller_video_path";
                colvarSellerVideoPath.DataType = DbType.AnsiString;
                colvarSellerVideoPath.MaxLength = 100;
                colvarSellerVideoPath.AutoIncrement = false;
                colvarSellerVideoPath.IsNullable = true;
                colvarSellerVideoPath.IsPrimaryKey = false;
                colvarSellerVideoPath.IsForeignKey = false;
                colvarSellerVideoPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerVideoPath);
                
                TableSchema.TableColumn colvarSellerCreateId = new TableSchema.TableColumn(schema);
                colvarSellerCreateId.ColumnName = "seller_create_id";
                colvarSellerCreateId.DataType = DbType.String;
                colvarSellerCreateId.MaxLength = 30;
                colvarSellerCreateId.AutoIncrement = false;
                colvarSellerCreateId.IsNullable = false;
                colvarSellerCreateId.IsPrimaryKey = false;
                colvarSellerCreateId.IsForeignKey = false;
                colvarSellerCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCreateId);
                
                TableSchema.TableColumn colvarSellerCreateTime = new TableSchema.TableColumn(schema);
                colvarSellerCreateTime.ColumnName = "seller_create_time";
                colvarSellerCreateTime.DataType = DbType.DateTime;
                colvarSellerCreateTime.MaxLength = 0;
                colvarSellerCreateTime.AutoIncrement = false;
                colvarSellerCreateTime.IsNullable = false;
                colvarSellerCreateTime.IsPrimaryKey = false;
                colvarSellerCreateTime.IsForeignKey = false;
                colvarSellerCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCreateTime);
                
                TableSchema.TableColumn colvarSellerModifyId = new TableSchema.TableColumn(schema);
                colvarSellerModifyId.ColumnName = "seller_modify_id";
                colvarSellerModifyId.DataType = DbType.String;
                colvarSellerModifyId.MaxLength = 30;
                colvarSellerModifyId.AutoIncrement = false;
                colvarSellerModifyId.IsNullable = true;
                colvarSellerModifyId.IsPrimaryKey = false;
                colvarSellerModifyId.IsForeignKey = false;
                colvarSellerModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerModifyId);
                
                TableSchema.TableColumn colvarSellerModifyTime = new TableSchema.TableColumn(schema);
                colvarSellerModifyTime.ColumnName = "seller_modify_time";
                colvarSellerModifyTime.DataType = DbType.DateTime;
                colvarSellerModifyTime.MaxLength = 0;
                colvarSellerModifyTime.AutoIncrement = false;
                colvarSellerModifyTime.IsNullable = true;
                colvarSellerModifyTime.IsPrimaryKey = false;
                colvarSellerModifyTime.IsForeignKey = false;
                colvarSellerModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerModifyTime);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarBusinessHourId = new TableSchema.TableColumn(schema);
                colvarBusinessHourId.ColumnName = "business_hour_id";
                colvarBusinessHourId.DataType = DbType.AnsiString;
                colvarBusinessHourId.MaxLength = 20;
                colvarBusinessHourId.AutoIncrement = false;
                colvarBusinessHourId.IsNullable = true;
                colvarBusinessHourId.IsPrimaryKey = false;
                colvarBusinessHourId.IsForeignKey = false;
                colvarBusinessHourId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourId);
                
                TableSchema.TableColumn colvarBusinessHourTypeId = new TableSchema.TableColumn(schema);
                colvarBusinessHourTypeId.ColumnName = "business_hour_type_id";
                colvarBusinessHourTypeId.DataType = DbType.Int32;
                colvarBusinessHourTypeId.MaxLength = 0;
                colvarBusinessHourTypeId.AutoIncrement = false;
                colvarBusinessHourTypeId.IsNullable = false;
                colvarBusinessHourTypeId.IsPrimaryKey = false;
                colvarBusinessHourTypeId.IsForeignKey = false;
                colvarBusinessHourTypeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourTypeId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
                colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeS.MaxLength = 0;
                colvarBusinessHourDeliverTimeS.AutoIncrement = false;
                colvarBusinessHourDeliverTimeS.IsNullable = true;
                colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeS.IsForeignKey = false;
                colvarBusinessHourDeliverTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeS);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliveryCharge = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliveryCharge.ColumnName = "business_hour_delivery_charge";
                colvarBusinessHourDeliveryCharge.DataType = DbType.Currency;
                colvarBusinessHourDeliveryCharge.MaxLength = 0;
                colvarBusinessHourDeliveryCharge.AutoIncrement = false;
                colvarBusinessHourDeliveryCharge.IsNullable = false;
                colvarBusinessHourDeliveryCharge.IsPrimaryKey = false;
                colvarBusinessHourDeliveryCharge.IsForeignKey = false;
                colvarBusinessHourDeliveryCharge.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliveryCharge);
                
                TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
                colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
                colvarBusinessHourOrderMinimum.MaxLength = 0;
                colvarBusinessHourOrderMinimum.AutoIncrement = false;
                colvarBusinessHourOrderMinimum.IsNullable = false;
                colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
                colvarBusinessHourOrderMinimum.IsForeignKey = false;
                colvarBusinessHourOrderMinimum.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderMinimum);
                
                TableSchema.TableColumn colvarBusinessHourPreparationTime = new TableSchema.TableColumn(schema);
                colvarBusinessHourPreparationTime.ColumnName = "business_hour_preparation_time";
                colvarBusinessHourPreparationTime.DataType = DbType.Int32;
                colvarBusinessHourPreparationTime.MaxLength = 0;
                colvarBusinessHourPreparationTime.AutoIncrement = false;
                colvarBusinessHourPreparationTime.IsNullable = false;
                colvarBusinessHourPreparationTime.IsPrimaryKey = false;
                colvarBusinessHourPreparationTime.IsForeignKey = false;
                colvarBusinessHourPreparationTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourPreparationTime);
                
                TableSchema.TableColumn colvarBusinessHourOnline = new TableSchema.TableColumn(schema);
                colvarBusinessHourOnline.ColumnName = "business_hour_online";
                colvarBusinessHourOnline.DataType = DbType.Boolean;
                colvarBusinessHourOnline.MaxLength = 0;
                colvarBusinessHourOnline.AutoIncrement = false;
                colvarBusinessHourOnline.IsNullable = false;
                colvarBusinessHourOnline.IsPrimaryKey = false;
                colvarBusinessHourOnline.IsForeignKey = false;
                colvarBusinessHourOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOnline);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
                colvarOrderTotalLimit.ColumnName = "order_total_limit";
                colvarOrderTotalLimit.DataType = DbType.Currency;
                colvarOrderTotalLimit.MaxLength = 0;
                colvarOrderTotalLimit.AutoIncrement = false;
                colvarOrderTotalLimit.IsNullable = true;
                colvarOrderTotalLimit.IsPrimaryKey = false;
                colvarOrderTotalLimit.IsForeignKey = false;
                colvarOrderTotalLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTotalLimit);
                
                TableSchema.TableColumn colvarDeliveryLimit = new TableSchema.TableColumn(schema);
                colvarDeliveryLimit.ColumnName = "delivery_limit";
                colvarDeliveryLimit.DataType = DbType.Int32;
                colvarDeliveryLimit.MaxLength = 0;
                colvarDeliveryLimit.AutoIncrement = false;
                colvarDeliveryLimit.IsNullable = true;
                colvarDeliveryLimit.IsPrimaryKey = false;
                colvarDeliveryLimit.IsForeignKey = false;
                colvarDeliveryLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryLimit);
                
                TableSchema.TableColumn colvarHoliday = new TableSchema.TableColumn(schema);
                colvarHoliday.ColumnName = "holiday";
                colvarHoliday.DataType = DbType.Int32;
                colvarHoliday.MaxLength = 0;
                colvarHoliday.AutoIncrement = false;
                colvarHoliday.IsNullable = false;
                colvarHoliday.IsPrimaryKey = false;
                colvarHoliday.IsForeignKey = false;
                colvarHoliday.IsReadOnly = false;
                
                schema.Columns.Add(colvarHoliday);
                
                TableSchema.TableColumn colvarBusinessHourCreateId = new TableSchema.TableColumn(schema);
                colvarBusinessHourCreateId.ColumnName = "business_hour_create_id";
                colvarBusinessHourCreateId.DataType = DbType.String;
                colvarBusinessHourCreateId.MaxLength = 30;
                colvarBusinessHourCreateId.AutoIncrement = false;
                colvarBusinessHourCreateId.IsNullable = false;
                colvarBusinessHourCreateId.IsPrimaryKey = false;
                colvarBusinessHourCreateId.IsForeignKey = false;
                colvarBusinessHourCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourCreateId);
                
                TableSchema.TableColumn colvarBusinessHourCreateTime = new TableSchema.TableColumn(schema);
                colvarBusinessHourCreateTime.ColumnName = "business_hour_create_time";
                colvarBusinessHourCreateTime.DataType = DbType.DateTime;
                colvarBusinessHourCreateTime.MaxLength = 0;
                colvarBusinessHourCreateTime.AutoIncrement = false;
                colvarBusinessHourCreateTime.IsNullable = false;
                colvarBusinessHourCreateTime.IsPrimaryKey = false;
                colvarBusinessHourCreateTime.IsForeignKey = false;
                colvarBusinessHourCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourCreateTime);
                
                TableSchema.TableColumn colvarBusinessHourModifyId = new TableSchema.TableColumn(schema);
                colvarBusinessHourModifyId.ColumnName = "business_hour_modify_id";
                colvarBusinessHourModifyId.DataType = DbType.String;
                colvarBusinessHourModifyId.MaxLength = 30;
                colvarBusinessHourModifyId.AutoIncrement = false;
                colvarBusinessHourModifyId.IsNullable = true;
                colvarBusinessHourModifyId.IsPrimaryKey = false;
                colvarBusinessHourModifyId.IsForeignKey = false;
                colvarBusinessHourModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourModifyId);
                
                TableSchema.TableColumn colvarBusinessHourModifyTime = new TableSchema.TableColumn(schema);
                colvarBusinessHourModifyTime.ColumnName = "business_hour_modify_time";
                colvarBusinessHourModifyTime.DataType = DbType.DateTime;
                colvarBusinessHourModifyTime.MaxLength = 0;
                colvarBusinessHourModifyTime.AutoIncrement = false;
                colvarBusinessHourModifyTime.IsNullable = true;
                colvarBusinessHourModifyTime.IsPrimaryKey = false;
                colvarBusinessHourModifyTime.IsForeignKey = false;
                colvarBusinessHourModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourModifyTime);
                
                TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
                colvarDepartment.ColumnName = "department";
                colvarDepartment.DataType = DbType.Int32;
                colvarDepartment.MaxLength = 0;
                colvarDepartment.AutoIncrement = false;
                colvarDepartment.IsNullable = false;
                colvarDepartment.IsPrimaryKey = false;
                colvarDepartment.IsForeignKey = false;
                colvarDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepartment);
                
                TableSchema.TableColumn colvarWeight = new TableSchema.TableColumn(schema);
                colvarWeight.ColumnName = "weight";
                colvarWeight.DataType = DbType.Int32;
                colvarWeight.MaxLength = 0;
                colvarWeight.AutoIncrement = false;
                colvarWeight.IsNullable = true;
                colvarWeight.IsPrimaryKey = false;
                colvarWeight.IsForeignKey = false;
                colvarWeight.IsReadOnly = false;
                
                schema.Columns.Add(colvarWeight);
                
                TableSchema.TableColumn colvarPostCkoutAction = new TableSchema.TableColumn(schema);
                colvarPostCkoutAction.ColumnName = "post_ckout_action";
                colvarPostCkoutAction.DataType = DbType.Int32;
                colvarPostCkoutAction.MaxLength = 0;
                colvarPostCkoutAction.AutoIncrement = false;
                colvarPostCkoutAction.IsNullable = false;
                colvarPostCkoutAction.IsPrimaryKey = false;
                colvarPostCkoutAction.IsForeignKey = false;
                colvarPostCkoutAction.IsReadOnly = false;
                
                schema.Columns.Add(colvarPostCkoutAction);
                
                TableSchema.TableColumn colvarPostCkoutArgs = new TableSchema.TableColumn(schema);
                colvarPostCkoutArgs.ColumnName = "post_ckout_args";
                colvarPostCkoutArgs.DataType = DbType.String;
                colvarPostCkoutArgs.MaxLength = 150;
                colvarPostCkoutArgs.AutoIncrement = false;
                colvarPostCkoutArgs.IsNullable = true;
                colvarPostCkoutArgs.IsPrimaryKey = false;
                colvarPostCkoutArgs.IsForeignKey = false;
                colvarPostCkoutArgs.IsReadOnly = false;
                
                schema.Columns.Add(colvarPostCkoutArgs);
                
                TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
                colvarCoordinate.ColumnName = "coordinate";
                colvarCoordinate.DataType = DbType.AnsiString;
                colvarCoordinate.MaxLength = -1;
                colvarCoordinate.AutoIncrement = false;
                colvarCoordinate.IsNullable = true;
                colvarCoordinate.IsPrimaryKey = false;
                colvarCoordinate.IsForeignKey = false;
                colvarCoordinate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCoordinate);
                
                TableSchema.TableColumn colvarDeliveryMinuteGap = new TableSchema.TableColumn(schema);
                colvarDeliveryMinuteGap.ColumnName = "delivery_minute_gap";
                colvarDeliveryMinuteGap.DataType = DbType.Int32;
                colvarDeliveryMinuteGap.MaxLength = 0;
                colvarDeliveryMinuteGap.AutoIncrement = false;
                colvarDeliveryMinuteGap.IsNullable = true;
                colvarDeliveryMinuteGap.IsPrimaryKey = false;
                colvarDeliveryMinuteGap.IsForeignKey = false;
                colvarDeliveryMinuteGap.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryMinuteGap);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_seller_city_biz_hour",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewSellerCityBizHour()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewSellerCityBizHour(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewSellerCityBizHour(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewSellerCityBizHour(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerBossName")]
        [Bindable(true)]
        public string SellerBossName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_boss_name");
		    }
            set 
		    {
			    SetColumnValue("seller_boss_name", value);
            }
        }
	      
        [XmlAttribute("SellerTel")]
        [Bindable(true)]
        public string SellerTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_tel");
		    }
            set 
		    {
			    SetColumnValue("seller_tel", value);
            }
        }
	      
        [XmlAttribute("SellerTel2")]
        [Bindable(true)]
        public string SellerTel2 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_tel2");
		    }
            set 
		    {
			    SetColumnValue("seller_tel2", value);
            }
        }
	      
        [XmlAttribute("SellerFax")]
        [Bindable(true)]
        public string SellerFax 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_fax");
		    }
            set 
		    {
			    SetColumnValue("seller_fax", value);
            }
        }
	      
        [XmlAttribute("SellerMobile")]
        [Bindable(true)]
        public string SellerMobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_mobile");
		    }
            set 
		    {
			    SetColumnValue("seller_mobile", value);
            }
        }
	      
        [XmlAttribute("SellerAddress")]
        [Bindable(true)]
        public string SellerAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_address");
		    }
            set 
		    {
			    SetColumnValue("seller_address", value);
            }
        }
	      
        [XmlAttribute("SellerEmail")]
        [Bindable(true)]
        public string SellerEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_email");
		    }
            set 
		    {
			    SetColumnValue("seller_email", value);
            }
        }
	      
        [XmlAttribute("SellerBlog")]
        [Bindable(true)]
        public string SellerBlog 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_blog");
		    }
            set 
		    {
			    SetColumnValue("seller_blog", value);
            }
        }
	      
        [XmlAttribute("SellerInvoice")]
        [Bindable(true)]
        public string SellerInvoice 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_invoice");
		    }
            set 
		    {
			    SetColumnValue("seller_invoice", value);
            }
        }
	      
        [XmlAttribute("SellerDescription")]
        [Bindable(true)]
        public string SellerDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_description");
		    }
            set 
		    {
			    SetColumnValue("seller_description", value);
            }
        }
	      
        [XmlAttribute("SellerStatus")]
        [Bindable(true)]
        public int SellerStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_status");
		    }
            set 
		    {
			    SetColumnValue("seller_status", value);
            }
        }
	      
        [XmlAttribute("SellerRemark")]
        [Bindable(true)]
        public string SellerRemark 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_remark");
		    }
            set 
		    {
			    SetColumnValue("seller_remark", value);
            }
        }
	      
        [XmlAttribute("SellerSales")]
        [Bindable(true)]
        public string SellerSales 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_sales");
		    }
            set 
		    {
			    SetColumnValue("seller_sales", value);
            }
        }
	      
        [XmlAttribute("SellerLogoimgPath")]
        [Bindable(true)]
        public string SellerLogoimgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_logoimg_path");
		    }
            set 
		    {
			    SetColumnValue("seller_logoimg_path", value);
            }
        }
	      
        [XmlAttribute("SellerVideoPath")]
        [Bindable(true)]
        public string SellerVideoPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_video_path");
		    }
            set 
		    {
			    SetColumnValue("seller_video_path", value);
            }
        }
	      
        [XmlAttribute("SellerCreateId")]
        [Bindable(true)]
        public string SellerCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_create_id");
		    }
            set 
		    {
			    SetColumnValue("seller_create_id", value);
            }
        }
	      
        [XmlAttribute("SellerCreateTime")]
        [Bindable(true)]
        public DateTime SellerCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("seller_create_time");
		    }
            set 
		    {
			    SetColumnValue("seller_create_time", value);
            }
        }
	      
        [XmlAttribute("SellerModifyId")]
        [Bindable(true)]
        public string SellerModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_modify_id");
		    }
            set 
		    {
			    SetColumnValue("seller_modify_id", value);
            }
        }
	      
        [XmlAttribute("SellerModifyTime")]
        [Bindable(true)]
        public DateTime? SellerModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("seller_modify_time");
		    }
            set 
		    {
			    SetColumnValue("seller_modify_time", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("BusinessHourId")]
        [Bindable(true)]
        public string BusinessHourId 
	    {
		    get
		    {
			    return GetColumnValue<string>("business_hour_id");
		    }
            set 
		    {
			    SetColumnValue("business_hour_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourTypeId")]
        [Bindable(true)]
        public int BusinessHourTypeId 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_type_id");
		    }
            set 
		    {
			    SetColumnValue("business_hour_type_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliveryCharge")]
        [Bindable(true)]
        public decimal BusinessHourDeliveryCharge 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_delivery_charge");
		    }
            set 
		    {
			    SetColumnValue("business_hour_delivery_charge", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderMinimum")]
        [Bindable(true)]
        public decimal BusinessHourOrderMinimum 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_order_minimum");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_minimum", value);
            }
        }
	      
        [XmlAttribute("BusinessHourPreparationTime")]
        [Bindable(true)]
        public int BusinessHourPreparationTime 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_preparation_time");
		    }
            set 
		    {
			    SetColumnValue("business_hour_preparation_time", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOnline")]
        [Bindable(true)]
        public bool BusinessHourOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool>("business_hour_online");
		    }
            set 
		    {
			    SetColumnValue("business_hour_online", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("OrderTotalLimit")]
        [Bindable(true)]
        public decimal? OrderTotalLimit 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("order_total_limit");
		    }
            set 
		    {
			    SetColumnValue("order_total_limit", value);
            }
        }
	      
        [XmlAttribute("DeliveryLimit")]
        [Bindable(true)]
        public int? DeliveryLimit 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_limit");
		    }
            set 
		    {
			    SetColumnValue("delivery_limit", value);
            }
        }
	      
        [XmlAttribute("Holiday")]
        [Bindable(true)]
        public int Holiday 
	    {
		    get
		    {
			    return GetColumnValue<int>("holiday");
		    }
            set 
		    {
			    SetColumnValue("holiday", value);
            }
        }
	      
        [XmlAttribute("BusinessHourCreateId")]
        [Bindable(true)]
        public string BusinessHourCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("business_hour_create_id");
		    }
            set 
		    {
			    SetColumnValue("business_hour_create_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourCreateTime")]
        [Bindable(true)]
        public DateTime BusinessHourCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_create_time");
		    }
            set 
		    {
			    SetColumnValue("business_hour_create_time", value);
            }
        }
	      
        [XmlAttribute("BusinessHourModifyId")]
        [Bindable(true)]
        public string BusinessHourModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("business_hour_modify_id");
		    }
            set 
		    {
			    SetColumnValue("business_hour_modify_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourModifyTime")]
        [Bindable(true)]
        public DateTime? BusinessHourModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_modify_time");
		    }
            set 
		    {
			    SetColumnValue("business_hour_modify_time", value);
            }
        }
	      
        [XmlAttribute("Department")]
        [Bindable(true)]
        public int Department 
	    {
		    get
		    {
			    return GetColumnValue<int>("department");
		    }
            set 
		    {
			    SetColumnValue("department", value);
            }
        }
	      
        [XmlAttribute("Weight")]
        [Bindable(true)]
        public int? Weight 
	    {
		    get
		    {
			    return GetColumnValue<int?>("weight");
		    }
            set 
		    {
			    SetColumnValue("weight", value);
            }
        }
	      
        [XmlAttribute("PostCkoutAction")]
        [Bindable(true)]
        public int PostCkoutAction 
	    {
		    get
		    {
			    return GetColumnValue<int>("post_ckout_action");
		    }
            set 
		    {
			    SetColumnValue("post_ckout_action", value);
            }
        }
	      
        [XmlAttribute("PostCkoutArgs")]
        [Bindable(true)]
        public string PostCkoutArgs 
	    {
		    get
		    {
			    return GetColumnValue<string>("post_ckout_args");
		    }
            set 
		    {
			    SetColumnValue("post_ckout_args", value);
            }
        }
	      
        [XmlAttribute("Coordinate")]
        [Bindable(true)]
        public string Coordinate 
	    {
		    get
		    {
			    return GetColumnValue<string>("coordinate");
		    }
            set 
		    {
			    SetColumnValue("coordinate", value);
            }
        }
	      
        [XmlAttribute("DeliveryMinuteGap")]
        [Bindable(true)]
        public int? DeliveryMinuteGap 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_minute_gap");
		    }
            set 
		    {
			    SetColumnValue("delivery_minute_gap", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CityName = @"city_name";
            
            public static string Code = @"code";
            
            public static string SellerId = @"seller_id";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerBossName = @"seller_boss_name";
            
            public static string SellerTel = @"seller_tel";
            
            public static string SellerTel2 = @"seller_tel2";
            
            public static string SellerFax = @"seller_fax";
            
            public static string SellerMobile = @"seller_mobile";
            
            public static string SellerAddress = @"seller_address";
            
            public static string SellerEmail = @"seller_email";
            
            public static string SellerBlog = @"seller_blog";
            
            public static string SellerInvoice = @"seller_invoice";
            
            public static string SellerDescription = @"seller_description";
            
            public static string SellerStatus = @"seller_status";
            
            public static string SellerRemark = @"seller_remark";
            
            public static string SellerSales = @"seller_sales";
            
            public static string SellerLogoimgPath = @"seller_logoimg_path";
            
            public static string SellerVideoPath = @"seller_video_path";
            
            public static string SellerCreateId = @"seller_create_id";
            
            public static string SellerCreateTime = @"seller_create_time";
            
            public static string SellerModifyId = @"seller_modify_id";
            
            public static string SellerModifyTime = @"seller_modify_time";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string BusinessHourId = @"business_hour_id";
            
            public static string BusinessHourTypeId = @"business_hour_type_id";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
            public static string BusinessHourDeliveryCharge = @"business_hour_delivery_charge";
            
            public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
            
            public static string BusinessHourPreparationTime = @"business_hour_preparation_time";
            
            public static string BusinessHourOnline = @"business_hour_online";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string OrderTotalLimit = @"order_total_limit";
            
            public static string DeliveryLimit = @"delivery_limit";
            
            public static string Holiday = @"holiday";
            
            public static string BusinessHourCreateId = @"business_hour_create_id";
            
            public static string BusinessHourCreateTime = @"business_hour_create_time";
            
            public static string BusinessHourModifyId = @"business_hour_modify_id";
            
            public static string BusinessHourModifyTime = @"business_hour_modify_time";
            
            public static string Department = @"department";
            
            public static string Weight = @"weight";
            
            public static string PostCkoutAction = @"post_ckout_action";
            
            public static string PostCkoutArgs = @"post_ckout_args";
            
            public static string Coordinate = @"coordinate";
            
            public static string DeliveryMinuteGap = @"delivery_minute_gap";
            
            public static string CityId = @"city_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

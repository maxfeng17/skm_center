﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the BlogPost class.
    /// </summary>
    [Serializable]
    public partial class BlogPostCollection : RepositoryList<BlogPost, BlogPostCollection>
    {
        public BlogPostCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BlogPostCollection</returns>
        public BlogPostCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BlogPost o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the blog_post table.
    /// </summary>
    [Serializable]
    public partial class BlogPost : RepositoryRecord<BlogPost>, IRecordBase
    {
        #region .ctors and Default Settings

        public BlogPost()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public BlogPost(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("blog_post", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarBeginDate = new TableSchema.TableColumn(schema);
                colvarBeginDate.ColumnName = "begin_date";
                colvarBeginDate.DataType = DbType.DateTime;
                colvarBeginDate.MaxLength = 0;
                colvarBeginDate.AutoIncrement = false;
                colvarBeginDate.IsNullable = false;
                colvarBeginDate.IsPrimaryKey = false;
                colvarBeginDate.IsForeignKey = false;
                colvarBeginDate.IsReadOnly = false;
                colvarBeginDate.DefaultSetting = @"";
                colvarBeginDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBeginDate);

                TableSchema.TableColumn colvarIsTop = new TableSchema.TableColumn(schema);
                colvarIsTop.ColumnName = "is_top";
                colvarIsTop.DataType = DbType.Boolean;
                colvarIsTop.MaxLength = 0;
                colvarIsTop.AutoIncrement = false;
                colvarIsTop.IsNullable = false;
                colvarIsTop.IsPrimaryKey = false;
                colvarIsTop.IsForeignKey = false;
                colvarIsTop.IsReadOnly = false;

                colvarIsTop.DefaultSetting = @"((0))";
                colvarIsTop.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsTop);

                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 255;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = false;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                colvarTitle.DefaultSetting = @"";
                colvarTitle.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTitle);

                TableSchema.TableColumn colvarKeyWord = new TableSchema.TableColumn(schema);
                colvarKeyWord.ColumnName = "key_word";
                colvarKeyWord.DataType = DbType.String;
                colvarKeyWord.MaxLength = 255;
                colvarKeyWord.AutoIncrement = false;
                colvarKeyWord.IsNullable = true;
                colvarKeyWord.IsPrimaryKey = false;
                colvarKeyWord.IsForeignKey = false;
                colvarKeyWord.IsReadOnly = false;
                colvarKeyWord.DefaultSetting = @"";
                colvarKeyWord.ForeignKeyTableName = "";
                schema.Columns.Add(colvarKeyWord);

                TableSchema.TableColumn colvarBlogDescription = new TableSchema.TableColumn(schema);
                colvarBlogDescription.ColumnName = "blog_description";
                colvarBlogDescription.DataType = DbType.String;
                colvarBlogDescription.MaxLength = 255;
                colvarBlogDescription.AutoIncrement = false;
                colvarBlogDescription.IsNullable = true;
                colvarBlogDescription.IsPrimaryKey = false;
                colvarBlogDescription.IsForeignKey = false;
                colvarBlogDescription.IsReadOnly = false;
                colvarBlogDescription.DefaultSetting = @"";
                colvarBlogDescription.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBlogDescription);

                TableSchema.TableColumn colvarBlogContent = new TableSchema.TableColumn(schema);
                colvarBlogContent.ColumnName = "blog_content";
                colvarBlogContent.DataType = DbType.String;
                colvarBlogContent.MaxLength = -1;
                colvarBlogContent.AutoIncrement = false;
                colvarBlogContent.IsNullable = false;
                colvarBlogContent.IsPrimaryKey = false;
                colvarBlogContent.IsForeignKey = false;
                colvarBlogContent.IsReadOnly = false;
                colvarBlogContent.DefaultSetting = @"";
                colvarBlogContent.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBlogContent);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.AnsiString;
                colvarModifyId.MaxLength = 50;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = false;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                colvarStatus.DefaultSetting = @"((0))";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarSort = new TableSchema.TableColumn(schema);
                colvarSort.ColumnName = "sort";
                colvarSort.DataType = DbType.Int32;
                colvarSort.MaxLength = 0;
                colvarSort.AutoIncrement = false;
                colvarSort.IsNullable = false;
                colvarSort.IsPrimaryKey = false;
                colvarSort.IsForeignKey = false;
                colvarSort.IsReadOnly = false;

                colvarSort.DefaultSetting = @"((99999))";
                colvarSort.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSort);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("blog_post", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("BeginDate")]
        [Bindable(true)]
        public DateTime BeginDate
        {
            get { return GetColumnValue<DateTime>(Columns.BeginDate); }
            set { SetColumnValue(Columns.BeginDate, value); }
        }

        [XmlAttribute("IsTop")]
        [Bindable(true)]
        public bool IsTop
        {
            get { return GetColumnValue<bool>(Columns.IsTop); }
            set { SetColumnValue(Columns.IsTop, value); }
        }

        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title
        {
            get { return GetColumnValue<string>(Columns.Title); }
            set { SetColumnValue(Columns.Title, value); }
        }

        [XmlAttribute("KeyWord")]
        [Bindable(true)]
        public string KeyWord
        {
            get { return GetColumnValue<string>(Columns.KeyWord); }
            set { SetColumnValue(Columns.KeyWord, value); }
        }

        [XmlAttribute("BlogDescription")]
        [Bindable(true)]
        public string BlogDescription
        {
            get { return GetColumnValue<string>(Columns.BlogDescription); }
            set { SetColumnValue(Columns.BlogDescription, value); }
        }

        [XmlAttribute("BlogContent")]
        [Bindable(true)]
        public string BlogContent
        {
            get { return GetColumnValue<string>(Columns.BlogContent); }
            set { SetColumnValue(Columns.BlogContent, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime
        {
            get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("Sort")]
        [Bindable(true)]
        public int Sort
        {
            get { return GetColumnValue<int>(Columns.Sort); }
            set { SetColumnValue(Columns.Sort, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn BeginDateColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn IsTopColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn KeyWordColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn BlogDescriptionColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn BlogContentColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn SortColumn
        {
            get { return Schema.Columns[13]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Guid = @"guid";
            public static string BeginDate = @"begin_date";
            public static string IsTop = @"is_top";
            public static string Title = @"title";
            public static string KeyWord = @"key_word";
            public static string BlogDescription = @"blog_description";
            public static string BlogContent = @"blog_content";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string Status = @"status";
            public static string Sort = @"sort";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

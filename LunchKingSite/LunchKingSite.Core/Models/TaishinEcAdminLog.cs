﻿using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the TaishinEcAdminLog class.
	/// </summary>
    [Serializable]
	public partial class TaishinEcAdminLogCollection : RepositoryList<TaishinEcAdminLog, TaishinEcAdminLogCollection>
	{	   
		public TaishinEcAdminLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TaishinEcAdminLogCollection</returns>
		public TaishinEcAdminLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TaishinEcAdminLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the taishin_ec_admin_log table.
	/// </summary>
	[Serializable]
	public partial class TaishinEcAdminLog : RepositoryRecord<TaishinEcAdminLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public TaishinEcAdminLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public TaishinEcAdminLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("taishin_ec_admin_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarFuncName = new TableSchema.TableColumn(schema);
				colvarFuncName.ColumnName = "func_name";
				colvarFuncName.DataType = DbType.String;
				colvarFuncName.MaxLength = 50;
				colvarFuncName.AutoIncrement = false;
				colvarFuncName.IsNullable = true;
				colvarFuncName.IsPrimaryKey = false;
				colvarFuncName.IsForeignKey = false;
				colvarFuncName.IsReadOnly = false;
				colvarFuncName.DefaultSetting = @"";
				colvarFuncName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFuncName);
				
				TableSchema.TableColumn colvarSearchContent = new TableSchema.TableColumn(schema);
				colvarSearchContent.ColumnName = "search_content";
				colvarSearchContent.DataType = DbType.String;
				colvarSearchContent.MaxLength = -1;
				colvarSearchContent.AutoIncrement = false;
				colvarSearchContent.IsNullable = true;
				colvarSearchContent.IsPrimaryKey = false;
				colvarSearchContent.IsForeignKey = false;
				colvarSearchContent.IsReadOnly = false;
				colvarSearchContent.DefaultSetting = @"";
				colvarSearchContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSearchContent);
				
				TableSchema.TableColumn colvarRsultContent = new TableSchema.TableColumn(schema);
				colvarRsultContent.ColumnName = "rsult_content";
				colvarRsultContent.DataType = DbType.String;
				colvarRsultContent.MaxLength = -1;
				colvarRsultContent.AutoIncrement = false;
				colvarRsultContent.IsNullable = true;
				colvarRsultContent.IsPrimaryKey = false;
				colvarRsultContent.IsForeignKey = false;
				colvarRsultContent.IsReadOnly = false;
				colvarRsultContent.DefaultSetting = @"";
				colvarRsultContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRsultContent);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateUser = new TableSchema.TableColumn(schema);
				colvarCreateUser.ColumnName = "create_user";
				colvarCreateUser.DataType = DbType.AnsiString;
				colvarCreateUser.MaxLength = 50;
				colvarCreateUser.AutoIncrement = false;
				colvarCreateUser.IsNullable = true;
				colvarCreateUser.IsPrimaryKey = false;
				colvarCreateUser.IsForeignKey = false;
				colvarCreateUser.IsReadOnly = false;
				colvarCreateUser.DefaultSetting = @"";
				colvarCreateUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateUser);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("taishin_ec_admin_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("FuncName")]
		[Bindable(true)]
		public string FuncName 
		{
			get { return GetColumnValue<string>(Columns.FuncName); }
			set { SetColumnValue(Columns.FuncName, value); }
		}
		  
		[XmlAttribute("SearchContent")]
		[Bindable(true)]
		public string SearchContent 
		{
			get { return GetColumnValue<string>(Columns.SearchContent); }
			set { SetColumnValue(Columns.SearchContent, value); }
		}
		  
		[XmlAttribute("RsultContent")]
		[Bindable(true)]
		public string RsultContent 
		{
			get { return GetColumnValue<string>(Columns.RsultContent); }
			set { SetColumnValue(Columns.RsultContent, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateUser")]
		[Bindable(true)]
		public string CreateUser 
		{
			get { return GetColumnValue<string>(Columns.CreateUser); }
			set { SetColumnValue(Columns.CreateUser, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn FuncNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SearchContentColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RsultContentColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateUserColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string FuncName = @"func_name";
			 public static string SearchContent = @"search_content";
			 public static string RsultContent = @"rsult_content";
			 public static string CreateTime = @"create_time";
			 public static string CreateUser = @"create_user";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

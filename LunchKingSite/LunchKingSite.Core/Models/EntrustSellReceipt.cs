using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the EntrustSellReceipt class.
	/// </summary>
    [Serializable]
	public partial class EntrustSellReceiptCollection : RepositoryList<EntrustSellReceipt, EntrustSellReceiptCollection>
	{	   
		public EntrustSellReceiptCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EntrustSellReceiptCollection</returns>
		public EntrustSellReceiptCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EntrustSellReceipt o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the entrust_sell_receipt table.
	/// </summary>
	[Serializable]
	public partial class EntrustSellReceipt : RepositoryRecord<EntrustSellReceipt>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public EntrustSellReceipt()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public EntrustSellReceipt(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("entrust_sell_receipt", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarIsPhysical = new TableSchema.TableColumn(schema);
				colvarIsPhysical.ColumnName = "is_physical";
				colvarIsPhysical.DataType = DbType.Boolean;
				colvarIsPhysical.MaxLength = 0;
				colvarIsPhysical.AutoIncrement = false;
				colvarIsPhysical.IsNullable = false;
				colvarIsPhysical.IsPrimaryKey = false;
				colvarIsPhysical.IsForeignKey = false;
				colvarIsPhysical.IsReadOnly = false;
				colvarIsPhysical.DefaultSetting = @"";
				colvarIsPhysical.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsPhysical);
				
				TableSchema.TableColumn colvarBuyerName = new TableSchema.TableColumn(schema);
				colvarBuyerName.ColumnName = "buyer_name";
				colvarBuyerName.DataType = DbType.String;
				colvarBuyerName.MaxLength = 50;
				colvarBuyerName.AutoIncrement = false;
				colvarBuyerName.IsNullable = true;
				colvarBuyerName.IsPrimaryKey = false;
				colvarBuyerName.IsForeignKey = false;
				colvarBuyerName.IsReadOnly = false;
				colvarBuyerName.DefaultSetting = @"";
				colvarBuyerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuyerName);
				
				TableSchema.TableColumn colvarBuyerAddress = new TableSchema.TableColumn(schema);
				colvarBuyerAddress.ColumnName = "buyer_address";
				colvarBuyerAddress.DataType = DbType.String;
				colvarBuyerAddress.MaxLength = 250;
				colvarBuyerAddress.AutoIncrement = false;
				colvarBuyerAddress.IsNullable = true;
				colvarBuyerAddress.IsPrimaryKey = false;
				colvarBuyerAddress.IsForeignKey = false;
				colvarBuyerAddress.IsReadOnly = false;
				colvarBuyerAddress.DefaultSetting = @"";
				colvarBuyerAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuyerAddress);
				
				TableSchema.TableColumn colvarComId = new TableSchema.TableColumn(schema);
				colvarComId.ColumnName = "com_id";
				colvarComId.DataType = DbType.AnsiString;
				colvarComId.MaxLength = 10;
				colvarComId.AutoIncrement = false;
				colvarComId.IsNullable = true;
				colvarComId.IsPrimaryKey = false;
				colvarComId.IsForeignKey = false;
				colvarComId.IsReadOnly = false;
				colvarComId.DefaultSetting = @"";
				colvarComId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarComId);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 50;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarReceiptRemark = new TableSchema.TableColumn(schema);
				colvarReceiptRemark.ColumnName = "receipt_remark";
				colvarReceiptRemark.DataType = DbType.String;
				colvarReceiptRemark.MaxLength = 200;
				colvarReceiptRemark.AutoIncrement = false;
				colvarReceiptRemark.IsNullable = true;
				colvarReceiptRemark.IsPrimaryKey = false;
				colvarReceiptRemark.IsForeignKey = false;
				colvarReceiptRemark.IsReadOnly = false;
				colvarReceiptRemark.DefaultSetting = @"";
				colvarReceiptRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiptRemark);
				
				TableSchema.TableColumn colvarReceiptCode = new TableSchema.TableColumn(schema);
				colvarReceiptCode.ColumnName = "receipt_code";
				colvarReceiptCode.DataType = DbType.AnsiString;
				colvarReceiptCode.MaxLength = 20;
				colvarReceiptCode.AutoIncrement = false;
				colvarReceiptCode.IsNullable = true;
				colvarReceiptCode.IsPrimaryKey = false;
				colvarReceiptCode.IsForeignKey = false;
				colvarReceiptCode.IsReadOnly = false;
				colvarReceiptCode.DefaultSetting = @"";
				colvarReceiptCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiptCode);
				
				TableSchema.TableColumn colvarMailbackAllowance = new TableSchema.TableColumn(schema);
				colvarMailbackAllowance.ColumnName = "mailback_allowance";
				colvarMailbackAllowance.DataType = DbType.Boolean;
				colvarMailbackAllowance.MaxLength = 0;
				colvarMailbackAllowance.AutoIncrement = false;
				colvarMailbackAllowance.IsNullable = false;
				colvarMailbackAllowance.IsPrimaryKey = false;
				colvarMailbackAllowance.IsForeignKey = false;
				colvarMailbackAllowance.IsReadOnly = false;

                colvarMailbackAllowance.DefaultSetting = @"((0))";
                colvarMailbackAllowance.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMailbackAllowance);

                TableSchema.TableColumn colvarMailbackReceipt = new TableSchema.TableColumn(schema);
                colvarMailbackReceipt.ColumnName = "mailback_receipt";
                colvarMailbackReceipt.DataType = DbType.Boolean;
                colvarMailbackReceipt.MaxLength = 0;
                colvarMailbackReceipt.AutoIncrement = false;
                colvarMailbackReceipt.IsNullable = false;
                colvarMailbackReceipt.IsPrimaryKey = false;
                colvarMailbackReceipt.IsForeignKey = false;
                colvarMailbackReceipt.IsReadOnly = false;

                colvarMailbackReceipt.DefaultSetting = @"((0))";
                colvarMailbackReceipt.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMailbackReceipt);

                TableSchema.TableColumn colvarMailbackPaper = new TableSchema.TableColumn(schema);
				colvarMailbackPaper.ColumnName = "mailback_paper";
				colvarMailbackPaper.DataType = DbType.Boolean;
				colvarMailbackPaper.MaxLength = 0;
				colvarMailbackPaper.AutoIncrement = false;
				colvarMailbackPaper.IsNullable = false;
				colvarMailbackPaper.IsPrimaryKey = false;
				colvarMailbackPaper.IsForeignKey = false;
                colvarMailbackPaper.IsReadOnly = false;

                colvarMailbackPaper.DefaultSetting = @"((0))";
                colvarMailbackPaper.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMailbackPaper);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 250;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = true;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);
				
				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "coupon_id";
				colvarCouponId.DataType = DbType.Int32;
				colvarCouponId.MaxLength = 0;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = true;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = false;
				colvarCouponId.IsReadOnly = false;
				colvarCouponId.DefaultSetting = @"";
				colvarCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponId);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;

                colvarTrustId.DefaultSetting = @"(newid())";
                colvarTrustId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustId);

                TableSchema.TableColumn colvarForBusiness = new TableSchema.TableColumn(schema);
				colvarForBusiness.ColumnName = "for_business";
				colvarForBusiness.DataType = DbType.Boolean;
				colvarForBusiness.MaxLength = 0;
				colvarForBusiness.AutoIncrement = false;
				colvarForBusiness.IsNullable = false;
				colvarForBusiness.IsPrimaryKey = false;
				colvarForBusiness.IsForeignKey = false;
				colvarForBusiness.IsReadOnly = false;
				
						colvarForBusiness.DefaultSetting = @"((0))";
				colvarForBusiness.ForeignKeyTableName = "";
				schema.Columns.Add(colvarForBusiness);
				
				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Currency;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = false;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				
						colvarAmount.DefaultSetting = @"((0))";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarTax = new TableSchema.TableColumn(schema);
                colvarTax.ColumnName = "tax";
                colvarTax.DataType = DbType.Decimal;
                colvarTax.MaxLength = 0;
                colvarTax.AutoIncrement = false;
                colvarTax.IsNullable = false;
                colvarTax.IsPrimaryKey = false;
                colvarTax.IsForeignKey = false;
                colvarTax.IsReadOnly = false;

                colvarTax.DefaultSetting = @"((0))";
                colvarTax.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTax);

                TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
				colvarItemQuantity.ColumnName = "item_quantity";
				colvarItemQuantity.DataType = DbType.Int32;
				colvarItemQuantity.MaxLength = 0;
				colvarItemQuantity.AutoIncrement = false;
				colvarItemQuantity.IsNullable = false;
				colvarItemQuantity.IsPrimaryKey = false;
				colvarItemQuantity.IsForeignKey = false;
				colvarItemQuantity.IsReadOnly = false;
				
						colvarItemQuantity.DefaultSetting = @"((0))";
				colvarItemQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemQuantity);
				
				TableSchema.TableColumn colvarItemUnitPrice = new TableSchema.TableColumn(schema);
				colvarItemUnitPrice.ColumnName = "item_unit_price";
				colvarItemUnitPrice.DataType = DbType.Currency;
				colvarItemUnitPrice.MaxLength = 0;
				colvarItemUnitPrice.AutoIncrement = false;
				colvarItemUnitPrice.IsNullable = false;
				colvarItemUnitPrice.IsPrimaryKey = false;
				colvarItemUnitPrice.IsForeignKey = false;
				colvarItemUnitPrice.IsReadOnly = false;
				
						colvarItemUnitPrice.DefaultSetting = @"((0))";
				colvarItemUnitPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemUnitPrice);
				
				TableSchema.TableColumn colvarReceiptCodeTime = new TableSchema.TableColumn(schema);
				colvarReceiptCodeTime.ColumnName = "receipt_code_time";
				colvarReceiptCodeTime.DataType = DbType.DateTime;
				colvarReceiptCodeTime.MaxLength = 0;
				colvarReceiptCodeTime.AutoIncrement = false;
				colvarReceiptCodeTime.IsNullable = true;
				colvarReceiptCodeTime.IsPrimaryKey = false;
				colvarReceiptCodeTime.IsForeignKey = false;
				colvarReceiptCodeTime.IsReadOnly = false;
				colvarReceiptCodeTime.DefaultSetting = @"";
				colvarReceiptCodeTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiptCodeTime);
				
				TableSchema.TableColumn colvarIsExported = new TableSchema.TableColumn(schema);
				colvarIsExported.ColumnName = "is_exported";
				colvarIsExported.DataType = DbType.Boolean;
				colvarIsExported.MaxLength = 0;
				colvarIsExported.AutoIncrement = false;
				colvarIsExported.IsNullable = false;
				colvarIsExported.IsPrimaryKey = false;
				colvarIsExported.IsForeignKey = false;
				colvarIsExported.IsReadOnly = false;
				
						colvarIsExported.DefaultSetting = @"((0))";
				colvarIsExported.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsExported);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((1))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("entrust_sell_receipt",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("IsPhysical")]
		[Bindable(true)]
		public bool IsPhysical 
		{
			get { return GetColumnValue<bool>(Columns.IsPhysical); }
			set { SetColumnValue(Columns.IsPhysical, value); }
		}
		  
		[XmlAttribute("BuyerName")]
		[Bindable(true)]
		public string BuyerName 
		{
			get { return GetColumnValue<string>(Columns.BuyerName); }
			set { SetColumnValue(Columns.BuyerName, value); }
		}
		  
		[XmlAttribute("BuyerAddress")]
		[Bindable(true)]
		public string BuyerAddress 
		{
			get { return GetColumnValue<string>(Columns.BuyerAddress); }
			set { SetColumnValue(Columns.BuyerAddress, value); }
		}
		  
		[XmlAttribute("ComId")]
		[Bindable(true)]
		public string ComId 
		{
			get { return GetColumnValue<string>(Columns.ComId); }
			set { SetColumnValue(Columns.ComId, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ReceiptRemark")]
		[Bindable(true)]
		public string ReceiptRemark 
		{
			get { return GetColumnValue<string>(Columns.ReceiptRemark); }
			set { SetColumnValue(Columns.ReceiptRemark, value); }
		}
		  
		[XmlAttribute("ReceiptCode")]
		[Bindable(true)]
		public string ReceiptCode 
		{
			get { return GetColumnValue<string>(Columns.ReceiptCode); }
			set { SetColumnValue(Columns.ReceiptCode, value); }
		}
		  
		[XmlAttribute("MailbackAllowance")]
		[Bindable(true)]
		public bool MailbackAllowance 
		{
			get { return GetColumnValue<bool>(Columns.MailbackAllowance); }
			set { SetColumnValue(Columns.MailbackAllowance, value); }
		}
		  
		[XmlAttribute("MailbackReceipt")]
		[Bindable(true)]
		public bool MailbackReceipt 
		{
			get { return GetColumnValue<bool>(Columns.MailbackReceipt); }
			set { SetColumnValue(Columns.MailbackReceipt, value); }
		}
		  
		[XmlAttribute("MailbackPaper")]
		[Bindable(true)]
		public bool MailbackPaper 
		{
			get { return GetColumnValue<bool>(Columns.MailbackPaper); }
			set { SetColumnValue(Columns.MailbackPaper, value); }
		}
		  
		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName 
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}
		  
		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public int? CouponId 
		{
			get { return GetColumnValue<int?>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}
		  
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		  
		[XmlAttribute("ForBusiness")]
		[Bindable(true)]
		public bool ForBusiness 
		{
			get { return GetColumnValue<bool>(Columns.ForBusiness); }
			set { SetColumnValue(Columns.ForBusiness, value); }
		}
		  
		[XmlAttribute("Amount")]
		[Bindable(true)]
		public decimal Amount 
		{
			get { return GetColumnValue<decimal>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}

        [XmlAttribute("Tax")]
        [Bindable(true)]
        public decimal Tax
        {
            get { return GetColumnValue<decimal>(Columns.Tax); }
            set { SetColumnValue(Columns.Tax, value); }
        }

        [XmlAttribute("ItemQuantity")]
		[Bindable(true)]
		public int ItemQuantity 
		{
			get { return GetColumnValue<int>(Columns.ItemQuantity); }
			set { SetColumnValue(Columns.ItemQuantity, value); }
		}
		  
		[XmlAttribute("ItemUnitPrice")]
		[Bindable(true)]
		public decimal ItemUnitPrice 
		{
			get { return GetColumnValue<decimal>(Columns.ItemUnitPrice); }
			set { SetColumnValue(Columns.ItemUnitPrice, value); }
		}
		  
		[XmlAttribute("ReceiptCodeTime")]
		[Bindable(true)]
		public DateTime? ReceiptCodeTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ReceiptCodeTime); }
			set { SetColumnValue(Columns.ReceiptCodeTime, value); }
		}
		  
		[XmlAttribute("IsExported")]
		[Bindable(true)]
		public bool IsExported 
		{
			get { return GetColumnValue<bool>(Columns.IsExported); }
			set { SetColumnValue(Columns.IsExported, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IsPhysicalColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BuyerNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn BuyerAddressColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ComIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiptRemarkColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiptCodeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn MailbackAllowanceColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn MailbackReceiptColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn MailbackPaperColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ForBusinessColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn TaxColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn ItemQuantityColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn ItemUnitPriceColumn
        {
            get { return Schema.Columns[22]; }
        }



        public static TableSchema.TableColumn ReceiptCodeTimeColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn IsExportedColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderGuid = @"order_guid";
			 public static string IsPhysical = @"is_physical";
			 public static string BuyerName = @"buyer_name";
			 public static string BuyerAddress = @"buyer_address";
			 public static string ComId = @"com_id";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string ReceiptRemark = @"receipt_remark";
			 public static string ReceiptCode = @"receipt_code";
			 public static string MailbackAllowance = @"mailback_allowance";
			 public static string MailbackReceipt = @"mailback_receipt";
			 public static string MailbackPaper = @"mailback_paper";
			 public static string ItemName = @"item_name";
			 public static string CouponId = @"coupon_id";
			 public static string TrustId = @"trust_id";
			 public static string ForBusiness = @"for_business";
			 public static string Amount = @"amount";
            public static string Tax = @"tax";
            public static string ItemQuantity = @"item_quantity";
			 public static string ItemUnitPrice = @"item_unit_price";
			 public static string ReceiptCodeTime = @"receipt_code_time";
			 public static string IsExported = @"is_exported";
			 public static string Type = @"type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

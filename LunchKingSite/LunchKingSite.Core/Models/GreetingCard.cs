using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the GreetingCard class.
	/// </summary>
    [Serializable]
	public partial class GreetingCardCollection : RepositoryList<GreetingCard, GreetingCardCollection>
	{	   
		public GreetingCardCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>GreetingCardCollection</returns>
		public GreetingCardCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                GreetingCard o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the greeting_card table.
	/// </summary>
	[Serializable]
	public partial class GreetingCard : RepositoryRecord<GreetingCard>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public GreetingCard()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public GreetingCard(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("greeting_card", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSenderMemberUniqueId = new TableSchema.TableColumn(schema);
				colvarSenderMemberUniqueId.ColumnName = "sender_member_unique_id";
				colvarSenderMemberUniqueId.DataType = DbType.Int32;
				colvarSenderMemberUniqueId.MaxLength = 0;
				colvarSenderMemberUniqueId.AutoIncrement = false;
				colvarSenderMemberUniqueId.IsNullable = false;
				colvarSenderMemberUniqueId.IsPrimaryKey = false;
				colvarSenderMemberUniqueId.IsForeignKey = false;
				colvarSenderMemberUniqueId.IsReadOnly = false;
				colvarSenderMemberUniqueId.DefaultSetting = @"";
				colvarSenderMemberUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSenderMemberUniqueId);
				
				TableSchema.TableColumn colvarSenderEmail = new TableSchema.TableColumn(schema);
				colvarSenderEmail.ColumnName = "sender_email";
				colvarSenderEmail.DataType = DbType.AnsiString;
				colvarSenderEmail.MaxLength = 100;
				colvarSenderEmail.AutoIncrement = false;
				colvarSenderEmail.IsNullable = false;
				colvarSenderEmail.IsPrimaryKey = false;
				colvarSenderEmail.IsForeignKey = false;
				colvarSenderEmail.IsReadOnly = false;
				colvarSenderEmail.DefaultSetting = @"";
				colvarSenderEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSenderEmail);
				
				TableSchema.TableColumn colvarSenderNickname = new TableSchema.TableColumn(schema);
				colvarSenderNickname.ColumnName = "sender_nickname";
				colvarSenderNickname.DataType = DbType.String;
				colvarSenderNickname.MaxLength = 30;
				colvarSenderNickname.AutoIncrement = false;
				colvarSenderNickname.IsNullable = false;
				colvarSenderNickname.IsPrimaryKey = false;
				colvarSenderNickname.IsForeignKey = false;
				colvarSenderNickname.IsReadOnly = false;
				colvarSenderNickname.DefaultSetting = @"";
				colvarSenderNickname.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSenderNickname);
				
				TableSchema.TableColumn colvarSendContent = new TableSchema.TableColumn(schema);
				colvarSendContent.ColumnName = "send_content";
				colvarSendContent.DataType = DbType.String;
				colvarSendContent.MaxLength = 50;
				colvarSendContent.AutoIncrement = false;
				colvarSendContent.IsNullable = false;
				colvarSendContent.IsPrimaryKey = false;
				colvarSendContent.IsForeignKey = false;
				colvarSendContent.IsReadOnly = false;
				colvarSendContent.DefaultSetting = @"";
				colvarSendContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendContent);
				
				TableSchema.TableColumn colvarSendTime = new TableSchema.TableColumn(schema);
				colvarSendTime.ColumnName = "send_time";
				colvarSendTime.DataType = DbType.DateTime;
				colvarSendTime.MaxLength = 0;
				colvarSendTime.AutoIncrement = false;
				colvarSendTime.IsNullable = false;
				colvarSendTime.IsPrimaryKey = false;
				colvarSendTime.IsForeignKey = false;
				colvarSendTime.IsReadOnly = false;
				colvarSendTime.DefaultSetting = @"";
				colvarSendTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendTime);
				
				TableSchema.TableColumn colvarReceiverEmail = new TableSchema.TableColumn(schema);
				colvarReceiverEmail.ColumnName = "receiver_email";
				colvarReceiverEmail.DataType = DbType.AnsiString;
				colvarReceiverEmail.MaxLength = 100;
				colvarReceiverEmail.AutoIncrement = false;
				colvarReceiverEmail.IsNullable = false;
				colvarReceiverEmail.IsPrimaryKey = false;
				colvarReceiverEmail.IsForeignKey = false;
				colvarReceiverEmail.IsReadOnly = false;
				colvarReceiverEmail.DefaultSetting = @"";
				colvarReceiverEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverEmail);
				
				TableSchema.TableColumn colvarReceiverTrimEmail = new TableSchema.TableColumn(schema);
				colvarReceiverTrimEmail.ColumnName = "receiver_trim_email";
				colvarReceiverTrimEmail.DataType = DbType.AnsiString;
				colvarReceiverTrimEmail.MaxLength = 100;
				colvarReceiverTrimEmail.AutoIncrement = false;
				colvarReceiverTrimEmail.IsNullable = false;
				colvarReceiverTrimEmail.IsPrimaryKey = false;
				colvarReceiverTrimEmail.IsForeignKey = false;
				colvarReceiverTrimEmail.IsReadOnly = false;
				colvarReceiverTrimEmail.DefaultSetting = @"";
				colvarReceiverTrimEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverTrimEmail);
				
				TableSchema.TableColumn colvarReceiverMemberUniqueId = new TableSchema.TableColumn(schema);
				colvarReceiverMemberUniqueId.ColumnName = "receiver_member_unique_id";
				colvarReceiverMemberUniqueId.DataType = DbType.Int32;
				colvarReceiverMemberUniqueId.MaxLength = 0;
				colvarReceiverMemberUniqueId.AutoIncrement = false;
				colvarReceiverMemberUniqueId.IsNullable = true;
				colvarReceiverMemberUniqueId.IsPrimaryKey = false;
				colvarReceiverMemberUniqueId.IsForeignKey = false;
				colvarReceiverMemberUniqueId.IsReadOnly = false;
				colvarReceiverMemberUniqueId.DefaultSetting = @"";
				colvarReceiverMemberUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiverMemberUniqueId);
				
				TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
				colvarEventId.ColumnName = "event_id";
				colvarEventId.DataType = DbType.Int32;
				colvarEventId.MaxLength = 0;
				colvarEventId.AutoIncrement = false;
				colvarEventId.IsNullable = false;
				colvarEventId.IsPrimaryKey = false;
				colvarEventId.IsForeignKey = false;
				colvarEventId.IsReadOnly = false;
				colvarEventId.DefaultSetting = @"";
				colvarEventId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventId);
				
				TableSchema.TableColumn colvarGift = new TableSchema.TableColumn(schema);
				colvarGift.ColumnName = "gift";
				colvarGift.DataType = DbType.Int32;
				colvarGift.MaxLength = 0;
				colvarGift.AutoIncrement = false;
				colvarGift.IsNullable = false;
				colvarGift.IsPrimaryKey = false;
				colvarGift.IsForeignKey = false;
				colvarGift.IsReadOnly = false;
				colvarGift.DefaultSetting = @"";
				colvarGift.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGift);
				
				TableSchema.TableColumn colvarSendStatus = new TableSchema.TableColumn(schema);
				colvarSendStatus.ColumnName = "send_status";
				colvarSendStatus.DataType = DbType.Int32;
				colvarSendStatus.MaxLength = 0;
				colvarSendStatus.AutoIncrement = false;
				colvarSendStatus.IsNullable = false;
				colvarSendStatus.IsPrimaryKey = false;
				colvarSendStatus.IsForeignKey = false;
				colvarSendStatus.IsReadOnly = false;
				
						colvarSendStatus.DefaultSetting = @"((0))";
				colvarSendStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendStatus);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("greeting_card",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SenderMemberUniqueId")]
		[Bindable(true)]
		public int SenderMemberUniqueId 
		{
			get { return GetColumnValue<int>(Columns.SenderMemberUniqueId); }
			set { SetColumnValue(Columns.SenderMemberUniqueId, value); }
		}
		  
		[XmlAttribute("SenderEmail")]
		[Bindable(true)]
		public string SenderEmail 
		{
			get { return GetColumnValue<string>(Columns.SenderEmail); }
			set { SetColumnValue(Columns.SenderEmail, value); }
		}
		  
		[XmlAttribute("SenderNickname")]
		[Bindable(true)]
		public string SenderNickname 
		{
			get { return GetColumnValue<string>(Columns.SenderNickname); }
			set { SetColumnValue(Columns.SenderNickname, value); }
		}
		  
		[XmlAttribute("SendContent")]
		[Bindable(true)]
		public string SendContent 
		{
			get { return GetColumnValue<string>(Columns.SendContent); }
			set { SetColumnValue(Columns.SendContent, value); }
		}
		  
		[XmlAttribute("SendTime")]
		[Bindable(true)]
		public DateTime SendTime 
		{
			get { return GetColumnValue<DateTime>(Columns.SendTime); }
			set { SetColumnValue(Columns.SendTime, value); }
		}
		  
		[XmlAttribute("ReceiverEmail")]
		[Bindable(true)]
		public string ReceiverEmail 
		{
			get { return GetColumnValue<string>(Columns.ReceiverEmail); }
			set { SetColumnValue(Columns.ReceiverEmail, value); }
		}
		  
		[XmlAttribute("ReceiverTrimEmail")]
		[Bindable(true)]
		public string ReceiverTrimEmail 
		{
			get { return GetColumnValue<string>(Columns.ReceiverTrimEmail); }
			set { SetColumnValue(Columns.ReceiverTrimEmail, value); }
		}
		  
		[XmlAttribute("ReceiverMemberUniqueId")]
		[Bindable(true)]
		public int? ReceiverMemberUniqueId 
		{
			get { return GetColumnValue<int?>(Columns.ReceiverMemberUniqueId); }
			set { SetColumnValue(Columns.ReceiverMemberUniqueId, value); }
		}
		  
		[XmlAttribute("EventId")]
		[Bindable(true)]
		public int EventId 
		{
			get { return GetColumnValue<int>(Columns.EventId); }
			set { SetColumnValue(Columns.EventId, value); }
		}
		  
		[XmlAttribute("Gift")]
		[Bindable(true)]
		public int Gift 
		{
			get { return GetColumnValue<int>(Columns.Gift); }
			set { SetColumnValue(Columns.Gift, value); }
		}
		  
		[XmlAttribute("SendStatus")]
		[Bindable(true)]
		public int SendStatus 
		{
			get { return GetColumnValue<int>(Columns.SendStatus); }
			set { SetColumnValue(Columns.SendStatus, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SenderMemberUniqueIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SenderEmailColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SenderNicknameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SendContentColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn SendTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverEmailColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverTrimEmailColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiverMemberUniqueIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn EventIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn GiftColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn SendStatusColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SenderMemberUniqueId = @"sender_member_unique_id";
			 public static string SenderEmail = @"sender_email";
			 public static string SenderNickname = @"sender_nickname";
			 public static string SendContent = @"send_content";
			 public static string SendTime = @"send_time";
			 public static string ReceiverEmail = @"receiver_email";
			 public static string ReceiverTrimEmail = @"receiver_trim_email";
			 public static string ReceiverMemberUniqueId = @"receiver_member_unique_id";
			 public static string EventId = @"event_id";
			 public static string Gift = @"gift";
			 public static string SendStatus = @"send_status";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VourcherPromo class.
	/// </summary>
    [Serializable]
	public partial class VourcherPromoCollection : RepositoryList<VourcherPromo, VourcherPromoCollection>
	{	   
		public VourcherPromoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VourcherPromoCollection</returns>
		public VourcherPromoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VourcherPromo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the vourcher_promo table.
	/// </summary>
	[Serializable]
	public partial class VourcherPromo : RepositoryRecord<VourcherPromo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VourcherPromo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VourcherPromo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("vourcher_promo", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
				colvarEventId.ColumnName = "event_id";
				colvarEventId.DataType = DbType.Int32;
				colvarEventId.MaxLength = 0;
				colvarEventId.AutoIncrement = false;
				colvarEventId.IsNullable = true;
				colvarEventId.IsPrimaryKey = false;
				colvarEventId.IsForeignKey = false;
				colvarEventId.IsReadOnly = false;
				colvarEventId.DefaultSetting = @"";
				colvarEventId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventId);
				
				TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
				colvarSellerId.ColumnName = "seller_id";
				colvarSellerId.DataType = DbType.AnsiString;
				colvarSellerId.MaxLength = 20;
				colvarSellerId.AutoIncrement = false;
				colvarSellerId.IsNullable = false;
				colvarSellerId.IsPrimaryKey = false;
				colvarSellerId.IsForeignKey = false;
				colvarSellerId.IsReadOnly = false;
				colvarSellerId.DefaultSetting = @"";
				colvarSellerId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarRank = new TableSchema.TableColumn(schema);
				colvarRank.ColumnName = "rank";
				colvarRank.DataType = DbType.Int32;
				colvarRank.MaxLength = 0;
				colvarRank.AutoIncrement = false;
				colvarRank.IsNullable = false;
				colvarRank.IsPrimaryKey = false;
				colvarRank.IsForeignKey = false;
				colvarRank.IsReadOnly = false;
				
						colvarRank.DefaultSetting = @"((0))";
				colvarRank.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRank);
				
				TableSchema.TableColumn colvarRatio = new TableSchema.TableColumn(schema);
				colvarRatio.ColumnName = "ratio";
				colvarRatio.DataType = DbType.Int32;
				colvarRatio.MaxLength = 0;
				colvarRatio.AutoIncrement = false;
				colvarRatio.IsNullable = false;
				colvarRatio.IsPrimaryKey = false;
				colvarRatio.IsForeignKey = false;
				colvarRatio.IsReadOnly = false;
				
						colvarRatio.DefaultSetting = @"((0))";
				colvarRatio.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRatio);
				
				TableSchema.TableColumn colvarTag = new TableSchema.TableColumn(schema);
				colvarTag.ColumnName = "tag";
				colvarTag.DataType = DbType.String;
				colvarTag.MaxLength = 50;
				colvarTag.AutoIncrement = false;
				colvarTag.IsNullable = true;
				colvarTag.IsPrimaryKey = false;
				colvarTag.IsForeignKey = false;
				colvarTag.IsReadOnly = false;
				colvarTag.DefaultSetting = @"";
				colvarTag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTag);
				
				TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
				colvarContent.ColumnName = "content";
				colvarContent.DataType = DbType.String;
				colvarContent.MaxLength = 1073741823;
				colvarContent.AutoIncrement = false;
				colvarContent.IsNullable = true;
				colvarContent.IsPrimaryKey = false;
				colvarContent.IsForeignKey = false;
				colvarContent.IsReadOnly = false;
				colvarContent.DefaultSetting = @"";
				colvarContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContent);
				
				TableSchema.TableColumn colvarLink = new TableSchema.TableColumn(schema);
				colvarLink.ColumnName = "link";
				colvarLink.DataType = DbType.String;
				colvarLink.MaxLength = 100;
				colvarLink.AutoIncrement = false;
				colvarLink.IsNullable = true;
				colvarLink.IsPrimaryKey = false;
				colvarLink.IsForeignKey = false;
				colvarLink.IsReadOnly = false;
				colvarLink.DefaultSetting = @"";
				colvarLink.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLink);
				
				TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
				colvarStartDate.ColumnName = "start_date";
				colvarStartDate.DataType = DbType.DateTime;
				colvarStartDate.MaxLength = 0;
				colvarStartDate.AutoIncrement = false;
				colvarStartDate.IsNullable = true;
				colvarStartDate.IsPrimaryKey = false;
				colvarStartDate.IsForeignKey = false;
				colvarStartDate.IsReadOnly = false;
				colvarStartDate.DefaultSetting = @"";
				colvarStartDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartDate);
				
				TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
				colvarEndDate.ColumnName = "end_date";
				colvarEndDate.DataType = DbType.DateTime;
				colvarEndDate.MaxLength = 0;
				colvarEndDate.AutoIncrement = false;
				colvarEndDate.IsNullable = true;
				colvarEndDate.IsPrimaryKey = false;
				colvarEndDate.IsForeignKey = false;
				colvarEndDate.IsReadOnly = false;
				colvarEndDate.DefaultSetting = @"";
				colvarEndDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndDate);
				
				TableSchema.TableColumn colvarPageCount = new TableSchema.TableColumn(schema);
				colvarPageCount.ColumnName = "page_count";
				colvarPageCount.DataType = DbType.Int32;
				colvarPageCount.MaxLength = 0;
				colvarPageCount.AutoIncrement = false;
				colvarPageCount.IsNullable = false;
				colvarPageCount.IsPrimaryKey = false;
				colvarPageCount.IsForeignKey = false;
				colvarPageCount.IsReadOnly = false;
				
						colvarPageCount.DefaultSetting = @"((0))";
				colvarPageCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPageCount);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("vourcher_promo",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("EventId")]
		[Bindable(true)]
		public int? EventId 
		{
			get { return GetColumnValue<int?>(Columns.EventId); }
			set { SetColumnValue(Columns.EventId, value); }
		}
		  
		[XmlAttribute("SellerId")]
		[Bindable(true)]
		public string SellerId 
		{
			get { return GetColumnValue<string>(Columns.SellerId); }
			set { SetColumnValue(Columns.SellerId, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("Rank")]
		[Bindable(true)]
		public int Rank 
		{
			get { return GetColumnValue<int>(Columns.Rank); }
			set { SetColumnValue(Columns.Rank, value); }
		}
		  
		[XmlAttribute("Ratio")]
		[Bindable(true)]
		public int Ratio 
		{
			get { return GetColumnValue<int>(Columns.Ratio); }
			set { SetColumnValue(Columns.Ratio, value); }
		}
		  
		[XmlAttribute("Tag")]
		[Bindable(true)]
		public string Tag 
		{
			get { return GetColumnValue<string>(Columns.Tag); }
			set { SetColumnValue(Columns.Tag, value); }
		}
		  
		[XmlAttribute("Content")]
		[Bindable(true)]
		public string Content 
		{
			get { return GetColumnValue<string>(Columns.Content); }
			set { SetColumnValue(Columns.Content, value); }
		}
		  
		[XmlAttribute("Link")]
		[Bindable(true)]
		public string Link 
		{
			get { return GetColumnValue<string>(Columns.Link); }
			set { SetColumnValue(Columns.Link, value); }
		}
		  
		[XmlAttribute("StartDate")]
		[Bindable(true)]
		public DateTime? StartDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.StartDate); }
			set { SetColumnValue(Columns.StartDate, value); }
		}
		  
		[XmlAttribute("EndDate")]
		[Bindable(true)]
		public DateTime? EndDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.EndDate); }
			set { SetColumnValue(Columns.EndDate, value); }
		}
		  
		[XmlAttribute("PageCount")]
		[Bindable(true)]
		public int PageCount 
		{
			get { return GetColumnValue<int>(Columns.PageCount); }
			set { SetColumnValue(Columns.PageCount, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn EventIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn RankColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn RatioColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn TagColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ContentColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn LinkColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn StartDateColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn EndDateColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn PageCountColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Type = @"type";
			 public static string EventId = @"event_id";
			 public static string SellerId = @"seller_id";
			 public static string SellerGuid = @"seller_guid";
			 public static string Rank = @"rank";
			 public static string Ratio = @"ratio";
			 public static string Tag = @"tag";
			 public static string Content = @"content";
			 public static string Link = @"link";
			 public static string StartDate = @"start_date";
			 public static string EndDate = @"end_date";
			 public static string PageCount = @"page_count";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

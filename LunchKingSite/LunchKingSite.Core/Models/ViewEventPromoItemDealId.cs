using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEventPromoItemDealId class.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromoItemDealIdCollection : ReadOnlyList<ViewEventPromoItemDealId, ViewEventPromoItemDealIdCollection>
    {        
        public ViewEventPromoItemDealIdCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_event_promo_item_deal_id view.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromoItemDealId : ReadOnlyRecord<ViewEventPromoItemDealId>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_event_promo_item_deal_id", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarEventPromoId = new TableSchema.TableColumn(schema);
                colvarEventPromoId.ColumnName = "event_promo_id";
                colvarEventPromoId.DataType = DbType.Int32;
                colvarEventPromoId.MaxLength = 0;
                colvarEventPromoId.AutoIncrement = false;
                colvarEventPromoId.IsNullable = false;
                colvarEventPromoId.IsPrimaryKey = false;
                colvarEventPromoId.IsForeignKey = false;
                colvarEventPromoId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventPromoId);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = false;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarItemType = new TableSchema.TableColumn(schema);
                colvarItemType.ColumnName = "item_type";
                colvarItemType.DataType = DbType.Int32;
                colvarItemType.MaxLength = 0;
                colvarItemType.AutoIncrement = false;
                colvarItemType.IsNullable = false;
                colvarItemType.IsPrimaryKey = false;
                colvarItemType.IsForeignKey = false;
                colvarItemType.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemType);
                
                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = true;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBid);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_event_promo_item_deal_id",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewEventPromoItemDealId()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEventPromoItemDealId(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewEventPromoItemDealId(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewEventPromoItemDealId(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("EventPromoId")]
        [Bindable(true)]
        public int EventPromoId 
	    {
		    get
		    {
			    return GetColumnValue<int>("event_promo_id");
		    }
            set 
		    {
			    SetColumnValue("event_promo_id", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int ItemId 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("ItemType")]
        [Bindable(true)]
        public int ItemType 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_type");
		    }
            set 
		    {
			    SetColumnValue("item_type", value);
            }
        }
	      
        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid? Bid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("bid");
		    }
            set 
		    {
			    SetColumnValue("bid", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string EventPromoId = @"event_promo_id";
            
            public static string ItemId = @"item_id";
            
            public static string ItemType = @"item_type";
            
            public static string Bid = @"bid";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

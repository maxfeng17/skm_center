using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewOrderReturnFormList class.
    /// </summary>
    [Serializable]
    public partial class ViewOrderReturnFormListCollection : ReadOnlyList<ViewOrderReturnFormList, ViewOrderReturnFormListCollection>
    {        
        public ViewOrderReturnFormListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_order_return_form_list view.
    /// </summary>
    [Serializable]
    public partial class ViewOrderReturnFormList : ReadOnlyRecord<ViewOrderReturnFormList>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_order_return_form_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
                colvarProductGuid.ColumnName = "product_guid";
                colvarProductGuid.DataType = DbType.Guid;
                colvarProductGuid.MaxLength = 0;
                colvarProductGuid.AutoIncrement = false;
                colvarProductGuid.IsNullable = false;
                colvarProductGuid.IsPrimaryKey = false;
                colvarProductGuid.IsForeignKey = false;
                colvarProductGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductGuid);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = false;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarComboPackCount = new TableSchema.TableColumn(schema);
                colvarComboPackCount.ColumnName = "combo_pack_count";
                colvarComboPackCount.DataType = DbType.Int32;
                colvarComboPackCount.MaxLength = 0;
                colvarComboPackCount.AutoIncrement = false;
                colvarComboPackCount.IsNullable = false;
                colvarComboPackCount.IsPrimaryKey = false;
                colvarComboPackCount.IsForeignKey = false;
                colvarComboPackCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarComboPackCount);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 500;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = false;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.DateTime;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseEndTime);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarPhoneNumber = new TableSchema.TableColumn(schema);
                colvarPhoneNumber.ColumnName = "phone_number";
                colvarPhoneNumber.DataType = DbType.AnsiString;
                colvarPhoneNumber.MaxLength = 50;
                colvarPhoneNumber.AutoIncrement = false;
                colvarPhoneNumber.IsNullable = true;
                colvarPhoneNumber.IsPrimaryKey = false;
                colvarPhoneNumber.IsForeignKey = false;
                colvarPhoneNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhoneNumber);
                
                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryAddress);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = false;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarOrderShipId = new TableSchema.TableColumn(schema);
                colvarOrderShipId.ColumnName = "order_ship_id";
                colvarOrderShipId.DataType = DbType.Int32;
                colvarOrderShipId.MaxLength = 0;
                colvarOrderShipId.AutoIncrement = false;
                colvarOrderShipId.IsNullable = true;
                colvarOrderShipId.IsPrimaryKey = false;
                colvarOrderShipId.IsForeignKey = false;
                colvarOrderShipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderShipId);
                
                TableSchema.TableColumn colvarShipTime = new TableSchema.TableColumn(schema);
                colvarShipTime.ColumnName = "ship_time";
                colvarShipTime.DataType = DbType.DateTime;
                colvarShipTime.MaxLength = 0;
                colvarShipTime.AutoIncrement = false;
                colvarShipTime.IsNullable = true;
                colvarShipTime.IsPrimaryKey = false;
                colvarShipTime.IsForeignKey = false;
                colvarShipTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipTime);
                
                TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
                colvarReturnFormId.ColumnName = "return_form_id";
                colvarReturnFormId.DataType = DbType.Int32;
                colvarReturnFormId.MaxLength = 0;
                colvarReturnFormId.AutoIncrement = false;
                colvarReturnFormId.IsNullable = false;
                colvarReturnFormId.IsPrimaryKey = false;
                colvarReturnFormId.IsForeignKey = false;
                colvarReturnFormId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnFormId);
                
                TableSchema.TableColumn colvarReturnApplicationTime = new TableSchema.TableColumn(schema);
                colvarReturnApplicationTime.ColumnName = "return_application_time";
                colvarReturnApplicationTime.DataType = DbType.DateTime;
                colvarReturnApplicationTime.MaxLength = 0;
                colvarReturnApplicationTime.AutoIncrement = false;
                colvarReturnApplicationTime.IsNullable = false;
                colvarReturnApplicationTime.IsPrimaryKey = false;
                colvarReturnApplicationTime.IsForeignKey = false;
                colvarReturnApplicationTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnApplicationTime);
                
                TableSchema.TableColumn colvarReturnReason = new TableSchema.TableColumn(schema);
                colvarReturnReason.ColumnName = "return_reason";
                colvarReturnReason.DataType = DbType.String;
                colvarReturnReason.MaxLength = 4000;
                colvarReturnReason.AutoIncrement = false;
                colvarReturnReason.IsNullable = true;
                colvarReturnReason.IsPrimaryKey = false;
                colvarReturnReason.IsForeignKey = false;
                colvarReturnReason.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnReason);
                
                TableSchema.TableColumn colvarProgressStatus = new TableSchema.TableColumn(schema);
                colvarProgressStatus.ColumnName = "progress_status";
                colvarProgressStatus.DataType = DbType.Int32;
                colvarProgressStatus.MaxLength = 0;
                colvarProgressStatus.AutoIncrement = false;
                colvarProgressStatus.IsNullable = false;
                colvarProgressStatus.IsPrimaryKey = false;
                colvarProgressStatus.IsForeignKey = false;
                colvarProgressStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarProgressStatus);
                
                TableSchema.TableColumn colvarVendorProcessTime = new TableSchema.TableColumn(schema);
                colvarVendorProcessTime.ColumnName = "vendor_process_time";
                colvarVendorProcessTime.DataType = DbType.DateTime;
                colvarVendorProcessTime.MaxLength = 0;
                colvarVendorProcessTime.AutoIncrement = false;
                colvarVendorProcessTime.IsNullable = true;
                colvarVendorProcessTime.IsPrimaryKey = false;
                colvarVendorProcessTime.IsForeignKey = false;
                colvarVendorProcessTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorProcessTime);
                
                TableSchema.TableColumn colvarVendorProgressStatus = new TableSchema.TableColumn(schema);
                colvarVendorProgressStatus.ColumnName = "vendor_progress_status";
                colvarVendorProgressStatus.DataType = DbType.Int32;
                colvarVendorProgressStatus.MaxLength = 0;
                colvarVendorProgressStatus.AutoIncrement = false;
                colvarVendorProgressStatus.IsNullable = true;
                colvarVendorProgressStatus.IsPrimaryKey = false;
                colvarVendorProgressStatus.IsForeignKey = false;
                colvarVendorProgressStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorProgressStatus);
                
                TableSchema.TableColumn colvarVendorMemo = new TableSchema.TableColumn(schema);
                colvarVendorMemo.ColumnName = "vendor_memo";
                colvarVendorMemo.DataType = DbType.String;
                colvarVendorMemo.MaxLength = 30;
                colvarVendorMemo.AutoIncrement = false;
                colvarVendorMemo.IsNullable = true;
                colvarVendorMemo.IsPrimaryKey = false;
                colvarVendorMemo.IsForeignKey = false;
                colvarVendorMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorMemo);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarRefundType = new TableSchema.TableColumn(schema);
                colvarRefundType.ColumnName = "refund_type";
                colvarRefundType.DataType = DbType.Int32;
                colvarRefundType.MaxLength = 0;
                colvarRefundType.AutoIncrement = false;
                colvarRefundType.IsNullable = false;
                colvarRefundType.IsPrimaryKey = false;
                colvarRefundType.IsForeignKey = false;
                colvarRefundType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRefundType);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = false;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarReceiverName = new TableSchema.TableColumn(schema);
                colvarReceiverName.ColumnName = "receiver_name";
                colvarReceiverName.DataType = DbType.String;
                colvarReceiverName.MaxLength = 50;
                colvarReceiverName.AutoIncrement = false;
                colvarReceiverName.IsNullable = true;
                colvarReceiverName.IsPrimaryKey = false;
                colvarReceiverName.IsForeignKey = false;
                colvarReceiverName.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverName);
                
                TableSchema.TableColumn colvarReceiverAddress = new TableSchema.TableColumn(schema);
                colvarReceiverAddress.ColumnName = "receiver_address";
                colvarReceiverAddress.DataType = DbType.String;
                colvarReceiverAddress.MaxLength = 200;
                colvarReceiverAddress.AutoIncrement = false;
                colvarReceiverAddress.IsNullable = true;
                colvarReceiverAddress.IsPrimaryKey = false;
                colvarReceiverAddress.IsForeignKey = false;
                colvarReceiverAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverAddress);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
                colvarSellerEmail.ColumnName = "seller_email";
                colvarSellerEmail.DataType = DbType.String;
                colvarSellerEmail.MaxLength = 200;
                colvarSellerEmail.AutoIncrement = false;
                colvarSellerEmail.IsNullable = true;
                colvarSellerEmail.IsPrimaryKey = false;
                colvarSellerEmail.IsForeignKey = false;
                colvarSellerEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerEmail);
                
                TableSchema.TableColumn colvarSignCompanyID = new TableSchema.TableColumn(schema);
                colvarSignCompanyID.ColumnName = "SignCompanyID";
                colvarSignCompanyID.DataType = DbType.AnsiString;
                colvarSignCompanyID.MaxLength = 20;
                colvarSignCompanyID.AutoIncrement = false;
                colvarSignCompanyID.IsNullable = true;
                colvarSignCompanyID.IsPrimaryKey = false;
                colvarSignCompanyID.IsForeignKey = false;
                colvarSignCompanyID.IsReadOnly = false;
                
                schema.Columns.Add(colvarSignCompanyID);
                
                TableSchema.TableColumn colvarReturnedPersonName = new TableSchema.TableColumn(schema);
                colvarReturnedPersonName.ColumnName = "returned_person_name";
                colvarReturnedPersonName.DataType = DbType.String;
                colvarReturnedPersonName.MaxLength = 100;
                colvarReturnedPersonName.AutoIncrement = false;
                colvarReturnedPersonName.IsNullable = true;
                colvarReturnedPersonName.IsPrimaryKey = false;
                colvarReturnedPersonName.IsForeignKey = false;
                colvarReturnedPersonName.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonName);
                
                TableSchema.TableColumn colvarReturnedPersonTel = new TableSchema.TableColumn(schema);
                colvarReturnedPersonTel.ColumnName = "returned_person_tel";
                colvarReturnedPersonTel.DataType = DbType.AnsiString;
                colvarReturnedPersonTel.MaxLength = 100;
                colvarReturnedPersonTel.AutoIncrement = false;
                colvarReturnedPersonTel.IsNullable = true;
                colvarReturnedPersonTel.IsPrimaryKey = false;
                colvarReturnedPersonTel.IsForeignKey = false;
                colvarReturnedPersonTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonTel);
                
                TableSchema.TableColumn colvarReturnedPersonEmail = new TableSchema.TableColumn(schema);
                colvarReturnedPersonEmail.ColumnName = "returned_person_email";
                colvarReturnedPersonEmail.DataType = DbType.String;
                colvarReturnedPersonEmail.MaxLength = 200;
                colvarReturnedPersonEmail.AutoIncrement = false;
                colvarReturnedPersonEmail.IsNullable = true;
                colvarReturnedPersonEmail.IsPrimaryKey = false;
                colvarReturnedPersonEmail.IsForeignKey = false;
                colvarReturnedPersonEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonEmail);
                
                TableSchema.TableColumn colvarDeEmpName = new TableSchema.TableColumn(schema);
                colvarDeEmpName.ColumnName = "de_emp_name";
                colvarDeEmpName.DataType = DbType.Int32;
                colvarDeEmpName.MaxLength = 0;
                colvarDeEmpName.AutoIncrement = false;
                colvarDeEmpName.IsNullable = false;
                colvarDeEmpName.IsPrimaryKey = false;
                colvarDeEmpName.IsForeignKey = false;
                colvarDeEmpName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeEmpName);
                
                TableSchema.TableColumn colvarOpEmpName = new TableSchema.TableColumn(schema);
                colvarOpEmpName.ColumnName = "op_emp_name";
                colvarOpEmpName.DataType = DbType.Int32;
                colvarOpEmpName.MaxLength = 0;
                colvarOpEmpName.AutoIncrement = false;
                colvarOpEmpName.IsNullable = true;
                colvarOpEmpName.IsPrimaryKey = false;
                colvarOpEmpName.IsForeignKey = false;
                colvarOpEmpName.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpEmpName);
                
                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = false;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType);
                
                TableSchema.TableColumn colvarLabelIconList = new TableSchema.TableColumn(schema);
                colvarLabelIconList.ColumnName = "label_icon_list";
                colvarLabelIconList.DataType = DbType.AnsiString;
                colvarLabelIconList.MaxLength = 255;
                colvarLabelIconList.AutoIncrement = false;
                colvarLabelIconList.IsNullable = true;
                colvarLabelIconList.IsPrimaryKey = false;
                colvarLabelIconList.IsForeignKey = false;
                colvarLabelIconList.IsReadOnly = false;
                
                schema.Columns.Add(colvarLabelIconList);
                
                TableSchema.TableColumn colvarThirdPartyPaymentSystem = new TableSchema.TableColumn(schema);
                colvarThirdPartyPaymentSystem.ColumnName = "third_party_payment_system";
                colvarThirdPartyPaymentSystem.DataType = DbType.Byte;
                colvarThirdPartyPaymentSystem.MaxLength = 0;
                colvarThirdPartyPaymentSystem.AutoIncrement = false;
                colvarThirdPartyPaymentSystem.IsNullable = false;
                colvarThirdPartyPaymentSystem.IsPrimaryKey = false;
                colvarThirdPartyPaymentSystem.IsForeignKey = false;
                colvarThirdPartyPaymentSystem.IsReadOnly = false;
                
                schema.Columns.Add(colvarThirdPartyPaymentSystem);
                
                TableSchema.TableColumn colvarProductDeliveryType = new TableSchema.TableColumn(schema);
                colvarProductDeliveryType.ColumnName = "product_delivery_type";
                colvarProductDeliveryType.DataType = DbType.Int32;
                colvarProductDeliveryType.MaxLength = 0;
                colvarProductDeliveryType.AutoIncrement = false;
                colvarProductDeliveryType.IsNullable = true;
                colvarProductDeliveryType.IsPrimaryKey = false;
                colvarProductDeliveryType.IsForeignKey = false;
                colvarProductDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductDeliveryType);
                
                TableSchema.TableColumn colvarIsReceipt = new TableSchema.TableColumn(schema);
                colvarIsReceipt.ColumnName = "is_receipt";
                colvarIsReceipt.DataType = DbType.Boolean;
                colvarIsReceipt.MaxLength = 0;
                colvarIsReceipt.AutoIncrement = false;
                colvarIsReceipt.IsNullable = true;
                colvarIsReceipt.IsPrimaryKey = false;
                colvarIsReceipt.IsForeignKey = false;
                colvarIsReceipt.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsReceipt);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarRefundFrom = new TableSchema.TableColumn(schema);
                colvarRefundFrom.ColumnName = "refund_from";
                colvarRefundFrom.DataType = DbType.Int32;
                colvarRefundFrom.MaxLength = 0;
                colvarRefundFrom.AutoIncrement = false;
                colvarRefundFrom.IsNullable = true;
                colvarRefundFrom.IsPrimaryKey = false;
                colvarRefundFrom.IsForeignKey = false;
                colvarRefundFrom.IsReadOnly = false;
                
                schema.Columns.Add(colvarRefundFrom);
                
                TableSchema.TableColumn colvarWmsOrderId = new TableSchema.TableColumn(schema);
                colvarWmsOrderId.ColumnName = "wms_order_id";
                colvarWmsOrderId.DataType = DbType.String;
                colvarWmsOrderId.MaxLength = 20;
                colvarWmsOrderId.AutoIncrement = false;
                colvarWmsOrderId.IsNullable = true;
                colvarWmsOrderId.IsPrimaryKey = false;
                colvarWmsOrderId.IsForeignKey = false;
                colvarWmsOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarWmsOrderId);
                
                TableSchema.TableColumn colvarOrderCreateId = new TableSchema.TableColumn(schema);
                colvarOrderCreateId.ColumnName = "order_create_id";
                colvarOrderCreateId.DataType = DbType.String;
                colvarOrderCreateId.MaxLength = 30;
                colvarOrderCreateId.AutoIncrement = false;
                colvarOrderCreateId.IsNullable = false;
                colvarOrderCreateId.IsPrimaryKey = false;
                colvarOrderCreateId.IsForeignKey = false;
                colvarOrderCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCreateId);
                
                TableSchema.TableColumn colvarIsCreditNoteReceived = new TableSchema.TableColumn(schema);
                colvarIsCreditNoteReceived.ColumnName = "is_credit_note_received";
                colvarIsCreditNoteReceived.DataType = DbType.Boolean;
                colvarIsCreditNoteReceived.MaxLength = 0;
                colvarIsCreditNoteReceived.AutoIncrement = false;
                colvarIsCreditNoteReceived.IsNullable = false;
                colvarIsCreditNoteReceived.IsPrimaryKey = false;
                colvarIsCreditNoteReceived.IsForeignKey = false;
                colvarIsCreditNoteReceived.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCreditNoteReceived);
                
                TableSchema.TableColumn colvarCreditNoteType = new TableSchema.TableColumn(schema);
                colvarCreditNoteType.ColumnName = "credit_note_type";
                colvarCreditNoteType.DataType = DbType.Int32;
                colvarCreditNoteType.MaxLength = 0;
                colvarCreditNoteType.AutoIncrement = false;
                colvarCreditNoteType.IsNullable = true;
                colvarCreditNoteType.IsPrimaryKey = false;
                colvarCreditNoteType.IsForeignKey = false;
                colvarCreditNoteType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreditNoteType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_order_return_form_list",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewOrderReturnFormList()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewOrderReturnFormList(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewOrderReturnFormList(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewOrderReturnFormList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ProductGuid")]
        [Bindable(true)]
        public Guid ProductGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("product_guid");
		    }
            set 
		    {
			    SetColumnValue("product_guid", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("ComboPackCount")]
        [Bindable(true)]
        public int ComboPackCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("combo_pack_count");
		    }
            set 
		    {
			    SetColumnValue("combo_pack_count", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_name");
		    }
            set 
		    {
			    SetColumnValue("deal_name", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public DateTime? UseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_end_time");
		    }
            set 
		    {
			    SetColumnValue("use_end_time", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("PhoneNumber")]
        [Bindable(true)]
        public string PhoneNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("phone_number");
		    }
            set 
		    {
			    SetColumnValue("phone_number", value);
            }
        }
	      
        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_address");
		    }
            set 
		    {
			    SetColumnValue("delivery_address", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal Total 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("OrderShipId")]
        [Bindable(true)]
        public int? OrderShipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_ship_id");
		    }
            set 
		    {
			    SetColumnValue("order_ship_id", value);
            }
        }
	      
        [XmlAttribute("ShipTime")]
        [Bindable(true)]
        public DateTime? ShipTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ship_time");
		    }
            set 
		    {
			    SetColumnValue("ship_time", value);
            }
        }
	      
        [XmlAttribute("ReturnFormId")]
        [Bindable(true)]
        public int ReturnFormId 
	    {
		    get
		    {
			    return GetColumnValue<int>("return_form_id");
		    }
            set 
		    {
			    SetColumnValue("return_form_id", value);
            }
        }
	      
        [XmlAttribute("ReturnApplicationTime")]
        [Bindable(true)]
        public DateTime ReturnApplicationTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("return_application_time");
		    }
            set 
		    {
			    SetColumnValue("return_application_time", value);
            }
        }
	      
        [XmlAttribute("ReturnReason")]
        [Bindable(true)]
        public string ReturnReason 
	    {
		    get
		    {
			    return GetColumnValue<string>("return_reason");
		    }
            set 
		    {
			    SetColumnValue("return_reason", value);
            }
        }
	      
        [XmlAttribute("ProgressStatus")]
        [Bindable(true)]
        public int ProgressStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("progress_status");
		    }
            set 
		    {
			    SetColumnValue("progress_status", value);
            }
        }
	      
        [XmlAttribute("VendorProcessTime")]
        [Bindable(true)]
        public DateTime? VendorProcessTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("vendor_process_time");
		    }
            set 
		    {
			    SetColumnValue("vendor_process_time", value);
            }
        }
	      
        [XmlAttribute("VendorProgressStatus")]
        [Bindable(true)]
        public int? VendorProgressStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vendor_progress_status");
		    }
            set 
		    {
			    SetColumnValue("vendor_progress_status", value);
            }
        }
	      
        [XmlAttribute("VendorMemo")]
        [Bindable(true)]
        public string VendorMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("vendor_memo");
		    }
            set 
		    {
			    SetColumnValue("vendor_memo", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("RefundType")]
        [Bindable(true)]
        public int RefundType 
	    {
		    get
		    {
			    return GetColumnValue<int>("refund_type");
		    }
            set 
		    {
			    SetColumnValue("refund_type", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("ReceiverName")]
        [Bindable(true)]
        public string ReceiverName 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_name");
		    }
            set 
		    {
			    SetColumnValue("receiver_name", value);
            }
        }
	      
        [XmlAttribute("ReceiverAddress")]
        [Bindable(true)]
        public string ReceiverAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_address");
		    }
            set 
		    {
			    SetColumnValue("receiver_address", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerEmail")]
        [Bindable(true)]
        public string SellerEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_email");
		    }
            set 
		    {
			    SetColumnValue("seller_email", value);
            }
        }
	      
        [XmlAttribute("SignCompanyID")]
        [Bindable(true)]
        public string SignCompanyID 
	    {
		    get
		    {
			    return GetColumnValue<string>("SignCompanyID");
		    }
            set 
		    {
			    SetColumnValue("SignCompanyID", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonName")]
        [Bindable(true)]
        public string ReturnedPersonName 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_name");
		    }
            set 
		    {
			    SetColumnValue("returned_person_name", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonTel")]
        [Bindable(true)]
        public string ReturnedPersonTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_tel");
		    }
            set 
		    {
			    SetColumnValue("returned_person_tel", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonEmail")]
        [Bindable(true)]
        public string ReturnedPersonEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_email");
		    }
            set 
		    {
			    SetColumnValue("returned_person_email", value);
            }
        }
	      
        [XmlAttribute("DeEmpName")]
        [Bindable(true)]
        public int DeEmpName 
	    {
		    get
		    {
			    return GetColumnValue<int>("de_emp_name");
		    }
            set 
		    {
			    SetColumnValue("de_emp_name", value);
            }
        }
	      
        [XmlAttribute("OpEmpName")]
        [Bindable(true)]
        public int? OpEmpName 
	    {
		    get
		    {
			    return GetColumnValue<int?>("op_emp_name");
		    }
            set 
		    {
			    SetColumnValue("op_emp_name", value);
            }
        }
	      
        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int DealType 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_type");
		    }
            set 
		    {
			    SetColumnValue("deal_type", value);
            }
        }
	      
        [XmlAttribute("LabelIconList")]
        [Bindable(true)]
        public string LabelIconList 
	    {
		    get
		    {
			    return GetColumnValue<string>("label_icon_list");
		    }
            set 
		    {
			    SetColumnValue("label_icon_list", value);
            }
        }
	      
        [XmlAttribute("ThirdPartyPaymentSystem")]
        [Bindable(true)]
        public byte ThirdPartyPaymentSystem 
	    {
		    get
		    {
			    return GetColumnValue<byte>("third_party_payment_system");
		    }
            set 
		    {
			    SetColumnValue("third_party_payment_system", value);
            }
        }
	      
        [XmlAttribute("ProductDeliveryType")]
        [Bindable(true)]
        public int? ProductDeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_delivery_type");
		    }
            set 
		    {
			    SetColumnValue("product_delivery_type", value);
            }
        }
	      
        [XmlAttribute("IsReceipt")]
        [Bindable(true)]
        public bool? IsReceipt 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_receipt");
		    }
            set 
		    {
			    SetColumnValue("is_receipt", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("RefundFrom")]
        [Bindable(true)]
        public int? RefundFrom 
	    {
		    get
		    {
			    return GetColumnValue<int?>("refund_from");
		    }
            set 
		    {
			    SetColumnValue("refund_from", value);
            }
        }
	      
        [XmlAttribute("WmsOrderId")]
        [Bindable(true)]
        public string WmsOrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("wms_order_id");
		    }
            set 
		    {
			    SetColumnValue("wms_order_id", value);
            }
        }
	      
        [XmlAttribute("OrderCreateId")]
        [Bindable(true)]
        public string OrderCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_create_id");
		    }
            set 
		    {
			    SetColumnValue("order_create_id", value);
            }
        }
	      
        [XmlAttribute("IsCreditNoteReceived")]
        [Bindable(true)]
        public bool IsCreditNoteReceived 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_credit_note_received");
		    }
            set 
		    {
			    SetColumnValue("is_credit_note_received", value);
            }
        }
	      
        [XmlAttribute("CreditNoteType")]
        [Bindable(true)]
        public int? CreditNoteType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("credit_note_type");
		    }
            set 
		    {
			    SetColumnValue("credit_note_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ProductGuid = @"product_guid";
            
            public static string ProductId = @"product_id";
            
            public static string ComboPackCount = @"combo_pack_count";
            
            public static string DealName = @"deal_name";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string UseEndTime = @"use_end_time";
            
            public static string OrderGuid = @"order_guid";
            
            public static string OrderId = @"order_id";
            
            public static string MemberName = @"member_name";
            
            public static string PhoneNumber = @"phone_number";
            
            public static string DeliveryAddress = @"delivery_address";
            
            public static string OrderStatus = @"order_status";
            
            public static string Total = @"total";
            
            public static string OrderShipId = @"order_ship_id";
            
            public static string ShipTime = @"ship_time";
            
            public static string ReturnFormId = @"return_form_id";
            
            public static string ReturnApplicationTime = @"return_application_time";
            
            public static string ReturnReason = @"return_reason";
            
            public static string ProgressStatus = @"progress_status";
            
            public static string VendorProcessTime = @"vendor_process_time";
            
            public static string VendorProgressStatus = @"vendor_progress_status";
            
            public static string VendorMemo = @"vendor_memo";
            
            public static string ModifyTime = @"modify_time";
            
            public static string RefundType = @"refund_type";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string ReceiverName = @"receiver_name";
            
            public static string ReceiverAddress = @"receiver_address";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerEmail = @"seller_email";
            
            public static string SignCompanyID = @"SignCompanyID";
            
            public static string ReturnedPersonName = @"returned_person_name";
            
            public static string ReturnedPersonTel = @"returned_person_tel";
            
            public static string ReturnedPersonEmail = @"returned_person_email";
            
            public static string DeEmpName = @"de_emp_name";
            
            public static string OpEmpName = @"op_emp_name";
            
            public static string DealType = @"deal_type";
            
            public static string LabelIconList = @"label_icon_list";
            
            public static string ThirdPartyPaymentSystem = @"third_party_payment_system";
            
            public static string ProductDeliveryType = @"product_delivery_type";
            
            public static string IsReceipt = @"is_receipt";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string RefundFrom = @"refund_from";
            
            public static string WmsOrderId = @"wms_order_id";
            
            public static string OrderCreateId = @"order_create_id";
            
            public static string IsCreditNoteReceived = @"is_credit_note_received";
            
            public static string CreditNoteType = @"credit_note_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

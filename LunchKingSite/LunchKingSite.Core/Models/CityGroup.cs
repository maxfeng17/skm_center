using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CityGroup class.
	/// </summary>
    [Serializable]
	public partial class CityGroupCollection : RepositoryList<CityGroup, CityGroupCollection>
	{	   
		public CityGroupCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CityGroupCollection</returns>
		public CityGroupCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CityGroup o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the city_group table.
	/// </summary>
	[Serializable]
	public partial class CityGroup : RepositoryRecord<CityGroup>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CityGroup()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CityGroup(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("city_group", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = false;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
				colvarCityName.ColumnName = "city_name";
				colvarCityName.DataType = DbType.String;
				colvarCityName.MaxLength = 20;
				colvarCityName.AutoIncrement = false;
				colvarCityName.IsNullable = false;
				colvarCityName.IsPrimaryKey = false;
				colvarCityName.IsForeignKey = false;
				colvarCityName.IsReadOnly = false;
				colvarCityName.DefaultSetting = @"";
				colvarCityName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityName);
				
				TableSchema.TableColumn colvarCityCode = new TableSchema.TableColumn(schema);
				colvarCityCode.ColumnName = "city_code";
				colvarCityCode.DataType = DbType.AnsiString;
				colvarCityCode.MaxLength = 10;
				colvarCityCode.AutoIncrement = false;
				colvarCityCode.IsNullable = true;
				colvarCityCode.IsPrimaryKey = false;
				colvarCityCode.IsForeignKey = false;
				colvarCityCode.IsReadOnly = false;
				colvarCityCode.DefaultSetting = @"";
				colvarCityCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityCode);
				
				TableSchema.TableColumn colvarRegionId = new TableSchema.TableColumn(schema);
				colvarRegionId.ColumnName = "region_id";
				colvarRegionId.DataType = DbType.Int32;
				colvarRegionId.MaxLength = 0;
				colvarRegionId.AutoIncrement = false;
				colvarRegionId.IsNullable = false;
				colvarRegionId.IsPrimaryKey = false;
				colvarRegionId.IsForeignKey = false;
				colvarRegionId.IsReadOnly = false;
				colvarRegionId.DefaultSetting = @"";
				colvarRegionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRegionId);
				
				TableSchema.TableColumn colvarRegionName = new TableSchema.TableColumn(schema);
				colvarRegionName.ColumnName = "region_name";
				colvarRegionName.DataType = DbType.String;
				colvarRegionName.MaxLength = 20;
				colvarRegionName.AutoIncrement = false;
				colvarRegionName.IsNullable = false;
				colvarRegionName.IsPrimaryKey = false;
				colvarRegionName.IsForeignKey = false;
				colvarRegionName.IsReadOnly = false;
				colvarRegionName.DefaultSetting = @"";
				colvarRegionName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRegionName);
				
				TableSchema.TableColumn colvarRegionCode = new TableSchema.TableColumn(schema);
				colvarRegionCode.ColumnName = "region_code";
				colvarRegionCode.DataType = DbType.AnsiString;
				colvarRegionCode.MaxLength = 10;
				colvarRegionCode.AutoIncrement = false;
				colvarRegionCode.IsNullable = true;
				colvarRegionCode.IsPrimaryKey = false;
				colvarRegionCode.IsForeignKey = false;
				colvarRegionCode.IsReadOnly = false;
				colvarRegionCode.DefaultSetting = @"";
				colvarRegionCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRegionCode);
				
				TableSchema.TableColumn colvarEnable = new TableSchema.TableColumn(schema);
				colvarEnable.ColumnName = "enable";
				colvarEnable.DataType = DbType.Boolean;
				colvarEnable.MaxLength = 0;
				colvarEnable.AutoIncrement = false;
				colvarEnable.IsNullable = false;
				colvarEnable.IsPrimaryKey = false;
				colvarEnable.IsForeignKey = false;
				colvarEnable.IsReadOnly = false;
				
						colvarEnable.DefaultSetting = @"((0))";
				colvarEnable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnable);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarBitNumber = new TableSchema.TableColumn(schema);
				colvarBitNumber.ColumnName = "bit_number";
				colvarBitNumber.DataType = DbType.Int32;
				colvarBitNumber.MaxLength = 0;
				colvarBitNumber.AutoIncrement = false;
				colvarBitNumber.IsNullable = false;
				colvarBitNumber.IsPrimaryKey = false;
				colvarBitNumber.IsForeignKey = false;
				colvarBitNumber.IsReadOnly = false;
				
						colvarBitNumber.DefaultSetting = @"((0))";
				colvarBitNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBitNumber);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("city_group",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int CityId 
		{
			get { return GetColumnValue<int>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("CityName")]
		[Bindable(true)]
		public string CityName 
		{
			get { return GetColumnValue<string>(Columns.CityName); }
			set { SetColumnValue(Columns.CityName, value); }
		}
		  
		[XmlAttribute("CityCode")]
		[Bindable(true)]
		public string CityCode 
		{
			get { return GetColumnValue<string>(Columns.CityCode); }
			set { SetColumnValue(Columns.CityCode, value); }
		}
		  
		[XmlAttribute("RegionId")]
		[Bindable(true)]
		public int RegionId 
		{
			get { return GetColumnValue<int>(Columns.RegionId); }
			set { SetColumnValue(Columns.RegionId, value); }
		}
		  
		[XmlAttribute("RegionName")]
		[Bindable(true)]
		public string RegionName 
		{
			get { return GetColumnValue<string>(Columns.RegionName); }
			set { SetColumnValue(Columns.RegionName, value); }
		}
		  
		[XmlAttribute("RegionCode")]
		[Bindable(true)]
		public string RegionCode 
		{
			get { return GetColumnValue<string>(Columns.RegionCode); }
			set { SetColumnValue(Columns.RegionCode, value); }
		}
		  
		[XmlAttribute("Enable")]
		[Bindable(true)]
		public bool Enable 
		{
			get { return GetColumnValue<bool>(Columns.Enable); }
			set { SetColumnValue(Columns.Enable, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("BitNumber")]
		[Bindable(true)]
		public int BitNumber 
		{
			get { return GetColumnValue<int>(Columns.BitNumber); }
			set { SetColumnValue(Columns.BitNumber, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CityNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CityCodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn RegionIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn RegionNameColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn RegionCodeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn EnableColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn BitNumberColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CityId = @"city_id";
			 public static string CityName = @"city_name";
			 public static string CityCode = @"city_code";
			 public static string RegionId = @"region_id";
			 public static string RegionName = @"region_name";
			 public static string RegionCode = @"region_code";
			 public static string Enable = @"enable";
			 public static string Type = @"type";
			 public static string BitNumber = @"bit_number";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SellerTreeLog class.
	/// </summary>
    [Serializable]
	public partial class SellerTreeLogCollection : RepositoryList<SellerTreeLog, SellerTreeLogCollection>
	{	   
		public SellerTreeLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SellerTreeLogCollection</returns>
		public SellerTreeLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SellerTreeLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the seller_tree_log table.
	/// </summary>
	[Serializable]
	public partial class SellerTreeLog : RepositoryRecord<SellerTreeLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SellerTreeLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SellerTreeLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("seller_tree_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarParentSellerGuid = new TableSchema.TableColumn(schema);
				colvarParentSellerGuid.ColumnName = "parent_seller_guid";
				colvarParentSellerGuid.DataType = DbType.Guid;
				colvarParentSellerGuid.MaxLength = 0;
				colvarParentSellerGuid.AutoIncrement = false;
				colvarParentSellerGuid.IsNullable = false;
				colvarParentSellerGuid.IsPrimaryKey = false;
				colvarParentSellerGuid.IsForeignKey = false;
				colvarParentSellerGuid.IsReadOnly = false;
				colvarParentSellerGuid.DefaultSetting = @"";
				colvarParentSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentSellerGuid);
				
				TableSchema.TableColumn colvarRootSellerGuid = new TableSchema.TableColumn(schema);
				colvarRootSellerGuid.ColumnName = "root_seller_guid";
				colvarRootSellerGuid.DataType = DbType.Guid;
				colvarRootSellerGuid.MaxLength = 0;
				colvarRootSellerGuid.AutoIncrement = false;
				colvarRootSellerGuid.IsNullable = false;
				colvarRootSellerGuid.IsPrimaryKey = false;
				colvarRootSellerGuid.IsForeignKey = false;
				colvarRootSellerGuid.IsReadOnly = false;
				colvarRootSellerGuid.DefaultSetting = @"";
				colvarRootSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRootSellerGuid);
				
				TableSchema.TableColumn colvarSort = new TableSchema.TableColumn(schema);
				colvarSort.ColumnName = "sort";
				colvarSort.DataType = DbType.Int32;
				colvarSort.MaxLength = 0;
				colvarSort.AutoIncrement = false;
				colvarSort.IsNullable = false;
				colvarSort.IsPrimaryKey = false;
				colvarSort.IsForeignKey = false;
				colvarSort.IsReadOnly = false;
				colvarSort.DefaultSetting = @"";
				colvarSort.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSort);
				
				TableSchema.TableColumn colvarReason = new TableSchema.TableColumn(schema);
				colvarReason.ColumnName = "reason";
				colvarReason.DataType = DbType.String;
				colvarReason.MaxLength = 100;
				colvarReason.AutoIncrement = false;
				colvarReason.IsNullable = true;
				colvarReason.IsPrimaryKey = false;
				colvarReason.IsForeignKey = false;
				colvarReason.IsReadOnly = false;
				colvarReason.DefaultSetting = @"";
				colvarReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReason);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarSellerTreeModifyTime = new TableSchema.TableColumn(schema);
				colvarSellerTreeModifyTime.ColumnName = "seller_tree_modify_time";
				colvarSellerTreeModifyTime.DataType = DbType.DateTime;
				colvarSellerTreeModifyTime.MaxLength = 0;
				colvarSellerTreeModifyTime.AutoIncrement = false;
				colvarSellerTreeModifyTime.IsNullable = true;
				colvarSellerTreeModifyTime.IsPrimaryKey = false;
				colvarSellerTreeModifyTime.IsForeignKey = false;
				colvarSellerTreeModifyTime.IsReadOnly = false;
				colvarSellerTreeModifyTime.DefaultSetting = @"";
				colvarSellerTreeModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerTreeModifyTime);
				
				TableSchema.TableColumn colvarSellerTreeModifyId = new TableSchema.TableColumn(schema);
				colvarSellerTreeModifyId.ColumnName = "seller_tree_modify_id";
				colvarSellerTreeModifyId.DataType = DbType.String;
				colvarSellerTreeModifyId.MaxLength = 256;
				colvarSellerTreeModifyId.AutoIncrement = false;
				colvarSellerTreeModifyId.IsNullable = true;
				colvarSellerTreeModifyId.IsPrimaryKey = false;
				colvarSellerTreeModifyId.IsForeignKey = false;
				colvarSellerTreeModifyId.IsReadOnly = false;
				colvarSellerTreeModifyId.DefaultSetting = @"";
				colvarSellerTreeModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerTreeModifyId);
				
				TableSchema.TableColumn colvarSellerTreeCreateTime = new TableSchema.TableColumn(schema);
				colvarSellerTreeCreateTime.ColumnName = "seller_tree_create_time";
				colvarSellerTreeCreateTime.DataType = DbType.DateTime;
				colvarSellerTreeCreateTime.MaxLength = 0;
				colvarSellerTreeCreateTime.AutoIncrement = false;
				colvarSellerTreeCreateTime.IsNullable = true;
				colvarSellerTreeCreateTime.IsPrimaryKey = false;
				colvarSellerTreeCreateTime.IsForeignKey = false;
				colvarSellerTreeCreateTime.IsReadOnly = false;
				colvarSellerTreeCreateTime.DefaultSetting = @"";
				colvarSellerTreeCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerTreeCreateTime);
				
				TableSchema.TableColumn colvarSellerTreeCreateId = new TableSchema.TableColumn(schema);
				colvarSellerTreeCreateId.ColumnName = "seller_tree_create_id";
				colvarSellerTreeCreateId.DataType = DbType.String;
				colvarSellerTreeCreateId.MaxLength = 256;
				colvarSellerTreeCreateId.AutoIncrement = false;
				colvarSellerTreeCreateId.IsNullable = true;
				colvarSellerTreeCreateId.IsPrimaryKey = false;
				colvarSellerTreeCreateId.IsForeignKey = false;
				colvarSellerTreeCreateId.IsReadOnly = false;
				colvarSellerTreeCreateId.DefaultSetting = @"";
				colvarSellerTreeCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerTreeCreateId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("seller_tree_log",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		
		[XmlAttribute("ParentSellerGuid")]
		[Bindable(true)]
		public Guid ParentSellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.ParentSellerGuid); }
			set { SetColumnValue(Columns.ParentSellerGuid, value); }
		}
		
		[XmlAttribute("RootSellerGuid")]
		[Bindable(true)]
		public Guid RootSellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.RootSellerGuid); }
			set { SetColumnValue(Columns.RootSellerGuid, value); }
		}
		
		[XmlAttribute("Sort")]
		[Bindable(true)]
		public int Sort 
		{
			get { return GetColumnValue<int>(Columns.Sort); }
			set { SetColumnValue(Columns.Sort, value); }
		}
		
		[XmlAttribute("Reason")]
		[Bindable(true)]
		public string Reason 
		{
			get { return GetColumnValue<string>(Columns.Reason); }
			set { SetColumnValue(Columns.Reason, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		[XmlAttribute("SellerTreeModifyTime")]
		[Bindable(true)]
		public DateTime? SellerTreeModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.SellerTreeModifyTime); }
			set { SetColumnValue(Columns.SellerTreeModifyTime, value); }
		}
		
		[XmlAttribute("SellerTreeModifyId")]
		[Bindable(true)]
		public string SellerTreeModifyId 
		{
			get { return GetColumnValue<string>(Columns.SellerTreeModifyId); }
			set { SetColumnValue(Columns.SellerTreeModifyId, value); }
		}
		
		[XmlAttribute("SellerTreeCreateTime")]
		[Bindable(true)]
		public DateTime? SellerTreeCreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.SellerTreeCreateTime); }
			set { SetColumnValue(Columns.SellerTreeCreateTime, value); }
		}
		
		[XmlAttribute("SellerTreeCreateId")]
		[Bindable(true)]
		public string SellerTreeCreateId 
		{
			get { return GetColumnValue<string>(Columns.SellerTreeCreateId); }
			set { SetColumnValue(Columns.SellerTreeCreateId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ParentSellerGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RootSellerGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SortColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ReasonColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerTreeModifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerTreeModifyIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerTreeCreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerTreeCreateIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SellerGuid = @"seller_guid";
			 public static string ParentSellerGuid = @"parent_seller_guid";
			 public static string RootSellerGuid = @"root_seller_guid";
			 public static string Sort = @"sort";
			 public static string Reason = @"reason";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
			 public static string SellerTreeModifyTime = @"seller_tree_modify_time";
			 public static string SellerTreeModifyId = @"seller_tree_modify_id";
			 public static string SellerTreeCreateTime = @"seller_tree_create_time";
			 public static string SellerTreeCreateId = @"seller_tree_create_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

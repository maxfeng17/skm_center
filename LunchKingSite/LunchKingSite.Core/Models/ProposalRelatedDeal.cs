﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProposalRelatedDeal class.
    /// </summary>
    [Serializable]
    public partial class ProposalRelatedDealCollection : RepositoryList<ProposalRelatedDeal, ProposalRelatedDealCollection>
    {
        public ProposalRelatedDealCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalRelatedDealCollection</returns>
        public ProposalRelatedDealCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProposalRelatedDeal o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the proposal_related_deal table.
    /// </summary>
    [Serializable]
    public partial class ProposalRelatedDeal : RepositoryRecord<ProposalRelatedDeal>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProposalRelatedDeal()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProposalRelatedDeal(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("proposal_related_deal", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarProposalId = new TableSchema.TableColumn(schema);
                colvarProposalId.ColumnName = "proposal_id";
                colvarProposalId.DataType = DbType.Int32;
                colvarProposalId.MaxLength = 0;
                colvarProposalId.AutoIncrement = false;
                colvarProposalId.IsNullable = false;
                colvarProposalId.IsPrimaryKey = false;
                colvarProposalId.IsForeignKey = false;
                colvarProposalId.IsReadOnly = false;
                colvarProposalId.DefaultSetting = @"";
                colvarProposalId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProposalId);

                TableSchema.TableColumn colvarRelatedId = new TableSchema.TableColumn(schema);
                colvarRelatedId.ColumnName = "related_id";
                colvarRelatedId.DataType = DbType.Int32;
                colvarRelatedId.MaxLength = 0;
                colvarRelatedId.AutoIncrement = false;
                colvarRelatedId.IsNullable = false;
                colvarRelatedId.IsPrimaryKey = false;
                colvarRelatedId.IsForeignKey = false;
                colvarRelatedId.IsReadOnly = false;
                colvarRelatedId.DefaultSetting = @"";
                colvarRelatedId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRelatedId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("proposal_related_deal", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("ProposalId")]
        [Bindable(true)]
        public int ProposalId
        {
            get { return GetColumnValue<int>(Columns.ProposalId); }
            set { SetColumnValue(Columns.ProposalId, value); }
        }

        [XmlAttribute("RelatedId")]
        [Bindable(true)]
        public int RelatedId
        {
            get { return GetColumnValue<int>(Columns.RelatedId); }
            set { SetColumnValue(Columns.RelatedId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ProposalIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn RelatedIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string ProposalId = @"proposal_id";
            public static string RelatedId = @"related_id";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

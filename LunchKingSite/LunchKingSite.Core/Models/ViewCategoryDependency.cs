using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewCategoryDependency class.
    /// </summary>
    [Serializable]
    public partial class ViewCategoryDependencyCollection : ReadOnlyList<ViewCategoryDependency, ViewCategoryDependencyCollection>
    {        
        public ViewCategoryDependencyCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_category_dependency view.
    /// </summary>
    [Serializable]
    public partial class ViewCategoryDependency : ReadOnlyRecord<ViewCategoryDependency>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_category_dependency", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarParentId = new TableSchema.TableColumn(schema);
                colvarParentId.ColumnName = "parent_id";
                colvarParentId.DataType = DbType.Int32;
                colvarParentId.MaxLength = 0;
                colvarParentId.AutoIncrement = false;
                colvarParentId.IsNullable = false;
                colvarParentId.IsPrimaryKey = false;
                colvarParentId.IsForeignKey = false;
                colvarParentId.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentId);
                
                TableSchema.TableColumn colvarParentName = new TableSchema.TableColumn(schema);
                colvarParentName.ColumnName = "parent_name";
                colvarParentName.DataType = DbType.String;
                colvarParentName.MaxLength = 50;
                colvarParentName.AutoIncrement = false;
                colvarParentName.IsNullable = true;
                colvarParentName.IsPrimaryKey = false;
                colvarParentName.IsForeignKey = false;
                colvarParentName.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentName);
                
                TableSchema.TableColumn colvarParentType = new TableSchema.TableColumn(schema);
                colvarParentType.ColumnName = "parent_type";
                colvarParentType.DataType = DbType.Int32;
                colvarParentType.MaxLength = 0;
                colvarParentType.AutoIncrement = false;
                colvarParentType.IsNullable = true;
                colvarParentType.IsPrimaryKey = false;
                colvarParentType.IsForeignKey = false;
                colvarParentType.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentType);
                
                TableSchema.TableColumn colvarParentCode = new TableSchema.TableColumn(schema);
                colvarParentCode.ColumnName = "parent_code";
                colvarParentCode.DataType = DbType.Int32;
                colvarParentCode.MaxLength = 0;
                colvarParentCode.AutoIncrement = false;
                colvarParentCode.IsNullable = true;
                colvarParentCode.IsPrimaryKey = false;
                colvarParentCode.IsForeignKey = false;
                colvarParentCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentCode);
                
                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = false;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarSeq);
                
                TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
                colvarCategoryId.ColumnName = "category_id";
                colvarCategoryId.DataType = DbType.Int32;
                colvarCategoryId.MaxLength = 0;
                colvarCategoryId.AutoIncrement = false;
                colvarCategoryId.IsNullable = false;
                colvarCategoryId.IsPrimaryKey = false;
                colvarCategoryId.IsForeignKey = false;
                colvarCategoryId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryId);
                
                TableSchema.TableColumn colvarCategoryName = new TableSchema.TableColumn(schema);
                colvarCategoryName.ColumnName = "category_name";
                colvarCategoryName.DataType = DbType.String;
                colvarCategoryName.MaxLength = 50;
                colvarCategoryName.AutoIncrement = false;
                colvarCategoryName.IsNullable = true;
                colvarCategoryName.IsPrimaryKey = false;
                colvarCategoryName.IsForeignKey = false;
                colvarCategoryName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryName);
                
                TableSchema.TableColumn colvarCategoryType = new TableSchema.TableColumn(schema);
                colvarCategoryType.ColumnName = "category_type";
                colvarCategoryType.DataType = DbType.Int32;
                colvarCategoryType.MaxLength = 0;
                colvarCategoryType.AutoIncrement = false;
                colvarCategoryType.IsNullable = true;
                colvarCategoryType.IsPrimaryKey = false;
                colvarCategoryType.IsForeignKey = false;
                colvarCategoryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryType);
                
                TableSchema.TableColumn colvarCategoryCode = new TableSchema.TableColumn(schema);
                colvarCategoryCode.ColumnName = "category_code";
                colvarCategoryCode.DataType = DbType.Int32;
                colvarCategoryCode.MaxLength = 0;
                colvarCategoryCode.AutoIncrement = false;
                colvarCategoryCode.IsNullable = true;
                colvarCategoryCode.IsPrimaryKey = false;
                colvarCategoryCode.IsForeignKey = false;
                colvarCategoryCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryCode);
                
                TableSchema.TableColumn colvarCategoryStatus = new TableSchema.TableColumn(schema);
                colvarCategoryStatus.ColumnName = "category_status";
                colvarCategoryStatus.DataType = DbType.Int32;
                colvarCategoryStatus.MaxLength = 0;
                colvarCategoryStatus.AutoIncrement = false;
                colvarCategoryStatus.IsNullable = false;
                colvarCategoryStatus.IsPrimaryKey = false;
                colvarCategoryStatus.IsForeignKey = false;
                colvarCategoryStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryStatus);
                
                TableSchema.TableColumn colvarIsShowFrontEnd = new TableSchema.TableColumn(schema);
                colvarIsShowFrontEnd.ColumnName = "is_show_front_end";
                colvarIsShowFrontEnd.DataType = DbType.Boolean;
                colvarIsShowFrontEnd.MaxLength = 0;
                colvarIsShowFrontEnd.AutoIncrement = false;
                colvarIsShowFrontEnd.IsNullable = false;
                colvarIsShowFrontEnd.IsPrimaryKey = false;
                colvarIsShowFrontEnd.IsForeignKey = false;
                colvarIsShowFrontEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsShowFrontEnd);
                
                TableSchema.TableColumn colvarIsShowBackEnd = new TableSchema.TableColumn(schema);
                colvarIsShowBackEnd.ColumnName = "is_show_back_end";
                colvarIsShowBackEnd.DataType = DbType.Boolean;
                colvarIsShowBackEnd.MaxLength = 0;
                colvarIsShowBackEnd.AutoIncrement = false;
                colvarIsShowBackEnd.IsNullable = false;
                colvarIsShowBackEnd.IsPrimaryKey = false;
                colvarIsShowBackEnd.IsForeignKey = false;
                colvarIsShowBackEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsShowBackEnd);
                
                TableSchema.TableColumn colvarIconType = new TableSchema.TableColumn(schema);
                colvarIconType.ColumnName = "icon_type";
                colvarIconType.DataType = DbType.Int32;
                colvarIconType.MaxLength = 0;
                colvarIconType.AutoIncrement = false;
                colvarIconType.IsNullable = false;
                colvarIconType.IsPrimaryKey = false;
                colvarIconType.IsForeignKey = false;
                colvarIconType.IsReadOnly = false;
                
                schema.Columns.Add(colvarIconType);
                
                TableSchema.TableColumn colvarImage = new TableSchema.TableColumn(schema);
                colvarImage.ColumnName = "image";
                colvarImage.DataType = DbType.String;
                colvarImage.MaxLength = 200;
                colvarImage.AutoIncrement = false;
                colvarImage.IsNullable = true;
                colvarImage.IsPrimaryKey = false;
                colvarImage.IsForeignKey = false;
                colvarImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarImage);
                
                TableSchema.TableColumn colvarIsFinal = new TableSchema.TableColumn(schema);
                colvarIsFinal.ColumnName = "is_final";
                colvarIsFinal.DataType = DbType.Boolean;
                colvarIsFinal.MaxLength = 0;
                colvarIsFinal.AutoIncrement = false;
                colvarIsFinal.IsNullable = false;
                colvarIsFinal.IsPrimaryKey = false;
                colvarIsFinal.IsForeignKey = false;
                colvarIsFinal.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsFinal);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_category_dependency",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewCategoryDependency()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewCategoryDependency(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewCategoryDependency(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewCategoryDependency(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ParentId")]
        [Bindable(true)]
        public int ParentId 
	    {
		    get
		    {
			    return GetColumnValue<int>("parent_id");
		    }
            set 
		    {
			    SetColumnValue("parent_id", value);
            }
        }
	      
        [XmlAttribute("ParentName")]
        [Bindable(true)]
        public string ParentName 
	    {
		    get
		    {
			    return GetColumnValue<string>("parent_name");
		    }
            set 
		    {
			    SetColumnValue("parent_name", value);
            }
        }
	      
        [XmlAttribute("ParentType")]
        [Bindable(true)]
        public int? ParentType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("parent_type");
		    }
            set 
		    {
			    SetColumnValue("parent_type", value);
            }
        }
	      
        [XmlAttribute("ParentCode")]
        [Bindable(true)]
        public int? ParentCode 
	    {
		    get
		    {
			    return GetColumnValue<int?>("parent_code");
		    }
            set 
		    {
			    SetColumnValue("parent_code", value);
            }
        }
	      
        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int Seq 
	    {
		    get
		    {
			    return GetColumnValue<int>("seq");
		    }
            set 
		    {
			    SetColumnValue("seq", value);
            }
        }
	      
        [XmlAttribute("CategoryId")]
        [Bindable(true)]
        public int CategoryId 
	    {
		    get
		    {
			    return GetColumnValue<int>("category_id");
		    }
            set 
		    {
			    SetColumnValue("category_id", value);
            }
        }
	      
        [XmlAttribute("CategoryName")]
        [Bindable(true)]
        public string CategoryName 
	    {
		    get
		    {
			    return GetColumnValue<string>("category_name");
		    }
            set 
		    {
			    SetColumnValue("category_name", value);
            }
        }
	      
        [XmlAttribute("CategoryType")]
        [Bindable(true)]
        public int? CategoryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category_type");
		    }
            set 
		    {
			    SetColumnValue("category_type", value);
            }
        }
	      
        [XmlAttribute("CategoryCode")]
        [Bindable(true)]
        public int? CategoryCode 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category_code");
		    }
            set 
		    {
			    SetColumnValue("category_code", value);
            }
        }
	      
        [XmlAttribute("CategoryStatus")]
        [Bindable(true)]
        public int CategoryStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("category_status");
		    }
            set 
		    {
			    SetColumnValue("category_status", value);
            }
        }
	      
        [XmlAttribute("IsShowFrontEnd")]
        [Bindable(true)]
        public bool IsShowFrontEnd 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_show_front_end");
		    }
            set 
		    {
			    SetColumnValue("is_show_front_end", value);
            }
        }
	      
        [XmlAttribute("IsShowBackEnd")]
        [Bindable(true)]
        public bool IsShowBackEnd 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_show_back_end");
		    }
            set 
		    {
			    SetColumnValue("is_show_back_end", value);
            }
        }
	      
        [XmlAttribute("IconType")]
        [Bindable(true)]
        public int IconType 
	    {
		    get
		    {
			    return GetColumnValue<int>("icon_type");
		    }
            set 
		    {
			    SetColumnValue("icon_type", value);
            }
        }
	      
        [XmlAttribute("Image")]
        [Bindable(true)]
        public string Image 
	    {
		    get
		    {
			    return GetColumnValue<string>("image");
		    }
            set 
		    {
			    SetColumnValue("image", value);
            }
        }
	      
        [XmlAttribute("IsFinal")]
        [Bindable(true)]
        public bool IsFinal 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_final");
		    }
            set 
		    {
			    SetColumnValue("is_final", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ParentId = @"parent_id";
            
            public static string ParentName = @"parent_name";
            
            public static string ParentType = @"parent_type";
            
            public static string ParentCode = @"parent_code";
            
            public static string Seq = @"seq";
            
            public static string CategoryId = @"category_id";
            
            public static string CategoryName = @"category_name";
            
            public static string CategoryType = @"category_type";
            
            public static string CategoryCode = @"category_code";
            
            public static string CategoryStatus = @"category_status";
            
            public static string IsShowFrontEnd = @"is_show_front_end";
            
            public static string IsShowBackEnd = @"is_show_back_end";
            
            public static string IconType = @"icon_type";
            
            public static string Image = @"image";
            
            public static string IsFinal = @"is_final";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BookingSystemApi class.
	/// </summary>
    [Serializable]
	public partial class BookingSystemApiCollection : RepositoryList<BookingSystemApi, BookingSystemApiCollection>
	{	   
		public BookingSystemApiCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BookingSystemApiCollection</returns>
		public BookingSystemApiCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BookingSystemApi o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the booking_system_api table.
	/// </summary>
	[Serializable]
	public partial class BookingSystemApi : RepositoryRecord<BookingSystemApi>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BookingSystemApi()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BookingSystemApi(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("booking_system_api", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarApiId = new TableSchema.TableColumn(schema);
				colvarApiId.ColumnName = "api_id";
				colvarApiId.DataType = DbType.Int32;
				colvarApiId.MaxLength = 0;
				colvarApiId.AutoIncrement = true;
				colvarApiId.IsNullable = false;
				colvarApiId.IsPrimaryKey = true;
				colvarApiId.IsForeignKey = false;
				colvarApiId.IsReadOnly = false;
				colvarApiId.DefaultSetting = @"";
				colvarApiId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApiId);
				
				TableSchema.TableColumn colvarApiKey = new TableSchema.TableColumn(schema);
				colvarApiKey.ColumnName = "api_key";
				colvarApiKey.DataType = DbType.AnsiString;
				colvarApiKey.MaxLength = 50;
				colvarApiKey.AutoIncrement = false;
				colvarApiKey.IsNullable = false;
				colvarApiKey.IsPrimaryKey = false;
				colvarApiKey.IsForeignKey = false;
				colvarApiKey.IsReadOnly = false;
				colvarApiKey.DefaultSetting = @"";
				colvarApiKey.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApiKey);
				
				TableSchema.TableColumn colvarServiceName = new TableSchema.TableColumn(schema);
				colvarServiceName.ColumnName = "service_name";
				colvarServiceName.DataType = DbType.AnsiString;
				colvarServiceName.MaxLength = 50;
				colvarServiceName.AutoIncrement = false;
				colvarServiceName.IsNullable = false;
				colvarServiceName.IsPrimaryKey = false;
				colvarServiceName.IsForeignKey = false;
				colvarServiceName.IsReadOnly = false;
				colvarServiceName.DefaultSetting = @"";
				colvarServiceName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceName);
				
				TableSchema.TableColumn colvarServiceType = new TableSchema.TableColumn(schema);
				colvarServiceType.ColumnName = "service_type";
				colvarServiceType.DataType = DbType.Int32;
				colvarServiceType.MaxLength = 0;
				colvarServiceType.AutoIncrement = false;
				colvarServiceType.IsNullable = false;
				colvarServiceType.IsPrimaryKey = false;
				colvarServiceType.IsForeignKey = false;
				colvarServiceType.IsReadOnly = false;
				
						colvarServiceType.DefaultSetting = @"((0))";
				colvarServiceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("booking_system_api",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("ApiId")]
		[Bindable(true)]
		public int ApiId 
		{
			get { return GetColumnValue<int>(Columns.ApiId); }
			set { SetColumnValue(Columns.ApiId, value); }
		}
		  
		[XmlAttribute("ApiKey")]
		[Bindable(true)]
		public string ApiKey 
		{
			get { return GetColumnValue<string>(Columns.ApiKey); }
			set { SetColumnValue(Columns.ApiKey, value); }
		}
		  
		[XmlAttribute("ServiceName")]
		[Bindable(true)]
		public string ServiceName 
		{
			get { return GetColumnValue<string>(Columns.ServiceName); }
			set { SetColumnValue(Columns.ServiceName, value); }
		}
		  
		[XmlAttribute("ServiceType")]
		[Bindable(true)]
		public int ServiceType 
		{
			get { return GetColumnValue<int>(Columns.ServiceType); }
			set { SetColumnValue(Columns.ServiceType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn ApiIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ApiKeyColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ServiceNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ServiceTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string ApiId = @"api_id";
			 public static string ApiKey = @"api_key";
			 public static string ServiceName = @"service_name";
			 public static string ServiceType = @"service_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHamiDealProperty class.
    /// </summary>
    [Serializable]
    public partial class ViewHamiDealPropertyCollection : ReadOnlyList<ViewHamiDealProperty, ViewHamiDealPropertyCollection>
    {        
        public ViewHamiDealPropertyCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hami_deal_property view.
    /// </summary>
    [Serializable]
    public partial class ViewHamiDealProperty : ReadOnlyRecord<ViewHamiDealProperty>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hami_deal_property", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = true;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType);
                
                TableSchema.TableColumn colvarSellerVerifyType = new TableSchema.TableColumn(schema);
                colvarSellerVerifyType.ColumnName = "seller_verify_type";
                colvarSellerVerifyType.DataType = DbType.Int32;
                colvarSellerVerifyType.MaxLength = 0;
                colvarSellerVerifyType.AutoIncrement = false;
                colvarSellerVerifyType.IsNullable = true;
                colvarSellerVerifyType.IsPrimaryKey = false;
                colvarSellerVerifyType.IsForeignKey = false;
                colvarSellerVerifyType.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerVerifyType);
                
                TableSchema.TableColumn colvarDealAccBusinessGroupId = new TableSchema.TableColumn(schema);
                colvarDealAccBusinessGroupId.ColumnName = "deal_acc_business_group_id";
                colvarDealAccBusinessGroupId.DataType = DbType.Int32;
                colvarDealAccBusinessGroupId.MaxLength = 0;
                colvarDealAccBusinessGroupId.AutoIncrement = false;
                colvarDealAccBusinessGroupId.IsNullable = true;
                colvarDealAccBusinessGroupId.IsPrimaryKey = false;
                colvarDealAccBusinessGroupId.IsForeignKey = false;
                colvarDealAccBusinessGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealAccBusinessGroupId);
                
                TableSchema.TableColumn colvarDealEmpName = new TableSchema.TableColumn(schema);
                colvarDealEmpName.ColumnName = "deal_emp_name";
                colvarDealEmpName.DataType = DbType.String;
                colvarDealEmpName.MaxLength = 50;
                colvarDealEmpName.AutoIncrement = false;
                colvarDealEmpName.IsNullable = true;
                colvarDealEmpName.IsPrimaryKey = false;
                colvarDealEmpName.IsForeignKey = false;
                colvarDealEmpName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEmpName);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 255;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 255;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarCityList = new TableSchema.TableColumn(schema);
                colvarCityList.ColumnName = "city_list";
                colvarCityList.DataType = DbType.AnsiString;
                colvarCityList.MaxLength = 255;
                colvarCityList.AutoIncrement = false;
                colvarCityList.IsNullable = true;
                colvarCityList.IsPrimaryKey = false;
                colvarCityList.IsForeignKey = false;
                colvarCityList.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityList);
                
                TableSchema.TableColumn colvarHamiCategory = new TableSchema.TableColumn(schema);
                colvarHamiCategory.ColumnName = "hami_category";
                colvarHamiCategory.DataType = DbType.AnsiString;
                colvarHamiCategory.MaxLength = 4;
                colvarHamiCategory.AutoIncrement = false;
                colvarHamiCategory.IsNullable = true;
                colvarHamiCategory.IsPrimaryKey = false;
                colvarHamiCategory.IsForeignKey = false;
                colvarHamiCategory.IsReadOnly = false;
                
                schema.Columns.Add(colvarHamiCategory);
                
                TableSchema.TableColumn colvarHamiCategoryDesc = new TableSchema.TableColumn(schema);
                colvarHamiCategoryDesc.ColumnName = "hami_category_desc";
                colvarHamiCategoryDesc.DataType = DbType.String;
                colvarHamiCategoryDesc.MaxLength = 100;
                colvarHamiCategoryDesc.AutoIncrement = false;
                colvarHamiCategoryDesc.IsNullable = true;
                colvarHamiCategoryDesc.IsPrimaryKey = false;
                colvarHamiCategoryDesc.IsForeignKey = false;
                colvarHamiCategoryDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarHamiCategoryDesc);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hami_deal_property",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHamiDealProperty()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHamiDealProperty(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHamiDealProperty(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHamiDealProperty(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int? DealType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_type");
		    }
            set 
		    {
			    SetColumnValue("deal_type", value);
            }
        }
	      
        [XmlAttribute("SellerVerifyType")]
        [Bindable(true)]
        public int? SellerVerifyType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seller_verify_type");
		    }
            set 
		    {
			    SetColumnValue("seller_verify_type", value);
            }
        }
	      
        [XmlAttribute("DealAccBusinessGroupId")]
        [Bindable(true)]
        public int? DealAccBusinessGroupId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_acc_business_group_id");
		    }
            set 
		    {
			    SetColumnValue("deal_acc_business_group_id", value);
            }
        }
	      
        [XmlAttribute("DealEmpName")]
        [Bindable(true)]
        public string DealEmpName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_emp_name");
		    }
            set 
		    {
			    SetColumnValue("deal_emp_name", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("CityList")]
        [Bindable(true)]
        public string CityList 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_list");
		    }
            set 
		    {
			    SetColumnValue("city_list", value);
            }
        }
	      
        [XmlAttribute("HamiCategory")]
        [Bindable(true)]
        public string HamiCategory 
	    {
		    get
		    {
			    return GetColumnValue<string>("hami_category");
		    }
            set 
		    {
			    SetColumnValue("hami_category", value);
            }
        }
	      
        [XmlAttribute("HamiCategoryDesc")]
        [Bindable(true)]
        public string HamiCategoryDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("hami_category_desc");
		    }
            set 
		    {
			    SetColumnValue("hami_category_desc", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string DealType = @"deal_type";
            
            public static string SellerVerifyType = @"seller_verify_type";
            
            public static string DealAccBusinessGroupId = @"deal_acc_business_group_id";
            
            public static string DealEmpName = @"deal_emp_name";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string ModifyTime = @"modify_time";
            
            public static string UniqueId = @"unique_id";
            
            public static string CityList = @"city_list";
            
            public static string HamiCategory = @"hami_category";
            
            public static string HamiCategoryDesc = @"hami_category_desc";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

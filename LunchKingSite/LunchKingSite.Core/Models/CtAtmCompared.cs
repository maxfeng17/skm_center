﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the CtAtmCompared class.
    /// </summary>
    [Serializable]
    public partial class CtAtmComparedCollection : RepositoryList<CtAtmCompared, CtAtmComparedCollection>
    {
        public CtAtmComparedCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CtAtmComparedCollection</returns>
        public CtAtmComparedCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CtAtmCompared o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the ct_atm_compared table.
    /// </summary>
    [Serializable]
    public partial class CtAtmCompared : RepositoryRecord<CtAtmCompared>, IRecordBase
    {
        #region .ctors and Default Settings

        public CtAtmCompared()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public CtAtmCompared(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("ct_atm_compared", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarVirtualAccount = new TableSchema.TableColumn(schema);
                colvarVirtualAccount.ColumnName = "virtual_account";
                colvarVirtualAccount.DataType = DbType.AnsiString;
                colvarVirtualAccount.MaxLength = 16;
                colvarVirtualAccount.AutoIncrement = false;
                colvarVirtualAccount.IsNullable = false;
                colvarVirtualAccount.IsPrimaryKey = false;
                colvarVirtualAccount.IsForeignKey = false;
                colvarVirtualAccount.IsReadOnly = false;
                colvarVirtualAccount.DefaultSetting = @"";
                colvarVirtualAccount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVirtualAccount);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Int32;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                colvarAmount.DefaultSetting = @"";
                colvarAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarPayTime = new TableSchema.TableColumn(schema);
                colvarPayTime.ColumnName = "pay_time";
                colvarPayTime.DataType = DbType.DateTime;
                colvarPayTime.MaxLength = 0;
                colvarPayTime.AutoIncrement = false;
                colvarPayTime.IsNullable = true;
                colvarPayTime.IsPrimaryKey = false;
                colvarPayTime.IsForeignKey = false;
                colvarPayTime.IsReadOnly = false;
                colvarPayTime.DefaultSetting = @"";
                colvarPayTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPayTime);

                TableSchema.TableColumn colvarAtmTime = new TableSchema.TableColumn(schema);
                colvarAtmTime.ColumnName = "atm_time";
                colvarAtmTime.DataType = DbType.AnsiString;
                colvarAtmTime.MaxLength = 50;
                colvarAtmTime.AutoIncrement = false;
                colvarAtmTime.IsNullable = false;
                colvarAtmTime.IsPrimaryKey = false;
                colvarAtmTime.IsForeignKey = false;
                colvarAtmTime.IsReadOnly = false;
                colvarAtmTime.DefaultSetting = @"";
                colvarAtmTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAtmTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("ct_atm_compared", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("VirtualAccount")]
        [Bindable(true)]
        public string VirtualAccount
        {
            get { return GetColumnValue<string>(Columns.VirtualAccount); }
            set { SetColumnValue(Columns.VirtualAccount, value); }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public int Amount
        {
            get { return GetColumnValue<int>(Columns.Amount); }
            set { SetColumnValue(Columns.Amount, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("PayTime")]
        [Bindable(true)]
        public DateTime? PayTime
        {
            get { return GetColumnValue<DateTime?>(Columns.PayTime); }
            set { SetColumnValue(Columns.PayTime, value); }
        }

        [XmlAttribute("AtmTime")]
        [Bindable(true)]
        public string AtmTime
        {
            get { return GetColumnValue<string>(Columns.AtmTime); }
            set { SetColumnValue(Columns.AtmTime, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn VirtualAccountColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn PayTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn AtmTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string VirtualAccount = @"virtual_account";
            public static string Amount = @"amount";
            public static string Status = @"status";
            public static string CreateTime = @"create_time";
            public static string PayTime = @"pay_time";
            public static string AtmTime = @"atm_time";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewYahooVoucherSeller class.
    /// </summary>
    [Serializable]
    public partial class ViewYahooVoucherSellerCollection : ReadOnlyList<ViewYahooVoucherSeller, ViewYahooVoucherSellerCollection>
    {        
        public ViewYahooVoucherSellerCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_yahoo_voucher_seller view.
    /// </summary>
    [Serializable]
    public partial class ViewYahooVoucherSeller : ReadOnlyRecord<ViewYahooVoucherSeller>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_yahoo_voucher_seller", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
                colvarEventId.ColumnName = "event_id";
                colvarEventId.DataType = DbType.Int32;
                colvarEventId.MaxLength = 0;
                colvarEventId.AutoIncrement = false;
                colvarEventId.IsNullable = false;
                colvarEventId.IsPrimaryKey = false;
                colvarEventId.IsForeignKey = false;
                colvarEventId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventId);
                
                TableSchema.TableColumn colvarContents = new TableSchema.TableColumn(schema);
                colvarContents.ColumnName = "contents";
                colvarContents.DataType = DbType.String;
                colvarContents.MaxLength = 1000;
                colvarContents.AutoIncrement = false;
                colvarContents.IsNullable = true;
                colvarContents.IsPrimaryKey = false;
                colvarContents.IsForeignKey = false;
                colvarContents.IsReadOnly = false;
                
                schema.Columns.Add(colvarContents);
                
                TableSchema.TableColumn colvarInstruction = new TableSchema.TableColumn(schema);
                colvarInstruction.ColumnName = "instruction";
                colvarInstruction.DataType = DbType.String;
                colvarInstruction.MaxLength = 1000;
                colvarInstruction.AutoIncrement = false;
                colvarInstruction.IsNullable = true;
                colvarInstruction.IsPrimaryKey = false;
                colvarInstruction.IsForeignKey = false;
                colvarInstruction.IsReadOnly = false;
                
                schema.Columns.Add(colvarInstruction);
                
                TableSchema.TableColumn colvarPicUrl = new TableSchema.TableColumn(schema);
                colvarPicUrl.ColumnName = "pic_url";
                colvarPicUrl.DataType = DbType.AnsiString;
                colvarPicUrl.MaxLength = 500;
                colvarPicUrl.AutoIncrement = false;
                colvarPicUrl.IsNullable = true;
                colvarPicUrl.IsPrimaryKey = false;
                colvarPicUrl.IsForeignKey = false;
                colvarPicUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarPicUrl);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarMode = new TableSchema.TableColumn(schema);
                colvarMode.ColumnName = "mode";
                colvarMode.DataType = DbType.Int32;
                colvarMode.MaxLength = 0;
                colvarMode.AutoIncrement = false;
                colvarMode.IsNullable = false;
                colvarMode.IsPrimaryKey = false;
                colvarMode.IsForeignKey = false;
                colvarMode.IsReadOnly = false;
                
                schema.Columns.Add(colvarMode);
                
                TableSchema.TableColumn colvarOthers = new TableSchema.TableColumn(schema);
                colvarOthers.ColumnName = "others";
                colvarOthers.DataType = DbType.String;
                colvarOthers.MaxLength = 500;
                colvarOthers.AutoIncrement = false;
                colvarOthers.IsNullable = true;
                colvarOthers.IsPrimaryKey = false;
                colvarOthers.IsForeignKey = false;
                colvarOthers.IsReadOnly = false;
                
                schema.Columns.Add(colvarOthers);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarEnable = new TableSchema.TableColumn(schema);
                colvarEnable.ColumnName = "enable";
                colvarEnable.DataType = DbType.Boolean;
                colvarEnable.MaxLength = 0;
                colvarEnable.AutoIncrement = false;
                colvarEnable.IsNullable = false;
                colvarEnable.IsPrimaryKey = false;
                colvarEnable.IsForeignKey = false;
                colvarEnable.IsReadOnly = false;
                
                schema.Columns.Add(colvarEnable);
                
                TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
                colvarStartDate.ColumnName = "start_date";
                colvarStartDate.DataType = DbType.DateTime;
                colvarStartDate.MaxLength = 0;
                colvarStartDate.AutoIncrement = false;
                colvarStartDate.IsNullable = true;
                colvarStartDate.IsPrimaryKey = false;
                colvarStartDate.IsForeignKey = false;
                colvarStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartDate);
                
                TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
                colvarEndDate.ColumnName = "end_date";
                colvarEndDate.DataType = DbType.DateTime;
                colvarEndDate.MaxLength = 0;
                colvarEndDate.AutoIncrement = false;
                colvarEndDate.IsNullable = true;
                colvarEndDate.IsPrimaryKey = false;
                colvarEndDate.IsForeignKey = false;
                colvarEndDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndDate);
                
                TableSchema.TableColumn colvarPageCount = new TableSchema.TableColumn(schema);
                colvarPageCount.ColumnName = "page_count";
                colvarPageCount.DataType = DbType.Int32;
                colvarPageCount.MaxLength = 0;
                colvarPageCount.AutoIncrement = false;
                colvarPageCount.IsNullable = false;
                colvarPageCount.IsPrimaryKey = false;
                colvarPageCount.IsForeignKey = false;
                colvarPageCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarPageCount);
                
                TableSchema.TableColumn colvarRatio = new TableSchema.TableColumn(schema);
                colvarRatio.ColumnName = "ratio";
                colvarRatio.DataType = DbType.Int32;
                colvarRatio.MaxLength = 0;
                colvarRatio.AutoIncrement = false;
                colvarRatio.IsNullable = false;
                colvarRatio.IsPrimaryKey = false;
                colvarRatio.IsForeignKey = false;
                colvarRatio.IsReadOnly = false;
                
                schema.Columns.Add(colvarRatio);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerDescription = new TableSchema.TableColumn(schema);
                colvarSellerDescription.ColumnName = "seller_description";
                colvarSellerDescription.DataType = DbType.String;
                colvarSellerDescription.MaxLength = 1073741823;
                colvarSellerDescription.AutoIncrement = false;
                colvarSellerDescription.IsNullable = true;
                colvarSellerDescription.IsPrimaryKey = false;
                colvarSellerDescription.IsForeignKey = false;
                colvarSellerDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerDescription);
                
                TableSchema.TableColumn colvarIsCloseDown = new TableSchema.TableColumn(schema);
                colvarIsCloseDown.ColumnName = "is_close_down";
                colvarIsCloseDown.DataType = DbType.Boolean;
                colvarIsCloseDown.MaxLength = 0;
                colvarIsCloseDown.AutoIncrement = false;
                colvarIsCloseDown.IsNullable = false;
                colvarIsCloseDown.IsPrimaryKey = false;
                colvarIsCloseDown.IsForeignKey = false;
                colvarIsCloseDown.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCloseDown);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_yahoo_voucher_seller",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewYahooVoucherSeller()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewYahooVoucherSeller(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewYahooVoucherSeller(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewYahooVoucherSeller(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("EventId")]
        [Bindable(true)]
        public int EventId 
	    {
		    get
		    {
			    return GetColumnValue<int>("event_id");
		    }
            set 
		    {
			    SetColumnValue("event_id", value);
            }
        }
	      
        [XmlAttribute("Contents")]
        [Bindable(true)]
        public string Contents 
	    {
		    get
		    {
			    return GetColumnValue<string>("contents");
		    }
            set 
		    {
			    SetColumnValue("contents", value);
            }
        }
	      
        [XmlAttribute("Instruction")]
        [Bindable(true)]
        public string Instruction 
	    {
		    get
		    {
			    return GetColumnValue<string>("instruction");
		    }
            set 
		    {
			    SetColumnValue("instruction", value);
            }
        }
	      
        [XmlAttribute("PicUrl")]
        [Bindable(true)]
        public string PicUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("pic_url");
		    }
            set 
		    {
			    SetColumnValue("pic_url", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("Mode")]
        [Bindable(true)]
        public int Mode 
	    {
		    get
		    {
			    return GetColumnValue<int>("mode");
		    }
            set 
		    {
			    SetColumnValue("mode", value);
            }
        }
	      
        [XmlAttribute("Others")]
        [Bindable(true)]
        public string Others 
	    {
		    get
		    {
			    return GetColumnValue<string>("others");
		    }
            set 
		    {
			    SetColumnValue("others", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("Enable")]
        [Bindable(true)]
        public bool Enable 
	    {
		    get
		    {
			    return GetColumnValue<bool>("enable");
		    }
            set 
		    {
			    SetColumnValue("enable", value);
            }
        }
	      
        [XmlAttribute("StartDate")]
        [Bindable(true)]
        public DateTime? StartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_date");
		    }
            set 
		    {
			    SetColumnValue("start_date", value);
            }
        }
	      
        [XmlAttribute("EndDate")]
        [Bindable(true)]
        public DateTime? EndDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_date");
		    }
            set 
		    {
			    SetColumnValue("end_date", value);
            }
        }
	      
        [XmlAttribute("PageCount")]
        [Bindable(true)]
        public int PageCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("page_count");
		    }
            set 
		    {
			    SetColumnValue("page_count", value);
            }
        }
	      
        [XmlAttribute("Ratio")]
        [Bindable(true)]
        public int Ratio 
	    {
		    get
		    {
			    return GetColumnValue<int>("ratio");
		    }
            set 
		    {
			    SetColumnValue("ratio", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerDescription")]
        [Bindable(true)]
        public string SellerDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_description");
		    }
            set 
		    {
			    SetColumnValue("seller_description", value);
            }
        }
	      
        [XmlAttribute("IsCloseDown")]
        [Bindable(true)]
        public bool IsCloseDown 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_close_down");
		    }
            set 
		    {
			    SetColumnValue("is_close_down", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string EventId = @"event_id";
            
            public static string Contents = @"contents";
            
            public static string Instruction = @"instruction";
            
            public static string PicUrl = @"pic_url";
            
            public static string Type = @"type";
            
            public static string Mode = @"mode";
            
            public static string Others = @"others";
            
            public static string Status = @"status";
            
            public static string Enable = @"enable";
            
            public static string StartDate = @"start_date";
            
            public static string EndDate = @"end_date";
            
            public static string PageCount = @"page_count";
            
            public static string Ratio = @"ratio";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerDescription = @"seller_description";
            
            public static string IsCloseDown = @"is_close_down";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

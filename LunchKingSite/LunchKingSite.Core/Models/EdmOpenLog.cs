using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the EdmOpenLog class.
    /// </summary>
    [Serializable]
    public partial class EdmOpenLogCollection : RepositoryList<EdmOpenLog, EdmOpenLogCollection>
    {
        public EdmOpenLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EdmOpenLogCollection</returns>
        public EdmOpenLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EdmOpenLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the edm_open_log table.
    /// </summary>
    [Serializable]
    public partial class EdmOpenLog : RepositoryRecord<EdmOpenLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public EdmOpenLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public EdmOpenLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("edm_open_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
                colvarEmail.ColumnName = "email";
                colvarEmail.DataType = DbType.String;
                colvarEmail.MaxLength = 200;
                colvarEmail.AutoIncrement = false;
                colvarEmail.IsNullable = false;
                colvarEmail.IsPrimaryKey = false;
                colvarEmail.IsForeignKey = false;
                colvarEmail.IsReadOnly = false;
                colvarEmail.DefaultSetting = @"";
                colvarEmail.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEmail);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;

                colvarType.DefaultSetting = @"((0))";
                colvarType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = true;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarEdmId = new TableSchema.TableColumn(schema);
                colvarEdmId.ColumnName = "edm_id";
                colvarEdmId.DataType = DbType.Int32;
                colvarEdmId.MaxLength = 0;
                colvarEdmId.AutoIncrement = false;
                colvarEdmId.IsNullable = true;
                colvarEdmId.IsPrimaryKey = false;
                colvarEdmId.IsForeignKey = false;
                colvarEdmId.IsReadOnly = false;
                colvarEdmId.DefaultSetting = @"";
                colvarEdmId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEdmId);

                TableSchema.TableColumn colvarCreateDate = new TableSchema.TableColumn(schema);
                colvarCreateDate.ColumnName = "create_date";
                colvarCreateDate.DataType = DbType.DateTime;
                colvarCreateDate.MaxLength = 0;
                colvarCreateDate.AutoIncrement = false;
                colvarCreateDate.IsNullable = true;
                colvarCreateDate.IsPrimaryKey = false;
                colvarCreateDate.IsForeignKey = false;
                colvarCreateDate.IsReadOnly = false;
                colvarCreateDate.DefaultSetting = @"";
                colvarCreateDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateDate);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarLoginDate = new TableSchema.TableColumn(schema);
                colvarLoginDate.ColumnName = "login_date";
                colvarLoginDate.DataType = DbType.DateTime;
                colvarLoginDate.MaxLength = 0;
                colvarLoginDate.AutoIncrement = false;
                colvarLoginDate.IsNullable = true;
                colvarLoginDate.IsPrimaryKey = false;
                colvarLoginDate.IsForeignKey = false;
                colvarLoginDate.IsReadOnly = false;
                colvarLoginDate.DefaultSetting = @"";
                colvarLoginDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLoginDate);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("edm_open_log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Email")]
        [Bindable(true)]
        public string Email
        {
            get { return GetColumnValue<string>(Columns.Email); }
            set { SetColumnValue(Columns.Email, value); }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get { return GetColumnValue<int>(Columns.Type); }
            set { SetColumnValue(Columns.Type, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int? UserId
        {
            get { return GetColumnValue<int?>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("EdmId")]
        [Bindable(true)]
        public int? EdmId
        {
            get { return GetColumnValue<int?>(Columns.EdmId); }
            set { SetColumnValue(Columns.EdmId, value); }
        }

        [XmlAttribute("CreateDate")]
        [Bindable(true)]
        public DateTime? CreateDate
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateDate); }
            set { SetColumnValue(Columns.CreateDate, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("LoginDate")]
        [Bindable(true)]
        public DateTime? LoginDate
        {
            get { return GetColumnValue<DateTime?>(Columns.LoginDate); }
            set { SetColumnValue(Columns.LoginDate, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn EmailColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn EdmIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateDateColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn LoginDateColumn
        {
            get { return Schema.Columns[7]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Email = @"email";
            public static string Type = @"type";
            public static string UserId = @"user_id";
            public static string EdmId = @"edm_id";
            public static string CreateDate = @"create_date";
            public static string ModifyTime = @"modify_time";
            public static string LoginDate = @"login_date";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

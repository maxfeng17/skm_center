using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ThirdPartyPayApiLog class.
	/// </summary>
    [Serializable]
	public partial class ThirdPartyPayApiLogCollection : RepositoryList<ThirdPartyPayApiLog, ThirdPartyPayApiLogCollection>
	{	   
		public ThirdPartyPayApiLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ThirdPartyPayApiLogCollection</returns>
		public ThirdPartyPayApiLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ThirdPartyPayApiLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the third_party_pay_api_log table.
	/// </summary>
	[Serializable]
	public partial class ThirdPartyPayApiLog : RepositoryRecord<ThirdPartyPayApiLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ThirdPartyPayApiLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ThirdPartyPayApiLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("third_party_pay_api_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarPaymentOrg = new TableSchema.TableColumn(schema);
				colvarPaymentOrg.ColumnName = "payment_org";
				colvarPaymentOrg.DataType = DbType.Int32;
				colvarPaymentOrg.MaxLength = 0;
				colvarPaymentOrg.AutoIncrement = false;
				colvarPaymentOrg.IsNullable = false;
				colvarPaymentOrg.IsPrimaryKey = false;
				colvarPaymentOrg.IsForeignKey = false;
				colvarPaymentOrg.IsReadOnly = false;
				colvarPaymentOrg.DefaultSetting = @"";
				colvarPaymentOrg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentOrg);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = true;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarApiUrl = new TableSchema.TableColumn(schema);
				colvarApiUrl.ColumnName = "api_url";
				colvarApiUrl.DataType = DbType.AnsiString;
				colvarApiUrl.MaxLength = 255;
				colvarApiUrl.AutoIncrement = false;
				colvarApiUrl.IsNullable = true;
				colvarApiUrl.IsPrimaryKey = false;
				colvarApiUrl.IsForeignKey = false;
				colvarApiUrl.IsReadOnly = false;
				colvarApiUrl.DefaultSetting = @"";
				colvarApiUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApiUrl);
				
				TableSchema.TableColumn colvarReturnCode = new TableSchema.TableColumn(schema);
				colvarReturnCode.ColumnName = "return_code";
				colvarReturnCode.DataType = DbType.Int32;
				colvarReturnCode.MaxLength = 0;
				colvarReturnCode.AutoIncrement = false;
				colvarReturnCode.IsNullable = true;
				colvarReturnCode.IsPrimaryKey = false;
				colvarReturnCode.IsForeignKey = false;
				colvarReturnCode.IsReadOnly = false;
				colvarReturnCode.DefaultSetting = @"";
				colvarReturnCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnCode);
				
				TableSchema.TableColumn colvarReturnMsg = new TableSchema.TableColumn(schema);
				colvarReturnMsg.ColumnName = "return_msg";
				colvarReturnMsg.DataType = DbType.String;
				colvarReturnMsg.MaxLength = 100;
				colvarReturnMsg.AutoIncrement = false;
				colvarReturnMsg.IsNullable = true;
				colvarReturnMsg.IsPrimaryKey = false;
				colvarReturnMsg.IsForeignKey = false;
				colvarReturnMsg.IsReadOnly = false;
				colvarReturnMsg.DefaultSetting = @"";
				colvarReturnMsg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnMsg);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 255;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = true;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarSourceIp = new TableSchema.TableColumn(schema);
				colvarSourceIp.ColumnName = "source_ip";
				colvarSourceIp.DataType = DbType.AnsiString;
				colvarSourceIp.MaxLength = 50;
				colvarSourceIp.AutoIncrement = false;
				colvarSourceIp.IsNullable = true;
				colvarSourceIp.IsPrimaryKey = false;
				colvarSourceIp.IsForeignKey = false;
				colvarSourceIp.IsReadOnly = false;
				colvarSourceIp.DefaultSetting = @"";
				colvarSourceIp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSourceIp);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("third_party_pay_api_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("PaymentOrg")]
		[Bindable(true)]
		public int PaymentOrg 
		{
			get { return GetColumnValue<int>(Columns.PaymentOrg); }
			set { SetColumnValue(Columns.PaymentOrg, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int? UserId 
		{
			get { return GetColumnValue<int?>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("ApiUrl")]
		[Bindable(true)]
		public string ApiUrl 
		{
			get { return GetColumnValue<string>(Columns.ApiUrl); }
			set { SetColumnValue(Columns.ApiUrl, value); }
		}
		  
		[XmlAttribute("ReturnCode")]
		[Bindable(true)]
		public int? ReturnCode 
		{
			get { return GetColumnValue<int?>(Columns.ReturnCode); }
			set { SetColumnValue(Columns.ReturnCode, value); }
		}
		  
		[XmlAttribute("ReturnMsg")]
		[Bindable(true)]
		public string ReturnMsg 
		{
			get { return GetColumnValue<string>(Columns.ReturnMsg); }
			set { SetColumnValue(Columns.ReturnMsg, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("SourceIp")]
		[Bindable(true)]
		public string SourceIp 
		{
			get { return GetColumnValue<string>(Columns.SourceIp); }
			set { SetColumnValue(Columns.SourceIp, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PaymentOrgColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ApiUrlColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnCodeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnMsgColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SourceIpColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string PaymentOrg = @"payment_org";
			 public static string UserId = @"user_id";
			 public static string ApiUrl = @"api_url";
			 public static string ReturnCode = @"return_code";
			 public static string ReturnMsg = @"return_msg";
			 public static string Memo = @"memo";
			 public static string SourceIp = @"source_ip";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealTimeslot class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealTimeslotCollection : ReadOnlyList<ViewHiDealTimeslot, ViewHiDealTimeslotCollection>
    {        
        public ViewHiDealTimeslotCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_timeslot view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealTimeslot : ReadOnlyRecord<ViewHiDealTimeslot>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_timeslot", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarHiDealGuid = new TableSchema.TableColumn(schema);
                colvarHiDealGuid.ColumnName = "hi_deal_guid";
                colvarHiDealGuid.DataType = DbType.Guid;
                colvarHiDealGuid.MaxLength = 0;
                colvarHiDealGuid.AutoIncrement = false;
                colvarHiDealGuid.IsNullable = false;
                colvarHiDealGuid.IsPrimaryKey = false;
                colvarHiDealGuid.IsForeignKey = false;
                colvarHiDealGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealGuid);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = true;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 50;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarPromoLongDesc = new TableSchema.TableColumn(schema);
                colvarPromoLongDesc.ColumnName = "promo_long_desc";
                colvarPromoLongDesc.DataType = DbType.String;
                colvarPromoLongDesc.MaxLength = 255;
                colvarPromoLongDesc.AutoIncrement = false;
                colvarPromoLongDesc.IsNullable = true;
                colvarPromoLongDesc.IsPrimaryKey = false;
                colvarPromoLongDesc.IsForeignKey = false;
                colvarPromoLongDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoLongDesc);
                
                TableSchema.TableColumn colvarPromoShortDesc = new TableSchema.TableColumn(schema);
                colvarPromoShortDesc.ColumnName = "promo_short_desc";
                colvarPromoShortDesc.DataType = DbType.String;
                colvarPromoShortDesc.MaxLength = 100;
                colvarPromoShortDesc.AutoIncrement = false;
                colvarPromoShortDesc.IsNullable = true;
                colvarPromoShortDesc.IsPrimaryKey = false;
                colvarPromoShortDesc.IsForeignKey = false;
                colvarPromoShortDesc.IsReadOnly = false;
                
                schema.Columns.Add(colvarPromoShortDesc);
                
                TableSchema.TableColumn colvarPicture = new TableSchema.TableColumn(schema);
                colvarPicture.ColumnName = "picture";
                colvarPicture.DataType = DbType.String;
                colvarPicture.MaxLength = -1;
                colvarPicture.AutoIncrement = false;
                colvarPicture.IsNullable = true;
                colvarPicture.IsPrimaryKey = false;
                colvarPicture.IsForeignKey = false;
                colvarPicture.IsReadOnly = false;
                
                schema.Columns.Add(colvarPicture);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarIsOpen = new TableSchema.TableColumn(schema);
                colvarIsOpen.ColumnName = "IsOpen";
                colvarIsOpen.DataType = DbType.Boolean;
                colvarIsOpen.MaxLength = 0;
                colvarIsOpen.AutoIncrement = false;
                colvarIsOpen.IsNullable = false;
                colvarIsOpen.IsPrimaryKey = false;
                colvarIsOpen.IsForeignKey = false;
                colvarIsOpen.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsOpen);
                
                TableSchema.TableColumn colvarIsShow = new TableSchema.TableColumn(schema);
                colvarIsShow.ColumnName = "IsShow";
                colvarIsShow.DataType = DbType.Boolean;
                colvarIsShow.MaxLength = 0;
                colvarIsShow.AutoIncrement = false;
                colvarIsShow.IsNullable = false;
                colvarIsShow.IsPrimaryKey = false;
                colvarIsShow.IsForeignKey = false;
                colvarIsShow.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsShow);
                
                TableSchema.TableColumn colvarIsSoldout = new TableSchema.TableColumn(schema);
                colvarIsSoldout.ColumnName = "is_soldout";
                colvarIsSoldout.DataType = DbType.Boolean;
                colvarIsSoldout.MaxLength = 0;
                colvarIsSoldout.AutoIncrement = false;
                colvarIsSoldout.IsNullable = true;
                colvarIsSoldout.IsPrimaryKey = false;
                colvarIsSoldout.IsForeignKey = false;
                colvarIsSoldout.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsSoldout);
                
                TableSchema.TableColumn colvarIsAlwaysMain = new TableSchema.TableColumn(schema);
                colvarIsAlwaysMain.ColumnName = "is_always_main";
                colvarIsAlwaysMain.DataType = DbType.Boolean;
                colvarIsAlwaysMain.MaxLength = 0;
                colvarIsAlwaysMain.AutoIncrement = false;
                colvarIsAlwaysMain.IsNullable = false;
                colvarIsAlwaysMain.IsPrimaryKey = false;
                colvarIsAlwaysMain.IsForeignKey = false;
                colvarIsAlwaysMain.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsAlwaysMain);
                
                TableSchema.TableColumn colvarCities = new TableSchema.TableColumn(schema);
                colvarCities.ColumnName = "cities";
                colvarCities.DataType = DbType.String;
                colvarCities.MaxLength = 100;
                colvarCities.AutoIncrement = false;
                colvarCities.IsNullable = true;
                colvarCities.IsPrimaryKey = false;
                colvarCities.IsForeignKey = false;
                colvarCities.IsReadOnly = false;
                
                schema.Columns.Add(colvarCities);
                
                TableSchema.TableColumn colvarIsVisa = new TableSchema.TableColumn(schema);
                colvarIsVisa.ColumnName = "is_visa";
                colvarIsVisa.DataType = DbType.Boolean;
                colvarIsVisa.MaxLength = 0;
                colvarIsVisa.AutoIncrement = false;
                colvarIsVisa.IsNullable = true;
                colvarIsVisa.IsPrimaryKey = false;
                colvarIsVisa.IsForeignKey = false;
                colvarIsVisa.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsVisa);
                
                TableSchema.TableColumn colvarEdmTitle = new TableSchema.TableColumn(schema);
                colvarEdmTitle.ColumnName = "edm_title";
                colvarEdmTitle.DataType = DbType.String;
                colvarEdmTitle.MaxLength = 40;
                colvarEdmTitle.AutoIncrement = false;
                colvarEdmTitle.IsNullable = true;
                colvarEdmTitle.IsPrimaryKey = false;
                colvarEdmTitle.IsForeignKey = false;
                colvarEdmTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarEdmTitle);
                
                TableSchema.TableColumn colvarEdmSubject = new TableSchema.TableColumn(schema);
                colvarEdmSubject.ColumnName = "edm_subject";
                colvarEdmSubject.DataType = DbType.String;
                colvarEdmSubject.MaxLength = 40;
                colvarEdmSubject.AutoIncrement = false;
                colvarEdmSubject.IsNullable = true;
                colvarEdmSubject.IsPrimaryKey = false;
                colvarEdmSubject.IsForeignKey = false;
                colvarEdmSubject.IsReadOnly = false;
                
                schema.Columns.Add(colvarEdmSubject);
                
                TableSchema.TableColumn colvarPrimaryBigPicture = new TableSchema.TableColumn(schema);
                colvarPrimaryBigPicture.ColumnName = "primary_big_picture";
                colvarPrimaryBigPicture.DataType = DbType.String;
                colvarPrimaryBigPicture.MaxLength = 200;
                colvarPrimaryBigPicture.AutoIncrement = false;
                colvarPrimaryBigPicture.IsNullable = true;
                colvarPrimaryBigPicture.IsPrimaryKey = false;
                colvarPrimaryBigPicture.IsForeignKey = false;
                colvarPrimaryBigPicture.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrimaryBigPicture);
                
                TableSchema.TableColumn colvarPrimarySmallPicture = new TableSchema.TableColumn(schema);
                colvarPrimarySmallPicture.ColumnName = "primary_small_picture";
                colvarPrimarySmallPicture.DataType = DbType.String;
                colvarPrimarySmallPicture.MaxLength = 200;
                colvarPrimarySmallPicture.AutoIncrement = false;
                colvarPrimarySmallPicture.IsNullable = true;
                colvarPrimarySmallPicture.IsPrimaryKey = false;
                colvarPrimarySmallPicture.IsForeignKey = false;
                colvarPrimarySmallPicture.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrimarySmallPicture);
                
                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = true;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarSeq);
                
                TableSchema.TableColumn colvarIsMain = new TableSchema.TableColumn(schema);
                colvarIsMain.ColumnName = "is_main";
                colvarIsMain.DataType = DbType.Boolean;
                colvarIsMain.MaxLength = 0;
                colvarIsMain.AutoIncrement = false;
                colvarIsMain.IsNullable = false;
                colvarIsMain.IsPrimaryKey = false;
                colvarIsMain.IsForeignKey = false;
                colvarIsMain.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsMain);
                
                TableSchema.TableColumn colvarCityCodeId = new TableSchema.TableColumn(schema);
                colvarCityCodeId.ColumnName = "city_code_id";
                colvarCityCodeId.DataType = DbType.Int32;
                colvarCityCodeId.MaxLength = 0;
                colvarCityCodeId.AutoIncrement = false;
                colvarCityCodeId.IsNullable = false;
                colvarCityCodeId.IsPrimaryKey = false;
                colvarCityCodeId.IsForeignKey = false;
                colvarCityCodeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityCodeId);
                
                TableSchema.TableColumn colvarCity = new TableSchema.TableColumn(schema);
                colvarCity.ColumnName = "city";
                colvarCity.DataType = DbType.String;
                colvarCity.MaxLength = 50;
                colvarCity.AutoIncrement = false;
                colvarCity.IsNullable = false;
                colvarCity.IsPrimaryKey = false;
                colvarCity.IsForeignKey = false;
                colvarCity.IsReadOnly = false;
                
                schema.Columns.Add(colvarCity);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = false;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_timeslot",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealTimeslot()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealTimeslot(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealTimeslot(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealTimeslot(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("HiDealGuid")]
        [Bindable(true)]
        public Guid HiDealGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("hi_deal_guid");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_guid", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int? DealType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_type");
		    }
            set 
		    {
			    SetColumnValue("deal_type", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("PromoLongDesc")]
        [Bindable(true)]
        public string PromoLongDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("promo_long_desc");
		    }
            set 
		    {
			    SetColumnValue("promo_long_desc", value);
            }
        }
	      
        [XmlAttribute("PromoShortDesc")]
        [Bindable(true)]
        public string PromoShortDesc 
	    {
		    get
		    {
			    return GetColumnValue<string>("promo_short_desc");
		    }
            set 
		    {
			    SetColumnValue("promo_short_desc", value);
            }
        }
	      
        [XmlAttribute("Picture")]
        [Bindable(true)]
        public string Picture 
	    {
		    get
		    {
			    return GetColumnValue<string>("picture");
		    }
            set 
		    {
			    SetColumnValue("picture", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_start_time", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("IsOpen")]
        [Bindable(true)]
        public bool IsOpen 
	    {
		    get
		    {
			    return GetColumnValue<bool>("IsOpen");
		    }
            set 
		    {
			    SetColumnValue("IsOpen", value);
            }
        }
	      
        [XmlAttribute("IsShow")]
        [Bindable(true)]
        public bool IsShow 
	    {
		    get
		    {
			    return GetColumnValue<bool>("IsShow");
		    }
            set 
		    {
			    SetColumnValue("IsShow", value);
            }
        }
	      
        [XmlAttribute("IsSoldout")]
        [Bindable(true)]
        public bool? IsSoldout 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_soldout");
		    }
            set 
		    {
			    SetColumnValue("is_soldout", value);
            }
        }
	      
        [XmlAttribute("IsAlwaysMain")]
        [Bindable(true)]
        public bool IsAlwaysMain 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_always_main");
		    }
            set 
		    {
			    SetColumnValue("is_always_main", value);
            }
        }
	      
        [XmlAttribute("Cities")]
        [Bindable(true)]
        public string Cities 
	    {
		    get
		    {
			    return GetColumnValue<string>("cities");
		    }
            set 
		    {
			    SetColumnValue("cities", value);
            }
        }
	      
        [XmlAttribute("IsVisa")]
        [Bindable(true)]
        public bool? IsVisa 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_visa");
		    }
            set 
		    {
			    SetColumnValue("is_visa", value);
            }
        }
	      
        [XmlAttribute("EdmTitle")]
        [Bindable(true)]
        public string EdmTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("edm_title");
		    }
            set 
		    {
			    SetColumnValue("edm_title", value);
            }
        }
	      
        [XmlAttribute("EdmSubject")]
        [Bindable(true)]
        public string EdmSubject 
	    {
		    get
		    {
			    return GetColumnValue<string>("edm_subject");
		    }
            set 
		    {
			    SetColumnValue("edm_subject", value);
            }
        }
	      
        [XmlAttribute("PrimaryBigPicture")]
        [Bindable(true)]
        public string PrimaryBigPicture 
	    {
		    get
		    {
			    return GetColumnValue<string>("primary_big_picture");
		    }
            set 
		    {
			    SetColumnValue("primary_big_picture", value);
            }
        }
	      
        [XmlAttribute("PrimarySmallPicture")]
        [Bindable(true)]
        public string PrimarySmallPicture 
	    {
		    get
		    {
			    return GetColumnValue<string>("primary_small_picture");
		    }
            set 
		    {
			    SetColumnValue("primary_small_picture", value);
            }
        }
	      
        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int? Seq 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seq");
		    }
            set 
		    {
			    SetColumnValue("seq", value);
            }
        }
	      
        [XmlAttribute("IsMain")]
        [Bindable(true)]
        public bool IsMain 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_main");
		    }
            set 
		    {
			    SetColumnValue("is_main", value);
            }
        }
	      
        [XmlAttribute("CityCodeId")]
        [Bindable(true)]
        public int CityCodeId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_code_id");
		    }
            set 
		    {
			    SetColumnValue("city_code_id", value);
            }
        }
	      
        [XmlAttribute("City")]
        [Bindable(true)]
        public string City 
	    {
		    get
		    {
			    return GetColumnValue<string>("city");
		    }
            set 
		    {
			    SetColumnValue("city", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string HiDealGuid = @"hi_deal_guid";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string DealType = @"deal_type";
            
            public static string Name = @"name";
            
            public static string PromoLongDesc = @"promo_long_desc";
            
            public static string PromoShortDesc = @"promo_short_desc";
            
            public static string Picture = @"picture";
            
            public static string DealStartTime = @"deal_start_time";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string IsOpen = @"IsOpen";
            
            public static string IsShow = @"IsShow";
            
            public static string IsSoldout = @"is_soldout";
            
            public static string IsAlwaysMain = @"is_always_main";
            
            public static string Cities = @"cities";
            
            public static string IsVisa = @"is_visa";
            
            public static string EdmTitle = @"edm_title";
            
            public static string EdmSubject = @"edm_subject";
            
            public static string PrimaryBigPicture = @"primary_big_picture";
            
            public static string PrimarySmallPicture = @"primary_small_picture";
            
            public static string Seq = @"seq";
            
            public static string IsMain = @"is_main";
            
            public static string CityCodeId = @"city_code_id";
            
            public static string City = @"city";
            
            public static string StartTime = @"start_time";
            
            public static string EndTime = @"end_time";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

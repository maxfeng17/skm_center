﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the SkmBeaconMessageTimeSlot class.
    /// </summary>
    [Serializable]
    public partial class SkmBeaconMessageTimeSlotCollection : RepositoryList<SkmBeaconMessageTimeSlot, SkmBeaconMessageTimeSlotCollection>
    {
        public SkmBeaconMessageTimeSlotCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmBeaconMessageTimeSlotCollection</returns>
        public SkmBeaconMessageTimeSlotCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmBeaconMessageTimeSlot o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the skm_beacon_message_time_slot table.
    /// </summary>
    [Serializable]
    public partial class SkmBeaconMessageTimeSlot : RepositoryRecord<SkmBeaconMessageTimeSlot>, IRecordBase
    {
        #region .ctors and Default Settings

        public SkmBeaconMessageTimeSlot()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SkmBeaconMessageTimeSlot(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("skm_beacon_message_time_slot", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = true;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                colvarSequence.DefaultSetting = @"";
                colvarSequence.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSequence);

                TableSchema.TableColumn colvarEffectiveStart = new TableSchema.TableColumn(schema);
                colvarEffectiveStart.ColumnName = "effective_start";
                colvarEffectiveStart.DataType = DbType.DateTime;
                colvarEffectiveStart.MaxLength = 0;
                colvarEffectiveStart.AutoIncrement = false;
                colvarEffectiveStart.IsNullable = true;
                colvarEffectiveStart.IsPrimaryKey = false;
                colvarEffectiveStart.IsForeignKey = false;
                colvarEffectiveStart.IsReadOnly = false;
                colvarEffectiveStart.DefaultSetting = @"";
                colvarEffectiveStart.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEffectiveStart);

                TableSchema.TableColumn colvarEffectiveEnd = new TableSchema.TableColumn(schema);
                colvarEffectiveEnd.ColumnName = "effective_end";
                colvarEffectiveEnd.DataType = DbType.DateTime;
                colvarEffectiveEnd.MaxLength = 0;
                colvarEffectiveEnd.AutoIncrement = false;
                colvarEffectiveEnd.IsNullable = true;
                colvarEffectiveEnd.IsPrimaryKey = false;
                colvarEffectiveEnd.IsForeignKey = false;
                colvarEffectiveEnd.IsReadOnly = false;
                colvarEffectiveEnd.DefaultSetting = @"";
                colvarEffectiveEnd.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEffectiveEnd);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarBeaconType = new TableSchema.TableColumn(schema);
                colvarBeaconType.ColumnName = "beacon_type";
                colvarBeaconType.DataType = DbType.Int32;
                colvarBeaconType.MaxLength = 0;
                colvarBeaconType.AutoIncrement = false;
                colvarBeaconType.IsNullable = true;
                colvarBeaconType.IsPrimaryKey = false;
                colvarBeaconType.IsForeignKey = false;
                colvarBeaconType.IsReadOnly = false;
                colvarBeaconType.DefaultSetting = @"";
                colvarBeaconType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBeaconType);

                TableSchema.TableColumn colvarShopCode = new TableSchema.TableColumn(schema);
                colvarShopCode.ColumnName = "shop_code";
                colvarShopCode.DataType = DbType.AnsiString;
                colvarShopCode.MaxLength = 20;
                colvarShopCode.AutoIncrement = false;
                colvarShopCode.IsNullable = true;
                colvarShopCode.IsPrimaryKey = false;
                colvarShopCode.IsForeignKey = false;
                colvarShopCode.IsReadOnly = false;
                colvarShopCode.DefaultSetting = @"";
                colvarShopCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShopCode);

                TableSchema.TableColumn colvarFloor = new TableSchema.TableColumn(schema);
                colvarFloor.ColumnName = "floor";
                colvarFloor.DataType = DbType.AnsiString;
                colvarFloor.MaxLength = 10;
                colvarFloor.AutoIncrement = false;
                colvarFloor.IsNullable = true;
                colvarFloor.IsPrimaryKey = false;
                colvarFloor.IsForeignKey = false;
                colvarFloor.IsReadOnly = false;
                colvarFloor.DefaultSetting = @"";
                colvarFloor.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFloor);

                TableSchema.TableColumn colvarBeaconId = new TableSchema.TableColumn(schema);
                colvarBeaconId.ColumnName = "beacon_id";
                colvarBeaconId.DataType = DbType.AnsiString;
                colvarBeaconId.MaxLength = 15;
                colvarBeaconId.AutoIncrement = false;
                colvarBeaconId.IsNullable = true;
                colvarBeaconId.IsPrimaryKey = false;
                colvarBeaconId.IsForeignKey = false;
                colvarBeaconId.IsReadOnly = false;
                colvarBeaconId.DefaultSetting = @"";
                colvarBeaconId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBeaconId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("skm_beacon_message_time_slot", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid
        {
            get { return GetColumnValue<Guid?>(Columns.BusinessHourGuid); }
            set { SetColumnValue(Columns.BusinessHourGuid, value); }
        }

        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public int? Sequence
        {
            get { return GetColumnValue<int?>(Columns.Sequence); }
            set { SetColumnValue(Columns.Sequence, value); }
        }

        [XmlAttribute("EffectiveStart")]
        [Bindable(true)]
        public DateTime? EffectiveStart
        {
            get { return GetColumnValue<DateTime?>(Columns.EffectiveStart); }
            set { SetColumnValue(Columns.EffectiveStart, value); }
        }

        [XmlAttribute("EffectiveEnd")]
        [Bindable(true)]
        public DateTime? EffectiveEnd
        {
            get { return GetColumnValue<DateTime?>(Columns.EffectiveEnd); }
            set { SetColumnValue(Columns.EffectiveEnd, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int? Status
        {
            get { return GetColumnValue<int?>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("BeaconType")]
        [Bindable(true)]
        public int? BeaconType
        {
            get { return GetColumnValue<int?>(Columns.BeaconType); }
            set { SetColumnValue(Columns.BeaconType, value); }
        }

        [XmlAttribute("ShopCode")]
        [Bindable(true)]
        public string ShopCode
        {
            get { return GetColumnValue<string>(Columns.ShopCode); }
            set { SetColumnValue(Columns.ShopCode, value); }
        }

        [XmlAttribute("Floor")]
        [Bindable(true)]
        public string Floor
        {
            get { return GetColumnValue<string>(Columns.Floor); }
            set { SetColumnValue(Columns.Floor, value); }
        }

        [XmlAttribute("BeaconId")]
        [Bindable(true)]
        public string BeaconId
        {
            get { return GetColumnValue<string>(Columns.BeaconId); }
            set { SetColumnValue(Columns.BeaconId, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn EffectiveStartColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn EffectiveEndColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn BeaconTypeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ShopCodeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn FloorColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn BeaconIdColumn
        {
            get { return Schema.Columns[9]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string BusinessHourGuid = @"business_hour_guid";
            public static string Sequence = @"sequence";
            public static string EffectiveStart = @"effective_start";
            public static string EffectiveEnd = @"effective_end";
            public static string Status = @"status";
            public static string BeaconType = @"beacon_type";
            public static string ShopCode = @"shop_code";
            public static string Floor = @"floor";
            public static string BeaconId = @"beacon_id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

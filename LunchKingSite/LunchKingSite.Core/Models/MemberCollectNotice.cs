using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MemberCollectNotice class.
	/// </summary>
    [Serializable]
	public partial class MemberCollectNoticeCollection : RepositoryList<MemberCollectNotice, MemberCollectNoticeCollection>
	{	   
		public MemberCollectNoticeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberCollectNoticeCollection</returns>
		public MemberCollectNoticeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberCollectNotice o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the member_collect_notice table.
	/// </summary>
	[Serializable]
	public partial class MemberCollectNotice : RepositoryRecord<MemberCollectNotice>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MemberCollectNotice()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MemberCollectNotice(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_collect_notice", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = true;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarDeviceId = new TableSchema.TableColumn(schema);
				colvarDeviceId.ColumnName = "device_id";
				colvarDeviceId.DataType = DbType.Int32;
				colvarDeviceId.MaxLength = 0;
				colvarDeviceId.AutoIncrement = false;
				colvarDeviceId.IsNullable = false;
				colvarDeviceId.IsPrimaryKey = true;
				colvarDeviceId.IsForeignKey = false;
				colvarDeviceId.IsReadOnly = false;
				colvarDeviceId.DefaultSetting = @"";
				colvarDeviceId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeviceId);
				
				TableSchema.TableColumn colvarTokenId = new TableSchema.TableColumn(schema);
				colvarTokenId.ColumnName = "token_id";
				colvarTokenId.DataType = DbType.Int32;
				colvarTokenId.MaxLength = 0;
				colvarTokenId.AutoIncrement = false;
				colvarTokenId.IsNullable = true;
				colvarTokenId.IsPrimaryKey = false;
				colvarTokenId.IsForeignKey = false;
				colvarTokenId.IsReadOnly = false;
				colvarTokenId.DefaultSetting = @"";
				colvarTokenId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTokenId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_collect_notice",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("DeviceId")]
		[Bindable(true)]
		public int DeviceId 
		{
			get { return GetColumnValue<int>(Columns.DeviceId); }
			set { SetColumnValue(Columns.DeviceId, value); }
		}
		  
		[XmlAttribute("TokenId")]
		[Bindable(true)]
		public int? TokenId 
		{
			get { return GetColumnValue<int?>(Columns.TokenId); }
			set { SetColumnValue(Columns.TokenId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DeviceIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TokenIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string UserId = @"user_id";
			 public static string DeviceId = @"device_id";
			 public static string TokenId = @"token_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

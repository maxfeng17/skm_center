using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the PcpImage class.
    /// </summary>
    [Serializable]
    public partial class PcpImageCollection : RepositoryList<PcpImage, PcpImageCollection>
    {
        public PcpImageCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpImageCollection</returns>
        public PcpImageCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpImage o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }


    /// <summary>
    /// This is an ActiveRecord class which wraps the pcp_image table.
    /// </summary>

    [Serializable]
    public partial class PcpImage : RepositoryRecord<PcpImage>, IRecordBase
    {
        #region .ctors and Default Settings

        public PcpImage()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public PcpImage(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("pcp_image", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = false;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;
                colvarSellerUserId.DefaultSetting = @"";
                colvarSellerUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerUserId);

                TableSchema.TableColumn colvarImageType = new TableSchema.TableColumn(schema);
                colvarImageType.ColumnName = "image_type";
                colvarImageType.DataType = DbType.Byte;
                colvarImageType.MaxLength = 0;
                colvarImageType.AutoIncrement = false;
                colvarImageType.IsNullable = false;
                colvarImageType.IsPrimaryKey = false;
                colvarImageType.IsForeignKey = false;
                colvarImageType.IsReadOnly = false;

                colvarImageType.DefaultSetting = @"((0))";
                colvarImageType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarImageType);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                colvarCreateTime.DefaultSetting = @"(getdate())";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarImageUrl = new TableSchema.TableColumn(schema);
                colvarImageUrl.ColumnName = "image_url";
                colvarImageUrl.DataType = DbType.AnsiString;
                colvarImageUrl.MaxLength = 100;
                colvarImageUrl.AutoIncrement = false;
                colvarImageUrl.IsNullable = false;
                colvarImageUrl.IsPrimaryKey = false;
                colvarImageUrl.IsForeignKey = false;
                colvarImageUrl.IsReadOnly = false;
                colvarImageUrl.DefaultSetting = @"";
                colvarImageUrl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarImageUrl);

                TableSchema.TableColumn colvarImageUrlCompressed = new TableSchema.TableColumn(schema);
                colvarImageUrlCompressed.ColumnName = "image_url_compressed";
                colvarImageUrlCompressed.DataType = DbType.AnsiString;
                colvarImageUrlCompressed.MaxLength = 100;
                colvarImageUrlCompressed.AutoIncrement = false;
                colvarImageUrlCompressed.IsNullable = true;
                colvarImageUrlCompressed.IsPrimaryKey = false;
                colvarImageUrlCompressed.IsForeignKey = false;
                colvarImageUrlCompressed.IsReadOnly = false;
                colvarImageUrlCompressed.DefaultSetting = @"";
                colvarImageUrlCompressed.ForeignKeyTableName = "";
                schema.Columns.Add(colvarImageUrlCompressed);

                TableSchema.TableColumn colvarIsDelete = new TableSchema.TableColumn(schema);
                colvarIsDelete.ColumnName = "is_delete";
                colvarIsDelete.DataType = DbType.Boolean;
                colvarIsDelete.MaxLength = 0;
                colvarIsDelete.AutoIncrement = false;
                colvarIsDelete.IsNullable = false;
                colvarIsDelete.IsPrimaryKey = false;
                colvarIsDelete.IsForeignKey = false;
                colvarIsDelete.IsReadOnly = false;

                colvarIsDelete.DefaultSetting = @"((0))";
                colvarIsDelete.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsDelete);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarModifyUserId = new TableSchema.TableColumn(schema);
                colvarModifyUserId.ColumnName = "modify_user_id";
                colvarModifyUserId.DataType = DbType.Int32;
                colvarModifyUserId.MaxLength = 0;
                colvarModifyUserId.AutoIncrement = false;
                colvarModifyUserId.IsNullable = true;
                colvarModifyUserId.IsPrimaryKey = false;
                colvarModifyUserId.IsForeignKey = false;
                colvarModifyUserId.IsReadOnly = false;
                colvarModifyUserId.DefaultSetting = @"";
                colvarModifyUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyUserId);

                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = false;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;

                colvarSequence.DefaultSetting = @"((0))";
                colvarSequence.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSequence);

                TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
                colvarGroupId.ColumnName = "group_id";
                colvarGroupId.DataType = DbType.Int32;
                colvarGroupId.MaxLength = 0;
                colvarGroupId.AutoIncrement = false;
                colvarGroupId.IsNullable = false;
                colvarGroupId.IsPrimaryKey = false;
                colvarGroupId.IsForeignKey = false;
                colvarGroupId.IsReadOnly = false;

                colvarGroupId.DefaultSetting = @"((0))";
                colvarGroupId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGroupId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("pcp_image", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int SellerUserId
        {
            get { return GetColumnValue<int>(Columns.SellerUserId); }
            set { SetColumnValue(Columns.SellerUserId, value); }
        }

        [XmlAttribute("ImageType")]
        [Bindable(true)]
        public byte ImageType
        {
            get { return GetColumnValue<byte>(Columns.ImageType); }
            set { SetColumnValue(Columns.ImageType, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ImageUrl")]
        [Bindable(true)]
        public string ImageUrl
        {
            get { return GetColumnValue<string>(Columns.ImageUrl); }
            set { SetColumnValue(Columns.ImageUrl, value); }
        }

        [XmlAttribute("ImageUrlCompressed")]
        [Bindable(true)]
        public string ImageUrlCompressed
        {
            get { return GetColumnValue<string>(Columns.ImageUrlCompressed); }
            set { SetColumnValue(Columns.ImageUrlCompressed, value); }
        }

        [XmlAttribute("IsDelete")]
        [Bindable(true)]
        public bool IsDelete
        {
            get { return GetColumnValue<bool>(Columns.IsDelete); }
            set { SetColumnValue(Columns.IsDelete, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ModifyUserId")]
        [Bindable(true)]
        public int? ModifyUserId
        {
            get { return GetColumnValue<int?>(Columns.ModifyUserId); }
            set { SetColumnValue(Columns.ModifyUserId, value); }
        }

        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public int Sequence
        {
            get { return GetColumnValue<int>(Columns.Sequence); }
            set { SetColumnValue(Columns.Sequence, value); }
        }

        [XmlAttribute("GroupId")]
        [Bindable(true)]
        public int GroupId
        {
            get { return GetColumnValue<int>(Columns.GroupId); }
            set { SetColumnValue(Columns.GroupId, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn SellerUserIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ImageTypeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ImageUrlColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ImageUrlCompressedColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn IsDeleteColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ModifyUserIdColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn GroupIdColumn
        {
            get { return Schema.Columns[10]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string SellerUserId = @"seller_user_id";
            public static string ImageType = @"image_type";
            public static string CreateTime = @"create_time";
            public static string ImageUrl = @"image_url";
            public static string ImageUrlCompressed = @"image_url_compressed";
            public static string IsDelete = @"is_delete";
            public static string ModifyTime = @"modify_time";
            public static string ModifyUserId = @"modify_user_id";
            public static string Sequence = @"sequence";
            public static string GroupId = @"group_id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponCloseDownExpirationChange class.
    /// </summary>
    [Serializable]
    public partial class ViewPponCloseDownExpirationChangeCollection : ReadOnlyList<ViewPponCloseDownExpirationChange, ViewPponCloseDownExpirationChangeCollection>
    {        
        public ViewPponCloseDownExpirationChangeCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_close_down_expiration_change view.
    /// </summary>
    [Serializable]
    public partial class ViewPponCloseDownExpirationChange : ReadOnlyRecord<ViewPponCloseDownExpirationChange>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_close_down_expiration_change", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBid);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = false;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarBhDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBhDeliverTimeE.ColumnName = "bh_deliver_time_e";
                colvarBhDeliverTimeE.DataType = DbType.DateTime;
                colvarBhDeliverTimeE.MaxLength = 0;
                colvarBhDeliverTimeE.AutoIncrement = false;
                colvarBhDeliverTimeE.IsNullable = true;
                colvarBhDeliverTimeE.IsPrimaryKey = false;
                colvarBhDeliverTimeE.IsForeignKey = false;
                colvarBhDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBhDeliverTimeE);
                
                TableSchema.TableColumn colvarBhChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarBhChangedExpireDate.ColumnName = "bh_changed_expire_date";
                colvarBhChangedExpireDate.DataType = DbType.DateTime;
                colvarBhChangedExpireDate.MaxLength = 0;
                colvarBhChangedExpireDate.AutoIncrement = false;
                colvarBhChangedExpireDate.IsNullable = true;
                colvarBhChangedExpireDate.IsPrimaryKey = false;
                colvarBhChangedExpireDate.IsForeignKey = false;
                colvarBhChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarBhChangedExpireDate);
                
                TableSchema.TableColumn colvarStoreChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarStoreChangedExpireDate.ColumnName = "store_changed_expire_date";
                colvarStoreChangedExpireDate.DataType = DbType.DateTime;
                colvarStoreChangedExpireDate.MaxLength = 0;
                colvarStoreChangedExpireDate.AutoIncrement = false;
                colvarStoreChangedExpireDate.IsNullable = true;
                colvarStoreChangedExpireDate.IsPrimaryKey = false;
                colvarStoreChangedExpireDate.IsForeignKey = false;
                colvarStoreChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreChangedExpireDate);
                
                TableSchema.TableColumn colvarSellerCloseDownDate = new TableSchema.TableColumn(schema);
                colvarSellerCloseDownDate.ColumnName = "seller_close_down_date";
                colvarSellerCloseDownDate.DataType = DbType.DateTime;
                colvarSellerCloseDownDate.MaxLength = 0;
                colvarSellerCloseDownDate.AutoIncrement = false;
                colvarSellerCloseDownDate.IsNullable = true;
                colvarSellerCloseDownDate.IsPrimaryKey = false;
                colvarSellerCloseDownDate.IsForeignKey = false;
                colvarSellerCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCloseDownDate);
                
                TableSchema.TableColumn colvarStoreCloseDownDate = new TableSchema.TableColumn(schema);
                colvarStoreCloseDownDate.ColumnName = "store_close_down_date";
                colvarStoreCloseDownDate.DataType = DbType.DateTime;
                colvarStoreCloseDownDate.MaxLength = 0;
                colvarStoreCloseDownDate.AutoIncrement = false;
                colvarStoreCloseDownDate.IsNullable = true;
                colvarStoreCloseDownDate.IsPrimaryKey = false;
                colvarStoreCloseDownDate.IsForeignKey = false;
                colvarStoreCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreCloseDownDate);
                
                TableSchema.TableColumn colvarBhDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarBhDeliverTimeS.ColumnName = "bh_deliver_time_s";
                colvarBhDeliverTimeS.DataType = DbType.DateTime;
                colvarBhDeliverTimeS.MaxLength = 0;
                colvarBhDeliverTimeS.AutoIncrement = false;
                colvarBhDeliverTimeS.IsNullable = true;
                colvarBhDeliverTimeS.IsPrimaryKey = false;
                colvarBhDeliverTimeS.IsForeignKey = false;
                colvarBhDeliverTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBhDeliverTimeS);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_close_down_expiration_change",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponCloseDownExpirationChange()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponCloseDownExpirationChange(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponCloseDownExpirationChange(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponCloseDownExpirationChange(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("bid");
		    }
            set 
		    {
			    SetColumnValue("bid", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("BhDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BhDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bh_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("bh_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("BhChangedExpireDate")]
        [Bindable(true)]
        public DateTime? BhChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bh_changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("bh_changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("StoreChangedExpireDate")]
        [Bindable(true)]
        public DateTime? StoreChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("store_changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("store_changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("SellerCloseDownDate")]
        [Bindable(true)]
        public DateTime? SellerCloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("seller_close_down_date");
		    }
            set 
		    {
			    SetColumnValue("seller_close_down_date", value);
            }
        }
	      
        [XmlAttribute("StoreCloseDownDate")]
        [Bindable(true)]
        public DateTime? StoreCloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("store_close_down_date");
		    }
            set 
		    {
			    SetColumnValue("store_close_down_date", value);
            }
        }
	      
        [XmlAttribute("BhDeliverTimeS")]
        [Bindable(true)]
        public DateTime? BhDeliverTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bh_deliver_time_s");
		    }
            set 
		    {
			    SetColumnValue("bh_deliver_time_s", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderId = @"order_id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string Bid = @"bid";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string StoreGuid = @"store_guid";
            
            public static string BhDeliverTimeE = @"bh_deliver_time_e";
            
            public static string BhChangedExpireDate = @"bh_changed_expire_date";
            
            public static string StoreChangedExpireDate = @"store_changed_expire_date";
            
            public static string SellerCloseDownDate = @"seller_close_down_date";
            
            public static string StoreCloseDownDate = @"store_close_down_date";
            
            public static string BhDeliverTimeS = @"bh_deliver_time_s";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

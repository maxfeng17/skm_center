using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVourcherSellerCollect class.
    /// </summary>
    [Serializable]
    public partial class ViewVourcherSellerCollectCollection : ReadOnlyList<ViewVourcherSellerCollect, ViewVourcherSellerCollectCollection>
    {        
        public ViewVourcherSellerCollectCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vourcher_seller_collect view.
    /// </summary>
    [Serializable]
    public partial class ViewVourcherSellerCollect : ReadOnlyRecord<ViewVourcherSellerCollect>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vourcher_seller_collect", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
                colvarEventName.ColumnName = "event_name";
                colvarEventName.DataType = DbType.String;
                colvarEventName.MaxLength = 100;
                colvarEventName.AutoIncrement = false;
                colvarEventName.IsNullable = false;
                colvarEventName.IsPrimaryKey = false;
                colvarEventName.IsForeignKey = false;
                colvarEventName.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventName);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarContents = new TableSchema.TableColumn(schema);
                colvarContents.ColumnName = "contents";
                colvarContents.DataType = DbType.String;
                colvarContents.MaxLength = 1000;
                colvarContents.AutoIncrement = false;
                colvarContents.IsNullable = true;
                colvarContents.IsPrimaryKey = false;
                colvarContents.IsForeignKey = false;
                colvarContents.IsReadOnly = false;
                
                schema.Columns.Add(colvarContents);
                
                TableSchema.TableColumn colvarInstruction = new TableSchema.TableColumn(schema);
                colvarInstruction.ColumnName = "instruction";
                colvarInstruction.DataType = DbType.String;
                colvarInstruction.MaxLength = 1000;
                colvarInstruction.AutoIncrement = false;
                colvarInstruction.IsNullable = true;
                colvarInstruction.IsPrimaryKey = false;
                colvarInstruction.IsForeignKey = false;
                colvarInstruction.IsReadOnly = false;
                
                schema.Columns.Add(colvarInstruction);
                
                TableSchema.TableColumn colvarPicUrl = new TableSchema.TableColumn(schema);
                colvarPicUrl.ColumnName = "pic_url";
                colvarPicUrl.DataType = DbType.AnsiString;
                colvarPicUrl.MaxLength = 500;
                colvarPicUrl.AutoIncrement = false;
                colvarPicUrl.IsNullable = true;
                colvarPicUrl.IsPrimaryKey = false;
                colvarPicUrl.IsForeignKey = false;
                colvarPicUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarPicUrl);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
                colvarStartDate.ColumnName = "start_date";
                colvarStartDate.DataType = DbType.DateTime;
                colvarStartDate.MaxLength = 0;
                colvarStartDate.AutoIncrement = false;
                colvarStartDate.IsNullable = true;
                colvarStartDate.IsPrimaryKey = false;
                colvarStartDate.IsForeignKey = false;
                colvarStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartDate);
                
                TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
                colvarEndDate.ColumnName = "end_date";
                colvarEndDate.DataType = DbType.DateTime;
                colvarEndDate.MaxLength = 0;
                colvarEndDate.AutoIncrement = false;
                colvarEndDate.IsNullable = true;
                colvarEndDate.IsPrimaryKey = false;
                colvarEndDate.IsForeignKey = false;
                colvarEndDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndDate);
                
                TableSchema.TableColumn colvarPageCount = new TableSchema.TableColumn(schema);
                colvarPageCount.ColumnName = "page_count";
                colvarPageCount.DataType = DbType.Int32;
                colvarPageCount.MaxLength = 0;
                colvarPageCount.AutoIncrement = false;
                colvarPageCount.IsNullable = false;
                colvarPageCount.IsPrimaryKey = false;
                colvarPageCount.IsForeignKey = false;
                colvarPageCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarPageCount);
                
                TableSchema.TableColumn colvarMaxQuantity = new TableSchema.TableColumn(schema);
                colvarMaxQuantity.ColumnName = "max_quantity";
                colvarMaxQuantity.DataType = DbType.Int32;
                colvarMaxQuantity.MaxLength = 0;
                colvarMaxQuantity.AutoIncrement = false;
                colvarMaxQuantity.IsNullable = false;
                colvarMaxQuantity.IsPrimaryKey = false;
                colvarMaxQuantity.IsForeignKey = false;
                colvarMaxQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarMaxQuantity);
                
                TableSchema.TableColumn colvarCurrentQuantity = new TableSchema.TableColumn(schema);
                colvarCurrentQuantity.ColumnName = "current_quantity";
                colvarCurrentQuantity.DataType = DbType.Int32;
                colvarCurrentQuantity.MaxLength = 0;
                colvarCurrentQuantity.AutoIncrement = false;
                colvarCurrentQuantity.IsNullable = false;
                colvarCurrentQuantity.IsPrimaryKey = false;
                colvarCurrentQuantity.IsForeignKey = false;
                colvarCurrentQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarCurrentQuantity);
                
                TableSchema.TableColumn colvarRatio = new TableSchema.TableColumn(schema);
                colvarRatio.ColumnName = "ratio";
                colvarRatio.DataType = DbType.Int32;
                colvarRatio.MaxLength = 0;
                colvarRatio.AutoIncrement = false;
                colvarRatio.IsNullable = false;
                colvarRatio.IsPrimaryKey = false;
                colvarRatio.IsForeignKey = false;
                colvarRatio.IsReadOnly = false;
                
                schema.Columns.Add(colvarRatio);
                
                TableSchema.TableColumn colvarEnable = new TableSchema.TableColumn(schema);
                colvarEnable.ColumnName = "enable";
                colvarEnable.DataType = DbType.Boolean;
                colvarEnable.MaxLength = 0;
                colvarEnable.AutoIncrement = false;
                colvarEnable.IsNullable = false;
                colvarEnable.IsPrimaryKey = false;
                colvarEnable.IsForeignKey = false;
                colvarEnable.IsReadOnly = false;
                
                schema.Columns.Add(colvarEnable);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 50;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 1073741823;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage);
                
                TableSchema.TableColumn colvarMode = new TableSchema.TableColumn(schema);
                colvarMode.ColumnName = "mode";
                colvarMode.DataType = DbType.Int32;
                colvarMode.MaxLength = 0;
                colvarMode.AutoIncrement = false;
                colvarMode.IsNullable = false;
                colvarMode.IsPrimaryKey = false;
                colvarMode.IsForeignKey = false;
                colvarMode.IsReadOnly = false;
                
                schema.Columns.Add(colvarMode);
                
                TableSchema.TableColumn colvarMagazine = new TableSchema.TableColumn(schema);
                colvarMagazine.ColumnName = "magazine";
                colvarMagazine.DataType = DbType.Boolean;
                colvarMagazine.MaxLength = 0;
                colvarMagazine.AutoIncrement = false;
                colvarMagazine.IsNullable = true;
                colvarMagazine.IsPrimaryKey = false;
                colvarMagazine.IsForeignKey = false;
                colvarMagazine.IsReadOnly = false;
                
                schema.Columns.Add(colvarMagazine);
                
                TableSchema.TableColumn colvarDiscount = new TableSchema.TableColumn(schema);
                colvarDiscount.ColumnName = "discount";
                colvarDiscount.DataType = DbType.Decimal;
                colvarDiscount.MaxLength = 0;
                colvarDiscount.AutoIncrement = false;
                colvarDiscount.IsNullable = true;
                colvarDiscount.IsPrimaryKey = false;
                colvarDiscount.IsForeignKey = false;
                colvarDiscount.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscount);
                
                TableSchema.TableColumn colvarOriginalPrice = new TableSchema.TableColumn(schema);
                colvarOriginalPrice.ColumnName = "original_price";
                colvarOriginalPrice.DataType = DbType.Int32;
                colvarOriginalPrice.MaxLength = 0;
                colvarOriginalPrice.AutoIncrement = false;
                colvarOriginalPrice.IsNullable = true;
                colvarOriginalPrice.IsPrimaryKey = false;
                colvarOriginalPrice.IsForeignKey = false;
                colvarOriginalPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarOriginalPrice);
                
                TableSchema.TableColumn colvarDiscountPrice = new TableSchema.TableColumn(schema);
                colvarDiscountPrice.ColumnName = "discount_price";
                colvarDiscountPrice.DataType = DbType.Int32;
                colvarDiscountPrice.MaxLength = 0;
                colvarDiscountPrice.AutoIncrement = false;
                colvarDiscountPrice.IsNullable = true;
                colvarDiscountPrice.IsPrimaryKey = false;
                colvarDiscountPrice.IsForeignKey = false;
                colvarDiscountPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountPrice);
                
                TableSchema.TableColumn colvarMessage1 = new TableSchema.TableColumn(schema);
                colvarMessage1.ColumnName = "message_1";
                colvarMessage1.DataType = DbType.String;
                colvarMessage1.MaxLength = 500;
                colvarMessage1.AutoIncrement = false;
                colvarMessage1.IsNullable = true;
                colvarMessage1.IsPrimaryKey = false;
                colvarMessage1.IsForeignKey = false;
                colvarMessage1.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage1);
                
                TableSchema.TableColumn colvarMessage2 = new TableSchema.TableColumn(schema);
                colvarMessage2.ColumnName = "message_2";
                colvarMessage2.DataType = DbType.String;
                colvarMessage2.MaxLength = 500;
                colvarMessage2.AutoIncrement = false;
                colvarMessage2.IsNullable = true;
                colvarMessage2.IsPrimaryKey = false;
                colvarMessage2.IsForeignKey = false;
                colvarMessage2.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage2);
                
                TableSchema.TableColumn colvarMessage3 = new TableSchema.TableColumn(schema);
                colvarMessage3.ColumnName = "message_3";
                colvarMessage3.DataType = DbType.String;
                colvarMessage3.MaxLength = 500;
                colvarMessage3.AutoIncrement = false;
                colvarMessage3.IsNullable = true;
                colvarMessage3.IsPrimaryKey = false;
                colvarMessage3.IsForeignKey = false;
                colvarMessage3.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage3);
                
                TableSchema.TableColumn colvarAllTable = new TableSchema.TableColumn(schema);
                colvarAllTable.ColumnName = "all_table";
                colvarAllTable.DataType = DbType.Boolean;
                colvarAllTable.MaxLength = 0;
                colvarAllTable.AutoIncrement = false;
                colvarAllTable.IsNullable = true;
                colvarAllTable.IsPrimaryKey = false;
                colvarAllTable.IsForeignKey = false;
                colvarAllTable.IsReadOnly = false;
                
                schema.Columns.Add(colvarAllTable);
                
                TableSchema.TableColumn colvarOneMeal = new TableSchema.TableColumn(schema);
                colvarOneMeal.ColumnName = "one_meal";
                colvarOneMeal.DataType = DbType.Boolean;
                colvarOneMeal.MaxLength = 0;
                colvarOneMeal.AutoIncrement = false;
                colvarOneMeal.IsNullable = true;
                colvarOneMeal.IsPrimaryKey = false;
                colvarOneMeal.IsForeignKey = false;
                colvarOneMeal.IsReadOnly = false;
                
                schema.Columns.Add(colvarOneMeal);
                
                TableSchema.TableColumn colvarMultipleMeals = new TableSchema.TableColumn(schema);
                colvarMultipleMeals.ColumnName = "multiple_meals";
                colvarMultipleMeals.DataType = DbType.Boolean;
                colvarMultipleMeals.MaxLength = 0;
                colvarMultipleMeals.AutoIncrement = false;
                colvarMultipleMeals.IsNullable = true;
                colvarMultipleMeals.IsPrimaryKey = false;
                colvarMultipleMeals.IsForeignKey = false;
                colvarMultipleMeals.IsReadOnly = false;
                
                schema.Columns.Add(colvarMultipleMeals);
                
                TableSchema.TableColumn colvarTheFourthApplied = new TableSchema.TableColumn(schema);
                colvarTheFourthApplied.ColumnName = "the_fourth_applied";
                colvarTheFourthApplied.DataType = DbType.Boolean;
                colvarTheFourthApplied.MaxLength = 0;
                colvarTheFourthApplied.AutoIncrement = false;
                colvarTheFourthApplied.IsNullable = true;
                colvarTheFourthApplied.IsPrimaryKey = false;
                colvarTheFourthApplied.IsForeignKey = false;
                colvarTheFourthApplied.IsReadOnly = false;
                
                schema.Columns.Add(colvarTheFourthApplied);
                
                TableSchema.TableColumn colvarGiftAttending = new TableSchema.TableColumn(schema);
                colvarGiftAttending.ColumnName = "gift_attending";
                colvarGiftAttending.DataType = DbType.Boolean;
                colvarGiftAttending.MaxLength = 0;
                colvarGiftAttending.AutoIncrement = false;
                colvarGiftAttending.IsNullable = true;
                colvarGiftAttending.IsPrimaryKey = false;
                colvarGiftAttending.IsForeignKey = false;
                colvarGiftAttending.IsReadOnly = false;
                
                schema.Columns.Add(colvarGiftAttending);
                
                TableSchema.TableColumn colvarGiftConsumption = new TableSchema.TableColumn(schema);
                colvarGiftConsumption.ColumnName = "gift_consumption";
                colvarGiftConsumption.DataType = DbType.Boolean;
                colvarGiftConsumption.MaxLength = 0;
                colvarGiftConsumption.AutoIncrement = false;
                colvarGiftConsumption.IsNullable = true;
                colvarGiftConsumption.IsPrimaryKey = false;
                colvarGiftConsumption.IsForeignKey = false;
                colvarGiftConsumption.IsReadOnly = false;
                
                schema.Columns.Add(colvarGiftConsumption);
                
                TableSchema.TableColumn colvarGiftReplacable = new TableSchema.TableColumn(schema);
                colvarGiftReplacable.ColumnName = "gift_replacable";
                colvarGiftReplacable.DataType = DbType.Boolean;
                colvarGiftReplacable.MaxLength = 0;
                colvarGiftReplacable.AutoIncrement = false;
                colvarGiftReplacable.IsNullable = true;
                colvarGiftReplacable.IsPrimaryKey = false;
                colvarGiftReplacable.IsForeignKey = false;
                colvarGiftReplacable.IsReadOnly = false;
                
                schema.Columns.Add(colvarGiftReplacable);
                
                TableSchema.TableColumn colvarGiftLimited = new TableSchema.TableColumn(schema);
                colvarGiftLimited.ColumnName = "gift_limited";
                colvarGiftLimited.DataType = DbType.Boolean;
                colvarGiftLimited.MaxLength = 0;
                colvarGiftLimited.AutoIncrement = false;
                colvarGiftLimited.IsNullable = true;
                colvarGiftLimited.IsPrimaryKey = false;
                colvarGiftLimited.IsForeignKey = false;
                colvarGiftLimited.IsReadOnly = false;
                
                schema.Columns.Add(colvarGiftLimited);
                
                TableSchema.TableColumn colvarGiftQuantity = new TableSchema.TableColumn(schema);
                colvarGiftQuantity.ColumnName = "gift_quantity";
                colvarGiftQuantity.DataType = DbType.Int32;
                colvarGiftQuantity.MaxLength = 0;
                colvarGiftQuantity.AutoIncrement = false;
                colvarGiftQuantity.IsNullable = true;
                colvarGiftQuantity.IsPrimaryKey = false;
                colvarGiftQuantity.IsForeignKey = false;
                colvarGiftQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarGiftQuantity);
                
                TableSchema.TableColumn colvarServiceFee = new TableSchema.TableColumn(schema);
                colvarServiceFee.ColumnName = "service_fee";
                colvarServiceFee.DataType = DbType.Boolean;
                colvarServiceFee.MaxLength = 0;
                colvarServiceFee.AutoIncrement = false;
                colvarServiceFee.IsNullable = true;
                colvarServiceFee.IsPrimaryKey = false;
                colvarServiceFee.IsForeignKey = false;
                colvarServiceFee.IsReadOnly = false;
                
                schema.Columns.Add(colvarServiceFee);
                
                TableSchema.TableColumn colvarServiceFeeAdditional = new TableSchema.TableColumn(schema);
                colvarServiceFeeAdditional.ColumnName = "service_fee_additional";
                colvarServiceFeeAdditional.DataType = DbType.Boolean;
                colvarServiceFeeAdditional.MaxLength = 0;
                colvarServiceFeeAdditional.AutoIncrement = false;
                colvarServiceFeeAdditional.IsNullable = true;
                colvarServiceFeeAdditional.IsPrimaryKey = false;
                colvarServiceFeeAdditional.IsForeignKey = false;
                colvarServiceFeeAdditional.IsReadOnly = false;
                
                schema.Columns.Add(colvarServiceFeeAdditional);
                
                TableSchema.TableColumn colvarConsumptionApplied = new TableSchema.TableColumn(schema);
                colvarConsumptionApplied.ColumnName = "consumption_applied";
                colvarConsumptionApplied.DataType = DbType.Boolean;
                colvarConsumptionApplied.MaxLength = 0;
                colvarConsumptionApplied.AutoIncrement = false;
                colvarConsumptionApplied.IsNullable = true;
                colvarConsumptionApplied.IsPrimaryKey = false;
                colvarConsumptionApplied.IsForeignKey = false;
                colvarConsumptionApplied.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsumptionApplied);
                
                TableSchema.TableColumn colvarMinConsumption = new TableSchema.TableColumn(schema);
                colvarMinConsumption.ColumnName = "min_consumption";
                colvarMinConsumption.DataType = DbType.Int32;
                colvarMinConsumption.MaxLength = 0;
                colvarMinConsumption.AutoIncrement = false;
                colvarMinConsumption.IsNullable = true;
                colvarMinConsumption.IsPrimaryKey = false;
                colvarMinConsumption.IsForeignKey = false;
                colvarMinConsumption.IsReadOnly = false;
                
                schema.Columns.Add(colvarMinConsumption);
                
                TableSchema.TableColumn colvarMinPersons = new TableSchema.TableColumn(schema);
                colvarMinPersons.ColumnName = "min_persons";
                colvarMinPersons.DataType = DbType.Int32;
                colvarMinPersons.MaxLength = 0;
                colvarMinPersons.AutoIncrement = false;
                colvarMinPersons.IsNullable = true;
                colvarMinPersons.IsPrimaryKey = false;
                colvarMinPersons.IsForeignKey = false;
                colvarMinPersons.IsReadOnly = false;
                
                schema.Columns.Add(colvarMinPersons);
                
                TableSchema.TableColumn colvarHolidayApplied = new TableSchema.TableColumn(schema);
                colvarHolidayApplied.ColumnName = "holiday_applied";
                colvarHolidayApplied.DataType = DbType.Boolean;
                colvarHolidayApplied.MaxLength = 0;
                colvarHolidayApplied.AutoIncrement = false;
                colvarHolidayApplied.IsNullable = true;
                colvarHolidayApplied.IsPrimaryKey = false;
                colvarHolidayApplied.IsForeignKey = false;
                colvarHolidayApplied.IsReadOnly = false;
                
                schema.Columns.Add(colvarHolidayApplied);
                
                TableSchema.TableColumn colvarWeekendApplied = new TableSchema.TableColumn(schema);
                colvarWeekendApplied.ColumnName = "weekend_applied";
                colvarWeekendApplied.DataType = DbType.Boolean;
                colvarWeekendApplied.MaxLength = 0;
                colvarWeekendApplied.AutoIncrement = false;
                colvarWeekendApplied.IsNullable = true;
                colvarWeekendApplied.IsPrimaryKey = false;
                colvarWeekendApplied.IsForeignKey = false;
                colvarWeekendApplied.IsReadOnly = false;
                
                schema.Columns.Add(colvarWeekendApplied);
                
                TableSchema.TableColumn colvarOtherTimes = new TableSchema.TableColumn(schema);
                colvarOtherTimes.ColumnName = "other_times";
                colvarOtherTimes.DataType = DbType.String;
                colvarOtherTimes.MaxLength = 500;
                colvarOtherTimes.AutoIncrement = false;
                colvarOtherTimes.IsNullable = true;
                colvarOtherTimes.IsPrimaryKey = false;
                colvarOtherTimes.IsForeignKey = false;
                colvarOtherTimes.IsReadOnly = false;
                
                schema.Columns.Add(colvarOtherTimes);
                
                TableSchema.TableColumn colvarOthers = new TableSchema.TableColumn(schema);
                colvarOthers.ColumnName = "others";
                colvarOthers.DataType = DbType.String;
                colvarOthers.MaxLength = 500;
                colvarOthers.AutoIncrement = false;
                colvarOthers.IsNullable = true;
                colvarOthers.IsPrimaryKey = false;
                colvarOthers.IsForeignKey = false;
                colvarOthers.IsReadOnly = false;
                
                schema.Columns.Add(colvarOthers);
                
                TableSchema.TableColumn colvarRestriction = new TableSchema.TableColumn(schema);
                colvarRestriction.ColumnName = "restriction";
                colvarRestriction.DataType = DbType.String;
                colvarRestriction.MaxLength = 1000;
                colvarRestriction.AutoIncrement = false;
                colvarRestriction.IsNullable = true;
                colvarRestriction.IsPrimaryKey = false;
                colvarRestriction.IsForeignKey = false;
                colvarRestriction.IsReadOnly = false;
                
                schema.Columns.Add(colvarRestriction);
                
                TableSchema.TableColumn colvarReturnTime = new TableSchema.TableColumn(schema);
                colvarReturnTime.ColumnName = "return_time";
                colvarReturnTime.DataType = DbType.DateTime;
                colvarReturnTime.MaxLength = 0;
                colvarReturnTime.AutoIncrement = false;
                colvarReturnTime.IsNullable = true;
                colvarReturnTime.IsPrimaryKey = false;
                colvarReturnTime.IsForeignKey = false;
                colvarReturnTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnTime);
                
                TableSchema.TableColumn colvarApproveTime = new TableSchema.TableColumn(schema);
                colvarApproveTime.ColumnName = "approve_time";
                colvarApproveTime.DataType = DbType.DateTime;
                colvarApproveTime.MaxLength = 0;
                colvarApproveTime.AutoIncrement = false;
                colvarApproveTime.IsNullable = true;
                colvarApproveTime.IsPrimaryKey = false;
                colvarApproveTime.IsForeignKey = false;
                colvarApproveTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarApproveTime);
                
                TableSchema.TableColumn colvarCityBit = new TableSchema.TableColumn(schema);
                colvarCityBit.ColumnName = "city_bit";
                colvarCityBit.DataType = DbType.Int32;
                colvarCityBit.MaxLength = 0;
                colvarCityBit.AutoIncrement = false;
                colvarCityBit.IsNullable = false;
                colvarCityBit.IsPrimaryKey = false;
                colvarCityBit.IsForeignKey = false;
                colvarCityBit.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityBit);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarCollectType = new TableSchema.TableColumn(schema);
                colvarCollectType.ColumnName = "collect_type";
                colvarCollectType.DataType = DbType.Int32;
                colvarCollectType.MaxLength = 0;
                colvarCollectType.AutoIncrement = false;
                colvarCollectType.IsNullable = false;
                colvarCollectType.IsPrimaryKey = false;
                colvarCollectType.IsForeignKey = false;
                colvarCollectType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCollectType);
                
                TableSchema.TableColumn colvarCollectStatus = new TableSchema.TableColumn(schema);
                colvarCollectStatus.ColumnName = "collect_status";
                colvarCollectStatus.DataType = DbType.Int32;
                colvarCollectStatus.MaxLength = 0;
                colvarCollectStatus.AutoIncrement = false;
                colvarCollectStatus.IsNullable = false;
                colvarCollectStatus.IsPrimaryKey = false;
                colvarCollectStatus.IsForeignKey = false;
                colvarCollectStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCollectStatus);
                
                TableSchema.TableColumn colvarCollectId = new TableSchema.TableColumn(schema);
                colvarCollectId.ColumnName = "collect_id";
                colvarCollectId.DataType = DbType.Int32;
                colvarCollectId.MaxLength = 0;
                colvarCollectId.AutoIncrement = false;
                colvarCollectId.IsNullable = false;
                colvarCollectId.IsPrimaryKey = false;
                colvarCollectId.IsForeignKey = false;
                colvarCollectId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCollectId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vourcher_seller_collect",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVourcherSellerCollect()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVourcherSellerCollect(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVourcherSellerCollect(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVourcherSellerCollect(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("EventName")]
        [Bindable(true)]
        public string EventName 
	    {
		    get
		    {
			    return GetColumnValue<string>("event_name");
		    }
            set 
		    {
			    SetColumnValue("event_name", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("Contents")]
        [Bindable(true)]
        public string Contents 
	    {
		    get
		    {
			    return GetColumnValue<string>("contents");
		    }
            set 
		    {
			    SetColumnValue("contents", value);
            }
        }
	      
        [XmlAttribute("Instruction")]
        [Bindable(true)]
        public string Instruction 
	    {
		    get
		    {
			    return GetColumnValue<string>("instruction");
		    }
            set 
		    {
			    SetColumnValue("instruction", value);
            }
        }
	      
        [XmlAttribute("PicUrl")]
        [Bindable(true)]
        public string PicUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("pic_url");
		    }
            set 
		    {
			    SetColumnValue("pic_url", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("StartDate")]
        [Bindable(true)]
        public DateTime? StartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_date");
		    }
            set 
		    {
			    SetColumnValue("start_date", value);
            }
        }
	      
        [XmlAttribute("EndDate")]
        [Bindable(true)]
        public DateTime? EndDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_date");
		    }
            set 
		    {
			    SetColumnValue("end_date", value);
            }
        }
	      
        [XmlAttribute("PageCount")]
        [Bindable(true)]
        public int PageCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("page_count");
		    }
            set 
		    {
			    SetColumnValue("page_count", value);
            }
        }
	      
        [XmlAttribute("MaxQuantity")]
        [Bindable(true)]
        public int MaxQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("max_quantity");
		    }
            set 
		    {
			    SetColumnValue("max_quantity", value);
            }
        }
	      
        [XmlAttribute("CurrentQuantity")]
        [Bindable(true)]
        public int CurrentQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("current_quantity");
		    }
            set 
		    {
			    SetColumnValue("current_quantity", value);
            }
        }
	      
        [XmlAttribute("Ratio")]
        [Bindable(true)]
        public int Ratio 
	    {
		    get
		    {
			    return GetColumnValue<int>("ratio");
		    }
            set 
		    {
			    SetColumnValue("ratio", value);
            }
        }
	      
        [XmlAttribute("Enable")]
        [Bindable(true)]
        public bool Enable 
	    {
		    get
		    {
			    return GetColumnValue<bool>("enable");
		    }
            set 
		    {
			    SetColumnValue("enable", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message 
	    {
		    get
		    {
			    return GetColumnValue<string>("message");
		    }
            set 
		    {
			    SetColumnValue("message", value);
            }
        }
	      
        [XmlAttribute("Mode")]
        [Bindable(true)]
        public int Mode 
	    {
		    get
		    {
			    return GetColumnValue<int>("mode");
		    }
            set 
		    {
			    SetColumnValue("mode", value);
            }
        }
	      
        [XmlAttribute("Magazine")]
        [Bindable(true)]
        public bool? Magazine 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("magazine");
		    }
            set 
		    {
			    SetColumnValue("magazine", value);
            }
        }
	      
        [XmlAttribute("Discount")]
        [Bindable(true)]
        public decimal? Discount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("discount");
		    }
            set 
		    {
			    SetColumnValue("discount", value);
            }
        }
	      
        [XmlAttribute("OriginalPrice")]
        [Bindable(true)]
        public int? OriginalPrice 
	    {
		    get
		    {
			    return GetColumnValue<int?>("original_price");
		    }
            set 
		    {
			    SetColumnValue("original_price", value);
            }
        }
	      
        [XmlAttribute("DiscountPrice")]
        [Bindable(true)]
        public int? DiscountPrice 
	    {
		    get
		    {
			    return GetColumnValue<int?>("discount_price");
		    }
            set 
		    {
			    SetColumnValue("discount_price", value);
            }
        }
	      
        [XmlAttribute("Message1")]
        [Bindable(true)]
        public string Message1 
	    {
		    get
		    {
			    return GetColumnValue<string>("message_1");
		    }
            set 
		    {
			    SetColumnValue("message_1", value);
            }
        }
	      
        [XmlAttribute("Message2")]
        [Bindable(true)]
        public string Message2 
	    {
		    get
		    {
			    return GetColumnValue<string>("message_2");
		    }
            set 
		    {
			    SetColumnValue("message_2", value);
            }
        }
	      
        [XmlAttribute("Message3")]
        [Bindable(true)]
        public string Message3 
	    {
		    get
		    {
			    return GetColumnValue<string>("message_3");
		    }
            set 
		    {
			    SetColumnValue("message_3", value);
            }
        }
	      
        [XmlAttribute("AllTable")]
        [Bindable(true)]
        public bool? AllTable 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("all_table");
		    }
            set 
		    {
			    SetColumnValue("all_table", value);
            }
        }
	      
        [XmlAttribute("OneMeal")]
        [Bindable(true)]
        public bool? OneMeal 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("one_meal");
		    }
            set 
		    {
			    SetColumnValue("one_meal", value);
            }
        }
	      
        [XmlAttribute("MultipleMeals")]
        [Bindable(true)]
        public bool? MultipleMeals 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("multiple_meals");
		    }
            set 
		    {
			    SetColumnValue("multiple_meals", value);
            }
        }
	      
        [XmlAttribute("TheFourthApplied")]
        [Bindable(true)]
        public bool? TheFourthApplied 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("the_fourth_applied");
		    }
            set 
		    {
			    SetColumnValue("the_fourth_applied", value);
            }
        }
	      
        [XmlAttribute("GiftAttending")]
        [Bindable(true)]
        public bool? GiftAttending 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("gift_attending");
		    }
            set 
		    {
			    SetColumnValue("gift_attending", value);
            }
        }
	      
        [XmlAttribute("GiftConsumption")]
        [Bindable(true)]
        public bool? GiftConsumption 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("gift_consumption");
		    }
            set 
		    {
			    SetColumnValue("gift_consumption", value);
            }
        }
	      
        [XmlAttribute("GiftReplacable")]
        [Bindable(true)]
        public bool? GiftReplacable 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("gift_replacable");
		    }
            set 
		    {
			    SetColumnValue("gift_replacable", value);
            }
        }
	      
        [XmlAttribute("GiftLimited")]
        [Bindable(true)]
        public bool? GiftLimited 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("gift_limited");
		    }
            set 
		    {
			    SetColumnValue("gift_limited", value);
            }
        }
	      
        [XmlAttribute("GiftQuantity")]
        [Bindable(true)]
        public int? GiftQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("gift_quantity");
		    }
            set 
		    {
			    SetColumnValue("gift_quantity", value);
            }
        }
	      
        [XmlAttribute("ServiceFee")]
        [Bindable(true)]
        public bool? ServiceFee 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("service_fee");
		    }
            set 
		    {
			    SetColumnValue("service_fee", value);
            }
        }
	      
        [XmlAttribute("ServiceFeeAdditional")]
        [Bindable(true)]
        public bool? ServiceFeeAdditional 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("service_fee_additional");
		    }
            set 
		    {
			    SetColumnValue("service_fee_additional", value);
            }
        }
	      
        [XmlAttribute("ConsumptionApplied")]
        [Bindable(true)]
        public bool? ConsumptionApplied 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("consumption_applied");
		    }
            set 
		    {
			    SetColumnValue("consumption_applied", value);
            }
        }
	      
        [XmlAttribute("MinConsumption")]
        [Bindable(true)]
        public int? MinConsumption 
	    {
		    get
		    {
			    return GetColumnValue<int?>("min_consumption");
		    }
            set 
		    {
			    SetColumnValue("min_consumption", value);
            }
        }
	      
        [XmlAttribute("MinPersons")]
        [Bindable(true)]
        public int? MinPersons 
	    {
		    get
		    {
			    return GetColumnValue<int?>("min_persons");
		    }
            set 
		    {
			    SetColumnValue("min_persons", value);
            }
        }
	      
        [XmlAttribute("HolidayApplied")]
        [Bindable(true)]
        public bool? HolidayApplied 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("holiday_applied");
		    }
            set 
		    {
			    SetColumnValue("holiday_applied", value);
            }
        }
	      
        [XmlAttribute("WeekendApplied")]
        [Bindable(true)]
        public bool? WeekendApplied 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("weekend_applied");
		    }
            set 
		    {
			    SetColumnValue("weekend_applied", value);
            }
        }
	      
        [XmlAttribute("OtherTimes")]
        [Bindable(true)]
        public string OtherTimes 
	    {
		    get
		    {
			    return GetColumnValue<string>("other_times");
		    }
            set 
		    {
			    SetColumnValue("other_times", value);
            }
        }
	      
        [XmlAttribute("Others")]
        [Bindable(true)]
        public string Others 
	    {
		    get
		    {
			    return GetColumnValue<string>("others");
		    }
            set 
		    {
			    SetColumnValue("others", value);
            }
        }
	      
        [XmlAttribute("Restriction")]
        [Bindable(true)]
        public string Restriction 
	    {
		    get
		    {
			    return GetColumnValue<string>("restriction");
		    }
            set 
		    {
			    SetColumnValue("restriction", value);
            }
        }
	      
        [XmlAttribute("ReturnTime")]
        [Bindable(true)]
        public DateTime? ReturnTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("return_time");
		    }
            set 
		    {
			    SetColumnValue("return_time", value);
            }
        }
	      
        [XmlAttribute("ApproveTime")]
        [Bindable(true)]
        public DateTime? ApproveTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("approve_time");
		    }
            set 
		    {
			    SetColumnValue("approve_time", value);
            }
        }
	      
        [XmlAttribute("CityBit")]
        [Bindable(true)]
        public int CityBit 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_bit");
		    }
            set 
		    {
			    SetColumnValue("city_bit", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("CollectType")]
        [Bindable(true)]
        public int CollectType 
	    {
		    get
		    {
			    return GetColumnValue<int>("collect_type");
		    }
            set 
		    {
			    SetColumnValue("collect_type", value);
            }
        }
	      
        [XmlAttribute("CollectStatus")]
        [Bindable(true)]
        public int CollectStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("collect_status");
		    }
            set 
		    {
			    SetColumnValue("collect_status", value);
            }
        }
	      
        [XmlAttribute("CollectId")]
        [Bindable(true)]
        public int CollectId 
	    {
		    get
		    {
			    return GetColumnValue<int>("collect_id");
		    }
            set 
		    {
			    SetColumnValue("collect_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string EventName = @"event_name";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string Contents = @"contents";
            
            public static string Instruction = @"instruction";
            
            public static string PicUrl = @"pic_url";
            
            public static string Status = @"status";
            
            public static string Type = @"type";
            
            public static string StartDate = @"start_date";
            
            public static string EndDate = @"end_date";
            
            public static string PageCount = @"page_count";
            
            public static string MaxQuantity = @"max_quantity";
            
            public static string CurrentQuantity = @"current_quantity";
            
            public static string Ratio = @"ratio";
            
            public static string Enable = @"enable";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string ModifyTime = @"modify_time";
            
            public static string Message = @"message";
            
            public static string Mode = @"mode";
            
            public static string Magazine = @"magazine";
            
            public static string Discount = @"discount";
            
            public static string OriginalPrice = @"original_price";
            
            public static string DiscountPrice = @"discount_price";
            
            public static string Message1 = @"message_1";
            
            public static string Message2 = @"message_2";
            
            public static string Message3 = @"message_3";
            
            public static string AllTable = @"all_table";
            
            public static string OneMeal = @"one_meal";
            
            public static string MultipleMeals = @"multiple_meals";
            
            public static string TheFourthApplied = @"the_fourth_applied";
            
            public static string GiftAttending = @"gift_attending";
            
            public static string GiftConsumption = @"gift_consumption";
            
            public static string GiftReplacable = @"gift_replacable";
            
            public static string GiftLimited = @"gift_limited";
            
            public static string GiftQuantity = @"gift_quantity";
            
            public static string ServiceFee = @"service_fee";
            
            public static string ServiceFeeAdditional = @"service_fee_additional";
            
            public static string ConsumptionApplied = @"consumption_applied";
            
            public static string MinConsumption = @"min_consumption";
            
            public static string MinPersons = @"min_persons";
            
            public static string HolidayApplied = @"holiday_applied";
            
            public static string WeekendApplied = @"weekend_applied";
            
            public static string OtherTimes = @"other_times";
            
            public static string Others = @"others";
            
            public static string Restriction = @"restriction";
            
            public static string ReturnTime = @"return_time";
            
            public static string ApproveTime = @"approve_time";
            
            public static string CityBit = @"city_bit";
            
            public static string SellerId = @"seller_id";
            
            public static string SellerName = @"seller_name";
            
            public static string UniqueId = @"unique_id";
            
            public static string CollectType = @"collect_type";
            
            public static string CollectStatus = @"collect_status";
            
            public static string CollectId = @"collect_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

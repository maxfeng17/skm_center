using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealFreight class.
	/// </summary>
    [Serializable]
	public partial class HiDealFreightCollection : RepositoryList<HiDealFreight, HiDealFreightCollection>
	{	   
		public HiDealFreightCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealFreightCollection</returns>
		public HiDealFreightCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealFreight o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_freight table.
	/// </summary>
	[Serializable]
	public partial class HiDealFreight : RepositoryRecord<HiDealFreight>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealFreight()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealFreight(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_freight", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
				colvarProductId.ColumnName = "product_id";
				colvarProductId.DataType = DbType.Int32;
				colvarProductId.MaxLength = 0;
				colvarProductId.AutoIncrement = false;
				colvarProductId.IsNullable = false;
				colvarProductId.IsPrimaryKey = false;
				colvarProductId.IsForeignKey = true;
				colvarProductId.IsReadOnly = false;
				colvarProductId.DefaultSetting = @"";
				
					colvarProductId.ForeignKeyTableName = "hi_deal_product";
				schema.Columns.Add(colvarProductId);
				
				TableSchema.TableColumn colvarStartAmount = new TableSchema.TableColumn(schema);
				colvarStartAmount.ColumnName = "start_amount";
				colvarStartAmount.DataType = DbType.Currency;
				colvarStartAmount.MaxLength = 0;
				colvarStartAmount.AutoIncrement = false;
				colvarStartAmount.IsNullable = false;
				colvarStartAmount.IsPrimaryKey = false;
				colvarStartAmount.IsForeignKey = false;
				colvarStartAmount.IsReadOnly = false;
				
						colvarStartAmount.DefaultSetting = @"((0))";
				colvarStartAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartAmount);
				
				TableSchema.TableColumn colvarEndAmount = new TableSchema.TableColumn(schema);
				colvarEndAmount.ColumnName = "end_amount";
				colvarEndAmount.DataType = DbType.Currency;
				colvarEndAmount.MaxLength = 0;
				colvarEndAmount.AutoIncrement = false;
				colvarEndAmount.IsNullable = false;
				colvarEndAmount.IsPrimaryKey = false;
				colvarEndAmount.IsForeignKey = false;
				colvarEndAmount.IsReadOnly = false;
				
						colvarEndAmount.DefaultSetting = @"((0))";
				colvarEndAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndAmount);
				
				TableSchema.TableColumn colvarFreightAmount = new TableSchema.TableColumn(schema);
				colvarFreightAmount.ColumnName = "freight_amount";
				colvarFreightAmount.DataType = DbType.Currency;
				colvarFreightAmount.MaxLength = 0;
				colvarFreightAmount.AutoIncrement = false;
				colvarFreightAmount.IsNullable = false;
				colvarFreightAmount.IsPrimaryKey = false;
				colvarFreightAmount.IsForeignKey = false;
				colvarFreightAmount.IsReadOnly = false;
				
						colvarFreightAmount.DefaultSetting = @"((0))";
				colvarFreightAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFreightAmount);
				
				TableSchema.TableColumn colvarFreightType = new TableSchema.TableColumn(schema);
				colvarFreightType.ColumnName = "freight_type";
				colvarFreightType.DataType = DbType.Int32;
				colvarFreightType.MaxLength = 0;
				colvarFreightType.AutoIncrement = false;
				colvarFreightType.IsNullable = false;
				colvarFreightType.IsPrimaryKey = false;
				colvarFreightType.IsForeignKey = false;
				colvarFreightType.IsReadOnly = false;
				colvarFreightType.DefaultSetting = @"";
				colvarFreightType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFreightType);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_freight",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ProductId")]
		[Bindable(true)]
		public int ProductId 
		{
			get { return GetColumnValue<int>(Columns.ProductId); }
			set { SetColumnValue(Columns.ProductId, value); }
		}
		  
		[XmlAttribute("StartAmount")]
		[Bindable(true)]
		public decimal StartAmount 
		{
			get { return GetColumnValue<decimal>(Columns.StartAmount); }
			set { SetColumnValue(Columns.StartAmount, value); }
		}
		  
		[XmlAttribute("EndAmount")]
		[Bindable(true)]
		public decimal EndAmount 
		{
			get { return GetColumnValue<decimal>(Columns.EndAmount); }
			set { SetColumnValue(Columns.EndAmount, value); }
		}
		  
		[XmlAttribute("FreightAmount")]
		[Bindable(true)]
		public decimal FreightAmount 
		{
			get { return GetColumnValue<decimal>(Columns.FreightAmount); }
			set { SetColumnValue(Columns.FreightAmount, value); }
		}
		  
		[XmlAttribute("FreightType")]
		[Bindable(true)]
		public int FreightType 
		{
			get { return GetColumnValue<int>(Columns.FreightType); }
			set { SetColumnValue(Columns.FreightType, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StartAmountColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn EndAmountColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn FreightAmountColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn FreightTypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ProductId = @"product_id";
			 public static string StartAmount = @"start_amount";
			 public static string EndAmount = @"end_amount";
			 public static string FreightAmount = @"freight_amount";
			 public static string FreightType = @"freight_type";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

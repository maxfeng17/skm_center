using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVbsReturnOrderList class.
    /// </summary>
    [Serializable]
    public partial class ViewVbsReturnOrderListCollection : ReadOnlyList<ViewVbsReturnOrderList, ViewVbsReturnOrderListCollection>
    {        
        public ViewVbsReturnOrderListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vbs_return_order_list view.
    /// </summary>
    [Serializable]
    public partial class ViewVbsReturnOrderList : ReadOnlyRecord<ViewVbsReturnOrderList>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vbs_return_order_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarProgressStatus = new TableSchema.TableColumn(schema);
                colvarProgressStatus.ColumnName = "progress_status";
                colvarProgressStatus.DataType = DbType.Int32;
                colvarProgressStatus.MaxLength = 0;
                colvarProgressStatus.AutoIncrement = false;
                colvarProgressStatus.IsNullable = false;
                colvarProgressStatus.IsPrimaryKey = false;
                colvarProgressStatus.IsForeignKey = false;
                colvarProgressStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarProgressStatus);
                
                TableSchema.TableColumn colvarVendorProgressStatus = new TableSchema.TableColumn(schema);
                colvarVendorProgressStatus.ColumnName = "vendor_progress_status";
                colvarVendorProgressStatus.DataType = DbType.Int32;
                colvarVendorProgressStatus.MaxLength = 0;
                colvarVendorProgressStatus.AutoIncrement = false;
                colvarVendorProgressStatus.IsNullable = true;
                colvarVendorProgressStatus.IsPrimaryKey = false;
                colvarVendorProgressStatus.IsForeignKey = false;
                colvarVendorProgressStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorProgressStatus);
                
                TableSchema.TableColumn colvarVendorMemo = new TableSchema.TableColumn(schema);
                colvarVendorMemo.ColumnName = "vendor_memo";
                colvarVendorMemo.DataType = DbType.String;
                colvarVendorMemo.MaxLength = 30;
                colvarVendorMemo.AutoIncrement = false;
                colvarVendorMemo.IsNullable = true;
                colvarVendorMemo.IsPrimaryKey = false;
                colvarVendorMemo.IsForeignKey = false;
                colvarVendorMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorMemo);
                
                TableSchema.TableColumn colvarVendorProcessTime = new TableSchema.TableColumn(schema);
                colvarVendorProcessTime.ColumnName = "vendor_process_time";
                colvarVendorProcessTime.DataType = DbType.DateTime;
                colvarVendorProcessTime.MaxLength = 0;
                colvarVendorProcessTime.AutoIncrement = false;
                colvarVendorProcessTime.IsNullable = true;
                colvarVendorProcessTime.IsPrimaryKey = false;
                colvarVendorProcessTime.IsForeignKey = false;
                colvarVendorProcessTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorProcessTime);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarReceiverAddress = new TableSchema.TableColumn(schema);
                colvarReceiverAddress.ColumnName = "receiver_address";
                colvarReceiverAddress.DataType = DbType.String;
                colvarReceiverAddress.MaxLength = 200;
                colvarReceiverAddress.AutoIncrement = false;
                colvarReceiverAddress.IsNullable = true;
                colvarReceiverAddress.IsPrimaryKey = false;
                colvarReceiverAddress.IsForeignKey = false;
                colvarReceiverAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverAddress);
                
                TableSchema.TableColumn colvarReturnReason = new TableSchema.TableColumn(schema);
                colvarReturnReason.ColumnName = "return_reason";
                colvarReturnReason.DataType = DbType.String;
                colvarReturnReason.MaxLength = 4000;
                colvarReturnReason.AutoIncrement = false;
                colvarReturnReason.IsNullable = true;
                colvarReturnReason.IsPrimaryKey = false;
                colvarReturnReason.IsForeignKey = false;
                colvarReturnReason.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnReason);
                
                TableSchema.TableColumn colvarReceiverName = new TableSchema.TableColumn(schema);
                colvarReceiverName.ColumnName = "receiver_name";
                colvarReceiverName.DataType = DbType.String;
                colvarReceiverName.MaxLength = 50;
                colvarReceiverName.AutoIncrement = false;
                colvarReceiverName.IsNullable = true;
                colvarReceiverName.IsPrimaryKey = false;
                colvarReceiverName.IsForeignKey = false;
                colvarReceiverName.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiverName);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
                colvarMobileNumber.ColumnName = "mobile_number";
                colvarMobileNumber.DataType = DbType.AnsiString;
                colvarMobileNumber.MaxLength = 50;
                colvarMobileNumber.AutoIncrement = false;
                colvarMobileNumber.IsNullable = true;
                colvarMobileNumber.IsPrimaryKey = false;
                colvarMobileNumber.IsForeignKey = false;
                colvarMobileNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobileNumber);
                
                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryAddress);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBid);
                
                TableSchema.TableColumn colvarMainBid = new TableSchema.TableColumn(schema);
                colvarMainBid.ColumnName = "main_bid";
                colvarMainBid.DataType = DbType.Guid;
                colvarMainBid.MaxLength = 0;
                colvarMainBid.AutoIncrement = false;
                colvarMainBid.IsNullable = true;
                colvarMainBid.IsPrimaryKey = false;
                colvarMainBid.IsForeignKey = false;
                colvarMainBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainBid);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = false;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountId);
                
                TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
                colvarReturnFormId.ColumnName = "return_form_id";
                colvarReturnFormId.DataType = DbType.Int32;
                colvarReturnFormId.MaxLength = 0;
                colvarReturnFormId.AutoIncrement = false;
                colvarReturnFormId.IsNullable = false;
                colvarReturnFormId.IsPrimaryKey = false;
                colvarReturnFormId.IsForeignKey = false;
                colvarReturnFormId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnFormId);
                
                TableSchema.TableColumn colvarComboPackCount = new TableSchema.TableColumn(schema);
                colvarComboPackCount.ColumnName = "combo_pack_count";
                colvarComboPackCount.DataType = DbType.Int32;
                colvarComboPackCount.MaxLength = 0;
                colvarComboPackCount.AutoIncrement = false;
                colvarComboPackCount.IsNullable = false;
                colvarComboPackCount.IsPrimaryKey = false;
                colvarComboPackCount.IsForeignKey = false;
                colvarComboPackCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarComboPackCount);
                
                TableSchema.TableColumn colvarShipTime = new TableSchema.TableColumn(schema);
                colvarShipTime.ColumnName = "ship_time";
                colvarShipTime.DataType = DbType.DateTime;
                colvarShipTime.MaxLength = 0;
                colvarShipTime.AutoIncrement = false;
                colvarShipTime.IsNullable = true;
                colvarShipTime.IsPrimaryKey = false;
                colvarShipTime.IsForeignKey = false;
                colvarShipTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipTime);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vbs_return_order_list",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVbsReturnOrderList()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVbsReturnOrderList(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVbsReturnOrderList(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVbsReturnOrderList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("ProgressStatus")]
        [Bindable(true)]
        public int ProgressStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("progress_status");
		    }
            set 
		    {
			    SetColumnValue("progress_status", value);
            }
        }
	      
        [XmlAttribute("VendorProgressStatus")]
        [Bindable(true)]
        public int? VendorProgressStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vendor_progress_status");
		    }
            set 
		    {
			    SetColumnValue("vendor_progress_status", value);
            }
        }
	      
        [XmlAttribute("VendorMemo")]
        [Bindable(true)]
        public string VendorMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("vendor_memo");
		    }
            set 
		    {
			    SetColumnValue("vendor_memo", value);
            }
        }
	      
        [XmlAttribute("VendorProcessTime")]
        [Bindable(true)]
        public DateTime? VendorProcessTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("vendor_process_time");
		    }
            set 
		    {
			    SetColumnValue("vendor_process_time", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ReceiverAddress")]
        [Bindable(true)]
        public string ReceiverAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_address");
		    }
            set 
		    {
			    SetColumnValue("receiver_address", value);
            }
        }
	      
        [XmlAttribute("ReturnReason")]
        [Bindable(true)]
        public string ReturnReason 
	    {
		    get
		    {
			    return GetColumnValue<string>("return_reason");
		    }
            set 
		    {
			    SetColumnValue("return_reason", value);
            }
        }
	      
        [XmlAttribute("ReceiverName")]
        [Bindable(true)]
        public string ReceiverName 
	    {
		    get
		    {
			    return GetColumnValue<string>("receiver_name");
		    }
            set 
		    {
			    SetColumnValue("receiver_name", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("MobileNumber")]
        [Bindable(true)]
        public string MobileNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile_number");
		    }
            set 
		    {
			    SetColumnValue("mobile_number", value);
            }
        }
	      
        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_address");
		    }
            set 
		    {
			    SetColumnValue("delivery_address", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("bid");
		    }
            set 
		    {
			    SetColumnValue("bid", value);
            }
        }
	      
        [XmlAttribute("MainBid")]
        [Bindable(true)]
        public Guid? MainBid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("main_bid");
		    }
            set 
		    {
			    SetColumnValue("main_bid", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_id");
		    }
            set 
		    {
			    SetColumnValue("account_id", value);
            }
        }
	      
        [XmlAttribute("ReturnFormId")]
        [Bindable(true)]
        public int ReturnFormId 
	    {
		    get
		    {
			    return GetColumnValue<int>("return_form_id");
		    }
            set 
		    {
			    SetColumnValue("return_form_id", value);
            }
        }
	      
        [XmlAttribute("ComboPackCount")]
        [Bindable(true)]
        public int ComboPackCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("combo_pack_count");
		    }
            set 
		    {
			    SetColumnValue("combo_pack_count", value);
            }
        }
	      
        [XmlAttribute("ShipTime")]
        [Bindable(true)]
        public DateTime? ShipTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ship_time");
		    }
            set 
		    {
			    SetColumnValue("ship_time", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderId = @"order_id";
            
            public static string ProgressStatus = @"progress_status";
            
            public static string VendorProgressStatus = @"vendor_progress_status";
            
            public static string VendorMemo = @"vendor_memo";
            
            public static string VendorProcessTime = @"vendor_process_time";
            
            public static string CreateTime = @"create_time";
            
            public static string ReceiverAddress = @"receiver_address";
            
            public static string ReturnReason = @"return_reason";
            
            public static string ReceiverName = @"receiver_name";
            
            public static string MemberName = @"member_name";
            
            public static string MobileNumber = @"mobile_number";
            
            public static string DeliveryAddress = @"delivery_address";
            
            public static string UniqueId = @"unique_id";
            
            public static string Bid = @"bid";
            
            public static string MainBid = @"main_bid";
            
            public static string ItemName = @"item_name";
            
            public static string AccountId = @"account_id";
            
            public static string ReturnFormId = @"return_form_id";
            
            public static string ComboPackCount = @"combo_pack_count";
            
            public static string ShipTime = @"ship_time";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the DocumentList class.
    /// </summary>
    [Serializable]
    public partial class DocumentListCollection : RepositoryList<DocumentList, DocumentListCollection>
    {
        public DocumentListCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DocumentListCollection</returns>
        public DocumentListCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DocumentList o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the document_list table.
    /// </summary>
    [Serializable]
    public partial class DocumentList : RepositoryRecord<DocumentList>, IRecordBase
    {
        #region .ctors and Default Settings

        public DocumentList()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public DocumentList(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("document_list", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarCategoryGuid = new TableSchema.TableColumn(schema);
                colvarCategoryGuid.ColumnName = "category_guid";
                colvarCategoryGuid.DataType = DbType.Guid;
                colvarCategoryGuid.MaxLength = 0;
                colvarCategoryGuid.AutoIncrement = false;
                colvarCategoryGuid.IsNullable = false;
                colvarCategoryGuid.IsPrimaryKey = false;
                colvarCategoryGuid.IsForeignKey = false;
                colvarCategoryGuid.IsReadOnly = false;
                colvarCategoryGuid.DefaultSetting = @"";
                colvarCategoryGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCategoryGuid);

                TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
                colvarUrl.ColumnName = "url";
                colvarUrl.DataType = DbType.AnsiString;
                colvarUrl.MaxLength = 2000;
                colvarUrl.AutoIncrement = false;
                colvarUrl.IsNullable = true;
                colvarUrl.IsPrimaryKey = false;
                colvarUrl.IsForeignKey = false;
                colvarUrl.IsReadOnly = false;
                colvarUrl.DefaultSetting = @"";
                colvarUrl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUrl);

                TableSchema.TableColumn colvarDocName = new TableSchema.TableColumn(schema);
                colvarDocName.ColumnName = "doc_name";
                colvarDocName.DataType = DbType.String;
                colvarDocName.MaxLength = 2000;
                colvarDocName.AutoIncrement = false;
                colvarDocName.IsNullable = false;
                colvarDocName.IsPrimaryKey = false;
                colvarDocName.IsForeignKey = false;
                colvarDocName.IsReadOnly = false;
                colvarDocName.DefaultSetting = @"";
                colvarDocName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDocName);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 100;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.AnsiString;
                colvarModifyId.MaxLength = 100;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = false;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarReadAuth = new TableSchema.TableColumn(schema);
                colvarReadAuth.ColumnName = "read_auth";
                colvarReadAuth.DataType = DbType.AnsiString;
                colvarReadAuth.MaxLength = 2000;
                colvarReadAuth.AutoIncrement = false;
                colvarReadAuth.IsNullable = true;
                colvarReadAuth.IsPrimaryKey = false;
                colvarReadAuth.IsForeignKey = false;
                colvarReadAuth.IsReadOnly = false;
                colvarReadAuth.DefaultSetting = @"";
                colvarReadAuth.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReadAuth);

                TableSchema.TableColumn colvarReadAuthPerson = new TableSchema.TableColumn(schema);
                colvarReadAuthPerson.ColumnName = "read_auth_person";
                colvarReadAuthPerson.DataType = DbType.AnsiString;
                colvarReadAuthPerson.MaxLength = 2000;
                colvarReadAuthPerson.AutoIncrement = false;
                colvarReadAuthPerson.IsNullable = true;
                colvarReadAuthPerson.IsPrimaryKey = false;
                colvarReadAuthPerson.IsForeignKey = false;
                colvarReadAuthPerson.IsReadOnly = false;
                colvarReadAuthPerson.DefaultSetting = @"";
                colvarReadAuthPerson.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReadAuthPerson);

                TableSchema.TableColumn colvarEditAuth = new TableSchema.TableColumn(schema);
                colvarEditAuth.ColumnName = "edit_auth";
                colvarEditAuth.DataType = DbType.AnsiString;
                colvarEditAuth.MaxLength = 2000;
                colvarEditAuth.AutoIncrement = false;
                colvarEditAuth.IsNullable = true;
                colvarEditAuth.IsPrimaryKey = false;
                colvarEditAuth.IsForeignKey = false;
                colvarEditAuth.IsReadOnly = false;
                colvarEditAuth.DefaultSetting = @"";
                colvarEditAuth.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEditAuth);

                TableSchema.TableColumn colvarEditAuthPerson = new TableSchema.TableColumn(schema);
                colvarEditAuthPerson.ColumnName = "edit_auth_person";
                colvarEditAuthPerson.DataType = DbType.AnsiString;
                colvarEditAuthPerson.MaxLength = 2000;
                colvarEditAuthPerson.AutoIncrement = false;
                colvarEditAuthPerson.IsNullable = true;
                colvarEditAuthPerson.IsPrimaryKey = false;
                colvarEditAuthPerson.IsForeignKey = false;
                colvarEditAuthPerson.IsReadOnly = false;
                colvarEditAuthPerson.DefaultSetting = @"";
                colvarEditAuthPerson.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEditAuthPerson);

                TableSchema.TableColumn colvarNetStatus = new TableSchema.TableColumn(schema);
                colvarNetStatus.ColumnName = "net_status";
                colvarNetStatus.DataType = DbType.Int32;
                colvarNetStatus.MaxLength = 0;
                colvarNetStatus.AutoIncrement = false;
                colvarNetStatus.IsNullable = false;
                colvarNetStatus.IsPrimaryKey = false;
                colvarNetStatus.IsForeignKey = false;
                colvarNetStatus.IsReadOnly = false;
                colvarNetStatus.DefaultSetting = @"";
                colvarNetStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarNetStatus);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("document_list", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("CategoryGuid")]
        [Bindable(true)]
        public Guid CategoryGuid
        {
            get { return GetColumnValue<Guid>(Columns.CategoryGuid); }
            set { SetColumnValue(Columns.CategoryGuid, value); }
        }

        [XmlAttribute("Url")]
        [Bindable(true)]
        public string Url
        {
            get { return GetColumnValue<string>(Columns.Url); }
            set { SetColumnValue(Columns.Url, value); }
        }

        [XmlAttribute("DocName")]
        [Bindable(true)]
        public string DocName
        {
            get { return GetColumnValue<string>(Columns.DocName); }
            set { SetColumnValue(Columns.DocName, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime
        {
            get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ReadAuth")]
        [Bindable(true)]
        public string ReadAuth
        {
            get { return GetColumnValue<string>(Columns.ReadAuth); }
            set { SetColumnValue(Columns.ReadAuth, value); }
        }

        [XmlAttribute("ReadAuthPerson")]
        [Bindable(true)]
        public string ReadAuthPerson
        {
            get { return GetColumnValue<string>(Columns.ReadAuthPerson); }
            set { SetColumnValue(Columns.ReadAuthPerson, value); }
        }

        [XmlAttribute("EditAuth")]
        [Bindable(true)]
        public string EditAuth
        {
            get { return GetColumnValue<string>(Columns.EditAuth); }
            set { SetColumnValue(Columns.EditAuth, value); }
        }

        [XmlAttribute("EditAuthPerson")]
        [Bindable(true)]
        public string EditAuthPerson
        {
            get { return GetColumnValue<string>(Columns.EditAuthPerson); }
            set { SetColumnValue(Columns.EditAuthPerson, value); }
        }

        [XmlAttribute("NetStatus")]
        [Bindable(true)]
        public int NetStatus
        {
            get { return GetColumnValue<int>(Columns.NetStatus); }
            set { SetColumnValue(Columns.NetStatus, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn CategoryGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn UrlColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn DocNameColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ReadAuthColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn ReadAuthPersonColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn EditAuthColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn EditAuthPersonColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn NetStatusColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[13]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"guid";
            public static string CategoryGuid = @"category_guid";
            public static string Url = @"url";
            public static string DocName = @"doc_name";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string ModifyTime = @"modify_time";
            public static string ModifyId = @"modify_id";
            public static string ReadAuth = @"read_auth";
            public static string ReadAuthPerson = @"read_auth_person";
            public static string EditAuth = @"edit_auth";
            public static string EditAuthPerson = @"edit_auth_person";
            public static string NetStatus = @"net_status";
            public static string Status = @"status";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

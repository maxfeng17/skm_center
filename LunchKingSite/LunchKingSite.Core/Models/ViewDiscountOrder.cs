using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewDiscountOrder class.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountOrderCollection : ReadOnlyList<ViewDiscountOrder, ViewDiscountOrderCollection>
    {        
        public ViewDiscountOrderCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_discount_order view.
    /// </summary>
    [Serializable]
    public partial class ViewDiscountOrder : ReadOnlyRecord<ViewDiscountOrder>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_discount_order", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarCampaignId = new TableSchema.TableColumn(schema);
                colvarCampaignId.ColumnName = "campaign_id";
                colvarCampaignId.DataType = DbType.Int32;
                colvarCampaignId.MaxLength = 0;
                colvarCampaignId.AutoIncrement = false;
                colvarCampaignId.IsNullable = true;
                colvarCampaignId.IsPrimaryKey = false;
                colvarCampaignId.IsForeignKey = false;
                colvarCampaignId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCampaignId);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 20;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = true;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarUseTime = new TableSchema.TableColumn(schema);
                colvarUseTime.ColumnName = "use_time";
                colvarUseTime.DataType = DbType.DateTime;
                colvarUseTime.MaxLength = 0;
                colvarUseTime.AutoIncrement = false;
                colvarUseTime.IsNullable = true;
                colvarUseTime.IsPrimaryKey = false;
                colvarUseTime.IsForeignKey = false;
                colvarUseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseTime);
                
                TableSchema.TableColumn colvarUseAmount = new TableSchema.TableColumn(schema);
                colvarUseAmount.ColumnName = "use_amount";
                colvarUseAmount.DataType = DbType.Currency;
                colvarUseAmount.MaxLength = 0;
                colvarUseAmount.AutoIncrement = false;
                colvarUseAmount.IsNullable = true;
                colvarUseAmount.IsPrimaryKey = false;
                colvarUseAmount.IsForeignKey = false;
                colvarUseAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseAmount);
                
                TableSchema.TableColumn colvarUseId = new TableSchema.TableColumn(schema);
                colvarUseId.ColumnName = "use_id";
                colvarUseId.DataType = DbType.Int32;
                colvarUseId.MaxLength = 0;
                colvarUseId.AutoIncrement = false;
                colvarUseId.IsNullable = true;
                colvarUseId.IsPrimaryKey = false;
                colvarUseId.IsForeignKey = false;
                colvarUseId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderAmount = new TableSchema.TableColumn(schema);
                colvarOrderAmount.ColumnName = "order_amount";
                colvarOrderAmount.DataType = DbType.Currency;
                colvarOrderAmount.MaxLength = 0;
                colvarOrderAmount.AutoIncrement = false;
                colvarOrderAmount.IsNullable = true;
                colvarOrderAmount.IsPrimaryKey = false;
                colvarOrderAmount.IsForeignKey = false;
                colvarOrderAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderAmount);
                
                TableSchema.TableColumn colvarCancelTime = new TableSchema.TableColumn(schema);
                colvarCancelTime.ColumnName = "cancel_time";
                colvarCancelTime.DataType = DbType.DateTime;
                colvarCancelTime.MaxLength = 0;
                colvarCancelTime.AutoIncrement = false;
                colvarCancelTime.IsNullable = true;
                colvarCancelTime.IsPrimaryKey = false;
                colvarCancelTime.IsForeignKey = false;
                colvarCancelTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCancelTime);
                
                TableSchema.TableColumn colvarOrderCost = new TableSchema.TableColumn(schema);
                colvarOrderCost.ColumnName = "order_cost";
                colvarOrderCost.DataType = DbType.Currency;
                colvarOrderCost.MaxLength = 0;
                colvarOrderCost.AutoIncrement = false;
                colvarOrderCost.IsNullable = true;
                colvarOrderCost.IsPrimaryKey = false;
                colvarOrderCost.IsForeignKey = false;
                colvarOrderCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCost);
                
                TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
                colvarOrderClassification.ColumnName = "order_classification";
                colvarOrderClassification.DataType = DbType.Int32;
                colvarOrderClassification.MaxLength = 0;
                colvarOrderClassification.AutoIncrement = false;
                colvarOrderClassification.IsNullable = true;
                colvarOrderClassification.IsPrimaryKey = false;
                colvarOrderClassification.IsForeignKey = false;
                colvarOrderClassification.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderClassification);
                
                TableSchema.TableColumn colvarReceivableId = new TableSchema.TableColumn(schema);
                colvarReceivableId.ColumnName = "receivable_id";
                colvarReceivableId.DataType = DbType.Int32;
                colvarReceivableId.MaxLength = 0;
                colvarReceivableId.AutoIncrement = false;
                colvarReceivableId.IsNullable = true;
                colvarReceivableId.IsPrimaryKey = false;
                colvarReceivableId.IsForeignKey = false;
                colvarReceivableId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceivableId);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 100;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
                colvarQty.ColumnName = "qty";
                colvarQty.DataType = DbType.Int32;
                colvarQty.MaxLength = 0;
                colvarQty.AutoIncrement = false;
                colvarQty.IsNullable = true;
                colvarQty.IsPrimaryKey = false;
                colvarQty.IsForeignKey = false;
                colvarQty.IsReadOnly = false;
                
                schema.Columns.Add(colvarQty);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarApplyTime = new TableSchema.TableColumn(schema);
                colvarApplyTime.ColumnName = "apply_time";
                colvarApplyTime.DataType = DbType.DateTime;
                colvarApplyTime.MaxLength = 0;
                colvarApplyTime.AutoIncrement = false;
                colvarApplyTime.IsNullable = true;
                colvarApplyTime.IsPrimaryKey = false;
                colvarApplyTime.IsForeignKey = false;
                colvarApplyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarApplyTime);
                
                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = true;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartTime);
                
                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = true;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndTime);
                
                TableSchema.TableColumn colvarCampaignCancelTime = new TableSchema.TableColumn(schema);
                colvarCampaignCancelTime.ColumnName = "campaign_cancel_time";
                colvarCampaignCancelTime.DataType = DbType.DateTime;
                colvarCampaignCancelTime.MaxLength = 0;
                colvarCampaignCancelTime.AutoIncrement = false;
                colvarCampaignCancelTime.IsNullable = true;
                colvarCampaignCancelTime.IsPrimaryKey = false;
                colvarCampaignCancelTime.IsForeignKey = false;
                colvarCampaignCancelTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCampaignCancelTime);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarApplyId = new TableSchema.TableColumn(schema);
                colvarApplyId.ColumnName = "apply_id";
                colvarApplyId.DataType = DbType.AnsiString;
                colvarApplyId.MaxLength = 50;
                colvarApplyId.AutoIncrement = false;
                colvarApplyId.IsNullable = true;
                colvarApplyId.IsPrimaryKey = false;
                colvarApplyId.IsForeignKey = false;
                colvarApplyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarApplyId);
                
                TableSchema.TableColumn colvarCancelId = new TableSchema.TableColumn(schema);
                colvarCancelId.ColumnName = "cancel_id";
                colvarCancelId.DataType = DbType.AnsiString;
                colvarCancelId.MaxLength = 50;
                colvarCancelId.AutoIncrement = false;
                colvarCancelId.IsNullable = true;
                colvarCancelId.IsPrimaryKey = false;
                colvarCancelId.IsForeignKey = false;
                colvarCancelId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCancelId);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarDiscountType = new TableSchema.TableColumn(schema);
                colvarDiscountType.ColumnName = "discount_type";
                colvarDiscountType.DataType = DbType.Int32;
                colvarDiscountType.MaxLength = 0;
                colvarDiscountType.AutoIncrement = false;
                colvarDiscountType.IsNullable = true;
                colvarDiscountType.IsPrimaryKey = false;
                colvarDiscountType.IsForeignKey = false;
                colvarDiscountType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountType);
                
                TableSchema.TableColumn colvarOwner = new TableSchema.TableColumn(schema);
                colvarOwner.ColumnName = "owner";
                colvarOwner.DataType = DbType.Int32;
                colvarOwner.MaxLength = 0;
                colvarOwner.AutoIncrement = false;
                colvarOwner.IsNullable = true;
                colvarOwner.IsPrimaryKey = false;
                colvarOwner.IsForeignKey = false;
                colvarOwner.IsReadOnly = false;
                
                schema.Columns.Add(colvarOwner);
                
                TableSchema.TableColumn colvarOwnerName = new TableSchema.TableColumn(schema);
                colvarOwnerName.ColumnName = "owner_name";
                colvarOwnerName.DataType = DbType.String;
                colvarOwnerName.MaxLength = 256;
                colvarOwnerName.AutoIncrement = false;
                colvarOwnerName.IsNullable = true;
                colvarOwnerName.IsPrimaryKey = false;
                colvarOwnerName.IsForeignKey = false;
                colvarOwnerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarOwnerName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_discount_order",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewDiscountOrder()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewDiscountOrder(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewDiscountOrder(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewDiscountOrder(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("CampaignId")]
        [Bindable(true)]
        public int? CampaignId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("campaign_id");
		    }
            set 
		    {
			    SetColumnValue("campaign_id", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal? Amount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("amount");
		    }
            set 
		    {
			    SetColumnValue("amount", value);
            }
        }
	      
        [XmlAttribute("UseTime")]
        [Bindable(true)]
        public DateTime? UseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_time");
		    }
            set 
		    {
			    SetColumnValue("use_time", value);
            }
        }
	      
        [XmlAttribute("UseAmount")]
        [Bindable(true)]
        public decimal? UseAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("use_amount");
		    }
            set 
		    {
			    SetColumnValue("use_amount", value);
            }
        }
	      
        [XmlAttribute("UseId")]
        [Bindable(true)]
        public int? UseId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("use_id");
		    }
            set 
		    {
			    SetColumnValue("use_id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderAmount")]
        [Bindable(true)]
        public decimal? OrderAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("order_amount");
		    }
            set 
		    {
			    SetColumnValue("order_amount", value);
            }
        }
	      
        [XmlAttribute("CancelTime")]
        [Bindable(true)]
        public DateTime? CancelTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("cancel_time");
		    }
            set 
		    {
			    SetColumnValue("cancel_time", value);
            }
        }
	      
        [XmlAttribute("OrderCost")]
        [Bindable(true)]
        public decimal? OrderCost 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("order_cost");
		    }
            set 
		    {
			    SetColumnValue("order_cost", value);
            }
        }
	      
        [XmlAttribute("OrderClassification")]
        [Bindable(true)]
        public int? OrderClassification 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_classification");
		    }
            set 
		    {
			    SetColumnValue("order_classification", value);
            }
        }
	      
        [XmlAttribute("ReceivableId")]
        [Bindable(true)]
        public int? ReceivableId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("receivable_id");
		    }
            set 
		    {
			    SetColumnValue("receivable_id", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("Qty")]
        [Bindable(true)]
        public int? Qty 
	    {
		    get
		    {
			    return GetColumnValue<int?>("qty");
		    }
            set 
		    {
			    SetColumnValue("qty", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal? Total 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("ApplyTime")]
        [Bindable(true)]
        public DateTime? ApplyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("apply_time");
		    }
            set 
		    {
			    SetColumnValue("apply_time", value);
            }
        }
	      
        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime? StartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_time");
		    }
            set 
		    {
			    SetColumnValue("start_time", value);
            }
        }
	      
        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime? EndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_time");
		    }
            set 
		    {
			    SetColumnValue("end_time", value);
            }
        }
	      
        [XmlAttribute("CampaignCancelTime")]
        [Bindable(true)]
        public DateTime? CampaignCancelTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("campaign_cancel_time");
		    }
            set 
		    {
			    SetColumnValue("campaign_cancel_time", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("ApplyId")]
        [Bindable(true)]
        public string ApplyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("apply_id");
		    }
            set 
		    {
			    SetColumnValue("apply_id", value);
            }
        }
	      
        [XmlAttribute("CancelId")]
        [Bindable(true)]
        public string CancelId 
	    {
		    get
		    {
			    return GetColumnValue<string>("cancel_id");
		    }
            set 
		    {
			    SetColumnValue("cancel_id", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("DiscountType")]
        [Bindable(true)]
        public int? DiscountType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("discount_type");
		    }
            set 
		    {
			    SetColumnValue("discount_type", value);
            }
        }
	      
        [XmlAttribute("Owner")]
        [Bindable(true)]
        public int? Owner 
	    {
		    get
		    {
			    return GetColumnValue<int?>("owner");
		    }
            set 
		    {
			    SetColumnValue("owner", value);
            }
        }
	      
        [XmlAttribute("OwnerName")]
        [Bindable(true)]
        public string OwnerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("owner_name");
		    }
            set 
		    {
			    SetColumnValue("owner_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string CampaignId = @"campaign_id";
            
            public static string Code = @"code";
            
            public static string Amount = @"amount";
            
            public static string UseTime = @"use_time";
            
            public static string UseAmount = @"use_amount";
            
            public static string UseId = @"use_id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string OrderAmount = @"order_amount";
            
            public static string CancelTime = @"cancel_time";
            
            public static string OrderCost = @"order_cost";
            
            public static string OrderClassification = @"order_classification";
            
            public static string ReceivableId = @"receivable_id";
            
            public static string Name = @"name";
            
            public static string Qty = @"qty";
            
            public static string Total = @"total";
            
            public static string ApplyTime = @"apply_time";
            
            public static string StartTime = @"start_time";
            
            public static string EndTime = @"end_time";
            
            public static string CampaignCancelTime = @"campaign_cancel_time";
            
            public static string CreateTime = @"create_time";
            
            public static string CreateId = @"create_id";
            
            public static string ApplyId = @"apply_id";
            
            public static string CancelId = @"cancel_id";
            
            public static string OrderId = @"order_id";
            
            public static string DiscountType = @"discount_type";
            
            public static string Owner = @"owner";
            
            public static string OwnerName = @"owner_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealTimeSlot class.
	/// </summary>
    [Serializable]
	public partial class DealTimeSlotCollection : RepositoryList<DealTimeSlot, DealTimeSlotCollection>
	{
        public DealTimeSlotCollection() { }
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealTimeSlotCollection</returns>
		public DealTimeSlotCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealTimeSlot o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_time_slot table.
	/// </summary>
	[Serializable]
	public partial class DealTimeSlot : RepositoryRecord<DealTimeSlot>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealTimeSlot()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }

        public DealTimeSlot(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("deal_time_slot", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_GUID";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = true;
                colvarBusinessHourGuid.IsForeignKey = true;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";
                colvarBusinessHourGuid.ForeignKeyTableName = "business_hour";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = true;
                colvarCityId.IsForeignKey = true;
                colvarCityId.IsReadOnly = false;
                colvarCityId.DefaultSetting = @"";
                colvarCityId.ForeignKeyTableName = "city";
                schema.Columns.Add(colvarCityId);

                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = false;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                colvarSequence.DefaultSetting = @"";
                colvarSequence.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSequence);

                TableSchema.TableColumn colvarEffectiveStart = new TableSchema.TableColumn(schema);
                colvarEffectiveStart.ColumnName = "effective_start";
                colvarEffectiveStart.DataType = DbType.DateTime;
                colvarEffectiveStart.MaxLength = 0;
                colvarEffectiveStart.AutoIncrement = false;
                colvarEffectiveStart.IsNullable = false;
                colvarEffectiveStart.IsPrimaryKey = true;
                colvarEffectiveStart.IsForeignKey = false;
                colvarEffectiveStart.IsReadOnly = false;
                colvarEffectiveStart.DefaultSetting = @"";
                colvarEffectiveStart.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEffectiveStart);

                TableSchema.TableColumn colvarEffectiveEnd = new TableSchema.TableColumn(schema);
                colvarEffectiveEnd.ColumnName = "effective_end";
                colvarEffectiveEnd.DataType = DbType.DateTime;
                colvarEffectiveEnd.MaxLength = 0;
                colvarEffectiveEnd.AutoIncrement = false;
                colvarEffectiveEnd.IsNullable = false;
                colvarEffectiveEnd.IsPrimaryKey = false;
                colvarEffectiveEnd.IsForeignKey = false;
                colvarEffectiveEnd.IsReadOnly = false;
                colvarEffectiveEnd.DefaultSetting = @"";
                colvarEffectiveEnd.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEffectiveEnd);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarIsInTurn = new TableSchema.TableColumn(schema);
                colvarIsInTurn.ColumnName = "is_in_turn";
                colvarIsInTurn.DataType = DbType.Boolean;
                colvarIsInTurn.MaxLength = 0;
                colvarIsInTurn.AutoIncrement = false;
                colvarIsInTurn.IsNullable = false;
                colvarIsInTurn.IsPrimaryKey = false;
                colvarIsInTurn.IsForeignKey = false;
                colvarIsInTurn.IsReadOnly = false;
                colvarIsInTurn.DefaultSetting = @"((0))";
                colvarIsInTurn.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsInTurn);

                TableSchema.TableColumn colvarIsLockSeq = new TableSchema.TableColumn(schema);
                colvarIsLockSeq.ColumnName = "is_lock_seq";
                colvarIsLockSeq.DataType = DbType.Boolean;
                colvarIsLockSeq.MaxLength = 0;
                colvarIsLockSeq.AutoIncrement = false;
                colvarIsLockSeq.IsNullable = false;
                colvarIsLockSeq.IsPrimaryKey = false;
                colvarIsLockSeq.IsForeignKey = false;
                colvarIsLockSeq.IsReadOnly = false;
                colvarIsLockSeq.DefaultSetting = @"((0))";
                colvarIsLockSeq.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsLockSeq);

                TableSchema.TableColumn colvarIsHotDeal = new TableSchema.TableColumn(schema);
                colvarIsHotDeal.ColumnName = "is_hot_deal";
                colvarIsHotDeal.DataType = DbType.Boolean;
                colvarIsHotDeal.MaxLength = 0;
                colvarIsHotDeal.AutoIncrement = false;
                colvarIsHotDeal.IsNullable = false;
                colvarIsHotDeal.IsPrimaryKey = false;
                colvarIsHotDeal.IsForeignKey = false;
                colvarIsHotDeal.IsReadOnly = false; colvarIsHotDeal.DefaultSetting = @"((0))";
                colvarIsHotDeal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsHotDeal);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("deal_time_slot", schema);
            }
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int CityId 
		{
			get { return GetColumnValue<int>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("Sequence")]
		[Bindable(true)]
		public int Sequence 
		{
			get { return GetColumnValue<int>(Columns.Sequence); }
			set { SetColumnValue(Columns.Sequence, value); }
		}
		  
		[XmlAttribute("EffectiveStart")]
		[Bindable(true)]
		public DateTime EffectiveStart 
		{
			get { return GetColumnValue<DateTime>(Columns.EffectiveStart); }
			set { SetColumnValue(Columns.EffectiveStart, value); }
		}
		  
		[XmlAttribute("EffectiveEnd")]
		[Bindable(true)]
		public DateTime EffectiveEnd 
		{
			get { return GetColumnValue<DateTime>(Columns.EffectiveEnd); }
			set { SetColumnValue(Columns.EffectiveEnd, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("IsInTurn")]
		[Bindable(true)]
		public bool IsInTurn 
		{
			get { return GetColumnValue<bool>(Columns.IsInTurn); }
			set { SetColumnValue(Columns.IsInTurn, value); }
		}
		  
		[XmlAttribute("IsLockSeq")]
		[Bindable(true)]
		public bool IsLockSeq 
		{
			get { return GetColumnValue<bool>(Columns.IsLockSeq); }
			set { SetColumnValue(Columns.IsLockSeq, value); }
		}
		  
		[XmlAttribute("IsHotDeal")]
		[Bindable(true)]
		public bool IsHotDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsHotDeal); }
			set { SetColumnValue(Columns.IsHotDeal, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn EffectiveStartColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EffectiveEndColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IsInTurnColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IsLockSeqColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IsHotDealColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string BusinessHourGuid = @"business_hour_GUID";
			 public static string CityId = @"city_id";
			 public static string Sequence = @"sequence";
			 public static string EffectiveStart = @"effective_start";
			 public static string EffectiveEnd = @"effective_end";
			 public static string Status = @"status";
			 public static string IsInTurn = @"is_in_turn";
			 public static string IsLockSeq = @"is_lock_seq";
			 public static string IsHotDeal = @"is_hot_deal";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

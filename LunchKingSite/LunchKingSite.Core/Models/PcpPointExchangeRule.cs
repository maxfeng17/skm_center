using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpPointExchangeRule class.
	/// </summary>
    [Serializable]
	public partial class PcpPointExchangeRuleCollection : RepositoryList<PcpPointExchangeRule, PcpPointExchangeRuleCollection>
	{	   
		public PcpPointExchangeRuleCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpPointExchangeRuleCollection</returns>
		public PcpPointExchangeRuleCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpPointExchangeRule o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_point_exchange_rule table.
	/// </summary>
	[Serializable]
	public partial class PcpPointExchangeRule : RepositoryRecord<PcpPointExchangeRule>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpPointExchangeRule()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpPointExchangeRule(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_point_exchange_rule", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
				colvarCardGroupId.ColumnName = "card_group_id";
				colvarCardGroupId.DataType = DbType.Int32;
				colvarCardGroupId.MaxLength = 0;
				colvarCardGroupId.AutoIncrement = false;
				colvarCardGroupId.IsNullable = false;
				colvarCardGroupId.IsPrimaryKey = false;
				colvarCardGroupId.IsForeignKey = false;
				colvarCardGroupId.IsReadOnly = false;
				colvarCardGroupId.DefaultSetting = @"";
				colvarCardGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardGroupId);
				
				TableSchema.TableColumn colvarPoint = new TableSchema.TableColumn(schema);
				colvarPoint.ColumnName = "point";
				colvarPoint.DataType = DbType.Int32;
				colvarPoint.MaxLength = 0;
				colvarPoint.AutoIncrement = false;
				colvarPoint.IsNullable = false;
				colvarPoint.IsPrimaryKey = false;
				colvarPoint.IsForeignKey = false;
				colvarPoint.IsReadOnly = false;
				colvarPoint.DefaultSetting = @"";
				colvarPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPoint);
				
				TableSchema.TableColumn colvarItemIdentityCode = new TableSchema.TableColumn(schema);
				colvarItemIdentityCode.ColumnName = "item_identity_code";
				colvarItemIdentityCode.DataType = DbType.AnsiString;
				colvarItemIdentityCode.MaxLength = 36;
				colvarItemIdentityCode.AutoIncrement = false;
				colvarItemIdentityCode.IsNullable = true;
				colvarItemIdentityCode.IsPrimaryKey = false;
				colvarItemIdentityCode.IsForeignKey = false;
				colvarItemIdentityCode.IsReadOnly = false;
				colvarItemIdentityCode.DefaultSetting = @"";
				colvarItemIdentityCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemIdentityCode);
				
				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 200;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = false;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = false;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				colvarEnabled.DefaultSetting = @"";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifiedId = new TableSchema.TableColumn(schema);
				colvarModifiedId.ColumnName = "modified_id";
				colvarModifiedId.DataType = DbType.Int32;
				colvarModifiedId.MaxLength = 0;
				colvarModifiedId.AutoIncrement = false;
				colvarModifiedId.IsNullable = true;
				colvarModifiedId.IsPrimaryKey = false;
				colvarModifiedId.IsForeignKey = false;
				colvarModifiedId.IsReadOnly = false;
				colvarModifiedId.DefaultSetting = @"";
				colvarModifiedId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedId);
				
				TableSchema.TableColumn colvarModifiedTime = new TableSchema.TableColumn(schema);
				colvarModifiedTime.ColumnName = "modified_time";
				colvarModifiedTime.DataType = DbType.DateTime;
				colvarModifiedTime.MaxLength = 0;
				colvarModifiedTime.AutoIncrement = false;
				colvarModifiedTime.IsNullable = true;
				colvarModifiedTime.IsPrimaryKey = false;
				colvarModifiedTime.IsForeignKey = false;
				colvarModifiedTime.IsReadOnly = false;
				colvarModifiedTime.DefaultSetting = @"";
				colvarModifiedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedTime);
				
				TableSchema.TableColumn colvarIsDelete = new TableSchema.TableColumn(schema);
				colvarIsDelete.ColumnName = "is_delete";
				colvarIsDelete.DataType = DbType.Boolean;
				colvarIsDelete.MaxLength = 0;
				colvarIsDelete.AutoIncrement = false;
				colvarIsDelete.IsNullable = false;
				colvarIsDelete.IsPrimaryKey = false;
				colvarIsDelete.IsForeignKey = false;
				colvarIsDelete.IsReadOnly = false;
				
						colvarIsDelete.DefaultSetting = @"((0))";
				colvarIsDelete.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDelete);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_point_exchange_rule",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CardGroupId")]
		[Bindable(true)]
		public int CardGroupId 
		{
			get { return GetColumnValue<int>(Columns.CardGroupId); }
			set { SetColumnValue(Columns.CardGroupId, value); }
		}
		  
		[XmlAttribute("Point")]
		[Bindable(true)]
		public int Point 
		{
			get { return GetColumnValue<int>(Columns.Point); }
			set { SetColumnValue(Columns.Point, value); }
		}
		  
		[XmlAttribute("ItemIdentityCode")]
		[Bindable(true)]
		public string ItemIdentityCode 
		{
			get { return GetColumnValue<string>(Columns.ItemIdentityCode); }
			set { SetColumnValue(Columns.ItemIdentityCode, value); }
		}
		  
		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName 
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool Enabled 
		{
			get { return GetColumnValue<bool>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifiedId")]
		[Bindable(true)]
		public int? ModifiedId 
		{
			get { return GetColumnValue<int?>(Columns.ModifiedId); }
			set { SetColumnValue(Columns.ModifiedId, value); }
		}
		  
		[XmlAttribute("ModifiedTime")]
		[Bindable(true)]
		public DateTime? ModifiedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifiedTime); }
			set { SetColumnValue(Columns.ModifiedTime, value); }
		}
		  
		[XmlAttribute("IsDelete")]
		[Bindable(true)]
		public bool IsDelete 
		{
			get { return GetColumnValue<bool>(Columns.IsDelete); }
			set { SetColumnValue(Columns.IsDelete, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CardGroupIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PointColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemIdentityCodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDeleteColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CardGroupId = @"card_group_id";
			 public static string Point = @"point";
			 public static string ItemIdentityCode = @"item_identity_code";
			 public static string ItemName = @"item_name";
			 public static string Enabled = @"enabled";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifiedId = @"modified_id";
			 public static string ModifiedTime = @"modified_time";
			 public static string IsDelete = @"is_delete";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

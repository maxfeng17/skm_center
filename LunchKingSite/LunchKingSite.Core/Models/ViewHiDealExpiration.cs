using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealExpiration class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealExpirationCollection : ReadOnlyList<ViewHiDealExpiration, ViewHiDealExpirationCollection>
    {        
        public ViewHiDealExpirationCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_expiration view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealExpiration : ReadOnlyRecord<ViewHiDealExpiration>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_expiration", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = false;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
                colvarProductGuid.ColumnName = "product_guid";
                colvarProductGuid.DataType = DbType.Guid;
                colvarProductGuid.MaxLength = 0;
                colvarProductGuid.AutoIncrement = false;
                colvarProductGuid.IsNullable = false;
                colvarProductGuid.IsPrimaryKey = false;
                colvarProductGuid.IsForeignKey = false;
                colvarProductGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductGuid);
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
                colvarUseStartTime.ColumnName = "use_start_time";
                colvarUseStartTime.DataType = DbType.DateTime;
                colvarUseStartTime.MaxLength = 0;
                colvarUseStartTime.AutoIncrement = false;
                colvarUseStartTime.IsNullable = true;
                colvarUseStartTime.IsPrimaryKey = false;
                colvarUseStartTime.IsForeignKey = false;
                colvarUseStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseStartTime);
                
                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.DateTime;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseEndTime);
                
                TableSchema.TableColumn colvarProductChangedExpireTime = new TableSchema.TableColumn(schema);
                colvarProductChangedExpireTime.ColumnName = "product_changed_expire_time";
                colvarProductChangedExpireTime.DataType = DbType.DateTime;
                colvarProductChangedExpireTime.MaxLength = 0;
                colvarProductChangedExpireTime.AutoIncrement = false;
                colvarProductChangedExpireTime.IsNullable = true;
                colvarProductChangedExpireTime.IsPrimaryKey = false;
                colvarProductChangedExpireTime.IsForeignKey = false;
                colvarProductChangedExpireTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductChangedExpireTime);
                
                TableSchema.TableColumn colvarSellerCloseDownDate = new TableSchema.TableColumn(schema);
                colvarSellerCloseDownDate.ColumnName = "seller_close_down_date";
                colvarSellerCloseDownDate.DataType = DbType.DateTime;
                colvarSellerCloseDownDate.MaxLength = 0;
                colvarSellerCloseDownDate.AutoIncrement = false;
                colvarSellerCloseDownDate.IsNullable = true;
                colvarSellerCloseDownDate.IsPrimaryKey = false;
                colvarSellerCloseDownDate.IsForeignKey = false;
                colvarSellerCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCloseDownDate);
                
                TableSchema.TableColumn colvarStoreCloseDownDate = new TableSchema.TableColumn(schema);
                colvarStoreCloseDownDate.ColumnName = "store_close_down_date";
                colvarStoreCloseDownDate.DataType = DbType.DateTime;
                colvarStoreCloseDownDate.MaxLength = 0;
                colvarStoreCloseDownDate.AutoIncrement = false;
                colvarStoreCloseDownDate.IsNullable = true;
                colvarStoreCloseDownDate.IsPrimaryKey = false;
                colvarStoreCloseDownDate.IsForeignKey = false;
                colvarStoreCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreCloseDownDate);
                
                TableSchema.TableColumn colvarVendorBillingModel = new TableSchema.TableColumn(schema);
                colvarVendorBillingModel.ColumnName = "vendor_billing_model";
                colvarVendorBillingModel.DataType = DbType.Int32;
                colvarVendorBillingModel.MaxLength = 0;
                colvarVendorBillingModel.AutoIncrement = false;
                colvarVendorBillingModel.IsNullable = false;
                colvarVendorBillingModel.IsPrimaryKey = false;
                colvarVendorBillingModel.IsForeignKey = false;
                colvarVendorBillingModel.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorBillingModel);
                
                TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
                colvarRemittanceType.ColumnName = "remittance_type";
                colvarRemittanceType.DataType = DbType.Int32;
                colvarRemittanceType.MaxLength = 0;
                colvarRemittanceType.AutoIncrement = false;
                colvarRemittanceType.IsNullable = false;
                colvarRemittanceType.IsPrimaryKey = false;
                colvarRemittanceType.IsForeignKey = false;
                colvarRemittanceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemittanceType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_expiration",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealExpiration()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealExpiration(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealExpiration(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealExpiration(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("ProductGuid")]
        [Bindable(true)]
        public Guid ProductGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("product_guid");
		    }
            set 
		    {
			    SetColumnValue("product_guid", value);
            }
        }
	      
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("UseStartTime")]
        [Bindable(true)]
        public DateTime? UseStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_start_time");
		    }
            set 
		    {
			    SetColumnValue("use_start_time", value);
            }
        }
	      
        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public DateTime? UseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_end_time");
		    }
            set 
		    {
			    SetColumnValue("use_end_time", value);
            }
        }
	      
        [XmlAttribute("ProductChangedExpireTime")]
        [Bindable(true)]
        public DateTime? ProductChangedExpireTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("product_changed_expire_time");
		    }
            set 
		    {
			    SetColumnValue("product_changed_expire_time", value);
            }
        }
	      
        [XmlAttribute("SellerCloseDownDate")]
        [Bindable(true)]
        public DateTime? SellerCloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("seller_close_down_date");
		    }
            set 
		    {
			    SetColumnValue("seller_close_down_date", value);
            }
        }
	      
        [XmlAttribute("StoreCloseDownDate")]
        [Bindable(true)]
        public DateTime? StoreCloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("store_close_down_date");
		    }
            set 
		    {
			    SetColumnValue("store_close_down_date", value);
            }
        }
	      
        [XmlAttribute("VendorBillingModel")]
        [Bindable(true)]
        public int VendorBillingModel 
	    {
		    get
		    {
			    return GetColumnValue<int>("vendor_billing_model");
		    }
            set 
		    {
			    SetColumnValue("vendor_billing_model", value);
            }
        }
	      
        [XmlAttribute("RemittanceType")]
        [Bindable(true)]
        public int RemittanceType 
	    {
		    get
		    {
			    return GetColumnValue<int>("remittance_type");
		    }
            set 
		    {
			    SetColumnValue("remittance_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ProductId = @"product_id";
            
            public static string ProductGuid = @"product_guid";
            
            public static string DealId = @"deal_id";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string StoreGuid = @"store_guid";
            
            public static string UseStartTime = @"use_start_time";
            
            public static string UseEndTime = @"use_end_time";
            
            public static string ProductChangedExpireTime = @"product_changed_expire_time";
            
            public static string SellerCloseDownDate = @"seller_close_down_date";
            
            public static string StoreCloseDownDate = @"store_close_down_date";
            
            public static string VendorBillingModel = @"vendor_billing_model";
            
            public static string RemittanceType = @"remittance_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewUserEvaluate class.
    /// </summary>
    [Serializable]
    public partial class ViewUserEvaluateCollection : ReadOnlyList<ViewUserEvaluate, ViewUserEvaluateCollection>
    {        
        public ViewUserEvaluateCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_user_evaluate view.
    /// </summary>
    [Serializable]
    public partial class ViewUserEvaluate : ReadOnlyRecord<ViewUserEvaluate>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_user_evaluate", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvar_1 = new TableSchema.TableColumn(schema);
                colvar_1.ColumnName = "1";
                colvar_1.DataType = DbType.Int32;
                colvar_1.MaxLength = 0;
                colvar_1.AutoIncrement = false;
                colvar_1.IsNullable = false;
                colvar_1.IsPrimaryKey = false;
                colvar_1.IsForeignKey = false;
                colvar_1.IsReadOnly = false;
                
                schema.Columns.Add(colvar_1);
                
                TableSchema.TableColumn colvar_2 = new TableSchema.TableColumn(schema);
                colvar_2.ColumnName = "2";
                colvar_2.DataType = DbType.Int32;
                colvar_2.MaxLength = 0;
                colvar_2.AutoIncrement = false;
                colvar_2.IsNullable = false;
                colvar_2.IsPrimaryKey = false;
                colvar_2.IsForeignKey = false;
                colvar_2.IsReadOnly = false;
                
                schema.Columns.Add(colvar_2);
                
                TableSchema.TableColumn colvar_3 = new TableSchema.TableColumn(schema);
                colvar_3.ColumnName = "3";
                colvar_3.DataType = DbType.Int32;
                colvar_3.MaxLength = 0;
                colvar_3.AutoIncrement = false;
                colvar_3.IsNullable = false;
                colvar_3.IsPrimaryKey = false;
                colvar_3.IsForeignKey = false;
                colvar_3.IsReadOnly = false;
                
                schema.Columns.Add(colvar_3);
                
                TableSchema.TableColumn colvar_4 = new TableSchema.TableColumn(schema);
                colvar_4.ColumnName = "4";
                colvar_4.DataType = DbType.Int32;
                colvar_4.MaxLength = 0;
                colvar_4.AutoIncrement = false;
                colvar_4.IsNullable = false;
                colvar_4.IsPrimaryKey = false;
                colvar_4.IsForeignKey = false;
                colvar_4.IsReadOnly = false;
                
                schema.Columns.Add(colvar_4);
                
                TableSchema.TableColumn colvarComment = new TableSchema.TableColumn(schema);
                colvarComment.ColumnName = "comment";
                colvarComment.DataType = DbType.String;
                colvarComment.MaxLength = 300;
                colvarComment.AutoIncrement = false;
                colvarComment.IsNullable = false;
                colvarComment.IsPrimaryKey = false;
                colvarComment.IsForeignKey = false;
                colvarComment.IsReadOnly = false;
                
                schema.Columns.Add(colvarComment);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_user_evaluate",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewUserEvaluate()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewUserEvaluate(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewUserEvaluate(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewUserEvaluate(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("trust_id");
		    }
            set 
		    {
			    SetColumnValue("trust_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("_1")]
        [Bindable(true)]
        public int _1 
	    {
		    get
		    {
			    return GetColumnValue<int>("1");
		    }
            set 
		    {
			    SetColumnValue("1", value);
            }
        }
	      
        [XmlAttribute("_2")]
        [Bindable(true)]
        public int _2 
	    {
		    get
		    {
			    return GetColumnValue<int>("2");
		    }
            set 
		    {
			    SetColumnValue("2", value);
            }
        }
	      
        [XmlAttribute("_3")]
        [Bindable(true)]
        public int _3 
	    {
		    get
		    {
			    return GetColumnValue<int>("3");
		    }
            set 
		    {
			    SetColumnValue("3", value);
            }
        }
	      
        [XmlAttribute("_4")]
        [Bindable(true)]
        public int _4 
	    {
		    get
		    {
			    return GetColumnValue<int>("4");
		    }
            set 
		    {
			    SetColumnValue("4", value);
            }
        }
	      
        [XmlAttribute("Comment")]
        [Bindable(true)]
        public string Comment 
	    {
		    get
		    {
			    return GetColumnValue<string>("comment");
		    }
            set 
		    {
			    SetColumnValue("comment", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string TrustId = @"trust_id";
            
            public static string CreateTime = @"create_time";
            
            public static string _1 = @"1";
            
            public static string _2 = @"2";
            
            public static string _3 = @"3";
            
            public static string _4 = @"4";
            
            public static string Comment = @"comment";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

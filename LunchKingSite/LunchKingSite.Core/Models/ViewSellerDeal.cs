using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewSellerDeal class.
    /// </summary>
    [Serializable]
    public partial class ViewSellerDealCollection : ReadOnlyList<ViewSellerDeal, ViewSellerDealCollection>
    {
        public ViewSellerDealCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_seller_deal view.
    /// </summary>
    [Serializable]
    public partial class ViewSellerDeal : ReadOnlyRecord<ViewSellerDeal>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_seller_deal", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
                colvarDepartment.ColumnName = "department";
                colvarDepartment.DataType = DbType.Int32;
                colvarDepartment.MaxLength = 0;
                colvarDepartment.AutoIncrement = false;
                colvarDepartment.IsNullable = false;
                colvarDepartment.IsPrimaryKey = false;
                colvarDepartment.IsForeignKey = false;
                colvarDepartment.IsReadOnly = false;

                schema.Columns.Add(colvarDepartment);

                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;

                schema.Columns.Add(colvarSellerId);

                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;

                schema.Columns.Add(colvarSellerName);

                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "CompanyName";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 50;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;

                schema.Columns.Add(colvarCompanyName);

                TableSchema.TableColumn colvarSignCompanyID = new TableSchema.TableColumn(schema);
                colvarSignCompanyID.ColumnName = "SignCompanyID";
                colvarSignCompanyID.DataType = DbType.AnsiString;
                colvarSignCompanyID.MaxLength = 20;
                colvarSignCompanyID.AutoIncrement = false;
                colvarSignCompanyID.IsNullable = true;
                colvarSignCompanyID.IsPrimaryKey = false;
                colvarSignCompanyID.IsForeignKey = false;
                colvarSignCompanyID.IsReadOnly = false;

                schema.Columns.Add(colvarSignCompanyID);

                TableSchema.TableColumn colvarIsCloseDown = new TableSchema.TableColumn(schema);
                colvarIsCloseDown.ColumnName = "is_close_down";
                colvarIsCloseDown.DataType = DbType.Boolean;
                colvarIsCloseDown.MaxLength = 0;
                colvarIsCloseDown.AutoIncrement = false;
                colvarIsCloseDown.IsNullable = false;
                colvarIsCloseDown.IsPrimaryKey = false;
                colvarIsCloseDown.IsForeignKey = false;
                colvarIsCloseDown.IsReadOnly = false;

                schema.Columns.Add(colvarIsCloseDown);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;

                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarDealGuid = new TableSchema.TableColumn(schema);
                colvarDealGuid.ColumnName = "deal_guid";
                colvarDealGuid.DataType = DbType.Guid;
                colvarDealGuid.MaxLength = 0;
                colvarDealGuid.AutoIncrement = false;
                colvarDealGuid.IsNullable = true;
                colvarDealGuid.IsPrimaryKey = false;
                colvarDealGuid.IsForeignKey = false;
                colvarDealGuid.IsReadOnly = false;

                schema.Columns.Add(colvarDealGuid);

                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = true;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;

                schema.Columns.Add(colvarDealId);

                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 250;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;

                schema.Columns.Add(colvarDealName);

                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;

                schema.Columns.Add(colvarDealStartTime);

                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;

                schema.Columns.Add(colvarDealEndTime);

                TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
                colvarUseStartTime.ColumnName = "use_start_time";
                colvarUseStartTime.DataType = DbType.DateTime;
                colvarUseStartTime.MaxLength = 0;
                colvarUseStartTime.AutoIncrement = false;
                colvarUseStartTime.IsNullable = true;
                colvarUseStartTime.IsPrimaryKey = false;
                colvarUseStartTime.IsForeignKey = false;
                colvarUseStartTime.IsReadOnly = false;

                schema.Columns.Add(colvarUseStartTime);

                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.DateTime;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;

                schema.Columns.Add(colvarUseEndTime);

                TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
                colvarSlug.ColumnName = "slug";
                colvarSlug.DataType = DbType.Int32;
                colvarSlug.MaxLength = 0;
                colvarSlug.AutoIncrement = false;
                colvarSlug.IsNullable = true;
                colvarSlug.IsPrimaryKey = false;
                colvarSlug.IsForeignKey = false;
                colvarSlug.IsReadOnly = false;

                schema.Columns.Add(colvarSlug);

                TableSchema.TableColumn colvarDealSource = new TableSchema.TableColumn(schema);
                colvarDealSource.ColumnName = "deal_source";
                colvarDealSource.DataType = DbType.Int32;
                colvarDealSource.MaxLength = 0;
                colvarDealSource.AutoIncrement = false;
                colvarDealSource.IsNullable = true;
                colvarDealSource.IsPrimaryKey = false;
                colvarDealSource.IsForeignKey = false;
                colvarDealSource.IsReadOnly = false;

                schema.Columns.Add(colvarDealSource);

                TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarChangedExpireDate.ColumnName = "changed_expire_date";
                colvarChangedExpireDate.DataType = DbType.DateTime;
                colvarChangedExpireDate.MaxLength = 0;
                colvarChangedExpireDate.AutoIncrement = false;
                colvarChangedExpireDate.IsNullable = true;
                colvarChangedExpireDate.IsPrimaryKey = false;
                colvarChangedExpireDate.IsForeignKey = false;
                colvarChangedExpireDate.IsReadOnly = false;

                schema.Columns.Add(colvarChangedExpireDate);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_seller_deal", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewSellerDeal()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewSellerDeal(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewSellerDeal(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewSellerDeal(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Department")]
        [Bindable(true)]
        public int Department
        {
            get
            {
                return GetColumnValue<int>("department");
            }
            set
            {
                SetColumnValue("department", value);
            }
        }

        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId
        {
            get
            {
                return GetColumnValue<string>("seller_id");
            }
            set
            {
                SetColumnValue("seller_id", value);
            }
        }

        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName
        {
            get
            {
                return GetColumnValue<string>("seller_name");
            }
            set
            {
                SetColumnValue("seller_name", value);
            }
        }

        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName
        {
            get
            {
                return GetColumnValue<string>("CompanyName");
            }
            set
            {
                SetColumnValue("CompanyName", value);
            }
        }

        [XmlAttribute("SignCompanyID")]
        [Bindable(true)]
        public string SignCompanyID
        {
            get
            {
                return GetColumnValue<string>("SignCompanyID");
            }
            set
            {
                SetColumnValue("SignCompanyID", value);
            }
        }

        [XmlAttribute("IsCloseDown")]
        [Bindable(true)]
        public bool IsCloseDown
        {
            get
            {
                return GetColumnValue<bool>("is_close_down");
            }
            set
            {
                SetColumnValue("is_close_down", value);
            }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid
        {
            get
            {
                return GetColumnValue<Guid>("seller_guid");
            }
            set
            {
                SetColumnValue("seller_guid", value);
            }
        }

        [XmlAttribute("DealGuid")]
        [Bindable(true)]
        public Guid? DealGuid
        {
            get
            {
                return GetColumnValue<Guid?>("deal_guid");
            }
            set
            {
                SetColumnValue("deal_guid", value);
            }
        }

        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int? DealId
        {
            get
            {
                return GetColumnValue<int?>("deal_id");
            }
            set
            {
                SetColumnValue("deal_id", value);
            }
        }

        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName
        {
            get
            {
                return GetColumnValue<string>("deal_name");
            }
            set
            {
                SetColumnValue("deal_name", value);
            }
        }

        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime
        {
            get
            {
                return GetColumnValue<DateTime?>("deal_start_time");
            }
            set
            {
                SetColumnValue("deal_start_time", value);
            }
        }

        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime
        {
            get
            {
                return GetColumnValue<DateTime?>("deal_end_time");
            }
            set
            {
                SetColumnValue("deal_end_time", value);
            }
        }

        [XmlAttribute("UseStartTime")]
        [Bindable(true)]
        public DateTime? UseStartTime
        {
            get
            {
                return GetColumnValue<DateTime?>("use_start_time");
            }
            set
            {
                SetColumnValue("use_start_time", value);
            }
        }

        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public DateTime? UseEndTime
        {
            get
            {
                return GetColumnValue<DateTime?>("use_end_time");
            }
            set
            {
                SetColumnValue("use_end_time", value);
            }
        }

        [XmlAttribute("Slug")]
        [Bindable(true)]
        public int? Slug
        {
            get
            {
                return GetColumnValue<int?>("slug");
            }
            set
            {
                SetColumnValue("slug", value);
            }
        }

        [XmlAttribute("DealSource")]
        [Bindable(true)]
        public int? DealSource
        {
            get
            {
                return GetColumnValue<int?>("deal_source");
            }
            set
            {
                SetColumnValue("deal_source", value);
            }
        }

        [XmlAttribute("ChangedExpireDate")]
        [Bindable(true)]
        public DateTime? ChangedExpireDate
        {
            get
            {
                return GetColumnValue<DateTime?>("changed_expire_date");
            }
            set
            {
                SetColumnValue("changed_expire_date", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Department = @"department";

            public static string SellerId = @"seller_id";

            public static string SellerName = @"seller_name";

            public static string CompanyName = @"CompanyName";

            public static string SignCompanyID = @"SignCompanyID";

            public static string IsCloseDown = @"is_close_down";

            public static string SellerGuid = @"seller_guid";

            public static string DealGuid = @"deal_guid";

            public static string DealId = @"deal_id";

            public static string DealName = @"deal_name";

            public static string DealStartTime = @"deal_start_time";

            public static string DealEndTime = @"deal_end_time";

            public static string UseStartTime = @"use_start_time";

            public static string UseEndTime = @"use_end_time";

            public static string Slug = @"slug";

            public static string DealSource = @"deal_source";

            public static string ChangedExpireDate = @"changed_expire_date";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

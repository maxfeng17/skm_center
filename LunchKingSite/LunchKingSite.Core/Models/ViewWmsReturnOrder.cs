using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewWmsReturnOrder class.
    /// </summary>
    [Serializable]
    public partial class ViewWmsReturnOrderCollection : ReadOnlyList<ViewWmsReturnOrder, ViewWmsReturnOrderCollection>
    {
        public ViewWmsReturnOrderCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_wms_return_order view.
    /// </summary>
    [Serializable]
    public partial class ViewWmsReturnOrder : ReadOnlyRecord<ViewWmsReturnOrder>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_wms_return_order", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;

                schema.Columns.Add(colvarSellerName);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;

                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarInfoGuid = new TableSchema.TableColumn(schema);
                colvarInfoGuid.ColumnName = "info_guid";
                colvarInfoGuid.DataType = DbType.Guid;
                colvarInfoGuid.MaxLength = 0;
                colvarInfoGuid.AutoIncrement = false;
                colvarInfoGuid.IsNullable = false;
                colvarInfoGuid.IsPrimaryKey = false;
                colvarInfoGuid.IsForeignKey = false;
                colvarInfoGuid.IsReadOnly = false;

                schema.Columns.Add(colvarInfoGuid);

                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_guid";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = false;
                colvarItemGuid.IsPrimaryKey = false;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;

                schema.Columns.Add(colvarItemGuid);

                TableSchema.TableColumn colvarReturnOrderGuid = new TableSchema.TableColumn(schema);
                colvarReturnOrderGuid.ColumnName = "return_order_guid";
                colvarReturnOrderGuid.DataType = DbType.Guid;
                colvarReturnOrderGuid.MaxLength = 0;
                colvarReturnOrderGuid.AutoIncrement = false;
                colvarReturnOrderGuid.IsNullable = false;
                colvarReturnOrderGuid.IsPrimaryKey = false;
                colvarReturnOrderGuid.IsForeignKey = false;
                colvarReturnOrderGuid.IsReadOnly = false;

                schema.Columns.Add(colvarReturnOrderGuid);

                TableSchema.TableColumn colvarPchomeProdId = new TableSchema.TableColumn(schema);
                colvarPchomeProdId.ColumnName = "pchome_prod_id";
                colvarPchomeProdId.DataType = DbType.String;
                colvarPchomeProdId.MaxLength = 50;
                colvarPchomeProdId.AutoIncrement = false;
                colvarPchomeProdId.IsNullable = true;
                colvarPchomeProdId.IsPrimaryKey = false;
                colvarPchomeProdId.IsForeignKey = false;
                colvarPchomeProdId.IsReadOnly = false;

                schema.Columns.Add(colvarPchomeProdId);

                TableSchema.TableColumn colvarPchomeReturnOrderId = new TableSchema.TableColumn(schema);
                colvarPchomeReturnOrderId.ColumnName = "pchome_return_order_id";
                colvarPchomeReturnOrderId.DataType = DbType.AnsiString;
                colvarPchomeReturnOrderId.MaxLength = 50;
                colvarPchomeReturnOrderId.AutoIncrement = false;
                colvarPchomeReturnOrderId.IsNullable = true;
                colvarPchomeReturnOrderId.IsPrimaryKey = false;
                colvarPchomeReturnOrderId.IsForeignKey = false;
                colvarPchomeReturnOrderId.IsReadOnly = false;

                schema.Columns.Add(colvarPchomeReturnOrderId);

                TableSchema.TableColumn colvarProductNo = new TableSchema.TableColumn(schema);
                colvarProductNo.ColumnName = "product_no";
                colvarProductNo.DataType = DbType.Int32;
                colvarProductNo.MaxLength = 0;
                colvarProductNo.AutoIncrement = false;
                colvarProductNo.IsNullable = false;
                colvarProductNo.IsPrimaryKey = false;
                colvarProductNo.IsForeignKey = false;
                colvarProductNo.IsReadOnly = false;

                schema.Columns.Add(colvarProductNo);

                TableSchema.TableColumn colvarProductBrandName = new TableSchema.TableColumn(schema);
                colvarProductBrandName.ColumnName = "product_brand_name";
                colvarProductBrandName.DataType = DbType.String;
                colvarProductBrandName.MaxLength = 70;
                colvarProductBrandName.AutoIncrement = false;
                colvarProductBrandName.IsNullable = false;
                colvarProductBrandName.IsPrimaryKey = false;
                colvarProductBrandName.IsForeignKey = false;
                colvarProductBrandName.IsReadOnly = false;

                schema.Columns.Add(colvarProductBrandName);

                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "product_name";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 200;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = false;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;

                schema.Columns.Add(colvarProductName);

                TableSchema.TableColumn colvarSpecName = new TableSchema.TableColumn(schema);
                colvarSpecName.ColumnName = "spec_name";
                colvarSpecName.DataType = DbType.String;
                colvarSpecName.MaxLength = 100;
                colvarSpecName.AutoIncrement = false;
                colvarSpecName.IsNullable = true;
                colvarSpecName.IsPrimaryKey = false;
                colvarSpecName.IsForeignKey = false;
                colvarSpecName.IsReadOnly = false;

                schema.Columns.Add(colvarSpecName);

                TableSchema.TableColumn colvarProductCode = new TableSchema.TableColumn(schema);
                colvarProductCode.ColumnName = "product_code";
                colvarProductCode.DataType = DbType.AnsiString;
                colvarProductCode.MaxLength = 50;
                colvarProductCode.AutoIncrement = false;
                colvarProductCode.IsNullable = true;
                colvarProductCode.IsPrimaryKey = false;
                colvarProductCode.IsForeignKey = false;
                colvarProductCode.IsReadOnly = false;

                schema.Columns.Add(colvarProductCode);

                TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
                colvarPrice.ColumnName = "price";
                colvarPrice.DataType = DbType.Int32;
                colvarPrice.MaxLength = 0;
                colvarPrice.AutoIncrement = false;
                colvarPrice.IsNullable = true;
                colvarPrice.IsPrimaryKey = false;
                colvarPrice.IsForeignKey = false;
                colvarPrice.IsReadOnly = false;

                schema.Columns.Add(colvarPrice);

                TableSchema.TableColumn colvarWarehouseType = new TableSchema.TableColumn(schema);
                colvarWarehouseType.ColumnName = "warehouse_type";
                colvarWarehouseType.DataType = DbType.Int32;
                colvarWarehouseType.MaxLength = 0;
                colvarWarehouseType.AutoIncrement = false;
                colvarWarehouseType.IsNullable = false;
                colvarWarehouseType.IsPrimaryKey = false;
                colvarWarehouseType.IsForeignKey = false;
                colvarWarehouseType.IsReadOnly = false;

                schema.Columns.Add(colvarWarehouseType);

                TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
                colvarQty.ColumnName = "qty";
                colvarQty.DataType = DbType.Int32;
                colvarQty.MaxLength = 0;
                colvarQty.AutoIncrement = false;
                colvarQty.IsNullable = false;
                colvarQty.IsPrimaryKey = false;
                colvarQty.IsForeignKey = false;
                colvarQty.IsReadOnly = false;

                schema.Columns.Add(colvarQty);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarSalesId = new TableSchema.TableColumn(schema);
                colvarSalesId.ColumnName = "sales_id";
                colvarSalesId.DataType = DbType.Int32;
                colvarSalesId.MaxLength = 0;
                colvarSalesId.AutoIncrement = false;
                colvarSalesId.IsNullable = false;
                colvarSalesId.IsPrimaryKey = false;
                colvarSalesId.IsForeignKey = false;
                colvarSalesId.IsReadOnly = false;

                schema.Columns.Add(colvarSalesId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarInvalidation = new TableSchema.TableColumn(schema);
                colvarInvalidation.ColumnName = "invalidation";
                colvarInvalidation.DataType = DbType.Int32;
                colvarInvalidation.MaxLength = 0;
                colvarInvalidation.AutoIncrement = false;
                colvarInvalidation.IsNullable = false;
                colvarInvalidation.IsPrimaryKey = false;
                colvarInvalidation.IsForeignKey = false;
                colvarInvalidation.IsReadOnly = false;

                schema.Columns.Add(colvarInvalidation);

                TableSchema.TableColumn colvarReason = new TableSchema.TableColumn(schema);
                colvarReason.ColumnName = "reason";
                colvarReason.DataType = DbType.String;
                colvarReason.MaxLength = 50;
                colvarReason.AutoIncrement = false;
                colvarReason.IsNullable = true;
                colvarReason.IsPrimaryKey = false;
                colvarReason.IsForeignKey = false;
                colvarReason.IsReadOnly = false;

                schema.Columns.Add(colvarReason);

                TableSchema.TableColumn colvarSource = new TableSchema.TableColumn(schema);
                colvarSource.ColumnName = "source";
                colvarSource.DataType = DbType.Int32;
                colvarSource.MaxLength = 0;
                colvarSource.AutoIncrement = false;
                colvarSource.IsNullable = false;
                colvarSource.IsPrimaryKey = false;
                colvarSource.IsForeignKey = false;
                colvarSource.IsReadOnly = false;

                schema.Columns.Add(colvarSource);

                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 1000;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = true;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;

                schema.Columns.Add(colvarRemark);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;

                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = true;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;

                schema.Columns.Add(colvarAccountId);

                TableSchema.TableColumn colvarErrorX = new TableSchema.TableColumn(schema);
                colvarErrorX.ColumnName = "error";
                colvarErrorX.DataType = DbType.String;
                colvarErrorX.MaxLength = 200;
                colvarErrorX.AutoIncrement = false;
                colvarErrorX.IsNullable = true;
                colvarErrorX.IsPrimaryKey = false;
                colvarErrorX.IsForeignKey = false;
                colvarErrorX.IsReadOnly = false;

                schema.Columns.Add(colvarErrorX);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_wms_return_order", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewWmsReturnOrder()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewWmsReturnOrder(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewWmsReturnOrder(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewWmsReturnOrder(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName
        {
            get
            {
                return GetColumnValue<string>("seller_name");
            }
            set
            {
                SetColumnValue("seller_name", value);
            }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid
        {
            get
            {
                return GetColumnValue<Guid>("seller_guid");
            }
            set
            {
                SetColumnValue("seller_guid", value);
            }
        }

        [XmlAttribute("InfoGuid")]
        [Bindable(true)]
        public Guid InfoGuid
        {
            get
            {
                return GetColumnValue<Guid>("info_guid");
            }
            set
            {
                SetColumnValue("info_guid", value);
            }
        }

        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid ItemGuid
        {
            get
            {
                return GetColumnValue<Guid>("item_guid");
            }
            set
            {
                SetColumnValue("item_guid", value);
            }
        }

        [XmlAttribute("ReturnOrderGuid")]
        [Bindable(true)]
        public Guid ReturnOrderGuid
        {
            get
            {
                return GetColumnValue<Guid>("return_order_guid");
            }
            set
            {
                SetColumnValue("return_order_guid", value);
            }
        }

        [XmlAttribute("PchomeProdId")]
        [Bindable(true)]
        public string PchomeProdId
        {
            get
            {
                return GetColumnValue<string>("pchome_prod_id");
            }
            set
            {
                SetColumnValue("pchome_prod_id", value);
            }
        }

        [XmlAttribute("PchomeReturnOrderId")]
        [Bindable(true)]
        public string PchomeReturnOrderId
        {
            get
            {
                return GetColumnValue<string>("pchome_return_order_id");
            }
            set
            {
                SetColumnValue("pchome_return_order_id", value);
            }
        }

        [XmlAttribute("ProductNo")]
        [Bindable(true)]
        public int ProductNo
        {
            get
            {
                return GetColumnValue<int>("product_no");
            }
            set
            {
                SetColumnValue("product_no", value);
            }
        }

        [XmlAttribute("ProductBrandName")]
        [Bindable(true)]
        public string ProductBrandName
        {
            get
            {
                return GetColumnValue<string>("product_brand_name");
            }
            set
            {
                SetColumnValue("product_brand_name", value);
            }
        }

        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName
        {
            get
            {
                return GetColumnValue<string>("product_name");
            }
            set
            {
                SetColumnValue("product_name", value);
            }
        }

        [XmlAttribute("SpecName")]
        [Bindable(true)]
        public string SpecName
        {
            get
            {
                return GetColumnValue<string>("spec_name");
            }
            set
            {
                SetColumnValue("spec_name", value);
            }
        }

        [XmlAttribute("ProductCode")]
        [Bindable(true)]
        public string ProductCode
        {
            get
            {
                return GetColumnValue<string>("product_code");
            }
            set
            {
                SetColumnValue("product_code", value);
            }
        }

        [XmlAttribute("Price")]
        [Bindable(true)]
        public int? Price
        {
            get
            {
                return GetColumnValue<int?>("price");
            }
            set
            {
                SetColumnValue("price", value);
            }
        }

        [XmlAttribute("WarehouseType")]
        [Bindable(true)]
        public int WarehouseType
        {
            get
            {
                return GetColumnValue<int>("warehouse_type");
            }
            set
            {
                SetColumnValue("warehouse_type", value);
            }
        }

        [XmlAttribute("Qty")]
        [Bindable(true)]
        public int Qty
        {
            get
            {
                return GetColumnValue<int>("qty");
            }
            set
            {
                SetColumnValue("qty", value);
            }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get
            {
                return GetColumnValue<int>("status");
            }
            set
            {
                SetColumnValue("status", value);
            }
        }

        [XmlAttribute("SalesId")]
        [Bindable(true)]
        public int SalesId
        {
            get
            {
                return GetColumnValue<int>("sales_id");
            }
            set
            {
                SetColumnValue("sales_id", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("Invalidation")]
        [Bindable(true)]
        public int Invalidation
        {
            get
            {
                return GetColumnValue<int>("invalidation");
            }
            set
            {
                SetColumnValue("invalidation", value);
            }
        }

        [XmlAttribute("Reason")]
        [Bindable(true)]
        public string Reason
        {
            get
            {
                return GetColumnValue<string>("reason");
            }
            set
            {
                SetColumnValue("reason", value);
            }
        }

        [XmlAttribute("Source")]
        [Bindable(true)]
        public int Source
        {
            get
            {
                return GetColumnValue<int>("source");
            }
            set
            {
                SetColumnValue("source", value);
            }
        }

        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark
        {
            get
            {
                return GetColumnValue<string>("remark");
            }
            set
            {
                SetColumnValue("remark", value);
            }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get
            {
                return GetColumnValue<string>("create_id");
            }
            set
            {
                SetColumnValue("create_id", value);
            }
        }

        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId
        {
            get
            {
                return GetColumnValue<string>("account_id");
            }
            set
            {
                SetColumnValue("account_id", value);
            }
        }

        [XmlAttribute("ErrorX")]
        [Bindable(true)]
        public string ErrorX
        {
            get
            {
                return GetColumnValue<string>("error");
            }
            set
            {
                SetColumnValue("error", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string SellerName = @"seller_name";

            public static string SellerGuid = @"seller_guid";

            public static string InfoGuid = @"info_guid";

            public static string ItemGuid = @"item_guid";

            public static string ReturnOrderGuid = @"return_order_guid";

            public static string PchomeProdId = @"pchome_prod_id";

            public static string PchomeReturnOrderId = @"pchome_return_order_id";

            public static string ProductNo = @"product_no";

            public static string ProductBrandName = @"product_brand_name";

            public static string ProductName = @"product_name";

            public static string SpecName = @"spec_name";

            public static string ProductCode = @"product_code";

            public static string Price = @"price";

            public static string WarehouseType = @"warehouse_type";

            public static string Qty = @"qty";

            public static string Status = @"status";

            public static string SalesId = @"sales_id";

            public static string CreateTime = @"create_time";

            public static string Invalidation = @"invalidation";

            public static string Reason = @"reason";

            public static string Source = @"source";

            public static string Remark = @"remark";

            public static string CreateId = @"create_id";

            public static string AccountId = @"account_id";

            public static string ErrorX = @"error";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealCloudStorage class.
	/// </summary>
    [Serializable]
	public partial class DealCloudStorageCollection : RepositoryList<DealCloudStorage, DealCloudStorageCollection>
	{	   
		public DealCloudStorageCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealCloudStorageCollection</returns>
		public DealCloudStorageCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealCloudStorage o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_cloud_storage table.
	/// </summary>
	[Serializable]
	public partial class DealCloudStorage : RepositoryRecord<DealCloudStorage>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealCloudStorage()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DealCloudStorage(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("deal_cloud_storage", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarS3IsReady = new TableSchema.TableColumn(schema);
				colvarS3IsReady.ColumnName = "s3_is_ready";
				colvarS3IsReady.DataType = DbType.Boolean;
				colvarS3IsReady.MaxLength = 0;
				colvarS3IsReady.AutoIncrement = false;
				colvarS3IsReady.IsNullable = false;
				colvarS3IsReady.IsPrimaryKey = false;
				colvarS3IsReady.IsForeignKey = false;
				colvarS3IsReady.IsReadOnly = false;
				colvarS3IsReady.DefaultSetting = @"";
				colvarS3IsReady.ForeignKeyTableName = "";
				schema.Columns.Add(colvarS3IsReady);
				
				TableSchema.TableColumn colvarS3MarkSync = new TableSchema.TableColumn(schema);
				colvarS3MarkSync.ColumnName = "s3_mark_sync";
				colvarS3MarkSync.DataType = DbType.Boolean;
				colvarS3MarkSync.MaxLength = 0;
				colvarS3MarkSync.AutoIncrement = false;
				colvarS3MarkSync.IsNullable = false;
				colvarS3MarkSync.IsPrimaryKey = false;
				colvarS3MarkSync.IsForeignKey = false;
				colvarS3MarkSync.IsReadOnly = false;
				colvarS3MarkSync.DefaultSetting = @"";
				colvarS3MarkSync.ForeignKeyTableName = "";
				schema.Columns.Add(colvarS3MarkSync);
				
				TableSchema.TableColumn colvarS3MarkDelete = new TableSchema.TableColumn(schema);
				colvarS3MarkDelete.ColumnName = "s3_mark_delete";
				colvarS3MarkDelete.DataType = DbType.Boolean;
				colvarS3MarkDelete.MaxLength = 0;
				colvarS3MarkDelete.AutoIncrement = false;
				colvarS3MarkDelete.IsNullable = false;
				colvarS3MarkDelete.IsPrimaryKey = false;
				colvarS3MarkDelete.IsForeignKey = false;
				colvarS3MarkDelete.IsReadOnly = false;
				colvarS3MarkDelete.DefaultSetting = @"";
				colvarS3MarkDelete.ForeignKeyTableName = "";
				schema.Columns.Add(colvarS3MarkDelete);
				
				TableSchema.TableColumn colvarS3ContentDescription = new TableSchema.TableColumn(schema);
				colvarS3ContentDescription.ColumnName = "s3_content_description";
				colvarS3ContentDescription.DataType = DbType.String;
				colvarS3ContentDescription.MaxLength = 1073741823;
				colvarS3ContentDescription.AutoIncrement = false;
				colvarS3ContentDescription.IsNullable = false;
				colvarS3ContentDescription.IsPrimaryKey = false;
				colvarS3ContentDescription.IsForeignKey = false;
				colvarS3ContentDescription.IsReadOnly = false;
				colvarS3ContentDescription.DefaultSetting = @"";
				colvarS3ContentDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarS3ContentDescription);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = false;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("deal_cloud_storage",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		
		[XmlAttribute("S3IsReady")]
		[Bindable(true)]
		public bool S3IsReady 
		{
			get { return GetColumnValue<bool>(Columns.S3IsReady); }
			set { SetColumnValue(Columns.S3IsReady, value); }
		}
		
		[XmlAttribute("S3MarkSync")]
		[Bindable(true)]
		public bool S3MarkSync 
		{
			get { return GetColumnValue<bool>(Columns.S3MarkSync); }
			set { SetColumnValue(Columns.S3MarkSync, value); }
		}
		
		[XmlAttribute("S3MarkDelete")]
		[Bindable(true)]
		public bool S3MarkDelete 
		{
			get { return GetColumnValue<bool>(Columns.S3MarkDelete); }
			set { SetColumnValue(Columns.S3MarkDelete, value); }
		}
		
		[XmlAttribute("S3ContentDescription")]
		[Bindable(true)]
		public string S3ContentDescription 
		{
			get { return GetColumnValue<string>(Columns.S3ContentDescription); }
			set { SetColumnValue(Columns.S3ContentDescription, value); }
		}
		
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn S3IsReadyColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn S3MarkSyncColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn S3MarkDeleteColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn S3ContentDescriptionColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string S3IsReady = @"s3_is_ready";
			 public static string S3MarkSync = @"s3_mark_sync";
			 public static string S3MarkDelete = @"s3_mark_delete";
			 public static string S3ContentDescription = @"s3_content_description";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
